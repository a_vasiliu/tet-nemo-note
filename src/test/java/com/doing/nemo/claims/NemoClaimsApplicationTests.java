package com.doing.nemo.claims;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = NemoClaimsApplication.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class NemoClaimsApplicationTests {

    static {

        System.setProperty("syslog.server", "localhost");
        System.setProperty("syslog.facility", "SYSLOG");
    }


    @Test
    public void contextLoads() {
    }
}
