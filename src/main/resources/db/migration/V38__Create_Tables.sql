CREATE TABLE IF NOT EXISTS lawyer_payments (
	id UUID PRIMARY KEY,
	plate varchar (255) NULL,
	practice_id int8 NULL,
	date_accident timestamptz NULL,
	damage numeric NULL,
	technical_shutdown numeric NULL,
    fee numeric NULL,
    expenses_suit numeric NULL,
    acc_sale varchar(255) NULL,
    payment_type varchar(255) NULL,
    amount numeric NULL,
    transaction_number varchar(255) NULL,
    bank varchar(255) NULL,
    note text NULL,
    lawyer_code int8 NULL,
    status varchar(255) NULL
);
