ALTER SEQUENCE practice_id_sequence RESTART WITH 4000000;
ALTER TABLE email_template ADD COLUMN IF NOT EXISTS flows jsonb;

UPDATE email_template
SET flows = '["FCM","FNI","FUL"]';

UPDATE event_type
SET description = 'Affido al gestore sinistri'
WHERE id = '4d29a87d-3e5e-4333-b852-7380a50eacc1';

UPDATE email_template
SET application_event_type_id ='4d29a87d-3e5e-4333-b852-7380a50eacc1', flows = '["FCM"]'
WHERE id = 'b894fe3c-179a-4b99-8031-c70328a633fe';

UPDATE email_template
SET application_event_type_id ='4534e2ae-d450-4e56-8588-c392c042c529', flows = '["FCM"]'
WHERE id = '96533511-f03d-4976-8e6f-b8e58afe9271';

UPDATE email_template
SET application_event_type_id ='4534e2ae-d450-4e56-8588-c392c042c529', flows = '["FNI"]'
WHERE id = '28d143a9-6317-4b85-9046-6d529c6d838c';

UPDATE email_template
SET application_event_type_id ='43ace40d-7488-4d5f-9b66-3cc0cdff0ea0', flows = '["FCM"]'
WHERE id = 'e3a10778-c3e3-4c06-bbab-5c1e551b1a1e';

UPDATE email_template
SET application_event_type_id ='43ace40d-7488-4d5f-9b66-3cc0cdff0ea0', flows = '["FNI"]'
WHERE id = '112e9a55-4946-4031-9625-bdfa9251a24f';

DELETE from email_template_recipient_type WHERE email_template_id IN ('72678d7e-c20f-421f-ac93-72036e67d0ec','21a6f7b2-6f27-4f17-90a2-a90d4b0bf232','423f014b-1c6d-4814-b5c6-a05406e3eae5',
'041b2d17-eb45-4c6f-8202-99b63002f2f8',
'e7d53773-a4ee-47b0-90a7-40245bb58084');

DELETE FROM email_template WHERE id IN ('72678d7e-c20f-421f-ac93-72036e67d0ec','21a6f7b2-6f27-4f17-90a2-a90d4b0bf232','423f014b-1c6d-4814-b5c6-a05406e3eae5',
'041b2d17-eb45-4c6f-8202-99b63002f2f8',
'e7d53773-a4ee-47b0-90a7-40245bb58084');

UPDATE contract_type
SET cod_contract_type = '2LF'
WHERE id = '3d65e206-f8fb-4585-a81a-539a48fcaa59';
