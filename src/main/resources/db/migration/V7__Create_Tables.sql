CREATE TABLE IF NOT EXISTS lock (
  user_id varchar(255),
  claim_id varchar(255),
  now timestamptz
);

ALTER TABLE claims ADD COLUMN IF NOT EXISTS refund jsonb;
