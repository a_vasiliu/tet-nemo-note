ALTER TABLE claims ADD COLUMN IF NOT EXISTS is_read bool;
UPDATE insurance_company SET name = 'SOGESSUR - Sogessur' WHERE id = 'a83be7e2-4a58-11e9-8646-d663bd873d93';

ALTER TABLE insurance_manager DROP CONSTRAINT IF EXISTS uc_insurancemanager;
INSERT INTO insurance_manager(ID,code,name,address,zip_code,city,province,state,reference_person,telephone,fax,email,web_site,is_active) VALUES
('bcdc40f8-8ecd-437f-9e6b-2f1b3341b25e',NULL,'Armonie Assicurative Srl','Via Lanino 5','20144','Milano','MI','Italia',NULL,'02 42292226',NULL,'massimiliano.granieri@aldautomotive.com',NULL,'TRUE')
,('7f5d78be-c2b5-407e-aebe-4ce3fcd3a1a0','T','MARSH (Milano)','Viale Bodio 33','20158','Milano','MI','Italia',NULL,NULL,NULL,'motor_claims@marsh.com',NULL,'TRUE')
,('8e978f22-6864-42ad-b591-50e4a705ebe3','X','Multi Serass',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ald@multiserass.com',NULL,'TRUE')
,('9f972ad4-15bd-4e2b-ad2a-7e3769cdf1ca','Y','PCA','S.S. 211 della Lomellina n.3/A - Loc. S. Guglielmo','15057','Tortona','AL','IT','Dott.ssa Isabella Ferrari - Claims Account','0131-872531','0131-872507','riccardo.ricci@pcabroker.com',NULL,NULL)
,('56d041fa-c289-49b3-b63b-832124ab11fe','G','Crawford',NULL,NULL,'MILANO','MI',NULL,'PATRIZIA GIORDANO - P.Giordano@crawco.it','02.48100423',NULL,'gestionelilly@broadspire.it',NULL,'TRUE')
,('7e942bee-0db5-4ce2-9239-ec3078640610','Z','Willis Italia Spa',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'casoc@willis.com',NULL,'TRUE')
,('e077f817-fdc3-4e08-84c9-6b8300199548','V','AON -MI-',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'federico.guarino@aon.it',NULL,'TRUE')
,('c9dc69ac-0694-4557-b7c3-c12726724997',NULL,'Alco Gestione Rischi',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'massimiliano.granieri@aldautomotive.com',NULL,'TRUE')
,('b7a6e4a6-1e4a-47fa-8ea9-69458a5c23a2',NULL,'Car Assistance',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'massimiliano.granieri@aldautomotive.com',NULL,'TRUE')
,('2492806c-707e-4cec-929b-1cd11a11c46a','U','Marsh (RM)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'TRUE')
,('f366c687-7103-4b88-842a-c3c779a0fbda','W','AON (RM)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'TRUE')
,('dabe14aa-5152-431e-b4be-52cfbc7194ad','J','Zurich (Account manager per Sanofi A.e Kimberly Clark)',NULL,NULL,NULL,NULL,NULL,'Iacometti Sergio',NULL,NULL,'sinistri.auto@it.zurich.com',NULL,'TRUE')
,('1b279bd0-b7c8-4510-b625-9762912ae93c',NULL,'Dekra',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ald.claims.it@dekra.com',NULL,'TRUE')
,('cbef02e7-5843-4d8e-8a85-9317d6783c10',NULL,'Eurorisarcimenti','Via Lanzo 207','10071','Borgaro T.se','TO',NULL,NULL,'011.470.32.95',NULL,'sinistri.adp@eurorisarcimenti.it',NULL,'TRUE')
,('3a9c59b3-ff9e-43cd-a614-44b4888df5ca',NULL,'IT DL ROM Servizio e Dipendenti','Via Alexandre Gustave Eiffel 15','148','Roma','RM','Italia','Manuela Fenorasi','06 65685466','06 65685337','servizioedipendenti@aldautomotive.com',NULL,NULL)
,('5f517d39-d7a9-428d-8a2a-718d210cd68d',NULL,'AON  (Sony)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'luca.bilotto@aon.it',NULL,'TRUE')
,('ec5877dc-594d-402b-bf0d-f01dcf275954',NULL,'GROUPAMA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'sandro.bisogno@groupama.it',NULL,'TRUE')
,('1e3bb713-2206-4883-91fd-a968f25c5b0e',NULL,'Marsh Spa',NULL,NULL,'Roma','RM',NULL,'Sig.ra Diana Monda','06.54516376',NULL,'Diana.Monda@Marsh.com',NULL,'TRUE')
,('8c78555b-92e1-46f6-8513-7fca4b0de6ca','Z','Willis Italia',NULL,NULL,'roma',NULL,NULL,NULL,NULL,NULL,'ita_claimszurich@willis.com',NULL,'TRUE')
,('cb894da3-38e6-4068-8e99-10a351e0e219',NULL,'AXA (Mondelez)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'sinistri.broker@axa.it',NULL,'TRUE')
,('e57f40fc-3669-41df-b20b-f1dd87dd5118',NULL,'AIG (x ROCHE)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'sinistri.aig@multiserass.com',NULL,'TRUE')
,('4d8843a1-73a9-4509-8d78-8c1f874856e1','V','AON -MI- (alcatel)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'paola.lo.jacono@aon.it',NULL,'TRUE')
,('f85e8a8b-0c85-4540-a16a-8390dcda5337',NULL,'Willis Italia Unipol',NULL,NULL,'roma',NULL,NULL,NULL,NULL,NULL,'ita-ald@willis.com',NULL,'TRUE')
,('f7e3f3d3-d407-433a-9fb3-501027067495','J','Zurich canale diretto',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'sinistri.auto@it.zurich.com',NULL,'TRUE')
,('c891d2a5-f13b-4de3-a084-ff1fb34dec1b',NULL,'Willis Italia (Diversey)',NULL,NULL,'roma',NULL,NULL,NULL,NULL,NULL,'Chiara.Frizziero@WillisTowersWatson.com',NULL,'TRUE')
,('3f764fae-8254-41a6-9964-c15d3cab2c88',NULL,'Willis Italia MI','Via Tortona 33','20144','Milano','MI',NULL,NULL,'02 47787 448','02 47787 492','emanuela.re@willistowerswatson.com',NULL,'TRUE')
,('0390d50e-98a0-4a80-9f5a-efa9dcf314ee',NULL,'Multiserass (AIG)','Via Roncaglia 13','20146','MIlano','MI',NULL,NULL,'24654741',NULL,'sinistri.aig@multiserass.com',NULL,'TRUE')
,('0c00b2be-ad99-435f-9cd3-2ff232e59c11',NULL,'AON -JDE-',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'manuela.marzoni@aon.it',NULL,'TRUE')
,('ecde45a8-f4a9-414a-9e58-410a1a61e908',NULL,'MAG JLT','Via della Moscova 3','20121','Milano','MI','Italia','Gabriele Bernardini','02 62711772',NULL,'Gabriele.Bernardini@MagJlt.Com',NULL,'TRUE')
,('8065ccd6-1839-4527-a2a7-e7a3d5ea2e71',NULL,'AON -Roma',NULL,NULL,NULL,NULL,NULL,'ald_claims@aon.it','02/454341',NULL,'ALD_Claims@aon.it',NULL,'TRUE')
,('9bc84ee0-73fc-4454-82ff-08608056d74f',NULL,'AXA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'antonio.dilascia@axa.it',NULL,'TRUE')
,('3700b293-860b-4074-b4b2-0d94f8d8a340',NULL,'AXA MIROGLIO',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'sinistri.ag0301@axa-agenzie.it',NULL,'TRUE')
,('44adb96a-8c4c-4e5c-8f4e-9d1cc3392a45',NULL,'Marsh Vodafone',NULL,NULL,'Roma',NULL,NULL,'Sig.ra Diana Monda',NULL,NULL,'vdf.claim@marsh.com',NULL,'TRUE')
,('64060ace-619d-4db3-ad3f-1bca432df0dc','V','AON -MI2-',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'barbara.di.giulio@aon.it',NULL,'TRUE')
,('1c5f073d-01df-473e-ac86-b7995addbff7',NULL,'ABC QZ Broker',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'m.grimaldi@areabroker.it',NULL,'TRUE')
,('89055c3e-68af-420e-a217-18b679d8d6c6',NULL,'AON -Roma Unipol',NULL,NULL,NULL,NULL,NULL,'ald_claims@aon.it','02/454341',NULL,'ald_claims.unipolsai@aon.it',NULL,'TRUE')
,('6c82eb20-d1d4-4dd6-9ac5-80e97e289484',NULL,'BROADSPIRE (CHUBB)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'cms@broadspire.it',NULL,'TRUE');

INSERT INTO recipient_type VALUES
    ('26c8d12c-59e7-11e9-8647-d663bd873d93','Utente Inerimento Pratica','NO','info@solges.it','<%UINS_EMAIL%>',false,'USER_INPUT_PRACTICE','REPAIR'),
    ('26c8d3e8-59e7-11e9-8647-d663bd873d93','Utente Modifica Pratica','NO','info@solges.it','<%UAGG_EMAIL%>',false,'USER_MODIFICATION_PRACTICE','REPAIR'),
    ('26c8d564-59e7-11e9-8647-d663bd873d93','Operatore a cui è assegnata la Pratica','NO','info@solges.it','<%OPER_EMAIL%>',false,'OPERATOR_TO_WHOM_THE_PRACTICE','REPAIR'),
    ('26c8d794-59e7-11e9-8647-d663bd873d93','Conducente ( Controparte )','NO','info@solges.it','<%COND_EMAIL%>',false,'DRIVER_COUNTERPARTY','REPAIR'),
    ('26c8d910-59e7-11e9-8647-d663bd873d93','Assicurato ( Controparte )','NO','info@solges.it','<%ASSI_EMAIL%>',false,'INSURED_COUNTERPARTY','REPAIR'),
    ('26c8dcd0-59e7-11e9-8647-d663bd873d93','Compagnia Assicuratrice ( Controparte )','NO','info@solges.it','<%COMP_EMAIL%>',false,'INSURANCE_COMPANY_COUNTERPARTY','REPAIR'),
    ('26c8de2e-59e7-11e9-8647-d663bd873d93','Riparatore Pratica','NO','info@solges.it','<%RIPA_EMAIL%>',false,'PRACTICAL_REPAIRER','REPAIR'),
    ('26c8df64-59e7-11e9-8647-d663bd873d93','Gestore Sinistri Pratica','NO','info@solges.it','<%GEST_EMAIL%>',false,'CLAIMS_MANAGER_REPAIR','REPAIR'),
    ('26c8e090-59e7-11e9-8647-d663bd873d93','Legale o Consulente','NO','info@solges.it','<%LEGA_EMAIL%>',false,'LEGAL_OR_CONSULTANT','REPAIR'),
    ('26c8e1b2-59e7-11e9-8647-d663bd873d93','Gruppo Contact Center','NO','info@solges.it','<%CONTACTC_EMAIL%>',false,'CONTACT_CENTER_GROUP','REPAIR');

INSERT INTO event_type(id,description,event_type,create_attachments,catalog,attachments_type,only_last,is_active,type_complaint) VALUES ('6064349f-3988-4098-ba37-08a6ef42f9d3','Inserimento Denuncia','COMPLAINT_INSERTION_REPAIR','false','',NULL,'false','true','REPAIR');
INSERT INTO event_type(id,description,event_type,create_attachments,catalog,attachments_type,only_last,is_active,type_complaint) VALUES ('33e2458c-104d-4c72-92bd-8c465ab4e2e4','Assegnazione Operatore','OPERATOR_ASSIGNMENT_REPAIR','false','',NULL,'false','true','REPAIR');
INSERT INTO event_type(id,description,event_type,create_attachments,catalog,attachments_type,only_last,is_active,type_complaint) VALUES ('cf0f8cd5-29b7-4c51-9341-e7b10befce1a','Variazione Dati Pratica','PRACTICAL_DATA_VARIATION_REPAIR','false','',NULL,'false','true','REPAIR');
INSERT INTO event_type(id,description,event_type,create_attachments,catalog,attachments_type,only_last,is_active,type_complaint) VALUES ('54706680-b9ff-441f-958c-53e739f529c3','Variazione Flusso','FLOW_VARIATION_REPAIR','false','',NULL,'false','true','REPAIR');
INSERT INTO event_type(id,description,event_type,create_attachments,catalog,attachments_type,only_last,is_active,type_complaint) VALUES ('e28200d7-d84a-434e-a331-995e2351e463','Variazione Status','STATUS_VARIATION_REPAIR','false','',NULL,'false','true','REPAIR');
INSERT INTO event_type(id,description,event_type,create_attachments,catalog,attachments_type,only_last,is_active,type_complaint) VALUES ('99647883-9e4c-44fe-ac60-1bdd401a860b','Variazione Gestione','MANAGEMENT_VARIATION_REPAIR','false','',NULL,'false','true','REPAIR');
INSERT INTO event_type(id,description,event_type,create_attachments,catalog,attachments_type,only_last,is_active,type_complaint) VALUES ('dc37ba3b-7903-48aa-9598-02c21e80ff96','Call Costroparte','COUNTERPARTY_CALL_REPAIR','false','',NULL,'false','true','REPAIR');
INSERT INTO event_type(id,description,event_type,create_attachments,catalog,attachments_type,only_last,is_active,type_complaint) VALUES ('034885e0-cfe2-42cd-9df1-413bfffc075b','Comunicazione Generica Controparte (Conducente)','GENERIC_COMUNICATION_COUNTERPARTY_REPAIR','false','',NULL,'false','true','REPAIR');
INSERT INTO event_type(id,description,event_type,create_attachments,catalog,attachments_type,only_last,is_active,type_complaint) VALUES ('70a93885-0999-4184-bef5-a70915e7be5e','Comunicazione Riparatori alla Controparte','REPAIR_COMUNICATION_COUNTERPARTY_REPAIR','false','',NULL,'false','true','REPAIR');
INSERT INTO event_type(id,description,event_type,create_attachments,catalog,attachments_type,only_last,is_active,type_complaint) VALUES ('4b4288e1-d295-4c45-866c-68d9b1518b5d','Comunicazione Generica Riparatore','GENERIC_REPAIR_COMUNICATION_REPAIR','false','',NULL,'false','true','REPAIR');
INSERT INTO event_type(id,description,event_type,create_attachments,catalog,attachments_type,only_last,is_active,type_complaint) VALUES ('7875b428-b50e-4dd9-a2d4-8913710cb226','Comunicazione Canalizzazione Riparatore','SEWER_REPAIR_COMUNICATION_REPAIR','false','',NULL,'false','true','REPAIR');
INSERT INTO event_type(id,description,event_type,create_attachments,catalog,attachments_type,only_last,is_active,type_complaint) VALUES ('61365c1a-ab85-4b72-8ceb-7539366591d3','Comunicazione Generica Gestore','GENERIC_MANAGEMENT_COMUNICATION_REPAIR','false','',NULL,'false','true','REPAIR');
INSERT INTO event_type(id,description,event_type,create_attachments,catalog,attachments_type,only_last,is_active,type_complaint) VALUES ('3e2192e1-408c-4a88-87d9-dd2305d084e8','Comunicazione Generica Legale','GENERIC_LEGAL_COMUNICATION_REPAIR','false','',NULL,'false','true','REPAIR');
INSERT INTO event_type(id,description,event_type,create_attachments,catalog,attachments_type,only_last,is_active,type_complaint) VALUES ('ba5a0022-2a24-4bca-a0da-6bb1d6aa1b53','Comunicazione Generica Controparte (Assicurato)','GENERIC_COUNTERPARTY_COMUNICATION_REPAIR','false','',NULL,'false','true','REPAIR');

INSERT INTO email_template (id, application_event_type_id, description, object, heading,body, foot, attach_file, splitting_recipients_email, is_active, type_complaint, type, client_code) VALUES
    ('e6c3a348-59e3-11e9-8647-d663bd873d93','70a93885-0999-4184-bef5-a70915e7be5e','Elenco Carrozzerie al Danneggiato','Riparazione auto ( <%VEIC_TARGA%> )',NULL,'Gentile Sig/Sig.ra <%COND_NOME%> <%COND_COGNOME%>,
come anticipato telefonicamente , Le inoltriamo di seguito  l’elenco delle carrozzerie individuate nella zona da Lei richiesta:

<%DESC_NOTE%>

Una volta effettuata la scelta della carrozzeria da Lei preferita, La preghiamo di contattarci al seguente numero: 06/66055523.

In attesa di ricevere un suo cortese riscontro, Le porgiamo cordiali saluti.
','Contact Center ALD Repair',false,true,true,'REPAIR','GENERIC',NULL),
    ('e6c3a5fa-59e3-11e9-8647-d663bd873d93','034885e0-cfe2-42cd-9df1-413bfffc075b','Comunicazione Informativa al Danneggiato','Informazione Sinistro ( <%VEIC_TARGA%> )',NULL,'Gentile Sig/Sig.ra <%COND_NOME%> <%COND_COGNOME%>,
a seguito del sinistro avvenuto in data <%SIN_DATA%> con il veicolo <%VEICPP_TARGA%>, di proprietà ALD Automotive Italiana s.r.l. , Le indichiamo i riferimenti del nostro Contact Center dove potrà ricevere informazioni inerenti la procedura per la liquidazione o la riparazione del danno subito.

Contact Center 06.66 05 55 23


Rimaniamo a sua disposizione per ogni eventuale informazione e/o chiarimento.','Cordiali Saluti.
ALD Automotive Italia S.r.l.',false,true,true,'REPAIR','GENERIC',NULL),
    ('e6c3a776-59e3-11e9-8647-d663bd873d93','7875b428-b50e-4dd9-a2d4-8913710cb226','Comunicazione Canalizzazione Riparatore','COD: <%RIPA_CODICE%> Riparazione Targa:<%VEIC_TARGA%>',NULL,'Spett.le <%RIPA_NOME%>,
con la presente Vi informiamo che la  Sig/Sig.ra <%COND_NOME%> <%COND_COGNOME%>, intende
riparare la propria auto targata <%VEIC_TARGA%>, danneggiata a seguito del sinistro subito in data <%SIN_DATA%> presso la Vostra carrozzeria, identificata con il codice fornitore in oggetto.

Di seguito i recapiti del danneggiato per fissare un appuntamento;

Telefono: <%COND_TELEFONO%>
Email: <%COND_EMAIL%>


In allegato la cessione del credito che dovr� esserci inoltrata, a mezzo Wincar, debitamente compilata e sottoscritta dalla Carrozzeria e dal Danneggiato.

Per eventuali informazioni e/o ulteriori chiarimenti o in assenza di risposta da parte del danneggiato, la preghiamo di contattarci al seguente numero 06/66055523.

Di seguito le informazioni in merito:

Con riferimento all�Accordo Quadro di collaborazione in vigore, volto alla fornitura ed esecuzione di servizi di assistenza e/o manutenzione a favore di ALD, con la presente Vi informiamo che ALD ha di recente stipulato con primaria Compagnia Assicurativa, la Sogessur SA, un contratto per la prestazione di servizi avente ad oggetto l�individuazione di una Rete di officine e carrozzerie per la realizzazione di preventivi e/o per l�esecuzione di riparazioni a regola d�arte di autoveicoli di propriet� della controparte (di seguito il �Danneggiato�) che, a seguito di incidente, hanno subito dei danni cagionati dal soggetto assicurato Sogessur.

Nell�ottica di un rafforzamento dei nostri rapporti di collaborazione, nonch� dell�innegabile interesse commerciale in capo alle Parti di collaborare, secondo le modalit� previste nel suddetto Accordo Quadro, Vi invitiamo ad eseguire, con puntualit� e compiutezza, i Servizi di riparazione di autoveicoli a favore del Danneggiato secondo le modalit� e termini di cui all�Accordo quadro di collaborazione gi� in vigore tra Voi e la Nostra Societ�.

Vi precisiamo, inoltre, che i Servizi da Voi espletati di cui alla presente dovranno seguire le seguenti prescrizioni/tempistiche da considerarsi essenziali e a contenuto prevalente rispetto ad eventuali disposizioni contrastanti contenute nell�Accordo  Quadro:

1. Il Danneggiato si recher� il giorno dell�appuntamento presso la Vs. Officina che dovr�:
a. effettuare un preventivo per le riparazioni dei danni conseguenti dal Sinistro e proporre al terzo danneggiato la riparazione
b. Se il danneggiato accetta la riparazione:
�   far sottoscrivere la Cessione del Credito sulla base del preventivo effettuato come da form contenuto nell�allegato alla presente;
�   indicare al terzo danneggiato che sar� ricontattato entro 3 giorni dove gli sar� comunicata l�accettazione o meno del preventivo da parte della compagnia assicurativa e che in caso affermativo sar� fissato un nuovo appuntamento per le riparazioni.
�   Inviare ad ALD la seguente documentazione:
1.      il preventivo di riparazione
2.      la scansione leggibile fronte/retro del libretto di circolazione;
3.      la scansione leggibile della cessione del credito, compilata in tutte le sue parti
4.      la scansione leggibile fronte/retro del documento d�identit� del proprietario del veicolo
5.      la scansione leggibile del modulo CAI
6.      foto chiare e dettagliate del danno
I documenti sopra elencati dovranno essere caricati sul sistema operativo in uso, Wincar, in una unica soluzione-

c. Se non accetta la riparazione informare il danneggiato dell�importo del preventivo che
in ogni caso dovr� inviare ad ALD entro 3 giorni dalla data del primo appuntamento.

2. Appena l�Officina ricever� l�autorizzazione del preventivo da parte di ALD, provveder� a contattare il terzo danneggiato per fissare un nuovo appuntamento.

3. L�Officina eseguir� i lavori di riparazione sulla base delle autorizzazioni ricevute da ALD e si impegner� a svolgere i lavori di riparazione in tempi brevi e a regola d�arte.

4. A lavoro eseguito, l�Officina Convenzionata emetter� fattura nei confronti del Danneggiato per l�importo corrispondente a quanto autorizzato nel preventivo di cui al precedente punto 2, la quale dovr� essere da Voi caricata nel sistema WINCAR ed inviata in formato PDF al seguente indirizzo:
aldrichiestedanni@m1.serfin97srl.net, specificando in oggetto la targa della vettura; la compagnia assicurativa provveder� con al pagamento entro 30 giorni data fattura.







','Cordiali Saluti.
ALD Automotive Italia S.r.l.',false,true,true,'REPAIR','GENERIC',NULL),
    ('e6c3ac4e-59e3-11e9-8647-d663bd873d93','61365c1a-ab85-4b72-8ceb-7539366591d3','Comunicazione Al Gestore Sinistri','Sinistro N. <%SIN_NUMEXT%>',NULL,'<%DESC_NOTE%>','Cordiali Saluti.
ALD Automotive Italia S.r.l.',false,true,true,'REPAIR','GENERIC',NULL),
    ('e6c3adca-59e3-11e9-8647-d663bd873d93','3e2192e1-408c-4a88-87d9-dd2305d084e8','Comunicazione Generica al Legale o Consulente','Sinistro: <%COND_NOME%> <%COND_COGNOME%> Targa:<%VEIC_TARGA%>',NULL,'Spett.le Avv.<%LEGA_NOME%>,

a seguito del colloquio telefonico avuto con il Suo cliente <%COND_NOME%> <%COND_COGNOME%>, la/il quale, dopo aver subito un sinistro in data <%SIN_DATA%>, intende riparare la propria auto con Targa :<%VEIC_TARGA%>, presso una nostra carrozzeria di fiducia.

A tal fine Le allego il modulo che dovr� restituirci debitamente compilato e sottoscritto per accettazione, al seguente indirizzo email: aldrichiestedanni@m1.serfin97srl.net

Per eventuali informazioni e/o ulteriori chiarimenti la preghiamo di contattarci al seguente numero 06/66055523.','Cordiali Saluti.
ALD Automotive Italia S.r.l.',false,true,true,'REPAIR','GENERIC',NULL),
    ('e6c3af0a-59e3-11e9-8647-d663bd873d93','4b4288e1-d295-4c45-866c-68d9b1518b5d','ComunicazioneGenerica Riparatore','COD: <%RIPA_CODICE%> Riparazione Targa:<%VEIC_TARGA%>',NULL,'Una volta effettuata la scelta della carrozzeria da Lei preferita, La preghiamo di contattarci al seguente numero: 06/66055523.','Cordiali Saluti.
ALD Automotive Italia S.r.l.',false,true,true,'REPAIR','GENERIC',NULL),
    ('e6c3b04a-59e3-11e9-8647-d663bd873d93','ba5a0022-2a24-4bca-a0da-6bb1d6aa1b53','Comunicazione Informativa Assicurato Danneggiato','Informazione Sinistro ( <%VEIC_TARGA%> )',NULL,'Gentile Sig/Sig.ra <%COND_NOME%> <%COND_COGNOME%>,
a seguito del sinistro avvenuto in data <%SIN_DATA%> con il veicolo <%VEICPP_TARGA%>, di propriet� ALD Automotive Italiana s.r.l. , Le indichiamo i riferimenti del nostro Contact Center dove potr� ricevere informazioni inerenti la procedura per la riparazione della sua auto.

Contact Center 06.66 05 55 23


Rimaniamo a sua disposizione per ogni eventuale informazione e/o chiarimento.

','Cordiali Saluti.
ALD Automotive Italia S.r.l.',false,true,true,'REPAIR','GENERIC',NULL);

INSERT INTO email_template_recipient_type (email_template_id, recipient_type_id) VALUES ('e6c3a348-59e3-11e9-8647-d663bd873d93', '26c8d794-59e7-11e9-8647-d663bd873d93');
INSERT INTO email_template_recipient_type (email_template_id, recipient_type_id) VALUES ('e6c3a5fa-59e3-11e9-8647-d663bd873d93', '26c8d794-59e7-11e9-8647-d663bd873d93');
INSERT INTO email_template_recipient_type (email_template_id, recipient_type_id) VALUES ('e6c3a776-59e3-11e9-8647-d663bd873d93', '26c8de2e-59e7-11e9-8647-d663bd873d93');
INSERT INTO email_template_recipient_type (email_template_id, recipient_type_id) VALUES ('e6c3ac4e-59e3-11e9-8647-d663bd873d93', '26c8df64-59e7-11e9-8647-d663bd873d93');
INSERT INTO email_template_recipient_type (email_template_id, recipient_type_id) VALUES ('e6c3adca-59e3-11e9-8647-d663bd873d93', '26c8e090-59e7-11e9-8647-d663bd873d93');
INSERT INTO email_template_recipient_type (email_template_id, recipient_type_id) VALUES ('e6c3af0a-59e3-11e9-8647-d663bd873d93', '26c8de2e-59e7-11e9-8647-d663bd873d93');
INSERT INTO email_template_recipient_type (email_template_id, recipient_type_id) VALUES ('e6c3b04a-59e3-11e9-8647-d663bd873d93', '26c8d910-59e7-11e9-8647-d663bd873d93');

ALTER TABLE appropriation ADD COLUMN IF NOT EXISTS created_by varchar (255);
ALTER TABLE appropriation ADD COLUMN IF NOT EXISTS updated_by varchar (255);
ALTER TABLE practice_car ADD COLUMN IF NOT EXISTS created_by varchar (255);
ALTER TABLE practice_car ADD COLUMN IF NOT EXISTS updated_by varchar (255);

ALTER TABLE broker ALTER COLUMN zip_code TYPE VARCHAR;
ALTER TABLE inspectorate ALTER COLUMN zip_code TYPE VARCHAR;
ALTER TABLE legal ALTER COLUMN zip_code TYPE VARCHAR;
ALTER TABLE insurance_company ALTER COLUMN zip_code TYPE VARCHAR;
ALTER TABLE insurance_manager ALTER COLUMN zip_code TYPE VARCHAR;
ALTER TABLE manager ALTER COLUMN zip_code TYPE VARCHAR;
ALTER TABLE personal_data ALTER COLUMN zip_code TYPE VARCHAR;
ALTER TABLE repairer ALTER COLUMN zip_code TYPE VARCHAR;

INSERT INTO legal (id,name,email,address,zip_code,city,province,state,vat_number,fiscal_code,telephone,fax,web_site,reference_person,external_code,is_active) VALUES
    ('a4f5190c-59fb-11e9-8647-d663bd873d93','TOMA DONATO','affidi@tomastudiolegale.it',null,null,null,null,null,null,null,null,null,null,null,'3.0',true),
    ('a4f51bfa-59fb-11e9-8647-d663bd873d93','TROCANO PIO MARIO','pm.trocano@tiscali.it','Via degli Olmetti, 46','00060','Formello','RM','Italia',null,null,'06.90405109','06.90756036',null,null,'2.0',true),
    ('a4f51d6c-59fb-11e9-8647-d663bd873d93','DE LUCA PAOLO','studiolegale.deluca@libero.it','VIA A. STOPPANI 10','000197','ROMA','RM','Italia',null,null,'068072339',null,null,null,'4.0',true),
    ('a4f51ec0-59fb-11e9-8647-d663bd873d93','ALICICCO ALESSANDRO','avv_alicicco@yahoo.it','PIAZZALE CLODIO 18','000195','ROMA','RM','Italia',null,null,'0645437796','0645437583',null,null,'1.0',true),
    ('a4f5200a-59fb-11e9-8647-d663bd873d93','GIUSEPPE TROCANO','trocano@tiscali.it','VIA VESPASIANO 48','000192','ROMA','RM','Italia',null,null,'0696541570',null,null,null,'Q',true),
    ('a4f5215e-59fb-11e9-8647-d663bd873d93','OFFERTA REALE',null,null,null,null,null,null,null,null,null,null,null,null,'C',true),
    ('a4f522b2-59fb-11e9-8647-d663bd873d93','ACCLAIMS','rivalse@acclaims.it',null,null,null,null,null,null,null,null,null,null,null,'K',true),
    ('a4f52582-59fb-11e9-8647-d663bd873d93','WILLCONSULTING SR','Sinistriflotte_willco@willis.com',null,null,null,null,null,null,null,null,null,null,null,'7.0',true),
    ('a4f526cc-59fb-11e9-8647-d663bd873d93','ACCLAIMS SR','rivalse@acclaims.it',null,null,null,null,null,null,null,null,null,null,null,'K',true),
    ('a4f52816-59fb-11e9-8647-d663bd873d93','ALCO SOLUTIONS','sinistri.ald@alcogroup.it',null,null,null,null,null,null,null,null,null,null,null,null,true),
    ('a4f52956-59fb-11e9-8647-d663bd873d93','AON','recoveryald@aon.it',null,null,null,null,null,null,null,null,null,null,null,null,true),
    ('a4f52aa0-59fb-11e9-8647-d663bd873d93','AON HEWITT SR','incarichiald@aon.it',null,null,null,null,null,null,null,null,null,null,null,null,true);


INSERT INTO inspectorate (id,name,email,address,zip_code,city,province,state,vat_number,fiscal_code,telephone,fax,web_site,reference_person,attorney,external_code,is_active)
VALUES ('65726fa0-59fb-11e9-8647-d663bd873d93','FONDIARIA SAI ISPETTORATO SDM MILANO','SDM.Milano@fondiaria-sai.it',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5, 'TRUE')
,('65727248-59fb-11e9-8647-d663bd873d93','FONSAI SDM - D.PECORA','d.pecora@milass.it',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'02.82696552',NULL,NULL,NULL,NULL,6,'TRUE')
,('65727392-59fb-11e9-8647-d663bd873d93','FONSAI - Perito Zagni','gianluca@studioerrezeta.it',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'02.39325708',NULL,NULL,NULL,NULL,6, 'TRUE')
,('657274c8-59fb-11e9-8647-d663bd873d93','FONSAI - Perito Gusmini','argus@argus.fastwebnet.it',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'02.66504179',NULL,NULL,NULL,NULL,NULL, 'TRUE')
,('657275f4-59fb-11e9-8647-d663bd873d93','FONSAI - Perito De Martino','demagusmi@tin.it',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL, 'TRUE')
,('65727a4a-59fb-11e9-8647-d663bd873d93','GENERALI ISPETT CARD SR','recoveryald@aon.it',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ALD@generali.com',NULL, 'TRUE');
INSERT INTO automatic_affiliation_legal_rule (id, selection_name, monthly_volume, current_month, current_volume, detail, annotations, complaints_without_ctp, foreign_country_claim, counterpart_type, claim_with_injured, damage_can_not_be_repaired, legal_id, min_import, max_import)
VALUES ('0ebb0f18-59fc-11e9-8647-d663bd873d93', 'affido rc attivo e concorsuale generico tranne sg compagnia estera e sx accaduti all''estero - no generali', 175, '04/2019', 85, null, null, null, 'FALSE', null, null, null, 'a4f5190c-59fb-11e9-8647-d663bd873d93', 20, 40),
('0ebb12f6-59fc-11e9-8647-d663bd873d93', 'affido rc attivo e concorsuale compagnia di ctp estera e generali (ald) vs sogessur', 100, '04/2019', 3, null, null, null, null, null, null, null, 'a4f522b2-59fb-11e9-8647-d663bd873d93', 20, 40),
('0ebb1490-59fc-11e9-8647-d663bd873d93', 'affido rc attivo e concorsuale accaduto all''estero', 100, '04/2019', 0, null, null, null, 'TRUE', null, null, null, 'a4f522b2-59fb-11e9-8647-d663bd873d93', 20, 40),
('0ebb15d0-59fc-11e9-8647-d663bd873d93', 'affido rc attivo e concorsuale generico tranne sg e generali', -1, '04/2019', 146, null, null, null, null, null, null, null, 'a4f522b2-59fb-11e9-8647-d663bd873d93', 20, 40),
('0ebb1706-59fc-11e9-8647-d663bd873d93', 'affido sx rc attivi e concorsuali con compagnia ctp non mappata', 100, '04/2019', 12, null, null, 'TRUE', null, null, null, null, 'a4f522b2-59fb-11e9-8647-d663bd873d93', 20, 40),
('71c75192-59fd-11e9-8647-d663bd873d93', 'sx rc attivi e concorsuali generali', 300, '04/2019', 14, null, null, null, null, null, null, null, 'a4f52956-59fb-11e9-8647-d663bd873d93', 20, 40),
('71c75458-59fd-11e9-8647-d663bd873d93', 'sx rc attivi e concorsuali generico escluso generali', 100, '04/2019', 6, null, null, null, null, null, null, null, 'a4f52956-59fb-11e9-8647-d663bd873d93', 20, 40);
INSERT INTO automatic_affiliation_rule_legal_to_claims_type (automatic_affiliation_rule_legal_id, claims_type_id)
VALUES ('0ebb0f18-59fc-11e9-8647-d663bd873d93','7a074aa2-4ae8-11e9-8646-d663bd873d93'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','7a074d9a-4ae8-11e9-8646-d663bd873d93'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','7a074aa2-4ae8-11e9-8646-d663bd873d93'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','7a074d9a-4ae8-11e9-8646-d663bd873d93'),
('0ebb1490-59fc-11e9-8647-d663bd873d93','7a074aa2-4ae8-11e9-8646-d663bd873d93'),
('0ebb1490-59fc-11e9-8647-d663bd873d93','7a074d9a-4ae8-11e9-8646-d663bd873d93'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','7a074aa2-4ae8-11e9-8646-d663bd873d93'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','7a074d9a-4ae8-11e9-8646-d663bd873d93'),
('0ebb1706-59fc-11e9-8647-d663bd873d93','7a074aa2-4ae8-11e9-8646-d663bd873d93'),
('0ebb1706-59fc-11e9-8647-d663bd873d93','7a074d9a-4ae8-11e9-8646-d663bd873d93'),
('71c75192-59fd-11e9-8647-d663bd873d93','7a074aa2-4ae8-11e9-8646-d663bd873d93'),
('71c75192-59fd-11e9-8647-d663bd873d93','7a074d9a-4ae8-11e9-8646-d663bd873d93'),
('71c75458-59fd-11e9-8647-d663bd873d93','7a074aa2-4ae8-11e9-8646-d663bd873d93'),
('71c75458-59fd-11e9-8647-d663bd873d93','7a074d9a-4ae8-11e9-8646-d663bd873d93');
INSERT INTO automatic_affiliation_rule_legal_to_counterpart_company (automatic_affiliation_rule_legal_id, counterpart_company_id)
VALUES ('0ebb0f18-59fc-11e9-8647-d663bd873d93','a83be2a6-4a58-11e9-8646-d663bd873d93'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','a83be648-4a58-11e9-8646-d663bd873d93'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','a83be7e2-4a58-11e9-8646-d663bd873d93'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','a83be940-4a58-11e9-8646-d663bd873d93'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc34770-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc34771-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc34772-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc34774-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc34775-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc34776-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc34777-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc34778-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc34779-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc3477a-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc3477b-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc3477c-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc3477d-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc3477e-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc3477f-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc34780-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc34781-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc34782-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc34783-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc34784-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc34785-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc34786-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc34787-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc34788-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc34789-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc3478a-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc3478b-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc3478c-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc3478d-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc3478e-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc3478f-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc34790-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc34791-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc34792-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc34793-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36e80-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36e81-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36e82-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36e83-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36e84-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36e85-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36e86-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36e87-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36e88-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36e89-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36e8a-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36e8b-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36e8c-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36e8d-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36e8e-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36e8f-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36e90-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36e91-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36e92-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36e93-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36e94-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36e95-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36e96-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36e97-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36e98-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36e99-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36e9a-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36e9b-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36e9c-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36e9d-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36e9e-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36e9f-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36ea0-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36ea1-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36ea2-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36ea3-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36ea4-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36ea5-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36ea6-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36ea7-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36ea8-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36ea9-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36eab-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36eac-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36ead-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36eae-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36eaf-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36eb0-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36eb1-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36eb2-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36eb3-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36eb4-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36eb5-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36eb6-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36eb7-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36eb8-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36eb9-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36eba-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36ebb-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36ebd-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36ebe-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36ebf-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36ec0-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36ec1-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36ec2-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36ec5-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc36ec6-513b-11e9-b475-0800200c9a66'),
('0ebb0f18-59fc-11e9-8647-d663bd873d93','5dc3959c-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','00000000-0000-0000-0000-000000000000'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','a83be2a6-4a58-11e9-8646-d663bd873d93'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','a83be648-4a58-11e9-8646-d663bd873d93'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','00000000-0000-0000-0000-000000000000'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','00000000-0000-0000-0000-000000000001'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc34770-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc34771-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc34772-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc34774-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc34775-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc34776-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc34777-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc34778-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc3477a-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc3477b-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc3477c-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc3477d-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc3477e-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc3477f-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc34780-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc34781-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc34782-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc34783-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc34784-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc34785-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc34786-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc34787-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc34788-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc34789-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc3478a-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc3478b-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc3478c-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc3478d-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc3478e-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc3478f-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc34790-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc34791-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc34792-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc34793-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36e80-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36e81-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36e82-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36e83-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36e84-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36e85-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36e86-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36e87-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36e88-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36e89-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36e8a-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36e8b-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36e8c-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36e8d-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36e8e-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36e8f-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36e90-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36e91-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36e92-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36e93-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36e94-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36e95-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36e96-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36e97-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36e98-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36e99-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36e9a-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36e9b-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36e9c-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36e9d-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36e9e-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36e9f-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36ea0-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36ea1-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36ea2-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36ea3-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36ea4-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36ea5-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36ea6-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36ea7-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36ea8-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36ea9-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36eab-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36eac-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36ead-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36eae-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36eaf-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36eb0-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36eb1-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36eb2-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36eb3-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36eb4-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36eb5-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36eb6-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36eb7-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36eb8-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36eb9-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36eba-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36ebb-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36ebe-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36ebf-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36ec0-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36ec2-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36ec3-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36ec5-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36ec6-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36ec7-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36ec8-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36ec9-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36eca-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36ecb-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36ecc-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36ecd-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36ece-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36ecf-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc36ed0-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc39590-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc39591-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc39592-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc39593-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc39594-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc39595-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc39596-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc39597-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc39598-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc3959a-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc3959c-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc3959e-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc3959f-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc395a0-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc395a1-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc395a2-513b-11e9-b475-0800200c9a66'),
('0ebb15d0-59fc-11e9-8647-d663bd873d93','5dc395a3-513b-11e9-b475-0800200c9a66'),
('71c75192-59fd-11e9-8647-d663bd873d93','a83be940-4a58-11e9-8646-d663bd873d93'),
('71c75192-59fd-11e9-8647-d663bd873d93','5dc36ebd-513b-11e9-b475-0800200c9a66'),
('71c75192-59fd-11e9-8647-d663bd873d93','5dc36ec1-513b-11e9-b475-0800200c9a66'),
('71c75192-59fd-11e9-8647-d663bd873d93','a83bdfb8-4a58-11e9-8646-d663bd873d93'),
('71c75192-59fd-11e9-8647-d663bd873d93','5dc34779-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','a83be2a6-4a58-11e9-8646-d663bd873d93'),
('71c75458-59fd-11e9-8647-d663bd873d93','a83be648-4a58-11e9-8646-d663bd873d93'),
('71c75458-59fd-11e9-8647-d663bd873d93','a83be7e2-4a58-11e9-8646-d663bd873d93'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc34770-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc34771-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc34772-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc34774-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc34775-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc34776-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc34777-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc34778-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc3477a-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc3477b-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc3477c-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc3477d-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc3477e-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc3477f-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc34780-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc34781-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc34782-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc34783-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc34784-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc34785-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc34786-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc34787-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc34788-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc34789-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc3478a-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc3478b-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc3478c-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc3478d-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc3478e-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc3478f-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc34790-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc34791-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc34792-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc34793-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36e80-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36e81-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36e82-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36e83-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36e84-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36e85-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36e86-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36e87-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36e88-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36e89-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36e8a-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36e8b-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36e8c-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36e8d-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36e8e-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36e8f-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36e90-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36e91-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36e92-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36e93-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36e94-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36e95-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36e96-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36e97-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36e98-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36e99-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36e9a-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36e9b-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36e9c-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36e9d-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36e9e-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36e9f-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36ea0-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36ea1-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36ea2-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36ea3-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36ea4-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36ea5-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36ea6-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36ea7-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36ea8-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36ea9-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36eab-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36eac-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36ead-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36eae-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36eaf-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36eb0-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36eb1-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36eb2-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36eb3-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36eb4-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36eb5-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36eb6-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36eb7-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36eb8-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36eb9-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36eba-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36ebb-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36ebf-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36ec0-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36ec2-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36ec3-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36ec5-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc36ec6-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc3959c-513b-11e9-b475-0800200c9a66'),
('71c75458-59fd-11e9-8647-d663bd873d93','5dc3959e-513b-11e9-b475-0800200c9a66');
INSERT INTO automatic_affiliation_rule_legal_to_damaged_company (automatic_affiliation_rule_legal_id, damaged_company_id)
VALUES ('0ebb12f6-59fc-11e9-8647-d663bd873d93','a83bdfb8-4a58-11e9-8646-d663bd873d93'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','a83be2a6-4a58-11e9-8646-d663bd873d93'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','a83be648-4a58-11e9-8646-d663bd873d93'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','a83be7e2-4a58-11e9-8646-d663bd873d93'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','a83be940-4a58-11e9-8646-d663bd873d93'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','00000000-0000-0000-0000-000000000000'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','00000000-0000-0000-0000-000000000001'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc34770-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc34771-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc34772-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc34774-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc34775-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc34776-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc34777-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc34778-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc34779-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc3477a-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc3477b-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc3477c-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc3477d-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc3477e-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc3477f-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc34780-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc34781-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc34782-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc34783-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc34784-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc34785-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc34786-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc34787-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc34788-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc34789-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc3478a-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc3478b-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc3478c-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc3478d-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc3478e-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc3478f-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc34790-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc34791-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc34792-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc34793-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36e80-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36e81-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36e82-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36e83-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36e84-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36e85-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36e86-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36e87-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36e88-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36e89-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36e8a-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36e8b-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36e8c-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36e8d-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36e8e-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36e8f-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36e90-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36e91-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36e92-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36e93-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36e94-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36e95-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36e96-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36e97-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36e98-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36e99-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36e9a-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36e9b-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36e9c-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36e9d-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36e9e-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36e9f-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36ea0-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36ea1-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36ea2-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36ea3-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36ea4-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36ea5-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36ea6-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36ea7-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36ea8-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36ea9-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36eab-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36eac-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36ead-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36eae-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36eaf-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36eb0-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36eb1-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36eb2-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36eb3-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36eb4-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36eb5-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36eb6-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36eb7-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36eb8-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36eb9-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36eba-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36ebb-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36ebd-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36ebe-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36ebf-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36ec0-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36ec1-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36ec2-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36ec5-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc36ec6-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc39599-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc3959c-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc3959e-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc3959f-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc395a0-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc395a1-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc395a2-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc395a3-513b-11e9-b475-0800200c9a66'),
('0ebb12f6-59fc-11e9-8647-d663bd873d93','5dc395ae-513b-11e9-b475-0800200c9a66');
