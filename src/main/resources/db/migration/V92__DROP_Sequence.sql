ALTER TABLE public.claims ALTER COLUMN practice_id DROP DEFAULT;
ALTER TABLE public.counterparty ALTER COLUMN practice_id_counterparty DROP DEFAULT;
DROP sequence if exists appropriation_sequence;
DROP sequence if exists practice_car_sequence;