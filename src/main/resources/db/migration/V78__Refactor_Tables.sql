ALTER TABLE claims_from_company ALTER COLUMN expert TYPE text USING expert::text;
ALTER TABLE claims_from_company ALTER COLUMN inspectorate TYPE text USING inspectorate::text;
