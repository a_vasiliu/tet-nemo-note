ALTER TABLE email_template DROP COLUMN IF EXISTS body;
ALTER TABLE email_template ADD COLUMN IF NOT EXISTS body text;
ALTER TABLE insurance_policy DROP COLUMN IF EXISTS number_policy;
ALTER TABLE insurance_policy ADD COLUMN IF NOT EXISTS number_policy varchar(255);
ALTER TABLE inspectorate DROP COLUMN address;
ALTER TABLE inspectorate ADD COLUMN address VARCHAR (255) null;
ALTER TABLE contract_type DROP COLUMN IF EXISTS ctrnote;
ALTER TABLE contract_type ADD COLUMN IF NOT EXISTS ctrnote text;

