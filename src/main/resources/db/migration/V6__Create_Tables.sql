ALTER TABLE contract_type ADD COLUMN IF NOT EXISTS default_flow varchar(500);
ALTER TABLE contract_type ADD COLUMN IF NOT EXISTS ricaricar Boolean;
ALTER TABLE contract_type ADD COLUMN IF NOT EXISTS franchise numeric;
ALTER TABLE personal_data ADD COLUMN IF NOT EXISTS pec varchar(500);
ALTER TABLE personal_data ADD COLUMN IF NOT EXISTS customer_id varchar(500);
ALTER TABLE event_type ADD COLUMN IF NOT EXISTS event_type varchar(500);
