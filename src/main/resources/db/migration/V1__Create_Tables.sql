CREATE SEQUENCE IF NOT EXISTS practice_id_sequence;
CREATE SEQUENCE IF NOT EXISTS risk_id_sequence;
CREATE SEQUENCE IF NOT EXISTS event_id_sequence;
CREATE SEQUENCE IF NOT EXISTS insurance_policy_id_sequence;

CREATE TABLE IF NOT EXISTS claims (
	id varchar(255) NOT NULL,
	cai_details jsonb NULL,
	complaint jsonb NULL,
	is_with_counterparty bool NULL,
	counterparty jsonb NULL,
	created_at timestamptz NULL,
	damaged jsonb NULL,
	deponent_list jsonb NULL,
	forms jsonb NULL,
	historical jsonb NULL,
	documentation bool NULL,
	notes jsonb NULL,
	practice_id int8 DEFAULT nextval('practice_id_sequence') NOT NULL,
	practice_manager varchar(255) NULL,
	status varchar(255) NULL,
	type varchar(255) NULL,
	update_at timestamptz NULL,
	user_entity varchar(255) NULL,
	wounded_list jsonb NULL,
	pai_comunication bool NULL,
	found_model jsonb NULL,
	forced boolean NULL,
	in_evidence boolean NULL,
  id_saleforce int8 NULL,
	CONSTRAINT claims_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS contract_type (
	id UUID PRIMARY KEY,
	cod_contract_type varchar(255) NOT NULL,
	description varchar(255) NOT NULL,
	loss_deductible_keys numeric NOT NULL,
	events_deductible numeric NOT NULL,
	flag_WS  bool NOT NULL,
	ownership varchar(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS claims_type (
	id UUID PRIMARY KEY,
	type varchar(255) NOT NULL,
  claims_accident_type varchar(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS flow_contract_type (
	contract_type_entity_id UUID NOT NULL,
	claims_type_entity_id UUID NOT NULL,
	flow varchar(255) NOT NULL,
	rebilling varchar(255)  NULL,
	FOREIGN KEY (contract_type_entity_id) REFERENCES contract_type (id),
	FOREIGN KEY (claims_type_entity_id) REFERENCES claims_type (id)
);

CREATE TABLE IF NOT EXISTS configure_evidence (
	id UUID PRIMARY KEY,
	practical_state_it varchar(255) NOT NULL,
	practical_state_en varchar(255) NOT NULL,
	days_of_stay_in_the_state int8 NOT NULL
);

CREATE TABLE IF NOT EXISTS anti_theft_service (
  id UUID PRIMARY KEY,
  code_anti_theft_service varchar (255),
  name varchar (255) NOT NULL,
  business_name varchar(255) NOT NULL,
  supplier_code numeric NOT NULL,
  email varchar (255) NULL,
  phone_number varchar (255) NOT NULL,
  provider_type varchar (255) NULL,
  time_out_in_seconds numeric NULL,
  end_point_url varchar (255) NULL,
  username varchar (255) NULL,
  password varchar (255) NULL,
  company_code varchar (255) NULL,
  active bool NULL,
  CONSTRAINT uc_anti_theft_service UNIQUE (name)
);

CREATE TABLE IF NOT EXISTS broker (
  id UUID PRIMARY KEY,
  business_name varchar (255) NOT NULL,
  address varchar (255) NULL,
  zip_code numeric NULL,
  city varchar (255) NULL,
  province varchar (255) NULL,
  state varchar (255) NOT NULL,
  phone_number varchar (255) NULL,
  fax varchar (255) NULL,
  email varchar (255) NULL,
  web_site varchar (255) NULL,
  contact varchar (255) NULL,
  CONSTRAINT uc_broker UNIQUE (business_name)
);

CREATE TABLE IF NOT EXISTS insurance_company (
  id UUID PRIMARY KEY,
  "name" varchar(255) NOT NULL,
  address varchar(255) NULL,
  zip_code numeric(8) NULL,
  city varchar(255) NULL,
  province varchar(255) NULL,
  state varchar(255) NULL,
  telephone varchar(255) NULL,
  fax varchar(255) NULL,
  email varchar(255) NULL,
  web_site varchar(255) NULL,
  contact varchar(255) NULL,
  ania_code numeric(8) NULL,
  join_card bool NULL,
  contact_pai varchar(255) NULL,
  email_pai varchar(255) NULL,
  CONSTRAINT uc_insurancecompany UNIQUE (name)
);

CREATE TABLE IF NOT EXISTS insurance_manager (
  id UUID PRIMARY KEY,
  code varchar(255) NULL,
  name varchar(255) NOT NULL,
  address varchar(255) NULL,
  zip_code numeric(8) NULL,
  city varchar(255) NULL,
  province varchar(255) NULL,
  state varchar(255) NULL,
  reference_person varchar(255) NULL,
  telephone varchar(255) NULL,
  fax varchar(255) NULL,
  email varchar(255) NULL,
  web_site varchar(255) NULL,
  CONSTRAINT uc_insurancemanager UNIQUE (name)
);

CREATE TABLE IF NOT EXISTS legal (
  id UUID PRIMARY KEY,
  name varchar(255) NOT NULL,
  email varchar(255) NULL,
  address varchar(255) NULL,
  zip_code numeric NULL,
  city varchar(255) NULL,
  province varchar(255) NULL,
  state varchar(255) NULL,
  vat_number varchar(255) NULL,
  fiscal_code varchar(255) NULL,
  telephone varchar(255) NULL,
  fax varchar(255) NULL,
  web_site varchar(255) NULL,
  reference_person varchar(255) NULL,
  external_code varchar(255) NULL,
  CONSTRAINT uc_legal UNIQUE (name)
);

CREATE TABLE IF NOT EXISTS automatic_affiliation_legal_rule (
  id UUID PRIMARY KEY,
  selection_name varchar(255) NULL,
  monthly_volume numeric NULL,
  current_month varchar(255) NULL,
  current_volume numeric  NULL,
  detail numeric NULL,
  annotations varchar(255),
  complaints_without_ctp bool,
  foreign_country_claim bool,
  counterpart_type varchar(255),
  claim_with_injured bool,
  damage_can_not_be_repaired bool,
  legal_id UUID,
  FOREIGN KEY (legal_id) REFERENCES legal (id),
  CONSTRAINT uc_name_legal UNIQUE (selection_name, legal_id)
);

CREATE TABLE IF NOT EXISTS inspectorate (
  id UUID PRIMARY KEY,
  name varchar (255) NOT NULL,
  email varchar (255) NOT NULL,
  address varchar (255) NOT NULL,
  zip_code numeric(8) NULL,
  city varchar (255) NULL,
  province varchar (255) NULL,
  state varchar (255) NULL,
  vat_number varchar (255) NULL,
  fiscal_code varchar (255) NULL,
  telephone varchar (255) NULL,
  fax varchar (255) NULL,
  web_site varchar (255) NULL,
  reference_person varchar (255) NULL,
  attorney varchar (255) NULL,
  external_code varchar (255) NULL,
  CONSTRAINT uc_inspectorate UNIQUE (name)

);


CREATE TABLE IF NOT EXISTS automatic_affiliation_rule_inspectorate (
  id UUID PRIMARY KEY,
  selection_name varchar (255) NULL,
  monthly_volume numeric(8) NULL,
  current_month varchar (255) NULL,
  current_volume numeric(8)  NULL,
  detail numeric(8) NULL,
  annotations varchar (255) NULL,
  select_only_complaints_without_ctp bool NULL,
  foreign_country_claim bool NULL,
  counterpart_type varchar (255) NULL,
  claim_with_injured bool NULL,
  damage_can_not_be_repaired bool NULL,
  inspectorate_id UUID,
  FOREIGN KEY (inspectorate_id) REFERENCES inspectorate (id),
  CONSTRAINT uc_automatic_affiliation_rule_inspectorate UNIQUE (selection_name,inspectorate_id)

);

CREATE TABLE IF NOT EXISTS automatic_affiliation_rule_inspectorate_to_claims_type (
  automatic_affiliation_rule_inspectorate_id UUID,
  claims_type_id UUID,
  FOREIGN KEY (automatic_affiliation_rule_inspectorate_id) REFERENCES automatic_affiliation_rule_inspectorate (id),
  FOREIGN KEY (claims_type_id) REFERENCES claims_type (id)
);

CREATE TABLE IF NOT EXISTS automatic_affiliation_rule_inspectorate_to_damaged_insurance (
  automatic_affiliation_rule_inspectorate_id UUID,
  insurance_company_id UUID,
  FOREIGN KEY (automatic_affiliation_rule_inspectorate_id) REFERENCES automatic_affiliation_rule_inspectorate (id),
  FOREIGN KEY (insurance_company_id) REFERENCES insurance_company (id)
);

CREATE TABLE IF NOT EXISTS automatic_affiliation_rule_inspectorate_to_counterparter_insurance (
  automatic_affiliation_rule_inspectorate_id UUID,
  insurance_company_id UUID,
  FOREIGN KEY (automatic_affiliation_rule_inspectorate_id) REFERENCES automatic_affiliation_rule_inspectorate (id),
  FOREIGN KEY (insurance_company_id) REFERENCES insurance_company (id)
);
CREATE TABLE IF NOT EXISTS event_type (
  id UUID PRIMARY KEY,
  description varchar(255) NOT NULL,
  create_attachments bool NOT NULL,
  catalog varchar(255),
  attachments_type varchar(255),
  only_last bool
);

CREATE TABLE IF NOT EXISTS automatic_affiliation_rule_legal_to_claims_type (
  automatic_affiliation_rule_legal_id UUID,
  claims_type_id UUID,
  FOREIGN KEY (automatic_affiliation_rule_legal_id) REFERENCES automatic_affiliation_legal_rule (id),
  FOREIGN KEY (claims_type_id) REFERENCES claims_type (id)
);

CREATE TABLE IF NOT EXISTS automatic_affiliation_rule_legal_to_damaged_company (
  automatic_affiliation_rule_legal_id UUID,
  damaged_company_id UUID,
  FOREIGN KEY (automatic_affiliation_rule_legal_id) REFERENCES automatic_affiliation_legal_rule (id),
  FOREIGN KEY (damaged_company_id) REFERENCES insurance_company (id)
);

CREATE TABLE IF NOT EXISTS automatic_affiliation_rule_legal_to_counterpart_company (
  automatic_affiliation_rule_legal_id UUID,
  counterpart_company_id UUID,
  FOREIGN KEY (automatic_affiliation_rule_legal_id) REFERENCES automatic_affiliation_legal_rule (id),
  FOREIGN KEY (counterpart_company_id) REFERENCES insurance_company (id)
);


CREATE TABLE IF NOT EXISTS email_template (
  id UUID PRIMARY KEY,
  application_event_type_id UUID NOT NULL,
  description varchar(255) NOT NULL,
  object varchar(255),
  heading varchar(255),
  body varchar(255),
  foot varchar(255),
  attach_file bool NOT NULL,
  splitting_recipients_email bool,
  client_code varchar(255),
  FOREIGN KEY (application_event_type_id) REFERENCES event_type (id)
);

CREATE TABLE IF NOT EXISTS recipient_type (
  id UUID PRIMARY KEY,
  description varchar(255) NOT NULL,
  fixed bool NOT NULL,
  email varchar(255) NOT NULL,
  db_field_email varchar(255)
);

CREATE TABLE IF NOT EXISTS email_template_recipient_type (
  email_template_id UUID NOT NULL,
  recipient_type_id UUID NOT NULL,
  PRIMARY KEY (email_template_id,recipient_type_id),
  FOREIGN KEY (email_template_id) REFERENCES email_template (id),
  FOREIGN KEY (recipient_type_id) REFERENCES recipient_type (id)
);

CREATE TABLE IF NOT EXISTS top_customer_guarantee (
  id UUID PRIMARY KEY,
  description varchar(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS attachment_type (
  id UUID PRIMARY KEY,
  name varchar(255) NOT NULL,
  groups varchar(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS motivation (
  id UUID PRIMARY KEY,
  answer_type varchar(255) NOT NULL,
  description varchar(255) NOT NULL,
  CONSTRAINT uc_motivation UNIQUE (answer_type,description)
);


CREATE TABLE IF NOT EXISTS insurance_policy (
  id UUID PRIMARY KEY,
  type varchar (255),
  description varchar (255),
  number_policy numeric,
  insurance_policy_id int8 DEFAULT nextval('insurance_policy_id_sequence') NOT NULL,
  insurance_company UUID,
  broker UUID,
  insurance_manager UUID,
  client_code numeric,
  client varchar (255),
  annotations varchar (255),
  beginning_validity date,
  end_validity date,
  book_register bool,
  book_register_code varchar (255),
  last_renewal_notification date,
  renewal_notification bool,
  FOREIGN KEY (broker) REFERENCES broker (id),
  FOREIGN KEY (insurance_manager) REFERENCES insurance_manager (id),
  FOREIGN KEY (insurance_company) REFERENCES insurance_company (id)
);

CREATE TABLE IF NOT EXISTS risk (
  id UUID PRIMARY KEY,
  risk_id int8 DEFAULT nextval('risk_id_sequence') NOT NULL,
  type varchar(255),
  description varchar (255),
  maximal numeric,
  exemption numeric,
  overdraft numeric,
  active bool,
  insurance_policy_id UUID,
  FOREIGN KEY (insurance_policy_id) REFERENCES insurance_policy (id)
);

CREATE TABLE IF NOT EXISTS attachment (
  id UUID PRIMARY KEY,
  blob_name varchar (255),
  resource_type varchar (255),
  resource_id varchar (255),
  file_name varchar (255),
  file_size numeric,
  mime_type varchar (255),
  description varchar (255),
  blob_type varchar (255),
  id_filemanager varchar (255),
  user_id numeric,
  attachment_type UUID,
  created_at date,
  insurance_policy_id UUID,
  FOREIGN KEY (attachment_type) REFERENCES attachment_type (id),
  FOREIGN KEY (insurance_policy_id) REFERENCES insurance_policy (id)
);

CREATE TABLE IF NOT EXISTS event (
  id UUID PRIMARY KEY,
  event_id int8 DEFAULT nextval('event_id_sequence') NOT NULL,
  event_type UUID,
  created_at date,
  user_id numeric,
  description varchar (255),
  insurance_policy_id UUID,
  FOREIGN KEY (event_type) REFERENCES event_type (id),
  FOREIGN KEY (insurance_policy_id) REFERENCES insurance_policy (id)
);



CREATE INDEX IF NOT EXISTS idx_id ON public.contract_type USING btree(id);
CREATE INDEX IF NOT EXISTS idx_id ON public.claims_type USING btree(id);
CREATE INDEX IF NOT EXISTS idx_id ON public.claims USING btree(id);
CREATE INDEX IF NOT EXISTS idx_practice_id ON public.claims USING btree(practice_id);
CREATE INDEX IF NOT EXISTS idx_risk_id ON public.risk USING btree(risk_id);
CREATE INDEX IF NOT EXISTS idx_event_id ON public.event USING btree(event_id);
CREATE INDEX IF NOT EXISTS idx_insurance_policy_id ON public.event USING btree(insurance_policy_id);
