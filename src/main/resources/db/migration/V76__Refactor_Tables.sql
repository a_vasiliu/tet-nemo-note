
/*START NEW CLAIMS TABLE */
CREATE TABLE IF NOT EXISTS public.claims_new
(
  id character varying(255) PRIMARY KEY ,
  practice_id bigint NOT NULL DEFAULT nextval('practice_id_sequence'::regclass) ,
  created_at timestamptz,
  updated_at timestamptz,
  user_entity character varying(255),
  status_updated_at timestamptz,
  status character varying(255),
  type character varying(255),
  type_accident character varying(255),
  responsible boolean,
  date_accident timestamptz,
  cai_details jsonb,
  is_with_counterparty boolean,
  claim_address_street character varying(255),
  claim_address_street_nr character varying(255),
  claim_address_zip character varying(255),
  claim_address_locality character varying(255),
  claim_address_province character varying(255),
  claim_address_region character varying(255),
  claim_address_state character varying(255),
  claim_address_formatted character varying(255),
  deponent_list jsonb,
  forms jsonb,
  historical jsonb,
  documentation boolean,
  practice_manager character varying(255),
  wounded_list jsonb,
  pai_comunication boolean,
  forced boolean,
  in_evidence boolean,
  id_saleforce bigint,
  motivation character varying(500),
  with_continuation boolean,
  exemption jsonb,
  is_read_msa boolean,
  is_authority_linkable boolean,
  po_variation boolean,
  legal_comunication boolean,
  total_penalty numeric(8,0),
  is_read_acclaims boolean,
  is_migrated boolean,
  client_id character varying(255),
  plate character varying(255),
  claim_locator character varying(255),
  activation character varying(255),
  property character varying(255),
  mod character varying(255),
  notification character varying(255),
  quote boolean,
  happened_abroad boolean,
  damage_to_objects boolean,
  damage_to_vehicles boolean,
  old_motorcycle_plates character varying(255),
  intervention_authority boolean,
  police boolean,
  cc boolean,
  vvuu boolean,
  authority_data character varying(255),
  witness_description text,
  recoverability boolean,
  recoverability_percent numeric,
  incomplete_motivation character varying(255),
  is_robbery boolean,
  center_notified boolean,
  happened_on_center boolean,
  provider_code character varying(255),
  theft_description character varying(255),
  is_with_counterparty_internal boolean,
  is_cai_signed boolean,
  impact_point jsonb,
  additional_costs jsonb,
  damaged_anti_theft_service_id uuid,
  CONSTRAINT claims_damaged_anti_theft_service_fk FOREIGN KEY (damaged_anti_theft_service_id) REFERENCES public.anti_theft_service (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE INDEX idx_practice_id_claims_   ON public.claims_new   USING btree   (practice_id);
/*END NEW CLAIMS TABLE*/

/* START NOTE */
CREATE TABLE IF NOT EXISTS note (
    note_id varchar(255) PRIMARY KEY,
    title varchar(255),
    note_type varchar(255),
    is_important boolean,
    description text,
    created_at timestamptz,
    user_id varchar(255),
    hidden boolean,
    claims_id varchar(255),
	CONSTRAINT note_fk FOREIGN KEY (claims_id) REFERENCES public.claims_new(id) ON DELETE CASCADE
);
/* END NOTE */

/*START DAMAGED->DRIVER*/
CREATE TABLE IF NOT EXISTS claims_damaged_driver (
	id varchar(255) NOT NULL,
	driver_id numeric NULL,
	identification varchar (255) NULL,
	official_registration varchar (255) NULL,
	trading_name varchar (255) NULL,
	first_name varchar (255) NULL,
    last_name varchar (255) NULL,
    fiscal_code varchar (255) NULL,
    date_of_birth timestamptz NULL,
    phone varchar (255) NULL,
    email varchar (255) NULL,
    pec varchar (255) NULL,
    sex varchar (255) NULL,
    customer_id varchar (255) NULL,
    driver_injury bool NULL,
    main_address jsonb NULL,
    driving_license jsonb NULL,
	CONSTRAINT claims_damaged_driver_pkey PRIMARY KEY (id),
	CONSTRAINT claims_damaged_driver_fk FOREIGN KEY (id) REFERENCES public.claims_new(id) ON DELETE CASCADE
);
/*END DAMAGED->CUSTOMER*/

/*START COMPLAINT->DATA ACCIDENT->FROM COMPANY*/
CREATE TABLE IF NOT EXISTS claims_from_company (
	id varchar(255) NOT NULL,
	company varchar(255) NULL,
	number_sx varchar(255) NULL,
	type_sx varchar(255) NULL,
	inspectorate varchar(255) NULL,
	expert varchar(255) NULL,
	note varchar(255) NULL,
	dwl_man timestamptz NULL,
	last_update timestamptz NULL,
	status varchar(255) NULL,
	global_reserve numeric NULL,
	total_paid numeric NULL,
	CONSTRAINT claims_from_company_pkey PRIMARY KEY (id),
    CONSTRAINT claims_from_company_fk FOREIGN KEY (id) REFERENCES public.claims_new(id) ON DELETE CASCADE
);
/*END COMPLAINT->DATA ACCIDENT->FROM COMPANY*/

/*START COMPLAINT->DATA ACCIDENT->ENTRUSTED*/
CREATE TABLE IF NOT EXISTS claims_entrusted (
	id varchar(255) NOT NULL,
	auto_entrust bool NULL,
	id_entrusted_to UUID NULL,
	entrusted_to varchar(255) NULL,
	entrusted_day timestamptz NULL,
	type varchar(255) NULL,
	description varchar(255) NULL,
	entrusted_email varchar(255) NULL,
	expert bool NULL,
	number_sx_counterparty varchar(255) NULL,
	dwl_man timestamptz NULL,
	status varchar(255) NULL,
	CONSTRAINT claims_entrusted_pkey PRIMARY KEY (id),
	CONSTRAINT claims_entrusted_fk FOREIGN KEY (id) REFERENCES public.claims_new(id) ON DELETE CASCADE
);
/*END COMPLAINT->DATA ACCIDENT->ENTRUSTED*/

/*START DAMAGED->CONTRACT*/
CREATE TABLE IF NOT EXISTS claims_damaged_contract (
	id varchar(255) NOT NULL,
	contract_id numeric NULL,
    status varchar (255) NULL,
	contract_version_id numeric NULL,
	mileage numeric NULL,
	duration numeric NULL,
	contract_type varchar (255) NULL,
	start_date date NULL,
	end_date date NULL,
	leasing_company_id numeric NULL,
	leasing_company varchar(255) NULL,
	succeeding_contract_id numeric NULL,
	fleet_vehicle_id numeric NULL,
	license_plate varchar(255) NULL,
	CONSTRAINT claims_damaged_contract_pkey PRIMARY KEY (id),
    CONSTRAINT claims_damaged_contract_fk FOREIGN KEY (id) REFERENCES public.claims_new(id) ON DELETE CASCADE
);
/*END DAMAGED->CONTRACT*/

/*START DAMAGED->CUSTOMER*/
CREATE TABLE IF NOT EXISTS claims_damaged_customer (
	id varchar(255) NOT NULL,
	customer_id numeric NULL,
	trading_name varchar (255) NULL,
    status varchar (255) NULL,
    legal_name varchar (255) NULL,
    vat_number varchar (255) NULL,
    official_registration varchar (255) NULL,
    email varchar (255) NULL,
    phonenr varchar (255) NULL,
    pec varchar (255) NULL,
    main_address jsonb NULL,
	CONSTRAINT claims_damaged_customer_pkey PRIMARY KEY (id),
    CONSTRAINT claims_damaged_customer_fk FOREIGN KEY (id) REFERENCES public.claims_new(id) ON DELETE CASCADE
);
/*END DAMAGED->CUSTOMER*/

/*START DAMAGED->VEHICLE*/
CREATE TABLE IF NOT EXISTS claims_damaged_vehicle (
	id varchar(255) NOT NULL,
	fleet_vehicle_id numeric NULL,
	make varchar (255) NULL,
	model varchar (255) NULL,
	model_year varchar (255) NULL,
	kw varchar (255) NULL,
    cc_3 varchar (255) NULL,
    hp numeric NULL,
    din_hp numeric NULL,
    seats numeric NULL,
    net_weight numeric NULL,
    max_weight numeric NULL,
    doors varchar (255) NULL,
    body_style varchar (255) NULL,
    nature varchar (255) NULL,
    fuel_type varchar (255) NULL,
    co_2emission varchar (255) NULL,
    consumption varchar (255) NULL,
    license_plate varchar (255) NULL,
    ownership varchar (255) NULL,
    chassis_number varchar (255) NULL,
    registration_country varchar (255) NULL,
    registration_date date NULL,
    last_known_mileage varchar (255) NULL,
    external_color varchar (255) NULL,
    internal_color varchar (255) NULL,
    net_book_value numeric NULL,
    wreck bool NULL,
    license_plate_trailer varchar (255) NULL,
    chassis_number_trailer varchar (255) NULL,
    registration_country_trailer varchar (255) NULL,
    wreck_date timestamptz NULL,
	CONSTRAINT claims_damaged_vehicle_pkey PRIMARY KEY (id),
    CONSTRAINT claims_damaged_vehicle_fk FOREIGN KEY (id) REFERENCES public.claims_new(id) ON DELETE CASCADE
);
/*END DAMAGED->VEHICLE*/

/*START DAMAGED->INSURANCE INFO*/
CREATE TABLE IF NOT EXISTS claims_damaged_insurance_info (
	id varchar(255) PRIMARY KEY,
	insurance_info_id varchar (255) NULL,
	is_card bool NULL,
	tpl_service_id varchar (255) NULL,
	tpl_service varchar (255) NULL,
	tpl_max_coverage varchar (255) NULL,
	tpl_deductible varchar (255) NULL,
	tpl_company_id varchar (255) NULL,
	tpl_company varchar (255) NULL,
	tpl_tariff_id varchar (255) NULL,
	tpl_tariff varchar (255) NULL,
	tpl_tariff_code varchar (255) NULL,
	tpl_start_date varchar (255) NULL,
	tpl_end_date varchar (255) NULL,
	tpl_policy_number varchar (255) NULL,
	tpl_company_description varchar (255) NULL,
	tpl_insurance_company_id uuid NULL,
	tpl_insurance_policy_id uuid NULL,
	theft_service_id numeric NULL,
	theft_service varchar (255) NULL,
	theft_deductible_id numeric NULL,
	theft_deductible varchar (255) NULL,
    pai_service_id varchar (255) NULL,
	pai_service varchar (255) NULL,
	pai_max_coverage varchar (255) NULL,
	pai_deductible_id varchar (255) NULL,
	pai_deductible varchar (255) NULL,
	pai_company_id varchar (255) NULL,
	pai_company varchar (255) NULL,
	pai_tariff_id varchar (255) NULL,
	pai_tariff varchar (255) NULL,
	pai_tariff_code varchar (255) NULL,
	pai_percentage varchar (255) NULL,
	pai_start_date varchar (255) NULL,
	pai_end_date varchar (255) NULL,
	pai_policy_number varchar (255) NULL,
	pai_medical_costs varchar (255) NULL,
	pai_insurance_company_id uuid NULL,
	pai_insurance_policy_id uuid NULL,
	legal_cost_service_id varchar (255) NULL,
	legal_cost_service varchar (255) NULL,
	legal_cost_company_id varchar (255) NULL,
	legal_cost_company varchar (255) NULL,
	legal_cost_tariff_id varchar (255) NULL,
	legal_cost_tariff varchar (255) NULL,
	legal_cost_tariff_code varchar (255) NULL,
	legal_cost_start_date varchar (255) NULL,
	legal_cost_end_date varchar (255) NULL,
	legal_cost_policy_number varchar (255) NULL,
	legal_cost_company_description varchar (255) NULL,
	legal_cost_insurance_company_id uuid NULL,
	legal_cost_insurance_policy_id uuid NULL,
	kasko_deductible_id numeric NULL,
	kasko_deductible_value varchar (255) NULL,
    CONSTRAINT claims_damaged_insurance_info_fk FOREIGN KEY (id) REFERENCES public.claims_new(id) ON DELETE CASCADE
);
/*END DAMAGED->DAMAGED->INSURANCE_INFO*/

/*START REFUND*/
CREATE TABLE IF NOT EXISTS claims_refund (
	id varchar(255) PRIMARY KEY,
	po_sum numeric NULL,
	wreck bool NULL,
	wreck_value_pre numeric NULL,
	wreck_value_post numeric NULL,
    NBV varchar (255) NULL,
    franchise_amount_fcm numeric NULL,
    total_refund_expected numeric NULL,
    total_liquidation_received numeric NULL,
    definition_date timestamptz NULL,
    blu_eurotax varchar (255) NULL,
    amount_to_be_debited numeric NULL,
    delta_debit_date timestamptz NULL,
    split_list jsonb NULL,
    CONSTRAINT claims_refund_fk FOREIGN KEY (id) REFERENCES public.claims_new(id) ON DELETE CASCADE
);
/*END REFUND*/

/*START THEFT*/
CREATE TABLE IF NOT EXISTS claims_theft (
	id varchar(255) PRIMARY KEY,
	is_found bool NULL,
	operations_center_notified bool NULL,
	theft_occurred_on_center_ald bool NULL,
	supplier_code varchar (255) NULL,
    complaint_authority_police bool NULL,
    complaint_authority_cc bool NULL,
    complaint_authority_vvuu bool NULL,
	authority_data varchar (255) NULL,
	authority_telephone varchar (255) NULL,
	find_date timestamptz NULL,
	hour varchar (255) NULL,
	found_abroad bool NULL,
	vehicle_co varchar (255) NULL,
	sequestered bool NULL,
	address jsonb NULL,
	find_authority_police bool NULL,
	find_authority_cc bool NULL,
	find_authority_vvuu bool NULL,
	find_authority_data text NULL,
	find_authority_telephone varchar (255) NULL,
	theft_notes text NULL,
	find_notes text NULL,
	first_key_reception timestamptz NULL,
	second_key_reception timestamptz NULL,
	original_complaint_reception timestamptz NULL,
	copy_complaint_reception timestamptz NULL,
	original_report_reception timestamptz NULL,
	copy_report_reception timestamptz NULL,
	loss_possession_request timestamptz NULL,
	loss_possession_reception timestamptz NULL,
	return_possession_request timestamptz NULL,
	return_possession_reception timestamptz NULL,
	robbery bool NULL,
	key_management bool NULL,
	account_management bool NULL,
	administrative_position bool NULL,
	practice_id numeric NULL,
    re_registration bool NULL,
    re_registration_request timestamptz NULL,
    re_registration_at timestamptz NULL,
    re_registration_salesforce_case varchar (255) NULL,
    certificate_duplication bool NULL,
    certificate_duplication_request timestamptz NULL,
    certificate_duplication_at timestamptz NULL,
    certificate_duplication_salesforce_case varchar (255) NULL,
    stamp bool NULL,
    stamp_request timestamptz NULL,
    stamp_at timestamptz NULL,
    stamp_salesforce_case varchar(255) NULL,
    demolition bool NULL,
    demolition_request timestamptz NULL,
    demolition_at timestamptz NULL,
    demolition_salesforce_case varchar(255) NULL,
    is_with_receptions bool NULL,
    is_under_seizure bool NULL,
    CONSTRAINT claims_theft_fk FOREIGN KEY (id) REFERENCES public.claims_new(id) ON DELETE CASCADE
);
/*END THEFT*/

/*START DAMAGED->METADATA*/
CREATE TABLE IF NOT EXISTS claims_metadata (
	id varchar(255) PRIMARY KEY,
	role varchar (255) NULL,
	email varchar (255) NULL,
	user_id numeric NULL,
	url_mail varchar (255) NULL,
	cod_plate varchar (255) NULL,
	contract_id numeric NULL,
	customer_id varchar (255) NULL,
	fiscal_code varchar(255) NULL,
	plate_customer_id numeric NULL,
	user_session_token varchar(255) NULL,
    CONSTRAINT claims_metadata_fk FOREIGN KEY (id) REFERENCES public.claims_new(id) ON DELETE CASCADE
);
/*END DAMAGED->DAMAGED->METADATA*/







/*ALTER TABLE public.counterparty ADD COLUMN IF NOT EXISTS claims_id_new varchar(255) NULL;
ALTER TABLE public.counterparty ADD CONSTRAINT counterparty_claims_new_fk FOREIGN KEY (claims_id_new) REFERENCES claims_new(id);

ALTER TABLE public.anti_theft_request ADD COLUMN IF NOT EXISTS claims_id_new varchar(255) NULL;
ALTER TABLE public.anti_theft_request ADD CONSTRAINT antitheft_request_claims_new_fk FOREIGN KEY (claims_id_new) REFERENCES claims_new(id);*/



/*START create antitheftservice */

CREATE TABLE public.anti_theft_request_new
(
  id character varying(255) NOT NULL,
  created_at timestamptz,
  updated_at timestamptz,
  practice_id character varying(255),
  claim_id character varying(255),
  id_transaction character varying(255),
  provider_type character varying(255),
  response_type character varying(255),
  outcome character varying(255),
  sub_report numeric,
  g_power numeric,
  crash_number numeric,
  anomaly boolean,
  pdf character varying(255),
  response_message character varying(255),
  CONSTRAINT anti_theft_request_pkey_new PRIMARY KEY (id),
  CONSTRAINT antithef_fkey FOREIGN KEY (claim_id)
      REFERENCES public.claims_new (id) ON DELETE NO ACTION
);

/*END create antitheftservice */




CREATE TABLE public.counterparty_new
(
  counterparty_id character varying(255) NOT NULL,
  practice_id_counterparty bigint NOT NULL DEFAULT nextval('counterparty_id_sequence'::regclass),
  type character varying(255),
  status_repair character varying(255),
  procedure_repair character varying(255),
  user_create character varying(255),
  created_at timestamptz,
  user_update character varying(255),
  updated_at timestamptz,
  assigned_to jsonb,
  assigned_at timestamptz,
  insured jsonb,
  insurance_company jsonb,
  driver jsonb,
  vehicle jsonb,
  is_cai_signed boolean,
  impact_point jsonb,
  responsible boolean,
  eligibility boolean,
  attachments jsonb,
  historicals jsonb,
  description character varying(255),
  last_contact jsonb,
  manager uuid,
  canalization jsonb,
  policy_number character varying(255),
  policy_beginning_validity date,
  policy_end_validity date,
  management_type character varying(255),
  asked_for_damages boolean,
  legal_or_consultant boolean,
  date_request_damages timestamptz,
  expiration_date timestamptz,
  legal character varying(255),
  email_legal character varying(255),
  claims_id character varying(255),
  is_ald boolean,
  authorities jsonb,
  is_read_msa boolean,
  repair_created_at timestamptz,
  replacement_car boolean,
  motivation text,
  replacement_plate character varying(255),
  is_complete_documentation boolean,
  CONSTRAINT counterparty_new_pkey PRIMARY KEY (counterparty_id),
  CONSTRAINT counterparty_claims_id_fkey_key FOREIGN KEY (claims_id)
      REFERENCES public.claims_new (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT counterparty_manager_fkey_new FOREIGN KEY (manager)
      REFERENCES public.manager (id) ON DELETE NO ACTION
);

/*START DAMAGED*/
ALTER TABLE claims ADD COLUMN IF NOT EXISTS is_cai_signed bool NULL;
ALTER TABLE claims ADD COLUMN IF NOT EXISTS impact_point jsonb NULL;
ALTER TABLE claims ADD COLUMN IF NOT EXISTS additional_costs jsonb NULL;
/*END DAMAGED*/

/*START DAMAGED->FLEET MANAGER*/
CREATE TABLE IF NOT EXISTS claims_damaged_fleet_manager (
    id varchar (255) PRIMARY KEY,
	fleet_manager_id numeric,
	identification varchar (255) NULL,
	official_registration varchar (255) NULL,
	first_name varchar (255) NULL,
	last_name varchar (255) NULL,
    title varchar (255) NULL,
    email varchar (255) NULL,
    phone_prefix varchar (255) NULL,
    phone varchar (255) NULL,
    secondary_phone_prefix varchar (255) NULL,
    secondary_phone varchar (255) NULL,
    sex varchar (255) NULL,
    customer_id numeric NULL,
    main_address jsonb NULL
);
CREATE TABLE IF NOT EXISTS claims_to_claims_damaged_fleet_manager (
  claims_id varchar (255),
  damaged_fleet_manager_id varchar (255),
  FOREIGN KEY (claims_id) REFERENCES claims_new (id),
  FOREIGN KEY (damaged_fleet_manager_id) REFERENCES claims_damaged_fleet_manager (id)
);
/*END DAMAGED->FLEET MANAGER*/

/*START DAMAGED->REGISTY LIST*/
CREATE TABLE IF NOT EXISTS claims_damaged_registry (
	id varchar(255) PRIMARY KEY,
	type varchar(255) NULL,
	id_transaction numeric NULL,
	cod_imei varchar (255) NULL,
	cod_pack varchar (255) NULL,
	cod_provider varchar (255) NULL,
    provider varchar (255) NULL,
    date_activation timestamptz NULL,
    date_end timestamptz (255) NULL,
    cod_serialnumber varchar (255) NULL,
    voucher_id numeric NULL,
    claim_id varchar (255) NULL,
    CONSTRAINT registry_fkey FOREIGN KEY (claim_id) REFERENCES public.claims_new (id)
);
/*END DAMAGED->REGISTRY*/

/*START AUTHORITY*/
CREATE TABLE IF NOT EXISTS claims_authority (
	id varchar(255) PRIMARY KEY,
	is_invoiced bool NULL,
	authority_dossier_id varchar (255) NULL,
	authority_working_id varchar (255) NULL,
	working_number varchar (255) NULL,
	authority_dossier_number varchar (255) NULL,
	total numeric NULL,
	accepting_date timestamptz NULL,
	authorization_date timestamptz NULL,
	rejection_date timestamptz NULL,
	rejection bool NULL,
	note_rejection text NULL,
	working_created_at timestamptz NULL,
	franchise_number numeric NULL,
	is_wreck bool NULL,
	wreck_casual varchar (255) NULL,
	event_date timestamptz NULL,
	event_type varchar (255) NULL,
	po_details jsonb NULL,
	status varchar (255) NULL,
	type varchar (255) NULL,
	is_not_duplicate bool NULL,
	oldest bool NULL,
	commodity_details jsonb NULL,
	working_status varchar (255) NULL,
	user_id varchar (255) NULL,
	firstname varchar (255) NULL,
	lastname varchar (255) NULL,
	nbv numeric NULL,
	wreck_value varchar (255) NULL,
	claims_linked_size numeric NULL,
	historical jsonb NULL,
	is_msa_downloaded bool NULL,
	is_migrated bool NULL
);
CREATE TABLE IF NOT EXISTS claims_to_claims_authority (
  claims_id varchar (255),
  authority_id varchar (255),
  FOREIGN KEY (claims_id) REFERENCES claims_new (id),
  FOREIGN KEY (authority_id) REFERENCES claims_authority (id)
);
/*END AUTHORITY*/

ALTER table public.claims ADD column converted bool DEFAULT FALSE;

/*OLD CLAIMS INDEXES*/
CREATE INDEX IF NOT EXISTS idx_claims_converted ON  claims USING btree(converted);
/*ANTI THEFT SERVICE INDEXES*/
CREATE INDEX IF NOT EXISTS idx_anti_theft_service_codpack ON  anti_theft_service USING btree(code_anti_theft_service);
/*NEW CLAIMS INDEXES*/
CREATE INDEX IF NOT EXISTS idx_claims_date_accident ON  claims_new USING btree(date_accident);
CREATE INDEX IF NOT EXISTS idx_claims_plate ON  claims_new USING btree(plate);
CREATE INDEX IF NOT EXISTS idx_claims_locator ON  claims_new USING btree(claim_locator);
CREATE INDEX IF NOT EXISTS idx_claims_practice_id ON  claims_new USING btree(practice_id);
CREATE INDEX IF NOT EXISTS idx_claims_in_evidence ON  claims_new USING btree(in_evidence);
CREATE INDEX IF NOT EXISTS idx_claims_with_continuation ON  claims_new USING btree(with_continuation);
CREATE INDEX IF NOT EXISTS idx_claims_type_accident ON  claims_new USING btree(type_accident);
CREATE INDEX IF NOT EXISTS idx_claims_status_updated_at ON  claims_new USING btree(status_updated_at);
CREATE INDEX IF NOT EXISTS idx_claims_created_at ON  claims_new USING btree(created_at);
CREATE INDEX IF NOT EXISTS idx_claims_is_read_acclaims ON  claims_new USING btree(is_read_acclaims);
CREATE INDEX IF NOT EXISTS idx_claims_is_with_counterparty ON  claims_new USING btree(is_with_counterparty);
CREATE INDEX IF NOT EXISTS idx_claims_type ON  claims_new USING btree(type);
CREATE INDEX IF NOT EXISTS idx_claims_status ON  claims_new USING btree(status);
CREATE INDEX IF NOT EXISTS idx_claims_po_var ON  claims_new USING btree(po_variation);
/*CLAIMS DAMAGED CUSTOMER INDEXES*/
CREATE INDEX IF NOT EXISTS idx_claims_damaged_customer_id ON  claims_damaged_customer USING btree(customer_id);
CREATE INDEX IF NOT EXISTS idx_claims_damaged_customer_name ON  claims_damaged_customer USING btree(legal_name);
/*CLAIMS DAMAGED CONTRACT INDEXES*/
CREATE INDEX IF NOT EXISTS idx_claims_damaged_contract_id ON  claims_damaged_contract USING btree(contract_id);
/*CLAIMS DAMAGED VEHICLE INDEXES*/
CREATE INDEX IF NOT EXISTS idx_claims_vehicle_fleet_vehicle_id ON  claims_damaged_vehicle USING btree(fleet_vehicle_id);
CREATE INDEX IF NOT EXISTS idx_claims_vehicle_license_plate ON  claims_damaged_vehicle USING btree(license_plate);
CREATE INDEX IF NOT EXISTS idx_claims_vehicle_chassis ON  claims_damaged_vehicle USING btree(chassis_number);
CREATE INDEX IF NOT EXISTS idx_claims_vehicle_nature ON  claims_damaged_vehicle USING btree(nature);
/*CLAIMS DAMAGED INSURANCE INFO INDEXES*/
CREATE INDEX IF NOT EXISTS idx_claims_insurance_info_tpl_company ON  claims_damaged_insurance_info USING btree(tpl_company);
CREATE INDEX IF NOT EXISTS idx_claims_insurance_info_insurance_company_id ON  claims_damaged_insurance_info USING btree(tpl_insurance_company_id);
/*CLAIMS ENTRUSTED INDEXES*/
CREATE INDEX IF NOT EXISTS idx_claims_entrusted_auto_entrust ON  claims_entrusted USING btree(auto_entrust);
CREATE INDEX IF NOT EXISTS idx_claims_entrusted_type ON  claims_entrusted USING btree(type);
CREATE INDEX IF NOT EXISTS idx_claims_entrusted_id_entrusted_to ON  claims_entrusted USING btree(id_entrusted_to);
CREATE INDEX IF NOT EXISTS idx_claims_entrusted_entrusted_day ON  claims_entrusted USING btree(entrusted_day);
/*CLAIMS THEFT INDEXES*/
CREATE INDEX IF NOT EXISTS idx_claims_theft_is_with_receptions ON  claims_theft USING btree(is_with_receptions);
CREATE INDEX IF NOT EXISTS idx_claims_theft_first_key_receptions ON  claims_theft USING btree(first_key_reception);
CREATE INDEX IF NOT EXISTS idx_claims_theft_second_key_receptions ON  claims_theft USING btree(second_key_reception);
/*CLAIMS AUTHORITY INDEXES*/
CREATE INDEX IF NOT EXISTS idx_auth_dossier_id ON  claims_authority USING btree(authority_dossier_id);
CREATE INDEX IF NOT EXISTS idx_auth_working_id ON  claims_authority USING btree(authority_working_id);
CREATE INDEX IF NOT EXISTS idx_auth_is_migrated ON  claims_authority USING btree(is_migrated);
CREATE INDEX IF NOT EXISTS idx_auth_dossier_number ON  claims_authority USING btree(authority_dossier_number);
CREATE INDEX IF NOT EXISTS idx_auth_working_number ON  claims_authority USING btree(working_number);
CREATE INDEX IF NOT EXISTS idx_auth_is_invoiced ON  claims_authority USING btree(is_invoiced);
/*CLAIMS REFUND INDEXES*/
CREATE INDEX IF NOT EXISTS idx_refund_definition_date ON  claims_refund USING btree(definition_date);
/*CLAIMS METADATA INDEXES*/
CREATE INDEX IF NOT EXISTS idx_metadata_user_id ON  claims_metadata USING btree(user_id);
/* CLAIMS FROM COMPANY */
CREATE INDEX IF NOT EXISTS idx_fc_number_sx ON  claims_from_company USING btree(number_sx);