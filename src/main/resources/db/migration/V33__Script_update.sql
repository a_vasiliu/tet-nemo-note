with counterparty_update as (
select('{'||index-1||',last_contact}')::text[] as path
from claims,jsonb_array_elements(counterparty) with ordinality arr(counterpart, index)
)
Update claims
set counterparty = jsonb_set(counterparty,counterparty_update.path,'null',false)
from counterparty_update;
