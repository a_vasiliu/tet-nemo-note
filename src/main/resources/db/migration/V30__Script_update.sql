ALTER TABLE data_management_export ADD COLUMN IF NOT EXISTS user_firstname varchar(255);
ALTER TABLE data_management_export ADD COLUMN IF NOT EXISTS user_lastname varchar(255);
ALTER TABLE data_management_export ADD COLUMN IF NOT EXISTS user_role varchar(255);
