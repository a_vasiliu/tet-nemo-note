﻿INSERT INTO attachment_type (id, name, groups, is_active)
VALUES('ebe5583c-4a55-11e9-8646-d663bd873d93','Modulo Blu (CAI)','RGC',TRUE);
INSERT INTO attachment_type (id, name, groups, is_active)
VALUES('ebe55b70-4a55-11e9-8646-d663bd873d93','Denuncia Autorità','RGAC',TRUE);
INSERT INTO attachment_type (id, name, groups, is_active)
VALUES('ebe55d0a-4a55-11e9-8646-d663bd873d93','Preventivo','R',TRUE);
INSERT INTO attachment_type (id, name, groups, is_active)
VALUES('ebe55e68-4a55-11e9-8646-d663bd873d93','Foto','R',TRUE);
INSERT INTO attachment_type (id, name, groups, is_active)
VALUES('ebe5628c-4a55-11e9-8646-d663bd873d93','Altro','RGAC',TRUE);
INSERT INTO attachment_type (id, name, groups, is_active)
VALUES('ebe56426-4a55-11e9-8646-d663bd873d93','Denuncia semplice','RGCA',TRUE);
INSERT INTO attachment_type (id, name, groups, is_active)
VALUES('ebe5658e-4a55-11e9-8646-d663bd873d93','Fattura','R',TRUE);
INSERT INTO attachment_type (id, name, groups, is_active)
VALUES('ebe566ec-4a55-11e9-8646-d663bd873d93','Liquidazione','RG',TRUE);
INSERT INTO attachment_type (id, name, groups, is_active)
VALUES('ebe56836-4a55-11e9-8646-d663bd873d93','Modulo furto','R',TRUE);
INSERT INTO attachment_type (id, name, groups, is_active)
VALUES('ebe56a7a-4a55-11e9-8646-d663bd873d93','Documentazione x ritiro veicolo','R',TRUE);
INSERT INTO attachment_type (id, name, groups, is_active)
VALUES('ebe56f02-4a55-11e9-8646-d663bd873d93','Riscontro della compagnia','R',TRUE);
INSERT INTO attachment_type (id, name, groups, is_active)
VALUES('ebe57088-4a55-11e9-8646-d663bd873d93','MODULO DI FURTO & RITROVAMENTO','RG',TRUE);
INSERT INTO attachment_type (id, name, groups, is_active)
VALUES('ebe571e6-4a55-11e9-8646-d663bd873d93','Modulo di Ritrovamento','RG',TRUE);
INSERT INTO attachment_type (id, name, groups, is_active)
VALUES('ebe57344-4a55-11e9-8646-d663bd873d93','Verbale ritr. e restituzione','RG',TRUE);
INSERT INTO attachment_type (id, name, groups, is_active)
VALUES('ebe575a6-4a55-11e9-8646-d663bd873d93','Allegato per Polizza','P',TRUE);
INSERT INTO attachment_type (id, name, groups, is_active)
VALUES('ebe5772c-4a55-11e9-8646-d663bd873d93','Integrazione di denuncia','RG',TRUE);
INSERT INTO attachment_type (id, name, groups, is_active)
VALUES('ebe57880-4a55-11e9-8646-d663bd873d93','Comunicazione generica autorità','RG',TRUE);
INSERT INTO attachment_type (id, name, groups, is_active)
VALUES('ebe57c9a-4a55-11e9-8646-d663bd873d93','Verbale sequestro e dissequestro','RG',TRUE);
INSERT INTO attachment_type (id, name, groups, is_active)
VALUES('ebe57e0c-4a55-11e9-8646-d663bd873d93','Lettera Affido PAI','R',TRUE);
INSERT INTO attachment_type (id, name, groups, is_active)
VALUES('ebe57f60-4a55-11e9-8646-d663bd873d93','VERBALE RIMPATRIO','RG',TRUE);
INSERT INTO attachment_type (id, name, groups, is_active)
VALUES('ebe580be-4a55-11e9-8646-d663bd873d93','Denuncia di Furto','RG',TRUE);
INSERT INTO attachment_type (id, name, groups, is_active)
VALUES('ebe582b2-4a55-11e9-8646-d663bd873d93','DENUNCIA DI FURTO & VERBALE RITROVAMENTO','RG',TRUE);
INSERT INTO attachment_type (id, name, groups, is_active)
VALUES('ebe5842e-4a55-11e9-8646-d663bd873d93','VERBALE SEQUESTRO','RG',TRUE);
INSERT INTO attachment_type (id, name, groups, is_active)
VALUES('ebe5888e-4a55-11e9-8646-d663bd873d93','VERBALE DISSEQUESTRO','RG',TRUE);
INSERT INTO attachment_type (id, name, groups, is_active)
VALUES('ebe58a1e-4a55-11e9-8646-d663bd873d93','Richiesta della Compagnia','RGCA',TRUE);
INSERT INTO attachment_type (id, name, groups, is_active)
VALUES('ebe58b68-4a55-11e9-8646-d663bd873d93','Reiezione','RGAC',TRUE);
INSERT INTO attachment_type (id, name, groups, is_active)
VALUES('ebe58cc6-4a55-11e9-8646-d663bd873d93','Modulo Wreck','RGAC',TRUE);
INSERT INTO attachment_type (id, name, groups, is_active)
VALUES('ebe58e1a-4a55-11e9-8646-d663bd873d93','COMUNICAZIONE AUTORITA’ RITROVAMENTO','RG',TRUE);
INSERT INTO attachment_type (id, name, groups, is_active)
VALUES('ebe58f78-4a55-11e9-8646-d663bd873d93','VERBALE RITROVAMENTO TARGA','RG',TRUE);
INSERT INTO attachment_type (id, name, groups, is_active)
VALUES('ebe59414-4a55-11e9-8646-d663bd873d93','VERBALE RITROVAMENTO MOTORE','RG',TRUE);
INSERT INTO attachment_type (id, name, groups, is_active)
VALUES('ebe596b2-4a55-11e9-8646-d663bd873d93','VERBALE RITROVAMENTO NAVIGATORE SATELLITARE','RG',TRUE);
INSERT INTO attachment_type (id, name, groups, is_active)
VALUES('ebe59874-4a55-11e9-8646-d663bd873d93','NOTIFICA DECRETO DI SEQUESTRO','RG',TRUE);
INSERT INTO attachment_type (id, name, groups, is_active)
VALUES('ebe599dc-4a55-11e9-8646-d663bd873d93','NOTIFICA DECRETO DI DISSEQUESTRO','RG',TRUE);
INSERT INTO attachment_type (id, name, groups, is_active)
VALUES('ebe59b3a-4a55-11e9-8646-d663bd873d93','DIC. ALD NON RECUPERO MEZZO RITROVATO DA FURTO','RG',TRUE);
INSERT INTO attachment_type (id, name, groups, is_active)
VALUES('ebe59c84-4a55-11e9-8646-d663bd873d93','DELEGA ALD AUTOMOTIVE','RG',TRUE);
INSERT INTO attachment_type (id, name, groups, is_active)
VALUES('ebe59dce-4a55-11e9-8646-d663bd873d93','DENUNCIA APPROPRIAZIONE INDEBITA','RG',TRUE);
INSERT INTO attachment_type (id, name, groups, is_active)
VALUES('ebe5a224-4a55-11e9-8646-d663bd873d93','INTEGRAZIONE VERBALE DI RITROVAMENTO','RG',TRUE);
INSERT INTO attachment_type (id, name, groups, is_active)
VALUES('ebe5a396-4a55-11e9-8646-d663bd873d93','VERBALE RITROVAMENTO TARGA','RG',TRUE);
INSERT INTO attachment_type (id, name, groups, is_active)
VALUES('ebe5a4ea-4a55-11e9-8646-d663bd873d93','VERBALE RITROVAMENTO COMPONENTI','RG',TRUE);
INSERT INTO attachment_type (id, name, groups, is_active)
VALUES('ebe5a648-4a55-11e9-8646-d663bd873d93','DIC ALD NON RECUPERO COMPONENTE RITROVATO DA FURTO','RG',TRUE);
INSERT INTO attachment_type (id, name, groups, is_active)
VALUES('ebe5a792-4a55-11e9-8646-d663bd873d93','REPORT CRASH PROVIDER','R',TRUE);
INSERT INTO claims_type (id, type, claims_accident_type)
VALUES ('7a0747aa-4ae8-11e9-8646-d663bd873d93','RC - Non Verificabile','RC_NON_VERIFICABILE');
INSERT INTO claims_type (id, type, claims_accident_type)
VALUES ('7a074aa2-4ae8-11e9-8646-d663bd873d93','RC - Attiva','RC_ATTIVA');
INSERT INTO claims_type (id, type, claims_accident_type)
VALUES ('7a074c32-4ae8-11e9-8646-d663bd873d93','RC - Passiva','RC_PASSIVA');
INSERT INTO claims_type (id, type, claims_accident_type)
VALUES ('7a074d9a-4ae8-11e9-8646-d663bd873d93','RC - Concorsuale','RC_CONCORSUALE');
INSERT INTO claims_type (id, type, claims_accident_type)
VALUES ('7a074f02-4ae8-11e9-8646-d663bd873d93','CARD - Non verificabile Firma Singola','CARD_NON_VERIFICABILE_FIRMA_SINGOLA');
INSERT INTO claims_type (id, type, claims_accident_type)
VALUES ('7a07504c-4ae8-11e9-8646-d663bd873d93','CARD - Attiva Firma Singola','CARD_ATTIVA_FIRMA_SINGOLA');
INSERT INTO claims_type (id, type, claims_accident_type)
VALUES ('7a0751a0-4ae8-11e9-8646-d663bd873d93','CARD - Passiva Firma Singola','CARD_PASSIVA_FIRMA_SINGOLA');
INSERT INTO claims_type (id, type, claims_accident_type)
VALUES ('7a07565a-4ae8-11e9-8646-d663bd873d93','CARD - Concorsuale Firma Singola','CARD_CONCORSUALE_FIRMA_SINGOLA');
INSERT INTO claims_type (id, type, claims_accident_type)
VALUES ('7a0758d0-4ae8-11e9-8646-d663bd873d93','CARD - Non verificabile doppia firma','CARD_NON_VERIFICABILE_DOPPIA_FIRMA');
INSERT INTO claims_type (id, type, claims_accident_type)
VALUES ('7a075bc8-4ae8-11e9-8646-d663bd873d93','CARD - Attiva doppia firma','CARD_ATTIVA_DOPPIA_FIRMA');
INSERT INTO claims_type (id, type, claims_accident_type)
VALUES ('7a075e8e-4ae8-11e9-8646-d663bd873d93','CARD - Passiva doppia firma','CARD_PASSIVA_DOPPIA_FIRMA');
INSERT INTO claims_type (id, type, claims_accident_type)
VALUES ('7a0761a4-4ae8-11e9-8646-d663bd873d93','CARD - Concorsuale doppia firma','CARD_CONCORSUALE_DOPPIA_FIRMA');
INSERT INTO claims_type (id, type, claims_accident_type)
VALUES ('7a0764b0-4ae8-11e9-8646-d663bd873d93','Kasko - Urto Contro oggetti fissi','KASKO_URTO_CONTRO_OGGETTI_FISSI');
INSERT INTO claims_type (id, type, claims_accident_type)
VALUES ('7a076b0e-4ae8-11e9-8646-d663bd873d93','Atto Vandalico','ATTO_VANDALICO');
INSERT INTO claims_type (id, type, claims_accident_type)
VALUES ('7a076e24-4ae8-11e9-8646-d663bd873d93','Danno Ritrovato in Parcheggio','DANNO_RITROVATO_IN_PARCHEGGIO');
INSERT INTO claims_type (id, type, claims_accident_type)
VALUES ('7a077108-4ae8-11e9-8646-d663bd873d93','Eventi Naturali','EVENTI_NATURALI');
INSERT INTO claims_type (id, type, claims_accident_type)
VALUES ('7a0773d8-4ae8-11e9-8646-d663bd873d93','Cristalli','CRISTALLI');
INSERT INTO claims_type (id, type, claims_accident_type)
VALUES ('7a07761c-4ae8-11e9-8646-d663bd873d93','Incendio','INCENDIO');
INSERT INTO claims_type (id, type, claims_accident_type)
VALUES ('7a07784c-4ae8-11e9-8646-d663bd873d93','Chiavi Smarrite o Rubate','CHIAVI_SMARRITE_O_RUBATE');
INSERT INTO claims_type (id, type, claims_accident_type)
VALUES ('7a077a86-4ae8-11e9-8646-d663bd873d93','Tentato Furto','TENTATO_FURTO');
INSERT INTO claims_type (id, type, claims_accident_type)
VALUES ('f0a13a82-4ae9-11e9-8646-d663bd873d93','Furto Parziale','FURTO_PARZIALE');
INSERT INTO claims_type (id, type, claims_accident_type)
VALUES ('f0a13e88-4ae9-11e9-8646-d663bd873d93','Furto e Ritrovamento','FURTO_E_RITROVAMENTO');
INSERT INTO claims_type (id, type, claims_accident_type)
VALUES ('f0a14144-4ae9-11e9-8646-d663bd873d93','Furto Totale','FURTO_TOTALE');
INSERT INTO insurance_company (id, name, address, zip_code, city, province, state, telephone, fax, email, web_site, contact, ania_code, join_card, contact_pai, email_pai, is_active)
VALUES ('a83bdfb8-4a58-11e9-8646-d663bd873d93', 'GENERALI ITALIA', null, null, null, null, 'Italia', null, null, 'flotte@generaligroup.com', null, null, 8, TRUE, null, null, TRUE);
INSERT INTO insurance_company (id, name, address, zip_code, city, province, state, telephone, fax, email, web_site, contact, ania_code, join_card, contact_pai, email_pai, is_active)
VALUES ('a83be2a6-4a58-11e9-8646-d663bd873d93', 'UnipolSai Assicurazioni', null, null, null, null, null, null, null, null, null, null, 34, TRUE, null, null, TRUE);
INSERT INTO insurance_company (id, name, address, zip_code, city, province, state, telephone, fax, email, web_site, contact, ania_code, join_card, contact_pai, email_pai, is_active)
VALUES ('a83be648-4a58-11e9-8646-d663bd873d93', 'ZURICH', 'Via Benigno Crespi 23', 20, 'MILANO', 'MI', 'ITALIA', null, null, null, null, null, null, TRUE, null, null, TRUE);
INSERT INTO insurance_company (id, name, address, zip_code, city, province, state, telephone, fax, email, web_site, contact, ania_code, join_card, contact_pai, email_pai, is_active)
VALUES ('a83be7e2-4a58-11e9-8646-d663bd873d93', 'SOGESSUR', 'VIA GUGLIELMO SILVA 34', 20, 'MILANO', 'MI', 'ITALIA', null, null, null, null, null, null, FALSE, null, null, TRUE);
INSERT INTO insurance_company (id, name, address, zip_code, city, province, state, telephone, fax, email, web_site, contact, ania_code, join_card, contact_pai, email_pai, is_active)
VALUES ('a83be940-4a58-11e9-8646-d663bd873d93', 'ALLEANZA TORO S.P.A', 'Via Mazzini, 53', null, 'TORINO', 'TO', 'Italia', null, null, null, null, null, 8, TRUE, null, null, TRUE);
INSERT INTO insurance_manager(id, name, address, zip_code, city, province, state, reference_person, telephone, email, is_active)
VALUES ('bf25ac0a-4a61-11e9-b4d7-d663bd873d93', 'AON (VODAFONE)', 'Via A. Ponti, 8/10', 20143, 'Milano', 'MI', 'Italia', 'Irene Fileccia - Maria Mottola', '+39.02.45434.45', 'SINISTRI.VODAFONE@AON.IT', TRUE);
INSERT INTO insurance_manager(id, name, email, is_active)
VALUES ('5eb70b84-4a61-11e9-8646-d663bd873d93', 'SG MULTISERASS', 'aldsgi@multiserass.com', TRUE);
INSERT INTO insurance_manager(id, code, name, email, is_active)
VALUES ('bf268332-4a61-11e9-b4d7-d663bd873d93', 'X', 'Multi Serass', 'ald@multiserass.com', TRUE);
INSERT INTO insurance_manager(id, code, name, email, is_active)
VALUES ('9acf4508-4a63-11e9-8646-d663bd873d93', 'J', 'Zurich canale diretto (EnterpriseEnterprise Services Italia srl)', 'sinistri.auto@it.zurich.com; segreteria@studioitaliaautomotive.it', TRUE);
INSERT INTO insurance_manager(id, name, email, reference_person, is_active)
VALUES ('9acf4846-4a63-11e9-8646-d663bd873d93', 'UnipolSai x Cameo', 'agenziasai@zavattaroassicurazioni.it', 'agenziasai@zavattaroassicurazioni.it', TRUE);
INSERT INTO insurance_policy (id, type, description, number_policy, insurance_company, broker, insurance_manager, client_code, client, annotations, beginning_validity, end_validity, book_register, book_register_code, last_renewal_notification, renewal_notification, is_active)
VALUES ('c1bc6f00-4a5f-11e9-8646-d663bd873d93', 'RCA', null, null, 'a83be940-4a58-11e9-8646-d663bd873d93', null, null, 350443, 'AGFA GRAPHICS SRL', 'Paolo Marco Benedetti', '2010-12-31', '2011-12-31', FALSE, null, null, FALSE, TRUE);
INSERT INTO insurance_policy (id, type, description, number_policy, insurance_company, broker, insurance_manager, client_code, client, annotations, beginning_validity, end_validity, book_register, book_register_code, last_renewal_notification, renewal_notification, is_active)
VALUES ('c1bc71e4-4a5f-11e9-8646-d663bd873d93', 'RCA', 'Sogessur ALDRe - STOCK', 'su2018lm001-ST00', 'a83be7e2-4a58-11e9-8646-d663bd873d93', null, '5eb70b84-4a61-11e9-8646-d663bd873d93', 1, 'ALD Automotive Italia', null, '2018-11-01', '2019-10-31', TRUE, '402968', null, FALSE, TRUE);
INSERT INTO insurance_policy (id, type, description, number_policy, insurance_company, broker, insurance_manager, client_code, client, annotations, beginning_validity, end_validity, book_register, book_register_code, last_renewal_notification, renewal_notification, is_active)
VALUES ('bf267c7a-4a61-11e9-b4d7-d663bd873d93', 'RCA', null, '92014603', 'a83be648-4a58-11e9-8646-d663bd873d93', null, 'bf25ac0a-4a61-11e9-b4d7-d663bd873d93', 578803, 'ES SHARED SERVICE CENTER SPA (ex HP)', null, '2018-10-31', '2019-10-31', FALSE, null, null, TRUE, TRUE);
INSERT INTO insurance_policy (id, type, description, number_policy, insurance_company, broker, insurance_manager, client_code, client, annotations, beginning_validity, end_validity, book_register, book_register_code, last_renewal_notification, renewal_notification, is_active)
VALUES ('bf268530-4a61-11e9-b4d7-d663bd873d93', 'RCA', null, '92014316', 'a83be648-4a58-11e9-8646-d663bd873d93', null, '9acf4508-4a63-11e9-8646-d663bd873d93', 495496, 'Enterprise Services Energy Italia S.r.l. (ex HP)', null, '2018-10-31', '2019-10-31', FALSE, null, null, TRUE, TRUE);
INSERT INTO insurance_policy (id, type, description, number_policy, insurance_company, broker, insurance_manager, client_code, client, annotations, beginning_validity, end_validity, book_register, book_register_code, last_renewal_notification, renewal_notification, is_active)
VALUES ('9acf49d6-4a63-11e9-8646-d663bd873d93', 'RCA', null, '1/57309/30/163722383', 'a83be2a6-4a58-11e9-8646-d663bd873d93', null, '9acf4846-4a63-11e9-8646-d663bd873d93', 40295639, 'CAMEO SPA', null, '2018-10-31', '2019-10-31', FALSE, null, null, TRUE, TRUE);


INSERT INTO risk(id, type, description, maximal, exemption, overdraft, active, insurance_policy_id)
VALUES ('1f3d120a-4aed-11e9-8646-d663bd873d93','Responsabilità Civile Auto (RCA)',null,10000000,null,null,null,'bf267c7a-4a61-11e9-b4d7-d663bd873d93');

INSERT INTO risk(id, type, description, maximal, exemption, overdraft, active, insurance_policy_id)
VALUES ('1f3d14e4-4aed-11e9-8646-d663bd873d93','Responsabilità Civile Auto (RCA)',null,10000000,null,null,null,'bf268530-4a61-11e9-b4d7-d663bd873d93');

INSERT INTO risk(id, type, description, maximal, exemption, overdraft, active, insurance_policy_id)
VALUES ('1f3d178c-4aed-11e9-8646-d663bd873d93','Responsabilità Civile Auto (RCA)',null,15000000,null,null,null,'9acf49d6-4a63-11e9-8646-d663bd873d93');
INSERT INTO risk(id, type, description, maximal, exemption, overdraft, active, insurance_policy_id)
VALUES ('1f3d2632-4aed-11e9-8646-d663bd873d93','Eventi Naturali',null,null,250,null,null,'9acf49d6-4a63-11e9-8646-d663bd873d93');
INSERT INTO risk(id, type, description, maximal, exemption, overdraft, active, insurance_policy_id)
VALUES ('1f3d18fe-4aed-11e9-8646-d663bd873d93','Atto Vandalico',null,null,250,null,null,'9acf49d6-4a63-11e9-8646-d663bd873d93');
INSERT INTO risk(id, type, description, maximal, exemption, overdraft, active, insurance_policy_id)
VALUES ('1f3d1b60-4aed-11e9-8646-d663bd873d93','Cristalli',null,null,null,null,null,'9acf49d6-4a63-11e9-8646-d663bd873d93');
INSERT INTO risk(id, type, description, maximal, exemption, overdraft, active, insurance_policy_id)
VALUES ('1f3d1e30-4aed-11e9-8646-d663bd873d93','Danno Ritrovato in Parcheggio',null,null,null,null,null,'9acf49d6-4a63-11e9-8646-d663bd873d93');
INSERT INTO risk(id, type, description, maximal, exemption, overdraft, active, insurance_policy_id)
VALUES ('1f3d1fac-4aed-11e9-8646-d663bd873d93','Kasko - Urto Contro oggetti fissi',null,null,null,null,null,'9acf49d6-4a63-11e9-8646-d663bd873d93');
INSERT INTO risk(id, type, description, maximal, exemption, overdraft, active, insurance_policy_id)
VALUES ('1f3d224a-4aed-11e9-8646-d663bd873d93','Furto Totale',null,null,null,null,null,'9acf49d6-4a63-11e9-8646-d663bd873d93');
INSERT INTO motivation_type (id, description, order_id, is_active)
VALUES('3363f83a-4a51-11e9-8646-d663bd873d93','Dinamica sinistro poco chiara',12,TRUE);
INSERT INTO motivation_type (id, description, order_id, is_active)
VALUES('3363fb1e-4a51-11e9-8646-d663bd873d93','Manca targa e nome assicurazione di controparte',18,TRUE);
INSERT INTO motivation_type (id, description, order_id, is_active)
VALUES('3363fc90-4a51-11e9-8646-d663bd873d93','Manca dichiarazione testimone con documento ident.',17,TRUE);
INSERT INTO motivation_type (id, description, order_id, is_active)
VALUES('3363fdda-4a51-11e9-8646-d663bd873d93','Mancano data e luogo del sinistro',16,TRUE);
INSERT INTO motivation_type (id, description, order_id, is_active)
VALUES('3363ff2e-4a51-11e9-8646-d663bd873d93','Manca dichiarazione dell''accaduto firmata',5,TRUE);
INSERT INTO motivation_type (id, description, order_id, is_active)
VALUES('33640366-4a51-11e9-8646-d663bd873d93','Targa di controparte assente',19,TRUE);
INSERT INTO motivation_type (id, description, order_id, is_active)
VALUES('33640514-4a51-11e9-8646-d663bd873d93','Assicurazione di controparte assente',8,TRUE);
INSERT INTO motivation_type (id, description, order_id, is_active)
VALUES('33640672-4a51-11e9-8646-d663bd873d93','Nome di controparte assente',20,TRUE);
INSERT INTO motivation_type (id, description, order_id, is_active)
VALUES('336407bc-4a51-11e9-8646-d663bd873d93','Manca dinamica',6,TRUE);
INSERT INTO motivation_type (id, description, order_id, is_active)
VALUES('336408fc-4a51-11e9-8646-d663bd873d93','Vedi motivazione:',7,TRUE);
INSERT INTO motivation_type (id, description, order_id, is_active)
VALUES('33640e56-4a51-11e9-8646-d663bd873d93','Manca data sinistro',11,TRUE);
INSERT INTO motivation_type (id, description, order_id, is_active)
VALUES('33640ffa-4a51-11e9-8646-d663bd873d93','Manca luogo sinistro',13,TRUE);
INSERT INTO motivation_type (id, description, order_id, is_active)
VALUES('3364114e-4a51-11e9-8646-d663bd873d93','Vedi mail inviata',32,TRUE);
INSERT INTO motivation_type (id, description, order_id, is_active)
VALUES('33641298-4a51-11e9-8646-d663bd873d93','Manca Data, Ora, Luogo e Dinamica',14,TRUE);
INSERT INTO motivation_type (id, description, order_id, is_active)
VALUES('336413d8-4a51-11e9-8646-d663bd873d93','Manca targa, nome e ass. del 3° veicolo coinvolto',15,TRUE);
INSERT INTO motivation_type (id, description, order_id, is_active)
VALUES('33641522-4a51-11e9-8646-d663bd873d93','Manca Ass. di ctp o in mancanza dich. teste + doc.',10,TRUE);
INSERT INTO motivation_type (id, description, order_id, is_active)
VALUES('33641662-4a51-11e9-8646-d663bd873d93','Manca targa e ass di controparte e-o dati autorità',21,TRUE);
INSERT INTO motivation_type (id, description, order_id, is_active)
VALUES('33641aea-4a51-11e9-8646-d663bd873d93','Manca dinamica e dati autorità intervenute',22,TRUE);
INSERT INTO motivation_type (id, description, order_id, is_active)
VALUES('33641c66-4a51-11e9-8646-d663bd873d93','Manca data, luogo e targa controparte',23,TRUE);
INSERT INTO motivation_type (id, description, order_id, is_active)
VALUES('33641db0-4a51-11e9-8646-d663bd873d93','Manca dinamica e Ass. di controparte',24,TRUE);
INSERT INTO motivation_type (id, description, order_id, is_active)
VALUES('33641efa-4a51-11e9-8646-d663bd873d93','Manca dinamica e dati ctp e-o dati autorità',25,TRUE);
INSERT INTO motivation_type (id, description, order_id, is_active)
VALUES('33642044-4a51-11e9-8646-d663bd873d93','Manca scambio generalità rilasciato da autorità',26,TRUE);
INSERT INTO motivation_type (id, description, order_id, is_active)
VALUES('3364218e-4a51-11e9-8646-d663bd873d93','Dinamica e dati completi 3° veicolo coinvolto',9,TRUE);
INSERT INTO motivation_type (id, description, order_id, is_active)
VALUES('336424f4-4a51-11e9-8646-d663bd873d93','Manca descrizione del sinistro da Lei firmata',1,TRUE);
INSERT INTO motivation_type (id, description, order_id, is_active)
VALUES('33642670-4a51-11e9-8646-d663bd873d93','Manca firma sulla sua dichiarazione',2,TRUE);
INSERT INTO motivation_type (id, description, order_id, is_active)
VALUES('336427c4-4a51-11e9-8646-d663bd873d93','Documento allegato illeggibile',3,TRUE);
INSERT INTO motivation_type (id, description, order_id, is_active)
VALUES('336429f4-4a51-11e9-8646-d663bd873d93','L''allegato non si apre',4,TRUE);
INSERT INTO motivation_type (id, description, order_id, is_active)
VALUES('33642b66-4a51-11e9-8646-d663bd873d93','Dati identificativi della autorità intervenuta',27,TRUE);
INSERT INTO motivation_type (id, description, order_id, is_active)
VALUES('33642cb0-4a51-11e9-8646-d663bd873d93','Non abbiamo ricevuto denuncia originale',28,TRUE);
INSERT INTO motivation_type (id, description, order_id, is_active)
VALUES('3364312e-4a51-11e9-8646-d663bd873d93','Non abbiamo ricevuto una chiave',29,TRUE);
INSERT INTO motivation_type (id, description, order_id, is_active)
VALUES('336432b4-4a51-11e9-8646-d663bd873d93','Non abbiamo ricevuto nessuna chiave',30,TRUE);
INSERT INTO motivation_type (id, description, order_id, is_active)
VALUES('33643462-4a51-11e9-8646-d663bd873d93','Non abbiamo ricevuto ne chiavi ne denuncia origin.',31,TRUE);
INSERT INTO motivation_type (id, description, order_id, is_active)
VALUES('336436c4-4a51-11e9-8646-d663bd873d93','Vedi richiesta della compagnia',32,TRUE);
INSERT INTO motivation_type (id, description, order_id, is_active)
VALUES('3364387c-4a51-11e9-8646-d663bd873d93','Manca la vostra denuncia di sinistro',33,TRUE);
INSERT INTO motivation_type (id, description, order_id, is_active)
VALUES('336439d0-4a51-11e9-8646-d663bd873d93','La targa di controparte non corrisponde al modello',34,TRUE);
INSERT INTO notification_type (id, description, is_active, order_id)
VALUES ('f7713c0e-4a53-11e9-8646-d663bd873d93','Denuncia Sinistro',TRUE,1);
INSERT INTO notification_type (id, description, is_active, order_id)
VALUES ('f7713f06-4a53-11e9-8646-d663bd873d93','Denuncia Sinistro Web',TRUE,2);
INSERT INTO notification_type (id, description, is_active, order_id)
VALUES ('f7714082-4a53-11e9-8646-d663bd873d93','Richiesta della Compagnia',TRUE,3);
INSERT INTO notification_type (id, description, is_active, order_id)
VALUES ('f77142c6-4a53-11e9-8646-d663bd873d93','Richiesta danni da controparte',TRUE,4);
INSERT INTO notification_type (id, description, is_active, order_id)
VALUES ('f7714460-4a53-11e9-8646-d663bd873d93','Citazione',TRUE,5);
INSERT INTO notification_type (id, description, is_active, order_id)
VALUES ('f77145d2-4a53-11e9-8646-d663bd873d93','Dichiarazione di irreparabilità del mezzo',TRUE,6);
INSERT INTO notification_type (id, description, is_active, order_id)
VALUES ('f7714a0a-4a53-11e9-8646-d663bd873d93','Offerta reale',TRUE,7);
INSERT INTO notification_type (id, description, is_active, order_id)
VALUES ('f7714b86-4a53-11e9-8646-d663bd873d93','Altro',TRUE,8);
INSERT INTO notification_type (id, description, is_active, order_id)
VALUES ('f7714cf8-4a53-11e9-8646-d663bd873d93','Denuncia Autorità',TRUE,9);
INSERT INTO notification_type (id, description, is_active, order_id)
VALUES ('f7714e56-4a53-11e9-8646-d663bd873d93','Soccorso stradale',TRUE,10);
INSERT INTO notification_type (id, description, is_active, order_id)
VALUES ('f7714fa0-4a53-11e9-8646-d663bd873d93','WINITY',TRUE,11);
INSERT INTO notification_type (id, description, is_active, order_id)
VALUES ('f77150f4-4a53-11e9-8646-d663bd873d93','Riparazione',TRUE,12);
INSERT INTO notification_type (id, description, is_active, order_id)
VALUES ('f7715536-4a53-11e9-8646-d663bd873d93','Perizia da stato d uso',TRUE,13);
INSERT INTO notification_type (id, description, is_active, order_id)
VALUES ('f77156bc-4a53-11e9-8646-d663bd873d93','Richiesta danni (DEKRA)',TRUE,14);
INSERT INTO notification_type (id, description, is_active, order_id)
VALUES ('f771581a-4a53-11e9-8646-d663bd873d93','Denuncia Sinistro + Citazione',TRUE,15);
INSERT INTO notification_type (id, description, is_active, order_id)
VALUES ('f77159b4-4a53-11e9-8646-d663bd873d93','Denuncia Sinistro Web + Citazione',TRUE,16);
INSERT INTO notification_type (id, description, is_active, order_id)
VALUES ('f7715b3a-4a53-11e9-8646-d663bd873d93','Richiesta danni da controparte + Citazione',TRUE,17);
INSERT INTO notification_type (id, description, is_active, order_id)
VALUES ('f7715edc-4a53-11e9-8646-d663bd873d93','Soccorso stradale + Citazione',TRUE,18);
INSERT INTO notification_type (id, description, is_active, order_id)
VALUES ('f7716062-4a53-11e9-8646-d663bd873d93','Richiesta danni (DEKRA) + Citazione',TRUE,19);
INSERT INTO notification_type (id, description, is_active, order_id)
VALUES ('f77161b6-4a53-11e9-8646-d663bd873d93','Richiesta danni (MSA)',TRUE,20);
INSERT INTO notification_type (id, description, is_active, order_id)
VALUES ('f771630a-4a53-11e9-8646-d663bd873d93','Richiesta danni (MSA) + Citazione',TRUE,21);
INSERT INTO notification_type (id, description, is_active, order_id)
VALUES ('f771645e-4a53-11e9-8646-d663bd873d93','Comunicazione telefonica',TRUE,22);
INSERT INTO notification_type (id, description, is_active, order_id)
VALUES ('f771688c-4a53-11e9-8646-d663bd873d93','Apertura da MSA',TRUE,23);
INSERT INTO notification_type (id, description, is_active, order_id)
VALUES ('f7716a3a-4a53-11e9-8646-d663bd873d93','Appropriazione indebita',TRUE,24);
INSERT INTO notification_type (id, description, is_active, order_id)
VALUES ('f7716b8e-4a53-11e9-8646-d663bd873d93','Furto per rapina',TRUE,25);
INSERT INTO notification_type (id, description, is_active, order_id)
VALUES ('f7716ce2-4a53-11e9-8646-d663bd873d93','Furto',TRUE,26);
INSERT INTO notification_type (id, description, is_active, order_id)
VALUES ('f7716e2c-4a53-11e9-8646-d663bd873d93','Appropriazione indebita subnoleggio',TRUE,27);
INSERT INTO inspectorate (id,name,email,address,zip_code,city,province,state,vat_number,fiscal_code,telephone,fax,web_site,reference_person,attorney,external_code,is_active)
VALUES('1db43884-4a5a-11e9-8646-d663bd873d93','GENERALI ISPETT CARD','ALD@generali.com',null,null,null,null,null,null,null,null,null,null,null,null,null, true);

INSERT INTO inspectorate (id,name,email,address,zip_code,city,province,state,vat_number,fiscal_code,telephone,fax,web_site,reference_person,attorney,external_code,is_active)
VALUES('1db43bc2-4a5a-11e9-8646-d663bd873d93','ZURICH ISPETT CARD','ald@studiopavin.it; ita_claimszurich@willis.com',null,null,null,null,null,null,null,null,null,null,null,null,null, true);

INSERT INTO inspectorate (id,name,email,address,zip_code,city,province,state,vat_number,fiscal_code,telephone,fax,web_site,reference_person,attorney,external_code,is_active)
VALUES('1db43ff0-4a5a-11e9-8646-d663bd873d93','UNIPOL ISPETT CARD','ALD_Claims.unipolsai@aon.it',null,null,null,null,null,null,null,null,null,null,null,null,null, true);

INSERT INTO inspectorate (id,name,email,address,zip_code,city,province,state,vat_number,fiscal_code,telephone,fax,web_site,reference_person,attorney,external_code,is_active)
VALUES('1db44180-4a5a-11e9-8646-d663bd873d93','WILLIS GENERALI (seguiti)','ald.flotte@willis.com',null,null,null,null,null,null,null,null,null,null,null,null,null, true);

INSERT INTO inspectorate (id,name,email,address,zip_code,city,province,state,vat_number,fiscal_code,telephone,fax,web_site,reference_person,attorney,external_code,is_active)
VALUES('1db443ec-4a5a-11e9-8646-d663bd873d93','SGS - Ispettorato Dekra','mariaantonietta.giannuzzi@dekra.com',null,null,null,null,null,null,null,null,null,null,null,null,null, true);


INSERT INTO automatic_affiliation_rule_inspectorate (id, selection_name, monthly_volume, current_month, current_volume, detail, annotations, select_only_complaints_without_ctp, foreign_country_claim, counterpart_type, claim_with_injured, damage_can_not_be_repaired, inspectorate_id, min_import, max_import)
VALUES ('e34ab356-4a65-11e9-8646-d663bd873d93', 'card attive e concorsuali generali italia', 1000, '03/2019', 79, null, null, null, null, null, null, null, '1db43884-4a5a-11e9-8646-d663bd873d93', 200, 250);
INSERT INTO automatic_affiliation_rule_inspectorate (id, selection_name, monthly_volume, current_month, current_volume, detail, annotations, select_only_complaints_without_ctp, foreign_country_claim, counterpart_type, claim_with_injured, damage_can_not_be_repaired, inspectorate_id, min_import, max_import)
VALUES ('e34ab784-4a65-11e9-8646-d663bd873d93', 'card attivi e concorsuali zurich', 100, '03/2019', 0, null, null, null, null, null, null, null, '1db43bc2-4a5a-11e9-8646-d663bd873d93', 200, 250);
INSERT INTO automatic_affiliation_rule_inspectorate (id, selection_name, monthly_volume, current_month, current_volume, detail, annotations, select_only_complaints_without_ctp, foreign_country_claim, counterpart_type, claim_with_injured, damage_can_not_be_repaired, inspectorate_id, min_import, max_import)
VALUES ('e34aba18-4a65-11e9-8646-d663bd873d93', 'affido card attive e concorsuali Unipolsai', 1000, '03/2019', 38, null, null, null, null, null, null, null, '1db43ff0-4a5a-11e9-8646-d663bd873d93', 200, 250);
INSERT INTO automatic_affiliation_rule_inspectorate_to_claims_type (automatic_affiliation_rule_inspectorate_id, claims_type_id)
VALUES ('e34ab356-4a65-11e9-8646-d663bd873d93','7a07504c-4ae8-11e9-8646-d663bd873d93');
INSERT INTO automatic_affiliation_rule_inspectorate_to_claims_type (automatic_affiliation_rule_inspectorate_id, claims_type_id)
VALUES ('e34ab356-4a65-11e9-8646-d663bd873d93','7a07565a-4ae8-11e9-8646-d663bd873d93');
INSERT INTO automatic_affiliation_rule_inspectorate_to_claims_type (automatic_affiliation_rule_inspectorate_id, claims_type_id)
VALUES ('e34ab356-4a65-11e9-8646-d663bd873d93','7a075bc8-4ae8-11e9-8646-d663bd873d93');
INSERT INTO automatic_affiliation_rule_inspectorate_to_claims_type (automatic_affiliation_rule_inspectorate_id, claims_type_id)
VALUES ('e34ab356-4a65-11e9-8646-d663bd873d93','7a0761a4-4ae8-11e9-8646-d663bd873d93');

INSERT INTO automatic_affiliation_rule_inspectorate_to_claims_type (automatic_affiliation_rule_inspectorate_id, claims_type_id)
VALUES ('e34ab784-4a65-11e9-8646-d663bd873d93','7a07504c-4ae8-11e9-8646-d663bd873d93');
INSERT INTO automatic_affiliation_rule_inspectorate_to_claims_type (automatic_affiliation_rule_inspectorate_id, claims_type_id)
VALUES ('e34ab784-4a65-11e9-8646-d663bd873d93','7a07565a-4ae8-11e9-8646-d663bd873d93');
INSERT INTO automatic_affiliation_rule_inspectorate_to_claims_type (automatic_affiliation_rule_inspectorate_id, claims_type_id)
VALUES ('e34ab784-4a65-11e9-8646-d663bd873d93','7a075bc8-4ae8-11e9-8646-d663bd873d93');
INSERT INTO automatic_affiliation_rule_inspectorate_to_claims_type (automatic_affiliation_rule_inspectorate_id, claims_type_id)
VALUES ('e34ab784-4a65-11e9-8646-d663bd873d93','7a0761a4-4ae8-11e9-8646-d663bd873d93');


INSERT INTO automatic_affiliation_rule_inspectorate_to_claims_type (automatic_affiliation_rule_inspectorate_id, claims_type_id)
VALUES ('e34aba18-4a65-11e9-8646-d663bd873d93','7a07504c-4ae8-11e9-8646-d663bd873d93');
INSERT INTO automatic_affiliation_rule_inspectorate_to_claims_type (automatic_affiliation_rule_inspectorate_id, claims_type_id)
VALUES ('e34aba18-4a65-11e9-8646-d663bd873d93','7a07565a-4ae8-11e9-8646-d663bd873d93');
INSERT INTO automatic_affiliation_rule_inspectorate_to_claims_type (automatic_affiliation_rule_inspectorate_id, claims_type_id)
VALUES ('e34aba18-4a65-11e9-8646-d663bd873d93','7a075bc8-4ae8-11e9-8646-d663bd873d93');
INSERT INTO automatic_affiliation_rule_inspectorate_to_claims_type (automatic_affiliation_rule_inspectorate_id, claims_type_id)
VALUES ('e34aba18-4a65-11e9-8646-d663bd873d93','7a0761a4-4ae8-11e9-8646-d663bd873d93');
INSERT INTO automatic_affiliation_rule_inspectorate_to_damaged_insurance (automatic_affiliation_rule_inspectorate_id, insurance_company_id)
VALUES ('e34ab784-4a65-11e9-8646-d663bd873d93','a83be648-4a58-11e9-8646-d663bd873d93');
INSERT INTO automatic_affiliation_rule_inspectorate_to_damaged_insurance (automatic_affiliation_rule_inspectorate_id, insurance_company_id)
VALUES ('e34ab356-4a65-11e9-8646-d663bd873d93','a83bdfb8-4a58-11e9-8646-d663bd873d93');
INSERT INTO automatic_affiliation_rule_inspectorate_to_damaged_insurance (automatic_affiliation_rule_inspectorate_id, insurance_company_id)
VALUES ('e34aba18-4a65-11e9-8646-d663bd873d93','a83be2a6-4a58-11e9-8646-d663bd873d93');
INSERT INTO legal (id,name,email,address,zip_code,city,province,state,vat_number,fiscal_code,telephone,fax,web_site,reference_person,external_code,is_active)
VALUES('28445608-4a5f-11e9-8646-d663bd873d93','Multiserass','aldsgi@multiserass.com',null,null,null,null,null,null,null,null,null,null,null,null, true);

INSERT INTO legal (id,name,email,address,zip_code,city,province,state,vat_number,fiscal_code,telephone,fax,web_site,reference_person,external_code,is_active)
VALUES('abefd042-4a5d-11e9-8646-d663bd873d93','Studio Celletti','incarichi.ald@studiolegalecelletti.191.it',null,null,null,null,null,null,null,null,null,null,null,null,true);

INSERT INTO legal (id,name,email,address,zip_code,city,province,state,vat_number,fiscal_code,telephone,fax,web_site,reference_person,external_code,is_active)
VALUES('abefd4fc-4a5d-11e9-8646-d663bd873d93','GBS (GENERALI)','flotte@generaligroup.com',null,null,null,null,null,null,null,null,null,null,null,null,true);

INSERT INTO legal (id,name,email,address,zip_code,city,province,state,vat_number,fiscal_code,telephone,fax,web_site,reference_person,external_code,is_active)
VALUES('abefd68c-4a5d-11e9-8646-d663bd873d93','WILLCONSULTING','Sinistriflotte_willco@willis.com',null,null,null,null,null,null,null,null,null,null,null,null,true);

INSERT INTO legal (id,name,email,address,zip_code,city,province,state,vat_number,fiscal_code,telephone,fax,web_site,reference_person,external_code,is_active)
VALUES('abefd808-4a5d-11e9-8646-d663bd873d93','WILLCO ZURICH','ita_claimszurich@willis.com',null,null,null,null,null,null,null,null,null,null,null,null, true);
INSERT INTO automatic_affiliation_legal_rule (id, selection_name, monthly_volume, current_month, current_volume, detail, annotations, complaints_without_ctp, foreign_country_claim, counterpart_type, claim_with_injured, damage_can_not_be_repaired, legal_id, min_import, max_import)
VALUES ('7d48fb18-4aeb-11e9-8646-d663bd873d93', 'rc attivi e concorsuali ald vs ald clienti diversi sg vs sg', 100, '03/2019', 7, null, null, null, null, null, null, null, '28445608-4a5f-11e9-8646-d663bd873d93', 20, 40);
INSERT INTO automatic_affiliation_rule_legal_to_claims_type (automatic_affiliation_rule_legal_id, claims_type_id)
VALUES ('7d48fb18-4aeb-11e9-8646-d663bd873d93','7a074aa2-4ae8-11e9-8646-d663bd873d93');
INSERT INTO automatic_affiliation_rule_legal_to_claims_type (automatic_affiliation_rule_legal_id, claims_type_id)
VALUES ('7d48fb18-4aeb-11e9-8646-d663bd873d93','7a074d9a-4ae8-11e9-8646-d663bd873d93');
INSERT INTO automatic_affiliation_rule_legal_to_counterpart_company (automatic_affiliation_rule_legal_id, counterpart_company_id)
VALUES ('7d48fb18-4aeb-11e9-8646-d663bd873d93','a83be7e2-4a58-11e9-8646-d663bd873d93');
INSERT INTO automatic_affiliation_rule_legal_to_damaged_company (automatic_affiliation_rule_legal_id, damaged_company_id)
VALUES ('7d48fb18-4aeb-11e9-8646-d663bd873d93','a83be7e2-4a58-11e9-8646-d663bd873d93');

