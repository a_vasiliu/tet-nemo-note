ALTER TABLE email_template ADD COLUMN IF NOT EXISTS accident_type_list jsonb NULL;

UPDATE email_template
SET accident_type_list = '["FURTO_TOTALE"]'
WHERE type = 'THEFT';


