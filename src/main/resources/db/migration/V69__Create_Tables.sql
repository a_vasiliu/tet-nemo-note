CREATE TABLE IF NOT EXISTS public.anti_theft_request(
  id varchar(255) PRIMARY KEY,
  created_at timestamptz,
  updated_at timestamptz,
  practice_id varchar(255),
  claim_id varchar(255),
  id_transaction varchar(255),
  provider_type varchar(255),
  response_type varchar(255),
  outcome varchar(255),
  sub_report numeric,
  g_power numeric,
  crash_number numeric,
  anomaly bool,
  pdf varchar(255),
  response_message varchar(255),
  CONSTRAINT antithef_fkey FOREIGN KEY (claim_id) REFERENCES public.claims (id)
);




