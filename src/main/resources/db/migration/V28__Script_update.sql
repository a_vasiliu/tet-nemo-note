ALTER TABLE claims ADD COLUMN IF NOT EXISTS is_authority_linkable boolean;
ALTER TABLE generic_attachment ADD COLUMN IF NOT EXISTS type_complaint varchar (255);
UPDATE generic_attachment
SET type_complaint = 'REPAIR';