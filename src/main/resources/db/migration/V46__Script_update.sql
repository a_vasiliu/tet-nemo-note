DROP TABLE IF EXISTS appropriation;
DROP TABLE IF EXISTS practice_car;
DROP TABLE IF EXISTS attachment;
ALTER TABLE claims DROP COLUMN IF EXISTS appropriation_practice_car;
ALTER TABLE insurance_policy_app DROP COLUMN IF EXISTS insurance_policy_id;
ALTER TABLE insurance_policy_app ADD COLUMN IF NOT EXISTS insurance_policy_id bigint;