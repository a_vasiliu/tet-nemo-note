ALTER TABLE claims ADD COLUMN IF NOT EXISTS is_read_acclaims bool;
ALTER TABLE claims RENAME COLUMN is_read TO is_read_msa;

INSERT INTO public.insurance_manager(
            id, name, address, zip_code, city, province, state,
            telephone, email, is_active, rif_code)
    VALUES ('cec2e5fb-6188-4920-8e0d-f78d17a7bc31', 'MSA ACCLAIMS', 'Via Roncaglia 13', '20146', 'Milano', 'MI', 'Italia',
            '024654741', 'rivalse@acclaims.it', true, 45);

INSERT INTO public.insurance_company(
            id, name, address, zip_code, city, province, state, telephone,
             email, is_active, code)
    VALUES ('6771d2fd-81aa-4738-b2c8-dc438d0fe29b', 'DAS', 'Via Enrico Fermi 9/B - 37135 Verona', '37135', 'Verona', 'VR',
    'Italia', '0458372611',
           'info@das.it', true, 318);