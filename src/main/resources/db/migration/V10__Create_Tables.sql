CREATE TABLE IF NOT EXISTS repairer(
  id UUID PRIMARY KEY,
  supplier_code varchar (255),
  business_name varchar (255),
  address varchar (255),
  zip_code numeric,
  locality varchar (255),
  prov varchar (255),
  reference_person varchar (255),
  phone varchar (255),
  fax varchar (255),
  email varchar (255),
  web_site varchar (255),
  vat_number varchar (255),
  fiscal_code varchar (255),
  legal_representative varchar (255)
);

CREATE SEQUENCE IF NOT EXISTS appropriation_sequence;
CREATE TABLE IF NOT EXISTS appropriation (
  id varchar (255) PRIMARY KEY,
  practice_id int8 DEFAULT nextval('appropriation_sequence') NOT NULL,
  type varchar (255) NULL,
  status varchar (255) NULL,
  following varchar (255) NULL,
  id_claims varchar (255) NULL,
  plate varchar (255) NULL,
  ownership varchar (255) NULL,
  notified timestamptz NULL,
  notified_by varchar (255) NULL,
  happened_at timestamptz NULL,
  authority varchar (255) NULL,
  telephone_one varchar (255) NULL,
  location_and_note varchar (255) NULL,
  request_one timestamptz NULL,
  request_two timestamptz NULL,
  effect_one timestamptz NULL,
  effect_two timestamptz NULL,
  delegate varchar (255) NULL,
  email varchar (255) NULL,
  telephone_two varchar (255) NULL,
  request_loss_possession varchar (255) NULL,
  loss_possession varchar (255) NULL,
  request_return_possession varchar (255) NULL,
  return_possession varchar (255) NULL,
  release_notified timestamptz NULL,
  release_notified_by varchar (255) NULL,
  release_happened_at timestamptz NULL,
  re_registration bool NULL,
  re_registration_request timestamptz NULL,
  re_registration_at timestamptz NULL,
  re_registration_salesforce_case varchar (255) NULL,
  certificate_duplication bool NULL,
  certificate_duplication_request timestamptz NULL,
  certificate_duplication_at timestamptz NULL,
  certificate_duplication_salesforce_case varchar (255) NULL,
  stamp bool NULL,
  stamp_request timestamptz NULL,
  stamp_at timestamptz NULL,
  stamp_salesforce_case varchar (255) NULL,
  demolition bool NULL,
  demolition_request timestamptz NULL,
  demolition_at timestamptz NULL,
  demolition_salesforce_case varchar (255) NULL,
  created_at timestamptz NULL,
  update_at timestamptz NULL,
  contract jsonb NULL,
  forms jsonb NULL,
	historical jsonb NULL,
	motivation varchar (255) NULL
  );
CREATE INDEX IF NOT EXISTS idx_appropriation_sequence ON public.appropriation USING btree(practice_id);

CREATE SEQUENCE IF NOT EXISTS practice_car_sequence;
CREATE TABLE IF NOT EXISTS practice_car (
  id varchar (255) PRIMARY KEY,
  practice_id int8 DEFAULT nextval('practice_car_sequence') NOT NULL,
  type varchar (255) NULL,
  practice_class varchar (255) NULL,
  status varchar (255) NULL,
  following varchar (255) NULL,
  id_claims varchar (255) NULL,
  plate varchar (255) NULL,
  ownership varchar (255) NULL,
  notified timestamptz NULL,
  notified_by varchar (255) NULL,
  happened_at timestamptz NULL,
  authority varchar (255) NULL,
  telephone_one varchar (255) NULL,
  location_and_note varchar (255) NULL,
  request_one timestamptz NULL,
  request_two timestamptz NULL,
  effect_one timestamptz NULL,
  effect_two timestamptz NULL,
  delegate varchar (255) NULL,
  email varchar (255) NULL,
  telephone_two varchar (255) NULL,
  request_loss_possession varchar (255) NULL,
  loss_possession varchar (255) NULL,
  request_return_possession varchar (255) NULL,
  return_possession varchar (255) NULL,
  release_notified timestamptz NULL,
  release_notified_by varchar (255) NULL,
  release_happened_at timestamptz NULL,
  re_registration bool NULL,
  re_registration_request timestamptz NULL,
  re_registration_at timestamptz NULL,
  re_registration_salesforce_case varchar (255) NULL,
  certificate_duplication bool NULL,
  certificate_duplication_request timestamptz NULL,
  certificate_duplication_at timestamptz NULL,
  certificate_duplication_salesforce_case varchar (255) NULL,
  stamp bool NULL,
  stamp_request timestamptz NULL,
  stamp_at timestamptz NULL,
  stamp_salesforce_case varchar (255) NULL,
  demolition bool NULL,
  demolition_request timestamptz NULL,
  demolition_at timestamptz NULL,
  demolition_salesforce_case varchar (255) NULL,
  created_at timestamptz NULL,
  update_at timestamptz NULL,
  contract jsonb NULL,
  forms jsonb NULL,
	historical jsonb NULL,
	motivation varchar (255) NULL
  );
CREATE INDEX IF NOT EXISTS idx_practice_car_sequence ON public.practice_car USING btree(practice_id);

ALTER TABLE claims add COLUMN IF NOT EXISTS appropriation_practice_car jsonb;

CREATE TABLE IF NOT EXISTS dm_systems(
  id UUID PRIMARY KEY,
	system_name varchar (255)
);

CREATE TABLE IF NOT EXISTS export_claims_type(
  id UUID PRIMARY KEY,
	claims_type varchar(255),
	dm_system_id UUID,
	code varchar (255),
	description varchar (255),
	FOREIGN KEY (dm_system_id) REFERENCES dm_systems (id)
);

CREATE TABLE IF NOT EXISTS export_notification_type(
  id UUID PRIMARY KEY,
	notification_type_id UUID,
	dm_system_id UUID,
	code varchar (255),
	description varchar (255),
	FOREIGN KEY (dm_system_id) REFERENCES dm_systems (id),
	FOREIGN KEY (notification_type_id) REFERENCES notification_type (id)
);

CREATE TABLE IF NOT EXISTS export_status_type(
  id UUID PRIMARY KEY,
	status_type varchar(255),
	flow_type varchar(255),
	dm_system_id UUID,
	code varchar (255),
	description varchar (255),
	FOREIGN KEY (dm_system_id) REFERENCES dm_systems (id)
);

CREATE TABLE IF NOT EXISTS supplier_code(
  id UUID PRIMARY KEY,
  vat_number varchar(255),
	denomination varchar(255),
	code_to_export varchar (255),
	CONSTRAINT uc_exportsuppliercode UNIQUE (vat_number)
);

