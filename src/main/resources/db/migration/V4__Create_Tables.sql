ALTER TABLE anti_theft_service ADD COLUMN is_active boolean;
ALTER TABLE broker ADD COLUMN is_active boolean;
ALTER TABLE inspectorate ADD COLUMN is_active boolean;
ALTER TABLE motivation ADD COLUMN is_active boolean;
ALTER TABLE personal_data ADD COLUMN is_active boolean;
ALTER TABLE recoverability ADD COLUMN is_active boolean;
ALTER TABLE top_customer_guarantee ADD COLUMN is_active boolean;
ALTER TABLE attachment_type ADD COLUMN is_active boolean;
/*ALTER TABLE contract_type ADD COLUMN is_active boolean;*/
ALTER TABLE notification_type ADD COLUMN is_active boolean;
ALTER TABLE insurance_company ADD COLUMN is_active boolean;
ALTER TABLE insurance_manager ADD COLUMN is_active boolean;
ALTER TABLE legal ADD COLUMN is_active boolean;
ALTER TABLE insurance_policy ADD COLUMN is_active boolean;
ALTER TABLE motivation_type ADD COLUMN is_active boolean;
ALTER TABLE configure_evidence ADD COLUMN is_active boolean;
ALTER TABLE email_template ADD COLUMN is_active boolean;
ALTER TABLE event_type ADD COLUMN is_active boolean;
ALTER TABLE recipient_type ADD COLUMN is_active boolean;
ALTER TABLE notification_type ADD COLUMN order_id numeric;
ALTER TABLE anti_theft_service ADD COLUMN type varchar(255);
ALTER TABLE automatic_affiliation_rule_inspectorate ADD COLUMN min_import NUMERIC ;
ALTER TABLE automatic_affiliation_rule_inspectorate ADD COLUMN max_import NUMERIC ;
ALTER TABLE automatic_affiliation_legal_rule ADD COLUMN min_import numeric;
ALTER TABLE automatic_affiliation_legal_rule ADD COLUMN max_import numeric;
alter table personal_data drop  column top_customer_id;
DROP TABLE top_customer_guarantee;
ALTER TABLE contract_type DROP COLUMN loss_deductible_keys;
ALTER TABLE contract_type DROP COLUMN events_deductible;
ALTER TABLE contract_type DROP COLUMN ownership;

