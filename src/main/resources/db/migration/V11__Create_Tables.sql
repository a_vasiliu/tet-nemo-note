ALTER TABLE event drop COLUMN IF EXISTS user_id;
ALTER TABLE event add COLUMN IF NOT EXISTS user_id varchar (255);
ALTER TABLE export_claims_type ADD COLUMN IF NOT EXISTS is_active boolean;
ALTER TABLE export_notification_type ADD COLUMN IF NOT EXISTS is_active boolean;
ALTER TABLE export_status_type ADD COLUMN IF NOT EXISTS is_active boolean;
ALTER TABLE supplier_code ADD COLUMN IF NOT EXISTS is_active boolean;
ALTER TABLE manager ADD COLUMN IF NOT EXISTS is_active boolean;
ALTER TABLE repairer ADD COLUMN IF NOT EXISTS is_active boolean;
ALTER TABLE contract_type ADD COLUMN IF NOT EXISTS is_active boolean;
ALTER TABLE claims add COLUMN IF NOT EXISTS exemption jsonb NULL;

CREATE TABLE IF NOT EXISTS data_management_export (
  id UUID PRIMARY KEY,
  "date" timestamptz NOT NULL,
  user_import varchar(255) NOT NULL,
  description varchar(255) NOT NULL,
  "result" varchar(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS exporting_file (
  id UUID PRIMARY KEY,
  data_management_export_id UUID,
  attachment_id UUID NOT NULL,
  file_name varchar(255) NOT NULL,
  file_size numeric NOT NULL,
  FOREIGN KEY (data_management_export_id) REFERENCES data_management_export (id)
);

CREATE TABLE IF NOT EXISTS data_management_import (
id UUID PRIMARY KEY,
"date" timestamptz NOT NULL,
user_import varchar(255) NOT NULL,
description varchar(255) NOT NULL,
file_type varchar(255) NOT NULL,
"result" varchar(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS processing_file (
id UUID PRIMARY KEY,
data_management_import_id UUID,
attachment_id UUID NOT NULL,
file_name varchar(255) NOT NULL,
file_size numeric NOT NULL,
FOREIGN KEY (data_management_import_id) REFERENCES data_management_import (id)

);

ALTER TABLE claims ADD COLUMN IF NOT EXISTS theft jsonb;

CREATE TABLE IF NOT EXISTS supplier (
	id UUID PRIMARY KEY,
	cod_supplier varchar(255) NULL,
	name varchar(255) NULL,
	email varchar (255),
	is_active bool,
	CONSTRAINT uc_supplier UNIQUE (cod_supplier)
);
