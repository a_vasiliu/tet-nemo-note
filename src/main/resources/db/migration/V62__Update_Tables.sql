DELETE FROM insurance_manager WHERE id= '7a667d33-94ea-4795-9364-ed132467fe8d';

UPDATE insurance_manager
SET name = 'MSA ACCLAIMS (gestione LC)'
WHERE id = 'cec2e5fb-6188-4920-8e0d-f78d17a7bc31';

UPDATE insurance_manager
SET name = 'Multiserass PAI',
 email ='Pai.sgi@multiserass.com',
 web_site ='sinistri@pec.multiserass.com',
 address ='Via Sangro,15',
 zip_code ='20123',
 city ='Milano',
 province='MI',
 telephone ='02 465474.1',
 reference_person ='Onofrio MADONIA (liquidatore)info su gestione; Valeria FARINA (amministrativa) info generiche'
WHERE id = '8e978f22-6864-42ad-b591-50e4a705ebe3';


insert into attachment_type(id, name, groups, is_active, type_complaint)
values ('5f97eb81-3143-4304-b104-4a0f1e705368', 'Richiesta Danni da CTP', 'RGCA', true, 'CLAIMS');


insert into insurance_company(id, name, address, zip_code, city, province, state, telephone, fax, email, web_site, contact, ania_code, join_card, contact_pai, email_pai, is_active, code)
values
('0cd05060-cd67-4957-83bc-940c58b05658', 'GREENVAL INSURANCE DAC', null, null, null, null, null, null, null, null, null, null, null, null, null, null, true, 319),
('6085153a-3198-4d5e-bab4-7eb5e05e5f1d', 'BENE ASSICURAZIONI S.p.A.', 'Via dei Valtorta, 48', '20', 'MILANO', 'MI', 'Italia', null, null, null, null, null, 486, true, null, null, true, 320);

update insurance_company set name = 'DAS DIFESA LEGALE', address = 'Via Enrico Fermi 9/B - 37135 Verona', zip_code = '35', city = 'VERONA', province = 'VR', state = 'Italia', telephone = '458372611', email = 'info@das.it'
where id = '6771d2fd-81aa-4738-b2c8-dc438d0fe29b';


insert into inspectorate(id, name, email, zip_code, city, province, state, vat_number, fiscal_code, telephone, fax, web_site, reference_person, attorney, external_code, is_active, address, code)
values ('e87e1b95-631a-4ac9-a2ab-0834a8535f95', 'CATTOLICA ISPETT CARD', 'ALD_CLAIMS.CATTOLICA@aon.it', null, null, null, null, null, null, null, null, null, null, null, null, true, null, 13);


INSERT INTO public.generic_attachment(
            id, created_at, original_name, description, attachment_id, is_active,
            type_complaint)
    VALUES ('5198af2c-fbf1-4dd9-9d36-86dd9de04269', now(), 'Manuale-Nemo-Repair.pdf', 'Cessione del credito VUOTA', '799b832a-1bd7-4f41-bc2a-3a4340dc3c23', true, 'REPAIR');


INSERT INTO public.generic_attachment(
            id, created_at, original_name, description, attachment_id, is_active,
            type_complaint)
    VALUES ('dab209dc-12e0-4ca9-a1d4-16c752822b0d', now(), 'cessione-SDS.pdf', 'Cessione del credito VUOTA', 'a61337e5-4ff2-4810-812c-b89a7cc35548', true, 'REPAIR');

INSERT INTO public.generic_attachment(
            id, created_at, original_name, description, attachment_id, is_active,
            type_complaint)
    VALUES ('b63018e7-cc83-426c-bec7-35faeff686b2', now(), 'Cess-carr-FIRME.pdf','Cessione del credito VUOTA', '799b832a-1bd7-4f41-bc2a-3a4340dc3c23', true,
            'REPAIR');

INSERT INTO public.generic_attachment(
id, created_at, original_name, description, attachment_id, is_active,
type_complaint)
VALUES ('56fa30c4-2c75-44fb-835d-0c3ffad1ab15', now(), 'Dichiarazione-avvocato.pdf','Dichiarazione avvocato 1', 'd01e0676-2f1f-4251-a6a2-a595e340aa74', true,
'REPAIR');

INSERT INTO public.generic_attachment(
            id, created_at, original_name, description, attachment_id, is_active,
            type_complaint)
    VALUES ('459ab7ab-7c4c-49f8-b23a-c33a341edc9a', now(),'tutela-legale-plus.pdf', 'Tutela legale plus', '9732d007-aa29-4afd-a164-acfee21b9b90', true,
            'CLAIMS');

INSERT INTO public.generic_attachment(
            id, created_at, original_name, description, attachment_id, is_active,
            type_complaint)
    VALUES ('3ef3b895-37f7-429d-8999-a60b9b3f4cf5', NOW(), 'tutela-legale-base.pdf','Tutela legale base', '427ea502-3b02-41c1-8495-c88e27e449a9', true,
            'CLAIMS');