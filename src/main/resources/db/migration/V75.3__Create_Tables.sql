CREATE TABLE IF NOT EXISTS public.claims_pending (
    practice_id int8 PRIMARY KEY,
    damaged_drivers_plate varchar(255) NOT NULL,
    date_accident timestamptz NOT NULL,
    type_accident varchar(255) NOT NULL,
    counterparty_insured_name varchar(255) NOT NULL,
    counterparty_insured_surname varchar(255) NOT NULL,
    counterparty_plate varchar(255) NOT NULL
    );
