UPDATE email_template
set body = 'a seguito della <%NOTTYPDENOTY%> ricevuta il <%REICDATECOMP%> La informiamo che non è possibile procedere alla gestione assicurativa del sinistro,
i cui riferimenti in oggetto, a causa dei seguenti motivi:<br/><%REATYPDESTYP%> <%REASDESCCOMP%>.<br/>
La preghiamo quindi di farci pervenire le informazioni mancanti entro e non oltre 15 (quindici) giorni
dalla presente utilizzando il seguente URL (copiandolo nella barra del browser):<%REPOSPUBLINK_MYALD%>
per allegare la documentazione.<br/>In alternativa potrà inviare l''''allegato tramite mail all’indirizzo gestioneclaims@aldautomotive.com o tramite raccomandata (allegando la presente) da inviare a: ALD Automotive Italia Srl
 - Viale Luca Gaurico, 187 - 00143 Roma - c.a. Ufficio Sinistri.<br/>Le ricordiamo che ai sensi dell’Art. 143 del D.L. 209 del 7 Settembre 2005, la denuncia di sinistro deve essere completa di tutti
 gli elementi necessari ad una corretta istruzione della pratica (data, luogo, modalità, generalità di tutte le persone coinvolte, compresi eventuali feriti, testimoni e autorità intervenute).
 <br/>Le rammentiamo inoltre che, a norma dell’art. 2 lett. i delle Condizioni Generali di Contratto, le denunce di sinistro devono essere inviate tramite raccomandata A.R. o inserite sul nostro sito online entro 3
 (tre) giorni dalla data di accadimento.<br/>Certi della Sua cortese collaborazione, restiamo a disposizione per qualsiasi informazione o chiarimento.<br/>Cordiali saluti.'
WHERE id='28d143a9-6317-4b85-9046-6d529c6d838c';


UPDATE email_template
set body = 'a seguito della <%NOTTYPDENOTY%> ricevuta il <%REICDATECOMP%>
La informiamo che non è possibile procedere alla gestione assicurativa del sinistro,
i cui riferimenti in oggetto, a causa dei seguenti motivi:<br/><%REATYPDESTYP%> <%REASDESCCOMP%>.
<br/>La preghiamo quindi di farci pervenire le informazioni mancanti entro e non oltre 15 (quindici)
giorni dalla presente utilizzando il seguente URL (copiandolo nella barra del browser):<%REPOSPUBLINK_MYALD%>
per allegare la documentazione.<br/>In alternativa potrà inviare l''''allegato tramite mail all’indirizzo
gestioneclaims@aldautomotive.com o tramite raccomandata (allegando la presente) da inviare a:
ALD Automotive Italia Srl - Viale Luca Gaurico, 187 - 00143 Roma - c.a. Ufficio Sinistri.<br/>
Le ricordiamo che ai sensi dell’Art. 143 del D.L. 209 del 7 Settembre 2005, la
denuncia di sinistro deve essere completa di tutti gli elementi necessari ad una corretta istruzione
della pratica (data, luogo, modalità, generalità di tutte le persone coinvolte, compresi eventuali feriti,
testimoni e autorità intervenute). <br/>Le rammentiamo inoltre che, a norma dell’art. 2 lett. i
delle Condizioni Generali di Contratto, le denunce di sinistro devono essere inviate tramite raccomandata A.R. o inserite sul nostro sito online entro 3 (tre) giorni dalla data di accadimento.<br/>
In caso di mancato riscontro saremo costretti, ns. malgrado, a gestire il sinistro come passivo riservandoci il diritto di procedere all’addebito di una penalità di € 200,00
per le spese e gli oneri amministrativi dovuti al pregiudizio arrecatoci dall’incompletezza della denuncia come indicato nel contratto quadro ALD.<br/>Certi della Sua cortese collaborazione,
restiamo a disposizione per qualsiasi informazione o chiarimento.<br/>Cordiali saluti.<br/>'
where id= '112e9a55-4946-4031-9625-bdfa9251a24f';


UPDATE email_template
set body = 'a seguito della <%NOTTYPDENOTY%> ricevuta il <%REICDATECOMP%>
La informiamo che non è possibile procedere alla gestione assicurativa del sinistro ed alla riparazione del veicolo,
i cui riferimenti in oggetto, a causa dei seguenti motivi:<br/><%REATYPDESTYP%> <%REASDESCCOMP%>.<br/>
La preghiamo quindi di farci pervenire le informazioni mancanti entro e non oltre 15 (quindici) giorni
 dalla presente utilizzando il seguente URL (copiandolo nella barra del browser):<%REPOSPUBLINK_MYALD%>
 per allegare la documentazione.<br/>In alternativa potrà inviare tramite raccomandata (allegando la presente)
 da inviare a: ALD Automotive Italia Srl - Viale Luca Gaurico, 187 - 00143 Roma - c.a. Ufficio Sinistri.
 <br/>Attenzione la presente mail verrà inoltrata anche al Fleet Manager della flotta.<br/>Le ricordiamo
 che ai sensi dell’Art. 143 del D.L. 209 del 7 Settembre 2005,
 la denuncia di sinistro deve essere completa di tutti gli elementi necessari ad una
  corretta istruzione della pratica (data, luogo, modalità, generalità di tutte le persone coinvolte,
   compresi eventuali feriti, testimoni e autorità intervenute). <br/>
   Le rammentiamo inoltre che, a norma dell’art. 2 lett. i delle Condizioni Generali di Contratto,
   le denunce di sinistro devono essere inviate tramite raccomandata A.R. o
   inserite sul nostro sito online entro 3 (tre) giorni dalla data di accadimento.<br/>
   In caso di mancato riscontro saremo costretti, ns. malgrado, a gestire:<br/>-
   il sinistro come passivo riservandoci il diritto di procedere all’addebito di una penalità di
   € 200,00 per le spese e gli oneri amministrativi dovuti al pregiudizio arrecatoci dall’incompletezza della denuncia  <br/>-
   all’addebito di penale per sinistro passivo <br/>- all’addebito per danni al veicolo<br/>
   il tutto come indicato nel contratto quadro e regolato per gli importi nella lettera di offerta.
   <br/>Codice Antifurto Satellitare  <%ANTITH_NAME%><br/>Certi della Sua cortese collaborazione,
   restiamo a disposizione per qualsiasi informazione o chiarimento.'
where id= '725c3737-aa13-4d78-a890-706fcbcccc18';


UPDATE email_template
set body = 'a seguito della <%NOTTYPDENOTY%> ricevuta il <%REICDATECOMP%>
La informiamo che non è possibile procedere alla gestione assicurativa del sinistro, i cui riferimenti in oggetto,
a causa dei seguenti motivi:<br/><%REATYPDESTYP%> <%REASDESCCOMP%>.<br/>La preghiamo quindi di farci pervenire
le informazioni mancanti entro e non oltre 15 (quindici) giorni dalla presente utilizzando il seguente URL
(copiandolo nella barra del browser):<%REPOSPUBLINK_MYALD%> per allegare la documentazione.<br/>
In alternativa potrà inviare l''''allegato tramite mail all’indirizzo gestioneclaims@aldautomotive.com
o tramite raccomandata (allegando la presente) da inviare a: ALD Automotive Italia Srl - Viale Luca Gaurico, 187 -
00143 Roma - c.a. Ufficio Sinistri.<br/>Le ricordiamo che ai sensi dell’Art. 143 del D.L. 209 del 7 Settembre 2005,
la denuncia di sinistro deve essere completa di tutti gli elementi necessari ad una corretta istruzione della pratica
(data, luogo, modalità, generalità di tutte le persone coinvolte, compresi eventuali feriti, testimoni e autorità intervenute).
<br/>Le rammentiamo inoltre che, a norma dell’art. 2 lett. i delle Condizioni Generali di Contratto,
le denunce di sinistro devono essere inviate tramite raccomandata A.R. o inserite sul nostro sito online entro 3 (
tre) giorni dalla data di accadimento.<br/>Certi della Sua cortese collaborazione, restiamo a disposizione per
qualsiasi informazione o chiarimento.<br/>Cordiali saluti.'
where id= '96533511-f03d-4976-8e6f-b8e58afe9271';




UPDATE email_template
set body = 'a seguito della <%NOTTYPDENOTY%> ricevuta il <%REICDATECOMP%>
La informiamo che non è possibile procedere alla gestione assicurativa del sinistro,
 i cui riferimenti in oggetto, a causa dei seguenti motivi:<br/><%REATYPDESTYP%> <%REASDESCCOMP%>.
 <br/>La preghiamo quindi di farci pervenire le informazioni mancanti entro e non oltre 15 (quindici)
 giorni dalla presente utilizzando il seguente URL (copiandolo nella barra del browser):<%REPOSPUBLINK_MYALD%>
 per allegare la documentazione.<br/>In alternativa potrà inviare l''''allegato tramite mail all’indirizzo
 gestioneclaims@aldautomotive.com o tramite raccomandata (allegando la presente)
 da inviare a: ALD Automotive Italia Srl - Viale Luca Gaurico, 187 - 00143 Roma - c.a.
 Ufficio Sinistri.<br/>Le ricordiamo che ai sensi dell’Art. 143 del D.L. 209 del 7 Settembre 2005,
 la denuncia di sinistro deve essere completa di tutti gli elementi necessari ad una corretta istruzione della pratica
  (data, luogo, modalità, generalità di tutte le persone coinvolte, compresi eventuali feriti, testimoni e autorità intervenute)
  . <br/>Le rammentiamo inoltre che, a norma dell’art. 2 lett. i delle Condizioni Generali di Contratto, le
   di sinistro devono essere inviate tramite raccomandata A.R. o inserite sul nostro sito online entro 3 (tre)
   giorni dalla data di accadimento.<br/>In caso di mancato riscontro saremo costretti, ns. malgrado, a gestire
   il sinistro come passivo riservandoci il diritto di procedere all’addebito di una penalità di € 200,00 per le
   spese e gli oneri amministrativi dovuti al pregiudizio arrecatoci dall’incompletezza della denuncia come indicato
   nel contratto quadro ALD.<br/>Certi della Sua cortese collaborazione, restiamo a disposizione per qualsiasi informazione
   o chiarimento.<br/>Cordiali saluti.<br/>'
where id= 'e3a10778-c3e3-4c06-bbab-5c1e551b1a1e';

update email_template
set body = 'a seguito della <%NOTTYPDENOTY%> ricevuta il <%REICDATECOMP%> La informiamo che non è possibile procedere alla gestione assicurativa del sinistro ed alla riparazione del veicolo, i cui riferimenti in oggetto, a causa dei seguenti motivi:<br/><%REATYPDESTYP%> <%REASDESCCOMP%>.<br/>La preghiamo quindi di farci pervenire le informazioni mancanti entro e non oltre 15 (quindici) giorni dalla presente utilizzando il seguente URL (copiandolo nella barra del browser):<%REPOSPUBLINK_MYALD%> per allegare la documentazione.<br/>In alternativa potrà inviare tramite raccomandata (allegando la presente) da inviare a: ALD Automotive Italia Srl - Viale Luca Gaurico, 187 - 00143 Roma - c.a. Ufficio Sinistri.<br/>Attenzione la presente mail verrà inoltrata anche al Fleet Manager della flotta.<br/>Le ricordiamo che ai sensi dell’Art. 143 del D.L. 209 del 7 Settembre 2005, la denuncia di sinistro deve essere completa di tutti gli elementi necessari ad una corretta istruzione della pratica (data, luogo, modalità, generalità di tutte le persone coinvolte, compresi eventuali feriti, testimoni e autorità intervenute). <br/>Le rammentiamo inoltre che, a norma dell’art. 2 lett. i delle Condizioni Generali di Contratto, le denunce di sinistro devono essere inviate tramite raccomandata A.R. o inserite sul nostro sito online entro 3 (tre) giorni dalla data di accadimento.<br/>In caso di mancato riscontro saremo costretti, ns. malgrado, a gestire:<br/>- il sinistro come passivo riservandoci il diritto di procedere all’addebito di una penalità di € 200,00 per le spese e gli oneri amministrativi dovuti al pregiudizio arrecatoci dall’incompletezza della denuncia  <br/>- all’addebito di penale per sinistro passivo <br/>- all’addebito per danni al veicolo<br/>il tutto come indicato nel contratto quadro e regolato per gli importi nella lettera di offerta.<br/>Codice Antifurto Satellitare  <%ANTITH_NAME%><br/>Certi della Sua cortese collaborazione, restiamo a disposizione per qualsiasi informazione o chiarimento.'
where id = '9e48f51a-6d6d-431a-a0f2-6d3aaf3fa236';



