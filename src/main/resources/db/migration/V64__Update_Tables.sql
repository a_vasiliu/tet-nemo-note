update email_template
set body = 'Con la presente inviamo l''incarico per procedere al
	rimborso del danno relativo al sinistro di cui gli estremi in oggetto.
	<br/>Tutta la documentazione attinente può essere visionata utilizzando il seguente URL,
	copiandolo nella barra del vostro browser:              <br/><%REPOSPUBLINK_EXTERNAL%><br/>Cordiali saluti.'
where id = 'dbfe984d-4fbc-4582-b424-70a7bc7950d7';

update email_template
set body = 'Con la presente notifichiamo la registrazione sui nostri sistemi del sinistro di cui gli estremi in oggetto.
		<br/>Tutta la documentazione raccolta sinora può essere visionata utilizzando il seguente URL,
		copiandolo nella barra del vostro browser:
		 <br/><%REPOSPUBLINK_EXTERNAL%><br/>Specifichiamo che la gestione del sinistro e la relativa classificazione,
		 sarà subordinata all’istruttoria e agli elementi probatori raccolti.
		 Per tali motivi potrà divergere da quanto inserito dal Cliente.<br/>Codice Antifurto Satellitare  <%ANTITH_NAME%>'
where id = '3050440d-96fb-4165-9fbd-591f58ab4e6c';

update email_template
set body = 'Con la presente la informiamo che potrà visionare tutta la documentazione relativa al sinistro di cui gli estremi
		in oggetto utilizzando il seguente URL (copiandolo nella barra del browser):
		<br/><%REPOSPUBLINK_EXTERNAL%><br/>Tale procedura sostituirà l’invio della documentazione cartacea.'
where id = '6108e983-49ac-4112-ae06-a1996a16b34b';

update email_template
set body = 'Le confermiamo il corretto inserimento della denuncia del mezzo targato<br/><%VEHIPLATDAMA%>, può procedere alla riparazione consegnando la presente mail al riparatore da Lei scelto.<br/>Se necessita di una copia della denuncia può scaricarla utilizzando il seguente URL
		(copiandolo nella barra del browser): <%REPOSPUBLINK_EXTERNAL%><br/>Le ricordiamo che potrà consultare i centri di assistenza convenzionati ALD Automotive nell''area personale di My ALD al seguente
		link http://mobilitysolutions.aldautomotive.it/area-personale/login o visitando il nostro sito http://www.aldautomotive.it/  nella sezione "Driver/Centri di assistenza”.<br/>In caso di danni ai soli cristalli o grandine, potrai selezionare il centro specializzato direttamente nel menù
		Tipo di Assistenza.<br/>Specifichiamo che la gestione del sinistro e la relativa classificazione, sarà subordinata all’istruttoria e agli elementi probatori raccolti. Per tali motivi potrà divergere da quanto inserito dal Cliente.
		<br/>Attenzione la presente mail verrà inoltrata anche al Fleet Manager della flotta.'
where id = '03f124c1-976a-468e-b904-6461f5f2e9c1';

update email_template
set body = 'Con la presente confermiamo l''inoltro delle presente
		denuncia per la gestione del recupero del danno N. <%EXTACCNUMCOM%>
		sul mezzo targato <%VEHIPLATDAMA%><br/>Tutta la documentazione attinente può essere visionata utilizzando il seguente URL,
		copiandolo nella barra del vostro browser:<br/><%REPOSPUBLINK_EXTERNAL%> '
where id = '540fc63b-ef8f-4821-9c68-3149f7a7507f';


update email_template
set body = 'Con la presente notifichiamo la registrazione sui nostri sistemi del sinistro di cui gli estremi in oggetto. <br/>
		Tutta la documentazione raccolta sinora può essere visionata utilizzando il seguente URL, copiandolo nella barra del vostro browser:
		 <br/><%REPOSPUBLINK_EXTERNAL%><br/>Codice Antifurto Satellitare  <%ANTITH_NAME%>'
where id = '1ade18bd-6f69-4692-83a4-5703e3ca9d7d';

update email_template
set body = 'Con la presente la informiamo che a seguito di una nuova verifica tecnica sul veicolo in oggetto è emerso che
		l''importo del danno riscontrato è stato modificato rispetto a quello precedentemente comunicato. <br/>La invitiamo pertanto a prendere nuova visione della
		 documentazione relativa utilizzando il seguente URL, copiandolo nella barra del vostro browser:
		    <br/><%REPOSPUBLINK_EXTERNAL%>'
where id = 'aeb7623e-6e0d-415a-aa31-4a9552c07917';

update email_template
set body = 'Di seguito il link per reperire la documentazione del sinistro:<%REPOSPUBLINK_EXTERNAL%><br/>In allegato le garanzie del cliente.'
where id = '82d40245-7fe7-465d-b7ec-55a660d43bbd';

update email_template
set body = 'Con la presente inviamo l''incarico
		per procedere al rimborso del danno relativo al sinistro di cui gli estremi in oggetto.
		<br/>Tutta la documentazione attinente può essere visionata utilizzando il seguente URL,
		copiandolo nella barra del vostro browser:               <br/><%REPOSPUBLINK_EXTERNAL%>'
where id = 'b894fe3c-179a-4b99-8031-c70328a633fe';

update email_template
set body = 'Di seguito il link per reperire la documentazione del sinistro:<%REPOSPUBLINK_EXTERNAL%><br/>In allegato le garanzie del cliente.'
where id = '3f4fb81d-b305-40ae-89d3-8f1edd869336';

