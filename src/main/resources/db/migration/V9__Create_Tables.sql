ALTER TABLE claims ADD COLUMN IF NOT EXISTS motivation varchar(500) NULL;
ALTER TABLE claims drop COLUMN IF EXISTS is_locked;
ALTER TABLE claims add COLUMN IF NOT EXISTS with_continuation bool;