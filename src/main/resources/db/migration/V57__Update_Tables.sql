ALTER TABLE counterparty ADD COLUMN IF NOT EXISTS repair_created_at timestamptz;
ALTER TABLE counterparty ADD COLUMN IF NOT EXISTS replacement_car bool;
ALTER TABLE counterparty ADD COLUMN IF NOT EXISTS motivation text;