CREATE TABLE IF NOT EXISTS insurance_policy_app
(
  id uuid NOT NULL,
  type character varying(255),
  description character varying(255),
  insurance_policy_id bigint,
  insurance_company uuid,
  insurance_manager uuid,
  client_code numeric,
  client character varying(255),
  annotations character varying(255),
  beginning_validity date,
  end_validity date,
  book_register boolean,
  book_register_code character varying(255),
  last_renewal_notification date,
  renewal_notification boolean,
  is_active boolean,
  number_policy character varying(255),
  attachment jsonb,
  franchise numeric,
  id_policy_websin numeric
);