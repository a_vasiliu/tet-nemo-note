CREATE TABLE IF NOT EXISTS external_communication(
    id UUID PRIMARY KEY,
    claims_id varchar(255) NOT NULL,
    external_service varchar(255) NOT NULL,
    request jsonb NOT NULL,
    created_at timestamp NOT NULL,
    sent_at timestamp,
    status varchar(255) NOT NULL,
    FOREIGN KEY (claims_id) REFERENCES claims_new (id)
);