ALTER TABLE public.claims_to_claims_authority ADD column oldest boolean;
ALTER TABLE public.claims_to_claims_authority ADD column is_not_duplicate boolean;
ALTER TABLE public.claims_to_claims_authority ADD column franchise_number numeric;
