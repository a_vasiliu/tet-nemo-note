UPDATE claims
SET damaged = jsonb_set( damaged, '{contract,contract_type}','"FUL"')
WHERE (damaged->>'contract')::jsonb->> 'contract_type' = 'BROK';

Alter Table anti_theft_service DROP COLUMN IF EXISTS phone_number;
Alter Table anti_theft_service DROP CONSTRAINT uc_anti_theft_service;
Alter Table anti_theft_service ADD COLUMN IF NOT EXISTS phone_number varchar(255) NULL;
INSERT INTO anti_theft_service(id,code_anti_theft_service,name,business_name,supplier_code,email,phone_number,provider_type,time_out_in_seconds,end_point_url,username,password,company_code,active,is_active) VALUES
 ('8e7fd65d-b0af-43f7-921a-d1550f0653d8','A1AS4','Movitrack - Lojack','Movitrack - Lojack',999994,'lojack@dainserire.it',60000004,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'TRUE')
,('1af8da4d-edb3-43ec-a437-96656ea73c18','ASAT1','Movitrack','Movitrack',999991,'movitrack@dainserie.it',60000001,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'TRUE')
,('0da0419b-3df6-4771-8d99-1c522c3205fc','ASAT2','ARX on demand','ARX on demand',999992,'ARXod@dainserire.it',60000002,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'TRUE')
,('b4fd5370-dbd3-45dc-b33f-b9330f6ad4c5','ASAT3','ARX H24','ARX H24',999993,'arx h24@dainserire.it',60000003,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'TRUE')
,('07cf6cd6-5300-4c83-950d-f8e19e93bcd8','ASAT4','Lojack','Lojack',999994,'lojack@dainserire.it',60000004,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'TRUE')
,('7d52d42b-f512-422e-9591-97bcc10d48b7','ASAT5','Movitrack','Movistrack',999995,'movistrack@dainserire.it',60000005,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'TRUE')
,('1e407f33-b410-4c11-b00c-5edcef026b7c','ASAT6','ARX VAN','ARX VAN',999996,'arx van@dainserire.it',60000006,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'TRUE')
,('ef3ac13b-8edc-4147-93f4-c5846668536e','BBOX1','Movitrack Black Box','Movitrack Black Box',999997,'movitrack black box@dainserire.it',60000007,NULL,NULL,NULL,NULL,NULL,NULL,'FALSE','TRUE')
,('c2faa208-a50e-4f83-b5c6-cf2ceaf288da','BBOX2','Octo Telematics Black Box','Octo Telematics Black Box',999998,'octo telematics black box@dainserire.it',60000008,'OCTO Telematics',180,'https://www.octotelematics.it/DossierCrashAld','ald-wscrash01','tZddsappP32sdk6Jfr','ALD1','TRUE','TRUE')
,('eaff5b8d-4599-4488-bc92-993be7f017f7','O1AS1','OCTO BASIC FURTO - Movitrack','OCTO BASIC FURTO - Movitrack',999998,NULL,60000008,'OCTO Telematics',180,'https://www.octotelematics.it/DossierCrashAld','ald-wscrash01','tZddsappP32sdk6Jfr','ALD1','TRUE','TRUE')
,('56a97d9f-074a-4415-8e47-89b541f50078','O1AS2','OCTO BASIC FURTO - ARX o.d.','OCTO BASIC FURTO - ARX o.d.',999999,NULL,60000009,'OCTO Telematics',180,'https://www.octotelematics.it/DossierCrashAld','ald-wscrash01','tZddsappP32sdk6Jfr','ALD1','TRUE','TRUE')
,('ae52078f-ca0d-429f-a67a-fd699819d80b','O1AS3','OCTO BASIC FURTO - ARX H24','OCTO BASIC FURTO - ARX H24',1000000,NULL,60000010,'OCTO Telematics',180,'https://www.octotelematics.it/DossierCrashAld','ald-wscrash01','tZddsappP32sdk6Jfr','ALD1','TRUE','TRUE')
,('6c559890-366a-40de-b9ba-ca28a9821db7','O1AS4','OCTO BASIC FURTO - Lojack','OCTO BASIC FURTO - Lojack',1000001,NULL,60000011,'OCTO Telematics',180,'https://www.octotelematics.it/DossierCrashAld','ald-wscrash01','tZddsappP32sdk6Jfr','ALD1','TRUE','TRUE')
,('f679a3e8-2b9a-453e-8e5e-01a4c5312467','O1AS5','OCTO BASIC FURTO - Movitrack','OCTO BASIC FURTO - Movitrack',1000002,NULL,60000012,'OCTO Telematics',180,'https://www.octotelematics.it/DossierCrashAld','ald-wscrash01','tZddsappP32sdk6Jfr','ALD1','TRUE','TRUE')
,('814bb23e-cf43-4fb2-b9d1-4b60224d86ab','O2AS1','OCTO FLEET&ECO DRIVE - Movitrack','OCTO FLEET&ECO DRIVE - Movitrack',1000004,NULL,60000014,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'TRUE')
,('c9a3958c-ca3d-45dd-a852-2d3fdcc1cf69','O2AS2','OCTO FLEETeECO DRIVE - ARX o.d.','OCTO FLEET&ECO DRIVE - ARX o.d.',1000005,NULL,60000015,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'TRUE')
,('fad7b92b-7efa-4690-b9a6-f37b5a769102','O2AS3','OCTO FLEETeECO DRIVE - ARX H24','OCTO FLEET&ECO DRIVE - ARX H24',1000006,NULL,60000016,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'TRUE')
,('4d830fd6-4acb-400e-aaa1-bfc24b5d8afc','O2AS4','OCTO FLEET&ECO DRIVE - Lojack','OCTO FLEET&ECO DRIVE - Lojack',1000007,NULL,60000017,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'TRUE')
,('be093f85-5727-40f6-bb7a-980456405cd0','O2AS5','OCTO FLEETeECO DRIVE - Movitrack','OCTO FLEET&ECO DRIVE - Movitrack',1000008,NULL,60000018,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'TRUE')
,('609078e1-451a-4f4b-a394-c55a4cd4b317','O3AS1','OCTO FLEET&ECO DRIVE PLUS - Movitrack','OCTO FLEET&ECO DRIVE PLUS - Movitrack',1000010,NULL,60000020,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'TRUE')
,('41b4d046-1915-454e-bb80-8905440ba605','O3AS2','OCTO FLEET&ECO DRIVE PLUS - ARX o.d.','OCTO FLEET&ECO DRIVE PLUS - ARX o.d.',1000011,NULL,60000021,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'TRUE')
,('1538b567-de4e-46ba-9f45-d813f29cffd0','O3AS3','OCTO FLEET&ECO DRIVE PLUS - ARX H24','OCTO FLEET&ECO DRIVE PLUS - ARX H24',1000012,NULL,60000022,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'TRUE')
,('e53f57b9-2cdb-41ec-a37f-6d10f876222d','O3AS4','OCTO FLEET&ECO DRIVE PLUS - Lojack','OCTO FLEET&ECO DRIVE PLUS - Lojack',1000013,NULL,60000023,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'TRUE')
,('508c9a78-216d-4a49-8b3c-9f9fda31c70d','O3AS5','OCTO FLEET&ECO DRIVE PLUS - Movitrack','OCTO FLEET&ECO DRIVE PLUS - Movitrack',1000014,NULL,60000024,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'TRUE')
,('f594ad34-a09f-4c1d-a5b6-c1858965e28c','O4AS1','OCTO SICUREZZAeCRASH MGMT - Movitrack','OCTO SICUREZZA&CRASH MGMT - Movitrack',1000016,NULL,60000026,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'TRUE')
,('58ff5711-6548-4c7e-b7f5-8df4792c7080','O4AS2','OCTO SICUREZZAeCRASH MGMT - ARX o.d.','OCTO SICUREZZA&CRASH MGMT - ARX o.d.',1000017,NULL,60000027,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'TRUE')
,('b2dfc4f5-51af-4fb0-a847-1c4ca019ffa0','O4AS3','OCTO SICUREZZAeCRASH MGMT - ARX H24','OCTO SICUREZZA&CRASH MGMT - ARX H24',1000018,NULL,60000028,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'TRUE')
,('f5081e69-0f37-4065-a109-6bbf3b288d17','O4AS4','OCTO SICUREZZAeCRASH MGMT - Lojack','OCTO SICUREZZA&CRASH MGMT - Lojack',1000019,NULL,60000029,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'TRUE')
,('ea1fc7a3-0fd5-401a-9058-6ba0eefcb4f2','O4AS5','OCTO SICUREZZAeCRASH MGMT - Movitrack','OCTO SICUREZZA&CRASH MGMT - Movitrack',1000020,NULL,60000030,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'TRUE')
,('bfe4541f-ee64-4e21-8559-abdda8118a92','O5AS1','OCTO SUPEREASY - Movitrack','OCTO SUPEREASY - Movitrack',1000022,NULL,60000032,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'TRUE')
,('2263e629-dceb-4be6-8a27-d4d259a4aceb','O5AS2','OCTO SUPEREASY - ARX o.d.','OCTO SUPEREASY - ARX o.d.',1000023,NULL,60000033,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'TRUE')
,('b0b45fed-abf4-42c8-a0c0-0acaff19a36e','O5AS3','OCTO SUPEREASY - ARX H24','OCTO SUPEREASY - ARX H24',1000024,NULL,60000034,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'TRUE')
,('08753673-3f76-41a7-b69b-621e61d2271c','O5AS4','OCTO SUPEREASY - Lojack','OCTO SUPEREASY - Lojack',1000025,NULL,60000035,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'TRUE')
,('24d5630f-35d0-4a98-81d6-9b0807628c3d','O5AS5','OCTO SUPEREASY - Movitrack','OCTO SUPEREASY - Movitrack',1000026,NULL,60000036,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'TRUE')
,('b6c1e6a3-ec47-4ddb-9766-76c64ba47a8d','OCT10','OCTO RICARICAR','OCTO RICARICAR',1000021,NULL,60000031,'OCTO Telematics',180,'https://www.octotelematics.it/DossierCrashAld','ald-wscrash01','tZddsappP32sdk6Jfr','ALD1','TRUE','TRUE')
,('5d6f9710-ad0a-48bb-b928-b9fcace9cf25','OCT11','OCTO FLEET&ECO DRIVE VOICE','OCTO FLEET&ECO DRIVE VOICE',1000021,NULL,60000031,'OCTO Telematics',180,'https://www.octotelematics.it/DossierCrashAld','ald-wscrash01','tZddsappP32sdk6Jfr','ALD1','TRUE','TRUE')
,('72dec45e-c574-47c3-888e-4ae301de3ccc','OCTL1','OCTO FLEET&ECO DRIVE VOICE - Lojack','OCTO FLEET&ECO DRIVE VOICE - Lojack',999999,NULL,NULL,'OCTO Telematics',180,'https://www.octotelematics.it/DossierCrashAld','ald-wscrash01','tZddsappP32sdk6Jfr','ALD1','TRUE','TRUE')
,('15570a20-9bd0-424c-9aed-38682dc217ca','OCTL2','OCTO RICARICAR - Lojack','OCTO RICARICAR - Lojack',999999,NULL,NULL,'OCTO Telematics',180,'https://www.octotelematics.it/DossierCrashAld','ald-wscrash01','tZddsappP32sdk6Jfr','ALD1','TRUE','TRUE')
,('b4bc8390-f693-4379-bdb3-884eb07730d9','OCTL3','OCTO RICARICAR EV - Lojack','OCTO RICARICAR EV - Lojack',999999,NULL,NULL,'OCTO Telematics',180,'https://www.octotelematics.it/DossierCrashAld','ald-wscrash01','tZddsappP32sdk6Jfr','ALD1','TRUE','TRUE')
,('c1abcd5d-2525-4993-ab26-2064ae7da5b9','OCTL4','OCTO RICARICAR MOTO - Lojack','OCTO RICARICAR MOTO - Lojack',999999,NULL,NULL,'OCTO Telematics',180,'https://www.octotelematics.it/DossierCrashAld','ald-wscrash01','tZddsappP32sdk6Jfr','ALD1','TRUE','TRUE')
,('7535ec54-ba38-4119-80c1-04e0bbc57b02','OCTL5','OCTO SICUREZZA&CRASH MGMT VOICE - Lojack','OCTO SICUREZZA&CRASH MGMT VOICE - Lojack',999999,NULL,NULL,'OCTO Telematics',180,'https://www.octotelematics.it/DossierCrashAld','ald-wscrash01','tZddsappP32sdk6Jfr','ALD1','TRUE','TRUE')
,('afd6c04c-9426-400c-a983-26b45b754ae7','OCTL6','OCTO VEICOLI ELETTRICI - Lojack','OCTO VEICOLI ELETTRICI - Lojack',999999,NULL,NULL,'OCTO Telematics',180,'https://www.octotelematics.it/DossierCrashAld','ald-wscrash01','tZddsappP32sdk6Jfr','ALD1','TRUE','TRUE')
,('ecbc8690-31b3-4f9e-b6af-4dcd546454cf','OCTO1','OCTO BASIC FURTO','OCTO BASIC FURTO',999997,NULL,60000007,'OCTO Telematics',180,'https://www.octotelematics.it/DossierCrashAld','ald-wscrash01','tZddsappP32sdk6Jfr','ALD1','TRUE','TRUE')
,('31a9ea19-0098-45f3-b7d3-1fd82d67df5c','OCTO2','OCTO FLEET&ECO DRIVE','OCTO FLEET&ECO DRIVE',1000003,NULL,60000013,'OCTO Telematics',180,'https://www.octotelematics.it/DossierCrashAld','ald-wscrash01','tZddsappP32sdk6Jfr','ALD1','TRUE','TRUE')
,('db7406b0-d2be-4d74-a9bd-62a89762004f','OCTO3','OCTO FLEET&ECO DRIVE PLUS','OCTO FLEET&ECO DRIVE PLUS',1000009,NULL,60000019,'OCTO Telematics',180,'https://www.octotelematics.it/DossierCrashAld','ald-wscrash01','tZddsappP32sdk6Jfr','ALD1','TRUE','TRUE')
,('0eabda02-3db9-4378-bb0b-2cf6fad19bf8','OCTO4','OCTO SICUREZZAeCRASH MGMT','OCTO SICUREZZA&CRASH MGMT',1000015,NULL,60000025,'OCTO Telematics',180,'https://www.octotelematics.it/DossierCrashAld','ald-wscrash01','tZddsappP32sdk6Jfr','ALD1','TRUE','TRUE')
,('cb463e86-a575-4166-9f4a-0ea5abe35ad8','OCTO5','OCTO SUPEREASY','OCTO SUPEREASY',1000021,NULL,60000031,'OCTO Telematics',180,'https://www.octotelematics.it/DossierCrashAld','ald-wscrash01','tZddsappP32sdk6Jfr','ALD1','TRUE','TRUE')
,('093af004-e2ac-4a02-b6ad-67af1b28efc8','OCTO6','OCTO VEICOLI ELETTRICI','OCTO VEICOLI ELETTRICI',1000021,NULL,60000031,'OCTO Telematics',180,'https://www.octotelematics.it/DossierCrashAld','ald-wscrash01','tZddsappP32sdk6Jfr','ALD1','TRUE','TRUE')
,('d07cb4c5-d8fc-49d5-a5d5-2d68650b584c','OCTO7','OCTO RICARICAR EV','OCTO RICARICAR EV',1000021,NULL,60000031,'OCTO Telematics',180,'https://www.octotelematics.it/DossierCrashAld','ald-wscrash01','tZddsappP32sdk6Jfr','ALD1','TRUE','TRUE')
,('6f1892e3-ab12-4c28-84b1-772ef03748d1','OCTO8','OCTO SICUREZZA&CRASH MGMT VOICE','OCTO SICUREZZA&CRASH MGMT VOICE',1000021,NULL,60000031,'OCTO Telematics',180,'https://www.octotelematics.it/DossierCrashAld','ald-wscrash01','tZddsappP32sdk6Jfr','ALD1','TRUE','TRUE')
,('29725992-c6c0-40c9-8791-a686f0d13b42','OCTO9','OCTO RICARICAR MOTO','OCTO RICARICAR MOTO',1000021,NULL,60000031,'OCTO Telematics',180,'https://www.octotelematics.it/DossierCrashAld','ald-wscrash01','tZddsappP32sdk6Jfr','ALD1','TRUE','TRUE')
,('aed5869f-e3c8-45e3-9060-1e2ee0cb3fb0','OCTX1','OCTO SUPEREASY - TEXA','OCTO SUPEREASY - TEXA',999999,NULL,NULL,'OCTO Telematics',180,'https://www.octotelematics.it/DossierCrashAld','ald-wscrash01','tZddsappP32sdk6Jfr','ALD1','TRUE','TRUE')
,('1dc742c0-b4f9-4173-b376-65def0a9253d','OCTXL','OCTO FLEET&ECO DRIVE - TEXA - Lojack','OCTO FLEET&ECO DRIVE - TEXA - Lojack',999999,NULL,NULL,'OCTO Telematics',180,'https://www.octotelematics.it/DossierCrashAld','ald-wscrash01','tZddsappP32sdk6Jfr','ALD1','TRUE','TRUE')
,('7516e4ba-6f21-4a23-82cd-fbbe19be51c3','T1AS4','TEXA - Lojack','TEXA - Lojack',999999,'lojack@dainserire.it',60000004,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'TRUE')
,('21add098-be12-4f4b-b470-4e27f0a4db2e','TEXA','TEXA NEW','TEXA',999999,'lojack@dainserire.it',60000004,NULL,NULL,'https://txmp-wa-001-wsald.azurewebsites.net/CrashService.svc','crashuserald','Dfj43?qd2!SM2$37F','ALD1','TRUE','TRUE')
,('9e93014b-4ced-4121-89fe-61cb6b54f8c9','TEXA1','TEXA','TEXA',999999,'lojack@dainserire.it',60000004,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'TRUE');