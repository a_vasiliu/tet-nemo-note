UPDATE anti_theft_service
SET provider_type = 'OCTO'
WHERE id in ('093af004-e2ac-4a02-b6ad-67af1b28efc8',
'0eabda02-3db9-4378-bb0b-2cf6fad19bf8',
'15570a20-9bd0-424c-9aed-38682dc217ca',
'1dc742c0-b4f9-4173-b376-65def0a9253d',
'29725992-c6c0-40c9-8791-a686f0d13b42',
'31a9ea19-0098-45f3-b7d3-1fd82d67df5c',
'56a97d9f-074a-4415-8e47-89b541f50078',
'5d6f9710-ad0a-48bb-b928-b9fcace9cf25',
'6c559890-366a-40de-b9ba-ca28a9821db7',
'6f1892e3-ab12-4c28-84b1-772ef03748d1',
'72dec45e-c574-47c3-888e-4ae301de3ccc',
'7535ec54-ba38-4119-80c1-04e0bbc57b02',
'ae52078f-ca0d-429f-a67a-fd699819d80b',
'aed5869f-e3c8-45e3-9060-1e2ee0cb3fb0',
'afd6c04c-9426-400c-a983-26b45b754ae7',
'b4bc8390-f693-4379-bdb3-884eb07730d9',
'b6c1e6a3-ec47-4ddb-9766-76c64ba47a8d',
'c1abcd5d-2525-4993-ab26-2064ae7da5b9',
'c2faa208-a50e-4f83-b5c6-cf2ceaf288da',
'cb463e86-a575-4166-9f4a-0ea5abe35ad8',
'd07cb4c5-d8fc-49d5-a5d5-2d68650b584c',
'db7406b0-d2be-4d74-a9bd-62a89762004f',
'eaff5b8d-4599-4488-bc92-993be7f017f7',
'ecbc8690-31b3-4f9e-b6af-4dcd546454cf',
'f679a3e8-2b9a-453e-8e5e-01a4c5312467');

UPDATE anti_theft_service
SET provider_type = 'TEXA'
WHERE id = '21add098-be12-4f4b-b470-4e27f0a4db2e';