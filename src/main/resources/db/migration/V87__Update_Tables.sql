ALTER TABLE public.external_communication ADD error_message varchar(255) NULL;
ALTER TABLE public.external_communication ADD last_retry_date timestamp NULL;
