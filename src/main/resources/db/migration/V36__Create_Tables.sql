CREATE SEQUENCE IF NOT EXISTS practice_entity_id_sequence;

CREATE TABLE IF NOT EXISTS practice (
	id UUID PRIMARY KEY,
	parent_id UUID NULL,
	status varchar (255) NULL,
	practice_type varchar(255) NULL,
	practice_id int8 DEFAULT nextval('practice_entity_id_sequence') NOT NULL,
	created_at timestamptz NULL,
	updated_at timestamptz NULL,
	created_by varchar(255) NULL,
	claims_id UUID NULL,
    processing_list jsonb NULL,
    attachment_list jsonb NULL,
    vehicle jsonb NULL,
    customer jsonb NULL,
    contract jsonb NULL
);
