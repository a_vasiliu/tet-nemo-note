create table if not exists claims_queue (
  uuid varchar(255) PRIMARY KEY,
  claim_id varchar(255),
  object_practice_id varchar(255),
  user_id varchar(255),
  user_first_name varchar(255),
  user_last_name varchar(255),
  created_at timestamptz,
  updated_at timestamptz,
  caller varchar(255),
  method_name varchar(255),
  retry_count integer,
  request_body text,
  status varchar(255),
  exception_message text
);
