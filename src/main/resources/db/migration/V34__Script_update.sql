CREATE SEQUENCE IF NOT EXISTS legal_code_sequence START 1;
ALTER TABLE legal ADD COLUMN IF NOT EXISTS code int8 DEFAULT nextval('legal_code_sequence') NOT NULL;

CREATE SEQUENCE IF NOT EXISTS inspectorate_code_sequence START 1;
ALTER TABLE inspectorate ADD COLUMN IF NOT EXISTS code int8 DEFAULT nextval('inspectorate_code_sequence') NOT NULL;

CREATE SEQUENCE IF NOT EXISTS insurance_company_code_sequence START 1;
ALTER TABLE insurance_company ADD COLUMN IF NOT EXISTS code int8 DEFAULT nextval('insurance_company_code_sequence') NOT NULL;

CREATE SEQUENCE IF NOT EXISTS manager_code_sequence START 1;
ALTER TABLE insurance_manager ADD COLUMN IF NOT EXISTS rif_code int8 DEFAULT nextval('manager_code_sequence') NOT NULL;

CREATE INDEX IF NOT EXISTS idx_legal_code ON public.legal USING btree(code);
CREATE INDEX IF NOT EXISTS idx_inspectorate_code ON public.inspectorate USING btree(code);
CREATE INDEX IF NOT EXISTS idx_insurance_company_code ON public.insurance_company USING btree(code);
CREATE INDEX IF NOT EXISTS idx_manager_code ON public.insurance_manager USING btree(rif_code);