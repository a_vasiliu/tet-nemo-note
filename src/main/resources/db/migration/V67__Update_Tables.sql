DELETE FROM email_template_recipient_type WHERE email_template_id = '79b8f4ec-5c89-4619-930d-abe81424dbe1';
DELETE FROM email_template WHERE id = '79b8f4ec-5c89-4619-930d-abe81424dbe1';
INSERT INTO public.email_template(
            id, application_event_type_id, description, object, heading,
            foot, attach_file, splitting_recipients_email, client_code, is_active,
            type, body, type_complaint, flows)
    VALUES ('79b8f4ec-5c89-4619-930d-abe81424dbe1', '299ead7b-1bcb-4a7b-bc03-640994194a9e', 'Validazione Furto Totale', 'VALIDAZIONE DENUNCIA N°<%IDCOMPLAINT%>  - <%ACCITYPECDES%> DEL <%ACCIDATECOMP%> TARGA <%VEHIPLATDAMA%>', 'Gent.le Cliente,',
            'Cordiali saluti.<br/>Insurance Department<br/>ALD AUTOMOTIVE ITALIA<br/>Attenzione: questa e-mail è stata generata da un sistema automatico, si prega cortesemente di non rispondere', false, false, '', true,
            'THEFT','Le confermiamo il corretto inserimento della denuncia del mezzo targato <br/> <%VEHIPLATDAMA%> , può procedere alla riparazione consegnando la presente mail al riparatore da Lei scelto. <br/> Se necessita di una copia della denuncia può scaricarla utilizzando il seguente URL\n\t\t(copiandolo nella barra del browser):  <%REPOSPUBLINK_EXTERNAL%> <br/> Le ricordiamo che potrà consultare i centri di assistenza convenzionati ALD Automotive nell''area personale di My ALD al seguente\n\t\tlink http://mobilitysolutions.aldautomotive.it/area-personale/login o visitando il nostro sito http://www.aldautomotive.it/  nella sezione \"Driver/Centri di assistenza”.<br/> In caso di danni ai soli cristalli o grandine, potrai selezionare il centro specializzato direttamente nel menù\n\t\tTipo di Assistenza. <br/> Specifichiamo che la gestione del sinistro e la relativa classificazione, sarà subordinata all’istruttoria e agli elementi probatori raccolti. Per tali motivi potrà divergere da quanto inserito dal Cliente.\n\t\t<br/> Attenzione la presente mail verrà inoltrata anche al Fleet Manager della flotta.', 'CLAIMS', '["FCM", "FNI", "FUL"]');
INSERT INTO public.email_template_recipient_type(
            email_template_id, recipient_type_id)
    VALUES ('79b8f4ec-5c89-4619-930d-abe81424dbe1', 'dd9acc7d-f677-4dec-99bd-f26783455624');
INSERT INTO public.email_template_recipient_type(
            email_template_id, recipient_type_id)
    VALUES ('79b8f4ec-5c89-4619-930d-abe81424dbe1', 'f841113c-4080-4107-8abd-90c2dac003c7');
INSERT INTO public.email_template_recipient_type(
            email_template_id, recipient_type_id)
    VALUES ('79b8f4ec-5c89-4619-930d-abe81424dbe1', '9e3060f4-a5ab-467a-8c73-3fa430112b31');



DELETE FROM email_template_recipient_type WHERE email_template_id = '62ac7f0b-3a90-4ead-a39a-d6cb176f07e1';
DELETE FROM email_template WHERE id = '62ac7f0b-3a90-4ead-a39a-d6cb176f07e1';
INSERT INTO public.email_template(
            id, application_event_type_id, description, object, heading,
            foot, attach_file, splitting_recipients_email, client_code, is_active,
            type, body, type_complaint, flows)
    VALUES ('62ac7f0b-3a90-4ead-a39a-d6cb176f07e1', '76c82420-7532-42c5-82af-8565aa352350', 'Inserimento Furto Totale', 'AVVENUTO INSERIMENTO DENUNCIA N°<%IDCOMPLAINT%> DEL <%ACCIDATECOMP%> TARGA <%VEHIPLATDAMA%>', null,
            'La ringraziamo per la collaborazione.<br/>Insurance Department<br/>ALD AUTOMOTIVE ITALIA<br/>Attenzione: questa e-mail è stata generata da un sistema automatico, si prega cortesemente di non rispondere.', false, false, '', true,
            'THEFT','Cod. Cliente: <%CUSTOMIDCUST%> Contratto: <%CUSTOIDCONTR%> Gent.le Cliente, <br/> l''inserimento della denuncia di sinistro relativa al mezzo di cui il numero di targa in oggetto è avvenuto con successo. Le riportiamo di seguito i dati inseriti: <br/>Conducente: <%DRIVSURNDAMA%> <%DRIVNAMEDAMA%><br/>Luogo del sinistro: <%ACCICITYCOMP%> (<%ACCIPROVCOMP%>)<br/>Descrizione del sinistro: <%DAMACOMMDAMA%> <br/>Danni al veicolo: <%DAMADESCDAMA%><br/>E-mail:<%DRIVEMAIDAMA%><br/>In caso stia denunciando un sinistro con il coinvolgimento di una terza parte (danni ricevuti o provocati ad altra/e autovettura o beni di proprietà altrui) a breve riceverà una mail di conferma sulla completezza dei dati necessari ad ALD con la quale potrà recarsi alla carrozzeria per la riparazione del danno, in caso contrario può procedere direttamente alla riparazione.<br/>Le ricordiamo che potrà consultare i centri di assistenza convenzionati ALD Automotive nell''area personale di My ALD al seguente link http://mobilitysolutions.aldautomotive.it/area-personale/login o visitando il nostro sito http://www.aldautomotive.it/  nella sezione \"Driver/Centri di assistenza”.<br/>In caso di danni ai soli cristalli o grandine, potrai selezionare il centro specializzato direttamente nel menù Tipo di Assistenza', 'CLAIMS', '["FCM", "FNI", "FUL"]');
INSERT INTO public.email_template_recipient_type(
            email_template_id, recipient_type_id)
    VALUES ('62ac7f0b-3a90-4ead-a39a-d6cb176f07e1', '9e3060f4-a5ab-467a-8c73-3fa430112b31');
INSERT INTO public.email_template_recipient_type(
            email_template_id, recipient_type_id)
    VALUES ('62ac7f0b-3a90-4ead-a39a-d6cb176f07e1', 'bc0c4dee-41f3-458a-9e78-68e38987c8d5');
INSERT INTO public.email_template_recipient_type(
            email_template_id, recipient_type_id)
    VALUES ('62ac7f0b-3a90-4ead-a39a-d6cb176f07e1', 'dd9acc7d-f677-4dec-99bd-f26783455624');
INSERT INTO public.email_template_recipient_type(
            email_template_id, recipient_type_id)
    VALUES ('62ac7f0b-3a90-4ead-a39a-d6cb176f07e1', 'f841113c-4080-4107-8abd-90c2dac003c7');

