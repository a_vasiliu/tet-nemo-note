ALTER TABLE practice ADD COLUMN IF NOT EXISTS finding jsonb;
ALTER TABLE practice ADD COLUMN IF NOT EXISTS misappropriation jsonb;
ALTER TABLE practice ADD COLUMN IF NOT EXISTS seizure jsonb;
ALTER TABLE practice ADD COLUMN IF NOT EXISTS release_from_seizure jsonb;