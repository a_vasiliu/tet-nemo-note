ALTER TABLE claims ADD COLUMN IF NOT EXISTS is_migrated bool;
UPDATE claims
SET is_migrated = false;