CREATE OR REPLACE VIEW public.search_dashboard_claims_view
AS SELECT DISTINCT claims_new.id,
    claims_new.practice_id,
    claims_new.created_at,
    claims_new.updated_at,
    claims_new.status_updated_at,
    claims_new.status,
    claims_new.type,
    claims_new.type_accident,
    claims_new.date_accident,
    claims_new.in_evidence,
    claims_new.po_variation,
    claims_new.client_id,
    claims_new.plate,
    claims_new.damaged_anti_theft_service_id,
    ats.business_name,
    claims_new.with_continuation,
    cu.customer_id,
    ce.auto_entrust,
    atrn.response_type,
    atrn.provider_type,
        CASE
            WHEN claims_pending.practice_id IS NOT NULL THEN true
            ELSE false
        END AS is_pending
   FROM claims_new
     LEFT JOIN claims_damaged_customer cu ON claims_new.id::text = cu.id::text
     LEFT JOIN claims_entrusted ce ON claims_new.id::text = ce.id::text
     LEFT JOIN claims_pending ON claims_new.practice_id = claims_pending.practice_id
     LEFT JOIN anti_theft_service ats ON ats.id = claims_new.damaged_anti_theft_service_id
     LEFT JOIN anti_theft_request_new atrn ON claims_new.id::text = atrn.claim_id::text
  WHERE 1 = 1
  ORDER BY claims_new.id, claims_new.practice_id, claims_new.created_at, claims_new.updated_at, claims_new.status_updated_at, claims_new.status, claims_new.type, claims_new.type_accident, claims_new.date_accident, claims_new.in_evidence, claims_new.po_variation, claims_new.client_id, claims_new.plate, claims_new.damaged_anti_theft_service_id, ats.business_name, claims_new.with_continuation, cu.customer_id, ce.auto_entrust, atrn.response_type, atrn.provider_type, (
        CASE
            WHEN claims_pending.practice_id IS NOT NULL THEN true
            ELSE false
        END) DESC;