ALTER TABLE contract_type ADD COLUMN IF NOT EXISTS ctrnote varchar(500);
ALTER TABLE recipient_type ADD COLUMN IF NOT EXISTS recipient_type varchar(500);
ALTER TABLE email_template ADD COLUMN IF NOT EXISTS "type" varchar(500);

