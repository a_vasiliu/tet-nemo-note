ALTER TABLE motivation ADD COLUMN IF NOT EXISTS type_complaint varchar (255);
ALTER TABLE manager ADD COLUMN IF NOT EXISTS type_complaint varchar (255);
ALTER TABLE event_type ADD COLUMN IF NOT EXISTS type_complaint varchar (255);
ALTER TABLE recipient_type ADD COLUMN IF NOT EXISTS type_complaint varchar (255);
ALTER TABLE attachment_type ADD COLUMN IF NOT EXISTS type_complaint varchar (255);
ALTER TABLE email_template ADD COLUMN IF NOT EXISTS type_complaint varchar (255);

CREATE TABLE IF NOT EXISTS generic_attachment (
  id UUID PRIMARY KEY,
  created_at timestamptz NOT NULL,
  original_name varchar(255) NOT NULL,
  description varchar(255) NOT NULL,
  attachment_id varchar(255) NOT NULL
);

