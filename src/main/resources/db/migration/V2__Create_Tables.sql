CREATE SEQUENCE IF NOT EXISTS contract_code_sequence;
CREATE SEQUENCE IF NOT EXISTS personal_code_sequence;
CREATE SEQUENCE IF NOT EXISTS insurance_policy_personal_code_sequence;
CREATE SEQUENCE IF NOT EXISTS motivation_type_id_sequence;

CREATE TABLE IF NOT EXISTS personal_data (
    id UUID PRIMARY KEY,
    personal_code int8 DEFAULT nextval('personal_code_sequence') NOT NULL,
    name varchar(255),
    address varchar(255),
    zip_code numeric ,
    city varchar(255),
    province varchar(255),
    state varchar(255),
    phone_number varchar(255),
    fax varchar(255),
    email varchar(255),
    web_site varchar(255),
    personal_group varchar(255),
    contact varchar(255),
    vat_number varchar(255),
    fiscal_code varchar(255),
    personal_father_id UUID,
    top_customer_id UUID,
    attivation date,
    fleet_manager_name varchar(255),
    fleet_manager_phone varchar(255),
    fleet_manager_mobile_phone varchar(255),
    fleet_manager_primary_email varchar(255),
    fleet_manager_secondary_email varchar(255),
    FOREIGN KEY (personal_father_id) REFERENCES personal_data (id),
    FOREIGN KEY (top_customer_id) REFERENCES top_customer_guarantee (id)
);

CREATE TABLE IF NOT EXISTS contract (
    id UUID PRIMARY KEY,
    contract_code int8 DEFAULT nextval('contract_code_sequence') NOT NULL,
    contract_type_id UUID,
    beginning_of_validity date,
    end_of_validity date,
    plate_number varchar(255),
    delivery_center varchar(255),
    ex_plate_number varchar (255),
    brand varchar(255),
    model varchar(255),
    hp numeric,
    kw numeric,
    weight numeric,
    matriculation date,
    color varchar(255),
    frame varchar(255),
    jato varchar(255),
    eurotax varchar(255),
    km_last_check numeric,
    book_register varchar(255),
    book_register_pai varchar(255),
    insurance_theft_lc numeric ,
    insurance_kasko_md numeric ,
    insurance_search_tpl numeric ,
    insurance_ex_thf numeric ,
    insurance_pai_max numeric ,
    insurance_deduct_md numeric ,
    insurance_pai_pass numeric ,
    insurance_ex_tpl numeric ,
    insurance_max_pass numeric,
    insurance_pai_ex numeric,
    anti_theft_id UUID,
    voucher_id int8,
    activation date ,
    contract_area_business varchar(255),
    km_contract numeric ,
    months_duration numeric ,
    begin_contract date,
    end_contract date,
    suspension date,
    reactivation date,
    lengthened date,
    cancellation date,
    returned date,
    lock_start date,
    lock_end date,
    personal_data_id UUID,
    FOREIGN KEY (anti_theft_id) REFERENCES anti_theft_service (id),
    FOREIGN KEY (contract_type_id) REFERENCES contract_type (id),
    FOREIGN KEY (personal_data_id) REFERENCES personal_data (id)
);

CREATE TABLE IF NOT EXISTS insurance_policy_personal_data (
    id UUID PRIMARY KEY,
    insurance_policy_id int8 DEFAULT nextval('insurance_policy_personal_code_sequence') NOT NULL,
    type varchar (255),
    description varchar (255),
    insurance_company_id UUID,
    number_policy int8,
    beginning_validity date,
    end_validity date,
    broker_id UUID,
    insurance_manager_id UUID,
    annotations varchar (255),
    book_register bool,
    book_register_code varchar (255),
    book_register_pai_code varchar (255),
    crystals_fr varchar (255),
    furinc varchar (255),
    massimal varchar (255),
    furinc_uncover varchar (255),
    kasko_fr varchar (255),
    crystals_max varchar (255),
    personal_data_id UUID,
    FOREIGN KEY (insurance_company_id) REFERENCES insurance_company (id),
    FOREIGN KEY (broker_id) REFERENCES broker (id),
    FOREIGN KEY (insurance_manager_id) REFERENCES insurance_manager (id),
    FOREIGN KEY (personal_data_id) REFERENCES personal_data (id)

);

CREATE TABLE IF NOT EXISTS custom_type_risk (
    id UUID PRIMARY KEY,
    claims_type_id UUID,
    flow varchar (255),
    type_of_chargeback varchar (255),
    personal_data_id UUID,
    FOREIGN KEY (claims_type_id) REFERENCES claims_type (id),
    FOREIGN KEY (personal_data_id) REFERENCES personal_data (id)
);

CREATE TABLE IF NOT EXISTS motivation_type (
  id UUID PRIMARY KEY,
  motivation_type_id int8 DEFAULT nextval('motivation_type_id_sequence') NOT NULL,
  description varchar (255) NOT NULL,
  order_id numeric,
  CONSTRAINT uc_motivation_type UNIQUE (description)
);

CREATE TABLE IF NOT EXISTS recoverability (
  id UUID PRIMARY KEY,
  management varchar (255) NOT NULL,
  claims_type UUID,
  counterpart bool NOT NULL,
  recoverability bool NOT NULL,
  percent_recoverability numeric NOT NULL,
  FOREIGN KEY (claims_type) REFERENCES claims_type (id)
);


CREATE TABLE IF NOT EXISTS notification_type (
  id UUID PRIMARY KEY,
  description varchar(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS manager (
  id UUID PRIMARY KEY,
  name varchar(255) NOT NULL,
  address varchar (255) NULL,
  zip_code numeric(8) NULL,
  locality varchar (255) NULL,
  prov varchar (255) NULL,
  country varchar (255) NULL,
  phone varchar (255) NULL,
  fax varchar (255) NULL,
  email varchar (255) NULL,
  website varchar (255) NULL,
  contact varchar (255) NULL,
  external_export_id varchar (255) NULL
);

CREATE INDEX IF NOT EXISTS idx_contract_id ON public.contract USING btree(contract_code);
CREATE INDEX IF NOT EXISTS idx_personal_id ON public.personal_data USING btree(personal_code);
CREATE INDEX IF NOT EXISTS idx_insurance_policy_personal_id ON public.insurance_policy_personal_data USING btree(insurance_policy_id);
CREATE INDEX IF NOT EXISTS idx_motivation_type_id ON public.motivation_type USING btree(motivation_type_id);
