package com.doing.nemo.claims.dto;

import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.enumerated.DamagedEnum.VehicleEnum.VehicleTypeEnum;
import com.doing.nemo.claims.entity.enumerated.RepairEnum.ManagementTypeEnum;
import com.doing.nemo.claims.entity.enumerated.RepairEnum.RepairProcedureEnum;
import com.doing.nemo.claims.entity.enumerated.RepairEnum.RepairStatusEnum;
import com.doing.nemo.claims.entity.jsonb.Authority;
import com.doing.nemo.claims.entity.jsonb.counterparty.InsuranceCompanyCounterparty;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.ImpactPoint;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.Insured;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbDriver.Driver;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbVehicle.Vehicle;
import com.doing.nemo.claims.entity.jsonb.forms.jsonbForms.Attachment;
import com.doing.nemo.claims.entity.jsonb.repair.Canalization;
import com.doing.nemo.claims.entity.jsonb.repair.HistoricalCounterparty;
import com.doing.nemo.claims.entity.jsonb.repair.LastContact;
import com.doing.nemo.claims.entity.settings.ManagerEntity;
import com.doing.nemo.dwh.client.annotations.DwhEvent;
import com.doing.nemo.dwh.client.annotations.DwhEventObjectId;
import com.doing.nemo.dwh.client.annotations.DwhEventTime;
import com.doing.nemo.dwh.client.payload.request.EventContextEnum;
import com.doing.nemo.dwh.client.payload.request.EventObjectTypeEnum;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@DwhEvent(context = EventContextEnum.CLAIMS, objectType = EventObjectTypeEnum.COUNTERPARTY)
public class DwhCounterpartyDTO {

    @DwhEventObjectId
    private String counterpartyId;

    @DwhEventTime(altProperty = "createdAt")
    private Instant createdAt;

    private Long practiceIdCounterparty;

    private VehicleTypeEnum type;

    private RepairStatusEnum repairStatus;

    private RepairProcedureEnum repairProcedure;

    private String userCreate;

    private String userUpdate;

    private Instant updateAt;

    private AssigneeRepair assignedTo;

    private Instant assignedAt;

    private Insured insured;

    private InsuranceCompanyCounterparty insuranceCompany;

    private Driver driver;

    private Vehicle vehicle;

    private Boolean isCaiSigned;

    private ImpactPoint impactPoint;

    private Boolean responsible;

    private Boolean eligibility;

    private List<Attachment> attachments;

    private List<HistoricalCounterparty> historicals;

    private String description;

    private List<LastContact> lastContact;

    private ManagerEntity manager;

    private Canalization canalization;

    private String policyNumber;

    private Instant policyBeginningValidity;

    private Instant policyEndValidity;

    private ManagementTypeEnum managementType;

    private Boolean askedForDamages;

    private Boolean legalOrConsultant;

    private Instant dateRequestDamages;

    private Instant expirationInstant;

    private String legal;

    private String emailLegal;

    //private ClaimsEntity claims;

    private Boolean isAld;

    private List<Authority> authorities;

    private Boolean isReadMsa;

    public List<Authority> getAuthorities() {
        if(authorities == null){
            return null;
        }
        return new ArrayList<>(authorities);
    }

    public void setAuthorities(List<Authority> authorities) {
        if(authorities != null)
        {
            this.authorities = new ArrayList<>(authorities);
        } else {
            this.authorities = null;
        }
    }

    public Boolean getReadMsa() {
        return isReadMsa;
    }

    public void setReadMsa(Boolean readMsa) {
        isReadMsa = readMsa;
    }

    public String getCounterpartyId() {
        return counterpartyId;
    }

    public void setCounterpartyId(String counterpartyId) {
        this.counterpartyId = counterpartyId;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public Long getPracticeIdCounterparty() {
        return practiceIdCounterparty;
    }

    public void setPracticeIdCounterparty(Long practiceIdCounterparty) {
        this.practiceIdCounterparty = practiceIdCounterparty;
    }

    public VehicleTypeEnum getType() {
        return type;
    }

    public void setType(VehicleTypeEnum type) {
        this.type = type;
    }

    public RepairStatusEnum getRepairStatus() {
        return repairStatus;
    }

    public void setRepairStatus(RepairStatusEnum repairStatus) {
        this.repairStatus = repairStatus;
    }

    public RepairProcedureEnum getRepairProcedure() {
        return repairProcedure;
    }

    public void setRepairProcedure(RepairProcedureEnum repairProcedure) {
        this.repairProcedure = repairProcedure;
    }

    public String getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }

    public String getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(String userUpdate) {
        this.userUpdate = userUpdate;
    }

    public Instant getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Instant updateAt) {
        this.updateAt = updateAt;
    }

    public AssigneeRepair getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(AssigneeRepair assignedTo) {
        this.assignedTo = assignedTo;
    }

    public Instant getAssignedAt() {
        return assignedAt;
    }

    public void setAssignedAt(Instant assignedAt) {
        this.assignedAt = assignedAt;
    }

    public Insured getInsured() {
        return insured;
    }

    public void setInsured(Insured insured) {
        this.insured = insured;
    }

    public InsuranceCompanyCounterparty getInsuranceCompany() {
        return insuranceCompany;
    }

    public void setInsuranceCompany(InsuranceCompanyCounterparty insuranceCompany) {
        this.insuranceCompany = insuranceCompany;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public Boolean getCaiSigned() {
        return isCaiSigned;
    }

    public void setCaiSigned(Boolean caiSigned) {
        isCaiSigned = caiSigned;
    }

    public ImpactPoint getImpactPoint() {
        return impactPoint;
    }

    public void setImpactPoint(ImpactPoint impactPoint) {
        this.impactPoint = impactPoint;
    }

    public Boolean getResponsible() {
        return responsible;
    }

    public void setResponsible(Boolean responsible) {
        this.responsible = responsible;
    }

    public Boolean getEligibility() {
        return eligibility;
    }

    public void setEligibility(Boolean eligibility) {
        this.eligibility = eligibility;
    }

    public List<Attachment> getAttachments() {
        if(attachments == null){
            return null;
        }
        return new ArrayList<>(attachments);
    }

    public void setAttachments(List<Attachment> attachments) {
        if(attachments != null)
        {
            this.attachments =  new ArrayList<>(attachments);
        } else {
            this.attachments = null;
        }
    }

    public List<HistoricalCounterparty> getHistoricals() {
        if(historicals == null){
            return null;
        }
        return new ArrayList<>(historicals);
    }

    public void setHistoricals(List<HistoricalCounterparty> historicals) {
        if(historicals != null)
        {
            this.historicals =new ArrayList<>(historicals);
        } else {
            this.historicals = null;
        }
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<LastContact> getLastContact() {
        if(lastContact == null){
            return null;
        }
        return new ArrayList<>(lastContact);
    }

    public void setLastContact(List<LastContact> lastContact) {
        if(lastContact != null)
        {
            this.lastContact = new ArrayList<>(lastContact);
        } else {
            this.lastContact = null;
        }
    }

    public ManagerEntity getManager() {
        return manager;
    }

    public void setManager(ManagerEntity manager) {
        this.manager = manager;
    }

    public Canalization getCanalization() {
        return canalization;
    }

    public void setCanalization(Canalization canalization) {
        this.canalization = canalization;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public Instant getPolicyBeginningValidity() {
        return policyBeginningValidity;
    }

    public void setPolicyBeginningValidity(Instant policyBeginningValidity) {
        this.policyBeginningValidity = policyBeginningValidity;
    }

    public Instant getPolicyEndValidity() {
        return policyEndValidity;
    }

    public void setPolicyEndValidity(Instant policyEndValidity) {
        this.policyEndValidity = policyEndValidity;
    }

    public ManagementTypeEnum getManagementType() {
        return managementType;
    }

    public void setManagementType(ManagementTypeEnum managementType) {
        this.managementType = managementType;
    }

    public Boolean getAskedForDamages() {
        return askedForDamages;
    }

    public void setAskedForDamages(Boolean askedForDamages) {
        this.askedForDamages = askedForDamages;
    }

    public Boolean getLegalOrConsultant() {
        return legalOrConsultant;
    }

    public void setLegalOrConsultant(Boolean legalOrConsultant) {
        this.legalOrConsultant = legalOrConsultant;
    }

    public Instant getDateRequestDamages() {
        return dateRequestDamages;
    }

    public void setDateRequestDamages(Instant dateRequestDamages) {
        this.dateRequestDamages = dateRequestDamages;
    }

    public Instant getExpirationInstant() {
        return expirationInstant;
    }

    public void setExpirationInstant(Instant expirationInstant) {
        this.expirationInstant = expirationInstant;
    }

    public String getLegal() {
        return legal;
    }

    public void setLegal(String legal) {
        this.legal = legal;
    }

    public String getEmailLegal() {
        return emailLegal;
    }

    public void setEmailLegal(String emailLegal) {
        this.emailLegal = emailLegal;
    }

    public Boolean getAld() {
        return isAld;
    }

    public void setAld(Boolean ald) {
        isAld = ald;
    }
}
