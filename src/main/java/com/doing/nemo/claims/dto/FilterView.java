package com.doing.nemo.claims.dto;

import com.doing.nemo.commons.exception.InternalException;

import java.lang.reflect.Field;
import java.time.Instant;
import java.util.List;

public class FilterView {
    private  Boolean inEvidence;
    private  Long practiceId;
    private List <String> flow;
    private List <String> status;
    private String plate;
    private List <String> typeAccident;
    private Instant dateAccidentFrom;
    private Instant dateAccidentTo;
    private Boolean withContinuation;
    private Instant createdAt;
    private  List<Long> clientListId;

    private String locator;
    private String company;
    private String clientName;
    private Boolean poVariation;
    private Boolean waitingForNumberSx;
    private Long fleetVehicleId;
    private Boolean waitingForAuthority;

    public Instant getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public List<String> getFlow() {
        return flow;
    }

    public void setFlow(List<String> flow) {
        this.flow = flow;
    }

    public List<String> getStatus() {
        return status;
    }

    public void setStatus(List<String> status) {
        this.status = status;
    }

    public List<String> getTypeAccident() {
        return typeAccident;
    }

    public void setTypeAccident(List<String> typeAccident) {
        this.typeAccident = typeAccident;
    }

    public Boolean getInEvidence() {
        return inEvidence;
    }

    public void setInEvidence(Boolean inEvidence) {
        this.inEvidence = inEvidence;
    }

    public Long getPracticeId() {
        return practiceId;
    }

    public void setPracticeId(Long practiceId) {
        this.practiceId = practiceId;
    }


    public List<Long> getClientListId() {
        return clientListId;
    }

    public void setClientListId(List<Long> clientListId) {
        this.clientListId = clientListId;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }



    public String getLocator() {
        return locator;
    }

    public void setLocator(String locator) {
        this.locator = locator;
    }

    public Instant getDateAccidentFrom() {
        return dateAccidentFrom;
    }

    public void setDateAccidentFrom(Instant dateAccidentFrom) {
        this.dateAccidentFrom = dateAccidentFrom;
    }

    public Instant getDateAccidentTo() {
        return dateAccidentTo;
    }

    public void setDateAccidentTo(Instant dateAccidentTo) {
        this.dateAccidentTo = dateAccidentTo;
    }



    public Boolean getWithContinuation() {
        return withContinuation;
    }

    public void setWithContinuation(Boolean withContinuation) {
        this.withContinuation = withContinuation;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public Boolean getPoVariation() {
        return poVariation;
    }

    public void setPoVariation(Boolean poVariation) {
        this.poVariation = poVariation;
    }

    public Boolean getWaitingForNumberSx() {
        return waitingForNumberSx;
    }

    public void setWaitingForNumberSx(Boolean waitingForNumberSx) {
        this.waitingForNumberSx = waitingForNumberSx;
    }

    public Long getFleetVehicleId() {
        return fleetVehicleId;
    }

    public void setFleetVehicleId(Long fleetVehicleId) {
        this.fleetVehicleId = fleetVehicleId;
    }

    public Boolean getWaitingForAuthority() {
        return waitingForAuthority;
    }

    public void setWaitingForAuthority(Boolean waitingForAuthority) {
        this.waitingForAuthority = waitingForAuthority;
    }

    public boolean fieldsAreNull() {
        try {
            for (Field f : getClass().getDeclaredFields())
                if (f.get(this) != null)
                    return false;
            return true;
        } catch (Exception e) {
            throw new InternalException();
        }
    }

}
