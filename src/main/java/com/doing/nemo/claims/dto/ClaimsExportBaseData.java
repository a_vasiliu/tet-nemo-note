package com.doing.nemo.claims.dto;

public class ClaimsExportBaseData {

    private String idPractice;

    private String driverSide;

    private String damagedPlate;

    private String dateAccident;

    public String getIdPractice() {
        return idPractice;
    }

    public ClaimsExportBaseData setIdPractice(Long idPractice) {
        this.idPractice = idPractice.toString();
        return this;
    }

    public String getDriverSide() {
        return driverSide;
    }

    public ClaimsExportBaseData setDriverSide(String driverSide) {
        this.driverSide = driverSide;
        return this;
    }

    public String getDamagedPlate() {
        return damagedPlate;
    }

    public ClaimsExportBaseData setDamagedPlate(String damagedPlate) {
        this.damagedPlate = damagedPlate;
        return this;
    }

    public String getDateAccident() {
        return dateAccident;
    }

    public ClaimsExportBaseData setDateAccident(String dateAccident) {
        this.dateAccident = dateAccident;
        return this;
    }

}
