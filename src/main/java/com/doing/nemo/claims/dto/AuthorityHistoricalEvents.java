package com.doing.nemo.claims.dto;

public class AuthorityHistoricalEvents {

    private HistoricalEventObject repairSend;

    private HistoricalEventObject repairApproved;

    public HistoricalEventObject getRepairSend() {
        return repairSend;
    }

    public void setRepairSend(HistoricalEventObject repairSend) {
        this.repairSend = repairSend;
    }

    public HistoricalEventObject getRepairApproved() {
        return repairApproved;
    }

    public void setRepairApproved(HistoricalEventObject repairApproved) {
        this.repairApproved = repairApproved;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("AuthorityHistoricalEvents{");
        sb.append("repairSend=").append(repairSend);
        sb.append(", repairApproved=").append(repairApproved);
        sb.append('}');
        return sb.toString();
    }

}
