package com.doing.nemo.claims.dto;

public class ClaimHistoricalEvents {

    private HistoricalEventObject claimCreation;

    public HistoricalEventObject getClaimCreation() {
        return claimCreation;
    }

    public void setClaimCreation(HistoricalEventObject claimCreation) {
        this.claimCreation = claimCreation;
    }

    @Override
    public String toString() {
        return "ClaimHistoricalEvents{" +
                "claimCreation=" + claimCreation +
                '}';
    }

}
