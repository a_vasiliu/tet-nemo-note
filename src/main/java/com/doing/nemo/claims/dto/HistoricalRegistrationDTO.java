package com.doing.nemo.claims.dto;

import com.doing.nemo.claims.controller.payload.response.VehicleRegistrationResponse;

import java.util.List;
import java.util.Objects;

public class HistoricalRegistrationDTO {
    private List<VehicleRegistrationResponse> historicalResponseList;

    public List<VehicleRegistrationResponse> getHistoricalResponseList() {
        return historicalResponseList;
    }

    public void setHistoricalResponseList(List<VehicleRegistrationResponse> historicalResponseList) {
        this.historicalResponseList = historicalResponseList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HistoricalRegistrationDTO that = (HistoricalRegistrationDTO) o;
        return historicalResponseList.equals(that.historicalResponseList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(historicalResponseList);
    }
}
