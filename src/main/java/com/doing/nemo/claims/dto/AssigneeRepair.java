package com.doing.nemo.claims.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
@JsonIgnoreProperties(ignoreUnknown = true)
public class AssigneeRepair implements Serializable {
    @JsonProperty("id")
    private String id;

    @JsonProperty("username")
    private String username;

    @JsonProperty("personal_details")
    private PersonalDetailsRepair personalDetails;

    public AssigneeRepair() {
    }

    public AssigneeRepair(String id, String username, PersonalDetailsRepair personalDetails) {
        this.id = id;
        this.username = username;
        this.personalDetails = personalDetails;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public PersonalDetailsRepair getPersonalDetails() {
        return personalDetails;
    }

    public void setPersonalDetails(PersonalDetailsRepair personalDetails) {
        this.personalDetails = personalDetails;
    }

    @Override
    public String toString() {
        return "AssigneeRepair{" +
                "id='" + id + '\'' +
                ", username='" + username + '\'' +
                ", personalDetails=" + personalDetails +
                '}';
    }
}
