package com.doing.nemo.claims.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CtpExportFilter extends AbstractExportFilter {

    @JsonProperty("plate_ald")
    private String plateAld;

    @JsonProperty("plate_ctp")
    private String plateCtp;

    @JsonProperty("practice_id")
    private String practiceId;

    @JsonProperty("repair_id")
    private String idRepair;

    @JsonProperty("status_repair")
    private String statusRepair;

    @JsonProperty("date_created_from")
    private String dateCreatedFrom;

    @JsonProperty("date_created_to")
    private String dateCreatedTo;

    @JsonProperty("date_canalization_from")
    private String dateCanalizationFrom;

    @JsonProperty("date_canalization_to")
    private String dateCanalizationTo;

    @JsonProperty("driver_firstname")
    private String driverFirstName;

    @JsonProperty("driver_lastname")
    private String driverLastName;

    @JsonProperty("last_contact_from")
    private String lastContactFrom;

    @JsonProperty("last_contact_to")
    private String lastContactTo;

    @JsonProperty("operator")
    private String operator;

    @JsonProperty("region")
    private String region;

    @JsonProperty("city")
    private String city;

    public String getPlateAld() {
        return plateAld;
    }

    public void setPlateAld(String plateAld) {
        this.plateAld = plateAld;
    }

    public String getPlateCtp() {
        return plateCtp;
    }

    public void setPlateCtp(String plateCtp) {
        this.plateCtp = plateCtp;
    }

    public String getPracticeId() {
        return practiceId;
    }

    public void setPracticeId(String practiceId) {
        this.practiceId = practiceId;
    }

    public String getIdRepair() {
        return idRepair;
    }

    public void setIdRepair(String idRepair) {
        this.idRepair = idRepair;
    }

    public String getStatusRepair() {
        return statusRepair;
    }

    public void setStatusRepair(String statusRepair) {
        this.statusRepair = statusRepair;
    }

    public String getDateCreatedFrom() {
        return dateCreatedFrom;
    }

    public void setDateCreatedFrom(String dateCreatedFrom) {
        this.dateCreatedFrom = dateCreatedFrom;
    }

    public String getDateCreatedTo() {
        return dateCreatedTo;
    }

    public void setDateCreatedTo(String dateCreatedTo) {
        this.dateCreatedTo = dateCreatedTo;
    }

    public String getDateCanalizationFrom() {
        return dateCanalizationFrom;
    }

    public void setDateCanalizationFrom(String dateCanalizationFrom) {
        this.dateCanalizationFrom = dateCanalizationFrom;
    }

    public String getDateCanalizationTo() {
        return dateCanalizationTo;
    }

    public void setDateCanalizationTo(String dateCanalizationTo) {
        this.dateCanalizationTo = dateCanalizationTo;
    }

    public String getDriverFirstName() {
        return driverFirstName;
    }

    public void setDriverFirstName(String driverFirstName) {
        this.driverFirstName = driverFirstName;
    }

    public String getDriverLastName() {
        return driverLastName;
    }

    public void setDriverLastName(String driverLastName) {
        this.driverLastName = driverLastName;
    }

    public String getLastContactFrom() {
        return lastContactFrom;
    }

    public void setLastContactFrom(String lastContactFrom) {
        this.lastContactFrom = lastContactFrom;
    }

    public String getLastContactTo() {
        return lastContactTo;
    }

    public void setLastContactTo(String lastContactTo) {
        this.lastContactTo = lastContactTo;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "CtpExportFilter{" +
                "plateAld='" + plateAld + '\'' +
                ", plateCtp='" + plateCtp + '\'' +
                ", practiceId='" + practiceId + '\'' +
                ", idRepair='" + idRepair + '\'' +
                ", statusRepair='" + statusRepair + '\'' +
                ", dateCreatedFrom='" + dateCreatedFrom + '\'' +
                ", dateCreatedTo='" + dateCreatedTo + '\'' +
                ", dateCanalizationFrom='" + dateCanalizationFrom + '\'' +
                ", dateCanalizationTo='" + dateCanalizationTo + '\'' +
                ", driverFirstName='" + driverFirstName + '\'' +
                ", driverLastName='" + driverLastName + '\'' +
                ", lastContactFrom='" + lastContactFrom + '\'' +
                ", lastContactTo='" + lastContactTo + '\'' +
                ", operator='" + operator + '\'' +
                ", region='" + region + '\'' +
                ", city='" + city + '\'' +
                '}';
    }
}
