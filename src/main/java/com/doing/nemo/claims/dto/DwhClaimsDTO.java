package com.doing.nemo.claims.dto;

import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsFlowEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.doing.nemo.claims.entity.jsonb.*;
import com.doing.nemo.claims.entity.jsonb.cai.Cai;
import com.doing.nemo.claims.entity.jsonb.complaint.Complaint;
import com.doing.nemo.claims.entity.jsonb.damaged.Damaged;
import com.doing.nemo.claims.entity.jsonb.forms.Forms;
import com.doing.nemo.claims.entity.jsonb.foundModel.FoundModel;
import com.doing.nemo.claims.entity.jsonb.refund.Refund;
import com.doing.nemo.dwh.client.annotations.DwhEvent;
import com.doing.nemo.dwh.client.annotations.DwhEventObjectId;
import com.doing.nemo.dwh.client.annotations.DwhEventTime;
import com.doing.nemo.dwh.client.payload.request.EventContextEnum;
import com.doing.nemo.dwh.client.payload.request.EventObjectTypeEnum;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@DwhEvent(context = EventContextEnum.CLAIMS, objectType = EventObjectTypeEnum.CLAIM)
public class DwhClaimsDTO {

    @DwhEventObjectId
    private String id;

    @DwhEventTime(altProperty = "createdAt")
    private Date createdAt;

    private Date updateAt;

    private Long practiceId;

    private String practiceManager;

    private Boolean paiComunication;

    private Boolean forced;

    private Boolean inEvidence;

    private String userId;

    private ClaimsStatusEnum status;

    private String motivation;

    private ClaimsFlowEnum type;

    private Boolean isCompleteDocumentation;

    private Complaint complaint;

    private Forms forms;

    private Damaged damaged;

    private Cai caiDetails;

    private Boolean isWithCounterparty;

    private List<Deponent> deponentList;

    private List<Wounded> woundedList;

    private List<Notes> notes;

    private List<Historical> historical;

    private FoundModel foundModel;

    private Exemption exemption;

    private Long idSaleforce;

    private Boolean withContinuation;

    private Refund refund;

    private MetadataClaim metadata;

    private Theft theft;

    private Boolean isRead;

    private List<Authority> authorities;

    private Boolean isAuthorityLinkable;

    private Boolean poVariation;

    private Boolean legalComunication;

    private Long totalPenalty;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getCreatedAt() {
        if(createdAt == null){
            return null;
        }
        return (Date)createdAt.clone();
    }

    public void setCreatedAt(Date createdAt) {
        if(createdAt != null)
        {
            this.createdAt = (Date)createdAt.clone();
        } else {
            this.createdAt = null;
        }
    }

    public Date getUpdateAt() {
        if(updateAt == null){
            return null;
        }
        return (Date)updateAt.clone();
    }

    public void setUpdateAt(Date updateAt) {
        if(updateAt != null)
        {
            this.updateAt = (Date)updateAt.clone();
        } else {
            this.updateAt = null;
        }
    }

    public Long getPracticeId() {
        return practiceId;
    }

    public void setPracticeId(Long practiceId) {
        this.practiceId = practiceId;
    }

    public String getPracticeManager() {
        return practiceManager;
    }

    public void setPracticeManager(String practiceManager) {
        this.practiceManager = practiceManager;
    }

    public Boolean getPaiComunication() {
        return paiComunication;
    }

    public void setPaiComunication(Boolean paiComunication) {
        this.paiComunication = paiComunication;
    }

    public Boolean getForced() {
        return forced;
    }

    public void setForced(Boolean forced) {
        this.forced = forced;
    }

    public Boolean getInEvidence() {
        return inEvidence;
    }

    public void setInEvidence(Boolean inEvidence) {
        this.inEvidence = inEvidence;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public ClaimsStatusEnum getStatus() {
        return status;
    }

    public void setStatus(ClaimsStatusEnum status) {
        this.status = status;
    }

    public String getMotivation() {
        return motivation;
    }

    public void setMotivation(String motivation) {
        this.motivation = motivation;
    }

    public ClaimsFlowEnum getType() {
        return type;
    }

    public void setType(ClaimsFlowEnum type) {
        this.type = type;
    }

    public Boolean getCompleteDocumentation() {
        return isCompleteDocumentation;
    }

    public void setCompleteDocumentation(Boolean completeDocumentation) {
        isCompleteDocumentation = completeDocumentation;
    }

    public Complaint getComplaint() {
        return complaint;
    }

    public void setComplaint(Complaint complaint) {
        this.complaint = complaint;
    }

    public Forms getForms() {
        return forms;
    }

    public void setForms(Forms forms) {
        this.forms = forms;
    }

    public Damaged getDamaged() {
        return damaged;
    }

    public void setDamaged(Damaged damaged) {
        this.damaged = damaged;
    }

    public Cai getCaiDetails() {
        return caiDetails;
    }

    public void setCaiDetails(Cai caiDetails) {
        this.caiDetails = caiDetails;
    }

    public Boolean getWithCounterparty() {
        return isWithCounterparty;
    }

    public void setWithCounterparty(Boolean withCounterparty) {
        isWithCounterparty = withCounterparty;
    }

    public List<Deponent> getDeponentList() {
        if(deponentList == null){
            return null;
        }
        return new ArrayList<>(deponentList);
    }

    public void setDeponentList(List<Deponent> deponentList) {
        if(deponentList != null)
        {
            this.deponentList = new ArrayList<>(deponentList);
        } else {
            this.deponentList = null;
        }
    }

    public List<Wounded> getWoundedList() {
        if(woundedList == null){
            return null;
        }
        return new ArrayList<>(woundedList);
    }

    public void setWoundedList(List<Wounded> woundedList) {
        if(woundedList != null)
        {
            this.woundedList = new ArrayList<>(woundedList);
        } else {
            this.woundedList = null;
        }
    }

    public List<Notes> getNotes() {
        if(notes == null){
            return null;
        }
        return new ArrayList<>(notes);
    }

    public void setNotes(List<Notes> notes) {
        if(notes != null)
        {
            this.notes = new ArrayList<>(notes);
        } else {
            this.notes = null;
        }
    }

    public List<Historical> getHistorical() {
        if(historical == null){
            return null;
        }
        return new ArrayList<>(historical);
    }

    public void setHistorical(List<Historical> historical) {
        if(historical != null)
        {
            this.historical =new ArrayList<>(historical);
        } else {
            this.historical = null;
        }
    }

    public FoundModel getFoundModel() {
        return foundModel;
    }

    public void setFoundModel(FoundModel foundModel) {
        this.foundModel = foundModel;
    }

    public Exemption getExemption() {
        return exemption;
    }

    public void setExemption(Exemption exemption) {
        this.exemption = exemption;
    }

    public Long getIdSaleforce() {
        return idSaleforce;
    }

    public void setIdSaleforce(Long idSaleforce) {
        this.idSaleforce = idSaleforce;
    }

    public Boolean getWithContinuation() {
        return withContinuation;
    }

    public void setWithContinuation(Boolean withContinuation) {
        this.withContinuation = withContinuation;
    }

    public Refund getRefund() {
        return refund;
    }

    public void setRefund(Refund refund) {
        this.refund = refund;
    }

    public MetadataClaim getMetadata() {
        return metadata;
    }

    public void setMetadata(MetadataClaim metadata) {
        this.metadata = metadata;
    }

    public Theft getTheft() {
        return theft;
    }

    public void setTheft(Theft theft) {
        this.theft = theft;
    }

    public Boolean getRead() {
        return isRead;
    }

    public void setRead(Boolean read) {
        isRead = read;
    }

    public List<Authority> getAuthorities() {
        if(authorities == null){
            return null;
        }
        return new ArrayList<>(authorities);
    }

    public void setAuthorities(List<Authority> authorities) {
        if(authorities != null){
            this.authorities =new ArrayList<>(authorities);
        } else {
            this.authorities = null;
        }
    }

    public Boolean getAuthorityLinkable() {
        return isAuthorityLinkable;
    }

    public void setAuthorityLinkable(Boolean authorityLinkable) {
        isAuthorityLinkable = authorityLinkable;
    }

    public Boolean getPoVariation() {
        return poVariation;
    }

    public void setPoVariation(Boolean poVariation) {
        this.poVariation = poVariation;
    }

    public Boolean getLegalComunication() {
        return legalComunication;
    }

    public void setLegalComunication(Boolean legalComunication) {
        this.legalComunication = legalComunication;
    }

    public Long getTotalPenalty() {
        return totalPenalty;
    }

    public void setTotalPenalty(Long totalPenalty) {
        this.totalPenalty = totalPenalty;
    }
}
