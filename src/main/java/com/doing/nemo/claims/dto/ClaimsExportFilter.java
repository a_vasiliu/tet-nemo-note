package com.doing.nemo.claims.dto;

import com.doing.nemo.claims.common.config.CommaSeparatedStringSetDeserializer;
import com.doing.nemo.claims.common.config.CommaSeparatedStringSetSerializer;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsFlowEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.EntrustedEnum.EntrustedEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ClaimsExportFilter extends AbstractExportFilter {

    @JsonProperty("date_accident_from")
    private String dateAccidentFrom;

    @JsonProperty("plate_ald")
    private String dateAccidentTo;

    @JsonProperty("date_insert_from")
    private String dateInsertFrom;

    @JsonProperty("date_insert_to")
    private String dateInsertTo;

    @JsonProperty("date_entrusted_from")
    private String dateEntrustedFrom;

    @JsonProperty("date_entrusted_to")
    private String dateEntrustedTo;

    @JsonProperty("practice_id_from")
    private Long practiceIdFrom;

    @JsonProperty("practice_id_to")
    private Long practiceIdTo;

    @JsonSerialize(using = CommaSeparatedStringSetSerializer.class)
    @JsonDeserialize(using = CommaSeparatedStringSetDeserializer.class)
    @JsonProperty("client_id")
    private List<String> clientIdList;

    @JsonIgnore
    private List<ClaimsFlowEnum> flow;

    @JsonIgnore
    private EntrustedEnum entrustedType;

    @JsonProperty("entrusted_id")
    private String entrustedId;

    @JsonProperty("recoverability")
    private Boolean isRecoverability;

    @JsonProperty("counterparty")
    private Boolean isCounterparty;

    @JsonIgnore
    private List<DataAccidentTypeAccidentEnum> typeAccident;

    @JsonIgnore
    private List<ClaimsStatusEnum> status;

    @JsonProperty("asc")
    private Boolean asc;

    @JsonProperty("order_by")
    private String orderBy;

    @JsonProperty("date_definition_from")
    private String dateDefinitionFrom;

    @JsonProperty("date_definition_to")
    private String dateDefinitionTo;

    public String getDateAccidentFrom() {
        return dateAccidentFrom;
    }

    public ClaimsExportFilter setDateAccidentFrom(String dateAccidentFrom) {
        this.dateAccidentFrom = dateAccidentFrom;
        return this;
    }

    public String getDateAccidentTo() {
        return dateAccidentTo;
    }

    public ClaimsExportFilter setDateAccidentTo(String dateAccidentTo) {
        this.dateAccidentTo = dateAccidentTo;
        return this;
    }

    public String getDateInsertFrom() {
        return dateInsertFrom;
    }

    public ClaimsExportFilter setDateInsertFrom(String dateInsertFrom) {
        this.dateInsertFrom = dateInsertFrom;
        return this;
    }

    public String getDateInsertTo() {
        return dateInsertTo;
    }

    public ClaimsExportFilter setDateInsertTo(String dateInsertTo) {
        this.dateInsertTo = dateInsertTo;
        return this;
    }

    public String getDateEntrustedFrom() {
        return dateEntrustedFrom;
    }

    public ClaimsExportFilter setDateEntrustedFrom(String dateEntrustedFrom) {
        this.dateEntrustedFrom = dateEntrustedFrom;
        return this;
    }

    public String getDateEntrustedTo() {
        return dateEntrustedTo;
    }

    public ClaimsExportFilter setDateEntrustedTo(String dateEntrustedTo) {
        this.dateEntrustedTo = dateEntrustedTo;
        return this;
    }

    public Long getPracticeIdFrom() {
        return practiceIdFrom;
    }

    public ClaimsExportFilter setPracticeIdFrom(Long practiceIdFrom) {
        this.practiceIdFrom = practiceIdFrom;
        return this;
    }

    public Long getPracticeIdTo() {
        return practiceIdTo;
    }

    public ClaimsExportFilter setPracticeIdTo(Long practiceIdTo) {
        this.practiceIdTo = practiceIdTo;
        return this;
    }

    public List<String> getClientIdList() {
        return clientIdList;
    }

    public ClaimsExportFilter setClientIdList(List<String> clientIdList) {
        this.clientIdList = clientIdList;
        return this;
    }

    public List<ClaimsFlowEnum> getFlow() {
        return flow;
    }

    public ClaimsExportFilter setFlow(List<ClaimsFlowEnum> flow) {
        this.flow = flow;
        return this;
    }

    public EntrustedEnum getEntrustedType() {
        return entrustedType;
    }

    public ClaimsExportFilter setEntrustedType(EntrustedEnum entrustedType) {
        this.entrustedType = entrustedType;
        return this;
    }

    public String getEntrustedId() {
        return entrustedId;
    }

    public ClaimsExportFilter setEntrustedId(String entrustedId) {
        this.entrustedId = entrustedId;
        return this;
    }

    public Boolean getRecoverability() {
        return isRecoverability;
    }

    public ClaimsExportFilter setRecoverability(Boolean recoverability) {
        isRecoverability = recoverability;
        return this;
    }

    public Boolean getCounterparty() {
        return isCounterparty;
    }

    public ClaimsExportFilter setCounterparty(Boolean counterparty) {
        isCounterparty = counterparty;
        return this;
    }

    public List<DataAccidentTypeAccidentEnum> getTypeAccident() {
        return typeAccident;
    }

    public ClaimsExportFilter setTypeAccident(List<DataAccidentTypeAccidentEnum> typeAccident) {
        this.typeAccident = typeAccident;
        return this;
    }

    public List<ClaimsStatusEnum> getStatus() {
        return status;
    }

    public ClaimsExportFilter setStatus(List<ClaimsStatusEnum> status) {
        this.status = status;
        return this;
    }

    public Boolean getAsc() {
        return asc;
    }

    public ClaimsExportFilter setAsc(Boolean asc) {
        this.asc = asc;
        return this;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public ClaimsExportFilter setOrderBy(String orderBy) {
        this.orderBy = orderBy;
        return this;
    }

    public String getDateDefinitionFrom() {
        return dateDefinitionFrom;
    }

    public ClaimsExportFilter setDateDefinitionFrom(String dateDefinitionFrom) {
        this.dateDefinitionFrom = dateDefinitionFrom;
        return this;
    }

    public String getDateDefinitionTo() {
        return dateDefinitionTo;
    }

    public ClaimsExportFilter setDateDefinitionTo(String dateDefinitionTo) {
        this.dateDefinitionTo = dateDefinitionTo;
        return this;
    }

}
