package com.doing.nemo.claims.dto;

import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;

public class FlowStatusAssociation {
    private ClaimsStatusEnum ful;
    private ClaimsStatusEnum fcm;
    private ClaimsStatusEnum fni;

    public FlowStatusAssociation() {}

    public FlowStatusAssociation(ClaimsStatusEnum ful, ClaimsStatusEnum fcm, ClaimsStatusEnum fni) {
        this.ful = ful;
        this.fcm = fcm;
        this.fni = fni;
    }

    public ClaimsStatusEnum getFul() {
        return ful;
    }

    public void setFul(ClaimsStatusEnum ful) {
        this.ful = ful;
    }

    public ClaimsStatusEnum getFcm() {
        return fcm;
    }

    public void setFcm(ClaimsStatusEnum fcm) {
        this.fcm = fcm;
    }

    public ClaimsStatusEnum getFni() {
        return fni;
    }

    public void setFni(ClaimsStatusEnum fni) {
        this.fni = fni;
    }

    @Override
    public String toString() {
        return "FlowStatusAssociation{" +
                "ful=" + ful +
                ", fcm=" + fcm +
                ", fni=" + fni +
                '}';
    }
}
