package com.doing.nemo.claims.dto;

import java.util.Date;

public class HistoricalEventObject {

    private Date date;
    private String username;

    public HistoricalEventObject(Date date){
        this.date = date;
    }

    public HistoricalEventObject(String username){
        this.username = username;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return "HistoricalEventObject{" +
                "date=" + date +
                ", username='" + username + '\'' +
                '}';
    }

}
