package com.doing.nemo.claims.dto;

public class CounterPartyHistoricalEvents {

    private HistoricalEventObject legal;

    private HistoricalEventObject counterpartyDriver;

    private HistoricalEventObject counterpartyInsured;

    private HistoricalEventObject repairer;

    public HistoricalEventObject getLegal() {
        return legal;
    }

    public void setLegal(HistoricalEventObject legal) {
        this.legal = legal;
    }

    public HistoricalEventObject getCounterpartyDriver() {
        return counterpartyDriver;
    }

    public void setCounterpartyDriver(HistoricalEventObject counterpartyDriver) {
        this.counterpartyDriver = counterpartyDriver;
    }

    public HistoricalEventObject getCounterpartyInsured() {
        return counterpartyInsured;
    }

    public void setCounterpartyInsured(HistoricalEventObject counterpartyInsured) {
        this.counterpartyInsured = counterpartyInsured;
    }

    public HistoricalEventObject getRepairer() {
        return repairer;
    }

    public void setRepairer(HistoricalEventObject repairer) {
        this.repairer = repairer;
    }

}
