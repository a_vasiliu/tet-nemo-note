package com.doing.nemo.claims.dto;


import com.fasterxml.jackson.annotation.JsonIgnoreType;

@JsonIgnoreType
public class StatsDTO {

    private Integer currentPage;
    private Integer itemCount;
    private Integer pageSize;
    private Integer pageCount;

    public StatsDTO() {
    }

    public StatsDTO(Integer currentPage, Integer itemCount, Integer pageSize, Integer pageCount) {
        this.currentPage = currentPage;
        this.itemCount = itemCount;
        this.pageSize = pageSize;
        this.pageCount = pageCount;
    }

    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

    public Integer getItemCount() {
        return itemCount;
    }

    public void setItemCount(Integer itemCount) {
        this.itemCount = itemCount;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getPageCount() {
        return pageCount;
    }

    public void setPageCount(Integer pageCount) {
        this.pageCount = pageCount;
    }
}
