package com.doing.nemo.claims.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

public class KafkaEventExportClaimsDTO {

    private String part;

    @JsonProperty("process_id")
    private String processId;

    private String content;

    //@JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("user_email")
    private String userEmail;

    //@JsonInclude(JsonInclude.Include.NON_NULL)
    private String headers;

    //@JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("template_name")
    private String templateName;

    public KafkaEventExportClaimsDTO() {

    }

    public KafkaEventExportClaimsDTO(String part, String processId, String content, String userEmail, String headers, String templateName) {
        this.part = part;
        this.processId = processId;
        this.content = content;
        this.userEmail = userEmail;
        this.headers = headers;
        this.templateName = templateName;
    }

    public String getPart() {
        return part;
    }

    public void setPart(String part) {
        this.part = part;
    }

    public String getProcessId() {
        return processId;
    }

    public void setProcessId(String processId) {
        this.processId = processId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getHeaders() {
        return headers;
    }

    public void setHeaders(String headers) {
        this.headers = headers;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("KafkaEventExportClaimsDTO{");
        sb.append("part='").append(part).append('\'');
        sb.append(", processId='").append(processId).append('\'');
        sb.append(", content='").append(content).append('\'');
        sb.append(", userEmail='").append(userEmail).append('\'');
        sb.append(", headers='").append(headers).append('\'');
        sb.append(", templateName='").append(templateName).append('\'');
        sb.append('}');
        return sb.toString();
    }

}
