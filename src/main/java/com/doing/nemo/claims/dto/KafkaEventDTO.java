package com.doing.nemo.claims.dto;


import com.fasterxml.jackson.annotation.JsonProperty;


public class KafkaEventDTO {

    private Long practice_id;

    private Long contract_id;

    private String contract_type;

    private String contract_status;

    private String plate;

    private String plate_registration;

    private String chassis;


    public KafkaEventDTO() {
    }
    public Long getPractice_id() {
        return practice_id;
    }

    public void setPractice_id(Long practice_id) {
        this.practice_id = practice_id;
    }

    public Long getContract_id() {
        return contract_id;
    }

    public void setContract_id(Long contract_id) {
        this.contract_id = contract_id;
    }

    public String getContract_type() {
        return contract_type;
    }

    public void setContract_type(String contract_type) {
        this.contract_type = contract_type;
    }

    public String getContract_status() {
        return contract_status;
    }

    public void setContract_status(String contract_status) {
        this.contract_status = contract_status;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public String getPlate_registration() {
        return plate_registration;
    }

    public void setPlate_registration(String plate_registration) {
        this.plate_registration = plate_registration;
    }

    public String getChassis() {
        return chassis;
    }

    public void setChassis(String chassis) {
        this.chassis = chassis;
    }

    @Override
    public String toString() {
        return "KafkaEventDTO{" +
                "practice_id=" + practice_id +
                ", contract_id=" + contract_id +
                ", contract_type='" + contract_type + '\'' +
                ", contract_status='" + contract_status + '\'' +
                ", plate='" + plate + '\'' +
                ", plate_registration='" + plate_registration + '\'' +
                ", chassis='" + chassis + '\'' +
                '}';
    }
}
