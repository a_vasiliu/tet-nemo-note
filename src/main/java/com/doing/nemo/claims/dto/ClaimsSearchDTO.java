package com.doing.nemo.claims.dto;

import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsFlowEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;

import javax.persistence.Column;
import java.time.Instant;

public class ClaimsSearchDTO {

    private String id;
    private Instant createdAt;
    private Instant updatedAt;
    private Instant statusUpdatedAt;
    private String type;
    private Long practiceId;
    private String status;
    private Boolean inEvidence;
    private Boolean poVariation;
    private Boolean  withContinuation;

    /* "complaint" */
    private String plate;
    private String typeAccident;
    private Instant dateAccident;
    private String clientId;

    /* "entrusted" */
    private Boolean autoEntrust;
    /* "damaged" */
    /* "customer" */
    private String customerId;

    /* "damaged anti_theft_service.business_name" */
    private String damagedAntiTheftService_id;
    private String businessName;


    private String responseType;

    private String providerType;

   /* private String crashReport;*/


    private Boolean isPending;

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public Boolean getWithContinuation() {
        return withContinuation;
    }

    public void setWithContinuation(Boolean withContinuation) {
        this.withContinuation = withContinuation;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public Instant getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Instant updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Instant getStatusUpdatedAt() {
        return statusUpdatedAt;
    }

    public void setStatusUpdatedAt(Instant statusUpdatedAt) {
        this.statusUpdatedAt = statusUpdatedAt;
    }

    public String getResponseType() {
        return responseType;
    }

    public void setResponseType(String responseType) {
        this.responseType = responseType;
    }

    public String getProviderType() {
        return providerType;
    }

    public void setProviderType(String providerType) {
        this.providerType = providerType;
    }

    public Long getPracticeId() {
        return practiceId;
    }

    public void setPracticeId(Long practiceId) {
        this.practiceId = practiceId;
    }

    public Boolean getInEvidence() {
        return inEvidence;
    }

    public void setInEvidence(Boolean inEvidence) {
        this.inEvidence = inEvidence;
    }

    public Boolean getPoVariation() {
        return poVariation;
    }

    public void setPoVariation(Boolean poVariation) {
        this.poVariation = poVariation;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }



    public Instant getDateAccident() {
        return dateAccident;
    }

    public void setDateAccident(Instant dateAccident) {
        this.dateAccident = dateAccident;
    }


    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public Boolean getAutoEntrust() {
        return autoEntrust;
    }

    public void setAutoEntrust(Boolean autoEntrust) {
        this.autoEntrust = autoEntrust;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getDamagedAntiTheftService_id() {
        return damagedAntiTheftService_id;
    }

    public void setDamagedAntiTheftService_id(String damagedAntiTheftService_id) {
        this.damagedAntiTheftService_id = damagedAntiTheftService_id;
    }

    public Boolean getPending() {
        return isPending;
    }

    public void setPending(Boolean pending) {
        isPending = pending;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTypeAccident() {
        return typeAccident;
    }

    public void setTypeAccident(String typeAccident) {
        this.typeAccident = typeAccident;
    }
}
