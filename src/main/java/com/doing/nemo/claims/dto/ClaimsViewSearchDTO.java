package com.doing.nemo.claims.dto;

import java.util.ArrayList;
import java.util.List;

public class ClaimsViewSearchDTO {

    private StatsDTO stats;
    private List<ClaimsSearchDTO> claims = new ArrayList<>();

    public ClaimsViewSearchDTO() {
    }

    public ClaimsViewSearchDTO(StatsDTO stats, List<ClaimsSearchDTO> claims) {
        this.stats = stats;
        this.claims = claims;
    }

    public StatsDTO getStats() {
        return stats;
    }

    public void setStats(StatsDTO stats) {
        this.stats = stats;
    }

    public List<ClaimsSearchDTO> getClaims() {
        return claims;
    }

    public void setClaims(List<ClaimsSearchDTO> claims) {
        this.claims = claims;
    }
}
