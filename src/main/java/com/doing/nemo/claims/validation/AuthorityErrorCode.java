package com.doing.nemo.claims.validation;

import com.doing.nemo.claims.controller.payload.response.authority.AuthorityErrorResponse;
import com.doing.nemo.commons.exception.InternalException;
import com.doing.nemo.commons.exception.NotFoundException;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.Response;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;

import java.io.IOException;
import java.util.*;

public enum AuthorityErrorCode {

    AUTHORITY_1025("1025"),
    AUTHORITY_1026("1026"),
    AUTHORITY_1028("1028"),
    AUTHORITY_1029("1029"),
    AUTHORITY_1031("1031"),
    AUTHORITY_1032("1032"),
    AUTHORITY_1039("1039"),
    AUTHORITY_1050("1050"),
    AUTHORITY_1124("1124"),
    AUTHORITY_1129("1129"),
    AUTHORITY_1130("1130"),
    AUTHORITY_1326("1326"),
    AUTHORITY_1327("1327"),
    AUTHORITY_1339("1339"),
    AUTHORITY_1340("1340"),
    AUTHORITY_1341("1341"),
    AUTHORITY_1436("1436"),
    AUTHORITY_4002("4002"),
    AUTHORITY_4003("4003"),
    AUTHORITY_6113("6113");

    private String value;

    private static Map<String, MessageCode> AUTHORITY_CLAIMS_MESSAGE_LOOKUP_TABLE;

    static {
        AUTHORITY_CLAIMS_MESSAGE_LOOKUP_TABLE = Collections.unmodifiableMap(new HashMap<String, MessageCode>() {{
            put(AUTHORITY_1025.value, MessageCode.CLAIMS_1131);
            put(AUTHORITY_1026.value, MessageCode.CLAIMS_1132);
            put(AUTHORITY_1028.value, MessageCode.CLAIMS_1133);
            put(AUTHORITY_1029.value, MessageCode.CLAIMS_1134);
            put(AUTHORITY_1031.value, MessageCode.CLAIMS_1135);
            put(AUTHORITY_1032.value, MessageCode.CLAIMS_1136);
            put(AUTHORITY_1039.value, MessageCode.CLAIMS_1137);
            put(AUTHORITY_1050.value, MessageCode.CLAIMS_1138);
            put(AUTHORITY_1124.value, MessageCode.CLAIMS_1139);
            put(AUTHORITY_1129.value, MessageCode.CLAIMS_1140);
            put(AUTHORITY_1130.value, MessageCode.CLAIMS_1141);
            put(AUTHORITY_1326.value, MessageCode.CLAIMS_1142);
            put(AUTHORITY_1327.value, MessageCode.CLAIMS_1143);
            put(AUTHORITY_1339.value, MessageCode.CLAIMS_1144);
            put(AUTHORITY_1340.value, MessageCode.CLAIMS_1145);
            put(AUTHORITY_1341.value, MessageCode.CLAIMS_1146);
            put(AUTHORITY_1436.value, MessageCode.CLAIMS_1147);
            put(AUTHORITY_4002.value, MessageCode.CLAIMS_1148);
            put(AUTHORITY_4003.value, MessageCode.CLAIMS_1149);
            put(AUTHORITY_6113.value, MessageCode.CLAIMS_1150);
        }});
    }

    public static MessageCode fromClaimsToAuthority(String claimsCode){

        Optional<String> key = AUTHORITY_CLAIMS_MESSAGE_LOOKUP_TABLE.keySet().stream().filter(item -> StringUtils.isNotBlank(claimsCode) && claimsCode.contains(item)).findFirst();

        MessageCode code = key.isPresent() ? AUTHORITY_CLAIMS_MESSAGE_LOOKUP_TABLE.get(key.get()) : null;

        if (code == null){
          return MessageCode.CLAIMS_1130;
        }

        return code;
    }

    public static void CheckResponseAndRaiseException(Response response, Boolean checkSuccessful) throws InternalException, IOException {
        CheckResponseAndRaiseException(response, null, checkSuccessful);
    }

    public static void CheckResponseAndRaiseException(Response response) throws InternalException, IOException {
        CheckResponseAndRaiseException(response, HttpStatus.OK, null);
    }

    public static void CheckResponseAndRaiseException(Response response, HttpStatus httpStatusExpected, Boolean checkSuccessful) throws InternalException, IOException {
        if (
            (httpStatusExpected != null && response.code() != httpStatusExpected.value()) ||
            (checkSuccessful != null && checkSuccessful && !response.isSuccessful())
        ) {
            String responseAsString = response.body().string();
            AuthorityErrorResponse claimsErrorResponse = adaptTo(responseAsString);

            MessageCode messageCode;
            String message;

            if (StringUtils.isNotBlank(claimsErrorResponse.getCode())) {
                messageCode = AuthorityErrorCode.fromClaimsToAuthority(claimsErrorResponse.getCode());
                message = claimsErrorResponse.getMessage();
            } else {
                messageCode = MessageCode.CLAIMS_1103;
                message = responseAsString;
            }

            if(messageCode != null && messageCode == MessageCode.CLAIMS_1144){
                throw new NotFoundException(messageCode.value(), messageCode);
            }

            throw new InternalException(messageCode.value(), messageCode);
        }
    }
    
    private static AuthorityErrorResponse adaptTo(String responseAsString) {
        try {
            return new ObjectMapper().readValue(responseAsString, AuthorityErrorResponse.class);

        }catch (Exception e){
            return new AuthorityErrorResponse();
        }
    }


    AuthorityErrorCode(String value){
        this.value = value;
    }
}
