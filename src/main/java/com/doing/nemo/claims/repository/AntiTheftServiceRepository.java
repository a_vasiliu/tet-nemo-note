package com.doing.nemo.claims.repository;


import com.doing.nemo.claims.entity.settings.AntiTheftServiceEntity;
import com.doing.nemo.claims.entity.settings.ContractEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface AntiTheftServiceRepository extends JpaRepository<AntiTheftServiceEntity, UUID> {

    @Query("SELECT a FROM AntiTheftServiceEntity a WHERE a.name = :name")
    AntiTheftServiceEntity searchAntiTheftServicebyName(@Param("name") String name);

    @Query("SELECT a FROM AntiTheftServiceEntity a WHERE a.id <> :id AND a.name = :name")
    AntiTheftServiceEntity searchAntiTheftServiceWithDifferentId(@Param("id") UUID id, @Param("name") String name);

    @Query("SELECT c FROM ContractEntity c INNER JOIN c.antiTheft a WHERE a.id = :theftId")
    ContractEntity searchContractForeignKey(@Param("theftId") UUID id);

    @Query("SELECT a FROM AntiTheftServiceEntity a WHERE a.codeAntiTheftService= :codeAntiTheftService")
    AntiTheftServiceEntity findAntiTheftByCodAntiTheftService(@Param("codeAntiTheftService") String codeAntiTheftService);


}
