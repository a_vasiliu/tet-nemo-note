package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.common.repository.GenericRepository;
import com.doing.nemo.claims.dto.FilterView;
import com.doing.nemo.claims.entity.ClaimsNewEntity;
import com.doing.nemo.claims.entity.view.SearchDashboardClaimsView;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface SearchDashboardClaimsViewRepository extends JpaRepository<SearchDashboardClaimsView, String> , JpaSpecificationExecutor<SearchDashboardClaimsView> {
}
