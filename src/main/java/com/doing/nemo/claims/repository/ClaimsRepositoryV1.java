package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.adapter.DateAdapter;
import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.ClaimsEntity_;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsFlowEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.EntrustedEnum.EntrustedEnum;
import com.doing.nemo.claims.entity.enumerated.DTO.ClaimsStats;
import com.doing.nemo.claims.entity.enumerated.DTO.ClaimsTheftStats;
import com.doing.nemo.claims.entity.enumerated.myAld.MyAldInsertedByEnum;
import com.doing.nemo.claims.entity.jsonb.Authority;
import com.doing.nemo.claims.service.GoLiveStrategyService;
import com.doing.nemo.claims.util.Util;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.BadParametersException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.Tuple;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import javax.transaction.Transactional;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.doing.nemo.claims.entity.jsonb.JsonbProcessingFunctions.JSONB_EXTRACT_PATH_TEXT;

@Repository
public class ClaimsRepositoryV1 {

    private static Logger LOGGER = LoggerFactory.getLogger(ClaimsRepositoryV1.class);
    @PersistenceContext
    @Autowired
    private EntityManager em;
    @Autowired
    private Util util;

    @Autowired
    private DateAdapter dateAdapter;

    @Value("${acclaims.id}")
    private String acclaimsId;

    @Value("${acclaims.sr.id}")
    private String acclaimsSrId;

    @Autowired
    private GoLiveStrategyService goLiveStrategyService;


    //QUERY per trasformare il vecchio nel nuovo

    public BigInteger getCountAllClaimsOldDB(){
        StringBuilder sql = new StringBuilder("SELECT COUNT (*) FROM claims claims  WHERE claims.converted IS NULL OR claims.converted = false");
        Query query = em.createNativeQuery(sql.toString());
        return (BigInteger) query.getSingleResult();
    }

    public List<ClaimsEntity> getOldClaimsPaginated(int pageSize, int page){
        StringBuilder sql = new StringBuilder("SELECT * FROM claims claims WHERE claims.converted IS NULL OR claims.converted = false ");
        sql.append(" ORDER BY claims.practice_id " );
        sql.append(" LIMIT " + pageSize );
        Query query = em.createNativeQuery(sql.toString(), ClaimsEntity.class);
        System.out.println(sql.toString());
        return query.getResultList();
    }
    //



    public BigInteger countClaimsForPagination(Boolean inEvidence, Long practiceId, List<String> flow, List<String> clientIdList, String plate, List<String> status, String locator, String dateAccidentFrom, String dateAccidentTo, List<String> typeAccident, Boolean asc, String orderBy, Integer page, Integer pageSize, Boolean withContinuation, String company, Boolean withReceptions, String clientName, Boolean poVariation, Long fleetVehicleId) {
        StringBuilder sql = new StringBuilder("SELECT COUNT (*) FROM claims claims ");
        StringBuilder whereCondition = new StringBuilder(" WHERE 1=1 ");
        if (dateAccidentFrom != null && dateAccidentTo != null) {
            if (dateAccidentTo.compareTo(dateAccidentFrom) < 0) {
                LOGGER.debug("the date_accident_to is less than the date_accident_from");
                throw new BadParametersException("the date_accident_to is less than the date_accident_from", MessageCode.CLAIMS_1011);
            } else {
                whereCondition.append(" AND ((((claims.complaint->>'data_accident')\\:\\:jsonb)->>'date_accident')\\:\\:timestamp BETWEEN '" + dateAccidentFrom + "'\\:\\:timestamp AND '" + dateAccidentTo + "'\\:\\:timestamp )");
            }
        } else if ((dateAccidentFrom != null) || (dateAccidentTo != null)) {
            LOGGER.debug("the date_accident_to and date_accident_from must be present together ");
            throw new BadParametersException("the date_accident_to and date_accident_from must be present together ", MessageCode.CLAIMS_1011);
        }

        if (clientIdList != null && !clientIdList.isEmpty()) {
            whereCondition.append(" AND (claims.damaged->>'customer')\\:\\:jsonb->>'customer_id' IN (:clientIdList) ");
        }

        if (Util.isNotEmpty(clientName)) {
            whereCondition.append(" AND (claims.damaged->>'customer')\\:\\:jsonb->>'legal_name' ILIKE '%" + clientName + "%'  ");
        }

        if (Util.isNotEmpty(plate)) {
            whereCondition.append(" AND claims.complaint->>'plate' ILIKE '%" + plate.trim() + "%' ");
        }

        if (fleetVehicleId != null) {
            whereCondition.append(" AND (claims.damaged->>'vehicle')\\:\\:jsonb->>'fleet_vehicle_id' = '" + fleetVehicleId + "'  ");
        }

        if (Util.isNotEmpty(locator)) {
            whereCondition.append(" AND claims.complaint->>'locator' ILIKE '%" + locator + "%' ");
        }

        if (practiceId != null) {
            whereCondition.append(" AND claims.practice_id = " + practiceId + " ");
        }
        if (inEvidence != null) {
            whereCondition.append(" AND claims.in_evidence = " + inEvidence + " ");
        }

        if (withContinuation != null) {
            whereCondition.append(" AND claims.with_continuation =" + withContinuation + " ");
        }

        if (poVariation != null) {
            whereCondition.append(" AND claims.po_variation =" + poVariation + " ");
        }

        if (withReceptions != null) {
            whereCondition.append(" AND (claims.theft->>'is_with_receptions')\\:\\:bool =" + withReceptions + " ");
        }

        if (typeAccident != null) {
            whereCondition.append(" AND (claims.complaint->>'data_accident')\\:\\:jsonb->>'type_accident' IN (:typeAccidentSet) ");
        }

        if (Util.isNotEmpty(company))
            whereCondition.append(" AND ((claims.damaged->>'insurance_company')\\:\\:jsonb->>'tpl')\\:\\:jsonb->>'company' ILIKE '%" + company + "%' ");

        if (status != null) {
            whereCondition.append(" AND claims.status IN (:statusSet) ");
        }

        if (flow != null) {
            whereCondition.append(" AND claims.type IN (:flowSet) ");
        }

        sql.append(whereCondition);

        Query query = em.createNativeQuery(sql.toString());

        if (typeAccident != null)
            query.setParameter("typeAccidentSet", typeAccident);
        if (flow != null)
            query.setParameter("flowSet", flow);
        if (status != null)
            query.setParameter("statusSet", status);
        if (clientIdList != null)
            query.setParameter("clientIdList", clientIdList);

        return (BigInteger) query.getSingleResult();

    }

    public List<ClaimsEntity> findClaimsForPagination(Boolean inEvidence, Long practiceId, List<String> flow, List<String> clientIdList, String plate, List<String> status, String locator, String dateAccidentFrom, String dateAccidentTo, List<String> typeAccident, Boolean asc, String orderBy, Integer page, Integer pageSize, Boolean withContinuation, String company, Boolean withReceptions, String clientName, Boolean poVariation, Long fleetVehicleId) {
        Map<String, String> mappOrder = new HashMap<>();
        mappOrder.put("type", " claims.type ");
        mappOrder.put("practice_id", " claims.practice_id ");
        mappOrder.put("date_accident", " (claims.complaint->>'data_accident')\\:\\:jsonb->>'date_accident' ");
        mappOrder.put("plate", " claims.complaint->>'plate' ");
        mappOrder.put("created_at", " claims.created_at ");
        mappOrder.put("type_accident", " (claims.complaint->>'data_accident')\\:\\:jsonb->>'type_accident' ");
        mappOrder.put("locator", " claims.complaint->>'locator' ");

        StringBuilder sql = new StringBuilder("SELECT * FROM claims claims ");
        StringBuilder whereCondition = new StringBuilder(" WHERE 1=1 ");

        if (dateAccidentFrom != null && dateAccidentTo != null) {

            if (dateAccidentTo.compareTo(dateAccidentFrom) < 0) {
                LOGGER.debug("the date_accident_to is less than the date_accident_from");
                throw new BadParametersException("the date_accident_to is less than the date_accident_from", MessageCode.CLAIMS_1011);
            } else {
                whereCondition.append(" AND ((((claims.complaint->>'data_accident')\\:\\:jsonb)->>'date_accident')\\:\\:timestamp BETWEEN '" + dateAccidentFrom + "'\\:\\:timestamp AND '" + dateAccidentTo + "'\\:\\:timestamp )");
            }
        } else if ((dateAccidentFrom != null) || (dateAccidentTo != null)) {
            LOGGER.debug("the date_accident_to and date_accident_from must be present together ");
            throw new BadParametersException("the date_accident_to and date_accident_from must be present together ", MessageCode.CLAIMS_1011);
        }

        if (clientIdList != null && !clientIdList.isEmpty()) {
            whereCondition.append(" AND (claims.damaged->>'customer')\\:\\:jsonb->>'customer_id' IN (:clientIdList) ");
        }

        if (Util.isNotEmpty(clientName)) {
            whereCondition.append(" AND (claims.damaged->>'customer')\\:\\:jsonb->>'legal_name' ILIKE '%" + clientName + "%'  ");
        }

        if (fleetVehicleId != null) {
            whereCondition.append(" AND (claims.damaged->>'vehicle')\\:\\:jsonb->>'fleet_vehicle_id' = '" + fleetVehicleId + "'  ");
        }

        if (Util.isNotEmpty(plate)) {
            whereCondition.append(" AND claims.complaint->>'plate' ILIKE '%" + plate.trim() + "%' ");
        }

        if (Util.isNotEmpty(locator)) {
            whereCondition.append(" AND claims.complaint->>'locator' ILIKE '%" + locator + "%' ");
        }

        if (practiceId != null) {
            whereCondition.append(" AND claims.practice_id = " + practiceId + " ");
        }
        if (inEvidence != null) {
            whereCondition.append(" AND claims.in_evidence = " + inEvidence + " ");
        }

        if (withContinuation != null) {
            whereCondition.append(" AND claims.with_continuation =" + withContinuation + " ");
        }

        if (poVariation != null) {
            whereCondition.append(" AND claims.po_variation =" + poVariation + " ");
        }

        if (withReceptions != null) {
            whereCondition.append(" AND (claims.theft->>'is_with_receptions')\\:\\:bool =" + withReceptions + " ");
        }

        if (typeAccident != null) {
            whereCondition.append(" AND (claims.complaint->>'data_accident')\\:\\:jsonb->>'type_accident' IN (:typeAccidentSet) ");
        }

        if (Util.isNotEmpty(company))
            whereCondition.append(" AND ((claims.damaged->>'insurance_company')\\:\\:jsonb->>'tpl')\\:\\:jsonb->>'company' ILIKE '%" + company + "%' ");

        if (status != null) {
            whereCondition.append(" AND claims.status IN (:statusSet) ");
        }

        if (flow != null) {
            whereCondition.append(" AND claims.type IN (:flowSet) ");
        }

        sql.append(whereCondition);

        String fieldToOrder = null;
        if (orderBy != null)
            fieldToOrder = mappOrder.get(orderBy);
        if (fieldToOrder != null && asc != null) {
            if (asc) {
                sql.append(" ORDER BY " + fieldToOrder + " ASC");
            } else {
                sql.append(" ORDER BY " + fieldToOrder + " DESC");
            }

        } else if (fieldToOrder != null) {
            sql.append(" ORDER BY " + fieldToOrder + " DESC");
        }
        sql.append(" LIMIT " + pageSize + " OFFSET " + (page - 1) * pageSize);

        Query query = em.createNativeQuery(sql.toString(), ClaimsEntity.class);
        if (typeAccident != null)
            query.setParameter("typeAccidentSet", typeAccident);
        if (flow != null)
            query.setParameter("flowSet", flow);
        if (status != null)
            query.setParameter("statusSet", status);
        if (clientIdList != null)
            query.setParameter("clientIdList", clientIdList);

        return query.getResultList();
    }

    public BigInteger countClaimsForPaginationWithoutDraftAndTheft(Boolean inEvidence, Long practiceId, List<String> flow, String clientId, String plate, List<String> status, String locator, String dateAccidentFrom, String dateAccidentTo, List<String> typeAccident, Boolean asc, String orderBy, Integer page, Integer pageSize, Boolean withContinuation, String company, String clientName, Boolean poVariation, Boolean waitingForNumberSx, Long fleetVehicleId, Boolean waitingForAuthority) {
        StringBuilder authorityTable = new StringBuilder("WITH AUTH AS (SELECT id, jsonb_array_elements(authority) AS authority FROM claims AS c WHERE c.status = 'WAITING_FOR_AUTHORITY' ) ");
        StringBuilder sql = new StringBuilder("SELECT COUNT (*) FROM claims c ");
        StringBuilder whereCondition = new StringBuilder(" WHERE 1=1 ");
        if (dateAccidentFrom != null && dateAccidentTo != null) {
            if (dateAccidentTo.compareTo(dateAccidentFrom) < 0) {
                LOGGER.debug("the date_accident_to is less than the date_accident_from");
                throw new BadParametersException("the date_accident_to is less than the date_accident_from", MessageCode.CLAIMS_1011);
            } else {
                whereCondition.append(" AND ((((c.complaint->>'data_accident')\\:\\:jsonb)->>'date_accident')\\:\\:timestamp BETWEEN '" + dateAccidentFrom + "'\\:\\:timestamp AND '" + dateAccidentTo + "'\\:\\:timestamp )");
            }
        } else if ((dateAccidentFrom != null) || (dateAccidentTo != null)) {
            LOGGER.debug("the date_accident_to and date_accident_from must be present together ");
            throw new BadParametersException("the date_accident_to and date_accident_from must be present together ", MessageCode.CLAIMS_1011);
        }

        if (fleetVehicleId != null) {
            whereCondition.append(" AND (c.damaged->>'vehicle')\\:\\:jsonb->>'fleet_vehicle_id' = '" + fleetVehicleId + "'  ");
        }

        if (Util.isNotEmpty(clientId)) {
            whereCondition.append(" AND (c.damaged->>'customer')\\:\\:jsonb->>'customer_id'  = '" + clientId + "' ");
        }

        if (Util.isNotEmpty(clientName)) {
            whereCondition.append(" AND (c.damaged->>'customer')\\:\\:jsonb->>'legal_name' ILIKE '%" + clientName + "%'  ");
        }

        if (Util.isNotEmpty(plate)) {
            whereCondition.append(" AND c.complaint->>'plate' ILIKE '%" + plate.trim() + "%' ");
        }

        if (Util.isNotEmpty(locator)) {
            whereCondition.append(" AND c.complaint->>'locator' ILIKE '%" + locator + "%' ");
        }

        if (practiceId != null) {
            whereCondition.append(" AND c.practice_id = " + practiceId + " ");
        }
        if (inEvidence != null) {
            whereCondition.append(" AND c.in_evidence = " + inEvidence + " ");
        }

        if (poVariation != null) {
            whereCondition.append(" AND c.po_variation =" + poVariation + " ");
        }


        if (withContinuation != null) {
            whereCondition.append(" AND c.with_continuation = " + withContinuation + " ");
        }

        if (typeAccident != null) {
            whereCondition.append(" AND (c.complaint->>'data_accident')\\:\\:jsonb->>'type_accident' IN (:typeAccidentSet) ");
        }

        if (Util.isNotEmpty(company))
            whereCondition.append(" AND ((c.damaged->>'insurance_company')\\:\\:jsonb->>'tpl')\\:\\:jsonb->>'company' ILIKE '%" + company + "%' ");

        if (status != null) {
            whereCondition.append(" AND c.status IN (:statusSet) AND c.status <> 'DRAFT'");
        } else {
            whereCondition.append(" AND c.status <> 'DRAFT'");
        }

        if (flow != null) {
            whereCondition.append(" AND c.type IN (:flowSet) ");
        }

        if (waitingForNumberSx != null && waitingForNumberSx) {
            whereCondition.append(" AND ((c.complaint->>'from_company')\\:\\:jsonb IS NULL OR (c.complaint->>'from_company')\\:\\:jsonb->>'number_sx' IS NULL OR (c.complaint->>'from_company')\\:\\:jsonb->>'number_sx' = '') ");

        } else if (waitingForNumberSx != null) {
            whereCondition.append(" AND ((c.complaint->>'from_company')\\:\\:jsonb IS NOT NULL AND (c.complaint->>'from_company')\\:\\:jsonb->>'number_sx' IS NOT NULL OR (c.complaint->>'from_company')\\:\\:jsonb->>'number_sx' != '') ");
        }

        if(waitingForAuthority != null){

            sql = authorityTable.append(sql);
            if(waitingForAuthority)
                whereCondition.append(" AND c.status = 'WAITING_FOR_AUTHORITY' AND c.authority IS NOT NULL AND jsonb_array_length(authority\\:\\:jsonb) <> 0 AND c.id NOT IN (SELECT c.id FROM claims as c JOIN AUTH as a USING(id) WHERE a.authority->'is_invoiced' = 'true' OR a.authority->>'event_type' = 'REJECT' )");
                //QUESTA HA SENSO FARLA?
            else
                whereCondition.append(" AND c.status = 'WAITING_FOR_AUTHORITY' AND c.authority IS NOT NULL AND jsonb_array_length(authority\\:\\:jsonb) <> 0 AND c.id IN (SELECT c.id FROM claims as c JOIN AUTH as a USING(id) WHERE a.authority->'is_invoiced' = 'true' OR a.authority->>'event_type' = 'REJECT' )");

        }

        sql.append(whereCondition);

        Query query = em.createNativeQuery(sql.toString());

        if (typeAccident != null)
            query.setParameter("typeAccidentSet", typeAccident);
        if (flow != null)
            query.setParameter("flowSet", flow);
        if (status != null)
            query.setParameter("statusSet", status);

        return (BigInteger) query.getSingleResult();

    }

    public BigInteger countClaimsForPaginationTheftOperator(Boolean inEvidence, Long practiceId, List<String> flow, String clientId, String plate, List<String> status, String locator, String dateAccidentFrom, String dateAccidentTo, List<String> typeAccident, Boolean asc, String orderBy, Integer page, Integer pageSize, Boolean withContinuation, String company, String clientName, Boolean poVariation, Boolean waitingForNumberSx, Long fleetVehicleId, Boolean waitingForAuthority) {
        StringBuilder authorityTable = new StringBuilder("WITH AUTH AS (SELECT id, jsonb_array_elements(authority) AS authority FROM claims AS c WHERE c.status = 'WAITING_FOR_AUTHORITY' ) ");
        StringBuilder sql = new StringBuilder("SELECT COUNT (*) FROM claims c ");
        StringBuilder whereCondition = new StringBuilder(" WHERE 1=1 ");
        if (dateAccidentFrom != null && dateAccidentTo != null) {
            if (dateAccidentTo.compareTo(dateAccidentFrom) < 0) {
                LOGGER.debug("the date_accident_to is less than the date_accident_from");
                throw new BadParametersException("the date_accident_to is less than the date_accident_from", MessageCode.CLAIMS_1011);
            } else {
                whereCondition.append(" AND ((((c.complaint->>'data_accident')\\:\\:jsonb)->>'date_accident')\\:\\:timestamp BETWEEN '" + dateAccidentFrom + "'\\:\\:timestamp AND '" + dateAccidentTo + "'\\:\\:timestamp )");
            }
        } else if ((dateAccidentFrom != null) || (dateAccidentTo != null)) {
            LOGGER.debug("the date_accident_to and date_accident_from must be present together ");
            throw new BadParametersException("the date_accident_to and date_accident_from must be present together ", MessageCode.CLAIMS_1011);
        }

        if (fleetVehicleId != null) {
            whereCondition.append(" AND (c.damaged->>'vehicle')\\:\\:jsonb->>'fleet_vehicle_id' = '" + fleetVehicleId + "'  ");
        }

        if (Util.isNotEmpty(clientId)) {
            whereCondition.append(" AND (c.damaged->>'customer')\\:\\:jsonb->>'customer_id'  = '" + clientId + "' ");
        }

        if (Util.isNotEmpty(clientName)) {
            whereCondition.append(" AND (c.damaged->>'customer')\\:\\:jsonb->>'legal_name' ILIKE '%" + clientName + "%'  ");
        }

        if (Util.isNotEmpty(plate)) {
            whereCondition.append(" AND c.complaint->>'plate' ILIKE '%" + plate.trim() + "%' ");
        }

        if (Util.isNotEmpty(locator)) {
            whereCondition.append(" AND c.complaint->>'locator' ILIKE '%" + locator + "%' ");
        }

        if (practiceId != null) {
            whereCondition.append(" AND c.practice_id = " + practiceId + " ");
        }
        if (inEvidence != null) {
            whereCondition.append(" AND c.in_evidence = " + inEvidence + " ");
        }

        if (poVariation != null) {
            whereCondition.append(" AND c.po_variation =" + poVariation + " ");
        }


        if (withContinuation != null) {
            whereCondition.append(" AND c.with_continuation = " + withContinuation + " ");
        }


        if (typeAccident != null) {
            whereCondition.append(" AND (c.complaint->>'data_accident')\\:\\:jsonb->>'type_accident' IN (:typeAccidentSet) ");
        } else {
            whereCondition.append(" AND ((c.complaint->>'data_accident')\\:\\:jsonb->>'type_accident' = 'FURTO_TOTALE' OR (c.complaint->>'data_accident')\\:\\:jsonb->>'type_accident' = 'FURTO_PARZIALE' OR (c.complaint->>'data_accident')\\:\\:jsonb->>'type_accident' = 'FURTO_E_RITROVAMENTO' )");
        }

        if (Util.isNotEmpty(company))
            whereCondition.append(" AND ((c.damaged->>'insurance_company')\\:\\:jsonb->>'tpl')\\:\\:jsonb->>'company' ILIKE '%" + company + "%' ");

        if (status != null) {
            whereCondition.append(" AND c.status IN (:statusSet) AND c.status <> 'DRAFT'");
        } else {
            whereCondition.append(" AND c.status <> 'DRAFT'");
        }

        if (flow != null) {
            whereCondition.append(" AND c.type IN (:flowSet) ");
        }

        if (waitingForNumberSx != null && waitingForNumberSx) {
            whereCondition.append(" AND ((c.complaint->>'from_company')\\:\\:jsonb IS NULL OR (c.complaint->>'from_company')\\:\\:jsonb->>'number_sx' IS NULL OR (c.complaint->>'from_company')\\:\\:jsonb->>'number_sx' = '') ");

        } else if (waitingForNumberSx != null) {
            whereCondition.append(" AND ((c.complaint->>'from_company')\\:\\:jsonb IS NOT NULL AND (c.complaint->>'from_company')\\:\\:jsonb->>'number_sx' IS NOT NULL OR (c.complaint->>'from_company')\\:\\:jsonb->>'number_sx' != '') ");
        }

        if(waitingForAuthority != null){

            sql = authorityTable.append(sql);
            if(waitingForAuthority)
                whereCondition.append(" AND c.status = 'WAITING_FOR_AUTHORITY' AND c.authority IS NOT NULL AND jsonb_array_length(authority\\:\\:jsonb) <> 0 AND c.id NOT IN (SELECT c.id FROM claims as c JOIN AUTH as a USING(id) WHERE a.authority->'is_invoiced' = 'true' OR a.authority->>'event_type' = 'REJECT' )");
                //QUESTA HA SENSO FARLA?
            else
                whereCondition.append(" AND c.status = 'WAITING_FOR_AUTHORITY' AND c.authority IS NOT NULL AND jsonb_array_length(authority\\:\\:jsonb) <> 0 AND c.id IN (SELECT c.id FROM claims as c JOIN AUTH as a USING(id) WHERE a.authority->'is_invoiced' = 'true' OR a.authority->>'event_type' = 'REJECT' )");

        }

        sql.append(whereCondition);

        Query query = em.createNativeQuery(sql.toString());

        if (typeAccident != null)
            query.setParameter("typeAccidentSet", typeAccident);
        if (flow != null)
            query.setParameter("flowSet", flow);
        if (status != null)
            query.setParameter("statusSet", status);

        return (BigInteger) query.getSingleResult();

    }

    public List<ClaimsEntity> findClaimsForPaginationWithoutDraftAndTheft(Boolean inEvidence, Long practiceId, List<String> flow, String clientId, String plate, List<String> status, String locator, String dateAccidentFrom, String dateAccidentTo, List<String> typeAccident, Boolean asc, String orderBy, Integer page, Integer pageSize, Boolean withContinuation, String company, String clientName, Boolean poVariation, Boolean waitingForNumberSx, Long fleetVehicleId, Boolean waitingForAuthority) {
        Map<String, String> mappOrder = new HashMap<>();
        mappOrder.put("type", " c.type ");
        mappOrder.put("practice_id", " c.practice_id ");
        mappOrder.put("date_accident", " (c.complaint->>'data_accident')\\:\\:jsonb->>'date_accident' ");
        mappOrder.put("plate", " c.complaint->>'plate' ");
        mappOrder.put("created_at", " c.created_at ");
        mappOrder.put("type_accident", " (c.complaint->>'data_accident')\\:\\:jsonb->>'type_accident' ");
        mappOrder.put("locator", " c.complaint->>'locator' ");
        StringBuilder authorityTable = new StringBuilder("WITH AUTH AS (SELECT id, jsonb_array_elements(authority) AS authority FROM claims AS c WHERE c.status = 'WAITING_FOR_AUTHORITY' ) ");
        StringBuilder sql = new StringBuilder("SELECT * FROM claims c ");
        StringBuilder whereCondition = new StringBuilder(" WHERE 1=1 ");

        if (dateAccidentFrom != null && dateAccidentTo != null) {
            if (dateAccidentTo.compareTo(dateAccidentFrom) < 0) {
                LOGGER.debug("the date_accident_to is less than the date_accident_from");
                throw new BadParametersException("the date_accident_to is less than the date_accident_from", MessageCode.CLAIMS_1011);
            } else {
                whereCondition.append(" AND ((((c.complaint->>'data_accident')\\:\\:jsonb)->>'date_accident')\\:\\:timestamp BETWEEN '" + dateAccidentFrom + "'\\:\\:timestamp AND '" + dateAccidentTo + "'\\:\\:timestamp )");
            }
        } else if ((dateAccidentFrom != null) || (dateAccidentTo != null)) {
            LOGGER.debug("the date_accident_to and date_accident_from must be present together ");
            throw new BadParametersException("the date_accident_to and date_accident_from must be present together ", MessageCode.CLAIMS_1011);
        }

        if (Util.isNotEmpty(clientId)) {
            whereCondition.append(" AND (c.damaged->>'customer')\\:\\:jsonb->>'customer_id'  = '" + clientId + "' ");
        }

        if (poVariation != null) {
            whereCondition.append(" AND c.po_variation =" + poVariation + " ");
        }

        if (Util.isNotEmpty(clientName)) {
            whereCondition.append(" AND (c.damaged->>'customer')\\:\\:jsonb->>'legal_name' ILIKE '%" + clientName + "%'  ");
        }

        if (Util.isNotEmpty(plate)) {
            whereCondition.append(" AND c.complaint->>'plate' ILIKE '%" + plate.trim() + "%' ");
        }

        if (Util.isNotEmpty(locator)) {
            whereCondition.append(" AND c.complaint->>'locator' ILIKE '%" + locator + "%' ");
        }

        if (fleetVehicleId != null) {
            whereCondition.append(" AND (c.damaged->>'vehicle')\\:\\:jsonb->>'fleet_vehicle_id' = '" + fleetVehicleId + "'  ");
        }

        if (practiceId != null) {
            whereCondition.append(" AND c.practice_id = " + practiceId + " ");
        }
        if (inEvidence != null) {
            whereCondition.append(" AND c.in_evidence = " + inEvidence + " ");
        }

        if (withContinuation != null) {
            whereCondition.append(" AND c.with_continuation = " + withContinuation + " ");
        }

        if (typeAccident != null) {
            whereCondition.append(" AND (c.complaint->>'data_accident')\\:\\:jsonb->>'type_accident' IN (:typeAccidentSet) ");
        }

        if (Util.isNotEmpty(company))
            whereCondition.append(" AND ((c.damaged->>'insurance_company')\\:\\:jsonb->>'tpl')\\:\\:jsonb->>'company' ILIKE '%" + company + "%' ");

        if (status != null) {
            whereCondition.append(" AND c.status IN (:statusSet) AND c.status <> 'DRAFT'");
        } else {
            whereCondition.append(" AND c.status <> 'DRAFT'");
        }

        if (flow != null) {
            whereCondition.append(" AND c.type IN (:flowSet) ");
        }

        if (waitingForNumberSx != null && waitingForNumberSx) {
            whereCondition.append(" AND ((c.complaint->>'from_company')\\:\\:jsonb IS NULL OR (c.complaint->>'from_company')\\:\\:jsonb->>'number_sx' IS NULL OR (c.complaint->>'from_company')\\:\\:jsonb->>'number_sx' = '') ");

        } else if (waitingForNumberSx != null) {
            whereCondition.append(" AND ((c.complaint->>'from_company')\\:\\:jsonb IS NOT NULL AND (c.complaint->>'from_company')\\:\\:jsonb->>'number_sx' IS NOT NULL OR (c.complaint->>'from_company')\\:\\:jsonb->>'number_sx' != '') ");
        }

        if(waitingForAuthority != null){

            sql = authorityTable.append(sql);
            if(waitingForAuthority)
                whereCondition.append(" AND c.status = 'WAITING_FOR_AUTHORITY' AND c.authority IS NOT NULL AND jsonb_array_length(authority\\:\\:jsonb) <> 0 AND c.id NOT IN (SELECT c.id FROM claims as c JOIN AUTH as a USING(id) WHERE a.authority->'is_invoiced' = 'true' OR a.authority->>'event_type' = 'REJECT' )");
            else
                whereCondition.append(" AND c.status = 'WAITING_FOR_AUTHORITY' AND c.authority IS NOT NULL AND jsonb_array_length(authority\\:\\:jsonb) <> 0 AND c.id IN (SELECT c.id FROM claims as c JOIN AUTH as a USING(id) WHERE a.authority->'is_invoiced' = 'true' OR a.authority->>'event_type' = 'REJECT' )");

        }

        sql.append(whereCondition);

        String fieldToOrder = null;
        if (orderBy != null)
            fieldToOrder = mappOrder.get(orderBy);
        if (fieldToOrder != null && asc != null) {
            if (asc) {
                sql.append(" ORDER BY " + fieldToOrder + " ASC");
            } else {
                sql.append(" ORDER BY " + fieldToOrder + " DESC");
            }

        } else if (fieldToOrder != null) {
            sql.append(" ORDER BY " + fieldToOrder + " DESC");
        }

        sql.append(" LIMIT " + pageSize + " OFFSET " + (page - 1) * pageSize);

        Query query = em.createNativeQuery(sql.toString(), ClaimsEntity.class);


        if (typeAccident != null)
            query.setParameter("typeAccidentSet", typeAccident);
        if (flow != null)
            query.setParameter("flowSet", flow);
        if (status != null)
            query.setParameter("statusSet", status);

        return query.getResultList();

    }

    public List<ClaimsEntity> findClaimsForPaginationTheftOperator(Boolean inEvidence, Long practiceId, List<String> flow, String clientId, String plate, List<String> status, String locator, String dateAccidentFrom, String dateAccidentTo, List<String> typeAccident, Boolean asc, String orderBy, Integer page, Integer pageSize, Boolean withContinuation, String company, String clientName, Boolean poVariation, Boolean waitingForNumberSx, Long fleetVehicleId, Boolean waitingForAuthority) {
        Map<String, String> mappOrder = new HashMap<>();
        mappOrder.put("type", " c.type ");
        mappOrder.put("practice_id", " c.practice_id ");
        mappOrder.put("date_accident", " (c.complaint->>'data_accident')\\:\\:jsonb->>'date_accident' ");
        mappOrder.put("plate", " c.complaint->>'plate' ");
        mappOrder.put("created_at", " c.created_at ");
        mappOrder.put("type_accident", " (c.complaint->>'data_accident')\\:\\:jsonb->>'type_accident' ");
        mappOrder.put("locator", " c.complaint->>'locator' ");
        StringBuilder authorityTable = new StringBuilder("WITH AUTH AS (SELECT id, jsonb_array_elements(authority) AS authority FROM claims AS c WHERE c.status = 'WAITING_FOR_AUTHORITY' ) ");
        StringBuilder sql = new StringBuilder("SELECT * FROM claims c ");
        StringBuilder whereCondition = new StringBuilder(" WHERE 1=1 ");

        if (dateAccidentFrom != null && dateAccidentTo != null) {
            if (dateAccidentTo.compareTo(dateAccidentFrom) < 0) {
                LOGGER.debug("the date_accident_to is less than the date_accident_from");
                throw new BadParametersException("the date_accident_to is less than the date_accident_from", MessageCode.CLAIMS_1011);
            } else {
                whereCondition.append(" AND ((((c.complaint->>'data_accident')\\:\\:jsonb)->>'date_accident')\\:\\:timestamp BETWEEN '" + dateAccidentFrom + "'\\:\\:timestamp AND '" + dateAccidentTo + "'\\:\\:timestamp )");
            }
        } else if ((dateAccidentFrom != null) || (dateAccidentTo != null)) {
            LOGGER.debug("the date_accident_to and date_accident_from must be present together ");
            throw new BadParametersException("the date_accident_to and date_accident_from must be present together ", MessageCode.CLAIMS_1011);
        }

        if (Util.isNotEmpty(clientId)) {
            whereCondition.append(" AND (c.damaged->>'customer')\\:\\:jsonb->>'customer_id'  = '" + clientId + "' ");
        }

        if (poVariation != null) {
            whereCondition.append(" AND c.po_variation =" + poVariation + " ");
        }

        if (Util.isNotEmpty(clientName)) {
            whereCondition.append(" AND (c.damaged->>'customer')\\:\\:jsonb->>'legal_name' ILIKE '%" + clientName + "%'  ");
        }

        if (Util.isNotEmpty(plate)) {
            whereCondition.append(" AND c.complaint->>'plate' ILIKE '%" + plate.trim() + "%' ");
        }

        if (Util.isNotEmpty(locator)) {
            whereCondition.append(" AND c.complaint->>'locator' ILIKE '%" + locator + "%' ");
        }

        if (fleetVehicleId != null) {
            whereCondition.append(" AND (c.damaged->>'vehicle')\\:\\:jsonb->>'fleet_vehicle_id' = '" + fleetVehicleId + "'  ");
        }

        if (practiceId != null) {
            whereCondition.append(" AND c.practice_id = " + practiceId + " ");
        }
        if (inEvidence != null) {
            whereCondition.append(" AND c.in_evidence = " + inEvidence + " ");
        }

        if (withContinuation != null) {
            whereCondition.append(" AND c.with_continuation = " + withContinuation + " ");
        }

        if (typeAccident != null) {
            whereCondition.append(" AND (c.complaint->>'data_accident')\\:\\:jsonb->>'type_accident' IN (:typeAccidentSet) ");
        } else {
            whereCondition.append(" AND ((c.complaint->>'data_accident')\\:\\:jsonb->>'type_accident' = 'FURTO_TOTALE' OR (c.complaint->>'data_accident')\\:\\:jsonb->>'type_accident' = 'FURTO_PARZIALE' OR (c.complaint->>'data_accident')\\:\\:jsonb->>'type_accident' = 'FURTO_E_RITROVAMENTO' )");
        }

        if (Util.isNotEmpty(company))
            whereCondition.append(" AND ((c.damaged->>'insurance_company')\\:\\:jsonb->>'tpl')\\:\\:jsonb->>'company' ILIKE '%" + company + "%' ");

        if (status != null) {
            whereCondition.append(" AND c.status IN (:statusSet) AND c.status <> 'DRAFT'");
        } else {
            whereCondition.append(" AND c.status <> 'DRAFT'");
        }

        if (flow != null) {
            whereCondition.append(" AND c.type IN (:flowSet) ");
        }

        if (waitingForNumberSx != null && waitingForNumberSx) {
            whereCondition.append(" AND ((c.complaint->>'from_company')\\:\\:jsonb IS NULL OR (c.complaint->>'from_company')\\:\\:jsonb->>'number_sx' IS NULL OR (c.complaint->>'from_company')\\:\\:jsonb->>'number_sx' = '') ");

        } else if (waitingForNumberSx != null) {
            whereCondition.append(" AND ((c.complaint->>'from_company')\\:\\:jsonb IS NOT NULL AND (c.complaint->>'from_company')\\:\\:jsonb->>'number_sx' IS NOT NULL OR (c.complaint->>'from_company')\\:\\:jsonb->>'number_sx' != '') ");
        }

        if(waitingForAuthority != null){

            sql = authorityTable.append(sql);
            if(waitingForAuthority)
                whereCondition.append(" AND c.status = 'WAITING_FOR_AUTHORITY' AND c.authority IS NOT NULL AND jsonb_array_length(authority\\:\\:jsonb) <> 0 AND c.id NOT IN (SELECT c.id FROM claims as c JOIN AUTH as a USING(id) WHERE a.authority->'is_invoiced' = 'true' OR a.authority->>'event_type' = 'REJECT' )");
            //QUESTA HA SENSO FARLA?
            else
                whereCondition.append(" AND c.status = 'WAITING_FOR_AUTHORITY' AND c.authority IS NOT NULL AND jsonb_array_length(authority\\:\\:jsonb) <> 0 AND c.id IN (SELECT c.id FROM claims as c JOIN AUTH as a USING(id) WHERE a.authority->'is_invoiced' = 'true' OR a.authority->>'event_type' = 'REJECT' )");

        }

        String fieldToOrder = null;
        if (orderBy != null)
            fieldToOrder = mappOrder.get(orderBy);
        if (fieldToOrder != null && asc != null) {
            if (asc) {
                sql.append(" ORDER BY " + fieldToOrder + " ASC");
            } else {
                sql.append(" ORDER BY " + fieldToOrder + " DESC");
            }

        } else if (fieldToOrder != null) {
            sql.append(" ORDER BY " + fieldToOrder + " DESC");
        }

        sql.append(" LIMIT " + pageSize + " OFFSET " + (page - 1) * pageSize);

        System.out.println(sql);

        Query query = em.createNativeQuery(sql.toString(), ClaimsEntity.class);


        if (typeAccident != null)
            query.setParameter("typeAccidentSet", typeAccident);
        if (flow != null)
            query.setParameter("flowSet", flow);
        if (status != null)
            query.setParameter("statusSet", status);

        return query.getResultList();

    }

    public BigInteger countClaimsForPaginationOnlyTheft(Boolean inEvidence, Long practiceId, List<String> flow, String clientId, String plate, List<String> status, String locator, String dateAccidentFrom, String dateAccidentTo, Boolean asc, String orderBy, Integer page, Integer pageSize, Boolean withContinuation, String company, Boolean withReceptions, String clientName) {
        StringBuilder sql = new StringBuilder("SELECT COUNT (*) FROM claims c ");
        StringBuilder whereCondition = new StringBuilder("WHERE 1=1 ");
        if (dateAccidentFrom != null && dateAccidentTo != null) {
            if (dateAccidentTo.compareTo(dateAccidentFrom) < 0) {
                LOGGER.debug("the date_accident_to is less than the date_accident_from");
                throw new BadParametersException("the date_accident_to is less than the date_accident_from", MessageCode.CLAIMS_1011);
            } else {
                whereCondition.append(" AND ((((c.complaint->>'data_accident')\\:\\:jsonb)->>'date_accident')\\:\\:timestamp BETWEEN '" + dateAccidentFrom + "'\\:\\:timestamp AND '" + dateAccidentTo + "'\\:\\:timestamp )");
            }
        } else if ((dateAccidentFrom != null) || (dateAccidentTo != null)) {
            LOGGER.debug("the date_accident_to and date_accident_from must be present together ");
            throw new BadParametersException("the date_accident_to and date_accident_from must be present together ", MessageCode.CLAIMS_1011);
        }

        if (clientId != null) {
            whereCondition.append(" AND (c.damaged->>'customer')\\:\\:jsonb->>'customer_id'  = '" + clientId + "' ");
        }

        if (clientName != null) {
            whereCondition.append(" AND (c.damaged->>'customer')\\:\\:jsonb->>'legal_name' ILIKE '%" + clientName + "%'  ");
        }

        if (plate != null) {
            whereCondition.append(" AND c.complaint->>'plate' ILIKE '%" + plate + "%' ");
        }

        if (locator != null) {
            whereCondition.append(" AND c.complaint->>'locator' ILIKE '%" + locator + "%' ");
        }

        if (practiceId != null) {
            whereCondition.append(" AND c.practice_id = " + practiceId + " ");
        }
        if (inEvidence != null) {
            whereCondition.append(" AND c.in_evidence = " + inEvidence + " ");
        }

        if (withContinuation != null) {
            whereCondition.append(" AND c.with_continuation = " + withContinuation + " ");
        }

        if (withReceptions != null) {
            whereCondition.append(" AND (c.theft->>'is_with_receptions')\\:\\:bool = " + withReceptions + " ");
        }
        whereCondition.append(" AND (c.complaint->>'data_accident')\\:\\:jsonb->>'type_accident' = 'FURTO_TOTALE'");
        if (company != null)
            whereCondition.append(" AND ((c.damaged->>'insurance_company')\\:\\:jsonb->>'tpl')\\:\\:jsonb->>'company' ILIKE '%" + company + "%' ");
        if (status != null) {
            whereCondition.append(" AND c.status IN (:statusSet) AND c.status <> 'DRAFT'");
        } else {
            whereCondition.append(" AND c.status <> 'DRAFT'");
        }
        if (flow != null) {
            whereCondition.append(" AND c.type IN (:flowSet) ");
        }
        sql.append(whereCondition);
        Query query = em.createNativeQuery(sql.toString());

        if (flow != null)
            query.setParameter("flowSet", flow);

        if (status != null)
            query.setParameter("statusSet", status);

        return (BigInteger) query.getSingleResult();

    }

    public List<ClaimsEntity> findClaimsForPaginationOnlyTheft(Boolean inEvidence, Long practiceId, List<String> flow, String clientId, String plate, List<String> status, String locator, String dateAccidentFrom, String dateAccidentTo, Boolean asc, String orderBy, Integer page, Integer pageSize, Boolean withContinuation, String company, Boolean withReceptions, String clientName) {
        Map<String, String> mappOrder = new HashMap<>();
        mappOrder.put("type", " c.type ");
        mappOrder.put("practice_id", " c.practice_id ");
        mappOrder.put("date_accident", " (c.complaint->>'data_accident')\\:\\:jsonb->>'date_accident' ");
        mappOrder.put("plate", " c.complaint->>'plate' ");
        mappOrder.put("created_at", " c.created_at ");
        mappOrder.put("type_accident", " (c.complaint->>'data_accident')\\:\\:jsonb->>'type_accident' ");
        mappOrder.put("locator", " c.complaint->>'locator' ");

        StringBuilder sql = new StringBuilder("SELECT * FROM claims c ");
        StringBuilder whereCondition = new StringBuilder("WHERE 1=1 ");
        if (dateAccidentFrom != null && dateAccidentTo != null) {
            if (dateAccidentTo.compareTo(dateAccidentFrom) < 0) {
                LOGGER.debug("the date_accident_to is less than the date_accident_from");
                throw new BadParametersException("the date_accident_to is less than the date_accident_from", MessageCode.CLAIMS_1011);
            } else {
                whereCondition.append(" AND ((((c.complaint->>'data_accident')\\:\\:jsonb)->>'date_accident')\\:\\:timestamp BETWEEN '" + dateAccidentFrom + "'\\:\\:timestamp AND '" + dateAccidentTo + "'\\:\\:timestamp )");
            }
        } else if ((dateAccidentFrom != null) || (dateAccidentTo != null)) {
            LOGGER.debug("the date_accident_to and date_accident_from must be present together ");
            throw new BadParametersException("the date_accident_to and date_accident_from must be present together ", MessageCode.CLAIMS_1011);
        }

        if (clientId != null) {
            whereCondition.append(" AND (c.damaged->>'customer')\\:\\:jsonb->>'customer_id' = '" + clientId + "' ");
        }

        if (clientName != null) {
            whereCondition.append(" AND (c.damaged->>'customer')\\:\\:jsonb->>'legal_name' ILIKE '%" + clientName + "%'  ");
        }

        if (plate != null) {
            whereCondition.append(" AND c.complaint->>'plate' ILIKE '%" + plate + "%' ");
        }

        if (locator != null) {
            whereCondition.append(" AND c.complaint->>'locator' ILIKE '%" + locator + "%' ");
        }

        if (practiceId != null) {
            whereCondition.append(" AND c.practice_id = " + practiceId + " ");
        }
        if (inEvidence != null) {
            whereCondition.append(" AND c.in_evidence = " + inEvidence + " ");
        }

        if (withContinuation != null) {
            whereCondition.append(" AND c.with_continuation = " + withContinuation + " ");
        }

        if (withReceptions != null) {
            whereCondition.append(" AND (c.theft->>'is_with_receptions')\\:\\:bool = " + withReceptions + " ");
        }

        whereCondition.append(" AND (c.complaint->>'data_accident')\\:\\:jsonb->>'type_accident' = 'FURTO_TOTALE'");

        if (company != null)
            whereCondition.append(" AND ((c.damaged->>'insurance_company')\\:\\:jsonb->>'tpl')\\:\\:jsonb->>'company' ILIKE '%" + company + "%' ");
        if (status != null) {
            whereCondition.append(" AND c.status IN (:statusSet) AND c.status <> 'DRAFT'");
        } else {
            whereCondition.append(" AND c.status <> 'DRAFT'");
        }
        if (flow != null) {
            whereCondition.append(" AND c.type IN (:flowSet) ");
        }
        sql.append(whereCondition);
        String fieldToOrder = null;
        if (orderBy != null)
            fieldToOrder = mappOrder.get(orderBy);
        if (fieldToOrder != null && asc != null) {
            if (asc) {
                sql.append(" ORDER BY " + fieldToOrder + " ASC");
            } else {
                sql.append(" ORDER BY " + fieldToOrder + " DESC");
            }

        } else if (fieldToOrder != null) {
            sql.append(" ORDER BY " + fieldToOrder + " DESC");
        }
        sql.append(" LIMIT " + pageSize + " OFFSET " + (page - 1) * pageSize);
        Query query = em.createNativeQuery(sql.toString(), ClaimsEntity.class);

        if (flow != null)
            query.setParameter("flowSet", flow);
        if (status != null)
            query.setParameter("statusSet", status);

        return query.getResultList();
    }


    public List<ClaimsEntity> findClaimsForPaginationAuthority(String plate, String chassis, Integer page, Integer pageSize, boolean isFirstCheck) {
        StringBuilder withCondition = new StringBuilder("WITH AUTH AS (SELECT id, jsonb_array_elements(authority) AS authority FROM claims ) ");
        StringBuilder sql = new StringBuilder("SELECT * FROM claims c ");
        StringBuilder whereCondition = new StringBuilder(" WHERE 1=1 ");

        if (Util.isNotEmpty(plate)) {
            whereCondition.append(" AND (c.damaged->>'vehicle')\\:\\:jsonb->>'license_plate'  = '" + plate + "' ");
        }

        if (Util.isNotEmpty(chassis) && whereCondition.indexOf("AND") > 1) {
            whereCondition.append(" OR (c.damaged->>'vehicle')\\:\\:jsonb->>'chassis_number' = '" + chassis + "' ");
        } else if (Util.isNotEmpty(chassis)) {
            whereCondition.append(" AND (c.damaged->>'vehicle')\\:\\:jsonb->>'chassis_number' = '" + chassis + "' ");
        }

        if(!isFirstCheck){
            //VERIFICO LAVORAZIONI PREGRESSE E PENDING
            whereCondition = this.setGoLivePendingWorkingConditions(whereCondition);
            sql = withCondition.append(sql);
            whereCondition = this.setGoLiveWhereConditions(whereCondition);
        }

        sql.append(whereCondition);

        sql.append(" LIMIT " + pageSize + " OFFSET " + (page - 1) * pageSize);
        Query query = em.createNativeQuery(sql.toString(), ClaimsEntity.class);
        query = this.setGoLiveParameters(query, isFirstCheck);
        return query.getResultList();
    }


    public BigInteger countClaimsForPaginationAuthority(String plate, String chassis, boolean isFirstCheck) {
        StringBuilder withCondition = new StringBuilder("WITH AUTH AS (SELECT id, jsonb_array_elements(authority) AS authority FROM claims ) ");
        StringBuilder sql = new StringBuilder("SELECT COUNT (*) FROM claims c ");
        StringBuilder whereCondition = new StringBuilder("WHERE 1=1 ");

        if (Util.isNotEmpty(plate)) {
            whereCondition.append(" AND (c.damaged->>'vehicle')\\:\\:jsonb->>'license_plate'  = '" + plate + "' ");
        }

        if (Util.isNotEmpty(chassis) && whereCondition.indexOf("AND") > 1) {
            whereCondition.append(" OR (c.damaged->>'vehicle')\\:\\:jsonb->>'chassis_number' = '" + chassis + "' ");
        } else if (Util.isNotEmpty(chassis)) {
            whereCondition.append(" AND (c.damaged->>'vehicle')\\:\\:jsonb->>'chassis_number' = '" + chassis + "' ");
        }

        if(!isFirstCheck){
            //VERIFICO LAVORAZIONI PREGRESSE E PENDING
            whereCondition = this.setGoLivePendingWorkingConditions(whereCondition);
            sql = withCondition.append(sql);
            whereCondition = this.setGoLiveWhereConditions(whereCondition);
        }
        sql.append(whereCondition);
        Query query = em.createNativeQuery(sql.toString());
        query = this.setGoLiveParameters(query,isFirstCheck);

        return (BigInteger) query.getSingleResult();

    }

    private StringBuilder setGoLivePendingWorkingConditions (StringBuilder whereCondition){
        return whereCondition.append(" AND c.id NOT IN (SELECT AUTH.id FROM AUTH where AUTH.authority->>'authority_working_id' is null) " +
                "AND ((c.damaged->>'vehicle')\\:\\:jsonb->>'license_plate')\\:\\:varchar NOT IN (SELECT damaged_drivers_plate FROM claims_pending) ");
    }

    private StringBuilder setGoLiveWhereConditions (StringBuilder whereCondition){
        if(!goLiveStrategyService.checkIfStepThree()){
            whereCondition.append(" AND (");
            whereCondition.append(" (c.damaged->>'customer')\\:\\:jsonb->>'customer_id' IN ( :customerList ) ");
            if(goLiveStrategyService.checkIfStepOne() || goLiveStrategyService.checkIfStepTwo() ){
                whereCondition.append(" OR (c.complaint->>'data_accident')\\:\\:jsonb->>'type_accident' IN ( :typeAccidentList ) ");
            }
            whereCondition.append(" ) ");
        }

        return whereCondition;
    }

    private Query setGoLiveParameters (Query query, Boolean isFirstCheck){
        if(query!=null && !isFirstCheck && !goLiveStrategyService.checkIfStepThree()){
            query.setParameter("customerList", goLiveStrategyService.getCustomerIdListToFilterStepMinusOneAndHigher());
            if(goLiveStrategyService.checkIfStepOne() || goLiveStrategyService.checkIfStepTwo()){
                query.setParameter("typeAccidentList", goLiveStrategyService.getTypeAccidentListToFilterStepTwoOne());
            }
        }
        return query;
    }

    public List<ClaimsEntity> findClaimsByDateAndVeichleType() {
        StringBuilder sql = new StringBuilder("SELECT * FROM claims ");
        StringBuilder whereCondition = new StringBuilder("WHERE " +
                "((now()\\:\\:date - ((claims.complaint->>'data_accident')\\:\\:jsonb->>'date_accident')\\:\\:date > 15) AND (claims.damaged->>'vehicle')\\:\\:jsonb->>'nature' LIKE 'MOTOR_CYCLE' AND claims.theft->>'first_key_reception' is null AND claims.theft->>'second_key_reception' is null) OR " +
                "((now()\\:\\:date - ((claims.complaint->>'data_accident')\\:\\:jsonb->>'date_accident')\\:\\:date > 30) AND claims.theft->>'first_key_reception' is null AND claims.theft->>'second_key_reception' is null AND ((claims.damaged->>'vehicle')\\:\\:jsonb->>'nature' LIKE 'MOTOR_VEHICLE' OR " +
                "(claims.damaged->>'vehicle')\\:\\:jsonb->>'nature' LIKE 'VAN'))");
        sql.append(whereCondition);
        Query query = em.createNativeQuery(sql.toString(), ClaimsEntity.class);
        return query.getResultList();
    }

    public List<ClaimsEntity> findClaimsByDateAndVeichleTypeChangeStatus() {
        StringBuilder sql = new StringBuilder("SELECT * FROM claims ");
        StringBuilder whereCondition = new StringBuilder(" WHERE " +
                "((now()\\:\\:date - ((claims.complaint->>'data_accident')\\:\\:jsonb->>'date_accident')\\:\\:date > 30) AND (claims.damaged->>'vehicle')\\:\\:jsonb->>'nature' LIKE 'MOTOR_CYCLE' AND claims.theft->>'first_key_reception' is null AND claims.theft->>'second_key_reception' is null) OR " +
                "((now()\\:\\:date - ((claims.complaint->>'data_accident')\\:\\:jsonb->>'date_accident')\\:\\:date > 60) AND claims.theft->>'first_key_reception' is null AND claims.theft->>'second_key_reception' is null AND ((claims.damaged->>'vehicle')\\:\\:jsonb->>'nature' LIKE 'MOTOR_VEHICLE' OR " +
                "(claims.damaged->>'vehicle')\\:\\:jsonb->>'nature' LIKE 'VAN'))");
        sql.append(whereCondition);
        Query query = em.createNativeQuery(sql.toString(), ClaimsEntity.class);
        return query.getResultList();
    }

    public List<ClaimsEntity> findTheftForLossPossession() {
        StringBuilder sql = new StringBuilder("SELECT * FROM claims ");
        StringBuilder whereCondition = new StringBuilder(" WHERE " +
                "(((claims.complaint->>'data_accident')\\:\\:jsonb->>'type_accident')  = 'FURTO_TOTALE' ) AND "+
                "(((now()\\:\\:date - ((claims.complaint->>'data_accident')\\:\\:jsonb->>'date_accident')\\:\\:date > 30) AND (claims.damaged->>'vehicle')\\:\\:jsonb->>'nature' LIKE 'MOTOR_CYCLE' ) OR " +
                "((now()\\:\\:date - ((claims.complaint->>'data_accident')\\:\\:jsonb->>'date_accident')\\:\\:date > 60) AND ((claims.damaged->>'vehicle')\\:\\:jsonb->>'nature' LIKE 'MOTOR_VEHICLE' OR " +
                "(claims.damaged->>'vehicle')\\:\\:jsonb->>'nature' LIKE 'VAN')))");
        sql.append(whereCondition);
        System.out.println(sql);
        Query query = em.createNativeQuery(sql.toString(), ClaimsEntity.class);
        return query.getResultList();
    }

    public List<Object[]> getStatsClaimsEvidence() {
        StringBuilder sql = new StringBuilder("SELECT claims.type, lower(claims.status), count(DISTINCT claims.id) AS counter, claims.in_evidence, claims.with_continuation FROM claims claims ");
        StringBuilder whereCondition = new StringBuilder(" WHERE claims.status != 'DRAFT' " +
                "AND claims.status != 'DEMOLITION' " +
                "AND claims.status != 'WAIT_LOST_POSSESSION' " +
                "AND claims.status != 'LOST_POSSESSION' " +
                "AND claims.status != 'WAIT_TO_RETURN_POSSESSION' " +
                "AND claims.status != 'RETURN_TO_POSSESSION' " +
                "AND claims.status != 'TO_REREGISTER' " +
                "AND claims.status != 'TO_WORK' " +
                "AND claims.status != 'BOOK_DUPLICATION' " +
                "AND claims.status != 'STAMP' " +
                "AND claims.status != 'DEMOLITION' " +
                "AND claims.status != 'RECEPTIONS_TO_BE_CONFIRMED'" +
                "AND claims.status != 'WAITING_FOR_AUTHORITY' AND claims.type != 'NO'  ");
        sql.append(whereCondition);
        sql.append(" GROUP BY claims.status, claims.type ,claims.in_evidence, claims.with_continuation");
        Query query = em.createNativeQuery(sql.toString());
        List<Object[]> resultList = query.getResultList();
        return resultList;
    }

    public List<Object[]> getStatsClaimsEvidenceWithStatusWaitingForAuthority() {
        StringBuilder withCondition = new StringBuilder("WITH AUTH AS (SELECT id, jsonb_array_elements(authority) AS authority FROM claims )");
        StringBuilder sql = new StringBuilder("SELECT claims.type, lower(claims.status), count(DISTINCT claims.id) AS counter, claims.in_evidence, claims.with_continuation FROM claims claims ");
        StringBuilder whereCondition = new StringBuilder(" WHERE claims.type != 'NO' AND claims.status = 'WAITING_FOR_AUTHORITY' AND claims.id NOT IN (SELECT claims.id FROM claims LEFT JOIN AUTH a USING(id) " +
                "WHERE a.authority->'is_invoiced' = 'true' )  ");
        sql.append(whereCondition);
        sql.append(" GROUP BY claims.status, claims.type ,claims.in_evidence, claims.with_continuation");
        withCondition.append(sql);
        Query query = em.createNativeQuery(withCondition.toString());
        List<Object[]> resultList = query.getResultList();
        return resultList;
    }

    public List<Object[]> getStatsClaimsEvidenceWithStatusWaitingForNumberSx() {
        StringBuilder sql = new StringBuilder("SELECT claims.type, lower(claims.status), count(DISTINCT claims.id) AS counter, claims.in_evidence, claims.with_continuation FROM claims claims ");
        StringBuilder whereCondition = new StringBuilder(" WHERE claims.type = 'FUL' AND claims.status = 'WAITING_FOR_AUTHORITY' AND ((claims.complaint->>'from_company')\\:\\:jsonb IS NULL OR (claims.complaint->>'from_company')\\:\\:jsonb->>'number_sx' IS NULL OR (claims.complaint->>'from_company')\\:\\:jsonb->>'number_sx' = '') ");
        sql.append(whereCondition);
        sql.append(" GROUP BY claims.status, claims.type ,claims.in_evidence, claims.with_continuation");
        Query query = em.createNativeQuery(sql.toString());
        List<Object[]> resultList = query.getResultList();
        return resultList;
    }

    public List<Object[]> getStatsClaimsEvidencePoVariation() {
        StringBuilder sql = new StringBuilder("SELECT claims.type, count(DISTINCT claims.id) AS counter, claims.in_evidence, claims.with_continuation FROM claims claims ");
        StringBuilder whereCondition = new StringBuilder(" WHERE claims.status != 'DRAFT' " +
                "AND claims.status != 'DEMOLITION' " +
                "AND claims.status != 'WAIT_LOST_POSSESSION' " +
                "AND claims.status != 'LOST_POSSESSION' " +
                "AND claims.status != 'WAIT_TO_RETURN_POSSESSION' " +
                "AND claims.status != 'RETURN_TO_POSSESSION' " +
                "AND claims.status != 'TO_REREGISTER' " +
                "AND claims.status != 'TO_WORK' " +
                "AND claims.status != 'BOOK_DUPLICATION' " +
                "AND claims.status != 'STAMP' " +
                "AND claims.status != 'DEMOLITION' " +
                "AND claims.status != 'RECEPTIONS_TO_BE_CONFIRMED' AND claims.type != 'NO' AND po_variation = true");
        sql.append(whereCondition);
        sql.append(" GROUP BY claims.type ,claims.in_evidence, claims.with_continuation");
        Query query = em.createNativeQuery(sql.toString());
        List<Object[]> resultList = query.getResultList();
        return resultList;
    }

    public List<Object[]> getClaimsStatsTotalCounter(Boolean inEvidence, Boolean withContinuation) {
        StringBuilder sql = new StringBuilder("SELECT  c.type as flow , count(*) as cont from claims c ");
        StringBuilder whereCondition = new StringBuilder(" where  c.type in ( 'FUL' ,'FCM', 'FNI') ");
        if (inEvidence != null) {
            whereCondition.append(" AND c.in_evidence = " + inEvidence + " ");
        }
        if (withContinuation != null) {
            whereCondition.append(" AND c.with_continuation = " + withContinuation + " ");
        }
        sql.append(whereCondition);
        sql.append(" GROUP BY  c.type ORDER BY c.type ");
        System.out.println(sql.toString());
        Query query = em.createNativeQuery(sql.toString());
        List<Object[]> resultList = query.getResultList();
        return resultList;
    }

    private StringBuffer getFrom() {
        return new StringBuffer(" FROM claims c ");
    }

    private StringBuffer getWhere() {
        return new StringBuffer(" WHERE 1 = 1 ");
    }

    /*
     * se withContinuation è
     *      true =>
     *          devo prendere tutti i record che hanno almeno un record in
     *          anti_theft_request il cui campo validate è a false
     *      false =>
     *          devo prendere tutti i record che non hanno nessun record
     *          in anti_theft_request con validate a false
     */
    private StringBuffer addWithContinuationFilter(Boolean withContinuation, StringBuffer where) {
        if (withContinuation != null) {
            StringBuffer atrSql;
            StringBuffer atrSelect = new StringBuffer(" SELECT claim_id ");
            StringBuffer atrFrom = new StringBuffer(" FROM anti_theft_request ");
            StringBuffer atrWhere = new StringBuffer(" WHERE 1=1 ");
            atrWhere.append(" AND (crash_report->>'validate')\\:\\:boolean = false");

            atrSql = new StringBuffer();
            atrSql
                .append("(")
                .append(atrSelect)
                .append(atrFrom)
                .append(atrWhere)
                .append(")")
            ;

            if( withContinuation ) {
                where
                    .append(" AND ( ")
                    .append("   c.with_continuation = true")
                    .append("   OR c.id IN " + atrSql)
                    .append(" ) ")
                ;
            } else {
                where
                    .append(" AND c.id NOT IN " + atrSql)
                    .append(" AND c.with_continuation = false")
                ;
            }
        }
        return where;
    }

    //INIZIO VERSIONE DASHBOARD 24_11_20
    public List<Object[]> getClaimsStatsBasedStatus(Boolean inEvidence, Boolean withContinuation) {
        StringBuffer sql;
        StringBuffer select = new StringBuffer(" SELECT c.status, c.type, COUNT(*) ");
        StringBuffer from = this.getFrom();
        StringBuffer where = this.getWhere();
        StringBuffer groupBy = new StringBuffer(" GROUP BY c.status, c.type ");
        /*
         * lo stato è: WAITING_FOR_VALIDATION, INCOMPLETE, WAITING_FOR_REFUND, CLOSED_PARTIAL_REFUND, TO_SEND_TO_CUSTOMER
         * oppure:
         *      lo stato è: TO_ENTRUST e:
         *          il tipo è FUL, e contemporaneamente il type_accident è RC_ATTIVA, RC_CONCORSUALE, CARD_ATTIVA_FIRMA_SINGOLA, CARD_CONCORSUALE_FIRMA_SINGOLA, CARD_ATTIVA_DOPPIA_FIRMA, CARD_CONCORSUALE_DOPPIA_FIRMA
         *          oppure:
         *          il tipo è FNI e FCM
         */
        where
            .append("AND (")
            .append("   c.status IN ('WAITING_FOR_VALIDATION','INCOMPLETE', 'WAITING_FOR_REFUND', 'CLOSED_PARTIAL_REFUND', 'TO_SEND_TO_CUSTOMER') ")
            .append("   OR (")
            .append("       c.status = 'TO_ENTRUST'")
            .append("       AND (")
            .append("           (")
            .append("               c.type = 'FUL' ")
            .append("               AND ((c.complaint->>'data_accident')\\:\\:jsonb)->>'type_accident' IN ('RC_ATTIVA', 'RC_CONCORSUALE', 'CARD_ATTIVA_FIRMA_SINGOLA', 'CARD_CONCORSUALE_FIRMA_SINGOLA', 'CARD_ATTIVA_DOPPIA_FIRMA', 'CARD_CONCORSUALE_DOPPIA_FIRMA')")
            .append("           )")
            .append("           OR c.type IN ('FNI', 'FCM')")
            .append("       )")
            .append("   )")
            .append(")")
        ;

        if (inEvidence != null) {
            where.append(" AND c.in_evidence = " + inEvidence);
        }

        where = this.addWithContinuationFilter(withContinuation, where);

        sql = select
            .append(from)
            .append(where)
            .append(groupBy)
        ;

        Query query = em.createNativeQuery(sql.toString());
        List<Object[]> resultList = query.getResultList();
        return resultList;
    }

    public List<Object[]> getStatsClaimWaitingForNumberSx(Boolean inEvidence, Boolean withContinuation) {
        StringBuffer sql;
        StringBuffer select = new StringBuffer(" SELECT  c.status, c.type, count(DISTINCT c.id) AS counter ");
        StringBuffer from = this.getFrom();
        StringBuffer where = this.getWhere();
        StringBuffer groupBy = new StringBuffer(" GROUP BY c.status, c.type ");
        where
            .append(" AND c.type = 'FUL' ")
            .append(" AND ((c.complaint->>'data_accident')\\:\\:jsonb)->>'type_accident' IN ('CARD_NON_VERIFICABILE_FIRMA_SINGOLA','CARD_ATTIVA_FIRMA_SINGOLA','CARD_PASSIVA_FIRMA_SINGOLA','CARD_CONCORSUALE_FIRMA_SINGOLA','CARD_NON_VERIFICABILE_DOPPIA_FIRMA','CARD_ATTIVA_DOPPIA_FIRMA','CARD_PASSIVA_DOPPIA_FIRMA', 'CARD_CONCORSUALE_DOPPIA_FIRMA','RC_NON_VERIFICABILE','RC_ATTIVA','RC_PASSIVA','RC_CONCORSUALE') ")
            .append(" AND c.status = 'WAITING_FOR_AUTHORITY' ")
            .append(" AND ( ")
            .append("       (c.complaint->>'from_company')\\:\\:jsonb IS NULL OR ")
            .append("       (c.complaint->>'from_company')\\:\\:jsonb->>'number_sx' IS NULL OR ")
            .append("       (c.complaint->>'from_company')\\:\\:jsonb->>'number_sx' = '' ")
            .append(" ) ")
        ;

        if (inEvidence != null) {
            where.append(" AND c.in_evidence = " + inEvidence + " ");
        }

        where = this.addWithContinuationFilter(withContinuation, where);
        sql = select
            .append(from)
            .append(where)
            .append(groupBy)
        ;

        Query query = em.createNativeQuery(sql.toString());
        List<Object[]> resultList = query.getResultList();

        return resultList;
    }

    public List<Object[]> getStatsClaimsWaitingForAuthority(Boolean inEvidence, Boolean withContinuation) {
        StringBuffer claimsSql = new StringBuffer();
        StringBuffer claimsSelect = new StringBuffer(" SELECT c.id ");
        StringBuffer claimsFrom = this.getFrom();
        claimsFrom.append(" LEFT JOIN auth a USING(id) ");
        StringBuffer claimsWhere = new StringBuffer(" WHERE a.authority->>'is_invoiced' = 'true' ");
        claimsWhere.append(" OR a.authority->>'event_type' = 'REJECT' ");
        claimsSql
            .append(claimsSelect)
            .append(claimsFrom)
            .append(claimsWhere)
        ;

        StringBuffer sql;
        StringBuffer withAuthority = new StringBuffer(" WITH AUTH AS (SELECT id, jsonb_array_elements(authority) AS authority FROM claims ) ");
        StringBuffer select = new StringBuffer(" SELECT c.status, c.type, count(DISTINCT c.id) AS counter ");
        StringBuffer from = this.getFrom();
        StringBuffer where = this.getWhere();
        StringBuffer groupBy = new StringBuffer(" GROUP BY c.id, c.status, c.type ");
        where
            .append(" AND (")
            .append("    (")
            .append("        c.type = 'FUL' AND")
            .append("        ((c.complaint->>'data_accident')\\:\\:jsonb)->>'type_accident' IN ('RC_ATTIVA', 'RC_CONCORSUALE', 'CARD_ATTIVA_FIRMA_SINGOLA', 'CARD_CONCORSUALE_FIRMA_SINGOLA', 'CARD_ATTIVA_DOPPIA_FIRMA', 'CARD_CONCORSUALE_DOPPIA_FIRMA')")
            .append("    )")
            .append("    OR c.type IN ('FNI', 'FCM')")
            .append(" )")
            .append(" AND c.status = 'WAITING_FOR_AUTHORITY'")
            .append(" AND c.authority IS NOT NULL")
            .append(" AND jsonb_array_length(authority\\:\\:jsonb) <> 0")
            .append(" AND c.id NOT IN (")
            .append(claimsSql)
            .append(" )")
        ;

        where = this.addWithContinuationFilter(withContinuation, where);

        if (inEvidence != null) {
            where.append(" AND c.in_evidence = " + inEvidence);
        }

        sql = withAuthority
            .append(select)
            .append(from)
            .append(where)
            .append(groupBy)
        ;

        Query query = em.createNativeQuery(sql.toString());
        List<Object[]> resultList = query.getResultList();

        return resultList;
    }


    //VARIAZIONE PO
    public List<Object[]> getStatsClaimsPoVariation(Boolean inEvidence, Boolean withContinuation) {
        StringBuffer sql;
        StringBuffer select = new StringBuffer(" SELECT c.status, c.type, COUNT(*) ");
        StringBuffer from = this.getFrom();
        StringBuffer where = this.getWhere();
        StringBuffer groupBy = new StringBuffer(" GROUP BY c.status, c.type ");

        where
            .append(" AND c.po_variation = true ")
            .append(" AND  ( ")
            .append("       ( ")
            .append("           c.type = 'FUL' ")
            .append("           AND ((c.complaint->>'data_accident')\\:\\:jsonb)->>'type_accident' IN ('RC_ATTIVA', 'RC_CONCORSUALE', 'CARD_ATTIVA_FIRMA_SINGOLA', 'CARD_CONCORSUALE_FIRMA_SINGOLA', 'CARD_ATTIVA_DOPPIA_FIRMA', 'CARD_CONCORSUALE_DOPPIA_FIRMA') ")
            .append("       ) ")
            .append("       OR c.type IN ('FNI', 'FCM') ")
            .append(" ) ")
        ;


        if (inEvidence != null) {
            where.append(" AND c.in_evidence = " + inEvidence);
        }

        where = this.addWithContinuationFilter(withContinuation, where);

        sql = select
            .append(from)
            .append(where)
            .append(groupBy)
        ;

        Query query = em.createNativeQuery(sql.toString());
        List<Object[]> resultList = query.getResultList();
        return resultList;
    }

    //FINE VERSIONE DASHBOARD 24_11_20



    @Modifying
    @Transactional
    public void updateClaimsEvidence() {

        StringBuilder sql = new StringBuilder("UPDATE claims SET in_evidence = true WHERE id IN " +
                " (SELECT claims.id From claims claims inner join configure_evidence ce ON claims.status = ce.practical_state_en\n" +
                "WHERE claims.in_evidence = false\n" +
                "AND ce.is_active=true AND ((claims.historical IS NOT NULL AND now()\\:\\:DATE - (claims.historical->(jsonb_array_length(claims.historical) -1)->>'update_at')\\:\\:DATE >= ce.days_of_stay_in_the_state )\n" +
                "OR (claims.historical IS NULL AND now()\\:\\:DATE - claims.created_at\\:\\:DATE >= ce.days_of_stay_in_the_state) )\n" +
                ")");

        Query query = em.createNativeQuery(sql.toString());
        query.executeUpdate();
    }


    public List<ClaimsEntity> getClaimsToAutomaticEntrust() {
        StringBuilder sql = new StringBuilder("SELECT * FROM claims c ");
        StringBuilder whereConditions = new StringBuilder(" WHERE c.status = 'TO_ENTRUST' AND ( ((c.complaint->>'entrusted')\\:\\:jsonb) is null OR (((c.complaint->>'entrusted')\\:\\:jsonb)->>'auto_entrust') is null  OR (((c.complaint->>'entrusted')\\:\\:jsonb)->>'auto_entrust')\\:\\:bool = false )");
        sql.append(whereConditions);
        Query query = em.createNativeQuery(sql.toString(), ClaimsEntity.class);
        return query.getResultList();
    }

    public ClaimsStats findClaimsStats() {

        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Tuple> claimsCriteriaQuery = criteriaBuilder.createTupleQuery();

        Subquery<Long> countWaitingForValidationSubQuery = claimsCriteriaQuery.subquery(Long.class);
        Root<ClaimsEntity> subQueryWaitingForValidation = countWaitingForValidationSubQuery.from(ClaimsEntity.class);
        countWaitingForValidationSubQuery.select(criteriaBuilder.count(subQueryWaitingForValidation));
        countWaitingForValidationSubQuery.where(criteriaBuilder.equal(subQueryWaitingForValidation.get("status"), ClaimsStatusEnum.WAITING_FOR_VALIDATION));

        Subquery<Long> countWaitingForAuthoritySubQuery = claimsCriteriaQuery.subquery(Long.class);
        Root<ClaimsEntity> subQueryWaitingForAuthority = countWaitingForAuthoritySubQuery.from(ClaimsEntity.class);
        countWaitingForAuthoritySubQuery.select(criteriaBuilder.count(subQueryWaitingForAuthority));
        countWaitingForAuthoritySubQuery.where(criteriaBuilder.equal(subQueryWaitingForAuthority.get("status"), ClaimsStatusEnum.WAITING_FOR_AUTHORITY));

        Subquery<Long> countDraftSubQuery = claimsCriteriaQuery.subquery(Long.class);
        Root<ClaimsEntity> subQueryDraft = countDraftSubQuery.from(ClaimsEntity.class);
        countDraftSubQuery.select(criteriaBuilder.count(subQueryDraft));
        countDraftSubQuery.where(criteriaBuilder.equal(subQueryDraft.get("status"), ClaimsStatusEnum.DRAFT));

        Subquery<Long> countDeletedSubQuery = claimsCriteriaQuery.subquery(Long.class);
        Root<ClaimsEntity> subQueryDeleted = countDeletedSubQuery.from(ClaimsEntity.class);
        countDeletedSubQuery.select(criteriaBuilder.count(subQueryDeleted));
        countDeletedSubQuery.where(criteriaBuilder.equal(subQueryDeleted.get("status"), ClaimsStatusEnum.DELETED));

        Subquery<Long> countToEntrustSubQuery = claimsCriteriaQuery.subquery(Long.class);
        Root<ClaimsEntity> subQueryToEntrust = countToEntrustSubQuery.from(ClaimsEntity.class);
        countToEntrustSubQuery.select(criteriaBuilder.count(subQueryToEntrust));
        countToEntrustSubQuery.where(criteriaBuilder.equal(subQueryToEntrust.get("status"), ClaimsStatusEnum.TO_ENTRUST));

        Subquery<Long> countClosedSubQuery = claimsCriteriaQuery.subquery(Long.class);
        Root<ClaimsEntity> subQueryClosed = countClosedSubQuery.from(ClaimsEntity.class);
        countClosedSubQuery.select(criteriaBuilder.count(subQueryClosed));
        countClosedSubQuery.where(criteriaBuilder.equal(subQueryClosed.get("status"), ClaimsStatusEnum.CLOSED));

        Subquery<Long> countClosedPracticeSubQuery = claimsCriteriaQuery.subquery(Long.class);
        Root<ClaimsEntity> subQueryClosedPractice = countClosedPracticeSubQuery.from(ClaimsEntity.class);
        countClosedPracticeSubQuery.select(criteriaBuilder.count(subQueryClosedPractice));
        countClosedPracticeSubQuery.where(criteriaBuilder.equal(subQueryClosedPractice.get("status"), ClaimsStatusEnum.CLOSED_PRACTICE));

        Subquery<Long> countIncompleteSubQuery = claimsCriteriaQuery.subquery(Long.class);
        Root<ClaimsEntity> subQueryIncomplete = countIncompleteSubQuery.from(ClaimsEntity.class);
        countIncompleteSubQuery.select(criteriaBuilder.count(subQueryIncomplete));
        countIncompleteSubQuery.where(criteriaBuilder.equal(subQueryIncomplete.get("status"), ClaimsStatusEnum.INCOMPLETE));

        Subquery<Long> countEntrustedAtSubQuery = claimsCriteriaQuery.subquery(Long.class);
        Root<ClaimsEntity> subQueryEntrustedAt = countEntrustedAtSubQuery.from(ClaimsEntity.class);
        countEntrustedAtSubQuery.select(criteriaBuilder.count(subQueryEntrustedAt));
        countEntrustedAtSubQuery.where(criteriaBuilder.equal(subQueryEntrustedAt.get("status"), ClaimsStatusEnum.ENTRUSTED_AT));

        Subquery<Long> countRejectedSubQuery = claimsCriteriaQuery.subquery(Long.class);
        Root<ClaimsEntity> subQueryRejected = countRejectedSubQuery.from(ClaimsEntity.class);
        countRejectedSubQuery.select(criteriaBuilder.count(subQueryRejected));
        countRejectedSubQuery.where(criteriaBuilder.equal(subQueryRejected.get("status"), ClaimsStatusEnum.REJECTED));

        Subquery<Long> countManagedSubQuery = claimsCriteriaQuery.subquery(Long.class);
        Root<ClaimsEntity> subQueryManaged = countManagedSubQuery.from(ClaimsEntity.class);
        countManagedSubQuery.select(criteriaBuilder.count(subQueryManaged));
        countManagedSubQuery.where(criteriaBuilder.equal(subQueryManaged.get("status"), ClaimsStatusEnum.MANAGED));

        Subquery<Long> countProposedAcceptedSubQuery = claimsCriteriaQuery.subquery(Long.class);
        Root<ClaimsEntity> subQueryProposedAccepted = countProposedAcceptedSubQuery.from(ClaimsEntity.class);
        countProposedAcceptedSubQuery.select(criteriaBuilder.count(subQueryProposedAccepted));
        countProposedAcceptedSubQuery.where(criteriaBuilder.equal(subQueryProposedAccepted.get("status"), ClaimsStatusEnum.PROPOSED_ACCEPTED));

        Subquery<Long> countWaitingForRefundSubQuery = claimsCriteriaQuery.subquery(Long.class);
        Root<ClaimsEntity> subQueryWaitingForRefund = countWaitingForRefundSubQuery.from(ClaimsEntity.class);
        countWaitingForRefundSubQuery.select(criteriaBuilder.count(subQueryWaitingForRefund));
        countWaitingForRefundSubQuery.where(criteriaBuilder.equal(subQueryWaitingForRefund.get("status"), ClaimsStatusEnum.WAITING_FOR_REFUND));

        Subquery<Long> countKaskoSubQuery = claimsCriteriaQuery.subquery(Long.class);
        Root<ClaimsEntity> subQueryKasko = countKaskoSubQuery.from(ClaimsEntity.class);
        countKaskoSubQuery.select(criteriaBuilder.count(subQueryKasko));
        countKaskoSubQuery.where(criteriaBuilder.equal(subQueryKasko.get("status"), ClaimsStatusEnum.WAITING_FOR_VALIDATION));

        claimsCriteriaQuery.multiselect(countWaitingForValidationSubQuery.getSelection().alias("waiting_for_validation"),
                countWaitingForAuthoritySubQuery.getSelection().alias("waiting_for_authority"),
                countDraftSubQuery.getSelection().alias("draft"), countDeletedSubQuery.getSelection().alias("deleted"),
                countToEntrustSubQuery.getSelection().alias("to_entrust"), countClosedSubQuery.getSelection().alias("closed"),
                countClosedPracticeSubQuery.getSelection().alias("closed_practice"), countIncompleteSubQuery.getSelection().alias("incomplete"),
                countEntrustedAtSubQuery.getSelection().alias("entrusted_at"), countRejectedSubQuery.getSelection().alias("rejected"),
                countManagedSubQuery.getSelection().alias("managed"), countProposedAcceptedSubQuery.getSelection().alias("proposted_accepted"),
                countWaitingForRefundSubQuery.getSelection().alias("waiting_for_refund"), countKaskoSubQuery.getSelection().alias("kasko"));
        claimsCriteriaQuery.from(ClaimsEntity.class);


        Query sql = em.createQuery(claimsCriteriaQuery);

        List<Tuple> t = sql.getResultList();

        ClaimsStats stats = new ClaimsStats(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        if (t != null && !t.isEmpty())
        {
            stats.setWaitingForValidation(((Number) t.get(0).get("waiting_for_validation")).intValue());
            stats.setWaitingForauthority(((Number) t.get(0).get("waiting_for_authority")).intValue());
            stats.setDraft(((Number) t.get(0).get("draft")).intValue());
            stats.setDeleted(((Number) t.get(0).get("deleted")).intValue());
            stats.setToEntrust(((Number) t.get(0).get("to_entrust")).intValue());
            stats.setClosed(((Number) t.get(0).get("closed")).intValue());
            stats.setClosedPractice(((Number) t.get(0).get("closed_practice")).intValue());
            stats.setIncomplete(((Number) t.get(0).get("incomplete")).intValue());
            stats.setEntrustedAt(((Number) t.get(0).get("entrusted_at")).intValue());
            stats.setRejected(((Number) t.get(0).get("rejected")).intValue());
            stats.setManaged(((Number) t.get(0).get("managed")).intValue());
            stats.setProposedAccepted(((Number) t.get(0).get("proposed_accepted")).intValue());
            stats.setWaitingForRefund(((Number) t.get(0).get("waiting_for_refund")).intValue());
            stats.setKasko(((Number) t.get(0).get("kasko")).intValue());
        }


        return stats;
    }

    public List<ClaimsEntity> getClaimsToAttachAuthority(String authorityDossierId, String authorityWorkingId, List<String> claimsIdList) {
        String claimsIdString = "";
        for (String currentId : claimsIdList) {
            claimsIdString += "'" + currentId + "',";
        }

        if (!claimsIdString.equals("")) {
            claimsIdString = claimsIdString.substring(0, claimsIdString.length() - 1);
        }

        StringBuilder query = new StringBuilder("WITH AU AS ( SELECT id, jsonb_array_elements(authority) AS aut FROM claims c) ");
        query.append("SELECT * FROM claims c WHERE c.id IN (SELECT DISTINCT c.id " +
                "FROM claims c LEFT JOIN AU using(id) " +
                "WHERE ( aut IS NULL OR (aut->>'authority_dossier_id' <> '" + authorityDossierId + "' AND aut->>'authority_working_id' <> '" + authorityWorkingId + "') OR (aut->>'authority_dossier_id' = '" + authorityDossierId + "' AND aut->>'authority_working_id' <> '" + authorityWorkingId + "') OR (aut->>'authority_dossier_id' IS NULL AND aut->>'authority_working_id' IS NULL AND c.is_migrated=true)) AND c.id  IN  (" + claimsIdString + ")" +
                " AND c.id NOT IN (SELECT DISTINCT(c.id) FROM claims c LEFT JOIN AU using(id) WHERE aut->>'authority_dossier_id' = '" + authorityDossierId + "' AND aut->>'authority_working_id' = '" + authorityWorkingId + "' AND c.id IN  (" + claimsIdString + ")))");

        System.out.println(query);
        Query sql = em.createNativeQuery(query.toString(), ClaimsEntity.class);
        return sql.getResultList();
    }

    public List<ClaimsEntity> getClaimsToDetattachAuthority(String authorityDossierId, String authorityWorkingId, List<String> claimsIdList) {
        String claimsIdString = "";


        for (String currentId : claimsIdList) {
            claimsIdString += "'" + currentId + "',";
        }
        StringBuilder query = new StringBuilder("WITH AU AS ( SELECT id, jsonb_array_elements(authority) AS aut FROM claims c) ");
        if (!claimsIdString.equals("")) {
            claimsIdString = claimsIdString.substring(0, claimsIdString.length() - 1);
            query.append("SELECT * " +
                    "FROM claims c LEFT JOIN AU using(id) " +
                    "WHERE aut->>'authority_dossier_id' = '" + authorityDossierId + "' AND aut->>'authority_working_id' = '" + authorityWorkingId + "' AND c.id NOT IN  (" + claimsIdString + ")");
        } else {

            query.append("SELECT * " +
                    "FROM claims c LEFT JOIN AU using(id) " +
                    "WHERE aut->>'authority_dossier_id' = '" + authorityDossierId + "' AND aut->>'authority_working_id' = '" + authorityWorkingId + "'");
        }

        Query sql = em.createNativeQuery(query.toString(), ClaimsEntity.class);


        return sql.getResultList();
    }


    public Authority getAuthorityPracticeByWorkingId(String claimsId, String authorityWorkingId) {



        StringBuilder query = new StringBuilder("WITH AU AS ( SELECT id, jsonb_array_elements(authority) AS aut FROM claims c) ");

        query.append("SELECT aut->>'authority_dossier_id' as authority_dossier_id, " +
                "aut->>'authority_working_id' as authority_working_id," +
                "aut->>'working_number' as working_number,  " +
                "aut->>'authority_dossier_number' as authority_dossier_number,  " +
                "aut->>'total' as total, " +
                "aut->>'accepting_date' as accepting_date," +
                "aut->>'authorization_date' as authorization_date, " +
                "aut->>'rejection_date' as rejection_date, " +
                "aut->>'rejection' as rejection, " +
                "aut->>'working_created_at' as working_created_at, " +
                "aut->>'franchise_number' as franchise_number, " +
                "aut->>'is_wreck' as is_wreck, " +
                "aut->>'wreck_casual' as wreck_casual, " +
                "aut->>'event_date' as event_date, " +
                "aut->>'event_type' as event_type, " +
                "aut->>'po_details' as po_details " +

                "FROM claims c LEFT JOIN AU using(id) " +
                "WHERE aut->>'authority_working_id' = '" + authorityWorkingId + "' AND c.id = '" +claimsId+ "'");


        Query sql = em.createNativeQuery(query.toString(), "authority");


        return (Authority) sql.getSingleResult();
    }

    public List<ClaimsEntity> getClaimsToAssociateAuthority(String authorityDossierId, String authorityWorkingId, List<String> claimsIdList) {
        String claimsIdString = "";


        for (String currentId : claimsIdList) {
            claimsIdString += "'" + currentId + "',";
        }
        StringBuilder query = new StringBuilder("WITH AU AS ( SELECT id, jsonb_array_elements(authority) AS aut FROM claims c) ");
        if (!claimsIdString.equals("")) {
            claimsIdString = claimsIdString.substring(0, claimsIdString.length() - 1);
            query.append("SELECT * " +
                    "FROM claims c LEFT JOIN AU using(id) " +
                    "WHERE aut->>'authority_dossier_id' = '" + authorityDossierId + "' AND aut->>'authority_working_id' = '" + authorityWorkingId + "' AND c.id IN  (" + claimsIdString + ")");
        }
        Query sql = em.createNativeQuery(query.toString(), ClaimsEntity.class);

        return sql.getResultList();
    }

    public List<ClaimsEntity> getClaimsToCheckDuplicateAuthority(String authorityDossierId, String authorityWorkingId) {

        StringBuilder query = new StringBuilder("WITH AU AS ( SELECT id, jsonb_array_elements(authority) AS aut FROM claims c) ");

        query.append("SELECT * " +
                "FROM claims c LEFT JOIN AU using(id) " +
                "WHERE aut->>'authority_dossier_id' = '" + authorityDossierId + "' AND aut->>'authority_working_id' = '" + authorityWorkingId + "'");

        Query sql = em.createNativeQuery(query.toString(), ClaimsEntity.class);


        return sql.getResultList();
    }

    public List<ClaimsEntity> getClaimsAttachedToAuthorityPractice(String authorityDossierId, String authorityWorkingId, List<String> claimsList) {

        String claimsIdString = "";

        for (String currentId : claimsList) {
            claimsIdString += "'" + currentId + "',";
        }
        if (!claimsIdString.equals(""))
            claimsIdString = claimsIdString.substring(0, claimsIdString.length() - 1);
        StringBuilder query = new StringBuilder("WITH AU AS ( SELECT id, jsonb_array_elements(authority) AS aut FROM claims c) ");

        query.append("SELECT * " +
                "FROM claims c LEFT JOIN AU using(id) " +
                "WHERE aut->>'authority_dossier_id' = '" + authorityDossierId + "' AND aut->>'authority_working_id' = '" + authorityWorkingId + "' AND c.id IN (" + claimsIdString + ")");

        System.out.println(query);
        Query sql = em.createNativeQuery(query.toString(), ClaimsEntity.class);

        return sql.getResultList();
    }

    public BigInteger countClaimsToCheckDuplicateAuthority(String authorityDossierId, String authorityWorkingId) {

        StringBuilder query = new StringBuilder("WITH AU AS ( SELECT id, jsonb_array_elements(authority) AS aut FROM claims c) ");

        query.append("SELECT COUNT(*) " +
                "FROM claims c LEFT JOIN AU using(id) " +
                "WHERE aut->>'authority_dossier_id' = '" + authorityDossierId + "' AND aut->>'authority_working_id' = '" + authorityWorkingId + "'");

        Query sql = em.createNativeQuery(query.toString());

        return (BigInteger) sql.getSingleResult();
    }

    public List<ClaimsEntity> getOtherClaimsEntityWithSameAuthorization(String idClaim, String authorityDossier, String authorityWorking) {
        StringBuilder query = new StringBuilder("WITH AU AS ( SELECT id, jsonb_array_elements(authority) AS aut FROM claims c ) ");

        query.append("SELECT * " +
                "FROM claims c LEFT JOIN AU using(id) " +
                "WHERE aut->>'authority_dossier_id' = '" + authorityDossier + "' AND aut->>'authority_working_id' = '" + authorityWorking + "' AND c.id <> '" + idClaim + "'");

        Query sql = em.createNativeQuery(query.toString(), ClaimsEntity.class);

        return sql.getResultList();
    }

    public List<Object> findClaimNoteById(String claimId, String noteId) {
        String auxiliaryTable = "WITH NOTE AS (SELECT id, jsonb_array_elements(notes) AS note FROM claims WHERE id = '" + claimId + "' ) ";
        String select = " select (note ->> 'title') as title, (note ->> 'note_type') as note_type, (note ->> 'description') as description, (note ->> 'is_important') as is_important, (note ->> 'note_id') as note_id, (note ->> 'user_id') as user_id, (note ->> 'created_at') as created_at, (note ->> 'hidden') as hidden FROM claims c LEFT JOIN NOTE USING (id) ";
        StringBuilder queryBuilder = new StringBuilder(auxiliaryTable);
        queryBuilder.append(select);
        StringBuilder whereClauseBuilder = new StringBuilder(" WHERE 1=1 ");
        if (noteId != null && !noteId.isEmpty())
            whereClauseBuilder.append(" AND (note ->> 'note_id') = '" + noteId + "' ");
        queryBuilder.append(whereClauseBuilder);
        Query sql = em.createNativeQuery(queryBuilder.toString());
        List<Object> list = sql.getResultList();
        return list;
    }

    @Modifying
    @Transactional
    public void deleteClaimsInBozzaRange(Integer range) {
        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("DELETE FROM claims c WHERE c.status = 'DRAFT' ");
        //StringBuilder whereClauseBuilder = new StringBuilder(" WHERE 1=1 ");
        if (range != null)
            queryBuilder.append(" AND DATE_PART('month', c.update_at ) > " + range);
        Query sql = em.createNativeQuery(queryBuilder.toString());
        sql.executeUpdate();
    }

    public List<ClaimsEntity> getClaimsByDamagedVehiclePlateAndDateAccident(String plate, String status) {

        String select = " select * FROM claims c ";
        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append(select);
        StringBuilder whereClauseBuilder = new StringBuilder(" WHERE c.damaged\\:\\:text <> '{}'\\:\\:text AND (c.damaged->>'vehicle')\\:\\:text <> '{}'\\:\\:text AND " + " c.complaint\\:\\:text <> '{}'\\:\\:text AND (c.complaint->>'data_accident')\\:\\:text <> '{}'\\:\\:text ");
        if (plate != null && !plate.isEmpty())
            whereClauseBuilder.append(" AND UPPER((c.damaged->>'vehicle')\\:\\:jsonb->>'license_plate') = UPPER('" + plate + "') ");
        if (status != null && !status.isEmpty())
            whereClauseBuilder.append(" AND (c.status = UPPER('" + status + "') )");
        queryBuilder.append(whereClauseBuilder);
        Query sql = em.createNativeQuery(queryBuilder.toString(), ClaimsEntity.class);
        List<ClaimsEntity> list = sql.getResultList();
        return list;
    }

    public List<ClaimsEntity> getClaimsTheftStatus(String dateFrom, String dateTo) {

        StringBuilder sql = new StringBuilder("SELECT * FROM claims c ");
        StringBuilder whereClauseBuilder = new StringBuilder(" WHERE ((c.complaint)\\:\\:jsonb->>'data_accident')\\:\\:jsonb->>'type_accident'\\:\\:text = 'FURTO_TOTALE' ");

        if (dateFrom != null && dateTo != null)
            whereClauseBuilder.append(" AND c.created_at\\:\\:date BETWEEN '" + dateFrom + "'\\:\\:date AND '" + dateTo + "'\\:\\:date ");
        else {
            LOGGER.debug(MessageCode.CLAIMS_1043.value());
            throw new BadParametersException(MessageCode.CLAIMS_1043);
        }

        sql.append(whereClauseBuilder);
        Query query = em.createNativeQuery(sql.toString(), ClaimsEntity.class);

        return query.getResultList();
    }

    public List<ClaimsEntity> getAcclaimsClaimsEntity(Boolean inEvidence, Long practiceId, String flow, String clientId, String plate,
                                                      String status, String locator, String dateAccidentFrom,
                                                      String dateAccidentTo, String typeAccident, Boolean asc, String orderBy, int page, int pageSize, Boolean withContinuation, String company, Boolean withReceptions, String clientName) {

        List<String> typeAccidentList = new ArrayList<String>() {{
            add("RC_ATTIVA");
            add("RC_PASSIVA");
            add("RC_CONCORSUALE");
            add("RC_NON_VERIFICABILE");
        }};
        Map<String, String> mappOrder = new HashMap<>();
        mappOrder.put("type", " claims.type ");
        mappOrder.put("practice_id", " claims.practice_id ");
        mappOrder.put("date_accident", " (claims.complaint->>'data_accident')\\:\\:jsonb->>'date_accident' ");
        mappOrder.put("plate", " claims.complaint->>'plate' ");
        mappOrder.put("created_at", " claims.created_at ");
        mappOrder.put("type_accident", " (claims.complaint->>'data_accident')\\:\\:jsonb->>'type_accident' ");
        mappOrder.put("locator", " claims.complaint->>'locator' ");

        StringBuilder sql = new StringBuilder("SELECT * FROM claims claims ");
        StringBuilder whereCondition = new StringBuilder(" WHERE (claims.is_read_acclaims IS NULL OR claims.is_read_acclaims = false) AND claims.complaint->>'entrusted' IS NOT NULL AND " +
                " ((claims.complaint->>'entrusted')\\:\\:jsonb) ->>'type' ='LEGAL' AND " +
                " ( ((claims.complaint->>'entrusted')\\:\\:jsonb )->>'id_entrusted_to' = '" + acclaimsId + "' OR ((claims.complaint->>'entrusted')\\:\\:jsonb )->>'id_entrusted_to' = '"+ acclaimsSrId + "') ");

        if (dateAccidentFrom != null && dateAccidentTo != null) {
            if (dateAccidentTo.compareTo(dateAccidentFrom) < 0) {
                LOGGER.debug("the date_accident_to is less than the date_accident_from");
                throw new BadParametersException("the date_accident_to is less than the date_accident_from", MessageCode.CLAIMS_1011);
            } else {
                whereCondition.append(" AND ((((claims.complaint->>'data_accident')\\:\\:jsonb)->>'date_accident')\\:\\:timestamp BETWEEN '" + dateAccidentFrom + "'\\:\\:timestamp AND '" + dateAccidentTo + "'\\:\\:timestamp )");
            }
        } else if ((dateAccidentFrom != null) || (dateAccidentTo != null)) {
            LOGGER.debug("the date_accident_to and date_accident_from must be present together ");
            throw new BadParametersException("the date_accident_to and date_accident_from must be present together ", MessageCode.CLAIMS_1011);
        }

        if (Util.isNotEmpty(clientId)) {
            whereCondition.append(" AND (claims.damaged->>'customer')\\:\\:jsonb->>'customer_id' = '" + clientId + "' ");
        }

        if (Util.isNotEmpty(clientName)) {
            whereCondition.append(" AND (claims.damaged->>'customer')\\:\\:jsonb->>'legal_name' ILIKE '%" + clientName + "%'  ");
        }

        if (Util.isNotEmpty(plate)) {
            whereCondition.append(" AND claims.complaint->>'plate' ILIKE '%" + plate + "%' ");
        }

        if (Util.isNotEmpty(locator)) {
            whereCondition.append(" AND claims.complaint->>'locator' ILIKE '%" + locator + "%' ");
        }

        if (practiceId != null) {
            whereCondition.append(" AND claims.practice_id = " + practiceId + " ");
        }
        if (inEvidence != null) {
            whereCondition.append(" AND claims.in_evidence = " + inEvidence + " ");
        }

        if (withContinuation != null) {
            whereCondition.append(" AND claims.with_continuation =" + withContinuation + " ");
        }

        if (withReceptions != null) {
            whereCondition.append(" AND (claims.theft->>'is_with_receptions')\\:\\:bool =" + withReceptions + " ");
        }

        if (typeAccidentList != null) {
            whereCondition.append(" AND (claims.complaint->>'data_accident')\\:\\:jsonb->>'type_accident' IN (:typeAccidentSet) ");
        }

        if (Util.isNotEmpty(company))
            whereCondition.append(" AND ((claims.damaged->>'insurance_company')\\:\\:jsonb->>'tpl')\\:\\:jsonb->>'company' ILIKE '%" + company + "%' ");

        if (status != null) {
            whereCondition.append(" AND claims.status IN (:statusSet) ");
        }

        if (flow != null) {
            whereCondition.append(" AND claims.type IN (:flowSet) ");
        }

        sql.append(whereCondition);

        String fieldToOrder = null;
        if (orderBy != null)
            fieldToOrder = mappOrder.get(orderBy);
        if (fieldToOrder != null && asc != null) {
            if (asc) {
                sql.append(" ORDER BY " + fieldToOrder + " ASC");
            } else {
                sql.append(" ORDER BY " + fieldToOrder + " DESC");
            }

        } else if (fieldToOrder != null) {
            sql.append(" ORDER BY " + fieldToOrder + " DESC");
        }
        sql.append(" LIMIT " + pageSize + " OFFSET " + (page - 1) * pageSize);
        Query query = em.createNativeQuery(sql.toString(), ClaimsEntity.class);
        if (typeAccidentList != null)
            query.setParameter("typeAccidentSet", typeAccidentList);
        if (flow != null)
            query.setParameter("flowSet", flow);
        if (status != null)
            query.setParameter("statusSet", status);
        return query.getResultList();
    }


    public BigInteger countClaimsForAcclaimsPagination(Boolean inEvidence, Long practiceId, List<String> flow, String clientId, String plate, List<String> status, String locator, String dateAccidentFrom, String dateAccidentTo, List<String> typeAccident, Boolean asc, String orderBy, Integer page, Integer pageSize, Boolean withContinuation, String company, Boolean withReceptions, String clientName) {

        StringBuilder sql = new StringBuilder("SELECT COUNT (*) FROM claims claims ");
        StringBuilder whereCondition = new StringBuilder(" WHERE (claims.is_read_acclaims IS NULL OR claims.is_read_acclaims = false) AND claims.complaint->>'entrusted' IS NOT NULL AND " +
                " ((claims.complaint->>'entrusted')\\:\\:jsonb) ->>'type' ='LEGAL' AND " +
                " ( ((claims.complaint->>'entrusted')\\:\\:jsonb )->>'id_entrusted_to' = '"+ acclaimsId  +"' OR ((claims.complaint->>'entrusted')\\:\\:jsonb )->>'id_entrusted_to' = '"+ acclaimsSrId +"') ");

        if (dateAccidentFrom != null && dateAccidentTo != null) {
            if (dateAccidentTo.compareTo(dateAccidentFrom) < 0) {
                LOGGER.debug("the date_accident_to is less than the date_accident_from");
                throw new BadParametersException("the date_accident_to is less than the date_accident_from", MessageCode.CLAIMS_1011);
            } else {
                whereCondition.append(" AND ((((claims.complaint->>'data_accident')\\:\\:jsonb)->>'date_accident')\\:\\:timestamp BETWEEN '" + dateAccidentFrom + "'\\:\\:timestamp AND '" + dateAccidentTo + "'\\:\\:timestamp )");
            }
        } else if ((dateAccidentFrom != null) || (dateAccidentTo != null)) {
            LOGGER.debug("the date_accident_to and date_accident_from must be present together ");
            throw new BadParametersException("the date_accident_to and date_accident_from must be present together ", MessageCode.CLAIMS_1011);
        }

        typeAccident = new ArrayList<String>() {{
            add("RC_ATTIVA");
            add("RC_PASSIVA");
            add("RC_CONCORSUALE");
            add("RC_NON_VERIFICABILE");
        }};
        if (Util.isNotEmpty(clientId)) {
            whereCondition.append(" AND (claims.damaged->>'customer')\\:\\:jsonb->>'customer_id' = '" + clientId + "' ");
        }

        if (Util.isNotEmpty(clientName)) {
            whereCondition.append(" AND (claims.damaged->>'customer')\\:\\:jsonb->>'legal_name' ILIKE '%" + clientName + "%'  ");
        }

        if (Util.isNotEmpty(plate)) {
            whereCondition.append(" AND claims.complaint->>'plate' ILIKE '%" + plate + "%' ");
        }

        if (Util.isNotEmpty(locator)) {
            whereCondition.append(" AND claims.complaint->>'locator' ILIKE '%" + locator + "%' ");
        }

        if (practiceId != null) {
            whereCondition.append(" AND claims.practice_id = " + practiceId + " ");
        }
        if (inEvidence != null) {
            whereCondition.append(" AND claims.in_evidence = " + inEvidence + " ");
        }

        if (withContinuation != null) {
            whereCondition.append(" AND claims.with_continuation =" + withContinuation + " ");
        }

        if (withReceptions != null) {
            whereCondition.append(" AND (claims.theft->>'is_with_receptions')\\:\\:bool =" + withReceptions + " ");
        }

        if (typeAccident != null) {
            whereCondition.append(" AND (claims.complaint->>'data_accident')\\:\\:jsonb->>'type_accident' IN (:typeAccidentSet) ");
        }

        if (Util.isNotEmpty(company))
            whereCondition.append(" AND ((claims.damaged->>'insurance_company')\\:\\:jsonb->>'tpl')\\:\\:jsonb->>'company' ILIKE '%" + company + "%' ");

        if (status != null) {
            whereCondition.append(" AND claims.status IN (:statusSet) ");
        }

        if (flow != null) {
            whereCondition.append(" AND claims.type IN (:flowSet) ");
        }

        sql.append(whereCondition);

        Query query = em.createNativeQuery(sql.toString());

        if (typeAccident != null)
            query.setParameter("typeAccidentSet", typeAccident);
        if (flow != null)
            query.setParameter("flowSet", flow);
        if (status != null)
            query.setParameter("statusSet", status);

        return (BigInteger) query.getSingleResult();

    }


    public List<ClaimsEntity> getMsaClaimsEntity(Boolean inEvidence, Long practiceId, String flow, String clientId, String plate,
                                                 String status, String locator, String dateAccidentFrom,
                                                 String dateAccidentTo, String typeAccident, Boolean asc, String orderBy, int page, int pageSize, Boolean withContinuation, String company, Boolean withReceptions, String clientName) {

        List<String> typeAccidentList = new ArrayList<String>() {{
            add("RC_ATTIVA");
            add("RC_PASSIVA");
            add("RC_CONCORSUALE");
            add("RC_NON_VERIFICABILE");
        }};
        Map<String, String> mappOrder = new HashMap<>();
        mappOrder.put("type", " claims.type ");
        mappOrder.put("practice_id", " claims.practice_id ");
        mappOrder.put("date_accident", " (claims.complaint->>'data_accident')\\:\\:jsonb->>'date_accident' ");
        mappOrder.put("plate", " claims.complaint->>'plate' ");
        mappOrder.put("created_at", " claims.created_at ");
        mappOrder.put("type_accident", " (claims.complaint->>'data_accident')\\:\\:jsonb->>'type_accident' ");
        mappOrder.put("locator", " claims.complaint->>'locator' ");

        StringBuilder sql = new StringBuilder("SELECT * FROM claims claims ");
        StringBuilder whereCondition = new StringBuilder(" WHERE (claims.is_read_msa IS NULL OR claims.is_read_msa = false) AND \n" +
                "claims.status != 'WAITING_FOR_VALIDATION' AND \n" +
                "((claims.complaint->>'entrusted' IS NOT NULL AND \n" +
                "((claims.complaint->>'entrusted')\\:\\:jsonb) ->>'type' ='LEGAL' AND \n" +
                " ((claims.complaint->>'entrusted')\\:\\:jsonb )->>'id_entrusted_to' = '28445608-4a5f-11e9-8646-d663bd873d93')\n" +
                "OR ((((claims.damaged ->>'insurance_company')\\:\\:jsonb )->>'tpl')\\:\\:jsonb ) ->> 'insurance_company_id' = 'a83be7e2-4a58-11e9-8646-d663bd873d93') ");

        if (dateAccidentFrom != null && dateAccidentTo != null) {
            if (dateAccidentTo.compareTo(dateAccidentFrom) < 0) {
                LOGGER.debug("the date_accident_to is less than the date_accident_from");
                throw new BadParametersException("the date_accident_to is less than the date_accident_from", MessageCode.CLAIMS_1011);
            } else {
                whereCondition.append(" AND ((((claims.complaint->>'data_accident')\\:\\:jsonb)->>'date_accident')\\:\\:timestamp BETWEEN '" + dateAccidentFrom + "'\\:\\:timestamp AND '" + dateAccidentTo + "'\\:\\:timestamp )");
            }
        } else if ((dateAccidentFrom != null) || (dateAccidentTo != null)) {
            LOGGER.debug("the date_accident_to and date_accident_from must be present together ");
            throw new BadParametersException("the date_accident_to and date_accident_from must be present together ", MessageCode.CLAIMS_1011);
        }

        if (Util.isNotEmpty(clientId)) {
            whereCondition.append(" AND (claims.damaged->>'customer')\\:\\:jsonb->>'customer_id' = '" + clientId + "' ");
        }

        if (Util.isNotEmpty(clientName)) {
            whereCondition.append(" AND (claims.damaged->>'customer')\\:\\:jsonb->>'legal_name' ILIKE '%" + clientName + "%'  ");
        }

        if (Util.isNotEmpty(plate)) {
            whereCondition.append(" AND claims.complaint->>'plate' ILIKE '%" + plate + "%' ");
        }

        if (Util.isNotEmpty(locator)) {
            whereCondition.append(" AND claims.complaint->>'locator' ILIKE '%" + locator + "%' ");
        }

        if (practiceId != null) {
            whereCondition.append(" AND claims.practice_id = " + practiceId + " ");
        }
        if (inEvidence != null) {
            whereCondition.append(" AND claims.in_evidence = " + inEvidence + " ");
        }

        if (withContinuation != null) {
            whereCondition.append(" AND claims.with_continuation =" + withContinuation + " ");
        }

        if (withReceptions != null) {
            whereCondition.append(" AND (claims.theft->>'is_with_receptions')\\:\\:bool =" + withReceptions + " ");
        }

        if (typeAccidentList != null) {
            whereCondition.append(" AND (claims.complaint->>'data_accident')\\:\\:jsonb->>'type_accident' IN (:typeAccidentSet) ");
        }

        if (Util.isNotEmpty(company))
            whereCondition.append(" AND ((claims.damaged->>'insurance_company')\\:\\:jsonb->>'tpl')\\:\\:jsonb->>'company' ILIKE '%" + company + "%' ");

        if (status != null) {
            whereCondition.append(" AND claims.status IN (:statusSet) ");
        }

        if (flow != null) {
            whereCondition.append(" AND claims.type IN (:flowSet) ");
        }

        sql.append(whereCondition);

        String fieldToOrder = null;
        if (orderBy != null)
            fieldToOrder = mappOrder.get(orderBy);
        if (fieldToOrder != null && asc != null) {
            if (asc) {
                sql.append(" ORDER BY " + fieldToOrder + " ASC");
            } else {
                sql.append(" ORDER BY " + fieldToOrder + " DESC");
            }

        } else if (fieldToOrder != null) {
            sql.append(" ORDER BY " + fieldToOrder + " DESC");
        }
        sql.append(" LIMIT " + pageSize + " OFFSET " + (page - 1) * pageSize);
        Query query = em.createNativeQuery(sql.toString(), ClaimsEntity.class);
        if (typeAccidentList != null)
            query.setParameter("typeAccidentSet", typeAccidentList);
        if (flow != null)
            query.setParameter("flowSet", flow);
        if (status != null)
            query.setParameter("statusSet", status);
        return query.getResultList();

    }


    public BigInteger countClaimsForMsaPagination(Boolean inEvidence, Long practiceId, List<String> flow, String clientId, String plate, List<String> status, String locator, String dateAccidentFrom, String dateAccidentTo, List<String> typeAccident, Boolean asc, String orderBy, Integer page, Integer pageSize, Boolean withContinuation, String company, Boolean withReceptions, String clientName) {
        StringBuilder sql = new StringBuilder("SELECT COUNT (*) FROM claims claims ");
        StringBuilder whereCondition = new StringBuilder(" WHERE (claims.is_read_msa IS NULL OR claims.is_read_msa = false) AND \n" +
                "claims.status != 'WAITING_FOR_VALIDATION' AND \n" +
                "((claims.complaint->>'entrusted' IS NOT NULL AND \n" +
                "((claims.complaint->>'entrusted')\\:\\:jsonb) ->>'type' ='LEGAL' AND \n" +
                " ((claims.complaint->>'entrusted')\\:\\:jsonb )->>'id_entrusted_to' = '28445608-4a5f-11e9-8646-d663bd873d93')\n" +
                "OR ((((claims.damaged ->>'insurance_company')\\:\\:jsonb )->>'tpl')\\:\\:jsonb ) ->> 'insurance_company_id' = 'a83be7e2-4a58-11e9-8646-d663bd873d93') ");

        typeAccident = new ArrayList<String>() {{
            add("RC_ATTIVA");
            add("RC_PASSIVA");
            add("RC_CONCORSUALE");
            add("RC_NON_VERIFICABILE");
        }};

        if (dateAccidentFrom != null && dateAccidentTo != null) {
            if (dateAccidentTo.compareTo(dateAccidentFrom) < 0) {
                LOGGER.debug("the date_accident_to is less than the date_accident_from");
                throw new BadParametersException("the date_accident_to is less than the date_accident_from", MessageCode.CLAIMS_1011);
            } else {
                whereCondition.append(" AND ((((claims.complaint->>'data_accident')\\:\\:jsonb)->>'date_accident')\\:\\:timestamp BETWEEN '" + dateAccidentFrom + "'\\:\\:timestamp AND '" + dateAccidentTo + "'\\:\\:timestamp )");
            }
        } else if ((dateAccidentFrom != null) || (dateAccidentTo != null)) {
            LOGGER.debug("the date_accident_to and date_accident_from must be present together ");
            throw new BadParametersException("the date_accident_to and date_accident_from must be present together ", MessageCode.CLAIMS_1011);
        }

        if (Util.isNotEmpty(clientId)) {
            whereCondition.append(" AND (claims.damaged->>'customer')\\:\\:jsonb->>'customer_id' = '" + clientId + "' ");
        }

        if (Util.isNotEmpty(clientName)) {
            whereCondition.append(" AND (claims.damaged->>'customer')\\:\\:jsonb->>'legal_name' ILIKE '%" + clientName + "%'  ");
        }

        if (Util.isNotEmpty(plate)) {
            whereCondition.append(" AND claims.complaint->>'plate' ILIKE '%" + plate + "%' ");
        }

        if (Util.isNotEmpty(locator)) {
            whereCondition.append(" AND claims.complaint->>'locator' ILIKE '%" + locator + "%' ");
        }

        if (practiceId != null) {
            whereCondition.append(" AND claims.practice_id = " + practiceId + " ");
        }
        if (inEvidence != null) {
            whereCondition.append(" AND claims.in_evidence = " + inEvidence + " ");
        }

        if (withContinuation != null) {
            whereCondition.append(" AND claims.with_continuation =" + withContinuation + " ");
        }

        if (withReceptions != null) {
            whereCondition.append(" AND (claims.theft->>'is_with_receptions')\\:\\:bool =" + withReceptions + " ");
        }

        if (typeAccident != null) {
            whereCondition.append(" AND (claims.complaint->>'data_accident')\\:\\:jsonb->>'type_accident' IN (:typeAccidentSet) ");
        }

        if (Util.isNotEmpty(company))
            whereCondition.append(" AND ((claims.damaged->>'insurance_company')\\:\\:jsonb->>'tpl')\\:\\:jsonb->>'company' ILIKE '%" + company + "%' ");

        if (status != null) {
            whereCondition.append(" AND claims.status IN (:statusSet) ");
        }

        if (flow != null) {
            whereCondition.append(" AND claims.type IN (:flowSet) ");
        }

        sql.append(whereCondition);

        Query query = em.createNativeQuery(sql.toString());

        if (typeAccident != null)
            query.setParameter("typeAccidentSet", typeAccident);
        if (flow != null)
            query.setParameter("flowSet", flow);
        if (status != null)
            query.setParameter("statusSet", status);

        return (BigInteger) query.getSingleResult();

    }

    public List<ClaimsEntity> findClaimsByDamagedPlateAndCounterpartyPlateName(String damagedPlate, String counterpartyPlate, String counterpartyFirstName, String counterpartyLastName, String dateAccidentFrom, String dateAccidentTo) {

        StringBuilder query = new StringBuilder();
        query.append("SELECT c.* " +
                "FROM claims c LEFT JOIN counterparty cp on c.id = cp.claims_id " +
                "WHERE c.status <> 'DRAFT' AND " +
                "c.status <> 'CLOSED' AND " +
                "c.status <> 'CLOSED_PRACTICE' ");

        if(Util.isNotEmpty(damagedPlate)){
            query.append("AND ((c.damaged ->> 'vehicle')\\:\\:jsonb) ->>'license_plate' = '" + damagedPlate + "' ");
        }
        if (Util.isNotEmpty(counterpartyPlate)) {
            query.append("AND cp.vehicle->> 'license_plate' = '" + counterpartyPlate + "' ");
        }
        if (Util.isNotEmpty(counterpartyFirstName) && Util.isNotEmpty(counterpartyLastName)) {
            query.append("AND lower(cp.insured->> 'firstname') = '" + counterpartyFirstName.toLowerCase() + "' " + "AND " +
                    "lower(cp.insured->> 'lastname') = '" + counterpartyLastName.toLowerCase() + "' ");
        }
        if (dateAccidentFrom != null && dateAccidentTo != null) {
            query.append(" AND ((((c.complaint->>'data_accident')\\:\\:jsonb)->>'date_accident')\\:\\:timestamp BETWEEN '" + dateAccidentFrom + "'\\:\\:timestamp AND '" + dateAccidentTo + "'\\:\\:timestamp )");
            query.append(" AND c.status <> 'WAITING_FOR_VALIDATION' ");
        }

        Query sql = em.createNativeQuery(query.toString(), ClaimsEntity.class);

        List<ClaimsEntity> response = sql.getResultList();

        return response;
    }


    public List<ClaimsEntity> findClaimsForPaginationWithoutDraft(Boolean inEvidence, Long practiceId, List<String> flow, List<String> clientIdList, String plate, List<String> status, String locator, String dateAccidentFrom, String dateAccidentTo, List<String> typeAccident, Boolean asc, String orderBy, Integer page, Integer pageSize, Boolean withContinuation, String company, Boolean withReceptions, String clientName, Boolean poVariation, Long fleetVehicleId) {
        Map<String, String> mappOrder = new HashMap<>();
        mappOrder.put("type", " claims.type ");
        mappOrder.put("practice_id", " claims.practice_id ");
        mappOrder.put("date_accident", " (claims.complaint->>'data_accident')\\:\\:jsonb->>'date_accident' ");
        mappOrder.put("plate", " claims.complaint->>'plate' ");
        mappOrder.put("created_at", " claims.created_at ");
        mappOrder.put("type_accident", " (claims.complaint->>'data_accident')\\:\\:jsonb->>'type_accident' ");
        mappOrder.put("locator", " claims.complaint->>'locator' ");

        StringBuilder sql = new StringBuilder("SELECT * FROM claims claims ");
        StringBuilder whereCondition = new StringBuilder(" WHERE 1=1 AND claims.status <> 'DRAFT' ");

        if (dateAccidentFrom != null && dateAccidentTo != null) {

            if (dateAccidentTo.compareTo(dateAccidentFrom) < 0) {
                LOGGER.debug("the date_accident_to is less than the date_accident_from");
                throw new BadParametersException("the date_accident_to is less than the date_accident_from", MessageCode.CLAIMS_1011);
            } else {
                whereCondition.append(" AND ((((claims.complaint->>'data_accident')\\:\\:jsonb)->>'date_accident')\\:\\:timestamp BETWEEN '" + dateAccidentFrom + "'\\:\\:timestamp AND '" + dateAccidentTo + "'\\:\\:timestamp )");
            }
        } else if ((dateAccidentFrom != null) || (dateAccidentTo != null)) {
            LOGGER.debug("the date_accident_to and date_accident_from must be present together ");
            throw new BadParametersException("the date_accident_to and date_accident_from must be present together ", MessageCode.CLAIMS_1011);
        }

        if (clientIdList != null && !clientIdList.isEmpty()) {
            whereCondition.append(" AND (claims.damaged->>'customer')\\:\\:jsonb->>'customer_id' IN (:clientIdList) ");
        }

        if (Util.isNotEmpty(clientName)) {
            whereCondition.append(" AND (claims.damaged->>'customer')\\:\\:jsonb->>'legal_name' ILIKE '%" + clientName + "%'  ");
        }

        if (fleetVehicleId != null) {
            whereCondition.append(" AND (claims.damaged->>'vehicle')\\:\\:jsonb->>'fleet_vehicle_id' = '" + fleetVehicleId + "'  ");
        }

        if (Util.isNotEmpty(plate)) {
            whereCondition.append(" AND claims.complaint->>'plate' ILIKE '%" + plate + "%' ");
        }

        if (Util.isNotEmpty(locator)) {
            whereCondition.append(" AND claims.complaint->>'locator' ILIKE '%" + locator + "%' ");
        }

        if (practiceId != null) {
            whereCondition.append(" AND claims.practice_id = " + practiceId + " ");
        }
        if (inEvidence != null) {
            whereCondition.append(" AND claims.in_evidence = " + inEvidence + " ");
        }

        if (withContinuation != null) {
            whereCondition.append(" AND claims.with_continuation =" + withContinuation + " ");
        }

        if (poVariation != null) {
            whereCondition.append(" AND claims.po_variation =" + poVariation + " ");
        }

        if (withReceptions != null) {
            whereCondition.append(" AND (claims.theft->>'is_with_receptions')\\:\\:bool =" + withReceptions + " ");
        }

        if (typeAccident != null) {
            whereCondition.append(" AND (claims.complaint->>'data_accident')\\:\\:jsonb->>'type_accident' IN (:typeAccidentSet) ");
        }

        if (Util.isNotEmpty(company))
            whereCondition.append(" AND ((claims.damaged->>'insurance_company')\\:\\:jsonb->>'tpl')\\:\\:jsonb->>'company' ILIKE '%" + company + "%' ");

        if (status != null) {
            whereCondition.append(" AND claims.status IN (:statusSet) ");
        }

        if (flow != null) {
            whereCondition.append(" AND claims.type IN (:flowSet) ");
        }

        sql.append(whereCondition);

        String fieldToOrder = null;
        if (orderBy != null)
            fieldToOrder = mappOrder.get(orderBy);
        if (fieldToOrder != null && asc != null) {
            if (asc) {
                sql.append(" ORDER BY " + fieldToOrder + " ASC");
            } else {
                sql.append(" ORDER BY " + fieldToOrder + " DESC");
            }

        } else if (fieldToOrder != null) {
            sql.append(" ORDER BY " + fieldToOrder + " DESC");
        }
        sql.append(" LIMIT " + pageSize + " OFFSET " + (page - 1) * pageSize);

        Query query = em.createNativeQuery(sql.toString(), ClaimsEntity.class);
        if (typeAccident != null)
            query.setParameter("typeAccidentSet", typeAccident);
        if (flow != null)
            query.setParameter("flowSet", flow);
        if (status != null)
            query.setParameter("statusSet", status);
        if (clientIdList != null)
            query.setParameter("clientIdList", clientIdList);

        return query.getResultList();
    }

    /* Pagination MyAld FleetManager
    * FILTRI
        1) contratto, targa e n° sinistro
        2) data del sinistro dal - data del sinistro al
        3) stato denuncia
        4) data inserimento sinistro dal - data inserimento sinistro al
        5) inserita da: me, altri utenti, operatore ald
    *
    * */
    public List<ClaimsEntity> findClaimsForPaginationWithoutDraftMyAldFleetManager(String contractPlateNumberSx,List<String> clientIdList,String clientId, List<String> status,String dateAccidentFrom, String dateAccidentTo, String dateCreatedFrom, String dateCreatedTo,  List<MyAldInsertedByEnum> insertedByList, String userId, Boolean asc, String orderBy, Integer page, Integer pageSize) {
        Map<String, String> mappOrder = new HashMap<>();
        mappOrder.put("practice_id", " claims.practice_id ");
        mappOrder.put("date_accident", " (claims.complaint->>'data_accident')\\:\\:jsonb->>'date_accident' ");
        mappOrder.put("plate", " claims.complaint->>'plate' ");
        mappOrder.put("created_at", " claims.created_at ");
        mappOrder.put("is_with_counterparty", " claims.is_with_counterparty ");

        StringBuilder sql = new StringBuilder("SELECT * FROM claims claims ");
        //NON RECUPERO I SINISTRI IN BOZZA PER IL MYALD
        StringBuilder whereCondition = new StringBuilder(" WHERE 1=1 AND claims.status <> 'DRAFT' ");

        if (dateAccidentFrom != null && dateAccidentTo != null) {
            dateAccidentTo = dateAccidentTo + "T23:59:59Z";
            if (dateAccidentTo.compareTo(dateAccidentFrom) < 0) {
                LOGGER.debug("the date_accident_to is less than the date_accident_from");
                throw new BadParametersException("the date_accident_to is less than the date_accident_from", MessageCode.CLAIMS_1011);
            } else {
                whereCondition.append(" AND ((((claims.complaint->>'data_accident')\\:\\:jsonb)->>'date_accident')\\:\\:timestamp BETWEEN '" + dateAccidentFrom + "'\\:\\:timestamp AND '" + dateAccidentTo + "'\\:\\:timestamp )");
            }
        } else if (dateAccidentFrom != null) {
            whereCondition.append(" AND (((claims.complaint->>'data_accident')\\:\\:jsonb)->>'date_accident')\\:\\:timestamp >= '" + dateAccidentFrom + "'\\:\\:timestamp");
        }else if (dateAccidentTo != null) {
            dateAccidentTo = dateAccidentTo + "T23:59:59Z";
            whereCondition.append(" AND (((claims.complaint->>'data_accident')\\:\\:jsonb)->>'date_accident')\\:\\:timestamp <= '" + dateAccidentTo + "'\\:\\:timestamp");
        }



        if (dateCreatedFrom != null && dateCreatedTo != null) {
            dateCreatedTo = dateCreatedTo + "T23:59:59Z";
            if (dateCreatedTo.compareTo(dateCreatedFrom) < 0) {
                LOGGER.debug("the date_created_to is less than the date_created_from");
                throw new BadParametersException("the date_created_to is less than the date_created_from", MessageCode.CLAIMS_1011);
            } else {
                whereCondition.append(" AND ((claims.created_at)\\:\\:timestamp BETWEEN '" + dateCreatedFrom + "'\\:\\:timestamp AND '" + dateCreatedTo + "'\\:\\:timestamp )");
            }
        } else if (dateCreatedFrom != null) {
            whereCondition.append(" AND (claims.created_at)\\:\\:timestamp >= '" + dateCreatedFrom + "'\\:\\:timestamp");
        }else if (dateCreatedTo != null) {
            dateCreatedTo = dateCreatedTo + "T23:59:59Z";
            whereCondition.append(" AND (claims.created_at)\\:\\:timestamp <= '" + dateCreatedTo + "'\\:\\:timestamp");
        }

        if(contractPlateNumberSx != null){
            whereCondition.append(" AND ((claims.damaged->>'contract')\\:\\:jsonb->>'contract_id' = '" + contractPlateNumberSx + "' OR UPPER(claims.complaint->>'plate') = '" + contractPlateNumberSx.toUpperCase() + "' "+" OR claims.practice_id\\:\\:varchar(255) = '" + contractPlateNumberSx + "' ) ");;
        }

        if (clientIdList != null && !clientIdList.isEmpty()) {
            whereCondition.append(" AND (claims.damaged->>'customer')\\:\\:jsonb->>'customer_id' IN (:clientIdList) ");
        }

        if (clientId != null && !clientId.isEmpty()) {
            whereCondition.append(" AND (claims.damaged->>'customer')\\:\\:jsonb->>'customer_id' = '"+clientId+"' ");
        }

        if (status != null) {
            whereCondition.append(" AND claims.status IN (:statusSet) ");
        }



        if(insertedByList != null && !insertedByList.isEmpty()){
            int count = 0;
            whereCondition.append( " AND ( ");

            if (insertedByList.contains(MyAldInsertedByEnum.ME)) {
                whereCondition.append(" (claims.metadata->>'metadata') IS NOT NULL AND (claims.metadata->>'metadata')\\:\\:jsonb->>'user_id' = '" + userId + "' ");
                count++;
            }
            if( insertedByList.contains(MyAldInsertedByEnum.OTHER)) {
                if (count > 0)
                    whereCondition.append(" OR (claims.metadata->>'metadata') IS NOT NULL AND (claims.metadata->>'metadata')\\:\\:jsonb->>'user_id' <> '" + userId + "' ");
                else
                    whereCondition.append(" (claims.metadata->>'metadata') IS NOT NULL AND (claims.metadata->>'metadata')\\:\\:jsonb->>'user_id' <> '" + userId + "' ");
                count++;
            }
            if(insertedByList.contains(MyAldInsertedByEnum.ALD_OPERATOR)){
                if( count > 0)
                    whereCondition.append(" OR (claims.metadata->>'metadata') IS  NULL ");
                else
                    whereCondition.append("  (claims.metadata->>'metadata') IS  NULL ");
            }
            whereCondition.append(" )");
        }


        sql.append(whereCondition);

        String fieldToOrder = null;
        if (orderBy != null)
            fieldToOrder = mappOrder.get(orderBy);
        if (fieldToOrder != null && asc != null) {
            if(orderBy.equalsIgnoreCase("is_with_counterparty")){
                asc=!asc;
            }
            if (asc) {
                sql.append(" ORDER BY " + fieldToOrder + " ASC");
            } else {
                sql.append(" ORDER BY " + fieldToOrder + " DESC");
            }

        } else if (fieldToOrder != null) {
            sql.append(" ORDER BY " + fieldToOrder + " DESC");
        }
        sql.append(" LIMIT " + pageSize + " OFFSET " + (page - 1) * pageSize);

        Query query = em.createNativeQuery(sql.toString(), ClaimsEntity.class);

        if (status != null)
            query.setParameter("statusSet", status);
        if (clientIdList != null)
            query.setParameter("clientIdList", clientIdList);

        System.out.println(sql);

        return query.getResultList();
    }

    public BigInteger countClaimsForPaginationWithoutDraftMyAldFleetManager(String contractPlateNumberSx, List<String> clientIdList,String clientId, List<String> status, String dateAccidentFrom, String dateAccidentTo, String dateCreatedFrom, String dateCreatedTo, List<MyAldInsertedByEnum> insertedByList, String userId) {


        StringBuilder sql = new StringBuilder("SELECT COUNT(*) FROM claims claims ");
        //NON RECUPERO I SINISTRI IN BOZZA PER IL MYALD
        StringBuilder whereCondition = new StringBuilder(" WHERE 1=1 AND claims.status <> 'DRAFT' ");

        if (dateAccidentFrom != null && dateAccidentTo != null) {
            dateAccidentTo = dateAccidentTo + "T23:59:59Z";
            if (dateAccidentTo.compareTo(dateAccidentFrom) < 0) {
                LOGGER.debug("the date_accident_to is less than the date_accident_from");
                throw new BadParametersException("the date_accident_to is less than the date_accident_from", MessageCode.CLAIMS_1011);
            } else {
                whereCondition.append(" AND ((((claims.complaint->>'data_accident')\\:\\:jsonb)->>'date_accident')\\:\\:timestamp BETWEEN '" + dateAccidentFrom + "'\\:\\:timestamp AND '" + dateAccidentTo + "'\\:\\:timestamp )");
            }
        } else if (dateAccidentFrom != null) {
            whereCondition.append(" AND (((claims.complaint->>'data_accident')\\:\\:jsonb)->>'date_accident')\\:\\:timestamp >= '" + dateAccidentFrom + "'\\:\\:timestamp");
        }else if (dateAccidentTo != null) {
            dateAccidentTo = dateAccidentTo + "T23:59:59Z";
            whereCondition.append(" AND (((claims.complaint->>'data_accident')\\:\\:jsonb)->>'date_accident')\\:\\:timestamp <= '" + dateAccidentTo + "'\\:\\:timestamp");
        }



        if (dateCreatedFrom != null && dateCreatedTo != null) {
            dateCreatedTo = dateCreatedTo + "T23:59:59Z";
            if (dateCreatedTo.compareTo(dateCreatedFrom) < 0) {
                LOGGER.debug("the date_created_to is less than the date_created_from");
                throw new BadParametersException("the date_created_to is less than the date_created_from", MessageCode.CLAIMS_1011);
            } else {
                whereCondition.append(" AND ((claims.created_at)\\:\\:timestamp BETWEEN '" + dateCreatedFrom + "'\\:\\:timestamp AND '" + dateCreatedTo + "'\\:\\:timestamp )");
            }
        } else if (dateCreatedFrom != null) {
            whereCondition.append(" AND (claims.created_at)\\:\\:timestamp >= '" + dateCreatedFrom + "'\\:\\:timestamp");
        }else if (dateCreatedTo != null) {
            dateCreatedTo = dateCreatedTo + "T23:59:59Z";
            whereCondition.append(" AND (claims.created_at)\\:\\:timestamp <= '" + dateCreatedTo + "'\\:\\:timestamp");
        }

        if(contractPlateNumberSx != null){
            whereCondition.append(" AND ((claims.damaged->>'contract')\\:\\:jsonb->>'contract_id' = '" + contractPlateNumberSx + "' OR UPPER(claims.complaint->>'plate') = '" + contractPlateNumberSx.toUpperCase() + "' "+" OR claims.practice_id\\:\\:varchar(255) = '" + contractPlateNumberSx + "' ) ");;
        }

        if (clientIdList != null && !clientIdList.isEmpty()) {
            whereCondition.append(" AND (claims.damaged->>'customer')\\:\\:jsonb->>'customer_id' IN (:clientIdList) ");
        }

        if (clientId != null && !clientId.isEmpty()) {
            whereCondition.append(" AND (claims.damaged->>'customer')\\:\\:jsonb->>'customer_id' = '"+clientId+"' ");
        }

        if (status != null) {
            whereCondition.append(" AND claims.status IN (:statusSet) ");
        }


        if(insertedByList != null && !insertedByList.isEmpty()){
            int count = 0;
            whereCondition.append( " AND ( ");

            if (insertedByList.contains(MyAldInsertedByEnum.ME)) {
                whereCondition.append(" (claims.metadata->>'metadata') IS NOT NULL AND (claims.metadata->>'metadata')\\:\\:jsonb->>'user_id' = '" + userId + "' ");
                count++;
            }
            if( insertedByList.contains(MyAldInsertedByEnum.OTHER)) {
                if (count > 0)
                    whereCondition.append(" OR (claims.metadata->>'metadata') IS NOT NULL AND (claims.metadata->>'metadata')\\:\\:jsonb->>'user_id' <> '" + userId + "' ");
                else
                    whereCondition.append(" (claims.metadata->>'metadata') IS NOT NULL AND (claims.metadata->>'metadata')\\:\\:jsonb->>'user_id' <> '" + userId + "' ");
                count++;
            }
            if(insertedByList.contains(MyAldInsertedByEnum.ALD_OPERATOR)){
                if( count > 0)
                    whereCondition.append(" OR (claims.metadata->>'metadata') IS  NULL ");
                else
                    whereCondition.append("  (claims.metadata->>'metadata') IS  NULL ");
            }

            whereCondition.append(" )");
        }

        sql.append(whereCondition);



        Query query = em.createNativeQuery(sql.toString());

        if (status != null)
            query.setParameter("statusSet", status);
        if (clientIdList != null)
            query.setParameter("clientIdList", clientIdList);

        System.out.println(sql);

        return (BigInteger)query.getSingleResult();
    }


    /* Pagination MyAld Driver
   * FILTRI
       1) contratto, n° sinistro
       2) data del sinistro dal - data del sinistro al
       3) stato denuncia
       4) data inserimento sinistro dal - data inserimento sinistro al
       5) lista di targhe
   *
   * */
    public List<ClaimsEntity> findClaimsForPaginationWithoutDraftMyAldDriver(List<String> plates,String contractPlateNumberSx, List<String> status,String dateAccidentFrom, String dateAccidentTo, String dateCreatedFrom, String dateCreatedTo, Boolean asc, String orderBy, Integer page, Integer pageSize, String userId, List<MyAldInsertedByEnum> insertedByList, Boolean isInclusive) {
        Map<String, String> mappOrder = new HashMap<>();
        mappOrder.put("practice_id", " claims.practice_id ");
        mappOrder.put("date_accident", " (claims.complaint->>'data_accident')\\:\\:jsonb->>'date_accident' ");
        mappOrder.put("plate", " claims.complaint->>'plate' ");
        mappOrder.put("created_at", " claims.created_at ");

        StringBuilder sql = new StringBuilder("SELECT * FROM claims claims ");
        //NON RECUPERO I SINISTRI IN BOZZA PER IL MYALD
        StringBuilder whereCondition = new StringBuilder(" WHERE 1=1 AND claims.status <> 'DRAFT' ");

        if (dateAccidentFrom != null && dateAccidentTo != null) {
            dateAccidentTo = dateAccidentTo + "T23:59:59Z";
            if (dateAccidentTo.compareTo(dateAccidentFrom) < 0) {
                LOGGER.debug("the date_accident_to is less than the date_accident_from");
                throw new BadParametersException("the date_accident_to is less than the date_accident_from", MessageCode.CLAIMS_1011);
            } else {
                whereCondition.append(" AND ((((claims.complaint->>'data_accident')\\:\\:jsonb)->>'date_accident')\\:\\:timestamp BETWEEN '" + dateAccidentFrom + "'\\:\\:timestamp AND '" + dateAccidentTo + "'\\:\\:timestamp )");
            }
        } else if (dateAccidentFrom != null) {
            whereCondition.append(" AND (((claims.complaint->>'data_accident')\\:\\:jsonb)->>'date_accident')\\:\\:timestamp >= '" + dateAccidentFrom + "'\\:\\:timestamp");
        }else if (dateAccidentTo != null) {
            dateAccidentTo = dateAccidentTo + "T23:59:59Z";
            whereCondition.append(" AND (((claims.complaint->>'data_accident')\\:\\:jsonb)->>'date_accident')\\:\\:timestamp <= '" + dateAccidentTo + "'\\:\\:timestamp");
        }



        if (dateCreatedFrom != null && dateCreatedTo != null) {
            dateCreatedTo = dateCreatedTo + "T23:59:59Z";
            if (dateCreatedTo.compareTo(dateCreatedFrom) < 0) {
                LOGGER.debug("the date_created_to is less than the date_created_from");
                throw new BadParametersException("the date_created_to is less than the date_created_from", MessageCode.CLAIMS_1011);
            } else {
                whereCondition.append(" AND ((claims.created_at)\\:\\:timestamp BETWEEN '" + dateCreatedFrom + "'\\:\\:timestamp AND '" + dateCreatedTo + "'\\:\\:timestamp )");
            }
        } else if (dateCreatedFrom != null) {
            whereCondition.append(" AND (claims.created_at)\\:\\:timestamp >= '" + dateCreatedFrom + "'\\:\\:timestamp");
        }else if (dateCreatedTo != null) {
            dateCreatedTo = dateCreatedTo + "T23:59:59Z";
            whereCondition.append(" AND (claims.created_at)\\:\\:timestamp <= '" + dateCreatedTo + "'\\:\\:timestamp");
        }

        if(contractPlateNumberSx != null){
            whereCondition.append(" AND ((claims.damaged->>'contract')\\:\\:jsonb->>'contract_id' = '" + contractPlateNumberSx + "' OR UPPER(claims.complaint->>'plate') = '" + contractPlateNumberSx.toUpperCase() + "' "+" OR claims.practice_id\\:\\:varchar(255) = '" + contractPlateNumberSx + "' ) ");;
        }


        if(isInclusive != null && isInclusive){
            whereCondition.append(" AND claims.complaint->>'plate' IN (:plateList) ");
        } else {
            whereCondition.append(" AND claims.complaint->>'plate' NOT IN (:plateList) ");
        }


        if (status != null) {
            whereCondition.append(" AND claims.status IN (:statusSet) ");
        }


        if(insertedByList != null && !insertedByList.isEmpty()){


            int count = 0;
            whereCondition.append( " AND ( ");

            if (insertedByList.contains(MyAldInsertedByEnum.ME)) {
                whereCondition.append(" ((claims.metadata->>'metadata') IS NOT NULL AND (claims.metadata->>'metadata')\\:\\:jsonb->>'user_id' = '" + userId + "' ) ");
                count++;
            }
            if( insertedByList.contains(MyAldInsertedByEnum.OTHER)) {
                if (count > 0)
                    whereCondition.append(" OR ((claims.metadata->>'metadata') IS NOT NULL AND (claims.metadata->>'metadata')\\:\\:jsonb->>'user_id' <> '" + userId + "' ) ");
                else
                    whereCondition.append(" ((claims.metadata->>'metadata') IS NOT NULL AND (claims.metadata->>'metadata')\\:\\:jsonb->>'user_id' <> '" + userId + "' ) ");
                count++;
            }
            if(insertedByList.contains(MyAldInsertedByEnum.ALD_OPERATOR)){
                if( count > 0)
                    whereCondition.append(" OR ( (claims.metadata->>'metadata') IS  NULL ) ");
                else
                    whereCondition.append("  ((claims.metadata->>'metadata') IS  NULL ) ");
            }

            whereCondition.append(" )");

        }

        sql.append(whereCondition);

        String fieldToOrder = null;
        if (orderBy != null)
            fieldToOrder = mappOrder.get(orderBy);
        if (fieldToOrder != null && asc != null) {
            if (asc) {
                sql.append(" ORDER BY " + fieldToOrder + " ASC");
            } else {
                sql.append(" ORDER BY " + fieldToOrder + " DESC");
            }

        } else if (fieldToOrder != null) {
            sql.append(" ORDER BY " + fieldToOrder + " DESC");
        }
        sql.append(" LIMIT " + pageSize + " OFFSET " + (page - 1) * pageSize);

        Query query = em.createNativeQuery(sql.toString(), ClaimsEntity.class);

        if (status != null)
            query.setParameter("statusSet", status);
        if (plates != null)
            query.setParameter("plateList", plates);



        System.out.println(sql);

        return query.getResultList();
    }

    public BigInteger countClaimsForPaginationWithoutDraftMyAldDriver(List<String> plates,String contractPlateNumberSx, List<String> status,String dateAccidentFrom, String dateAccidentTo, String dateCreatedFrom, String dateCreatedTo,  String userId, List<MyAldInsertedByEnum> insertedByList, Boolean isInclusive) {

        StringBuilder sql = new StringBuilder("SELECT COUNT(*) FROM claims claims ");
        //NON RECUPERO I SINISTRI IN BOZZA PER IL MYALD
        StringBuilder whereCondition = new StringBuilder(" WHERE 1=1 AND claims.status <> 'DRAFT' ");

        if (dateAccidentFrom != null && dateAccidentTo != null) {
            dateAccidentTo = dateAccidentTo + "T23:59:59Z";
            if (dateAccidentTo.compareTo(dateAccidentFrom) < 0) {
                LOGGER.debug("the date_accident_to is less than the date_accident_from");
                throw new BadParametersException("the date_accident_to is less than the date_accident_from", MessageCode.CLAIMS_1011);
            } else {
                whereCondition.append(" AND ((((claims.complaint->>'data_accident')\\:\\:jsonb)->>'date_accident')\\:\\:timestamp BETWEEN '" + dateAccidentFrom + "'\\:\\:timestamp AND '" + dateAccidentTo + "'\\:\\:timestamp )");
            }
        } else if (dateAccidentFrom != null) {
            whereCondition.append(" AND (((claims.complaint->>'data_accident')\\:\\:jsonb)->>'date_accident')\\:\\:timestamp >= '" + dateAccidentFrom + "'\\:\\:timestamp");
        }else if (dateAccidentTo != null) {
            dateAccidentTo = dateAccidentTo + "T23:59:59Z";
            whereCondition.append(" AND (((claims.complaint->>'data_accident')\\:\\:jsonb)->>'date_accident')\\:\\:timestamp <= '" + dateAccidentTo + "'\\:\\:timestamp");
        }



        if (dateCreatedFrom != null && dateCreatedTo != null) {
            dateCreatedTo = dateCreatedTo + "T23:59:59Z";
            if (dateCreatedTo.compareTo(dateCreatedFrom) < 0) {
                LOGGER.debug("the date_created_to is less than the date_created_from");
                throw new BadParametersException("the date_created_to is less than the date_created_from", MessageCode.CLAIMS_1011);
            } else {
                whereCondition.append(" AND ((claims.created_at)\\:\\:timestamp BETWEEN '" + dateCreatedFrom + "'\\:\\:timestamp AND '" + dateCreatedTo + "'\\:\\:timestamp )");
            }
        } else if (dateCreatedFrom != null) {
            whereCondition.append(" AND (claims.created_at)\\:\\:timestamp >= '" + dateCreatedFrom + "'\\:\\:timestamp");
        }else if (dateCreatedTo != null) {
            dateCreatedTo = dateCreatedTo + "T23:59:59Z";
            whereCondition.append(" AND (claims.created_at)\\:\\:timestamp <= '" + dateCreatedTo + "'\\:\\:timestamp");
        }

        if(contractPlateNumberSx != null){
            whereCondition.append(" AND ((claims.damaged->>'contract')\\:\\:jsonb->>'contract_id' = '" + contractPlateNumberSx + "' OR UPPER(claims.complaint->>'plate') = '" + contractPlateNumberSx.toUpperCase() + "' "+" OR claims.practice_id\\:\\:varchar(255) = '" + contractPlateNumberSx + "' ) ");;
        }


        if(isInclusive != null && isInclusive){
            whereCondition.append(" AND claims.complaint->>'plate' IN (:plateList) ");
        } else {
            whereCondition.append(" AND claims.complaint->>'plate' NOT IN (:plateList) ");
        }



        if (status != null) {
            whereCondition.append(" AND claims.status IN (:statusSet) ");
        }

        if(insertedByList != null && !insertedByList.isEmpty()){
            int count = 0;
            whereCondition.append( " AND ( ");

            if (insertedByList.contains(MyAldInsertedByEnum.ME)) {
                whereCondition.append(" ((claims.metadata->>'metadata') IS NOT NULL AND (claims.metadata->>'metadata')\\:\\:jsonb->>'user_id' = '" + userId + "' ) ");
                count++;
            }
            if( insertedByList.contains(MyAldInsertedByEnum.OTHER)) {
                if (count > 0)
                    whereCondition.append(" OR ((claims.metadata->>'metadata') IS NOT NULL AND (claims.metadata->>'metadata')\\:\\:jsonb->>'user_id' <> '" + userId + "' ) ");
                else
                    whereCondition.append(" ( (claims.metadata->>'metadata') IS NOT NULL AND (claims.metadata->>'metadata')\\:\\:jsonb->>'user_id' <> '" + userId + "' ) ");
                count++;
            }
            if(insertedByList.contains(MyAldInsertedByEnum.ALD_OPERATOR)){
                if( count > 0)
                    whereCondition.append(" OR ((claims.metadata->>'metadata') IS  NULL ) ");
                else
                    whereCondition.append("  ((claims.metadata->>'metadata') IS  NULL ) ");
            }
            whereCondition.append(" )");
        }



        sql.append(whereCondition);


        Query query = em.createNativeQuery(sql.toString());

        if (status != null)
            query.setParameter("statusSet", status);
        if (plates != null)
            query.setParameter("plateList", plates);



        System.out.println(sql);

        return (BigInteger) query.getSingleResult();
    }

    @Transactional
    public void resetAuthority(){
        StringBuilder sql = new StringBuilder("UPDATE claims SET authority = null ");
        Query query = em.createNativeQuery(sql.toString());
        query.executeUpdate();
    }

    @Modifying
    @Transactional
    public void deleteClaims(List<Long> practiceIdList) {
        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("DELETE FROM claims c ");

        StringBuilder whereCondition = new StringBuilder(" WHERE 1=1 ");
        if (practiceIdList != null && !practiceIdList.isEmpty()) {
            whereCondition.append(" AND c.practice_id IN (:practiceIdList) ");
        }


        queryBuilder.append(whereCondition);

        Query sql = em.createNativeQuery(queryBuilder.toString());
        sql.setParameter("practiceIdList", practiceIdList);
        sql.executeUpdate();
    }

    //query per estrarre tutte le pratiche authority di cui richiedere i po
    public List<ClaimsEntity> getClaimsToInvoce() {

        String auxiliaryTableAuthority = "WITH AUTH AS (SELECT id, jsonb_array_elements(authority) AS authority FROM claims AS c) ";

        StringBuilder sqlQuery = new StringBuilder(auxiliaryTableAuthority);
        sqlQuery.append("SELECT * FROM claims c JOIN AUTH a USING(id) ");

        StringBuilder whereCondition = new StringBuilder(" WHERE a.authority->>'is_invoiced' = 'false'");

        sqlQuery.append(whereCondition);

        Query query = em.createNativeQuery(sqlQuery.toString(), ClaimsEntity.class);

        return query.getResultList();

    }

    public List<ClaimsEntity> findNotClosedTheftWhitPlate(String plate, String data) {

        if ( data == null || data.isEmpty()){
            data = DateUtil.convertUTCInstantToIS08601String(DateUtil.getNowInstant());
        }

        StringBuilder sql = new StringBuilder("SELECT * FROM claims c ");
        StringBuilder whereCondition = new StringBuilder(" WHERE status != 'CLOSED' AND status != 'CLOSED_PRACTICE' AND status!='DELETED' ");

        whereCondition.append("AND ((c.damaged->>'vehicle')\\:\\:jsonb)->>'license_plate' = '" + plate + "' " +
                "AND (  " +
                "( " +
                "   ((complaint->>'data_accident')\\:\\:jsonb)->>'type_accident' = 'FURTO_TOTALE' AND  " +
                "   (((complaint->>'data_accident')\\:\\:jsonb)->>'date_accident')\\:\\:timestamp <= ('"+ data +"')\\:\\:timestamp  " +
                ") " +
                "OR " +
                "( " +
                "  ((complaint->>'data_accident')\\:\\:jsonb)->>'type_accident' = 'FURTO_E_RITROVAMENTO' AND " +
                "  ( ('"+ data +"')\\:\\:timestamp BETWEEN(((complaint->>'data_accident')\\:\\:jsonb)->>'date_accident')\\:\\:timestamp AND (theft->>'find_date')\\:\\:timestamp)\n" +
                ") " +
                ")   ");

        sql.append(whereCondition);

        Query query = em.createNativeQuery(sql.toString(), ClaimsEntity.class);

        return query.getResultList();
    }




}
