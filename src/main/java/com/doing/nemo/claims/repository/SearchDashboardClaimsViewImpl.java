package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.dto.FilterView;
import com.doing.nemo.claims.entity.ClaimsNewEntity;
import com.doing.nemo.claims.entity.view.SearchDashboardClaimsView;
import com.doing.nemo.claims.entity.view.SearchDashboardClaimsView_;
import com.doing.nemo.claims.repository.specification.SearchDashboardClaimsViewSpecification;
import com.doing.nemo.claims.util.Util;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.BadParametersException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

@Repository
public class SearchDashboardClaimsViewImpl {

    private static Logger LOGGER = LoggerFactory.getLogger(SearchDashboardClaimsViewImpl.class);

    @PersistenceContext
    @Autowired
    private EntityManager entityManager;

    @Autowired
    SearchDashboardClaimsViewRepository searchDashboardClaimsViewRepository;

    public static Specification<SearchDashboardClaimsView> distinct() {
        return (root, query, cb) -> {
            query.distinct(true);
            return null;
        };
    }


 /*  public Page<SearchDashboardClaimsView> findClaimsViewForPaginationWithoutDraftAndTheft(FilterView filter, Pageable pageable){


       CriteriaBuilder builder = entityManager.getCriteriaBuilder();
       CriteriaQuery<SearchDashboardClaimsView> query = builder.createQuery(SearchDashboardClaimsView.class);
       SearchDashboardClaimsViewSpecification spec = new SearchDashboardClaimsViewSpecification();


       Root<SearchDashboardClaimsView> root = query.from(SearchDashboardClaimsView.class);
       //query.where(spec.getDateAccidentBetween(filter.getDateAccidentFrom() , filter.getDateAccidentTo()));
       query.select(SearchDashboardClaimsView_.*).


       customerRepository.findAll(where(customerHasBirthday()).and(isLongTermCustomer()));


       StringBuffer sql = null;

       if(filter.getWaitingForAuthority()!=null) {
           sql = new StringBuffer("SELECT DISTINCT claims.*  ");
       }else{
           // Non è necessaria la DISTINCT in quanto la select sarà composta
           // Solo da tabelle con relazione 1 a 1
           sql = new StringBuffer("SELECT claims.*  ");
       }
       StringBuffer fromCondition = new StringBuffer(" FROM search_dashboard_claims_view claims");
       StringBuffer whereCondition = new StringBuffer(" WHERE 1=1 ");

       *//********************* creazione dei filtri *********************************//*

       if (filter.getDateAccidentFrom() != null && filter.getDateAccidentTo() != null) {

           if (filter.getDateAccidentTo().compareTo(filter.getDateAccidentFrom()) < 0) {
               LOGGER.debug("the date_accident_to is less than the date_accident_from");
               throw new BadParametersException("the date_accident_to is less than the date_accident_from", MessageCode.CLAIMS_1011);
           } else {
               whereCondition.append(" AND (claims.date_accident\\:\\:timestamp BETWEEN '" + filter.getDateAccidentFrom() + "'\\:\\:timestamp AND '" + filter.getDateAccidentTo()  + "'\\:\\:timestamp )");
           }
       } else if ((filter.getDateAccidentFrom() != null) || (filter.getDateAccidentTo() != null)) {
           LOGGER.debug("the date_accident_to and date_accident_from must be present together ");
           throw new BadParametersException("the date_accident_to and date_accident_from must be present together ", MessageCode.CLAIMS_1011);
       }


       if (Util.isNotEmpty(filter.getPlate())) {
           whereCondition.append(" AND claims.plate ILIKE '%" + filter.getPlate().trim() + "%' ");
       }


       if (filter.getPracticeId() != null) {
           whereCondition.append(" AND claims.practice_id = " + filter.getPracticeId() + " ");
       }

       if (filter.getInEvidence() != null) {
           if(filter.getInEvidence()) {
               whereCondition.append(" AND claims.in_evidence = " + filter.getInEvidence() + " ");
           }else{
               whereCondition.append(" AND (claims.in_evidence IS NULL OR claims.in_evidence = " + filter.getInEvidence() + ") ");
           }
       }

       if (filter.getWithContinuation() != null) {
           whereCondition = this.addWithContinuationFilter(filter.getWithContinuation(),whereCondition);
       }

       if (filter.getPoVariation() != null) {
           if(filter.getPoVariation()) {
               whereCondition.append(" AND claims.po_variation =" + filter.getPoVariation() + " ");
           }else{
               whereCondition.append(" AND ( claims.po_variation IS NULL OR claims.po_variation =" + filter.getPoVariation() + ") ");
           }
       }


       if (filter.getTypeAccident() != null) {
           whereCondition.append(" AND claims.type_accident IN (:typeAccidentSet) ");
       }


       if (filter.getStatus() != null) {
           whereCondition.append(" AND claims.status IN (:statusSet) AND claims.status <> 'DRAFT'");
       } else {
           whereCondition.append(" AND claims.status <> 'DRAFT'");
       }

       if (filter.getFlow() != null) {
           whereCondition.append(" AND claims.type IN (:flowSet) ");
       }

       if (filter.getWaitingForNumberSx() != null) {
           fromCondition.append(" left join claims_from_company fc on claims.id = fc.id ");
           if (filter.getWaitingForNumberSx()) {
               whereCondition.append(" AND (fc.number_sx IS NULL OR fc.number_sx = '') ");
           } else{
               whereCondition.append(" AND (fc.number_sx IS NOT NULL AND fc.number_sx != '') ");
           }
       }

       if(filter.getWaitingForAuthority() != null){
           fromCondition.append(" inner join claims_to_claims_authority ca on claims.id = ca.claims_id inner join claims_authority cat on ca.authority_id = cat.id ");
           if(filter.getWaitingForAuthority()) {
               whereCondition.append(" AND (cat.is_invoiced is null or cat.is_invoiced <> 'true') AND (cat.event_type is null or cat.event_type <> 'REJECT') ");
           }else
               whereCondition.append(" AND claims.status = 'WAITING_FOR_AUTHORITY' AND cat.is_invoiced = true ");
       }


     *//*   sql.append(fromCondition);
        sql.append(whereCondition);

        String fieldToOrder = null;
        if (orderBy != null)
            fieldToOrder = mappOrder.get(orderBy);
        if (fieldToOrder != null && asc != null) {
            if (asc) {
                sql.append(" ORDER BY " + fieldToOrder + " ASC");
            } else {
                sql.append(" ORDER BY " + fieldToOrder + " DESC");
            }

        } else if (fieldToOrder != null) {
            sql.append(" ORDER BY " + fieldToOrder + " DESC");
        }

        sql.append(" LIMIT " + pageSize + " OFFSET " + (page - 1) * pageSize);
*//*

        System.out.println(sql.toString());


*//*
       if (filter.getTypeAccident() != null)
           query.setParameter("typeAccidentSet", filter.getTypeAccident());
       if (filter.getFlow() != null)
           query.setParameter("flowSet", filter.getFlow());
       if (filter.getStatus() != null)
           query.setParameter("statusSet", filter.getStatus());*//*


       //   return query.getResultList();



       *//******************* creazione dei filtri ***********************************//*

       // searchDashboardClaimsViewRepository.findClaimsForPaginationWithoutDraftAndTheft(claimsRequestFilterViewDTO);

       // paginationResult = searchDashboardClaimsViewRepository.findAll(pageable);


        return null;
   }
*/

  /*  private Specification<SearchDashboardClaimsView> getWorkingsBySearchFiltersAndDeletedAt(ClaimsRequestFilterViewDTO filter) {


        Map<String, String> mappOrder = new HashMap<>();
        mappOrder.put("type", " claims.type ");
        mappOrder.put("practice_id", " claims.practice_id ");
        mappOrder.put("date_accident", " claims.date_accident ");
        mappOrder.put("plate", " claims.plate ");
        mappOrder.put("created_at", " claims.created_at ");
        mappOrder.put("type_accident", " claims.type_accident ");
        mappOrder.put("locator", " claims.locator ");

        StringBuffer sql = null;
        if(filter.getWaitingForAuthority()!=null) {
            sql = new StringBuffer("SELECT DISTINCT claims.*  ");
        }else{
            // Non è necessaria la DISTINCT in quanto la select sarà composta
            // Solo da tabelle con relazione 1 a 1
            sql = new StringBuffer("SELECT claims.*  ");
        }
        StringBuffer fromCondition = new StringBuffer(" FROM search_dashboard_claims_view claims");
        StringBuffer whereCondition = new StringBuffer(" WHERE 1=1 ");




        if (filter.getDateAccidentFrom() != null && filter.getDateAccidentTo() != null) {

            if (filter.getDateAccidentTo().compareTo(filter.getDateAccidentFrom()) < 0) {
                LOGGER.debug("the date_accident_to is less than the date_accident_from");
                throw new BadParametersException("the date_accident_to is less than the date_accident_from", MessageCode.CLAIMS_1011);
            } else {
                whereCondition.append(" AND (claims.date_accident\\:\\:timestamp BETWEEN '" + filter.getDateAccidentFrom() + "'\\:\\:timestamp AND '" + filter.getDateAccidentTo() + "'\\:\\:timestamp )");
            }
        } else if ((filter.getDateAccidentFrom() != null) || (filter.getDateAccidentTo() != null)) {
            LOGGER.debug("the date_accident_to and date_accident_from must be present together ");
            throw new BadParametersException("the date_accident_to and date_accident_from must be present together ", MessageCode.CLAIMS_1011);
        }

        if((filter.getClientListId() != null && !filter.getClientListId().isEmpty()) || (filter.getClientName() != null && !filter.getClientName().isEmpty()) ) {
            fromCondition.append(" inner join claims_damaged_customer cu on claims.id = cu.id ");
            if (filter.getClientListId() != null && !filter.getClientListId().isEmpty()) {
                whereCondition.append(" AND cu.customer_id IN (:clientIdList) ");
            }

            if (Util.isNotEmpty(filter.getClientName())) {
                whereCondition.append(" AND cu.legal_name ILIKE '%" + filter.getClientName() + "%'  ");
            }
        }

        if (filter.getFleetVehicleId() != null) {
            fromCondition.append(" inner join claims_damaged_vehicle ve on claims.id = ve.id ");
            whereCondition.append(" AND ve.fleet_vehicle_id = '" + filter.getFleetVehicleId() + "'  ");
        }

        if (Util.isNotEmpty(filter.getPlate())) {
            whereCondition.append(" AND claims.plate ILIKE '%" + filter.getPlate().trim() + "%' ");
        }

        if (Util.isNotEmpty(filter.getLocator())) {
            whereCondition.append(" AND claims.claim_locator ILIKE '%" + filter.getLocator() + "%' ");
        }

        if (filter.getPracticeId() != null) {
            whereCondition.append(" AND claims.practice_id = " + filter.getPracticeId() + " ");
        }

        if (filter.getInEvidence() != null) {
            if(filter.getInEvidence()) {
                whereCondition.append(" AND claims.in_evidence = " + filter.getInEvidence() + " ");
            }else{
                whereCondition.append(" AND (claims.in_evidence IS NULL OR claims.in_evidence = " + filter.getInEvidence() + ") ");
            }
        }

        if (filter.getWithContinuation() != null) {
            whereCondition = this.addWithContinuationFilter(filter.getWithContinuation(),whereCondition);
        }

        if (filter.getPoVariation() != null) {
            if(filter.getPoVariation()) {
                whereCondition.append(" AND claims.po_variation =" + filter.getPoVariation() + " ");
            }else{
                whereCondition.append(" AND ( claims.po_variation IS NULL OR claims.po_variation =" + filter.getPoVariation() + ") ");
            }
        }


        if (filter.getTypeAccident() != null) {
            whereCondition.append(" AND claims.type_accident IN (:typeAccidentSet) ");
        }

        if (Util.isNotEmpty(filter.getCompany())) {
            fromCondition.append(" inner join claims_damaged_insurance_info ins on claims.id = ins.id ");
            whereCondition.append(" AND ins.tpl_company ILIKE '%" + filter.getCompany() + "%' ");
        }

        if (filter.getStatus() != null) {
            whereCondition.append(" AND claims.status IN (:statusSet) AND claims.status <> 'DRAFT'");
        } else {
            whereCondition.append(" AND claims.status <> 'DRAFT'");
        }

        if (filter.getFlow() != null) {
            whereCondition.append(" AND claims.type IN (:flowSet) ");
        }

        if (filter.getWaitingForNumberSx() != null) {
            fromCondition.append(" left join claims_from_company fc on claims.id = fc.id ");
            if (filter.getWaitingForNumberSx()) {
                whereCondition.append(" AND (fc.number_sx IS NULL OR fc.number_sx = '') ");
            } else{
                whereCondition.append(" AND (fc.number_sx IS NOT NULL AND fc.number_sx != '') ");
            }
        }

        if(filter.getWaitingForAuthority() != null){
            fromCondition.append(" inner join claims_to_claims_authority ca on claims.id = ca.claims_id inner join claims_authority cat on ca.authority_id = cat.id ");
            if(filter.getWaitingForAuthority()) {
                whereCondition.append(" AND (cat.is_invoiced is null or cat.is_invoiced <> 'true') AND (cat.event_type is null or cat.event_type <> 'REJECT') ");
            }else
                whereCondition.append(" AND claims.status = 'WAITING_FOR_AUTHORITY' AND cat.is_invoiced = true ");
        }

        sql.append(fromCondition);
        sql.append(whereCondition);

        String fieldToOrder = null;
        if (orderBy != null)
            fieldToOrder = mappOrder.get(orderBy);
        if (fieldToOrder != null && asc != null) {
            if (asc) {
                sql.append(" ORDER BY " + fieldToOrder + " ASC");
            } else {
                sql.append(" ORDER BY " + fieldToOrder + " DESC");
            }

        } else if (fieldToOrder != null) {
            sql.append(" ORDER BY " + fieldToOrder + " DESC");
        }

        sql.append(" LIMIT " + pageSize + " OFFSET " + (page - 1) * pageSize);

        Query query = entityManager.createNativeQuery(sql.toString(), ClaimsNewEntity.class);
        System.out.println(sql.toString());


        if (filter.getTypeAccident() != null)
            query.setParameter("typeAccidentSet", filter.getTypeAccident());
        if (filter.getFlow() != null)
            query.setParameter("flowSet", filter.getFlow());
        if (filter.getStatus() != null)
            query.setParameter("statusSet", filter.getStatus());
        if (filter.getClientListId() != null)
            query.setParameter("clientIdList", filter.getClientListId());

      //  return query.getResultList();

        return Specification.where(distinct())
                .and(specificationByExample)
                .and(includeDeleted ? null : specificationByDeletedAtNull)
                .and(specificationByPoFilter)
                .and(specificationByGroupedStatus)
                .and(specificationByApproved)
                .and(specificationByRejectedGroup)
                .and(specificationWorkingChannellingWorking)
                .and(specificationByCommodityIdList)
                .and(specificationByUsernameAndUserGroup)
                .and(specificationByDossierExcludeStatus)
                .and(specificationByDossierDeletedAtNull);
    }
*/

  /*  public Page<SearchDashboardClaimsView> findClaimsForPaginationWithoutDraftAndTheft(ClaimsRequestFilterViewDTO filter) {


       // Sort sort = pageable.getSort();

        Specification<SearchDashboardClaimsView> specificationQuery = this.getWorkingsBySearchFiltersAndDeletedAt(filter);


        CriteriaQuery<SearchDashboardClaimsView> criteriaSelectQuery = completeDossier
                ? this.buildCompleteDossierSelectQuery(specificationQuery, sort)
                : this.buildDossierSelectQuery(specificationQuery, sort);

        Query query = entityManager.createQuery(criteriaSelectQuery);
        query.setFirstResult(page * pageSize);
        query.setMaxResults(pageSize);

        List<SearchDashboardClaimsView> resultQuery = query.getResultList();

      *//*  CriteriaQuery<Long> criteriaCountQuery = this.buildDossierCountQuery(specificationQuery);
        Query countQuery = entityManager.createQuery(criteriaCountQuery);*//*

        long count = (long) countQuery.getSingleResult();

        return new PageImpl<>(resultQuery, PageRequest.of(page, pageSize, sort), count);
    }


    private CriteriaQuery<SearchDashboardClaimsView> buildMultiSelectQuery(Specification<SearchDashboardClaimsView> inputSpec){

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<SearchDashboardClaimsView> query = cb.createQuery(SearchDashboardClaimsView.class);
        Root<SearchDashboardClaimsView> root = query.from(SearchDashboardClaimsView.class);
        query.multiselect(root.get(SearchDossierWorkingCompleteView_.dossierId),
                root.get(SearchDossierWorkingCompleteView_.workingClaims),
                root.get(SearchDossierWorkingCompleteView_.workingCommodityId),
                root.get(SearchDossierWorkingCompleteView_.invoicingOutput),
                root.get(SearchDossierWorkingCompleteView_.workingStatus),
                root.get(SearchDossierWorkingCompleteView_.flowType)).distinct(true);
        query.where(inputSpec.toPredicate(root,query,cb));

        return query;
    }*/

    /*
     * se withContinuation è
     *      true =>
     *          devo prendere tutti i record che hanno almeno un record in
     *          anti_theft_request il cui campo validate è a false
     *      false =>
     *          devo prendere tutti i record che non hanno nessun record
     *          in anti_theft_request con validate a false
     */
    private StringBuffer addWithContinuationFilter(Boolean withContinuation, StringBuffer where) {
        if (withContinuation != null) {
            StringBuffer atrSql;
            StringBuffer atrSelect = new StringBuffer(" SELECT claim_id ");
            StringBuffer atrFrom = new StringBuffer(" FROM anti_theft_request ");
            StringBuffer atrWhere = new StringBuffer(" WHERE 1=1 ");
            atrWhere.append(" AND (crash_report->>'validate')\\:\\:boolean = false");

            atrSql = new StringBuffer();
            atrSql
                    .append("(")
                    .append(atrSelect)
                    .append(atrFrom)
                    .append(atrWhere)
                    .append(")")
            ;

            if( withContinuation ) {
                where
                        .append(" AND ( ")
                        .append("   claims.with_continuation = true")
                        .append("   OR claims.id IN " + atrSql)
                        .append(" ) ")
                        .append("AND claims.status <> 'DRAFT'")

                ;
            } else {
                where
                        .append(" AND claims.id NOT IN " + atrSql)
                        .append(" AND claims.with_continuation = false ")
                ;
            }
        }
        return where;
    }
}
