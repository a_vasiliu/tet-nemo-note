package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.controller.payload.response.ClosingResponseV1;
import com.doing.nemo.claims.dto.ClaimsExportFilter;
import com.doing.nemo.claims.entity.ClaimsNewEntity;
import com.doing.nemo.claims.entity.enumerated.myAld.MyAldInsertedByEnum;
import org.springframework.stereotype.Repository;
import java.math.BigInteger;
import java.util.List;

@Repository
public interface ClaimsNewRepositoryCustom {
    List<Object[]> getStatsClaimsEvidence();
    List<Object[]> getStatsClaimsEvidenceWithStatusWaitingForAuthority();
    List<Object[]> getStatsClaimsEvidenceWithStatusWaitingForNumberSx();
    List<Object[]> getStatsClaimsEvidencePoVariation();
    BigInteger countClaimsForPagination(Boolean inEvidence, Long practiceId, List<String> flow, List<Long> clientIdList, String plate, List<String> status, String locator, String dateAccidentFrom, String dateAccidentTo, List<String> typeAccident, Boolean asc, String orderBy, Integer page, Integer pageSize, Boolean withContinuation, String company, Boolean withReceptions, String clientName, Boolean poVariation, Long fleetVehicleId);
    List<ClaimsNewEntity> findClaimsForPagination(Boolean inEvidence, Long practiceId, List<String> flow, List<Long> clientIdList, String plate, List<String> status, String locator, String dateAccidentFrom, String dateAccidentTo, List<String> typeAccident, Boolean asc, String orderBy, Integer page, Integer pageSize, Boolean withContinuation, String company, Boolean withReceptions, String clientName, Boolean poVariation, Long fleetVehicleId);
    BigInteger countClaimsForPaginationWithoutDraftAndTheft(Boolean inEvidence, Long practiceId, List<String> flow, List<Long> clientIdList, String plate, List<String> status, String locator, String dateAccidentFrom, String dateAccidentTo, List<String> typeAccident, Boolean asc, String orderBy, Integer page, Integer pageSize, Boolean withContinuation, String company, String clientName, Boolean poVariation, Boolean waitingForNumberSx, Long fleetVehicleId, Boolean waitingForAuthority);
    List<ClaimsNewEntity> findClaimsForPaginationWithoutDraftAndTheft(Boolean inEvidence, Long practiceId, List<String> flow, List<Long> clientIdList, String plate, List<String> status, String locator, String dateAccidentFrom, String dateAccidentTo, List<String> typeAccident, Boolean asc, String orderBy, Integer page, Integer pageSize, Boolean withContinuation, String company, String clientName, Boolean poVariation, Boolean waitingForNumberSx, Long fleetVehicleId, Boolean waitingForAuthority);
    BigInteger countClaimsForPaginationTheftOperator(Boolean inEvidence, Long practiceId, List<String> flow, String clientId, String plate, List<String> status, String locator, String dateAccidentFrom, String dateAccidentTo, List<String> typeAccident, Boolean asc, String orderBy, Integer page, Integer pageSize, Boolean withContinuation, String company, String clientName, Boolean poVariation, Boolean waitingForNumberSx, Long fleetVehicleId, Boolean waitingForAuthority);
    List<ClaimsNewEntity> findClaimsForPaginationTheftOperator(Boolean inEvidence, Long practiceId, List<String> flow, String clientId, String plate, List<String> status, String locator, String dateAccidentFrom, String dateAccidentTo, List<String> typeAccident, Boolean asc, String orderBy, Integer page, Integer pageSize, Boolean withContinuation, String company, String clientName, Boolean poVariation, Boolean waitingForNumberSx, Long fleetVehicleId, Boolean waitingForAuthority);
    List<ClaimsNewEntity> getClaimsAttachedToAuthorityPractice(String authorityDossierId, String authorityWorkingId, List<String> claimsList);
    List<ClaimsNewEntity> getClaimsToAttachAuthority(String authorityDossierId, String authorityWorkingId, List<String> claimsIdList);
    List<ClaimsNewEntity> getClaimsToDetattachAuthority(String authorityDossierId, String authorityWorkingId, List<String> claimsIdList);
    List<ClaimsNewEntity> getClaimsToCheckDuplicateAuthority(String authorityDossierId, String authorityWorkingId);
    BigInteger countClaimsToCheckDuplicateAuthority(String authorityDossierId, String authorityWorkingId);
    List<ClaimsNewEntity> findClaimsForPaginationAuthority(List<String> plate, String chassis, Integer page, Integer pageSize, Boolean isFirstCheck);
    BigInteger countClaimsForPaginationAuthority(List<String> plate, String chassis, Boolean isFirstCheck);
    List<ClaimsNewEntity> getClaimsToInvoce();
    List<ClaimsNewEntity> getClaimsToAutomaticEntrust(Boolean skipMigratedClaimsAutomaticEntrust);
    BigInteger countClaimsForAcclaimsPagination(Boolean inEvidence, Long practiceId, String flow, String clientId, String plate, String status, String locator, String dateAccidentFrom, String dateAccidentTo, List<String> typeAccident, Boolean asc, String orderBy, Integer page, Integer pageSize, Boolean withContinuation, String company, Boolean withReceptions, String clientName);
    List<ClaimsNewEntity> getAcclaimsClaimsEntity(Boolean inEvidence, Long practiceId, String flow, String clientId, String plate,
                                                  String status, String locator, String dateAccidentFrom,
                                                  String dateAccidentTo, String typeAccident, Boolean asc, String orderBy, int page, int pageSize, Boolean withContinuation, String company, Boolean withReceptions, String clientName);
    List<ClaimsNewEntity> getMsaClaimsEntity(Boolean inEvidence, Long practiceId, String flow, String clientId, String plate,
                                             String status, String locator, String dateAccidentFrom,
                                             String dateAccidentTo, String typeAccident, Boolean asc, String orderBy, int page, int pageSize, Boolean withContinuation, String company, Boolean withReceptions, String clientName);
    BigInteger countClaimsForMsaPagination(Boolean inEvidence, Long practiceId, String flow, String clientId, String plate, String status, String locator, String dateAccidentFrom, String dateAccidentTo, List<String> typeAccident, Boolean asc, String orderBy, Integer page, Integer pageSize, Boolean withContinuation, String company, Boolean withReceptions, String clientName);
    List<ClaimsNewEntity> findClaimsForPaginationWithoutDraft(Boolean inEvidence, Long practiceId, List<String> flow, List<String> clientIdList, String plate, List<String> status, String locator, String dateAccidentFrom, String dateAccidentTo, List<String> typeAccident, Boolean asc, String orderBy, Integer page, Integer pageSize, Boolean withContinuation, String company, Boolean withReceptions, String clientName, Boolean poVariation, Long fleetVehicleId);
    List<ClaimsNewEntity> findClaimsByDamagedPlateAndCounterpartyPlateName(String damagedPlate, String counterpartyPlate, String counterpartyFirstName, String counterpartyLastName, String dateAccidentFrom, String dateAccidentTo);
    void updateClaimsEvidence();
    void deleteClaimsInBozzaRange(Integer range);
    List<ClaimsNewEntity> findNotClosedTheftWhitPlate(String plate, String data);
    //integrazione con closing
    List<ClosingResponseV1> findClosing(String idContract);
    //integrazione myald
    List<ClaimsNewEntity> findClaimsForPaginationWithoutDraftMyAldFleetManager(String contractPlateNumberSx, List<String> clientIdList, String clientId, List<String> status, String dateAccidentFrom, String dateAccidentTo, String dateCreatedFrom, String dateCreatedTo, List<MyAldInsertedByEnum> insertedByList, String userId, Boolean asc, String orderBy, Integer page, Integer pageSize);
    BigInteger countClaimsForPaginationWithoutDraftMyAldFleetManager(String contractPlateNumberSx, List<String> clientIdList,String clientId, List<String> status, String dateAccidentFrom, String dateAccidentTo, String dateCreatedFrom, String dateCreatedTo, List<MyAldInsertedByEnum> insertedByList, String userId);
    List<ClaimsNewEntity> findClaimsForPaginationWithoutDraftMyAldDriver(List<String> plates,String contractPlateNumberSx, List<String> status,String dateAccidentFrom, String dateAccidentTo, String dateCreatedFrom, String dateCreatedTo, Boolean asc, String orderBy, Integer page, Integer pageSize, String userId, List<MyAldInsertedByEnum> insertedByList, Boolean isInclusive);
    BigInteger countClaimsForPaginationWithoutDraftMyAldDriver(List<String> plates,String contractPlateNumberSx, List<String> status,String dateAccidentFrom, String dateAccidentTo, String dateCreatedFrom, String dateCreatedTo,  String userId, List<MyAldInsertedByEnum> insertedByList, Boolean isInclusive);
    //integrazione pratiche auto
    List<ClaimsNewEntity> findTheftForLossPossessionNotMigrated(String goLiveStepZeroActivationDate);
    List<ClaimsNewEntity> findTheftForLossPossessionMigrated(String goLiveStepZeroActivationDate);
    List<Object[]> getClaimsStatsBasedStatus(Boolean inEvidence, Boolean withContinuation);
    List<Object[]> getStatsClaimWaitingForNumberSx(Boolean inEvidence, Boolean withContinuation);
    List<Object[]> getStatsClaimsWaitingForAuthority(Boolean inEvidence, Boolean withContinuation);
    List<Object[]> getStatsClaimsPoVariation(Boolean inEvidence, Boolean withContinuation);

    List<Object[]> getClaimsStatsTotalCounter(Boolean inEvidence, Boolean withContinuation);

    BigInteger countExportClaims(ClaimsExportFilter filter);

    List<ClaimsNewEntity> exportClaimsByPages(ClaimsExportFilter filter, Integer page, Integer pageSize);



}
