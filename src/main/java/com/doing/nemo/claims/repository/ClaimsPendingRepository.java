package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.entity.ClaimsPendingEntity;
import com.doing.nemo.claims.entity.settings.PersonalDataEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.List;

@Repository
public interface ClaimsPendingRepository extends JpaRepository<ClaimsPendingEntity, Long> {

    @Query("SELECT cp FROM ClaimsPendingEntity cp  WHERE cp.damagedDriversPlate = :plate")
    List<ClaimsPendingEntity> findByPlate(@Param("plate") String plate);

    @Query("SELECT cp FROM ClaimsPendingEntity cp WHERE cp.damagedDriversPlate = :plate AND cp.dateAccident BETWEEN :startDate AND :endDate")
    List<ClaimsPendingEntity> findByPlateAndBetweenDateAccident(@Param("plate") String plate, @Param("startDate") Instant startDate, @Param("endDate") Instant endDate);

    @Query("SELECT cp FROM ClaimsPendingEntity cp  WHERE cp.practiceId = :practice")
    List<ClaimsPendingEntity> findByPracticeId(@Param("practice") Long practice);

}
