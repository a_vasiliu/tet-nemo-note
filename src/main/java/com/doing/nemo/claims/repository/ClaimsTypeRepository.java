package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.settings.ClaimsTypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface ClaimsTypeRepository extends JpaRepository<ClaimsTypeEntity, UUID> {

    @Query(value = "SELECT c FROM ClaimsTypeEntity c WHERE c.claimsAccidentType = :claimsAccidentType")
    ClaimsTypeEntity getClaimsTypeEntityByClaimsAccidentType(@Param("claimsAccidentType") DataAccidentTypeAccidentEnum claimsAccidentType);



    @Query(nativeQuery = true,value = "SELECT tab.type, tab.claims_accident_type, tab.flow, tab.tipo FROM ( " +
            " SELECT claims_type.type, claims_type.claims_accident_type, custom_type_risk.flow , 'FLOW_CUSTOMER_RISK' tipo " +
            " FROM claims_type  INNER JOIN custom_type_risk  ON claims_type.id = custom_type_risk.claims_type_id "+
            "INNER JOIN personal_data     ON custom_type_risk.personal_data_id = personal_data.id "+
            " where personal_data.customer_id = :customerId "+
            " UNION ALL "+
            " SELECT claims_type.type, claims_type.claims_accident_type, flow_contract_type.flow , 'FLOW_CONTRACT_TYPE' tipo "+
            " FROM claims_type  INNER JOIN flow_contract_type ON claims_type.id = flow_contract_type.claims_type_entity_id "+
            " INNER JOIN contract_type      ON flow_contract_type.contract_type_entity_id = contract_type.id "+
            " where contract_type.cod_contract_type = :contractType  ) tab"

    )
    List<Object[]> getFlowTypeChecker(@Param("customerId") String customerId, @Param("contractType") String contractType);


}
