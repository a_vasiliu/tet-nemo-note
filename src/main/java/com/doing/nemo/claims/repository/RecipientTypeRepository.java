package com.doing.nemo.claims.repository;


import com.doing.nemo.claims.entity.enumerated.ClaimsRepairEnum;
import com.doing.nemo.claims.entity.enumerated.RecipientEnum;
import com.doing.nemo.claims.entity.settings.RecipientTypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface RecipientTypeRepository extends JpaRepository<RecipientTypeEntity, UUID> {
    @Query("SELECT r FROM RecipientTypeEntity r WHERE r.recipientType = :recipientType")
    RecipientTypeEntity searchRecipientTypeByType(@Param("recipientType") RecipientEnum recipientType);

    @Query("SELECT r FROM RecipientTypeEntity r WHERE r.typeComplaint = :typeComplaint")
    List<RecipientTypeEntity> searchRecipientTypeByTypeComplaint(@Param("typeComplaint") ClaimsRepairEnum typeComplaint);
}
