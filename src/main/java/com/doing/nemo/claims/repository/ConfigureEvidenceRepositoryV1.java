package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.entity.settings.ConfigureEvidenceEntity;
import com.doing.nemo.claims.entity.settings.ConfigureEvidenceEntity_;
import com.doing.nemo.claims.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class ConfigureEvidenceRepositoryV1 {

    @PersistenceContext
    @Autowired
    private EntityManager em;

    public List<ConfigureEvidenceEntity> findConfigureEvidencesFiltered(Integer page, Integer pageSize, String practicalStateIt, String practicalStateEn, Long daysOfStayInTheState, Boolean isActive, String orderBy, Boolean asc) {
        Map<String, String> mappOrder = new HashMap<>();
        mappOrder.put("practical_state_it", "practicalStateIt");
        mappOrder.put("practical_state_en", "practicalStateEn");
        mappOrder.put("days_of_stay_in_the_state", "daysOfStayInTheState");
        mappOrder.put("is_active", "isActive");

        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<ConfigureEvidenceEntity> configureEvidenceCriteriaQuery = criteriaBuilder.createQuery(ConfigureEvidenceEntity.class);
        Root<ConfigureEvidenceEntity> configureEvidenceRoot = configureEvidenceCriteriaQuery.from(ConfigureEvidenceEntity.class);

        configureEvidenceCriteriaQuery.select(configureEvidenceRoot);

        //Constructing list of parameters
        List<Predicate> predicates = new ArrayList<>();

        if (Util.isNotEmpty(practicalStateIt)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    configureEvidenceRoot.get(ConfigureEvidenceEntity_.practicalStateIt)), "%"+practicalStateIt.toUpperCase()+"%"));
        }

        if (Util.isNotEmpty(practicalStateEn)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    configureEvidenceRoot.get(ConfigureEvidenceEntity_.practicalStateEn)), "%"+practicalStateEn.toUpperCase()+"%"));
        }

        if (daysOfStayInTheState != null) {
            predicates.add(criteriaBuilder.equal(
                    configureEvidenceRoot.get(ConfigureEvidenceEntity_.daysOfStayInTheState), daysOfStayInTheState));
        }
        if (isActive != null) {
            predicates.add(criteriaBuilder.equal(
                    configureEvidenceRoot.get(ConfigureEvidenceEntity_.isActive), isActive));
        }


        configureEvidenceCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));

        //List<Order> orderByList = new LinkedList<>();
        String fieldToOrder = null;
        if(orderBy != null)
            fieldToOrder = mappOrder.get(orderBy);
        if(fieldToOrder != null && asc != null){
            if (asc) {
                configureEvidenceCriteriaQuery.orderBy(criteriaBuilder.asc(configureEvidenceRoot.get(fieldToOrder)));
            } else {
                configureEvidenceCriteriaQuery.orderBy(criteriaBuilder.desc(configureEvidenceRoot.get(fieldToOrder)));
            }

        } else if(orderBy != null) {
            configureEvidenceCriteriaQuery.orderBy(criteriaBuilder.desc(configureEvidenceRoot.get(fieldToOrder)));
        }

        List<ConfigureEvidenceEntity> configureEvidenceEntityList = em.createQuery(configureEvidenceCriteriaQuery).setFirstResult((page-1)*pageSize).setMaxResults(pageSize).getResultList();
        return configureEvidenceEntityList;

    }
    
    public Long countConfigureEvidenceFiltered(String practicalStateIt, String practicalStateEn, Long daysOfStayInTheState, Boolean isActive) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<Long> configureEvidenceCriteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<ConfigureEvidenceEntity> configureEvidenceRoot = configureEvidenceCriteriaQuery.from(ConfigureEvidenceEntity.class);

        configureEvidenceCriteriaQuery.select(criteriaBuilder.count(configureEvidenceRoot));

        //Constructing list of parameters
        List<Predicate> predicates = new ArrayList<>();

        if (Util.isNotEmpty(practicalStateIt)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    configureEvidenceRoot.get(ConfigureEvidenceEntity_.practicalStateIt)), "%"+practicalStateIt.toUpperCase()+"%"));
        }

        if (Util.isNotEmpty(practicalStateEn)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    configureEvidenceRoot.get(ConfigureEvidenceEntity_.practicalStateEn)), "%"+practicalStateEn.toUpperCase()+"%"));
        }

        if (daysOfStayInTheState != null) {
            predicates.add(criteriaBuilder.equal(
                    configureEvidenceRoot.get(ConfigureEvidenceEntity_.daysOfStayInTheState), daysOfStayInTheState));
        }
        if (isActive != null) {
            predicates.add(criteriaBuilder.equal(
                    configureEvidenceRoot.get(ConfigureEvidenceEntity_.isActive), isActive));
        }

        configureEvidenceCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));
        Query sql = em.createQuery(configureEvidenceCriteriaQuery);
        return (Long) sql.getSingleResult();

    }


    





}
