package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.entity.settings.LockEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface LockRepository extends JpaRepository<LockEntity, String> {

    @Query("SELECT l FROM LockEntity l WHERE l.now = (SELECT MAX (l.now) FROM LockEntity l WHERE l.claimId = :claimId AND l.userId = :userId)")
    LockEntity searchByUserIdAndClaimId(@Param("userId") String userId, @Param("claimId") String claimId);

    @Query("SELECT l FROM LockEntity l WHERE l.now = (SELECT MAX (l.now) FROM LockEntity l WHERE l.claimId = :claimId)")
    LockEntity searchByClaimId(@Param("claimId") String claimId);

    @Transactional
    @Modifying
    @Query("DELETE FROM LockEntity l where l.now < current_timestamp")
    void serchUnlockableClaims();


}
