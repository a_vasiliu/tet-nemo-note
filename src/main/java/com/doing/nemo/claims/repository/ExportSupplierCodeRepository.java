package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.entity.settings.ExportSupplierCodeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface ExportSupplierCodeRepository extends JpaRepository<ExportSupplierCodeEntity, UUID> {
    @Query("SELECT e FROM ExportSupplierCodeEntity e WHERE e.vatNumber = :vatNumber AND e.isActive = true")
    ExportSupplierCodeEntity searchExportSupplierCodeByVatNumber(@Param("vatNumber") String vatNumber);

    @Query("SELECT e FROM ExportSupplierCodeEntity e WHERE e.vatNumber = :vatNumber")
    List<ExportSupplierCodeEntity> checkDuplicateExportSupplierCode(@Param("vatNumber") String vatNumber);

    @Query("SELECT e FROM ExportSupplierCodeEntity e WHERE e.vatNumber = :vatNumber AND e.id <> :id")
    ExportSupplierCodeEntity searchExportSupplierCodeByVatNumberWithDifferentId(@Param("vatNumber") String vatNumber, @Param("id") UUID id);
}
