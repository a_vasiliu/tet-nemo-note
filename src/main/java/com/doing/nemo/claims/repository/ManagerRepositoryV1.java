package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.entity.enumerated.ClaimsRepairEnum;
import com.doing.nemo.claims.entity.settings.ManagerEntity;
import com.doing.nemo.claims.entity.settings.ManagerEntity_;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class ManagerRepositoryV1 {

    @PersistenceContext
    @Autowired
    private EntityManager em;

    public List<ManagerEntity> findManagersFiltered(Integer page, Integer pageSize, ClaimsRepairEnum claimsRepairEnum, String name, String prov, String phone, String fax, String email, String externalExportId, Boolean isActive, String orderBy, Boolean asc, String address, String zipCode, String locality, String country, Long code) {
        String query = new String();
        Map<String, String> mappOrder = new HashMap<>();
        mappOrder.put("name", "name");
        mappOrder.put("prov", "prov");
        mappOrder.put("address", "address");
        mappOrder.put("zip_code", "zipCode");
        mappOrder.put("locality", "locality");
        mappOrder.put("country", "country");
        mappOrder.put("phone", "phone");
        mappOrder.put("fax", "fax");
        mappOrder.put("email", "email");
        mappOrder.put("external_export_id", "externalExportId");
        mappOrder.put("is_active", "isActive");
        mappOrder.put("code", "code");
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<ManagerEntity> managerCriteriaQuery = criteriaBuilder.createQuery(ManagerEntity.class);
        Root<ManagerEntity> managerRoot = managerCriteriaQuery.from(ManagerEntity.class);
        managerCriteriaQuery.select(managerRoot);
        //Constructing list of parameters
        List<Predicate> predicates = new ArrayList<>();

        if(name != null && !name.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    managerRoot.get(ManagerEntity_.name)), "%"+name.toUpperCase()+"%"));
        }

        if(prov != null && !prov.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    managerRoot.get(ManagerEntity_.prov)), "%"+prov.toUpperCase()+"%"));
        }

        if(address != null && !address.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    managerRoot.get(ManagerEntity_.address)), "%"+address.toUpperCase()+"%"));
        }

        if(zipCode != null && !zipCode.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    managerRoot.get(ManagerEntity_.zipCode)), "%"+zipCode.toUpperCase()+"%"));
        }

        if(locality != null && !locality.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    managerRoot.get(ManagerEntity_.locality)), "%"+locality.toUpperCase()+"%"));
        }

        if(country != null && !country.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    managerRoot.get(ManagerEntity_.country)), "%"+country.toUpperCase()+"%"));
        }

        if(phone != null && !phone.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    managerRoot.get(ManagerEntity_.phone)), "%"+phone.toUpperCase()+"%"));
        }

        if(fax != null && !fax.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    managerRoot.get(ManagerEntity_.fax)), "%"+fax.toUpperCase()+"%"));
        }

        if(email != null && !email.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    managerRoot.get(ManagerEntity_.email)), "%"+email.toUpperCase()+"%"));
        }

        if(externalExportId != null && !externalExportId.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    managerRoot.get(ManagerEntity_.externalExportId)), "%"+externalExportId.toUpperCase()+"%"));
        }

        if (isActive != null) {
            predicates.add(criteriaBuilder.equal(
                    managerRoot.get(ManagerEntity_.isActive), isActive));
        }

        managerCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));

        String fieldToOrder = null;
        if(orderBy != null)
            fieldToOrder = mappOrder.get(orderBy);
        if(fieldToOrder != null && asc != null){
            if (asc) {
                managerCriteriaQuery.orderBy(criteriaBuilder.asc(managerRoot.get(fieldToOrder)));
            } else {
                managerCriteriaQuery.orderBy(criteriaBuilder.desc(managerRoot.get(fieldToOrder)));
            }

        } else if(fieldToOrder != null) {
            managerCriteriaQuery.orderBy(criteriaBuilder.desc(managerRoot.get(fieldToOrder)));
        }

        List<ManagerEntity> managerEntityList = em.createQuery(managerCriteriaQuery).setFirstResult((page-1)*pageSize).setMaxResults(pageSize).getResultList();
        return managerEntityList;
    }
    
    public Long countManagerFiltered(ClaimsRepairEnum claimsRepairEnum, String name, String prov, String phone, String fax, String email, String externalExportId, Boolean isActive, String address, String zipCode, String locality, String country, Long code) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<Long> managerCriteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<ManagerEntity> managerRoot = managerCriteriaQuery.from(ManagerEntity.class);
        managerCriteriaQuery.select(criteriaBuilder.count(managerRoot));
        //Constructing list of parameters
        List<Predicate> predicates = new ArrayList<>();

        if(name != null && !name.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    managerRoot.get(ManagerEntity_.name)), "%"+name.toUpperCase()+"%"));
        }

        if(prov != null && !prov.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    managerRoot.get(ManagerEntity_.prov)), "%"+prov.toUpperCase()+"%"));
        }

        if(address != null && !address.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    managerRoot.get(ManagerEntity_.address)), "%"+address.toUpperCase()+"%"));
        }

        if(zipCode != null && !zipCode.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    managerRoot.get(ManagerEntity_.zipCode)), "%"+zipCode.toUpperCase()+"%"));
        }

        if(locality != null && !locality.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    managerRoot.get(ManagerEntity_.locality)), "%"+locality.toUpperCase()+"%"));
        }

        if(country != null && !country.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    managerRoot.get(ManagerEntity_.country)), "%"+country.toUpperCase()+"%"));
        }

        if(phone != null && !phone.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    managerRoot.get(ManagerEntity_.phone)), "%"+phone.toUpperCase()+"%"));
        }

        if(fax != null && !fax.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    managerRoot.get(ManagerEntity_.fax)), "%"+fax.toUpperCase()+"%"));
        }

        if(email != null && !email.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    managerRoot.get(ManagerEntity_.email)), "%"+email.toUpperCase()+"%"));
        }

        if(externalExportId != null && !externalExportId.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    managerRoot.get(ManagerEntity_.externalExportId)), "%"+externalExportId.toUpperCase()+"%"));
        }

        if (isActive != null) {
            predicates.add(criteriaBuilder.equal(
                    managerRoot.get(ManagerEntity_.isActive), isActive));
        }

        managerCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));
        Query sql = em.createQuery(managerCriteriaQuery);
        return (Long)sql.getSingleResult();
    }


    





}
