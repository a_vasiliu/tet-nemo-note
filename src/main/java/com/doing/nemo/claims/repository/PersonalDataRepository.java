package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.entity.settings.PersonalDataEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface PersonalDataRepository extends JpaRepository<PersonalDataEntity, UUID> {

    @Query("SELECT p FROM PersonalDataEntity p  WHERE p.customerId = :customerId")
    PersonalDataEntity findByCustomerId(@Param("customerId") String customerId);

    @Query(value = "SELECT p FROM PersonalDataEntity p WHERE p.id <> :id AND p.customerId = :customerId")
    PersonalDataEntity findByCustomerIdWithDifferentUUID (@Param("id") UUID id, @Param("customerId") String customerId);

}
