package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsFlowEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.doing.nemo.claims.entity.settings.ExportStatusTypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface ExportStatusTypeRepository extends JpaRepository<ExportStatusTypeEntity, UUID> {

    @Query("SELECT es FROM ExportStatusTypeEntity es WHERE es.statusType = :claimsStatusType AND es.flowType = :flowType AND es.isActive = true")
    ExportStatusTypeEntity getStatusTypeByClaimsStatusAndFlowType(@Param("claimsStatusType")ClaimsStatusEnum claimsStatus, @Param("flowType")ClaimsFlowEnum flowType);

    @Query("SELECT es FROM ExportStatusTypeEntity es WHERE es.statusType = :claimsStatusType AND es.flowType = :flowType")
    List<ExportStatusTypeEntity> checkDuplicateExportStatusType(@Param("claimsStatusType")ClaimsStatusEnum claimsStatus, @Param("flowType")ClaimsFlowEnum flowType);

}
