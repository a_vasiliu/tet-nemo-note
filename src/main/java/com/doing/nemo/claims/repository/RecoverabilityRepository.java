package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.settings.RecoverabilityEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.UUID;

public interface RecoverabilityRepository extends JpaRepository<RecoverabilityEntity, UUID> {
    @Query("SELECT r FROM RecoverabilityEntity r inner join r.claimsType c WHERE r.management = :management AND c.claimsAccidentType = :typeClaim AND r.isActive=true")
    RecoverabilityEntity searchRecoverabilyByFlowAndClaimType(@Param("management") String management, @Param("typeClaim") DataAccidentTypeAccidentEnum typeClaim);

    @Query("SELECT r FROM RecoverabilityEntity r inner join r.claimsType c WHERE c.claimsAccidentType = :typeClaim")
    RecoverabilityEntity searchRecoverabilyByClaimType(@Param("typeClaim") DataAccidentTypeAccidentEnum typeClaim);
}
