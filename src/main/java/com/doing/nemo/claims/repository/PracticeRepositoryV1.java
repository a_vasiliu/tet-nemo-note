package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.entity.CounterpartyEntity;
import com.doing.nemo.claims.entity.enumerated.PracticeStatusEnum;
import com.doing.nemo.claims.entity.enumerated.PracticeTypeEnum;
import com.doing.nemo.claims.entity.settings.PracticeEntity;
import com.doing.nemo.claims.entity.settings.PracticeEntity_;
import com.doing.nemo.claims.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.*;
import java.math.BigInteger;
import java.time.Instant;
import java.util.*;

import static com.doing.nemo.claims.entity.jsonb.JsonbProcessingFunctions.JSONB_EXTRACT_PATH_TEXT;


@Repository
public class PracticeRepositoryV1 {

    @PersistenceContext
    @Autowired
    private EntityManager em;


    public List<PracticeEntity> findPracticesFiltered(Integer page, Integer pageSize, Long practiceId, PracticeTypeEnum practiceType, String vehiclePlate, String idContract, String statusList, String fromDate, String toDate , String orderBy, Boolean asc, String fleetVehicleId) {

        Map<String, String> mappOrder = new HashMap<>();
        mappOrder.put("license_plate", "licensePlate");
        mappOrder.put("practice_id", "practiceId");
        mappOrder.put("practice_type", "practiceType");
        mappOrder.put("contract_id", "contractId");
        mappOrder.put("created_at", "createdAt");
        mappOrder.put("status", "status");

        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<PracticeEntity> practiceCriteriaQuery = criteriaBuilder.createQuery(PracticeEntity.class);
        Root<PracticeEntity> practiceRoot = practiceCriteriaQuery.from(PracticeEntity.class);

        practiceCriteriaQuery.select(practiceRoot);

        //Constructing list of parameters
        List<Predicate> predicates = new ArrayList<>();

        if (practiceId != null) {
            predicates.add(criteriaBuilder.equal(
                    practiceRoot.get(PracticeEntity_.practiceId), practiceId));
        }

        if (practiceType != null) {
            predicates.add(criteriaBuilder.equal(
                    practiceRoot.get(PracticeEntity_.practiceType), practiceType));
        }

        if(Util.isNotEmpty(statusList)){
            List<String> statusStringList = Arrays.asList(statusList.split("\\s*,\\s*"));
            List<PracticeStatusEnum> statusTypeList = new LinkedList<>();
            for(String status: statusStringList)
                statusTypeList.add(PracticeStatusEnum.create(status));
            predicates.add(practiceRoot.get(PracticeEntity_.status).in(statusTypeList));
        }

        if(Util.isNotEmpty(vehiclePlate)){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(criteriaBuilder
                    .function(JSONB_EXTRACT_PATH_TEXT.value(),
                            String.class,
                            practiceRoot.get("vehicle"),
                            criteriaBuilder.literal("license_plate"))),"%"+vehiclePlate.toUpperCase()+"%"));
        }

        if(Util.isNotEmpty(fleetVehicleId)){
            predicates.add(criteriaBuilder.equal(criteriaBuilder
                    .function(JSONB_EXTRACT_PATH_TEXT.value(),
                            String.class,
                            practiceRoot.get("vehicle"),
                            criteriaBuilder.literal("fleet_vehicle_id")),fleetVehicleId));
        }

        if(Util.isNotEmpty(idContract)){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(criteriaBuilder
                    .function(JSONB_EXTRACT_PATH_TEXT.value(),
                            String.class,
                            practiceRoot.get("contract"),
                            criteriaBuilder.literal("contract_id"))),"%"+idContract.toUpperCase()+"%"));
        }


        if(Util.isNotEmpty(toDate) && Util.isNotEmpty(fromDate) && toDate.compareTo(fromDate)>0){
            Instant toDateInstant = DateUtil.convertIS08601StringToUTCInstant(toDate);
            predicates.add(criteriaBuilder.lessThanOrEqualTo(
                    practiceRoot.get(PracticeEntity_.createdAt), toDateInstant));

            Instant fromDateInstant = DateUtil.convertIS08601StringToUTCInstant(fromDate);
            predicates.add(criteriaBuilder.greaterThanOrEqualTo(
                    practiceRoot.get(PracticeEntity_.createdAt), fromDateInstant));

        } else if(Util.isNotEmpty(fromDate)){

            Instant fromDateInstant = DateUtil.convertIS08601StringToUTCInstant(fromDate);
            predicates.add(criteriaBuilder.greaterThanOrEqualTo(
                    practiceRoot.get(PracticeEntity_.createdAt), fromDateInstant));

        } else if(Util.isNotEmpty(toDate)){

            Instant toDateInstant = DateUtil.convertIS08601StringToUTCInstant(toDate);
            predicates.add(criteriaBuilder.lessThanOrEqualTo(
                    practiceRoot.get(PracticeEntity_.createdAt), toDateInstant));;

        }

        practiceCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));


        List<Order> orderByList = new LinkedList<>();
        //orderByList.add(criteriaBuilder.asc(practiceRoot.get(PracticeEntity_.practiceId)));
        //orderByList.add(criteriaBuilder.asc(practiceRoot.get(PracticeEntity_.createdAt)));
        practiceCriteriaQuery.orderBy(orderByList);


        String fieldToOrder = null;
        if(orderBy != null) {
            fieldToOrder = mappOrder.get(orderBy);
            if (fieldToOrder != null) {
                if (!fieldToOrder.equalsIgnoreCase("licensePlate") && !fieldToOrder.equalsIgnoreCase("contractId")) {
                    if (asc != null) {
                        if (asc) {
                            practiceCriteriaQuery.orderBy(criteriaBuilder.asc(practiceRoot.get(fieldToOrder)));
                        } else {
                            practiceCriteriaQuery.orderBy(criteriaBuilder.desc(practiceRoot.get(fieldToOrder)));
                        }

                    } else {
                        practiceCriteriaQuery.orderBy(criteriaBuilder.desc(practiceRoot.get(fieldToOrder)));
                    }
                } else if (fieldToOrder.equalsIgnoreCase("contractId")) {
                    Expression exp = criteriaBuilder.function(JSONB_EXTRACT_PATH_TEXT.value(),
                            String.class,
                            practiceRoot.get("contract"),
                            criteriaBuilder.literal("contract_id"));

                    if (asc != null) {
                        if (asc) {
                            practiceCriteriaQuery.orderBy(criteriaBuilder.asc(exp));
                        } else {
                            practiceCriteriaQuery.orderBy(criteriaBuilder.desc(exp));
                        }

                    } else {
                        practiceCriteriaQuery.orderBy(criteriaBuilder.desc(exp));
                    }
                } else if (fieldToOrder.equalsIgnoreCase("licensePlate")) {
                    Expression exp = criteriaBuilder.function(JSONB_EXTRACT_PATH_TEXT.value(),
                            String.class,
                            practiceRoot.get("vehicle"),
                            criteriaBuilder.literal("license_plate"));

                    if (asc != null) {
                        if (asc) {
                            practiceCriteriaQuery.orderBy(criteriaBuilder.asc(exp));
                        } else {
                            practiceCriteriaQuery.orderBy(criteriaBuilder.desc(exp));
                        }

                    } else {
                        practiceCriteriaQuery.orderBy(criteriaBuilder.desc(exp));
                    }
                }
            }
        }

        List<PracticeEntity> practiceEntityList = em.createQuery(practiceCriteriaQuery).setFirstResult((page-1)*pageSize).setMaxResults(pageSize).getResultList();
        return practiceEntityList;

    }


    public Long countPracticeFiltered(Long practiceId, PracticeTypeEnum practiceType, String vehiclePlate, String idContract, String statusList, String fromDate, String toDate, String fleetVehicleId) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<Long> practiceCriteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<PracticeEntity> practiceRoot = practiceCriteriaQuery.from(PracticeEntity.class);

        practiceCriteriaQuery.select(criteriaBuilder.count(practiceRoot));

        //Constructing list of parameters
        List<Predicate> predicates = new ArrayList<>();

        if (practiceId != null) {
            predicates.add(criteriaBuilder.equal(
                    practiceRoot.get(PracticeEntity_.practiceId), practiceId));
        }

        if (practiceType != null) {
            predicates.add(criteriaBuilder.equal(
                    practiceRoot.get(PracticeEntity_.practiceType), practiceType));
        }

        if(fleetVehicleId != null){
            predicates.add(criteriaBuilder.equal(criteriaBuilder
                    .function(JSONB_EXTRACT_PATH_TEXT.value(),
                            String.class,
                            practiceRoot.get("vehicle"),
                            criteriaBuilder.literal("fleet_vehicle_id")),fleetVehicleId));
        }

        if(Util.isNotEmpty(statusList)){
            List<String> statusStringList = Arrays.asList(statusList.split("\\s*,\\s*"));
            List<PracticeStatusEnum> statusTypeList = new LinkedList<>();
            for(String status: statusStringList)
                statusTypeList.add(PracticeStatusEnum.create(status));
            predicates.add(practiceRoot.get(PracticeEntity_.status).in(statusTypeList));
        }

        if(Util.isNotEmpty(vehiclePlate)){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(criteriaBuilder
                    .function(JSONB_EXTRACT_PATH_TEXT.value(),
                            String.class,
                            practiceRoot.get("vehicle"),
                            criteriaBuilder.literal("license_plate"))),"%"+vehiclePlate.toUpperCase()+"%"));
        }

        if(Util.isNotEmpty(idContract)){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(criteriaBuilder
                    .function(JSONB_EXTRACT_PATH_TEXT.value(),
                            String.class,
                            practiceRoot.get("contract"),
                            criteriaBuilder.literal("contract_id"))),"%"+idContract.toUpperCase()+"%"));
        }


        if(Util.isNotEmpty(toDate) && Util.isNotEmpty(fromDate) && toDate.compareTo(fromDate)>0){
            Instant toDateInstant = DateUtil.convertIS08601StringToUTCInstant(toDate);
            predicates.add(criteriaBuilder.lessThanOrEqualTo(
                    practiceRoot.get(PracticeEntity_.createdAt), toDateInstant));

            Instant fromDateInstant = DateUtil.convertIS08601StringToUTCInstant(fromDate);
            predicates.add(criteriaBuilder.greaterThanOrEqualTo(
                    practiceRoot.get(PracticeEntity_.createdAt), fromDateInstant));

        } else if(Util.isNotEmpty(fromDate)){

            Instant fromDateInstant = DateUtil.convertIS08601StringToUTCInstant(fromDate);
            predicates.add(criteriaBuilder.greaterThanOrEqualTo(
                    practiceRoot.get(PracticeEntity_.createdAt), fromDateInstant));

        } else if(Util.isNotEmpty(toDate)){

            Instant toDateInstant = DateUtil.convertIS08601StringToUTCInstant(toDate);
            predicates.add(criteriaBuilder.lessThanOrEqualTo(
                    practiceRoot.get(PracticeEntity_.createdAt), toDateInstant));;

        }

        practiceCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));
        Query sql = em.createQuery(practiceCriteriaQuery);
        return (Long)sql.getSingleResult();
    }



    public List<PracticeEntity> findPracticesPlateAndClaims(String vehiclePlate, UUID claimsId) {

        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<PracticeEntity> practiceCriteriaQuery = criteriaBuilder.createQuery(PracticeEntity.class);
        Root<PracticeEntity> practiceRoot = practiceCriteriaQuery.from(PracticeEntity.class);

        practiceCriteriaQuery.select(practiceRoot);

        //Constructing list of parameters
        List<Predicate> predicates = new ArrayList<>();

        if (claimsId != null) {
            predicates.add(criteriaBuilder.equal(
                    practiceRoot.get(PracticeEntity_.claimsId), claimsId));
        }

        if(Util.isNotEmpty(vehiclePlate)){
            predicates.add(criteriaBuilder.equal(criteriaBuilder.upper(criteriaBuilder
                    .function(JSONB_EXTRACT_PATH_TEXT.value(),
                            String.class,
                            practiceRoot.get("vehicle"),
                            criteriaBuilder.literal("license_plate"))),vehiclePlate.toUpperCase()));
        }


        practiceCriteriaQuery.where(criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()])));

        List<PracticeEntity> practiceEntityList = em.createQuery(practiceCriteriaQuery).getResultList();
        return practiceEntityList;

    }

    public List<PracticeEntity> findPracticesPlateAndClaimsOr(String vehiclePlate, UUID claimsId) {

        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<PracticeEntity> practiceCriteriaQuery = criteriaBuilder.createQuery(PracticeEntity.class);
        Root<PracticeEntity> practiceRoot = practiceCriteriaQuery.from(PracticeEntity.class);

        practiceCriteriaQuery.select(practiceRoot);

        //Constructing list of parameters
        List<Predicate> predicates = new ArrayList<>();

        if (claimsId != null) {
            predicates.add(criteriaBuilder.equal(
                    practiceRoot.get(PracticeEntity_.claimsId), claimsId));
        }

        if(Util.isNotEmpty(vehiclePlate)){
            predicates.add(criteriaBuilder.equal(criteriaBuilder.upper(criteriaBuilder
                    .function(JSONB_EXTRACT_PATH_TEXT.value(),
                            String.class,
                            practiceRoot.get("vehicle"),
                            criteriaBuilder.literal("license_plate"))),vehiclePlate.toUpperCase()));
        }


        practiceCriteriaQuery.where(criteriaBuilder.or(predicates.toArray(new Predicate[predicates.size()])));

        List<PracticeEntity> practiceEntityList = em.createQuery(practiceCriteriaQuery).getResultList();
        return practiceEntityList;

    }


    public List<PracticeEntity> findPracticesPlateClaimsTypeDate(String vehiclePlate, UUID claimsId, PracticeTypeEnum practiceType, String dateClaims, PracticeStatusEnum practiceStatusEnum, Boolean last) {

        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<PracticeEntity> practiceCriteriaQuery = criteriaBuilder.createQuery(PracticeEntity.class);
        Root<PracticeEntity> practiceRoot = practiceCriteriaQuery.from(PracticeEntity.class);

        practiceCriteriaQuery.select(practiceRoot);

        //Constructing list of parameters
        List<Predicate> predicates = new ArrayList<>();

        if (claimsId != null) {
            predicates.add(criteriaBuilder.equal(
                    practiceRoot.get(PracticeEntity_.claimsId), claimsId));
        }

        if (Util.isNotEmpty(vehiclePlate)) {
            predicates.add(criteriaBuilder.equal(criteriaBuilder.upper(criteriaBuilder
                    .function(JSONB_EXTRACT_PATH_TEXT.value(),
                            String.class,
                            practiceRoot.get("vehicle"),
                            criteriaBuilder.literal("license_plate"))),vehiclePlate.toUpperCase()));
        }

        if (practiceType != null) {
            predicates.add(criteriaBuilder.equal(
                    practiceRoot.get(PracticeEntity_.practiceType), practiceType));
        }

        if (practiceStatusEnum != null) {
            predicates.add(criteriaBuilder.equal(
                    practiceRoot.get(PracticeEntity_.status), practiceStatusEnum));
        }

        if(Util.isNotEmpty(dateClaims)){
            Instant dateClaimsInstant = DateUtil.convertIS08601StringToUTCInstant(dateClaims);
            predicates.add(criteriaBuilder.greaterThanOrEqualTo(
                    practiceRoot.get(PracticeEntity_.createdAt), dateClaimsInstant));;
        }

        practiceCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));

        if(last!=null && last){
            practiceCriteriaQuery.orderBy(criteriaBuilder.desc(practiceRoot.get(PracticeEntity_.createdAt)));
        }

        List<PracticeEntity> practiceEntityList = em.createQuery(practiceCriteriaQuery).getResultList();
        return practiceEntityList;

    }

    public List<PracticeEntity> findPracticesAuthorityPagination(Integer page, Integer pageSize, String vehiclePlate, String chassis, List<String> type) {

        StringBuilder withCondition = new StringBuilder("WITH PROC AS (SELECT id, jsonb_array_elements(processing_list) pr FROM practice ) ");
        StringBuilder sql = new StringBuilder("SELECT * FROM (SELECT DISTINCT ON (p.id) p.* FROM practice p LEFT JOIN PROC pr USING(id) " );
        StringBuilder whereCondition = new StringBuilder(" WHERE 1=1 ");

        if (Util.isNotEmpty(vehiclePlate)) {
            whereCondition.append(" AND ( p.vehicle->>'license_plate'  = '" + vehiclePlate + "' ");
        }

        if (Util.isNotEmpty(chassis) && whereCondition.indexOf("AND") > 1) {
            whereCondition.append(" OR p.vehicle->>'chassis_number' = '" + chassis + "' ) ");
        } else if (Util.isNotEmpty(chassis)) {
            whereCondition.append(" AND p.vehicle->>'chassis_number' = '" + chassis + "' ");
        } else if(whereCondition.indexOf("AND") > 1) {
            whereCondition.append(" ) ");
        }

        whereCondition.append(" AND pr->>'processing_type' IN (:typeSet) AND p.practice_type IN ('RELEASE_FROM_SEIZURE', 'RECOVERY', 'GENERIC', 'RETURN_POSSESSION')) pra ");

        sql.append(whereCondition);

        sql.append(" LIMIT " + pageSize + " OFFSET " + (page - 1) * pageSize);

        withCondition.append(sql);

        Query query = em.createNativeQuery(withCondition.toString(), PracticeEntity.class);

        if (type != null)
            query.setParameter("typeSet", type);


        return query.getResultList();

    }

    public BigInteger countPracticesAuthorityPagination(String vehiclePlate, String chassis, List<String> type) {

        StringBuilder withCondition = new StringBuilder("WITH PROC AS (SELECT id, jsonb_array_elements(processing_list) pr FROM practice ) ");
        StringBuilder sql = new StringBuilder("SELECT COUNT(*) FROM (SELECT DISTINCT ON (p.id) p.* FROM practice p LEFT JOIN PROC pr USING(id) " );
        StringBuilder whereCondition = new StringBuilder(" WHERE 1=1 ");

        if (Util.isNotEmpty(vehiclePlate)) {
            whereCondition.append(" AND ( p.vehicle->>'license_plate'  = '" + vehiclePlate + "' ");
        }

        if (Util.isNotEmpty(chassis) && whereCondition.indexOf("AND") > 1) {
            whereCondition.append(" OR p.vehicle->>'chassis_number' = '" + chassis + "' )  ");
        } else if (Util.isNotEmpty(chassis)) {
            whereCondition.append(" AND p.vehicle->>'chassis_number' = '" + chassis + "' ");
        } else if(whereCondition.indexOf("AND") > 1){
            whereCondition.append(" ) ");
        }

        whereCondition.append(" AND pr->>'processing_type' IN (:typeSet) AND p.practice_type IN ('RELEASE_FROM_SEIZURE', 'RECOVERY', 'GENERIC', 'RETURN_POSSESSION')) pra ");
        sql.append(whereCondition);
        withCondition.append(sql);
        Query query = em.createNativeQuery(withCondition.toString());
        if (type != null)
            query.setParameter("typeSet", type);
        return (BigInteger)query.getSingleResult();

    }

    public List<PracticeEntity> getPracticeToAttachAuthority(String authorityDossierId, String authorityWorkingId, List<String> practiceIdList) {
        String practiceIdString = "";
        for (String currentId : practiceIdList) {
            practiceIdString += "'" + currentId + "',";
        }

        if (!practiceIdString.equals("")) {
            practiceIdString = practiceIdString.substring(0, practiceIdString.length() - 1);
        }

        StringBuilder query = new StringBuilder("WITH AU AS ( SELECT id, jsonb_array_elements(processing_list) AS aut FROM practice p) ");
        query.append("SELECT * FROM practice p WHERE p.id IN (SELECT DISTINCT p.id " +
                "FROM practice p LEFT JOIN AU using(id) " +
                "WHERE ( aut->>'dossier_id' IS NULL AND aut->>'working_id' IS NULL  AND p.id  IN  (" + practiceIdString + ") " +
                ") )");

        Query sql = em.createNativeQuery(query.toString(), PracticeEntity.class);
        return sql.getResultList();
    }

    public List<PracticeEntity> getPracticeToDetattachAuthority(String authorityDossierId, String authorityWorkingId, List<String> practiceIdList) {
        String practiceIdString = "";


        for (String currentId : practiceIdList) {
            practiceIdString += "'" + currentId + "',";
        }
        StringBuilder query = new StringBuilder("WITH AU AS ( SELECT id, jsonb_array_elements(processing_list) AS aut FROM practice p) ");
        if (!practiceIdString.equals("")) {
            practiceIdString = practiceIdString.substring(0, practiceIdString.length() - 1);
            query.append("SELECT * " +
                    "FROM practice p LEFT JOIN AU using(id) " +
                    "WHERE aut->>'dossier_id' = '" + authorityDossierId + "' AND aut->>'working_id' = '" + authorityWorkingId + "' AND p.id NOT IN  (" + practiceIdString + ")");
        } else {

            query.append("SELECT * " +
                    "FROM practice p LEFT JOIN AU using(id) " +
                    "WHERE aut->>'dossier_id' = '" + authorityDossierId + "' AND aut->>'working_id' = '" + authorityWorkingId + "'");
        }

        Query sql = em.createNativeQuery(query.toString(), PracticeEntity.class);


        return sql.getResultList();
    }

    public List<PracticeEntity> getPracticeToAssociateAuthority(String authorityDossierId, String authorityWorkingId, List<String> practiceIdList) {
        String practiceIdString = "";


        for (String currentId : practiceIdList) {
            practiceIdString += "'" + currentId + "',";
        }
        StringBuilder query = new StringBuilder("WITH AU AS ( SELECT id, jsonb_array_elements(processing_list) AS aut FROM practice p) ");
        if (!practiceIdString.equals("")) {
            practiceIdString = practiceIdString.substring(0, practiceIdString.length() - 1);
            query.append("SELECT * FROM practice p WHERE p.id IN (SELECT DISTINCT p.id " +
                    "FROM practice p LEFT JOIN AU using(id) " +
                    "WHERE aut->>'dossier_id' = '" + authorityDossierId + "' AND aut->>'working_id' = '" + authorityWorkingId + "' AND p.id IN  (" + practiceIdString + ") )");
        }
        Query sql = em.createNativeQuery(query.toString(), PracticeEntity.class);

        return sql.getResultList();
    }

    public List<PracticeEntity> getPracticeToApproveAuthority(String authorityDossierId, String authorityWorkingId) {

        StringBuilder query = new StringBuilder("WITH AU AS ( SELECT id, jsonb_array_elements(processing_list) AS aut FROM practice p) ");

        query.append("SELECT * FROM practice p WHERE p.id IN (SELECT DISTINCT p.id " +
                "FROM practice p LEFT JOIN AU using(id) " +
                "WHERE aut->>'dossier_id' = '" + authorityDossierId + "' AND aut->>'working_id' = '" + authorityWorkingId + "' )");

        Query sql = em.createNativeQuery(query.toString(), PracticeEntity.class);

        return sql.getResultList();
    }

    public List<PracticeEntity> getPracticeAlreadyAssociatedWithoutOne(String authorityDossierId, String authorityWorkingId, String practiceId) {

        StringBuilder query = new StringBuilder("WITH AU AS ( SELECT id, jsonb_array_elements(processing_list) AS aut FROM practice p) ");
            query.append("SELECT * " +
                    "FROM practice p WHERE p.id IN (SELECT DISTINCT p.id FROM practice p LEFT JOIN AU using(id) " +
                    "WHERE aut->>'dossier_id' IS NOT NULL AND aut->>'dossier_id' = '" + authorityDossierId + "' AND aut->>'working_id' IS NOT NULL AND aut->>'working_id' = '" + authorityWorkingId + "' AND p.id <> '"+practiceId+"' )");

        Query sql = em.createNativeQuery(query.toString(), PracticeEntity.class);


        return sql.getResultList();
    }


    public List<PracticeEntity> findPracticeToExport(Long practiceIdFrom, Long practiceIdTo, String licensePlate, String contractId, String dateCreatedFrom, String dateCreatedTo, String status, String fleetVehicleId, Boolean asc, String orderBy) {

        Map<String, String> mappOrder = new HashMap<>();
        mappOrder.put("license_plate", "licensePlate");
        mappOrder.put("practice_id", "practiceId");
        mappOrder.put("practice_type", "practiceType");
        mappOrder.put("contract_id", "contractId");
        mappOrder.put("created_at", "createdAt");
        mappOrder.put("status", "status");

        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<PracticeEntity> practiceCriteriaQuery = criteriaBuilder.createQuery(PracticeEntity.class);
        Root<PracticeEntity> practiceRoot = practiceCriteriaQuery.from(PracticeEntity.class);

        practiceCriteriaQuery.select(practiceRoot);

        //Constructing list of parameters
        List<Predicate> predicates = new ArrayList<>();


        predicates.add(criteriaBuilder.equal(
                practiceRoot.get(PracticeEntity_.practiceType), PracticeTypeEnum.LOSS_POSSESSION));


        if (practiceIdFrom != null && practiceIdTo != null && practiceIdTo.compareTo(practiceIdFrom)>0) {
            predicates.add(criteriaBuilder.lessThanOrEqualTo(
                    practiceRoot.get(PracticeEntity_.practiceId), practiceIdTo));
            predicates.add(criteriaBuilder.greaterThanOrEqualTo(
                    practiceRoot.get(PracticeEntity_.practiceId), practiceIdFrom));
        } else if (practiceIdFrom != null){
            predicates.add(criteriaBuilder.greaterThanOrEqualTo(
                    practiceRoot.get(PracticeEntity_.practiceId), practiceIdFrom));
        } else if ( practiceIdTo != null){
            predicates.add(criteriaBuilder.lessThanOrEqualTo(
                    practiceRoot.get(PracticeEntity_.practiceId), practiceIdTo));
        }

        if(Util.isNotEmpty(status)){
            List<String> statusStringList = Arrays.asList(status.split("\\s*,\\s*"));
            List<PracticeStatusEnum> statusTypeList = new LinkedList<>();
            for(String currentStatus: statusStringList)
                statusTypeList.add(PracticeStatusEnum.create(currentStatus));
            predicates.add(practiceRoot.get(PracticeEntity_.status).in(statusTypeList));
        }

        if(Util.isNotEmpty(licensePlate)){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(criteriaBuilder
                    .function(JSONB_EXTRACT_PATH_TEXT.value(),
                            String.class,
                            practiceRoot.get("vehicle"),
                            criteriaBuilder.literal("license_plate"))),"%"+licensePlate.toUpperCase()+"%"));
        }

        if(Util.isNotEmpty(fleetVehicleId)){
            predicates.add(criteriaBuilder.equal(criteriaBuilder
                    .function(JSONB_EXTRACT_PATH_TEXT.value(),
                            String.class,
                            practiceRoot.get("vehicle"),
                            criteriaBuilder.literal("fleet_vehicle_id")),fleetVehicleId));
        }

        if(Util.isNotEmpty(contractId)){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(criteriaBuilder
                    .function(JSONB_EXTRACT_PATH_TEXT.value(),
                            String.class,
                            practiceRoot.get("contract"),
                            criteriaBuilder.literal("contract_id"))),"%"+contractId.toUpperCase()+"%"));
        }


        if(Util.isNotEmpty(dateCreatedTo) && Util.isNotEmpty(dateCreatedFrom) && dateCreatedTo.compareTo(dateCreatedFrom)>0){
            Instant toDateInstant = DateUtil.convertIS08601StringToUTCInstant(dateCreatedTo);
            predicates.add(criteriaBuilder.lessThanOrEqualTo(
                    practiceRoot.get(PracticeEntity_.createdAt), toDateInstant));

            Instant fromDateInstant = DateUtil.convertIS08601StringToUTCInstant(dateCreatedFrom);
            predicates.add(criteriaBuilder.greaterThanOrEqualTo(
                    practiceRoot.get(PracticeEntity_.createdAt), fromDateInstant));

        } else if(Util.isNotEmpty(dateCreatedFrom)){

            Instant fromDateInstant = DateUtil.convertIS08601StringToUTCInstant(dateCreatedFrom);
            predicates.add(criteriaBuilder.greaterThanOrEqualTo(
                    practiceRoot.get(PracticeEntity_.createdAt), fromDateInstant));

        } else if(Util.isNotEmpty(dateCreatedTo)){

            Instant toDateInstant = DateUtil.convertIS08601StringToUTCInstant(dateCreatedTo);
            predicates.add(criteriaBuilder.lessThanOrEqualTo(
                    practiceRoot.get(PracticeEntity_.createdAt), toDateInstant));;

        }

        practiceCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));


        List<Order> orderByList = new LinkedList<>();
        //orderByList.add(criteriaBuilder.asc(practiceRoot.get(PracticeEntity_.practiceId)));
        //orderByList.add(criteriaBuilder.asc(practiceRoot.get(PracticeEntity_.createdAt)));
        practiceCriteriaQuery.orderBy(orderByList);


        String fieldToOrder = null;
        if(orderBy != null) {
            fieldToOrder = mappOrder.get(orderBy);
            if (fieldToOrder != null) {
                if (!fieldToOrder.equalsIgnoreCase("licensePlate") && !fieldToOrder.equalsIgnoreCase("contractId")) {
                    if (asc != null) {
                        if (asc) {
                            practiceCriteriaQuery.orderBy(criteriaBuilder.asc(practiceRoot.get(fieldToOrder)));
                        } else {
                            practiceCriteriaQuery.orderBy(criteriaBuilder.desc(practiceRoot.get(fieldToOrder)));
                        }

                    } else {
                        practiceCriteriaQuery.orderBy(criteriaBuilder.desc(practiceRoot.get(fieldToOrder)));
                    }
                } else if (fieldToOrder.equalsIgnoreCase("contractId")) {
                    Expression exp = criteriaBuilder.function(JSONB_EXTRACT_PATH_TEXT.value(),
                            String.class,
                            practiceRoot.get("contract"),
                            criteriaBuilder.literal("contract_id"));

                    if (asc != null) {
                        if (asc) {
                            practiceCriteriaQuery.orderBy(criteriaBuilder.asc(exp));
                        } else {
                            practiceCriteriaQuery.orderBy(criteriaBuilder.desc(exp));
                        }

                    } else {
                        practiceCriteriaQuery.orderBy(criteriaBuilder.desc(exp));
                    }
                } else if (fieldToOrder.equalsIgnoreCase("licensePlate")) {
                    Expression exp = criteriaBuilder.function(JSONB_EXTRACT_PATH_TEXT.value(),
                            String.class,
                            practiceRoot.get("vehicle"),
                            criteriaBuilder.literal("license_plate"));

                    if (asc != null) {
                        if (asc) {
                            practiceCriteriaQuery.orderBy(criteriaBuilder.asc(exp));
                        } else {
                            practiceCriteriaQuery.orderBy(criteriaBuilder.desc(exp));
                        }

                    } else {
                        practiceCriteriaQuery.orderBy(criteriaBuilder.desc(exp));
                    }
                }
            }
        }

        List<PracticeEntity> practiceEntityList = em.createQuery(practiceCriteriaQuery).getResultList();
        return practiceEntityList;

    }
}
