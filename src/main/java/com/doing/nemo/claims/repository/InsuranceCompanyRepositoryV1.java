package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.entity.settings.InsuranceCompanyEntity;
import com.doing.nemo.claims.entity.settings.InsuranceCompanyEntity_;
import com.doing.nemo.claims.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.*;

@Repository
public class InsuranceCompanyRepositoryV1 {
    @PersistenceContext
    @Autowired
    private EntityManager em;

    public List<InsuranceCompanyEntity> findPaginationInsuranceCompany(Integer page, Integer pageSize, String orderBy, Boolean asc, String name, String zipCode, String city, String province, String telephone, Boolean isActive, Long code){

        Map<String, String> mappOrder = new HashMap<>();
        mappOrder.put("name", "name");
        mappOrder.put("zip_code", "zipCode");
        mappOrder.put("city", "city");
        mappOrder.put("province", "province");
        mappOrder.put("telephone", "telephone");
        mappOrder.put("is_active", "isActive");
        mappOrder.put("code", "code");

        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<InsuranceCompanyEntity> insuranceCompanyCriteriaQuery = criteriaBuilder.createQuery(InsuranceCompanyEntity.class);
        Root<InsuranceCompanyEntity> insuranceCompanyRoot = insuranceCompanyCriteriaQuery.from(InsuranceCompanyEntity.class);

        insuranceCompanyCriteriaQuery.select(insuranceCompanyRoot);

        //Constructing list of parameters
        List<Predicate> predicates = new ArrayList<>();

        if (Util.isNotEmpty(name)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    insuranceCompanyRoot.get(InsuranceCompanyEntity_.name)), "%"+name.toUpperCase()+"%"));
        }
        if (Util.isNotEmpty(zipCode)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    insuranceCompanyRoot.get(InsuranceCompanyEntity_.zipCode)), "%"+zipCode.toUpperCase()+"%"));
        }
        if (Util.isNotEmpty(city)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    insuranceCompanyRoot.get(InsuranceCompanyEntity_.city)), "%"+city.toUpperCase()+"%"));
        }
        if (Util.isNotEmpty(province)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    insuranceCompanyRoot.get(InsuranceCompanyEntity_.province)), "%"+province.toUpperCase()+"%"));
        }
        if (Util.isNotEmpty(telephone)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    insuranceCompanyRoot.get(InsuranceCompanyEntity_.telephone)), "%"+telephone.toUpperCase()+"%"));
        }
        if (isActive != null) {
            predicates.add(criteriaBuilder.equal(
                    insuranceCompanyRoot.get(InsuranceCompanyEntity_.isActive), isActive));
        }
        if (code != null) {
            predicates.add(criteriaBuilder.equal(
                    insuranceCompanyRoot.get(InsuranceCompanyEntity_.code), code));
        }

        insuranceCompanyCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));

        //List<Order> orderByList = new LinkedList<>();
        String fieldToOrder = null;
        if(orderBy != null)
            fieldToOrder = mappOrder.get(orderBy);
        if(fieldToOrder != null && asc != null){
            if (asc) {
                insuranceCompanyCriteriaQuery.orderBy(criteriaBuilder.asc(insuranceCompanyRoot.get(fieldToOrder)));
            } else {
                insuranceCompanyCriteriaQuery.orderBy(criteriaBuilder.desc(insuranceCompanyRoot.get(fieldToOrder)));
            }

        } else if(fieldToOrder != null) {
            insuranceCompanyCriteriaQuery.orderBy(criteriaBuilder.desc(insuranceCompanyRoot.get(fieldToOrder)));
        }

        List<InsuranceCompanyEntity> insuranceCompanyEntityList = em.createQuery(insuranceCompanyCriteriaQuery).setFirstResult((page-1)*pageSize).setMaxResults(pageSize).getResultList();
        return insuranceCompanyEntityList;
    }

    public Long countPaginationInsuranceCompany(String name, String zipCode, String city, String province, String telephone, Boolean isActive, Long code) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<Long> insuranceCompanyCriteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<InsuranceCompanyEntity> insuranceCompanyRoot = insuranceCompanyCriteriaQuery.from(InsuranceCompanyEntity.class);

        insuranceCompanyCriteriaQuery.select(criteriaBuilder.count(insuranceCompanyRoot));

        //Constructing list of parameters
        List<Predicate> predicates = new ArrayList<>();

        if (Util.isNotEmpty(name)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    insuranceCompanyRoot.get(InsuranceCompanyEntity_.name)), "%"+name.toUpperCase()+"%"));
        }
        if (Util.isNotEmpty(zipCode)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    insuranceCompanyRoot.get(InsuranceCompanyEntity_.zipCode)), "%"+zipCode.toUpperCase()+"%"));
        }
        if (Util.isNotEmpty(city)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    insuranceCompanyRoot.get(InsuranceCompanyEntity_.city)), "%"+city.toUpperCase()+"%"));
        }
        if (Util.isNotEmpty(province)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    insuranceCompanyRoot.get(InsuranceCompanyEntity_.province)), "%"+province.toUpperCase()+"%"));
        }
        if (Util.isNotEmpty(telephone)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    insuranceCompanyRoot.get(InsuranceCompanyEntity_.telephone)), "%"+telephone.toUpperCase()+"%"));
        }
        if (isActive != null) {
            predicates.add(criteriaBuilder.equal(
                    insuranceCompanyRoot.get(InsuranceCompanyEntity_.isActive), isActive));
        }
        if (code != null) {
            predicates.add(criteriaBuilder.equal(
                    insuranceCompanyRoot.get(InsuranceCompanyEntity_.code), code));
        }

        insuranceCompanyCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));
        Query sql = em.createQuery(insuranceCompanyCriteriaQuery);
        return (Long)sql.getSingleResult();

    }

    @Transactional
    @Modifying
    public void deleteAllRules(UUID counterpartId) {
        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("DELETE FROM automatic_affiliation_rule_legal_to_counterpart_company ");
        StringBuilder whereClauseBuilder = new StringBuilder(" WHERE 1=1");

        if (Util.isNotEmpty(counterpartId.toString())) {
            whereClauseBuilder.append(" AND counterpart_company_id = '"+counterpartId.toString()+"'");
        }
        queryBuilder.append(whereClauseBuilder);

        em.createNativeQuery(queryBuilder.toString()).executeUpdate();
    }

}
