package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.entity.SupplierEntity;
import com.doing.nemo.claims.entity.SupplierEntity_;
import com.doing.nemo.claims.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class SupplierRepositoryV1 {

    @PersistenceContext
    @Autowired
    private EntityManager em;
    
    public List<SupplierEntity> findSuppliersFiltered(Integer page, Integer pageSize, String orderBy, Boolean asc, String name, String email, Boolean isActive, String codSupplier) {

        Map<String, String> mappOrder = new HashMap<>();
        mappOrder.put("cod_supplier", "codSupplier");
        mappOrder.put("name", "name");
        mappOrder.put("email", "email");
        mappOrder.put("is_active", "isActive");

        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<SupplierEntity> supplierCriteriaQuery = criteriaBuilder.createQuery(SupplierEntity.class);
        Root<SupplierEntity> supplierRoot = supplierCriteriaQuery.from(SupplierEntity.class);

        supplierCriteriaQuery.select(supplierRoot);

        //Constructing list of parameters
        List<Predicate> predicates = new ArrayList<>();

        if (Util.isNotEmpty(name)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    supplierRoot.get(SupplierEntity_.name)), "%"+name.toUpperCase()+"%"));
        }
        if (Util.isNotEmpty(codSupplier)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    supplierRoot.get(SupplierEntity_.codSupplier)), "%"+codSupplier.toUpperCase()+"%"));
        }
        if (Util.isNotEmpty(email)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    supplierRoot.get(SupplierEntity_.email)), "%"+email.toUpperCase()+"%"));
        }
        if (isActive != null) {
            predicates.add(criteriaBuilder.equal(
                    supplierRoot.get(SupplierEntity_.isActive), isActive));
        }

        supplierCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));


        //List<Order> orderByList = new LinkedList<>();
        String fieldToOrder = null;
        if(orderBy != null)
            fieldToOrder = mappOrder.get(orderBy);
        if(fieldToOrder != null && asc != null){
            if (asc) {
                supplierCriteriaQuery.orderBy(criteriaBuilder.asc(supplierRoot.get(fieldToOrder)));
            } else {
                supplierCriteriaQuery.orderBy(criteriaBuilder.desc(supplierRoot.get(fieldToOrder)));
            }

        } else if(fieldToOrder != null) {
            supplierCriteriaQuery.orderBy(criteriaBuilder.asc(supplierRoot.get(fieldToOrder)));
        }
        
        List<SupplierEntity> supplierEntityList = em.createQuery(supplierCriteriaQuery).setFirstResult((page-1)*pageSize).setMaxResults(pageSize).getResultList();
        return supplierEntityList;
    }
    
    public Long countSupplierFiltered(String name, String email,  Boolean isActive, String codSupplier) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<Long> supplierCriteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<SupplierEntity> supplierRoot = supplierCriteriaQuery.from(SupplierEntity.class);

        supplierCriteriaQuery.select(criteriaBuilder.count(supplierRoot));

        //Constructing list of parameters
        List<Predicate> predicates = new ArrayList<>();

        if (Util.isNotEmpty(name)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    supplierRoot.get(SupplierEntity_.name)), "%"+name.toUpperCase()+"%"));
        }
        if (Util.isNotEmpty(email)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    supplierRoot.get(SupplierEntity_.email)), "%"+email.toUpperCase()+"%"));
        }
        if (Util.isNotEmpty(codSupplier)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    supplierRoot.get(SupplierEntity_.codSupplier)), "%"+codSupplier.toUpperCase()+"%"));
        }
        if (isActive != null) {
            predicates.add(criteriaBuilder.equal(
                    supplierRoot.get(SupplierEntity_.isActive), isActive));
        }

        supplierCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));
        Query sql = em.createQuery(supplierCriteriaQuery);
        return (Long)sql.getSingleResult();
    }

}
