package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.entity.ExternalCommunicationEntity;
import com.doing.nemo.claims.entity.enumerated.ExternalCommunicationEnum;
import io.lettuce.core.dynamic.annotation.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface ExternalCommunicationRepository extends JpaRepository<ExternalCommunicationEntity, UUID> {
    @Query("SELECT DISTINCT(p.claimsId) FROM external_communication p WHERE p.status = :status")
    List<String> getAllIdClaimsByStatus(@Param("status") ExternalCommunicationEnum status);

}
