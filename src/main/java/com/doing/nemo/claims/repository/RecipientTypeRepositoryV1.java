package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.entity.enumerated.ClaimsRepairEnum;
import com.doing.nemo.claims.entity.enumerated.RecipientEnum;
import com.doing.nemo.claims.entity.settings.RecipientTypeEntity;
import com.doing.nemo.claims.entity.settings.RecipientTypeEntity_;
import com.doing.nemo.claims.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class RecipientTypeRepositoryV1 {
    @PersistenceContext
    @Autowired
    private EntityManager em;

    public List<RecipientTypeEntity> findPaginationRecipientType(Integer page, Integer pageSize, ClaimsRepairEnum claimsRepairEnum, String description, Boolean fixed, String email, String dbFieldEmail, Boolean isActive, RecipientEnum recipientType, String orderBy, Boolean asc){

        Map<String, String> mapOrder = new HashMap<>();
        mapOrder.put("description", "description");
        mapOrder.put("fixed", "fixed");
        mapOrder.put("db_field_email", "dbFieldEmail");
        mapOrder.put("is_active", "isActive");
        mapOrder.put("recipient_type", "recipientType");
        mapOrder.put("type_complaint", "typeComplaint");
        mapOrder.put("email", "email");


        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<RecipientTypeEntity> recipientTypeCriteriaQuery = criteriaBuilder.createQuery(RecipientTypeEntity.class);
        Root<RecipientTypeEntity> recipientTypeRoot = recipientTypeCriteriaQuery.from(RecipientTypeEntity.class);

        recipientTypeCriteriaQuery.select(recipientTypeRoot);

        //Constructing list of parameters
        List<Predicate> predicates = new ArrayList<>();

        if (claimsRepairEnum != null) {
            predicates.add(criteriaBuilder.equal(
                    recipientTypeRoot.get(RecipientTypeEntity_.typeComplaint), claimsRepairEnum));
        }
        if (Util.isNotEmpty(email)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    recipientTypeRoot.get(RecipientTypeEntity_.email)), "%"+email.toUpperCase()+"%"));
        }
        if (isActive != null) {
            predicates.add(criteriaBuilder.equal(
                    recipientTypeRoot.get(RecipientTypeEntity_.isActive), isActive));
        }
        if (Util.isNotEmpty(description)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    recipientTypeRoot.get(RecipientTypeEntity_.description)), "%"+description.toUpperCase()+"%"));
        }
        if (fixed != null) {
            predicates.add(criteriaBuilder.equal(
                    recipientTypeRoot.get(RecipientTypeEntity_.fixed), fixed));
        }
        if (Util.isNotEmpty(dbFieldEmail)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    recipientTypeRoot.get(RecipientTypeEntity_.dbFieldEmail)), "%"+dbFieldEmail.toUpperCase()+"%"));
        }
        if (recipientType != null) {
            predicates.add(criteriaBuilder.equal(
                    recipientTypeRoot.get(RecipientTypeEntity_.recipientType), recipientType));
        }
        recipientTypeCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));

        //List<Order> orderByList = new LinkedList<>();
        String fieldToOrder = null;
        if(orderBy != null)
            fieldToOrder = mapOrder.get(orderBy);
        if(fieldToOrder != null && asc != null){
            if (asc) {
                recipientTypeCriteriaQuery.orderBy(criteriaBuilder.asc(recipientTypeRoot.get(fieldToOrder)));
            } else {
                recipientTypeCriteriaQuery.orderBy(criteriaBuilder.desc(recipientTypeRoot.get(fieldToOrder)));
            }

        } else if(orderBy != null) {
            recipientTypeCriteriaQuery.orderBy(criteriaBuilder.desc(recipientTypeRoot.get(fieldToOrder)));
        }

        List<RecipientTypeEntity> recipientTypeEntityList = em.createQuery(recipientTypeCriteriaQuery).setFirstResult((page-1)*pageSize).setMaxResults(pageSize).getResultList();
        return recipientTypeEntityList;
    }

    public Long countPaginationRecipientType(ClaimsRepairEnum claimsRepairEnum, String description, Boolean fixed, String email, String dbFieldEmail, Boolean isActive, RecipientEnum recipientType) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<Long> recipientTypeCriteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<RecipientTypeEntity> recipientTypeRoot = recipientTypeCriteriaQuery.from(RecipientTypeEntity.class);

        recipientTypeCriteriaQuery.select(criteriaBuilder.count(recipientTypeRoot));

        //Constructing list of parameters
        List<Predicate> predicates = new ArrayList<>();

        if (claimsRepairEnum != null) {
            predicates.add(criteriaBuilder.equal(
                    recipientTypeRoot.get(RecipientTypeEntity_.typeComplaint), claimsRepairEnum));
        }
        if (Util.isNotEmpty(email)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    recipientTypeRoot.get(RecipientTypeEntity_.email)), "%"+email.toUpperCase()+"%"));
        }
        if (isActive != null) {
            predicates.add(criteriaBuilder.equal(
                    recipientTypeRoot.get(RecipientTypeEntity_.isActive), isActive));
        }
        if (Util.isNotEmpty(description)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    recipientTypeRoot.get(RecipientTypeEntity_.description)), "%"+description.toUpperCase()+"%"));
        }
        if (fixed != null) {
            predicates.add(criteriaBuilder.equal(
                    recipientTypeRoot.get(RecipientTypeEntity_.fixed), fixed));
        }
        if (Util.isNotEmpty(dbFieldEmail)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    recipientTypeRoot.get(RecipientTypeEntity_.dbFieldEmail)), "%"+dbFieldEmail.toUpperCase()+"%"));
        }
        if (recipientType != null) {
            predicates.add(criteriaBuilder.equal(
                    recipientTypeRoot.get(RecipientTypeEntity_.recipientType), recipientType));
        }

            recipientTypeCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));
            Query sql = em.createQuery(recipientTypeCriteriaQuery);
            return (Long) sql.getSingleResult();
    }
}
