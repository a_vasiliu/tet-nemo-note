package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.entity.enumerated.AntitheftEnum.AntitheftTypeEnum;
import com.doing.nemo.claims.entity.settings.AntiTheftServiceEntity;
import com.doing.nemo.claims.entity.settings.AntiTheftServiceEntity_;
import com.doing.nemo.claims.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class AntiTheftServiceRepositoryV1 {

    @PersistenceContext
    @Autowired
    private EntityManager em;

    
    public List<AntiTheftServiceEntity> findAntiTheftServicesFiltered(Integer page, Integer pageSize, String orderBy, Boolean asc, String codeAntiTheftService, String name, String businessName, Integer supplierCode, String email, String phoneNumber, String providerType, Integer timeOutInSeconds, String endPointUrl, String username, String password, String companyCode, Boolean active, Boolean isActive, AntitheftTypeEnum type) {
        Map<String, String> mappOrder = new HashMap<>();
        mappOrder.put("code_anti_theft_service", "codeAntiTheftService");
        mappOrder.put("name", "name");
        mappOrder.put("business_name", "businessName");
        mappOrder.put("supplier_code", "supplierCode");
        mappOrder.put("email", "email");
        mappOrder.put("phone_number", "phoneNumber");
        mappOrder.put("provider_type", "providerType");
        mappOrder.put("time_out_in_seconds", "timeOutInSeconds");
        mappOrder.put("end_point_url", "endPointUrl");
        mappOrder.put("username", "username");
        mappOrder.put("company_code", "companyCode");
        mappOrder.put("active", "active");
        mappOrder.put("is_active", "isActive");
        mappOrder.put("type", "type");

        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<AntiTheftServiceEntity> antiTheftServiceCriteriaQuery = criteriaBuilder.createQuery(AntiTheftServiceEntity.class);
        Root<AntiTheftServiceEntity> antiTheftServiceRoot = antiTheftServiceCriteriaQuery.from(AntiTheftServiceEntity.class);

        antiTheftServiceCriteriaQuery.select(antiTheftServiceRoot);

        //Constructing list of parameters
        List<Predicate> predicates = new ArrayList<>();

        if (Util.isNotEmpty(codeAntiTheftService)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    antiTheftServiceRoot.get(AntiTheftServiceEntity_.codeAntiTheftService)), "%"+codeAntiTheftService.toUpperCase()+"%"));
        }
        if (Util.isNotEmpty(name)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    antiTheftServiceRoot.get(AntiTheftServiceEntity_.name)), "%"+name.toUpperCase()+"%"));
        }
        if (Util.isNotEmpty(businessName)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    antiTheftServiceRoot.get(AntiTheftServiceEntity_.businessName)), "%"+businessName.toUpperCase()+"%"));
        }
        if (Util.isNotEmpty(email)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    antiTheftServiceRoot.get(AntiTheftServiceEntity_.email)), "%"+email.toUpperCase()+"%"));
        }
        if (Util.isNotEmpty(phoneNumber)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    antiTheftServiceRoot.get(AntiTheftServiceEntity_.phoneNumber)), "%"+phoneNumber.toUpperCase()+"%"));
        }
        if (Util.isNotEmpty(providerType)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    antiTheftServiceRoot.get(AntiTheftServiceEntity_.providerType)), "%"+providerType.toUpperCase()+"%"));
        }
        if (Util.isNotEmpty(endPointUrl)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    antiTheftServiceRoot.get(AntiTheftServiceEntity_.endPointUrl)), "%"+endPointUrl.toUpperCase()+"%"));
        }
        if (Util.isNotEmpty(username)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    antiTheftServiceRoot.get(AntiTheftServiceEntity_.username)), "%"+username.toUpperCase()+"%"));
        }
        if (Util.isNotEmpty(password)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    antiTheftServiceRoot.get(AntiTheftServiceEntity_.password)), "%"+password.toUpperCase()+"%"));
        }
        if (Util.isNotEmpty(companyCode)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    antiTheftServiceRoot.get(AntiTheftServiceEntity_.companyCode)), "%"+companyCode.toUpperCase()+"%"));
        }
        if (supplierCode != null) {
            predicates.add(criteriaBuilder.equal(
                    antiTheftServiceRoot.get(AntiTheftServiceEntity_.supplierCode), supplierCode));
        }
        if (timeOutInSeconds != null) {
            predicates.add(criteriaBuilder.equal(
                    antiTheftServiceRoot.get(AntiTheftServiceEntity_.timeOutInSeconds), timeOutInSeconds));
        }
        if (isActive != null) {
            predicates.add(criteriaBuilder.equal(
                    antiTheftServiceRoot.get(AntiTheftServiceEntity_.isActive), isActive));
        }
        if (active != null) {
            predicates.add(criteriaBuilder.equal(
                    antiTheftServiceRoot.get(AntiTheftServiceEntity_.active), active));
        }
        if (type != null) {
            predicates.add(criteriaBuilder.equal(
                    antiTheftServiceRoot.get(AntiTheftServiceEntity_.type), type));
        }
        antiTheftServiceCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));

        //List<Order> orderByList = new LinkedList<>();
        String fieldToOrder = null;
        if(orderBy != null)
            fieldToOrder = mappOrder.get(orderBy);
        if(fieldToOrder != null && asc != null){
            if (asc) {
                antiTheftServiceCriteriaQuery.orderBy(criteriaBuilder.asc(antiTheftServiceRoot.get(fieldToOrder)));
            } else {
                antiTheftServiceCriteriaQuery.orderBy(criteriaBuilder.desc(antiTheftServiceRoot.get(fieldToOrder)));
            }

        } else if(orderBy != null) {
            antiTheftServiceCriteriaQuery.orderBy(criteriaBuilder.desc(antiTheftServiceRoot.get(fieldToOrder)));
        }

        List<AntiTheftServiceEntity> antiTheftServiceEntityList = em.createQuery(antiTheftServiceCriteriaQuery).setFirstResult((page-1)*pageSize).setMaxResults(pageSize).getResultList();
        return antiTheftServiceEntityList;

    }
    
    public Long countAntiTheftServiceFiltered(String codeAntiTheftService, String name, String businessName, Integer supplierCode, String email, String phoneNumber, String providerType, Integer timeOutInSeconds, String endPointUrl, String username, String password, String companyCode, Boolean active, Boolean isActive, AntitheftTypeEnum type) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<Long> antiTheftServiceCriteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<AntiTheftServiceEntity> antiTheftServiceRoot = antiTheftServiceCriteriaQuery.from(AntiTheftServiceEntity.class);

        antiTheftServiceCriteriaQuery.select(criteriaBuilder.count(antiTheftServiceRoot));

        //Constructing list of parameters
        List<Predicate> predicates = new ArrayList<>();

        if (Util.isNotEmpty(codeAntiTheftService)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    antiTheftServiceRoot.get(AntiTheftServiceEntity_.codeAntiTheftService)), "%"+codeAntiTheftService.toUpperCase()+"%"));
        }
        if (Util.isNotEmpty(name)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    antiTheftServiceRoot.get(AntiTheftServiceEntity_.name)), "%"+name.toUpperCase()+"%"));
        }
        if (Util.isNotEmpty(businessName)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    antiTheftServiceRoot.get(AntiTheftServiceEntity_.businessName)), "%"+businessName.toUpperCase()+"%"));
        }
        if (Util.isNotEmpty(email)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    antiTheftServiceRoot.get(AntiTheftServiceEntity_.email)), "%"+email.toUpperCase()+"%"));
        }
        if (Util.isNotEmpty(phoneNumber)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    antiTheftServiceRoot.get(AntiTheftServiceEntity_.phoneNumber)), "%"+phoneNumber.toUpperCase()+"%"));
        }
        if (Util.isNotEmpty(providerType)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    antiTheftServiceRoot.get(AntiTheftServiceEntity_.providerType)), "%"+providerType.toUpperCase()+"%"));
        }
        if (Util.isNotEmpty(endPointUrl)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    antiTheftServiceRoot.get(AntiTheftServiceEntity_.endPointUrl)), "%"+endPointUrl.toUpperCase()+"%"));
        }
        if (Util.isNotEmpty(username)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    antiTheftServiceRoot.get(AntiTheftServiceEntity_.username)), "%"+username.toUpperCase()+"%"));
        }
        if (Util.isNotEmpty(password)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    antiTheftServiceRoot.get(AntiTheftServiceEntity_.password)), "%"+password.toUpperCase()+"%"));
        }
        if (Util.isNotEmpty(companyCode)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    antiTheftServiceRoot.get(AntiTheftServiceEntity_.companyCode)), "%"+companyCode.toUpperCase()+"%"));
        }
        if (supplierCode != null) {
            predicates.add(criteriaBuilder.equal(
                    antiTheftServiceRoot.get(AntiTheftServiceEntity_.supplierCode), supplierCode));
        }
        if (timeOutInSeconds != null) {
            predicates.add(criteriaBuilder.equal(
                    antiTheftServiceRoot.get(AntiTheftServiceEntity_.timeOutInSeconds), timeOutInSeconds));
        }
        if (isActive != null) {
            predicates.add(criteriaBuilder.equal(
                    antiTheftServiceRoot.get(AntiTheftServiceEntity_.isActive), isActive));
        }
        if (active != null) {
            predicates.add(criteriaBuilder.equal(
                    antiTheftServiceRoot.get(AntiTheftServiceEntity_.active), active));
        }
        if (type != null) {
            predicates.add(criteriaBuilder.equal(
                    antiTheftServiceRoot.get(AntiTheftServiceEntity_.type), type));
        }

        antiTheftServiceCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));
        Query sql = em.createQuery(antiTheftServiceCriteriaQuery);
        return (Long) sql.getSingleResult();

    }

    public AntiTheftServiceEntity searchAntiTheftServicebyProvider(String provider){

        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<AntiTheftServiceEntity> antiTheftServiceCriteriaQuery = criteriaBuilder.createQuery(AntiTheftServiceEntity.class);
        Root<AntiTheftServiceEntity> antiTheftServiceRoot = antiTheftServiceCriteriaQuery.from(AntiTheftServiceEntity.class);

        antiTheftServiceCriteriaQuery.select(antiTheftServiceRoot);

        //Constructing list of parameters
        List<Predicate> predicates = new ArrayList<>();

        if (Util.isNotEmpty(provider)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    antiTheftServiceRoot.get(AntiTheftServiceEntity_.providerType)), "%"+provider.toUpperCase()+"%"));
        }

        antiTheftServiceCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));

        List<AntiTheftServiceEntity> antiTheftServiceEntityList = em.createQuery(antiTheftServiceCriteriaQuery).getResultList();

        if(antiTheftServiceEntityList!=null && !antiTheftServiceEntityList.isEmpty())
            return antiTheftServiceEntityList.get(0);
        else return null;

    }





}
