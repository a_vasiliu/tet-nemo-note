package com.doing.nemo.claims.repository;


import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

@Repository
public class ClaimsAuthorityRepositoryV1 {

    @PersistenceContext
    @Autowired
    private EntityManager em;

    private static Logger LOGGER = LoggerFactory.getLogger(ClaimsAuthorityRepositoryV1.class);

    @Transactional
    @Modifying
    public void deleteAuthorityOrphanal() {

        StringBuilder query = new StringBuilder("DELETE  " +
                "FROM claims_authority where id NOT IN ( SELECT authority_id FROM claims_to_claims_authority) ");

        Query sql = em.createNativeQuery(query.toString());

        int rowNumberDeleted = sql.executeUpdate();
        LOGGER.debug("[AUTHORITY] number orphan authorities {}", rowNumberDeleted);
    }

}
