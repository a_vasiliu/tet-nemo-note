package com.doing.nemo.claims.repository;


import com.doing.nemo.claims.entity.settings.DataManagementImportEntity;
import com.doing.nemo.claims.entity.settings.DataManagementImportEntity_;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class DataManagementImportRepositoryV1 {
    @PersistenceContext
    @Autowired
    private EntityManager em;

    public List<DataManagementImportEntity> findPaginationDataManagementImport(Integer page, Integer pageSize){
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<DataManagementImportEntity> dataManagementImportCriteriaQuery = criteriaBuilder.createQuery(DataManagementImportEntity.class);
        Root<DataManagementImportEntity> dataManagementImportRoot = dataManagementImportCriteriaQuery.from(DataManagementImportEntity.class);
        dataManagementImportCriteriaQuery.select(dataManagementImportRoot);
        dataManagementImportCriteriaQuery.orderBy(criteriaBuilder.desc(dataManagementImportRoot.get(DataManagementImportEntity_.date)));

        List<DataManagementImportEntity> dataManagementImportEntityList = em.createQuery(dataManagementImportCriteriaQuery).setFirstResult((page-1)*pageSize).setMaxResults(pageSize).getResultList();
        return dataManagementImportEntityList;
    }

    public Long countPaginationDataManagementImport() {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<Long> dataManagementImportCriteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<DataManagementImportEntity> dataManagementImportRoot = dataManagementImportCriteriaQuery.from(DataManagementImportEntity.class);
        dataManagementImportCriteriaQuery.select(criteriaBuilder.count(dataManagementImportRoot));

        Query sql = em.createQuery(dataManagementImportCriteriaQuery);
        return (Long)sql.getSingleResult();
    }
}
