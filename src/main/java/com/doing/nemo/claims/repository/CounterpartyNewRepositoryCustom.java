package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.dto.CtpExportFilter;
import com.doing.nemo.claims.entity.CounterpartyNewEntity;
import com.doing.nemo.claims.entity.enumerated.DTO.CounterpartyStats;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.List;

@Repository
public interface CounterpartyNewRepositoryCustom {

    List<CounterpartyNewEntity> findCounterpartyForPaginationAuthority(String plate, String chassis, Integer page, Integer pageSize);

    BigInteger countCounterpartyForPaginationAuthority(String plate, String chassis);

    List<CounterpartyNewEntity> findCounterpartyForPagination(int page, int pageSize, String denomination, String plate, Long practiceIdCounterparty, String statusRepair, String dateCreatedFrom, String dateCreatedTo, Boolean asc, String orderBy, String addressSx, String fromCompany, String dateSxFrom, String dateSxTo, String region, String province, String lastContact, String assignedTo, String locality, String recall, String centerEmail, Boolean msaPagination);

    BigInteger countCounterpartyForPagination(int page, int pageSize, String denomination, String plate, Long practiceIdCounterparty, String statusRepair, String dateCreatedFrom, String dateCreatedTo, Boolean asc, String orderBy, String addressSx, String fromCompany, String dateSxFrom, String dateSxTo, String region, String province, String lastContact, String assignedTo, String locality, String recall, String centerEmail, Boolean msaPagination);

    List<CounterpartyNewEntity> findCounterpartyForPaginationWithoutToSort(int page, int pageSize, String denomination, String plate, Long practiceIdCounterparty, String statusRepair, String dateCreatedFrom, String dateCreatedTo, Boolean asc, String orderBy, String addressSx, String fromCompany, String dateSxFrom, String dateSxTo, String region, String province, String lastContact, String assignedTo, String locality, String recall);

    BigInteger countCounterpartyForPaginationWithoutToSort(int page, int pageSize, String denomination, String plate, Long practiceIdCounterparty, String statusRepair, String dateCreatedFrom, String dateCreatedTo, Boolean asc, String orderBy, String addressSx, String fromCompany, String dateSxFrom, String dateSxTo, String region, String province, String lastContact, String assignedTo, String locality, String recall);

    CounterpartyStats findCounterpartyStats(String nemoUserId);

    BigInteger countCounterpartyForExport(CtpExportFilter filter);

    List<CounterpartyNewEntity> findCounterpartyForExportByPages(CtpExportFilter filter, int page, int pageSize);
}
