package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.entity.enumerated.ClaimsRepairEnum;
import com.doing.nemo.claims.entity.enumerated.EventTypeEnum;
import com.doing.nemo.claims.entity.settings.EventTypeEntity;
import com.doing.nemo.claims.entity.settings.EventTypeEntity_;
import com.doing.nemo.claims.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class EventTypeRepositoryV1 {
    @PersistenceContext
    @Autowired
    private EntityManager em;


    public List<EventTypeEntity> findPaginationEventType(Integer page, Integer pageSize, ClaimsRepairEnum typeComplaint, String orderBy, String description, EventTypeEnum eventType, Boolean createAttachments, String catalog, String attachmentsType, Boolean onlyLast, Boolean isActive, Boolean asc){

        Map<String, String> mappOrder = new HashMap<>();
        mappOrder.put("type_complaint", "typeComplaint");
        mappOrder.put("description", "description");
        mappOrder.put("event_type", "eventType");
        mappOrder.put("create_attachments", "createAttachments");
        mappOrder.put("catalog", "catalog");
        mappOrder.put("attachments_type", "attachmentsType");
        mappOrder.put("only_last", "onlyLast");
        mappOrder.put("is_active", "isActive");

        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<EventTypeEntity> eventTypeCriteriaQuery = criteriaBuilder.createQuery(EventTypeEntity.class);
        Root<EventTypeEntity> eventTypeRoot = eventTypeCriteriaQuery.from(EventTypeEntity.class);

        eventTypeCriteriaQuery.select(eventTypeRoot);

        //Constructing list of parameters
        List<Predicate> predicates = new ArrayList<>();

        if (typeComplaint != null) {
            predicates.add(criteriaBuilder.equal(
                    eventTypeRoot.get(EventTypeEntity_.typeComplaint), typeComplaint));
        }
        if (Util.isNotEmpty(description)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    eventTypeRoot.get(EventTypeEntity_.description)), "%"+description.toUpperCase()+"%"));
        }
        if (eventType != null) {
            predicates.add(criteriaBuilder.equal(
                    eventTypeRoot.get(EventTypeEntity_.eventType), eventType));
        }
        if (createAttachments != null) {
            predicates.add(criteriaBuilder.equal(
                    eventTypeRoot.get(EventTypeEntity_.createAttachments), createAttachments));
        }
        if (Util.isNotEmpty(catalog)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    eventTypeRoot.get(EventTypeEntity_.catalog)), "%"+catalog.toUpperCase()+"%"));
        }
        if (Util.isNotEmpty(attachmentsType)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    eventTypeRoot.get(EventTypeEntity_.attachmentsType)), "%"+attachmentsType.toUpperCase()+"%"));
        }
        if (onlyLast != null) {
            predicates.add(criteriaBuilder.equal(
                    eventTypeRoot.get(EventTypeEntity_.onlyLast), onlyLast));
        }
        if (isActive != null) {
            predicates.add(criteriaBuilder.equal(
                    eventTypeRoot.get(EventTypeEntity_.isActive), isActive));
        }
        
        
        eventTypeCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));

        //List<Order> orderByList = new LinkedList<>();
        String fieldToOrder = null;
        if(orderBy != null)
            fieldToOrder = mappOrder.get(orderBy);
        if(fieldToOrder != null && asc != null){
            if (asc) {
                eventTypeCriteriaQuery.orderBy(criteriaBuilder.asc(eventTypeRoot.get(fieldToOrder)));
            } else {
                eventTypeCriteriaQuery.orderBy(criteriaBuilder.desc(eventTypeRoot.get(fieldToOrder)));
            }

        } else if(orderBy != null) {
            eventTypeCriteriaQuery.orderBy(criteriaBuilder.desc(eventTypeRoot.get(fieldToOrder)));
        }

        List<EventTypeEntity> eventTypeEntityList = em.createQuery(eventTypeCriteriaQuery).setFirstResult((page-1)*pageSize).setMaxResults(pageSize).getResultList();
        return eventTypeEntityList;
    }

    public Long countPaginationEventType(ClaimsRepairEnum typeComplaint, String description, EventTypeEnum eventType, Boolean createAttachments, String catalog, String attachmentsType, Boolean onlyLast, Boolean isActive) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<Long> eventTypeCriteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<EventTypeEntity> eventTypeRoot = eventTypeCriteriaQuery.from(EventTypeEntity.class);

        eventTypeCriteriaQuery.select(criteriaBuilder.count(eventTypeRoot));

        //Constructing list of parameters
        List<Predicate> predicates = new ArrayList<>();

        if (typeComplaint != null) {
            predicates.add(criteriaBuilder.equal(
                    eventTypeRoot.get(EventTypeEntity_.typeComplaint), typeComplaint));
        }
        if (Util.isNotEmpty(description)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    eventTypeRoot.get(EventTypeEntity_.description)), "%"+description.toUpperCase()+"%"));
        }
        if (eventType != null) {
            predicates.add(criteriaBuilder.equal(
                    eventTypeRoot.get(EventTypeEntity_.eventType), eventType));
        }
        if (createAttachments != null) {
            predicates.add(criteriaBuilder.equal(
                    eventTypeRoot.get(EventTypeEntity_.createAttachments), createAttachments));
        }
        if (Util.isNotEmpty(catalog)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    eventTypeRoot.get(EventTypeEntity_.catalog)), "%"+catalog.toUpperCase()+"%"));
        }
        if (Util.isNotEmpty(attachmentsType)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    eventTypeRoot.get(EventTypeEntity_.attachmentsType)), "%"+attachmentsType.toUpperCase()+"%"));
        }
        if (onlyLast != null) {
            predicates.add(criteriaBuilder.equal(
                    eventTypeRoot.get(EventTypeEntity_.onlyLast), onlyLast));
        }
        if (isActive != null) {
            predicates.add(criteriaBuilder.equal(
                    eventTypeRoot.get(EventTypeEntity_.isActive), isActive));
        }

        eventTypeCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));
        Query sql = em.createQuery(eventTypeCriteriaQuery);
        return (Long) sql.getSingleResult();

    }
}
