package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.entity.enumerated.PolicyEnum.PolicyTypeEnum;
import com.doing.nemo.claims.entity.settings.InsurancePolicyEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Repository
public interface InsurancePolicyRepository extends JpaRepository<InsurancePolicyEntity, UUID> {

    @Query("SELECT i FROM InsurancePolicyEntity i WHERE i.type = :type")
    InsurancePolicyEntity searchInsurancePolicyByType(@Param("type") String type);

    @Query("SELECT i FROM InsurancePolicyEntity i WHERE i.id <> :id AND i.type = :type")
    List<InsurancePolicyEntity> searchInsurancePolicyWithDifferentId(@Param("id") UUID id, @Param("type") String type);

    @Query("SELECT i FROM InsurancePolicyEntity i WHERE i.numberPolicy = :numberPolicy")
    InsurancePolicyEntity searchInsurancePolicyByPolicyNumber(@Param("numberPolicy") Long numberPolicy);

    @Query("SELECT i FROM InsurancePolicyEntity i WHERE i.clientCode = :clientCode")
    List<InsurancePolicyEntity> searchInsurancePolicyByClientCode(@Param("clientCode") Long clientCode);

    @Query("SELECT i FROM InsurancePolicyEntity i WHERE i.renewalNotification = true and i.endValidity < :now")
    List<InsurancePolicyEntity> searchInsurancePolicyByRenewalNotification(@Param("now") Instant now);

    @Query("SELECT i FROM InsurancePolicyEntity i WHERE  i.bookRegisterCode = :bookRegisterCode AND i.beginningValidity <= :dateAccident AND i.endValidity >= :dateAccident AND i.type = :typeEnum")
    List<InsurancePolicyEntity> searchInsurancePolicyForGetContract(@Param("bookRegisterCode") String bookRegisterCode, @Param("dateAccident") Instant dateAccident, @Param("typeEnum")PolicyTypeEnum typeEnum);

    @Query("SELECT i FROM InsurancePolicyEntity i WHERE i.clientCode = :clientCode AND i.beginningValidity <= :dateAccident AND i.endValidity >= :dateAccident AND i.type = :typeEnum")
    List<InsurancePolicyEntity> searchInsurancePolicyForGetContractByCustomer(@Param("clientCode") Long clientCode, @Param("dateAccident") Instant dateAccident,  @Param("typeEnum")PolicyTypeEnum typeEnum);

    @Query("SELECT i FROM InsurancePolicyEntity i WHERE i.bookRegisterCode = :bookRegisterCode AND i.beginningValidity <= :dateAccident AND i.endValidity >= :dateAccident AND i.type = :typeEnum")
    InsurancePolicyEntity searchInsurancePolicyCounterparty(@Param("bookRegisterCode") String bookRegisterCode, @Param("dateAccident") Instant dateAccident,  @Param("typeEnum")PolicyTypeEnum typeEnum);

    @Query("SELECT i FROM InsurancePolicyEntity i WHERE i.beginningValidity <= :dateAccident AND i.endValidity >= :dateAccident AND i.type = :typeEnum")
    InsurancePolicyEntity searchInsurancePolicyCounterparty(@Param("dateAccident") Instant dateAccident,  @Param("typeEnum")PolicyTypeEnum typeEnum);
}
