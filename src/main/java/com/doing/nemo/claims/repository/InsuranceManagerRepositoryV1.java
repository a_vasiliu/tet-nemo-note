package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.entity.settings.InsuranceManagerEntity;
import com.doing.nemo.claims.entity.settings.InsuranceManagerEntity_;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class InsuranceManagerRepositoryV1 {
    @PersistenceContext
    @Autowired
    private EntityManager em;

    public List<InsuranceManagerEntity> findPaginationInsuranceManager(Integer page, Integer pageSize, String orderBy, Boolean asc, String name, String province, String telephone, String fax, String email, Boolean isActive, String address, String zipCode, String city, String state, Long rifCode){
        Map<String, String> mappOrder = new HashMap<>();
        mappOrder.put("name", "name");
        mappOrder.put("province", "province");
        mappOrder.put("telephone", "telephone");
        mappOrder.put("fax", "fax");
        mappOrder.put("email", "email");
        mappOrder.put("is_active", "isActive");
        mappOrder.put("address", "address");
        mappOrder.put("zip_code", "zipCode");
        mappOrder.put("city", "city");
        mappOrder.put("state", "state");
        mappOrder.put("rif_code", "rifCode");
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<InsuranceManagerEntity> insuranceManagerCriteriaQuery = criteriaBuilder.createQuery(InsuranceManagerEntity.class);
        Root<InsuranceManagerEntity> insuranceManagerRoot = insuranceManagerCriteriaQuery.from(InsuranceManagerEntity.class);
        insuranceManagerCriteriaQuery.select(insuranceManagerRoot);
        //Constructing list of parameters
        List<Predicate> predicates = new ArrayList<>();

        if(name != null && !name.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    insuranceManagerRoot.get(InsuranceManagerEntity_.name)), "%"+name.toUpperCase()+"%"));
        }

        if(province != null && !province.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    insuranceManagerRoot.get(InsuranceManagerEntity_.province)), "%"+province.toUpperCase()+"%"));
        }

        if(address != null && !address.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    insuranceManagerRoot.get(InsuranceManagerEntity_.address)), "%"+address.toUpperCase()+"%"));
        }

        if(zipCode != null && !zipCode.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    insuranceManagerRoot.get(InsuranceManagerEntity_.zipCode)), "%"+zipCode.toUpperCase()+"%"));
        }

        if(city != null && !city.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    insuranceManagerRoot.get(InsuranceManagerEntity_.city)), "%"+city.toUpperCase()+"%"));
        }

        if(state != null && !state.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    insuranceManagerRoot.get(InsuranceManagerEntity_.state)), "%"+state.toUpperCase()+"%"));
        }

        if(telephone != null && !telephone.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    insuranceManagerRoot.get(InsuranceManagerEntity_.telephone)), "%"+telephone.toUpperCase()+"%"));
        }

        if(fax != null && !fax.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    insuranceManagerRoot.get(InsuranceManagerEntity_.fax)), "%"+fax.toUpperCase()+"%"));
        }

        if(email != null && !email.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    insuranceManagerRoot.get(InsuranceManagerEntity_.email)), "%"+email.toUpperCase()+"%"));
        }

        if (isActive != null) {
            predicates.add(criteriaBuilder.equal(
                    insuranceManagerRoot.get(InsuranceManagerEntity_.isActive), isActive));
        }
        if (rifCode != null) {
            predicates.add(criteriaBuilder.equal(
                    insuranceManagerRoot.get(InsuranceManagerEntity_.rifCode), rifCode));
        }


        insuranceManagerCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));

        String fieldToOrder = null;
        if(orderBy != null)
            fieldToOrder = mappOrder.get(orderBy);
        if(fieldToOrder != null && asc != null){
            if (asc) {
                insuranceManagerCriteriaQuery.orderBy(criteriaBuilder.asc(insuranceManagerRoot.get(fieldToOrder)));
            } else {
                insuranceManagerCriteriaQuery.orderBy(criteriaBuilder.desc(insuranceManagerRoot.get(fieldToOrder)));
            }

        } else if(fieldToOrder != null) {
            insuranceManagerCriteriaQuery.orderBy(criteriaBuilder.desc(insuranceManagerRoot.get(fieldToOrder)));
        }

        List<InsuranceManagerEntity> insuranceManagerEntityList = em.createQuery(insuranceManagerCriteriaQuery).setFirstResult((page-1)*pageSize).setMaxResults(pageSize).getResultList();
        return insuranceManagerEntityList;
    }

    public Long countPaginationInsuranceManager(String name, String province, String telephone, String fax, String email, Boolean isActive, String address, String zipCode, String city, String state, Long rifCode) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<Long> insuranceInsuranceManagerCriteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<InsuranceManagerEntity> insuranceInsuranceManagerRoot = insuranceInsuranceManagerCriteriaQuery.from(InsuranceManagerEntity.class);
        insuranceInsuranceManagerCriteriaQuery.select(criteriaBuilder.count(insuranceInsuranceManagerRoot));
        //Constructing list of parameters
        List<Predicate> predicates = new ArrayList<>();

        if(name != null && !name.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    insuranceInsuranceManagerRoot.get(InsuranceManagerEntity_.name)), "%"+name.toUpperCase()+"%"));
        }

        if(province != null && !province.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    insuranceInsuranceManagerRoot.get(InsuranceManagerEntity_.province)), "%"+province.toUpperCase()+"%"));
        }

        if(address != null && !address.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    insuranceInsuranceManagerRoot.get(InsuranceManagerEntity_.address)), "%"+address.toUpperCase()+"%"));
        }

        if(zipCode != null && !zipCode.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    insuranceInsuranceManagerRoot.get(InsuranceManagerEntity_.zipCode)), "%"+zipCode.toUpperCase()+"%"));
        }

        if(city != null && !city.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    insuranceInsuranceManagerRoot.get(InsuranceManagerEntity_.city)), "%"+city.toUpperCase()+"%"));
        }

        if(state != null && !state.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    insuranceInsuranceManagerRoot.get(InsuranceManagerEntity_.state)), "%"+state.toUpperCase()+"%"));
        }

        if(telephone != null && !telephone.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    insuranceInsuranceManagerRoot.get(InsuranceManagerEntity_.telephone)), "%"+telephone.toUpperCase()+"%"));
        }

        if(fax != null && !fax.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    insuranceInsuranceManagerRoot.get(InsuranceManagerEntity_.fax)), "%"+fax.toUpperCase()+"%"));
        }

        if(email != null && !email.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    insuranceInsuranceManagerRoot.get(InsuranceManagerEntity_.email)), "%"+email.toUpperCase()+"%"));
        }

        if (isActive != null) {
            predicates.add(criteriaBuilder.equal(
                    insuranceInsuranceManagerRoot.get(InsuranceManagerEntity_.isActive), isActive));
        }

        if (rifCode != null) {
            predicates.add(criteriaBuilder.equal(
                    insuranceInsuranceManagerRoot.get(InsuranceManagerEntity_.rifCode), rifCode));
        }

        insuranceInsuranceManagerCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));
        Query sql = em.createQuery(insuranceInsuranceManagerCriteriaQuery);
        return (Long)sql.getSingleResult();
    }
}
