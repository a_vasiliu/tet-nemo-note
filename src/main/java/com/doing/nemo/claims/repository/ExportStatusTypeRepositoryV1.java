package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsFlowEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.doing.nemo.claims.entity.settings.ExportStatusTypeEntity;
import com.doing.nemo.claims.entity.settings.ExportStatusTypeEntity_;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class ExportStatusTypeRepositoryV1 {
    @PersistenceContext
    @Autowired
    private EntityManager em;

    public List<ExportStatusTypeEntity> findExportStatusType(Integer page, Integer pageSize, String orderBy, Boolean asc, ClaimsStatusEnum statusType, ClaimsFlowEnum flowType, String code, String description, Boolean isActive){
        Map<String, String> mappOrder = new HashMap<>();
        mappOrder.put("status_type", "statusType");
        mappOrder.put("flow_type", "flowType");
        mappOrder.put("code", "code");
        mappOrder.put("description", "description");
        mappOrder.put("is_active", "isActive");
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<ExportStatusTypeEntity> exportStatusTypeCriteriaQuery = criteriaBuilder.createQuery(ExportStatusTypeEntity.class);
        Root<ExportStatusTypeEntity> exportStatusTypeRoot = exportStatusTypeCriteriaQuery.from(ExportStatusTypeEntity.class);
        exportStatusTypeCriteriaQuery.select(exportStatusTypeRoot);
        //Constructing list of parameters
        List<Predicate> predicates = new ArrayList<>();

        if (code != null && !code.isEmpty()) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    exportStatusTypeRoot.get(ExportStatusTypeEntity_.code)), "%" + code.toUpperCase() + "%"));
        }

        if (statusType != null) {
            predicates.add(criteriaBuilder.equal(
                    exportStatusTypeRoot.get(ExportStatusTypeEntity_.statusType), statusType ));
        }

        if (flowType != null) {
            predicates.add(criteriaBuilder.equal(
                    exportStatusTypeRoot.get(ExportStatusTypeEntity_.flowType), flowType ));
        }

        if (description != null && !description.isEmpty()) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    exportStatusTypeRoot.get(ExportStatusTypeEntity_.description)), "%" + description.toUpperCase() + "%"));
        }

        if (isActive != null) {
            predicates.add(criteriaBuilder.equal(
                    exportStatusTypeRoot.get(ExportStatusTypeEntity_.isActive), isActive));
        }

        exportStatusTypeCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));

        String fieldToOrder = null;
        if (orderBy != null)
            fieldToOrder = mappOrder.get(orderBy);
        if (fieldToOrder != null && asc != null) {
            if (asc) {
                exportStatusTypeCriteriaQuery.orderBy(criteriaBuilder.asc(exportStatusTypeRoot.get(fieldToOrder)));
            } else {
                exportStatusTypeCriteriaQuery.orderBy(criteriaBuilder.desc(exportStatusTypeRoot.get(fieldToOrder)));
            }

        } else if (fieldToOrder != null) {
            exportStatusTypeCriteriaQuery.orderBy(criteriaBuilder.desc(exportStatusTypeRoot.get(fieldToOrder)));
        }

        List<ExportStatusTypeEntity> exportStatusTypeEntityList = em.createQuery(exportStatusTypeCriteriaQuery).setFirstResult((page - 1) * pageSize).setMaxResults(pageSize).getResultList();
        return exportStatusTypeEntityList;
    }

    public Long countPaginationExportStatusType(ClaimsStatusEnum statusType, ClaimsFlowEnum flowType, String code, String description, Boolean isActive) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<Long> exportStatusTypeCriteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<ExportStatusTypeEntity> exportStatusTypeRoot = exportStatusTypeCriteriaQuery.from(ExportStatusTypeEntity.class);
        exportStatusTypeCriteriaQuery.select(criteriaBuilder.count(exportStatusTypeRoot));
        //Constructing list of parameters
        List<Predicate> predicates = new ArrayList<>();

        if (code != null && !code.isEmpty()) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    exportStatusTypeRoot.get(ExportStatusTypeEntity_.code)), "%" + code.toUpperCase() + "%"));
        }

        if (statusType != null) {
            predicates.add(criteriaBuilder.equal(
                    exportStatusTypeRoot.get(ExportStatusTypeEntity_.statusType), statusType ));
        }

        if (flowType != null) {
            predicates.add(criteriaBuilder.equal(
                    exportStatusTypeRoot.get(ExportStatusTypeEntity_.flowType), flowType ));
        }

        if (description != null && !description.isEmpty()) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    exportStatusTypeRoot.get(ExportStatusTypeEntity_.description)), "%" + description.toUpperCase() + "%"));
        }

        if (isActive != null) {
            predicates.add(criteriaBuilder.equal(
                    exportStatusTypeRoot.get(ExportStatusTypeEntity_.isActive), isActive));
        }

        exportStatusTypeCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));
        Query sql = em.createQuery(exportStatusTypeCriteriaQuery);
        return (Long) sql.getSingleResult();
    }
}
