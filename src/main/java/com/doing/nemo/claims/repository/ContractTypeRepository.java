package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.entity.settings.ContractTypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ContractTypeRepository extends JpaRepository<ContractTypeEntity, UUID> {

    @Query(value = "SELECT c FROM ContractTypeEntity c WHERE c.codContractType = :codContract")
    ContractTypeEntity findByCodContract(String codContract);

    @Query("SELECT c FROM ContractTypeEntity c WHERE c.codContractType = :contractType")
    ContractTypeEntity searchContractByCode(@Param("contractType") String contractType);
}
