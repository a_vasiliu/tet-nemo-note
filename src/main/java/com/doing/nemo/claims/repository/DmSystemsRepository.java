package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.entity.settings.DmSystemsEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface DmSystemsRepository extends JpaRepository<DmSystemsEntity, UUID> {
}
