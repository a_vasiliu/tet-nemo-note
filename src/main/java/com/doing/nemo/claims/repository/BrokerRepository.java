package com.doing.nemo.claims.repository;


import com.doing.nemo.claims.entity.settings.BrokerEntity;
import com.doing.nemo.claims.entity.settings.InsurancePolicyPersonalDataEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface BrokerRepository extends JpaRepository<BrokerEntity, UUID> {


    @Query("SELECT b FROM BrokerEntity b WHERE b.businessName = :businessName")
    BrokerEntity searchBrokerbyName(@Param("businessName") String businessName);

    @Query("SELECT b FROM BrokerEntity b WHERE b.id <> :id AND b.businessName = :businessName")
    BrokerEntity searchBrokerWithDifferentId(@Param("id") UUID id, @Param("businessName") String businessName);

    @Query("SELECT i FROM InsurancePolicyPersonalDataEntity i INNER JOIN i.brokerEntity b WHERE b.id = :brokerId")
    InsurancePolicyPersonalDataEntity searchInsurancePolicyForeignKey(@Param("brokerId") UUID id);

}
