package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.entity.settings.CustomTypeRiskEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository

public interface CustomTypeRiskRepository extends JpaRepository<CustomTypeRiskEntity, UUID> {

}
