package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.entity.enumerated.ClaimsRepairEnum;
import com.doing.nemo.claims.entity.settings.GenericAttachmentEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface GenericAttachmentRepository extends JpaRepository<GenericAttachmentEntity, UUID> {

    @Query("SELECT g FROM GenericAttachmentEntity g WHERE g.typeComplaint = :typeComplaint")
    List<GenericAttachmentEntity> searchGenericAttachmentByTypeComplaint(@Param("typeComplaint") ClaimsRepairEnum typeComplaint);

}
