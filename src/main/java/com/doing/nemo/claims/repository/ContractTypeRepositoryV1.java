package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsFlowEnum;
import com.doing.nemo.claims.entity.settings.ContractTypeEntity;
import com.doing.nemo.claims.entity.settings.ContractTypeEntity_;
import com.doing.nemo.claims.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class ContractTypeRepositoryV1 {
    @PersistenceContext
    @Autowired
    private EntityManager em;

    @Autowired
    private Util util;

    public List<ContractTypeEntity> findPaginationContractType(Integer page, Integer pageSize, String orderBy, Boolean asc, String codContractType, String description, Boolean flagWS, ClaimsFlowEnum defaultFlow, String ctrnote, Boolean ricaricar, Double franchise, Boolean isActive){
        Map<String, String> mappOrder = new HashMap<>();
        mappOrder.put("cod_contract_type", "codContractType");
        mappOrder.put("description", "description");
        mappOrder.put("unlocking_entry", "flagWS");
        mappOrder.put("default_flow", "defaultFlow");
        mappOrder.put("ctrnote", "ctrnote");
        mappOrder.put("ricaricar", "ricaricar");
        mappOrder.put("franchise", "franchise");
        mappOrder.put("is_active", "isActive");

        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<ContractTypeEntity> contractTypeCriteriaQuery = criteriaBuilder.createQuery(ContractTypeEntity.class);
        Root<ContractTypeEntity> contractTypeRoot = contractTypeCriteriaQuery.from(ContractTypeEntity.class);

        contractTypeCriteriaQuery.select(contractTypeRoot);

        //Constructing list of parameters
        List<Predicate> predicates = new ArrayList<>();

        if (Util.isNotEmpty(codContractType)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    contractTypeRoot.get(ContractTypeEntity_.codContractType)), "%"+codContractType.toUpperCase()+"%"));
        }

        if (Util.isNotEmpty(ctrnote)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    contractTypeRoot.get(ContractTypeEntity_.ctrnote)), "%"+ctrnote.toUpperCase()+"%"));
        }

        if (Util.isNotEmpty(description)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    contractTypeRoot.get(ContractTypeEntity_.description)), "%"+description.toUpperCase()+"%"));
        }

        if (ricaricar != null) {
            predicates.add(criteriaBuilder.equal(
                    contractTypeRoot.get(ContractTypeEntity_.ricaricar), ricaricar));
        }

        if (franchise != null) {
            predicates.add(criteriaBuilder.equal(
                    contractTypeRoot.get(ContractTypeEntity_.franchise), franchise));
        }

        if (flagWS != null) {
            predicates.add(criteriaBuilder.equal(
                    contractTypeRoot.get(ContractTypeEntity_.flagWS), flagWS));
        }

        if (defaultFlow != null) {
            predicates.add(criteriaBuilder.equal(
                    contractTypeRoot.get(ContractTypeEntity_.defaultFlow), defaultFlow));
        }

        if (isActive != null) {
            predicates.add(criteriaBuilder.equal(
                    contractTypeRoot.get(ContractTypeEntity_.isActive), isActive));
        }


        contractTypeCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));

        //List<Order> orderByList = new LinkedList<>();
        String fieldToOrder = null;
        if(orderBy != null)
            fieldToOrder = mappOrder.get(orderBy);
        if(fieldToOrder != null && asc != null){
            if (asc) {
                contractTypeCriteriaQuery.orderBy(criteriaBuilder.asc(contractTypeRoot.get(fieldToOrder)));
            } else {
                contractTypeCriteriaQuery.orderBy(criteriaBuilder.desc(contractTypeRoot.get(fieldToOrder)));
            }

        } else if(orderBy != null) {
            contractTypeCriteriaQuery.orderBy(criteriaBuilder.desc(contractTypeRoot.get(fieldToOrder)));
        }

        List<ContractTypeEntity> contractTypeEntityList = em.createQuery(contractTypeCriteriaQuery).setFirstResult((page-1)*pageSize).setMaxResults(pageSize).getResultList();
        return contractTypeEntityList;
    }

    public Long countPaginationContractType(String codContractType, String description, Boolean flagWS, ClaimsFlowEnum defaultFlow, String ctrnote, Boolean ricaricar, Double franchise, Boolean isActive) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<Long> contractTypeCriteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<ContractTypeEntity> contractTypeRoot = contractTypeCriteriaQuery.from(ContractTypeEntity.class);

        contractTypeCriteriaQuery.select(criteriaBuilder.count(contractTypeRoot));

        //Constructing list of parameters
        List<Predicate> predicates = new ArrayList<>();

        if (Util.isNotEmpty(codContractType)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    contractTypeRoot.get(ContractTypeEntity_.codContractType)), "%"+codContractType.toUpperCase()+"%"));
        }

        if (Util.isNotEmpty(ctrnote)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    contractTypeRoot.get(ContractTypeEntity_.ctrnote)), "%"+ctrnote.toUpperCase()+"%"));
        }

        if (Util.isNotEmpty(description)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    contractTypeRoot.get(ContractTypeEntity_.description)), "%"+description.toUpperCase()+"%"));
        }

        if (ricaricar != null) {
            predicates.add(criteriaBuilder.equal(
                    contractTypeRoot.get(ContractTypeEntity_.ricaricar), ricaricar));
        }

        if (franchise != null) {
            predicates.add(criteriaBuilder.equal(
                    contractTypeRoot.get(ContractTypeEntity_.franchise), franchise));
        }

        if (flagWS != null) {
            predicates.add(criteriaBuilder.equal(
                    contractTypeRoot.get(ContractTypeEntity_.flagWS), flagWS));
        }

        if (defaultFlow != null) {
            predicates.add(criteriaBuilder.equal(
                    contractTypeRoot.get(ContractTypeEntity_.defaultFlow), defaultFlow));
        }

        if (isActive != null) {
            predicates.add(criteriaBuilder.equal(
                    contractTypeRoot.get(ContractTypeEntity_.isActive), isActive));
        }

        contractTypeCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));
        Query sql = em.createQuery(contractTypeCriteriaQuery);
        return (Long) sql.getSingleResult();

    }
}
