package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.entity.enumerated.ClaimsRepairEnum;
import com.doing.nemo.claims.entity.settings.MotivationEntity;
import com.doing.nemo.claims.entity.settings.MotivationEntity_;
import com.doing.nemo.claims.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class MotivationRepositoryV1 {

    @PersistenceContext
    @Autowired
    private EntityManager em;

    public List<MotivationEntity> findMotivationsFiltered(Integer page, Integer pageSize, ClaimsRepairEnum typeComplaint, String answerType, String description, Boolean isActive,Boolean asc, String orderBy) {
        Map<String, String> mappOrder = new HashMap<>();
        mappOrder.put("answer_type", "answerType");
        mappOrder.put("description", "description");
        //mappOrder.put("type_complaint", "typeComplaint");
        mappOrder.put("is_active", "isActive");

        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<MotivationEntity> motivationCriteriaQuery = criteriaBuilder.createQuery(MotivationEntity.class);
        Root<MotivationEntity> motivationRoot = motivationCriteriaQuery.from(MotivationEntity.class);

        motivationCriteriaQuery.select(motivationRoot);

        //Constructing list of parameters
        List<Predicate> predicates = new ArrayList<>();

        if (Util.isNotEmpty(answerType)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    motivationRoot.get(MotivationEntity_.answerType)), "%"+answerType.toUpperCase()+"%"));
        }
        if (Util.isNotEmpty(description)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    motivationRoot.get(MotivationEntity_.description)), "%"+description.toUpperCase()+"%"));
        }
        /*if (typeComplaint != null) {
            predicates.add(criteriaBuilder.equal(
                    motivationRoot.get(MotivationEntity_.typeComplaint), typeComplaint));
        }*/
        if (isActive != null) {
            predicates.add(criteriaBuilder.equal(
                    motivationRoot.get(MotivationEntity_.isActive), isActive));
        }

        motivationCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));

        //List<Order> orderByList = new LinkedList<>();
        String fieldToOrder = null;
        if(orderBy != null)
            fieldToOrder = mappOrder.get(orderBy);
        if(fieldToOrder != null && asc != null){
            if (asc) {
                motivationCriteriaQuery.orderBy(criteriaBuilder.asc(motivationRoot.get(fieldToOrder)));
            } else {
                motivationCriteriaQuery.orderBy(criteriaBuilder.desc(motivationRoot.get(fieldToOrder)));
            }

        } else if(fieldToOrder != null) {
            motivationCriteriaQuery.orderBy(criteriaBuilder.desc(motivationRoot.get(fieldToOrder)));
        }

        List<MotivationEntity> motivationEntityList = em.createQuery(motivationCriteriaQuery).setFirstResult((page-1)*pageSize).setMaxResults(pageSize).getResultList();
        return motivationEntityList;

    }
    
    public Long countMotivationFiltered(ClaimsRepairEnum typeComplaint, String answerType, String description, Boolean isActive) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<Long> motivationCriteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<MotivationEntity> motivationRoot = motivationCriteriaQuery.from(MotivationEntity.class);

        motivationCriteriaQuery.select(criteriaBuilder.count(motivationRoot));

        //Constructing list of parameters
        List<Predicate> predicates = new ArrayList<>();

        if (Util.isNotEmpty(answerType)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    motivationRoot.get(MotivationEntity_.answerType)), "%"+answerType.toUpperCase()+"%"));
        }
        if (Util.isNotEmpty(description)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    motivationRoot.get(MotivationEntity_.description)), "%"+description.toUpperCase()+"%"));
        }
        /*if (typeComplaint != null) {
            predicates.add(criteriaBuilder.equal(
                    motivationRoot.get(MotivationEntity_.typeComplaint), typeComplaint));
        }*/
        if (isActive != null) {
            predicates.add(criteriaBuilder.equal(
                    motivationRoot.get(MotivationEntity_.isActive), isActive));
        }

        motivationCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));
        Query sql = em.createQuery(motivationCriteriaQuery);
        return (Long)sql.getSingleResult();

    }


    





}
