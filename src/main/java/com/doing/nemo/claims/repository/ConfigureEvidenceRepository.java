package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.entity.settings.ConfigureEvidenceEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;
@Repository
public interface ConfigureEvidenceRepository extends JpaRepository<ConfigureEvidenceEntity, UUID> {
    @Query("SELECT c FROM ConfigureEvidenceEntity c WHERE c.practicalStateEn = :practicalStateEn AND c.isActive = true")
    ConfigureEvidenceEntity searchEvidenceByPracticalState(@Param("practicalStateEn") String practicalStateEn);

    @Query("SELECT c FROM ConfigureEvidenceEntity c WHERE c.practicalStateEn = :practicalStateEn ")
    List<ConfigureEvidenceEntity> checkDuplicateEvidence(@Param("practicalStateEn") String practicalStateEn);

}
