package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.entity.settings.LawyerPaymentsEntity;
import com.doing.nemo.claims.util.Util;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.BadParametersException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class LawyerPaymentsEntityRepositoryV1 {

    private static Logger LOGGER = LoggerFactory.getLogger(LawyerPaymentsEntityRepositoryV1.class);
    @PersistenceContext
    @Autowired
    private EntityManager em;

    public BigInteger countLawyerPaymentsForPagination(String plate, Long practiceId, LocalDateTime dateAccidentFrom, LocalDateTime dateAccidentTo,
                                                       String paymentType, String bank, String lawyerCode, String accSale, String status) {

        StringBuilder sql = new StringBuilder("SELECT COUNT (*) FROM lawyer_payments l ");
        StringBuilder whereCondition = new StringBuilder(" WHERE 1=1 ");

        if (dateAccidentFrom != null && dateAccidentTo != null) {

            if (dateAccidentTo.compareTo(dateAccidentFrom) < 0) {
                LOGGER.debug("the date_accident_to is less than the date_accident_from");
                throw new BadParametersException("the date_accident_to is less than the date_accident_from", MessageCode.CLAIMS_1011);
            } else {
                whereCondition.append(" AND (l.date_accident BETWEEN '" + dateAccidentFrom + "'\\:\\:timestamp AND '" + dateAccidentTo + "'\\:\\:timestamp )");
            }
        } else if ((dateAccidentFrom != null && dateAccidentTo == null) || (dateAccidentFrom == null && dateAccidentTo != null)) {
            LOGGER.debug("the date_accident_to and date_accident_from must be present together ");
            throw new BadParametersException("the date_accident_to and date_accident_from must be present together ", MessageCode.CLAIMS_1011);
        }

        if (Util.isNotEmpty(plate)) {
            whereCondition.append(" AND l.plate = '" + plate + "' ");
        }

        if (practiceId != null) {
            whereCondition.append(" AND l.practice_id = " + practiceId + " ");
        }

        if (status != null) {
            whereCondition.append(" AND l.status ILIKE '" + status + "' ");
        }

        if (Util.isNotEmpty(paymentType)) {
            whereCondition.append(" AND l.payment_type ILIKE '" + paymentType + "' ");
        }

        if (Util.isNotEmpty(bank)) {
            whereCondition.append(" AND l.bank ILIKE '%" + bank + "%' ");
        }

        if (lawyerCode != null) {
            whereCondition.append(" AND l.lawyer_code = " + lawyerCode + " ");
        }

        if (Util.isNotEmpty(accSale)) {
            whereCondition.append(" AND l.acc_sale ILIKE '" + accSale + "' ");
        }

        sql.append(whereCondition);

        Query query = em.createNativeQuery(sql.toString());

        return (BigInteger) query.getSingleResult();

    }

    public List<LawyerPaymentsEntity> findLawyerPaymentsForPagination(String plate, Long practiceId, LocalDateTime dateAccidentFrom, LocalDateTime dateAccidentTo,
                                                                      String paymentType, String bank, String lawyerCode, String accSale, String status, int page, int pageSize) {

        Map<String, String> mappOrder = new HashMap<>();
        mappOrder.put("plate", " claims.type ");
        mappOrder.put("practice_id", " claims.practice_id ");
        mappOrder.put("date_accident", " (claims.complaint->>'data_accident')\\:\\:jsonb->>'date_accident' ");
        mappOrder.put("acc_sale", " claims.complaint->>'plate' ");
        mappOrder.put("payment_type", " claims.created_at ");
        mappOrder.put("bank", " (claims.complaint->>'data_accident')\\:\\:jsonb->>'type_accident' ");
        mappOrder.put("lawyer_code", " claims.complaint->>'locator' ");

        StringBuilder sql = new StringBuilder("SELECT * FROM lawyer_payments l");
        StringBuilder whereCondition = new StringBuilder(" WHERE  1 = 1 ");

        if (dateAccidentFrom != null && dateAccidentTo != null) {

            if (dateAccidentTo.compareTo(dateAccidentFrom) < 0) {
                LOGGER.debug("the date_accident_to is less than the date_accident_from");
                throw new BadParametersException("the date_accident_to is less than the date_accident_from", MessageCode.CLAIMS_1011);
            } else {
                whereCondition.append(" AND (l.date_accident BETWEEN '" + dateAccidentFrom + "'\\:\\:timestamp AND '" + dateAccidentTo + "'\\:\\:timestamp )");
            }
        } else if ((dateAccidentFrom != null && dateAccidentTo == null) || (dateAccidentFrom == null && dateAccidentTo != null)) {
            LOGGER.debug("the date_accident_to and date_accident_from must be present together ");
            throw new BadParametersException("the date_accident_to and date_accident_from must be present together ", MessageCode.CLAIMS_1011);
        }

        if (Util.isNotEmpty(plate)) {
            whereCondition.append(" AND l.plate = '" + plate + "' ");
        }

        if (status != null) {
            whereCondition.append(" AND l.status ILIKE '" + status + "' ");
        }

        if (practiceId != null) {
            whereCondition.append(" AND l.practice_id = " + practiceId + " ");
        }

        if (Util.isNotEmpty(paymentType)) {
            whereCondition.append(" AND l.payment_type ILIKE '" + paymentType + "' ");
        }

        if (Util.isNotEmpty(bank)) {
            whereCondition.append(" AND l.bank ILIKE '%" + bank + "%' ");
        }

        if (lawyerCode != null) {
            whereCondition.append(" AND l.lawyer_code = " + lawyerCode + " ");
        }

        if (Util.isNotEmpty(accSale)) {
            whereCondition.append(" AND l.acc_sale ILIKE '" + accSale + "' ");
        }

        sql.append(whereCondition);

       /* String fieldToOrder = null;
        if (orderBy != null)
            fieldToOrder = mappOrder.get(orderBy);
        if (fieldToOrder != null && asc != null) {
            if (asc) {
                sql.append(" ORDER BY " + fieldToOrder + " ASC");
            } else {
                sql.append(" ORDER BY " + fieldToOrder + " DESC");
            }

        } else if (fieldToOrder != null) {
            sql.append(" ORDER BY " + fieldToOrder + " DESC");
        }*/

        sql.append(" LIMIT " + pageSize + " OFFSET " + (page - 1) * pageSize);

        Query query = em.createNativeQuery(sql.toString(), LawyerPaymentsEntity.class);


        return query.getResultList();
    }

}
