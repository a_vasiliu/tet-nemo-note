package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.entity.settings.InspectorateEntity;
import com.doing.nemo.claims.entity.settings.InspectorateEntity_;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class InspectorateRepositoryV1 {

    @PersistenceContext
    @Autowired
    private EntityManager em;
    
    public List<InspectorateEntity> findInspectoratesFiltered(Integer page, Integer pageSize, String orderBy, Boolean asc, String name, String email, String province, String telephone, String fax, Boolean isActive, String address, String zipCode, String city, String state, Long code) {
        Map<String, String> mappOrder = new HashMap<>();
        mappOrder.put("name", "name");
        mappOrder.put("province", "province");
        mappOrder.put("telephone", "telephone");
        mappOrder.put("fax", "fax");
        mappOrder.put("email", "email");
        mappOrder.put("is_active", "isActive");
        mappOrder.put("address", "address");
        mappOrder.put("zip_code", "zipCode");
        mappOrder.put("city", "city");
        mappOrder.put("state", "state");
        mappOrder.put("code", "code");
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<InspectorateEntity> inspectorateCriteriaQuery = criteriaBuilder.createQuery(InspectorateEntity.class);
        Root<InspectorateEntity> inspectorateRoot = inspectorateCriteriaQuery.from(InspectorateEntity.class);
        inspectorateCriteriaQuery.select(inspectorateRoot);
        //Constructing list of parameters
        List<Predicate> predicates = new ArrayList<>();

        if(name != null && !name.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    inspectorateRoot.get(InspectorateEntity_.name)), "%"+name.toUpperCase()+"%"));
        }

        if(province != null && !province.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    inspectorateRoot.get(InspectorateEntity_.province)), "%"+province.toUpperCase()+"%"));
        }

        if(address != null && !address.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    inspectorateRoot.get(InspectorateEntity_.address)), "%"+address.toUpperCase()+"%"));
        }

        if(zipCode != null && !zipCode.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    inspectorateRoot.get(InspectorateEntity_.zipCode)), "%"+zipCode.toUpperCase()+"%"));
        }

        if(city != null && !city.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    inspectorateRoot.get(InspectorateEntity_.city)), "%"+city.toUpperCase()+"%"));
        }

        if(state != null && !state.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    inspectorateRoot.get(InspectorateEntity_.state)), "%"+state.toUpperCase()+"%"));
        }

        if(telephone != null && !telephone.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    inspectorateRoot.get(InspectorateEntity_.telephone)), "%"+telephone.toUpperCase()+"%"));
        }

        if(fax != null && !fax.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    inspectorateRoot.get(InspectorateEntity_.fax)), "%"+fax.toUpperCase()+"%"));
        }

        if(email != null && !email.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    inspectorateRoot.get(InspectorateEntity_.email)), "%"+email.toUpperCase()+"%"));
        }

        if (isActive != null) {
            predicates.add(criteriaBuilder.equal(
                    inspectorateRoot.get(InspectorateEntity_.isActive), isActive));
        }

        if (code != null) {
            predicates.add(criteriaBuilder.equal(
                    inspectorateRoot.get(InspectorateEntity_.code), code));
        }



        inspectorateCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));

        String fieldToOrder = null;
        if(orderBy != null)
            fieldToOrder = mappOrder.get(orderBy);
        if(fieldToOrder != null && asc != null){
            if (asc) {
                inspectorateCriteriaQuery.orderBy(criteriaBuilder.asc(inspectorateRoot.get(fieldToOrder)));
            } else {
                inspectorateCriteriaQuery.orderBy(criteriaBuilder.desc(inspectorateRoot.get(fieldToOrder)));
            }

        } else if(fieldToOrder != null) {
            inspectorateCriteriaQuery.orderBy(criteriaBuilder.desc(inspectorateRoot.get(fieldToOrder)));
        }

        List<InspectorateEntity> inspectorateEntityList = em.createQuery(inspectorateCriteriaQuery).setFirstResult((page-1)*pageSize).setMaxResults(pageSize).getResultList();
        return inspectorateEntityList;

    }
    
    public Long countInspectorateFiltered(String name, String email, String province, String telephone, String fax, Boolean isActive, String address, String zipCode, String city, String state, Long code) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<Long> inspectorateCriteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<InspectorateEntity> inspectorateRoot = inspectorateCriteriaQuery.from(InspectorateEntity.class);
        inspectorateCriteriaQuery.select(criteriaBuilder.count(inspectorateRoot));
        //Constructing list of parameters
        List<Predicate> predicates = new ArrayList<>();

        if(name != null && !name.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    inspectorateRoot.get(InspectorateEntity_.name)), "%"+name.toUpperCase()+"%"));
        }

        if(province != null && !province.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    inspectorateRoot.get(InspectorateEntity_.province)), "%"+province.toUpperCase()+"%"));
        }

        if(address != null && !address.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    inspectorateRoot.get(InspectorateEntity_.address)), "%"+address.toUpperCase()+"%"));
        }

        if(zipCode != null && !zipCode.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    inspectorateRoot.get(InspectorateEntity_.zipCode)), "%"+zipCode.toUpperCase()+"%"));
        }

        if(city != null && !city.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    inspectorateRoot.get(InspectorateEntity_.city)), "%"+city.toUpperCase()+"%"));
        }

        if(state != null && !state.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    inspectorateRoot.get(InspectorateEntity_.state)), "%"+state.toUpperCase()+"%"));
        }

        if(telephone != null && !telephone.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    inspectorateRoot.get(InspectorateEntity_.telephone)), "%"+telephone.toUpperCase()+"%"));
        }

        if(fax != null && !fax.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    inspectorateRoot.get(InspectorateEntity_.fax)), "%"+fax.toUpperCase()+"%"));
        }

        if(email != null && !email.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    inspectorateRoot.get(InspectorateEntity_.email)), "%"+email.toUpperCase()+"%"));
        }

        if (isActive != null) {
            predicates.add(criteriaBuilder.equal(
                    inspectorateRoot.get(InspectorateEntity_.isActive), isActive));
        }
        if (code != null) {
            predicates.add(criteriaBuilder.equal(
                    inspectorateRoot.get(InspectorateEntity_.code), code));
        }
        if (code != null) {
            predicates.add(criteriaBuilder.equal(
                    inspectorateRoot.get(InspectorateEntity_.code), code));
        }

        inspectorateCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));
        Query sql = em.createQuery(inspectorateCriteriaQuery);
        return (Long)sql.getSingleResult();

    }


    





}
