package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.entity.settings.LegalEntity;
import com.doing.nemo.claims.entity.settings.LegalEntity_;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class LegalRepositoryV1 {
    @PersistenceContext
    @Autowired
    private EntityManager em;

    public List<LegalEntity> findPaginationLegal(Integer page, Integer pageSize, String orderBy, Boolean asc, String name, String email, String province, String telephone, String fax, Boolean isActive, String address, String zipCode, String city, String state, Long code){
        Map<String, String> mappOrder = new HashMap<>();
        mappOrder.put("name", "name");
        mappOrder.put("province", "province");
        mappOrder.put("telephone", "telephone");
        mappOrder.put("fax", "fax");
        mappOrder.put("email", "email");
        mappOrder.put("is_active", "isActive");
        mappOrder.put("address", "address");
        mappOrder.put("zip_code", "zipCode");
        mappOrder.put("city", "city");
        mappOrder.put("state", "state");
        mappOrder.put("code", "code");
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<LegalEntity> legalCriteriaQuery = criteriaBuilder.createQuery(LegalEntity.class);
        Root<LegalEntity> legalRoot = legalCriteriaQuery.from(LegalEntity.class);
        legalCriteriaQuery.select(legalRoot);
        //Constructing list of parameters
        List<Predicate> predicates = new ArrayList<>();

        if(name != null && !name.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    legalRoot.get(LegalEntity_.name)), "%"+name.toUpperCase()+"%"));
        }

        if(province != null && !province.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    legalRoot.get(LegalEntity_.province)), "%"+province.toUpperCase()+"%"));
        }

        if(address != null && !address.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    legalRoot.get(LegalEntity_.address)), "%"+address.toUpperCase()+"%"));
        }

        if(zipCode != null && !zipCode.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    legalRoot.get(LegalEntity_.zipCode)), "%"+zipCode.toUpperCase()+"%"));
        }

        if(city != null && !city.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    legalRoot.get(LegalEntity_.city)), "%"+city.toUpperCase()+"%"));
        }

        if(state != null && !state.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    legalRoot.get(LegalEntity_.state)), "%"+state.toUpperCase()+"%"));
        }

        if(telephone != null && !telephone.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    legalRoot.get(LegalEntity_.telephone)), "%"+telephone.toUpperCase()+"%"));
        }

        if(fax != null && !fax.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    legalRoot.get(LegalEntity_.fax)), "%"+fax.toUpperCase()+"%"));
        }

        if(email != null && !email.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    legalRoot.get(LegalEntity_.email)), "%"+email.toUpperCase()+"%"));
        }

        if (isActive != null) {
            predicates.add(criteriaBuilder.equal(
                    legalRoot.get(LegalEntity_.isActive), isActive));
        }
        if (code != null) {
            predicates.add(criteriaBuilder.equal(
                    legalRoot.get(LegalEntity_.code), code));
        }

        legalCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));

        String fieldToOrder = null;
        if(orderBy != null)
            fieldToOrder = mappOrder.get(orderBy);
        if(fieldToOrder != null && asc != null){
            if (asc) {
                legalCriteriaQuery.orderBy(criteriaBuilder.asc(legalRoot.get(fieldToOrder)));
            } else {
                legalCriteriaQuery.orderBy(criteriaBuilder.desc(legalRoot.get(fieldToOrder)));
            }

        } else if(fieldToOrder != null) {
            legalCriteriaQuery.orderBy(criteriaBuilder.desc(legalRoot.get(fieldToOrder)));
        }

        List<LegalEntity> legalEntityList = em.createQuery(legalCriteriaQuery).setFirstResult((page-1)*pageSize).setMaxResults(pageSize).getResultList();
        return legalEntityList;
    }

    public Long countPaginationLegal(String name, String email, String province, String telephone, String fax, Boolean isActive, String address, String zipCode, String city, String state, Long code) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<Long> legalCriteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<LegalEntity> legalRoot = legalCriteriaQuery.from(LegalEntity.class);
        legalCriteriaQuery.select(criteriaBuilder.count(legalRoot));
        //Constructing list of parameters
        List<Predicate> predicates = new ArrayList<>();

        if(name != null && !name.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    legalRoot.get(LegalEntity_.name)), "%"+name.toUpperCase()+"%"));
        }

        if(province != null && !province.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    legalRoot.get(LegalEntity_.province)), "%"+province.toUpperCase()+"%"));
        }

        if(address != null && !address.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    legalRoot.get(LegalEntity_.address)), "%"+address.toUpperCase()+"%"));
        }

        if(zipCode != null && !zipCode.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    legalRoot.get(LegalEntity_.zipCode)), "%"+zipCode.toUpperCase()+"%"));
        }

        if(city != null && !city.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    legalRoot.get(LegalEntity_.city)), "%"+city.toUpperCase()+"%"));
        }

        if(state != null && !state.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    legalRoot.get(LegalEntity_.state)), "%"+state.toUpperCase()+"%"));
        }

        if(telephone != null && !telephone.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    legalRoot.get(LegalEntity_.telephone)), "%"+telephone.toUpperCase()+"%"));
        }

        if(fax != null && !fax.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    legalRoot.get(LegalEntity_.fax)), "%"+fax.toUpperCase()+"%"));
        }

        if(email != null && !email.isEmpty()){
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    legalRoot.get(LegalEntity_.email)), "%"+email.toUpperCase()+"%"));
        }

        if (isActive != null) {
            predicates.add(criteriaBuilder.equal(
                    legalRoot.get(LegalEntity_.isActive), isActive));
        }

        if (code != null) {
            predicates.add(criteriaBuilder.equal(
                    legalRoot.get(LegalEntity_.code), code));
        }

        legalCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));
        Query sql = em.createQuery(legalCriteriaQuery);
        return (Long)sql.getSingleResult();

    }
}
