package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.settings.ExportClaimsTypeEntity;
import com.doing.nemo.claims.entity.settings.ExportClaimsTypeEntity_;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class ExportClaimsTypeRepositoryV1 {
    @PersistenceContext
    @Autowired
    private EntityManager em;

    public List<ExportClaimsTypeEntity> findExportClaimsType(Integer page, Integer pageSize, String orderBy, Boolean asc, DataAccidentTypeAccidentEnum claimsType, String code, String description, Boolean isActive){
        String query = new String();
        Map<String, String> mappOrder = new HashMap<>();
        mappOrder.put("claims_type", "claimsType");
        mappOrder.put("code", "code");
        mappOrder.put("description", "description");
        mappOrder.put("is_active", "isActive");
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<ExportClaimsTypeEntity> exportClaimsTypeCriteriaQuery = criteriaBuilder.createQuery(ExportClaimsTypeEntity.class);
        Root<ExportClaimsTypeEntity> exportClaimsTypeRoot = exportClaimsTypeCriteriaQuery.from(ExportClaimsTypeEntity.class);
        exportClaimsTypeCriteriaQuery.select(exportClaimsTypeRoot);
        //Constructing list of parameters
        List<Predicate> predicates = new ArrayList<>();

        if (code != null && !code.isEmpty()) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    exportClaimsTypeRoot.get(ExportClaimsTypeEntity_.code)), "%" + code.toUpperCase() + "%"));
        }

        if (claimsType != null) {
            predicates.add(criteriaBuilder.equal(
                    exportClaimsTypeRoot.get(ExportClaimsTypeEntity_.claimsType), claimsType ));
        }

        if (description != null && !description.isEmpty()) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    exportClaimsTypeRoot.get(ExportClaimsTypeEntity_.description)), "%" + description.toUpperCase() + "%"));
        }

        if (isActive != null) {
            predicates.add(criteriaBuilder.equal(
                    exportClaimsTypeRoot.get(ExportClaimsTypeEntity_.isActive), isActive));
        }

        exportClaimsTypeCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));

        String fieldToOrder = null;
        if (orderBy != null)
            fieldToOrder = mappOrder.get(orderBy);
        if (fieldToOrder != null && asc != null) {
            if (asc) {
                exportClaimsTypeCriteriaQuery.orderBy(criteriaBuilder.asc(exportClaimsTypeRoot.get(fieldToOrder)));
            } else {
                exportClaimsTypeCriteriaQuery.orderBy(criteriaBuilder.desc(exportClaimsTypeRoot.get(fieldToOrder)));
            }

        } else if (fieldToOrder != null) {
            exportClaimsTypeCriteriaQuery.orderBy(criteriaBuilder.desc(exportClaimsTypeRoot.get(fieldToOrder)));
        }

        List<ExportClaimsTypeEntity> exportClaimsTypeEntityList = em.createQuery(exportClaimsTypeCriteriaQuery).setFirstResult((page - 1) * pageSize).setMaxResults(pageSize).getResultList();
        return exportClaimsTypeEntityList;
    }

    public Long countPaginationExportClaimsType(DataAccidentTypeAccidentEnum claimsType, String code, String description, Boolean isActive) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<Long> exportClaimsTypeCriteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<ExportClaimsTypeEntity> exportClaimsTypeRoot = exportClaimsTypeCriteriaQuery.from(ExportClaimsTypeEntity.class);
        exportClaimsTypeCriteriaQuery.select(criteriaBuilder.count(exportClaimsTypeRoot));
        //Constructing list of parameters
        List<Predicate> predicates = new ArrayList<>();

        if (code != null && !code.isEmpty()) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    exportClaimsTypeRoot.get(ExportClaimsTypeEntity_.code)), "%" + code.toUpperCase() + "%"));
        }

        if (claimsType != null) {
            predicates.add(criteriaBuilder.equal(
                    exportClaimsTypeRoot.get(ExportClaimsTypeEntity_.claimsType), claimsType ));
        }

        if (description != null && !description.isEmpty()) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    exportClaimsTypeRoot.get(ExportClaimsTypeEntity_.description)), "%" + description.toUpperCase() + "%"));
        }

        if (isActive != null) {
            predicates.add(criteriaBuilder.equal(
                    exportClaimsTypeRoot.get(ExportClaimsTypeEntity_.isActive), isActive));
        }

        exportClaimsTypeCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));
        Query sql = em.createQuery(exportClaimsTypeCriteriaQuery);
        return (Long) sql.getSingleResult();

    }
}
