package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.entity.settings.NotificationTypeEntity;
import com.doing.nemo.claims.entity.settings.NotificationTypeEntity_;
import com.doing.nemo.claims.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class NotificationTypeRepositoryV1 {
    @PersistenceContext
    @Autowired
    private EntityManager em;


    public List<NotificationTypeEntity> findPaginationNotificationType(Integer page, Integer pageSize, String description, Boolean isActive, Integer orderId, String orderBy, Boolean asc){
        String query = new String();
        Map<String, String> mappOrder = new HashMap<>();
        mappOrder.put("description", "description");
        mappOrder.put("order_id", "orderId");
        mappOrder.put("is_active", "isActive");


        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<NotificationTypeEntity> notificationTypeCriteriaQuery = criteriaBuilder.createQuery(NotificationTypeEntity.class);
        Root<NotificationTypeEntity> notificationTypeRoot = notificationTypeCriteriaQuery.from(NotificationTypeEntity.class);

        notificationTypeCriteriaQuery.select(notificationTypeRoot);

        //Constructing list of parameters
        List<Predicate> predicates = new ArrayList<>();

        if (Util.isNotEmpty(description)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    notificationTypeRoot.get(NotificationTypeEntity_.description)), "%"+description.toUpperCase()+"%"));
        }
        if (orderId != null) {
            predicates.add(criteriaBuilder.equal(
                    notificationTypeRoot.get(NotificationTypeEntity_.orderId), orderId));
        }
        if (isActive != null) {
            predicates.add(criteriaBuilder.equal(
                    notificationTypeRoot.get(NotificationTypeEntity_.isActive), isActive));
        }

        notificationTypeCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));

        //List<Order> orderByList = new LinkedList<>();
        String fieldToOrder = null;
        if(orderBy != null)
            fieldToOrder = mappOrder.get(orderBy);
        if(fieldToOrder != null && asc != null){
            if (asc) {
                notificationTypeCriteriaQuery.orderBy(criteriaBuilder.asc(notificationTypeRoot.get(fieldToOrder)));
            } else {
                notificationTypeCriteriaQuery.orderBy(criteriaBuilder.desc(notificationTypeRoot.get(fieldToOrder)));
            }

        } else if(fieldToOrder != null) {
            notificationTypeCriteriaQuery.orderBy(criteriaBuilder.desc(notificationTypeRoot.get(fieldToOrder)));
        }

        List<NotificationTypeEntity> notificationTypeEntityList = em.createQuery(notificationTypeCriteriaQuery).setFirstResult((page-1)*pageSize).setMaxResults(pageSize).getResultList();
        return notificationTypeEntityList;
    }

    public Long countPaginationNotificationType(String description, Boolean isActive, Integer orderId) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<Long> notificationTypeCriteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<NotificationTypeEntity> notificationTypeRoot = notificationTypeCriteriaQuery.from(NotificationTypeEntity.class);

        notificationTypeCriteriaQuery.select(criteriaBuilder.count(notificationTypeRoot));

        //Constructing list of parameters
        List<Predicate> predicates = new ArrayList<>();

        if (Util.isNotEmpty(description)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    notificationTypeRoot.get(NotificationTypeEntity_.description)), "%"+description.toUpperCase()+"%"));
        }
        if (orderId != null) {
            predicates.add(criteriaBuilder.equal(
                    notificationTypeRoot.get(NotificationTypeEntity_.orderId), orderId));
        }
        if (isActive != null) {
            predicates.add(criteriaBuilder.equal(
                    notificationTypeRoot.get(NotificationTypeEntity_.isActive), isActive));
        }

        notificationTypeCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));
        Query sql = em.createQuery(notificationTypeCriteriaQuery);
        return (Long)sql.getSingleResult();

    }
}
