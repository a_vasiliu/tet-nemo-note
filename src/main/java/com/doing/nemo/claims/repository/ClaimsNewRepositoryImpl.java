package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.adapter.DateAdapter;
import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.response.ClosingResponseV1;
import com.doing.nemo.claims.dto.ClaimsExportFilter;
import com.doing.nemo.claims.entity.*;
import com.doing.nemo.claims.entity.claims.*;

import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsFlowEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.EntrustedEnum.EntrustedEnum;
import com.doing.nemo.claims.entity.enumerated.DamagedEnum.VehicleEnum.VehicleNatureEnum;
import com.doing.nemo.claims.entity.enumerated.myAld.MyAldInsertedByEnum;
import com.doing.nemo.claims.entity.jsonb.Exemption;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.ImpactPoint;
import com.doing.nemo.claims.entity.settings.ContractTypeEntity;
import com.doing.nemo.claims.entity.settings.ContractTypeEntity_;
import com.doing.nemo.claims.service.GoLiveStrategyService;
import com.doing.nemo.claims.util.Util;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.BadParametersException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import javax.persistence.criteria.*;
import javax.persistence.metamodel.SingularAttribute;
import javax.transaction.Transactional;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;

import static com.doing.nemo.claims.entity.jsonb.JsonbProcessingFunctions.JSONB_EXTRACT_PATH_TEXT;

@Repository
public class ClaimsNewRepositoryImpl {

    private static Logger LOGGER = LoggerFactory.getLogger(ClaimsNewRepositoryImpl.class);
    @PersistenceContext
    @Autowired
    private EntityManager em;
    @Autowired
    private Util util;

    @Autowired
    private DateAdapter dateAdapter;

    @Value("${acclaims.id}")
    private String acclaimsId;

    @Value("${acclaims.sr.id}")
    private String acclaimsSrId;

    @Value("${time.zone}")
    private String timeZoneApplication;

    @Value("${acclaims.extraction.startdate}")
    private String entrustedAtStart;
    //START per stats


    @Autowired
    private GoLiveStrategyService goLiveStrategyService;

    public List<Object[]> getStatsClaimsEvidence() {
        StringBuilder sql = new StringBuilder("SELECT claims.type, lower(claims.status), count(DISTINCT claims.id) AS counter, claims.in_evidence, claims.with_continuation FROM claims_new claims ");
        StringBuilder whereCondition = new StringBuilder(" WHERE claims.status NOT IN ('DRAFT','DEMOLITION', 'WAIT_LOST_POSSESSION','LOST_POSSESSION','WAIT_TO_RETURN_POSSESSION', 'RETURN_TO_POSSESSION', 'TO_REREGISTER', 'TO_WORK', 'BOOK_DUPLICATION','STAMP', 'DEMOLITION', 'RECEPTIONS_TO_BE_CONFIRMED', 'WAITING_FOR_AUTHORITY' ) " +
                " AND claims.type != 'NO'  ");
        sql.append(whereCondition);
        sql.append(" GROUP BY claims.status, claims.type ,claims.in_evidence, claims.with_continuation");
        Query query = em.createNativeQuery(sql.toString());
        List<Object[]> resultList = query.getResultList();
        return resultList;
    }


    public List<Object[]> getStatsClaimsEvidenceWithStatusWaitingForAuthority() {

        StringBuilder sql = new StringBuilder("SELECT claims.type, lower(claims.status), count(DISTINCT claims.id) AS counter, claims.in_evidence, claims.with_continuation FROM (claims_new claims left join claims_to_claims_authority claut on claims.id = claut.claims_id) left join claims_authority ca on claut.authority_id = ca.id ");
        StringBuilder whereCondition = new StringBuilder(" WHERE claims.type != 'NO' AND claims.status = 'WAITING_FOR_AUTHORITY' AND  ( ca.is_invoiced IS NULL OR ca.is_invoiced = false)  ");
        sql.append(whereCondition);
        sql.append(" GROUP BY claims.status, claims.type ,claims.in_evidence, claims.with_continuation");

        Query query = em.createNativeQuery(sql.toString());
        List<Object[]> resultList = query.getResultList();
        return resultList;
    }


    public List<Object[]> getStatsClaimsEvidenceWithStatusWaitingForNumberSx() {
        StringBuilder sql = new StringBuilder("SELECT claims.type, lower(claims.status), count(DISTINCT claims.id) AS counter, claims.in_evidence, claims.with_continuation FROM claims_new claims left join claims_from_company fc on claims.id = fc.id ");
        StringBuilder whereCondition = new StringBuilder(" WHERE claims.type = 'FUL' AND claims.status = 'WAITING_FOR_AUTHORITY' AND (fc.number_sx IS NULL OR fc.number_sx = '') ");
        sql.append(whereCondition);
        sql.append(" GROUP BY claims.status, claims.type ,claims.in_evidence, claims.with_continuation");
        Query query = em.createNativeQuery(sql.toString());
        List<Object[]> resultList = query.getResultList();
        return resultList;
    }


    public List<Object[]> getStatsClaimsEvidencePoVariation() {
        StringBuilder sql = new StringBuilder("SELECT claims.type, count(DISTINCT claims.id) AS counter, claims.in_evidence, claims.with_continuation FROM claims_new claims ");
        StringBuilder whereCondition = new StringBuilder(" WHERE claims.status NOT IN ('DRAFT','DEMOLITION', 'WAIT_LOST_POSSESSION','LOST_POSSESSION','WAIT_TO_RETURN_POSSESSION', 'RETURN_TO_POSSESSION', 'TO_REREGISTER', 'TO_WORK', 'BOOK_DUPLICATION','STAMP', 'DEMOLITION', 'RECEPTIONS_TO_BE_CONFIRMED') " +
                " AND claims.type != 'NO' AND po_variation = true ");
        sql.append(whereCondition);
        sql.append(" GROUP BY claims.type ,claims.in_evidence, claims.with_continuation");
        Query query = em.createNativeQuery(sql.toString());
        List<Object[]> resultList = query.getResultList();
        return resultList;
    }

    //END per stats


    //START paginazione

    public BigInteger countClaimsForPagination(Boolean inEvidence, Long practiceId, List<String> flow, List<Long> clientIdList, String plate, List<String> status, String locator, String dateAccidentFrom, String dateAccidentTo, List<String> typeAccident, Boolean asc, String orderBy, Integer page, Integer pageSize, Boolean withContinuation, String company, Boolean withReceptions, String clientName, Boolean poVariation, Long fleetVehicleId) {
        Map<String, String> mappOrder = new HashMap<>();
        mappOrder.put("type", " claims.type ");
        mappOrder.put("practice_id", " claims.practice_id ");
        mappOrder.put("date_accident", " claims.date_accident ");
        mappOrder.put("plate", " claims.plate ");
        mappOrder.put("created_at", " claims.created_at ");
        mappOrder.put("type_accident", " claims.type_accident ");
        mappOrder.put("locator", " claims.locator ");

        StringBuffer sql = new StringBuffer("SELECT COUNT (*) ");
        StringBuffer fromCondition = new StringBuffer(" FROM claims_new claims");
        StringBuffer whereCondition = new StringBuffer(" WHERE 1=1 ");




        if (dateAccidentFrom != null && dateAccidentTo != null) {

            if (dateAccidentTo.compareTo(dateAccidentFrom) < 0) {
                LOGGER.debug("the date_accident_to is less than the date_accident_from");
                throw new BadParametersException("the date_accident_to is less than the date_accident_from", MessageCode.CLAIMS_1011);
            } else {
                whereCondition.append(" AND (claims.date_accident\\:\\:timestamp BETWEEN '" + dateAccidentFrom + "'\\:\\:timestamp AND '" + dateAccidentTo + "'\\:\\:timestamp )");
            }
        } else if ((dateAccidentFrom != null) || (dateAccidentTo != null)) {
            LOGGER.debug("the date_accident_to and date_accident_from must be present together ");
            throw new BadParametersException("the date_accident_to and date_accident_from must be present together ", MessageCode.CLAIMS_1011);
        }

        if((clientIdList != null && !clientIdList.isEmpty()) || (clientName != null && !clientName.isEmpty()) ) {
            fromCondition.append(" inner join claims_damaged_customer cu on claims.id = cu.id ");
            if (clientIdList != null && !clientIdList.isEmpty()) {
                whereCondition.append(" AND cu.customer_id IN (:clientIdList) ");
            }

            if (Util.isNotEmpty(clientName)) {
                whereCondition.append(" AND cu.legal_name ILIKE '%" + clientName + "%'  ");
            }
        }

        if (fleetVehicleId != null) {
            fromCondition.append(" inner join claims_damaged_vehicle ve on claims.id = ve.id ");
            whereCondition.append(" AND ve.fleet_vehicle_id = '" + fleetVehicleId + "'  ");
        }

        if (Util.isNotEmpty(plate)) {
            whereCondition.append(" AND claims.plate ILIKE '%" + plate.trim() + "%' ");
        }

        if (Util.isNotEmpty(locator)) {
            whereCondition.append(" AND claims.claim_locator ILIKE '%" + locator + "%' ");
        }

        if (practiceId != null) {
            whereCondition.append(" AND claims.practice_id = " + practiceId + " ");
        }
        if (inEvidence != null) {
            if(inEvidence) {
                whereCondition.append(" AND claims.in_evidence = " + inEvidence + " ");
            }else{
                whereCondition.append(" AND (claims.in_evidence IS NULL OR claims.in_evidence = " + inEvidence + ") ");
            }
        }

        if (withContinuation != null) {
            whereCondition = this.addWithContinuationFilter(withContinuation,whereCondition);
        }

        if (poVariation != null) {
            if(poVariation) {
                whereCondition.append(" AND claims.po_variation =" + poVariation + " ");
            }else{
                whereCondition.append(" AND ( claims.po_variation IS NULL OR claims.po_variation =" + poVariation + ") ");
            }
        }

        if (withReceptions != null) {
            fromCondition.append(" inner join claims_theft th on claims.id = th.id ");
            if(withReceptions) {
                whereCondition.append(" AND th.is_with_receptions\\:\\:bool =" + withReceptions + " ");
            }else{
                whereCondition.append(" AND ( th.is_with_receptions IS NULL OR th.is_with_receptions\\:\\:bool =" + withReceptions + ") ");
            }
        }

        if (typeAccident != null) {
            whereCondition.append(" AND claims.type_accident IN (:typeAccidentSet) ");
        }

        if (Util.isNotEmpty(company)) {
            fromCondition.append(" inner join claims_damaged_insurance_info ins on claims.id = ins.id ");
            whereCondition.append(" AND ins.tpl_company ILIKE '%" + company + "%' ");
        }
        if (status != null) {
            whereCondition.append(" AND claims.status IN (:statusSet) ");
        }

        if (flow != null) {
            whereCondition.append(" AND claims.type IN (:flowSet) ");
        }

        sql.append(fromCondition);
        sql.append(whereCondition);

        Query query = em.createNativeQuery(sql.toString());

        if (typeAccident != null)
            query.setParameter("typeAccidentSet", typeAccident);
        if (flow != null)
            query.setParameter("flowSet", flow);
        if (status != null)
            query.setParameter("statusSet", status);
        if (clientIdList != null)
            query.setParameter("clientIdList", clientIdList);

        return (BigInteger) query.getSingleResult();
    }



    public List<ClaimsNewEntity> findClaimsForPagination(Boolean inEvidence, Long practiceId, List<String> flow, List<Long> clientIdList, String plate, List<String> status, String locator, String dateAccidentFrom, String dateAccidentTo, List<String> typeAccident, Boolean asc, String orderBy, Integer page, Integer pageSize, Boolean withContinuation, String company, Boolean withReceptions, String clientName, Boolean poVariation, Long fleetVehicleId) {
        Map<String, String> mappOrder = new HashMap<>();
        mappOrder.put("type", " claims.type ");
        mappOrder.put("practice_id", " claims.practice_id ");
        mappOrder.put("date_accident", " claims.date_accident ");
        mappOrder.put("plate", " claims.plate ");
        mappOrder.put("created_at", " claims.created_at ");
        mappOrder.put("type_accident", " claims.type_accident ");
        mappOrder.put("locator", " claims.locator ");

        StringBuffer sql = new StringBuffer("SELECT *  ");
        StringBuffer fromCondition = new StringBuffer(" FROM claims_new claims");
        StringBuffer whereCondition = new StringBuffer(" WHERE 1=1 ");




        if (dateAccidentFrom != null && dateAccidentTo != null) {

            if (dateAccidentTo.compareTo(dateAccidentFrom) < 0) {
                LOGGER.debug("the date_accident_to is less than the date_accident_from");
                throw new BadParametersException("the date_accident_to is less than the date_accident_from", MessageCode.CLAIMS_1011);
            } else {
                whereCondition.append(" AND (claims.date_accident\\:\\:timestamp BETWEEN '" + dateAccidentFrom + "'\\:\\:timestamp AND '" + dateAccidentTo + "'\\:\\:timestamp )");
            }
        } else if ((dateAccidentFrom != null) || (dateAccidentTo != null)) {
            LOGGER.debug("the date_accident_to and date_accident_from must be present together ");
            throw new BadParametersException("the date_accident_to and date_accident_from must be present together ", MessageCode.CLAIMS_1011);
        }

        if((clientIdList != null && !clientIdList.isEmpty()) || (clientName != null && !clientName.isEmpty()) ) {
            fromCondition.append(" inner join claims_damaged_customer cu on claims.id = cu.id ");
            if (clientIdList != null && !clientIdList.isEmpty()) {
                whereCondition.append(" AND cu.customer_id IN (:clientIdList) ");
            }

            if (Util.isNotEmpty(clientName)) {
                whereCondition.append(" AND cu.legal_name ILIKE '%" + clientName + "%'  ");
            }
        }

        if (fleetVehicleId != null) {
            fromCondition.append(" inner join claims_damaged_vehicle ve on claims.id = ve.id ");
            whereCondition.append(" AND ve.fleet_vehicle_id = '" + fleetVehicleId + "'  ");
        }

        if (Util.isNotEmpty(plate)) {
            whereCondition.append(" AND claims.plate ILIKE '%" + plate.trim() + "%' ");
        }

        if (Util.isNotEmpty(locator)) {
            whereCondition.append(" AND claims.claim_locator ILIKE '%" + locator + "%' ");
        }

        if (practiceId != null) {
            whereCondition.append(" AND claims.practice_id = " + practiceId + " ");
        }
        if (inEvidence != null) {
            if(inEvidence) {
                whereCondition.append(" AND claims.in_evidence = " + inEvidence + " ");
            }else{
                whereCondition.append(" AND (claims.in_evidence IS NULL OR claims.in_evidence = " + inEvidence + ") ");
            }
        }

        if (withContinuation != null) {
            whereCondition = this.addWithContinuationFilter(withContinuation,whereCondition);
        }

        if (poVariation != null) {
            if(poVariation) {
                whereCondition.append(" AND claims.po_variation =" + poVariation + " ");
            }else{
                whereCondition.append(" AND ( claims.po_variation IS NULL OR claims.po_variation =" + poVariation + ") ");
            }
        }

        if (withReceptions != null) {
            fromCondition.append(" inner join claims_theft th on claims.id = th.id ");
            if(withReceptions) {
                whereCondition.append(" AND th.is_with_receptions\\:\\:bool =" + withReceptions + " ");
            }else{
                whereCondition.append(" AND ( th.is_with_receptions IS NULL OR th.is_with_receptions\\:\\:bool =" + withReceptions + ") ");
            }
        }

        if (typeAccident != null) {
            whereCondition.append(" AND claims.type_accident IN (:typeAccidentSet) ");
        }

        if (Util.isNotEmpty(company)) {
            fromCondition.append(" inner join claims_damaged_insurance_info ins on claims.id = ins.id ");
            whereCondition.append(" AND ins.tpl_company ILIKE '%" + company + "%' ");
        }
        if (status != null) {
            whereCondition.append(" AND claims.status IN (:statusSet) ");
        }

        if (flow != null) {
            whereCondition.append(" AND claims.type IN (:flowSet) ");
        }

        sql.append(fromCondition);
        sql.append(whereCondition);

        String fieldToOrder = null;
        if (orderBy != null)
            fieldToOrder = mappOrder.get(orderBy);
        if (fieldToOrder != null && asc != null) {
            if (asc) {
                sql.append(" ORDER BY " + fieldToOrder + " ASC");
            } else {
                sql.append(" ORDER BY " + fieldToOrder + " DESC");
            }

        } else if (fieldToOrder != null) {
            sql.append(" ORDER BY " + fieldToOrder + " DESC");
        }
        sql.append(" LIMIT " + pageSize + " OFFSET " + (page - 1) * pageSize);


        Query query = em.createNativeQuery(sql.toString(), ClaimsNewEntity.class);
        if (typeAccident != null)
            query.setParameter("typeAccidentSet", typeAccident);
        if (flow != null)
            query.setParameter("flowSet", flow);
        if (status != null)
            query.setParameter("statusSet", status);
        if (clientIdList != null)
            query.setParameter("clientIdList", clientIdList);

        return query.getResultList();
    }

    //paginazione operatore

    public BigInteger countClaimsForPaginationWithoutDraftAndTheft(Boolean inEvidence, Long practiceId, List<String> flow, List<Long> clientIdList, String plate, List<String> status, String locator, String dateAccidentFrom, String dateAccidentTo, List<String> typeAccident, Boolean asc, String orderBy, Integer page, Integer pageSize, Boolean withContinuation, String company, String clientName, Boolean poVariation, Boolean waitingForNumberSx, Long fleetVehicleId, Boolean waitingForAuthority) {
        Map<String, String> mappOrder = new HashMap<>();
        mappOrder.put("type", " claims.type ");
        mappOrder.put("practice_id", " claims.practice_id ");
        mappOrder.put("date_accident", " claims.date_accident ");
        mappOrder.put("plate", " claims.plate ");
        mappOrder.put("created_at", " claims.created_at ");
        mappOrder.put("type_accident", " claims.type_accident ");
        mappOrder.put("locator", " claims.locator ");

        StringBuffer sql = null;
        if(waitingForAuthority!=null) {
            sql = new StringBuffer("SELECT COUNT(DISTINCT claims.id)  ");
        }else{
            // Non è necessaria la DISTINCT in quanto la select sarà composta
            // Solo da tabelle con relazione 1 a 1
            sql = new StringBuffer("SELECT COUNT(claims.id)  ");
        }
        StringBuffer fromCondition = new StringBuffer(" FROM claims_new claims");
        StringBuffer whereCondition = new StringBuffer(" WHERE 1=1 ");




        if (dateAccidentFrom != null && dateAccidentTo != null) {

            if (dateAccidentTo.compareTo(dateAccidentFrom) < 0) {
                LOGGER.debug("the date_accident_to is less than the date_accident_from");
                throw new BadParametersException("the date_accident_to is less than the date_accident_from", MessageCode.CLAIMS_1011);
            } else {
                whereCondition.append(" AND (claims.date_accident\\:\\:timestamp BETWEEN '" + dateAccidentFrom + "'\\:\\:timestamp AND '" + dateAccidentTo + "'\\:\\:timestamp )");
            }
        } else if ((dateAccidentFrom != null) || (dateAccidentTo != null)) {
            LOGGER.debug("the date_accident_to and date_accident_from must be present together ");
            throw new BadParametersException("the date_accident_to and date_accident_from must be present together ", MessageCode.CLAIMS_1011);
        }

        if((clientIdList != null && !clientIdList.isEmpty()) || (clientName != null && !clientName.isEmpty()) ) {
            fromCondition.append(" inner join claims_damaged_customer cu on claims.id = cu.id ");
            if (clientIdList != null && !clientIdList.isEmpty()) {
                whereCondition.append(" AND cu.customer_id IN (:clientIdList) ");
            }

            if (Util.isNotEmpty(clientName)) {
                whereCondition.append(" AND cu.legal_name ILIKE '%" + clientName + "%'  ");
            }
        }

        if (fleetVehicleId != null) {
            fromCondition.append(" inner join claims_damaged_vehicle ve on claims.id = ve.id ");
            whereCondition.append(" AND ve.fleet_vehicle_id = '" + fleetVehicleId + "'  ");
        }

        if (Util.isNotEmpty(plate)) {
            whereCondition.append(" AND claims.plate ILIKE '%" + plate.trim() + "%' ");
        }

        if (Util.isNotEmpty(locator)) {
            whereCondition.append(" AND claims.claim_locator ILIKE '%" + locator + "%' ");
        }

        if (practiceId != null) {
            whereCondition.append(" AND claims.practice_id = " + practiceId + " ");
        }

        if (inEvidence != null) {
            if(inEvidence) {
                whereCondition.append(" AND claims.in_evidence = " + inEvidence + " ");
            }else{
                whereCondition.append(" AND (claims.in_evidence IS NULL OR claims.in_evidence = " + inEvidence + ") ");
            }
        }

        if (withContinuation != null) {
            whereCondition = this.addWithContinuationFilter(withContinuation,whereCondition);
        }

        if (poVariation != null) {
            if(poVariation) {
                whereCondition.append(" AND claims.po_variation =" + poVariation + " ");
            }else{
                whereCondition.append(" AND ( claims.po_variation IS NULL OR claims.po_variation =" + poVariation + ") ");
            }
        }

        if (typeAccident != null) {
            whereCondition.append(" AND claims.type_accident IN (:typeAccidentSet) ");
        }

        if (Util.isNotEmpty(company)) {
            fromCondition.append(" inner join claims_damaged_insurance_info ins on claims.id = ins.id ");
            whereCondition.append(" AND ins.tpl_company ILIKE '%" + company + "%' ");
        }

        if (status != null) {
            whereCondition.append(" AND claims.status IN (:statusSet) AND claims.status <> 'DRAFT'");
        } else {
            whereCondition.append(" AND claims.status <> 'DRAFT'");
        }

        if (flow != null) {
            whereCondition.append(" AND claims.type IN (:flowSet) ");
        }

        if (waitingForNumberSx != null) {
            fromCondition.append(" left join claims_from_company fc on claims.id = fc.id ");
            if (waitingForNumberSx) {
                whereCondition.append(" AND (fc.number_sx IS NULL OR fc.number_sx = '') ");
            } else{
                whereCondition.append(" AND (fc.number_sx IS NOT NULL AND fc.number_sx != '') ");
            }
        }

        if(waitingForAuthority != null){
            fromCondition.append(" inner join claims_to_claims_authority ca on claims.id = ca.claims_id inner join claims_authority cat on ca.authority_id = cat.id ");
            if(waitingForAuthority) {
                whereCondition.append(" AND (cat.is_invoiced is null or cat.is_invoiced <> 'true') AND (cat.event_type is null or cat.event_type <> 'REJECT') ");
            }else
                whereCondition.append(" AND claims.status = 'WAITING_FOR_AUTHORITY' AND cat.is_invoiced = true ");
        }

        sql.append(fromCondition);
        sql.append(whereCondition);

        Query query = em.createNativeQuery(sql.toString());
        System.out.println(sql.toString());
        if (typeAccident != null)
            query.setParameter("typeAccidentSet", typeAccident);
        if (flow != null)
            query.setParameter("flowSet", flow);
        if (status != null)
            query.setParameter("statusSet", status);
        if (clientIdList != null)
            query.setParameter("clientIdList", clientIdList);

        return (BigInteger) query.getSingleResult();

    }



    public List<ClaimsNewEntity> findClaimsForPaginationWithoutDraftAndTheft(Boolean inEvidence, Long practiceId, List<String> flow, List<Long> clientIdList, String plate, List<String> status, String locator, String dateAccidentFrom, String dateAccidentTo, List<String> typeAccident, Boolean asc, String orderBy, Integer page, Integer pageSize, Boolean withContinuation, String company, String clientName, Boolean poVariation, Boolean waitingForNumberSx, Long fleetVehicleId, Boolean waitingForAuthority) {
        Map<String, String> mappOrder = new HashMap<>();
        mappOrder.put("type", " claims.type ");
        mappOrder.put("practice_id", " claims.practice_id ");
        mappOrder.put("date_accident", " claims.date_accident ");
        mappOrder.put("plate", " claims.plate ");
        mappOrder.put("created_at", " claims.created_at ");
        mappOrder.put("type_accident", " claims.type_accident ");
        mappOrder.put("locator", " claims.locator ");

        StringBuffer sql = null;
        if(waitingForAuthority!=null) {
            sql = new StringBuffer("SELECT DISTINCT claims.*  ");
        }else{
            // Non è necessaria la DISTINCT in quanto la select sarà composta
            // Solo da tabelle con relazione 1 a 1
            sql = new StringBuffer("SELECT claims.*  ");
        }
        StringBuffer fromCondition = new StringBuffer(" FROM claims_new claims");
        StringBuffer whereCondition = new StringBuffer(" WHERE 1=1 ");




        if (dateAccidentFrom != null && dateAccidentTo != null) {

            if (dateAccidentTo.compareTo(dateAccidentFrom) < 0) {
                LOGGER.debug("the date_accident_to is less than the date_accident_from");
                throw new BadParametersException("the date_accident_to is less than the date_accident_from", MessageCode.CLAIMS_1011);
            } else {
                whereCondition.append(" AND (claims.date_accident\\:\\:timestamp BETWEEN '" + dateAccidentFrom + "'\\:\\:timestamp AND '" + dateAccidentTo + "'\\:\\:timestamp )");
            }
        } else if ((dateAccidentFrom != null) || (dateAccidentTo != null)) {
            LOGGER.debug("the date_accident_to and date_accident_from must be present together ");
            throw new BadParametersException("the date_accident_to and date_accident_from must be present together ", MessageCode.CLAIMS_1011);
        }

        if((clientIdList != null && !clientIdList.isEmpty()) || (clientName != null && !clientName.isEmpty()) ) {
            fromCondition.append(" inner join claims_damaged_customer cu on claims.id = cu.id ");
            if (clientIdList != null && !clientIdList.isEmpty()) {
                whereCondition.append(" AND cu.customer_id IN (:clientIdList) ");
            }

            if (Util.isNotEmpty(clientName)) {
                whereCondition.append(" AND cu.legal_name ILIKE '%" + clientName + "%'  ");
            }
        }

        if (fleetVehicleId != null) {
            fromCondition.append(" inner join claims_damaged_vehicle ve on claims.id = ve.id ");
            whereCondition.append(" AND ve.fleet_vehicle_id = '" + fleetVehicleId + "'  ");
        }

        if (Util.isNotEmpty(plate)) {
            whereCondition.append(" AND claims.plate ILIKE '%" + plate.trim() + "%' ");
        }

        if (Util.isNotEmpty(locator)) {
            whereCondition.append(" AND claims.claim_locator ILIKE '%" + locator + "%' ");
        }

        if (practiceId != null) {
            whereCondition.append(" AND claims.practice_id = " + practiceId + " ");
        }

        if (inEvidence != null) {
            if(inEvidence) {
                whereCondition.append(" AND claims.in_evidence = " + inEvidence + " ");
            }else{
                whereCondition.append(" AND (claims.in_evidence IS NULL OR claims.in_evidence = " + inEvidence + ") ");
            }
        }

        if (withContinuation != null) {
            whereCondition = this.addWithContinuationFilter(withContinuation,whereCondition);
        }

        if (poVariation != null) {
            if(poVariation) {
                whereCondition.append(" AND claims.po_variation =" + poVariation + " ");
            }else{
                whereCondition.append(" AND ( claims.po_variation IS NULL OR claims.po_variation =" + poVariation + ") ");
            }
        }


        if (typeAccident != null) {
            whereCondition.append(" AND claims.type_accident IN (:typeAccidentSet) ");
        }

        if (Util.isNotEmpty(company)) {
            fromCondition.append(" inner join claims_damaged_insurance_info ins on claims.id = ins.id ");
            whereCondition.append(" AND ins.tpl_company ILIKE '%" + company + "%' ");
        }

        if (status != null) {
            whereCondition.append(" AND claims.status IN (:statusSet) AND claims.status <> 'DRAFT'");
        } else {
            whereCondition.append(" AND claims.status <> 'DRAFT'");
        }

        if (flow != null) {
            whereCondition.append(" AND claims.type IN (:flowSet) ");
        }

        if (waitingForNumberSx != null) {
            fromCondition.append(" left join claims_from_company fc on claims.id = fc.id ");
            if (waitingForNumberSx) {
                whereCondition.append(" AND (fc.number_sx IS NULL OR fc.number_sx = '') ");
            } else{
                whereCondition.append(" AND (fc.number_sx IS NOT NULL AND fc.number_sx != '') ");
            }
        }

        if(waitingForAuthority != null){
            fromCondition.append(" inner join claims_to_claims_authority ca on claims.id = ca.claims_id inner join claims_authority cat on ca.authority_id = cat.id ");
            if(waitingForAuthority) {
                whereCondition.append(" AND (cat.is_invoiced is null or cat.is_invoiced <> 'true') AND (cat.event_type is null or cat.event_type <> 'REJECT') ");
            }else
                whereCondition.append(" AND claims.status = 'WAITING_FOR_AUTHORITY' AND cat.is_invoiced = true ");
        }

        sql.append(fromCondition);
        sql.append(whereCondition);

        String fieldToOrder = null;
        if (orderBy != null)
            fieldToOrder = mappOrder.get(orderBy);
        if (fieldToOrder != null && asc != null) {
            if (asc) {
                sql.append(" ORDER BY " + fieldToOrder + " ASC");
            } else {
                sql.append(" ORDER BY " + fieldToOrder + " DESC");
            }

        } else if (fieldToOrder != null) {
            sql.append(" ORDER BY " + fieldToOrder + " DESC");
        }

        sql.append(" LIMIT " + pageSize + " OFFSET " + (page - 1) * pageSize);

        Query query = em.createNativeQuery(sql.toString(), ClaimsNewEntity.class);
        System.out.println(sql.toString());


        if (typeAccident != null)
            query.setParameter("typeAccidentSet", typeAccident);
        if (flow != null)
            query.setParameter("flowSet", flow);
        if (status != null)
            query.setParameter("statusSet", status);
        if (clientIdList != null)
            query.setParameter("clientIdList", clientIdList);

        return query.getResultList();

    }


    public BigInteger countClaimsForPaginationTheftOperator(Boolean inEvidence, Long practiceId, List<String> flow, String clientId, String plate, List<String> status, String locator, String dateAccidentFrom, String dateAccidentTo, List<String> typeAccident, Boolean asc, String orderBy, Integer page, Integer pageSize, Boolean withContinuation, String company, String clientName, Boolean poVariation, Boolean waitingForNumberSx, Long fleetVehicleId, Boolean waitingForAuthority) {
        StringBuffer sql = new StringBuffer("SELECT COUNT(*)  ");
        StringBuffer fromCondition = new StringBuffer(" FROM claims_new claims");
        StringBuffer whereCondition = new StringBuffer(" WHERE 1=1 ");
        if (dateAccidentFrom != null && dateAccidentTo != null) {

            if (dateAccidentTo.compareTo(dateAccidentFrom) < 0) {
                LOGGER.debug("the date_accident_to is less than the date_accident_from");
                throw new BadParametersException("the date_accident_to is less than the date_accident_from", MessageCode.CLAIMS_1011);
            } else {
                whereCondition.append(" AND (claims.date_accident\\:\\:timestamp BETWEEN '" + dateAccidentFrom + "'\\:\\:timestamp AND '" + dateAccidentTo + "'\\:\\:timestamp )");
            }
        } else if ((dateAccidentFrom != null) || (dateAccidentTo != null)) {
            LOGGER.debug("the date_accident_to and date_accident_from must be present together ");
            throw new BadParametersException("the date_accident_to and date_accident_from must be present together ", MessageCode.CLAIMS_1011);
        }

        if((clientId != null && !clientId.isEmpty()) || (clientName != null && !clientName.isEmpty()) ) {
            fromCondition.append(" inner join claims_damaged_customer cu on claims.id = cu.id ");
            if (clientId != null && !clientId.isEmpty()) {
                whereCondition.append(" AND cu.customer_id = '"+clientId+"' ");
            }

            if (Util.isNotEmpty(clientName)) {
                whereCondition.append(" AND cu.legal_name ILIKE '%" + clientName + "%'  ");
            }
        }

        if (poVariation != null) {
            if(poVariation) {
                whereCondition.append(" AND claims.po_variation =" + poVariation + " ");
            }else{
                whereCondition.append(" AND ( claims.po_variation IS NULL OR claims.po_variation =" + poVariation + ") ");
            }
        }

        if (Util.isNotEmpty(plate)) {
            whereCondition.append(" AND claims.plate ILIKE '%" + plate.trim() + "%' ");
        }

        if (Util.isNotEmpty(locator)) {
            whereCondition.append(" AND claims.claim_locator ILIKE '%" + locator + "%' ");
        }

        if (fleetVehicleId != null) {
            fromCondition.append(" inner join claims_damaged_vehicle ve on claims.id = ve.id ");
            whereCondition.append(" AND ve.fleet_vehicle_id = '" + fleetVehicleId + "'  ");
        }

        if (practiceId != null) {
            whereCondition.append(" AND claims.practice_id = " + practiceId + " ");
        }

        if (inEvidence != null) {
            if(inEvidence) {
                whereCondition.append(" AND claims.in_evidence = " + inEvidence + " ");
            }else{
                whereCondition.append(" AND (claims.in_evidence IS NULL OR claims.in_evidence = " + inEvidence + ") ");
            }
        }

        if (withContinuation != null) {
            whereCondition = this.addWithContinuationFilter(withContinuation,whereCondition);
        }

        if (typeAccident != null) {
            whereCondition.append(" AND claims.type_accident IN IN (:typeAccidentSet) ");
        } else {
            whereCondition.append(" AND claims.type_accident  = 'FURTO_TOTALE' OR claims.type_accident  = 'FURTO_PARZIALE' OR claims.type_accident  = 'FURTO_E_RITROVAMENTO' )");
        }

        if (Util.isNotEmpty(company)) {
            fromCondition.append(" inner join claims_damaged_insurance_info ins on claims.id = ins.id ");
            whereCondition.append(" AND ins.tpl_company ILIKE '%" + company + "%' ");
        }

        if (status != null) {
            whereCondition.append(" AND claims.status IN (:statusSet) AND claims.status <> 'DRAFT'");
        } else {
            whereCondition.append(" AND claims.status <> 'DRAFT'");
        }

        if (flow != null) {
            whereCondition.append(" AND claims.type IN (:flowSet) ");
        }

        if (waitingForNumberSx != null) {
            fromCondition.append(" left join claims_from_company fc on claims.id = fc.id ");
            if (waitingForNumberSx) {
                whereCondition.append(" AND (fc.number_sx IS NULL OR fc.number_sx = '') ");
            } else{
                whereCondition.append(" AND (fc.number_sx IS NOT NULL AND fc.number_sx != '') ");
            }
        }

        if(waitingForAuthority != null){
            fromCondition.append(" left join claims_to_claims_authority ca on claims.id = ca.claims_id left join claims_authority cat on ca.authority_id = cat.id ");
            if(waitingForAuthority) {
                whereCondition.append(" AND (cat.is_invoiced is null or cat.is_invoiced <> 'true') AND (cat.event_type is null or cat.event_type <> 'REJECT') ");
            }else
                whereCondition.append(" AND claims.status = 'WAITING_FOR_AUTHORITY' AND cat.is_invoiced = true ");
        }

        sql.append(fromCondition);
        sql.append(whereCondition);

        Query query = em.createNativeQuery(sql.toString());

        if (typeAccident != null)
            query.setParameter("typeAccidentSet", typeAccident);
        if (flow != null)
            query.setParameter("flowSet", flow);
        if (status != null)
            query.setParameter("statusSet", status);

        return (BigInteger) query.getSingleResult();

    }


    public List<ClaimsNewEntity> findClaimsForPaginationTheftOperator(Boolean inEvidence, Long practiceId, List<String> flow, String clientId, String plate, List<String> status, String locator, String dateAccidentFrom, String dateAccidentTo, List<String> typeAccident, Boolean asc, String orderBy, Integer page, Integer pageSize, Boolean withContinuation, String company, String clientName, Boolean poVariation, Boolean waitingForNumberSx, Long fleetVehicleId, Boolean waitingForAuthority) {
        Map<String, String> mappOrder = new HashMap<>();
        mappOrder.put("type", " claims.type ");
        mappOrder.put("practice_id", " claims.practice_id ");
        mappOrder.put("date_accident", " claims.date_accident ");
        mappOrder.put("plate", " claims.plate ");
        mappOrder.put("created_at", " claims.created_at ");
        mappOrder.put("type_accident", " claims.type_accident ");
        mappOrder.put("locator", " claims.locator ");

        StringBuffer sql = new StringBuffer("SELECT *  ");
        StringBuffer fromCondition = new StringBuffer(" FROM claims_new claims");
        StringBuffer whereCondition = new StringBuffer(" WHERE 1=1 ");

        if (dateAccidentFrom != null && dateAccidentTo != null) {

            if (dateAccidentTo.compareTo(dateAccidentFrom) < 0) {
                LOGGER.debug("the date_accident_to is less than the date_accident_from");
                throw new BadParametersException("the date_accident_to is less than the date_accident_from", MessageCode.CLAIMS_1011);
            } else {
                whereCondition.append(" AND (claims.date_accident\\:\\:timestamp BETWEEN '" + dateAccidentFrom + "'\\:\\:timestamp AND '" + dateAccidentTo + "'\\:\\:timestamp )");
            }
        } else if ((dateAccidentFrom != null) || (dateAccidentTo != null)) {
            LOGGER.debug("the date_accident_to and date_accident_from must be present together ");
            throw new BadParametersException("the date_accident_to and date_accident_from must be present together ", MessageCode.CLAIMS_1011);
        }

        if((clientId != null && !clientId.isEmpty()) || (clientName != null && !clientName.isEmpty()) ) {
            fromCondition.append(" inner join claims_damaged_customer cu on claims.id = cu.id ");
            if (clientId != null && !clientId.isEmpty()) {
                whereCondition.append(" AND cu.customer_id = '"+clientId+"' ");
            }

            if (Util.isNotEmpty(clientName)) {
                whereCondition.append(" AND cu.legal_name ILIKE '%" + clientName + "%'  ");
            }
        }

        if (poVariation != null) {
            if(poVariation) {
                whereCondition.append(" AND claims.po_variation =" + poVariation + " ");
            }else{
                whereCondition.append(" AND ( claims.po_variation IS NULL OR claims.po_variation =" + poVariation + ") ");
            }
        }

        if (Util.isNotEmpty(plate)) {
            whereCondition.append(" AND claims.plate ILIKE '%" + plate.trim() + "%' ");
        }

        if (Util.isNotEmpty(locator)) {
            whereCondition.append(" AND claims.claim_locator ILIKE '%" + locator + "%' ");
        }

        if (fleetVehicleId != null) {
            fromCondition.append(" inner join claims_damaged_vehicle ve on claims.id = ve.id ");
            whereCondition.append(" AND ve.fleet_vehicle_id = '" + fleetVehicleId + "'  ");
        }

        if (practiceId != null) {
            whereCondition.append(" AND claims.practice_id = " + practiceId + " ");
        }

        if (inEvidence != null) {
            if(inEvidence) {
                whereCondition.append(" AND claims.in_evidence = " + inEvidence + " ");
            }else{
                whereCondition.append(" AND (claims.in_evidence IS NULL OR claims.in_evidence = " + inEvidence + ") ");
            }
        }

        if (withContinuation != null) {
            whereCondition = this.addWithContinuationFilter(withContinuation,whereCondition);
        }

        if (typeAccident != null) {
            whereCondition.append(" AND claims.type_accident IN IN (:typeAccidentSet) ");
        } else {
            whereCondition.append(" AND claims.type_accident = 'FURTO_TOTALE' OR claims.type_accident  = 'FURTO_PARZIALE' OR claims.type_accident  = 'FURTO_E_RITROVAMENTO' )");
        }

        if (Util.isNotEmpty(company)) {
            fromCondition.append(" inner join claims_damaged_insurance_info ins on claims.id = ins.id ");
            whereCondition.append(" AND ins.tpl_company ILIKE '%" + company + "%' ");
        }

        if (status != null) {
            whereCondition.append(" AND claims.status IN (:statusSet) AND claims.status <> 'DRAFT'");
        } else {
            whereCondition.append(" AND claims.status <> 'DRAFT'");
        }

        if (flow != null) {
            whereCondition.append(" AND claims.type IN (:flowSet) ");
        }

        if (waitingForNumberSx != null) {
            fromCondition.append(" left join claims_from_company fc on claims.id = fc.id ");
            if (waitingForNumberSx) {
                whereCondition.append(" AND (fc.number_sx IS NULL OR fc.number_sx = '') ");
            } else{
                whereCondition.append(" AND (fc.number_sx IS NOT NULL AND fc.number_sx != '') ");
            }
        }

        if(waitingForAuthority != null){
            fromCondition.append(" left join claims_to_claims_authority ca on claims.id = ca.claims_id left join claims_authority cat on ca.authority_id = cat.id ");
            if(waitingForAuthority) {
                whereCondition.append(" AND (cat.is_invoiced is null or cat.is_invoiced <> 'true') AND (cat.event_type is null or cat.event_type <> 'REJECT') ");
            }else
                whereCondition.append(" AND claims.status = 'WAITING_FOR_AUTHORITY' AND cat.is_invoiced = true ");
        }

        sql.append(fromCondition);
        sql.append(whereCondition);


        String fieldToOrder = null;
        if (orderBy != null)
            fieldToOrder = mappOrder.get(orderBy);
        if (fieldToOrder != null && asc != null) {
            if (asc) {
                sql.append(" ORDER BY " + fieldToOrder + " ASC");
            } else {
                sql.append(" ORDER BY " + fieldToOrder + " DESC");
            }

        } else if (fieldToOrder != null) {
            sql.append(" ORDER BY " + fieldToOrder + " DESC");
        }

        sql.append(" LIMIT " + pageSize + " OFFSET " + (page - 1) * pageSize);

        Query query = em.createNativeQuery(sql.toString(), ClaimsNewEntity.class);


        if (typeAccident != null)
            query.setParameter("typeAccidentSet", typeAccident);
        if (flow != null)
            query.setParameter("flowSet", flow);
        if (status != null)
            query.setParameter("statusSet", status);

        return query.getResultList();

    }




    //END paginazione

    //INIZIO INTEGRAZIONE AUTHORITY

    public List<ClaimsNewEntity> getClaimsAttachedToAuthorityPractice(String authorityDossierId, String authorityWorkingId, List<String> claimsList) {

        String claimsIdString = "";

        for (String currentId : claimsList) {
            claimsIdString += "'" + currentId + "',";
        }

        if (!claimsIdString.equals("")) {
            claimsIdString = claimsIdString.substring(0, claimsIdString.length() - 1);
        }
        StringBuilder query = new StringBuilder("SELECT * " +
                "FROM claims_new c inner join claims_to_claims_authority cta on c.id = cta.claims_id inner join claims_authority ca on cta.authority_id = ca.id " +
                "WHERE ca.authority_dossier_id = '" + authorityDossierId + "' AND ca.authority_working_id = '" + authorityWorkingId + "' AND c.id IN (" + claimsIdString + ")");

        Query sql = em.createNativeQuery(query.toString(), ClaimsNewEntity.class);

        return sql.getResultList();
    }



    public List<ClaimsNewEntity> getClaimsToAttachAuthority(String authorityDossierId, String authorityWorkingId, List<String> claimsIdList) {
        String claimsIdString = "";
        for (String currentId : claimsIdList) {
            claimsIdString += "'" + currentId + "',";
        }

        if (!claimsIdString.equals("")) {
            claimsIdString = claimsIdString.substring(0, claimsIdString.length() - 1);
        }

        StringBuilder query = new StringBuilder("SELECT * FROM claims_new c WHERE c.id IN (SELECT DISTINCT c.id " +
                "FROM claims_new c left join claims_to_claims_authority cta on c.id = cta.claims_id left join claims_authority ca on cta.authority_id = ca.id " +
                "WHERE ((ca.authority_dossier_id <> '" + authorityDossierId + "' AND ca.authority_working_id <> '" + authorityWorkingId + "') OR (ca.authority_dossier_id = '" + authorityDossierId + "' AND ca.authority_working_id <> '" + authorityWorkingId + "') OR (ca.authority_dossier_id IS NULL AND ca.authority_working_id IS NULL ))) AND c.id  IN  (" + claimsIdString + ")" +
                " AND c.id NOT IN (SELECT DISTINCT(c.id) FROM claims_new c inner join claims_to_claims_authority cta on c.id = cta.claims_id inner join claims_authority ca on cta.authority_id = ca.id WHERE ca.authority_dossier_id = '" + authorityDossierId + "' AND ca.authority_working_id = '" + authorityWorkingId + "' AND c.id IN  (" + claimsIdString + "))");

        Query sql = em.createNativeQuery(query.toString(), ClaimsNewEntity.class);
        return sql.getResultList();
    }


    public List<ClaimsNewEntity> getClaimsToDetattachAuthority(String authorityDossierId, String authorityWorkingId, List<String> claimsIdList) {
        String claimsIdString = "";


        for (String currentId : claimsIdList) {
            claimsIdString += "'" + currentId + "',";
        }
        StringBuilder query = new StringBuilder("");
        if (!claimsIdString.equals("")) {
            claimsIdString = claimsIdString.substring(0, claimsIdString.length() - 1);
            query.append("SELECT * " +
                    "FROM claims_new c inner join claims_to_claims_authority cta on c.id = cta.claims_id inner join claims_authority ca on cta.authority_id = ca.id " +
                    "WHERE ca.authority_dossier_id = '" + authorityDossierId + "' AND ca.authority_working_id = '" + authorityWorkingId + "' AND c.id NOT IN  (" + claimsIdString + ")");
        } else {

            query.append("SELECT * " +
                    "FROM claims_new c inner join claims_to_claims_authority cta on c.id = cta.claims_id inner join claims_authority ca on cta.authority_id = ca.id " +
                    "WHERE ca.authority_dossier_id = '" + authorityDossierId + "' AND ca.authority_working_id = '" + authorityWorkingId + "'");
        }

        Query sql = em.createNativeQuery(query.toString(), ClaimsNewEntity.class);

        return sql.getResultList();
    }


    public List<ClaimsNewEntity> getClaimsToCheckDuplicateAuthority(String authorityDossierId, String authorityWorkingId) {

        StringBuilder query = new StringBuilder("SELECT * " +
                "FROM claims_new c inner join claims_to_claims_authority cta getClaimsToDetattachAuthorityon c.id = cta.claims_id inner join claims_authority ca on cta.authority_id = ca.id " +
                "WHERE ca.authority_dossier_id = '" + authorityDossierId + "' AND ca.authority_working_id = '" + authorityWorkingId + "'");

        Query sql = em.createNativeQuery(query.toString(), ClaimsNewEntity.class);


        return sql.getResultList();
    }


    public BigInteger countClaimsToCheckDuplicateAuthority(String authorityDossierId, String authorityWorkingId) {

        StringBuilder query = new StringBuilder("SELECT COUNT(*) " +
                "FROM claims_new c inner join claims_to_claims_authority cta on c.id = cta.claims_id inner join claims_authority ca on cta.authority_id = ca.id " +
                "WHERE ca.authority_dossier_id = '" + authorityDossierId + "' AND ca.authority_working_id = '" + authorityWorkingId + "'");

        Query sql = em.createNativeQuery(query.toString());

        return (BigInteger) sql.getSingleResult();
    }


    public List<ClaimsNewEntity> findClaimsForPaginationAuthority(List<String> plates, String chassis, Integer page, Integer pageSize, Boolean isFirstCheck) {
        List<ClaimsNewEntity> resultList = null;
        StringBuilder sql = new StringBuilder("SELECT DISTINCT c.* FROM claims_new c left join claims_damaged_vehicle cv on c.id = cv.id ");
        StringBuilder whereCondition = new StringBuilder(" WHERE 1=1 ");

        if (!plates.isEmpty()) {
            if (plates.size()==1) {
                whereCondition.append(" AND cv.license_plate  = '" + plates.get(0) + "' ");
            }else {
                String queryInOfplates= StringUtils.EMPTY;
                for(String plate: plates) {
                    if (queryInOfplates.isEmpty())
                        queryInOfplates = "'"+plate+"'";
                    else
                        queryInOfplates= queryInOfplates + "," +  "'"+plate+"'";
                }

                whereCondition.append(" AND cv.license_plate  in (" + queryInOfplates + ")");
            }
        }

        if (Util.isNotEmpty(chassis) && whereCondition.indexOf("AND") > 1) {
            whereCondition.append(" OR cv.chassis_number = '" + chassis + "' ");
        } else if (Util.isNotEmpty(chassis)) {
            whereCondition.append(" AND cv.chassis_number = '" + chassis + "' ");
        }

        if(!isFirstCheck){
            whereCondition
                    .append(" AND c.id NOT IN (SELECT ctca.claims_id FROM claims_to_claims_authority ctca " +
                            "LEFT JOIN claims_authority ca ON ctca.authority_id = ca.id WHERE ca.authority_working_id IS NULL OR ca.authority_working_id = '' ) ")
                    .append(" AND c.practice_id NOT IN (SELECT practice_id FROM claims_pending)");
        }

        sql.append(whereCondition);

        sql.append(" LIMIT " + pageSize + " OFFSET " + (page - 1) * pageSize);
        Query query = em.createNativeQuery(sql.toString(), ClaimsNewEntity.class);

        try {
            resultList = query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultList;
    }


    public BigInteger countClaimsForPaginationAuthority(List<String> plates, String chassis, Boolean isFirstCheck) {
        StringBuilder sql = new StringBuilder("SELECT COUNT(distinct c.id) FROM claims_new c left join claims_damaged_vehicle cv on c.id = cv.id ");
        StringBuilder whereCondition = new StringBuilder("WHERE 1=1 ");

        if (!plates.isEmpty()) {
            if (plates.size()==1) {
                whereCondition.append(" AND cv.license_plate  = '" + plates.get(0) + "' ");
            }else {
                String queryInOfplates= StringUtils.EMPTY;
                for(String plate: plates) {
                    if (queryInOfplates.isEmpty())
                        queryInOfplates = "'"+plate+"'";
                    else
                        queryInOfplates= queryInOfplates + "," +  "'"+plate+"'";
                }

                whereCondition.append(" AND cv.license_plate  in (" + queryInOfplates + ")");
            }
        }

        if (Util.isNotEmpty(chassis) && whereCondition.indexOf("AND") > 1) {
            whereCondition.append(" OR cv.chassis_number = '" + chassis + "' ");
        } else if (Util.isNotEmpty(chassis)) {
            whereCondition.append(" AND cv.chassis_number = '" + chassis + "' ");
        }

        if(!isFirstCheck){
            whereCondition
                    .append(" AND c.id NOT IN (SELECT ctca.claims_id FROM claims_to_claims_authority ctca " +
                            "LEFT JOIN claims_authority ca ON ctca.authority_id = ca.id WHERE ca.authority_working_id IS NULL OR ca.authority_working_id = '' ) ")
                    .append(" AND cv.license_plate NOT IN (SELECT damaged_drivers_plate FROM claims_pending)");
        }

        sql.append(whereCondition);

        Query query = em.createNativeQuery(sql.toString());

        return (BigInteger) query.getSingleResult();

    }

    //FINE INTEGRAZIONE AUTHORITY


    //INIZIO INTEGRAZIONE INVOICING

    //query per estrarre tutte le pratiche authority di cui richiedere i po

    public List<ClaimsNewEntity> getClaimsToInvoce() {

        StringBuilder sqlQuery = new StringBuilder("SELECT * " +
                "FROM claims_new c inner join claims_to_claims_authority cta on c.id = cta.claims_id inner join claims_authority ca on cta.authority_id = ca.id " +
                "WHERE ca.is_invoiced IS NULL OR ca.is_invoiced = false  and authority_working_id is not null and ca.status = 'AUTHORIZED' and event_type = 'END_WORKING'");

        Query query = em.createNativeQuery(sqlQuery.toString(), ClaimsNewEntity.class);

        return query.getResultList();

    }


    //FINE INTEGRAZIONE INVOICING

    //QUERY AFFIDO AUTOMATICO

    public List<ClaimsNewEntity> getClaimsToAutomaticEntrust(Boolean skipMigratedClaimsAutomaticEntrust) {
        StringBuffer sql =
                new StringBuffer()
                        .append("SELECT * FROM claims_new c  ")
                        .append("LEFT JOIN claims_entrusted ce ")
                        .append("ON c.id = ce.id ")
                        .append("WHERE c.status = 'TO_ENTRUST' ")
                        .append("AND ")
                        .append("( ce.auto_entrust IS NULL OR ce.auto_entrust = false) ");
        sql
                .append(" AND type_accident not in (")
                .append(" 'CARD_PASSIVA_FIRMA_SINGOLA','CARD_PASSIVA_DOPPIA_FIRMA','RC_PASSIVA' ")
                .append(")");

        if(skipMigratedClaimsAutomaticEntrust){
            sql
                    .append(" AND ")
                    .append("c.practice_id >= 4000000 ");
        }

        Query query = em.createNativeQuery(sql.toString(), ClaimsNewEntity.class);
        return query.getResultList();
    }





    //START INTEGRAZIONE MSA


    //funzione centralizzata per la get e per il conteggio per acclaims
    private List<Predicate> getResultFilterGetAcclaimsEntities(CriteriaBuilder criteriaBuilder, Root claimsRoot, Join joinEntrusted, Boolean inEvidence, Long practiceId, String flow, String clientId, String plate,
                                                               String status, String locator, String dateAccidentFrom,
                                                               String dateAccidentTo,  Boolean withContinuation, String company, Boolean withReceptions, String clientName){

        List<DataAccidentTypeAccidentEnum> typeAccidentList = new ArrayList<DataAccidentTypeAccidentEnum>() {{
            add(DataAccidentTypeAccidentEnum.RC_ATTIVA);
            add(DataAccidentTypeAccidentEnum.RC_PASSIVA);
            add(DataAccidentTypeAccidentEnum.RC_CONCORSUALE);
            add(DataAccidentTypeAccidentEnum.RC_NON_VERIFICABILE);
        }};



        List<Predicate> predicates = new ArrayList<>();
        predicates.add(criteriaBuilder.or(criteriaBuilder.isNull(claimsRoot.get(ClaimsNewEntity_.isReadAcclaims)), criteriaBuilder.isFalse(claimsRoot.get(ClaimsNewEntity_.isReadAcclaims))));
        predicates.add(criteriaBuilder.equal(joinEntrusted.get(ClaimsEntrustedEntity_.entrustedType),EntrustedEnum.LEGAL));
        predicates.add(criteriaBuilder.or( criteriaBuilder.equal(joinEntrusted.get(ClaimsEntrustedEntity_.idEntrustedTo), UUID.fromString(acclaimsId)), criteriaBuilder.equal(joinEntrusted.get(ClaimsEntrustedEntity_.idEntrustedTo),UUID.fromString(acclaimsSrId))));

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = simpleDateFormat.parse(entrustedAtStart);
            predicates.add(criteriaBuilder.greaterThan(joinEntrusted.get(ClaimsEntrustedEntity_.entrustedDay),date.toInstant()));
        } catch (ParseException e) {
            e.printStackTrace();
        }


        if (dateAccidentFrom != null && dateAccidentTo != null) {
            if (dateAccidentTo.compareTo(dateAccidentFrom) < 0) {
                LOGGER.debug("the date_accident_to is less than the date_accident_from");
                throw new BadParametersException("the date_accident_to is less than the date_accident_from", MessageCode.CLAIMS_1011);
            } else {

                Instant instantDateAccidentFrom = DateUtil.convertIS08601StringToUTCInstant(dateAccidentFrom);
                Instant instantDateAccidentTo = DateUtil.convertIS08601StringToUTCInstant(dateAccidentTo);
                predicates.add(criteriaBuilder.between(
                        claimsRoot.get(ClaimsNewEntity_.dateAccident),instantDateAccidentFrom, instantDateAccidentTo));

            }
        } else if ((dateAccidentFrom != null) || (dateAccidentTo != null)) {
            LOGGER.debug("the date_accident_to and date_accident_from must be present together ");
            throw new BadParametersException("the date_accident_to and date_accident_from must be present together ", MessageCode.CLAIMS_1011);
        }
         /**** NE-1488 scarico da parte di Acclaims delle denunce affidate fino a che non stiamo a STEP 3 , saranno presi in considerazione solo i sx 199 ***/
        if (goLiveStrategyService.checkIfStepThree()) {
            if (Util.isNotEmpty(clientId) || Util.isNotEmpty(clientName)) {
                Join<ClaimsNewEntity, ClaimsDamagedCustomerEntity> claimsCustomerJoin = claimsRoot.join(ClaimsNewEntity_.claimsDamagedCustomerEntity, JoinType.INNER);
                if (Util.isNotEmpty(clientId)) {
                    predicates.add(criteriaBuilder.equal(claimsCustomerJoin.get(ClaimsDamagedCustomerEntity_.customerId), clientId));
                }

                if (Util.isNotEmpty(clientName)) {
                    predicates.add(criteriaBuilder.like(criteriaBuilder.upper(claimsCustomerJoin.get(ClaimsDamagedCustomerEntity_.legalName)), "%" + clientName.toUpperCase() + "%"));
                }
            }
        }else{
            /* scarico di Accliams solo per clienti 199 */
              Join<ClaimsNewEntity, ClaimsDamagedCustomerEntity> claimsCustomerJoin = claimsRoot.join(ClaimsNewEntity_.claimsDamagedCustomerEntity, JoinType.INNER);
              predicates.add(criteriaBuilder.equal(claimsCustomerJoin.get(ClaimsDamagedCustomerEntity_.customerId), "199"));
        }

        if (Util.isNotEmpty(plate)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(claimsRoot.get(ClaimsNewEntity_.plate)),  "%" + plate.toUpperCase() + "%"));
        }

        if (Util.isNotEmpty(locator)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(claimsRoot.get(ClaimsNewEntity_.claimLocator)),  "%" + locator.toUpperCase() + "%"));
        }

        if (practiceId != null) {
            predicates.add(criteriaBuilder.equal(claimsRoot.get(ClaimsNewEntity_.practiceId), practiceId  ));
        }

        if (inEvidence != null) {
            predicates.add(criteriaBuilder.equal(claimsRoot.get(ClaimsNewEntity_.inEvidence), inEvidence  ));
        }

        if (withContinuation != null) {
            predicates.add(criteriaBuilder.equal(claimsRoot.get(ClaimsNewEntity_.withContinuation), withContinuation  ));
        }

        if (withReceptions != null) {
            Join<ClaimsNewEntity, ClaimsTheftEntity> claimsTheftJoin =  claimsRoot.join(ClaimsNewEntity_.claimsTheftEntity, JoinType.INNER);
            predicates.add(criteriaBuilder.equal(claimsTheftJoin.get(ClaimsTheftEntity_.isWithReceptions), withReceptions  ));

        }

        if (typeAccidentList != null) {
            predicates.add(claimsRoot.get(ClaimsNewEntity_.typeAccident).in(typeAccidentList));
        }

        if (Util.isNotEmpty(company)) {
            Join<ClaimsNewEntity, ClaimsDamagedInsuranceInfoEntity> claimsInsuranceInfoJoin = claimsRoot.join(ClaimsNewEntity_.claimsDamagedInsuranceInfoEntity, JoinType.INNER);
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(claimsInsuranceInfoJoin.get(ClaimsDamagedInsuranceInfoEntity_.tplCompany)),  "%" + company + "%"));
        }

        if (status != null) {
            List<String> statusStringList = Arrays.asList(status.split("\\s*,\\s*"));
            List<ClaimsStatusEnum> statusTypeList = new LinkedList<>();
            for(String currentStatus: statusStringList) {
                statusTypeList.add(ClaimsStatusEnum.create(currentStatus));
            }
            predicates.add(claimsRoot.get(ClaimsNewEntity_.status).in(statusTypeList));
        }

        if (Util.isNotEmpty(flow)) {
            List<String> flowStringList = Arrays.asList(flow.split("\\s*,\\s*"));
            List<ClaimsFlowEnum> flowTypeString = new LinkedList<>();
            for(String currentFlow: flowStringList) {
                flowTypeString.add(ClaimsFlowEnum.create(currentFlow));
            }
            predicates.add(claimsRoot.get(ClaimsNewEntity_.type).in(flowTypeString));
        }



        return predicates;

    }


    public BigInteger countClaimsForAcclaimsPagination(Boolean inEvidence, Long practiceId, String flow, String clientId, String plate, String status, String locator, String dateAccidentFrom, String dateAccidentTo, List<String> typeAccident, Boolean asc, String orderBy, Integer page, Integer pageSize, Boolean withContinuation, String company, Boolean withReceptions, String clientName) {

        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Long> claimsNewEntityCriteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<ClaimsNewEntity> claimsNewEntityRoot = claimsNewEntityCriteriaQuery.from(ClaimsNewEntity.class);
        claimsNewEntityCriteriaQuery.select(criteriaBuilder.count(claimsNewEntityRoot));

        Join<ClaimsNewEntity, ClaimsEntrustedEntity> claimsEntrustedJoin = claimsNewEntityRoot.join(ClaimsNewEntity_.claimsEntrustedEntity,JoinType.INNER);


        List<Predicate> predicateList =this.getResultFilterGetAcclaimsEntities( criteriaBuilder, claimsNewEntityRoot, claimsEntrustedJoin, inEvidence, practiceId, flow, clientId, plate, status, locator, dateAccidentFrom, dateAccidentTo,withContinuation,company,withReceptions,clientName);
        if(predicateList != null && !predicateList.isEmpty()){
            claimsNewEntityCriteriaQuery.where(predicateList.toArray(new Predicate[predicateList.size()]));
        }



        Long longResult = em.createQuery(claimsNewEntityCriteriaQuery).getSingleResult();
        BigInteger bigIntegerResult = null;
        if(longResult != null) {
            bigIntegerResult =BigInteger.valueOf(longResult.longValue());
        }else{
            bigIntegerResult =BigInteger.valueOf(0);
        }

        return bigIntegerResult ;

    }


    public List<ClaimsNewEntity> getAcclaimsClaimsEntity(Boolean inEvidence, Long practiceId, String flow, String clientId, String plate,
                                                         String status, String locator, String dateAccidentFrom,
                                                         String dateAccidentTo, String typeAccident, Boolean asc, String orderBy, int page, int pageSize, Boolean withContinuation, String company, Boolean withReceptions, String clientName) {

        Map<String,Object> mappOrder = new HashMap<>();
        mappOrder.put("type", ClaimsNewEntity_.type);
        mappOrder.put("practice_id", ClaimsNewEntity_.practiceId);
        mappOrder.put("date_accident", ClaimsNewEntity_.dateAccident);
        mappOrder.put("plate", ClaimsNewEntity_.plate);
        mappOrder.put("created_at", ClaimsNewEntity_.createdAt);
        mappOrder.put("type_accident", ClaimsNewEntity_.typeAccident);
        mappOrder.put("locator", ClaimsNewEntity_.claimLocator);


        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<ClaimsNewEntity> claimsNewEntityCriteriaQuery = criteriaBuilder.createQuery(ClaimsNewEntity.class);
        Root<ClaimsNewEntity> claimsNewEntityRoot = claimsNewEntityCriteriaQuery.from(ClaimsNewEntity.class);



        Join<ClaimsNewEntity, ClaimsEntrustedEntity> claimsEntrustedJoin = claimsNewEntityRoot.join(ClaimsNewEntity_.claimsEntrustedEntity,JoinType.INNER);
        claimsNewEntityCriteriaQuery.select(claimsNewEntityRoot);

        List<Predicate> predicateList =this.getResultFilterGetAcclaimsEntities( criteriaBuilder, claimsNewEntityRoot, claimsEntrustedJoin, inEvidence, practiceId, flow, clientId, plate, status, locator, dateAccidentFrom, dateAccidentTo,withContinuation,company,withReceptions,clientName);
        if(predicateList != null && !predicateList.isEmpty()){
            claimsNewEntityCriteriaQuery.where(predicateList.toArray(new Predicate[predicateList.size()]));
        }

        SingularAttribute fieldToOrder = null;
        if (orderBy != null) {
            fieldToOrder = (SingularAttribute)mappOrder.get(orderBy);
        }

        if (fieldToOrder != null && asc != null) {
            if (asc) {
                claimsNewEntityCriteriaQuery.orderBy(criteriaBuilder.asc(claimsNewEntityRoot.get(fieldToOrder)));
            } else {
                claimsNewEntityCriteriaQuery.orderBy(criteriaBuilder.desc(claimsNewEntityRoot.get(fieldToOrder)));
            }

        } else if (fieldToOrder != null) {
            claimsNewEntityCriteriaQuery.orderBy(criteriaBuilder.desc(claimsNewEntityRoot.get(fieldToOrder)));
        }



        return em.createQuery(claimsNewEntityCriteriaQuery)
                .setFirstResult((page - 1) * pageSize)
                .setMaxResults(pageSize)
                .getResultList();


    }

    private List<Predicate> getResultFilterGetMsaEntities(CriteriaBuilder criteriaBuilder, Root claimsRoot, Join joinEntrusted, Join claimsInsuranceInfoJoin, Boolean inEvidence, Long practiceId, String flow, String clientId, String plate,
                                                          String status, String locator, String dateAccidentFrom,
                                                          String dateAccidentTo,  Boolean withContinuation, String company, Boolean withReceptions, String clientName){

        List<DataAccidentTypeAccidentEnum> typeAccidentList = new ArrayList<DataAccidentTypeAccidentEnum>() {{
            add(DataAccidentTypeAccidentEnum.RC_ATTIVA);
            add(DataAccidentTypeAccidentEnum.RC_PASSIVA);
            add(DataAccidentTypeAccidentEnum.RC_CONCORSUALE);
            add(DataAccidentTypeAccidentEnum.RC_NON_VERIFICABILE);
        }};



        List<Predicate> predicates = new ArrayList<>();
        predicates.add(criteriaBuilder.greaterThan(claimsRoot.get(ClaimsNewEntity_.practiceId),4999999));
        predicates.add(criteriaBuilder.or(criteriaBuilder.isNull(claimsRoot.get(ClaimsNewEntity_.isRead)), criteriaBuilder.isFalse(claimsRoot.get(ClaimsNewEntity_.isRead))));

        predicates.add(criteriaBuilder.or(
                criteriaBuilder.and(
                        criteriaBuilder.equal(joinEntrusted.get(ClaimsEntrustedEntity_.entrustedType),EntrustedEnum.LEGAL),
                        criteriaBuilder.equal(joinEntrusted.get(ClaimsEntrustedEntity_.idEntrustedTo), UUID.fromString("28445608-4a5f-11e9-8646-d663bd873d93"))
                ),
                criteriaBuilder.equal(claimsInsuranceInfoJoin.get(ClaimsDamagedInsuranceInfoEntity_.tplInsuranceCompanyId), UUID.fromString("a83be7e2-4a58-11e9-8646-d663bd873d93"))
                )
        );

        predicates.add(criteriaBuilder.notEqual(claimsRoot.get(ClaimsNewEntity_.status), ClaimsStatusEnum.WAITING_FOR_VALIDATION));


        if (dateAccidentFrom != null && dateAccidentTo != null) {
            if (dateAccidentTo.compareTo(dateAccidentFrom) < 0) {
                LOGGER.debug("the date_accident_to is less than the date_accident_from");
                throw new BadParametersException("the date_accident_to is less than the date_accident_from", MessageCode.CLAIMS_1011);
            } else {

                Instant instantDateAccidentFrom = DateUtil.convertIS08601StringToUTCInstant(dateAccidentFrom);
                Instant instantDateAccidentTo = DateUtil.convertIS08601StringToUTCInstant(dateAccidentTo);
                predicates.add(criteriaBuilder.between(
                        claimsRoot.get(ClaimsNewEntity_.dateAccident),instantDateAccidentFrom, instantDateAccidentTo));

            }
        } else if ((dateAccidentFrom != null) || (dateAccidentTo != null)) {
            LOGGER.debug("the date_accident_to and date_accident_from must be present together ");
            throw new BadParametersException("the date_accident_to and date_accident_from must be present together ", MessageCode.CLAIMS_1011);
        }

        if (Util.isNotEmpty(clientId) || Util.isNotEmpty(clientName) ) {
            Join<ClaimsNewEntity, ClaimsDamagedCustomerEntity> claimsCustomerJoin =  claimsRoot.join(ClaimsNewEntity_.claimsDamagedCustomerEntity, JoinType.INNER);
            if (Util.isNotEmpty(clientId)) {
                predicates.add(criteriaBuilder.equal(claimsCustomerJoin.get(ClaimsDamagedCustomerEntity_.customerId),clientId));
            }

            if (Util.isNotEmpty(clientName)) {
                predicates.add(criteriaBuilder.like(criteriaBuilder.upper(claimsCustomerJoin.get(ClaimsDamagedCustomerEntity_.legalName)),  "%" + clientName.toUpperCase() + "%"));
            }
        }

        if (Util.isNotEmpty(plate)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(claimsRoot.get(ClaimsNewEntity_.plate)),  "%" + plate.toUpperCase() + "%"));
        }

        if (Util.isNotEmpty(locator)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(claimsRoot.get(ClaimsNewEntity_.claimLocator)),  "%" + locator.toUpperCase() + "%"));
        }

        if (practiceId != null) {
            predicates.add(criteriaBuilder.equal(claimsRoot.get(ClaimsNewEntity_.practiceId), practiceId  ));
        }

        if (inEvidence != null) {
            predicates.add(criteriaBuilder.equal(claimsRoot.get(ClaimsNewEntity_.inEvidence), inEvidence  ));
        }

        if (withContinuation != null) {
            predicates.add(criteriaBuilder.equal(claimsRoot.get(ClaimsNewEntity_.withContinuation), withContinuation  ));
        }

        if (withReceptions != null) {
            Join<ClaimsNewEntity, ClaimsTheftEntity> claimsTheftJoin =  claimsRoot.join(ClaimsNewEntity_.claimsTheftEntity, JoinType.INNER);
            predicates.add(criteriaBuilder.equal(claimsTheftJoin.get(ClaimsTheftEntity_.isWithReceptions), withReceptions  ));

        }

        if (typeAccidentList != null) {
            predicates.add(claimsRoot.get(ClaimsNewEntity_.typeAccident).in(typeAccidentList));
        }

        if (Util.isNotEmpty(company)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(claimsInsuranceInfoJoin.get(ClaimsDamagedInsuranceInfoEntity_.tplCompany)),  "%" + company + "%"));
        }

        if (status != null) {
            List<String> statusStringList = Arrays.asList(status.split("\\s*,\\s*"));
            List<ClaimsStatusEnum> statusTypeList = new LinkedList<>();
            for(String currentStatus: statusStringList) {
                statusTypeList.add(ClaimsStatusEnum.create(currentStatus));
            }
            predicates.add(claimsRoot.get(ClaimsNewEntity_.status).in(statusTypeList));
        }

        if (Util.isNotEmpty(flow)) {
            List<String> flowStringList = Arrays.asList(flow.split("\\s*,\\s*"));
            List<ClaimsFlowEnum> flowTypeString = new LinkedList<>();
            for(String currentFlow: flowStringList) {
                flowTypeString.add(ClaimsFlowEnum.create(currentFlow));
            }
            predicates.add(claimsRoot.get(ClaimsNewEntity_.type).in(flowTypeString));
        }



        return predicates;

    }



    public List<ClaimsNewEntity> getMsaClaimsEntity(Boolean inEvidence, Long practiceId, String flow, String clientId, String plate,
                                                    String status, String locator, String dateAccidentFrom,
                                                    String dateAccidentTo, String typeAccident, Boolean asc, String orderBy, int page, int pageSize, Boolean withContinuation, String company, Boolean withReceptions, String clientName) {


        Map<String,Object> mappOrder = new HashMap<>();
        mappOrder.put("type", ClaimsNewEntity_.type);
        mappOrder.put("practice_id", ClaimsNewEntity_.practiceId);
        mappOrder.put("date_accident", ClaimsNewEntity_.dateAccident);
        mappOrder.put("plate", ClaimsNewEntity_.plate);
        mappOrder.put("created_at", ClaimsNewEntity_.createdAt);
        mappOrder.put("type_accident", ClaimsNewEntity_.typeAccident);
        mappOrder.put("locator", ClaimsNewEntity_.claimLocator);

        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<ClaimsNewEntity> claimsNewEntityCriteriaQuery = criteriaBuilder.createQuery(ClaimsNewEntity.class);
        Root<ClaimsNewEntity> claimsNewEntityRoot = claimsNewEntityCriteriaQuery.from(ClaimsNewEntity.class);
        Join<ClaimsNewEntity, ClaimsEntrustedEntity> claimsEntrustedJoin = claimsNewEntityRoot.join(ClaimsNewEntity_.claimsEntrustedEntity,JoinType.LEFT);
        Join<ClaimsNewEntity, ClaimsDamagedInsuranceInfoEntity> claimsInsuranceInfoJoin = claimsNewEntityRoot.join(ClaimsNewEntity_.claimsDamagedInsuranceInfoEntity,JoinType.INNER);
        claimsNewEntityCriteriaQuery.select(claimsNewEntityRoot);



        List<Predicate> predicateList =this.getResultFilterGetMsaEntities( criteriaBuilder, claimsNewEntityRoot, claimsEntrustedJoin,claimsInsuranceInfoJoin, inEvidence, practiceId, flow, clientId, plate, status, locator, dateAccidentFrom, dateAccidentTo,withContinuation,company,withReceptions,clientName);
        if(predicateList != null && !predicateList.isEmpty()){
            claimsNewEntityCriteriaQuery.where(predicateList.toArray(new Predicate[predicateList.size()]));
        }

        SingularAttribute fieldToOrder = null;
        if (orderBy != null) {
            fieldToOrder = (SingularAttribute)mappOrder.get(orderBy);
        }

        if (fieldToOrder != null && asc != null) {
            if (asc) {
                claimsNewEntityCriteriaQuery.orderBy(criteriaBuilder.asc(claimsNewEntityRoot.get(fieldToOrder)));
            } else {
                claimsNewEntityCriteriaQuery.orderBy(criteriaBuilder.desc(claimsNewEntityRoot.get(fieldToOrder)));
            }

        } else if (fieldToOrder != null) {
            claimsNewEntityCriteriaQuery.orderBy(criteriaBuilder.desc(claimsNewEntityRoot.get(fieldToOrder)));
        }



        return em.createQuery(claimsNewEntityCriteriaQuery)
                .setFirstResult((page - 1) * pageSize)
                .setMaxResults(pageSize)
                .getResultList();


    }



    public BigInteger countClaimsForMsaPagination(Boolean inEvidence, Long practiceId, String flow, String clientId, String plate, String status, String locator, String dateAccidentFrom, String dateAccidentTo, List<String> typeAccident, Boolean asc, String orderBy, Integer page, Integer pageSize, Boolean withContinuation, String company, Boolean withReceptions, String clientName) {


        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Long> claimsNewEntityCriteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<ClaimsNewEntity> claimsNewEntityRoot = claimsNewEntityCriteriaQuery.from(ClaimsNewEntity.class);
        claimsNewEntityCriteriaQuery.select(criteriaBuilder.count(claimsNewEntityRoot));

        Join<ClaimsNewEntity, ClaimsEntrustedEntity> claimsEntrustedJoin = claimsNewEntityRoot.join(ClaimsNewEntity_.claimsEntrustedEntity,JoinType.LEFT);
        Join<ClaimsNewEntity, ClaimsDamagedInsuranceInfoEntity> claimsInsuranceInfoJoin = claimsNewEntityRoot.join(ClaimsNewEntity_.claimsDamagedInsuranceInfoEntity,JoinType.INNER);


        List<Predicate> predicateList =this.getResultFilterGetMsaEntities( criteriaBuilder, claimsNewEntityRoot, claimsEntrustedJoin,claimsInsuranceInfoJoin, inEvidence, practiceId, flow, clientId, plate, status, locator, dateAccidentFrom, dateAccidentTo,withContinuation,company,withReceptions,clientName);
        if(predicateList != null && !predicateList.isEmpty()){
            claimsNewEntityCriteriaQuery.where(predicateList.toArray(new Predicate[predicateList.size()]));
        }



        Long longResult = em.createQuery(claimsNewEntityCriteriaQuery).getSingleResult();
        BigInteger bigIntegerResult = null;
        if(longResult != null) {
            bigIntegerResult =BigInteger.valueOf(longResult.longValue());
        }else{
            bigIntegerResult =BigInteger.valueOf(0);
        }

        return bigIntegerResult ;

    }

    //END INTEGRAZIONE MSA


    //INIZIO QUERY per controllo di duplicati


    //filtri per le query findClaimsForPaginationWithoutDraft
    private List<Predicate> getResultFilterPaginationWithoutDraft(CriteriaBuilder criteriaBuilder, Root claimsRoot,Boolean inEvidence, Long practiceId, List<String> flow, List<String> clientIdList, String plate, List<String> status, String locator, String dateAccidentFrom, String dateAccidentTo, List<String> typeAccident, Boolean withContinuation, String company, Boolean withReceptions, String clientName, Boolean poVariation, Long fleetVehicleId){

        List<Predicate> predicates = new ArrayList<>();
        predicates.add(criteriaBuilder.notEqual(claimsRoot.get(ClaimsNewEntity_.status), ClaimsStatusEnum.DRAFT));


        if (dateAccidentFrom != null && dateAccidentTo != null) {
            if (dateAccidentTo.compareTo(dateAccidentFrom) < 0) {
                LOGGER.debug("the date_accident_to is less than the date_accident_from");
                throw new BadParametersException("the date_accident_to is less than the date_accident_from", MessageCode.CLAIMS_1011);
            } else {

                Instant instantDateAccidentFrom = DateUtil.convertIS08601StringToUTCInstant(dateAccidentFrom);
                Instant instantDateAccidentTo = DateUtil.convertIS08601StringToUTCInstant(dateAccidentTo);
                predicates.add(criteriaBuilder.between(
                        claimsRoot.get(ClaimsNewEntity_.dateAccident),instantDateAccidentFrom, instantDateAccidentTo));

            }
        } else if ((dateAccidentFrom != null) || (dateAccidentTo != null)) {
            LOGGER.debug("the date_accident_to and date_accident_from must be present together ");
            throw new BadParametersException("the date_accident_to and date_accident_from must be present together ", MessageCode.CLAIMS_1011);
        }

        if ((clientIdList != null && !clientIdList.isEmpty()) || Util.isNotEmpty(clientName) ) {
            Join<ClaimsNewEntity, ClaimsDamagedCustomerEntity> claimsCustomerJoin =  claimsRoot.join(ClaimsNewEntity_.claimsDamagedCustomerEntity, JoinType.INNER);
            if (clientIdList != null && !clientIdList.isEmpty()) {
                List<Long> clientListLong = new LinkedList<>();
                for(String currentClient: clientIdList){
                    clientListLong.add(Long.parseLong(currentClient));
                }

                predicates.add(claimsCustomerJoin.get(ClaimsDamagedCustomerEntity_.customerId).in(clientListLong));
            }

            if (Util.isNotEmpty(clientName)) {
                predicates.add(criteriaBuilder.like(criteriaBuilder.upper(claimsCustomerJoin.get(ClaimsDamagedCustomerEntity_.legalName)),  "%" + clientName.toUpperCase() + "%"));
            }
        }

        if (Util.isNotEmpty(plate)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(claimsRoot.get(ClaimsNewEntity_.plate)),  "%" + plate.toUpperCase() + "%"));
        }

        if (Util.isNotEmpty(locator)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(claimsRoot.get(ClaimsNewEntity_.claimLocator)),  "%" + locator.toUpperCase() + "%"));
        }

        if (practiceId != null) {
            predicates.add(criteriaBuilder.equal(claimsRoot.get(ClaimsNewEntity_.practiceId), practiceId  ));
        }

        if (inEvidence != null) {
            predicates.add(criteriaBuilder.equal(claimsRoot.get(ClaimsNewEntity_.inEvidence), inEvidence  ));
        }

        if (withContinuation != null) {
            predicates.add(criteriaBuilder.equal(claimsRoot.get(ClaimsNewEntity_.withContinuation), withContinuation  ));
        }

        if (withReceptions != null) {
            Join<ClaimsNewEntity, ClaimsTheftEntity> claimsTheftJoin =  claimsRoot.join(ClaimsNewEntity_.claimsTheftEntity, JoinType.INNER);
            predicates.add(criteriaBuilder.equal(claimsTheftJoin.get(ClaimsTheftEntity_.isWithReceptions), withReceptions  ));

        }

        if (Util.isNotEmpty(company)) {
            Join<ClaimsNewEntity, ClaimsDamagedInsuranceInfoEntity> claimsInsuranceInfoJoin =  claimsRoot.join(ClaimsNewEntity_.claimsDamagedInsuranceInfoEntity, JoinType.INNER);
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(claimsInsuranceInfoJoin.get(ClaimsDamagedInsuranceInfoEntity_.tplCompany)),  "%" + company + "%"));
        }

        if (status != null && !status.isEmpty()) {
            List<ClaimsStatusEnum> claimsStatusEnums = new LinkedList<>();
            for(String currentStatus: status) {
                claimsStatusEnums.add(ClaimsStatusEnum.create(currentStatus));
            }
            predicates.add(claimsRoot.get(ClaimsNewEntity_.status).in(claimsStatusEnums));
        }



        if(typeAccident != null && !typeAccident.isEmpty()){
            List<DataAccidentTypeAccidentEnum> typeAccidentEnums = new LinkedList<>();
            for(String currentString: typeAccident){
                typeAccidentEnums.add(DataAccidentTypeAccidentEnum.create(currentString));
            }
            predicates.add(claimsRoot.get(ClaimsNewEntity_.typeAccident).in(typeAccidentEnums));
        }

        if(poVariation != null){
            predicates.add(criteriaBuilder.equal(claimsRoot.get(ClaimsNewEntity_.poVariation), poVariation));
        }

        if(fleetVehicleId != null){
            Join<ClaimsNewEntity, ClaimsDamagedVehicleEntity>  joinVehicle = claimsRoot.join(ClaimsNewEntity_.claimsDamagedVehicleEntity, JoinType.INNER);
            predicates.add(criteriaBuilder.equal(joinVehicle.get(ClaimsDamagedVehicleEntity_.fleetVehicleId),fleetVehicleId ));
        }


        if(flow != null && !flow.isEmpty()){
            List<ClaimsFlowEnum> claimsFlowEnums = new LinkedList<>();
            for(String currentFlow : flow){
                claimsFlowEnums.add(ClaimsFlowEnum.create(currentFlow));
            }
            predicates.add(claimsRoot.get(ClaimsNewEntity_.type).in(claimsFlowEnums));

        }

        return predicates;

    }



    public List<ClaimsNewEntity> findClaimsForPaginationWithoutDraft(Boolean inEvidence, Long practiceId, List<String> flow, List<String> clientIdList, String plate, List<String> status, String locator, String dateAccidentFrom, String dateAccidentTo, List<String> typeAccident, Boolean asc, String orderBy, Integer page, Integer pageSize, Boolean withContinuation, String company, Boolean withReceptions, String clientName, Boolean poVariation, Long fleetVehicleId) {


        Map<String,Object> mappOrder = new HashMap<>();
        mappOrder.put("type", ClaimsNewEntity_.type);
        mappOrder.put("practice_id", ClaimsNewEntity_.practiceId);
        mappOrder.put("date_accident", ClaimsNewEntity_.dateAccident);
        mappOrder.put("plate", ClaimsNewEntity_.plate);
        mappOrder.put("created_at", ClaimsNewEntity_.createdAt);
        mappOrder.put("type_accident", ClaimsNewEntity_.typeAccident);
        mappOrder.put("locator", ClaimsNewEntity_.claimLocator);

        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<ClaimsNewEntity> claimsNewEntityCriteriaQuery = criteriaBuilder.createQuery(ClaimsNewEntity.class);
        Root<ClaimsNewEntity> claimsNewEntityRoot = claimsNewEntityCriteriaQuery.from(ClaimsNewEntity.class);

        claimsNewEntityCriteriaQuery.select(claimsNewEntityRoot);



        List<Predicate> predicateList =this.getResultFilterPaginationWithoutDraft( criteriaBuilder, claimsNewEntityRoot, inEvidence, practiceId, flow, clientIdList, plate, status, locator, dateAccidentFrom, dateAccidentTo,typeAccident,withContinuation,company,withReceptions,clientName, poVariation,fleetVehicleId);
        if(predicateList != null && !predicateList.isEmpty()){
            claimsNewEntityCriteriaQuery.where(predicateList.toArray(new Predicate[predicateList.size()]));
        }

        SingularAttribute fieldToOrder = null;
        if (orderBy != null) {
            fieldToOrder = (SingularAttribute)mappOrder.get(orderBy);
        }

        if (fieldToOrder != null && asc != null) {
            if (asc) {
                claimsNewEntityCriteriaQuery.orderBy(criteriaBuilder.asc(claimsNewEntityRoot.get(fieldToOrder)));
            } else {
                claimsNewEntityCriteriaQuery.orderBy(criteriaBuilder.desc(claimsNewEntityRoot.get(fieldToOrder)));
            }

        } else if (fieldToOrder != null) {
            claimsNewEntityCriteriaQuery.orderBy(criteriaBuilder.desc(claimsNewEntityRoot.get(fieldToOrder)));
        }



        return em.createQuery(claimsNewEntityCriteriaQuery)
                .setFirstResult((page - 1) * pageSize)
                .setMaxResults(pageSize)
                .getResultList();


    }


    public List<ClaimsNewEntity> findClaimsByDamagedPlateAndCounterpartyPlateName(String damagedPlate, String counterpartyPlate, String counterpartyFirstName, String counterpartyLastName, String dateAccidentFrom, String dateAccidentTo) {

        List<ClaimsStatusEnum> claimsStatusEnums = new ArrayList<ClaimsStatusEnum>() {{
            add(ClaimsStatusEnum.DRAFT);
            add(ClaimsStatusEnum.CLOSED);
            add(ClaimsStatusEnum.CLOSED_PRACTICE);
        }};

        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<ClaimsNewEntity> claimsNewEntityCriteriaQuery = criteriaBuilder.createQuery(ClaimsNewEntity.class);
        Root<ClaimsNewEntity> claimsNewEntityRoot = claimsNewEntityCriteriaQuery.from(ClaimsNewEntity.class);
        Join<ClaimsNewEntity,CounterpartyNewEntity> joinCounterparty =claimsNewEntityRoot.join(ClaimsNewEntity_.counterparts,JoinType.LEFT);
        claimsNewEntityCriteriaQuery.select(claimsNewEntityRoot);


        List<Predicate> predicates = new LinkedList<>();
        if (claimsStatusEnums != null && !claimsStatusEnums.isEmpty()) {
            predicates.add(criteriaBuilder.not(claimsNewEntityRoot.get(ClaimsNewEntity_.status).in(claimsStatusEnums)));
        }

        if(damagedPlate!=null){
            Join<ClaimsNewEntity,ClaimsDamagedVehicleEntity> joinClaimsVehicle = claimsNewEntityRoot.join(ClaimsNewEntity_.claimsDamagedVehicleEntity, JoinType.INNER);
            predicates.add(criteriaBuilder.equal(joinClaimsVehicle.get(ClaimsDamagedVehicleEntity_.licensePlate), damagedPlate));
        }else{
            Join<ClaimsNewEntity,ClaimsDamagedVehicleEntity> joinClaimsVehicle = claimsNewEntityRoot.join(ClaimsNewEntity_.claimsDamagedVehicleEntity, JoinType.INNER);
            predicates.add(criteriaBuilder.equal(joinClaimsVehicle.get(ClaimsDamagedVehicleEntity_.licensePlate), ""));
        }

        /*Se la targa è presente recupero tutte le controparti con quella targa, altrimenti recupero quelle con null o stringa vuota*/
        if(counterpartyPlate!=null) {
            predicates.add(criteriaBuilder.equal(
                    criteriaBuilder.upper(criteriaBuilder.function(JSONB_EXTRACT_PATH_TEXT.value(), String.class, joinCounterparty.get(CounterpartyNewEntity_.vehicle), criteriaBuilder.literal("license_plate"))),
                    counterpartyPlate.toUpperCase()));
        }else{
            predicates.add(criteriaBuilder.equal(
                    criteriaBuilder.upper(criteriaBuilder.function(JSONB_EXTRACT_PATH_TEXT.value(), String.class, joinCounterparty.get(CounterpartyNewEntity_.vehicle), criteriaBuilder.literal("license_plate"))),
                    ""));
        }

        if (counterpartyFirstName!=null) {
            predicates.add(criteriaBuilder.equal(
                    criteriaBuilder.upper(criteriaBuilder.function(JSONB_EXTRACT_PATH_TEXT.value(),String.class, joinCounterparty.get(CounterpartyNewEntity_.insured), criteriaBuilder.literal("firstname"))),
                    counterpartyFirstName.toUpperCase()));
        }else{
            predicates.add(criteriaBuilder.equal(
                    criteriaBuilder.upper(criteriaBuilder.function(JSONB_EXTRACT_PATH_TEXT.value(),String.class, joinCounterparty.get(CounterpartyNewEntity_.insured), criteriaBuilder.literal("firstname"))),""));
        }

        if(counterpartyLastName!=null){
            predicates.add(criteriaBuilder.equal(
                    criteriaBuilder.upper(criteriaBuilder.function(JSONB_EXTRACT_PATH_TEXT.value(),String.class, joinCounterparty.get(CounterpartyNewEntity_.insured), criteriaBuilder.literal("lastname"))),
                    counterpartyLastName.toUpperCase()));
        }else{
            predicates.add(criteriaBuilder.equal(
                    criteriaBuilder.upper(criteriaBuilder.function(JSONB_EXTRACT_PATH_TEXT.value(),String.class, joinCounterparty.get(CounterpartyNewEntity_.insured), criteriaBuilder.literal("lastname"))), ""));
        }

        if (dateAccidentFrom != null && dateAccidentTo != null) {

            Instant instantDateAccidentFrom = DateUtil.convertIS08601StringToUTCInstant(dateAccidentFrom);
            Instant instantDateAccidentTo = DateUtil.convertIS08601StringToUTCInstant(dateAccidentTo);
            predicates.add(criteriaBuilder.between(
                    claimsNewEntityRoot.get(ClaimsNewEntity_.dateAccident),instantDateAccidentFrom, instantDateAccidentTo));

            predicates.add(criteriaBuilder.notEqual(claimsNewEntityRoot.get(ClaimsNewEntity_.status), ClaimsStatusEnum.WAITING_FOR_VALIDATION));

        }
        if(predicates!= null && !predicates.isEmpty()){
            claimsNewEntityCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));
        }


        return em.createQuery(claimsNewEntityCriteriaQuery).getResultList();

    }



    //FINE QUERY controllo dei duplicati




    //START IN EVIDENCE

    @Modifying
    @Transactional
    public void updateClaimsEvidence() {
        //REFACTOR

        StringBuilder sql = new StringBuilder("UPDATE claims_new SET in_evidence = true WHERE id IN " +
                " (SELECT claims.id From claims_new claims inner join configure_evidence ce ON claims.status = ce.practical_state_en\n" +
                "WHERE ce.is_active = true AND claims.in_evidence = false\n" +
                "AND (now()\\:\\:DATE - claims.status_updated_at\\:\\:DATE) >= ce.days_of_stay_in_the_state )");

        Query query = em.createNativeQuery(sql.toString());
        query.executeUpdate();
    }

    //END IN EVIDENCE

    //START eliminazione claims DRAFT


    @Modifying
    @Transactional
    public void deleteClaimsInBozzaRange(Integer range) {
        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("DELETE FROM claims_new c WHERE c.status = 'DRAFT' ");
        //StringBuilder whereClauseBuilder = new StringBuilder(" WHERE 1=1 ");
        if (range != null)
            queryBuilder.append(" AND DATE_PART('month', c.updated_at ) > " + range);
        Query sql = em.createNativeQuery(queryBuilder.toString());
        sql.executeUpdate();
    }

    //END eliminazione claims DRAFT


    //START controllo in inserimento che non siano presenti furti


    public List<ClaimsNewEntity> findNotClosedTheftWhitPlate(String plate, String data) {

        List<ClaimsStatusEnum> claimsStatusEnums = new ArrayList<ClaimsStatusEnum>() {{
            add(ClaimsStatusEnum.DELETED);
            add(ClaimsStatusEnum.CLOSED);
            add(ClaimsStatusEnum.CLOSED_PRACTICE);
        }};



        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<ClaimsNewEntity> claimsNewEntityCriteriaQuery = criteriaBuilder.createQuery(ClaimsNewEntity.class);
        Root<ClaimsNewEntity> claimsNewEntityRoot = claimsNewEntityCriteriaQuery.from(ClaimsNewEntity.class);
        //Join<ClaimsNewEntity,CounterpartyNewEntity> joinCounterparty =claimsNewEntityRoot.join(ClaimsNewEntity_.counterparts,JoinType.LEFT);
        claimsNewEntityCriteriaQuery.select(claimsNewEntityRoot);



        if ( data == null || data.isEmpty()){
            data = DateUtil.convertUTCInstantToIS08601String(DateUtil.getNowInstant());
        }

        Instant dateAccidentInstant = DateUtil.convertIS08601StringToUTCInstant(data);

        List<Predicate> predicates = new LinkedList<>();
        if (claimsStatusEnums != null && !claimsStatusEnums.isEmpty()) {
            predicates.add(criteriaBuilder.not(claimsNewEntityRoot.get(ClaimsNewEntity_.status).in(claimsStatusEnums)));
        }


        Join<ClaimsNewEntity,ClaimsDamagedVehicleEntity> joinClaimsVehicle =claimsNewEntityRoot.join(ClaimsNewEntity_.claimsDamagedVehicleEntity,JoinType.INNER);
        predicates.add(criteriaBuilder.equal(joinClaimsVehicle.get(ClaimsDamagedVehicleEntity_.licensePlate), plate));

        Join<ClaimsNewEntity,ClaimsTheftEntity> joinClaimsTheft =claimsNewEntityRoot.join(ClaimsNewEntity_.claimsTheftEntity,JoinType.LEFT);

        predicates.add(criteriaBuilder.or(
                criteriaBuilder.and(
                        criteriaBuilder.lessThanOrEqualTo(claimsNewEntityRoot.get(ClaimsNewEntity_.dateAccident),dateAccidentInstant),
                        criteriaBuilder.equal(claimsNewEntityRoot.get(ClaimsNewEntity_.typeAccident), DataAccidentTypeAccidentEnum.FURTO_TOTALE)
                ),
                criteriaBuilder.and(
                        criteriaBuilder.between( criteriaBuilder.literal(dateAccidentInstant), claimsNewEntityRoot.get(ClaimsNewEntity_.dateAccident),joinClaimsTheft.get(ClaimsTheftEntity_.findDate)),
                        criteriaBuilder.equal(claimsNewEntityRoot.get(ClaimsNewEntity_.typeAccident), DataAccidentTypeAccidentEnum.FURTO_E_RITROVAMENTO)
                )
                )
        );

        claimsNewEntityCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));


        return em.createQuery(claimsNewEntityCriteriaQuery).getResultList();


    }

    //END controllo in inserimento che non siano presenti furti


    //START integrazione con closing

    public List<ClosingResponseV1> findClosing(String idContract) {

        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Tuple> claimsCriteriaQuery = criteriaBuilder.createTupleQuery();
        Root<ClaimsNewEntity> claimsNewEntityRoot = claimsCriteriaQuery.from(ClaimsNewEntity.class);
        Join<ClaimsNewEntity, ClaimsDamagedContractEntity> joinClaimsContract = claimsNewEntityRoot.join(ClaimsNewEntity_.claimsDamagedContractEntity, JoinType.INNER);
        Root<ContractTypeEntity> contractTypeEntityRoot = claimsCriteriaQuery.from(ContractTypeEntity.class);


        List<Predicate> predicates = new LinkedList<>();

        predicates.add(criteriaBuilder.equal(joinClaimsContract.get(ClaimsDamagedContractEntity_.contractType),contractTypeEntityRoot.get(ContractTypeEntity_.codContractType) ));
        predicates.add(criteriaBuilder.equal(joinClaimsContract.get(ClaimsDamagedContractEntity_.contractId),idContract));


        claimsCriteriaQuery.multiselect(
                claimsNewEntityRoot.get(ClaimsNewEntity_.practiceId).alias("id_complaint"),
                claimsNewEntityRoot.get(ClaimsNewEntity_.createdAt).alias("claims_insert_date"),
                contractTypeEntityRoot.get(ContractTypeEntity_.franchise).alias("exemption"),
                claimsNewEntityRoot.get(ClaimsNewEntity_.status).alias("status"),
                claimsNewEntityRoot.get(ClaimsNewEntity_.impactPoint).alias("impact_point"),
                claimsNewEntityRoot.get(ClaimsNewEntity_.exemption).alias("exemption_class")

        );

        claimsCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));

        Query sql = em.createQuery(claimsCriteriaQuery);
        List<Tuple> result =  sql.getResultList();
        List<ClosingResponseV1> responseV1List = new LinkedList<>();



        for(Tuple t : result){
            ClosingResponseV1 closingResponseV1=new ClosingResponseV1();
            closingResponseV1.setIdComplaint((t.get("id_complaint")).toString());
            Instant dateData = (Instant)t.get("claims_insert_date");
            if(dateData != null) {

                closingResponseV1.setClaimsInsertDate(Date.from(dateData));
            }

            ClaimsStatusEnum claimsStatusEnum = (ClaimsStatusEnum)t.get("status");
            if(claimsStatusEnum != null){
                closingResponseV1.setStatus(claimsStatusEnum.getValue());
            }

            closingResponseV1.setExemption(((Number)t.get("exemption")).doubleValue());

            ImpactPoint impactPoint = (ImpactPoint) t.get("impact_point");
            if(impactPoint != null){
                closingResponseV1.setDamageList(impactPoint.getIncidentDescription());
            }

            Exemption exemption = (Exemption)t.get("exemption_class");
            if(exemption != null){
                closingResponseV1.setDeductPay1(exemption.getDeductPaid1());
                closingResponseV1.setDeductPay2(exemption.getDeductPaid2());
            }
            responseV1List.add(closingResponseV1);

        }


        return responseV1List;

    }


    //END integrazione con closing

    //INIZIO integrazione MyALD



    public List<Predicate> getFilterMyAldWithoutDraftMyAldFleetManager(CriteriaBuilder criteriaBuilder , Root claimsRoot, String contractPlateNumberSx, List<String> clientIdList, String clientId, List<String> status, String dateAccidentFrom, String dateAccidentTo, String dateCreatedFrom, String dateCreatedTo, List<MyAldInsertedByEnum> insertedByList, String userId) {
        List<Predicate> predicates = new LinkedList<>();

        predicates.add(criteriaBuilder.notEqual(claimsRoot.get(ClaimsNewEntity_.status), ClaimsStatusEnum.DRAFT));


        if (dateAccidentFrom != null && dateAccidentTo != null) {
            dateAccidentFrom = dateAccidentFrom + "T00:00:00Z";
            dateAccidentTo = dateAccidentTo + "T23:59:59Z";

            Instant instantDateAccidentFrom = DateUtil.convertIS08601StringToUTCInstant(dateAccidentFrom);
            Instant instantDateAccidentTo = DateUtil.convertIS08601StringToUTCInstant(dateAccidentTo);

            if (dateAccidentTo.compareTo(dateAccidentFrom) < 0) {
                LOGGER.debug("the date_accident_to is less than the date_accident_from");
                throw new BadParametersException("the date_accident_to is less than the date_accident_from", MessageCode.CLAIMS_1011);
            } else {
                predicates.add(criteriaBuilder.between(
                        claimsRoot.get(ClaimsNewEntity_.dateAccident),instantDateAccidentFrom, instantDateAccidentTo));
            }
        }else if (dateAccidentFrom != null) {
            dateAccidentFrom = dateAccidentFrom + "T00:00:00Z";
            Instant instantDateAccidentFrom = DateUtil.convertIS08601StringToUTCInstant(dateAccidentFrom);
            predicates.add(criteriaBuilder.greaterThanOrEqualTo(
                    claimsRoot.get(ClaimsNewEntity_.dateAccident),instantDateAccidentFrom));
        }else if (dateAccidentTo != null) {
            dateAccidentTo = dateAccidentTo + "T23:59:59.00Z";
            Instant instantDateAccidentTo = DateUtil.convertIS08601StringToUTCInstant(dateAccidentTo);
            predicates.add(criteriaBuilder.lessThanOrEqualTo(
                    claimsRoot.get(ClaimsNewEntity_.dateAccident),instantDateAccidentTo));

        }


        if (dateCreatedFrom != null && dateCreatedTo != null) {
            dateCreatedFrom = dateCreatedFrom + "T00:00:00Z";
            dateCreatedTo = dateCreatedTo + "T23:59:59.00Z";

            Instant instantDateCreatedFrom = DateUtil.convertIS08601StringToUTCInstant(dateCreatedFrom);
            Instant instantDateCreatedTo = DateUtil.convertIS08601StringToUTCInstant(dateCreatedTo);

            if (dateCreatedTo.compareTo(dateCreatedFrom) < 0) {
                LOGGER.debug("the date_created_to is less than the date_created_from");
                throw new BadParametersException("the date_created_to is less than the date_created_from", MessageCode.CLAIMS_1011);
            } else {
                predicates.add(criteriaBuilder.between(
                        claimsRoot.get(ClaimsNewEntity_.createdAt),instantDateCreatedFrom, instantDateCreatedTo));
            }
        } else if (dateCreatedFrom != null) {
            dateCreatedFrom = dateCreatedFrom + "T00:00:00Z";
            Instant instantDateCreatedFrom = DateUtil.convertIS08601StringToUTCInstant(dateCreatedFrom);
            predicates.add(criteriaBuilder.greaterThanOrEqualTo(
                    claimsRoot.get(ClaimsNewEntity_.createdAt),instantDateCreatedFrom));

        }else if (dateCreatedTo != null) {
            dateCreatedTo = dateCreatedTo + "T23:59:59.00Z";
            Instant instantDateCreatedTo = DateUtil.convertIS08601StringToUTCInstant(dateCreatedTo);
            predicates.add(criteriaBuilder.lessThanOrEqualTo(
                    claimsRoot.get(ClaimsNewEntity_.createdAt),instantDateCreatedTo));
        }

        if(Util.isNotEmpty(contractPlateNumberSx)){
            Join<ClaimsNewEntity, ClaimsDamagedContractEntity> joinClaimsContract = claimsRoot.join(ClaimsNewEntity_.claimsDamagedContractEntity, JoinType.INNER);


            predicates.add(criteriaBuilder.or(
                    criteriaBuilder.equal(joinClaimsContract.get(ClaimsDamagedContractEntity_.contractId).as(String.class), contractPlateNumberSx),
                    criteriaBuilder.equal(claimsRoot.get(ClaimsNewEntity_.plate), contractPlateNumberSx),
                    criteriaBuilder.equal(claimsRoot.get(ClaimsNewEntity_.practiceId).as(String.class),contractPlateNumberSx)
            ));
        }

        if ((clientIdList != null && !clientIdList.isEmpty()) ||  Util.isNotEmpty(clientId)){
            Join<ClaimsNewEntity, ClaimsDamagedCustomerEntity> joinClaimsCustomer = claimsRoot.join(ClaimsNewEntity_.claimsDamagedCustomerEntity, JoinType.INNER);

            if (clientIdList != null && !clientIdList.isEmpty()) {
                List<Long> clientListLong = new LinkedList<>();
                for(String currentClient: clientIdList){
                    clientListLong.add(Long.parseLong(currentClient));
                }

                predicates.add(joinClaimsCustomer.get(ClaimsDamagedCustomerEntity_.customerId).in(clientListLong));
            }

            if (Util.isNotEmpty(clientId)) {

                predicates.add(criteriaBuilder.equal(
                        joinClaimsCustomer.get(ClaimsDamagedCustomerEntity_.customerId),clientId));

            }
        }

        if (status != null && !status.isEmpty()) {
            List<ClaimsStatusEnum> claimsStatusEnums = new LinkedList<>();
            for(String currentStatus : status){
                claimsStatusEnums.add(ClaimsStatusEnum.create(currentStatus));
            }
            predicates.add(claimsRoot.get(ClaimsNewEntity_.status).in(claimsStatusEnums));
        }



        if(insertedByList != null && !insertedByList.isEmpty()){
            List<Predicate> listPredicateMetadata = new LinkedList<>();
            Join<ClaimsNewEntity, ClaimsMetadataEntity> joinClaimsMetadata = claimsRoot.join(ClaimsNewEntity_.claimsMetadataEntity, JoinType.LEFT);

            int count = 0;

            if (insertedByList.contains(MyAldInsertedByEnum.ME)) {
                listPredicateMetadata.add(criteriaBuilder.equal(joinClaimsMetadata.get(ClaimsMetadataEntity_.userId), userId));
                count++;
            }
            if( insertedByList.contains(MyAldInsertedByEnum.OTHER)) {
                if (count > 0) {
                    Predicate predicate = criteriaBuilder.or(criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()])),criteriaBuilder.notEqual(joinClaimsMetadata.get(ClaimsMetadataEntity_.userId), userId));
                    listPredicateMetadata.clear();
                    listPredicateMetadata.add(predicate);

                }else {
                    listPredicateMetadata.add(criteriaBuilder.notEqual(joinClaimsMetadata.get(ClaimsMetadataEntity_.userId), userId));
                }
                count++;
            }
            if(insertedByList.contains(MyAldInsertedByEnum.ALD_OPERATOR)){
                if( count > 0) {
                    Predicate predicate = criteriaBuilder.or(criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()])),criteriaBuilder.isNull(joinClaimsMetadata.get(ClaimsMetadataEntity_.id)) );
                    predicates.clear();
                    listPredicateMetadata.add(predicate);
                }else {
                    listPredicateMetadata.add(criteriaBuilder.isNull(joinClaimsMetadata.get(ClaimsMetadataEntity_.id)));
                }
            }

            if(!listPredicateMetadata.isEmpty()){
                predicates.addAll(listPredicateMetadata);

            }

        }

        return  predicates;
    }



    /* Pagination MyAld FleetManager
   * FILTRI
       1) contratto, targa e n° sinistro
       2) data del sinistro dal - data del sinistro al
       3) stato denuncia
       4) data inserimento sinistro dal - data inserimento sinistro al
       5) inserita da: me, altri utenti, operatore ald
   *
   * */
    public List<ClaimsNewEntity> findClaimsForPaginationWithoutDraftMyAldFleetManager(String contractPlateNumberSx, List<String> clientIdList, String clientId, List<String> status, String dateAccidentFrom, String dateAccidentTo, String dateCreatedFrom, String dateCreatedTo, List<MyAldInsertedByEnum> insertedByList, String userId, Boolean asc, String orderBy, Integer page, Integer pageSize) {
        Map<String, Object> mappOrder = new HashMap<>();
        mappOrder.put("practice_id", ClaimsNewEntity_.practiceId);
        mappOrder.put("date_accident", ClaimsNewEntity_.dateAccident);
        mappOrder.put("plate", ClaimsNewEntity_.plate);
        mappOrder.put("created_at", ClaimsNewEntity_.createdAt);
        mappOrder.put("is_with_counterparty", ClaimsNewEntity_.isWithCounterparty);


        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<ClaimsNewEntity> claimsNewEntityCriteriaQuery = criteriaBuilder.createQuery(ClaimsNewEntity.class);
        Root<ClaimsNewEntity> claimsNewEntityRoot = claimsNewEntityCriteriaQuery.from(ClaimsNewEntity.class);

        List<Predicate> predicateList = this.getFilterMyAldWithoutDraftMyAldFleetManager(criteriaBuilder, claimsNewEntityRoot,  contractPlateNumberSx,  clientIdList,  clientId,  status,  dateAccidentFrom,  dateAccidentTo,  dateCreatedFrom,  dateCreatedTo,  insertedByList,  userId);
        claimsNewEntityCriteriaQuery.where(predicateList.toArray(new Predicate[predicateList.size()]));


        SingularAttribute fieldToOrder = null;
        if (orderBy != null) {
            fieldToOrder = (SingularAttribute) mappOrder.get(orderBy);
        }
        if (fieldToOrder != null && asc != null) {
            if(orderBy.equalsIgnoreCase("is_with_counterparty")){
                asc=!asc;
            }
            if (asc) {
                claimsNewEntityCriteriaQuery.orderBy(criteriaBuilder.asc(claimsNewEntityRoot.get(fieldToOrder)));
            } else {
                claimsNewEntityCriteriaQuery.orderBy(criteriaBuilder.desc(claimsNewEntityRoot.get(fieldToOrder)));
            }

        } else if (fieldToOrder != null) {
            claimsNewEntityCriteriaQuery.orderBy(criteriaBuilder.desc(claimsNewEntityRoot.get(fieldToOrder)));
        }



        return em.createQuery(claimsNewEntityCriteriaQuery)
                .setFirstResult((page - 1) * pageSize)
                .setMaxResults(pageSize)
                .getResultList();
    }

    public BigInteger countClaimsForPaginationWithoutDraftMyAldFleetManager(String contractPlateNumberSx, List<String> clientIdList,String clientId, List<String> status, String dateAccidentFrom, String dateAccidentTo, String dateCreatedFrom, String dateCreatedTo, List<MyAldInsertedByEnum> insertedByList, String userId) {

        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Long> claimsNewEntityCriteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<ClaimsNewEntity> claimsNewEntityRoot = claimsNewEntityCriteriaQuery.from(ClaimsNewEntity.class);
        claimsNewEntityCriteriaQuery.select(criteriaBuilder.count(claimsNewEntityRoot));


        List<Predicate> predicateList = this.getFilterMyAldWithoutDraftMyAldFleetManager(criteriaBuilder, claimsNewEntityRoot,  contractPlateNumberSx,  clientIdList,  clientId,  status,  dateAccidentFrom,  dateAccidentTo,  dateCreatedFrom,  dateCreatedTo,  insertedByList,  userId);
        claimsNewEntityCriteriaQuery.where(predicateList.toArray(new Predicate[predicateList.size()]));


        Long longResult = em.createQuery(claimsNewEntityCriteriaQuery).getSingleResult();
        BigInteger bigIntegerResult = null;
        if(longResult != null) {
            bigIntegerResult =BigInteger.valueOf(longResult.longValue());
        }else{
            bigIntegerResult =BigInteger.valueOf(0);
        }

        return bigIntegerResult ;
    }



    //funzione per foltre rotta ClaimsForPaginationWithoutDraftMyAldDriver
    private List<Predicate> getFilterClaimsForPaginationWithoutDraftMyAldDriver(CriteriaBuilder criteriaBuilder , Root claimsRoot, List<String> plates, Boolean isInclusive,String contractPlateNumberSx, List<String> status, String dateAccidentFrom, String dateAccidentTo, String dateCreatedFrom, String dateCreatedTo, List<MyAldInsertedByEnum> insertedByList, String userId) {
        List<Predicate> predicates = new LinkedList<>();

        predicates.add(criteriaBuilder.notEqual(claimsRoot.get(ClaimsNewEntity_.status), ClaimsStatusEnum.DRAFT));


        if (dateAccidentFrom != null && dateAccidentTo != null) {
            dateAccidentFrom = dateAccidentFrom + "T00:00:00Z";
            dateAccidentTo = dateAccidentTo + "T23:59:59Z";

            Instant instantDateAccidentFrom = DateUtil.convertIS08601StringToUTCInstant(dateAccidentFrom);
            Instant instantDateAccidentTo = DateUtil.convertIS08601StringToUTCInstant(dateAccidentTo);

            if (dateAccidentTo.compareTo(dateAccidentFrom) < 0) {
                LOGGER.debug("the date_accident_to is less than the date_accident_from");
                throw new BadParametersException("the date_accident_to is less than the date_accident_from", MessageCode.CLAIMS_1011);
            } else {
                predicates.add(criteriaBuilder.between(
                        claimsRoot.get(ClaimsNewEntity_.dateAccident),instantDateAccidentFrom, instantDateAccidentTo));
            }
        }else if (dateAccidentFrom != null) {
            dateAccidentFrom = dateAccidentFrom + "T00:00:00Z";
            Instant instantDateAccidentFrom = DateUtil.convertIS08601StringToUTCInstant(dateAccidentFrom);
            predicates.add(criteriaBuilder.greaterThanOrEqualTo(
                    claimsRoot.get(ClaimsNewEntity_.dateAccident),instantDateAccidentFrom));
        }else if (dateAccidentTo != null) {
            dateAccidentTo = dateAccidentTo + "T23:59:59.00Z";
            Instant instantDateAccidentTo = DateUtil.convertIS08601StringToUTCInstant(dateAccidentTo);
            predicates.add(criteriaBuilder.lessThanOrEqualTo(
                    claimsRoot.get(ClaimsNewEntity_.dateAccident),instantDateAccidentTo));

        }


        if (dateCreatedFrom != null && dateCreatedTo != null) {
            dateCreatedFrom = dateCreatedFrom + "T00:00:00Z";
            dateCreatedTo = dateCreatedTo + "T23:59:59.00Z";

            Instant instantDateCreatedFrom = DateUtil.convertIS08601StringToUTCInstant(dateCreatedFrom);
            Instant instantDateCreatedTo = DateUtil.convertIS08601StringToUTCInstant(dateCreatedTo);

            if (dateCreatedTo.compareTo(dateCreatedFrom) < 0) {
                LOGGER.debug("the date_created_to is less than the date_created_from");
                throw new BadParametersException("the date_created_to is less than the date_created_from", MessageCode.CLAIMS_1011);
            } else {
                predicates.add(criteriaBuilder.between(
                        claimsRoot.get(ClaimsNewEntity_.createdAt),instantDateCreatedFrom, instantDateCreatedTo));
            }
        } else if (dateCreatedFrom != null) {
            dateCreatedFrom = dateCreatedFrom + "T00:00:00Z";
            Instant instantDateCreatedFrom = DateUtil.convertIS08601StringToUTCInstant(dateCreatedFrom);
            predicates.add(criteriaBuilder.greaterThanOrEqualTo(
                    claimsRoot.get(ClaimsNewEntity_.createdAt),instantDateCreatedFrom));

        }else if (dateCreatedTo != null) {
            dateCreatedTo = dateCreatedTo + "T23:59:59.00Z";
            Instant instantDateCreatedTo = DateUtil.convertIS08601StringToUTCInstant(dateCreatedTo);
            predicates.add(criteriaBuilder.lessThanOrEqualTo(
                    claimsRoot.get(ClaimsNewEntity_.createdAt),instantDateCreatedTo));
        }

        if(Util.isNotEmpty(contractPlateNumberSx)){
            Join<ClaimsNewEntity, ClaimsDamagedContractEntity> joinClaimsContract = claimsRoot.join(ClaimsNewEntity_.claimsDamagedContractEntity, JoinType.INNER);


            predicates.add(criteriaBuilder.or(
                    criteriaBuilder.equal(joinClaimsContract.get(ClaimsDamagedContractEntity_.contractId).as(String.class), contractPlateNumberSx),
                    criteriaBuilder.equal(claimsRoot.get(ClaimsNewEntity_.plate), contractPlateNumberSx),
                    criteriaBuilder.equal(claimsRoot.get(ClaimsNewEntity_.practiceId).as(String.class),contractPlateNumberSx)
            ));
        }



        if (status != null && !status.isEmpty()) {
            List<ClaimsStatusEnum> claimsStatusEnums = new LinkedList<>();
            for(String currentStatus : status){
                claimsStatusEnums.add(ClaimsStatusEnum.create(currentStatus));
            }
            predicates.add(claimsRoot.get(ClaimsNewEntity_.status).in(claimsStatusEnums));
        }

        if(isInclusive != null && isInclusive){
            predicates.add(claimsRoot.get(ClaimsNewEntity_.plate).in(plates));
        } else {
            predicates.add(criteriaBuilder.not(claimsRoot.get(ClaimsNewEntity_.plate).in(plates)));
        }

        if(insertedByList != null && !insertedByList.isEmpty()){
            List<Predicate> listPredicateMetadata = new LinkedList<>();
            Join<ClaimsNewEntity, ClaimsMetadataEntity> joinClaimsMetadata = claimsRoot.join(ClaimsNewEntity_.claimsMetadataEntity, JoinType.LEFT);

            int count = 0;

            if (insertedByList.contains(MyAldInsertedByEnum.ME)) {
                listPredicateMetadata.add(criteriaBuilder.equal(joinClaimsMetadata.get(ClaimsMetadataEntity_.userId), userId));
                count++;
            }
            if( insertedByList.contains(MyAldInsertedByEnum.OTHER)) {
                if (count > 0) {
                    Predicate predicate = criteriaBuilder.or(criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()])),criteriaBuilder.notEqual(joinClaimsMetadata.get(ClaimsMetadataEntity_.userId), userId));
                    listPredicateMetadata.clear();
                    listPredicateMetadata.add(predicate);

                }else {
                    listPredicateMetadata.add(criteriaBuilder.notEqual(joinClaimsMetadata.get(ClaimsMetadataEntity_.userId), userId));
                }
                count++;
            }
            if(insertedByList.contains(MyAldInsertedByEnum.ALD_OPERATOR)){
                if( count > 0) {
                    Predicate predicate = criteriaBuilder.or(criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()])),criteriaBuilder.isNull(joinClaimsMetadata.get(ClaimsMetadataEntity_.id)) );
                    predicates.clear();
                    listPredicateMetadata.add(predicate);
                }else {
                    listPredicateMetadata.add(criteriaBuilder.isNull(joinClaimsMetadata.get(ClaimsMetadataEntity_.id)));
                }
            }

            if(!listPredicateMetadata.isEmpty()){
                predicates.addAll(listPredicateMetadata);

            }

        }

        return  predicates;
    }



    /* Pagination MyAld Driver
  * FILTRI
      1) contratto, n° sinistro
      2) data del sinistro dal - data del sinistro al
      3) stato denuncia
      4) data inserimento sinistro dal - data inserimento sinistro al
      5) lista di targhe
  *
  * */
    public List<ClaimsNewEntity> findClaimsForPaginationWithoutDraftMyAldDriver(List<String> plates,String contractPlateNumberSx, List<String> status,String dateAccidentFrom, String dateAccidentTo, String dateCreatedFrom, String dateCreatedTo, Boolean asc, String orderBy, Integer page, Integer pageSize, String userId, List<MyAldInsertedByEnum> insertedByList, Boolean isInclusive) {
        Map<String, Object> mappOrder = new HashMap<>();
        mappOrder.put("practice_id", ClaimsNewEntity_.practiceId);
        mappOrder.put("date_accident", ClaimsNewEntity_.dateAccident);
        mappOrder.put("plate", ClaimsNewEntity_.plate);
        mappOrder.put("created_at", ClaimsNewEntity_.createdAt);


        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<ClaimsNewEntity> claimsNewEntityCriteriaQuery = criteriaBuilder.createQuery(ClaimsNewEntity.class);
        Root<ClaimsNewEntity> claimsNewEntityRoot = claimsNewEntityCriteriaQuery.from(ClaimsNewEntity.class);


        List<Predicate> predicateList = this.getFilterClaimsForPaginationWithoutDraftMyAldDriver(criteriaBuilder, claimsNewEntityRoot, plates, isInclusive, contractPlateNumberSx,   status,  dateAccidentFrom,  dateAccidentTo,  dateCreatedFrom,  dateCreatedTo,  insertedByList,  userId);
        claimsNewEntityCriteriaQuery.where(predicateList.toArray(new Predicate[predicateList.size()]));


        SingularAttribute fieldToOrder = null;
        if (orderBy != null) {
            fieldToOrder = (SingularAttribute) mappOrder.get(orderBy);
        }
        if (fieldToOrder != null && asc != null) {
            if (asc) {
                claimsNewEntityCriteriaQuery.orderBy(criteriaBuilder.asc(claimsNewEntityRoot.get(fieldToOrder)));
            } else {
                claimsNewEntityCriteriaQuery.orderBy(criteriaBuilder.desc(claimsNewEntityRoot.get(fieldToOrder)));
            }

        } else if (fieldToOrder != null) {
            claimsNewEntityCriteriaQuery.orderBy(criteriaBuilder.desc(claimsNewEntityRoot.get(fieldToOrder)));
        }


        return em.createQuery(claimsNewEntityCriteriaQuery)
                .setFirstResult((page - 1) * pageSize)
                .setMaxResults(pageSize)
                .getResultList();
    }

    public BigInteger countClaimsForPaginationWithoutDraftMyAldDriver(List<String> plates,String contractPlateNumberSx, List<String> status,String dateAccidentFrom, String dateAccidentTo, String dateCreatedFrom, String dateCreatedTo,  String userId, List<MyAldInsertedByEnum> insertedByList, Boolean isInclusive) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Long> claimsNewEntityCriteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<ClaimsNewEntity> claimsNewEntityRoot = claimsNewEntityCriteriaQuery.from(ClaimsNewEntity.class);
        claimsNewEntityCriteriaQuery.select(criteriaBuilder.count(claimsNewEntityRoot));

        List<Predicate> predicateList = this.getFilterClaimsForPaginationWithoutDraftMyAldDriver(criteriaBuilder, claimsNewEntityRoot, plates, isInclusive, contractPlateNumberSx,   status,  dateAccidentFrom,  dateAccidentTo,  dateCreatedFrom,  dateCreatedTo,  insertedByList,  userId);
        claimsNewEntityCriteriaQuery.where(predicateList.toArray(new Predicate[predicateList.size()]));


        Long longResult = em.createQuery(claimsNewEntityCriteriaQuery).getSingleResult();
        BigInteger bigIntegerResult = null;
        if(longResult != null) {
            bigIntegerResult =BigInteger.valueOf(longResult.longValue());
        }else{
            bigIntegerResult =BigInteger.valueOf(0);
        }

        return bigIntegerResult ;

    }

    //FINE INTEGRAZIONE MyALD

    //INIZIO INTEGRAZIONE PRATICHE AUTO



    private Long checkHoursDifference (Date dateUTC){

        String DATE_FORMAT = "dd-M-yyyy hh:mm:ss a";

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-M-yyyy hh:mm:ss a");
        String dateInString = simpleDateFormat.format(dateUTC);
        LocalDateTime ldt = LocalDateTime.parse(dateInString, DateTimeFormatter.ofPattern(DATE_FORMAT));

        ZoneId romeId = ZoneId.of(timeZoneApplication);
        ZoneId utcId = ZoneId.of("UTC");

        //LocalDateTime + ZoneId = ZonedDateTime
        ZonedDateTime utcTime = ldt.atZone(utcId);
        ZonedDateTime romeTime = ldt.atZone(romeId);
        LOGGER.debug("Date ("+ timeZoneApplication + ") : " + romeTime);
        LOGGER.debug("Date (UTC) : " + utcTime);
        Long diff = ChronoUnit.HOURS.between(romeTime,utcTime);
        return diff;

    }



    public List<ClaimsNewEntity> findTheftForLossPossessionMigrated(String goLiveStepZeroActivationDate){
        StringBuffer atrSelectFrom = new StringBuffer().append("select " +
                " c.* " +
                "from " +
                " claims_new c " +
                "inner join claims_damaged_vehicle cdv on " +
                " c.id = cdv.id " +
                "where " +
                " c.type_accident = 'FURTO_TOTALE' " +
                " and ((now()\\:\\:date - c.date_accident\\:\\:date > 30 " +
                "  and cdv.nature like 'MOTOR_CYCLE') " +
                " or (now()\\:\\:date - c.date_accident\\:\\:date > 60 " +
                "  and (cdv.nature like 'MOTOR_VEHICLE' " +
                "   or cdv.nature like 'VAN') )) ");
        if(goLiveStepZeroActivationDate!=null){
            atrSelectFrom.append(" and (c.is_migrated <> true " +
                    "  or ( c.is_migrated = true " +
                    "   and (:stepZeroDate\\:\\:date - c.date_accident\\:\\:date < 60 " +
                    "    and (cdv.nature like 'MOTOR_VEHICLE' " +
                    "     or cdv.nature like 'VAN')) " +
                    "   or (:stepZeroDate\\:\\:date - c.date_accident\\:\\:date < 30 " +
                    "    and cdv.nature like 'MOTOR_CYCLE') ) ) ");
        }
        Query query = em.createNativeQuery(atrSelectFrom.toString(),ClaimsNewEntity.class);
        if(goLiveStepZeroActivationDate!=null){
            query.setParameter("stepZeroDate", goLiveStepZeroActivationDate);
        }
        List<ClaimsNewEntity> resultList = query.getResultList();
        return resultList;
    }

    public List<ClaimsNewEntity> findTheftForLossPossessionNotMigrated(String goLiveStepZeroActivationDate ) {

        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<ClaimsNewEntity> claimsNewEntityCriteriaQuery = criteriaBuilder.createQuery(ClaimsNewEntity.class);
        Root<ClaimsNewEntity> claimsRoot = claimsNewEntityCriteriaQuery.from(ClaimsNewEntity.class);
        Join<ClaimsNewEntity,ClaimsDamagedVehicleEntity> joinClaimsVehicle = claimsRoot.join(ClaimsNewEntity_.claimsDamagedVehicleEntity, JoinType.INNER);
        List<Predicate> predicates = new LinkedList<>();
        Predicate whereFurtoTotale = criteriaBuilder.equal(claimsRoot.get(ClaimsNewEntity_.typeAccident), DataAccidentTypeAccidentEnum.FURTO_TOTALE);



        Integer hour = null;
        if(checkHoursDifference(new Date()) == 1){
            hour = 23;
        }else{
            hour = 22;
        }


        //oggi -30
        Instant instantMotorCycle = new Date().toInstant();
        instantMotorCycle = instantMotorCycle.plus( -30, ChronoUnit.DAYS);
        instantMotorCycle = instantMotorCycle.atZone(ZoneOffset.UTC)
                .withHour(hour)
                .withMinute(59)
                .withSecond(59)
                .toInstant();


        //oggi -60
        Instant instantMotorVehicle = new Date().toInstant();
        instantMotorVehicle = instantMotorVehicle.plus( -60, ChronoUnit.DAYS);
        instantMotorVehicle = instantMotorVehicle.atZone(ZoneOffset.UTC)
                .withHour(hour)
                .withMinute(59)
                .withSecond(59)
                .toInstant();

        List<VehicleNatureEnum> vehicleNatureEnums60days = new LinkedList<VehicleNatureEnum>(){{
            add(VehicleNatureEnum.MOTOR_VEHICLE);
            add(VehicleNatureEnum.VAN);
        }};


        Predicate  firstAnd = criteriaBuilder.and(
                criteriaBuilder.equal(joinClaimsVehicle.get(ClaimsDamagedVehicleEntity_.nature), VehicleNatureEnum.MOTOR_CYCLE),
                criteriaBuilder.lessThanOrEqualTo(claimsRoot.get(ClaimsNewEntity_.dateAccident), instantMotorCycle)
        );

        Predicate  secondAnd = criteriaBuilder.and(
                joinClaimsVehicle.get(ClaimsDamagedVehicleEntity_.nature).in(vehicleNatureEnums60days),
                criteriaBuilder.lessThanOrEqualTo(claimsRoot.get(ClaimsNewEntity_.dateAccident), instantMotorVehicle)
        );


        Predicate or = criteriaBuilder.or(criteriaBuilder.and(firstAnd),criteriaBuilder.and(secondAnd));

        Predicate andTotal = null;
        if(goLiveStepZeroActivationDate != null && !goLiveStepZeroActivationDate.isEmpty()){
            Predicate isMigrated = criteriaBuilder.notEqual(claimsRoot.get(ClaimsNewEntity_.isMigrated), true);
            andTotal = criteriaBuilder.and(whereFurtoTotale,isMigrated, or);
        } else {
            andTotal = criteriaBuilder.and(whereFurtoTotale, or);
        }


        predicates.add(andTotal);

        claimsNewEntityCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));

        return em.createQuery(claimsNewEntityCriteriaQuery)
                .getResultList();
    }

    //FINE INTEGRAZIONE PRATICHE AUTO

    //INIZIO VERSIONE DASHBOARD
    public List<Object[]> getClaimsStatsBasedStatus(Boolean inEvidence, Boolean withContinuation) {
        StringBuffer atrSelectFrom = new StringBuffer()
                .append("SELECT claims.status, claims.type, count(*) FROM claims_new claims ");
        StringBuffer atrWhere = new StringBuffer()
                .append("WHERE ")
                .append("( ")
                .append(    "claims.status IN ('WAITING_FOR_VALIDATION','INCOMPLETE', 'WAITING_FOR_REFUND', 'CLOSED_PARTIAL_REFUND' , 'TO_SEND_TO_CUSTOMER') ")
                .append("   OR ( ")
                .append("           claims.status = 'TO_ENTRUST' ")
                .append("           AND ( ")
                .append("                   ( ")
                .append("                       claims.type = 'FUL' ")
                .append("                       AND ")
                .append("                       claims.type_accident IN ('RC_ATTIVA', 'RC_CONCORSUALE', 'CARD_ATTIVA_FIRMA_SINGOLA', 'CARD_CONCORSUALE_FIRMA_SINGOLA', 'CARD_ATTIVA_DOPPIA_FIRMA', 'CARD_CONCORSUALE_DOPPIA_FIRMA') ")
                .append("                    ) ")
                .append("                    OR claims.type IN ('FNI', 'FCM') ")
                .append("               ) ")
                .append("       ) ")
                .append(") ");
        StringBuffer atrGroup = new StringBuffer()
                .append("GROUP BY claims.status, claims.type ");

        if (inEvidence != null) {
            atrWhere.append(" AND claims.in_evidence = " + inEvidence + " ");
        }
        if (withContinuation != null) {
            atrWhere = this.addWithContinuationFilter(withContinuation,atrWhere);
        }
        StringBuffer atrSql = new StringBuffer()
                .append(atrSelectFrom)
                .append(atrWhere)
                .append(atrGroup);
        System.out.println("[DASHBOARD - QUERY BASED STATUS] "+atrSql);
        Query query = em.createNativeQuery(atrSql.toString());
        List<Object[]> resultList = query.getResultList();
        return resultList;
    }

    public List<Object[]> getStatsClaimWaitingForNumberSx(Boolean inEvidence, Boolean withContinuation) {
        StringBuffer atrSelectFrom = new StringBuffer()
                .append("SELECT  claims.status, claims.type, COUNT(DISTINCT claims.id) AS counter ")
                .append("FROM claims_new claims ");
        StringBuffer atrLeftJoin = new StringBuffer()
                .append("LEFT JOIN claims_from_company from_company ")
                .append("ON claims.id = from_company.id ");
        StringBuffer atrWhere = new StringBuffer()
                .append("WHERE ")
                .append("   claims.type = 'FUL' ")
                .append("   AND ")
                .append("   claims.type_accident IN ('CARD_NON_VERIFICABILE_FIRMA_SINGOLA','CARD_ATTIVA_FIRMA_SINGOLA','CARD_PASSIVA_FIRMA_SINGOLA','CARD_CONCORSUALE_FIRMA_SINGOLA','CARD_NON_VERIFICABILE_DOPPIA_FIRMA','CARD_ATTIVA_DOPPIA_FIRMA','CARD_PASSIVA_DOPPIA_FIRMA', 'CARD_CONCORSUALE_DOPPIA_FIRMA','RC_NON_VERIFICABILE','RC_ATTIVA','RC_PASSIVA','RC_CONCORSUALE') " )
                .append("   AND ")
                .append("   claims.status = 'WAITING_FOR_AUTHORITY' ")
                .append("   AND ")
                .append("       ( ")
                .append("           from_company IS NULL OR ")
                .append("           from_company.number_sx = '' OR ")
                .append("           from_company.number_sx IS NULL ")
                .append("       ) ");

        StringBuffer atrGroup = new StringBuffer()
                .append("GROUP BY claims.status, claims.type ");

        if (inEvidence != null) {
            atrWhere.append(" AND claims.in_evidence = " + inEvidence + " ");
        }
        if (withContinuation != null) {
            atrWhere = this.addWithContinuationFilter(withContinuation,atrWhere);
        }

        StringBuffer atrSql = new StringBuffer()
                .append(atrSelectFrom)
                .append(atrLeftJoin)
                .append(atrWhere)
                .append(atrGroup);
        System.out.println("[DASHBOARD - QUERY WAITING FOR NUMB. SX] "+atrSql);

        Query query = em.createNativeQuery(atrSql.toString());
        List<Object[]> resultList = query.getResultList();

        return resultList;
    }

    public List<Object[]> getStatsClaimsWaitingForAuthority(Boolean inEvidence, Boolean withContinuation) {
        StringBuffer atrSql = new StringBuffer();
        StringBuffer atrSelectFrom = new StringBuffer()
                .append("SELECT claims.status, claims.type, COUNT(DISTINCT claims.id) AS counter ")
                .append("FROM claims_new claims ");
        StringBuffer atrInJoin = new StringBuffer()
                .append("INNER JOIN ")
                .append("( ")
                .append("  SELECT cta.claims_id AS cid FROM  claims_to_claims_authority cta ")
                .append("  INNER JOIN claims_authority ca ON cta.authority_id = ca.id ")
                .append("  WHERE (ca.is_invoiced is null or ca.is_invoiced <> 'true') AND (ca.event_type is null or ca.event_type <> 'REJECT') ")
                .append(") ")
                .append("AS authority_info  ")
                .append("ON authority_info.cid = claims.id ");
        StringBuffer atrWhere = new StringBuffer()
                .append("WHERE ")
                .append("( ")
                .append(   "(")
                .append(      "claims.type = 'FUL' ")
                .append(      "AND claims.type_accident IN ('RC_ATTIVA', 'RC_CONCORSUALE', 'CARD_ATTIVA_FIRMA_SINGOLA', 'CARD_CONCORSUALE_FIRMA_SINGOLA', 'CARD_ATTIVA_DOPPIA_FIRMA', 'CARD_CONCORSUALE_DOPPIA_FIRMA') ")
                .append(   ") ")
                .append(  "OR  claims.type IN ('FNI', 'FCM') ")
                .append(")")
                .append("AND claims.status = 'WAITING_FOR_AUTHORITY' ");
        StringBuffer atrGroup = new StringBuffer()
                .append("GROUP BY claims.status, claims.type ");

        if(inEvidence != null){
            atrWhere.append(" AND claims.in_evidence = " + inEvidence + " ");
        }
        if (withContinuation != null) {
            atrWhere = this.addWithContinuationFilter(withContinuation,atrWhere);
        }

        atrSql
                .append(atrSelectFrom)
                .append(atrInJoin)
                .append(atrWhere)
                .append(atrGroup);
        System.out.println("[DASHBOARD - QUERY WAITING FOR AUTHORITY] "+atrSql);

        Query query = em.createNativeQuery(atrSql.toString());
        List<Object[]> resultList = query.getResultList();

        return resultList;
    }

    //VARIAZIONE PO
    public List<Object[]> getStatsClaimsPoVariation(Boolean inEvidence, Boolean withContinuation) {
        StringBuffer atrSelectFrom = new StringBuffer()
                .append("SELECT claims.status, claims.type, COUNT(*) ")
                .append("FROM claims_new claims ");
        StringBuffer atrWhere = new StringBuffer()
                .append("WHERE ")
                .append("       claims.po_variation = true ")
                .append("       AND ( ")
                .append("               ( ")
                .append("                   claims.type = 'FUL' ")
                .append("                   AND ")
                .append("                   claims.type_accident IN ('RC_ATTIVA', 'RC_CONCORSUALE', 'CARD_ATTIVA_FIRMA_SINGOLA', 'CARD_CONCORSUALE_FIRMA_SINGOLA', 'CARD_ATTIVA_DOPPIA_FIRMA', 'CARD_CONCORSUALE_DOPPIA_FIRMA') ")
                .append("               ) ")
                .append("               OR claims.type IN ('FNI', 'FCM') ")
                .append("           ) ");

        StringBuffer atrGroup = new StringBuffer()
                .append("GROUP BY claims.status, claims.type ");
        if (inEvidence != null) {
            atrWhere.append(" AND claims.in_evidence = " + inEvidence + " ");
        }
        if (withContinuation != null) {
            atrWhere = this.addWithContinuationFilter(withContinuation,atrWhere);
        }
        StringBuffer atrSql = new StringBuffer()
                .append(atrSelectFrom)
                .append(atrWhere)
                .append(atrGroup);
        System.out.println("[DASHBOARD - QUERY PO VARIATION] "+atrSql);

        Query query = em.createNativeQuery(atrSql.toString());
        List<Object[]> resultList = query.getResultList();
        return resultList;
    }

    public List<Object[]> getClaimsStatsTotalCounter(Boolean inEvidence, Boolean withContinuation) {
        StringBuffer atrSelectFrom = new StringBuffer()
                .append("SELECT claims.type as flow , COUNT(*) as cont ")
                .append("FROM claims_new claims ");
        StringBuffer atrWhere = new StringBuffer()
                .append("WHERE ")
                .append("       claims.type in ( 'FUL' ,'FCM', 'FNI','NO') ");

        StringBuffer atrGroup = new StringBuffer()
                .append("GROUP BY  claims.type ORDER BY claims.type ");

        if (inEvidence != null) {
            atrWhere.append(" AND claims.in_evidence = " + inEvidence + " ");
        }
        if (withContinuation != null) {
            atrWhere = this.addWithContinuationFilter(withContinuation,atrWhere);
        }
        StringBuffer atrSql = new StringBuffer()
                .append(atrSelectFrom)
                .append(atrWhere)
                .append(atrGroup);
        System.out.println("[DASHBOARD - QUERY TOTAL COUNT] "+atrSql);


        Query query = em.createNativeQuery(atrSql.toString());
        List<Object[]> resultList = query.getResultList();
        return resultList;
    }

    /*
     * se withContinuation è
     *      true =>
     *          devo prendere tutti i record che hanno almeno un record in
     *          anti_theft_request il cui campo validate è a false
     *      false =>
     *          devo prendere tutti i record che non hanno nessun record
     *          in anti_theft_request con validate a false
     */
    private StringBuffer addWithContinuationFilter(Boolean withContinuation, StringBuffer where) {

        if( withContinuation ) {
            where
                    .append(" AND ( ")
                    .append("   claims.with_continuation = true")
                    .append(" ) ");
        } else {
            where
                    .append(" AND claims.with_continuation = false ");
        }
        return where;
    }

    public List<ClaimsNewEntity> exportClaimsByPages(ClaimsExportFilter filter, Integer page, Integer pageSize) {

        Map<String, String> mappOrder = new HashMap<>();
        mappOrder.put("type", " claims.type ");
        mappOrder.put("practice_id", " claims.practice_id ");
        mappOrder.put("date_accident", " claims.date_accident ");
        mappOrder.put("plate", " claims.plate ");
        mappOrder.put("created_at", " claims.created_at ");
        mappOrder.put("type_accident", " claims.type_accident ");
        mappOrder.put("locator", " claims.claim_locator ");

        StringBuilder sql = new StringBuilder()
                .append("SELECT * FROM claims_new claims ");
        StringBuilder whereCondition = new StringBuilder()
                .append("WHERE 1=1 ");

        //DATA INCIDENTE
        if (filter.getDateAccidentFrom() != null && filter.getDateAccidentTo() != null) {
            LocalDateTime localDateTimeFrom = DateAdapter.adptDateToLocalDataTime(DateUtil.convertIS08601StringToUTCDate(filter.getDateAccidentFrom()));
            LocalDateTime localDateTimeTo = DateAdapter.adptDateToLocalDataTime(DateUtil.convertIS08601StringToUTCDate(filter.getDateAccidentTo()));
            if (localDateTimeTo.isBefore(localDateTimeFrom)) {

                throw new BadParametersException("the date_accident_to is less than the date_accident_from", MessageCode.CLAIMS_1011);
            }
            else {
                whereCondition.append(" AND (claims.date_accident\\:\\:timestamp BETWEEN '" + filter.getDateAccidentFrom() + "'\\:\\:timestamp AND '" + filter.getDateAccidentTo() + "'\\:\\:timestamp ) ");
            }
        }
        else if (filter.getDateAccidentFrom() != null) {
            whereCondition.append(" AND (claims.date_accident\\:\\:timestamp >= '" + filter.getDateAccidentFrom() +"'\\:\\:timestamp) ");
        }
        else if(filter.getDateAccidentTo() != null) {
            whereCondition.append(" AND (claims.date_accident\\:\\:timestamp <= '" + filter.getDateAccidentTo() +"'\\:\\:timestamp) ");
        }

        if( filter.getClientIdList() != null && !filter.getClientIdList().isEmpty() ) {
            sql.append(" inner join claims_damaged_customer cu on claims.id = cu.id ");
            whereCondition.append(" AND cu.customer_id IN (:clientIdList) ");
        }

        //DATA CREAZIONE
        if ( filter.getDateInsertFrom() != null && filter.getDateInsertTo() != null) {
            LocalDateTime localDateTimeFrom = DateAdapter.adptDateToLocalDataTime(DateUtil.convertIS08601StringToUTCDate( filter.getDateInsertFrom() ));
            LocalDateTime localDateTimeTo = DateAdapter.adptDateToLocalDataTime(DateUtil.convertIS08601StringToUTCDate( filter.getDateInsertTo()));
            if (localDateTimeTo.isBefore(localDateTimeFrom)) {

                throw new BadParametersException("the date_insert_to is less than the date_insert_from", MessageCode.CLAIMS_1011);
            }
            else {
                whereCondition.append(" AND (claims.created_at\\:\\:timestamp BETWEEN '" + filter.getDateInsertFrom() + "'\\:\\:timestamp AND '" + filter.getDateInsertTo() + "'\\:\\:timestamp ) ");
            }
        }
        else if ( filter.getDateInsertFrom() != null) {
            whereCondition.append(" AND (claims.created_at\\:\\:timestamp >= '" + filter.getDateInsertFrom() +"'\\:\\:timestamp) ");
        }
        else if( filter.getDateInsertTo() != null) {
            whereCondition.append(" AND (claims.created_at\\:\\:timestamp <= '" + filter.getDateInsertTo() +"'\\:\\:timestamp) ");
        }

        if( filter.getDateEntrustedFrom() != null || filter.getDateEntrustedTo() != null || filter.getEntrustedType() != null || filter.getEntrustedId() != null) {
            sql.append(" inner join claims_entrusted ce on claims.id = ce.id ");
            //DATA AFFIDAZIONE
            if ( filter.getDateEntrustedFrom() != null && filter.getDateEntrustedTo() != null) {
                LocalDateTime localDateTimeFrom = DateAdapter.adptDateToLocalDataTime(DateUtil.convertIS08601StringToUTCDate(filter.getDateEntrustedFrom()));
                LocalDateTime localDateTimeTo = DateAdapter.adptDateToLocalDataTime(DateUtil.convertIS08601StringToUTCDate(filter.getDateEntrustedTo()));
                if (localDateTimeTo.isBefore(localDateTimeFrom)) {

                    throw new BadParametersException("the date_Entrusted_to is less than the date_Entrusted_from", MessageCode.CLAIMS_1011);
                }
                else {
                    whereCondition.append(" AND (ce.entrusted_day\\:\\:timestamp BETWEEN '" + filter.getDateEntrustedFrom() + "'\\:\\:timestamp AND '" + filter.getDateEntrustedTo() + "'\\:\\:timestamp ) ");
                }
            }
            else if ( filter.getDateEntrustedFrom() != null) {
                whereCondition.append(" AND (ce.entrusted_day\\:\\:timestamp >= '" + filter.getDateEntrustedFrom() +"'\\:\\:timestamp) ");
            }
            else if( filter.getDateEntrustedTo() != null) {
                whereCondition.append(" AND (ce.entrusted_day\\:\\:timestamp <= '" + filter.getDateEntrustedTo() +"'\\:\\:timestamp) ");
            }

            //AFFIDATARIO
            if( filter.getEntrustedType() != null && filter.getEntrustedId() != null) {
                whereCondition.append(" AND (ce.type = '" + filter.getEntrustedType().getValue() + "' AND ce.id_entrusted_to = '" + filter.getEntrustedId() + "') ");
            }
            else if(filter.getEntrustedType() != null) {
                whereCondition.append(" AND (ce.type = '" + filter.getEntrustedType().getValue() + "' ) ");
            }
            else if(filter.getEntrustedId() != null) {
                whereCondition.append(" AND (ce.id_entrusted_to = '" + filter.getEntrustedId() + "' ) ");
            }
        }

        //ID PRATICA
        if (filter.getPracticeIdFrom() != null && filter.getPracticeIdTo() != null) {
            if (filter.getPracticeIdFrom().intValue() > filter.getPracticeIdTo().intValue()) {
                throw new BadParametersException("the practice_id_to is less than the practice_id_from", MessageCode.CLAIMS_1011);
            }
            else {
                whereCondition.append(" AND (claims.practice_id >= '" + filter.getPracticeIdFrom() + "' AND claims.practice_id <= '" + filter.getPracticeIdTo() + "' )");
            }
        }
        else if (filter.getPracticeIdFrom() != null) {
            whereCondition.append(" AND (claims.practice_id >= '" + filter.getPracticeIdFrom() + "' )");
        }
        else if( filter.getPracticeIdTo() != null) {
            whereCondition.append(" AND (claims.practice_id <= '" + filter.getPracticeIdTo() + "' )");
        }

        //TIPOLOGIA INCIDENTE
        if (filter.getTypeAccident() != null) {
            whereCondition.append(" AND claims.type_accident IN (:typeAccidentSet) ");
        }
        //STATO SINISTRO
        if (filter.getStatus() != null) {
            whereCondition.append(" AND claims.status IN (:statusSet) ");
        }

        //FLUSSO
        if (filter.getFlow() != null) {
            whereCondition.append(" AND claims.type IN (:flowSet) ");
        }

        //RECUPERABILITA'
        if( filter.getRecoverability() != null) {
            whereCondition.append(" AND (claims.recoverability = '" + filter.getRecoverability() + "' )");
        }

        //CONTROPARTE
        if( filter.getCounterparty() != null) {
            whereCondition.append(" AND (claims.is_with_counterparty = " + filter.getCounterparty() + " )");
        }

        //DATA DEFINIZIONE
        if(filter.getDateDefinitionFrom() != null || filter.getDateDefinitionTo() != null) {
            sql.append(" inner join claims_refund cr on claims.id = cr.id ");
            if (filter.getDateDefinitionFrom() != null && filter.getDateDefinitionTo() != null) {
                LocalDateTime localDateTimeFrom = DateAdapter.adptDateToLocalDataTime(DateUtil.convertIS08601StringToUTCDate(filter.getDateDefinitionFrom()));
                LocalDateTime localDateTimeTo = DateAdapter.adptDateToLocalDataTime(DateUtil.convertIS08601StringToUTCDate(filter.getDateDefinitionTo()));
                if (localDateTimeTo.isBefore(localDateTimeFrom)) {

                    throw new BadParametersException("the date_Definition_to is less than the date_Definition_from", MessageCode.CLAIMS_1011);
                }
                else {
                    whereCondition.append(" AND (cr.definition_date\\:\\:timestamp BETWEEN '" + filter.getDateDefinitionFrom() + "'\\:\\:timestamp AND '" + filter.getDateDefinitionTo() + "'\\:\\:timestamp ) ");
                }
            }
            else if (filter.getDateDefinitionFrom() != null) {
                whereCondition.append(" AND (cr.definition_date\\:\\:timestamp >= '" + filter.getDateDefinitionFrom() +"'\\:\\:timestamp) ");
            }
            else if(filter.getDateDefinitionTo() != null) {
                whereCondition.append(" AND (cr.definition_date\\:\\:timestamp <= '" + filter.getDateDefinitionTo() +"'\\:\\:timestamp) ");
            }
        }

        sql.append(whereCondition);

        String fieldToOrder = null;
        if (filter.getOrderBy() != null) {
            fieldToOrder = mappOrder.get(filter.getOrderBy());
        }
        if (fieldToOrder != null && filter.getAsc() != null) {
            if (filter.getAsc()) {
                sql.append(" ORDER BY " + fieldToOrder + " ASC");
            }
            else {
                sql.append(" ORDER BY " + fieldToOrder + " DESC");
            }
        }
        else if (fieldToOrder != null) {
            sql.append(" ORDER BY " + fieldToOrder + " DESC");
        }

        //LIMIT
        sql.append(" LIMIT " + pageSize + " OFFSET " + (page - 1) * pageSize);

        Query query = em.createNativeQuery(sql.toString(), ClaimsNewEntity.class);
        if (filter.getTypeAccident() != null) {
            query.setParameter("typeAccidentSet", DataAccidentTypeAccidentEnum.getStringListFromTypeAccidentList(filter.getTypeAccident()));
        }
        if (filter.getFlow() != null) {
            query.setParameter("flowSet", ClaimsFlowEnum.getStringListFromFlowList(filter.getFlow()));
        }
        if (filter.getStatus() != null) {
            query.setParameter("statusSet", ClaimsStatusEnum.getStringListFromStatusList(filter.getStatus()));
        }
        if (filter.getClientIdList() != null) {
            query.setParameter("clientIdList", filter.getClientIdList());
        }

        return query.getResultList();
    }

    public BigInteger countExportClaims(ClaimsExportFilter filter) {
        StringBuilder sql = new StringBuilder("SELECT COUNT(*) FROM claims_new claims ");
        StringBuilder whereCondition = new StringBuilder(" WHERE 1=1 ");

        //DATA INCIDENTE
        if (filter.getDateAccidentFrom() != null && filter.getDateAccidentTo() != null) {
            LocalDateTime localDateTimeFrom = DateAdapter.adptDateToLocalDataTime(DateUtil.convertIS08601StringToUTCDate(filter.getDateAccidentFrom()));
            LocalDateTime localDateTimeTo = DateAdapter.adptDateToLocalDataTime(DateUtil.convertIS08601StringToUTCDate(filter.getDateAccidentTo()));
            if (localDateTimeTo.isBefore(localDateTimeFrom)) {

                throw new BadParametersException("the date_accident_to is less than the date_accident_from", MessageCode.CLAIMS_1011);
            }
            else {
                whereCondition.append(" AND (claims.date_accident\\:\\:timestamp BETWEEN '" + filter.getDateAccidentFrom() + "'\\:\\:timestamp AND '" + filter.getDateAccidentTo() + "'\\:\\:timestamp ) ");
            }
        }
        else if (filter.getDateAccidentFrom() != null) {
            whereCondition.append(" AND (claims.date_accident\\:\\:timestamp >= '" + filter.getDateAccidentFrom() +"'\\:\\:timestamp) ");
        }
        else if(filter.getDateAccidentTo() != null){
            whereCondition.append(" AND (claims.date_accident\\:\\:timestamp <= '" + filter.getDateAccidentTo() +"'\\:\\:timestamp) ");
        }

        if((filter.getClientIdList() != null && !filter.getClientIdList().isEmpty())) {
            sql.append(" inner join claims_damaged_customer cu on claims.id = cu.id ");
            whereCondition.append(" AND cu.customer_id IN (:clientIdList) ");
        }

        //DATA CREAZIONE
        if (filter.getDateInsertFrom() != null && filter.getDateInsertTo() != null) {
            LocalDateTime localDateTimeFrom = DateAdapter.adptDateToLocalDataTime(DateUtil.convertIS08601StringToUTCDate(filter.getDateInsertFrom()));
            LocalDateTime localDateTimeTo = DateAdapter.adptDateToLocalDataTime(DateUtil.convertIS08601StringToUTCDate(filter.getDateInsertTo()));
            if (localDateTimeTo.isBefore(localDateTimeFrom)) {

                throw new BadParametersException("the date_insert_to is less than the date_insert_from", MessageCode.CLAIMS_1011);
            }
            else {
                whereCondition.append(" AND (claims.created_at\\:\\:timestamp BETWEEN '" + filter.getDateInsertFrom() + "'\\:\\:timestamp AND '" + filter.getDateInsertTo() + "'\\:\\:timestamp ) ");
            }
        }
        else if (filter.getDateInsertFrom() != null) {
            whereCondition.append(" AND (claims.created_at\\:\\:timestamp >= '" + filter.getDateInsertFrom() +"'\\:\\:timestamp) ");
        }
        else if( filter.getDateInsertTo() != null) {
            whereCondition.append(" AND (claims.created_at\\:\\:timestamp <= '" + filter.getDateInsertTo() +"'\\:\\:timestamp) ");
        }

        if(filter.getDateEntrustedFrom() != null || filter.getDateEntrustedTo() != null || filter.getEntrustedType() != null || filter.getEntrustedId() != null) {
            sql.append(" inner join claims_entrusted ce on claims.id = ce.id ");
            //DATA AFFIDAZIONE
            if (filter.getDateEntrustedFrom() != null && filter.getDateEntrustedTo() != null) {
                LocalDateTime localDateTimeFrom = DateAdapter.adptDateToLocalDataTime(DateUtil.convertIS08601StringToUTCDate(filter.getDateEntrustedFrom()));
                LocalDateTime localDateTimeTo = DateAdapter.adptDateToLocalDataTime(DateUtil.convertIS08601StringToUTCDate(filter.getDateEntrustedTo()));
                if (localDateTimeTo.isBefore(localDateTimeFrom)) {

                    throw new BadParametersException("the date_Entrusted_to is less than the date_Entrusted_from", MessageCode.CLAIMS_1011);
                }
                else {
                    whereCondition.append(" AND (ce.entrusted_day\\:\\:timestamp BETWEEN '" + filter.getDateEntrustedFrom() + "'\\:\\:timestamp AND '" + filter.getDateEntrustedTo() + "'\\:\\:timestamp ) ");
                }
            }
            else if (filter.getDateEntrustedFrom() != null) {
                whereCondition.append(" AND (ce.entrusted_day\\:\\:timestamp >= '" + filter.getDateEntrustedFrom() +"'\\:\\:timestamp) ");
            }
            else if(filter.getDateEntrustedTo() != null) {
                whereCondition.append(" AND (ce.entrusted_day\\:\\:timestamp <= '" + filter.getDateEntrustedTo() +"'\\:\\:timestamp) ");
            }

            //AFFIDATARIO
            if(filter.getEntrustedType() != null && filter.getEntrustedId() != null) {
                whereCondition.append(" AND (ce.type = '" + filter.getEntrustedType().getValue() + "' AND ce.id_entrusted_to = '" + filter.getEntrustedId() + "') ");
            }
            else if(filter.getEntrustedType() != null) {
                whereCondition.append(" AND (ce.type = '" + filter.getEntrustedType().getValue() + "' ) ");
            }
            else if(filter.getEntrustedId() != null) {
                whereCondition.append(" AND (ce.id_entrusted_to = '" + filter.getEntrustedId() + "' ) ");
            }
        }

        //ID PRATICA
        if (filter.getPracticeIdFrom() != null && filter.getPracticeIdTo() != null) {
            if (filter.getPracticeIdFrom().intValue() > filter.getPracticeIdTo().intValue()) {
                throw new BadParametersException("the practice_id_to is less than the practice_id_from", MessageCode.CLAIMS_1011);
            }
            else {
                whereCondition.append(" AND (claims.practice_id >= '" + filter.getPracticeIdFrom() + "' AND claims.practice_id <= '" + filter.getPracticeIdTo() + "' )");
            }
        }
        else if ( filter.getPracticeIdFrom() != null) {
            whereCondition.append(" AND (claims.practice_id >= '" + filter.getPracticeIdFrom() + "' )");
        }
        else if ( filter.getPracticeIdTo() != null) {
            whereCondition.append(" AND (claims.practice_id <= '" + filter.getPracticeIdTo() + "' )");
        }

        //TIPOLOGIA INCIDENTE
        if (filter.getTypeAccident() != null) {
            whereCondition.append(" AND claims.type_accident IN (:typeAccidentSet) ");
        }

        //STATO SINISTRO
        if (filter.getStatus() != null) {
            whereCondition.append(" AND claims.status IN (:statusSet) ");
        }

        //FLUSSO
        if (filter.getFlow() != null) {
            whereCondition.append(" AND claims.type IN (:flowSet) ");
        }

        //RECUPERABILITA'
        if( filter.getRecoverability() != null) {
            whereCondition.append(" AND (claims.recoverability = '" + filter.getRecoverability() + "' )");
        }

        //CONTROPARTE
        if( filter.getCounterparty() != null) {
            whereCondition.append(" AND (claims.is_with_counterparty = " + filter.getCounterparty() + " )");
        }

        //DATA DEFINIZIONE
        if( filter.getDateDefinitionFrom() != null || filter.getDateDefinitionTo() != null) {
            sql.append(" inner join claims_refund cr on claims.id = cr.id ");
            if ( filter.getDateDefinitionFrom() != null && filter.getDateDefinitionTo() != null) {
                LocalDateTime localDateTimeFrom = DateAdapter.adptDateToLocalDataTime(DateUtil.convertIS08601StringToUTCDate( filter.getDateDefinitionFrom() ));
                LocalDateTime localDateTimeTo = DateAdapter.adptDateToLocalDataTime(DateUtil.convertIS08601StringToUTCDate( filter.getDateDefinitionTo() ));
                if (localDateTimeTo.isBefore(localDateTimeFrom)) {

                    throw new BadParametersException("the date_Definition_to is less than the date_Definition_from", MessageCode.CLAIMS_1011);
                }
                else {
                    whereCondition.append(" AND (cr.definition_date\\:\\:timestamp BETWEEN '" + filter.getDateDefinitionFrom() + "'\\:\\:timestamp AND '" + filter.getDateDefinitionTo() + "'\\:\\:timestamp ) ");
                }
            }
            else if ( filter.getDateDefinitionFrom() != null) {
                whereCondition.append(" AND (cr.definition_date\\:\\:timestamp >= '" + filter.getDateDefinitionFrom() +"'\\:\\:timestamp) ");
            }
            else if( filter.getDateDefinitionTo() != null) {
                whereCondition.append(" AND (cr.definition_date\\:\\:timestamp <= '" + filter.getDateDefinitionTo() +"'\\:\\:timestamp) ");
            }
        }

        sql.append(whereCondition);

        Query query = em.createNativeQuery(sql.toString());
        if ( filter.getTypeAccident() != null) {
            query.setParameter("typeAccidentSet", DataAccidentTypeAccidentEnum.getStringListFromTypeAccidentList( filter.getTypeAccident() ));
        }
        if ( filter.getFlow() != null) {
            query.setParameter("flowSet", ClaimsFlowEnum.getStringListFromFlowList( filter.getFlow() ));
        }
        if ( filter.getStatus() != null) {
            query.setParameter("statusSet", ClaimsStatusEnum.getStringListFromStatusList( filter.getStatus() ));
        }
        if ( filter.getClientIdList() != null) {
            query.setParameter("clientIdList", filter.getClientIdList());
        }
        return (BigInteger) query.getSingleResult();
    }

}
