package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.entity.settings.ExportSupplierCodeEntity;
import com.doing.nemo.claims.entity.settings.ExportSupplierCodeEntity_;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class ExportSupplierCodeRepositoryV1 {
    @PersistenceContext
    @Autowired
    private EntityManager em;

    public List<ExportSupplierCodeEntity> findExportSupplierCode(Integer page, Integer pageSize, String orderBy, Boolean asc, String vatNumber, String denomination, String codeToExport, Boolean isActive) {
        Map<String, String> mappOrder = new HashMap<>();
        mappOrder.put("vat_number", "vatNumber");
        mappOrder.put("denomination", "denomination");
        mappOrder.put("code_to_export", "codeToExport");
        mappOrder.put("is_active", "isActive");
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<ExportSupplierCodeEntity> exportSupplierCodeCriteriaQuery = criteriaBuilder.createQuery(ExportSupplierCodeEntity.class);
        Root<ExportSupplierCodeEntity> exportSupplierCodeRoot = exportSupplierCodeCriteriaQuery.from(ExportSupplierCodeEntity.class);
        exportSupplierCodeCriteriaQuery.select(exportSupplierCodeRoot);
        //Constructing list of parameters
        List<Predicate> predicates = new ArrayList<>();

        if (vatNumber != null && !vatNumber.isEmpty()) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    exportSupplierCodeRoot.get(ExportSupplierCodeEntity_.vatNumber)), "%" + vatNumber.toUpperCase() + "%"));
        }

        if (denomination != null && !denomination.isEmpty()) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    exportSupplierCodeRoot.get(ExportSupplierCodeEntity_.denomination)), "%" + denomination.toUpperCase() + "%"));
        }

        if (codeToExport != null && !codeToExport.isEmpty()) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    exportSupplierCodeRoot.get(ExportSupplierCodeEntity_.codeToExport)), "%" + codeToExport.toUpperCase() + "%"));
        }

        if (isActive != null) {
            predicates.add(criteriaBuilder.equal(
                    exportSupplierCodeRoot.get(ExportSupplierCodeEntity_.isActive), isActive));
        }

        exportSupplierCodeCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));

        String fieldToOrder = null;
        if (orderBy != null)
            fieldToOrder = mappOrder.get(orderBy);
        if (fieldToOrder != null && asc != null) {
            if (asc) {
                exportSupplierCodeCriteriaQuery.orderBy(criteriaBuilder.asc(exportSupplierCodeRoot.get(fieldToOrder)));
            } else {
                exportSupplierCodeCriteriaQuery.orderBy(criteriaBuilder.desc(exportSupplierCodeRoot.get(fieldToOrder)));
            }

        } else if (fieldToOrder != null) {
            exportSupplierCodeCriteriaQuery.orderBy(criteriaBuilder.desc(exportSupplierCodeRoot.get(fieldToOrder)));
        }

        List<ExportSupplierCodeEntity> exportSupplierCodeEntityList = em.createQuery(exportSupplierCodeCriteriaQuery).setFirstResult((page - 1) * pageSize).setMaxResults(pageSize).getResultList();
        return exportSupplierCodeEntityList;
    }

    public Long countPaginationExportSupplierCode(String vatNumber, String denomination, String codeToExport, Boolean isActive) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<Long> exportSupplierCodeCriteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<ExportSupplierCodeEntity> exportSupplierCodeRoot = exportSupplierCodeCriteriaQuery.from(ExportSupplierCodeEntity.class);
        exportSupplierCodeCriteriaQuery.select(criteriaBuilder.count(exportSupplierCodeRoot));

        //Constructing list of parameters
        List<Predicate> predicates = new ArrayList<>();

        if (vatNumber != null && !vatNumber.isEmpty()) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    exportSupplierCodeRoot.get(ExportSupplierCodeEntity_.vatNumber)), "%" + vatNumber.toUpperCase() + "%"));
        }

        if (denomination != null && !denomination.isEmpty()) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    exportSupplierCodeRoot.get(ExportSupplierCodeEntity_.denomination)), "%" + denomination.toUpperCase() + "%"));
        }

        if (codeToExport != null && !codeToExport.isEmpty()) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    exportSupplierCodeRoot.get(ExportSupplierCodeEntity_.codeToExport)), "%" + codeToExport.toUpperCase() + "%"));
        }

        if (isActive != null) {
            predicates.add(criteriaBuilder.equal(
                    exportSupplierCodeRoot.get(ExportSupplierCodeEntity_.isActive), isActive));
        }

        exportSupplierCodeCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));
        Query sql = em.createQuery(exportSupplierCodeCriteriaQuery);
        return (Long) sql.getSingleResult();
    }
}
