package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.entity.settings.InsuranceManagerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface InsuranceManagerRepository extends JpaRepository<InsuranceManagerEntity, UUID> {

    @Query("SELECT i FROM InsuranceManagerEntity i WHERE i.name = :name")
    InsuranceManagerEntity searchInsuranceManagerbyName(@Param("name") String name);

    @Query("SELECT i FROM InsuranceManagerEntity i WHERE i.rifCode = :code")
    InsuranceManagerEntity searchManagerByCode(@Param("code") long code);

    @Query("SELECT i FROM InsuranceManagerEntity i WHERE i.name = :name AND i.id <> :id")
    InsuranceManagerEntity searchInsuranceManagerbyNameWithDifferentId(@Param("name") String name, @Param("id") UUID id);

    @Query("SELECT i FROM InsurancePolicyPersonalDataEntity i INNER JOIN i.insuranceManagerEntity ins WHERE ins.id = :managerId")
    InsuranceManagerEntity searchInsurancePolicyForeignKey(@Param("managerId") UUID id);
}
