package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.common.repository.specification.ExampleSpecification;
import com.doing.nemo.claims.entity.view.SearchDashboardClaimsView;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.query.QueryUtils;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import java.time.Instant;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.springframework.data.domain.ExampleMatcher.GenericPropertyMatchers.ignoreCase;


@Repository
public class SearchDashboardClaimsViewManager {

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private SearchDashboardClaimsViewRepository searchDashboardClaimsViewRepository;


    public static Specification<SearchDashboardClaimsView> distinct() {
        return (root, query, cb) -> {
            query.distinct(true);
            return null;
        };
    }


 /*   *//**//*//////////
    private Specification<SearchDashboardClaimsView> getDossiersByStatusesSearchFiltersAndDeletedAt(
            SearchDashboardClaimsView entity,
            boolean isSupervisor,
            boolean includeDeleted,
            List<WorkingStatus> workingStatuses,
            List<DossierStatus> dossierStatuses) {

        Specification<SearchDashboardClaimsView> specificationByWorkingStatuses = this.getSpecificationByGroupedDefaultWorkingStatus(isSupervisor);
        Specification<SearchDashboardClaimsView> specificationByDossierStatuses = this.getSpecificationByGroupedDefaultDossierStatus(isSupervisor);
        Specification<SearchDashboardClaimsView> specificationByDeletedAtNull = null;

        if(entity != null) {

            if (CollectionUtils.isNotEmpty(dossierStatuses)) {
                specificationByDossierStatuses = this.getSpecificationByGroupedDossierStatus(new HashSet<>(dossierStatuses));
                entity.setStatus(null);
            }

            if (CollectionUtils.isNotEmpty(workingStatuses)) {
                specificationByWorkingStatuses = this.getSpecificationByGroupedStatus(new HashSet<>(workingStatuses));
                entity.setWorkingStatus(null);
            }
        }

        if(!includeDeleted){

            specificationByDeletedAtNull = this.getSpecificationByDossierDeletedAtNull();
        }

        Specification<SearchDashboardClaimsView> specificationByExample = this.getSpecificationByExample(entity);


        return Specification.where(distinct())
                .and(specificationByExample)
                .and(specificationByDeletedAtNull)
                .and(specificationByWorkingStatuses)
                .and(specificationByDossierStatuses)
                ;
    }
 *//**//*//////////
    private Specification<SearchDashboardClaimsView> getSpecificationByGroupedDefaultDossierStatus(boolean isSupervisor){

        Set<DossierStatus> defaultDossierStatus = isSupervisor
                ? DossierStatus.supervisorConsultationDefaultStatus()
                : DossierStatus.authorityConsultationDefaultStatus();

        return (root, query, cb) -> {
            final Path<DossierStatus> group = root.get(SearchDossierWorkingCompleteView_.status);
            return group.in(defaultDossierStatus);
        };
    }
 *//**//*//////////
    private Specification<SearchDashboardClaimsView> getSpecificationByGroupedDefaultWorkingStatus(boolean isSupervisor) {

        Set<WorkingStatus> defaultWorkingStatus = isSupervisor
                ? WorkingStatus.supervisorConsultationDefaultStatus()
                : WorkingStatus.authorityConsultationDefaultStatus();

        return (root, query, cb) -> {
            Path<WorkingStatus> values = root.get(SearchDossierWorkingCompleteView_.workingStatus);
            return cb.or(values.isNull(), values.in(defaultWorkingStatus));
        };
    }



*//**//*//////////
    public Page<SearchDashboardClaimsView> findBySearchFiltersAndDeletedAtListQuery(
            SearchDashboardClaimsView entity,
            boolean isSupervisor,
            boolean includeDeleted,
            List<WorkingStatus> workingStatuses,
            List<DossierStatus> dossierStatuses,
            Pageable pageable) {

        Specification<SearchDashboardClaimsView> specificationQuery = this.getDossiersByStatusesSearchFiltersAndDeletedAt(
                entity, isSupervisor, includeDeleted, workingStatuses, dossierStatuses);

        Integer page = pageable.getPageNumber();
        Integer pageSize = pageable.getPageSize();
        Sort sort = pageable.getSort();

        CriteriaQuery<SearchDashboardClaimsView> criteriaSelectQuery = this.buildDossierSelectQuery(specificationQuery, sort);

        Query query = entityManager.createQuery(criteriaSelectQuery);
        query.setFirstResult(page * pageSize);
        query.setMaxResults(pageSize);

        List<SearchDashboardClaimsView> resultQuery = query.getResultList();

        CriteriaQuery<Long> criteriaCountQuery = this.buildDossierCountQuery(specificationQuery);
        Query countQuery = entityManager.createQuery(criteriaCountQuery);

        long count = (long) countQuery.getSingleResult();

        return new PageImpl<>(resultQuery, PageRequest.of(page, pageSize, sort), count);
    }



*//**//*//////////
    private Specification<SearchDashboardClaimsView> getSpecificationByExample(SearchDashboardClaimsView entity) {

        ExampleMatcher matcher = ExampleMatcher.matching()
                .withMatcher("workingNumberPrefix", ignoreCase())
                .withMatcher("brand", ignoreCase())
                .withMatcher("driverFirstName", ignoreCase())
                .withMatcher("driverLastName", ignoreCase())
                .withMatcher("chassis", ignoreCase())
                .withMatcher("customerTradingName", ignoreCase());

        return new ExampleSpecification<>(Example.of(entity, matcher));
    }



*//**//*//////////
    private Specification<SearchDashboardClaimsView> getSpecificationByDossierDeletedAtNull() {

        return (root, query, cb) -> {
            final Path<Instant> group = root.get(SearchDossierWorkingCompleteView_.deletedAt);
            return group.isNull();
        };
    }


*//**//*//////////
    private CriteriaQuery<SearchDashboardClaimsView> buildDossierSelectQuery(Specification<SearchDashboardClaimsView> inputSpec, Sort sort){

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<SearchDashboardClaimsView> query = cb.createQuery(SearchDashboardClaimsView.class);
        Root<SearchDashboardClaimsView> root = query.from(SearchDashboardClaimsView.class);
        query.multiselect(
                root.get(SearchDossierWorkingCompleteView_.dossierId),
                root.get(SearchDossierWorkingCompleteView_.createdAt),
                root.get(SearchDossierWorkingCompleteView_.updatedAt),
                root.get(SearchDossierWorkingCompleteView_.priority),
                root.get(SearchDossierWorkingCompleteView_.dossierNumber),
                root.get(SearchDossierWorkingCompleteView_.plate),
                root.get(SearchDossierWorkingCompleteView_.brand),
                root.get(SearchDossierWorkingCompleteView_.model),
                root.get(SearchDossierWorkingCompleteView_.dossierCommodityCenterId),
                root.get(SearchDossierWorkingCompleteView_.status),
                root.get(SearchDossierWorkingCompleteView_.chassis),
                root.get(SearchDossierWorkingCompleteView_.flowType),
                root.get(SearchDossierWorkingCompleteView_.closedAt),
                root.get(SearchDossierWorkingCompleteView_.automaticallyClosedAt),
                root.get(SearchDossierWorkingCompleteView_.contractId),
                root.get(SearchDossierWorkingCompleteView_.customerId),
                root.get(SearchDossierWorkingCompleteView_.driverId),
                root.get(SearchDossierWorkingCompleteView_.acceptingDataCommodityId),
                root.get(SearchDossierWorkingCompleteView_.customerTradingName)
        ).distinct(true);

        query.where(inputSpec.toPredicate(root,query,cb));
        query.orderBy(QueryUtils.toOrders(sort, root, cb));

        return query;
    }
*/
    private CriteriaQuery<Long> buildDossierCountQuery(Specification<SearchDashboardClaimsView> inputSpec){

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> query = cb.createQuery(Long.class);
        Root<SearchDashboardClaimsView> root = query.from(SearchDashboardClaimsView.class);
        query.select(cb.countDistinct(root));
        query.where(inputSpec.toPredicate(root,query,cb));

        return query;
    }
}
