package com.doing.nemo.claims.repository;


import com.doing.nemo.claims.entity.ClaimsAuthorityEmbeddedEntity;
import com.doing.nemo.claims.entity.ClaimsAuthorityId;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;


@Repository
public interface ClaimsAuthorityEmbeddedRepositorty extends JpaRepository<ClaimsAuthorityEmbeddedEntity, ClaimsAuthorityId> {


}
