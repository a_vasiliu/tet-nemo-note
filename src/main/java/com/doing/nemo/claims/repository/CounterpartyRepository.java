package com.doing.nemo.claims.repository;


import com.doing.nemo.claims.entity.CounterpartyEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CounterpartyRepository extends JpaRepository<CounterpartyEntity, String> {

}
