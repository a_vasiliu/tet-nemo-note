package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.entity.MilesExternalEntity;
import com.doing.nemo.claims.entity.enumerated.ExternalCommunicationEnum;
import io.lettuce.core.dynamic.annotation.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface MilesExternalRepository extends JpaRepository<MilesExternalEntity, UUID> {
    @Query ("SELECT r FROM MilesExternalEntity r WHERE r.status = :status AND r.claimsId = :claimsId")
    List<MilesExternalEntity> searchByStatus(@Param("status") ExternalCommunicationEnum status, @Param("claimsId") String claimsId);
}
