package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.enumerated.DamagedEnum.VehicleEnum.VehicleTypeEnum;
import com.doing.nemo.claims.entity.settings.LegalEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface LegalRepository extends JpaRepository<LegalEntity, UUID> {

    /* QUERY CHE CONSIDERA L'IMPORTO TOTALE PASSATO DA AUTHORITY (DA CONSIDERARE QUANDO AVREMO INTEGRATO AUTHORITY)
    @Query("SELECT l from LegalEntity l inner join l.automaticAffiliationRuleEntityList a " +
            "inner join a.claimsType c inner join a.damagedInsuranceCompanyList da " +
            "where a.monthlyVolume >  a.currentVolume AND (c.claimsAccidentType = null OR c.claimsAccidentType = :typeClaim)  " +
            "AND (a.foreignCountryClaim = null OR a.foreignCountryClaim = :isAbroad) AND (a.claimWithInjured = null OR a.claimWithInjured = :isWounded) " +
            "AND (a.damageCanNotBeRepaired = null OR a.damageCanNotBeRepaired = :isRecoverability) AND a.currentMonth = :date " +
            "AND (a.complaintsWithoutCtp = null OR a.complaintsWithoutCtp = :isCounterparty) AND da.name = :damagedCompany AND a.minImport <= :amount AND a.maxImport >= :amount")
    List<LegalEntity> getMaxVolumeLegal(@Param("isCounterparty") Boolean isCounterParty, @Param("isAbroad") Boolean isAbroad, @Param("isWounded") Boolean isWounded,
                                        @Param("isRecoverability") Boolean isRecoverability, @Param("typeClaim") DataAccidentTypeAccidentEnum typeClaim, @Param("date") String date, @Param("damagedCompany") String damagedCompany, @Param("amount") Double amount);
*/

    @Query("SELECT l from LegalEntity l inner join l.automaticAffiliationRuleEntityList a " +
            "left join a.claimsType c left join a.damagedInsuranceCompanyList da " +
            "where l.isActive = true AND (c.claimsAccidentType is null OR c.claimsAccidentType = :typeClaim)  " +
            "AND (a.foreignCountryClaim is null OR a.foreignCountryClaim = :isAbroad) AND (a.claimWithInjured is null OR a.claimWithInjured = :isWounded) " +
            "AND (a.damageCanNotBeRepaired is null OR a.damageCanNotBeRepaired = :isRecoverability) AND a.currentMonth = :date " +
            "AND (a.complaintsWithoutCtp is null OR a.complaintsWithoutCtp = :isCounterparty) " +
            "AND (a.counterpartType is null OR a.counterpartType = :counterpartyType) " +
            "AND (da.id is null OR da.id = :damagedCompany) AND (a.minImport is null OR :amount is null OR a.minImport <= :amount) AND a.monthlyVolume <> -1 AND (a.monthlyVolume > a.currentVolume) AND (a.monthlyVolume - a.currentVolume) >= all (select a.monthlyVolume - a.currentVolume from LegalEntity l inner join l.automaticAffiliationRuleEntityList a " +
            "left join a.claimsType c left join a.damagedInsuranceCompanyList da " +
            "where l.isActive = true AND (c.claimsAccidentType is null OR c.claimsAccidentType = :typeClaim)  " +
            "AND (a.foreignCountryClaim is null OR a.foreignCountryClaim = :isAbroad) AND (a.claimWithInjured is null OR a.claimWithInjured = :isWounded) " +
            "AND (a.damageCanNotBeRepaired is null OR a.damageCanNotBeRepaired = :isRecoverability) AND a.currentMonth = :date " +
            "AND (a.complaintsWithoutCtp is null OR a.complaintsWithoutCtp = :isCounterparty) " +
            "AND (a.counterpartType is null OR a.counterpartType = :counterpartyType) " +
            "AND (da.id is null OR da.id = :damagedCompany) AND a.monthlyVolume <> -1 AND (a.monthlyVolume > a.currentVolume) AND (a.minImport is null OR :amount is null OR a.minImport <= :amount) AND (a.maxImport is null OR :amount is null OR a.maxImport >= :amount)) " +
            "AND (a.maxImport is null OR :amount is null OR a.maxImport >= :amount)")
    List<LegalEntity> getMaxVolumeLegal(@Param("isCounterparty") Boolean isCounterParty, @Param("isAbroad") Boolean isAbroad, @Param("isWounded") Boolean isWounded,
                                        @Param("isRecoverability") Boolean isRecoverability, @Param("typeClaim") DataAccidentTypeAccidentEnum typeClaim, @Param("date") String date, @Param("damagedCompany") UUID damagedCompany, @Param("amount") Double amount, @Param("counterpartyType") VehicleTypeEnum counterpartyType);



    @Query("SELECT l from LegalEntity l inner join l.automaticAffiliationRuleEntityList a " +
            "left join a.claimsType c left join a.damagedInsuranceCompanyList da left join a.counterpartInsuranceCompanyList ca " +
            "where l.isActive = true AND (c.claimsAccidentType = null OR c.claimsAccidentType = :typeClaim)  " +
            "AND (a.foreignCountryClaim = null OR a.foreignCountryClaim = :isAbroad) AND (a.claimWithInjured = null OR a.claimWithInjured = :isWounded) " +
            "AND (a.damageCanNotBeRepaired = null OR a.damageCanNotBeRepaired = :isRecoverability) AND a.currentMonth = :date " +
            "AND (a.complaintsWithoutCtp = null OR a.complaintsWithoutCtp = :isCounterparty) " +
            "AND (a.counterpartType is null OR a.counterpartType = :counterpartyType) " +
            "AND (da.id = null OR da.id = :damagedCompany) AND (ca.id = null OR ca.id = :counterpartyCompany) AND (a.minImport is null OR :amount is null OR a.minImport <= :amount) AND a.monthlyVolume <> -1 AND (a.monthlyVolume > a.currentVolume) AND (a.monthlyVolume - a.currentVolume) >= all (select a.monthlyVolume - a.currentVolume from LegalEntity l inner join l.automaticAffiliationRuleEntityList a " +
            "left join a.claimsType c left join a.damagedInsuranceCompanyList da left join a.counterpartInsuranceCompanyList ca " +
            "where l.isActive = true AND (c.claimsAccidentType = null OR c.claimsAccidentType = :typeClaim)  " +
            "AND (a.foreignCountryClaim = null OR a.foreignCountryClaim = :isAbroad) AND (a.claimWithInjured = null OR a.claimWithInjured = :isWounded) " +
            "AND (a.damageCanNotBeRepaired = null OR a.damageCanNotBeRepaired = :isRecoverability) AND a.currentMonth = :date " +
            "AND (a.complaintsWithoutCtp = null OR a.complaintsWithoutCtp = :isCounterparty) " +
            "AND (a.counterpartType is null OR a.counterpartType = :counterpartyType) " +
            "AND (da.id = null OR da.id = :damagedCompany) AND (ca.id = null OR ca.id = :counterpartyCompany) AND (a.minImport is null OR :amount is null OR a.minImport <= :amount) AND  a.monthlyVolume <> -1 AND (a.monthlyVolume > a.currentVolume) AND (a.maxImport is null OR :amount is null OR a.maxImport >= :amount)) " +
            "AND (a.maxImport is null OR :amount is null OR a.maxImport >= :amount)")
    List<LegalEntity> getMaxVolumeLegalWITHCounterparty(@Param("isCounterparty") Boolean isCounterParty, @Param("isAbroad") Boolean isAbroad, @Param("isWounded") Boolean isWounded,
                                                        @Param("isRecoverability") Boolean isRecoverability, @Param("typeClaim") DataAccidentTypeAccidentEnum typeClaim, @Param("date") String date, @Param("damagedCompany") UUID damagedCompany, @Param("amount") Double amount, @Param("counterpartyCompany") UUID counterpartyCompany, @Param("counterpartyType") VehicleTypeEnum counterpartyType);




    @Query("SELECT l from LegalEntity l inner join l.automaticAffiliationRuleEntityList a " +
            "left join a.claimsType c left join a.damagedInsuranceCompanyList da left join a.counterpartInsuranceCompanyList ca " +
            "where l.isActive = true AND (c.claimsAccidentType = null OR c.claimsAccidentType = :typeClaim)  " +
            "AND (a.foreignCountryClaim = null OR a.foreignCountryClaim = :isAbroad) AND (a.claimWithInjured = null OR a.claimWithInjured = :isWounded) " +
            "AND (a.damageCanNotBeRepaired = null OR a.damageCanNotBeRepaired = :isRecoverability) AND a.currentMonth = :date " +
            "AND (a.complaintsWithoutCtp = null OR a.complaintsWithoutCtp = :isCounterparty) " +
            "AND (a.counterpartType is null OR a.counterpartType = :counterpartyType) " +
            "AND (da.id = null OR da.id = :damagedCompany) AND (ca.id = null OR ca.id = :counterpartyCompany) AND (a.minImport is null OR :amount is null OR a.minImport <= :amount)  AND (a.maxImport is null OR :amount is null OR a.maxImport >= :amount) AND a.monthlyVolume = 0")
    List<LegalEntity> getMaxVolumeLegalWITHCounterpartyAndMonthlyVolumeZero(@Param("isCounterparty") Boolean isCounterParty, @Param("isAbroad") Boolean isAbroad, @Param("isWounded") Boolean isWounded,
                                                        @Param("isRecoverability") Boolean isRecoverability, @Param("typeClaim") DataAccidentTypeAccidentEnum typeClaim, @Param("date") String date, @Param("damagedCompany") UUID damagedCompany, @Param("amount") Double amount, @Param("counterpartyCompany") UUID counterpartyCompany, @Param("counterpartyType") VehicleTypeEnum counterpartyType);


    @Query("SELECT l from LegalEntity l inner join l.automaticAffiliationRuleEntityList a " +
            "left join a.claimsType c left join a.damagedInsuranceCompanyList da " +
            "where l.isActive = true AND (c.claimsAccidentType = null OR c.claimsAccidentType = :typeClaim)  " +
            "AND (a.foreignCountryClaim = null OR a.foreignCountryClaim = :isAbroad) AND (a.claimWithInjured = null OR a.claimWithInjured = :isWounded) " +
            "AND (a.damageCanNotBeRepaired = null OR a.damageCanNotBeRepaired = :isRecoverability) AND a.currentMonth = :date " +
            "AND (a.complaintsWithoutCtp = null OR a.complaintsWithoutCtp = :isCounterparty) " +
            "AND (a.counterpartType is null OR a.counterpartType = :counterpartyType) " +
            "AND (da.id = null OR da.id = :damagedCompany) AND (a.minImport is null OR :amount is null OR a.minImport <= :amount)  AND (a.maxImport is null OR :amount is null OR a.maxImport >= :amount) AND a.monthlyVolume = 0")
    List<LegalEntity> getMaxVolumeLegalAndMonthlyVolumeZero(@Param("isCounterparty") Boolean isCounterParty, @Param("isAbroad") Boolean isAbroad, @Param("isWounded") Boolean isWounded,
                                                                            @Param("isRecoverability") Boolean isRecoverability, @Param("typeClaim") DataAccidentTypeAccidentEnum typeClaim, @Param("date") String date, @Param("damagedCompany") UUID damagedCompany, @Param("amount") Double amount, @Param("counterpartyType") VehicleTypeEnum counterpartyType);


    @Query("SELECT l FROM LegalEntity l WHERE l.name = :name")
    LegalEntity searchLegalbyName(@Param("name") String name);

    @Query("SELECT l FROM LegalEntity l WHERE l.code = :code")
    LegalEntity searchLegalbyCode(@Param("code") long code);

    @Query("SELECT l FROM LegalEntity l WHERE l.id <> :id AND l.name = :name")
    LegalEntity searchLegalWithDifferentId(@Param("id") UUID id, @Param("name") String name);

}
