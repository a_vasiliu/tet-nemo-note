package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.entity.settings.MotivationTypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.UUID;

public interface MotivationTypeRepository extends JpaRepository<MotivationTypeEntity, UUID> {

    @Query("SELECT m FROM MotivationTypeEntity m WHERE m.description = :description")
    MotivationTypeEntity searchMotivationTypebyDescription(@Param("description") String description);

    @Query("SELECT m FROM MotivationTypeEntity m WHERE m.description = :description AND m.id <> :id")
    MotivationTypeEntity searchMotivationTypebyDescriptionWithDifferentId(@Param("description") String description, @Param("id") UUID id);

    @Query("SELECT m FROM MotivationTypeEntity m ORDER BY m.orderId ASC")
    List<MotivationTypeEntity> findAllOrderedByOrderId();
}
