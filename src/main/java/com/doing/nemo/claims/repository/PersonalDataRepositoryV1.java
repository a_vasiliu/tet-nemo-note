package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.entity.settings.PersonalDataEntity;
import com.doing.nemo.claims.entity.settings.PersonalDataEntity_;
import com.doing.nemo.claims.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class PersonalDataRepositoryV1 {

    @PersistenceContext
    @Autowired
    private EntityManager em;

    @Autowired
    private Util util;
    
    public List<PersonalDataEntity> findPersonalDatasFilteredAscName(Integer page, Integer pageSize, String orderBy, Boolean asc, Long personalCode, String personalGroup, String name, String city, String province, String phoneNumber, String fax, String vatNumber, Boolean isActive, String customerId) {
        Map<String, String> mapOrder = new HashMap<>();
        mapOrder.put("personal_code", "personalCode");
        mapOrder.put("customer_id", "customerId");
        mapOrder.put("personal_group", "personalGroup");
        mapOrder.put("name", "name");
        mapOrder.put("city", "city");
        mapOrder.put("province", "province");
        mapOrder.put("phone_number", "phoneNumber");
        mapOrder.put("fax", "fax");
        mapOrder.put("vat_number", "vatNumber");
        mapOrder.put("is_active", "isActive");
        mapOrder.put("address", "address");
        mapOrder.put("zip_code", "zipCode");
        mapOrder.put("email", "email");
        mapOrder.put("pec", "pec");
        mapOrder.put("web_site", "webSite");
        mapOrder.put("contact", "contact");
        mapOrder.put("fiscal_code", "fiscalCode");
        mapOrder.put("personal_father_id", "personalFatherId");
        mapOrder.put("personal_father_name", "personalFatherName");
        mapOrder.put("created_at", "createdAt");

        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<PersonalDataEntity> personalDataCriteriaQuery = criteriaBuilder.createQuery(PersonalDataEntity.class);
        Root<PersonalDataEntity> personalDataRoot = personalDataCriteriaQuery.from(PersonalDataEntity.class);

        personalDataCriteriaQuery.select(personalDataRoot);

        //Constructing list of parameters
        List<Predicate> predicates = new ArrayList<>();

        if (personalCode != null) {
            predicates.add(criteriaBuilder.equal(
                    personalDataRoot.get(PersonalDataEntity_.personalCode), personalCode));
        }
        if (Util.isNotEmpty(personalGroup)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    personalDataRoot.get(PersonalDataEntity_.personalGroup)), "%"+personalGroup.toUpperCase()+"%"));
        }
        if (Util.isNotEmpty(name)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    personalDataRoot.get(PersonalDataEntity_.name)), "%"+name.toUpperCase()+"%"));
        }
        if (Util.isNotEmpty(city)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    personalDataRoot.get(PersonalDataEntity_.city)), "%"+city.toUpperCase()+"%"));
        }
        if (Util.isNotEmpty(province)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    personalDataRoot.get(PersonalDataEntity_.province)), "%"+province.toUpperCase()+"%"));
        }
        if (Util.isNotEmpty(phoneNumber)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    personalDataRoot.get(PersonalDataEntity_.phoneNumber)), "%"+phoneNumber.toUpperCase()+"%"));
        }
        if (Util.isNotEmpty(fax)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    personalDataRoot.get(PersonalDataEntity_.fax)), "%"+fax.toUpperCase()+"%"));
        }
        if (Util.isNotEmpty(vatNumber)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    personalDataRoot.get(PersonalDataEntity_.vatNumber)), "%"+vatNumber.toUpperCase()+"%"));
        }
        if (Util.isNotEmpty(customerId)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    personalDataRoot.get(PersonalDataEntity_.customerId)), "%"+customerId.toUpperCase()+"%"));
        }
        if (isActive != null) {
            predicates.add(criteriaBuilder.equal(
                    personalDataRoot.get(PersonalDataEntity_.isActive), isActive));
        }
        personalDataCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));
        
        //List<Order> orderByList = new LinkedList<>();
        String fieldToOrder = null;
        if(orderBy != null)
            fieldToOrder = mapOrder.get(orderBy);
        if(fieldToOrder != null && asc != null){
            if (asc) {
                personalDataCriteriaQuery.orderBy(criteriaBuilder.asc(personalDataRoot.get(fieldToOrder)));
            } else {
                personalDataCriteriaQuery.orderBy(criteriaBuilder.desc(personalDataRoot.get(fieldToOrder)));
            }

        } else if(orderBy != null) {
            personalDataCriteriaQuery.orderBy(criteriaBuilder.desc(personalDataRoot.get(fieldToOrder)));
        }

        List<PersonalDataEntity> personalDataEntityList = em.createQuery(personalDataCriteriaQuery).setFirstResult((page-1)*pageSize).setMaxResults(pageSize).getResultList();
        return personalDataEntityList;
        
    }
    
    public Long countPersonalDataFiltered(Long personalCode, String personalGroup, String name, String city, String province, String phoneNumber, String fax, String vatNumber, Boolean isActive, String customerId) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<Long> personalDataCriteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<PersonalDataEntity> personalDataRoot = personalDataCriteriaQuery.from(PersonalDataEntity.class);

        personalDataCriteriaQuery.select(criteriaBuilder.count(personalDataRoot));

        //Constructing list of parameters
        List<Predicate> predicates = new ArrayList<>();

        if (personalCode != null) {
            predicates.add(criteriaBuilder.equal(
                    personalDataRoot.get(PersonalDataEntity_.personalCode), personalCode));
        }
        if (Util.isNotEmpty(personalGroup)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    personalDataRoot.get(PersonalDataEntity_.personalGroup)), "%"+personalGroup.toUpperCase()+"%"));
        }
        if (Util.isNotEmpty(name)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    personalDataRoot.get(PersonalDataEntity_.name)), "%"+name.toUpperCase()+"%"));
        }
        if (Util.isNotEmpty(city)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    personalDataRoot.get(PersonalDataEntity_.city)), "%"+city.toUpperCase()+"%"));
        }
        if (Util.isNotEmpty(province)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    personalDataRoot.get(PersonalDataEntity_.province)), "%"+province.toUpperCase()+"%"));
        }
        if (Util.isNotEmpty(phoneNumber)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    personalDataRoot.get(PersonalDataEntity_.phoneNumber)), "%"+phoneNumber.toUpperCase()+"%"));
        }
        if (Util.isNotEmpty(fax)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    personalDataRoot.get(PersonalDataEntity_.fax)), "%"+fax.toUpperCase()+"%"));
        }
        if (Util.isNotEmpty(vatNumber)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    personalDataRoot.get(PersonalDataEntity_.vatNumber)), "%"+vatNumber.toUpperCase()+"%"));
        }
        if (Util.isNotEmpty(customerId)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    personalDataRoot.get(PersonalDataEntity_.customerId)), "%"+customerId.toUpperCase()+"%"));
        }
        if (isActive != null) {
            predicates.add(criteriaBuilder.equal(
                    personalDataRoot.get(PersonalDataEntity_.isActive), isActive));
        }

        personalDataCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));
        Query sql = em.createQuery(personalDataCriteriaQuery);
        return (Long)sql.getSingleResult();

    }

}
