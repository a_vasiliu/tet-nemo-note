package com.doing.nemo.claims.repository;


import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.enumerated.DamagedEnum.VehicleEnum.VehicleTypeEnum;
import com.doing.nemo.claims.entity.settings.AutomaticAffiliationRuleInspectorateEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

@Repository
public interface AutomaticAffiliationRuleInspectorateRepository extends JpaRepository<AutomaticAffiliationRuleInspectorateEntity, UUID> {

    @Query("select a " +
            "\t\t\tfrom InspectorateEntity i inner join i.automaticAffiliationRuleEntityList a left join a.claimsTypeEntityList c left join a.damagedInsuranceCompanyList dam \n" +
            "\t\t\twhere a.monthlyVolume >  a.currentVolume AND a.currentMonth = :currentDate AND (c.claimsAccidentType = null OR c.claimsAccidentType = :typeClaim)  AND\n" +
            "\t\t\t(a.foreignCountryClaim = null OR a.foreignCountryClaim = :isAbroad) AND (a.claimWithInjured = null OR a.claimWithInjured = :isWounded) AND " +
            "(a.damageCanNotBeRepaired = null OR a.damageCanNotBeRepaired = :isRecoverability) AND (a.selectOnlyComplaintsWithoutCTP = null OR a.selectOnlyComplaintsWithoutCTP = :isCounterparty) " +
            " AND i.id = :inspectorateId AND (dam.id = null OR dam.id = :damagedInsurance) AND (a.minImport<= :amount AND a.maxImport >= :amount) " +
            " order by a.monthlyVolume desc, a.currentVolume desc")
    List<AutomaticAffiliationRuleInspectorateEntity> getMaxVolumeRule(@Param("isCounterparty") Boolean isCounterParty, @Param("isAbroad") Boolean isAbroad, @Param("isWounded") Boolean isWounded,
                                                                      @Param("isRecoverability") Boolean isRecoverability, @Param("typeClaim") DataAccidentTypeAccidentEnum typeClaim, @Param("currentDate") String currentDate, @Param("inspectorateId") UUID inspectorateId, @Param("damagedInsurance") UUID damagedInsurance, @Param("amount") Double amount);



    @Query("SELECT a from InspectorateEntity i inner join i.automaticAffiliationRuleEntityList a " +
            "left join a.claimsTypeEntityList c left join a.damagedInsuranceCompanyList da " +
            "where i.isActive= true AND i.id = :legalId AND (c.claimsAccidentType is null OR c.claimsAccidentType = :typeClaim)  " +
            "AND (a.foreignCountryClaim is null OR a.foreignCountryClaim = :isAbroad) AND (a.claimWithInjured is null OR a.claimWithInjured = :isWounded) " +
            "AND (a.damageCanNotBeRepaired is null OR a.damageCanNotBeRepaired = :isRecoverability) AND a.currentMonth = :date " +
            "AND (a.selectOnlyComplaintsWithoutCTP is null OR a.selectOnlyComplaintsWithoutCTP = :isCounterparty) AND a.counterPartType = :counterpartyType AND (da.id is null OR da.id = :damagedCompany) AND (a.minImport is null OR :amount is null OR a.minImport <= :amount)  AND a.monthlyVolume <> -1 AND (a.monthlyVolume > a.currentVolume) AND (a.monthlyVolume - a.currentVolume) >= all (select a.monthlyVolume - a.currentVolume from InspectorateEntity i inner join i.automaticAffiliationRuleEntityList a " +
            "left join a.claimsTypeEntityList c left join a.damagedInsuranceCompanyList da " +
            "where i.isActive= true AND (c.claimsAccidentType is null OR c.claimsAccidentType = :typeClaim)  " +
            "AND (a.foreignCountryClaim is null OR a.foreignCountryClaim = :isAbroad) AND (a.claimWithInjured is null OR a.claimWithInjured = :isWounded) " +
            "AND (a.damageCanNotBeRepaired is null OR a.damageCanNotBeRepaired = :isRecoverability) AND a.currentMonth = :date " +
            "AND (a.selectOnlyComplaintsWithoutCTP is null OR a.selectOnlyComplaintsWithoutCTP = :isCounterparty) AND a.counterPartType = :counterpartyType AND (da.id is null OR da.id = :damagedCompany) AND (a.minImport is null OR :amount is null OR a.minImport <= :amount) AND (a.maxImport is null OR :amount is null OR a.maxImport >= :amount) AND a.monthlyVolume <> -1 AND (a.monthlyVolume > a.currentVolume)) " +
            "AND (a.maxImport is null OR :amount is null OR a.maxImport >= :amount)")
    List<AutomaticAffiliationRuleInspectorateEntity> getMaxVolumeRule(@Param("isCounterparty") Boolean isCounterParty, @Param("isAbroad") Boolean isAbroad, @Param("isWounded") Boolean isWounded,
                                                               @Param("isRecoverability") Boolean isRecoverability, @Param("typeClaim") DataAccidentTypeAccidentEnum typeClaim, @Param("date") String date, @Param("damagedCompany") UUID damagedCompany, @Param("amount") Double amount, @Param("counterpartyType") VehicleTypeEnum counterpartyType, @Param("legalId") UUID legalId);




    @Query("SELECT a from InspectorateEntity i inner join i.automaticAffiliationRuleEntityList a " +
            "left join a.claimsTypeEntityList c left join a.damagedInsuranceCompanyList da left join a.counterParterInsuranceCompanyList ca " +
            "where i.isActive= true AND i.id = :legalId AND (c.claimsAccidentType = null OR c.claimsAccidentType = :typeClaim)  " +
            "AND (a.foreignCountryClaim = null OR a.foreignCountryClaim = :isAbroad) AND (a.claimWithInjured = null OR a.claimWithInjured = :isWounded) " +
            "AND (a.damageCanNotBeRepaired = null OR a.damageCanNotBeRepaired = :isRecoverability) AND a.currentMonth = :date " +
            "AND (a.selectOnlyComplaintsWithoutCTP = null OR a.selectOnlyComplaintsWithoutCTP = :isCounterparty) AND (a.counterPartType = null OR a.counterPartType = :counterpartyType )AND (da.id = null OR da.id = :damagedCompany) AND (ca.id = null OR ca.id = :counterpartyCompany) AND (a.minImport is null OR :amount is null OR a.minImport <= :amount) AND a.monthlyVolume <> -1 AND (a.monthlyVolume > a.currentVolume) AND (a.monthlyVolume - a.currentVolume) >= all (select a.monthlyVolume - a.currentVolume from InspectorateEntity i inner join i.automaticAffiliationRuleEntityList a " +
            "left join a.claimsTypeEntityList c left join a.damagedInsuranceCompanyList da left join a.counterParterInsuranceCompanyList ca " +
            "where i.isActive= true AND (c.claimsAccidentType = null OR c.claimsAccidentType = :typeClaim)  " +
            "AND (a.foreignCountryClaim = null OR a.foreignCountryClaim = :isAbroad) AND (a.claimWithInjured = null OR a.claimWithInjured = :isWounded) " +
            "AND (a.damageCanNotBeRepaired = null OR a.damageCanNotBeRepaired = :isRecoverability) AND a.currentMonth = :date " +
            "AND (a.selectOnlyComplaintsWithoutCTP = null OR a.selectOnlyComplaintsWithoutCTP = :isCounterparty) AND (a.counterPartType = null OR a.counterPartType = :counterpartyType) AND (da.id = null OR da.id = :damagedCompany) AND (ca.id = null OR ca.id = :counterpartyCompany) AND (a.minImport is null OR :amount is null OR a.minImport <= :amount) AND (a.maxImport is null OR :amount is null OR a.maxImport >= :amount) AND a.monthlyVolume <> -1 AND (a.monthlyVolume > a.currentVolume)) " +
            "AND (a.maxImport is null OR :amount is null OR a.maxImport >= :amount)")
    List<AutomaticAffiliationRuleInspectorateEntity> getMaxVolumeRuleWITHCounterparty(@Param("isCounterparty") Boolean isCounterParty, @Param("isAbroad") Boolean isAbroad, @Param("isWounded") Boolean isWounded,
                                                                               @Param("isRecoverability") Boolean isRecoverability, @Param("typeClaim") DataAccidentTypeAccidentEnum typeClaim, @Param("date") String date, @Param("damagedCompany") UUID damagedCompany, @Param("amount") Double amount, @Param("counterpartyCompany") UUID counterpartyCompany, @Param("counterpartyType") VehicleTypeEnum counterpartyType, @Param("legalId") UUID legalId);




    @Query("SELECT a from InspectorateEntity i inner join i.automaticAffiliationRuleEntityList a " +
            "left join a.claimsTypeEntityList c left join a.damagedInsuranceCompanyList da left join a.counterParterInsuranceCompanyList ca " +
            "where i.isActive= true AND i.id = :legalId AND (c.claimsAccidentType = null OR c.claimsAccidentType = :typeClaim)  " +
            "AND (a.foreignCountryClaim = null OR a.foreignCountryClaim = :isAbroad) AND (a.claimWithInjured = null OR a.claimWithInjured = :isWounded) " +
            "AND (a.damageCanNotBeRepaired = null OR a.damageCanNotBeRepaired = :isRecoverability) AND a.currentMonth = :date " +
            "AND (a.selectOnlyComplaintsWithoutCTP = null OR a.selectOnlyComplaintsWithoutCTP = :isCounterparty) AND a.counterPartType = :counterpartyType AND (da.id = null OR da.id = :damagedCompany) AND (ca.id = null OR ca.id = :counterpartyCompany) AND (a.minImport is null OR :amount is null OR a.minImport <= :amount)  AND (a.maxImport is null OR :amount is null OR a.maxImport >= :amount) AND a.monthlyVolume = 0")
    List<AutomaticAffiliationRuleInspectorateEntity> getMaxVolumeRuleWITHCounterpartyAndMonthlyVolumeZero(@Param("isCounterparty") Boolean isCounterParty, @Param("isAbroad") Boolean isAbroad, @Param("isWounded") Boolean isWounded,
                                                                                                   @Param("isRecoverability") Boolean isRecoverability, @Param("typeClaim") DataAccidentTypeAccidentEnum typeClaim, @Param("date") String date, @Param("damagedCompany") UUID damagedCompany, @Param("amount") Double amount, @Param("counterpartyCompany") UUID counterpartyCompany, @Param("counterpartyType") VehicleTypeEnum counterpartyType, @Param("legalId") UUID legalId);


    @Query("SELECT a from InspectorateEntity i inner join i.automaticAffiliationRuleEntityList a " +
            "left join a.claimsTypeEntityList c left join a.damagedInsuranceCompanyList da " +
            "where i.isActive= true AND i.id = :legalId AND (c.claimsAccidentType = null OR c.claimsAccidentType = :typeClaim)  " +
            "AND (a.foreignCountryClaim = null OR a.foreignCountryClaim = :isAbroad) AND (a.claimWithInjured = null OR a.claimWithInjured = :isWounded) " +
            "AND (a.damageCanNotBeRepaired = null OR a.damageCanNotBeRepaired = :isRecoverability) AND a.currentMonth = :date " +
            "AND (a.selectOnlyComplaintsWithoutCTP = null OR a.selectOnlyComplaintsWithoutCTP = :isCounterparty) AND a.counterPartType = :counterpartyType AND (da.id = null OR da.id = :damagedCompany) AND (a.minImport is null OR :amount is null OR a.minImport <= :amount)  AND (a.maxImport is null OR :amount is null OR a.maxImport >= :amount) AND a.monthlyVolume = 0")
    List<AutomaticAffiliationRuleInspectorateEntity> getMaxVolumeRuleAndMonthlyVolumeZero(@Param("isCounterparty") Boolean isCounterParty, @Param("isAbroad") Boolean isAbroad, @Param("isWounded") Boolean isWounded,
                                                                                   @Param("isRecoverability") Boolean isRecoverability, @Param("typeClaim") DataAccidentTypeAccidentEnum typeClaim, @Param("date") String date, @Param("damagedCompany") UUID damagedCompany, @Param("amount") Double amount, @Param("counterpartyType") VehicleTypeEnum counterpartyType, @Param("legalId") UUID legalId);



    @Transactional
    @Modifying
    @Query("UPDATE AutomaticAffiliationRuleInspectorateEntity aut " +
            "SET aut.currentVolume = aut.currentVolume+1 WHERE aut.id = :idRule")
    void updateCurrentVolumeRuleById(@Param("idRule") UUID idRule);

    @Query("SELECT a FROM InspectorateEntity i inner join i.automaticAffiliationRuleEntityList a WHERE i.id = :inspectorateId AND a.selectionName = :name")
    AutomaticAffiliationRuleInspectorateEntity searchRuleForInspectoratebyName(@Param("name") String name, @Param("inspectorateId") UUID inspectorateId);

}
