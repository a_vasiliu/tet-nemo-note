package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.entity.settings.ExportNotificationTypeEntity;
import com.doing.nemo.claims.entity.settings.ExportNotificationTypeEntity_;
import com.doing.nemo.claims.entity.settings.NotificationTypeEntity;
import com.doing.nemo.claims.entity.settings.NotificationTypeEntity_;
import com.doing.nemo.claims.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class ExportNotificationTypeRepositoryV1 {
    @PersistenceContext
    @Autowired
    private EntityManager em;

    public List<ExportNotificationTypeEntity> findExportNotificationType(Integer page, Integer pageSize, String orderBy, Boolean asc, String descriptionNotification, String code, String description, Boolean isActive){

        Map<String, String> mappOrder = new HashMap<>();
        mappOrder.put("code", "code");
        mappOrder.put("description", "description");
        mappOrder.put("is_active", "isActive");

        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<ExportNotificationTypeEntity> exportNotificationTypeCriteriaQuery = criteriaBuilder.createQuery(ExportNotificationTypeEntity.class);
        Root<ExportNotificationTypeEntity> exportNotificationTypeRoot = exportNotificationTypeCriteriaQuery.from(ExportNotificationTypeEntity.class);
        Join<ExportNotificationTypeEntity, NotificationTypeEntity> notificationTypeEntityJoin = exportNotificationTypeRoot.join(ExportNotificationTypeEntity_.notificationType, JoinType.LEFT);
        exportNotificationTypeCriteriaQuery.select(exportNotificationTypeRoot);

        //Constructing list of parameters
        List<Predicate> predicates = new ArrayList<>();

        if (Util.isNotEmpty(descriptionNotification)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    notificationTypeEntityJoin.get(NotificationTypeEntity_.description)), "%"+descriptionNotification.toUpperCase()+"%"));
        }
        if (Util.isNotEmpty(code)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    exportNotificationTypeRoot.get(ExportNotificationTypeEntity_.code)), code.toUpperCase()));
        }
        if (Util.isNotEmpty(description)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    exportNotificationTypeRoot.get(ExportNotificationTypeEntity_.description)), "%"+description.toUpperCase()+"%"));
        }
        if (isActive != null) {
            predicates.add(criteriaBuilder.equal(
                    exportNotificationTypeRoot.get(ExportNotificationTypeEntity_.isActive), isActive));
        }
        exportNotificationTypeCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));

        //List<Order> orderByList = new LinkedList<>();
        String fieldToOrder = null;
        if(orderBy != null && orderBy.equalsIgnoreCase("description_notification")){
            if (asc!= null && asc) {
                exportNotificationTypeCriteriaQuery.orderBy(criteriaBuilder.asc(notificationTypeEntityJoin.get("description")));
            } else {
                exportNotificationTypeCriteriaQuery.orderBy(criteriaBuilder.desc(notificationTypeEntityJoin.get("description")));
            }
        } else {
            if(orderBy != null)
                fieldToOrder = mappOrder.get(orderBy);
            if(fieldToOrder != null && asc != null){
                if (asc) {
                    exportNotificationTypeCriteriaQuery.orderBy(criteriaBuilder.asc(exportNotificationTypeRoot.get(fieldToOrder)));
                } else {
                    exportNotificationTypeCriteriaQuery.orderBy(criteriaBuilder.desc(exportNotificationTypeRoot.get(fieldToOrder)));
                }

            } else if(orderBy != null) {
                exportNotificationTypeCriteriaQuery.orderBy(criteriaBuilder.desc(exportNotificationTypeRoot.get(fieldToOrder)));
            }
        }

        List<ExportNotificationTypeEntity> exportNotificationTypeEntityList = em.createQuery(exportNotificationTypeCriteriaQuery).setFirstResult((page-1)*pageSize).setMaxResults(pageSize).getResultList();
        return exportNotificationTypeEntityList;
    }

    public Long countPaginationExportNotificationType(String descriptionNotification, String code, String description, Boolean isActive) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<Long> exportNotificationTypeCriteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<ExportNotificationTypeEntity> exportNotificationTypeRoot = exportNotificationTypeCriteriaQuery.from(ExportNotificationTypeEntity.class);
        Join<ExportNotificationTypeEntity, NotificationTypeEntity> notificationTypeEntityJoin = exportNotificationTypeRoot.join(ExportNotificationTypeEntity_.notificationType, JoinType.LEFT);
        exportNotificationTypeCriteriaQuery.select(criteriaBuilder.count(exportNotificationTypeRoot));

        //Constructing list of parameters
        List<Predicate> predicates = new ArrayList<>();

        if (Util.isNotEmpty(descriptionNotification)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    notificationTypeEntityJoin.get(NotificationTypeEntity_.description)), "%"+descriptionNotification.toUpperCase()+"%"));
        }
        if (Util.isNotEmpty(code)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    exportNotificationTypeRoot.get(ExportNotificationTypeEntity_.code)), code.toUpperCase()));
        }
        if (Util.isNotEmpty(description)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    exportNotificationTypeRoot.get(ExportNotificationTypeEntity_.description)), "%"+description.toUpperCase()+"%"));
        }
        if (isActive != null) {
            predicates.add(criteriaBuilder.equal(
                    exportNotificationTypeRoot.get(ExportNotificationTypeEntity_.isActive), isActive));
        }
        exportNotificationTypeCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));


        Query sql = em.createQuery(exportNotificationTypeCriteriaQuery);
        return (Long)sql.getSingleResult();

    }
}
