package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.entity.enumerated.ClaimsRepairEnum;
import com.doing.nemo.claims.entity.settings.MotivationEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.UUID;

public interface MotivationRepository extends JpaRepository<MotivationEntity, UUID> {


    @Query("SELECT m FROM MotivationEntity m WHERE m.answerType = :answerType AND m.description = :description")
    MotivationEntity searchMotivationbyAnswerAndDescription(@Param("answerType") String answerType, @Param("description") String description);

    @Query("SELECT m FROM MotivationEntity m WHERE m.id <> :id AND m.answerType = :answerType AND m.description = :description")
    MotivationEntity searchMotivationrWithDifferentId(@Param("id") UUID id, @Param("answerType") String answerType, @Param("description") String description);

    @Query("SELECT m FROM MotivationEntity m WHERE m.typeComplaint = :typeComplaint")
    List<MotivationEntity> searchManagerByTypeComplaint(@Param("typeComplaint") ClaimsRepairEnum typeComplaint);

    @Query("SELECT m FROM MotivationEntity m WHERE m.typeComplaint = :typeComplaint AND m.isActive = :isActive")
    List<MotivationEntity> searchManagerByTypeComplaintAndIsActiveFilter(@Param("typeComplaint") ClaimsRepairEnum typeComplaint, @Param("isActive") Boolean isActive);
}
