package com.doing.nemo.claims.repository;


import com.doing.nemo.claims.entity.ClaimsNewEntity;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsFlowEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.LockModeType;
import java.util.List;
import java.util.Optional;

import java.util.List;
import java.util.UUID;


public interface ClaimsNewRepository extends JpaRepository<ClaimsNewEntity, String> , ClaimsNewRepositoryCustom{

    @Query(value = "SELECT c FROM ClaimsNewEntity c WHERE c.practiceId = :practiceId")
    ClaimsNewEntity getClaimsByIdPratica( @Param("practiceId") Long practiceId );

    @Query("SELECT c FROM ClaimsNewEntity c WHERE c.status = :status AND c.type = :type")
    List<ClaimsNewEntity> searchByStatusAndFlow(@Param("status") ClaimsStatusEnum status, @Param("type") ClaimsFlowEnum type);

    @Transactional(isolation=Isolation.REPEATABLE_READ)
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    @Query("SELECT c FROM ClaimsNewEntity c WHERE c.id = :id")
    Optional<ClaimsNewEntity> findByIdForUpdate(@Param("id") String id);
}
