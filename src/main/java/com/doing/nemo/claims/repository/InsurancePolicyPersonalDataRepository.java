package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.entity.settings.InsurancePolicyPersonalDataEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface InsurancePolicyPersonalDataRepository extends JpaRepository<InsurancePolicyPersonalDataEntity, UUID> {
}
