package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.enumerated.DamagedEnum.VehicleEnum.VehicleTypeEnum;
import com.doing.nemo.claims.entity.settings.InspectorateEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface InspectorateRepository extends JpaRepository<InspectorateEntity, UUID> {

    /* LEGAME CON AUTHORITY
    @Query("SELECT i\n" +
            "\t\t\tfrom InspectorateEntity i inner join i.automaticAffiliationRuleEntityList a inner join a.claimsTypeEntityList c inner join a.counterParterInsuranceCompanyList co " +
            "inner join a.damagedInsuranceCompanyList dam \n" +
            "\t\t\twhere a.monthlyVolume >  a.currentVolume AND a.currentMonth = :currentDate AND c.claimsAccidentType = :typeClaim AND\n" +
            "\t\t\t(a.foreignCountryClaim = null OR a.foreignCountryClaim = :isAbroad) AND (a.claimWithInjured = null OR a.claimWithInjured = :isWounded) AND (a.damageCanNotBeRepaired = null OR a.damageCanNotBeRepaired = :isRecoverability) AND (a.selectOnlyComplaintsWithoutCTP = null OR a.selectOnlyComplaintsWithoutCTP = :isCounterparty) " +
            "AND dam.name = :damagedInsurance AND (a.minImport<= :amount AND a.maxImport >= :amount)")
    List<InspectorateEntity> getMaxVolumeInspectorate(@Param("isCounterparty") Boolean isCounterParty, @Param("isAbroad") Boolean isAbroad, @Param("isWounded") Boolean isWounded,
                                                      @Param("isRecoverability") Boolean isRecoverability, @Param("typeClaim") DataAccidentTypeAccidentEnum typeClaim, @Param("currentDate") String currentDate, @Param("damagedInsurance") String damagedInsurance, @Param("amount") Double amount);*/


    @Query("SELECT i\n" +
            "\t\t\tfrom InspectorateEntity i inner join i.automaticAffiliationRuleEntityList a left join a.claimsTypeEntityList c  " +
            "left join a.damagedInsuranceCompanyList dam \n" +
            "\t\t\twhere i.isActive = true AND a.monthlyVolume >  a.currentVolume AND a.currentMonth = :currentDate AND (c.claimsAccidentType = null OR c.claimsAccidentType = :typeClaim) AND\n" +
            "\t\t\t(a.foreignCountryClaim = null OR a.foreignCountryClaim = :isAbroad) AND (a.claimWithInjured = null OR a.claimWithInjured = :isWounded) AND (a.damageCanNotBeRepaired = null OR a.damageCanNotBeRepaired = :isRecoverability) AND (a.selectOnlyComplaintsWithoutCTP = null OR a.selectOnlyComplaintsWithoutCTP = :isCounterparty) " +
            "AND (dam.id = null OR dam.id = :damagedInsurance) AND (a.minImport is null OR :amount is null OR a.minImport <= :amount) AND (a.maxImport is null OR :amount is null OR a.maxImport >= :amount)")
    List<InspectorateEntity> getMaxVolumeInspectorateP(@Param("isCounterparty") Boolean isCounterParty, @Param("isAbroad") Boolean isAbroad, @Param("isWounded") Boolean isWounded,
                                                      @Param("isRecoverability") Boolean isRecoverability, @Param("typeClaim") DataAccidentTypeAccidentEnum typeClaim, @Param("currentDate") String currentDate, @Param("damagedInsurance") UUID damagedInsurance, @Param("amount") Double amount);


    @Query("SELECT i from InspectorateEntity i inner join i.automaticAffiliationRuleEntityList a " +
            "left join a.claimsTypeEntityList c left join a.damagedInsuranceCompanyList da " +
            "where i.isActive = true AND (c.claimsAccidentType is null OR c.claimsAccidentType = :typeClaim)  " +
            "AND (a.foreignCountryClaim is null OR a.foreignCountryClaim = :isAbroad) AND (a.claimWithInjured is null OR a.claimWithInjured = :isWounded) " +
            "AND (a.damageCanNotBeRepaired is null OR a.damageCanNotBeRepaired = :isRecoverability) AND a.currentMonth = :date " +
            "AND (a.selectOnlyComplaintsWithoutCTP is null OR a.selectOnlyComplaintsWithoutCTP = :isCounterparty) " +
            "AND (a.counterPartType is null OR a.counterPartType = :counterpartyType) " +
            "AND (da.id is null OR da.id = :damagedCompany) AND (a.minImport is null OR :amount is null OR a.minImport <= :amount) AND a.monthlyVolume <> -1 AND (a.monthlyVolume > a.currentVolume) AND (a.monthlyVolume - a.currentVolume) >= all (select a.monthlyVolume - a.currentVolume from InspectorateEntity i inner join i.automaticAffiliationRuleEntityList a " +
            "left join a.claimsTypeEntityList c left join a.damagedInsuranceCompanyList da " +
            "where i.isActive = true AND (c.claimsAccidentType is null OR c.claimsAccidentType = :typeClaim)  " +
            "AND (a.foreignCountryClaim is null OR a.foreignCountryClaim = :isAbroad) AND (a.claimWithInjured is null OR a.claimWithInjured = :isWounded) " +
            "AND (a.damageCanNotBeRepaired is null OR a.damageCanNotBeRepaired = :isRecoverability) AND a.currentMonth = :date " +
            "AND (a.selectOnlyComplaintsWithoutCTP is null OR a.selectOnlyComplaintsWithoutCTP = :isCounterparty) " +
            "AND (a.counterPartType is null OR a.counterPartType = :counterpartyType) " +
            "AND (da.id is null OR da.id = :damagedCompany) AND (a.minImport is null OR :amount is null OR a.minImport <= :amount) AND a.monthlyVolume <> -1 AND  (a.monthlyVolume > a.currentVolume) AND (a.maxImport is null OR :amount is null OR a.maxImport >= :amount)) " +
            "AND (a.maxImport is null OR :amount is null OR a.maxImport >= :amount)")
    List<InspectorateEntity> getMaxVolumeInspectorate(@Param("isCounterparty") Boolean isCounterParty, @Param("isAbroad") Boolean isAbroad, @Param("isWounded") Boolean isWounded,
                                        @Param("isRecoverability") Boolean isRecoverability, @Param("typeClaim") DataAccidentTypeAccidentEnum typeClaim, @Param("date") String date, @Param("damagedCompany") UUID damagedCompany, @Param("amount") Double amount, @Param("counterpartyType") VehicleTypeEnum counterpartyType);


    @Query("SELECT i from InspectorateEntity i inner join i.automaticAffiliationRuleEntityList a " +
            "left join a.claimsTypeEntityList c left join a.damagedInsuranceCompanyList da left join a.counterParterInsuranceCompanyList ca " +
            "where i.isActive = true AND (c.claimsAccidentType = null OR c.claimsAccidentType = :typeClaim)  " +
            "AND (a.foreignCountryClaim = null OR a.foreignCountryClaim = :isAbroad) AND (a.claimWithInjured = null OR a.claimWithInjured = :isWounded) " +
            "AND (a.damageCanNotBeRepaired = null OR a.damageCanNotBeRepaired = :isRecoverability) AND a.currentMonth = :date " +
            "AND (a.selectOnlyComplaintsWithoutCTP = null OR a.selectOnlyComplaintsWithoutCTP = :isCounterparty) " +
            "AND (a.counterPartType is null OR a.counterPartType = :counterpartyType) " +
            "AND (da.id = null OR da.id = :damagedCompany) AND (ca.id = null OR ca.id = :counterpartyCompany) AND (a.minImport is null OR :amount is null OR a.minImport <= :amount) AND a.monthlyVolume <> -1 AND (a.monthlyVolume > a.currentVolume) AND (a.monthlyVolume - a.currentVolume) >= all (select a.monthlyVolume - a.currentVolume from InspectorateEntity i inner join i.automaticAffiliationRuleEntityList a " +
            "left join a.claimsTypeEntityList c left join a.damagedInsuranceCompanyList da left join a.counterParterInsuranceCompanyList ca " +
            "where i.isActive = true AND (c.claimsAccidentType = null OR c.claimsAccidentType = :typeClaim)  " +
            "AND (a.foreignCountryClaim = null OR a.foreignCountryClaim = :isAbroad) AND (a.claimWithInjured = null OR a.claimWithInjured = :isWounded) " +
            "AND (a.damageCanNotBeRepaired = null OR a.damageCanNotBeRepaired = :isRecoverability) AND a.currentMonth = :date " +
            "AND (a.selectOnlyComplaintsWithoutCTP = null OR a.selectOnlyComplaintsWithoutCTP = :isCounterparty) " +
            "AND (a.counterPartType is null OR a.counterPartType = :counterpartyType) " +
            "AND (da.id = null OR da.id = :damagedCompany) AND (ca.id = null OR ca.id = :counterpartyCompany) AND (a.minImport is null OR :amount is null OR a.minImport <= :amount) AND a.monthlyVolume <> -1 AND (a.monthlyVolume > a.currentVolume) AND (a.maxImport is null OR :amount is null OR a.maxImport >= :amount)) " +
            "AND (a.maxImport is null OR :amount is null OR a.maxImport >= :amount)")
    List<InspectorateEntity> getMaxVolumeInspectorateWITHCounterparty(@Param("isCounterparty") Boolean isCounterParty, @Param("isAbroad") Boolean isAbroad, @Param("isWounded") Boolean isWounded,
                                                        @Param("isRecoverability") Boolean isRecoverability, @Param("typeClaim") DataAccidentTypeAccidentEnum typeClaim, @Param("date") String date, @Param("damagedCompany") UUID damagedCompany, @Param("amount") Double amount, @Param("counterpartyCompany") UUID counterpartyCompany, @Param("counterpartyType") VehicleTypeEnum counterpartyType);


    @Query("SELECT i from InspectorateEntity i inner join i.automaticAffiliationRuleEntityList a " +
            "left join a.claimsTypeEntityList c left join a.damagedInsuranceCompanyList da left join a.counterParterInsuranceCompanyList ca " +
            "where i.isActive = true AND (c.claimsAccidentType = null OR c.claimsAccidentType = :typeClaim)  " +
            "AND (a.foreignCountryClaim = null OR a.foreignCountryClaim = :isAbroad) AND (a.claimWithInjured = null OR a.claimWithInjured = :isWounded) " +
            "AND (a.damageCanNotBeRepaired = null OR a.damageCanNotBeRepaired = :isRecoverability) AND a.currentMonth = :date " +
            "AND (a.selectOnlyComplaintsWithoutCTP = null OR a.selectOnlyComplaintsWithoutCTP = :isCounterparty) " +
            "AND (a.counterPartType is null OR a.counterPartType = :counterpartyType) " +
            "AND (da.id = null OR da.id = :damagedCompany) AND (ca.id = null OR ca.id = :counterpartyCompany) AND (a.minImport is null OR :amount is null OR a.minImport <= :amount)  AND (a.maxImport is null OR :amount is null OR a.maxImport >= :amount) AND a.monthlyVolume = 0")
    List<InspectorateEntity> getMaxVolumeInspectorateWITHCounterpartyAndMonthlyVolumeZero(@Param("isCounterparty") Boolean isCounterParty, @Param("isAbroad") Boolean isAbroad, @Param("isWounded") Boolean isWounded,
                                                                            @Param("isRecoverability") Boolean isRecoverability, @Param("typeClaim") DataAccidentTypeAccidentEnum typeClaim, @Param("date") String date, @Param("damagedCompany") UUID damagedCompany, @Param("amount") Double amount, @Param("counterpartyCompany") UUID counterpartyCompany, @Param("counterpartyType") VehicleTypeEnum counterpartyType);


    @Query("SELECT i from InspectorateEntity i inner join i.automaticAffiliationRuleEntityList a " +
            "left join a.claimsTypeEntityList c left join a.damagedInsuranceCompanyList da " +
            "where i.isActive = true AND (c.claimsAccidentType = null OR c.claimsAccidentType = :typeClaim)  " +
            "AND (a.foreignCountryClaim = null OR a.foreignCountryClaim = :isAbroad) AND (a.claimWithInjured = null OR a.claimWithInjured = :isWounded) " +
            "AND (a.damageCanNotBeRepaired = null OR a.damageCanNotBeRepaired = :isRecoverability) AND a.currentMonth = :date " +
            "AND (a.selectOnlyComplaintsWithoutCTP = null OR a.selectOnlyComplaintsWithoutCTP = :isCounterparty) " +
            "AND (a.counterPartType is null OR a.counterPartType = :counterpartyType) " +
            "AND (da.id = null OR da.id = :damagedCompany) AND (a.minImport is null OR :amount is null OR a.minImport <= :amount)  AND (a.maxImport is null OR :amount is null OR a.maxImport >= :amount) AND a.monthlyVolume = 0")
    List<InspectorateEntity> getMaxVolumeInspectorateAndMonthlyVolumeZero(@Param("isCounterparty") Boolean isCounterParty, @Param("isAbroad") Boolean isAbroad, @Param("isWounded") Boolean isWounded,
                                                            @Param("isRecoverability") Boolean isRecoverability, @Param("typeClaim") DataAccidentTypeAccidentEnum typeClaim, @Param("date") String date, @Param("damagedCompany") UUID damagedCompany, @Param("amount") Double amount, @Param("counterpartyType") VehicleTypeEnum counterpartyType);



    @Query("SELECT i FROM InspectorateEntity i WHERE i.name = :name")
    InspectorateEntity searchInspectoratebyName(@Param("name") String name);

    @Query("SELECT i FROM InspectorateEntity i WHERE i.code = :code")
    InspectorateEntity searchInspectoratebyCode(@Param("code") long code);

    @Query("SELECT i FROM InspectorateEntity i WHERE i.id <> :id AND i.name = :name")
    InspectorateEntity searchInspectorateWithDifferentId(@Param("id") UUID id, @Param("name") String name);

}
