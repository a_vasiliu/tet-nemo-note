package com.doing.nemo.claims.repository;


import com.doing.nemo.claims.entity.claims.ClaimsAuthorityEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;


@Repository
public interface ClaimsAuthorityRepositorty extends JpaRepository<ClaimsAuthorityEntity, String> {


    @Query("SELECT a FROM ClaimsAuthorityEntity a WHERE a.workingNumber = :workingNumber AND a.authorityDossierNumber = :dossierNumber")
    ClaimsAuthorityEntity searchByWorkingNumberAuthorityDossierNumber(
            @Param("workingNumber") String workingNumber,
            @Param("dossierNumber") String dossierNumber);

    @Query("SELECT a FROM ClaimsAuthorityEntity a WHERE a.authorityWorkingId = :workingId AND a.authorityDossierId = :dossierId")
    ClaimsAuthorityEntity searchByWorkingIdAuthorityDossierId(
            @Param("workingId") String workingId,
            @Param("dossierId") String dossierId);

}
