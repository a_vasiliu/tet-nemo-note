package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.entity.enumerated.AttachmentEnum.GroupsEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsRepairEnum;
import com.doing.nemo.claims.entity.settings.AttachmentTypeEntity;
import com.doing.nemo.claims.entity.settings.AttachmentTypeEntity_;
import com.doing.nemo.claims.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class AttachmentTypeRepositoryV1 {

    @PersistenceContext
    @Autowired
    private EntityManager em;
    
    public List<AttachmentTypeEntity> findAttachmentTypesFiltered(Integer page, Integer pageSize, ClaimsRepairEnum claimsRepairEnum, String name, GroupsEnum groupsEnum, Boolean isActive, String orderBy, Boolean asc) {
        Map<String, String> mappOrder = new HashMap<>();
        mappOrder.put("name", "name");
        mappOrder.put("groups", "groups");
        mappOrder.put("type_complaint", "typeComplaint");
        mappOrder.put("is_active", "isActive");

        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<AttachmentTypeEntity> attachmentTypeCriteriaQuery = criteriaBuilder.createQuery(AttachmentTypeEntity.class);
        Root<AttachmentTypeEntity> attachmentTypeRoot = attachmentTypeCriteriaQuery.from(AttachmentTypeEntity.class);

        attachmentTypeCriteriaQuery.select(attachmentTypeRoot);

        //Constructing list of parameters
        List<Predicate> predicates = new ArrayList<>();

        if (claimsRepairEnum != null) {
            predicates.add(criteriaBuilder.equal(
                    attachmentTypeRoot.get(AttachmentTypeEntity_.typeComplaint), claimsRepairEnum));
        }
        if (Util.isNotEmpty(name)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    attachmentTypeRoot.get(AttachmentTypeEntity_.name)), "%"+name.toUpperCase()+"%"));
        }
        if (isActive != null) {
            predicates.add(criteriaBuilder.equal(
                    attachmentTypeRoot.get(AttachmentTypeEntity_.isActive), isActive));
        }
        if (groupsEnum != null) {
            predicates.add(criteriaBuilder.equal(
                    attachmentTypeRoot.get(AttachmentTypeEntity_.groups), groupsEnum));
        }
        attachmentTypeCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));

        //List<Order> orderByList = new LinkedList<>();
        String fieldToOrder = null;
        if(orderBy != null)
            fieldToOrder = mappOrder.get(orderBy);
        if(fieldToOrder != null && asc != null){
            if (asc) {
                attachmentTypeCriteriaQuery.orderBy(criteriaBuilder.asc(attachmentTypeRoot.get(fieldToOrder)));
            } else {
                attachmentTypeCriteriaQuery.orderBy(criteriaBuilder.desc(attachmentTypeRoot.get(fieldToOrder)));
            }

        } else if(orderBy != null) {
            attachmentTypeCriteriaQuery.orderBy(criteriaBuilder.desc(attachmentTypeRoot.get(fieldToOrder)));
        }

        List<AttachmentTypeEntity> attachmentTypeEntityList = em.createQuery(attachmentTypeCriteriaQuery).setFirstResult((page-1)*pageSize).setMaxResults(pageSize).getResultList();
        return attachmentTypeEntityList;

    }
    
    public Long countAttachmentTypeFiltered(ClaimsRepairEnum claimsRepairEnum,String name, GroupsEnum groupsEnum, Boolean isActive) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<Long> attachmentTypeCriteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<AttachmentTypeEntity> attachmentTypeRoot = attachmentTypeCriteriaQuery.from(AttachmentTypeEntity.class);

        attachmentTypeCriteriaQuery.select(criteriaBuilder.count(attachmentTypeRoot));

        //Constructing list of parameters
        List<Predicate> predicates = new ArrayList<>();

        if (claimsRepairEnum != null) {
            predicates.add(criteriaBuilder.equal(
                    attachmentTypeRoot.get(AttachmentTypeEntity_.typeComplaint), claimsRepairEnum));
        }
        if (Util.isNotEmpty(name)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    attachmentTypeRoot.get(AttachmentTypeEntity_.name)), "%"+name.toUpperCase()+"%"));
        }
        if (isActive != null) {
            predicates.add(criteriaBuilder.equal(
                    attachmentTypeRoot.get(AttachmentTypeEntity_.isActive), isActive));
        }
        if (groupsEnum != null) {
            predicates.add(criteriaBuilder.equal(
                    attachmentTypeRoot.get(AttachmentTypeEntity_.groups), groupsEnum));
        }

        attachmentTypeCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));
        Query sql = em.createQuery(attachmentTypeCriteriaQuery);
        return (Long) sql.getSingleResult();

    }


    





}
