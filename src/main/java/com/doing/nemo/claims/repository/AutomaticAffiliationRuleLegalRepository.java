package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.enumerated.DamagedEnum.VehicleEnum.VehicleTypeEnum;
import com.doing.nemo.claims.entity.settings.AutomaticAffiliationRuleLegalEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

@Repository
public interface AutomaticAffiliationRuleLegalRepository extends JpaRepository<AutomaticAffiliationRuleLegalEntity, UUID> {


    @Query("select a " +
            "\t\t\tfrom LegalEntity i inner join i.automaticAffiliationRuleEntityList a left join a.claimsType c left join a.damagedInsuranceCompanyList co\n" +
            "\t\t\twhere a.monthlyVolume >  a.currentVolume AND a.currentMonth = :currentDate AND (c.claimsAccidentType = null OR c.claimsAccidentType = :typeClaim) AND\n" +
            "\t\t\t(a.foreignCountryClaim = null OR a.foreignCountryClaim = :isAbroad) AND (a.claimWithInjured = null OR a.claimWithInjured = :isWounded) AND  " +
            "(a.complaintsWithoutCtp = null OR a.complaintsWithoutCtp = :isCounterparty) AND (a.damageCanNotBeRepaired = null OR a.damageCanNotBeRepaired = :isRecoverability) " +
            "AND (co.id = null OR co.id = :damagedCompany) AND i.id = :legalId AND a.minImport <= :amount AND a.maxImport >= :amount" +
            " order by a.monthlyVolume desc, a.currentVolume desc")
    List<AutomaticAffiliationRuleLegalEntity> getMaxVolumeRuleLegal(@Param("isCounterparty") Boolean isCounterParty, @Param("isAbroad") Boolean isAbroad, @Param("isWounded") Boolean isWounded,
                                                                    @Param("isRecoverability") Boolean isRecoverability, @Param("typeClaim") DataAccidentTypeAccidentEnum typeClaim, @Param("currentDate") String currentDate, @Param("legalId") UUID legalId, @Param("damagedCompany") UUID damagedCompany, @Param("amount") Double amount);


    @Transactional
    @Modifying
    @Query("update AutomaticAffiliationRuleLegalEntity au set au.currentVolume = au.currentVolume+1 where au.id = :idRule")
    void updateRule(@Param("idRule") UUID idRule);

    @Query("SELECT a FROM LegalEntity l inner join l.automaticAffiliationRuleEntityList a WHERE l.id = :legalId AND a.selectionName = :name")
    AutomaticAffiliationRuleLegalEntity searchRuleForLegalbyName(@Param("name") String name, @Param("legalId") UUID legalId);



























    @Query("SELECT a from LegalEntity l inner join l.automaticAffiliationRuleEntityList a " +
            "left join a.claimsType c left join a.damagedInsuranceCompanyList da " +
            "where l.isActive = true AND l.id = :legalId AND (c.claimsAccidentType is null OR c.claimsAccidentType = :typeClaim)  " +
            "AND (a.foreignCountryClaim is null OR a.foreignCountryClaim = :isAbroad) AND (a.claimWithInjured is null OR a.claimWithInjured = :isWounded) " +
            "AND (a.damageCanNotBeRepaired is null OR a.damageCanNotBeRepaired = :isRecoverability) AND a.currentMonth = :date " +
            "AND (a.complaintsWithoutCtp is null OR a.complaintsWithoutCtp = :isCounterparty) " +
            "AND (a.counterpartType is null OR a.counterpartType = :counterpartyType) " +
            "AND (da.id is null OR da.id = :damagedCompany) AND (a.minImport is null OR :amount is null OR a.minImport <= :amount)  AND a.monthlyVolume <> -1 AND (a.monthlyVolume > a.currentVolume) AND (a.monthlyVolume - a.currentVolume) >= all (select a.monthlyVolume - a.currentVolume from LegalEntity l inner join l.automaticAffiliationRuleEntityList a " +
            "left join a.claimsType c left join a.damagedInsuranceCompanyList da " +
            "where l.isActive = true AND (c.claimsAccidentType is null OR c.claimsAccidentType = :typeClaim)  " +
            "AND (a.foreignCountryClaim is null OR a.foreignCountryClaim = :isAbroad) AND (a.claimWithInjured is null OR a.claimWithInjured = :isWounded) " +
            "AND (a.damageCanNotBeRepaired is null OR a.damageCanNotBeRepaired = :isRecoverability) AND a.currentMonth = :date " +
            "AND (a.complaintsWithoutCtp is null OR a.complaintsWithoutCtp = :isCounterparty) " +
            "AND (a.counterpartType is null OR a.counterpartType = :counterpartyType) " +
            "AND (da.id is null OR da.id = :damagedCompany) AND (a.minImport is null OR :amount is null OR a.minImport <= :amount) AND a.monthlyVolume <> -1 AND (a.maxImport is null OR :amount is null OR a.maxImport >= :amount)) " +
            "AND (a.maxImport is null OR :amount is null OR a.maxImport >= :amount)")
    List<AutomaticAffiliationRuleLegalEntity> getMaxVolumeRule(@Param("isCounterparty") Boolean isCounterParty, @Param("isAbroad") Boolean isAbroad, @Param("isWounded") Boolean isWounded,
                                        @Param("isRecoverability") Boolean isRecoverability, @Param("typeClaim") DataAccidentTypeAccidentEnum typeClaim, @Param("date") String date, @Param("damagedCompany") UUID damagedCompany, @Param("amount") Double amount, @Param("counterpartyType") VehicleTypeEnum counterpartyType, @Param("legalId") UUID legalId);




    @Query("SELECT a from LegalEntity l inner join l.automaticAffiliationRuleEntityList a " +
            "left join a.claimsType c left join a.damagedInsuranceCompanyList da left join a.counterpartInsuranceCompanyList ca " +
            "where l.isActive = true AND l.id = :legalId AND (c.claimsAccidentType = null OR c.claimsAccidentType = :typeClaim)  " +
            "AND (a.foreignCountryClaim = null OR a.foreignCountryClaim = :isAbroad) AND (a.claimWithInjured = null OR a.claimWithInjured = :isWounded) " +
            "AND (a.damageCanNotBeRepaired = null OR a.damageCanNotBeRepaired = :isRecoverability) AND a.currentMonth = :date " +
            "AND (a.complaintsWithoutCtp = null OR a.complaintsWithoutCtp = :isCounterparty) " +
            "AND (a.counterpartType is null OR a.counterpartType = :counterpartyType) " +
            "AND (da.id = null OR da.id = :damagedCompany) AND (ca.id = null OR ca.id = :counterpartyCompany) AND (a.minImport is null OR :amount is null OR a.minImport <= :amount) AND a.monthlyVolume <> -1 AND (a.monthlyVolume > a.currentVolume) AND (a.monthlyVolume - a.currentVolume) >= all (select a.monthlyVolume - a.currentVolume from LegalEntity l inner join l.automaticAffiliationRuleEntityList a " +
            "left join a.claimsType c left join a.damagedInsuranceCompanyList da left join a.counterpartInsuranceCompanyList ca " +
            "where l.isActive = true AND (c.claimsAccidentType = null OR c.claimsAccidentType = :typeClaim)  " +
            "AND (a.foreignCountryClaim = null OR a.foreignCountryClaim = :isAbroad) AND (a.claimWithInjured = null OR a.claimWithInjured = :isWounded) " +
            "AND (a.damageCanNotBeRepaired = null OR a.damageCanNotBeRepaired = :isRecoverability) AND a.currentMonth = :date " +
            "AND (a.complaintsWithoutCtp = null OR a.complaintsWithoutCtp = :isCounterparty) " +
            "AND (a.counterpartType is null OR a.counterpartType = :counterpartyType) " +
            "AND (da.id = null OR da.id = :damagedCompany) AND (ca.id = null OR ca.id = :counterpartyCompany) AND (a.minImport is null OR :amount is null OR a.minImport <= :amount) AND a.monthlyVolume <> -1 AND (a.maxImport is null OR :amount is null OR a.maxImport >= :amount)) " +
            "AND (a.maxImport is null OR :amount is null OR a.maxImport >= :amount)")
    List<AutomaticAffiliationRuleLegalEntity> getMaxVolumeRuleWITHCounterparty(@Param("isCounterparty") Boolean isCounterParty, @Param("isAbroad") Boolean isAbroad, @Param("isWounded") Boolean isWounded,
                                                        @Param("isRecoverability") Boolean isRecoverability, @Param("typeClaim") DataAccidentTypeAccidentEnum typeClaim, @Param("date") String date, @Param("damagedCompany") UUID damagedCompany, @Param("amount") Double amount, @Param("counterpartyCompany") UUID counterpartyCompany, @Param("counterpartyType") VehicleTypeEnum counterpartyType, @Param("legalId") UUID legalId);




    @Query("SELECT a from LegalEntity l inner join l.automaticAffiliationRuleEntityList a " +
            "left join a.claimsType c left join a.damagedInsuranceCompanyList da left join a.counterpartInsuranceCompanyList ca " +
            "where l.isActive = true AND l.id = :legalId AND (c.claimsAccidentType = null OR c.claimsAccidentType = :typeClaim)  " +
            "AND (a.foreignCountryClaim = null OR a.foreignCountryClaim = :isAbroad) AND (a.claimWithInjured = null OR a.claimWithInjured = :isWounded) " +
            "AND (a.damageCanNotBeRepaired = null OR a.damageCanNotBeRepaired = :isRecoverability) AND a.currentMonth = :date " +
            "AND (a.complaintsWithoutCtp = null OR a.complaintsWithoutCtp = :isCounterparty) " +
            "AND (a.counterpartType is null OR a.counterpartType = :counterpartyType) " +
            "AND (da.id = null OR da.id = :damagedCompany) AND (ca.id = null OR ca.id = :counterpartyCompany) AND (a.minImport is null OR :amount is null OR a.minImport <= :amount)  AND (a.maxImport is null OR :amount is null OR a.maxImport >= :amount) AND a.monthlyVolume = 0")
    List<AutomaticAffiliationRuleLegalEntity> getMaxVolumeRuleWITHCounterpartyAndMonthlyVolumeZero(@Param("isCounterparty") Boolean isCounterParty, @Param("isAbroad") Boolean isAbroad, @Param("isWounded") Boolean isWounded,
                                                                            @Param("isRecoverability") Boolean isRecoverability, @Param("typeClaim") DataAccidentTypeAccidentEnum typeClaim, @Param("date") String date, @Param("damagedCompany") UUID damagedCompany, @Param("amount") Double amount, @Param("counterpartyCompany") UUID counterpartyCompany, @Param("counterpartyType") VehicleTypeEnum counterpartyType, @Param("legalId") UUID legalId);


    @Query("SELECT a from LegalEntity l inner join l.automaticAffiliationRuleEntityList a " +
            "left join a.claimsType c left join a.damagedInsuranceCompanyList da left join a.counterpartInsuranceCompanyList ca " +
            "where l.isActive = true AND l.id = :legalId AND (c.claimsAccidentType = null OR c.claimsAccidentType = :typeClaim)  " +
            "AND (a.foreignCountryClaim = null OR a.foreignCountryClaim = :isAbroad) AND (a.claimWithInjured = null OR a.claimWithInjured = :isWounded) " +
            "AND (a.damageCanNotBeRepaired = null OR a.damageCanNotBeRepaired = :isRecoverability) AND a.currentMonth = :date " +
            "AND (a.complaintsWithoutCtp = null OR a.complaintsWithoutCtp = :isCounterparty) " +
            "AND (a.counterpartType is null OR a.counterpartType = :counterpartyType) " +
            "AND (da.id = null OR da.id = :damagedCompany) AND (a.minImport is null OR :amount is null OR a.minImport <= :amount)  AND (a.maxImport is null OR :amount is null OR a.maxImport >= :amount) AND a.monthlyVolume = 0")
    List<AutomaticAffiliationRuleLegalEntity> getMaxVolumeRuleAndMonthlyVolumeZero(@Param("isCounterparty") Boolean isCounterParty, @Param("isAbroad") Boolean isAbroad, @Param("isWounded") Boolean isWounded,
                                                            @Param("isRecoverability") Boolean isRecoverability, @Param("typeClaim") DataAccidentTypeAccidentEnum typeClaim, @Param("date") String date, @Param("damagedCompany") UUID damagedCompany, @Param("amount") Double amount, @Param("counterpartyType") VehicleTypeEnum counterpartyType, @Param("legalId") UUID legalId);
}
