package com.doing.nemo.claims.repository;


import com.doing.nemo.claims.entity.enumerated.ClaimsRepairEnum;
import com.doing.nemo.claims.entity.settings.AttachmentTypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface AttachmentTypeRepository extends JpaRepository<AttachmentTypeEntity, UUID> {
    @Query("SELECT a FROM AttachmentTypeEntity a WHERE a.typeComplaint = :typeComplaint")
    List<AttachmentTypeEntity> searchAttachmentTypeByTypeComplaint(@Param("typeComplaint") ClaimsRepairEnum typeComplaint);
}
