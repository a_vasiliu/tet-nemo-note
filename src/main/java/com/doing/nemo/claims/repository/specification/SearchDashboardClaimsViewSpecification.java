package com.doing.nemo.claims.repository.specification;

import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.entity.ClaimsAuthorityEmbeddedEntity;
import com.doing.nemo.claims.entity.ClaimsNewEntity;
import com.doing.nemo.claims.entity.ClaimsNewEntity_;
import com.doing.nemo.claims.entity.claims.*;
import com.doing.nemo.claims.entity.enumerated.AuthorityEnum.AuthorityEventTypeEnum;
import com.doing.nemo.claims.entity.view.SearchDashboardClaimsView;
import com.doing.nemo.claims.entity.view.SearchDashboardClaimsView_;
import com.doing.nemo.claims.util.Util;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.time.Instant;
import java.util.Collection;
import java.util.List;

public class SearchDashboardClaimsViewSpecification {

    public Specification<SearchDashboardClaimsView> getPreFilter(Instant datePreFilter){
        return new Specification<SearchDashboardClaimsView>() {
            @Override
            public Predicate toPredicate(Root<SearchDashboardClaimsView> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                if (datePreFilter==null){
                    return cb.conjunction();
                }

                return cb.or(
                            cb.between(
                                     root.get(SearchDashboardClaimsView_.createdAt),
                                     cb.literal(datePreFilter),
                                     cb.literal((DateUtil.getNowInstant()))
                                     ),
                            cb.between(
                                    root.get(SearchDashboardClaimsView_.updatedAt),
                                    cb.literal(datePreFilter),
                                    cb.literal((DateUtil.getNowInstant()))
                            )
                       );

            }

        };

    }
    public Specification<SearchDashboardClaimsView> getDateAccidentBetween(Instant dateAccidentFrom, Instant dateAccidentTo) {
        return new Specification<SearchDashboardClaimsView>() {
            @Override
            public Predicate toPredicate(Root<SearchDashboardClaimsView> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                if (dateAccidentFrom == null&&dateAccidentTo==null) {
                    return cb.conjunction();
                }

                return cb.between
                        (root.get(SearchDashboardClaimsView_.dateAccident),
                                cb.literal(dateAccidentFrom),
                                cb.literal((dateAccidentTo)));
            }

        };
    }

    public Specification<SearchDashboardClaimsView> getGreaterThanOrEqualTo(Instant dateAccidentFrom){
        return new Specification<SearchDashboardClaimsView>() {
            @Override
            public Predicate toPredicate(Root<SearchDashboardClaimsView> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                return cb.greaterThanOrEqualTo(root.get(SearchDashboardClaimsView_.dateAccident),dateAccidentFrom);
            }
        };
    }

    public Specification<SearchDashboardClaimsView> getLessThanOrEqualTo(Instant dateAccidentTo){
        return new Specification<SearchDashboardClaimsView>() {
            @Override
            public Predicate toPredicate(Root<SearchDashboardClaimsView> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                return  cb.lessThanOrEqualTo(root.get(SearchDashboardClaimsView_.dateAccident),dateAccidentTo);
            }
        };
    }


    public Specification<SearchDashboardClaimsView> getEqualPlate(String plate) {
        return new Specification<SearchDashboardClaimsView>() {
            @Override
            public Predicate toPredicate(Root<SearchDashboardClaimsView> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                if (plate == null) {
                    return cb.conjunction();
                }
                return cb.like(root.get(SearchDashboardClaimsView_.plate), plate);
            }
        };
    }

    public Specification<SearchDashboardClaimsView> getEqualPracticeId(Long practiceId){
        return new Specification<SearchDashboardClaimsView>() {
            @Override
            public Predicate toPredicate(Root<SearchDashboardClaimsView> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                if (practiceId == null) {
                    return cb.conjunction();
                }
                return cb.equal(root.get(SearchDashboardClaimsView_.practiceId), practiceId);
            }
        };
    }

    public Specification<SearchDashboardClaimsView> isInEvidence(Boolean inEvidence){
        return new Specification<SearchDashboardClaimsView>() {
            @Override
            public Predicate toPredicate(Root<SearchDashboardClaimsView> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                if (inEvidence == null) {
                    return cb.conjunction();
                }
                return cb.equal(root.get(SearchDashboardClaimsView_.inEvidence),inEvidence);
            }
        };
    }

    public Specification<SearchDashboardClaimsView> isPoVariation(Boolean poVariation){
        return new Specification<SearchDashboardClaimsView>() {
            @Override
            public Predicate toPredicate(Root<SearchDashboardClaimsView> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                if (poVariation == null) {
                    return cb.conjunction();
                }
                return cb.equal(root.get(SearchDashboardClaimsView_.poVariation),poVariation);
            }
        };
    }



    public Specification<SearchDashboardClaimsView> statusIn(List<String>  statusIds){

        return new Specification<SearchDashboardClaimsView>() {
            @Override
            public Predicate toPredicate(Root<SearchDashboardClaimsView> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                final Path<String> group = root.get(SearchDashboardClaimsView_.status);
                return group.in(statusIds);
            }
        };
    }

    public Specification<SearchDashboardClaimsView> typeAccidentIn(List<String> typeAccidentIds){
        return new Specification<SearchDashboardClaimsView>() {
            @Override
            public Predicate toPredicate(Root<SearchDashboardClaimsView> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                final Path<String> group = root.get(SearchDashboardClaimsView_.typeAccident);
                return group.in(typeAccidentIds);
            }
        };
    }

    public Specification<SearchDashboardClaimsView> typeFlowIn(List<String> typeFlowIds){
        return new Specification<SearchDashboardClaimsView>() {
            @Override
            public Predicate toPredicate(Root<SearchDashboardClaimsView> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                final Path<String> group = root.get(SearchDashboardClaimsView_.type);
                return group.in(typeFlowIds);
            }
        };
    }


    public Specification<SearchDashboardClaimsView> withContinuation(Boolean continuation){
        return new Specification<SearchDashboardClaimsView>() {
            @Override
            public Predicate toPredicate(Root<SearchDashboardClaimsView> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                if (continuation == null) {
                    return cb.conjunction();
                }
                return cb.equal(root.get(SearchDashboardClaimsView_.withContinuation),continuation);
            }
        };
    }

  /*  public Specification<SearchDashboardClaimsView> isContinuation(Boolean continuation){
        return new Specification<SearchDashboardClaimsView>() {
            @Override
            public Predicate toPredicate(Root<SearchDashboardClaimsView> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

                Subquery<AntiTheftRequestView> antiTheftRequestSubQuery = query.subquery(AntiTheftRequestView.class);
                Root<AntiTheftRequestView> antiTheftRequestView = antiTheftRequestSubQuery.from(AntiTheftRequestView.class);
                antiTheftRequestSubQuery.select(antiTheftRequestView.get("claimId"));
                //antiTheftRequestSubQuery.where(cb.equal())

              //  antiTheftRequestSubQuery.where(cb.equal(antiTheftRequestView.get("isValidate"),"false"));

                final Path<String> group = root.get(SearchDashboardClaimsView_.id);

          *//*      if (continuation){
                    return group.in(antiTheftRequestSubQuery);
                }else{
                    return group.in(antiTheftRequestSubQuery).not();
                }*//*

                cb.and(
                        cb.equal(root.get(SearchDashboardClaimsView_.withContinuation),continuation),
                        cb.equal(antiTheftRequestView.get("isValidate"),"false")
                );

             if (continuation){
                    return group.in(antiTheftRequestSubQuery);
                }else{
                    return group.in(antiTheftRequestSubQuery).not();
                }

            }
        };
    }
*/


    public Specification<SearchDashboardClaimsView> waitingForNumberSx(Boolean waitingForNumberSx){
        return new Specification<SearchDashboardClaimsView>() {
            @Override
            public Predicate toPredicate(Root<SearchDashboardClaimsView> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                query.distinct(true);

                Join<SearchDashboardClaimsView, ClaimsFromCompanyEntity> claimsFromCompanyJoin =  root.join(SearchDashboardClaimsView_.claimsFromCompanyEntity, JoinType.LEFT);

                if (waitingForNumberSx!=null){
                    return     cb.or(
                            cb.isNull(claimsFromCompanyJoin.get("numberSx")),
                            cb.equal(claimsFromCompanyJoin.get("numberSx"),"")

                    );


    /*
                    query.distinct(true);
                    Root<SearchDashboardClaimsView> claim = root;
                    Subquery<ClaimsFromCompanyEntity> claimsFromCompanySubQuery = query.subquery(ClaimsFromCompanyEntity.class);
                    Root<ClaimsFromCompanyEntity> claimsFromCompany = claimsFromCompanySubQuery.from(ClaimsFromCompanyEntity.class);
                    Expression<Collection<SearchDashboardClaimsView>> claimsFromCompanyToClaims = claimsFromCompany.get("claim");
                    claimsFromCompanySubQuery.select(claimsFromCompany);

                    if (waitingForNumberSx) {
                        claimsFromCompanySubQuery.where(
                                //whereCondition.append(" AND (fc.number_sx IS NULL OR fc.number_sx = '') ");
                                cb.or(
                                        cb.isNull(claimsFromCompany.get("numberSx")),
                                        cb.equal(claimsFromCompany.get("numberSx"),"")
                                )
                        );
                    } else{

                        //whereCondition.append(" AND (fc.number_sx IS NOT NULL AND fc.number_sx != '') ");
                        claimsFromCompanySubQuery.where(
                                cb.and(
                                        cb.isNotNull(claimsFromCompany.get("numberSx")),
                                        cb.notEqual(claimsFromCompany.get("numberSx"),"")
                                )
                        );

                    }

                    final Path<String> group = root.get(SearchDashboardClaimsView_.id);
                    return group.in(claimsFromCompanySubQuery);*/


                }else{
                    return cb.conjunction();
                }

            }
        };
    }


    public Specification<SearchDashboardClaimsView> waitingForAuthority(Boolean waitingForAuthority){
        return new Specification<SearchDashboardClaimsView>() {
            @Override
            public Predicate toPredicate(Root<SearchDashboardClaimsView> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

                query.distinct(true);
                Join<SearchDashboardClaimsView, ClaimsAuthorityEmbeddedEntity> claimsToClaimsAuthorityJoin = root.join("claimsAuthorityEmbeddedEntities");
                Join<ClaimsAuthorityEmbeddedEntity,ClaimsAuthorityEntity> claimsAuthorityJoin = claimsToClaimsAuthorityJoin.join("claimsAuthorityEntity");

               // query.select(claimsToClaimsAuthorityJoin.get("claims").get("id"));

                if (waitingForAuthority) {
                    //  whereCondition.append(" AND (cat.is_invoiced is null or cat.is_invoiced <> 'true') AND (cat.event_type is null or cat.event_type <> 'REJECT') ");
;
                      return   cb.and(
                                    cb.or(
                                            cb.isNull(claimsAuthorityJoin.get("isInvoiced")),
                                            cb.isFalse(claimsAuthorityJoin.get("isInvoiced"))
                                    ),
                                    cb.or(
                                            cb.isNull(claimsAuthorityJoin.get("eventType")),
                                            cb.notEqual(claimsAuthorityJoin.get("eventType"), AuthorityEventTypeEnum.REJECT)
                                    )
                            );

                } else{
                    return   cb.and(
                                   cb.equal(root.get(SearchDashboardClaimsView_.status), "WAITING_FOR_AUTHORITY"),
                                   cb.isTrue(claimsAuthorityJoin.get("isInvoiced"))
                                );
                }

            }
        };
    }

   /* public Specification<SearchDashboardClaimsView> waitingForAuthority(Boolean waitingForAuthority){
        return new Specification<SearchDashboardClaimsView>() {
            @Override
            public Predicate toPredicate(Root<SearchDashboardClaimsView> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                query.distinct(true);
                Root<SearchDashboardClaimsView> claim = root;

                Subquery<ClaimsAuthorityEmbeddedEntity> claimstoClaimsAuthoritySubQuery = query.subquery(ClaimsAuthorityEmbeddedEntity.class);
                Root<ClaimsAuthorityEmbeddedEntity> claimstoClaimsAuthority = claimstoClaimsAuthoritySubQuery.from(ClaimsAuthorityEmbeddedEntity.class);
                claimstoClaimsAuthoritySubQuery.select(claimstoClaimsAuthority);


                Join<ClaimsAuthorityEmbeddedEntity,ClaimsAuthorityEntity> claimsAuthorityEntity = claimstoClaimsAuthority.join("claimsAuthorityEntity");

                if (waitingForAuthority) {
                    //  whereCondition.append(" AND (cat.is_invoiced is null or cat.is_invoiced <> 'true') AND (cat.event_type is null or cat.event_type <> 'REJECT') ");
                    claimstoClaimsAuthoritySubQuery.select(claimstoClaimsAuthority);
                    claimstoClaimsAuthoritySubQuery.where(
                    cb.and(
                                  cb.or(
                                          cb.isNull(claimsAuthorityEntity.get("isInvoiced")),
                                          cb.isFalse(claimsAuthorityEntity.get("isInvoiced"))
                                  )
                    ),
                    cb.and(
                            cb.or(
                                    cb.isNull(claimsAuthorityEntity.get("eventType")),
                                    cb.notEqual(claimsAuthorityEntity.get("event_type"),"REJECT")
                            )
                    ));
                }else{
                    claimstoClaimsAuthoritySubQuery.select(claimstoClaimsAuthority);
                    // whereCondition.append(" AND claims.status = 'WAITING_FOR_AUTHORITY' AND cat.is_invoiced = true ");
                    claimstoClaimsAuthoritySubQuery.where(
                        cb.and(cb.equal(root.get(SearchDashboardClaimsView_.status), "WAITING_FOR_AUTHORITY")),
                        cb.and(cb.isTrue(claimsAuthorityEntity.get("isInvoiced")))
                    );
                }


             *//*   final Path<String> group = root.get(SearchDashboardClaimsView_.id);
                return group.in(claimstoClaimsAuthoritySubQuery);*//*
                return cb.exists(claimstoClaimsAuthoritySubQuery);


            }
        };
    }*/

 /*

        if(waitingForAuthority != null){
        fromCondition.append(" inner join claims_to_claims_authority ca on claims.id = ca.claims_id inner join claims_authority cat on ca.authority_id = cat.id ");
        if(waitingForAuthority) {
            whereCondition.append(" AND (cat.is_invoiced is null or cat.is_invoiced <> 'true') AND (cat.event_type is null or cat.event_type <> 'REJECT') ");
        }else
            whereCondition.append(" AND claims.status = 'WAITING_FOR_AUTHORITY' AND cat.is_invoiced = true ");
    }*/



}

