package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.entity.settings.BrokerEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.List;

@Repository
public class BrokerRepositoryV1 {

    @PersistenceContext
    @Autowired
    private EntityManager em;
    
    public List<BrokerEntity> findBrokersFiltered(Integer page, Integer pageSize) {
        String query = new String();
        query+=" SELECT * FROM broker b ";
        query+=" LIMIT " + pageSize + " OFFSET "+(page-1)*pageSize;

        Query sql = em.createNativeQuery(query, BrokerEntity.class);
        //resetQuery();
        return sql.getResultList();

    }
    
    public BigInteger countBrokerFiltered() {
        String query = new String();
        query+="SELECT COUNT(*) FROM broker b ";

        Query sql = em.createNativeQuery(query);
        //resetQuery();
        return (BigInteger)sql.getSingleResult();

    }


    





}
