package com.doing.nemo.claims.repository;


import com.doing.nemo.claims.entity.ClaimsEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface ClaimsRepository extends JpaRepository<ClaimsEntity, String> {

    @Query("SELECT c FROM ClaimsEntity c WHERE c.practiceId = :practiceId")
    ClaimsEntity searchClaimsByPractice(@Param("practiceId") Long practiceId);

    @Query(value = "SELECT c FROM ClaimsEntity c WHERE c.practiceId = :practiceId")
    ClaimsEntity getClaimsByIdPratica( @Param("practiceId") Long practiceId );

    @Query(value = "SELECT c FROM ClaimsEntity c ORDER BY c.practiceId ASC")
    List<ClaimsEntity> getAllClaims();

    @Query(value = "SELECT c FROM ClaimsEntity c WHERE cast(c.createdAt AS date) = :now AND cast(c.createdAt AS date) = cast(c.updateAt AS date)  ")
    List<ClaimsEntity> getClaimsCreatedNow(@Param("now") Date now);

    @Query(value = "SELECT c FROM ClaimsEntity c WHERE :now = cast(c.updateAt AS date) AND cast(c.createdAt as date) < cast(c.updateAt as date) ")
    List<ClaimsEntity> getClaimsUpdateNow(@Param("now") Date now);




/*    @Query(value = "SELECT c FROM ClaimsEntity c WHERE :now = cast(c.updateAt AS date) AND cast(c.createdAt AS date) <> cast(c.updateAt AS date)")
    List<ClaimsEntity> getClaimsUpdateNow(@Param("now") Date now);*/


}
