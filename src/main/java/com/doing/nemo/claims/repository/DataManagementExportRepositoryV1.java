package com.doing.nemo.claims.repository;


import com.doing.nemo.claims.entity.settings.DataManagementExportEntity;
import com.doing.nemo.claims.entity.settings.DataManagementExportEntity_;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class DataManagementExportRepositoryV1 {
   
    @PersistenceContext
    @Autowired
    private EntityManager em;

    public List<DataManagementExportEntity> findPaginationDataManagementExport(Integer page, Integer pageSize, String typeExport){
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<DataManagementExportEntity> dataManagementExportCriteriaQuery = criteriaBuilder.createQuery(DataManagementExportEntity.class);
        Root<DataManagementExportEntity> dataManagementExportRoot = dataManagementExportCriteriaQuery.from(DataManagementExportEntity.class);
        dataManagementExportCriteriaQuery.select(dataManagementExportRoot);

        Predicate predicate = criteriaBuilder.equal(dataManagementExportRoot.get(DataManagementExportEntity_.description), typeExport.toUpperCase());
        dataManagementExportCriteriaQuery.where(predicate);

        dataManagementExportCriteriaQuery.orderBy(criteriaBuilder.desc(dataManagementExportRoot.get(DataManagementExportEntity_.date)));

        List<DataManagementExportEntity> dataManagementExportEntityList = em.createQuery(dataManagementExportCriteriaQuery).setFirstResult((page-1)*pageSize).setMaxResults(pageSize).getResultList();
        return dataManagementExportEntityList;
    }

    public Long countPaginationDataManagementExport(String typeExport) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<Long> dataManagementExportCriteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<DataManagementExportEntity> dataManagementExportRoot = dataManagementExportCriteriaQuery.from(DataManagementExportEntity.class);
        dataManagementExportCriteriaQuery.select(criteriaBuilder.count(dataManagementExportRoot));

        Predicate predicate = criteriaBuilder.equal(dataManagementExportRoot.get(DataManagementExportEntity_.description), typeExport.toUpperCase());
        dataManagementExportCriteriaQuery.where(predicate);

        Query sql = em.createQuery(dataManagementExportCriteriaQuery);
        return (Long)sql.getSingleResult();
    }
}
