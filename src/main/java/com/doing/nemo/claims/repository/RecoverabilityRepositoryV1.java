package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.settings.ClaimsTypeEntity;
import com.doing.nemo.claims.entity.settings.ClaimsTypeEntity_;
import com.doing.nemo.claims.entity.settings.RecoverabilityEntity;
import com.doing.nemo.claims.entity.settings.RecoverabilityEntity_;
import com.doing.nemo.claims.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class RecoverabilityRepositoryV1 {
    @PersistenceContext
    @Autowired
    private EntityManager em;


    public List<RecoverabilityEntity> findPaginationRecoverability(Integer page, Integer pageSize, String management, DataAccidentTypeAccidentEnum dataAccidentTypeAccident, Boolean counterpart, Boolean recoverability, Double percentRecoverability, Boolean isActive, String orderBy, Boolean asc){

        Map<String, String> mapOrder = new HashMap<>();
        mapOrder.put("management", "management");
        mapOrder.put("claims_accident_type", "claimsAccidentType");
        mapOrder.put("counterpart", "counterpart");
        mapOrder.put("recoverability", "recoverability");
        mapOrder.put("percent_recoverability", "percentRecoverability");
        mapOrder.put("is_active", "isActive");


        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<RecoverabilityEntity> recoverabilityCriteriaQuery = criteriaBuilder.createQuery(RecoverabilityEntity.class);
        Root<RecoverabilityEntity> recoverabilityRoot = recoverabilityCriteriaQuery.from(RecoverabilityEntity.class);
        Join<RecoverabilityEntity, ClaimsTypeEntity> recoverabilityJoin = recoverabilityRoot.join(RecoverabilityEntity_.claimsType, JoinType.LEFT);

        recoverabilityCriteriaQuery.select(recoverabilityRoot); 

        //Constructing list of parameters
        List<Predicate> predicates = new ArrayList<>();
        if (Util.isNotEmpty(management)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    recoverabilityRoot.get(RecoverabilityEntity_.management)), "%"+management.toUpperCase()+"%"));
        }
        if (dataAccidentTypeAccident != null) {
            predicates.add(criteriaBuilder.equal(
                    recoverabilityJoin.get(ClaimsTypeEntity_.claimsAccidentType), dataAccidentTypeAccident));
        }
        if (counterpart != null) {
            predicates.add(criteriaBuilder.equal(
                    recoverabilityRoot.get(RecoverabilityEntity_.counterpart), counterpart));
        }
        if (isActive != null) {
            predicates.add(criteriaBuilder.equal(
                    recoverabilityRoot.get(RecoverabilityEntity_.isActive), isActive));
        }
        if (percentRecoverability != null) {
            predicates.add(criteriaBuilder.equal(
                    recoverabilityRoot.get(RecoverabilityEntity_.percentRecoverability), percentRecoverability));
        }
        if (recoverability != null) {
            predicates.add(criteriaBuilder.equal(
                    recoverabilityRoot.get(RecoverabilityEntity_.recoverability), recoverability));
        }
        recoverabilityCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));
        
        //List<Order> orderByList = new LinkedList<>();
        String fieldToOrder = null;
        if(orderBy != null)
            fieldToOrder = mapOrder.get(orderBy);


        if(fieldToOrder != null && asc != null){
            if (asc) {

                if(fieldToOrder.equalsIgnoreCase("claimsAccidentType"))
                    recoverabilityCriteriaQuery.orderBy(criteriaBuilder.asc(recoverabilityJoin.get(fieldToOrder)));
                else
                    recoverabilityCriteriaQuery.orderBy(criteriaBuilder.asc(recoverabilityRoot.get(fieldToOrder)));
            } else {

                if(fieldToOrder.equalsIgnoreCase("claimsAccidentType"))
                    recoverabilityCriteriaQuery.orderBy(criteriaBuilder.desc(recoverabilityJoin.get(fieldToOrder)));
                else
                    recoverabilityCriteriaQuery.orderBy(criteriaBuilder.desc(recoverabilityRoot.get(fieldToOrder)));
            }

        } else if(fieldToOrder != null) {

            if(fieldToOrder.equalsIgnoreCase("claimsAccidentType"))
                recoverabilityCriteriaQuery.orderBy(criteriaBuilder.desc(recoverabilityJoin.get(fieldToOrder)));
            else
                recoverabilityCriteriaQuery.orderBy(criteriaBuilder.desc(recoverabilityRoot.get(fieldToOrder)));
        }

        List<RecoverabilityEntity> recoverabilityEntityList = em.createQuery(recoverabilityCriteriaQuery).setFirstResult((page-1)*pageSize).setMaxResults(pageSize).getResultList();
        return recoverabilityEntityList;
    }

    public Long countPaginationRecoverability(String management, DataAccidentTypeAccidentEnum dataAccidentTypeAccident, Boolean counterpart, Boolean recoverability, Double percentRecoverability, Boolean isActive) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<Long> recoverabilityCriteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<RecoverabilityEntity> recoverabilityRoot = recoverabilityCriteriaQuery.from(RecoverabilityEntity.class);
        Join<RecoverabilityEntity, ClaimsTypeEntity> recoverabilityJoin = recoverabilityRoot.join(RecoverabilityEntity_.claimsType, JoinType.LEFT);

        recoverabilityCriteriaQuery.select(criteriaBuilder.count(recoverabilityRoot));

        //Constructing list of parameters
        List<Predicate> predicates = new ArrayList<>();

        if (Util.isNotEmpty(management)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    recoverabilityRoot.get(RecoverabilityEntity_.management)), "%"+management.toUpperCase()+"%"));
        }
        if (dataAccidentTypeAccident != null) {
            predicates.add(criteriaBuilder.equal(
                    recoverabilityJoin.get(ClaimsTypeEntity_.claimsAccidentType), dataAccidentTypeAccident));
        }
        if (counterpart != null) {
            predicates.add(criteriaBuilder.equal(
                    recoverabilityRoot.get(RecoverabilityEntity_.counterpart), counterpart));
        }
        if (isActive != null) {
            predicates.add(criteriaBuilder.equal(
                    recoverabilityRoot.get(RecoverabilityEntity_.isActive), isActive));
        }
        if (percentRecoverability != null) {
            predicates.add(criteriaBuilder.equal(
                    recoverabilityRoot.get(RecoverabilityEntity_.percentRecoverability), percentRecoverability));
        }
        if (recoverability != null) {
            predicates.add(criteriaBuilder.equal(
                    recoverabilityRoot.get(RecoverabilityEntity_.recoverability), recoverability));
        }
        recoverabilityCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));

        Query sql = em.createQuery(recoverabilityCriteriaQuery);
        return (Long)sql.getSingleResult();

    }
}
