package com.doing.nemo.claims.repository;


import com.doing.nemo.claims.entity.enumerated.ClaimsRepairEnum;
import com.doing.nemo.claims.entity.enumerated.EventTypeEnum;
import com.doing.nemo.claims.entity.settings.EventTypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface EventTypeRepository extends JpaRepository<EventTypeEntity, UUID> {
    @Query("SELECT e FROM EventTypeEntity e WHERE e.eventType = :eventType")
    EventTypeEntity searchEventTypeByType(@Param("eventType") EventTypeEnum eventType);

    @Query("SELECT e FROM EventTypeEntity e WHERE e.typeComplaint = :typeComplaint")
    List<EventTypeEntity> searchEventTypeByTypeComplaint(@Param("typeComplaint") ClaimsRepairEnum typeComplaint);
}
