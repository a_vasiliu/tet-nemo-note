package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.entity.settings.InsuranceCompanyEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface InsuranceCompanyRepository extends JpaRepository<InsuranceCompanyEntity, UUID> {

    @Query(value = "select i from InsuranceCompanyEntity i where i.name = :name")
    InsuranceCompanyEntity findByCardCompanyByName(@Param("name") String name);

    @Query(value = "select i from InsuranceCompanyEntity i where i.name = :name")
    List<InsuranceCompanyEntity> findAllByCardCompanyByName(@Param("name") String name);

    @Query("SELECT i FROM InsuranceCompanyEntity i WHERE i.name = :name")
    InsuranceCompanyEntity searchInsuranceCompanybyName(@Param("name") String name);

    @Query("SELECT i FROM InsuranceCompanyEntity i WHERE i.name = :name AND i.id <> :id")
    InsuranceCompanyEntity searchInsuranceCompanybyNameWithDifferentId(@Param("name") String name, @Param("id") UUID id);

    @Query("SELECT i FROM InsuranceCompanyEntity i order by i.id")
    List<InsuranceCompanyEntity> selectAllByOrderAsc();

    @Query("SELECT i FROM InsuranceCompanyEntity i WHERE i.aniaCode = :aniaCode")
    List<InsuranceCompanyEntity> searchByAniaCode(@Param("aniaCode") Integer aniaCode);

}
