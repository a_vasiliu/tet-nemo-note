package com.doing.nemo.claims.repository;


import com.doing.nemo.claims.entity.settings.SubgroupIdsEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface SubgroupIdsRepository extends JpaRepository<SubgroupIdsEntity, UUID> {
}
