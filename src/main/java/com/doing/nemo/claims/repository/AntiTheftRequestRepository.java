package com.doing.nemo.claims.repository;


import com.doing.nemo.claims.entity.AntiTheftRequestEntity;
import com.doing.nemo.claims.entity.settings.AntiTheftServiceEntity;
import com.doing.nemo.claims.entity.settings.ContractEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface AntiTheftRequestRepository extends JpaRepository<AntiTheftRequestEntity, String> {

    @Query(value = "SELECT a FROM AntiTheftRequestEntity a  WHERE a.claim.id = :claimId ")
    List<AntiTheftRequestEntity> getAllCrashByClaim(@Param("claimId") String  claimId );
}
