package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.entity.settings.PracticeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface PracticeRepository extends JpaRepository<PracticeEntity, UUID> {

    @Query("SELECT p FROM PracticeEntity p ORDER BY p.practiceId ASC , p.createdAt ASC")
    List<PracticeEntity> searchPractice();

}
