package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.entity.enumerated.PolicyEnum.PolicyTypeEnum;
import com.doing.nemo.claims.entity.settings.InsurancePolicyEntity;
import com.doing.nemo.claims.entity.settings.InsurancePolicyEntity_;
import com.doing.nemo.claims.service.impl.ClaimsServiceImpl;
import com.doing.nemo.claims.util.Util;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.*;

@Repository
public class InsurancePolicyRepositoryV1 {
    @PersistenceContext
    @Autowired
    private EntityManager em;

    private static Logger LOGGER = LoggerFactory.getLogger(ClaimsServiceImpl.class);

    public List<InsurancePolicyEntity> findInsurancePolicy(String dateValidity, Boolean isActive){

        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<InsurancePolicyEntity> insurancePolicyCriteriaQuery = criteriaBuilder.createQuery(InsurancePolicyEntity.class);
        Root<InsurancePolicyEntity> insurancePolicyRoot = insurancePolicyCriteriaQuery.from(InsurancePolicyEntity.class);

        insurancePolicyCriteriaQuery.select(insurancePolicyRoot);

        //Constructing list of parameters
        List<Predicate> predicates = new ArrayList<>();

        if(Util.isNotEmpty(dateValidity)){
                if(!dateValidity.contains("T"))
                    dateValidity+="T00:00:00Z";
                Instant dateValidityInstant = DateUtil.convertIS08601StringToUTCInstant(dateValidity);
                //Date beginningValidityFormat=new SimpleDateFormat("yyyy-MM-dd").parse(beginningValidity);
                predicates.add(criteriaBuilder.lessThanOrEqualTo(
                        insurancePolicyRoot.get(InsurancePolicyEntity_.beginningValidity), dateValidityInstant));
                predicates.add(criteriaBuilder.greaterThanOrEqualTo(
                    insurancePolicyRoot.get(InsurancePolicyEntity_.endValidity), dateValidityInstant));
        }

        if (isActive != null) {
            predicates.add(criteriaBuilder.equal(
                    insurancePolicyRoot.get(InsurancePolicyEntity_.isActive), isActive));
        }

        insurancePolicyCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));

        //List<Order> orderByList = new LinkedList<>();
        String fieldToOrder = null;



        List<InsurancePolicyEntity> insurancePolicyEntityList = em.createQuery(insurancePolicyCriteriaQuery).getResultList();
        return insurancePolicyEntityList;
    }


    public List<InsurancePolicyEntity> findPaginationInsurancePolicy(Integer page, Integer pageSize, String orderBy, Boolean asc, PolicyTypeEnum type, Long clientCode, String client, String numberPolicy, String beginningValidity, String endValidity, String bookRegisterCode, Boolean isActive){
        String query = new String();
        Map<String, String> mappOrder = new HashMap<>();
        mappOrder.put("type", "type");
        mappOrder.put("client_code", "clientCode");
        mappOrder.put("client", "client");
        mappOrder.put("number_policy", "numberPolicy");
        mappOrder.put("beginning_validity", "beginningValidity");
        mappOrder.put("end_validity", "endValidity");
        mappOrder.put("book_register_code", "bookRegisterCode");
        mappOrder.put("is_active", "isActive");

        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<InsurancePolicyEntity> insurancePolicyCriteriaQuery = criteriaBuilder.createQuery(InsurancePolicyEntity.class);
        Root<InsurancePolicyEntity> insurancePolicyRoot = insurancePolicyCriteriaQuery.from(InsurancePolicyEntity.class);

        insurancePolicyCriteriaQuery.select(insurancePolicyRoot);

        //Constructing list of parameters
        List<Predicate> predicates = new ArrayList<>();

        if (type != null) {
            predicates.add(criteriaBuilder.equal(
                    insurancePolicyRoot.get(InsurancePolicyEntity_.type), type));
        }
        if (clientCode!= null) {
            predicates.add(criteriaBuilder.equal(
                    insurancePolicyRoot.get(InsurancePolicyEntity_.clientCode), clientCode));
        }
        if (Util.isNotEmpty(client)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    insurancePolicyRoot.get(InsurancePolicyEntity_.client)), "%"+client.toUpperCase()+"%"));
        }
        if (Util.isNotEmpty(numberPolicy)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    insurancePolicyRoot.get(InsurancePolicyEntity_.numberPolicy)), "%"+numberPolicy.toUpperCase()+"%"));
        }
        if(Util.isNotEmpty(beginningValidity)){
            try {
                if(!beginningValidity.contains("T"))
                    beginningValidity+="T00:00:00Z";
                Instant beginningValidityInstant = DateUtil.convertIS08601StringToUTCInstant(beginningValidity);
                Date beginningValidityFormat=new SimpleDateFormat("yyyy-MM-dd").parse(beginningValidity);
                predicates.add(criteriaBuilder.lessThanOrEqualTo(
                        insurancePolicyRoot.get(InsurancePolicyEntity_.beginningValidity), beginningValidityInstant));
            } catch (ParseException e) {
                LOGGER.debug(e.getMessage());
            }
        }
        if(Util.isNotEmpty(endValidity)){
            try {
                if(!endValidity.contains("T"))
                    endValidity+="T00:00:00Z";
                Instant endValidityInstant = DateUtil.convertIS08601StringToUTCInstant(endValidity);
                Date endValidityFormat=new SimpleDateFormat("yyyy-MM-dd").parse(endValidity);
                predicates.add(criteriaBuilder.greaterThanOrEqualTo(
                        insurancePolicyRoot.get(InsurancePolicyEntity_.endValidity), endValidityInstant));
            } catch (ParseException e) {
                LOGGER.debug(e.getMessage());
            }
        }
        if (Util.isNotEmpty(bookRegisterCode)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    insurancePolicyRoot.get(InsurancePolicyEntity_.bookRegisterCode)), "%"+bookRegisterCode.toUpperCase()+"%"));
        }
        if (isActive != null) {
            predicates.add(criteriaBuilder.equal(
                    insurancePolicyRoot.get(InsurancePolicyEntity_.isActive), isActive));
        }

        insurancePolicyCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));

        //List<Order> orderByList = new LinkedList<>();
        String fieldToOrder = null;
        if(orderBy != null)
            fieldToOrder = mappOrder.get(orderBy);
        if(fieldToOrder != null && asc != null){
            if (asc) {
                insurancePolicyCriteriaQuery.orderBy(criteriaBuilder.asc(insurancePolicyRoot.get(fieldToOrder)));
            } else {
                insurancePolicyCriteriaQuery.orderBy(criteriaBuilder.desc(insurancePolicyRoot.get(fieldToOrder)));
            }

        } else if(fieldToOrder != null) {
            insurancePolicyCriteriaQuery.orderBy(criteriaBuilder.desc(insurancePolicyRoot.get(fieldToOrder)));
        }


        List<InsurancePolicyEntity> insurancePolicyEntityList = em.createQuery(insurancePolicyCriteriaQuery).setFirstResult((page-1)*pageSize).setMaxResults(pageSize).getResultList();
        return insurancePolicyEntityList;
    }

    public Long countPaginationInsurancePolicy(PolicyTypeEnum type, Long clientCode, String client, String numberPolicy, String beginningValidity, String endValidity, String bookRegisterCode, Boolean isActive) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<Long> insurancePolicyCriteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<InsurancePolicyEntity> insurancePolicyRoot = insurancePolicyCriteriaQuery.from(InsurancePolicyEntity.class);

        insurancePolicyCriteriaQuery.select(criteriaBuilder.count(insurancePolicyRoot));

        //Constructing list of parameters
        List<Predicate> predicates = new ArrayList<>();

        if (type != null) {
            predicates.add(criteriaBuilder.equal(
                    insurancePolicyRoot.get(InsurancePolicyEntity_.type), type));
        }
        if (clientCode!= null) {
            predicates.add(criteriaBuilder.equal(
                    insurancePolicyRoot.get(InsurancePolicyEntity_.clientCode), clientCode));
        }
        if (Util.isNotEmpty(client)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    insurancePolicyRoot.get(InsurancePolicyEntity_.client)), "%"+client.toUpperCase()+"%"));
        }
        if (Util.isNotEmpty(numberPolicy)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    insurancePolicyRoot.get(InsurancePolicyEntity_.numberPolicy)), "%"+numberPolicy.toUpperCase()+"%"));
        }
        if(Util.isNotEmpty(beginningValidity)){
            if(!beginningValidity.contains("T"))
                beginningValidity+="T00:00:00Z";
            Instant beginningValidityInstant = DateUtil.convertIS08601StringToUTCInstant(beginningValidity);
            predicates.add(criteriaBuilder.lessThanOrEqualTo(
                    insurancePolicyRoot.get(InsurancePolicyEntity_.beginningValidity), beginningValidityInstant));

        }
        if(Util.isNotEmpty(endValidity)){
            if(!endValidity.contains("T"))
                endValidity+="T00:00:00Z";
            Instant endValidityInstant = DateUtil.convertIS08601StringToUTCInstant(endValidity);
            predicates.add(criteriaBuilder.greaterThanOrEqualTo(
                    insurancePolicyRoot.get(InsurancePolicyEntity_.endValidity), endValidityInstant));

        }
        if (Util.isNotEmpty(bookRegisterCode)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    insurancePolicyRoot.get(InsurancePolicyEntity_.bookRegisterCode)), "%"+bookRegisterCode.toUpperCase()+"%"));
        }
        if (isActive != null) {
            predicates.add(criteriaBuilder.equal(
                    insurancePolicyRoot.get(InsurancePolicyEntity_.isActive), isActive));
        }

        insurancePolicyCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));
        Query sql = em.createQuery(insurancePolicyCriteriaQuery);
        return (Long)sql.getSingleResult();

    }
}
