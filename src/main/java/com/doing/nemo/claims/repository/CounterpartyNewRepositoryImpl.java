package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.dto.CtpExportFilter;
import com.doing.nemo.claims.entity.*;
import com.doing.nemo.claims.entity.enumerated.DTO.CounterpartyStats;
import com.doing.nemo.claims.entity.enumerated.DamagedEnum.VehicleEnum.VehicleTypeEnum;
import com.doing.nemo.claims.entity.enumerated.RepairEnum.RepairStatusEnum;
import com.doing.nemo.claims.util.Util;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.BadParametersException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.Tuple;
import javax.persistence.criteria.*;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.doing.nemo.claims.entity.jsonb.JsonbProcessingFunctions.JSONB_EXTRACT_PATH_TEXT;

@Repository
public class CounterpartyNewRepositoryImpl {

    @PersistenceContext
    @Autowired
    private EntityManager em;

    @Autowired
    private Util util;

    /*
    X	Targa ALD
    X	Targa CTP
    X	ID SX
    X	ID Repair
    X	Stato controparte
    X	Data Inserimento (arco temporale)
    ?	Data canalizzazione (arco temporale) // non la abbiamo
    ?   Conducente // counterparty.driver combo first name + last name?
    X	Ultimo contatto (arco temporale)
    ?   Operatore // assigner_to (dentro counterparty)
    X	Regione
    X	Città -> locality
    */


    //INIZIO INTEGRAZIONE CON AUTHORITY
    public List<CounterpartyNewEntity> getCounterpartyByPlate(String plate){
        StringBuilder sqlQuery = new StringBuilder();
        sqlQuery.append("SELECT * " +
                "FROM counterparty_new c ");

        StringBuilder whereCondition = new StringBuilder("WHERE  c.type = '" + VehicleTypeEnum.VEHICLE + "' AND c.eligibility = true ");
        if (Util.isNotEmpty(plate)) {
            whereCondition.append(" AND c.vehicle->>'license_plate' = '" + plate + "' ");

        }
        sqlQuery.append(whereCondition);
        Query query = em.createNativeQuery(sqlQuery.toString(), CounterpartyNewEntity.class);


        return query.getResultList();

    }

    public List<CounterpartyNewEntity> findCounterpartyForPaginationAuthority(String plate, String chassis, Integer page, Integer pageSize) {
        StringBuilder sql = new StringBuilder("SELECT * FROM counterparty_new c ");
        StringBuilder whereCondition = new StringBuilder(" WHERE c.type = '" + VehicleTypeEnum.VEHICLE + "' AND c.eligibility = true ");

        if (Util.isNotEmpty(plate)) {
            whereCondition.append(" AND c.vehicle->>'license_plate'  = '" + plate + "' ");
        }

        if (Util.isNotEmpty(chassis) && whereCondition.indexOf("AND") > 1) {
            whereCondition.append(" OR c.vehicle->>->>'chassis_number' = '" + chassis + "' ");
        } else if (Util.isNotEmpty(chassis)) {
            whereCondition.append(" AND c.vehicle->>->>'chassis_number' = '" + chassis + "' ");
        }

        sql.append(whereCondition);

        sql.append(" LIMIT " + pageSize + " OFFSET " + (page - 1) * pageSize);
        Query query = em.createNativeQuery(sql.toString(), CounterpartyNewEntity.class);

        return query.getResultList();
    }


    public BigInteger countCounterpartyForPaginationAuthority(String plate, String chassis) {
        StringBuilder sql = new StringBuilder("SELECT COUNT(*) FROM counterparty_new c ");
        StringBuilder whereCondition = new StringBuilder(" WHERE c.type = '" + VehicleTypeEnum.VEHICLE + "' AND c.eligibility = true ");

        if (Util.isNotEmpty(plate)) {
            whereCondition.append(" AND c.vehicle->>'license_plate'  = '" + plate + "' ");
        }

        if (Util.isNotEmpty(chassis) && whereCondition.indexOf("AND") > 1) {
            whereCondition.append(" OR c.vehicle->>->>'chassis_number' = '" + chassis + "' ");
        } else if (Util.isNotEmpty(chassis)) {
            whereCondition.append(" AND c.vehicle->>->>'chassis_number' = '" + chassis + "' ");
        }

        sql.append(whereCondition);
        Query query = em.createNativeQuery(sql.toString());

        return (BigInteger) query.getSingleResult();

    }

    public List<CounterpartyNewEntity> getCounterpartyToAttachAuthority(String authorityDossierId, String authorityWorkingId, List<String> counterpartyIdList) {
        String counterpartyIdString = "";
        for (String currentId : counterpartyIdList) {
            counterpartyIdString += "'" + currentId + "',";
        }

        if (!counterpartyIdString.equals("")) {
            counterpartyIdString = counterpartyIdString.substring(0, counterpartyIdString.length() - 1);
        }

        StringBuilder query = new StringBuilder("WITH AU AS ( SELECT counterparty_id, jsonb_array_elements(authorities) AS aut FROM counterparty_new c) ");
        query.append("SELECT * FROM counterparty_new c WHERE c.counterparty_id IN (SELECT DISTINCT c.counterparty_id " +
                "FROM counterparty_new c LEFT JOIN AU using(counterparty_id) " +
                "WHERE ( aut IS NULL OR (aut->>'authority_dossier_id' <> '" + authorityDossierId + "' AND aut->>'authority_working_id' <> '" + authorityWorkingId + "') OR (aut->>'authority_dossier_id' = '" + authorityDossierId + "' AND aut->>'authority_working_id' <> '" + authorityWorkingId + "')) AND c.counterparty_id  IN  (" + counterpartyIdString + ")" +
                " AND c.counterparty_id NOT IN (SELECT DISTINCT(c.counterparty_id) FROM counterparty c LEFT JOIN AU using(counterparty_id) WHERE aut->>'authority_dossier_id' = '" + authorityDossierId + "' AND aut->>'authority_working_id' = '" + authorityWorkingId + "' AND c.counterparty_id IN  (" + counterpartyIdString + ")))");

        Query sql = em.createNativeQuery(query.toString(), CounterpartyNewEntity.class);
        return sql.getResultList();
    }


    public List<CounterpartyNewEntity> getCounterpartyToDetattachAuthority(String authorityDossierId, String authorityWorkingId, List<String> counterpartyIdList) {
        String counterpartyIdString = "";


        for (String currentId : counterpartyIdList) {
            counterpartyIdString += "'" + currentId + "',";
        }
        StringBuilder query = new StringBuilder("WITH AU AS ( SELECT counterparty_id, jsonb_array_elements(authorities) AS aut FROM counterparty_new c) ");
        if (!counterpartyIdString.equals("")) {
            counterpartyIdString = counterpartyIdString.substring(0, counterpartyIdString.length() - 1);
            query.append("SELECT * " +
                    "FROM counterparty_new c LEFT JOIN AU using(counterparty_id) " +
                    "WHERE aut->>'authority_dossier_id' = '" + authorityDossierId + "' AND aut->>'authority_working_id' = '" + authorityWorkingId + "' AND c.counterparty_id NOT IN  (" + counterpartyIdString + ")");
        } else {

            query.append("SELECT * " +
                    "FROM counterparty_new c LEFT JOIN AU using(counterparty_id) " +
                    "WHERE aut->>'authority_dossier_id' = '" + authorityDossierId + "' AND aut->>'authority_working_id' = '" + authorityWorkingId + "'");
        }

        Query sql = em.createNativeQuery(query.toString(), CounterpartyNewEntity.class);


        return sql.getResultList();
    }

    public List<CounterpartyNewEntity> getCounterpartyToApproveAuthority(String authorityDossierId, String authorityWorkingId) {
        StringBuilder query = new StringBuilder("WITH AU AS ( SELECT counterparty_id, jsonb_array_elements(authorities) AS aut FROM counterparty_new c) ");

        query.append("SELECT * " +
                "FROM counterparty_new c LEFT JOIN AU using(counterparty_id) " +
                "WHERE aut->>'authority_dossier_id' = '" + authorityDossierId + "' AND aut->>'authority_working_id' = '" + authorityWorkingId + "'");

        Query sql = em.createNativeQuery(query.toString(), CounterpartyNewEntity.class);

        return sql.getResultList();
    }


    //FINE INTEGRAZIONE CON AUTHORITY


    //INIZIO INTEGRAZIONE MSA

    public List<CounterpartyNewEntity> findCounterpartyForPagination(int page, int pageSize, String denomination, String plate, Long practiceIdCounterparty, String statusRepair, String dateCreatedFrom, String dateCreatedTo, Boolean asc, String orderBy, String addressSx, String fromCompany, String dateSxFrom, String dateSxTo, String region, String province, String lastContact, String assignedTo, String locality, String recall, String centerEmail, Boolean msaPagination) {
        Map<String, String> mappOrder = new HashMap<>();
        mappOrder.put("license_plate", " t.vehicle\\:\\:jsonb->>'license_plate' ");
        mappOrder.put("denomination", " (t.insurance_company\\:\\:jsonb->>'entity')\\:\\:jsonb->>'name' ");
        mappOrder.put("practice_id", " t.practice_id ");
        mappOrder.put("created_at", " t.claims_created_at ");
        mappOrder.put("locator", " t.locator ");
        mappOrder.put("region", " t.region ");
        mappOrder.put("address", " t.formatted_address ");
        mappOrder.put("province", " t.province ");
        mappOrder.put("locality", " t.locality ");
        mappOrder.put("number_sx", " t.number_sx ");
        mappOrder.put("assigned_to", " t.assigned_to ");
        //mappOrder.put("last_call", " (count->>'last_contact')\\:\\:jsonb->>'last_call' ");
        //mappOrder.put("recall", " (count->>'last_contact')\\:\\:jsonb->>'recall' ");
        mappOrder.put("date_accident", " t.date_accident ");


        String auxiliaryTableCounterparty = "WITH" +
                " LC AS ( SELECT counterparty_id, jsonb_array_elements(counterparty_new.last_contact) last_cont FROM counterparty_new ) ";

        StringBuilder sqlQuery = new StringBuilder(auxiliaryTableCounterparty);
        sqlQuery.append("SELECT * FROM (SELECT DISTINCT ON (c.counterparty_id) c.*, " +
                "claims.id as id, " +
                "claims.practice_id, " +
                "claims.claim_locator as locator, " +
                "claims.claim_address_region as region," +
                "claims.claim_address_formatted  as address, " +
                "claims.claim_address_province as province, " +
                "fc.number_sx as number_sx, " +
                "claims.date_accident as date_accident, " +
                "claims.claim_address_locality as locality, " +
                "(claims.created_at)\\:\\:timestamp as claims_created_at " +
                "FROM counterparty_new c INNER JOIN claims_new claims on c.claims_id = claims.id LEFT JOIN LC USING (counterparty_id) LEFT JOIN claims_from_company fc on claims_id = fc.id ");

        StringBuilder whereCondition = new StringBuilder("WHERE  c.type = '" + VehicleTypeEnum.VEHICLE + "' AND c.eligibility = true ");

        if(msaPagination != null && msaPagination){
            whereCondition.append(" AND (c.is_read_msa IS NULL OR c.is_read_msa = false)");
        }
        if (Util.isNotEmpty(region)) {
            whereCondition.append(" AND claims.claim_address_region ILIKE '%" + region + "%' ");
        }
        if (Util.isNotEmpty(addressSx)) {
            whereCondition.append(" AND claims.claim_address_formatted ILIKE '%" + addressSx + "%' ");
        }
        if (Util.isNotEmpty(province)) {
            whereCondition.append(" AND claims.claim_address_province ILIKE '%" + province + "%' ");
        }
        if (Util.isNotEmpty(locality)) {
            whereCondition.append(" AND claims.locality ILIKE '%" + locality + "%' ");
        }
        if (Util.isNotEmpty(fromCompany)) {
            whereCondition.append(" AND fc.number_sx ILIKE '%" + fromCompany + "%' ");
        }
        if (Util.isNotEmpty(assignedTo)) {
            whereCondition.append(" AND c.assigned_to->>'id' =  '" + assignedTo + "' ");

        }
        if (Util.isNotEmpty(lastContact)) {
            whereCondition.append(" AND   (last_cont->>'last_call')\\:\\:timestamp >= ('" + lastContact + "')\\:\\:timestamp ");

        }

        if (Util.isNotEmpty(recall)) {
            whereCondition.append(" AND   (last_cont->>'recall')\\:\\:timestamp >= ('" + recall + "')\\:\\:timestamp ");

        }


        if (dateSxFrom != null && dateSxTo != null) {
            if (dateSxFrom.length() == 28) dateSxFrom = dateSxFrom.substring(0, 23);
            if (dateSxTo.length() == 28) dateSxTo = dateSxTo.substring(0, 23);
            if (dateSxTo.compareTo(dateSxFrom) < 0) {
                throw new BadParametersException("the date_sx_to is less than the date_sx_from", MessageCode.CLAIMS_1011);
            } else {
                whereCondition.append(" AND claims.data_accident\\:\\:timestamp >= '" + dateSxFrom + "'\\:\\:timestamp   AND claims.data_accident\\:\\:timestamp <= '" + dateSxTo + "'\\:\\:timestamp  ");
            }
        } else if ((dateSxFrom != null) || (dateSxTo != null)) {
            throw new BadParametersException("the date_sx_to and date_sx_from must be present together ", MessageCode.CLAIMS_1011);
        }


        if (dateCreatedFrom != null && dateCreatedTo != null) {
            if (dateCreatedFrom.length() == 28) dateCreatedFrom = dateCreatedFrom.substring(0, 23);
            if (dateCreatedTo.length() == 28) dateCreatedTo = dateCreatedTo.substring(0, 23);
            if (dateCreatedTo.compareTo(dateCreatedFrom) < 0) {
                throw new BadParametersException("the date_created_to is less than the date_created_from", MessageCode.CLAIMS_1011);
            } else {
                whereCondition.append(" AND ( ( (claims.created_at\\:\\:timestamp >= '" + dateCreatedFrom + "'\\:\\:timestamp  ) AND (claims.created_at\\:\\:timestamp <= '" + dateCreatedTo + "'\\:\\:timestamp ) ) ) ");
            }
        } else if ((dateCreatedFrom != null) || (dateCreatedTo != null)) {
            throw new BadParametersException("the date_created_to and date_created_from must be present together ", MessageCode.CLAIMS_1011);
        }

        if (Util.isNotEmpty(denomination)) {
            whereCondition.append(" AND c.insurance_company->>'name' ILIKE '%" + denomination + "%' ");
        }

        if (practiceIdCounterparty != null) {
            whereCondition.append(" AND c.practice_id_counterparty = " + practiceIdCounterparty);
        }

        if (statusRepair != null) {

            RepairStatusEnum repairStatusEnum = RepairStatusEnum.create(statusRepair);
            whereCondition.append(" AND ( lower(c.status_repair) ILIKE lower('" + repairStatusEnum.getValue() + "')) ");
        }

        if (util.isNotEmpty(plate)) {
            whereCondition.append(" AND ( c.vehicle->>'license_plate' ILIKE '%" + plate + "%') ");
        }
        sqlQuery.append(whereCondition);
        sqlQuery.append(" ORDER BY c.counterparty_id ) t");

        String fieldToOrder = null;
        if (orderBy != null)
            fieldToOrder = mappOrder.get(orderBy);
        if (fieldToOrder != null && asc != null) {
            if (asc) {
                sqlQuery.append(" ORDER BY " + fieldToOrder + " ASC");
            } else {
                sqlQuery.append(" ORDER BY " + fieldToOrder + " DESC");
            }

        } else if (fieldToOrder != null) {
            sqlQuery.append(" ORDER BY " + fieldToOrder + " DESC");
        }

        sqlQuery.append(" LIMIT " + pageSize + " OFFSET " + (page - 1) * pageSize);

        System.out.println(sqlQuery);

        Query query = em.createNativeQuery(sqlQuery.toString(), "CounterpartyNewResults");


        return query.getResultList();

    }


    public BigInteger countCounterpartyForPagination(int page, int pageSize, String denomination, String plate, Long practiceIdCounterparty, String statusRepair, String dateCreatedFrom, String dateCreatedTo, Boolean asc, String orderBy, String addressSx, String fromCompany, String dateSxFrom, String dateSxTo, String region, String province, String lastContact, String assignedTo, String locality, String recall, String centerEmail, Boolean msaPagination) {

        String auxiliaryTableCounterparty = "WITH" +
                " LC AS ( SELECT counterparty_id, jsonb_array_elements(counterparty_new.last_contact) last_cont FROM counterparty_new ) ";

        StringBuilder sqlQuery = new StringBuilder(auxiliaryTableCounterparty);
        sqlQuery.append("SELECT COUNT(*) FROM (SELECT DISTINCT ON (c.counterparty_id) c.*, " +
                "claims.id as id, " +
                "claims.practice_id, " +
                "claim_locator as locator, " +
                "claims.claim_address_region as region," +
                "claims.claim_address_formatted as address, " +
                "claims.claim_address_province as province, " +
                "fc.number_sx as number_sx, " +
                "claims.date_accident, " +
                "claims.claim_address_locality as locality, " +
                "(claims.created_at)\\:\\:timestamp as claims_created_at " +
                "FROM counterparty_new c INNER JOIN claims_new claims on c.claims_id = claims.id LEFT JOIN LC USING (counterparty_id) LEFT JOIN claims_from_company fc on claims_id = fc.id ");

        StringBuilder whereCondition = new StringBuilder("WHERE  c.type = '" + VehicleTypeEnum.VEHICLE + "' AND c.eligibility = true ");

        if(msaPagination != null && msaPagination){
            whereCondition.append(" AND (c.is_read_msa IS NULL OR c.is_read_msa = false)");
        }

        if (Util.isNotEmpty(region)) {
            whereCondition.append(" AND claims.claim_address_region ILIKE '%" + region + "%' ");
        }
        if (Util.isNotEmpty(addressSx)) {
            whereCondition.append(" AND claims.claim_address_formatted ILIKE '%" + addressSx + "%' ");
        }
        if (Util.isNotEmpty(province)) {
            whereCondition.append(" AND claims.claim_address_province ILIKE '%" + province + "%' ");
        }
        if (Util.isNotEmpty(locality)) {
            whereCondition.append(" AND claims.locality ILIKE '%" + locality + "%' ");
        }
        if (Util.isNotEmpty(fromCompany)) {
            whereCondition.append(" AND fc.number_sx ILIKE '%" + fromCompany + "%' ");
        }
        if (Util.isNotEmpty(assignedTo)) {
            whereCondition.append(" AND c.assigned_to->>'id' =  '" + assignedTo + "' ");

        }
        if (Util.isNotEmpty(lastContact)) {
            whereCondition.append(" AND   (last_cont->>'last_call')\\:\\:timestamp >= ('" + lastContact + "')\\:\\:timestamp ");

        }

        if (Util.isNotEmpty(recall)) {
            whereCondition.append(" AND   (last_cont->>'recall')\\:\\:timestamp >= ('" + recall + "')\\:\\:timestamp ");

        }


        if (dateSxFrom != null && dateSxTo != null) {
            if (dateSxFrom.length() == 28) dateSxFrom = dateSxFrom.substring(0, 23);
            if (dateSxTo.length() == 28) dateSxTo = dateSxTo.substring(0, 23);
            if (dateSxTo.compareTo(dateSxFrom) < 0) {
                throw new BadParametersException("the date_sx_to is less than the date_sx_from", MessageCode.CLAIMS_1011);
            } else {
                whereCondition.append(" AND claims.data_accident\\:\\:timestamp >= '" + dateSxFrom + "'\\:\\:timestamp   AND claims.data_accident\\:\\:timestamp <= '" + dateSxTo + "'\\:\\:timestamp  ");
            }
        } else if ((dateSxFrom != null) || (dateSxTo != null)) {
            throw new BadParametersException("the date_sx_to and date_sx_from must be present together ", MessageCode.CLAIMS_1011);
        }


        if (dateCreatedFrom != null && dateCreatedTo != null) {
            if (dateCreatedFrom.length() == 28) dateCreatedFrom = dateCreatedFrom.substring(0, 23);
            if (dateCreatedTo.length() == 28) dateCreatedTo = dateCreatedTo.substring(0, 23);
            if (dateCreatedTo.compareTo(dateCreatedFrom) < 0) {
                throw new BadParametersException("the date_created_to is less than the date_created_from", MessageCode.CLAIMS_1011);
            } else {
                whereCondition.append(" AND ( ( (claims.created_at\\:\\:timestamp >= '" + dateCreatedFrom + "'\\:\\:timestamp  ) AND (claims.created_at\\:\\:timestamp <= '" + dateCreatedTo + "'\\:\\:timestamp ) ) ) ");
            }
        } else if ((dateCreatedFrom != null) || (dateCreatedTo != null)) {
            throw new BadParametersException("the date_created_to and date_created_from must be present together ", MessageCode.CLAIMS_1011);
        }

        if (Util.isNotEmpty(denomination)) {
            whereCondition.append(" AND c.insurance_company->>'name' ILIKE '%" + denomination + "%' ");
        }

        if (practiceIdCounterparty != null) {
            whereCondition.append(" AND c.practice_id_counterparty = " + practiceIdCounterparty);
        }

        if (statusRepair != null) {

            RepairStatusEnum repairStatusEnum = RepairStatusEnum.create(statusRepair);
            whereCondition.append(" AND ( lower(c.status_repair) ILIKE lower('" + repairStatusEnum.getValue() + "')) ");
        }

        if (util.isNotEmpty(plate)) {
            whereCondition.append(" AND ( c.vehicle->>'license_plate' ILIKE '%" + plate + "%') ");
        }
        sqlQuery.append(whereCondition);
        sqlQuery.append("ORDER BY c.counterparty_id) t");

        //sqlQuery.append(" LIMIT " + pageSize + " OFFSET " + (page - 1) * pageSize);

        System.out.println(sqlQuery);

        Query query = em.createNativeQuery(sqlQuery.toString());


        return (BigInteger) query.getSingleResult();

    }


    //FINE INTEGRAZIONE MSA

    //PAGINAZIONE SENZA TO_SORT
    public List<CounterpartyNewEntity> findCounterpartyForPaginationWithoutToSort(int page, int pageSize, String denomination, String plate, Long practiceIdCounterparty, String statusRepair, String dateCreatedFrom, String dateCreatedTo, Boolean asc, String orderBy, String addressSx, String fromCompany, String dateSxFrom, String dateSxTo, String region, String province, String lastContact, String assignedTo, String locality, String recall) {
        Map<String, String> mappOrder = new HashMap<>();
        mappOrder.put("license_plate", " t.vehicle\\:\\:jsonb->>'license_plate' ");
        mappOrder.put("denomination", " (t.insurance_company\\:\\:jsonb->>'entity')\\:\\:jsonb->>'name' ");
        mappOrder.put("practice_id", " t.practice_id ");
        mappOrder.put("created_at", " t.claims_created_at ");
        mappOrder.put("locator", " t.locator ");
        mappOrder.put("region", " t.region ");
        mappOrder.put("address", " t.formatted_address ");
        mappOrder.put("province", " t.province ");
        mappOrder.put("locality", " t.locality ");
        mappOrder.put("number_sx", " t.number_sx ");
        mappOrder.put("assigned_to", " t.assigned_to ");
        //mappOrder.put("last_call", " (count->>'last_contact')\\:\\:jsonb->>'last_call' ");
        //mappOrder.put("recall", " (count->>'last_contact')\\:\\:jsonb->>'recall' ");
        mappOrder.put("date_accident", " t.date_accident ");

        String auxiliaryTableCounterparty = "WITH" +
                " LC AS ( SELECT counterparty_id, jsonb_array_elements(counterparty_new.last_contact) last_cont FROM counterparty_new ) ";

        StringBuilder sqlQuery = new StringBuilder(auxiliaryTableCounterparty);
        sqlQuery.append("SELECT * FROM (SELECT DISTINCT ON (c.counterparty_id) c.*, " +
                "claims.id as id, " +
                "claims.practice_id, " +
                "claim_locator as locator, " +
                "claims.claim_address_region as region," +
                "claims.claim_address_formatted as address, " +
                "claims.claim_address_province as province, " +
                "fc.number_sx as number_sx, " +
                "claims.date_accident, " +
                "claims.claim_address_locality as locality, " +
                "(claims.created_at)\\:\\:timestamp as claims_created_at " +
                "FROM counterparty_new c " +
                "INNER JOIN claims_new claims on c.claims_id = claims.id " +
                "INNER JOIN claims_from_company fc ON claims.id = fc.id " +
                "LEFT JOIN LC USING (counterparty_id) ");

        StringBuilder whereCondition = new StringBuilder("WHERE  claims.is_with_counterparty = true  AND c.status_repair <> '"+ RepairStatusEnum.TO_SORT +"' AND c.type = '" + VehicleTypeEnum.VEHICLE + "' AND c.eligibility = true ");

        if (Util.isNotEmpty(region)) {
            whereCondition.append(" AND claims.claim_address_region ILIKE '%" + region + "%' ");
        }
        if (Util.isNotEmpty(addressSx)) {
            whereCondition.append(" AND claims.claim_address_formatted ILIKE '%" + addressSx + "%' ");
        }
        if (Util.isNotEmpty(province)) {
            whereCondition.append(" AND claims.claim_address_province ILIKE '%" + province + "%' ");
        }
        if (Util.isNotEmpty(locality)) {
            whereCondition.append(" AND claims.locality ILIKE '%" + locality + "%' ");
        }
        if (Util.isNotEmpty(fromCompany)) {
            whereCondition.append(" AND fc.number_sx ILIKE '%" + fromCompany + "%' ");
        }
        if (Util.isNotEmpty(assignedTo)) {
            whereCondition.append(" AND c.assigned_to->>'id' =  '" + assignedTo + "' ");

        }
        if (Util.isNotEmpty(lastContact)) {
            whereCondition.append(" AND   (last_cont->>'last_call')\\:\\:timestamp >= ('" + lastContact + "')\\:\\:timestamp ");

        }

        if (Util.isNotEmpty(recall)) {
            whereCondition.append(" AND   (last_cont->>'recall')\\:\\:timestamp >= ('" + recall + "')\\:\\:timestamp ");

        }


        if (dateSxFrom != null && dateSxTo != null) {
            if (dateSxFrom.length() == 28) dateSxFrom = dateSxFrom.substring(0, 23);
            if (dateSxTo.length() == 28) dateSxTo = dateSxTo.substring(0, 23);
            if (dateSxTo.compareTo(dateSxFrom) < 0) {
                throw new BadParametersException("the date_sx_to is less than the date_sx_from", MessageCode.CLAIMS_1011);
            } else {
                whereCondition.append(" AND claims.data_accident\\:\\:timestamp >= '" + dateSxFrom + "'\\:\\:timestamp   AND claims.data_accident\\:\\:timestamp <= '" + dateSxTo + "'\\:\\:timestamp  ");
            }
        } else if ((dateSxFrom != null) || (dateSxTo != null)) {
            throw new BadParametersException("the date_sx_to and date_sx_from must be present together ", MessageCode.CLAIMS_1011);
        }


        if (dateCreatedFrom != null && dateCreatedTo != null) {
            if (dateCreatedFrom.length() == 28) dateCreatedFrom = dateCreatedFrom.substring(0, 23);
            if (dateCreatedTo.length() == 28) dateCreatedTo = dateCreatedTo.substring(0, 23);
            if (dateCreatedTo.compareTo(dateCreatedFrom) < 0) {
                throw new BadParametersException("the date_created_to is less than the date_created_from", MessageCode.CLAIMS_1011);
            } else {
                whereCondition.append(" AND ( ( (claims.created_at\\:\\:timestamp >= '" + dateCreatedFrom + "'\\:\\:timestamp  ) AND (claims.created_at\\:\\:timestamp <= '" + dateCreatedTo + "'\\:\\:timestamp ) ) ) ");
            }
        } else if ((dateCreatedFrom != null) || (dateCreatedTo != null)) {
            throw new BadParametersException("the date_created_to and date_created_from must be present together ", MessageCode.CLAIMS_1011);
        }

        if (Util.isNotEmpty(denomination)) {
            whereCondition.append(" AND c.insurance_company->>'name' ILIKE '%" + denomination + "%' ");
        }

        if (practiceIdCounterparty != null) {
            whereCondition.append(" AND c.practice_id_counterparty = " + practiceIdCounterparty);
        }

        if (statusRepair != null) {

            RepairStatusEnum repairStatusEnum = RepairStatusEnum.create(statusRepair);
            whereCondition.append(" AND ( lower(c.status_repair) ILIKE lower('" + repairStatusEnum.getValue() + "')) ");
        }

        if (util.isNotEmpty(plate)) {
            whereCondition.append(" AND ( c.vehicle->>'license_plate' ILIKE '%" + plate + "%') ");
        }
        sqlQuery.append(whereCondition);
        sqlQuery.append("ORDER BY c.counterparty_id ) t ");

        String fieldToOrder = null;
        if (orderBy != null)
            fieldToOrder = mappOrder.get(orderBy);
        if (fieldToOrder != null && asc != null) {
            if (asc) {
                sqlQuery.append(" ORDER BY " + fieldToOrder + " ASC");
            } else {
                sqlQuery.append(" ORDER BY " + fieldToOrder + " DESC");
            }

        } else if (fieldToOrder != null) {
            sqlQuery.append(" ORDER BY " + fieldToOrder + " DESC");
        }

        sqlQuery.append(" LIMIT " + pageSize + " OFFSET " + (page - 1) * pageSize);
        System.out.println(sqlQuery);
        Query query = em.createNativeQuery(sqlQuery.toString(), "CounterpartyNewResults");


        return query.getResultList();

    }

    //COUNT PAGINAZIONE SENZA TO_SORT
    public BigInteger countCounterpartyForPaginationWithoutToSort(int page, int pageSize, String denomination, String plate, Long practiceIdCounterparty, String statusRepair, String dateCreatedFrom, String dateCreatedTo, Boolean asc, String orderBy, String addressSx, String fromCompany, String dateSxFrom, String dateSxTo, String region, String province, String lastContact, String assignedTo, String locality, String recall) {

        String auxiliaryTableCounterparty = "WITH" +
                " LC AS ( SELECT counterparty_id, jsonb_array_elements(counterparty_new.last_contact) last_cont FROM counterparty_new ) ";

        StringBuilder sqlQuery = new StringBuilder(auxiliaryTableCounterparty);
        sqlQuery.append("SELECT COUNT(*) FROM (SELECT DISTINCT ON (c.counterparty_id) c.*, " +
                "claims.id as id, " +
                "claims.practice_id, " +
                "claim_locator as locator, " +
                "claims.claim_address_region as region," +
                "claims.claim_address_formatted as address, " +
                "claims.claim_address_province as province, " +
                "fc.number_sx as number_sx, " +
                "claims.date_accident, " +
                "claims.claim_address_locality as locality, " +
                "(claims.created_at)\\:\\:timestamp as claims_created_at " +
                "FROM counterparty_new c " +
                "INNER JOIN claims_new claims on c.claims_id = claims.id " +
                "INNER JOIN claims_from_company fc ON claims.id = fc.id " +
                "LEFT JOIN LC USING (counterparty_id) ");

        StringBuilder whereCondition = new StringBuilder("WHERE  claims.is_with_counterparty = true  AND c.status_repair <> '"+ RepairStatusEnum.TO_SORT +"' AND c.type = '" + VehicleTypeEnum.VEHICLE + "' AND c.eligibility = true ");

        if (Util.isNotEmpty(region)) {
            whereCondition.append(" AND claims.claim_address_region ILIKE '%" + region + "%' ");
        }
        if (Util.isNotEmpty(addressSx)) {
            whereCondition.append(" AND claims.claim_address_formatted ILIKE '%" + addressSx + "%' ");
        }
        if (Util.isNotEmpty(province)) {
            whereCondition.append(" AND claims.claim_address_province ILIKE '%" + province + "%' ");
        }
        if (Util.isNotEmpty(locality)) {
            whereCondition.append(" AND claims.locality ILIKE '%" + locality + "%' ");
        }
        if (Util.isNotEmpty(fromCompany)) {
            whereCondition.append(" AND fc.number_sx ILIKE '%" + fromCompany + "%' ");
        }
        if (Util.isNotEmpty(assignedTo)) {
            whereCondition.append(" AND c.assigned_to->>'id' =  '" + assignedTo + "' ");

        }
        if (Util.isNotEmpty(lastContact)) {
            whereCondition.append(" AND   (last_cont->>'last_call')\\:\\:timestamp >= ('" + lastContact + "')\\:\\:timestamp ");

        }

        if (Util.isNotEmpty(recall)) {
            whereCondition.append(" AND   (last_cont->>'recall')\\:\\:timestamp >= ('" + recall + "')\\:\\:timestamp ");

        }
        /*if(util.isNotEmpty(centerEmail)){
            whereCondition.append(" AND (c.canalization->>'center')\\:\\:jsonb->>'email' ILIKE '%" + centerEmail+ "%' ");
        }*/
        /*if(util.isNotEmpty(dateSx)){
            whereCondition.append(" AND   ((claims.complaint->>'data_accident')\\:\\:jsonb->>'date_accident')\\:\\:timestamp >= ('" + dateSx + "')\\:\\:timestamp ");

        }*/


        if (dateSxFrom != null && dateSxTo != null) {
            if (dateSxFrom.length() == 28) dateSxFrom = dateSxFrom.substring(0, 23);
            if (dateSxTo.length() == 28) dateSxTo = dateSxTo.substring(0, 23);
            if (dateSxTo.compareTo(dateSxFrom) < 0) {
                throw new BadParametersException("the date_sx_to is less than the date_sx_from", MessageCode.CLAIMS_1011);
            } else {
                whereCondition.append(" AND claims.data_accident\\:\\:timestamp >= '" + dateSxFrom + "'\\:\\:timestamp   AND claims.data_accident\\:\\:timestamp <= '" + dateSxTo + "'\\:\\:timestamp  ");
            }
        } else if ((dateSxFrom != null) || (dateSxTo != null)) {
            throw new BadParametersException("the date_sx_to and date_sx_from must be present together ", MessageCode.CLAIMS_1011);
        }


        if (dateCreatedFrom != null && dateCreatedTo != null) {
            if (dateCreatedFrom.length() == 28) dateCreatedFrom = dateCreatedFrom.substring(0, 23);
            if (dateCreatedTo.length() == 28) dateCreatedTo = dateCreatedTo.substring(0, 23);
            if (dateCreatedTo.compareTo(dateCreatedFrom) < 0) {
                throw new BadParametersException("the date_created_to is less than the date_created_from", MessageCode.CLAIMS_1011);
            } else {
                whereCondition.append(" AND ( ( (claims.created_at\\:\\:timestamp >= '" + dateCreatedFrom + "'\\:\\:timestamp  ) AND (claims.created_at\\:\\:timestamp <= '" + dateCreatedTo + "'\\:\\:timestamp ) ) ) ");
            }
        } else if ((dateCreatedFrom != null) || (dateCreatedTo != null)) {
            throw new BadParametersException("the date_created_to and date_created_from must be present together ", MessageCode.CLAIMS_1011);
        }

        if (Util.isNotEmpty(denomination)) {
            whereCondition.append(" AND c.insurance_company->>'name' ILIKE '%" + denomination + "%' ");
        }

        if (practiceIdCounterparty != null) {
            whereCondition.append(" AND c.practice_id_counterparty = " + practiceIdCounterparty);
        }

        if (statusRepair != null) {

            RepairStatusEnum repairStatusEnum = RepairStatusEnum.create(statusRepair);
            whereCondition.append(" AND ( lower(c.status_repair) ILIKE lower('" + repairStatusEnum.getValue() + "')) ");
        }

        if (util.isNotEmpty(plate)) {
            whereCondition.append(" AND ( c.vehicle->>'license_plate' ILIKE '%" + plate + "%') ");
        }
        sqlQuery.append(whereCondition);
        sqlQuery.append("ORDER BY c.counterparty_id ) t ");
        //sqlQuery.append(" LIMIT " + pageSize + " OFFSET " + (page - 1) * pageSize);

        Query query = em.createNativeQuery(sqlQuery.toString());


        return (BigInteger) query.getSingleResult();

    }

    public CounterpartyStats findCounterpartyStats(String nemoUserId) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Tuple> counterpartyCriteriaQuery = criteriaBuilder.createTupleQuery();

        Subquery<Long> countWaitingForQuotation = counterpartyCriteriaQuery.subquery(Long.class);
        Root<CounterpartyNewEntity> subQueryRepairing = countWaitingForQuotation.from(CounterpartyNewEntity.class);
        countWaitingForQuotation.select(criteriaBuilder.count(subQueryRepairing));
        List<Predicate> predicates = new ArrayList<>();
        predicates.add(criteriaBuilder.equal(subQueryRepairing.get(CounterpartyNewEntity_.eligibility), true));
        predicates.add(criteriaBuilder.equal(subQueryRepairing.get("repairStatus"), RepairStatusEnum.WAITING_FOR_QUOTATION));
        if(nemoUserId != null && !nemoUserId.isEmpty()){
            predicates.add(criteriaBuilder.equal(criteriaBuilder
                    .function(JSONB_EXTRACT_PATH_TEXT.value(),
                            String.class,
                            subQueryRepairing.get(CounterpartyNewEntity_.assignedTo),
                            criteriaBuilder.literal("id")), nemoUserId));
        }
        countWaitingForQuotation.where(predicates.toArray(new Predicate[predicates.size()]));


        Subquery<Long> countToSortSubQuery = counterpartyCriteriaQuery.subquery(Long.class);
        Root<CounterpartyNewEntity> subQueryToSort = countToSortSubQuery.from(CounterpartyNewEntity.class);
        countToSortSubQuery.select(criteriaBuilder.count(subQueryToSort));
        predicates = new ArrayList<>();
        predicates.add(criteriaBuilder.equal(subQueryToSort.get(CounterpartyNewEntity_.eligibility), true));
        predicates.add(criteriaBuilder.equal(subQueryToSort.get("repairStatus"), RepairStatusEnum.TO_SORT));
        if(nemoUserId != null && !nemoUserId.isEmpty()){
            predicates.add(criteriaBuilder.equal(criteriaBuilder
                    .function(JSONB_EXTRACT_PATH_TEXT.value(),
                            String.class,
                            subQueryToSort.get(CounterpartyNewEntity_.assignedTo),
                            criteriaBuilder.literal("id")), nemoUserId));
        }
        countToSortSubQuery.where(predicates.toArray(new Predicate[predicates.size()]));


        Subquery<Long> countClosedDenial = counterpartyCriteriaQuery.subquery(Long.class);
        Root<CounterpartyNewEntity> subQueryCancelled = countClosedDenial.from(CounterpartyNewEntity.class);
        countClosedDenial.select(criteriaBuilder.count(subQueryCancelled));
        predicates = new ArrayList<>();
        predicates.add(criteriaBuilder.equal(subQueryCancelled.get(CounterpartyNewEntity_.eligibility), true));
        predicates.add(criteriaBuilder.equal(subQueryCancelled.get("repairStatus"), RepairStatusEnum.CLOSED_DENIAL));
        if(nemoUserId != null && !nemoUserId.isEmpty()){
            predicates.add(criteriaBuilder.equal(criteriaBuilder
                    .function(JSONB_EXTRACT_PATH_TEXT.value(),
                            String.class,
                            subQueryCancelled.get(CounterpartyNewEntity_.assignedTo),
                            criteriaBuilder.literal("id")), nemoUserId));
        }
        countClosedDenial.where(predicates.toArray(new Predicate[predicates.size()]));

        Subquery<Long> countQuotationApproved = counterpartyCriteriaQuery.subquery(Long.class);
        Root<CounterpartyNewEntity> subQueryWreck = countQuotationApproved.from(CounterpartyNewEntity.class);
        countQuotationApproved.select(criteriaBuilder.count(subQueryWreck));
        predicates = new ArrayList<>();
        predicates.add(criteriaBuilder.equal(subQueryWreck.get(CounterpartyNewEntity_.eligibility), true));
        predicates.add(criteriaBuilder.equal(subQueryWreck.get("repairStatus"), RepairStatusEnum.QUOTATION_APPROVED));
        if(nemoUserId != null && !nemoUserId.isEmpty()){
            predicates.add(criteriaBuilder.equal(criteriaBuilder
                    .function(JSONB_EXTRACT_PATH_TEXT.value(),
                            String.class,
                            subQueryWreck.get(CounterpartyNewEntity_.assignedTo),
                            criteriaBuilder.literal("id")), nemoUserId));
        }
        countQuotationApproved.where(predicates.toArray(new Predicate[predicates.size()]));

        Subquery<Long> countUnderRepair = counterpartyCriteriaQuery.subquery(Long.class);
        Root<CounterpartyNewEntity> subQueryClosed = countUnderRepair.from(CounterpartyNewEntity.class);
        countUnderRepair.select(criteriaBuilder.count(subQueryClosed));
        predicates = new ArrayList<>();
        predicates.add(criteriaBuilder.equal(subQueryClosed.get(CounterpartyNewEntity_.eligibility), true));
        predicates.add(criteriaBuilder.equal(subQueryClosed.get("repairStatus"), RepairStatusEnum.UNDER_REPAIR));
        if(nemoUserId != null && !nemoUserId.isEmpty()){
            predicates.add(criteriaBuilder.equal(criteriaBuilder
                    .function(JSONB_EXTRACT_PATH_TEXT.value(),
                            String.class,
                            subQueryClosed.get(CounterpartyNewEntity_.assignedTo),
                            criteriaBuilder.literal("id")), nemoUserId));
        }
        countUnderRepair.where(predicates.toArray(new Predicate[predicates.size()]));

        Subquery<Long> countUnderLiquidation = counterpartyCriteriaQuery.subquery(Long.class);
        Root<CounterpartyNewEntity> subQueryConfirmedLiquidate = countUnderLiquidation.from(CounterpartyNewEntity.class);
        countUnderLiquidation.select(criteriaBuilder.count(subQueryConfirmedLiquidate));
        predicates = new ArrayList<>();
        predicates.add(criteriaBuilder.equal(subQueryConfirmedLiquidate.get(CounterpartyNewEntity_.eligibility), true));
        predicates.add(criteriaBuilder.equal(subQueryConfirmedLiquidate.get("repairStatus"), RepairStatusEnum.UNDER_LIQUIDATION));
        if(nemoUserId != null && !nemoUserId.isEmpty()){
            predicates.add(criteriaBuilder.equal(criteriaBuilder
                    .function(JSONB_EXTRACT_PATH_TEXT.value(),
                            String.class,
                            subQueryConfirmedLiquidate.get(CounterpartyNewEntity_.assignedTo),
                            criteriaBuilder.literal("id")), nemoUserId));
        }
        countUnderLiquidation.where(predicates.toArray(new Predicate[predicates.size()]));

        Subquery<Long> countClosedLiquidated = counterpartyCriteriaQuery.subquery(Long.class);
        Root<CounterpartyNewEntity> subQueryWaitingForInvoice = countClosedLiquidated.from(CounterpartyNewEntity.class);
        countClosedLiquidated.select(criteriaBuilder.count(subQueryWaitingForInvoice));
        predicates = new ArrayList<>();
        predicates.add(criteriaBuilder.equal(subQueryWaitingForInvoice.get(CounterpartyNewEntity_.eligibility), true));
        predicates.add(criteriaBuilder.equal(subQueryWaitingForInvoice.get("repairStatus"), RepairStatusEnum.CLOSED_LIQUIDATED));
        if(nemoUserId != null && !nemoUserId.isEmpty()){
            predicates.add(criteriaBuilder.equal(criteriaBuilder
                    .function(JSONB_EXTRACT_PATH_TEXT.value(),
                            String.class,
                            subQueryWaitingForInvoice.get(CounterpartyNewEntity_.assignedTo),
                            criteriaBuilder.literal("id")), nemoUserId));
        }
        countClosedLiquidated.where(predicates.toArray(new Predicate[predicates.size()]));

        Subquery<Long> countClosedRepaired = counterpartyCriteriaQuery.subquery(Long.class);
        Root<CounterpartyNewEntity> subQueryWaitingForNetworkToLiquidate = countClosedRepaired.from(CounterpartyNewEntity.class);
        countClosedRepaired.select(criteriaBuilder.count(subQueryWaitingForNetworkToLiquidate));
        predicates = new ArrayList<>();
        predicates.add(criteriaBuilder.equal(subQueryWaitingForNetworkToLiquidate.get(CounterpartyNewEntity_.eligibility), true));
        predicates.add(criteriaBuilder.equal(subQueryWaitingForNetworkToLiquidate.get("repairStatus"), RepairStatusEnum.CLOSED_REPAIRED));
        if(nemoUserId != null && !nemoUserId.isEmpty()){
            predicates.add(criteriaBuilder.equal(criteriaBuilder
                    .function(JSONB_EXTRACT_PATH_TEXT.value(),
                            String.class,
                            subQueryWaitingForNetworkToLiquidate.get(CounterpartyNewEntity_.assignedTo),
                            criteriaBuilder.literal("id")), nemoUserId));
        }
        countClosedRepaired.where(predicates.toArray(new Predicate[predicates.size()]));

        Subquery<Long> countRepaired = counterpartyCriteriaQuery.subquery(Long.class);
        Root<CounterpartyNewEntity> subQueryWaitingForNetworkToRepair = countRepaired.from(CounterpartyNewEntity.class);
        countRepaired.select(criteriaBuilder.count(subQueryWaitingForNetworkToRepair));
        predicates = new ArrayList<>();
        predicates.add(criteriaBuilder.equal(subQueryWaitingForNetworkToRepair.get(CounterpartyNewEntity_.eligibility), true));
        predicates.add(criteriaBuilder.equal(subQueryWaitingForNetworkToRepair.get("repairStatus"), RepairStatusEnum.REPAIRED));
        if(nemoUserId != null && !nemoUserId.isEmpty()){
            predicates.add(criteriaBuilder.equal(criteriaBuilder
                    .function(JSONB_EXTRACT_PATH_TEXT.value(),
                            String.class,
                            subQueryWaitingForNetworkToRepair.get(CounterpartyNewEntity_.assignedTo),
                            criteriaBuilder.literal("id")), nemoUserId));
        }
        countRepaired.where(predicates.toArray(new Predicate[predicates.size()]));

        Subquery<Long> countToContactSubQuery = counterpartyCriteriaQuery.subquery(Long.class);
        Root<CounterpartyNewEntity> subQueryToContact = countToContactSubQuery.from(CounterpartyNewEntity.class);
        countToContactSubQuery.select(criteriaBuilder.count(subQueryToContact));
        predicates = new ArrayList<>();
        predicates.add(criteriaBuilder.equal(subQueryToContact.get(CounterpartyNewEntity_.eligibility), true));
        predicates.add(criteriaBuilder.equal(subQueryToContact.get("repairStatus"), RepairStatusEnum.TO_CONTACT));
        if(nemoUserId != null && !nemoUserId.isEmpty()){
            predicates.add(criteriaBuilder.equal(criteriaBuilder
                    .function(JSONB_EXTRACT_PATH_TEXT.value(),
                            String.class,
                            subQueryToContact.get(CounterpartyNewEntity_.assignedTo),
                            criteriaBuilder.literal("id")), nemoUserId));
        }
        countToContactSubQuery.where(predicates.toArray(new Predicate[predicates.size()]));

        Subquery<Long> countToRecontact = counterpartyCriteriaQuery.subquery(Long.class);
        Root<CounterpartyNewEntity> subQueryToContactAgain = countToRecontact.from(CounterpartyNewEntity.class);
        countToRecontact.select(criteriaBuilder.count(subQueryToContactAgain));
        predicates = new ArrayList<>();
        predicates.add(criteriaBuilder.equal(subQueryToContactAgain.get(CounterpartyNewEntity_.eligibility), true));
        predicates.add(criteriaBuilder.equal(subQueryToContactAgain.get("repairStatus"), RepairStatusEnum.TO_RECONTACT));
        if(nemoUserId != null && !nemoUserId.isEmpty()){
            predicates.add(criteriaBuilder.equal(criteriaBuilder
                    .function(JSONB_EXTRACT_PATH_TEXT.value(),
                            String.class,
                            subQueryToContactAgain.get(CounterpartyNewEntity_.assignedTo),
                            criteriaBuilder.literal("id")), nemoUserId));
        }
        countToRecontact.where(predicates.toArray(new Predicate[predicates.size()]));

        counterpartyCriteriaQuery.multiselect(countWaitingForQuotation.getSelection().alias("waiting_for_quotation"),
                countToSortSubQuery.getSelection().alias("to_sort"),
                countClosedDenial.getSelection().alias("closed_denial"), countQuotationApproved.getSelection().alias("quotation_approved"),
                countUnderRepair.getSelection().alias("under_repair"), countUnderLiquidation.getSelection().alias("under_liquidation"),
                countClosedLiquidated.getSelection().alias("closed_liquidated"),
                countClosedRepaired.getSelection().alias("closed_repaired"),
                countRepaired.getSelection().alias("repaired"),
                countToContactSubQuery.getSelection().alias("to_contact"), countToRecontact.getSelection().alias("to_recontact"));
        counterpartyCriteriaQuery.from(ClaimsEntity.class);

        Query sql = em.createQuery(counterpartyCriteriaQuery);

        List<Tuple> t = sql.getResultList();

        CounterpartyStats stats = new CounterpartyStats(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        if (t != null && !t.isEmpty())
        {
            stats.setWaitingForQuotation(((Number) t.get(0).get("waiting_for_quotation")).intValue());
            stats.setToSort(((Number) t.get(0).get("to_sort")).intValue());
            stats.setToContact(((Number) t.get(0).get("to_contact")).intValue());
            stats.setToRecontact(((Number) t.get(0).get("to_recontact")).intValue());
            stats.setRepaired(((Number) t.get(0).get("repaired")).intValue());
            stats.setClosedRepaired(((Number) t.get(0).get("closed_repaired")).intValue());
            stats.setClosedLiquidated(((Number) t.get(0).get("closed_liquidated")).intValue());
            stats.setUnderLiquidation(((Number) t.get(0).get("under_liquidation")).intValue());
            stats.setUnderRepair(((Number) t.get(0).get("under_repair")).intValue());
            stats.setQuotationApproved(((Number) t.get(0).get("quotation_approved")).intValue());
            stats.setClosedDenial(((Number) t.get(0).get("closed_denial")).intValue());

        }


        return stats;
    }

    /*
    X	Targa ALD
    X	Targa CTP
    X	ID SX
    X	ID Repair
    X	Stato controparte
    X	Data Inserimento (arco temporale)
    ?	Data canalizzazione (arco temporale) // non la abbiamo
    ?   Conducente // counterparty.driver combo first name + last name?
    X	Ultimo contatto (arco temporale)
    ?   Operatore // assigner_to (dentro counterparty)
    X	Regione
    X	Città -> locality
    */
    public BigInteger countCounterpartyForExport(CtpExportFilter filter) {
        String auxiliaryTableCounterparty = "WITH LC AS ( SELECT counterparty_id, jsonb_array_elements(last_contact) last_cont FROM counterparty_new ) ";

        StringBuilder sqlQuery = new StringBuilder(auxiliaryTableCounterparty);
        sqlQuery.append("SELECT COUNT(*) FROM (SELECT DISTINCT ON (c.counterparty_id) c.*, " +
                "claims.id as id, " +
                "claims.practice_id, " +
                "c.practice_id_counterparty, " +
                "c.driver ->>'id' as driver_id, " +
                "c.assigned_to," +
                "claims.claim_address_region as region," +
                "claims.claim_address_locality as city, " +
                "(claims.created_at)\\:\\:timestamp as claims_created_at " +
                "FROM counterparty_new c INNER JOIN claims_new claims on c.claims_id = claims.id LEFT JOIN LC USING (counterparty_id) ");

        // ELIGIBILITY AND VEHICLE TYPE
        StringBuilder whereCondition = new StringBuilder("WHERE c.type = '" + VehicleTypeEnum.VEHICLE + "' AND c.eligibility = true ");

        if (Util.isNotEmpty(filter.getIdRepair())) {
            whereCondition.append(" AND c.practice_id_counterparty = " + filter.getIdRepair());
        }
        if (Util.isNotEmpty(filter.getDriverFirstName())) {
            whereCondition.append(" AND c.driver ->>'firstname' ILIKE '%" + filter.getDriverFirstName() + "%' ");
        }
        if (Util.isNotEmpty(filter.getDriverLastName())) {
            whereCondition.append(" AND c.driver ->>'lastname' ILIKE '%" + filter.getDriverLastName() + "%' ");
        }
        if (Util.isNotEmpty(filter.getOperator())) {
            whereCondition.append(" AND c.assigned_to ->>'id' = '" + filter.getOperator() + "' ");
        }
        if (Util.isNotEmpty(filter.getRegion())) {
            whereCondition.append(" AND claims.claim_address_region ILIKE '%" + filter.getRegion() + "%' ");
        }
        if (Util.isNotEmpty(filter.getCity())) {
            whereCondition.append(" AND claims.claim_address_locality ILIKE '%" + filter.getCity() + "%' ");
        }

        if (filter.getLastContactFrom() != null && filter.getLastContactTo() != null) {
            if (filter.getLastContactFrom().length() == 28) {
                filter.setLastContactFrom( filter.getLastContactFrom().substring(0, 23) ) ;
            }
            if (filter.getLastContactTo().length() == 28) {
                filter.setLastContactTo( filter.getLastContactTo().substring(0, 23) ); ;
            }
            if ( filter.getLastContactTo().compareTo(filter.getLastContactFrom() ) < 0) {
                throw new BadParametersException("the lastContactFrom is less than the lastContactTo", MessageCode.CLAIMS_1011);
            } else {
                whereCondition.append(" AND (last_cont->>'last_call')\\:\\:timestamp >= ('" + filter.getLastContactFrom() + "')\\:\\:timestamp AND (last_cont->>'last_call')\\:\\:timestamp <= ('" + filter.getLastContactTo() + "')\\:\\:timestamp ");
            }
        } else if ( filter.getLastContactFrom() != null || filter.getLastContactTo() != null ) {
            throw new BadParametersException("the lastContactFrom and lastContactTo must be present together ", MessageCode.CLAIMS_1011);
        }

        if ( Util.isNotEmpty( filter.getPracticeId() ) ) {
            whereCondition.append(" AND claims.practice_id =" + filter.getPracticeId());
        }

        if ( filter.getDateCreatedFrom() != null && filter.getDateCreatedTo() != null ) {
            if (filter.getDateCreatedFrom().length() == 28) {
                filter.setDateCreatedFrom( filter.getDateCreatedFrom().substring(0, 23) );
            }
            if ( filter.getDateCreatedTo().length() == 28) {
                filter.setDateCreatedTo( filter.getDateCreatedTo().substring(0, 23) );
            }
            if ( filter.getDateCreatedTo().compareTo( filter.getDateCreatedFrom() ) < 0) {
                throw new BadParametersException("the date_created_to is less than the date_created_from", MessageCode.CLAIMS_1011);
            }
            else {
                whereCondition.append(" AND ( ( (c.created_at\\:\\:timestamp >= '" + filter.getDateCreatedFrom() + "'\\:\\:timestamp  ) AND (c.created_at\\:\\:timestamp <= '" + filter.getDateCreatedTo() + "'\\:\\:timestamp ) ) ) ");
            }
        } else if ( filter.getDateCreatedFrom() != null || filter.getDateCreatedTo() != null ) {
            throw new BadParametersException("the date_created_to and date_created_from must be present together ", MessageCode.CLAIMS_1011);
        }

        if ( filter.getDateCanalizationFrom() != null && filter.getDateCanalizationTo() != null) {
            if ( filter.getDateCanalizationFrom().length() == 28 ) {
                filter.setDateCanalizationFrom( filter.getDateCanalizationFrom().substring(0, 23) );
            }
            if ( filter.getDateCanalizationTo().length() == 28) {
                filter.setDateCanalizationTo( filter.getDateCanalizationTo().substring(0, 23) );
            }
            if (filter.getDateCanalizationTo().compareTo( filter.getDateCanalizationFrom() ) < 0) {
                throw new BadParametersException("the date_canalization_to is less than the date_canalization_from", MessageCode.CLAIMS_1011);
            } else {
                whereCondition.append(" AND ( ( (c.repair_created_at\\:\\:timestamp >= '" + filter.getDateCanalizationFrom() + "'\\:\\:timestamp  ) AND (c.repair_created_at\\:\\:timestamp <= '" + filter.getDateCanalizationTo() + "'\\:\\:timestamp ) ) ) ");
            }
        }
        else if ( filter.getDateCanalizationFrom() != null || filter.getDateCanalizationTo() != null) {
            throw new BadParametersException("the date_canalization_from and date_canalization_to must be present together ", MessageCode.CLAIMS_1011);
        }

        if ( filter.getStatusRepair() != null) {
            RepairStatusEnum repairStatusEnum = RepairStatusEnum.create( filter.getStatusRepair() );
            whereCondition.append(" AND ( lower(c.status_repair) ILIKE lower('" + repairStatusEnum.getValue() + "')) ");
        }

        if ( util.isNotEmpty(filter.getPlateCtp()) ) {
            whereCondition.append(" AND c.vehicle->>'license_plate' ILIKE '%" + filter.getPlateCtp() + "%' ");
        }
        if (Util.isNotEmpty( filter.getPlateAld() )) {
            whereCondition.append(" AND claims.plate ILIKE '%" + filter.getPlateAld() + "%' ");
        }
        sqlQuery.append(whereCondition);
        sqlQuery.append(" ORDER BY c.counterparty_id ) t");

        System.out.println(sqlQuery);

        Query query = em.createNativeQuery(sqlQuery.toString());

        return (BigInteger) query.getSingleResult();
    }

    /*
    X	Targa ALD
    X	Targa CTP
    X	ID SX
    X	ID Repair
    X	Stato controparte
    X	Data Inserimento (arco temporale)
    ?	Data canalizzazione (arco temporale) // non la abbiamo
    ?   Conducente // counterparty.driver combo first name + last name?
    X	Ultimo contatto (arco temporale)
    ?   Operatore // assigner_to (dentro counterparty)
    X	Regione
    X	Città -> locality
    */
    public List<CounterpartyNewEntity> findCounterpartyForExportByPages(CtpExportFilter filter, int page, int pageSize) {
        String auxiliaryTableCounterparty = "WITH LC AS ( SELECT counterparty_id, jsonb_array_elements(last_contact) last_cont FROM counterparty_new ) ";

        StringBuilder sqlQuery = new StringBuilder(auxiliaryTableCounterparty);
        sqlQuery.append("SELECT * FROM (SELECT DISTINCT ON (c.counterparty_id) c.*, " +
                "claims.id as id, " +
                "claims.practice_id, " +
                "c.practice_id_counterparty, " +
                "c.driver ->>'id' as driver_id, " +
                "c.assigned_to," +
                "claims.claim_address_region as region," +
                "claims.claim_address_locality as city, " +
                "(claims.created_at)\\:\\:timestamp as claims_created_at " +
                "FROM counterparty_new c INNER JOIN claims_new claims on c.claims_id = claims.id LEFT JOIN LC USING (counterparty_id) ");

        // ELIGIBILITY AND VEHICLE TYPE
        StringBuilder whereCondition = new StringBuilder("WHERE c.type = '" + VehicleTypeEnum.VEHICLE + "' AND c.eligibility = true ");

        if (Util.isNotEmpty(filter.getIdRepair())) {
            whereCondition.append(" AND c.practice_id_counterparty = " + filter.getIdRepair());
        }
        if (Util.isNotEmpty(filter.getDriverFirstName())) {
            whereCondition.append(" AND c.driver ->>'firstname' ILIKE '%" + filter.getDriverFirstName() + "%' ");
        }
        if (Util.isNotEmpty( filter.getDriverLastName() )) {
            whereCondition.append(" AND c.driver ->>'lastname' ILIKE '%" + filter.getDriverLastName() + "%' ");
        }
        if (Util.isNotEmpty( filter.getOperator() )) {
            whereCondition.append(" AND c.assigned_to ->>'id' = '" + filter.getOperator() + "' ");
        }
        if (Util.isNotEmpty( filter.getRegion() )) {
            whereCondition.append(" AND claims.claim_address_region ILIKE '%" + filter.getRegion() + "%' ");
        }
        if (Util.isNotEmpty( filter.getCity() )) {
            whereCondition.append(" AND claims.claim_address_locality ILIKE '%" + filter.getCity() + "%' ");
        }

        if (filter.getLastContactFrom() != null && filter.getLastContactTo() != null) {
            if ( filter.getLastContactFrom().length() == 28) {
                filter.setLastContactFrom( filter.getLastContactFrom().substring(0, 23) ) ;
            }
            if ( filter.getLastContactTo().length() == 28) {
                filter.setLastContactTo( filter.getLastContactTo().substring(0, 23) );
            }
            if ( filter.getLastContactTo().compareTo( filter.getLastContactFrom() ) < 0 ) {
                throw new BadParametersException("the lastContactFrom is less than the lastContactTo", MessageCode.CLAIMS_1011);
            }
            else {
                whereCondition.append(" AND (last_cont->>'last_call')\\:\\:timestamp >= ('" + filter.getLastContactFrom() + "')\\:\\:timestamp AND (last_cont->>'last_call')\\:\\:timestamp <= ('" + filter.getLastContactTo() + "')\\:\\:timestamp ");
            }
        }
        else if ( filter.getLastContactFrom() != null || filter.getLastContactTo() != null) {
            throw new BadParametersException("the lastContactFrom and lastContactTo must be present together ", MessageCode.CLAIMS_1011);
        }

        if ( Util.isNotEmpty(filter.getPracticeId()) ) {
            whereCondition.append(" AND claims.practice_id =" + filter.getPracticeId());
        }

        if ( filter.getDateCreatedFrom() != null && filter.getDateCreatedTo() != null) {
            if ( filter.getDateCreatedFrom().length() == 28) {
                filter.setDateCreatedFrom( filter.getDateCreatedFrom().substring(0, 23) );
            }
            if ( filter.getDateCreatedTo().length() == 28) {
                filter.setDateCreatedTo( filter.getDateCreatedTo().substring(0, 23) );
            }
            if ( filter.getDateCreatedTo().compareTo( filter.getDateCreatedFrom() ) < 0 ) {
                throw new BadParametersException("the date_created_to is less than the date_created_from", MessageCode.CLAIMS_1011);
            }
            else {
                whereCondition.append(" AND ( ( (c.created_at\\:\\:timestamp >= '" + filter.getDateCreatedFrom() + "'\\:\\:timestamp  ) AND (c.created_at\\:\\:timestamp <= '" + filter.getDateCreatedTo() + "'\\:\\:timestamp ) ) ) ");
            }
        }
        else if ( filter.getDateCreatedFrom() != null || filter.getDateCreatedTo() != null ) {
            throw new BadParametersException("the date_created_to and date_created_from must be present together ", MessageCode.CLAIMS_1011);
        }

        if ( filter.getDateCanalizationFrom() != null && filter.getDateCanalizationTo() != null) {
            if ( filter.getDateCanalizationFrom().length() == 28) {
                filter.setDateCanalizationFrom( filter.getDateCanalizationFrom().substring(0, 23) );
            }
            if ( filter.getDateCanalizationTo().length() == 28) {
                filter.setDateCanalizationTo( filter.getDateCanalizationTo().substring(0, 23) );
            }
            if ( filter.getDateCanalizationTo().compareTo( filter.getDateCanalizationFrom() ) < 0 ) {
                throw new BadParametersException("the date_canalization_to is less than the date_canalization_from", MessageCode.CLAIMS_1011);
            } else {
                whereCondition.append(" AND ( ( (c.repair_created_at\\:\\:timestamp >= '" + filter.getDateCanalizationFrom() + "'\\:\\:timestamp  ) AND (c.repair_created_at\\:\\:timestamp <= '" + filter.getDateCanalizationTo() + "'\\:\\:timestamp ) ) ) ");
            }
        } else if ( filter.getDateCanalizationFrom() != null || filter.getDateCanalizationTo() != null ) {
            throw new BadParametersException("the date_canalization_from and date_canalization_to must be present together ", MessageCode.CLAIMS_1011);
        }

        if ( filter.getStatusRepair() != null) {
            RepairStatusEnum repairStatusEnum = RepairStatusEnum.create( filter.getStatusRepair() );
            whereCondition.append(" AND ( lower(c.status_repair) ILIKE lower('" + repairStatusEnum.getValue() + "')) ");
        }

        if (util.isNotEmpty( filter.getPlateCtp() )) {
            whereCondition.append(" AND c.vehicle->>'license_plate' ILIKE '%" + filter.getPlateCtp() + "%' ");
        }
        if (Util.isNotEmpty( filter.getPlateAld() )) {
            whereCondition.append(" AND claims.plate ILIKE '%" + filter.getPlateAld() + "%' ");
        }
        sqlQuery.append(whereCondition);
        sqlQuery.append(" ORDER BY c.counterparty_id ) t");

        //LIMIT
        sqlQuery.append(" LIMIT " + pageSize + " OFFSET " + (page - 1) * pageSize);

        System.out.println(sqlQuery);

        Query query = em.createNativeQuery(sqlQuery.toString(), "CounterpartyNewResults");

        return query.getResultList();
    }

}
