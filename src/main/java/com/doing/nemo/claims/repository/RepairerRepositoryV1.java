package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.entity.settings.RepairerEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class RepairerRepositoryV1 {
    @PersistenceContext
    @Autowired
    private EntityManager em;

    public List<RepairerEntity> findPaginationRepairer(Integer page, Integer pageSize){
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<RepairerEntity> repairerCriteriaQuery = criteriaBuilder.createQuery(RepairerEntity.class);
        Root<RepairerEntity> repairerRoot = repairerCriteriaQuery.from(RepairerEntity.class);
        repairerCriteriaQuery.select(repairerRoot);

        List<RepairerEntity> repairerEntityList = em.createQuery(repairerCriteriaQuery).setFirstResult((page-1)*pageSize).setMaxResults(pageSize).getResultList();
        return repairerEntityList;
    }

    public Long countPaginationRepairer() {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<Long> repairerCriteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<RepairerEntity> repairerRoot = repairerCriteriaQuery.from(RepairerEntity.class);
        repairerCriteriaQuery.select(criteriaBuilder.count(repairerRoot));

        Query sql = em.createQuery(repairerCriteriaQuery);
        return (Long)sql.getSingleResult();
    }
}
