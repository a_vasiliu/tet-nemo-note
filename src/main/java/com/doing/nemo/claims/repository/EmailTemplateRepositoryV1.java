package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.entity.enumerated.ClaimsRepairEnum;
import com.doing.nemo.claims.entity.enumerated.EmailTemplateEnum;
import com.doing.nemo.claims.entity.enumerated.EventTypeEnum;
import com.doing.nemo.claims.entity.settings.EmailTemplateEntity;
import com.doing.nemo.claims.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.*;

import static com.doing.nemo.claims.entity.jsonb.JsonbProcessingFunctions.JSONB_ARRAY_ELEMENTS;


@Repository
public class EmailTemplateRepositoryV1 {

    @PersistenceContext
    @Autowired
    private EntityManager em;

    public List<EmailTemplateEntity> getTemplateEntityByEventDescription(String descriptionEvent, String flow, String customerId, String type, String typeAccident) {
        String auxiliaryTableFlows = "WITH FL AS (SELECT id, ".concat(JSONB_ARRAY_ELEMENTS.value()).concat("(flows) AS flow FROM email_template) ");
        String joinTable = " INNER JOIN FL USING (id) INNER JOIN event_type ON e.application_event_type_id = event_type.id ";
        StringBuilder queryBuilder = new StringBuilder(auxiliaryTableFlows);
        queryBuilder.append(" SELECT e.* FROM email_template e");
        queryBuilder.append(joinTable);
        StringBuilder whereClauseBuilder = new StringBuilder(" WHERE e.is_active = true ");

        if (Util.isNotEmpty(descriptionEvent)) {
            whereClauseBuilder.append(" AND event_type = '" + descriptionEvent + "' ");
        }
        if (Util.isNotEmpty(flow)) {
            whereClauseBuilder.append(" AND flow\\:\\:varchar like '%"+flow+"%' ");
        }
        if (Util.isNotEmpty(typeAccident)) {
            whereClauseBuilder.append(" AND accident_type_list\\:\\:varchar like '%"+typeAccident+"%' ");
        } else {
            whereClauseBuilder.append(" AND (accident_type_list\\:\\:varchar IS NULL OR  accident_type_list\\:\\:varchar = '' ) ");
        }
        if (Util.isNotEmpty(customerId)) {
            whereClauseBuilder.append(" AND client_code = '"+customerId+"' ");
        } else {
            whereClauseBuilder.append(" AND (client_code IS NULL OR client_code = '') ");
        }

        queryBuilder.append(whereClauseBuilder);

        Query sql = em.createNativeQuery(queryBuilder.toString(), EmailTemplateEntity.class);

        return sql.getResultList();
    }

    public List<EmailTemplateEntity> findPaginationEmailTemplate(Integer page, Integer pageSize,String flows, ClaimsRepairEnum claimsRepairEnum, String orderBy, Boolean asc, EventTypeEnum applicationEventTypeId, EmailTemplateEnum type, String description, String object, String heading, String body, String foot, Boolean attachFile, Boolean splittingRecipientsEmail, String clientCode, Boolean isActive){

        Map<String, String> mappOrder = new HashMap<>();
        mappOrder.put("event_type", " event_type ");
        mappOrder.put("event_type_description", " event_type.description ");
        mappOrder.put("type", " e.type ");
        mappOrder.put("description", " e.description ");
        mappOrder.put("object", " e.object ");
        mappOrder.put("heading", " e.heading ");
        mappOrder.put("body", " e.body ");
        mappOrder.put("foot", " e.foot ");
        mappOrder.put("attach_file", " e.attach_file ");
        mappOrder.put("splitting_recipients_email", " e.splitting_recipients_email ");
        mappOrder.put("client_code", " e.client_code ");
        mappOrder.put("type_complaint", " e.type_complaint ");
        mappOrder.put("is_active", " e.is_active ");

        String auxiliaryTableFlows = "WITH FL AS (SELECT id, ".concat(JSONB_ARRAY_ELEMENTS.value()).concat("(flows) AS flow FROM email_template) ");
        String joinTable = " INNER JOIN FL USING (id) INNER JOIN event_type ON e.application_event_type_id = event_type.id ";
        StringBuilder queryBuilder = new StringBuilder(auxiliaryTableFlows);
        queryBuilder.append(" SELECT DISTINCT e.*,event_type.event_type, event_type.description  FROM email_template e ");
        queryBuilder.append(joinTable);
        StringBuilder whereClauseBuilder = new StringBuilder(" WHERE 1=1 ");

        String order = null;
        if(asc != null){
            if (asc) {
                order = "ASC";
            } else {
                order = "DESC";
            }
        }
        List<String> whereCondition = new LinkedList<>();
        if(claimsRepairEnum != null)
            whereClauseBuilder.append(" AND e.type_complaint = '"+claimsRepairEnum.getValue()+"' ");
        if(applicationEventTypeId != null)
            whereClauseBuilder.append(" AND event_type = '"+applicationEventTypeId+"' ");
        if(type != null)
            whereClauseBuilder.append(" AND e.type = '"+type.getValue()+"' ");
        if(Util.isNotEmpty(description))
            whereClauseBuilder.append(" AND e.description ILIKE '%"+description+"%' ");
        if(Util.isNotEmpty(object))
            whereClauseBuilder.append(" AND e.object ILIKE '%"+object+"%' ");
        if(Util.isNotEmpty(heading))
            whereClauseBuilder.append(" AND e.heading ILIKE '%"+heading+"%' ");
        if(Util.isNotEmpty(body))
            whereClauseBuilder.append(" AND e.body ILIKE '%"+body+"%' ");
        if(Util.isNotEmpty(foot))
            whereClauseBuilder.append(" AND e.foot ILIKE '%"+foot+"%' ");
        if(attachFile != null)
            whereClauseBuilder.append(" AND e.attach_file = "+attachFile+" ");
        if(splittingRecipientsEmail != null)
            whereClauseBuilder.append(" AND e.splitting_recipients_email = "+splittingRecipientsEmail+" ");
        if(Util.isNotEmpty(clientCode))
            whereClauseBuilder.append(" AND e.client_code ILIKE '%"+clientCode+"%' ");
        if(isActive != null)
            whereClauseBuilder.append(" AND e.is_active = "+isActive+" ");
        if (Util.isNotEmpty(flows)) {

            List<String> flowList = Arrays.asList(flows.split("\\s*,\\s*"));
            List<String> flowTypeList = new LinkedList<>();
            int i =1;
            whereClauseBuilder.append(" AND ( ");
            for(String flow: flowList) {
                if(i == 1)
                    whereClauseBuilder.append(" upper(flow\\:\\:varchar) like upper('%" + flow + "%') ");
                else  whereClauseBuilder.append(" OR upper(flow\\:\\:varchar) like upper('%" + flow + "%') ");
                i++;
            }
            whereClauseBuilder.append(" )");
        }

        queryBuilder.append(whereClauseBuilder);
        String fieldOrder = null;
        if(orderBy != null) fieldOrder = mappOrder.get(orderBy);
        if(fieldOrder != null) {
            if (asc != null) {
                queryBuilder.append(" ORDER BY " + fieldOrder + " " + order);
            } else  {
                queryBuilder.append(" ORDER BY " +fieldOrder + " ");
            }
        }

        queryBuilder.append(" LIMIT " + pageSize + " OFFSET "+(page-1)*pageSize);


        Query sql = em.createNativeQuery(queryBuilder.toString(), "OrderResults");

        List<EmailTemplateEntity> result = sql.getResultList();
        //resetQuery();
        return result;
    }

    public BigInteger countPaginationEmailTemplate(ClaimsRepairEnum claimsRepairEnum, String flows, EventTypeEnum applicationEventTypeId, EmailTemplateEnum type, String description, String object, String heading, String body, String foot, Boolean attachFile, Boolean splittingRecipientsEmail, String clientCode, Boolean isActive) {
        String auxiliaryTableFlows = "WITH FL AS (SELECT id, ".concat(JSONB_ARRAY_ELEMENTS.value()).concat("(flows) AS flow FROM email_template) ");
        String joinTable = " INNER JOIN FL USING (id) INNER JOIN event_type ON e.application_event_type_id = event_type.id ";
        StringBuilder queryBuilder = new StringBuilder(auxiliaryTableFlows);
        queryBuilder.append(" SELECT count(DISTINCT(e.id)) FROM email_template e ");
        queryBuilder.append(joinTable);
        StringBuilder whereClauseBuilder = new StringBuilder(" WHERE 1=1 ");

        //List<String> whereCondition = new LinkedList<>();
        if(claimsRepairEnum != null)
            whereClauseBuilder.append(" AND e.type_complaint = '"+claimsRepairEnum.getValue()+"' ");
        if(applicationEventTypeId != null)
            whereClauseBuilder.append(" AND event_type = '"+applicationEventTypeId+"' ");
        if(type != null)
            whereClauseBuilder.append(" AND e.type = '"+type.getValue()+"' ");
        if(Util.isNotEmpty(description))
            whereClauseBuilder.append(" AND e.description ILIKE '%"+description+"%' ");
        if(Util.isNotEmpty(object))
            whereClauseBuilder.append(" AND e.object ILIKE '%"+object+"%' ");
        if(Util.isNotEmpty(heading))
            whereClauseBuilder.append(" AND e.heading ILIKE '%"+heading+"%' ");
        if(Util.isNotEmpty(body))
            whereClauseBuilder.append(" AND e.body ILIKE '%"+body+"%' ");
        if(Util.isNotEmpty(foot))
            whereClauseBuilder.append(" AND e.foot ILIKE '%"+foot+"%' ");
        if(attachFile != null)
            whereClauseBuilder.append(" AND e.attach_file = "+attachFile+" ");
        if(splittingRecipientsEmail != null)
            whereClauseBuilder.append(" AND e.splitting_recipients_email = "+splittingRecipientsEmail+" ");
        if(Util.isNotEmpty(clientCode))
            whereClauseBuilder.append(" AND e.client_code ILIKE '%"+clientCode+"%' ");
        if(isActive != null)
            whereClauseBuilder.append(" AND e.is_active = "+isActive+" ");
        if (Util.isNotEmpty(flows)) {

            List<String> flowList = Arrays.asList(flows.split("\\s*,\\s*"));
            List<String> flowTypeList = new LinkedList<>();
            int i =1;
            whereClauseBuilder.append(" AND ( ");
            for(String flow: flowList) {
                if(i == 1)
                    whereClauseBuilder.append(" upper(flow\\:\\:varchar) like upper('%" + flow + "%') ");
                else  whereClauseBuilder.append(" OR upper(flow\\:\\:varchar like upper('%" + flow + "%') ");
                i++;
            }
            whereClauseBuilder.append(" )");
        }

        queryBuilder.append(whereClauseBuilder);

        Query sql = em.createNativeQuery(queryBuilder.toString());
        //resetQuery();
        return (BigInteger)sql.getSingleResult();

    }
}
