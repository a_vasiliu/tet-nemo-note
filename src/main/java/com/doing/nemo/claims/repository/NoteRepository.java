package com.doing.nemo.claims.repository;


import com.doing.nemo.claims.entity.NoteEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NoteRepository extends JpaRepository<NoteEntity, String> {}
