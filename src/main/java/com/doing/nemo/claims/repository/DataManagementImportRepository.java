package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.entity.enumerated.FileImportTypeEnum;
import com.doing.nemo.claims.entity.settings.DataManagementImportEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface DataManagementImportRepository extends JpaRepository<DataManagementImportEntity, UUID> {


    /*
    @Query("SELECT d FROM DataManagementImportEntity d WHERE d.result = 'WAITING_FOR_PROCESSING' AND d.fileType = :fileInputType")
    List<DataManagementImportEntity> getDataManagement(FileImportTypeEnum fileInputType);
    */

    @Query("SELECT d FROM DataManagementImportEntity d WHERE (d.result = 'WAITING_FOR_PROCESSING' OR d.result = 'WAITING_FOR_PROCESSING_WITH_WASTE') AND d.fileType = :fileInputType")
    List<DataManagementImportEntity> getDataManagement(FileImportTypeEnum fileInputType);

}
