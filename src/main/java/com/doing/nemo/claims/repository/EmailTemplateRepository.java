package com.doing.nemo.claims.repository;


import com.doing.nemo.claims.entity.enumerated.ClaimsRepairEnum;
import com.doing.nemo.claims.entity.settings.EmailTemplateEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface EmailTemplateRepository extends JpaRepository<EmailTemplateEntity, UUID> {

    @Query("SELECT et FROM EmailTemplateEntity et WHERE et.typeComplaint = :typeComplaint")
    List<EmailTemplateEntity> searchEmailTemplateByTypeComplaint(@Param("typeComplaint") ClaimsRepairEnum typeComplaint);
}
