package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.entity.settings.FlowContractTypeEntity;
import com.doing.nemo.claims.entity.settings.FlowContractTypeId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FlowContractTypeRepository extends JpaRepository<FlowContractTypeEntity, FlowContractTypeId> {

}
