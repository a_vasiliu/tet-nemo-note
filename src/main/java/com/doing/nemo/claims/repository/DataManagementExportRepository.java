package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.entity.settings.DataManagementExportEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface DataManagementExportRepository extends JpaRepository<DataManagementExportEntity, UUID> {

    @Query(value= " SELECT d FROM DataManagementExportEntity d ORDER BY d.date DESC")
    List<DataManagementExportEntity> getAllDataOrderBiDateDesc();

}
