package com.doing.nemo.claims.repository;


import com.doing.nemo.claims.entity.enumerated.ClaimsRepairEnum;
import com.doing.nemo.claims.entity.settings.ManagerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface ManagerRepository extends JpaRepository<ManagerEntity, UUID> {

    @Query("SELECT m FROM ManagerEntity m WHERE m.name = :name")
    ManagerEntity searchManagerByName(@Param("name") String name);

    @Query("SELECT m FROM ManagerEntity m WHERE m.typeComplaint = :typeComplaint")
    List<ManagerEntity> searchManagerByTypeComplaint(@Param("typeComplaint") ClaimsRepairEnum typeComplaint);
}
