package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.settings.ExportClaimsTypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface ExportClaimsTypeRepository extends JpaRepository<ExportClaimsTypeEntity, UUID> {

    @Query("SELECT exp FROM ExportClaimsTypeEntity exp WHERE exp.claimsType = :claimsType AND exp.isActive = true")
    ExportClaimsTypeEntity findExportClaimsTypeByClaimsType(@Param("claimsType")DataAccidentTypeAccidentEnum claimsType);

    @Query("SELECT exp FROM ExportClaimsTypeEntity exp WHERE exp.claimsType = :claimsType ")
    List<ExportClaimsTypeEntity> checkDuplicateExportClaimsType(@Param("claimsType")DataAccidentTypeAccidentEnum claimsType);
}
