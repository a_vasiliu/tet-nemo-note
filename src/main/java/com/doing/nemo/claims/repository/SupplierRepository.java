package com.doing.nemo.claims.repository;


import com.doing.nemo.claims.entity.SupplierEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface SupplierRepository extends JpaRepository<SupplierEntity, UUID> {


    @Query("SELECT s FROM SupplierEntity s WHERE s.codSupplier = :codSupplier")
    SupplierEntity searchSupplierbyCode(@Param("codSupplier") String codSupplier);

    @Query("SELECT s FROM SupplierEntity s WHERE s.id <> :id AND s.codSupplier = :codSupplier")
    SupplierEntity searchSupplierWithDifferentId(@Param("id") UUID id, @Param("codSupplier") String codSupplier);



}
