package com.doing.nemo.claims.repository;


import com.doing.nemo.claims.entity.settings.NotificationTypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface NotificationTypeRepository extends JpaRepository<NotificationTypeEntity, UUID> {
    @Query("SELECT n FROM NotificationTypeEntity n ORDER BY n.orderId ASC")
    List<NotificationTypeEntity> findAllWithOrder();
}
