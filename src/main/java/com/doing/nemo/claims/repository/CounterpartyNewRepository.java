package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.entity.CounterpartyNewEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import java.util.List;

public interface CounterpartyNewRepository extends JpaRepository<CounterpartyNewEntity, String>,CounterpartyNewRepositoryCustom {

    @Query("SELECT c FROM CounterpartyNewEntity c WHERE c.claims.id = :claimsId")
    List<CounterpartyNewEntity> findAllByClaimsId(@Param("claimsId") String claimsId);

    @Query("SELECT c FROM CounterpartyNewEntity c WHERE practice_id_counterparty = :practice_id_counterparty ")
    CounterpartyNewEntity findByPracticeIdCounterparty(@Param("practice_id_counterparty") Long practiceIdCounterparty);
}
