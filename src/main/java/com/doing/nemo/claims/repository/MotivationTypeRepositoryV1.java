package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.entity.settings.MotivationTypeEntity;
import com.doing.nemo.claims.entity.settings.MotivationTypeEntity_;
import com.doing.nemo.claims.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class MotivationTypeRepositoryV1 {

    @PersistenceContext
    @Autowired
    private EntityManager em;


    public List<MotivationTypeEntity> findMotivationTypesFiltered(Integer page, Integer pageSize, String orderBy, Boolean asc, Long motivationTypeId, String description, Integer orderId, Boolean isActive) {

        Map<String, String> mappOrder = new HashMap<>();
        mappOrder.put("motivation_type_id", "motivationTypeId");
        mappOrder.put("description", "description");
        mappOrder.put("order", "orderId");
        mappOrder.put("is_active", "isActive");

        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<MotivationTypeEntity> motivationTypeCriteriaQuery = criteriaBuilder.createQuery(MotivationTypeEntity.class);
        Root<MotivationTypeEntity> motivationTypeRoot = motivationTypeCriteriaQuery.from(MotivationTypeEntity.class);

        motivationTypeCriteriaQuery.select(motivationTypeRoot);

        //Constructing list of parameters
        List<Predicate> predicates = new ArrayList<>();

        if (motivationTypeId != null) {
            predicates.add(criteriaBuilder.equal(
                    motivationTypeRoot.get(MotivationTypeEntity_.motivationTypeId), motivationTypeId));
        }
        if (Util.isNotEmpty(description)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    motivationTypeRoot.get(MotivationTypeEntity_.description)), "%"+description.toUpperCase()+"%"));
        }
        if (orderId != null) {
            predicates.add(criteriaBuilder.equal(
                    motivationTypeRoot.get(MotivationTypeEntity_.orderId), orderId));
        }
        if (isActive != null) {
            predicates.add(criteriaBuilder.equal(
                    motivationTypeRoot.get(MotivationTypeEntity_.isActive), isActive));
        }

        motivationTypeCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));

        //List<Order> orderByList = new LinkedList<>();
        String fieldToOrder = null;
        if(orderBy != null)
            fieldToOrder = mappOrder.get(orderBy);
        if(fieldToOrder != null && asc != null){
            if (asc) {
                motivationTypeCriteriaQuery.orderBy(criteriaBuilder.asc(motivationTypeRoot.get(fieldToOrder)));
            } else {
                motivationTypeCriteriaQuery.orderBy(criteriaBuilder.desc(motivationTypeRoot.get(fieldToOrder)));
            }

        } else if(fieldToOrder != null) {
            motivationTypeCriteriaQuery.orderBy(criteriaBuilder.desc(motivationTypeRoot.get(fieldToOrder)));
        }

        List<MotivationTypeEntity> motivationTypeEntityList = em.createQuery(motivationTypeCriteriaQuery).setFirstResult((page-1)*pageSize).setMaxResults(pageSize).getResultList();
        return motivationTypeEntityList;

    }

    public Long countMotivationTypeFiltered(Long motivationTypeId, String description, Integer orderId, Boolean isActive) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<Long> motivationTypeCriteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<MotivationTypeEntity> motivationTypeRoot = motivationTypeCriteriaQuery.from(MotivationTypeEntity.class);

        motivationTypeCriteriaQuery.select(criteriaBuilder.count(motivationTypeRoot));

        //Constructing list of parameters
        List<Predicate> predicates = new ArrayList<>();

        if (motivationTypeId != null) {
            predicates.add(criteriaBuilder.equal(
                    motivationTypeRoot.get(MotivationTypeEntity_.motivationTypeId), motivationTypeId));
        }
        if (Util.isNotEmpty(description)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.upper(
                    motivationTypeRoot.get(MotivationTypeEntity_.description)), "%"+description.toUpperCase()+"%"));
        }
        if (orderId != null) {
            predicates.add(criteriaBuilder.equal(
                    motivationTypeRoot.get(MotivationTypeEntity_.orderId), orderId));
        }
        if (isActive != null) {
            predicates.add(criteriaBuilder.equal(
                    motivationTypeRoot.get(MotivationTypeEntity_.isActive), isActive));
        }

        motivationTypeCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));
        Query sql = em.createQuery(motivationTypeCriteriaQuery);
        return (Long)sql.getSingleResult();

    }

}
