package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.entity.enumerated.ClaimsRepairEnum;
import com.doing.nemo.claims.entity.settings.GenericAttachmentEntity;
import com.doing.nemo.claims.entity.settings.GenericAttachmentEntity_;
import com.doing.nemo.claims.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Repository
public class GenericAttachmentRepositoryV1 {
    @PersistenceContext
    @Autowired
    private EntityManager em;

    public List<GenericAttachmentEntity> findPaginationGenericAttachment(Integer page, Integer pageSize, String claimsRepairEnum){
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<GenericAttachmentEntity> genericAttachmentCriteriaQuery = criteriaBuilder.createQuery(GenericAttachmentEntity.class);
        Root<GenericAttachmentEntity> genericAttachmentRoot = genericAttachmentCriteriaQuery.from(GenericAttachmentEntity.class);
        genericAttachmentCriteriaQuery.select(genericAttachmentRoot);

        //Constructing list of parameters
        List<Predicate> predicates = new ArrayList<>();


        if (Util.isNotEmpty(claimsRepairEnum)) {
            predicates.add(criteriaBuilder.equal(
                    genericAttachmentRoot.get(GenericAttachmentEntity_.typeComplaint), ClaimsRepairEnum.create(claimsRepairEnum)));
        }

        genericAttachmentCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));

        List<GenericAttachmentEntity> genericAttachmentEntityList = em.createQuery(genericAttachmentCriteriaQuery).setFirstResult((page-1)*pageSize).setMaxResults(pageSize).getResultList();
        return genericAttachmentEntityList;
    }

    public Long countPaginationGenericAttachment(String claimsRepairEnum) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<Long> genericAttachmentCriteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<GenericAttachmentEntity> genericAttachmentRoot = genericAttachmentCriteriaQuery.from(GenericAttachmentEntity.class);
        genericAttachmentCriteriaQuery.select(criteriaBuilder.count(genericAttachmentRoot));


        //Constructing list of parameters
        List<Predicate> predicates = new ArrayList<>();


        if (Util.isNotEmpty(claimsRepairEnum)) {
            predicates.add(criteriaBuilder.equal(
                    genericAttachmentRoot.get(GenericAttachmentEntity_.typeComplaint), ClaimsRepairEnum.create(claimsRepairEnum)));
        }

        genericAttachmentCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));

        Query sql = em.createQuery(genericAttachmentCriteriaQuery);
        return (Long)sql.getSingleResult();
    }
}
