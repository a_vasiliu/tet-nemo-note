package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.ComplaintNotificationEnum;
import com.doing.nemo.claims.entity.settings.ExportNotificationTypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface ExportNotificationTypeRepository extends JpaRepository<ExportNotificationTypeEntity, UUID> {

    @Query(value = "SELECT exp FROM ExportNotificationTypeEntity exp WHERE notificationType.description = :notificationDescription AND exp.isActive = true")
    ExportNotificationTypeEntity findExportNotificationTypeByNotificationDescription(String notificationDescription);

    @Query(value = "SELECT exp FROM ExportNotificationTypeEntity exp WHERE notificationType.description = :notificationDescription")
    List<ExportNotificationTypeEntity> checkDuplicateExportNotificationType(String notificationDescription);

    @Query("SELECT n FROM ExportNotificationTypeEntity n WHERE n.notificationType = :notificationType")
    ExportNotificationTypeEntity findExportNotificationTypeByNotificationType(@Param("notificationType") ComplaintNotificationEnum notificationType);

}
