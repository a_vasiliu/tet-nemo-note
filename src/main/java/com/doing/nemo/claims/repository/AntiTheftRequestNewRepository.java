package com.doing.nemo.claims.repository;


import com.doing.nemo.claims.entity.AntiTheftRequestEntity;
import com.doing.nemo.claims.entity.AntiTheftRequestNewEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AntiTheftRequestNewRepository extends JpaRepository<AntiTheftRequestNewEntity, String> {

    @Query(value = "SELECT a FROM AntiTheftRequestNewEntity a  WHERE a.claim.id = :claimId ")
    List<AntiTheftRequestNewEntity> getAllCrashByClaim(@Param("claimId") String claimId);
}
