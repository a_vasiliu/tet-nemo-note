package com.doing.nemo.claims.repository;
import com.doing.nemo.claims.entity.MilesExternalEntity;
import com.doing.nemo.claims.entity.WebSinExternalEntity;
import com.doing.nemo.claims.entity.enumerated.ExternalCommunicationEnum;
import io.lettuce.core.dynamic.annotation.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface WebSinExternalRepository extends JpaRepository<WebSinExternalEntity, UUID> {
    @Query("SELECT r FROM WebSinExternalEntity r WHERE r.status = :status and r.claimsId = :claimsId")
    List<WebSinExternalEntity> searchByStatus(@Param("status") ExternalCommunicationEnum status, @Param("claimsId") String claimsId);
}
