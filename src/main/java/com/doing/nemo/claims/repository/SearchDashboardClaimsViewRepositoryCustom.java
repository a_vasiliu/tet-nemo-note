package com.doing.nemo.claims.repository;

import com.doing.nemo.claims.dto.FilterView;
import com.doing.nemo.claims.entity.view.SearchDashboardClaimsView;
import com.doing.nemo.claims.repository.specification.SearchDashboardClaimsViewSpecification;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Repository;

@Repository
public interface SearchDashboardClaimsViewRepositoryCustom {
  //  BigInteger countClaimsForPaginationWithoutDraftAndTheft(Boolean inEvidence, Long practiceId, List<String> flow, List<Long> clientIdList, String plate, List<String> status, String locator, String dateAccidentFrom, String dateAccidentTo, List<String> typeAccident, Boolean asc, String orderBy, Integer page, Integer pageSize, Boolean withContinuation, String company, String clientName, Boolean poVariation, Boolean waitingForNumberSx, Long fleetVehicleId, Boolean waitingForAuthority);
  //Page<SearchDashboardClaimsView> findClaimsViewForPaginationWithoutDraftAndTheft(FilterView filter, Pageable pageable);
  Page<SearchDashboardClaimsView> findAll(Specification<SearchDashboardClaimsView> spec, Pageable pageable);
}
