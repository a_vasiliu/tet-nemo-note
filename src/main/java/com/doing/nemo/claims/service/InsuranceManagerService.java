package com.doing.nemo.claims.service;

import com.doing.nemo.claims.controller.payload.response.InsuranceManagerResponseV1;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.settings.InsuranceManagerEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public interface InsuranceManagerService {

    InsuranceManagerEntity insertManager(InsuranceManagerEntity insuranceManagerEntity);

    InsuranceManagerEntity updateManager(InsuranceManagerEntity insuranceManagerEntity);

    InsuranceManagerResponseV1 deleteManager(UUID uuid);

    InsuranceManagerEntity selectManager(UUID uuid);

    List<InsuranceManagerEntity> selectAllManager();

    InsuranceManagerEntity updateStatusManager(UUID uuid);

    Pagination<InsuranceManagerEntity> paginationInsuranceManager(int page, int pageSize, String orderBy, Boolean asc, String name, String province, String telephone, String fax, String email, Boolean isActive, String address, String zipCode, String city, String state, Long rifCode);
}
