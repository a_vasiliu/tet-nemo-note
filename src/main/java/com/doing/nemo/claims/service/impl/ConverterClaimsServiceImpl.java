package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.adapter.*;
import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.entity.ClaimsAuthorityEmbeddedEntity;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.ClaimsNewEntity;
import com.doing.nemo.claims.entity.claims.*;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.doing.nemo.claims.entity.jsonb.Address;
import com.doing.nemo.claims.entity.jsonb.Authority;
import com.doing.nemo.claims.entity.jsonb.Historical;
import com.doing.nemo.claims.entity.jsonb.Theft;
import com.doing.nemo.claims.entity.jsonb.complaint.Complaint;
import com.doing.nemo.claims.entity.jsonb.complaint.jsonbComplaint.Entrusted;
import com.doing.nemo.claims.entity.jsonb.complaint.jsonbComplaint.FromCompany;
import com.doing.nemo.claims.entity.jsonb.complaint.jsonbComplaint.jsonbDataAccidents.DataAccident;
import com.doing.nemo.claims.entity.jsonb.damaged.Damaged;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.Contract;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.Customer;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.FleetManager;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.InsuranceCompany;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbAntiTheftService.AntiTheftService;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbDriver.Driver;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbVehicle.Vehicle;
import com.doing.nemo.claims.entity.jsonb.refund.Refund;
import com.doing.nemo.claims.entity.settings.AntiTheftServiceEntity;
import com.doing.nemo.claims.repository.AntiTheftServiceRepository;
import com.doing.nemo.claims.repository.ClaimsNewRepository;
import com.doing.nemo.claims.repository.ClaimsRepository;
import com.doing.nemo.claims.repository.ClaimsRepositoryV1;
import com.doing.nemo.claims.service.ClaimsService;
import com.doing.nemo.claims.service.ConverterClaimsService;
import com.doing.nemo.claims.util.MigrationUtil;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.LinkedList;
import java.util.List;

@Service
public class ConverterClaimsServiceImpl implements ConverterClaimsService {

    private static Logger LOGGER = LoggerFactory.getLogger(ConverterClaimsServiceImpl.class);

    @Autowired
    private ClaimsRepositoryV1 claimsRepositoryV1 ;

    @Autowired
    private ClaimsRepository claimsRepository;

    @Autowired
    private ClaimsNewRepository claimsNewRepository;

    @Autowired
    private AntiTheftServiceRepository antiTheftServiceRepository;

    @Autowired
    private AuthorityAdapter authorityAdapter;

    @Autowired
    private @Lazy ClaimsService claimsService;

    @Override
    public void convertClaims(Integer countTotalClaims){
        LOGGER.debug("extract method");

        //int coutTotalClaims =  claimsRepositoryV1.getCountAllClaimsOldDB() != null ? claimsRepositoryV1.getCountAllClaimsOldDB().intValue() : 0;
        LOGGER.debug("coutTotalClaims: " + countTotalClaims);
        double pagesNumberDiv = (double)countTotalClaims / 100;
        int pagesNumber = (int) Math.ceil(pagesNumberDiv);



        LOGGER.debug("pagesNumber: " + pagesNumber);
        LOGGER.debug("pageSize: " + 100);

        Boolean isEmpty = false;
        int page = 1;
        while (page <= pagesNumber && !isEmpty){
            LOGGER.debug("currentPage: " + page);

            List<ClaimsEntity> claimsEntityList = claimsRepositoryV1.getOldClaimsPaginated(100,page );
            if(claimsEntityList != null && !claimsEntityList.isEmpty()){
                LOGGER.debug("calling convetList");
                this.convertListPageSizeClaimsEntities(claimsEntityList);
            } else {
                isEmpty = true;
                LOGGER.debug("List empty. End of the conversion logic.");
            }
            page++;
        }
        /*for (int page = 1; page <= pagesNumber; page++) {
            LOGGER.debug("currentPage: " + page);

            List<ClaimsEntity> claimsEntityList = claimsRepositoryV1.getOldClaimsPaginated(100,page );
            if(claimsEntityList != null && !claimsEntityList.isEmpty()){
                LOGGER.debug("calling convetList");
                this.convertListPageSizeClaimsEntities(claimsEntityList);
            }

        }*/
    }




    private void convertListPageSizeClaimsEntities(List<ClaimsEntity> claimsEntityList){
        for(ClaimsEntity currentOldClaims : claimsEntityList){
            ClaimsNewEntity claimsNewEntity = this.convertOldEntityToNewClaimsEntity(currentOldClaims);
            claimsNewRepository.save(claimsNewEntity);
            currentOldClaims.setConverted(true);
            claimsRepository.save(currentOldClaims);
        }
    }


    private Instant getStatusUpdatedAt(List<Historical> historicalList, Instant createdAt){
        if(historicalList == null || historicalList.isEmpty()){
            return createdAt;
        }else{
            Instant lastStatusUpdated = null;
            for(Historical currentHistorical: historicalList){
                if(currentHistorical.getStatusEntityOld() != null && !currentHistorical.getStatusEntityOld().equals(currentHistorical.getStatusEntityNew())){
                    lastStatusUpdated = DateUtil.convertIS08601StringToUTCInstant(DateUtil.convertUTCDateToIS08601String(currentHistorical.getUpdateAt()));
                }
            }
            if(lastStatusUpdated == null){
                return createdAt;
            }
            return lastStatusUpdated;
        }
    }


    @Override
    public ClaimsNewEntity convertOldEntityToNewClaimsEntity(ClaimsEntity claimsEntity) {

        if(claimsEntity == null){
            //return null;
        }

        ClaimsNewEntity claimsNewEntity = new ClaimsNewEntity();

        claimsNewEntity.setId(claimsEntity.getId());
        claimsNewEntity.setCreatedAt(claimsEntity.getCreatedAt());
        claimsNewEntity.setUpdateAt(claimsEntity.getUpdateAt());

        if(claimsEntity.getPracticeId() != null) {
            LOGGER.debug("convertMethod claim practiceId " + claimsEntity.getPracticeId());
            claimsNewEntity.setPracticeId(claimsEntity.getPracticeId());
        }else{
            LOGGER.debug("convertMethod new claim to be inserted");
        }

        if (claimsEntity.getStatus()!=null&&claimsEntity.getStatus().getValue().equalsIgnoreCase("send_to_client")){
            claimsNewEntity.setStatus(ClaimsStatusEnum.TO_SEND_TO_CUSTOMER);
        }else{
            claimsNewEntity.setStatus(claimsEntity.getStatus());
        }

        claimsNewEntity.setStatusUpdatedAt(this.getStatusUpdatedAt(claimsEntity.getHistorical(),claimsNewEntity.getCreatedAt()));
        claimsNewEntity.setPracticeManager(claimsEntity.getPracticeManager());
        claimsNewEntity.setPaiComunication(claimsEntity.getPaiComunication());
        claimsNewEntity.setInEvidence(claimsEntity.getInEvidence());
        claimsNewEntity.setUserId(claimsEntity.getUserId());
        claimsNewEntity.setForced(claimsEntity.getForced());
        claimsNewEntity.setMotivation(claimsEntity.getMotivation());
        claimsNewEntity.setType(claimsEntity.getType());
        claimsNewEntity.setCompleteDocumentation(claimsEntity.getCompleteDocumentation());
        claimsNewEntity.setCaiDetails(claimsEntity.getCaiDetails());
        claimsNewEntity.setWithCounterparty(claimsEntity.getWithCounterparty());
        claimsNewEntity.setCounterparts(CounterpartyAdapter.adptCounterpartyOldToCounterpartyNewList(claimsEntity.getCounterparts()));
        claimsNewEntity.setDeponentList(claimsEntity.getDeponentList());
        claimsNewEntity.setWoundedList(claimsEntity.getWoundedList());
        claimsNewEntity.setNotes(NotesAdapter.adptNotesListRequestToNotesList(claimsEntity.getNotes()));
        claimsNewEntity.setHistorical(claimsEntity.getHistorical());
        claimsNewEntity.setExemption(claimsEntity.getExemption());
        claimsNewEntity.setIdSaleforce(claimsEntity.getIdSaleforce());
        claimsNewEntity.setWithContinuation(claimsEntity.getWithContinuation());
        claimsNewEntity.setRead(claimsEntity.getRead());
        claimsNewEntity.setAuthorityLinkable(claimsEntity.getAuthorityLinkable());
        claimsNewEntity.setPoVariation(claimsEntity.getPoVariation());
        claimsNewEntity.setLegalComunication(claimsEntity.getLegalComunication());
        claimsNewEntity.setTotalPenalty(claimsEntity.getTotalPenalty());
        claimsNewEntity.setReadAcclaims(claimsEntity.getReadAcclaims());
        claimsNewEntity.setMigrated(claimsEntity.getMigrated());
        claimsNewEntity.setForms(claimsEntity.getForms());
        claimsNewEntity.setAntiTheftRequestEntities(AntiTheftRequestAdapter.adptAntiTheftRequestOldToAntiTheftRequestNewList(claimsEntity.getAntiTheftRequestEntities(), claimsNewEntity));
        if(claimsEntity.getMetadata() != null) {
            claimsNewEntity.setClaimsMetadataEntity(MetadataAdapter.adptFromMetadataRequestToMetadataEntity(claimsEntity.getMetadata().getMetadata()));
        }

        //COMPLAINT

        if(claimsEntity.getComplaint()!=null){
            Complaint complaint = claimsEntity.getComplaint();
            claimsNewEntity.setClientId(complaint.getClientId());
            claimsNewEntity.setLocator(complaint.getLocator());
            claimsNewEntity.setActivation(complaint.getActivation());
            claimsNewEntity.setProperty(complaint.getProperty());
            claimsNewEntity.setMod(complaint.getMod());
            claimsNewEntity.setQuote(complaint.getQuote());
            claimsNewEntity.setNotification(complaint.getNotification());
            claimsNewEntity.setPlate(complaint.getPlate());
            if(complaint.getDataAccident()!=null){
                DataAccident dataAccident =complaint.getDataAccident();
                claimsNewEntity.setTypeAccident(dataAccident.getTypeAccident());
                claimsNewEntity.setResponsible(dataAccident.getResponsible());
                claimsNewEntity.setDateAccident(DateUtil.convertIS08601StringToUTCInstant(DateUtil.convertUTCDateToIS08601String(dataAccident.getDateAccident())));
                claimsNewEntity.setHappenedAbroad(dataAccident.getHappenedAbroad());
                claimsNewEntity.setDamageToObjects(dataAccident.getDamageToObjects());
                claimsNewEntity.setDamageToVehicles(dataAccident.getDamageToVehicles());
                claimsNewEntity.setOldMotorcyclePlates(dataAccident.getOldMotorcyclePlates());
                claimsNewEntity.setInterventionAuthority(dataAccident.getInterventionAuthority());
                claimsNewEntity.setPolice(dataAccident.getPolice());
                claimsNewEntity.setCc(dataAccident.getCc());
                claimsNewEntity.setVvuu(dataAccident.getVvuu());
                claimsNewEntity.setAuthorityData(dataAccident.getAuthorityData());
                claimsNewEntity.setWitnessDescription(dataAccident.getWitnessDescription());
                claimsNewEntity.setRecoverability(dataAccident.getRecoverability());
                claimsNewEntity.setRecoverabilityPercent(dataAccident.getRecoverabilityPercent());
                claimsNewEntity.setIncompleteMotivation(dataAccident.getIncompleteMotivation());
                claimsNewEntity.setRobbery(dataAccident.getRobbery());
                claimsNewEntity.setCenterNotified(dataAccident.getCenterNotified());
                claimsNewEntity.setHappenedOnCenter(dataAccident.getHappenedOnCenter());
                claimsNewEntity.setProviderCode(dataAccident.getProviderCode());
                claimsNewEntity.setTheftDescription(dataAccident.getTheftDescription());
                claimsNewEntity.setWithCounterpartyInternal(dataAccident.getWithCounterparty());
                if(dataAccident.getAddress()!=null){
                    Address addressRequest = dataAccident.getAddress();
                    claimsNewEntity.setClaimAddressStreet(addressRequest.getStreet());
                    claimsNewEntity.setClaimAddressStreetNr(addressRequest.getStreetNr());
                    claimsNewEntity.setClaimAddressStreetZip(addressRequest.getZip());
                    claimsNewEntity.setClaimAddressStreetLocality(addressRequest.getLocality());
                    claimsNewEntity.setClaimAddressStreetProvince(addressRequest.getProvince());
                    claimsNewEntity.setClaimAddressStreetRegion(addressRequest.getRegion());
                    claimsNewEntity.setClaimAddressStreetState(addressRequest.getState());
                    claimsNewEntity.setClaimAddressFormatted(addressRequest.getFormattedAddress());
                }
            }
            //FROM COMPANY
            if(complaint.getFromCompany()!=null){
                ClaimsFromCompanyEntity claimsFromCompanyEntity = FromCompanyAdapter.adptFromCompanyEntityToFromClaimsCompanyNewEntity(complaint.getFromCompany());
                claimsNewEntity.setClaimsFromCompanyEntity(claimsFromCompanyEntity);
            }

            //ENTRUSTED
            if(complaint.getEntrusted() != null){
                Entrusted entrusted = complaint.getEntrusted();
                ClaimsEntrustedEntity claimsEntrustedEntity = EntrustedAdapter.adptEntrustedEntityToEntrustedNewEntity(entrusted);
                claimsNewEntity.setClaimsEntrustedEntity(claimsEntrustedEntity);
            }
        }


        //DAMAGED
        if(claimsEntity.getDamaged() != null){
            Damaged damaged = claimsEntity.getDamaged();


            claimsNewEntity.setCaiSigned(damaged.getCaiSigned());
            claimsNewEntity.setImpactPoint(damaged.getImpactPoint());
            claimsNewEntity.setAdditionalCosts(damaged.getAdditionalCosts());
            if(damaged.getContract()!=null){
                ClaimsDamagedContractEntity claimsDamagedContractEntity = ContractAdapter.adptFromContractEntityToClaimsDamagedContractNewEntity(damaged.getContract());
                claimsNewEntity.setClaimsDamagedContractEntity(claimsDamagedContractEntity);
            }
            if(damaged.getCustomer()!=null){
                ClaimsDamagedCustomerEntity claimsDamagedCustomerEntity = CustomerAdapter.adptCustomerRequestToClaimsDamagedCustomerEntity(damaged.getCustomer());
                claimsNewEntity.setClaimsDamagedCustomerEntity(claimsDamagedCustomerEntity);
                if(claimsNewEntity.getClaimsDamagedCustomerEntity().getCustomerId() != null) {
                    claimsNewEntity.setClientId(claimsNewEntity.getClaimsDamagedCustomerEntity().getCustomerId().toString());
                }
            }
            if(damaged.getDriver()!=null){
                ClaimsDamagedDriverEntity claimsDamagedDriverEntity = DriverAdapter.adptDriverEntityToClaimsDamagedDriverEntity(damaged.getDriver());
                claimsNewEntity.setClaimsDamagedDriverEntity(claimsDamagedDriverEntity);

            }
            if(damaged.getVehicle()!=null){
                ClaimsDamagedVehicleEntity claimsDamagedVehicleEntity = VehicleAdapter.adptVehicleEntityToClDamNewVehicle(damaged.getVehicle());
                claimsNewEntity.setClaimsDamagedVehicleEntity(claimsDamagedVehicleEntity);
                if(claimsNewEntity.getClaimsDamagedVehicleEntity() != null && claimsNewEntity.getPlate()!=null) {
                    claimsNewEntity.setPlate(claimsNewEntity.getClaimsDamagedVehicleEntity().getLicensePlate());
                }
            }
            if(damaged.getInsuranceCompany()!=null){
                ClaimsDamagedInsuranceInfoEntity claimsDamagedInsuranceInfoEntity = InsuranceCompanyAdapter.adptInsuranceCompanyEntityToClDamInsuranceNewCompany(damaged.getInsuranceCompany());
                claimsNewEntity.setClaimsDamagedInsuranceInfoEntity(claimsDamagedInsuranceInfoEntity);
            }
            if(damaged.getFleetManagerList()!=null && !damaged.getFleetManagerList().isEmpty()){
                List<ClaimsDamagedFleetManagerEntity> claimsDamagedFleetManagerEntityList = FleetManagerAdpter.adptFromFMListJsonbToClaimsDamagedFMNew(damaged.getFleetManagerList());
                claimsNewEntity.setClaimsDamagedFleetManagerEntityList(claimsDamagedFleetManagerEntityList);
            }

            if(damaged.getAntiTheftService()!=null && damaged.getAntiTheftService().getId() != null){
                // Optional<AntiTheftServiceEntity> optionalAntiTheftServiceEntity = antiTheftServiceRepository.findById(damaged.getAntiTheftService().getId());
                AntiTheftServiceEntity antiTheftServiceEntity = new AntiTheftServiceEntity();
                antiTheftServiceEntity.setId(damaged.getAntiTheftService().getId());
                claimsNewEntity.setAntiTheftServiceEntity(antiTheftServiceEntity);
            }

            if(damaged.getAntiTheftService()!=null && damaged.getAntiTheftService().getRegistryList()!=null && !damaged.getAntiTheftService().getRegistryList().isEmpty()){
                List<ClaimsDamagedRegistryEntity> claimsDamagedRegistryEntityList = RegistryAdapter.adptFromRegistryListJsonbToClaimsDamagedRegistryList(damaged.getAntiTheftService().getRegistryList());
                claimsNewEntity.setClaimsDamagedRegistryEntityList(claimsDamagedRegistryEntityList);

                if(damaged.getAntiTheftService().getId() == null){
                    AntiTheftServiceEntity   antiTheftService  = antiTheftServiceRepository.findAntiTheftByCodAntiTheftService(damaged.getAntiTheftService().getRegistryList().get(0).getCodPack());
                        claimsNewEntity.setAntiTheftServiceEntity(antiTheftService);

                }
            }


        }


        if(claimsEntity.getRefund() != null){
            ClaimsRefundEntity claimsRefundEntity = RefundAdapter.adptFromRefundJsonbToClaimsRefundEntity(claimsEntity.getRefund());
            claimsNewEntity.setClaimsRefundEntity(claimsRefundEntity);
        }
        if(claimsEntity.getTheft() != null){
            ClaimsTheftEntity claimsTheftEntity = TheftClaimsAdapter.adptTheftJsonbToClaimsTheftEntity(claimsEntity.getTheft());
            claimsNewEntity.setClaimsTheftEntity(claimsTheftEntity);
        }


        if(claimsEntity.getAuthorities() != null && !claimsEntity.getAuthorities().isEmpty()){
            List<ClaimsAuthorityEmbeddedEntity> claimsAuthorityEmbeddedEntities = authorityAdapter.adtpFromAuthorityListJsonbToClaimsAuthorityListEntity(claimsNewEntity,claimsEntity.getAuthorities());
            claimsNewEntity.setClaimsAuthorityEmbeddedEntities(claimsAuthorityEmbeddedEntities);
        }else{
            claimsNewEntity.setClaimsAuthorityEmbeddedEntities(null);
        }



        return claimsNewEntity;


    }


    @Override
    public List<ClaimsEntity> wrapFromClaimsNewEntityListToClaimsEntityList(List<ClaimsNewEntity> claimsNewEntityList){
        List<ClaimsEntity> claimsEntityList = new LinkedList<>();
        for(ClaimsNewEntity currentEntity: claimsNewEntityList){
            claimsEntityList.add(this.wrapFromClaimsNewEntityToClaimsEntity(currentEntity));

        }
        return claimsEntityList;
    }



    @Override
    public ClaimsEntity wrapFromClaimsNewEntityToClaimsEntity(ClaimsNewEntity claimsNewEntity) {

        if(claimsNewEntity == null){
            return null;
        }
        LOGGER.debug("convertMethod claims practiceId " + claimsNewEntity.getPracticeId());
        ClaimsEntity claimsEntity = new ClaimsEntity();
        claimsEntity.setId(claimsNewEntity.getId());
        claimsEntity.setCreatedAt(claimsNewEntity.getCreatedAt());
        claimsEntity.setUpdateAt(claimsNewEntity.getUpdateAt());
        claimsEntity.setType(claimsNewEntity.getType());
        claimsEntity.setPracticeId(claimsNewEntity.getPracticeId());
        claimsEntity.setStatus(claimsNewEntity.getStatus());
        claimsEntity.setInEvidence(claimsNewEntity.getInEvidence());
        claimsEntity.setPoVariation(claimsNewEntity.getPoVariation());



        claimsEntity.setPracticeManager(claimsNewEntity.getPracticeManager());
        claimsEntity.setPaiComunication(claimsNewEntity.getPaiComunication());

        claimsEntity.setUserId(claimsNewEntity.getUserId());
        claimsEntity.setMotivation(claimsNewEntity.getMotivation());

        claimsEntity.setForced(claimsNewEntity.getForced());
        claimsEntity.setCompleteDocumentation(claimsNewEntity.getCompleteDocumentation());
        claimsEntity.setCaiDetails(claimsNewEntity.getCaiDetails());
        claimsEntity.setWithCounterparty(claimsNewEntity.getWithCounterparty());
        claimsEntity.setCounterparts(CounterpartyAdapter.adptCounterpartyNewToCounterpartyOldList(claimsNewEntity.getCounterparts()));
        claimsEntity.setDeponentList(claimsNewEntity.getDeponentList());
        claimsEntity.setWoundedList(claimsNewEntity.getWoundedList());
        claimsEntity.setNotes(NotesAdapter.adptNoteEntityListToNotesList(claimsNewEntity.getNotes()));

        claimsEntity.setHistorical(claimsNewEntity.getHistorical());

        claimsEntity.setExemption(claimsNewEntity.getExemption());
        claimsEntity.setIdSaleforce(claimsNewEntity.getIdSaleforce());
        claimsEntity.setWithContinuation(claimsNewEntity.getWithContinuation());
        claimsEntity.setRead(claimsNewEntity.getRead());
        claimsEntity.setAuthorityLinkable(claimsNewEntity.getAuthorityLinkable());
        claimsEntity.setLegalComunication(claimsNewEntity.getLegalComunication());
        claimsEntity.setTotalPenalty(claimsNewEntity.getTotalPenalty());
        claimsEntity.setReadAcclaims(claimsNewEntity.getReadAcclaims());
        claimsEntity.setMigrated(claimsNewEntity.getMigrated());

        /* gestione degl iallegato doppioni */
        //claimsEntity.setForms(claimsNewEntity.getForms());
        claimsEntity.setForms(MigrationUtil.getDistinctAttachmentList(claimsNewEntity.getForms()));

        claimsEntity.setAntiTheftRequestEntities(AntiTheftRequestAdapter.adptAntiTheftRequestNewToAntiTheftRequestOldList(claimsNewEntity.getAntiTheftRequestEntities()));


        //COMPLAINT
        Complaint complaint = new Complaint();
        DataAccident dataAccident = new DataAccident();

        complaint.setClientId(claimsNewEntity.getClientId());
        complaint.setActivation(claimsNewEntity.getActivation());
        complaint.setMod(claimsNewEntity.getMod());
        complaint.setLocator(claimsNewEntity.getLocator());
        complaint.setQuote(claimsNewEntity.getQuote());
        complaint.setNotification(claimsNewEntity.getNotification());
        complaint.setProperty(claimsNewEntity.getProperty());
        complaint.setPlate(claimsNewEntity.getPlate());

        dataAccident.setTypeAccident(claimsNewEntity.getTypeAccident());
        dataAccident.setDateAccident(DateUtil.convertIS08601StringToUTCDate(DateUtil.convertUTCInstantToIS08601String(claimsNewEntity.getDateAccident())));
        dataAccident.setResponsible(claimsNewEntity.getResponsible());
        dataAccident.setHappenedAbroad(claimsNewEntity.getHappenedAbroad());
        dataAccident.setDamageToObjects(claimsNewEntity.getDamageToObjects());
        dataAccident.setDamageToVehicles(claimsNewEntity.getDamageToVehicles());
        dataAccident.setOldMotorcyclePlates(claimsNewEntity.getOldMotorcyclePlates());
        dataAccident.setInterventionAuthority(claimsNewEntity.getInterventionAuthority());
        dataAccident.setPolice(claimsNewEntity.getPolice());
        dataAccident.setCc(claimsNewEntity.getCc());
        dataAccident.setVvuu(claimsNewEntity.getVvuu());
        dataAccident.setAuthorityData(claimsNewEntity.getAuthorityData());
        dataAccident.setWitnessDescription(claimsNewEntity.getWitnessDescription());
        dataAccident.setRecoverability(claimsNewEntity.getRecoverability());
        dataAccident.setRecoverabilityPercent(claimsNewEntity.getRecoverabilityPercent());
        dataAccident.setIncompleteMotivation(claimsNewEntity.getIncompleteMotivation());
        dataAccident.setRobbery(claimsNewEntity.getRobbery());
        dataAccident.setCenterNotified(claimsNewEntity.getCenterNotified());
        dataAccident.setHappenedOnCenter(claimsNewEntity.getHappenedOnCenter());
        dataAccident.setProviderCode(claimsNewEntity.getProviderCode());
        dataAccident.setTheftDescription(claimsNewEntity.getTheftDescription());
        dataAccident.setWithCounterparty(claimsNewEntity.getWithCounterpartyInternal());

        if(claimsNewEntity.getClaimAddressStreet()!=null || claimsNewEntity.getClaimAddressStreetZip()!=null || claimsNewEntity.getClaimAddressStreetNr()!=null
        || claimsNewEntity.getClaimAddressStreetLocality()!=null || claimsNewEntity.getClaimAddressStreetProvince()!=null || claimsNewEntity.getClaimAddressStreetRegion()!=null
        || claimsNewEntity.getClaimAddressStreetState()!=null || claimsNewEntity.getClaimAddressFormatted()!=null){
            Address addressDataAccident = new Address();
            addressDataAccident.setStreet(claimsNewEntity.getClaimAddressStreet());
            addressDataAccident.setStreetNr(claimsNewEntity.getClaimAddressStreetNr());
            addressDataAccident.setZip(claimsNewEntity.getClaimAddressStreetZip());
            addressDataAccident.setLocality(claimsNewEntity.getClaimAddressStreetLocality());
            addressDataAccident.setProvince(claimsNewEntity.getClaimAddressStreetProvince());
            addressDataAccident.setRegion(claimsNewEntity.getClaimAddressStreetRegion());
            addressDataAccident.setState(claimsNewEntity.getClaimAddressStreetState());
            addressDataAccident.setFormattedAddress(claimsNewEntity.getClaimAddressFormatted());
            dataAccident.setAddress(addressDataAccident);
        }


        complaint.setDataAccident(dataAccident);

        //FROM COMPANY
        if(claimsNewEntity.getClaimsFromCompanyEntity()!=null){
            FromCompany fromCompany  = FromCompanyAdapter.adptFromClaimsFromCompanyToFromClaimsJsonb(claimsNewEntity.getClaimsFromCompanyEntity());
            complaint.setFromCompany(fromCompany);
        }

        //ENTRUSTED
        if(claimsNewEntity.getClaimsEntrustedEntity()!= null){
            Entrusted entrusted  = EntrustedAdapter.adptFromClaimsEntrustedToEntrustedJsonb(claimsNewEntity.getClaimsEntrustedEntity());
            complaint.setEntrusted(entrusted);
        }


        claimsEntity.setComplaint(complaint);


        //DAMAGED
        Damaged damaged = new Damaged();

        damaged.setCaiSigned(claimsNewEntity.getCaiSigned());
        damaged.setImpactPoint(claimsNewEntity.getImpactPoint());
        damaged.setAdditionalCosts(claimsNewEntity.getAdditionalCosts());

        if(claimsNewEntity.getClaimsDamagedContractEntity()!=null){
            Contract contract= ContractAdapter.adptFromContractEntityToClaimsDamagedContractNewEntity(claimsNewEntity.getClaimsDamagedContractEntity());
            damaged.setContract(contract);
        }
        if(claimsNewEntity.getClaimsDamagedCustomerEntity()!=null){
            Customer customer = CustomerAdapter.adptClaimsCustomerToCustomerJsonb(claimsNewEntity.getClaimsDamagedCustomerEntity());
            damaged.setCustomer(customer);

        }
        if(claimsNewEntity.getClaimsDamagedDriverEntity()!=null){
            Driver driver = DriverAdapter.adptfromClaimsDriverEntityToDriverJsonb(claimsNewEntity.getClaimsDamagedDriverEntity());
            damaged.setDriver(driver);

        }
        if(claimsNewEntity.getClaimsDamagedVehicleEntity()!=null){
            Vehicle vehicle = VehicleAdapter.adptFromClaimsVehicleToVehicleJsonb(claimsNewEntity.getClaimsDamagedVehicleEntity());
            if( vehicle != null && claimsNewEntity.getClaimsDamagedVehicleEntity() != null) {
                vehicle.setLicensePlate(claimsNewEntity.getClaimsDamagedVehicleEntity().getLicensePlate());
            }
            damaged.setVehicle(vehicle);
        }
        if(claimsNewEntity.getClaimsDamagedInsuranceInfoEntity()!=null){
            InsuranceCompany insuranceCompany = InsuranceCompanyAdapter.adptClaimsInsuranceCompanyEntityToInsuranceCompanyJsonb(claimsNewEntity.getClaimsDamagedInsuranceInfoEntity());
            damaged.setInsuranceCompany(insuranceCompany);
        }
        if(claimsNewEntity.getClaimsDamagedFleetManagerEntityList()!=null && !claimsNewEntity.getClaimsDamagedFleetManagerEntityList().isEmpty()){
            List<FleetManager> fleetManagerList = FleetManagerAdpter.adptFromClaimsFMListToFMListJsonb(claimsNewEntity.getClaimsDamagedFleetManagerEntityList());
            damaged.setFleetManagerList(fleetManagerList);
        }


        //settare AntitheftService

        if(claimsNewEntity.getAntiTheftServiceEntity() != null || claimsNewEntity.getClaimsDamagedRegistryEntityList()!=null){
            AntiTheftService antiTheftService = AntiTheftServiceAdapter.adptAntiTheftServiceEntityToAntiTheftService(claimsNewEntity.getAntiTheftServiceEntity());
            if(claimsNewEntity.getClaimsDamagedRegistryEntityList() != null && !claimsNewEntity.getClaimsDamagedRegistryEntityList().isEmpty()) {
                antiTheftService.setRegistryList(RegistryAdapter.adptFromClaimsRegistryListToRegistryListJsonb(claimsNewEntity.getClaimsDamagedRegistryEntityList()));
            }
            damaged.setAntiTheftService(antiTheftService);
        }

        claimsEntity.setDamaged(damaged);

        if(claimsNewEntity.getClaimsRefundEntity() != null){
            Refund refund = RefundAdapter.adptClaimsFromRefundToRefundJsonb(claimsNewEntity.getClaimsRefundEntity() );
            claimsEntity.setRefund(refund);
        }



        if(claimsNewEntity.getClaimsTheftEntity() != null){
            Theft theft = TheftClaimsAdapter.adptClaimsTheftToTheftJsonb(claimsNewEntity.getClaimsTheftEntity());
            claimsEntity.setTheft(theft);
        }


        if(claimsNewEntity.getClaimsAuthorityEmbeddedEntities() != null && !claimsNewEntity.getClaimsAuthorityEmbeddedEntities().isEmpty()){
            List<Authority> authorityList = authorityAdapter.adtpClaimsFromAuthorityListToAuthorityListJsonb(claimsNewEntity.getClaimsAuthorityEmbeddedEntities());
            claimsEntity.setAuthorities(authorityList);
        }

        if(claimsNewEntity.getClaimsMetadataEntity() != null){
            claimsEntity.setMetadata(MetadataAdapter.adptFromMetadataEntityToMetadataJsonb(claimsNewEntity.getClaimsMetadataEntity()));
        }


        return claimsEntity;


    }

    @Override
    public List<ClaimsEntity> wrapFromClaimsComplaintOperatorNewEntityListToClaimsEntityList(List<ClaimsNewEntity> claimsNewEntityList){
        List<ClaimsEntity> claimsEntityList = new LinkedList<>();
        for(ClaimsNewEntity currentEntity: claimsNewEntityList){
            claimsEntityList.add(this.wrapFromClaimsComplaintOperatorNewEntityToClaimsEntity(currentEntity));

        }
        return claimsEntityList;
    }

    @Override
    public ClaimsEntity wrapFromClaimsComplaintOperatorNewEntityToClaimsEntity(ClaimsNewEntity claimsNewEntity) {

        if(claimsNewEntity == null){
            return null;
        }
        LOGGER.debug("convertMethod claims practiceId " + claimsNewEntity.getPracticeId());
        ClaimsEntity claimsEntity = new ClaimsEntity();
        claimsEntity.setId(claimsNewEntity.getId());
        claimsEntity.setCreatedAt(claimsNewEntity.getCreatedAt());
        claimsEntity.setUpdateAt(claimsNewEntity.getUpdateAt());
        claimsEntity.setType(claimsNewEntity.getType());
        claimsEntity.setPracticeId(claimsNewEntity.getPracticeId());
        claimsEntity.setStatus(claimsNewEntity.getStatus());
        claimsEntity.setInEvidence(claimsNewEntity.getInEvidence());
        claimsEntity.setPoVariation(claimsNewEntity.getPoVariation());

        claimsEntity.setAntiTheftRequestEntities(AntiTheftRequestAdapter.adptAntiTheftRequestNewToAntiTheftRequestOldList(claimsNewEntity.getAntiTheftRequestEntities()));

        //COMPLAINT
        Complaint complaint = new Complaint();
        DataAccident dataAccident = new DataAccident();
        complaint.setPlate(claimsNewEntity.getPlate());


        dataAccident.setTypeAccident(claimsNewEntity.getTypeAccident());
        dataAccident.setDateAccident(DateUtil.convertIS08601StringToUTCDate(DateUtil.convertUTCInstantToIS08601String(claimsNewEntity.getDateAccident())));
        complaint.setDataAccident(dataAccident);

        //ENTRUSTED
        if(claimsNewEntity.getClaimsEntrustedEntity()!= null){
            Entrusted entrusted  = EntrustedAdapter.adptFromClaimsEntrustedToEntrustedJsonb(claimsNewEntity.getClaimsEntrustedEntity());
            complaint.setEntrusted(entrusted);
        }

        claimsEntity.setComplaint(complaint);


        //DAMAGED
        Damaged damaged = new Damaged();

        if(claimsNewEntity.getClaimsDamagedCustomerEntity()!=null){
            Customer customer = CustomerAdapter.adptClaimsCustomerToCustomerJsonb(claimsNewEntity.getClaimsDamagedCustomerEntity());
            damaged.setCustomer(customer);
        }

        //settare AntitheftService

        if(claimsNewEntity.getAntiTheftServiceEntity() != null || claimsNewEntity.getClaimsDamagedRegistryEntityList()!=null){
            AntiTheftService antiTheftService = AntiTheftServiceAdapter.adptAntiTheftServiceEntityToAntiTheftService(claimsNewEntity.getAntiTheftServiceEntity());
            if(claimsNewEntity.getClaimsDamagedRegistryEntityList() != null && !claimsNewEntity.getClaimsDamagedRegistryEntityList().isEmpty()) {
                antiTheftService.setRegistryList(RegistryAdapter.adptFromClaimsRegistryListToRegistryListJsonb(claimsNewEntity.getClaimsDamagedRegistryEntityList()));
            }
            damaged.setAntiTheftService(antiTheftService);
        }

        claimsEntity.setDamaged(damaged);

        claimsEntity.setPending(claimsService.isPending(claimsNewEntity.getPracticeId()));

        return claimsEntity;

    }
}
