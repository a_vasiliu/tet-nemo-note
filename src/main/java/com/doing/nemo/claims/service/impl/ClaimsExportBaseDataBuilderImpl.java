package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.dto.ClaimsExportBaseData;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.service.ClaimsExportBaseDataBuilder;
import org.springframework.stereotype.Service;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

@Service
public class ClaimsExportBaseDataBuilderImpl implements ClaimsExportBaseDataBuilder {

    // data
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

    public ClaimsExportBaseData build(ClaimsEntity claimsEntity) {
        simpleDateFormat.setTimeZone( TimeZone.getTimeZone("Europe/Rome") );

        ClaimsExportBaseData claimsExportBaseData = new ClaimsExportBaseData();
        claimsExportBaseData.setIdPractice( claimsEntity.getPracticeId() );

        // targa
        if( claimsEntity.getDamaged() != null && claimsEntity.getDamaged().getVehicle() != null ) {
            claimsExportBaseData.setDamagedPlate( claimsEntity.getDamaged().getVehicle().getLicensePlate() );
        }
        else {
            claimsExportBaseData.setDamagedPlate("");
        }

        // passeggeri
        if( claimsEntity.getCaiDetails() != null && claimsEntity.getCaiDetails().getDriverSide() != null ) {
            claimsExportBaseData.setDriverSide( claimsEntity.getCaiDetails().getDriverSide() );
        }
        else {
            claimsExportBaseData.setDriverSide("");
        }

        // data sinistro
        if( claimsEntity.getComplaint() != null && claimsEntity.getComplaint().getDataAccident() != null ) {
            claimsExportBaseData.setDateAccident( simpleDateFormat.format(claimsEntity.getComplaint().getDataAccident().getDateAccident()) );
        }
        else {
            claimsExportBaseData.setDateAccident("");
        }

        return claimsExportBaseData;
    }

}
