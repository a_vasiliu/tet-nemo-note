package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.controller.payload.response.SupplierResponseV1;
import com.doing.nemo.claims.entity.SupplierEntity;
import com.doing.nemo.claims.repository.SupplierRepository;
import com.doing.nemo.claims.repository.SupplierRepositoryV1;
import com.doing.nemo.claims.service.SupplierService;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class SupplierServiceImpl implements SupplierService {

    private static Logger LOGGER = LoggerFactory.getLogger(SupplierServiceImpl.class);
    @Autowired
    private SupplierRepository supplierRepository;
    @Autowired
    private SupplierRepositoryV1 supplierRepositoryV1;

    @Override
    public SupplierEntity insertSupplier(SupplierEntity supplierEntity) {

        if (supplierRepository.searchSupplierbyCode(supplierEntity.getCodSupplier()) != null) {
            LOGGER.debug("Error in Suppliers, duplicate code not admitted");
            throw new BadRequestException("Error in Suppliers, duplicate code not admitted", MessageCode.CLAIMS_1009);
        }
        supplierRepository.save(supplierEntity);

        return supplierEntity;
    }

    @Override
    public SupplierEntity updateSupplier(SupplierEntity supplierEntity) {
        if (supplierRepository.searchSupplierWithDifferentId(supplierEntity.getId(), supplierEntity.getCodSupplier()) != null) {
            LOGGER.debug("Error in Suppliers, duplicate code not admitted");
            throw new BadRequestException("Error in Suppliers, duplicate code not admitted", MessageCode.CLAIMS_1009);
        }
        supplierRepository.save(supplierEntity);
        return supplierEntity;
    }

    @Override
    public SupplierEntity selectSupplier(UUID id) {
        Optional<SupplierEntity> supplierEntity = supplierRepository.findById(id);
        if (!supplierEntity.isPresent()) {
            LOGGER.debug("Supplier with id " + id + " not found");
            throw new NotFoundException("Supplier with id " + id + " not found", MessageCode.CLAIMS_1010);
        }

        return supplierEntity.get();
    }

    @Override
    public List<SupplierEntity> selectAllSupplier() {
        List<SupplierEntity> resultListSupplier = supplierRepository.findAll();

        return resultListSupplier;
    }

    @Override
    public SupplierResponseV1 deleteSupplier(UUID id) {
        SupplierEntity supplierEntity = selectSupplier(id);
        //if(supplierRepository.searchInsurancePolicyForeignKey(id) != null)
        //  throw new NotFoundException("Foreign key in InsurancePolicy. Supplier impossible to delete");
        supplierRepository.delete(supplierEntity);
        return null;
    }

    @Override
    public SupplierEntity updateStatus(UUID uuid) {
        SupplierEntity supplierEntity = selectSupplier(uuid);
        if (supplierEntity.getActive()) {
            supplierEntity.setActive(false);
        } else {
            supplierEntity.setActive(true);
        }

        supplierRepository.save(supplierEntity);
        return supplierEntity;
    }

    @Override
    public List<SupplierEntity> findSuppliersFiltered(Integer page, Integer pageSize, String orderBy, Boolean asc, String name, String email, Boolean isActive, String codSupplier) {
        return supplierRepositoryV1.findSuppliersFiltered(page, pageSize, orderBy, asc, name, email, isActive, codSupplier);
    }

    @Override
    public Long countSupplierFiltered(String name, String email, Boolean isActive, String codSupplier) {
        return supplierRepositoryV1.countSupplierFiltered(name, email, isActive, codSupplier);
    }


}