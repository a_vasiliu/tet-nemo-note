package com.doing.nemo.claims.service;

import com.doing.nemo.claims.controller.payload.response.ClaimsCheckerResponseV1;
import com.doing.nemo.claims.controller.payload.response.FlowTypeResponseV1;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public interface ClaimsCheckerService {
   ClaimsCheckerResponseV1 checkClaimsDuplicate(String plate, String dataAccident, String typeAccident, String idClaims);
   ClaimsCheckerResponseV1 checkIfExistsAnotherCounterpartyWithEqualsPlateName(String driverPlate, String dateAccident, String counterpartyName, String counterpartySurnameName, String counterpartyPlate, String idClaims);
   ClaimsCheckerResponseV1 checkClaimsInValidation(String idClaims);
   FlowTypeResponseV1 checkFlowtype(String contractType, String customerId);
}
