package com.doing.nemo.claims.service;

import com.doing.nemo.claims.controller.payload.response.LegalResponseV1;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.enumerated.DamagedEnum.VehicleEnum.VehicleTypeEnum;
import com.doing.nemo.claims.entity.settings.AutomaticAffiliationRuleLegalEntity;
import com.doing.nemo.claims.entity.settings.LegalEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public interface LegalService {
    LegalEntity insertLegal(LegalEntity legalEntity);

    LegalEntity updateLegalWithoutRule(LegalEntity legalEntity);

    LegalResponseV1 deleteLegal(UUID uuid);

    LegalEntity selectLegal(UUID uuid);

    List<LegalEntity> selectAllLegal();

    LegalEntity insertLegalRule(List<AutomaticAffiliationRuleLegalEntity> automaticAffiliationRuleLegalEntityList, UUID uuid);

    LegalEntity updateLegalRule(List<AutomaticAffiliationRuleLegalEntity> automaticAffiliationRuleLegalEntityList, LegalEntity legalEntity);

    LegalEntity updateLegal(LegalEntity legalEntity, UUID uuid);

    LegalEntity updateStatusLegal(UUID uuid);
    LegalEntity selectLegalWithMaxVolume(Boolean isNotCompanyCounterParty, Boolean isAbroad, Boolean isWounded, Boolean isRecoverability, UUID counterparty, DataAccidentTypeAccidentEnum typeClaim, VehicleTypeEnum counterpartType, UUID damaged, Double amount, Long practiceId);
    Pagination<LegalEntity> paginationLegal(int page, int pageSize, String orderBy, Boolean asc, String name, String email, String province, String telephone, String fax, Boolean isActive, String address, String zipCode, String city, String state, Long code);
    void resetRuleJob();
}
