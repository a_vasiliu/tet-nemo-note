package com.doing.nemo.claims.service;

import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.settings.ExportSupplierCodeEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public interface ExportSupplierCodeService {
    ExportSupplierCodeEntity saveExportSupplierCode(ExportSupplierCodeEntity exportSupplierCodeEntity);
    ExportSupplierCodeEntity updateExportSupplierCode(ExportSupplierCodeEntity exportSupplierCodeEntity);
    void deleteExportSupplierCode(UUID uuid);
    ExportSupplierCodeEntity getExportSupplierCode(UUID uuid);
    List<ExportSupplierCodeEntity> getAllExportSupplierCode();
    ExportSupplierCodeEntity patchActive(UUID uuid);
    Pagination<ExportSupplierCodeEntity> findExportSupplierCode(Integer page, Integer pageSize, String orderBy, Boolean asc, String vatNumber, String denomination, String codeToExport, Boolean isActive);

}
