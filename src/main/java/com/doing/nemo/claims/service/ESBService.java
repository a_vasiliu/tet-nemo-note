package com.doing.nemo.claims.service;

import com.doing.nemo.claims.controller.payload.response.ContractInfoPracticeResponseV1;
import com.doing.nemo.claims.controller.payload.response.ContractInfoResponseAldVSAldV1;
import com.doing.nemo.claims.controller.payload.response.ContractInfoResponseV1;
import com.doing.nemo.claims.controller.payload.response.VehicleRegistrationResponse;
import com.doing.nemo.claims.entity.esb.ContractESB.ContractEsb;
import com.doing.nemo.claims.entity.esb.CustomerESB.CustomerEsb;
import com.doing.nemo.claims.entity.esb.FleetManagerESB.FleetManagerEsb;
import com.doing.nemo.claims.entity.esb.InsuranceCompanyESB.InsuranceCompanyEsb;
import com.doing.nemo.claims.entity.esb.RegistryESB;
import com.doing.nemo.claims.entity.esb.VehicleESB.VehicleEsb;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.middleware.client.payload.response.MiddlewareClaimInfoResponse;
import com.doing.nemo.middleware.client.payload.response.MiddlewareVehicleDetailResponse;

import java.io.IOException;
import java.text.ParseException;
import java.time.Instant;
import java.util.List;
import java.util.concurrent.BlockingQueue;

public interface ESBService {

    ContractInfoResponseV1 getContractByContractIdOrPlate(String stringOrPlate, String date, String profile) throws IOException;

    CustomerEsb getCustomer(String customerId) throws IOException;

    VehicleEsb getVehicle(String plate) throws IOException;

    ContractEsb getContractDetail(String contract, String date) throws IOException;

    BlockingQueue asynchronousEsbcalls(String idCustomer, String idDriver, String plate, boolean isPlate, String idContract, String date, BlockingQueue responseList, Boolean isAld);

    ContractInfoResponseV1 getDriverContractCustomerCustomerInfo(String contractOrPlate, String date,  String endDateCheck, String profile) throws IOException;


    ContractInfoResponseAldVSAldV1 getContractByContractIdOrPlateAldVSAld(String contractOrPlate, String date) throws IOException;

    FleetManagerEsb checkFleetManager(String customerId, String fleetManagerId) throws IOException;

    /*@Retryable(
            maxAttempts = 3,
            backoff = @Backoff(delay = 1))
    @Async
    void callToOctoAsync(ClaimsEntity claimsEntity, ClaimsStatusEnum previousStatus, AntiTheftRequest antiTheftRequest, String user, String userName) ;

    @Recover
    void recoverCallToOcto(ClaimsEntity claimsEntity, ClaimsStatusEnum previousStatus, AntiTheftRequest antiTheftRequest);*/

    List<RegistryESB> getTelematics(String idContract) throws IOException;

    List<FleetManagerEsb> getFleetManagers(String idCustomer) throws IOException;

    InsuranceCompanyEsb getInsuranceCompany(String idcontract, String date) throws IOException;

    ContractInfoPracticeResponseV1 getDriverContractCustomerCustomerInfoPractice(String contractOrPlate, String date) throws IOException;
    ContractInfoPracticeResponseV1 getContractByContractIdOrPlatePractice(String contractOrPlate, String date) throws IOException;
    BlockingQueue asynchronousEsbcallsPractice(String idCustomer, String idDriver, String plate, boolean isPlate, String idContract, String date, BlockingQueue responseList);

    ContractInfoResponseV1 getContractInfoEnjoy (String plate, String date) throws ParseException;
    Boolean getContractByContractIdOrPlateGoLiveMSA(String contractOrPlate, String date);

    VehicleEsb getPlateFromVehicleRegistrations(String plate, String date) throws IOException;

    VehicleRegistrationResponse getVehicleRegistrationsByPlate(String plate) throws IOException;

    MiddlewareClaimInfoResponse getPlateFromEnjoyVehicleRegistrations(String plate, String queryDate, Instant instantRif) throws IOException, NotFoundException;
}

