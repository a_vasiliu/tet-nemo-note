package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.adapter.ClaimsCheckerAdapter;
import com.doing.nemo.claims.adapter.DateAdapter;
import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.response.ClaimsCheckerResponseV1;
import com.doing.nemo.claims.controller.payload.response.FlowTypeResponseV1;
import com.doing.nemo.claims.controller.payload.response.claims.ClaimsFlowType;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.ClaimsNewEntity;
import com.doing.nemo.claims.entity.ClaimsPendingEntity;
import com.doing.nemo.claims.entity.CounterpartyEntity;
import com.doing.nemo.claims.entity.enumerated.DamagedEnum.VehicleEnum.VehicleTypeEnum;
import com.doing.nemo.claims.repository.*;
import com.doing.nemo.claims.service.ClaimsCheckerService;
import com.doing.nemo.claims.service.ConverterClaimsService;
import com.doing.nemo.claims.service.GoLiveStrategyService;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import io.netty.util.internal.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class ClaimsCheckerServiceImpl implements ClaimsCheckerService {

    private static Logger LOGGER = LoggerFactory.getLogger(ClaimsCheckerServiceImpl.class);

    @Autowired
    private ClaimsNewRepository claimsNewRepository;

    @Autowired
    private ConverterClaimsService converterClaimsService;
    private ClaimsRepository claimsRepository;
    @Autowired
    private GoLiveStrategyService goLiveStrategyService;
    @Autowired
    private ClaimsPendingRepository claimsPendingRepository;
    @Autowired
    private ClaimsTypeRepository claimsTypeRepository;


    @Value("${time.zone}")
    private String timeZoneApplication;

    public Long checkHoursDifference (Date dateUTC) throws ParseException {

        String DATE_FORMAT = "dd-M-yyyy hh:mm:ss a";

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-M-yyyy hh:mm:ss a");
        String dateInString = simpleDateFormat.format(dateUTC);
        LocalDateTime ldt = LocalDateTime.parse(dateInString, DateTimeFormatter.ofPattern(DATE_FORMAT));

        ZoneId romeId = ZoneId.of(timeZoneApplication);
        ZoneId utcId = ZoneId.of("UTC");

        //LocalDateTime + ZoneId = ZonedDateTime
        ZonedDateTime utcTime = ldt.atZone(utcId);
        ZonedDateTime romeTime = ldt.atZone(romeId);
        LOGGER.debug("Date ("+ timeZoneApplication + ") : " + romeTime);
        LOGGER.debug("Date (UTC) : " + utcTime);
        Long diff = ChronoUnit.HOURS.between(romeTime,utcTime);
        return diff;

    }



    //REFACTOR
    @Override
    public ClaimsCheckerResponseV1 checkClaimsDuplicate(String plate, String dateAccident, String typeAccident, String idClaims) {

        Integer hourDayMinus = 22;
        Integer hourDayPlus = 21;

        ClaimsCheckerResponseV1 claimsCheckerResponseV1 = new ClaimsCheckerResponseV1();

        List<String> typeAccidentList = null;
        if (typeAccident != null) {
            typeAccidentList = new ArrayList<String>() {{
                add(typeAccident);
            }};
        }

        Date dateUtc =DateUtil.convertIS08601StringToUTCDate(dateAccident);
        LocalDateTime dateTimeDateAccident = DateAdapter.adptDateToLocalDataTime(dateUtc);

        if(dateTimeDateAccident != null){
            try {
                Long differenceHour = checkHoursDifference(dateUtc);
                if(differenceHour > 1){
                    //2 ore di differenze
                    hourDayMinus = 22;
                    hourDayPlus = 21;
                }else{
                    hourDayMinus = 23;
                    hourDayPlus = 22;
                }


            } catch (ParseException e) {
                LOGGER.debug(e.getMessage(), e);
            }
        }




        if (dateTimeDateAccident.getHour() < hourDayMinus)
            dateTimeDateAccident = dateTimeDateAccident.minusDays(1);

        //CONTROLLO GIORNO STESSO
        LocalDateTime dateFrom = dateTimeDateAccident.withHour(hourDayMinus).withMinute(0).withSecond(0);
        LocalDateTime dateTo = dateTimeDateAccident.plusDays(1).withHour(hourDayPlus).withMinute(59).withSecond(59);

        if (typeAccidentList != null) {
            //REFACTOR
            //recupero nuova entità
            List<ClaimsNewEntity> claimsEntityNewListToday = claimsNewRepository.findClaimsForPaginationWithoutDraft(null, null, null, null, plate, null, null, DateUtil.convertUTCDateToIS08601String(DateAdapter.adptLocalDateTimeToDate(dateFrom)), DateUtil.convertUTCDateToIS08601String(DateAdapter.adptLocalDateTimeToDate(dateTo)), typeAccidentList, null, null, 1, 5, null, null, null, null, null, null);
            if (claimsEntityNewListToday != null && !claimsEntityNewListToday.isEmpty()) {
                List<ClaimsEntity> claimsEntityListToday =converterClaimsService.wrapFromClaimsNewEntityListToClaimsEntityList(claimsEntityNewListToday);

                if (!(checkSameClaim(claimsEntityListToday, idClaims))) {
                    claimsEntityListToday = this.deleteSameClaim(claimsEntityListToday, idClaims);
                    claimsCheckerResponseV1.setCode(MessageCode.CLAIMS_1078.name());
                    claimsCheckerResponseV1.setMessage(MessageCode.CLAIMS_1078.value());
                    claimsCheckerResponseV1.setClaims(ClaimsCheckerAdapter.adptClaimsEntityListToClaimsCheckerResponseList(claimsEntityListToday));
                    return claimsCheckerResponseV1;
                    //throw new BadRequestException(MessageCode.CLAIMS_1078);
                }
            }
        }

        //REFACTOR
        //recupero nuova entità
        List<ClaimsNewEntity> claimsEntityNewListToday =  claimsNewRepository.findClaimsForPaginationWithoutDraft(null, null, null, null, plate, null, null, DateUtil.convertUTCDateToIS08601String(DateAdapter.adptLocalDateTimeToDate(dateFrom)), DateUtil.convertUTCDateToIS08601String(DateAdapter.adptLocalDateTimeToDate(dateTo)), null, null, null, 1, 5, null, null, null, null, null, null);
        if (claimsEntityNewListToday != null && !claimsEntityNewListToday.isEmpty()) {
            //conversione nella vecchia entità
            List<ClaimsEntity> claimsEntityListToday =converterClaimsService.wrapFromClaimsNewEntityListToClaimsEntityList(claimsEntityNewListToday);
            if (!(checkSameClaim(claimsEntityListToday, idClaims))) {
                claimsEntityListToday = this.deleteSameClaim(claimsEntityListToday, idClaims);
                claimsCheckerResponseV1.setCode(MessageCode.CLAIMS_1058.name());
                claimsCheckerResponseV1.setMessage(MessageCode.CLAIMS_1058.value());
                claimsCheckerResponseV1.setClaims(ClaimsCheckerAdapter.adptClaimsEntityListToClaimsCheckerResponseList(claimsEntityListToday));
                return claimsCheckerResponseV1;
                //throw new BadRequestException(MessageCode.CLAIMS_1058);
            }
        }

        //Controllo Pending ad Oggi
        if(goLiveStrategyService.getGoLivePendingCheck()){
            List<ClaimsPendingEntity> claimsPendingEntityList= claimsPendingRepository.findByPlateAndBetweenDateAccident(plate, DateAdapter.adptLocalDateTimeToDate(dateFrom).toInstant(), DateAdapter.adptLocalDateTimeToDate(dateTo).toInstant());
            if(!CollectionUtils.isEmpty(claimsPendingEntityList)){
                claimsCheckerResponseV1.setCode(MessageCode.CLAIMS_1058.name());
                claimsCheckerResponseV1.setMessage(MessageCode.CLAIMS_1058.value());
                return claimsCheckerResponseV1;
            }
        }

        //CONTROLLO DATA DI IERI
        dateFrom = dateTimeDateAccident.minusDays(1).withHour(hourDayMinus).withMinute(0).withSecond(0);
        dateTo = dateTimeDateAccident.withHour(hourDayPlus).withMinute(59).withSecond(59);

        if (typeAccidentList != null) {
            //REFACTOR
            //recupero nuova entità
            List<ClaimsNewEntity> claimsNewEntityListYesterday = claimsNewRepository.findClaimsForPaginationWithoutDraft(null, null, null, null, plate, null, null, DateUtil.convertUTCDateToIS08601String(DateAdapter.adptLocalDateTimeToDate(dateFrom)), DateUtil.convertUTCDateToIS08601String(DateAdapter.adptLocalDateTimeToDate(dateTo)), typeAccidentList, null, null, 1, 5, null, null, null, null, null, null);
            if (claimsNewEntityListYesterday != null && !claimsNewEntityListYesterday.isEmpty()) {
                //conversione nella vecchia entità
                List<ClaimsEntity> claimsEntityListYesterday =converterClaimsService.wrapFromClaimsNewEntityListToClaimsEntityList(claimsNewEntityListYesterday);
                if (!checkSameClaim(claimsEntityListYesterday, idClaims)) {
                    claimsEntityListYesterday = this.deleteSameClaim(claimsEntityListYesterday, idClaims);
                    claimsCheckerResponseV1.setCode(MessageCode.CLAIMS_1079.name());
                    claimsCheckerResponseV1.setMessage(MessageCode.CLAIMS_1079.value());
                    claimsCheckerResponseV1.setClaims(ClaimsCheckerAdapter.adptClaimsEntityListToClaimsCheckerResponseList(claimsEntityListYesterday));
                    return claimsCheckerResponseV1;
                    //throw new BadRequestException(MessageCode.CLAIMS_1079);
                }
            }
        }

        //REFACTOR
        //recupero nuova entità
        List<ClaimsNewEntity> claimsNewEntityListYesterday = claimsNewRepository.findClaimsForPaginationWithoutDraft(null, null, null, null, plate, null, null, DateUtil.convertUTCDateToIS08601String(DateAdapter.adptLocalDateTimeToDate(dateFrom)), DateUtil.convertUTCDateToIS08601String(DateAdapter.adptLocalDateTimeToDate(dateTo)), null, null, null, 1, 5, null, null, null, null, null, null);
        if (claimsNewEntityListYesterday != null && !claimsNewEntityListYesterday.isEmpty()) {
            //conversione nella vecchia entità
            List<ClaimsEntity> claimsEntityListYesterday =converterClaimsService.wrapFromClaimsNewEntityListToClaimsEntityList(claimsNewEntityListYesterday);
            if (!checkSameClaim(claimsEntityListYesterday, idClaims)) {
                claimsEntityListYesterday = this.deleteSameClaim(claimsEntityListYesterday, idClaims);
                claimsCheckerResponseV1.setCode(MessageCode.CLAIMS_1073.name());
                claimsCheckerResponseV1.setMessage(MessageCode.CLAIMS_1073.value());
                claimsCheckerResponseV1.setClaims(ClaimsCheckerAdapter.adptClaimsEntityListToClaimsCheckerResponseList(claimsEntityListYesterday));
                return claimsCheckerResponseV1;
                //throw new BadRequestException(MessageCode.CLAIMS_1073);
            }
        }
        //Controllo Pending ad IERI
        if(goLiveStrategyService.getGoLivePendingCheck()){
            List<ClaimsPendingEntity> claimsPendingEntityList= claimsPendingRepository.findByPlateAndBetweenDateAccident(plate, DateAdapter.adptLocalDateTimeToDate(dateFrom).toInstant(), DateAdapter.adptLocalDateTimeToDate(dateTo).toInstant());
            if(!CollectionUtils.isEmpty(claimsPendingEntityList)){
                claimsCheckerResponseV1.setCode(MessageCode.CLAIMS_1073.name());
                claimsCheckerResponseV1.setMessage(MessageCode.CLAIMS_1073.value());
                return claimsCheckerResponseV1;
            }
        }

        //CONROLLO DOMANI
        dateFrom = dateTimeDateAccident.plusDays(1).withHour(hourDayMinus).withMinute(0).withSecond(0);
        dateTo = dateTimeDateAccident.plusDays(2).withHour(hourDayPlus).withMinute(59).withSecond(59);

        if (typeAccidentList != null) {
            //REFACTOR
            //recupero della nuoca entità

            List<ClaimsNewEntity> claimsNewEntityListTomorrow = claimsNewRepository.findClaimsForPaginationWithoutDraft(null, null, null, null, plate, null, null, DateUtil.convertUTCDateToIS08601String(DateAdapter.adptLocalDateTimeToDate(dateFrom)), DateUtil.convertUTCDateToIS08601String(DateAdapter.adptLocalDateTimeToDate(dateTo)), typeAccidentList, null, null, 1, 5, null, null, null, null, null, null);
            if (claimsNewEntityListTomorrow != null && !claimsNewEntityListTomorrow.isEmpty()) {
                //conversione nella vecchia entità
                List<ClaimsEntity> claimsEntityListTomorrow =converterClaimsService.wrapFromClaimsNewEntityListToClaimsEntityList(claimsNewEntityListTomorrow);
                if (!checkSameClaim(claimsEntityListTomorrow, idClaims)) {

                    claimsEntityListTomorrow = this.deleteSameClaim(claimsEntityListTomorrow, idClaims);
                    claimsCheckerResponseV1.setCode(MessageCode.CLAIMS_1077.name());
                    claimsCheckerResponseV1.setMessage(MessageCode.CLAIMS_1077.value());
                    claimsCheckerResponseV1.setClaims(ClaimsCheckerAdapter.adptClaimsEntityListToClaimsCheckerResponseList(claimsEntityListTomorrow));
                    return claimsCheckerResponseV1;
                    //throw new BadRequestException(MessageCode.CLAIMS_1077);
                }
            }
        }

        //REFACTOR
        //recupero della nuova entità
        List<ClaimsNewEntity> claimsNewEntityListTomorrow = claimsNewRepository.findClaimsForPaginationWithoutDraft(null, null, null, null, plate, null, null, DateUtil.convertUTCDateToIS08601String(DateAdapter.adptLocalDateTimeToDate(dateFrom)), DateUtil.convertUTCDateToIS08601String(DateAdapter.adptLocalDateTimeToDate(dateTo)), null, null, null, 1, 5, null, null, null, null, null, null);
        if (claimsNewEntityListTomorrow != null && !claimsNewEntityListTomorrow.isEmpty()) {
            List<ClaimsEntity> claimsEntityListTomorrow =converterClaimsService.wrapFromClaimsNewEntityListToClaimsEntityList(claimsNewEntityListTomorrow);
            if (!checkSameClaim(claimsEntityListTomorrow, idClaims)) {
                claimsEntityListTomorrow = this.deleteSameClaim(claimsEntityListTomorrow, idClaims);
                claimsCheckerResponseV1.setCode(MessageCode.CLAIMS_1074.name());
                claimsCheckerResponseV1.setMessage(MessageCode.CLAIMS_1074.value());
                claimsCheckerResponseV1.setClaims(ClaimsCheckerAdapter.adptClaimsEntityListToClaimsCheckerResponseList(claimsEntityListTomorrow));
                return claimsCheckerResponseV1;
                //throw new BadRequestException(MessageCode.CLAIMS_1074);
            }
        }
        //Controllo Pending ad DOMANI
        if(goLiveStrategyService.getGoLivePendingCheck()){
            List<ClaimsPendingEntity> claimsPendingEntityList= claimsPendingRepository.findByPlateAndBetweenDateAccident(plate, DateAdapter.adptLocalDateTimeToDate(dateFrom).toInstant(), DateAdapter.adptLocalDateTimeToDate(dateTo).toInstant());
            if(!CollectionUtils.isEmpty(claimsPendingEntityList)){
                claimsCheckerResponseV1.setCode(MessageCode.CLAIMS_1074.name());
                claimsCheckerResponseV1.setMessage(MessageCode.CLAIMS_1074.value());
                return claimsCheckerResponseV1;
            }
        }

        claimsCheckerResponseV1.setMessage(null);
        claimsCheckerResponseV1.setClaims(null);
        return claimsCheckerResponseV1;

    }



    //REFACTOR
    @Override
    public ClaimsCheckerResponseV1 checkIfExistsAnotherCounterpartyWithEqualsPlateName(String driverPlate, String dateAccident, String counterpartyName, String counterpartySurnameName, String counterpartyPlate, String idClaims) {

        Integer hourDayMinus = 22;
        Integer hourDayPlus = 21;

        ClaimsCheckerResponseV1 claimsCheckerResponseV1 = new ClaimsCheckerResponseV1();

        if (driverPlate != null) {
            if (dateAccident != null) {

                Date dateUtc =DateUtil.convertIS08601StringToUTCDate(dateAccident);
                LocalDateTime dateTimeDateAccident = DateAdapter.adptDateToLocalDataTime(dateUtc);

                if(dateTimeDateAccident != null){
                    try {
                        Long differenceHour = checkHoursDifference(dateUtc);
                        if(differenceHour > 1){
                            //2 ore di differenze
                            hourDayMinus = 22;
                            hourDayPlus = 21;
                        }else{
                            hourDayMinus = 23;
                            hourDayPlus = 22;
                        }


                    } catch (ParseException e) {
                        LOGGER.debug(e.getMessage(), e);
                    }
                }

                if (dateTimeDateAccident.getHour() < hourDayMinus)
                    dateTimeDateAccident = dateTimeDateAccident.minusDays(1);

                //IERI
                LocalDateTime dateFrom = dateTimeDateAccident.minusDays(1).withHour(hourDayMinus).withMinute(0).withSecond(0);
                LocalDateTime dateTo = dateTimeDateAccident.withHour(hourDayPlus).withMinute(59).withSecond(59);

                //stessa targa danneggiato, targa controparte, nome e cognome controparte, data incidente oggi-1 (validato).
                if (counterpartyPlate != null && counterpartyName != null && counterpartySurnameName != null) {
                    //REFACTOR
                    //recupero vecchia entità
                    List<ClaimsNewEntity> claimsNewEntityListYesterdaySamePlate = claimsNewRepository.findClaimsByDamagedPlateAndCounterpartyPlateName(driverPlate, counterpartyPlate, counterpartyName, counterpartySurnameName, DateUtil.convertUTCDateToIS08601String(DateAdapter.adptLocalDateTimeToDate(dateFrom)), DateUtil.convertUTCDateToIS08601String(DateAdapter.adptLocalDateTimeToDate(dateTo)));
                    if (claimsNewEntityListYesterdaySamePlate != null && !claimsNewEntityListYesterdaySamePlate.isEmpty()) {
                        //conversione della nuova entità
                        List<ClaimsEntity> claimsEntityListYesterdaySamePlate = converterClaimsService.wrapFromClaimsNewEntityListToClaimsEntityList(claimsNewEntityListYesterdaySamePlate);
                        if (!(checkSameClaim(claimsEntityListYesterdaySamePlate, idClaims))) {
                            claimsEntityListYesterdaySamePlate = this.deleteSameClaim(claimsEntityListYesterdaySamePlate, idClaims);
                            claimsCheckerResponseV1.setCode(MessageCode.CLAIMS_1075.name());
                            claimsCheckerResponseV1.setMessage(MessageCode.CLAIMS_1075.value());
                            claimsCheckerResponseV1.setClaims(ClaimsCheckerAdapter.adptClaimsEntityListToClaimsCheckerResponseList(claimsEntityListYesterdaySamePlate));
                            return claimsCheckerResponseV1;
                            //throw new BadRequestException(MessageCode.CLAIMS_1075);
                        }
                    }
                }

                //stessa targa danneggiato, targa controparte, data incidente oggi-1 (validato).
                if (!StringUtil.isNullOrEmpty(counterpartyPlate)) {
                    //REFACTOR
                    //recupero vecchia entità
                    List<ClaimsNewEntity> claimsNewEntityListYesterdaySamePlate = claimsNewRepository.findClaimsByDamagedPlateAndCounterpartyPlateName(driverPlate, counterpartyPlate, null, null, DateUtil.convertUTCDateToIS08601String(DateAdapter.adptLocalDateTimeToDate(dateFrom)), DateUtil.convertUTCDateToIS08601String(DateAdapter.adptLocalDateTimeToDate(dateTo)));
                    if (claimsNewEntityListYesterdaySamePlate != null && !claimsNewEntityListYesterdaySamePlate.isEmpty()) {
                        //conversione nel vecchio
                        List<ClaimsEntity> claimsEntityListYesterdaySamePlate = converterClaimsService.wrapFromClaimsNewEntityListToClaimsEntityList(claimsNewEntityListYesterdaySamePlate);
                        if (!(checkSameClaim(claimsEntityListYesterdaySamePlate, idClaims))) {
                            claimsEntityListYesterdaySamePlate = this.deleteSameClaim(claimsEntityListYesterdaySamePlate, idClaims);
                            claimsCheckerResponseV1.setCode(MessageCode.CLAIMS_1080.name());
                            claimsCheckerResponseV1.setMessage(MessageCode.CLAIMS_1080.value());
                            claimsCheckerResponseV1.setClaims(ClaimsCheckerAdapter.adptClaimsEntityListToClaimsCheckerResponseList(claimsEntityListYesterdaySamePlate));
                            return claimsCheckerResponseV1;
                            //throw new BadRequestException(MessageCode.CLAIMS_1080);
                        }
                    }
                }

                //stessa targa danneggiato, nome e cognome controparte, data incidente oggi-1 (validato).
                if (counterpartyName != null && counterpartySurnameName != null) {
                    //recupero della nuova entità
                    List<ClaimsNewEntity> claimsNewEntityListYesterday = claimsNewRepository.findClaimsByDamagedPlateAndCounterpartyPlateName(driverPlate, null, counterpartyName, counterpartySurnameName, DateUtil.convertUTCDateToIS08601String(DateAdapter.adptLocalDateTimeToDate(dateFrom)), DateUtil.convertUTCDateToIS08601String(DateAdapter.adptLocalDateTimeToDate(dateTo)));
                    if (claimsNewEntityListYesterday != null && !claimsNewEntityListYesterday.isEmpty()) {
                        //conversione nella vecchia entità
                        List<ClaimsEntity> claimsEntityListYesterday = converterClaimsService.wrapFromClaimsNewEntityListToClaimsEntityList(claimsNewEntityListYesterday);
                        if (!(checkSameClaim(claimsEntityListYesterday, idClaims))) {
                            claimsEntityListYesterday = this.deleteSameClaim(claimsEntityListYesterday, idClaims);
                            claimsCheckerResponseV1.setCode(MessageCode.CLAIMS_1063.name());
                            claimsCheckerResponseV1.setMessage(MessageCode.CLAIMS_1063.value());
                            claimsCheckerResponseV1.setClaims(ClaimsCheckerAdapter.adptClaimsEntityListToClaimsCheckerResponseList(claimsEntityListYesterday));
                            return claimsCheckerResponseV1;
                            //throw new BadRequestException(MessageCode.CLAIMS_1063);
                        }
                    }
                }

                //CONTROLLO GIORNO STESSO
                dateFrom = dateTimeDateAccident.withHour(hourDayMinus).withMinute(0).withSecond(0);
                dateTo = dateTimeDateAccident.plusDays(1).withHour(hourDayPlus).withMinute(59).withSecond(59);

                //stessa targa danneggiato, targa controparte, nome e cognome controparte, data incidente oggi (validato).
                if (counterpartyPlate != null && counterpartyName != null && counterpartySurnameName != null) {
                    //recupero della nuova entità
                    List<ClaimsNewEntity> claimsNewEntityListTodaySamePlate = claimsNewRepository.findClaimsByDamagedPlateAndCounterpartyPlateName(driverPlate, counterpartyPlate, counterpartyName, counterpartySurnameName, DateUtil.convertUTCDateToIS08601String(DateAdapter.adptLocalDateTimeToDate(dateFrom)), DateUtil.convertUTCDateToIS08601String(DateAdapter.adptLocalDateTimeToDate(dateTo)));
                    if (claimsNewEntityListTodaySamePlate != null && !claimsNewEntityListTodaySamePlate.isEmpty()) {
                        //conversione nella vecchia entità
                        List<ClaimsEntity> claimsEntityListTodaySamePlate = converterClaimsService.wrapFromClaimsNewEntityListToClaimsEntityList(claimsNewEntityListTodaySamePlate);
                        if (!(checkSameClaim(claimsEntityListTodaySamePlate, idClaims))) {
                            claimsEntityListTodaySamePlate = this.deleteSameClaim(claimsEntityListTodaySamePlate, idClaims);
                            claimsCheckerResponseV1.setCode(MessageCode.CLAIMS_1081.name());
                            claimsCheckerResponseV1.setMessage(MessageCode.CLAIMS_1081.value());
                            claimsCheckerResponseV1.setClaims(ClaimsCheckerAdapter.adptClaimsEntityListToClaimsCheckerResponseList(claimsEntityListTodaySamePlate));
                            return claimsCheckerResponseV1;
                            //throw new BadRequestException(MessageCode.CLAIMS_1081);
                        }
                    }
                }

                //stessa targa danneggiato, targa controparte, data incidente oggi (validato).
                if (!StringUtil.isNullOrEmpty(counterpartyPlate)) {
                    //REFACTOR
                    //recupero della nuova entità
                    List<ClaimsNewEntity> claimsNewEntityListTodaySamePlate = claimsNewRepository.findClaimsByDamagedPlateAndCounterpartyPlateName(driverPlate, counterpartyPlate, null, null, DateUtil.convertUTCDateToIS08601String(DateAdapter.adptLocalDateTimeToDate(dateFrom)), DateUtil.convertUTCDateToIS08601String(DateAdapter.adptLocalDateTimeToDate(dateTo)));

                    if (claimsNewEntityListTodaySamePlate != null && !claimsNewEntityListTodaySamePlate.isEmpty()) {
                        //conversione nella vecchia entità
                        List<ClaimsEntity> claimsEntityListTodaySamePlate = converterClaimsService.wrapFromClaimsNewEntityListToClaimsEntityList(claimsNewEntityListTodaySamePlate);
                        if (!(checkSameClaim(claimsEntityListTodaySamePlate, idClaims))) {
                            claimsEntityListTodaySamePlate = this.deleteSameClaim(claimsEntityListTodaySamePlate, idClaims);
                            claimsCheckerResponseV1.setCode(MessageCode.CLAIMS_1076.name());
                            claimsCheckerResponseV1.setMessage(MessageCode.CLAIMS_1076.value());
                            claimsCheckerResponseV1.setClaims(ClaimsCheckerAdapter.adptClaimsEntityListToClaimsCheckerResponseList(claimsEntityListTodaySamePlate));
                            return claimsCheckerResponseV1;
                            //throw new BadRequestException(MessageCode.CLAIMS_1076);
                        }
                    }
                }

                //stessa targa danneggiato, nome e cognome controparte, data incidente oggi (validato).
                if (counterpartyName != null && counterpartySurnameName != null) {

                    //REFACTOR
                    //recupero della nuova entità
                    List<ClaimsNewEntity> claimsNewEntityListToday = claimsNewRepository.findClaimsByDamagedPlateAndCounterpartyPlateName(driverPlate, null, counterpartyName, counterpartySurnameName, DateUtil.convertUTCDateToIS08601String(DateAdapter.adptLocalDateTimeToDate(dateFrom)), DateUtil.convertUTCDateToIS08601String(DateAdapter.adptLocalDateTimeToDate(dateTo)));
                    if (claimsNewEntityListToday != null && !claimsNewEntityListToday.isEmpty()) {
                        //conversione nella vecchia entità
                        List<ClaimsEntity> claimsEntityListToday = converterClaimsService.wrapFromClaimsNewEntityListToClaimsEntityList(claimsNewEntityListToday);
                        if (!(checkSameClaim(claimsEntityListToday, idClaims))) {
                            claimsEntityListToday = this.deleteSameClaim(claimsEntityListToday, idClaims);
                            claimsCheckerResponseV1.setCode(MessageCode.CLAIMS_1071.name());
                            claimsCheckerResponseV1.setMessage(MessageCode.CLAIMS_1071.value());
                            claimsCheckerResponseV1.setClaims(ClaimsCheckerAdapter.adptClaimsEntityListToClaimsCheckerResponseList(claimsEntityListToday));
                            return claimsCheckerResponseV1;
                            //throw new BadRequestException(MessageCode.CLAIMS_1071);
                        }
                    }
                }

                //DOMANI
                dateFrom = dateTimeDateAccident.plusDays(1).withHour(hourDayMinus).withMinute(0).withSecond(0);
                dateTo = dateTimeDateAccident.plusDays(2).withHour(hourDayMinus).withMinute(59).withSecond(59);

                //stessa targa danneggiato, targa controparte, nome e cognome controparte, data incidente domani (validato).
                if (counterpartyPlate != null && counterpartyName != null && counterpartySurnameName != null) {
                    //REFACTOR
                    //recupero della vecchia entità
                    List<ClaimsNewEntity> claimsNewEntityListTomorrowSamePlate = claimsNewRepository.findClaimsByDamagedPlateAndCounterpartyPlateName(driverPlate, counterpartyPlate, counterpartyName, counterpartySurnameName, DateUtil.convertUTCDateToIS08601String(DateAdapter.adptLocalDateTimeToDate(dateFrom)), DateUtil.convertUTCDateToIS08601String(DateAdapter.adptLocalDateTimeToDate(dateTo)));
                    if (claimsNewEntityListTomorrowSamePlate != null && !claimsNewEntityListTomorrowSamePlate.isEmpty()) {
                        //conversione nella vecchia entità
                        List<ClaimsEntity> claimsEntityListTomorrowSamePlate = converterClaimsService.wrapFromClaimsNewEntityListToClaimsEntityList(claimsNewEntityListTomorrowSamePlate);
                        if (!(checkSameClaim(claimsEntityListTomorrowSamePlate, idClaims))) {
                            claimsEntityListTomorrowSamePlate = this.deleteSameClaim(claimsEntityListTomorrowSamePlate, idClaims);
                            claimsCheckerResponseV1.setCode(MessageCode.CLAIMS_1082.name());
                            claimsCheckerResponseV1.setMessage(MessageCode.CLAIMS_1082.value());
                            claimsCheckerResponseV1.setClaims(ClaimsCheckerAdapter.adptClaimsEntityListToClaimsCheckerResponseList(claimsEntityListTomorrowSamePlate));
                            return claimsCheckerResponseV1;
                            //throw new BadRequestException(MessageCode.CLAIMS_1082);
                        }
                    }
                }

                //stessa targa danneggiato, targa controparte, data incidente domani (validato).
                if (!StringUtil.isNullOrEmpty(counterpartyPlate)) {
                    //REFACTOR
                    //recupero della nuova entità
                    List<ClaimsNewEntity> claimsNewEntityListTomorrowSamePlate = claimsNewRepository.findClaimsByDamagedPlateAndCounterpartyPlateName(driverPlate, counterpartyPlate, null, null, DateUtil.convertUTCDateToIS08601String(DateAdapter.adptLocalDateTimeToDate(dateFrom)), DateUtil.convertUTCDateToIS08601String(DateAdapter.adptLocalDateTimeToDate(dateTo)));
                    if (claimsNewEntityListTomorrowSamePlate != null && !claimsNewEntityListTomorrowSamePlate.isEmpty()) {
                        //conversione nella vecchia entità
                        List<ClaimsEntity> claimsEntityListTomorrowSamePlate = converterClaimsService.wrapFromClaimsNewEntityListToClaimsEntityList(claimsNewEntityListTomorrowSamePlate);
                        if (!(checkSameClaim(claimsEntityListTomorrowSamePlate, idClaims))) {
                            claimsEntityListTomorrowSamePlate = this.deleteSameClaim(claimsEntityListTomorrowSamePlate, idClaims);
                            claimsCheckerResponseV1.setCode(MessageCode.CLAIMS_1083.name());
                            claimsCheckerResponseV1.setMessage(MessageCode.CLAIMS_1083.value());
                            claimsCheckerResponseV1.setClaims(ClaimsCheckerAdapter.adptClaimsEntityListToClaimsCheckerResponseList(claimsEntityListTomorrowSamePlate));
                            return claimsCheckerResponseV1;
                            //throw new BadRequestException(MessageCode.CLAIMS_1083);
                        }
                    }
                }

                //stessa targa danneggiato, nome e cognome controparte, data incidente domani (validato).
                if (counterpartyName != null && counterpartySurnameName != null) {
                    //REFACTOR
                    //recupero della nuova entità
                    List<ClaimsNewEntity> claimsNewEntityListTomorrow = claimsNewRepository.findClaimsByDamagedPlateAndCounterpartyPlateName(driverPlate, null, counterpartyName, counterpartySurnameName, DateUtil.convertUTCDateToIS08601String(DateAdapter.adptLocalDateTimeToDate(dateFrom)), DateUtil.convertUTCDateToIS08601String(DateAdapter.adptLocalDateTimeToDate(dateTo)));
                    if (claimsNewEntityListTomorrow != null && !claimsNewEntityListTomorrow.isEmpty()) {
                        //conversione nella vecchia entità
                        List<ClaimsEntity> claimsEntityListTomorrow = converterClaimsService.wrapFromClaimsNewEntityListToClaimsEntityList(claimsNewEntityListTomorrow);
                        if (!(checkSameClaim(claimsEntityListTomorrow, idClaims))) {
                            claimsEntityListTomorrow = this.deleteSameClaim(claimsEntityListTomorrow, idClaims);
                            claimsCheckerResponseV1.setCode(MessageCode.CLAIMS_1072.name());
                            claimsCheckerResponseV1.setMessage(MessageCode.CLAIMS_1072.value());
                            claimsCheckerResponseV1.setClaims(ClaimsCheckerAdapter.adptClaimsEntityListToClaimsCheckerResponseList(claimsEntityListTomorrow));
                            return claimsCheckerResponseV1;
                            //throw new BadRequestException(MessageCode.CLAIMS_1072);
                        }
                    }
                }
            }


            //stessa targa danneggiato, targa controparte, nome e cognome controparte.
            //modificato eliminando targa danneggiato
            if (counterpartyPlate != null && counterpartyName != null && counterpartySurnameName != null) {
                //REFACTOR
                //recupero della vecchia entità
                List<ClaimsNewEntity> claimsNewEntityList2 = claimsNewRepository.findClaimsByDamagedPlateAndCounterpartyPlateName(null, counterpartyPlate, counterpartyName, counterpartySurnameName, null, null);
                if (claimsNewEntityList2 != null && !claimsNewEntityList2.isEmpty()) {
                    List<ClaimsEntity> claimsEntityList2 = converterClaimsService.wrapFromClaimsNewEntityListToClaimsEntityList(claimsNewEntityList2);
                    if (!checkSameClaim(claimsEntityList2, idClaims)) {
                        claimsEntityList2 = this.deleteSameClaim(claimsEntityList2, idClaims);
                        claimsCheckerResponseV1.setCode(MessageCode.CLAIMS_1059.name());
                        claimsCheckerResponseV1.setMessage(MessageCode.CLAIMS_1059.value());
                        claimsCheckerResponseV1.setClaims(ClaimsCheckerAdapter.adptClaimsEntityListToClaimsCheckerResponseList(claimsEntityList2));
                        return claimsCheckerResponseV1;
                        //throw new BadRequestException(MessageCode.CLAIMS_1059);
                    }
                }
            }

            //stessa targa danneggiato e targa controparte.
            //modificato eliminando targa danneggiato
            if (!StringUtil.isNullOrEmpty(counterpartyPlate)) {
                //REFACTOR
                //recupero della vecchia entità
                List<ClaimsNewEntity> claimsNewEntityList3 = claimsNewRepository.findClaimsByDamagedPlateAndCounterpartyPlateName(null, counterpartyPlate, null, null, null, null);
                if (claimsNewEntityList3 != null && !claimsNewEntityList3.isEmpty()) {
                    //conversione nella vecchia entità
                    List<ClaimsEntity> claimsEntityList3 = converterClaimsService.wrapFromClaimsNewEntityListToClaimsEntityList(claimsNewEntityList3);
                    if (!checkSameClaim(claimsEntityList3, idClaims)) {
                        claimsEntityList3 = this.deleteSameClaim(claimsEntityList3, idClaims);
                        claimsCheckerResponseV1.setCode(MessageCode.CLAIMS_1060.name());
                        claimsCheckerResponseV1.setMessage(MessageCode.CLAIMS_1060.value());
                        claimsCheckerResponseV1.setClaims(ClaimsCheckerAdapter.adptClaimsEntityListToClaimsCheckerResponseList(claimsEntityList3));
                        return claimsCheckerResponseV1;
                        //throw new BadRequestException(MessageCode.CLAIMS_1060);
                    }
                }
            }

            //stessa targa danneggiato, nome e cognome controparte.
            //modificato eliminando targa danneggiato
            if (counterpartyName != null && counterpartySurnameName != null) {
                //recupero della nuova entità
                List<ClaimsNewEntity> claimsNewEntityList4 = claimsNewRepository.findClaimsByDamagedPlateAndCounterpartyPlateName(null, null, counterpartyName, counterpartySurnameName, null, null);
                if (claimsNewEntityList4 != null && !claimsNewEntityList4.isEmpty()) {
                    //conversione nella vecchia entità
                    List<ClaimsEntity> claimsEntityList4 = converterClaimsService.wrapFromClaimsNewEntityListToClaimsEntityList(claimsNewEntityList4);
                    if (!checkSameClaim(claimsEntityList4, idClaims)) {
                        claimsEntityList4 = this.deleteSameClaim(claimsEntityList4, idClaims);
                        claimsCheckerResponseV1.setCode(MessageCode.CLAIMS_1061.name());
                        claimsCheckerResponseV1.setMessage(MessageCode.CLAIMS_1061.value());
                        claimsCheckerResponseV1.setClaims(ClaimsCheckerAdapter.adptClaimsEntityListToClaimsCheckerResponseList(claimsEntityList4));
                        return claimsCheckerResponseV1;
                        //throw new BadRequestException(MessageCode.CLAIMS_1061);
                    }
                }
            }
        }

        claimsCheckerResponseV1.setMessage(null);
        claimsCheckerResponseV1.setClaims(null);
        return claimsCheckerResponseV1;

    }


    //REFACTOR
    @Override
    public ClaimsCheckerResponseV1 checkClaimsInValidation(String idClaims) {

        //recupero della vecchia entità
        Optional<ClaimsNewEntity> claimsNewEntityOptional = claimsNewRepository.findById(idClaims);

        if (!claimsNewEntityOptional.isPresent()) {
            LOGGER.debug(MessageCode.CLAIMS_1010.value());
            throw new BadRequestException(MessageCode.CLAIMS_1010);
        }

        String plate = null;
        String dateAccident = null;
        String typeAccident = null;
        //conversione nella vecchia entità
        ClaimsEntity claimsEntity = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntityOptional.get());
        if(claimsEntity.getDamaged() != null &&  claimsEntity.getDamaged().getVehicle() != null ) {
            plate = claimsEntity.getDamaged().getVehicle().getLicensePlate();
        }
        if(claimsEntity.getComplaint() != null &&  claimsEntity.getComplaint().getDataAccident() != null && claimsEntity.getComplaint().getDataAccident().getDateAccident() != null){
            dateAccident = DateUtil.convertUTCDateToIS08601String(claimsEntity.getComplaint().getDataAccident().getDateAccident());
        }
        if(claimsEntity.getComplaint() != null &&  claimsEntity.getComplaint().getDataAccident() != null && claimsEntity.getComplaint().getDataAccident().getTypeAccident() != null) {
            typeAccident = claimsEntity.getComplaint().getDataAccident().getTypeAccident().getValue();
        }

        ClaimsCheckerResponseV1 claimsCheckerResponseV1 = this.checkClaimsDuplicate(plate, dateAccident, typeAccident, idClaims);
        if (claimsCheckerResponseV1.getCode() != null)
            return claimsCheckerResponseV1;

        List<CounterpartyEntity> counterparties = claimsEntity.getCounterparts();

        if(counterparties != null ) {
            for (CounterpartyEntity counterparty : counterparties) {
                String counterpartyPlate = null;
                String counterpartyFirstname = null;
                String counterpartyLastname = null;

                if (counterparty.getType() != null && counterparty.getType().equals(VehicleTypeEnum.VEHICLE) && counterparty.getVehicle() != null) {
                    counterpartyPlate = counterparty.getVehicle().getLicensePlate();
                }

                if (counterparty.getInsured() != null) {
                    counterpartyFirstname = counterparty.getInsured().getFirstname();
                    counterpartyLastname = counterparty.getInsured().getLastname();
                }

                if (counterpartyPlate != null || (counterpartyFirstname != null && counterpartyLastname != null)) {
                    claimsCheckerResponseV1 = this.checkIfExistsAnotherCounterpartyWithEqualsPlateName(plate, dateAccident, counterpartyFirstname, counterpartyLastname, counterpartyPlate, idClaims);
                    if (claimsCheckerResponseV1.getCode() != null)
                        return claimsCheckerResponseV1;
                }
            }
        }


        return claimsCheckerResponseV1;
    }

    @Override
    public FlowTypeResponseV1 checkFlowtype(String contractType, String customerId) {

        FlowTypeResponseV1 flowTypeResponseV1 = new FlowTypeResponseV1();
        ClaimsFlowType claimsFlowType = new ClaimsFlowType();
        List<ClaimsFlowType> listClaimsFlowType = new ArrayList<>();

        List<Object[]> flowTypeForCustomerRisk = claimsTypeRepository.getFlowTypeChecker(customerId,contractType);

        for (Object[] currentObj : flowTypeForCustomerRisk) {
            claimsFlowType = new ClaimsFlowType();
            claimsFlowType.setType((String) currentObj[0]);
            claimsFlowType.setClaimsAccidentType((String) currentObj[1]);
            claimsFlowType.setFlow((String) currentObj[2]);
            claimsFlowType.setTypeRisk((String) currentObj[3]);
            listClaimsFlowType.add(claimsFlowType);
        }

        flowTypeResponseV1.setClaimsFlowType(listClaimsFlowType);

        return flowTypeResponseV1;
    }

    private static boolean checkSameClaim(List<ClaimsEntity> claimsEntities, String idClaims) {
        if (idClaims == null) return false;
        if (claimsEntities.size() == 1) {
            return claimsEntities.get(0).getId().equals(idClaims);
        }
        return false;
    }

    private List<ClaimsEntity> deleteSameClaim(List<ClaimsEntity> claimsEntities, String idClaims) {
        if (claimsEntities != null && idClaims != null) {
            claimsEntities.removeIf(claimsEntity -> claimsEntity.getId().equals(idClaims));
        }
        return claimsEntities;
    }

}
