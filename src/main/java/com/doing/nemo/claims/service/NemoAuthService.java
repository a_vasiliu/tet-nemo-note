package com.doing.nemo.claims.service;

import com.doing.nemo.claims.controller.payload.request.NemoAuthExternalMetadataRequestV1;
import com.doing.nemo.claims.controller.payload.response.NemoAuthCompleteResponseV1;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Map;

@Service
public interface NemoAuthService {
    NemoAuthCompleteResponseV1 postGrant (Map<String,String> metadata) throws IOException;
    public String getExternalToken(NemoAuthExternalMetadataRequestV1 nemoAuthExternalMetadataRequestV1, String claimsId, String practiceId, Boolean isIncomplete) throws IOException;
}
