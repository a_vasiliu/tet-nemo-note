package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.adapter.FleetManagerAdpter;
import com.doing.nemo.claims.controller.payload.response.PersonalDataResponseV1;
import com.doing.nemo.claims.entity.esb.CustomerESB.CustomerEsb;
import com.doing.nemo.claims.entity.esb.FleetManagerESB.FleetManagerEsb;
import com.doing.nemo.claims.entity.jsonb.personalData.FleetManagerPersonalData;
import com.doing.nemo.claims.entity.settings.*;
import com.doing.nemo.claims.repository.PersonalDataRepository;
import com.doing.nemo.claims.repository.PersonalDataRepositoryV1;
import com.doing.nemo.claims.service.*;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.exception.InternalException;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.apache.commons.lang.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

import java.io.IOException;

@Service
public class PersonalDataServiceImpl implements PersonalDataService {

    private static Logger LOGGER = LoggerFactory.getLogger(PersonalDataServiceImpl.class);
    @Autowired
    private PersonalDataRepository personalDataRepository;
    @Autowired
    private PersonalDataRepositoryV1 personalDataRepositoryV1;
    @Autowired
    private ContractPersonalDataService contractPersonalDataService;
    @Autowired
    private CustomTypeRiskService customTypeRiskService;
    @Autowired
    private InsurancePolicyPersonalDataService insurancePolicyPersonalDataService;
    @Autowired
    private ESBService esbService;

    @Override
    public PersonalDataEntity insertPersonalData(PersonalDataEntity personalDataEntity) {
        if (personalDataRepository.findByCustomerId(personalDataEntity.getCustomerId()) != null) {
            LOGGER.debug("Error in PersonalData, duplicate customer_id not admitted");
            throw new BadRequestException("Error in PersonalData, duplicate customer_id not admitted", MessageCode.CLAIMS_1009);
        }

        if(personalDataEntity.getCustomTypeRiskEntityList() != null) {

            Set<ClaimsTypeEntity> uniqueTypeRisk = new HashSet<>();

            for (CustomTypeRiskEntity customTypeRiskEntity : personalDataEntity.getCustomTypeRiskEntityList()) {
                uniqueTypeRisk.add(customTypeRiskEntity.getClaimsTypeEntity());
            }

            if (uniqueTypeRisk.size() < personalDataEntity.getCustomTypeRiskEntityList().size()) {
                LOGGER.debug("Error in PersonalData, duplicate personal type risk on list recived not admitted");
                throw new BadRequestException("Error in PersonalData, duplicate personal type risk on list recived not admitted not admitted", MessageCode.CLAIMS_2013);
            }
        }

        checkAndUpdateFleetManagerID(personalDataEntity);

        personalDataRepository.save(personalDataEntity);

        return personalDataEntity;
    }

    @Override
    public PersonalDataEntity insertPersonalDataContract(List<ContractEntity> contractEntityList, UUID uuid) {
        PersonalDataEntity personalDataEntity = selectPersonalData(uuid);
        contractEntityList = contractPersonalDataService.updateContractList(contractEntityList);

        personalDataEntity.getContractEntityList().addAll(contractEntityList);
        return personalDataEntity;
    }

    @Override
    public PersonalDataEntity insertPersonalDataCustomTypeRisk(List<CustomTypeRiskEntity> customTypeRiskEntityList, UUID uuid) {
        PersonalDataEntity personalDataEntity = selectPersonalData(uuid);


        if (customTypeRiskEntityList != null && personalDataEntity.getCustomTypeRiskEntityList()!= null) {

            for(CustomTypeRiskEntity customTypeRiskEntityNew: customTypeRiskEntityList){
                for(CustomTypeRiskEntity customTypeRiskEntityOld: personalDataEntity.getCustomTypeRiskEntityList()){
                    if(customTypeRiskEntityNew.getClaimsTypeEntity().equals(customTypeRiskEntityOld.getClaimsTypeEntity())){
                        LOGGER.debug("Error in PersonalData, duplicate personal type risk not admitted");
                        throw new BadRequestException("Error in PersonalData, duplicate personal type risk not admitted", MessageCode.CLAIMS_2013);
                    }
                }
            }

        }

        if(customTypeRiskEntityList != null){

            Set<ClaimsTypeEntity> uniqueTypeRisk = new HashSet<>();

            for(CustomTypeRiskEntity customTypeRiskEntityNew: customTypeRiskEntityList){
                uniqueTypeRisk.add(customTypeRiskEntityNew.getClaimsTypeEntity());
            }

            if(uniqueTypeRisk.size() < customTypeRiskEntityList.size()){
                LOGGER.debug("Error in PersonalData, duplicate personal type risk on list recived not admitted");
                throw new BadRequestException("Error in PersonalData, duplicate personal type risk on list recived not admitted not admitted", MessageCode.CLAIMS_2013);
            }

        }

        customTypeRiskEntityList = customTypeRiskService.updateCustomTypeRiskList(customTypeRiskEntityList);

        personalDataEntity.getCustomTypeRiskEntityList().addAll(customTypeRiskEntityList);
        return personalDataEntity;
    }

    @Override
    public PersonalDataEntity insertPersonalDataInsurancePolicyPersonalData(List<InsurancePolicyPersonalDataEntity> insurancePolicyPersonalDataEntityList, UUID uuid) {
        PersonalDataEntity personalDataEntity = selectPersonalData(uuid);
        insurancePolicyPersonalDataEntityList = insurancePolicyPersonalDataService.updateInsurancePolicyPersonalDataList(insurancePolicyPersonalDataEntityList);

        personalDataEntity.getInsurancePolicyPersonalDataEntityList().addAll(insurancePolicyPersonalDataEntityList);
        return personalDataEntity;
    }


    @Override
    public PersonalDataEntity updatePersonalData(PersonalDataEntity personalDataEntity, UUID uuid) {
        Optional<PersonalDataEntity> personalDataEntityOptional = personalDataRepository.findById(uuid);
        if (!personalDataEntityOptional.isPresent()) {
            LOGGER.debug("Personal data with id " + uuid + " not found");
            throw new NotFoundException("Personal data with id " + uuid + " not found", MessageCode.CLAIMS_1010);
        }


        if (personalDataRepository.findByCustomerIdWithDifferentUUID(personalDataEntity.getId(), personalDataEntity.getCustomerId()) != null) {
            LOGGER.debug("Error in PersonalData, duplicate customer_id not admitted");
            throw new BadRequestException("Error in PersonalData, duplicate customer_id not admitted", MessageCode.CLAIMS_1009);
        }

        PersonalDataEntity personalDataEntity1 = personalDataEntityOptional.get();


        if(personalDataEntity.getCustomTypeRiskEntityList() != null) {

            Set<ClaimsTypeEntity> uniqueTypeRisk = new HashSet<>();

            for (CustomTypeRiskEntity customTypeRiskEntity : personalDataEntity.getCustomTypeRiskEntityList()) {
                uniqueTypeRisk.add(customTypeRiskEntity.getClaimsTypeEntity());
            }

            if (uniqueTypeRisk.size() < personalDataEntity.getCustomTypeRiskEntityList().size()) {
                LOGGER.debug("Error in PersonalData, duplicate personal type risk on list recived not admitted");
                throw new BadRequestException("Error in PersonalData, duplicate personal type risk on list recived not admitted not admitted", MessageCode.CLAIMS_2013);
            }
        }
        checkAndUpdateFleetManagerID(personalDataEntity);
        contractPersonalDataService.deleteContractList(personalDataEntity1.getContractEntityList());
        customTypeRiskService.deleteCustomTypeRiskList(personalDataEntity1.getCustomTypeRiskEntityList());
        insurancePolicyPersonalDataService.deleteInsurancePolicyPersonalDataList(personalDataEntity1.getInsurancePolicyPersonalDataEntityList());
        List<ContractEntity> contractEntityList = contractPersonalDataService.updateContractList(personalDataEntity.getContractEntityList());
        List<CustomTypeRiskEntity> customTypeRiskEntityList = customTypeRiskService.updateCustomTypeRiskList(personalDataEntity.getCustomTypeRiskEntityList());
        List<InsurancePolicyPersonalDataEntity> insurancePolicyPersonalDataEntityList = insurancePolicyPersonalDataService.updateInsurancePolicyPersonalDataList(personalDataEntity.getInsurancePolicyPersonalDataEntityList());

        personalDataEntity.setContractEntityList(contractEntityList);
        personalDataEntity.setCustomTypeRiskEntityList(customTypeRiskEntityList);
        personalDataEntity.setInsurancePolicyPersonalDataEntityList(insurancePolicyPersonalDataEntityList);
        personalDataRepository.save(personalDataEntity);

        //aggiornamento dell'id cliente da verificare

        return personalDataEntity;

    }

    private void checkAndUpdateFleetManagerID (PersonalDataEntity personalDataEntity){
        //Aggiunta dell'ID generato in maniera incrementale
        List<FleetManagerPersonalData> fleetManagerPersonalData = personalDataEntity.getFleetManagerPersonalData();

        //Inserisco in un set tutti gli ID dei fleetManager IMPORTATI
        if(fleetManagerPersonalData != null && fleetManagerPersonalData.size() > 0) {

            Set<Long> hashSetID = new HashSet<>();
            Long maxID = 0L;

            for (FleetManagerPersonalData fleetM : fleetManagerPersonalData) {
                if (fleetM.getIsImported() != null && fleetM.getIsImported()) {
                    hashSetID.add(fleetM.getFleetManagerId());
                }

                //Metto nell'hashSet gli ID già settati dei fleetManagerPersonalData e mi salvo il massimo
                if(fleetM.getIsImported()!=null && !fleetM.getIsImported()){
                    if(fleetM.getFleetManagerId() != null) {
                        hashSetID.add(fleetM.getFleetManagerId());

                        if(fleetM.getFleetManagerId() > maxID){
                            maxID = fleetM.getFleetManagerId();
                        }

                    }
                }

            }
            for(FleetManagerPersonalData fleetM : fleetManagerPersonalData){
                Integer grandezzaSet = hashSetID.size();
                //Setti gli ID per i FleetManager che non lo hanno
                if(fleetM.getFleetManagerId() == null){
                    while(grandezzaSet.equals(hashSetID.size())){
                        maxID++;
                        hashSetID.add(maxID);
                    }
                    fleetM.setFleetManagerId(maxID);
                }
            }
        }

    }

    @Override
    public PersonalDataEntity selectPersonalData(UUID id) {
        Optional<PersonalDataEntity> personalDataEntity = personalDataRepository.findById(id);
        if (!personalDataEntity.isPresent()) {
            LOGGER.debug("PersonalData with id " + id + " not found");
            throw new NotFoundException("PersonalData with id " + id + " not found", MessageCode.CLAIMS_1010);
        }

        return personalDataEntity.get();
    }

    @Override
    public List<PersonalDataEntity> selectAllPersonalData() {
        List<PersonalDataEntity> resultListPersonalData = personalDataRepository.findAll();

        return resultListPersonalData;
    }

    @Override
    public PersonalDataResponseV1 deletePersonalData(UUID id) {
        PersonalDataEntity personalDataEntity = selectPersonalData(id);
        //if(personalDataRepository.searchFatherPersonalDataForeignKey(id) != null)
        //throw new NotFoundException("Foreign key in PersonalData. PersonalData impossible to delete");
        personalDataRepository.delete(personalDataEntity);
        return null;
    }

    @Override
    public PersonalDataEntity updateStatus(UUID uuid) {
        PersonalDataEntity personalDataEntity = selectPersonalData(uuid);
        if (personalDataEntity.getActive()) {
            personalDataEntity.setActive(false);
        } else {
            personalDataEntity.setActive(true);
        }

        personalDataRepository.save(personalDataEntity);
        return personalDataEntity;
    }

    /*@Override
    public void saveEsbPersonalData(PersonalDataEntity personalDataEntity) {
        PersonalDataEntity personalDataEntityFound = personalDataRepository.findByCustomerId(personalDataEntity.getCustomerId());
        if (personalDataEntityFound == null) {
            personalDataRepository.save(personalDataEntity);
        } else {
            if(personalDataEntity.getCustomerId() != null)
                personalDataEntityFound.setCustomerId(personalDataEntity.getCustomerId());
            if (personalDataEntity.getName() != null)
                personalDataEntityFound.setName(personalDataEntity.getName());
            if (personalDataEntity.getVatNumber() != null)
                personalDataEntityFound.setVatNumber(personalDataEntity.getVatNumber());
            if (personalDataEntity.getPhoneNumber() != null)
                personalDataEntityFound.setName(personalDataEntity.getPhoneNumber());
            if (personalDataEntity.getAddress() != null)
                personalDataEntityFound.setAddress(personalDataEntity.getAddress());
            if (personalDataEntity.getCity() != null)
                personalDataEntityFound.setCity(personalDataEntity.getCity());
            if (personalDataEntity.getProvince() != null)
                personalDataEntityFound.setProvince(personalDataEntity.getProvince());
            if (personalDataEntity.getZipCode() != null)
                personalDataEntityFound.setZipCode(personalDataEntity.getZipCode());
            if (personalDataEntity.getState() != null)
                personalDataEntityFound.setState(personalDataEntity.getState());
            if (personalDataEntity.getEmail() != null)
                personalDataEntityFound.setEmail(personalDataEntity.getEmail());
            if (personalDataEntity.getPec() != null)
                personalDataEntityFound.setPec(personalDataEntity.getPec());
            if( personalDataEntity.getFleetManagerPersonalData() != null)
                personalDataEntityFound.setFleetManagerPersonalData(personalDataEntity.getFleetManagerPersonalData());

            personalDataRepository.save(personalDataEntityFound);
        }


    }*/

    @Override
    public PersonalDataEntity attachFatherToPersonalDataInsert(PersonalDataEntity personalDataEntity) {
        if (personalDataEntity.getPersonalFatherId() != null) {
            try {
                CustomerEsb customerEsbFather = esbService.getCustomer(personalDataEntity.getPersonalFatherId());
                if (customerEsbFather != null) {
                    personalDataEntity.setPersonalFatherId(customerEsbFather.getCustomerId());
                    personalDataEntity.setPersonalFatherName(customerEsbFather.getTradingName());
                } else personalDataEntity.setPersonalFatherId(null);

            } catch (IOException e) {
                LOGGER.debug("Error esb");
                throw new InternalException( MessageCode.CLAIMS_1018,e );
            }
        }
        return personalDataEntity;
    }

    @Override
    public PersonalDataEntity attachFatherToPersonalDataUpdate(PersonalDataEntity personalDataEntity, String precPersonalDataFather) {
        if (personalDataEntity.getPersonalFatherId() != null && (precPersonalDataFather == null || !precPersonalDataFather.equalsIgnoreCase(personalDataEntity.getPersonalFatherName()))) {
            try {
                CustomerEsb customerEsbFather = esbService.getCustomer(personalDataEntity.getPersonalFatherId());
                if (customerEsbFather != null) {
                    personalDataEntity.setPersonalFatherId(customerEsbFather.getCustomerId());
                    personalDataEntity.setPersonalFatherName(customerEsbFather.getTradingName());
                } else {
                    personalDataEntity.setPersonalFatherId(null);
                    personalDataEntity.setPersonalFatherName(null);
                }

            } catch (IOException e) {
                LOGGER.debug("Error esb");
                throw new InternalException(MessageCode.CLAIMS_1018, e);
            }
        }
        return personalDataEntity;
    }

    @Override
    public List<PersonalDataEntity> findPersonalDatasFilteredAscName(Integer page, Integer pageSize, String orderBy, Boolean asc, Long personalCode, String personalGroup, String name, String city, String province, String phoneNumber, String fax, String vatNumber, Boolean isActive, String customerId) {
        return personalDataRepositoryV1.findPersonalDatasFilteredAscName(page, pageSize, orderBy, asc, personalCode, personalGroup, name, city, province, phoneNumber, fax, vatNumber, isActive, customerId);
    }

    @Override
    public Long countPersonalDataFiltered(Long personalCode, String personalGroup, String name, String city, String province, String phoneNumber, String fax, String vatNumber, Boolean isActive, String customerId) {
        return personalDataRepositoryV1.countPersonalDataFiltered(personalCode, personalGroup, name, city, province, phoneNumber, fax, vatNumber, isActive, customerId);
    }

    @Override
    public PersonalDataEntity getPersonalDataAndUpdateFleetManagerList (UUID id) {
        PersonalDataEntity personalDataEntity= this.selectPersonalData(id);

        try {

            List<FleetManagerEsb> fleetManagerEsbList = esbService.getFleetManagers(personalDataEntity.getCustomerId());
            List<FleetManagerPersonalData> fleetManagerPersonalDataEsbList = FleetManagerAdpter.adptFromFleetManagerESBToFleetManagerPersonalData(fleetManagerEsbList);

            if(personalDataEntity.getFleetManagerPersonalData() != null){
                for(FleetManagerPersonalData fleetManagerPersonalDataDB :personalDataEntity.getFleetManagerPersonalData()){
                    if(fleetManagerPersonalDataEsbList!=null) {

                        Optional<FleetManagerPersonalData> optionalFleetManager = fleetManagerPersonalDataEsbList.stream().filter(
                                fleetManagerStream -> ObjectUtils.equals(fleetManagerStream.getFleetManagerId(),fleetManagerPersonalDataDB.getFleetManagerId())
                        ).findFirst();

                        if (!optionalFleetManager.isPresent() && !fleetManagerPersonalDataDB.getIsImported()) {
                            fleetManagerPersonalDataEsbList.add(fleetManagerPersonalDataDB);
                        }
                        if (optionalFleetManager.isPresent()) {
                            optionalFleetManager.get().setFleetManagerSecondaryEmail(fleetManagerPersonalDataDB.getFleetManagerSecondaryEmail());
                            optionalFleetManager.get().setDisableNotification(fleetManagerPersonalDataDB.isDisableNotification());
                        }
                    }
                }
            }

            personalDataEntity.setFleetManagerPersonalData(fleetManagerPersonalDataEsbList);

            personalDataRepository.save(personalDataEntity);

        } catch (IOException e) {
            LOGGER.debug("Error esb");
            throw new InternalException(MessageCode.CLAIMS_1018, e);
        }

        return personalDataEntity;
    }

}