package com.doing.nemo.claims.service;

import com.doing.nemo.claims.entity.settings.RiskEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface RiskService {
    //RiskEntity updateRiskEntity(RiskEntity riskEntity);
    List<RiskEntity> updateRiskEntityList(List<RiskEntity> riskEntities);

    void deleteRiskEntityList(List<RiskEntity> riskEntities);
}
