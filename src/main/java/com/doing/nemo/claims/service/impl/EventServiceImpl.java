package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.entity.settings.EventEntity;
import com.doing.nemo.claims.repository.EventRepository;
import com.doing.nemo.claims.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class EventServiceImpl implements EventService {
    @Autowired
    private EventRepository eventRepository;

    @Override
    public List<EventEntity> updateEventEntityList(List<EventEntity> eventEntities) {
        List<EventEntity> eventEntityList = new ArrayList<>();
        if (eventEntities != null) {

            for (EventEntity eventEntity : eventEntities) {
                eventRepository.save(eventEntity);
                EventEntity eventEntity1 = eventRepository.getOne(eventEntity.getId());
                eventEntityList.add(eventEntity1);
            }
        }
        return eventEntityList;
    }

    @Override
    public void deleteEventEntityList(List<EventEntity> eventEntities) {
        if (eventEntities != null) {
            for (EventEntity eventEntity : eventEntities) {
                eventRepository.delete(eventEntity);
            }
        }
    }
}
