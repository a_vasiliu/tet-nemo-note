package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.controller.payload.response.PaginationStats;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.settings.ExportNotificationTypeEntity;
import com.doing.nemo.claims.repository.ExportNotificationTypeRepository;
import com.doing.nemo.claims.repository.ExportNotificationTypeRepositoryV1;
import com.doing.nemo.claims.service.ExportNotificationTypeService;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ExportNotificationTypeServiceImpl implements ExportNotificationTypeService {
    private static Logger LOGGER = LoggerFactory.getLogger(ExportNotificationTypeServiceImpl.class);
    @Autowired
    private ExportNotificationTypeRepository exportNotificationTypeRepository;
    @Autowired
    private ExportNotificationTypeRepositoryV1 exportNotificationTypeRepositoryV1;

    @Override
    public ExportNotificationTypeEntity saveExportNotificationType(ExportNotificationTypeEntity exportNotificationTypeEntity) {

        if (exportNotificationTypeEntity != null) {
            List<ExportNotificationTypeEntity> exportNotificationTypeEntities =exportNotificationTypeRepository.checkDuplicateExportNotificationType(exportNotificationTypeEntity.getNotificationType().getDescription());
            if (exportNotificationTypeEntities != null && !exportNotificationTypeEntities.isEmpty()) {
                LOGGER.debug(MessageCode.CLAIMS_1069.value());
                throw new BadRequestException(MessageCode.CLAIMS_1069);
            }

            exportNotificationTypeRepository.save(exportNotificationTypeEntity);
        }
        return exportNotificationTypeEntity;
    }

    @Override
    public ExportNotificationTypeEntity updateExportNotificationType(ExportNotificationTypeEntity exportNotificationTypeEntity) {

        if (exportNotificationTypeEntity != null)
            exportNotificationTypeRepository.save(exportNotificationTypeEntity);

        return exportNotificationTypeEntity;
    }

    @Override
    public void deleteExportNotificationType(UUID uuid) {
        Optional<ExportNotificationTypeEntity> exportNotificationTypeEntityOptional = exportNotificationTypeRepository.findById(uuid);
        if (!exportNotificationTypeEntityOptional.isPresent()) {
            LOGGER.debug("Export Notification TypeEnum with id " + uuid + " not found");
            throw new NotFoundException("Export Notification TypeEnum with id " + uuid + " not found", MessageCode.CLAIMS_1010);
        }
        exportNotificationTypeRepository.delete(exportNotificationTypeEntityOptional.get());
    }

    @Override
    public ExportNotificationTypeEntity getExportNotificationType(UUID uuid) {
        Optional<ExportNotificationTypeEntity> exportNotificationTypeEntityOptional = exportNotificationTypeRepository.findById(uuid);
        if (!exportNotificationTypeEntityOptional.isPresent()) {
            LOGGER.debug("Export Notification TypeEnum with id " + uuid + " not found");
            throw new NotFoundException("Export Notification TypeEnum with id " + uuid + " not found", MessageCode.CLAIMS_1010);
        }
        return exportNotificationTypeEntityOptional.get();
    }

    @Override
    public List<ExportNotificationTypeEntity> getAllExportNotificationType() {
        List<ExportNotificationTypeEntity> exportNotificationTypeEntityList = exportNotificationTypeRepository.findAll();
        return exportNotificationTypeEntityList;
    }

    @Override
    public ExportNotificationTypeEntity patchActive(UUID uuid) {
        ExportNotificationTypeEntity exportNotificationTypeEntity = getExportNotificationType(uuid);
        if (exportNotificationTypeEntity.getActive()) {
            exportNotificationTypeEntity.setActive(false);
        } else exportNotificationTypeEntity.setActive(true);

        exportNotificationTypeRepository.save(exportNotificationTypeEntity);
        return exportNotificationTypeEntity;
    }

    @Override
    public Pagination<ExportNotificationTypeEntity> findExportNotificationType(Integer page, Integer pageSize, String orderBy, Boolean asc, String descriptionNotification, String code, String description, Boolean isActive) {
        Pagination<ExportNotificationTypeEntity> exportNotificationTypeEntityPagination = new Pagination<>();
        List<ExportNotificationTypeEntity> exportNotificationTypeEntityList = exportNotificationTypeRepositoryV1.findExportNotificationType(page, pageSize, orderBy, asc, descriptionNotification, code, description, isActive);
        Long itemCount = exportNotificationTypeRepositoryV1.countPaginationExportNotificationType(descriptionNotification, code, description, isActive);
        exportNotificationTypeEntityPagination.setItems(exportNotificationTypeEntityList);
        exportNotificationTypeEntityPagination.setStats(new PaginationStats(itemCount.intValue(), page, pageSize));

        return exportNotificationTypeEntityPagination;
    }

}
