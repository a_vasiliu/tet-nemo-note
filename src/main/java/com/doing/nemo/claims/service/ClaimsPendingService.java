package com.doing.nemo.claims.service;

import com.doing.nemo.claims.controller.payload.response.PersonalDataResponseV1;
import com.doing.nemo.claims.entity.settings.ContractEntity;
import com.doing.nemo.claims.entity.settings.CustomTypeRiskEntity;
import com.doing.nemo.claims.entity.settings.InsurancePolicyPersonalDataEntity;
import com.doing.nemo.claims.entity.settings.PersonalDataEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public interface ClaimsPendingService {
    Boolean checkIfExistsPendingWorking(String plate, String dateAccident);
    Boolean checkIfExistsPendingWorking(Long practiceId);
}
