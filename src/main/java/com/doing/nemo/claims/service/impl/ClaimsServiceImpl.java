package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.adapter.*;
import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.common.util.ServiceUtil;
import com.doing.nemo.claims.controller.payload.request.*;
import com.doing.nemo.claims.controller.payload.request.claims.DamagedRequest;
import com.doing.nemo.claims.controller.payload.request.messaging.Identity;
import com.doing.nemo.claims.controller.payload.request.refund.SplitRequest;
import com.doing.nemo.claims.controller.payload.response.*;
import com.doing.nemo.claims.adapter.OctoAdapter;
import com.doing.nemo.claims.crashservices.octo.OctoClient;
import com.doing.nemo.claims.adapter.TexaAdapter;
import com.doing.nemo.claims.crashservices.texa.org.datacontract.schemas._2004._07.texa_tmd_bol_m2mtrack_ald_webservice_service.GetVoucherInfoRequest;
import com.doing.nemo.claims.crashservices.texa.org.datacontract.schemas._2004._07.texa_tmd_bol_m2mtrack_ald_webservice_service.GetVoucherInfoResponse;
import com.doing.nemo.claims.crashservices.texa.org.tempuri.CrashService;
import com.doing.nemo.claims.crashservices.texa.org.tempuri.ICrashService;
import com.doing.nemo.claims.dto.*;
import com.doing.nemo.claims.entity.*;
import com.doing.nemo.claims.entity.claims.*;
import com.doing.nemo.claims.entity.enumerated.*;
import com.doing.nemo.claims.entity.enumerated.AuthorityEnum.AuthorityEventTypeEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsFlowEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsHistoricalUserEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.EntrustedEnum.EntrustedEnum;
import com.doing.nemo.claims.entity.enumerated.DTO.ClaimsStats;
import com.doing.nemo.claims.entity.enumerated.DamagedEnum.VehicleEnum.VehicleTypeEnum;
import com.doing.nemo.claims.entity.enumerated.DamagedEnum.WoundedEnum.WoundedTypeEnum;
import com.doing.nemo.claims.entity.enumerated.PolicyEnum.PolicyTypeEnum;
import com.doing.nemo.claims.entity.jsonb.*;
import com.doing.nemo.claims.entity.jsonb.cai.Cai;
import com.doing.nemo.claims.entity.jsonb.cai.CaiDetails;
import com.doing.nemo.claims.entity.jsonb.complaint.Complaint;
import com.doing.nemo.claims.entity.jsonb.complaint.jsonbComplaint.Entrusted;
import com.doing.nemo.claims.entity.jsonb.complaint.jsonbComplaint.jsonbDataAccidents.DataAccident;
import com.doing.nemo.claims.entity.jsonb.damaged.Damaged;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.Contract;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.Customer;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.InsuranceCompany;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbAntiTheftService.AntiTheftRequest;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbAntiTheftService.AntiTheftService;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbAntiTheftService.Registry;
import com.doing.nemo.claims.entity.jsonb.forms.jsonbForms.Attachment;
import com.doing.nemo.claims.entity.jsonb.refund.Refund;
import com.doing.nemo.claims.entity.jsonb.refund.Split;
import com.doing.nemo.claims.entity.settings.*;
import com.doing.nemo.claims.entity.view.SearchDashboardClaimsView;
import com.doing.nemo.claims.repository.*;
import com.doing.nemo.claims.repository.specification.SearchDashboardClaimsViewSpecification;
import com.doing.nemo.claims.service.*;
import com.doing.nemo.claims.util.Util;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.BadParametersException;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.exception.InternalException;
import com.doing.nemo.commons.exception.NotFoundException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import java.io.IOException;
import java.math.BigInteger;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.util.*;

@Service
public class ClaimsServiceImpl implements ClaimsService {

    @Autowired
    private IncidentService incidentService;

    @Autowired
    private IncidentAdapter incidentAdapter;

    @Autowired
    private DwhClaimsService dwhClaimsService;

    @Autowired
    private NoteRepository noteRepository;

    @Autowired
    private ClaimsPendingService claimsPendingService;

    @Value("${dwh.call}")
    private Boolean dwhCall;

    @Value("${skip.migrated.claims.automatic.entrust}")
    private Boolean skipMigratedClaimsAutomaticEntrust;
    @Value("${csv.export.max.rows}")
    private Integer csvMaxRows;

    /************ For Dashboard Table of Claims *****************/
    @Value("#{${claims.orderBy.fields}}")
    private Map<String, String> supportedOrderByFields;

    @Value("${claims.orderBy.default}")
    private String defaultOrderByField;

    @Value("${claims.search.defaultPage}")
    private Integer defaultPage;

    @Value("${claims.search.defaultPageSize}")
    private Integer defaultPageSize;

    @Value("${claims.search.defaultASCOrder}")
    private Boolean defaultASCOrder;

    @Value("${claims.search.prefilter.gg}")
    private Integer defaultPrefilterGG;

    @Value("#{${claims.orderBy.defaultSortToAppend}}")
    private Map<String, String> defaultSortToAppend;

    @Autowired
    private CacheService cacheService;

    @Autowired
    private OctoClient octoClient;

    @Autowired
    private ServiceUtil serviceUtil;

    @Autowired
    private RedisKeyGeneratorService redisKeyGenerator;

    @Autowired
    private ExternalCommunicationService externalCommunicationService;

    private static final String CACHENAME = "claims.";
    private static final String STATSkEY = "claimsStats::";
    private static final String STATSEVIDENCE = "evidenceStats::";
    private static final String STATSTHEFT = "theftStats::";
    private static final String STATSSTATUS = "statusStats::";
    private static final String COUNTERPARTYKEY = "counterpartyStats::";

    public static final String REDIS_KEY_IN_EVIDENCE = "evidence";
    public static final String REDIS_KEY_WITH_CONTINUATION = "withContinuation";
    public static final String REDIS_KEY_NULL = "Null";
    public static final String REDIS_KEY_TRUE = "True";
    public static final String REDIS_KEY_FALSE = "False";
    public static final String SEPARATOR = ".";
    public static final String EOL = "::";

    //private static String soapEndPointUrlOcto = "https://www.octotelematics.it/DossierCrashAld";

    //private static String soapEndPointUrlLojack = "https://txmp-wa-001-wsald.azurewebsites.net/CrashService.svc";

    private static Logger LOGGER = LoggerFactory.getLogger(ClaimsServiceImpl.class);
    private Set<DataAccidentTypeAccidentEnum> activeSet = new HashSet<DataAccidentTypeAccidentEnum>() {{
        add(DataAccidentTypeAccidentEnum.RC_ATTIVA);
        add(DataAccidentTypeAccidentEnum.RC_CONCORSUALE);
        add(DataAccidentTypeAccidentEnum.CARD_ATTIVA_FIRMA_SINGOLA);
        add(DataAccidentTypeAccidentEnum.CARD_CONCORSUALE_FIRMA_SINGOLA);
        add(DataAccidentTypeAccidentEnum.CARD_ATTIVA_DOPPIA_FIRMA);
        add(DataAccidentTypeAccidentEnum.CARD_CONCORSUALE_DOPPIA_FIRMA);
    }};
    private Set<DataAccidentTypeAccidentEnum> passiveSet = new HashSet<DataAccidentTypeAccidentEnum>() {{
        add(DataAccidentTypeAccidentEnum.RC_PASSIVA);
        add(DataAccidentTypeAccidentEnum.CARD_PASSIVA_FIRMA_SINGOLA);
        add(DataAccidentTypeAccidentEnum.CARD_PASSIVA_DOPPIA_FIRMA);
    }};
    private Set<DataAccidentTypeAccidentEnum> theftSet = new HashSet<DataAccidentTypeAccidentEnum>() {{
        add(DataAccidentTypeAccidentEnum.TENTATO_FURTO);
        add(DataAccidentTypeAccidentEnum.FURTO_PARZIALE);
        //add(DataAccidentTypeAccidentEnum.FURTO_E_RITROVAMENTO);
        //add(DataAccidentTypeAccidentEnum.FURTO_TOTALE);
    }};
    private Set<DataAccidentTypeAccidentEnum> cardSet = new HashSet<DataAccidentTypeAccidentEnum>() {{
    /*    add(DataAccidentTypeAccidentEnum.CARD_PASSIVA_FIRMA_SINGOLA);
        add(DataAccidentTypeAccidentEnum.CARD_PASSIVA_DOPPIA_FIRMA);*/
        add(DataAccidentTypeAccidentEnum.CARD_ATTIVA_FIRMA_SINGOLA);
        add(DataAccidentTypeAccidentEnum.CARD_CONCORSUALE_FIRMA_SINGOLA);
        add(DataAccidentTypeAccidentEnum.CARD_ATTIVA_DOPPIA_FIRMA);
        add(DataAccidentTypeAccidentEnum.CARD_CONCORSUALE_DOPPIA_FIRMA);
    }};
    private static Map<TypeAccidentForClosureEnum, Pair> typeMap = new HashMap<TypeAccidentForClosureEnum, Pair>() {{
        put(TypeAccidentForClosureEnum.COMPETITION, new Pair(true, true));
        put(TypeAccidentForClosureEnum.NOT_VERIFIABLE, new Pair(false, false));
        put(TypeAccidentForClosureEnum.FUL_WITHOUT_COUNTERPARTY, new Pair(false, false));
        put(TypeAccidentForClosureEnum.FUL_ACTIVE, new Pair(true, true));
        put(TypeAccidentForClosureEnum.FUL_PASSIVE, new Pair(false, false));
        put(TypeAccidentForClosureEnum.FNI_WITHOUT_COUNTERPARTY, new Pair(false, true));
        put(TypeAccidentForClosureEnum.FNI_ACTIVE, new Pair(true, true));
        put(TypeAccidentForClosureEnum.FNI_PASSIVE, new Pair(false, true));
        put(TypeAccidentForClosureEnum.FCM_WITHOUT_COUNTERPARTY, new Pair(true, true));
        put(TypeAccidentForClosureEnum.FCM_ACTIVE_PASSIVE, new Pair(true, true));
    }};
    private static Map<TypeAccidentForClosureEnum, Pair> typeMapFNI = new HashMap<TypeAccidentForClosureEnum, Pair>() {{
        put(TypeAccidentForClosureEnum.FNI_WITHOUT_COUNTERPARTY, new Pair(true, true));
        put(TypeAccidentForClosureEnum.FNI_ACTIVE, new Pair(true, true));
        put(TypeAccidentForClosureEnum.FNI_PASSIVE, new Pair(true, true));
    }};
    private static Map<ClaimsStatusEnum, Pair> statusMap = new HashMap<ClaimsStatusEnum, Pair>() {{
        put(ClaimsStatusEnum.DRAFT, new Pair(false, false));
        put(ClaimsStatusEnum.WAITING_FOR_VALIDATION, new Pair(false, false));
        put(ClaimsStatusEnum.WAITING_FOR_AUTHORITY, new Pair(true, true));
        put(ClaimsStatusEnum.TO_ENTRUST, new Pair(true, true));
        put(ClaimsStatusEnum.WAITING_FOR_REFUND, new Pair(true, true));
        put(ClaimsStatusEnum.CLOSED_TOTAL_REFUND, new Pair(false, false));
        put(ClaimsStatusEnum.DELETED, new Pair(false, false));
        put(ClaimsStatusEnum.REJECTED, new Pair(false, false));
        put(ClaimsStatusEnum.INCOMPLETE, new Pair(true, true));
        put(ClaimsStatusEnum.CLOSED_PARTIAL_REFUND, new Pair(true, true));
    }};
    private static String[][] caiRules = {
            {"NC", "T", "R", "R", "R", "R", "R", "NC", "R", "NC", "R", "R", "NC", "NC", "R", "R", "T", "R"},
            {"R", "NC", "R", "R", "R", "R", "R", "R", "R", "R", "R", "R", "R", "R", "R", "R", "R", "R"},
            {"T", "T", "C", "C", "C", "C", "R", "R", "C", "T", "T", "T", "T", "T", "R", "R", "T", "C"},
            {"T", "T", "C", "C", "C", "C", "T", "T", "R", "NC", "NC", "NC", "T", "T", "R", "R", "NC", "R"},
            {"T", "T", "C", "C", "C", "C", "T", "T", "C", "T", "T", "T", "T", "T", "C", "R", "T", "C"},
            {"T", "T", "C", "C", "C", "C", "T", "T", "R", "T", "NC", "NC", "T", "NC", "R", "R", "T", "R"},
            {"T", "T", "T", "R", "R", "R", "C", "T", "R", "NC", "R", "NC", "R", "NC", "R", "R", "T", "R"},
            {"NC", "T", "T", "R", "R", "R", "R", "C", "R", "C", "R", "R", "R", "R", "R", "R", "T", "R"},
            {"T", "T", "C", "T", "C", "T", "T", "T", "NC", "NC", "C", "T", "T", "T", "R", "NC", "T", "C"},
            {"NC", "T", "R", "NC", "R", "R", "NC", "C", "NC", "C", "R", "R", "R", "R", "R", "R", "NC", "R"},
            {"T", "T", "R", "NC", "R", "NC", "T", "T", "C", "T", "C", "T", "T", "T", "C", "R", "T", "R"},
            {"T", "T", "R", "NC", "R", "NC", "NC", "T", "R", "T", "R", "C", "T", "C", "R", "R", "T", "C"},
            {"NC", "T", "R", "R", "R", "R", "T", "T", "R", "T", "R", "R", "C", "NC", "R", "R", "T", "R",},
            {"NC", "T", "R", "R", "R", "NC", "NC", "T", "R", "T", "R", "C", "NC", "C", "R", "R", "T", "R",},
            {"T", "T", "T", "T", "C", "T", "T", "T", "T", "T", "C", "T", "T", "T", "C", "C", "T", "C",},
            {"T", "T", "T", "T", "T", "T", "T", "T", "NC", "T", "T", "T", "T", "T", "C", "C", "T", "C",},
            {"R", "T", "R", "NC", "R", "R", "R", "R", "R", "NC", "R", "R", "R", "R", "R", "R", "C", "R",},
            {"T", "T", "C", "T", "C", "T", "T", "T", "C", "T", "T", "C", "T", "T", "C", "C", "T", "C",}
    };

    private static final String PLACEHOLDER_ID = "<%IDCOMPLAINT%>";


    @Value("${octo.company.code}")
    private String octoCompanyCode;
    @Value("${octo.request.type}")
    private String octoRequestType;
    @Value("${octo.user}")
    private String octoUser;
    @Value("${octo.pwd}")
    private String octoPwd;

    @Value("${texa.user}")
    private String texaUser;
    @Value("${texa.pwd}")
    private String texaPwd;
    @Value("${texa.endpoint}")
    private String texaEndPoint;
    @Value("${texa.company.code}")
    private String texaCompanyCode;
    @Value("${texa.request.type}")
    private String texaRequestType;


    @Value("${legal.aon}")
    private String legalAon;
    @Value("${legal.msa}")
    private String legalMsa;
    @Value("${company.sogessur}")
    private String companySogessur;

    @Value("${company.no.system}")
    private String noCompany;

    @Autowired
    private ContractService contractService;
    @Autowired
    private LegalService legalService;
    @Autowired
    private InspectorateService inspectorateService;

    @Autowired
    private Util util;
    @Autowired
    private AntiTheftServiceRepository antiTheftServiceRepository;
    @Autowired
    private ClaimsRepository claimsRepository;
    @Autowired
    private ClaimsRepositoryV1 claimsRepositoryV1;
    @Autowired
    private CounterpartyAdapter counterpartyAdapter;
    @Autowired
    private InsurancePolicyRepository insurancePolicyRepository;
    @Autowired
    private InsuranceCompanyRepository insuranceCompanyRepository;
    @Autowired
    private FileManagerService fileManagerService;
    @Autowired
    private MessagingService messagingService;
    @Autowired
    private ESBService esbService;

    @Autowired
    private RecoverabilityService recoverabilityService;

    @Autowired
    private AntiTheftRequestService antiTheftRequestService;

    @Autowired
    private ClaimsNewRepository claimsNewRepository;

    @Autowired
    private SearchDashboardClaimsViewRepository searchDashboardClaimsViewRepository;

    @Autowired
    private CsvBuilderService csvBuilderService;
    @Autowired
    private GoLiveStrategyService goLiveStrategyService;

    @Autowired
    private ConverterClaimsService converterClaimsService;
    @Autowired
    private ClaimsViewSearchAdapter claimsViewSearchAdapter;


    @Autowired
    private LegalRepository legalRepository;
    private FlowStatusAssociation waiting_for_validation = new FlowStatusAssociation(ClaimsStatusEnum.WAITING_FOR_VALIDATION, ClaimsStatusEnum.WAITING_FOR_VALIDATION, ClaimsStatusEnum.WAITING_FOR_VALIDATION);
    private FlowStatusAssociation incomplete = new FlowStatusAssociation(ClaimsStatusEnum.INCOMPLETE, ClaimsStatusEnum.INCOMPLETE, ClaimsStatusEnum.INCOMPLETE);
    private FlowStatusAssociation rejected = new FlowStatusAssociation(ClaimsStatusEnum.REJECTED, ClaimsStatusEnum.REJECTED, ClaimsStatusEnum.REJECTED);
    private FlowStatusAssociation waiting_for_authority = new FlowStatusAssociation(ClaimsStatusEnum.WAITING_FOR_AUTHORITY, ClaimsStatusEnum.WAITING_FOR_AUTHORITY, ClaimsStatusEnum.WAITING_FOR_AUTHORITY);
    private FlowStatusAssociation closed_total_refund = new FlowStatusAssociation(ClaimsStatusEnum.CLOSED_TOTAL_REFUND, ClaimsStatusEnum.CLOSED_TOTAL_REFUND, ClaimsStatusEnum.CLOSED_TOTAL_REFUND);
    private FlowStatusAssociation deleted = new FlowStatusAssociation(ClaimsStatusEnum.DELETED, ClaimsStatusEnum.DELETED, ClaimsStatusEnum.DELETED);
    private FlowStatusAssociation managed = new FlowStatusAssociation(ClaimsStatusEnum.MANAGED, ClaimsStatusEnum.WAITING_FOR_AUTHORITY, ClaimsStatusEnum.WAITING_FOR_AUTHORITY);
    private FlowStatusAssociation closed = new FlowStatusAssociation(ClaimsStatusEnum.CLOSED, ClaimsStatusEnum.CLOSED, ClaimsStatusEnum.CLOSED);
    private FlowStatusAssociation to_entrust = new FlowStatusAssociation(ClaimsStatusEnum.TO_ENTRUST, ClaimsStatusEnum.TO_ENTRUST, ClaimsStatusEnum.TO_SEND_TO_CUSTOMER);
    private FlowStatusAssociation waiting_for_refund = new FlowStatusAssociation(ClaimsStatusEnum.WAITING_FOR_REFUND, ClaimsStatusEnum.WAITING_FOR_REFUND, ClaimsStatusEnum.TO_SEND_TO_CUSTOMER);
    private FlowStatusAssociation closed_partial_refund = new FlowStatusAssociation(ClaimsStatusEnum.CLOSED_PARTIAL_REFUND, ClaimsStatusEnum.CLOSED_PARTIAL_REFUND, ClaimsStatusEnum.TO_SEND_TO_CUSTOMER);
    //fare i controlli che ci siano l'ok authority e il num sx per FUL , se il controllo è positivo si va in to entrust
    private FlowStatusAssociation to_send_to_customer = new FlowStatusAssociation(ClaimsStatusEnum.WAITING_FOR_AUTHORITY, ClaimsStatusEnum.TO_ENTRUST, ClaimsStatusEnum.TO_SEND_TO_CUSTOMER);
    private Map<ClaimsStatusEnum, FlowStatusAssociation> flowStatusAssociationMap = new HashMap<ClaimsStatusEnum, FlowStatusAssociation>() {{
        put(ClaimsStatusEnum.WAITING_FOR_VALIDATION, waiting_for_validation);
        put(ClaimsStatusEnum.INCOMPLETE, incomplete);
        put(ClaimsStatusEnum.REJECTED, rejected);
        put(ClaimsStatusEnum.WAITING_FOR_AUTHORITY, waiting_for_authority);
        put(ClaimsStatusEnum.CLOSED_TOTAL_REFUND, closed_total_refund);
        put(ClaimsStatusEnum.DELETED, deleted);
        put(ClaimsStatusEnum.MANAGED, managed);
        put(ClaimsStatusEnum.CLOSED, closed);
        put(ClaimsStatusEnum.TO_ENTRUST, to_entrust);
        put(ClaimsStatusEnum.WAITING_FOR_REFUND, waiting_for_refund);
        put(ClaimsStatusEnum.CLOSED_PARTIAL_REFUND, closed_partial_refund);
        put(ClaimsStatusEnum.TO_SEND_TO_CUSTOMER, to_send_to_customer);
    }};


    public static boolean isAuthorityAndSxNumber(ClaimsEntity claimsEntity) {
        boolean isAuthority = false;
        boolean isSxNumber = false;
        boolean isRejected = false;

        if (claimsEntity.getAuthorities() != null && !claimsEntity.getAuthorities().isEmpty()) {
            Iterator<Authority> authIt = claimsEntity.getAuthorities().iterator();

            while (authIt.hasNext() && !isAuthority && !isRejected) {
                Authority authority = authIt.next();
                if (authority.getInvoiced()) {
                    isAuthority = true;
                }
                if (authority.getEventType() != null && authority.getEventType().equals(AuthorityEventTypeEnum.REJECT)) {
                    isRejected = true;
                }
            }
        }

        if (claimsEntity.getType().equals(ClaimsFlowEnum.FUL) && claimsEntity.getComplaint() != null &&
                claimsEntity.getComplaint().getFromCompany() != null &&
                claimsEntity.getComplaint().getFromCompany().getNumberSx() != null &&
                !claimsEntity.getComplaint().getFromCompany().getNumberSx().equals("")
        ) {
            isSxNumber = true;
        }

        if ((isAuthority || isRejected) && ((claimsEntity.getType().equals(ClaimsFlowEnum.FUL) && isSxNumber) || !claimsEntity.getType().equals(ClaimsFlowEnum.FUL))) {
            return true;
        } else {
            return false;
        }
    }


    @Override
    public Map<String, Object> getFlowDetailsByCai(Cai cai, String contractType, Damaged damaged, List<CounterpartyEntity> counterpartyList, Boolean isCaiSignedA, Boolean isCaiSignedB) {
        DataAccidentTypeAccidentEnum claimsType = this.caiCheck(cai.getVehicleA(), cai.getVehicleB(), damaged, counterpartyList, isCaiSignedA, isCaiSignedB);
        ClaimsFlowEnum claimsFlowEnum = contractService.getPersonalRiskFlowType(contractType, claimsType,
                damaged.getCustomer().getCustomerId());

        Map<String, Object> result = new HashMap<>();
        result.put("type", claimsType);
        result.put("flow", claimsFlowEnum);
        return result;
    }

    public DataAccidentTypeAccidentEnum caiCheck(CaiDetails caiA, CaiDetails caiB, Damaged damaged, List<CounterpartyEntity> counterpartyList, Boolean isCaiSignedA, Boolean isCaiSignedB) {

        Boolean isCard = this.checkIsCard(damaged, counterpartyList);
        if (isCard == null) {
            isCard = false;
        }

        int risA = getCaiConditionSelected(caiA);
        if (risA == -1) {
            if (isCard) {
                if (isCaiSignedA && isCaiSignedB) {
                    return DataAccidentTypeAccidentEnum.CARD_NON_VERIFICABILE_DOPPIA_FIRMA;
                } else {
                    return DataAccidentTypeAccidentEnum.CARD_NON_VERIFICABILE_FIRMA_SINGOLA;
                }
            } else {
                return DataAccidentTypeAccidentEnum.RC_NON_VERIFICABILE;
            }
        }
        int risB = getCaiConditionSelected(caiB);
        if (risB == -1) {
            if (isCard) {
                if (isCaiSignedA && isCaiSignedB) {
                    return DataAccidentTypeAccidentEnum.CARD_NON_VERIFICABILE_DOPPIA_FIRMA;
                } else {
                    return DataAccidentTypeAccidentEnum.CARD_NON_VERIFICABILE_FIRMA_SINGOLA;
                }
            } else {
                return DataAccidentTypeAccidentEnum.RC_NON_VERIFICABILE;
            }
        }
        switch (caiRules[risA][risB]) {
            case "T":
                if (isCard) {
                    if (isCaiSignedA && isCaiSignedB) {
                        return DataAccidentTypeAccidentEnum.CARD_PASSIVA_DOPPIA_FIRMA;
                    } else {
                        return DataAccidentTypeAccidentEnum.CARD_PASSIVA_FIRMA_SINGOLA;
                    }
                } else {
                    return DataAccidentTypeAccidentEnum.RC_PASSIVA;
                }
            case "R":
                if (isCard) {
                    if (isCaiSignedA && isCaiSignedB) {
                        return DataAccidentTypeAccidentEnum.CARD_ATTIVA_DOPPIA_FIRMA;
                    } else {
                        return DataAccidentTypeAccidentEnum.CARD_ATTIVA_FIRMA_SINGOLA;
                    }
                } else {
                    return DataAccidentTypeAccidentEnum.RC_ATTIVA;
                }
            case "C":
                if (isCard) {
                    if (isCaiSignedA && isCaiSignedB) {
                        return DataAccidentTypeAccidentEnum.CARD_CONCORSUALE_DOPPIA_FIRMA;
                    } else {
                        return DataAccidentTypeAccidentEnum.CARD_CONCORSUALE_FIRMA_SINGOLA;
                    }
                } else {
                    return DataAccidentTypeAccidentEnum.RC_CONCORSUALE;
                }
            case "NC":
                if (isCard) {
                    if (isCaiSignedA && isCaiSignedB) {
                        return DataAccidentTypeAccidentEnum.CARD_NON_VERIFICABILE_DOPPIA_FIRMA;
                    } else {
                        return DataAccidentTypeAccidentEnum.CARD_NON_VERIFICABILE_FIRMA_SINGOLA;
                    }
                } else {
                    return DataAccidentTypeAccidentEnum.RC_NON_VERIFICABILE;
                }
            default:
                LOGGER.debug(MessageCode.CLAIMS_1116.value());
                throw new BadRequestException(MessageCode.CLAIMS_1116);
        }
    }

    public static int getCaiConditionSelected(CaiDetails caiDetails) {
        int ris = 0;
        if (caiDetails.getCondition1().getValue())
            ris = 1;
        if (caiDetails.getCondition2().getValue())
            if (ris == 0) ris = 2;
            else return -1;
        if (caiDetails.getCondition3().getValue())
            if (ris == 0) ris = 3;
            else return -1;
        if (caiDetails.getCondition4().getValue())
            if (ris == 0) ris = 4;
            else return -1;
        if (caiDetails.getCondition5().getValue())
            if (ris == 0) ris = 5;
            else return -1;
        if (caiDetails.getCondition6().getValue())
            if (ris == 0) ris = 6;
            else return -1;
        if (caiDetails.getCondition7().getValue())
            if (ris == 0) ris = 7;
            else return -1;
        if (caiDetails.getCondition8().getValue())
            if (ris == 0) ris = 8;
            else return -1;
        if (caiDetails.getCondition9().getValue())
            if (ris == 0) ris = 9;
            else return -1;
        if (caiDetails.getCondition10().getValue())
            if (ris == 0) ris = 10;
            else return -1;
        if (caiDetails.getCondition11().getValue())
            if (ris == 0) ris = 11;
            else return -1;
        if (caiDetails.getCondition12().getValue())
            if (ris == 0) ris = 12;
            else return -1;
        if (caiDetails.getCondition13().getValue())
            if (ris == 0) ris = 13;
            else return -1;
        if (caiDetails.getCondition14().getValue())
            if (ris == 0) ris = 14;
            else return -1;
        if (caiDetails.getCondition15().getValue())
            if (ris == 0) ris = 15;
            else return -1;
        if (caiDetails.getCondition16().getValue())
            if (ris == 0) ris = 16;
            else return -1;
        if (caiDetails.getCondition17().getValue())
            if (ris == 0) ris = 17;
            else return -1;
        return ris;
    }

    @Override
    public Pagination<ClaimsEntity> getClaimsPagination(Boolean inEvidence, Long practiceId, String flow, List<Long> clientIdList, String plate,
                                                        String status, String locator, String dateAccidentFrom,
                                                        String dateAccidentTo, String typeAccident, Boolean asc, String orderBy, int page, int pageSize, Boolean withContinuation, String company, Boolean withReceptions, String clientName, Boolean poVariation, Long fleetVehicleId) {
        try {

            List<String> flowsList = null;
            if (flow != null)
                flowsList = util.splitRequest(flow, ClaimsFlowEnum.class);

            List<String> statusList = null;
            if (status != null)
                statusList = util.splitRequest(status, ClaimsStatusEnum.class);

            List<String> typeAccidentList = null;
            if (typeAccident != null)
                typeAccidentList = util.splitRequest(typeAccident, DataAccidentTypeAccidentEnum.class);


            Pagination<ClaimsEntity> claimsPagination = new Pagination<>();


            List<ClaimsNewEntity> claimsNewEntitiesList = claimsNewRepository.findClaimsForPagination(inEvidence, practiceId, flowsList, clientIdList, plate, statusList, locator, dateAccidentFrom, dateAccidentTo, typeAccidentList, asc, orderBy, page, pageSize, withContinuation, company, withReceptions, clientName, poVariation, fleetVehicleId);
            List<ClaimsEntity> claimsList = converterClaimsService.wrapFromClaimsNewEntityListToClaimsEntityList(claimsNewEntitiesList);

            BigInteger itemCount = claimsNewRepository.countClaimsForPagination(inEvidence, practiceId, flowsList, clientIdList, plate, statusList, locator, dateAccidentFrom, dateAccidentTo, typeAccidentList, asc, orderBy, page, pageSize, withContinuation, company, withReceptions, clientName, poVariation, fleetVehicleId);
            //Mi serve necessariemante per calcolare la paina di start
            claimsPagination.setStats(new PaginationStats(itemCount.intValue(), page, pageSize));

            /*int start = claimsPagination.getStats().getStartPage();
            int count = 0;*/
            //List<ClaimsEntity> claimsForPagination = new LinkedList<>();

            /*while (count < claimsPagination.getStats().getPageSize() && ((start - 1) + count) < claimsList.size()) {
                claimsForPagination.add((claimsList.get((start - 1) + count)));
                count++;
            }*/

            claimsPagination.setItems(claimsList);

            return claimsPagination;

        } catch (BadParametersException ex) {
            LOGGER.debug(ex.getMessage());
            throw ex;
        } catch (BadRequestException ex) {
            LOGGER.debug("There are problems with the incoming Date_accident, the correct format is DD-MM-YYYY");
            throw new BadRequestException(MessageCode.CLAIMS_1011, ex);
        }
    }


    @Override
    public Pagination<ClaimsEntity> getClaimsPaginationWithoutDraftAndTheft(Boolean inEvidence, Long practiceId, String flow, List<Long> clientListId, String plate,
                                                                            String status, String locator, String dateAccidentFrom,
                                                                            String dateAccidentTo, String typeAccident, Boolean asc, String orderBy, int page, int pageSize, Boolean withContinuation, String company, String clientName, Boolean poVariation, Boolean waitingForNumberSx, Long fleetVehicleId, Boolean waitingForAuthority) {
        try {

            List<String> flowsList = null;
            if (flow != null)
                flowsList = util.splitRequest(flow, ClaimsFlowEnum.class);

            List<String> statusList = null;
            if (status != null)
                statusList = util.splitRequest(status, ClaimsStatusEnum.class);

            List<String> typeAccidentList = null;
            if (typeAccident != null)
                typeAccidentList = util.splitRequest(typeAccident, DataAccidentTypeAccidentEnum.class);

            Pagination<ClaimsEntity> claimsPagination = new Pagination<>();
            List<ClaimsNewEntity> claimsNewList = claimsNewRepository.findClaimsForPaginationWithoutDraftAndTheft(inEvidence, practiceId, flowsList, clientListId, plate, statusList, locator, dateAccidentFrom, dateAccidentTo, typeAccidentList, asc, orderBy, page, pageSize, withContinuation, company, clientName, poVariation, waitingForNumberSx, fleetVehicleId, waitingForAuthority);

            /*
             * Sostituisco la vecchia chiamata di wrap con il nuovo metodo che gestisce solo i campi mostrati nella tabella denunce
             *
             * Vecchia chiamata al wrap
             * List<ClaimsEntity> claimsList = converterClaimsService.wrapFromClaimsNewEntityListToClaimsEntityList(claimsNewList);
             */

            List<ClaimsEntity> claimsList = converterClaimsService.wrapFromClaimsComplaintOperatorNewEntityListToClaimsEntityList(claimsNewList);

            BigInteger itemCount = claimsNewRepository.countClaimsForPaginationWithoutDraftAndTheft(inEvidence, practiceId, flowsList, clientListId, plate, statusList, locator, dateAccidentFrom, dateAccidentTo, typeAccidentList, asc, orderBy, page, pageSize, withContinuation, company, clientName, poVariation, waitingForNumberSx, fleetVehicleId, waitingForAuthority);

            //Mi serve necessariemante per calcolare la paina di start
            claimsPagination.setStats(new PaginationStats(itemCount.intValue(), page, pageSize));

            /*int start = claimsPagination.getStats().getStartPage();
            int count = 0;*/
            //List<ClaimsEntity> claimsForPagination = new LinkedList<>();

            /*while (count < claimsPagination.getStats().getPageSize() && ((start - 1) + count) < claimsList.size()) {
                claimsForPagination.add((claimsList.get((start - 1) + count)));
                count++;
            }*/

            claimsPagination.setItems(claimsList);

            return claimsPagination;

        } catch (BadParametersException ex) {
            LOGGER.debug(ex.getMessage());
            throw ex;
        } catch (BadRequestException ex) {
            LOGGER.debug("There are problems with the incoming Date_accident, the correct format is DD-MM-YYYY");
            throw new BadRequestException(MessageCode.CLAIMS_1011, ex);
        }
    }

    @Override
    public ClaimsViewSearchDTO getClaimsViewPaginationWithoutDraftAndTheft(FilterView claimsRequestFilterViewDTO,
                                                                           Boolean asc, String orderBy, int page, int pageSize) {

 
        Pageable pageable = serviceUtil.buildPaginationWithFinalDefaultSort(page, pageSize, orderBy, asc,
                defaultPage, defaultPageSize, defaultOrderByField,
                defaultASCOrder, supportedOrderByFields, defaultSortToAppend
        );

        if (((claimsRequestFilterViewDTO != null && !claimsRequestFilterViewDTO.fieldsAreNull()) || (!claimsRequestFilterViewDTO.fieldsAreNull())) ){
            return (
                    this.containsPostFilters(claimsRequestFilterViewDTO)
                            ? this.retrievePageableClaimsWithPostFilter(claimsRequestFilterViewDTO,  pageable)
                            : this.retrievePageableClaimsWithPostFilter(claimsRequestFilterViewDTO,  pageable)
            );
        } else {
            return this.retrievePageableClaimsWithoutFilter(pageable);
        }
    }


    private boolean containsPostFilters(FilterView claimsRequestFilterViewDTO) {
        return true;
    }

    private ClaimsViewSearchDTO retrievePageableClaimsWithPostFilter(FilterView claimsRequestFilterViewDTO,  Pageable pageable) {


        Page<SearchDashboardClaimsView> paginationResult = this.retrievePaginatedSearchDashboardClaimsView(
                claimsRequestFilterViewDTO,  pageable);

        List<ClaimsSearchDTO> claimsDTOList = claimsViewSearchAdapter.adaptClaimsSearchViewToDTO(paginationResult.getContent());

        return this.buildSearchClaimsViewResponse(claimsDTOList, paginationResult);

    }

    private ClaimsViewSearchDTO retrievePageableClaimsWithoutFilter(Pageable pageable){

        FilterView preFilter = new FilterView();
        Instant nowInstant = DateUtil.getNowInstant();
        Instant nowInstantPreFilter = nowInstant.minus(defaultPrefilterGG, ChronoUnit.DAYS);
        preFilter.setCreatedAt(nowInstantPreFilter);
        //preFilter.setUpdateAt(nowInstant);

        return retrievePageableClaimsWithPostFilter(preFilter,pageable);

     /*   Page<SearchDashboardClaimsView>  paginationResult = searchDashboardClaimsViewRepository.findAll(pageable);

        List<ClaimsSearchDTO> claimsDTOList = claimsViewSearchAdapter.adaptClaimsSearchViewToDTO(paginationResult.getContent());

        return this.buildSearchClaimsViewResponse(claimsDTOList, paginationResult);*/

    }

    private ClaimsViewSearchDTO buildSearchClaimsViewResponse(List<ClaimsSearchDTO> claims, Page<SearchDashboardClaimsView> paginationResult){

        return new ClaimsViewSearchDTO(
                new StatsDTO(paginationResult.getNumber() + 1,
                        (int) paginationResult.getTotalElements(),
                        paginationResult.getSize(),
                        paginationResult.getTotalPages()),
                claims
        );
    }


    private Page<SearchDashboardClaimsView> retrievePaginatedSearchDashboardClaimsView(FilterView filter, Pageable pageable){

        Page<SearchDashboardClaimsView> paginationResult;
        SearchDashboardClaimsViewSpecification specSearchDashboardClaimsView = new SearchDashboardClaimsViewSpecification();

        Specification<SearchDashboardClaimsView> spec = null;
        boolean filterPresent= false;



        if (filter.getPlate()!=null&&!filter.getPlate().isEmpty()) {
            filterPresent=true;
            spec = Specification.where(specSearchDashboardClaimsView.getEqualPlate(filter.getPlate()));
        }

        if (filter.getCreatedAt()!=null){
            if (filterPresent){
                spec = spec.and(specSearchDashboardClaimsView.getPreFilter(filter.getCreatedAt()));
            }else{
                filterPresent=true;
                spec = Specification.where(specSearchDashboardClaimsView.getPreFilter(filter.getCreatedAt()));
            }
        }


        if (filter.getPracticeId()!=null){
            if (filterPresent){
                spec = spec.and(specSearchDashboardClaimsView.getEqualPracticeId(filter.getPracticeId()));
            }else{
                filterPresent=true;
                spec = Specification.where(specSearchDashboardClaimsView.getEqualPracticeId(filter.getPracticeId()));
            }
        }

        if  (filter.getInEvidence()!=null){
            if (filterPresent){
                spec =    spec.and(specSearchDashboardClaimsView.isInEvidence(filter.getInEvidence()));
            }else{
                filterPresent=true;
                spec = Specification.where(specSearchDashboardClaimsView.isInEvidence(filter.getInEvidence()));
            }
        }

        if  (filter.getPoVariation()!=null){
            if (filterPresent){
                spec =    spec.and(specSearchDashboardClaimsView.isPoVariation(filter.getPoVariation()));
            }else{
                filterPresent=true;
                spec = Specification.where(specSearchDashboardClaimsView.isPoVariation(filter.getPoVariation()));
            }
        }

        if  (filter.getDateAccidentFrom()!=null&&filter.getDateAccidentTo()==null){
            if (filterPresent){
                spec = spec.and(specSearchDashboardClaimsView.getGreaterThanOrEqualTo((filter.getDateAccidentFrom())));
            }else{
                filterPresent=true;
                spec = Specification.where(specSearchDashboardClaimsView.getGreaterThanOrEqualTo((filter.getDateAccidentFrom())));
            }
        }else if (filter.getDateAccidentFrom()==null&&filter.getDateAccidentTo()!=null){
            if (filterPresent){
                spec = spec.and(specSearchDashboardClaimsView.getLessThanOrEqualTo((filter.getDateAccidentTo())));
            }else{
                filterPresent=true;
                spec = Specification.where(specSearchDashboardClaimsView.getLessThanOrEqualTo((filter.getDateAccidentTo())));
            }
        }else if (filter.getDateAccidentFrom()!=null&&filter.getDateAccidentTo()!=null){
            if (filterPresent){
                spec = spec.and(specSearchDashboardClaimsView.getDateAccidentBetween(filter.getDateAccidentFrom(),filter.getDateAccidentTo()));
            }else{
                filterPresent=true;
                spec = Specification.where(specSearchDashboardClaimsView.getDateAccidentBetween(filter.getDateAccidentFrom(),filter.getDateAccidentTo()));
            }
        }

        if (filter.getWithContinuation()!=null){
            if (filterPresent){
                spec = spec.and(specSearchDashboardClaimsView.withContinuation(filter.getWithContinuation()));
            }else{
                filterPresent= true;
                spec= Specification.where(specSearchDashboardClaimsView.withContinuation(filter.getWithContinuation()));
            }
        }

    /*    if  (filter.getWithContinuation()!=null){
            if (filterPresent){
                spec =  spec.and(specSearchDashboardClaimsView.isContinuation(filter.getWithContinuation()));


            }else{
                filterPresent=true;
                spec = Specification.where(specSearchDashboardClaimsView.isContinuation(filter.getWithContinuation()));
            }
        }
*/
        if (filter.getStatus()!=null){
            if (filterPresent){
               spec = spec.and(specSearchDashboardClaimsView.statusIn(filter.getStatus()));
            }else {
                filterPresent=true;
                spec = Specification.where(specSearchDashboardClaimsView.statusIn(filter.getStatus()));
            }
        }

        if (filter.getFlow()!=null){
            if (filterPresent){
                spec = spec.and(specSearchDashboardClaimsView.typeFlowIn(filter.getFlow()));
            }else {
                filterPresent=true;
                spec = Specification.where(specSearchDashboardClaimsView.typeFlowIn(filter.getFlow()));
            }
        }

        if (filter.getTypeAccident()!=null){
            if (filterPresent){
                spec = spec.and(specSearchDashboardClaimsView.typeAccidentIn(filter.getTypeAccident()));
            }else {
                filterPresent=true;
                spec = Specification.where(specSearchDashboardClaimsView.typeAccidentIn(filter.getTypeAccident()));
            }
        }

      if (filter.getWaitingForNumberSx()!=null){



          if (filterPresent){
              spec = spec.and(specSearchDashboardClaimsView.waitingForNumberSx(filter.getWaitingForNumberSx()));
         }else{
              filterPresent=true;
              spec = Specification.where(specSearchDashboardClaimsView.waitingForNumberSx(filter.getWaitingForNumberSx()));
          }






      }

      if (filter.getWaitingForAuthority()!=null){
          if (filterPresent){
              spec = spec.and(specSearchDashboardClaimsView.waitingForAuthority(filter.getWaitingForAuthority()));
          }else{
              filterPresent=true;
              spec = Specification.where(specSearchDashboardClaimsView.waitingForAuthority(filter.getWaitingForAuthority()));
          }
      }

        paginationResult=  searchDashboardClaimsViewRepository.findAll(spec,pageable);
        return paginationResult;

    }

  /*  private ClaimsResponsePaginationV2 retrievePageableClaimsWithoutFilter(Pageable pageable) {

        Page<SearchDashboardClaimsView> paginationResult;

  *//*      if (CollectionUtils.isNotEmpty(commodityIds)) {
            List<String> centerIds = retrieveCenterIdsFromCommodities(permissionClient, commodityIds);
            genericServiceValidator.checkIsEmpty(centerIds, "centerIds");

            paginationResult = authorityDossierRepository.findDistinctIdByCommodityCenterIdInAndDeletedAtIsNull(centerIds, pageable);
        } else {

            boolean isAuthoritySupervisor = profileList.contains(genericUtil.retrieveProfileValueByGatewatProfilesMap(profiles));

            paginationResult = (isAuthoritySupervisor)
                    ? authorityDossierRepository.findDistinctIdByDeletedAtIsNull(pageable)
                    : authorityDossierRepositoryManager.findByUserId(userId, pageable);
        }

        List<AuthorityDossierDTO> dossierDTOList = authorityDossierEntityDTOAdapter.adaptDossierLiteToDTO(paginationResult.getContent());

        return this.buildSearchDossiersResponse(dossierDTOList, paginationResult);*//*
        return paginationResult;
    }*/





 /*   private ClaimsResponsePaginationV2 retrievePageableClaimsWithPreFilter(AuthorityDossierDTO authorityDossierDTO, List<String> commodityIds, String userId,
                                                                           GatewayProfilesMap profiles, SearchDossierWorkingCompleteView view, WorkingEntity workingEntityFilters,
                                                                           WorkingDashboardStatus workingDashboardStatus, Pageable pageable) {

        boolean isAssistanceCenter = CollectionUtils.isNotEmpty(commodityIds);

        Page<SearchDossierWorkingCompleteView> paginationResult = isAssistanceCenter
                ? this.retrieveACFilteredAndPaginatedWorkings(false, authorityDossierDTO, commodityIds, view, workingEntityFilters, workingDashboardStatus, pageable)
                : this.retrieveAdminFilteredAndPaginatedWorkings(false, authorityDossierDTO, userId, profiles, view, workingEntityFilters, workingDashboardStatus, pageable);

        List<AuthorityDossierDTO> dossierDTOList = authorityDossierCompleteViewEntityAdapter.adaptToDTO(paginationResult.getContent());

        return this.buildSearchDossiersResponse(dossierDTOList, paginationResult);
    }*/





    @Override
    public Pagination<ClaimsEntity> getClaimsPaginationTheftOperator(Boolean inEvidence, Long practiceId, String flow, String clientId, String plate,
                                                                     String status, String locator, String dateAccidentFrom,
                                                                     String dateAccidentTo, String typeAccident, Boolean asc, String orderBy, int page, int pageSize, Boolean withContinuation, String company, String clientName, Boolean poVariation, Boolean waitingForNumberSx, Long fleetVehicleId, Boolean waitingForAuthority) {
        try {

            List<String> flowsList = null;
            if (flow != null)
                flowsList = util.splitRequest(flow, ClaimsFlowEnum.class);

            List<String> statusList = null;
            if (status != null)
                statusList = util.splitRequest(status, ClaimsStatusEnum.class);

            List<String> typeAccidentList = null;
            if (typeAccident != null)
                typeAccidentList = util.splitRequest(typeAccident, DataAccidentTypeAccidentEnum.class);

            Pagination<ClaimsEntity> claimsPagination = new Pagination<>();
            List<ClaimsNewEntity> claimsNewList = claimsNewRepository.findClaimsForPaginationTheftOperator(inEvidence, practiceId, flowsList, clientId, plate, statusList, locator, dateAccidentFrom, dateAccidentTo, typeAccidentList, asc, orderBy, page, pageSize, withContinuation, company, clientName, poVariation, waitingForNumberSx, fleetVehicleId, waitingForAuthority);
            List<ClaimsEntity> claimsList = converterClaimsService.wrapFromClaimsNewEntityListToClaimsEntityList(claimsNewList);
            BigInteger itemCount = claimsNewRepository.countClaimsForPaginationTheftOperator(inEvidence, practiceId, flowsList, clientId, plate, statusList, locator, dateAccidentFrom, dateAccidentTo, typeAccidentList, asc, orderBy, page, pageSize, withContinuation, company, clientName, poVariation, waitingForNumberSx, fleetVehicleId, waitingForAuthority);
            //Mi serve necessariemante per calcolare la paina di start
            claimsPagination.setStats(new PaginationStats(itemCount.intValue(), page, pageSize));

            /*int start = claimsPagination.getStats().getStartPage();
            int count = 0;*/
            //List<ClaimsEntity> claimsForPagination = new LinkedList<>();

            /*while (count < claimsPagination.getStats().getPageSize() && ((start - 1) + count) < claimsList.size()) {
                claimsForPagination.add((claimsList.get((start - 1) + count)));
                count++;
            }*/

            claimsPagination.setItems(claimsList);

            return claimsPagination;

        } catch (BadParametersException ex) {
            LOGGER.debug(ex.getMessage());
            throw ex;
        } catch (BadRequestException ex) {
            LOGGER.debug("There are problems with the incoming Date_accident, the correct format is DD-MM-YYYY");
            throw new BadRequestException(MessageCode.CLAIMS_1011, ex);
        }
    }


    @Override
    public Pagination<ClaimsEntity> getClaimsPaginationOnlyTheft(Boolean inEvidence, Long practiceId, String flow, String clientId, String plate,
                                                                 String status, String locator, String dateAccidentFrom,
                                                                 String dateAccidentTo, Boolean asc, String orderBy, int page, int pageSize, Boolean withContinuation, String company, Boolean withReceptions, String clientName) {
        try {

            List<String> flowsList = null;
            if (flow != null)
                flowsList = util.splitRequest(flow, ClaimsFlowEnum.class);

            List<String> statusList = null;
            if (status != null)
                statusList = util.splitRequest(status, ClaimsStatusEnum.class);

            Pagination<ClaimsEntity> claimsPagination = new Pagination<>();
            List<ClaimsEntity> claimsList = claimsRepositoryV1.findClaimsForPaginationOnlyTheft(inEvidence, practiceId, flowsList, clientId, plate, statusList, locator, dateAccidentFrom, dateAccidentTo, asc, orderBy, page, pageSize, withContinuation, company, withReceptions, clientName);

            BigInteger itemCount = claimsRepositoryV1.countClaimsForPaginationOnlyTheft(inEvidence, practiceId, flowsList, clientId, plate, statusList, locator, dateAccidentFrom, dateAccidentTo, asc, orderBy, page, pageSize, withContinuation, company, withReceptions, clientName);
            //Mi serve necessariemante per calcolare la paina di start
            claimsPagination.setStats(new PaginationStats(itemCount.intValue(), page, pageSize));

            /*int start = claimsPagination.getStats().getStartPage();
            int count = 0;*/
            //List<ClaimsEntity> claimsForPagination = new LinkedList<>();

            /*while (count < claimsPagination.getStats().getPageSize() && ((start - 1) + count) < claimsList.size()) {
                claimsForPagination.add((claimsList.get((start - 1) + count)));
                count++;
            }*/

            claimsPagination.setItems(claimsList);

            return claimsPagination;

        } catch (BadParametersException ex) {
            LOGGER.debug(ex.getMessage());
            throw ex;
        } catch (BadRequestException ex) {
            LOGGER.debug("There are problems with the incoming Date_accident, the correct format is DD-MM-YYYY");
            throw new BadRequestException(MessageCode.CLAIMS_1011, ex);
        }
    }


    //REFACTOR
    @Override
    public Pagination<ClaimsEntity> getClaimsPaginationAuthority(List<String> plates, String chassis, int page, int pageSize,Boolean isFirstCheck) {
        try {

            Pagination<ClaimsEntity> claimsPagination = new Pagination<>();
            List<ClaimsNewEntity> claimsNewList = claimsNewRepository.findClaimsForPaginationAuthority(plates, chassis, page, pageSize, isFirstCheck);
            List<ClaimsEntity> claimsList = converterClaimsService.wrapFromClaimsNewEntityListToClaimsEntityList(claimsNewList);
            BigInteger itemCount = claimsNewRepository.countClaimsForPaginationAuthority(plates, chassis, isFirstCheck);


            //Mi serve necessariemante per calcolare la paina di start
            claimsPagination.setStats(new PaginationStats(itemCount.intValue(), page, pageSize));

            claimsPagination.setItems(claimsList);

            return claimsPagination;

        } catch (BadParametersException ex) {
            LOGGER.debug(ex.getMessage());
            throw ex;
        } catch (BadRequestException ex) {
            LOGGER.debug("There are problems with the incoming Date_accident, the correct format is DD-MM-YYYY");
            throw new BadRequestException(MessageCode.CLAIMS_1011, ex);
        }
    }

    /*public Pagination<CounterpartyResponseV1> getCounterpartyPagination(String denomination, String plate, Long practiceId, String statusRepair, String dateCreatedFrom,
                                                                        String dateCreatedTo, Boolean asc, String orderBy, int page, int pageSize, String addressSx, String fromCompany, String dateSxFrom, String dateSxTo, String region, String province, String lastContact, String assignedTo, String locality, String recall, String centerEmail) throws IOException {

        List<CounterpartyResponseV1> counterpartyList = new LinkedList<>();
        Pagination<CounterpartyResponseV1> counterpartyPagination = new Pagination<>();
        List<Object> counterpartyObj = claimsRepositoryV1.findCounterpartyForPagination(page, pageSize, denomination, plate, practiceId, statusRepair, dateCreatedFrom, dateCreatedTo, asc, orderBy, addressSx, fromCompany, dateSxFrom, dateSxTo, region, province, lastContact, assignedTo, locality, recall, centerEmail);

        for (Object currentObject : counterpartyObj) {

            CounterpartyResponseV1 counterpartyResponseV1 = CounterpartyAdapter.adptObjectToCounterpartyResponse(currentObject);
            counterpartyList.add(counterpartyResponseV1);
        }

        counterpartyPagination.setStats(new PaginationStats(counterpartyList.size(), page, pageSize));

        int start = counterpartyPagination.getStats().getStartPage();
        int count = 0;

        List<CounterpartyResponseV1> counterpartyForPagination = new LinkedList<>();

        while (count < counterpartyPagination.getStats().getPageSize() && ((start - 1) + count) < counterpartyList.size()) {
            counterpartyForPagination.add((counterpartyList.get((start - 1) + count)));
            count++;
        }

        counterpartyPagination.setItems(counterpartyForPagination);

        return counterpartyPagination;
    }*/


    public Stats createStatsEntity(List<Object[]> listFromQuery, List<Object[]> listFromQueryWaitingForAuthority, List<Object[]> listFromQueryWaitingForNumberSx, List<Object[]> listFromQueryPoValidation) {

        Stats stats = new Stats();

        //Conta le stats per tutti i claims tranne quelli con flusso ful ed in stato Waiting for Authority (per poter distinguero successivamente quelli in attesa di numero sinistro)
        for (Object[] currentObj : listFromQuery) {

            String flow = (String) currentObj[0];
            String status = (String) currentObj[1];
            BigInteger bigInteger = (BigInteger) currentObj[2];
            Integer counter = Integer.parseInt(bigInteger.toString());
            Boolean inEvidence = (Boolean) currentObj[3];
            Boolean withContinuation = (Boolean) currentObj[4];
            //Boolean poVariation = (Boolean) currentObj[5];


            stats = updateStatsCounter(stats, flow, status, counter, inEvidence, withContinuation);

                /*if (poVariation != null && poVariation)
                    stats = this.updateStatsCounter(stats, flow, "po_variation", counter, inEvidence, withContinuation);*/

        }

        //Conteggio pratiche in attesa di autorizzazione
        for (Object[] currentObj : listFromQueryWaitingForAuthority) {

            String flow = (String) currentObj[0];
            String status = (String) currentObj[1];
            BigInteger bigInteger = (BigInteger) currentObj[2];
            Integer counter = Integer.parseInt(bigInteger.toString());
            Boolean inEvidence = (Boolean) currentObj[3];
            Boolean withContinuation = (Boolean) currentObj[4];
            //Boolean poVariation = (Boolean) currentObj[5];

            stats = updateStatsCounter(stats, flow, status, counter, inEvidence, withContinuation);

            /*if (poVariation != null && poVariation)
                stats = this.updateStatsCounter(stats, flow, "po_variation", counter, inEvidence, withContinuation);*/

        }

        //Conteggio pratiche in attesa del numero sinistro
        for (Object[] currentObj : listFromQueryWaitingForNumberSx) {

            String flow = (String) currentObj[0];
            String status = (String) currentObj[1];
            BigInteger bigInteger = (BigInteger) currentObj[2];
            Integer counter = Integer.parseInt(bigInteger.toString());
            Boolean inEvidence = (Boolean) currentObj[3];
            Boolean withContinuation = (Boolean) currentObj[4];
            //Boolean poVariation = (Boolean) currentObj[5];

            stats = updateStatsCounter(stats, flow, "waiting_for_number_sx", counter, inEvidence, withContinuation);

            /*if (poVariation != null && poVariation)
                stats = this.updateStatsCounter(stats, flow, "po_variation", counter, inEvidence, withContinuation);*/

        }

        //Conteggio pratiche po_variation
        for (Object[] currentObj : listFromQueryPoValidation) {

            String flow = (String) currentObj[0];
            //String status = (String) currentObj[1];
            BigInteger bigInteger = (BigInteger) currentObj[1];
            Integer counter = Integer.parseInt(bigInteger.toString());
            Boolean inEvidence = (Boolean) currentObj[2];
            Boolean withContinuation = (Boolean) currentObj[3];
            //Boolean poVariation = (Boolean) currentObj[5];

            //stats = this.updateStatsCounter(stats, flow, "waiting_for_number_sx", counter, inEvidence, withContinuation);

            stats = updateStatsCounter(stats, flow, "po_variation", counter, inEvidence, withContinuation);

        }

        return stats;
    }

    private static Stats updateStatsCounter(Stats stats, String flow, String status, Integer counter, Boolean inEvidence, Boolean withContinuation) {

        ClaimsStatusEnum claimsStatus = ClaimsStatusEnum.create(status);

        Stats.StatsElement statsElement = stats.getStats(claimsStatus);

        if (statsElement == null) {

            statsElement = new Stats.StatsElement();
            statsElement.setClaimsStatus(ClaimsStatusEnum.create(status));
            statsElement.setFul(new Stats.InternalCounter());
            statsElement.setFni(new Stats.InternalCounter());
            statsElement.setFcm(new Stats.InternalCounter());
        }

        Stats.InternalCounter internalCounter = new Stats.InternalCounter();
        if (flow.equalsIgnoreCase(ClaimsFlowEnum.FUL.getValue())) {

            internalCounter = statsElement.getFul();
        } else if (flow.equalsIgnoreCase(ClaimsFlowEnum.FCM.getValue())) {

            internalCounter = statsElement.getFcm();
        } else if (flow.equalsIgnoreCase(ClaimsFlowEnum.FNI.getValue())) {

            internalCounter = statsElement.getFni();
        }

        internalCounter.setTotal(internalCounter.getTotal() + counter);

        if (inEvidence && withContinuation) {

            internalCounter.setWithContinuationInEvidence(internalCounter.getWithContinuationInEvidence() + counter);
            internalCounter.setInEvidence(internalCounter.getInEvidence() + counter);
            internalCounter.setWithContinuation(internalCounter.getWithContinuation() + counter);

        } else if (inEvidence) {

            internalCounter.setInEvidence(internalCounter.getInEvidence() + counter);

        } else if (withContinuation) {

            internalCounter.setWithContinuation(internalCounter.getWithContinuation() + counter);
        }

        if (!stats.getStats().contains(statsElement)) {
            List<Stats.StatsElement> statsElements = stats.getStats();
            statsElements.add(statsElement);
            stats.setStats(statsElements);
        }

        return stats;
    }


    @Override
    public ClaimsStats getClaimsStatsAllStatus() {

        ClaimsStats claimsStats = claimsRepositoryV1.findClaimsStats();
        return claimsStats;
    }

    @Override
    public Stats getClaimsStats(Boolean isInEvidence) {
        /* Retrieve Cached Dashboard */
        Stats cached = null;
        if (isInEvidence) {
            cached = cacheService.getCachedClaimsStats(CACHENAME, STATSEVIDENCE);
        } else {
            cached = cacheService.getCachedClaimsStats(CACHENAME, STATSkEY);
        }
        if (cached != null) {
            return cached;
        }
        //NEW IMPLEMENTATION REFACTOR


        //


        //List<Object[]> listStatsEntities = claimsRepositoryV1.getStatsClaimsEvidence();
        List<Object[]> listStatsEntities = claimsNewRepository.getStatsClaimsEvidence();
        //List<Object[]> listStatsEntitiesWaitingForAuthority = claimsRepositoryV1.getStatsClaimsEvidenceWithStatusWaitingForAuthority();
        List<Object[]> listStatsEntitiesWaitingForAuthority = claimsNewRepository.getStatsClaimsEvidenceWithStatusWaitingForAuthority();
        //List<Object[]> listStatsEntitiesWaitingForNumberSx = claimsRepositoryV1.getStatsClaimsEvidenceWithStatusWaitingForNumberSx();
        List<Object[]> listStatsEntitiesWaitingForNumberSx = claimsNewRepository.getStatsClaimsEvidenceWithStatusWaitingForNumberSx();
        //List<Object[]> listStatsEntitiesPoValidation = claimsRepositoryV1.getStatsClaimsEvidencePoVariation();
        List<Object[]> listStatsEntitiesPoValidation = claimsNewRepository.getStatsClaimsEvidencePoVariation();

        Stats stats = this.createStatsEntity(listStatsEntities, listStatsEntitiesWaitingForAuthority, listStatsEntitiesWaitingForNumberSx, listStatsEntitiesPoValidation);
        ObjectMapper oMapper = new ObjectMapper();
        Map<String, Object> toCache = oMapper.convertValue(stats, Map.class);

        /* Save Cache Dashboard */
        if (isInEvidence) {
            cacheService.cacheClaimsStats(toCache, CACHENAME, STATSEVIDENCE);
        } else {
            cacheService.cacheClaimsStats(toCache, CACHENAME, STATSkEY);
        }

        return stats;
    }

    @Override
    public ClaimsResponseStatsEvidenceV2 getClaimsStatsV2(Boolean inEvidence, Boolean withContinuation) {
        String key = this.redisKeyGenerator.getFullKeyForRedis(inEvidence, withContinuation);

        /* Retrieve Cached Dashboard */
        ClaimsResponseStatsEvidenceV2 cached = null;
        cached = cacheService.getCachedClaimsStatsV2(CACHENAME, key);

        if(cached != null){
            return cached;
        }

        ClaimsResponseStatsEvidenceV2 responseDashboard= new ClaimsResponseStatsEvidenceV2();
        List<Object[]> listTotalCounter = null;
        //recupero stati sinistro con conteggi senza manipolazione (wainting_for_validation,wainting_for_refund, incomplete,closed_partial_refund, to_send_to_customer)
        List<Object[]> listStatsEntitiesBasedStatus = claimsNewRepository.getClaimsStatsBasedStatus(inEvidence,withContinuation);
        List<Object[]> listStatsWaintingAuthority = claimsNewRepository.getStatsClaimsWaitingForAuthority(inEvidence,withContinuation);
        List<Object[]> listStatsPoVariation =  claimsNewRepository.getStatsClaimsPoVariation(inEvidence,withContinuation);
        List<Object[]> listStatsWaintingNumSx = claimsNewRepository.getStatsClaimWaitingForNumberSx(inEvidence,withContinuation);

        List<List<Object[]>> listOfQuery= new LinkedList<>();
        //importante il numero con cui vengono aggiunti nella lista rispetto al metodo getTotalMap
        listOfQuery.add(listStatsEntitiesBasedStatus);
        listOfQuery.add(listStatsWaintingAuthority);
        listOfQuery.add(listStatsPoVariation);
        listOfQuery.add(listStatsWaintingNumSx);

        responseDashboard.setStatus(this.getTotalMap(listOfQuery));

        //i totali vengono mostrati solo con i filtri altrimenti non vengono visualizzati nella dashboard FE
        if(inEvidence != null || withContinuation != null) {
            listTotalCounter = claimsNewRepository.getClaimsStatsTotalCounter( inEvidence,  withContinuation);
            StatsDashboardV1 statsDashboardV1 = responseDashboard.getStats();
            Integer ful = 0;
            Integer fcm = 0;
            Integer fni = 0;
            Integer no = 0;
            for (Object[] currentObj : listTotalCounter) {
                if(currentObj[0].equals("FUL")){
                    ful = Integer.parseInt(((BigInteger)currentObj[1]).toString());
                }else if(currentObj[0].equals("FNI")){
                    fni = Integer.parseInt(((BigInteger)currentObj[1]).toString());
                }else if(currentObj[0].equals("FCM")){
                    fcm = Integer.parseInt(((BigInteger)currentObj[1]).toString());
                }else if(currentObj[0].equals("NO")){
                    no = Integer.parseInt(((BigInteger)currentObj[1]).toString());
                }
            }


            statsDashboardV1.setFcm(fcm);
            statsDashboardV1.setFni(fni);
            statsDashboardV1.setFul(ful);
            statsDashboardV1.setTot(ful + fcm + fni+no);
            responseDashboard.setStats(statsDashboardV1);
        }

        ObjectMapper oMapper = new ObjectMapper();
        Map<String, Object> toCache = oMapper.convertValue(responseDashboard, Map.class);

        /* Save Cache Dashboard */
        cacheService.cacheClaimsStats(toCache, CACHENAME, key);

        return responseDashboard;

    }

    private Map<String, StatusDashboardElementV1> getTotalMap(List<List<Object[]>> lists){

        Map<String, StatusDashboardElementV1> mappaStatsStatus= this.initMapStatus();



        for(int i=0; i< lists.size(); i++) {
            List<Object[]> currentList =lists.get(i);

            for(Object[] currentObj : currentList){

                String status = (String) currentObj[0];

                String flow = (String) currentObj[1];
                BigInteger bigInteger = (BigInteger) currentObj[2];
                Integer counter = Integer.parseInt(bigInteger.toString());

                if(i==2){
                    //la terza lista rappresenta la lista variazione po
                    status = "PO_VARIATION";
                }

                if(i==3){
                    //la quarta lista rappresenta la lista in attesa numero sinistro
                    status = "WAITING_FOR_NUMBER_SX";
                }

                if (status.equalsIgnoreCase("CLOSED_PARTIAL_REFUND")) {
                    //lo status closed_partial_refun deve essere conteggiato nello status WAITING_FOR_REFUND
                    status = "WAITING_FOR_REFUND";
                }

                StatusDashboardElementV1 statusDashboardElementV1 = mappaStatsStatus.get(status);
                if (statusDashboardElementV1 == null) {
                    statusDashboardElementV1 = new StatusDashboardElementV1();
                }

                int counterApp = 0;

                if (flow.equalsIgnoreCase("FUL")) {
                    counterApp = statusDashboardElementV1.getFul() != null ? statusDashboardElementV1.getFul() : 0;
                    statusDashboardElementV1.setFul(counterApp + counter);
                } else if (flow.equalsIgnoreCase("FNI")) {
                    counterApp = statusDashboardElementV1.getFni() != null ? statusDashboardElementV1.getFni() : 0;
                    statusDashboardElementV1.setFni(counterApp + counter);
                } else if (flow.equalsIgnoreCase("FCM")) {
                    counterApp = statusDashboardElementV1.getFcm() != null ? statusDashboardElementV1.getFcm() : 0;
                    statusDashboardElementV1.setFcm(counterApp + counter);
                }

                mappaStatsStatus.put(status, statusDashboardElementV1);

            }
        }

        return mappaStatsStatus;
    }


    //funzione per inizalizzare la struttura per ogni status della pratica per le stats
    private   Map<String, StatusDashboardElementV1> initMapStatus(){
        Map<String, StatusDashboardElementV1> mappaStatsStatus= new HashMap<>();

        mappaStatsStatus.put("WAITING_FOR_VALIDATION",this.initStatusDashboardElement() );
        mappaStatsStatus.put("INCOMPLETE",this.initStatusDashboardElement() );
        mappaStatsStatus.put("WAITING_FOR_AUTHORITY",this.initStatusDashboardElement() );
        mappaStatsStatus.put("WAITING_FOR_NUMBER_SX",this.initStatusDashboardElement() );
        mappaStatsStatus.put("TO_ENTRUST",this.initStatusDashboardElement() );
        mappaStatsStatus.put("WAITING_FOR_REFUND",this.initStatusDashboardElement() );
        mappaStatsStatus.put("TO_SEND_TO_CUSTOMER",this.initStatusDashboardElement() );
        mappaStatsStatus.put("PO_VARIATION",this.initStatusDashboardElement() );


        return mappaStatsStatus;

    }



    //funzione per inizializzare i flussi per ogni stato
    private StatusDashboardElementV1 initStatusDashboardElement(){
        StatusDashboardElementV1 statusDashboardElementV1 = new StatusDashboardElementV1();
        statusDashboardElementV1.setFcm(0);
        statusDashboardElementV1.setFni(0);
        statusDashboardElementV1.setFul(0);

        return  statusDashboardElementV1;
    }




    //REFACTOR
    @Override
    public List<Historical> getClaimsHistorical(String idClaims) {
        //recupero nella nuova entità
        Optional<ClaimsNewEntity> claimsEntityOptional = claimsNewRepository.findById(idClaims);
        if (!claimsEntityOptional.isPresent()) {
            LOGGER.debug("Claim not found");
            throw new NotFoundException("Claim not found", MessageCode.CLAIMS_1010);
        }

        ClaimsNewEntity claimsEntity = claimsEntityOptional.get();
        System.out.println(claimsEntity.getPracticeId());
        //non c'è necessità di convertire nel vecchio historical resta un jsonb
        return claimsEntity.getHistorical();
    }

    @Transactional
    @Override
    public void deleteClaims(String idClaims) {
        ClaimsEntity claimsEntity = claimsRepository.getOne(idClaims);
        claimsRepository.deleteById(idClaims);
        if (dwhCall) {
            dwhClaimsService.sendMessage(claimsEntity, com.doing.nemo.dwh.client.payload.request.EventTypeEnum.DELETED);
        }

        cacheService.deleteClaimsStats(CACHENAME, STATSkEY);
        cacheService.deleteClaimsStats(CACHENAME, STATSEVIDENCE);
        cacheService.deleteClaimsStats(CACHENAME, STATSSTATUS);
        cacheService.deleteClaimsStats(CACHENAME, STATSTHEFT);
        cacheService.deleteClaimsStats(CACHENAME, COUNTERPARTYKEY);
    }

    public CounterpartyEntity getCounterpartyResponsable(List<CounterpartyEntity> counterpartyList) {
        if (counterpartyList == null || counterpartyList.isEmpty())
            return null;
        if (counterpartyList.size() == 1)
            return counterpartyList.get(0);
        Iterator<CounterpartyEntity> iterator = counterpartyList.iterator();
        boolean isResponsible = false;
        while (iterator.hasNext() && !isResponsible) {
            CounterpartyEntity counterparty = iterator.next();
            if (counterparty.getResponsible() != null && counterparty.getResponsible())
                return counterparty;
        }
        return null;
    }

    //REFACTOR
    @Transactional
    @Override
    public void setAutomaticEntrust() throws IOException {
        //recupero nuova entità
        List<ClaimsNewEntity> claimsNewList = claimsNewRepository.getClaimsToAutomaticEntrust(skipMigratedClaimsAutomaticEntrust);
        //conversione nella vecchia entità
        List<ClaimsEntity> claimsList = converterClaimsService.wrapFromClaimsNewEntityListToClaimsEntityList(claimsNewList);
        if (CollectionUtils.isNotEmpty(claimsList)) {
            LOGGER.debug("[ENTRUST] Claims List TO_ENTRUST size : " + claimsList.size());
            for (ClaimsEntity currentClaim : claimsList) {
                LOGGER.debug("[ENTRUST " + currentClaim.getPracticeId() + "] Check if claim with practice_id " + currentClaim.getPracticeId() + " could be entrusted ");

                ClaimsEntity oldClaims = new ClaimsEntity();
                oldClaims.setStatus(currentClaim.getStatus());
                oldClaims.setType(currentClaim.getType());
                oldClaims.setComplaint(new Complaint());
                oldClaims.getComplaint().setDataAccident(new DataAccident());
                oldClaims.getComplaint().getDataAccident().setTypeAccident(currentClaim.getComplaint().getDataAccident().getTypeAccident());
                ClaimsStatusEnum oldStatus = currentClaim.getStatus();
                IncidentRequestV1 oldIncident = incidentAdapter.adptFromClaimsEntityToIncidentRequest(currentClaim, null, false, false);
                String description = "";
                if (currentClaim.getDamaged().getInsuranceCompany().getTpl().getInsuranceCompanyId() != null) {
                    InsuranceCompanyEntity claimInsuranceCompany = insuranceCompanyRepository.getOne(currentClaim.getDamaged().getInsuranceCompany().getTpl().getInsuranceCompanyId());

                    //Variabili per l'invio della mail in caso di affido
                    boolean isMailToSend = false;
                    EventTypeEnum eventTypeTosend = null;
                    //tipo sinistro
                    DataAccidentTypeAccidentEnum typeAccident = currentClaim.getComplaint().getDataAccident().getTypeAccident();
                    //è accaduto all'estero
                    Boolean isAbroad = currentClaim.getComplaint().getDataAccident().getHappenedAbroad();
                    //ci sono feriti
                    Boolean isWounded = currentClaim.getWoundedList() != null && currentClaim.getWoundedList().size() > 0;
                    //è riparabile
                    Boolean isRecoverability = currentClaim.getDamaged().getVehicle().getWreck();

                    //Recupero della controparte responsabile
                    CounterpartyEntity counterparty = this.getCounterpartyResponsable(currentClaim.getCounterparts());
                    //tipo controparte
                    VehicleTypeEnum typeCounterparty = null;
                    //compagnia assicuratrice controparte
                    InsuranceCompanyEntity companyCounterparty = null;
                    //compagnia controparte NON PRESENTE
                    Boolean isNotCompanyCounterparty = null;
                    UUID counterpartyCompanyId = null;
                    if (counterparty != null) {
                        typeCounterparty = counterparty.getType();
                        companyCounterparty = counterparty.getInsuranceCompany().getEntity();
                        isNotCompanyCounterparty = companyCounterparty.getId().equals(UUID.fromString(noCompany));
                        counterpartyCompanyId = companyCounterparty.getId();
                    }
                    //dati rimborso per somma PO
                    Double sumPo = currentClaim.getRefund() != null ? currentClaim.getRefund().getPoSum() : null;
                    SimpleDateFormat sdf = new SimpleDateFormat("MM/yyyy");
                    String dataStr = sdf.format(new Date());

                    boolean canEntrust = this.checkIfCanEntrustClaim(currentClaim);
                    LOGGER.debug("[ENTRUST " + currentClaim.getPracticeId() + "] Claim entrusted variable checker");
                    LOGGER.debug("[ENTRUST " + currentClaim.getPracticeId() + "] claimInsuranceCompany : " + claimInsuranceCompany.getId() + ", typeAccident: " + typeAccident +
                            ", claimFlow: " + currentClaim.getType() + ", currentMonth: " + dataStr +
                            ", isAbroad: " + isAbroad + ", isWounded: " + isWounded + ", isRecoverability: " + isRecoverability + ", ResponsibleCounterpartyID: " + (counterparty != null ? counterparty.getCounterpartyId() : null) +
                            ", typeCounterparty: " + typeCounterparty + ", counterpartyCompanyId: " + counterpartyCompanyId +
                            ", sumPo: " + sumPo);
                    this.automaticEntrustClaim(currentClaim, eventTypeTosend, description, isNotCompanyCounterparty, isAbroad, isWounded, isRecoverability, counterpartyCompanyId, typeAccident, typeCounterparty, claimInsuranceCompany, sumPo, oldStatus, oldClaims, oldIncident, isMailToSend, canEntrust);
                }
            }
            cacheService.deleteClaimsStats(CACHENAME, STATSkEY);
            cacheService.deleteClaimsStats(CACHENAME, STATSEVIDENCE);
            cacheService.deleteClaimsStats(CACHENAME, STATSSTATUS);
        } else {
            LOGGER.debug("[ENTRUST] Claims List TO_ENTRUST: null or empty");
        }
    }

    private boolean checkIfCanEntrustClaim(ClaimsEntity currentClaim) {
       Boolean isCheckEntrust = true;

        if (!goLiveStrategyService.checkIfStepThree()) {
            if (goLiveStrategyService.checkIfStepMinusOne()) {
                if (goLiveStrategyService.getCustomerIdListToFilterStepMinusOneAndHigher().contains(currentClaim.getDamaged().getCustomer().getCustomerId())){
                    isCheckEntrust = true;
                }else{
                    isCheckEntrust = false;
                }

            } else if (goLiveStrategyService.checkIfStepZero()) {
                if (goLiveStrategyService.getCustomerIdListToFilterStepMinusOneAndHigher().contains(currentClaim.getDamaged().getCustomer().getCustomerId())){
                    isCheckEntrust = true;
                }else if (goLiveStrategyService.getTypeAccidentListToFilterStepZero().contains(currentClaim.getComplaint().getDataAccident().getTypeAccident().getValue())){
                    isCheckEntrust = true;
                }else{
                    isCheckEntrust = false;
                }

            } else if (goLiveStrategyService.checkIfStepOne()) {
                if (goLiveStrategyService.getCustomerIdListToFilterStepMinusOneAndHigher().contains(currentClaim.getDamaged().getCustomer().getCustomerId())){
                    isCheckEntrust = true;
                }else if (goLiveStrategyService.getTypeAccidentListToFilterStepZero().contains(currentClaim.getComplaint().getDataAccident().getTypeAccident().getValue())){
                    isCheckEntrust = true;
                }else{
                    isCheckEntrust = false;
                }

            }else if (goLiveStrategyService.checkIfStepTwo()){
                if (goLiveStrategyService.getCustomerIdListToFilterStepMinusOneAndHigher().contains(currentClaim.getDamaged().getCustomer().getCustomerId())){
                    isCheckEntrust = true;
                }else if (goLiveStrategyService.getTypeAccidentListToFilterStepTwoOne().contains(currentClaim.getComplaint().getDataAccident().getTypeAccident().getValue())){
                    isCheckEntrust = true;
                }else{
                    isCheckEntrust = false;
                }
            }
        }
        return isCheckEntrust;
    }

    private void automaticEntrustClaim(ClaimsEntity currentClaim, EventTypeEnum eventTypeTosend, String description,
                                       Boolean isNotCompanyCounterparty, Boolean isAbroad, Boolean isWounded,
                                       Boolean isRecoverability, UUID counterpartyCompanyId, DataAccidentTypeAccidentEnum typeAccident,
                                       VehicleTypeEnum typeCounterparty, InsuranceCompanyEntity claimInsuranceCompany, Double sumPo,
                                       ClaimsStatusEnum oldStatus, ClaimsEntity oldClaims, IncidentRequestV1 oldIncident,
                                       boolean isMailToSend, boolean canEntrust) throws IOException {
        //flusso FUL
        if (canEntrust) {
            if (currentClaim.getType().equals(ClaimsFlowEnum.FUL)) {
                LOGGER.debug("[ENTRUST " + currentClaim.getPracticeId() + "] FUL flow");
                //se il sinistro è passivo va in chiudi pratica
                if (passiveSet.contains(currentClaim.getComplaint().getDataAccident().getTypeAccident())) {
                    LOGGER.debug("[ENTRUST " + currentClaim.getPracticeId() + "] Passive Set");
                    //PASSIVO
                  // i sx passivi non cambiano di stato currentClaim.setStatus(ClaimsStatusEnum.CLOSED);
                } else if (activeSet.contains(currentClaim.getComplaint().getDataAccident().getTypeAccident())) {
                    LOGGER.debug("[ENTRUST " + currentClaim.getPracticeId() + "] Active Set");
                    //da controllare solo la controparte responsabile
                    Boolean isCard = this.checkIsCard(currentClaim.getDamaged(), currentClaim.getCounterparts(), currentClaim.getComplaint().getDataAccident().getTypeAccident());
                    //Boolean isCard = insuranceCompanyService.getCardCompanyByName(currentClaim.getDamaged().getInsuranceCompany().getTpl().getCompany());
                    //controllo che la compagnia assicuratrice è card oppure no
                    LOGGER.debug("[ENTRUST " + currentClaim.getPracticeId() + "] isCard: " + isCard);
                    if (isCard == null) {
                        Entrusted entrusted = new Entrusted();
                        currentClaim.setStatus(ClaimsStatusEnum.TO_ENTRUST);
                        entrusted.setAutoEntrust(false);
                        Complaint complaint = currentClaim.getComplaint();
                        complaint.setEntrusted(entrusted);
                        currentClaim.setComplaint(complaint);
                        eventTypeTosend = EventTypeEnum.ENTRUST;
                        description += "Sinistro non valido per l'affido automatico poiché di tipologia CARD ma con compagnie non aderenti";
                        LOGGER.debug("[ENTRUST " + currentClaim.getPracticeId() + "] claim's type CARD, but companies' damaged and counterparty not Card");
                    } else {

                        if (isCard) {
                            // se CARD è true la pratica è affidata alla compagnia assicuratrice
                            InspectorateEntity inspectorate = inspectorateService.selectInspectorateWithMaxVolume(isNotCompanyCounterparty, isAbroad, isWounded, isRecoverability, counterpartyCompanyId, typeAccident, typeCounterparty, claimInsuranceCompany.getId(), sumPo, currentClaim.getPracticeId());

                            Entrusted entrusted = new Entrusted();
                            entrusted.setEntrustedType(EntrustedEnum.INSPECTORATE);

                            if (inspectorate != null) {
                                LOGGER.debug("[ENTRUST " + currentClaim.getPracticeId() + "] Claim entrusted to inspectorate: " + inspectorate.getId());
                                entrusted.setEntrustedDay(new Date());
                                entrusted.setIdEntrustedTo(inspectorate.getId());
                                entrusted.setEntrustedTo(inspectorate.getName());
                                entrusted.setEntrustedEmail(inspectorate.getEmail());
                                currentClaim.setStatus(ClaimsStatusEnum.WAITING_FOR_REFUND);
                                entrusted.setAutoEntrust(true);
                                eventTypeTosend = EventTypeEnum.ENTRUST_TO_THE_COMPANY;
                                isMailToSend = true;
                            } else {
                                LOGGER.debug("[ENTRUST " + currentClaim.getPracticeId() + "] Claim's entrust false");
                                currentClaim.setStatus(ClaimsStatusEnum.TO_ENTRUST);
                                entrusted.setAutoEntrust(false);
                                description = "Sinistro non valido per l'affido automatico poiché non è stato trovato un ispettorato valido";
                            }

                            Complaint complaint = currentClaim.getComplaint();
                            complaint.setEntrusted(entrusted);
                            currentClaim.setComplaint(complaint);


                        } else {
                            //gestione affido studio legale
                            //valore amount fittizio
                            LegalEntity legal = legalService.selectLegalWithMaxVolume(isNotCompanyCounterparty, isAbroad, isWounded, isRecoverability, counterpartyCompanyId, typeAccident, typeCounterparty, claimInsuranceCompany.getId(), sumPo, currentClaim.getPracticeId());

                            Entrusted entrusted = new Entrusted();
                            entrusted.setEntrustedType(EntrustedEnum.LEGAL);

                            if (legal != null) {
                                //valore amount fittizio
                                LOGGER.debug("[ENTRUST " + currentClaim.getPracticeId() + "] Claim entrusted to legal: " + legal.getId());
                                entrusted.setIdEntrustedTo(legal.getId());
                                entrusted.setEntrustedDay(new Date());
                                entrusted.setEntrustedTo(legal.getName());
                                entrusted.setEntrustedEmail(legal.getEmail());
                                //cambia lo stato della pratica in stato successivo


                                currentClaim.setStatus(ClaimsStatusEnum.WAITING_FOR_REFUND);
                                //deve partire l'email dell'affido
                                entrusted.setAutoEntrust(true);
                                eventTypeTosend = EventTypeEnum.ENTRUST_TO_THE_LEGAL;
                                isMailToSend = true;
                            } else {
                                LOGGER.debug("[ENTRUST " + currentClaim.getPracticeId() + "] Claim's entrust false");
                                currentClaim.setStatus(ClaimsStatusEnum.TO_ENTRUST);
                                entrusted.setAutoEntrust(false);
                                description = "Sinistro non valido per l'affido automatico poiché non è stato trovato un legale valido";
                            }


                            Complaint complaint = currentClaim.getComplaint();
                            complaint.setEntrusted(entrusted);
                            currentClaim.setComplaint(complaint);
                        }
                    }
                }
            } else if (currentClaim.getType().equals(ClaimsFlowEnum.FCM)) {
                LOGGER.debug("[ENTRUST " + currentClaim.getPracticeId() + "] FCM flow");

                Boolean isCard = this.checkIsCard(currentClaim.getDamaged(), currentClaim.getCounterparts(), currentClaim.getComplaint().getDataAccident().getTypeAccident());
                LOGGER.debug("[ENTRUST " + currentClaim.getPracticeId() + "] isCard: " + isCard);
                if (isCard == null) {
                    Entrusted entrusted = new Entrusted();
                    currentClaim.setStatus(ClaimsStatusEnum.TO_ENTRUST);
                    entrusted.setAutoEntrust(false);
                    Complaint complaint = currentClaim.getComplaint();
                    complaint.setEntrusted(entrusted);
                    currentClaim.setComplaint(complaint);
                    eventTypeTosend = EventTypeEnum.ENTRUST;
                    description += "Sinistro non valido per l'affido automatico poiché di tipologia CARD ma con compagnie non aderenti";
                    LOGGER.debug("[ENTRUST " + currentClaim.getPracticeId() + "] claim's type CARD, but companies' damaged and counterparty not Card");
                } else {
                    if (currentClaim.getWithCounterparty() &&
                            !theftSet.contains(currentClaim.getComplaint().getDataAccident().getTypeAccident()) &&
                            !isCard) {

                        LOGGER.debug("[ENTRUST " + currentClaim.getPracticeId() + "] Flow FCM no Card, no theft, counterpaty true");
                        //valore amount fittizio
                        LegalEntity legal = legalService.selectLegalWithMaxVolume(isNotCompanyCounterparty, isAbroad, isWounded, isRecoverability, counterpartyCompanyId, typeAccident, typeCounterparty, claimInsuranceCompany.getId(), sumPo, currentClaim.getPracticeId());
                        Entrusted entrusted = new Entrusted();
                        entrusted.setEntrustedType(EntrustedEnum.LEGAL);
                        if (legal != null) {
                            LOGGER.debug("[ENTRUST " + currentClaim.getPracticeId() + "] Claim entrusted to legal: " + legal.getId());
                            //valore amount fittizio
                            entrusted.setEntrustedDay(new Date());
                            entrusted.setIdEntrustedTo(legal.getId());
                            entrusted.setEntrustedTo(legal.getName());
                            entrusted.setEntrustedEmail(legal.getEmail());
                            currentClaim.setStatus(ClaimsStatusEnum.WAITING_FOR_REFUND);
                            entrusted.setAutoEntrust(true);
                            eventTypeTosend = EventTypeEnum.ENTRUST_TO_THE_LEGAL;
                            isMailToSend = true;
                            //affida e cambia stato
                            //manda comunicazione
                        } else {
                            LOGGER.debug("[ENTRUST " + currentClaim.getPracticeId() + "] Claim's entrust false");
                            currentClaim.setStatus(ClaimsStatusEnum.TO_ENTRUST);
                            entrusted.setAutoEntrust(false);
                            description = "Sinistro non valido per l'affido automatico poiché non è stato trovato un legale valido";

                        }
                        Complaint complaint = currentClaim.getComplaint();
                        complaint.setEntrusted(entrusted);
                        currentClaim.setComplaint(complaint);

                    } else {
                        LOGGER.debug("[ENTRUST " + currentClaim.getPracticeId() + "] Flow FCM  Card OR theft OR counterpaty false");
                        Entrusted entrusted = new Entrusted();
                        entrusted.setEntrustedType(EntrustedEnum.MANAGER);
                        //recupero dati Compagnia assicuratrice dal cliente la polizza
                        String clientCode = currentClaim.getDamaged().getCustomer().getCustomerId();
                        InsurancePolicyEntity insurancePolicyEntity = null;

                        if (currentClaim.getDamaged() != null && currentClaim.getDamaged().getInsuranceCompany() != null && currentClaim.getDamaged().getInsuranceCompany().getTpl() != null) {
                            Optional<InsurancePolicyEntity> optionalInsurancePolicy = insurancePolicyRepository.findById(currentClaim.getDamaged().getInsuranceCompany().getTpl().getInsurancePolicyId());
                            if (optionalInsurancePolicy.isPresent()) {
                                insurancePolicyEntity = optionalInsurancePolicy.get();
                            }
                        }

                        if (insurancePolicyEntity != null && insurancePolicyEntity.getInsuranceManagerEntity() != null
                                && insurancePolicyEntity.getInsuranceManagerEntity().getActive()) {
                            LOGGER.debug("[ENTRUST " + currentClaim.getPracticeId() + "] Manager with id " + insurancePolicyEntity.getInsuranceManagerEntity().getId() + " of insurance:  " + insurancePolicyEntity.getId() + " found");
                            entrusted.setIdEntrustedTo(insurancePolicyEntity.getInsuranceManagerEntity().getId());
                            entrusted.setEntrustedTo(insurancePolicyEntity.getInsuranceManagerEntity().getName());
                            entrusted.setEntrustedEmail(insurancePolicyEntity.getInsuranceManagerEntity().getEmail());
                            entrusted.setEntrustedDay(new Date());
                            currentClaim.setStatus(ClaimsStatusEnum.WAITING_FOR_REFUND);
                            entrusted.setAutoEntrust(true);
                            eventTypeTosend = EventTypeEnum.ENTRUST_TO_THE_CLAIMS_MANAGER;
                            isMailToSend = true;
                            //mandare comunicazione di affido
                        } else {

                            if (insurancePolicyEntity == null) {
                                LOGGER.debug("[ENTRUST " + currentClaim.getPracticeId() + "] Entrust failed, policy with id :  " + currentClaim.getDamaged().getInsuranceCompany().getTpl().getInsurancePolicyId() + " not present");
                            } else {
                                LOGGER.debug("[ENTRUST " + currentClaim.getPracticeId() + "] Entrust failed, manager of insurance policy:  " + insurancePolicyEntity.getId() + " not present");
                            }

                            currentClaim.setStatus(ClaimsStatusEnum.TO_ENTRUST);
                            entrusted.setAutoEntrust(false);
                            description = "Sinistro non valido per l'affido automatico poiché non è stato trovato un manager valido";
                        }

                        Complaint complaint = currentClaim.getComplaint();
                        complaint.setEntrusted(entrusted);
                        currentClaim.setComplaint(complaint);

                    }
                }
            }

            //nel caso di tipologie di sinistro che non rientrano in nessuna casistiva viene comunque data evidenza all'operatore del fallimento dell'affido
            if (currentClaim.getStatus().equals(ClaimsStatusEnum.CLOSED)) {
                Entrusted entrusted = null;
                if (currentClaim.getComplaint() != null && currentClaim.getComplaint().getEntrusted() == null) {
                    entrusted = new Entrusted();
                } else if (currentClaim.getComplaint() != null) {
                    entrusted = currentClaim.getComplaint().getEntrusted();
                }
                entrusted.setAutoEntrust(false);
                currentClaim.getComplaint().setEntrusted(entrusted);
                description = "Sinistro passivo chiuso dal processo di affido automatico";
                LOGGER.debug("[ENTRUST " + currentClaim.getPracticeId() + "] claim into passive set. It will be closed");
            }
        } else {
            Entrusted entrusted = new Entrusted();
            currentClaim.setStatus(ClaimsStatusEnum.TO_ENTRUST);
            entrusted.setAutoEntrust(false);
            Complaint complaint = currentClaim.getComplaint();
            complaint.setEntrusted(entrusted);
            currentClaim.setComplaint(complaint);
            description += "Sinistro non valido per l'affido automatico poiché non può essere lavorato in base alle impostazioni dello step attuale su NEMO";
            LOGGER.debug("[ENTRUST " + currentClaim.getPracticeId() + "] claim is not workable on NEMO in accordance with the settings of the current step. Entrust failed");
        }


        Historical historical = new Historical(eventTypeTosend, ClaimsStatusEnum.TO_ENTRUST, currentClaim.getStatus(), currentClaim.getComplaint().getDataAccident().getTypeAccident(), currentClaim.getComplaint().getDataAccident().getTypeAccident(), DateUtil.getNowDate(), null, ClaimsHistoricalUserEnum.SYSTEM_AUTOMATIC_ENTRUST.getValue(), currentClaim.getType().getValue(), currentClaim.getType().getValue(), description);


        //conversione nella nuova entità
        ClaimsNewEntity claimsNewEntity = converterClaimsService.convertOldEntityToNewClaimsEntity(currentClaim);
        //salvataggio nuova entità
        claimsNewRepository.save(claimsNewEntity);

        //recupero nuova entità persistita precedentemente
        Optional<ClaimsNewEntity> optionalNewClaimsEntity = claimsNewRepository.findById(currentClaim.getId());
        //controllo che esiste
        if (optionalNewClaimsEntity.isPresent()) {

            //converto nella vecchia entità
            currentClaim = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(optionalNewClaimsEntity.get());

            if (isMailToSend && eventTypeTosend != null) {
                //invio email

                List<EmailTemplateMessagingResponseV1> emailTemplateMessagingResponseV1s = messagingService.getMailTemplateByTypeEvent(eventTypeTosend, currentClaim.getId());

                if (emailTemplateMessagingResponseV1s != null && !emailTemplateMessagingResponseV1s.isEmpty()) {

                    List<EmailTemplateMessagingRequestV1> emailToBeSend = MessagingAdapter.adptEmailTemplateMessagingRequestToEmailTemplateMessagingResponseList(emailTemplateMessagingResponseV1s);
                    List<EmailTemplateMessagingRequestV1> listTos = new LinkedList<>();
                    List<EmailTemplateMessagingRequestV1> listNotTos = new LinkedList<>();
                    for (EmailTemplateMessagingRequestV1 currentTemplate : emailToBeSend) {
                        if (currentTemplate.getTos() != null && !currentTemplate.getTos().isEmpty()) {
                            listTos.add(currentTemplate);
                        } else {
                            listNotTos.add(currentTemplate);
                        }
                    }
                    List<EmailTemplateMessagingRequestV1> emailTemplateMessaging = new LinkedList<>();
                    try {

                        emailTemplateMessaging = messagingService.splitEmailIfContainsUrl(listTos, currentClaim.getId());
                    } catch (Exception e) {
                        LOGGER.criticalError("NEMO-CLAIMS", "[ENTRUST] Placheholder error parsing for claim " + currentClaim.getId() + " email entrust. Email will be send without placeholder parsing", e);
                        emailTemplateMessaging.addAll(listTos);
                    }
                    emailTemplateMessaging.addAll(listNotTos);
                    try {
                        description = messagingService.sendMailAndCreateLogs(emailTemplateMessaging, null, currentClaim.getCreatedAt());
                    } catch (Exception e) {
                        LOGGER.criticalError("NEMO-CLAIMS", "[ENTRUST] Sending email for claim " + currentClaim.getId(), e);
                    }

                }

            } else {
                if (description.equals(""))
                    description += "Affido automatico fallito";


            }

            currentClaim.addHistorical(historical);
            currentClaim.setInEvidence(false);
            currentClaim.setUpdateAt(DateUtil.getNowInstant());
            //converto nella nuova entità
            claimsNewEntity = converterClaimsService.convertOldEntityToNewClaimsEntity(currentClaim);
            claimsNewRepository.save(claimsNewEntity);

            if (dwhCall) {
                dwhClaimsService.sendMessage(currentClaim, com.doing.nemo.dwh.client.payload.request.EventTypeEnum.UPDATED);
            }
            //Intervento 6 - C
            externalCommunicationService.modifyIncidentAsync(currentClaim.getId(), oldStatus, false, oldIncident, false, oldClaims);
        }
    }



    //ALD VS ALD
    @Override
    @Transactional
    public ClaimsEntity checkALDvsALD(ClaimsEntity claimsEntity) {
        if (claimsEntity == null) {
            return null;
        }
        CounterpartyEntity counterparty = this.getCounterpartyResponsable(claimsEntity.getCounterparts());
        if (counterparty == null) {
            return claimsEntity;
        }
        if (counterparty.getIsAld() != null && counterparty.getIsAld()) {
            ClaimsNewEntity claimsNewEntity = null;
            //Inserire ID nell'insured
            if (claimsEntity.getDamaged().getCustomer().getCustomerId().equalsIgnoreCase(counterparty.getInsured().getCustomerId())) {
                if (activeSet.contains(claimsEntity.getComplaint().getDataAccident().getTypeAccident())) {
                    String description = "Chiuso senza seguito poiché sinistro attivo e con controparte appartenente alla flotta ALD";
                    Historical historical = new Historical(EventTypeEnum.CLOSING_WITHOUT_SEQUEL, claimsEntity.getStatus(), ClaimsStatusEnum.CLOSED, claimsEntity.getComplaint().getDataAccident().getTypeAccident(), claimsEntity.getComplaint().getDataAccident().getTypeAccident(), DateUtil.getNowDate(), null, ClaimsHistoricalUserEnum.SYSTEM_ALDVSALD.getValue(), claimsEntity.getType().getValue(), claimsEntity.getType().getValue(), description);
                    claimsEntity.setStatus(ClaimsStatusEnum.CLOSED);
                    if (claimsEntity.getHistorical() == null)
                        claimsEntity.setHistorical(new LinkedList<>());

                    List<Historical> historicalList = claimsEntity.getHistorical();
                    historicalList.add(historical);
                    claimsEntity.setHistorical(historicalList);
                    //Devo salvare la nuova entità
                    claimsNewEntity = converterClaimsService.convertOldEntityToNewClaimsEntity(claimsEntity);
                    claimsNewRepository.save(claimsNewEntity);
                    if (dwhCall) {
                        dwhClaimsService.sendMessage(claimsEntity, com.doing.nemo.dwh.client.payload.request.EventTypeEnum.UPDATED);
                    }
                }
            } else {
                if (counterparty.getInsuranceCompany() != null && counterparty.getInsuranceCompany().getEntity() != null) {
                    InsuranceCompanyEntity insuranceCompanyEntity = counterparty.getInsuranceCompany().getEntity();
                    if (claimsEntity.getDamaged().getInsuranceCompany().getTpl().getInsuranceCompanyId().equals(insuranceCompanyEntity.getId())) {
                        if (insuranceCompanyEntity.getId().equals(UUID.fromString(companySogessur))) {
                            //AFFIDO A MSA
                            Entrusted entrusted = new Entrusted();
                            Optional<LegalEntity> legalEntityOptional = legalRepository.findById(UUID.fromString(legalMsa));
                            if (legalEntityOptional.isPresent()) {
                                LegalEntity legalEntity = legalEntityOptional.get();
                                entrusted.setAutoEntrust(false);
                                entrusted.setIdEntrustedTo(UUID.fromString(legalMsa));
                                entrusted.setEntrustedTo(legalEntity.getName());
                                entrusted.setEntrustedEmail(legalEntity.getEmail());
                                entrusted.setEntrustedDay(DateUtil.getNowDate());
                                entrusted.setEntrustedType(EntrustedEnum.LEGAL);
                                claimsEntity.getComplaint().setEntrusted(entrusted);
                            }
                        } else {
                            //AFFIDO A AON
                            Entrusted entrusted = new Entrusted();
                            Optional<LegalEntity> legalEntityOptional = legalRepository.findById(UUID.fromString(legalAon));
                            if (legalEntityOptional.isPresent()) {
                                LegalEntity legalEntity = legalEntityOptional.get();
                                entrusted.setAutoEntrust(false);
                                entrusted.setIdEntrustedTo(UUID.fromString(legalAon));
                                entrusted.setEntrustedEmail(legalEntity.getEmail());
                                entrusted.setEntrustedTo(legalEntity.getName());
                                entrusted.setEntrustedDay(DateUtil.getNowDate());
                                entrusted.setEntrustedType(EntrustedEnum.LEGAL);
                                claimsEntity.getComplaint().setEntrusted(entrusted);
                            }
                        }
                    } else {
                        if (insuranceCompanyEntity.getId().equals(UUID.fromString(companySogessur))) {
                            //AFFIDO A MSA
                            Entrusted entrusted = new Entrusted();
                            Optional<LegalEntity> legalEntityOptional = legalRepository.findById(UUID.fromString(legalMsa));
                            if (legalEntityOptional.isPresent()) {
                                LegalEntity legalEntity = legalEntityOptional.get();
                                entrusted.setAutoEntrust(false);
                                entrusted.setIdEntrustedTo(UUID.fromString(legalMsa));
                                entrusted.setEntrustedTo(legalEntity.getName());
                                entrusted.setEntrustedEmail(legalEntity.getEmail());
                                entrusted.setEntrustedDay(DateUtil.getNowDate());
                                entrusted.setEntrustedType(EntrustedEnum.LEGAL);
                                claimsEntity.getComplaint().setEntrusted(entrusted);
                            }
                        } else {
                            //AFFIDO A AON
                            Entrusted entrusted = new Entrusted();
                            Optional<LegalEntity> legalEntityOptional = legalRepository.findById(UUID.fromString(legalAon));
                            if (legalEntityOptional.isPresent()) {
                                LegalEntity legalEntity = legalEntityOptional.get();
                                entrusted.setAutoEntrust(false);
                                entrusted.setIdEntrustedTo(UUID.fromString(legalAon));
                                entrusted.setEntrustedTo(legalEntity.getName());
                                entrusted.setEntrustedEmail(legalEntity.getEmail());
                                entrusted.setEntrustedDay(DateUtil.getNowDate());
                                entrusted.setEntrustedType(EntrustedEnum.LEGAL);
                                claimsEntity.getComplaint().setEntrusted(entrusted);
                            }
                        }
                    }


                    List<EmailTemplateMessagingResponseV1> emailTemplateEntity = messagingService.getMailTemplateByTypeEvent(EventTypeEnum.ENTRUST_TO_THE_LEGAL, claimsEntity.getId());
                    String description = "";

                    if (emailTemplateEntity != null && !emailTemplateEntity.isEmpty()) {

                        List<EmailTemplateMessagingRequestV1> listRequestEmail = (MessagingAdapter.adptEmailTemplateMessagingRequestToEmailTemplateMessagingResponseList(emailTemplateEntity));

                        List<EmailTemplateMessagingRequestV1> listTos = new LinkedList<>();
                        List<EmailTemplateMessagingRequestV1> listNotTos = new LinkedList<>();

                        for (EmailTemplateMessagingRequestV1 currentTemplate : listRequestEmail) {
                            if (currentTemplate.getTos() != null && !currentTemplate.getTos().isEmpty() && currentTemplate.getObject() != null && !currentTemplate.getObject().isEmpty()) {
                                listTos.add(currentTemplate);
                            } else {
                                listNotTos.add(currentTemplate);
                            }
                        }

                        List<EmailTemplateMessagingRequestV1> emailTemplateMessaging = messagingService.splitEmailIfContainsUrl(listTos, claimsEntity.getId());
                        emailTemplateMessaging.addAll(listNotTos);
                        description = messagingService.sendMailAndCreateLogs(emailTemplateMessaging, null, claimsEntity.getCreatedAt());

                    }


                    Historical historical = new Historical(EventTypeEnum.ENTRUST_TO_THE_LEGAL, claimsEntity.getStatus(), ClaimsStatusEnum.WAITING_FOR_REFUND, claimsEntity.getComplaint().getDataAccident().getTypeAccident(), claimsEntity.getComplaint().getDataAccident().getTypeAccident(), DateUtil.getNowDate(), null, ClaimsHistoricalUserEnum.SYSTEM_ALDVSALD.getValue(), claimsEntity.getType().getValue(), claimsEntity.getType().getValue(), description);

                    if (claimsEntity.getHistorical() == null)
                        claimsEntity.setHistorical(new LinkedList<>());

                    List<Historical> historicalList = claimsEntity.getHistorical();
                    historicalList.add(historical);
                    claimsEntity.setHistorical(historicalList);
                    claimsEntity.setStatus(ClaimsStatusEnum.WAITING_FOR_REFUND);


                    claimsNewEntity = converterClaimsService.convertOldEntityToNewClaimsEntity(claimsEntity);
                    claimsNewRepository.save(claimsNewEntity);
                }
            }
        }
        return claimsEntity;
    }

    public AlertCodeListResponseV1 checkSameCustomerAldVSAld(ClaimsInsertRequestV1 claimsInsertRequestV1) {
        ClaimsEntity claimsEntity = this.adptClaimsInsertRequestToClaimsEntityWithFlow(claimsInsertRequestV1);
        AlertCodeListResponseV1 alertCodeListResponseV1 = new AlertCodeListResponseV1();
        List<MessageCode> messageCodes = new ArrayList<>();
        CounterpartyEntity counterparty = getCounterpartyResponsable(claimsEntity.getCounterparts());
        if (counterparty == null)
            return alertCodeListResponseV1;

        if (counterparty.getAld() != null && counterparty.getAld()) {
            if (claimsEntity.getDamaged().getCustomer().getCustomerId().equalsIgnoreCase(counterparty.getInsured().getCustomerId())
            ) {
                if (activeSet.contains(claimsEntity.getComplaint().getDataAccident().getTypeAccident())) {
                    messageCodes.add(MessageCode.CLAIMS_2001);
                } else if (passiveSet.contains(claimsEntity.getComplaint().getDataAccident().getTypeAccident())) {
                    messageCodes.add(MessageCode.CLAIMS_2003);

                }
            }

            if (claimsEntity.getDamaged().getInsuranceCompany().getTpl().getInsuranceCompanyId().equals(counterparty.getInsuranceCompany().getEntity().getId()))
                messageCodes.add(MessageCode.CLAIMS_2002);

        }
        if (!messageCodes.isEmpty())
            alertCodeListResponseV1.setAlertCodeList(messageCodes);
        return alertCodeListResponseV1;
    }

    //chrone a mezzanotte di ogni giorno per l'affido automatico
    //@Scheduled(cron = "0 0 1 * * *")
    //@Scheduled(cron = "*/10 * * * * *")

    public ClaimsEntity patchValidateAttachments(String claimsId, List<IdRequestV1> idAttachments) {
        Optional<ClaimsNewEntity> claimsEntityOptional = claimsNewRepository.findById(claimsId);
        if(claimsEntityOptional.isPresent()){
            ClaimsNewEntity claimsEntity = claimsEntityOptional.get();
            Boolean check = false;
            if (claimsEntity.getForms() != null && claimsEntity.getForms().getAttachment() != null) {
                for (Attachment attachment : claimsEntity.getForms().getAttachment()) {
                    for (IdRequestV1 attachmentIdRequestV1 : idAttachments) {
                        if (attachmentIdRequestV1.getId().equals(attachment.getFileManagerId())) {
                            attachment.setValidate(true);
                        }
                    }
                }
                for (Attachment attachment : claimsEntity.getForms().getAttachment()) {
                    if (!attachment.getValidate()) {
                        check = true;
                        break;
                    }
                }
            }

            if(claimsEntity.getAntiTheftRequestEntities()!=null){
                for(AntiTheftRequestNewEntity current : claimsEntity.getAntiTheftRequestEntities()){
                    for (IdRequestV1 attachmentIdRequestV1 : idAttachments) {
                        if (current.getCrashReport()!=null && !current.getCrashReport().getValidate() && attachmentIdRequestV1.getId().equals(current.getCrashReport().getFileManagerId())) {
                            current.getCrashReport().setValidate(true);
                        }
                    }
                }
                if(!check){
                    for (AntiTheftRequestNewEntity current : claimsEntity.getAntiTheftRequestEntities()) {
                        if (current.getCrashReport()!=null && !current.getCrashReport().getValidate()) {
                            check = true;
                            break;
                        }
                    }
                }
            }

            claimsEntity.setWithContinuation(check);
            claimsNewRepository.save(claimsEntity);
            ClaimsEntity oldClaims = this.findById(claimsId);
            if(dwhCall){
                dwhClaimsService.sendMessage(oldClaims, com.doing.nemo.dwh.client.payload.request.EventTypeEnum.UPDATED);
            }
            cacheService.deleteClaimsStats(CACHENAME, STATSkEY);
            cacheService.deleteClaimsStats(CACHENAME, STATSEVIDENCE);
            cacheService.deleteClaimsStats(CACHENAME, STATSSTATUS);
            cacheService.deleteClaimsStats(CACHENAME,STATSTHEFT);
            return oldClaims;
        } else {
            LOGGER.info("Claims with id " + claimsId + " not found");
            throw new NotFoundException("Claims with id " + claimsId + " not found", MessageCode.CLAIMS_1010);
        }

    }


    public ClaimsEntity patchValidateAttachmentsCounterparty(String claimsId, String counterpartyId, List<IdRequestV1> idAttachments) {
        ClaimsEntity claimsEntity = claimsRepository.getOne(claimsId);
        if (claimsEntity != null) {
            Iterator<CounterpartyEntity> iterator = claimsEntity.getCounterparts().iterator();
            Boolean isFound = false;
            Boolean check = false;
            while (iterator.hasNext() && !isFound) {
                CounterpartyEntity counterparty = iterator.next();
                if (counterparty.getCounterpartyId().equals(counterpartyId)) {
                    for (Attachment attachment : counterparty.getAttachments()) {
                        for (IdRequestV1 attachmentIdRequestV1 : idAttachments) {
                            if (attachmentIdRequestV1.getId().equals(attachment.getFileManagerId())) {
                                attachment.setValidate(true);
                            }
                        }
                    }

                    for (Attachment attachment : counterparty.getAttachments()) {
                        if (!attachment.getValidate()) {
                            check = true;
                            isFound = true;
                            break;
                        }
                    }
                }
            }
            claimsEntity.setWithContinuation(check);
            claimsRepository.save(claimsEntity);
            if(dwhCall){
                dwhClaimsService.sendMessage(claimsEntity, com.doing.nemo.dwh.client.payload.request.EventTypeEnum.UPDATED);
            }
        } else {
            LOGGER.debug("Claims with id " + claimsId + " not found");
            throw new NotFoundException("Claims with id " + claimsId + " not found", MessageCode.CLAIMS_1010);
        }
        return claimsEntity;
    }

    public UploadFileResponseV1 setPdfOnFileManager(String claimsId, String pdfBase64, String fileManagerId) {
        if (pdfBase64 != null) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd_MM_yyyy");
            String dateCreated = simpleDateFormat.format(new Date());
            UploadFileRequestV1 uploadFileRequestV1 = new UploadFileRequestV1();
            uploadFileRequestV1.setUuid(fileManagerId);
            uploadFileRequestV1.setResourceId(claimsId);
            uploadFileRequestV1.setResourceType("claims");
            uploadFileRequestV1.setBlobType("pdf");
            uploadFileRequestV1.setFileContent(pdfBase64);
            uploadFileRequestV1.setDescription("CRASH_REPORT");
            uploadFileRequestV1.setFileName("Log_" + dateCreated + "_" + uploadFileRequestV1.getUuid() + ".pdf");

            UploadFileResponseV1 uploadFileResponseV1=null;
            try {
                uploadFileResponseV1= fileManagerService.octoFileManager(uploadFileRequestV1, claimsId);
            } catch (IOException e) {
                LOGGER.debug("Error Upload File: " + e.getMessage());
                throw new InternalException(MessageCode.CLAIMS_1003, e);
            }

            UploadFile uploadFile = UploadFileAdapter.adptUploadFileRequestToUploadFile(uploadFileRequestV1);
            List<UploadFile> uploadFileList = new ArrayList<>();
            uploadFileList.add(uploadFile);
            externalCommunicationService.uploadFile(uploadFileList,claimsId);
            return uploadFileResponseV1;
        }
        return null;
    }

    @Override
    public void sendEmailTheft() {
        List<ClaimsEntity> claimsEntityList = claimsRepositoryV1.findClaimsByDateAndVeichleType();

        for (ClaimsEntity claimsEntity : claimsEntityList) {
            if (claimsEntity.getStatus().equals(ClaimsStatusEnum.MANAGED) && claimsEntity.getComplaint() != null && claimsEntity.getComplaint().getDataAccident() != null && claimsEntity.getComplaint().getDataAccident().getTypeAccident() != null && claimsEntity.getComplaint().getDataAccident().getTypeAccident().equals(DataAccidentTypeAccidentEnum.FURTO_TOTALE)) {
                // claimsEntity.setStatus(ClaimsStatusEnum.WAIT_LOST_POSSESSION);
                List<EmailTemplateMessagingResponseV1> emailTemplateMessagingResponseV1List = messagingService.getMailTemplateByTypeEvent(EventTypeEnum.TAKE_IN_CHARGE_VALIDATED_THEFT, claimsEntity.getId());


                for (EmailTemplateMessagingResponseV1 emailTemplateMessagingResponseV1 : emailTemplateMessagingResponseV1List) {
                    if (claimsEntity.getDamaged() != null) {
                        if (claimsEntity.getDamaged().getVehicle() != null) {
                            if (claimsEntity.getDamaged().getVehicle().getLicensePlate() != null) {
                                emailTemplateMessagingResponseV1.setObject(emailTemplateMessagingResponseV1.getObject().replaceAll("<%VEHIPLATDAMA%>", claimsEntity.getDamaged().getVehicle().getLicensePlate()));
                                emailTemplateMessagingResponseV1.setBody(emailTemplateMessagingResponseV1.getBody().replaceAll("<%VEHIPLATDAMA%>", claimsEntity.getDamaged().getVehicle().getLicensePlate()));
                            }
                        }
                        if (claimsEntity.getDamaged().getDriver() != null) {
                            if (claimsEntity.getDamaged().getDriver().getLastname() != null)
                            {
                                emailTemplateMessagingResponseV1.setBody(emailTemplateMessagingResponseV1.getBody().replaceAll("<%DRIVSURNDAMA%>", claimsEntity.getDamaged().getDriver().getLastname()));
                            }
                        }

                    }
                    if (claimsEntity.getComplaint() != null && claimsEntity.getComplaint().getDataAccident() != null && claimsEntity.getComplaint().getDataAccident().getDateAccident() != null)
                        emailTemplateMessagingResponseV1.setBody(emailTemplateMessagingResponseV1.getBody().replaceAll("<%ACCIDATECOMP%>", claimsEntity.getComplaint().getDataAccident().getDateAccident().toString()));

                }


                String description = "";
                List<EmailTemplateMessagingRequestV1>  listTemplateEmail = MessagingAdapter.adptEmailTemplateMessagingRequestToEmailTemplateMessagingResponseList(emailTemplateMessagingResponseV1List);
                if(listTemplateEmail != null && !listTemplateEmail.isEmpty()){
                    List<EmailTemplateMessagingRequestV1> listTos = new LinkedList<>();
                    List<EmailTemplateMessagingRequestV1> listNotTos = new LinkedList<>();
                    for(EmailTemplateMessagingRequestV1 currentTemplate: listTemplateEmail){
                        if (currentTemplate.getTos() != null && !currentTemplate.getTos().isEmpty() && currentTemplate.getObject()!= null && !currentTemplate.getObject().isEmpty()) {
                            listTos.add(currentTemplate);
                        }else{
                            listNotTos.add(currentTemplate);
                        }
                    }

                    List<EmailTemplateMessagingRequestV1> emailTemplateMessaging = messagingService.splitEmailIfContainsUrl(listTos, claimsEntity.getId());
                    emailTemplateMessaging.addAll(listNotTos);

                    description = messagingService.sendMailAndCreateLogs(emailTemplateMessaging, null, claimsEntity.getCreatedAt());
                }




                Historical historical = new Historical(EventTypeEnum.TAKE_IN_CHARGE_VALIDATED_THEFT, claimsEntity.getStatus(), claimsEntity.getStatus(), claimsEntity.getComplaint().getDataAccident().getTypeAccident(), claimsEntity.getComplaint().getDataAccident().getTypeAccident(), DateUtil.getNowDate(), null, null, claimsEntity.getType().getValue(), claimsEntity.getType().getValue(), description);
                claimsEntity.addHistorical(historical);
                claimsRepository.save(claimsEntity);
                if(dwhCall){
                    dwhClaimsService.sendMessage(claimsEntity, com.doing.nemo.dwh.client.payload.request.EventTypeEnum.UPDATED);
                }
                //Elimina i template senza destinatari e i destinatari senza e-mail
                List<EmailTemplateMessagingResponseV1> emailTemplateMessagingFinal = new LinkedList<>();
                for (EmailTemplateMessagingResponseV1 emailTemplateMessagingResponseV1 : emailTemplateMessagingResponseV1List) {
                    if (emailTemplateMessagingResponseV1.getTos() != null && !emailTemplateMessagingResponseV1.getTos().isEmpty()) {
                        List<Identity> identityList = new LinkedList<>();
                        for (Identity identity : emailTemplateMessagingResponseV1.getTos()) {
                            if (identity.getEmail() != null && !identity.getEmail().isEmpty()) {
                                identityList.add(identity);
                            }
                        }

                        if (!identityList.isEmpty()) {
                            emailTemplateMessagingResponseV1.setTos(identityList);
                            emailTemplateMessagingFinal.add(emailTemplateMessagingResponseV1);
                        }

                    }
                }

                messagingService.sendEmail(MessagingAdapter.adptEmailTemplateMessagingRequestToEmailTemplateMessagingResponseList(emailTemplateMessagingFinal), null, claimsEntity.getCreatedAt());
            }
        }

    }

    @Override
    public void sendEmailTheftChangeStatus() throws IOException {
        List<ClaimsEntity> claimsEntityList = claimsRepositoryV1.findClaimsByDateAndVeichleTypeChangeStatus();

        for (ClaimsEntity claimsEntity : claimsEntityList) {
            ClaimsEntity oldClaims = new ClaimsEntity();
            oldClaims.setStatus(claimsEntity.getStatus());
            oldClaims.setType(claimsEntity.getType());
            oldClaims.setComplaint(new Complaint());
            oldClaims.getComplaint().setDataAccident(new DataAccident());
            oldClaims.getComplaint().getDataAccident().setTypeAccident(claimsEntity.getComplaint().getDataAccident().getTypeAccident());
            IncidentRequestV1 oldIncident = incidentAdapter.adptFromClaimsEntityToIncidentRequest(claimsEntity,null,false, false);
            if (claimsEntity.getStatus().equals(ClaimsStatusEnum.MANAGED) && claimsEntity.getComplaint() != null && claimsEntity.getComplaint().getDataAccident() != null && claimsEntity.getComplaint().getDataAccident().getTypeAccident() != null && claimsEntity.getComplaint().getDataAccident().getTypeAccident().equals(DataAccidentTypeAccidentEnum.FURTO_TOTALE)) {
                ClaimsStatusEnum oldStatus = claimsEntity.getStatus();
                claimsEntity.setStatus(ClaimsStatusEnum.WAIT_LOST_POSSESSION);
                cacheService.deleteClaimsStats(CACHENAME, STATSkEY);
                cacheService.deleteClaimsStats(CACHENAME, STATSEVIDENCE);
                cacheService.deleteClaimsStats(CACHENAME, STATSSTATUS);
                cacheService.deleteClaimsStats(CACHENAME, STATSTHEFT);

                List<EmailTemplateMessagingResponseV1> emailTemplateMessagingResponseV1List = messagingService.getMailTemplateByTypeEvent(EventTypeEnum.TAKE_IN_CHARGE_VALIDATED_THEFT, claimsEntity.getId());


                for (EmailTemplateMessagingResponseV1 emailTemplateMessagingResponseV1 : emailTemplateMessagingResponseV1List) {
                    if (claimsEntity.getDamaged() != null) {
                        if (claimsEntity.getDamaged().getVehicle() != null) {
                            if (claimsEntity.getDamaged().getVehicle().getLicensePlate() != null) {
                                emailTemplateMessagingResponseV1.setObject(emailTemplateMessagingResponseV1.getObject().replaceAll("<%VEHIPLATDAMA%>", claimsEntity.getDamaged().getVehicle().getLicensePlate()));
                                emailTemplateMessagingResponseV1.setBody(emailTemplateMessagingResponseV1.getBody().replaceAll("<%VEHIPLATDAMA%>", claimsEntity.getDamaged().getVehicle().getLicensePlate()));
                            }
                        }
                        if (claimsEntity.getDamaged().getDriver() != null) {
                            if (claimsEntity.getDamaged().getDriver().getLastname() != null)
                                emailTemplateMessagingResponseV1.setBody(emailTemplateMessagingResponseV1.getBody().replaceAll("<%DRIVSURNDAMA%>", claimsEntity.getDamaged().getDriver().getLastname()));
                        }

                    }
                    if (claimsEntity.getComplaint() != null && claimsEntity.getComplaint().getDataAccident() != null && claimsEntity.getComplaint().getDataAccident().getDateAccident() != null)
                        emailTemplateMessagingResponseV1.setBody(emailTemplateMessagingResponseV1.getBody().replaceAll("<%ACCIDATECOMP%>", claimsEntity.getComplaint().getDataAccident().getDateAccident().toString()));

                }


                String description = "";

                description = messagingService.sendMailAndCreateLogs(MessagingAdapter.adptEmailTemplateMessagingRequestToEmailTemplateMessagingResponseList(emailTemplateMessagingResponseV1List), null, claimsEntity.getCreatedAt());

                Historical historical = new Historical(EventTypeEnum.TAKE_IN_CHARGE_VALIDATED_THEFT, claimsEntity.getStatus(), claimsEntity.getStatus(), claimsEntity.getComplaint().getDataAccident().getTypeAccident(), claimsEntity.getComplaint().getDataAccident().getTypeAccident(), DateUtil.getNowDate(), null, null, claimsEntity.getType().getValue(), claimsEntity.getType().getValue(), description);
                claimsEntity.addHistorical(historical);
                claimsRepository.save(claimsEntity);
                if(dwhCall){
                    dwhClaimsService.sendMessage(claimsEntity, com.doing.nemo.dwh.client.payload.request.EventTypeEnum.UPDATED);
                }
                //Elimina i template senza destinatari e i destinatari senza e-mail
                List<EmailTemplateMessagingResponseV1> emailTemplateMessagingFinal = new LinkedList<>();
                for (EmailTemplateMessagingResponseV1 emailTemplateMessagingResponseV1 : emailTemplateMessagingResponseV1List) {
                    if (emailTemplateMessagingResponseV1.getTos() != null && !emailTemplateMessagingResponseV1.getTos().isEmpty()) {
                        List<Identity> identityList = new LinkedList<>();
                        for (Identity identity : emailTemplateMessagingResponseV1.getTos()) {
                            if (identity.getEmail() != null && !identity.getEmail().isEmpty()) {
                                identityList.add(identity);
                            }
                        }

                        if (!identityList.isEmpty()) {
                            emailTemplateMessagingResponseV1.setTos(identityList);
                            emailTemplateMessagingFinal.add(emailTemplateMessagingResponseV1);
                        }

                    }
                }
                //Intervento 6 - C
                externalCommunicationService.modifyIncidentAsync(claimsEntity.getId(),oldStatus, false,oldIncident, false, oldClaims);
                messagingService.sendEmail(MessagingAdapter.adptEmailTemplateMessagingRequestToEmailTemplateMessagingResponseList(emailTemplateMessagingFinal), null, claimsEntity.getCreatedAt());
            }
        }

    }

    @Override
    public void sendToClient() throws IOException {

        List<ClaimsNewEntity> claimsNewList = null;
        try {
            claimsNewList = claimsNewRepository.searchByStatusAndFlow(ClaimsStatusEnum.TO_SEND_TO_CUSTOMER,ClaimsFlowEnum.FNI);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //conversione nella vecchia entità
        List<ClaimsEntity> claimsList = converterClaimsService.wrapFromClaimsNewEntityListToClaimsEntityList(claimsNewList);
        if (CollectionUtils.isNotEmpty(claimsList)) {
            LOGGER.debug("[SENT_TO_CLIENT] Claims List SENT_TO_CLIENT size : " + claimsList.size());
            for (ClaimsEntity claimsEntity : claimsList) {

                ClaimsEntity oldClaims = new ClaimsEntity();
                oldClaims.setStatus(claimsEntity.getStatus());
                oldClaims.setType(claimsEntity.getType());
                oldClaims.setComplaint(new Complaint());
                oldClaims.getComplaint().setDataAccident(new DataAccident());
                oldClaims.getComplaint().getDataAccident().setTypeAccident(claimsEntity.getComplaint().getDataAccident().getTypeAccident());
                IncidentRequestV1 oldIncident = incidentAdapter.adptFromClaimsEntityToIncidentRequest(claimsEntity,null,false, false);
                //if (claimsEntity.getStatus().equals(ClaimsStatusEnum.MANAGED) && claimsEntity.getComplaint() != null && claimsEntity.getComplaint().getDataAccident() != null && claimsEntity.getComplaint().getDataAccident().getTypeAccident() != null && claimsEntity.getComplaint().getDataAccident().getTypeAccident().equals(DataAccidentTypeAccidentEnum.FURTO_TOTALE)) {
                    ClaimsStatusEnum oldStatus = claimsEntity.getStatus();

                    List<EmailTemplateMessagingResponseV1> emailTemplateMessagingResponseV1List = messagingService.getMailTemplateByTypeEvent(EventTypeEnum.SEND_TO_CLIENT, claimsEntity.getId());

                    /*****************  DA VERIFICARE *****************************************/
  /*                  for (EmailTemplateMessagingResponseV1 emailTemplateMessagingResponseV1 : emailTemplateMessagingResponseV1List) {
                        if (claimsEntity.getDamaged() != null) {
                            if (claimsEntity.getDamaged().getVehicle() != null) {
                                if (claimsEntity.getDamaged().getVehicle().getLicensePlate() != null) {
                                    emailTemplateMessagingResponseV1.setObject(emailTemplateMessagingResponseV1.getObject().replaceAll("<%VEHIPLATDAMA%>", claimsEntity.getDamaged().getVehicle().getLicensePlate()));
                                    emailTemplateMessagingResponseV1.setBody(emailTemplateMessagingResponseV1.getBody().replaceAll("<%VEHIPLATDAMA%>", claimsEntity.getDamaged().getVehicle().getLicensePlate()));
                                }
                            }
                            if (claimsEntity.getDamaged().getDriver() != null) {
                                if (claimsEntity.getDamaged().getDriver().getLastname() != null)
                                    emailTemplateMessagingResponseV1.setBody(emailTemplateMessagingResponseV1.getBody().replaceAll("<%DRIVSURNDAMA%>", claimsEntity.getDamaged().getDriver().getLastname()));
                            }

                        }
                        if (claimsEntity.getComplaint() != null && claimsEntity.getComplaint().getDataAccident() != null && claimsEntity.getComplaint().getDataAccident().getDateAccident() != null)
                            emailTemplateMessagingResponseV1.setBody(emailTemplateMessagingResponseV1.getBody().replaceAll("<%ACCIDATECOMP%>", claimsEntity.getComplaint().getDataAccident().getDateAccident().toString()));

                    }*/
                /*****************  DA VERIFICARE *****************************************/
                if (emailTemplateMessagingResponseV1List.size()>0) {

                    String description = "";
                    List<EmailTemplateMessagingRequestV1>  listTemplateEmail = MessagingAdapter.adptEmailTemplateMessagingRequestToEmailTemplateMessagingResponseList(emailTemplateMessagingResponseV1List);
                    if(listTemplateEmail != null && !listTemplateEmail.isEmpty()) {
                        List<EmailTemplateMessagingRequestV1> listTos = new LinkedList<>();
                        List<EmailTemplateMessagingRequestV1> listNotTos = new LinkedList<>();
                        for (EmailTemplateMessagingRequestV1 currentTemplate : listTemplateEmail) {

                            if (claimsEntity.getDamaged() != null) {
                                if (claimsEntity.getDamaged().getVehicle() != null) {
                                    if (claimsEntity.getDamaged().getVehicle().getLicensePlate() != null) {
                                        currentTemplate.setObject(currentTemplate.getObject().replaceAll("<%VEHIPLATDAMA%>", claimsEntity.getDamaged().getVehicle().getLicensePlate()));
                                        currentTemplate.setBody(currentTemplate.getBody().replaceAll("<%VEHIPLATDAMA%>", claimsEntity.getDamaged().getVehicle().getLicensePlate()));
                                    }
                                }
                                if (claimsEntity.getDamaged().getDriver() != null) {
                                    if (claimsEntity.getDamaged().getDriver().getLastname() != null)
                                        currentTemplate.setBody(currentTemplate.getBody().replaceAll("<%DRIVSURNDAMA%>", claimsEntity.getDamaged().getDriver().getLastname()));
                                }

                            }
                            if (claimsEntity.getComplaint() != null && claimsEntity.getComplaint().getDataAccident() != null && claimsEntity.getComplaint().getDataAccident().getDateAccident() != null)
                                currentTemplate.setBody(currentTemplate.getBody().replaceAll("<%ACCIDATECOMP%>", claimsEntity.getComplaint().getDataAccident().getDateAccident().toString()));



                            if (currentTemplate.getTos() != null && !currentTemplate.getTos().isEmpty() && currentTemplate.getObject() != null && !currentTemplate.getObject().isEmpty()) {
                                listTos.add(currentTemplate);
                            } else {
                                listNotTos.add(currentTemplate);
                            }



                        }

                        List<EmailTemplateMessagingRequestV1> emailTemplateMessaging = messagingService.splitEmailIfContainsUrl(listTos, claimsEntity.getId());
                        emailTemplateMessaging.addAll(listNotTos);




                        description = messagingService.sendMailAndCreateLogs(emailTemplateMessaging, null, claimsEntity.getCreatedAt());
                    }

                    Historical historical = new Historical(EventTypeEnum.SEND_TO_CLIENT, claimsEntity.getStatus(), ClaimsStatusEnum.CLOSED_TOTAL_REFUND, claimsEntity.getComplaint().getDataAccident().getTypeAccident(), claimsEntity.getComplaint().getDataAccident().getTypeAccident(), DateUtil.getNowDate(), null, ClaimsHistoricalUserEnum.SYSTEM_AUTOMATIC_SENDTOCLIENT.getValue() , claimsEntity.getType().getValue(), claimsEntity.getType().getValue(), description);

                    claimsEntity.setStatus(ClaimsStatusEnum.CLOSED_TOTAL_REFUND);
                    claimsEntity.setUpdateAt(DateUtil.getNowInstant());

            /*        //Elimina i template senza destinatari e i destinatari senza e-mail
                    List<EmailTemplateMessagingResponseV1> emailTemplateMessagingFinal = new LinkedList<>();
                    for (EmailTemplateMessagingResponseV1 emailTemplateMessagingResponseV1 : emailTemplateMessagingResponseV1List) {
                        if (emailTemplateMessagingResponseV1.getTos() != null && !emailTemplateMessagingResponseV1.getTos().isEmpty()) {
                            List<Identity> identityList = new LinkedList<>();
                            for (Identity identity : emailTemplateMessagingResponseV1.getTos()) {
                                if (identity.getEmail() != null && !identity.getEmail().isEmpty()) {
                                    identityList.add(identity);
                                }
                            }

                            if (!identityList.isEmpty()) {
                                emailTemplateMessagingResponseV1.setTos(identityList);
                                emailTemplateMessagingFinal.add(emailTemplateMessagingResponseV1);
                            }

                        }
                    }*/

                    claimsEntity.addHistorical(historical);


                    //converto nella nuova entità
                    ClaimsNewEntity claimsNewEntity = converterClaimsService.convertOldEntityToNewClaimsEntity(claimsEntity);
                    claimsNewRepository.save(claimsNewEntity);

                    //Intervento 6 - C
                    externalCommunicationService.modifyIncidentAsync(claimsEntity.getId(), oldStatus, false, oldIncident, false, oldClaims);
                    //messagingService.sendEmail(MessagingAdapter.adptEmailTemplateMessagingRequestToEmailTemplateMessagingResponseList(emailTemplateMessagingFinal), null, claimsEntity.getCreatedAt());



                    if (dwhCall) {
                        dwhClaimsService.sendMessage(claimsEntity, com.doing.nemo.dwh.client.payload.request.EventTypeEnum.UPDATED);
                    }
                    //}
                }
            }
            cacheService.deleteClaimsStats(CACHENAME, STATSkEY);
            cacheService.deleteClaimsStats(CACHENAME, STATSEVIDENCE);
            cacheService.deleteClaimsStats(CACHENAME, STATSSTATUS);
            cacheService.deleteClaimsStats(CACHENAME, STATSTHEFT);
        }

    }


    @Override
    public List<AntiTheftRequestNewEntity> getLogForClaim(String claimsId) {
        Optional<ClaimsNewEntity> claimsNewEntityOptional = claimsNewRepository.findById(claimsId);
        if(!claimsNewEntityOptional.isPresent()){
            throw new NotFoundException(MessageCode.CLAIMS_1010);
        }

        return claimsNewEntityOptional.get().getAntiTheftRequestEntities();
    }

    /*@Override
    public List<ClaimsEntity> patchIsWithReceptionsTheft(String claimsId) {
        List<ClaimsEntity> claimsToReturn = new LinkedList<>();
        String[] claimsIdList = claimsId.split(",");
        for(String id : claimsIdList){
            ClaimsEntity claimsEntity = claimsRepository.getOne(id);
            if(claimsEntity != null && claimsEntity.getTheft() != null && claimsEntity.getTheft().getWithReceptions()==true){
                claimsEntity.getTheft().setWithReceptions(false);
                claimsRepository.save(claimsEntity);
                claimsToReturn.add(claimsEntity);
            }
        }
        return claimsToReturn;
    }*/




    @Override
    public boolean checkIsCard(Damaged damaged, List<CounterpartyEntity> counterpartyList) {
        InsuranceCompanyEntity damagedCompany = null;
        if (damaged != null && damaged.getInsuranceCompany() != null && damaged.getInsuranceCompany().getTpl() != null && damaged.getInsuranceCompany().getTpl().getInsuranceCompanyId() != null) {
            Optional<InsuranceCompanyEntity> insuranceCompanyEntityOptional = insuranceCompanyRepository.findById(damaged.getInsuranceCompany().getTpl().getInsuranceCompanyId());
            if (insuranceCompanyEntityOptional.isPresent())
                damagedCompany = insuranceCompanyEntityOptional.get();
        }
        boolean isCard = true;
        if (damagedCompany != null && damagedCompany.getJoinCard() != null && damagedCompany.getJoinCard()) {
            if (counterpartyList != null && !counterpartyList.isEmpty()) {
                if (counterpartyList.size() > 1) return false;
                CounterpartyEntity counterparty = counterpartyList.get(0);
                if (counterparty.getInsuranceCompany() != null) {
                    if (counterparty.getInsuranceCompany().getEntity().getJoinCard() != null && counterparty.getInsuranceCompany().getEntity().getJoinCard())
                        isCard = true;
                }
            }
        } else isCard = false;
        return isCard;
    }

    //PER AFFIDO AUTOMATICO
    public Boolean checkIsCard(Damaged damaged, List<CounterpartyEntity> counterpartyList, DataAccidentTypeAccidentEnum typeAccident) {
        /*Boolean isCard = this.checkIsCard(damaged, counterpartyList);
         *//*if (isCard) {
            if (cardSet.contains(typeAccident)) {
                return true;
            }
            return false;
        }
        if (!cardSet.contains(typeAccident)) {
            return false;
        }
        return null;*//*
        if(isCard && cardSet.contains(typeAccident)){
            return true;
        }else if(isCard && !cardSet.contains(typeAccident) ){
            return null;
        }else if(!isCard){
            return false;
        }

        return false;*/
        return cardSet.contains(typeAccident);
    }

    //REFACTOR
    @Override
    public ClaimsEntity updateLinkable(String idClaims) {
        ClaimsEntity claimsEntity = null;
        ClaimsNewEntity claimsNewEntity = null;
        Optional<ClaimsNewEntity> claimsEntityOptional = claimsNewRepository.findById(idClaims);
        if (claimsEntityOptional.isPresent())
            claimsNewEntity = claimsEntityOptional.get();
        if (claimsNewEntity == null) {
            LOGGER.debug(MessageCode.CLAIMS_1010.value());
            throw new BadRequestException(MessageCode.CLAIMS_1010);
        }
        if (claimsNewEntity.getAuthorityLinkable() == null || !claimsNewEntity.getAuthorityLinkable())
            claimsNewEntity.setAuthorityLinkable(true);
        else claimsNewEntity.setAuthorityLinkable(false);

        claimsNewRepository.save(claimsNewEntity);

        claimsEntity = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntity);

        if(dwhCall){
            dwhClaimsService.sendMessage(claimsEntity, com.doing.nemo.dwh.client.payload.request.EventTypeEnum.UPDATED);
        }
        return claimsEntity;
    }


    @Override
    public Damaged getInfoChangeDate(ClaimsEntity claimsEntity, Date newDate) throws IOException {

        AntiTheftService antiTheftService = claimsEntity.getDamaged().getAntiTheftService();

        ContractInfoResponseV1 contractInfoResponseV1 = esbService.getContractByContractIdOrPlate(claimsEntity.getDamaged().getVehicle().getLicensePlate(), DateUtil.convertUTCDateToIS08601String(newDate),null);

        Damaged damaged = new Damaged();
        damaged.setDriver(DriverAdapter.adptDriverResponseToDriver(contractInfoResponseV1.getDriverResponse()));
        damaged.setContract(ContractAdapter.adptContractResponseToContract(contractInfoResponseV1.getContractResponse()));
        damaged.setVehicle(VehicleAdapter.adptVehicleResponseToVehicle(contractInfoResponseV1.getVehicleResponse()));
        damaged.setCustomer(CustomerAdapter.adptCustomerResponseToCustomer(contractInfoResponseV1.getCustomerResponse()));
        damaged.setFleetManagerList(FleetManagerAdpter.adptFromFMResponseToFM(contractInfoResponseV1.getFleetManagerResponseList()));
        damaged.setInsuranceCompany(InsuranceCompanyAdapter.adptInsuranceCompanyResponseToInsuranceCompany(contractInfoResponseV1.getInsuranceCompany()));
        damaged.setAntiTheftService(antiTheftService);

        return damaged;
    }

    //cambiamento di flusso con relativi cambiamenti di stati
    //entro solo se il flusso è cambiato
    @Override
    public ClaimsStatusEnum getStatusChangeFlow(ClaimsEntity claimsEntity, ClaimsFlowEnum claimsFlowOld, Boolean oldWithCounterparty) throws NoSuchMethodException {
        //setting della mappa
        FlowStatusAssociation flowStatusAssociation = flowStatusAssociationMap.get(claimsEntity.getStatus());
        ClaimsFlowEnum claimsFlowNew = claimsEntity.getType();
        ClaimsStatusEnum candidateStatus = null;
        if (claimsFlowNew.equals(ClaimsFlowEnum.FUL)) {
            candidateStatus = flowStatusAssociation.getFul();

        } else if (claimsFlowNew.equals(ClaimsFlowEnum.FCM)) {
            candidateStatus = flowStatusAssociation.getFcm();

        } else if (claimsFlowNew.equals(ClaimsFlowEnum.FNI)) {
            candidateStatus = flowStatusAssociation.getFni();
        } else {
            candidateStatus = claimsEntity.getStatus();
        }

        if(claimsFlowNew == ClaimsFlowEnum.FUL && oldWithCounterparty  && !claimsEntity.getWithCounterparty()){
            candidateStatus = ClaimsStatusEnum.MANAGED;
        }else if(claimsFlowNew == ClaimsFlowEnum.FUL && !oldWithCounterparty  && claimsEntity.getWithCounterparty()){
            candidateStatus = ClaimsStatusEnum.WAITING_FOR_AUTHORITY;
        }

        if(candidateStatus.equals(ClaimsStatusEnum.WAITING_FOR_AUTHORITY) && ClaimsServiceImpl.isAuthorityAndSxNumber(claimsEntity)) {
            if (claimsEntity.getType().equals(ClaimsFlowEnum.FNI)) {
                candidateStatus = ClaimsStatusEnum.TO_SEND_TO_CUSTOMER;
            } else {
                candidateStatus = ClaimsStatusEnum.TO_ENTRUST;
            }
        }

        return candidateStatus;
    }

    @Override
    public boolean checkPolicy(InsuranceCompany insuranceCompany, Date dateAccident, Customer customer) {
        if (dateAccident != null) {
            System.out.println(dateAccident);
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
            String formattedDate = format.format(dateAccident);
            Instant dateInstant = DateUtil.convertIS08601StringToUTCInstant(formattedDate);
            List<InsurancePolicyEntity> insurancePolicyEntityList = null;
            InsurancePolicyEntity insurancePolicyEntity = null;
            if (insuranceCompany != null && insuranceCompany.getTpl() != null) {
                String tariffId = insuranceCompany.getTpl().getTariffId();
                if (tariffId != null) {

                    insurancePolicyEntityList = insurancePolicyRepository.searchInsurancePolicyForGetContract(tariffId, dateInstant, PolicyTypeEnum.RCA);
                    System.out.println(insurancePolicyEntityList);
                    if (insurancePolicyEntityList != null && !insurancePolicyEntityList.isEmpty()) {
                        insurancePolicyEntity = insurancePolicyEntityList.get(0);
                        if (insurancePolicyEntity != null) return true;
                    }
                } else if (customer != null) {

                    String clientCode = customer.getCustomerId();
                    if (clientCode != null) {
                        insurancePolicyEntityList = insurancePolicyRepository.searchInsurancePolicyForGetContractByCustomer(Long.valueOf(clientCode), dateInstant, PolicyTypeEnum.RCA);
                        if (insurancePolicyEntityList != null && !insurancePolicyEntityList.isEmpty()) {
                            insurancePolicyEntity = insurancePolicyEntityList.get(0);
                            if (insurancePolicyEntity != null) return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    @Override
    public ClaimsEntity checkInfoClaimsForWaitingForValidation(ClaimsEntity claimsDraft, String username) {


        if (claimsDraft.getWithCounterparty() && claimsDraft.getCaiDetails() != null &&
                claimsDraft.getCaiDetails().getVehicleA() != null && claimsDraft.getCaiDetails().getVehicleB() != null &&
                claimsDraft.getCaiDetails().getDriverSide() != null
        ) {

            String note = new String();
            note += "VEICOLO A: ";
            note += CaiAdapter.adptCaiConditionToCaiNote(claimsDraft.getCaiDetails().getVehicleA());
            note += "\nVEICOLO B: ";
            note += CaiAdapter.adptCaiConditionToCaiNote(claimsDraft.getCaiDetails().getVehicleB());

            if (claimsDraft.getDamaged().getImpactPoint() != null)
                claimsDraft.getDamaged().getImpactPoint().setIncidentDescription(note);

            if (claimsDraft.getComplaint() != null && claimsDraft.getComplaint().getDataAccident() != null) {
                if (claimsDraft.getCaiDetails().getVehicleA() != null && claimsDraft.getCaiDetails().getVehicleB() != null) {
                    Boolean isCaiSignedA = false;
                    Boolean isCaiSignedB = false;

                    if (claimsDraft.getDamaged() != null) {
                        isCaiSignedA = claimsDraft.getDamaged().getCaiSigned();
                    }
                    if (claimsDraft.getCounterparts() != null && claimsDraft.getCounterparts().size() > 0) {
                        isCaiSignedB = claimsDraft.getCounterparts().get(0).getCaiSigned();
                    }

                    if (isCaiSignedA == null) {
                        isCaiSignedA = false;
                    }

                    if (isCaiSignedB == null) {
                        isCaiSignedB = false;
                    }

                    Map<String, Object> caiResult = this.getFlowDetailsByCai(claimsDraft.getCaiDetails(), claimsDraft.getDamaged().getContract().getContractType(), claimsDraft.getDamaged(), claimsDraft.getCounterparts(), isCaiSignedA, isCaiSignedB);
                    DataAccident dataAccident = claimsDraft.getComplaint().getDataAccident();

                    dataAccident.setTypeAccident((DataAccidentTypeAccidentEnum) caiResult.get("type"));
                    Complaint complaint = claimsDraft.getComplaint();
                    complaint.setDataAccident(dataAccident);
                    claimsDraft.setComplaint(complaint);
                }

            }
        }


        Damaged damaged = claimsDraft.getDamaged();
        //mappatura del flusso attraverso il tipo contratto e il tipo sinistro ottenuti dalle tabelle di websin
        try {
            ContractTypeEntity contractTypeEntity = contractService.getContractType(damaged.getContract().getContractType());
            if(contractTypeEntity == null){
                LOGGER.debug(MessageCode.CLAIMS_1010.value());
                throw new BadRequestException(MessageCode.CLAIMS_1010);
            }
            if (contractTypeEntity == null || !contractTypeEntity.getFlagWS()) {
                LOGGER.debug(MessageCode.CLAIMS_1042.value());
                throw new BadRequestException(MessageCode.CLAIMS_1042);
            } else {
                claimsDraft.setType(contractService.getPersonalRiskFlowType(damaged.getContract().getContractType(),
                        claimsDraft.getComplaint().getDataAccident().getTypeAccident(),
                        damaged.getCustomer().getCustomerId())); /* tipo di flusso */
                if (claimsDraft.getType().equals(ClaimsFlowEnum.NO)) {
                    LOGGER.debug("It's not possible insert a claim with NO flow");
                    throw new BadRequestException("It's not possible insert a claim with NO flow", MessageCode.CLAIMS_1057);
                }
            }
        } catch (BadRequestException e) {
            LOGGER.debug(e.getMessage());
            throw e;
        }

        Complaint complaintClaims = claimsDraft.getComplaint();
        if (damaged != null) {
            InsuranceCompany insuranceCompany = damaged.getInsuranceCompany();
            Customer customer = damaged.getCustomer();
            if (complaintClaims != null) {
                DataAccident dataAccident = complaintClaims.getDataAccident();
                if (dataAccident != null) {
                    Date date = dataAccident.getDateAccident();
                    if (!this.checkPolicy(insuranceCompany, date, customer)) {
                        LOGGER.debug(MessageCode.CLAIMS_1054.value());
                        throw new BadRequestException(MessageCode.CLAIMS_1054);
                    }
                }
            }
        }

        if (damaged != null && complaintClaims != null) {
            if (damaged.getVehicle() != null)
                claimsDraft.getComplaint().setPlate(damaged.getVehicle().getLicensePlate());

            if (damaged.getCustomer() != null)
                claimsDraft.getComplaint().setClientId(damaged.getCustomer().getCustomerId());
        }

        //di defaul è in waiting_for_validation

        if (claimsDraft.getCounterparts() != null && !claimsDraft.getCounterparts().isEmpty()) {
            claimsDraft.setStatus(ClaimsStatusEnum.WAITING_FOR_VALIDATION);
            claimsDraft.setAuthorityLinkable(false);
        } else {
            //flusso senza controparti va in automatico in autogestione
            claimsDraft.setStatus(ClaimsStatusEnum.MANAGED);
            claimsDraft.setAuthorityLinkable(true);
        }

        if (claimsDraft.getCounterparts() != null) {
            String plate = null;

            if (claimsDraft.getDamaged() != null && claimsDraft.getDamaged().getVehicle() != null && claimsDraft.getDamaged().getVehicle().getLicensePlate() != null)
                plate = claimsDraft.getDamaged().getVehicle().getLicensePlate();

            if (plate != null) {

                for (CounterpartyEntity counterparty : claimsDraft.getCounterparts()) {

                    if (counterparty.getVehicle() != null && counterparty.getVehicle().getLicensePlate() != null) {

                        if (counterparty.getVehicle().getLicensePlate().equals(plate)) {
                            LOGGER.debug(MessageCode.CLAIMS_1041.value());
                            throw new BadRequestException(MessageCode.CLAIMS_1041);
                        }
                    }

                    if (counterparty.getUserCreate() == null || counterparty.getUserCreate().equalsIgnoreCase(""))
                        counterparty.setUserCreate(claimsDraft.getUserId());
                }
            }
        }

        //mappatura del flusso attraverso il tipo contratto e il tipo sinistro ottenuti dalle tabelle di websin

        if (claimsDraft.getTheft() != null) {
            claimsDraft.getTheft().setWithReceptions(false);
        }

        if(claimsDraft.getCounterparts() != null && claimsDraft.getCounterparts().size() == 1){
            if(activeSet.contains(claimsDraft.getComplaint().getDataAccident().getTypeAccident()))
                claimsDraft.getCounterparts().get(0).setResponsible(true);
        }

        Instant nowInstant = DateUtil.getNowInstant();
        claimsDraft.setUpdateAt(nowInstant);
        claimsRepository.save(claimsDraft);
        if(dwhCall){
            dwhClaimsService.sendMessage(claimsDraft, com.doing.nemo.dwh.client.payload.request.EventTypeEnum.UPDATED);
        }
        String codAntiTheftService = "";
        if (damaged.getAntiTheftService() != null) {
            //recupero localizzatore da tabella setting AntiTheftServiceEntity
            //da capire se avremo mai un localizzatore per il furto e se ci viene passato un tipo
            if (damaged.getAntiTheftService().getRegistryList() != null && !damaged.getAntiTheftService().getRegistryList().isEmpty()) {

                //for(Registry currentRegistry : damaged.getAntiTheftService().getRegistryList()){
                Registry currentRegistry = damaged.getAntiTheftService().getRegistryList().get(0);
                codAntiTheftService = AntiTheftServiceAdapter.adptProviderToAntiTheftServiceEntity(currentRegistry.getCodPack());
                AntiTheftServiceEntity antiTheftServiceEntity = antiTheftServiceRepository.findAntiTheftByCodAntiTheftService(codAntiTheftService);
                AntiTheftService antiTheftService = AntiTheftServiceAdapter.adptAntiTheftServiceEntityToAntiTheftService(antiTheftServiceEntity);

                if (antiTheftService != null) {

                    String idAntiTheftRequest = UUID.randomUUID().toString();
                    //TO REFACTOR
                    //antiTheftRequestService.insertAntiTheftRequest(idAntiTheftRequest, claimsDraft, antiTheftService.getProviderType(), "1");

                    antiTheftService.setRegistryList(damaged.getAntiTheftService().getRegistryList());
                    claimsDraft.getDamaged().setAntiTheftService(antiTheftService);
                    //this.callToOctoAsync(claimsDraft, claimsDraft.getStatus(), new AntiTheftRequest(), claimsDraft.getUserId(), username, idAntiTheftRequest);


                }
            }
        }


        if (claimsDraft.getType() != null && claimsDraft.getComplaint() != null && claimsDraft.getComplaint().getDataAccident() != null && claimsDraft.getComplaint().getDataAccident().getTypeAccident() != null) {

            RecoverabilityEntity recoverabilityEntity = recoverabilityService.getRecoverabilityByFlowAndTypeClaim(claimsDraft.getType().getValue().toUpperCase(), claimsDraft.getComplaint().getDataAccident().getTypeAccident());
            claimsDraft.getComplaint().getDataAccident().setRecoverability(recoverabilityEntity.getRecoverability());
            claimsDraft.getComplaint().getDataAccident().setRecoverabilityPercent(recoverabilityEntity.getPercentRecoverability());

        }

        return claimsDraft;
    }

    @Override
    public void recoverCallToOcto(ClaimsNewEntity claimsNewEntity, ClaimsStatusEnum previousStatus, AntiTheftRequest antiTheftRequest,String userId, String userName, String idAntiTheftRequest) {


        String responseType = "-1";
        String responseMessage = "Impossibile contattare il servizio";

        antiTheftRequestService.updateAntiTheftRequest(idAntiTheftRequest,null,null,responseType,null,responseMessage);
        ClaimsEntity claimsEntity = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewRepository.getOne(claimsNewEntity.getId()));

        if(dwhCall){
            dwhClaimsService.sendMessage(claimsEntity, com.doing.nemo.dwh.client.payload.request.EventTypeEnum.UPDATED);
        }
    }

    @Override
    public void callToOctoAsync(ClaimsNewEntity claimsNewEntity, ClaimsStatusEnum previousStatus, AntiTheftRequest antiTheftRequest, String userId, String userName, String idAntiTheftRequest) {

        String fileManagerId;
        String pdfBase64 = null;
        AntiTheftServiceEntity antiTheftService = claimsNewEntity.getAntiTheftServiceEntity();
        Boolean isValidated = false;
        if(claimsNewEntity.getStatus().equals(ClaimsStatusEnum.MANAGED)){
            isValidated = true;
        }

        if (claimsNewEntity.getClaimsDamagedRegistryEntityList() != null && !claimsNewEntity.getClaimsDamagedRegistryEntityList().isEmpty() && claimsNewEntity.getClaimsDamagedRegistryEntityList().get(0).getVoucherId()!=null) {
            //Dati per chiamata
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            simpleDateFormat.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));
            String dateTime = simpleDateFormat.format(DateUtil.convertIS08601StringToUTCDate(DateUtil.convertUTCInstantToIS08601String(claimsNewEntity.getDateAccident())));
            String plate = claimsNewEntity.getPlate();

            //Recuperiamo il registry e voucher
            ClaimsDamagedRegistryEntity registry = claimsNewEntity.getClaimsDamagedRegistryEntityList().get(0);
            Long voucher = registry.getVoucherId().longValue();


            GetVoucherInfoResponse responseTEXA = null;
            com.doing.nemo.claims.crashservices.octo.GetVoucherInfoResponse responseOCTO = null;
            String responseType = null;
            String pdf = null;
            String responseMessage = null;


            //SCELTA DEL SERVIZIO
            Boolean isChoosen = false;
            if(antiTheftService.getProviderType()!=null && antiTheftService.getProviderType().equalsIgnoreCase(ProviderTypeEnum.TEXA.getValue())){
                isChoosen = true;
                try {
                    CrashService crashService = new CrashService(new URL(texaEndPoint));
                    ICrashService dossierCrashService = crashService.getBasicHttpBindingICrashService();
                    GetVoucherInfoRequest request  = TexaAdapter.adptFromInputToTexaRequest(claimsNewEntity.getPracticeId().toString(), texaCompanyCode, dateTime, texaUser, texaPwd, texaRequestType, plate,voucher);
                    LOGGER.debug("TEXA Request: "+request);
                    responseTEXA = dossierCrashService.getVoucherInfo(request);
                    LOGGER.debug("TEXA Endpoint: "+texaEndPoint);
                    LOGGER.debug("TEXA Response: "+responseTEXA);
                    responseType = responseTEXA.getRespCode() != null && responseTEXA.getRespCode().getValue()!=null ? responseTEXA.getRespCode().getValue().toString(): null;
                } catch (Exception e){
                    LOGGER.error("TEXA Error: "+ e.getMessage());
                    //antiTheftRequest.setResponseType("-1");
                    responseType = "-1";
                }
                //Se la chiamata va a buon fine
                /*if(!antiTheftRequest.getResponseType().equals("-1"))
                    antiTheftRequest = TexaAdapter.adptFromTexaResponseToAntiTheftRequest(response, antiTheftRequest);*/
            }
            else if(antiTheftService.getProviderType()!=null && antiTheftService.getProviderType().equalsIgnoreCase(ProviderTypeEnum.OCTO.getValue())){
                isChoosen = true;

                try {
                    com.doing.nemo.claims.crashservices.octo.GetVoucherInfoRequest request  = OctoAdapter.adptFromInputToOctoRequest(claimsNewEntity.getPracticeId().toString(), octoCompanyCode, dateTime, octoUser, octoPwd, octoRequestType, plate,voucher);
                    LOGGER.debug("OCTO Request: "+request);
                    responseOCTO = octoClient.callGetVoucherInfo(request);
                    LOGGER.debug("OCTO Response: "+responseOCTO);

                    responseType = responseOCTO.getRespCode().toString();
                } catch (Exception e){
                    LOGGER.error("OCTO Error: "+ e.getMessage());
                    //antiTheftRequest.setResponseType("-1");
                    responseType = "-1";
                }
                //Se la chiamata va a buon fine
                /*if(!antiTheftRequest.getResponseType().equals("-1"))
                    antiTheftRequest = OctoAdapter.adptFromOctoResponseToAntiTheftRequest(response, antiTheftRequest);*/
            }
            Attachment attachment = null;
            if (isChoosen && responseType.equalsIgnoreCase("0")) {
                //caso OK chiamata octo/texa
                fileManagerId = UUID.randomUUID().toString();
                pdfBase64 = null;
                if(responseOCTO != null){
                    pdfBase64 = responseOCTO.getPdfReport();
                }else if(responseTEXA != null){
                    if(responseTEXA.getPdfReport() != null && responseTEXA.getPdfReport().getValue()!=null) {
                        pdfBase64 = Base64Utils.encodeToString(responseTEXA.getPdfReport().getValue());
                    }
                }

                responseType = "1";
                pdf = fileManagerId;
                responseMessage = "Operazione correttamente eseguita";
                if (pdfBase64 != null) {
                    try {
                        UploadFileResponseV1 uploadFileResponseV1 = this.setPdfOnFileManager(claimsNewEntity.getId(), pdfBase64, fileManagerId);
                        attachment = new Attachment(
                                uploadFileResponseV1.getUuid(),
                                uploadFileResponseV1.getFileName(),
                                uploadFileResponseV1.getBlobName(),
                                uploadFileResponseV1.getFileSize(),
                                uploadFileResponseV1.getMimeType(),
                                uploadFileResponseV1.getDescription(),
                                DateUtil.getNowDate(),
                                userId,
                                userName,
                                uploadFileResponseV1.getThumbnailId(),
                                false,
                                null,
                                false,
                                isValidated
                        );


                    } catch (Exception ex) {
                        responseType = "-1";
                        pdf = null;
                        responseMessage = "Unable to upload pfd to filemanager";
                        LOGGER.debug("Unable to upload pfd to filemanager for crash service on claim's id " + claimsNewEntity.getId());

                    }

                }

            }
            else if (isChoosen && responseType.equalsIgnoreCase("-1")) {
                //exception funzione di recovery
                LOGGER.debug(MessageCode.CLAIMS_1019.value() + " - code: CLAIMS_1019");
                throw new InternalException(MessageCode.CLAIMS_1019);
            }
            else if(isChoosen){
                //caso in cui chiamata ad octo/texa Ok ma non è stato recuperato il crash report

                // da mandare responseType = 1 in aggiornamento alla funzione

                responseType = "1";
                pdf = null;
                if(responseOCTO != null) {
                    responseMessage = responseOCTO.getRespMsg();
                }else if(responseTEXA != null ){
                    responseMessage = responseTEXA.getRespMsg()!= null ? responseTEXA.getRespMsg().getValue(): null;
                }

            }
            else {
                LOGGER.debug("SOAP Call: Provider nella tabella antitheftservice corrispondente al codpack " + registry.getCodPack()  +" non è di tipo OCTO ne TEXA");
                responseType = "1";
                pdf = null;
                responseMessage = "Provider nella tabella antitheftservice corrispondente al codpack " + registry.getCodPack()  +" non è di tipo OCTO ne TEXA";


            }

            antiTheftRequestService.updateAntiTheftRequest (idAntiTheftRequest, responseTEXA, responseOCTO,  responseType, attachment , responseMessage );

            ClaimsEntity claimsEntity = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntity);
            if(dwhCall){
                dwhClaimsService.sendMessage(claimsEntity, com.doing.nemo.dwh.client.payload.request.EventTypeEnum.UPDATED);
            }



        }
    }



    @Override
    public void callToOcto(ClaimsNewEntity claimsNewEntity, ClaimsStatusEnum previousStatus, AntiTheftRequest antiTheftRequest, String userId, String userName, String idAntiTheftRequest ) {

        String fileManagerId;
        String pdfBase64;

        AntiTheftServiceEntity antiTheftService = claimsNewEntity.getAntiTheftServiceEntity();

        Boolean isValidated = false;
        if(claimsNewEntity.getStatus().equals(ClaimsStatusEnum.MANAGED)){
            isValidated = true;
        }

        if (claimsNewEntity.getClaimsDamagedRegistryEntityList() != null && !claimsNewEntity.getClaimsDamagedRegistryEntityList().isEmpty() && claimsNewEntity.getClaimsDamagedRegistryEntityList().get(0).getVoucherId()!=null) {
            //DATI PER CHIAMATA SOAP
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            simpleDateFormat.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));
            String dateTime = simpleDateFormat.format(DateUtil.convertIS08601StringToUTCDate(DateUtil.convertUTCInstantToIS08601String(claimsNewEntity.getDateAccident())));
            String plate = claimsNewEntity.getPlate();

            //Recuperiamo il registry e voucher
            ClaimsDamagedRegistryEntity registry = claimsNewEntity.getClaimsDamagedRegistryEntityList().get(0);
            Long voucher = registry.getVoucherId().longValue();




            //SCELTA DEL SERVIZIO
            Boolean isChoosen = false;


            GetVoucherInfoResponse responseTEXA = null;
            com.doing.nemo.claims.crashservices.octo.GetVoucherInfoResponse responseOCTO = null;
            String responseType = null;
            String pdf = null;
            String responseMessage = null;
            Attachment attachment = null;
            if(antiTheftService.getProviderType()!=null && antiTheftService.getProviderType().equalsIgnoreCase(ProviderTypeEnum.TEXA.getValue())){
                isChoosen=true;

                try {
                    CrashService crashService = new CrashService(new URL(texaEndPoint));
                    ICrashService dossierCrashService = crashService.getBasicHttpBindingICrashService();
                    GetVoucherInfoRequest request  = TexaAdapter.adptFromInputToTexaRequest(claimsNewEntity.getPracticeId().toString(), texaCompanyCode, dateTime, texaUser, texaPwd, texaRequestType, plate,voucher);
                    LOGGER.debug("TEXA Request: "+request);
                    responseTEXA = dossierCrashService.getVoucherInfo(request);
                    LOGGER.debug("TEXA Endpoint: "+texaEndPoint);
                    LOGGER.debug("TEXA Response: "+responseTEXA);
                    responseType = responseTEXA.getRespCode() != null && responseTEXA.getRespCode().getValue()!=null ? responseTEXA.getRespCode().getValue().toString(): null;
                } catch (Exception e){
                    LOGGER.error("TEXA Error: "+ e.getMessage());
                    responseType = "-1";
                }

            }
            else if(antiTheftService.getProviderType()!=null &&  antiTheftService.getProviderType().equalsIgnoreCase(ProviderTypeEnum.OCTO.getValue())){
                isChoosen=true;
                try {
                    com.doing.nemo.claims.crashservices.octo.GetVoucherInfoRequest request  = OctoAdapter.adptFromInputToOctoRequest(claimsNewEntity.getPracticeId().toString(), octoCompanyCode, dateTime, octoUser, octoPwd, octoRequestType, plate,voucher);
                    LOGGER.debug("OCTO Request: "+request);
                    responseOCTO = octoClient.callGetVoucherInfo(request);
                    LOGGER.debug("OCTO Response: "+responseOCTO);
                    responseType = responseOCTO.getRespCode().toString();
                } catch (Exception e){
                    LOGGER.error("OCTO Error: "+ e.getMessage());
                    responseType = "-1";
                }

            }

            if (isChoosen && responseType.equalsIgnoreCase("0")) {

                fileManagerId = UUID.randomUUID().toString();
                pdfBase64 = null;
                if(responseOCTO != null){
                    pdfBase64 = responseOCTO.getPdfReport();
                }else if(responseTEXA != null){
                    if(responseTEXA.getPdfReport() != null && responseTEXA.getPdfReport().getValue()!=null) {
                        pdfBase64 = Base64Utils.encodeToString(responseTEXA.getPdfReport().getValue());
                    }
                }

                responseType = "1";
                pdf = fileManagerId;
                responseMessage = "Operazione correttamente eseguita";

                if (pdfBase64 != null) {
                    try {
                        UploadFileResponseV1 uploadFileResponseV1 = this.setPdfOnFileManager(claimsNewEntity.getId(), pdfBase64, fileManagerId);
                        attachment = new Attachment(
                                uploadFileResponseV1.getUuid(),
                                uploadFileResponseV1.getFileName(),
                                uploadFileResponseV1.getBlobName(),
                                uploadFileResponseV1.getFileSize(),
                                uploadFileResponseV1.getMimeType(),
                                uploadFileResponseV1.getDescription(),
                                DateUtil.getNowDate(),
                                userId,
                                userName,
                                uploadFileResponseV1.getThumbnailId(),
                                false,
                                null,
                                false,
                                isValidated
                        );

                    } catch (Exception ex) {
                        responseType = "-1";
                        pdf = null;
                        responseMessage = "Unable to upload pfd to filemanager";
                        LOGGER.debug("Unable to upload pfd to filemanager for crash service on claim's id " + claimsNewEntity.getId());
                    }
                }

            } else if (isChoosen && responseType.equalsIgnoreCase("-1")) {
                LOGGER.debug(MessageCode.CLAIMS_1019.value());
                throw new InternalException(MessageCode.CLAIMS_1019);

            } else if(isChoosen) {
                //caso in cui chiamata ad octo/texa Ok ma non è stato recuperato il crash report

                // da mandare responseType = 1 in aggiornamento alla funzione

                responseType = "1";
                pdf = null;
                if(responseOCTO != null) {
                    responseMessage = responseOCTO.getRespMsg();
                }else if(responseTEXA != null ){
                    responseMessage = responseTEXA.getRespMsg()!= null ? responseTEXA.getRespMsg().getValue(): null;
                }
            } else {
                LOGGER.debug("SOAP Call: Provider nella tabella antitheftservice corrispondente al codpack " + registry.getCodPack()  +" non è di tipo OCTO ne TEXA");
                responseType = "1";
                pdf = null;
                responseMessage = "Provider nella tabella antitheftservice corrispondente al codpack " + registry.getCodPack()  +" non è di tipo OCTO ne TEXA";
            }

            antiTheftRequestService.updateAntiTheftRequest (idAntiTheftRequest, responseTEXA, responseOCTO,  responseType,  attachment, responseMessage );

            ClaimsEntity claimsEntity = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntity);

            if(dwhCall){
                dwhClaimsService.sendMessage(claimsEntity, com.doing.nemo.dwh.client.payload.request.EventTypeEnum.UPDATED);
            }
            //  }
        }
    }

    @Override
    public AntiTheftServiceEntity getAntiTheftServiceByRegistry (ClaimsNewEntity claimsEntity){
        if( claimsEntity.getClaimsDamagedRegistryEntityList()!=null && !claimsEntity.getClaimsDamagedRegistryEntityList().isEmpty()) {
            ClaimsDamagedRegistryEntity currentRegistry = claimsEntity.getClaimsDamagedRegistryEntityList().get(0);
            String codAntiTheftService = AntiTheftServiceAdapter.adptProviderToAntiTheftServiceEntity(currentRegistry.getCodPack());
            AntiTheftServiceEntity antiTheftServiceEntity = antiTheftServiceRepository.findAntiTheftByCodAntiTheftService(codAntiTheftService);
            return antiTheftServiceEntity;
        }
        return null;
    }
    @Override
    public Boolean isPai (ClaimsEntity claimsEntity) {
        Boolean isFound = false;
        if (claimsEntity.getType().equals(ClaimsFlowEnum.FUL) || claimsEntity.getType().equals(ClaimsFlowEnum.FNI)) {
            //if (claimsEntity.getCounterparts() == null || claimsEntity.getCounterparts().isEmpty()) {
            if (claimsEntity.getDamaged() != null &&
                    claimsEntity.getDamaged().getInsuranceCompany() != null &&
                    claimsEntity.getDamaged().getInsuranceCompany().getPai() != null &&
                    claimsEntity.getDamaged().getInsuranceCompany().getPai().getInsurancePolicyId() != null
            ) {
                if (claimsEntity.getWoundedList() != null) {
                    Iterator<Wounded> iterator = claimsEntity.getWoundedList().iterator();
                    while (iterator.hasNext() && !isFound) {
                        Wounded currentWounded = iterator.next();
                        if (currentWounded.getType().equals(WoundedTypeEnum.DRIVER) ) {
                            isFound = true;
                        }
                    }
                }

            }
            //}
        }
        return isFound;
    }

    @Override
    public ClaimsEntity checkStatusByFlowExternalAndDraft(ClaimsEntity claimsEntity) {
        if (claimsEntity.getType().equals(ClaimsFlowEnum.FCM)) {
            claimsEntity.setStatus(ClaimsStatusEnum.WAITING_FOR_VALIDATION);
            claimsEntity.setAuthorityLinkable(false);
        } else if (claimsEntity.getType().equals(ClaimsFlowEnum.FUL) || claimsEntity.getType().equals(ClaimsFlowEnum.FNI)) {
            if (claimsEntity.getCounterparts() != null && !claimsEntity.getCounterparts().isEmpty()) {
                claimsEntity.setStatus(ClaimsStatusEnum.WAITING_FOR_VALIDATION);
                claimsEntity.setAuthorityLinkable(false);
            } else {
                ClaimsStatusEnum paiStatus;
                if (claimsEntity.getDamaged().getInsuranceCompany().getPai() != null) {

                    Boolean isFound = false;

                    if (claimsEntity.getWoundedList() != null) {
                        Iterator<Wounded> iterator = claimsEntity.getWoundedList().iterator();
                        while (iterator.hasNext() && !isFound) {
                            Wounded currentWounded = iterator.next();
                            if (currentWounded.getType().equals(WoundedTypeEnum.DRIVER) ) {
                                isFound = true;
                            }
                        }
                    }

                    if(isFound){
                        paiStatus = ClaimsStatusEnum.WAITING_FOR_VALIDATION;
                        claimsEntity.setAuthorityLinkable(false);
                    } else {
                        paiStatus = ClaimsStatusEnum.MANAGED;
                        claimsEntity.setAuthorityLinkable(true);
                    }

                } else {
                    paiStatus = ClaimsStatusEnum.MANAGED;
                    claimsEntity.setAuthorityLinkable(true);
                }
                claimsEntity.setStatus(paiStatus);
            }
        }
        return claimsEntity;
    }


   /* @Override
    public ClaimsEntity checkStatusByFlowExternalAndDraftLog (ClaimsEntity claimsEntity, String userName){
        if(claimsEntity.getType().equals(ClaimsFlowEnum.FCM)){
            Historical historical = new Historical(EventTypeEnum.COMPLAINT_INSERTION, claimsEntity.getStatus(), ClaimsStatusEnum.WAITING_FOR_VALIDATION, claimsEntity.getComplaint().getDataAccident().getTypeAccident(), claimsEntity.getComplaint().getDataAccident().getTypeAccident(), DateUtil.getNowDate(), claimsEntity.getUserId(),  userName, claimsEntity.getType().getValue(), claimsEntity.getType().getValue(),  null);
            claimsEntity.addHistorical(historical);
        } else if(claimsEntity.getType().equals(ClaimsFlowEnum.FUL) || claimsEntity.getType().equals(ClaimsFlowEnum.FNI)){
            if(claimsEntity.getCounterparty()!=null && !claimsEntity.getCounterparty().isEmpty()){
                Historical historical = new Historical(EventTypeEnum.COMPLAINT_INSERTION, claimsEntity.getStatus(), ClaimsStatusEnum.WAITING_FOR_VALIDATION, claimsEntity.getComplaint().getDataAccident().getTypeAccident(), claimsEntity.getComplaint().getDataAccident().getTypeAccident(), DateUtil.getNowDate(), claimsEntity.getUserId(),  userName, claimsEntity.getType().getValue(), claimsEntity.getType().getValue(),  null);
                claimsEntity.addHistorical(historical);
            } else {
                ClaimsStatusEnum paiStatus;
                if(claimsEntity.getDamaged().getInsuranceCompany().getPai()!=null){
                    paiStatus = ClaimsStatusEnum.WAITING_FOR_VALIDATION;
                } else {
                    paiStatus = ClaimsStatusEnum.MANAGED;
                }
                Historical historical = new Historical(EventTypeEnum.COMPLAINT_INSERTION, claimsEntity.getStatus(), paiStatus, claimsEntity.getComplaint().getDataAccident().getTypeAccident(), claimsEntity.getComplaint().getDataAccident().getTypeAccident(), DateUtil.getNowDate(), claimsEntity.getUserId(),  userName, claimsEntity.getType().getValue(), claimsEntity.getType().getValue(),  null);
                claimsEntity.addHistorical(historical);
            }
        }
        return claimsEntity;
    }*/

    private String sendEmailInternalByEventType(EventTypeEnum eventTypeEnum, List<EmailTemplateMessagingValidatedRequestV1> emailTemplateListRequestV1List, ClaimsEntity claimsEntity) {
        String description = "";
        if (emailTemplateListRequestV1List == null) {
            return description;
        }
        emailTemplateListRequestV1List = addPlaceholderPracticeId(emailTemplateListRequestV1List, claimsEntity);
        for (EmailTemplateMessagingValidatedRequestV1 emailTemplateMessagingValidatedRequestV1 : emailTemplateListRequestV1List) {
            if (emailTemplateMessagingValidatedRequestV1.getEventType().equals(eventTypeEnum)) {
                if (emailTemplateMessagingValidatedRequestV1.getTemplateList() != null && !emailTemplateMessagingValidatedRequestV1.getTemplateList().isEmpty()) {

                    List<EmailTemplateMessagingRequestV1> listTos = new LinkedList<>();
                    List<EmailTemplateMessagingRequestV1> listNotTos = new LinkedList<>();
                    for(EmailTemplateMessagingRequestV1 currentTemplate: emailTemplateMessagingValidatedRequestV1.getTemplateList()){
                        if (currentTemplate.getTos() != null && !currentTemplate.getTos().isEmpty() && currentTemplate.getObject()!= null && !currentTemplate.getObject().isEmpty()) {
                            listTos.add(currentTemplate);
                        }else{
                            listNotTos.add(currentTemplate);
                        }
                    }

                    List<EmailTemplateMessagingRequestV1> emailTemplateMessaging = null;
                    if(EventTypeEnum.COMPLAINT_INSERTION_INCOMPLETE.equals(eventTypeEnum)){
                        emailTemplateMessaging = messagingService.splitEmailIfContainsUrl(listTos, claimsEntity.getId(), true);
                    } else{
                        emailTemplateMessaging = messagingService.splitEmailIfContainsUrl(listTos, claimsEntity.getId());
                    }
                    emailTemplateMessaging.addAll(listNotTos);
                    description = messagingService.sendMailAndCreateLogs(emailTemplateMessaging, claimsEntity.getMotivation(), claimsEntity.getCreatedAt());
                }
            }
        }
        return description;
    }

    private List<EmailTemplateMessagingValidatedRequestV1> addPlaceholderPracticeId(List<EmailTemplateMessagingValidatedRequestV1> messagingValidatedRequestV1s, ClaimsEntity claimsEntity){
        if(messagingValidatedRequestV1s == null){
            return new ArrayList<>();
        }
        List<EmailTemplateMessagingValidatedRequestV1> responseV1s = new ArrayList<>();
        for(EmailTemplateMessagingValidatedRequestV1 request : messagingValidatedRequestV1s){
            EmailTemplateMessagingValidatedRequestV1 newRequest = new EmailTemplateMessagingValidatedRequestV1();
            newRequest.setEventType(request.getEventType());
            newRequest.setMotivation(request.getMotivation());
            List<EmailTemplateMessagingRequestV1> emailTemplateMessagingRequestV1s = request.getTemplateList();
            List<EmailTemplateMessagingRequestV1> newTemplateEmail = new ArrayList<>();
            if(emailTemplateMessagingRequestV1s != null){
                for(EmailTemplateMessagingRequestV1 email : emailTemplateMessagingRequestV1s){
                    if(email.getObject() != null){
                        System.out.println(PLACEHOLDER_ID);
                        System.out.println(claimsEntity.getPracticeId());
                        email.setObject(email.getObject().replaceAll(PLACEHOLDER_ID, claimsEntity.getPracticeId().toString()));
                    }
                    if(email.getHeading() != null){
                        email.setHeading(email.getHeading().replaceAll(PLACEHOLDER_ID, claimsEntity.getPracticeId().toString()));
                    }
                    if(email.getBody() != null){
                        email.setBody(email.getBody().replaceAll(PLACEHOLDER_ID, claimsEntity.getPracticeId().toString()));
                    }
                    if(email.getFoot() != null){
                        email.setFoot(email.getFoot().replaceAll(PLACEHOLDER_ID, claimsEntity.getPracticeId().toString()));
                    }
                    newTemplateEmail.add(email);
                }
            }
            newRequest.setTemplateList(newTemplateEmail);
            responseV1s.add(newRequest);
        }

        return responseV1s;
    }

    @Override
    public ClaimsEntity checkStatusByFlowInternal(ClaimsEntity claimsEntity, String userName, List<EmailTemplateMessagingValidatedRequestV1> emailTemplateListRequestV1List, boolean isIncomplete) {

        System.out.println("Practice id: " + claimsEntity.getPracticeId());
        String description = null;
        // CO-684 Se mi trovo nel caso di Inserimento in incompleta vengono inviate le mail di Inserimento e di Inserimento_Incomplete
        // Altrimenti si fa il giro classico.
        if(isIncomplete) {
            claimsEntity.setStatus(ClaimsStatusEnum.INCOMPLETE);
            description = sendEmailInternalByEventType(EventTypeEnum.COMPLAINT_INSERTION, emailTemplateListRequestV1List, claimsEntity);
            Historical historical = new Historical(EventTypeEnum.COMPLAINT_INSERTION, claimsEntity.getStatus(), ClaimsStatusEnum.INCOMPLETE, claimsEntity.getComplaint().getDataAccident().getTypeAccident(), claimsEntity.getComplaint().getDataAccident().getTypeAccident(), DateUtil.getNowDate(), claimsEntity.getUserId(), userName, claimsEntity.getType().getValue(), claimsEntity.getType().getValue(), description);
            claimsEntity.addHistorical(historical);
            description = sendEmailInternalByEventType(EventTypeEnum.COMPLAINT_INSERTION_INCOMPLETE, emailTemplateListRequestV1List, claimsEntity);
            Historical historicalIncomplete = new Historical(EventTypeEnum.COMPLAINT_INSERTION_INCOMPLETE, claimsEntity.getStatus(), ClaimsStatusEnum.INCOMPLETE, claimsEntity.getComplaint().getDataAccident().getTypeAccident(), claimsEntity.getComplaint().getDataAccident().getTypeAccident(), DateUtil.getNowDate(), claimsEntity.getUserId(), userName, claimsEntity.getType().getValue(), claimsEntity.getType().getValue(), description);
            claimsEntity.addHistorical(historicalIncomplete);

        } else if (claimsEntity.getType().equals(ClaimsFlowEnum.FCM)) {
            description = sendEmailInternalByEventType(EventTypeEnum.COMPLAINT_INSERTION, emailTemplateListRequestV1List, claimsEntity);
            Historical historical = new Historical(EventTypeEnum.COMPLAINT_INSERTION, claimsEntity.getStatus(), ClaimsStatusEnum.WAITING_FOR_AUTHORITY, claimsEntity.getComplaint().getDataAccident().getTypeAccident(), claimsEntity.getComplaint().getDataAccident().getTypeAccident(), DateUtil.getNowDate(), claimsEntity.getUserId(), userName, claimsEntity.getType().getValue(), claimsEntity.getType().getValue(), description);
            claimsEntity.addHistorical(historical);
            description = sendEmailInternalByEventType(EventTypeEnum.COMPLAINT_VALIDATION, emailTemplateListRequestV1List, claimsEntity);
            Historical historicalValidation = new Historical(EventTypeEnum.COMPLAINT_VALIDATION, claimsEntity.getStatus(), ClaimsStatusEnum.WAITING_FOR_AUTHORITY, claimsEntity.getComplaint().getDataAccident().getTypeAccident(), claimsEntity.getComplaint().getDataAccident().getTypeAccident(), DateUtil.getNowDate(), claimsEntity.getUserId(), userName, claimsEntity.getType().getValue(), claimsEntity.getType().getValue(), description);
            claimsEntity.addHistorical(historicalValidation);
            claimsEntity.setStatus(ClaimsStatusEnum.WAITING_FOR_AUTHORITY);

        } else if (claimsEntity.getType().equals(ClaimsFlowEnum.FUL) || claimsEntity.getType().equals(ClaimsFlowEnum.FNI)) {
            /*if (claimsEntity.getWithCounterparty() != null && claimsEntity.getWithCounterparty()) {
                description = sendEmailInternalByEventType(EventTypeEnum.COMPLAINT_INSERTION, emailTemplateListRequestV1List, claimsEntity);
                Historical historical = new Historical(EventTypeEnum.COMPLAINT_INSERTION, ClaimsStatusEnum.WAITING_FOR_AUTHORITY, ClaimsStatusEnum.WAITING_FOR_AUTHORITY, claimsEntity.getComplaint().getDataAccident().getTypeAccident(), claimsEntity.getComplaint().getDataAccident().getTypeAccident(), DateUtil.getNowDate(), claimsEntity.getUserId(), userName, claimsEntity.getType().getValue(), claimsEntity.getType().getValue(), description);
                claimsEntity.addHistorical(historical);
                description = sendEmailInternalByEventType(EventTypeEnum.COMPLAINT_VALIDATION, emailTemplateListRequestV1List, claimsEntity);
                Historical historicalValidation = new Historical(EventTypeEnum.COMPLAINT_VALIDATION, ClaimsStatusEnum.WAITING_FOR_AUTHORITY, ClaimsStatusEnum.WAITING_FOR_AUTHORITY, claimsEntity.getComplaint().getDataAccident().getTypeAccident(), claimsEntity.getComplaint().getDataAccident().getTypeAccident(), DateUtil.getNowDate(), claimsEntity.getUserId(), userName, claimsEntity.getType().getValue(), claimsEntity.getType().getValue(), description);
                claimsEntity.addHistorical(historicalValidation);
                claimsEntity.setStatus(ClaimsStatusEnum.WAITING_FOR_AUTHORITY);
            } else {*/
            ClaimsStatusEnum statusEnum;
            if ((claimsEntity.getWithCounterparty() != null && claimsEntity.getWithCounterparty()) || claimsEntity.getType().equals(ClaimsFlowEnum.FNI)) {
                statusEnum = ClaimsStatusEnum.WAITING_FOR_AUTHORITY;
            } else {
                statusEnum = ClaimsStatusEnum.MANAGED;
            }

            if (claimsEntity.getDamaged() != null &&
                    claimsEntity.getDamaged().getInsuranceCompany() != null &&
                    claimsEntity.getDamaged().getInsuranceCompany().getPai() != null &&
                    claimsEntity.getDamaged().getInsuranceCompany().getPai().getInsurancePolicyId() != null
            ) {
                Boolean isFound = false;

                if (claimsEntity.getWoundedList() != null && !claimsEntity.getWoundedList().isEmpty()) {
                    Iterator<Wounded> iterator = claimsEntity.getWoundedList().iterator();
                    while (iterator.hasNext() && !isFound) {
                        Wounded currentWounded = iterator.next();
                        if (currentWounded.getType().equals(WoundedTypeEnum.DRIVER)) {
                            isFound = true;
                        }
                    }
                }
                description = sendEmailInternalByEventType(EventTypeEnum.COMPLAINT_INSERTION, emailTemplateListRequestV1List, claimsEntity);
                Historical historical = new Historical(EventTypeEnum.COMPLAINT_INSERTION, statusEnum, statusEnum, claimsEntity.getComplaint().getDataAccident().getTypeAccident(), claimsEntity.getComplaint().getDataAccident().getTypeAccident(), DateUtil.getNowDate(), claimsEntity.getUserId(), userName, claimsEntity.getType().getValue(), claimsEntity.getType().getValue(), description);
                claimsEntity.addHistorical(historical);
                description = sendEmailInternalByEventType(EventTypeEnum.COMPLAINT_VALIDATION, emailTemplateListRequestV1List, claimsEntity);
                Historical historicalValidation = new Historical(EventTypeEnum.COMPLAINT_VALIDATION, statusEnum, statusEnum, claimsEntity.getComplaint().getDataAccident().getTypeAccident(), claimsEntity.getComplaint().getDataAccident().getTypeAccident(), DateUtil.getNowDate(), claimsEntity.getUserId(), userName, claimsEntity.getType().getValue(), claimsEntity.getType().getValue(), description);
                claimsEntity.addHistorical(historicalValidation);
                if (isFound) {
                    List<EmailTemplateMessagingRequestV1> listEntrustPai = null;
                    for (EmailTemplateMessagingValidatedRequestV1 emailTemplateMessagingValidatedRequestV1 : emailTemplateListRequestV1List) {
                        if (emailTemplateMessagingValidatedRequestV1.getEventType().equals(EventTypeEnum.ENTRUST_PAI)) {
                            if (emailTemplateMessagingValidatedRequestV1.getTemplateList() != null && !emailTemplateMessagingValidatedRequestV1.getTemplateList().isEmpty()) {
                                listEntrustPai = emailTemplateMessagingValidatedRequestV1.getTemplateList();
                            }
                        }
                    }
                    if (listEntrustPai != null && !listEntrustPai.isEmpty()) {
                        try {
                            claimsEntity.setPaiComunication(true);
                            claimsEntity = messagingService.sendEmailEntrustPaiInternal(listEntrustPai, claimsEntity, userName, statusEnum);
                        } catch (IOException e) {
                            LOGGER.debug(e.getMessage());
                        }
                    }
                }


            } else {
                description = sendEmailInternalByEventType(EventTypeEnum.COMPLAINT_INSERTION, emailTemplateListRequestV1List, claimsEntity);
                Historical historical = new Historical(EventTypeEnum.COMPLAINT_INSERTION, statusEnum, statusEnum, claimsEntity.getComplaint().getDataAccident().getTypeAccident(), claimsEntity.getComplaint().getDataAccident().getTypeAccident(), DateUtil.getNowDate(), claimsEntity.getUserId(), userName, claimsEntity.getType().getValue(), claimsEntity.getType().getValue(), description);
                claimsEntity.addHistorical(historical);
                description = sendEmailInternalByEventType(EventTypeEnum.COMPLAINT_VALIDATION, emailTemplateListRequestV1List, claimsEntity);
                Historical historicalValidation = new Historical(EventTypeEnum.COMPLAINT_VALIDATION, statusEnum, statusEnum, claimsEntity.getComplaint().getDataAccident().getTypeAccident(), claimsEntity.getComplaint().getDataAccident().getTypeAccident(), DateUtil.getNowDate(), claimsEntity.getUserId(), userName, claimsEntity.getType().getValue(), claimsEntity.getType().getValue(), description);
                claimsEntity.addHistorical(historicalValidation);
            }
            claimsEntity.setStatus(statusEnum);
            //}
        }

        return claimsEntity;
    }

    @Override
    public List<EventTypeEnum> checkEventTypeByFlowInternal(ClaimsEntity claimsEntity,boolean isIncomplete) {
        List<EventTypeEnum> eventTypeEnumList = new ArrayList<>();
        eventTypeEnumList.add(EventTypeEnum.COMPLAINT_INSERTION);
        if (!isIncomplete) {
            eventTypeEnumList.add(EventTypeEnum.COMPLAINT_VALIDATION);
            if (claimsEntity.getType().equals(ClaimsFlowEnum.FUL) || claimsEntity.getType().equals(ClaimsFlowEnum.FNI)) {
            /*if (claimsEntity.getCounterparts() == null || claimsEntity.getCounterparts().isEmpty() &&
                    claimsEntity.getDamaged().getInsuranceCompany().getPai() != null) {*/
                if (claimsEntity.getDamaged() != null &&
                        claimsEntity.getDamaged().getInsuranceCompany() != null &&
                        claimsEntity.getDamaged().getInsuranceCompany().getPai() != null &&
                        claimsEntity.getDamaged().getInsuranceCompany().getPai().getInsurancePolicyId() != null &&
                        claimsEntity.getWoundedList() != null &&
                        !claimsEntity.getWoundedList().isEmpty()
                ) {
                    Boolean isFound = false;
                    Iterator<Wounded> iterator = claimsEntity.getWoundedList().iterator();
                    while (iterator.hasNext() && !isFound) {
                        Wounded currentWounded = iterator.next();
                        if (currentWounded.getType().equals(WoundedTypeEnum.DRIVER)) {
                            isFound = true;
                        }
                    }
                    if (isFound) {
                        eventTypeEnumList.add(EventTypeEnum.ENTRUST_PAI);
                    }
                }
            }
        }else{
            eventTypeEnumList.add(EventTypeEnum.COMPLAINT_INSERTION_INCOMPLETE);
        }
        return eventTypeEnumList;
    }

    @Override
    public void checkCaiWithMoreCounterparty (ClaimsEntity claimsEntity){
        if(claimsEntity.getCounterparts()!=null && claimsEntity.getCounterparts().size()>=2 &&
                claimsEntity.getCaiDetails() != null){
            LOGGER.debug(MessageCode.CLAIMS_1064.value());
            throw new BadRequestException(MessageCode.CLAIMS_1064);
        }
    }

    @Override
    public void checkWoundedAttachments(ClaimsEntity claimsEntity) {
        if (claimsEntity.getWoundedList() != null) {
            for (Wounded wounded : claimsEntity.getWoundedList()) {
                if (wounded.getEmergencyRoom() && (wounded.getAttachmentList() == null || wounded.getAttachmentList().isEmpty())) {
                    LOGGER.debug(MessageCode.CLAIMS_1065.value());
                    throw new BadRequestException(MessageCode.CLAIMS_1065);
                }

            }
        }
    }

    @Override
    public void checkDeponentAttachments(ClaimsEntity claimsEntity) {
        if (claimsEntity.getDeponentList() != null) {
            for (Deponent deponent : claimsEntity.getDeponentList()) {
                if (deponent.getAttachments() == null || deponent.getAttachments().isEmpty()) {
                    LOGGER.debug(MessageCode.CLAIMS_1066.value());
                    throw new BadRequestException(MessageCode.CLAIMS_1066);
                }

            }
        }
    }

    public ClaimsEntity adptClaimsInsertRequestToClaimsEntityWithFlow(ClaimsInsertRequestV1 claimsInsertRequestV1) {
        ClaimsEntity claimsEntity = new ClaimsEntity();
        Damaged damaged = DamagedAdapter.adptDamagedRequestInsertToDamaged(claimsInsertRequestV1.getDamaged());
        Complaint complaintClaims = ComplaintAdapter.adptComplaintRequestToComplaint(claimsInsertRequestV1.getComplaint());
        if (damaged != null) {
            InsuranceCompany insuranceCompany = damaged.getInsuranceCompany();
            Customer customer = damaged.getCustomer();
            if (complaintClaims != null) {
                DataAccident dataAccident = complaintClaims.getDataAccident();
                if (dataAccident != null) {
                    Date date = dataAccident.getDateAccident();
                    if (!this.checkPolicy(insuranceCompany, date, customer)) {
                        LOGGER.debug(MessageCode.CLAIMS_1054.value());
                        throw new BadRequestException(MessageCode.CLAIMS_1054);
                    }
                }
            }
        }

        claimsEntity.setDamaged(damaged);
        if (damaged != null && claimsInsertRequestV1.getComplaint() != null) {

            if (damaged.getVehicle() != null)
                claimsInsertRequestV1.getComplaint().setPlate(damaged.getVehicle().getLicensePlate());

            if (damaged.getCustomer() != null)
                claimsInsertRequestV1.getComplaint().setClientId(damaged.getCustomer().getCustomerId());
        }

        //mappatura del flusso attraverso il tipo contratto e il tipo sinistro ottenuti dalle tabelle di websin
        try {
            ContractTypeEntity contractTypeEntity = contractService.getContractType(claimsInsertRequestV1.getDamaged().getContract().getContractType());

            if(contractTypeEntity == null){
                LOGGER.debug(MessageCode.CLAIMS_1010.value());
                throw new BadRequestException(MessageCode.CLAIMS_1010);
            }

            if (!contractTypeEntity.getFlagWS()) {
                LOGGER.debug(MessageCode.CLAIMS_1042.value());
                throw new BadRequestException(MessageCode.CLAIMS_1042);
            } else {

                if (claimsInsertRequestV1.getComplaint().getDataAccident().getTypeAccident() == null) {
                    //se il tipo sinistro non è stato inserito recupero flusso di default
                    claimsEntity.setType(contractTypeEntity.getDefaultFlow());

                } else {
                    claimsEntity.setType(contractService.getPersonalRiskFlowType(claimsInsertRequestV1.getDamaged().getContract().getContractType(),
                            claimsInsertRequestV1.getComplaint().getDataAccident().getTypeAccident(),
                            claimsInsertRequestV1.getDamaged().getCustomer().getCustomerId().toString())); /* tipo di flusso */
                    if (claimsEntity.getType().equals(ClaimsFlowEnum.NO)) {
                        LOGGER.debug(MessageCode.CLAIMS_1057.value());
                        throw new BadRequestException("It's not possible insert a claim with NO flow", MessageCode.CLAIMS_1057);
                    }
                }
            }
        } catch (BadRequestException e) {
            LOGGER.debug(e.getMessage());
            throw e;
        }


        //di defaul è in waiting_for_validation

        if (claimsInsertRequestV1.getCounterparty() != null && !claimsInsertRequestV1.getCounterparty().isEmpty()) {
            claimsEntity.setStatus(ClaimsStatusEnum.WAITING_FOR_VALIDATION);
            claimsEntity.setAuthorityLinkable(false);
        } else {
            //flusso senza controparti va in automatico in autogestione
            claimsEntity.setStatus(ClaimsStatusEnum.MANAGED);
            claimsEntity.setAuthorityLinkable(true);
        }

        claimsEntity.setCounterparts(counterpartyAdapter.adptCounterpartyRequestToCounterparty(claimsInsertRequestV1.getCounterparty()));
        //mappatura del flusso attraverso il tipo contratto e il tipo sinistro ottenuti dalle tabelle di websin
        claimsEntity.setWoundedList(WoundedAdapter.adptWoundedRequestToWounded(claimsInsertRequestV1.getWoundedList()));
        claimsEntity.setCaiDetails(CaiAdapter.adptCaiToCaiResponse(claimsInsertRequestV1.getCaiRequest()));

        claimsEntity.setComplaint(ComplaintAdapter.adptComplaintRequestToComplaint(claimsInsertRequestV1.getComplaint()));
        if (claimsInsertRequestV1.getWithCounterparty() && claimsInsertRequestV1.getCaiRequest() != null &&
                claimsInsertRequestV1.getCaiRequest().getVehicleA() != null && claimsInsertRequestV1.getCaiRequest().getVehicleB() != null &&
                claimsInsertRequestV1.getCaiRequest().getDriverSide() != null
        ) {
            if (claimsInsertRequestV1.getComplaint() != null && claimsInsertRequestV1.getComplaint().getDataAccident() != null && claimsInsertRequestV1.getComplaint().getDataAccident().getTypeAccident() == null) {
                if (claimsInsertRequestV1.getCaiRequest().getVehicleA() != null && claimsInsertRequestV1.getCaiRequest().getVehicleB() != null) {
                    Boolean isCaiSignedA = false;
                    Boolean isCaiSignedB = false;
                    String companyDenomination = "";

                    if (claimsEntity.getDamaged() != null) {
                        isCaiSignedA = claimsEntity.getDamaged().getCaiSigned();
                        if (claimsEntity.getDamaged().getInsuranceCompany() != null && claimsEntity.getDamaged().getInsuranceCompany().getTpl() != null) {
                            companyDenomination = claimsEntity.getDamaged().getInsuranceCompany().getTpl().getCompany();
                        }
                    }
                    if (claimsEntity.getCounterparts() != null && claimsEntity.getCounterparts().size() > 0) {
                        isCaiSignedB = claimsEntity.getCounterparts().get(0).getCaiSigned();
                    }

                    if (isCaiSignedA == null) {
                        isCaiSignedA = false;
                    }

                    if (isCaiSignedB == null) {
                        isCaiSignedB = false;
                    }

                    Map<String, Object> caiResult = this.getFlowDetailsByCai(claimsEntity.getCaiDetails(), claimsEntity.getDamaged().getContract().getContractType(), claimsEntity.getDamaged(), claimsEntity.getCounterparts(), isCaiSignedA, isCaiSignedB);

                    claimsEntity.setType((ClaimsFlowEnum) caiResult.get("flow"));
                    DataAccident dataAccident = claimsEntity.getComplaint().getDataAccident();

                    dataAccident.setTypeAccident((DataAccidentTypeAccidentEnum) caiResult.get("type"));
                    Complaint complaint = claimsEntity.getComplaint();
                    complaint.setDataAccident(dataAccident);
                    claimsEntity.setComplaint(complaint);
                }

            }
        } else if (!claimsInsertRequestV1.getWithCounterparty()) {
            if (claimsInsertRequestV1.getDamaged() != null && claimsInsertRequestV1.getDamaged().getImpactPoint() != null)
                claimsEntity.getDamaged().getImpactPoint().setIncidentDescription(claimsInsertRequestV1.getDamaged().getImpactPoint().getIncidentDescription());
        } else if ((claimsInsertRequestV1.getCaiRequest() == null ||
                (claimsInsertRequestV1.getCaiRequest().getVehicleA() == null && claimsInsertRequestV1.getCaiRequest().getVehicleB() == null &&
                        claimsInsertRequestV1.getCaiRequest().getDriverSide() ==  null )) && (claimsInsertRequestV1.getComplaint() != null && claimsInsertRequestV1.getComplaint().getDataAccident() != null && claimsInsertRequestV1.getComplaint().getDataAccident().getTypeAccident() == null)){
            if(this.checkIsCard(claimsEntity.getDamaged(), claimsEntity.getCounterparts()))
                claimsEntity.getComplaint().getDataAccident().setTypeAccident(DataAccidentTypeAccidentEnum.CARD_NON_VERIFICABILE_FIRMA_SINGOLA);
            else
                claimsEntity.getComplaint().getDataAccident().setTypeAccident(DataAccidentTypeAccidentEnum.RC_NON_VERIFICABILE);
        }
        //aggiunta metadati
        MetadataClaim metadataClaim = new MetadataClaim();
        metadataClaim.setMetadata(claimsInsertRequestV1.getMetadata());
        claimsEntity.setMetadata(metadataClaim);

        //claimsEntity = claimsService.checkStatusByFlowInternal(claimsEntity,null);
        return claimsEntity;
    }



    @Override
    public ContractTypeEntity getContractType(String contractType) {
        return contractService.getContractType(contractType);
    }

    @Override
    public ClaimsFlowEnum getFlowContractType(String contractType, DataAccidentTypeAccidentEnum claimsType) {
        return contractService.getFlowContractType(contractType, claimsType);
    }

    @Override
    public ClaimsFlowEnum getPersonalRiskFlowType(String contractType, DataAccidentTypeAccidentEnum claimsType, String customerId){
        return contractService.getPersonalRiskFlowType(contractType, claimsType, customerId);
    }

    public Boolean isInsuranceChanged(Damaged damaged, ClaimsEntity claimsEntity) {
        if (damaged.getInsuranceCompany().getTpl().getInsurancePolicyId().equals(claimsEntity.getDamaged().getInsuranceCompany().getTpl().getInsurancePolicyId())) {
            if (damaged.getInsuranceCompany().getPai() != null && claimsEntity.getDamaged().getInsuranceCompany().getPai() != null) {
                if (damaged.getInsuranceCompany().getPai().getInsurancePolicyId() != null && claimsEntity.getDamaged().getInsuranceCompany().getPai().getInsurancePolicyId() != null) {
                    if (damaged.getInsuranceCompany().getPai().getInsurancePolicyId().equals(claimsEntity.getDamaged().getInsuranceCompany().getPai().getInsurancePolicyId())) {
                        if (damaged.getInsuranceCompany().getLegalCost() != null && claimsEntity.getDamaged().getInsuranceCompany().getLegalCost() != null) {
                            if (damaged.getInsuranceCompany().getLegalCost().getInsurancePolicyId() != null && claimsEntity.getDamaged().getInsuranceCompany().getLegalCost().getInsurancePolicyId() != null) {
                                if (damaged.getInsuranceCompany().getLegalCost().getInsurancePolicyId().equals(claimsEntity.getDamaged().getInsuranceCompany().getLegalCost().getInsurancePolicyId())) {
                                    return false;
                                }
                            }
                        }
                    }
                }
            }
        }
        return true;
    }


    //REFACTOR
    public ClaimsUpdateProcedureEnum checkModifyClaims(ClaimsUpdateRequestV1 claimsUpdateRequest) throws IOException {

        Optional<ClaimsNewEntity> claimsNewEntityOptional = claimsNewRepository.findById(claimsUpdateRequest.getId());
        if (!claimsNewEntityOptional.isPresent()) {
            LOGGER.debug("claims not found");
            throw new NotFoundException("claims not found");
        }

        ClaimsNewEntity claimsNewEntity = claimsNewEntityOptional.get();
        ClaimsEntity claimsEntity = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntity);

        //data vecchia
        LocalDateTime dataAccidentOld = DateAdapter.adptDateToLocalDataTime(claimsEntity.getComplaint().getDataAccident().getDateAccident());
        //data nuova
        LocalDateTime dataAccidentNew = DateAdapter.adptDateToLocalDataTime(DateUtil.convertIS08601StringToUTCDate(claimsUpdateRequest.getComplaint().getDataAccident().getDateAccident()));

        if (!dataAccidentOld.isEqual(dataAccidentNew)) {
            Damaged damaged = this.getInfoChangeDate(claimsEntity, DateUtil.convertIS08601StringToUTCDate(claimsUpdateRequest.getComplaint().getDataAccident().getDateAccident()));
            if (!damaged.getCustomer().getCustomerId().equals(claimsEntity.getDamaged().getCustomer().getCustomerId())) {
                return ClaimsUpdateProcedureEnum.CLAIM_TRANSFER;
            } else if (this.isInsuranceChanged(damaged, claimsEntity)) {
                return ClaimsUpdateProcedureEnum.CHANGE_POLICY;
            }

        }
        return ClaimsUpdateProcedureEnum.UPDATE;

    }

    @Override
    public ClaimsStatusEnum checkStatusByFlowValidated(ClaimsEntity claimsEntity) {

        if (claimsEntity.getWithCounterparty()) {
            ClaimsStatusEnum nextStatus = ClaimsStatusEnum.WAITING_FOR_AUTHORITY;
            if (isAuthorityAndSxNumber(claimsEntity)) {
                if (claimsEntity.getType().equals(ClaimsFlowEnum.FNI)) {
                    nextStatus = ClaimsStatusEnum.TO_SEND_TO_CUSTOMER;
                } else {
                    nextStatus = ClaimsStatusEnum.TO_ENTRUST;
                }
            }
            return nextStatus;
        } else if (claimsEntity.getType().equals(ClaimsFlowEnum.FCM) || claimsEntity.getType().equals(ClaimsFlowEnum.FNI)) {
            return ClaimsStatusEnum.WAITING_FOR_AUTHORITY;
        } else {
            //Verificare se deve andare in managed quando è senza controparte
            return ClaimsStatusEnum.MANAGED;
        }

    }


    //REFACTOR
    @Override
    public ClaimsEntity addSplit(String claimsId, List<SplitRequest> splitListRequest) {

        List<Split> splitList = SplitAdapter.adptFromSplitRequestListToSplitList(splitListRequest);
        //Recupero della vecchia entità
        Optional<ClaimsNewEntity> optClaims = claimsNewRepository.findById(claimsId);
        if (optClaims.isPresent()) {

            //conversione nella vecchia entità
            ClaimsEntity claimsEntity = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(optClaims.get());
            ClaimsEntity oldClaims = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(optClaims.get());


            ClaimsStatusEnum statusOld = claimsEntity.getStatus();
            IncidentRequestV1 oldIncident = incidentAdapter.adptFromClaimsEntityToIncidentRequest(claimsEntity,null,false, false);



            Refund refund = claimsEntity.getRefund();
            if (refund == null)
                refund = new Refund();

            List<Split> splitRefundList = refund.getSplitList();
            if (splitRefundList == null)
                splitRefundList = new LinkedList<>();

            splitRefundList.addAll(splitList);

            refund.setSplitList(splitRefundList);
            Split split = refund.getSplitList().get(splitRefundList.size()-1);
            refund.setDefinitionDate(split.getIssueDate());
            Double totalLiquidationReceived = 0.0;
            for(Split currentSplit : refund.getSplitList()){
                totalLiquidationReceived += currentSplit.getTotalPaid();
            }

            refund.setTotalLiquidationReceived(totalLiquidationReceived);

            if(claimsEntity.getType() != null && claimsEntity.getType().equals(ClaimsFlowEnum.FCM) && refund.getPoSum() != null && refund.getTotalLiquidationReceived()!= null){
                refund.setAmountToBeDebited(refund.getPoSum() - refund.getTotalLiquidationReceived());
            }
            refund = this.setRefundFranchiseAmountFcm(claimsEntity,refund);

            claimsEntity.setUpdateAt(DateUtil.getNowInstant());
            claimsEntity.setRefund(refund);

            ClaimsNewEntity claimsNewEntity = converterClaimsService.convertOldEntityToNewClaimsEntity(claimsEntity);
            //Salvo la nuova entità
            claimsNewRepository.save(claimsNewEntity);

            if(dwhCall){
                dwhClaimsService.sendMessage(claimsEntity, com.doing.nemo.dwh.client.payload.request.EventTypeEnum.UPDATED);
            }

            //Intervento 6 - C
            try {
                externalCommunicationService.modifyIncidentAsync(claimsEntity.getId(),statusOld, false,oldIncident, false, oldClaims);
            } catch (IOException e) {
                throw new BadRequestException(MessageCode.CLAIMS_1112, e);
            }


            claimsNewEntity = claimsNewRepository.getOne(claimsId);
            return converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntity);

        }
        LOGGER.debug(MessageCode.CLAIMS_1010.value());
        throw new BadRequestException(MessageCode.CLAIMS_1010);

    }

    //REFACTOR
    @Override
    public ClaimsEntity deleteSplit(String claimsId, String splitId) {

        //recupero della nuova entità
        Optional<ClaimsNewEntity> optClaims = claimsNewRepository.findById(claimsId);
        if (!optClaims.isPresent()) {

            LOGGER.debug(MessageCode.CLAIMS_1010.value());
            throw new BadRequestException(MessageCode.CLAIMS_1010);

        }

        //conversione nella vecchia entità
        ClaimsEntity claimsEntity = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(optClaims.get());
        ClaimsEntity oldClaims = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(optClaims.get());


        ClaimsStatusEnum statusOld = claimsEntity.getStatus();
        IncidentRequestV1 oldIncident = incidentAdapter.adptFromClaimsEntityToIncidentRequest(claimsEntity,null,false, false);



        Refund refund = claimsEntity.getRefund();
        if(refund!=null && refund.getSplitList()!=null && !refund.getSplitList().isEmpty()) {

            List<Split> splitRefundList = refund.getSplitList();
            Split found = null;
            Iterator<Split> iterator = splitRefundList.iterator();
            boolean isFound = false;
            boolean isLast = false;
            while (iterator.hasNext() && !isFound) {
                Split current = iterator.next();
                if (current.getId().equals(UUID.fromString(splitId))) {
                    found = current;
                    isFound = true;
                }
                if(!iterator.hasNext())
                    isLast = true;
            }
            System.out.println(found);
            if (found == null) {
                LOGGER.debug(MessageCode.CLAIMS_1010.value());
                throw new BadRequestException(MessageCode.CLAIMS_1010);
            }
            splitRefundList.remove(found);
            refund.setSplitList(splitRefundList);
            if(isLast && !splitRefundList.isEmpty()){
                refund.setDefinitionDate(splitRefundList.get(splitRefundList.size()-1).getIssueDate());
            } else if(isLast){
                refund.setDefinitionDate(null);
            }
            Double totalLiquidationReceived = 0.0;
            for (Split currentSplit : refund.getSplitList()) {
                totalLiquidationReceived += currentSplit.getTotalPaid();
            }

            refund.setTotalLiquidationReceived(totalLiquidationReceived);
            if (claimsEntity.getType() != null && claimsEntity.getType().equals(ClaimsFlowEnum.FCM) && refund.getPoSum() != null && refund.getTotalLiquidationReceived() != null) {
                refund.setAmountToBeDebited(refund.getPoSum() - refund.getTotalLiquidationReceived());
            }
            refund = this.setRefundFranchiseAmountFcm(claimsEntity, refund);

            claimsEntity.setUpdateAt(DateUtil.getNowInstant());
            claimsEntity.setRefund(refund);

            ClaimsNewEntity claimsNewEntity = converterClaimsService.convertOldEntityToNewClaimsEntity(claimsEntity);
            //Salvo la nuova entità
            claimsNewRepository.save(claimsNewEntity);
            if (dwhCall) {
                dwhClaimsService.sendMessage(claimsEntity, com.doing.nemo.dwh.client.payload.request.EventTypeEnum.UPDATED);
            }

            //Intervento 6 - C
            try {
                externalCommunicationService.modifyIncidentAsync(claimsEntity.getId(), statusOld, false, oldIncident,isLast, oldClaims);
            } catch (IOException e) {
                throw new BadRequestException(MessageCode.CLAIMS_1112, e);
            }


            claimsNewEntity = claimsNewRepository.getOne(claimsId);
            return converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntity);

        } else {
            LOGGER.debug(MessageCode.CLAIMS_1010.value());
            throw new BadRequestException(MessageCode.CLAIMS_1010);
        }

    }

    //REFACTOR
    @Override
    public ClaimsEntity updateSplit(String claimsId, String splitId, SplitRequest splitUpdateRequest) {

        Split splitUpdate = SplitAdapter.adptFromSplitRequestToSplit(splitUpdateRequest);
        //recupero della nuova entità
        Optional<ClaimsNewEntity> optClaims = claimsNewRepository.findById(claimsId);
        if (optClaims.isPresent()) {

            //conversione nella vecchia entità
            ClaimsEntity claimsEntity = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(optClaims.get());
            ClaimsEntity oldClaims = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(optClaims.get());

            ClaimsStatusEnum statusOld = claimsEntity.getStatus();
            IncidentRequestV1 oldIncident = incidentAdapter.adptFromClaimsEntityToIncidentRequest(claimsEntity,null,false, false);

            Boolean update = false;

            Refund refund = null;
            if (claimsEntity.getRefund() != null) {

                refund = claimsEntity.getRefund();

                if (refund.getSplitList() != null) {

                    for (Split att : refund.getSplitList()) {

                        if (att.getId() != null && att.getId().equals(UUID.fromString(splitId))) {

                            att.setIssueDate(splitUpdate.getIssueDate());
                            att.setLegalFees(splitUpdate.getLegalFees());
                            att.setMaterialAmount(splitUpdate.getMaterialAmount());
                            att.setNote(splitUpdate.getNote());
                            att.setReceivedSum(splitUpdate.getReceivedSum());
                            att.setRefundType(splitUpdate.getRefundType());
                            att.setTechnicalStop(splitUpdate.getTechnicalStop());
                            att.setTotalPaid(splitUpdate.getTotalPaid());
                            att.setType(splitUpdate.getType());

                            update = true;
                            claimsEntity.setRefund(refund);
                            claimsRepository.save(claimsEntity);

                        }
                    }

                    if (!update) {
                        LOGGER.debug("Split not found");
                        throw new NotFoundException("Split not found. ", MessageCode.CLAIMS_1010);
                    }
                    Double totalLiquidationReceived = 0.0;
                    for(Split currentSplit : refund.getSplitList()){
                        totalLiquidationReceived += currentSplit.getTotalPaid();
                    }

                    refund.setTotalLiquidationReceived(totalLiquidationReceived);
                    if(claimsEntity.getType() != null && claimsEntity.getType().equals(ClaimsFlowEnum.FCM) && refund.getPoSum() != null && refund.getTotalLiquidationReceived()!= null){
                        refund.setAmountToBeDebited(refund.getPoSum() - refund.getTotalLiquidationReceived());
                    }
                    refund = this.setRefundFranchiseAmountFcm(claimsEntity,refund);

                    claimsEntity.setRefund(refund);
                    claimsEntity.setUpdateAt(DateUtil.getNowInstant());

                    ClaimsNewEntity claimsNewEntity = converterClaimsService.convertOldEntityToNewClaimsEntity(claimsEntity);
                    //Salvo la nuova entità
                    claimsNewRepository.save(claimsNewEntity);
                    if(dwhCall){
                        dwhClaimsService.sendMessage(claimsEntity, com.doing.nemo.dwh.client.payload.request.EventTypeEnum.UPDATED);
                    }

                    //Intervento 6 - C
                    try {
                        externalCommunicationService.modifyIncidentAsync(claimsEntity.getId(),statusOld, false,oldIncident, false, oldClaims);
                    } catch (IOException e) {
                        throw new BadRequestException(MessageCode.CLAIMS_1112, e);
                    }



                    claimsNewEntity = claimsNewRepository.getOne(claimsId);
                    return converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntity);
                }
            }
            LOGGER.debug(MessageCode.CLAIMS_1093.value());
            throw new BadRequestException(MessageCode.CLAIMS_1093);
        }
        LOGGER.debug("Claims not found. ");
        throw new NotFoundException("Claims not found. ", MessageCode.CLAIMS_1010);
    }

    private Boolean checkClosureByType(ClaimsEntity claimsEntity, ClaimsStatusEnum newClosureStatus,EventTypeEnum nextEvent) {
        String mapKey = null;
        DataAccidentTypeAccidentEnum claimsType = claimsEntity.getComplaint().getDataAccident().getTypeAccident();
        if (claimsType.equals(DataAccidentTypeAccidentEnum.RC_CONCORSUALE) || claimsType.equals(DataAccidentTypeAccidentEnum.CARD_CONCORSUALE_DOPPIA_FIRMA) || claimsType.equals(DataAccidentTypeAccidentEnum.CARD_CONCORSUALE_FIRMA_SINGOLA))
            mapKey = "COMPETITION";
        else if (claimsType.equals(DataAccidentTypeAccidentEnum.RC_NON_VERIFICABILE) || claimsType.equals(DataAccidentTypeAccidentEnum.CARD_NON_VERIFICABILE_DOPPIA_FIRMA) || claimsType.equals(DataAccidentTypeAccidentEnum.CARD_NON_VERIFICABILE_FIRMA_SINGOLA))
            mapKey = "NOT_VERIFIABLE";
        else {
            if (claimsEntity.getType().equals(ClaimsFlowEnum.FCM))
                mapKey = "FCM_";
            else if (claimsEntity.getType().equals(ClaimsFlowEnum.FNI))
                mapKey = "FNI_";
            else if (claimsEntity.getType().equals(ClaimsFlowEnum.FUL))
                mapKey = "FUL_";
            if(mapKey!=null){
                if((claimsEntity.getCounterparts()==null || claimsEntity.getCounterparts().isEmpty()) || claimsEntity.getWithCounterparty().equals(false))
                    mapKey = mapKey + "WITHOUT_COUNTERPARTY";
                else {
                    if (claimsEntity.getType().equals(ClaimsFlowEnum.FCM))
                        mapKey = mapKey + "ACTIVE_PASSIVE";
                    else {
                        if (activeSet.contains(claimsType))
                            mapKey = mapKey + "ACTIVE";
                        else if (passiveSet.contains(claimsType))
                            mapKey = mapKey + "PASSIVE";
                    }
                }
            }
        }
        TypeAccidentForClosureEnum keyMapType = TypeAccidentForClosureEnum.create(mapKey);
        if (keyMapType == null)
            return true;

        if (nextEvent.equals(EventTypeEnum.SEND_TO_CLIENT)){
            Pair pair = typeMapFNI.get(keyMapType);
            if (pair == null) {
                return true;
            } else {
                if (newClosureStatus.equals(ClaimsStatusEnum.CLOSED))
                    return pair.getClosedWithoutSequel();
                else if (newClosureStatus.equals(ClaimsStatusEnum.CLOSED_PARTIAL_REFUND) || newClosureStatus.equals(ClaimsStatusEnum.CLOSED_TOTAL_REFUND))
                    return pair.getClosed();
            }
        }else {

            Pair pair = typeMap.get(keyMapType);
            if (pair == null) {
                return true;
            } else {
                if (newClosureStatus.equals(ClaimsStatusEnum.CLOSED))
                    return pair.getClosedWithoutSequel();
                else if (newClosureStatus.equals(ClaimsStatusEnum.CLOSED_PARTIAL_REFUND) || newClosureStatus.equals(ClaimsStatusEnum.CLOSED_TOTAL_REFUND))
                    return pair.getClosed();
            }
        }
        return true;
    }

    private static Boolean checkClosureByStatus(ClaimsEntity claimsEntity, ClaimsStatusEnum newClosureStatus) {
        ClaimsStatusEnum claimsStatus = claimsEntity.getStatus();
        Pair pair = statusMap.get(claimsStatus);
        if (pair == null) {
            return true;
        } else {
            if (newClosureStatus.equals(ClaimsStatusEnum.CLOSED))
                return pair.getClosedWithoutSequel();
            else if (newClosureStatus.equals(ClaimsStatusEnum.CLOSED_PARTIAL_REFUND) || newClosureStatus.equals(ClaimsStatusEnum.CLOSED_TOTAL_REFUND))
                return pair.getClosed();
        }
        return true;
    }

    @Override
    public void checkClosureByTypeAndStatus(ClaimsEntity claimsEntity, ClaimsStatusEnum newClosureStatus, EventTypeEnum nextEvent) {
        if (checkClosureByType(claimsEntity, newClosureStatus, nextEvent)) {
            if (!checkClosureByStatus(claimsEntity, newClosureStatus)) {
                LOGGER.debug(MessageCode.CLAIMS_1094.value());
                throw new BadRequestException(MessageCode.CLAIMS_1094);
            }
        } else {
            LOGGER.debug(MessageCode.CLAIMS_1095.value());
            throw new BadRequestException(MessageCode.CLAIMS_1095);
        }
    }

    private static class Pair {

        private Boolean closed;
        private Boolean closedWithoutSequel;

        public Pair() {
        }

        public Pair(Boolean closed, Boolean closedWithoutSequel) {
            this.closed = closed;
            this.closedWithoutSequel = closedWithoutSequel;
        }

        public Boolean getClosed() {
            return closed;
        }

        public void setClosed(Boolean closed) {
            this.closed = closed;
        }

        public Boolean getClosedWithoutSequel() {
            return closedWithoutSequel;
        }

        public void setClosedWithoutSequel(Boolean closedWithoutSequel) {
            this.closedWithoutSequel = closedWithoutSequel;
        }
    }

    @Override
    public ClaimsEntity patchContractInfo(String claimsId, DamagedRequest damagedRequest, String userId, String userName){


        Optional<ClaimsEntity> optClaims = claimsRepository.findById(claimsId);

        if(!optClaims.isPresent()){
            LOGGER.debug(MessageCode.CLAIMS_1010.value());
            throw new NotFoundException(MessageCode.CLAIMS_1010);
        }

        ClaimsEntity claims = optClaims.get();

        Damaged damagedPatch = DamagedAdapter.adptDamagedRequestToDamaged(damagedRequest);
        Damaged damaged = null;

        if(claims.getDamaged() != null)
            damaged = claims.getDamaged();



        String description = null;

        if(damaged != null){

            if(damaged.getContract() != null){

                Contract contract = claims.getDamaged().getContract();

                Contract contractPatch = damagedPatch.getContract();

                if(contractPatch != null) {

                    if (contractPatch.getStartDate() != null) {

                        if (contract.getStartDate() != null && !contract.getStartDate().equals(contractPatch.getStartDate())) {

                            description = this.createChangeInfoDescriptinForLog(description, "Inizio Contratto", DateUtil.getDateStringWithSeparationCharacters(contract.getStartDate()), DateUtil.getDateStringWithSeparationCharacters(contractPatch.getStartDate()));
                            contract.setStartDate(contractPatch.getStartDate());

                        } else {

                            if (contract.getStartDate() == null && contractPatch.getStartDate() != null) {

                                contract.setStartDate(contractPatch.getStartDate());
                                description = this.createChangeInfoDescriptinForLog(description, "Inizio Contratto", "Nessuno", DateUtil.getDateStringWithSeparationCharacters(contractPatch.getStartDate()));
                            }
                        }
                    }

                    if (contractPatch.getEndDate() != null) {

                        if (contract.getEndDate() != null && !contract.getEndDate().equals(contractPatch.getEndDate())) {

                            description = this.createChangeInfoDescriptinForLog(description, "Fine Contratto", DateUtil.getDateStringWithSeparationCharacters(contract.getEndDate()), DateUtil.getDateStringWithSeparationCharacters(contractPatch.getEndDate()));
                            contract.setEndDate(contractPatch.getEndDate());

                        } else {

                            if (contract.getEndDate() == null && contractPatch.getEndDate() != null) {

                                contract.setEndDate(contractPatch.getEndDate());
                                description = this.createChangeInfoDescriptinForLog(description, "Fine Contratto", "Nessuno", DateUtil.getDateStringWithSeparationCharacters(contractPatch.getEndDate()));
                            }
                        }
                    }

                    if (contract.getStartDate() != null && contract.getEndDate() != null) {

                        if (contract.getEndDate().compareTo(contract.getStartDate()) < 0) {
                            LOGGER.debug(MessageCode.CLAIMS_1104.value());
                            throw new BadRequestException(MessageCode.CLAIMS_1104);
                        }

                        int months = Period.between(DateUtil.convertUTCDateToLocalDate(contract.getStartDate()), DateUtil.convertUTCDateToLocalDate(contract.getEndDate())).getMonths();
                        int years = Period.between(DateUtil.convertUTCDateToLocalDate(contract.getStartDate()), DateUtil.convertUTCDateToLocalDate(contract.getEndDate())).getYears() * 12;
                        Integer newDuration = months + years;

                        if (contract.getDuration() != null && !contract.getDuration().equals(newDuration)) {

                            description = this.createChangeInfoDescriptinForLog(description, "Durata Contratto", contract.getDuration().toString(), newDuration.toString());
                        } else if (contract.getDuration() == null && !contract.getDuration().equals(newDuration)) {
                            description = this.createChangeInfoDescriptinForLog(description, "Durata Contratto", "Nessuno", newDuration.toString());
                        }
                        contract.setDuration(months + years);
                    }
                    claims.getDamaged().setContract(contract);
                }
            }

            Registry registry = null;
            if(damaged.getAntiTheftService() != null && damaged.getAntiTheftService().getRegistryList() != null && !damaged.getAntiTheftService().getRegistryList().isEmpty()) {
                registry = damaged.getAntiTheftService().getRegistryList().get(0);
            } else {
                registry = new Registry();
            }
            Registry registryPatch = null;
            if (damagedPatch.getAntiTheftService() != null && damagedPatch.getAntiTheftService().getRegistryList() != null && !damagedPatch.getAntiTheftService().getRegistryList().isEmpty())
                registryPatch = damagedPatch.getAntiTheftService().getRegistryList().get(0);

            if (registryPatch != null){

                if (registryPatch.getCodPack() != null) {

                    if (registry.getCodPack() != null && !registry.getCodPack().equalsIgnoreCase(registryPatch.getCodPack())) {

                        description = this.createChangeInfoDescriptinForLog(description, "Apparato Telematico", registry.getCodPack(), registryPatch.getCodPack());
                        registry.setCodPack(registryPatch.getCodPack());

                    } else if (registry.getCodPack() == null && registryPatch.getCodPack() != null) {
                        registry.setCodPack(registryPatch.getCodPack());
                        description = this.createChangeInfoDescriptinForLog(description, "Apparato Telematico", "Nessuno", registryPatch.getCodPack());
                    }
                }

                if (registryPatch.getVoucherId() != null) {

                    if (registry.getVoucherId() != null && !registry.getVoucherId().equals(registryPatch.getVoucherId())) {

                        description = this.createChangeInfoDescriptinForLog(description, "Voucher ID", registry.getVoucherId().toString(), registryPatch.getVoucherId().toString());
                        registry.setVoucherId(registryPatch.getVoucherId());

                    } else if (registry.getVoucherId() == null && registryPatch.getVoucherId() != null) {
                        registry.setVoucherId(registryPatch.getVoucherId());
                        description = this.createChangeInfoDescriptinForLog(description, "Voucher ID", "Nessuno", registryPatch.getVoucherId().toString());
                    }
                }

                if (registryPatch.getVoucherId() != null || registryPatch.getCodPack() != null) {

                    if (registryPatch.getCodPack() != null)
                        registry.setCodPack(registryPatch.getCodPack());

                    if (registryPatch.getVoucherId() != null)
                        registry.setVoucherId(registryPatch.getVoucherId());

                    String codAntiTheftService = AntiTheftServiceAdapter.adptProviderToAntiTheftServiceEntity(registry.getCodPack());
                    AntiTheftServiceEntity antiTheftServiceEntity = antiTheftServiceRepository.findAntiTheftByCodAntiTheftService(codAntiTheftService);
                    AntiTheftService antiTheftService = AntiTheftServiceAdapter.adptAntiTheftServiceEntityToAntiTheftService(antiTheftServiceEntity);

                    if (antiTheftService != null) {

                        antiTheftService.setRegistryList(new LinkedList<>());
                        List<Registry> registries = antiTheftService.getRegistryList();
                        registries.add(registry);
                        antiTheftService.setRegistryList(registries);
                        claims.getDamaged().setAntiTheftService(antiTheftService);

                        String idAntiTheftRequest = UUID.randomUUID().toString();
                        // TO REFACTOR
                        // antiTheftRequestService.insertAntiTheftRequest(idAntiTheftRequest, claims, antiTheftService.getProviderType(), "1");
                        // this.callToOctoAsync(claims, claims.getStatus(), new AntiTheftRequest(), userId, userName, idAntiTheftRequest );
                    }
                }
            }
        }

        if(description != null){

            Historical historical = new Historical();

            historical.setUserId(userId);
            historical.setUserName(userName);
            historical.setEventType(EventTypeEnum.EDIT_PRACTICE_DATA);
            historical.setStatusEntityOld(claims.getStatus());
            historical.setStatusEntityNew(claims.getStatus());
            historical.setOldType(claims.getComplaint().getDataAccident().getTypeAccident());
            historical.setNewType(claims.getComplaint().getDataAccident().getTypeAccident());
            historical.setOldFlow(claims.getType().getValue());
            historical.setNewFlow(claims.getType().getValue());
            historical.setComunicationDescription(description);
            historical.setUpdateAt(DateUtil.getNowDate());

            if( claims.getHistorical() == null)
                claims.setHistorical(new LinkedList<>());

            List<Historical> historicalList = claims.getHistorical();
            historicalList.add(historical);
            claims.setHistorical(historicalList);
        }

        claimsRepository.save(claims);
        if(dwhCall){
            dwhClaimsService.sendMessage(claims, com.doing.nemo.dwh.client.payload.request.EventTypeEnum.UPDATED);
        }
        return claims;
    }

    public String createChangeInfoDescriptinForLog(String description , String attribute, String oldValue, String newValue ){

        if(description == null)
            description = "";

        if(oldValue == null)
            oldValue = "Nessuno";

        if(newValue == null)
            newValue = "Nessuno";

        description += "Campo: "+attribute +".  Valore precedente: "+oldValue + "." + " Valore attuale: "+newValue+". <br>";

        return description;

    }




    //REFACTOR
    @Override
    public boolean isNotClosedTheft(String plate, String date){

        List<ClaimsNewEntity> claimsEntityList = claimsNewRepository.findNotClosedTheftWhitPlate(plate, date);
        //non c'è necessità di convertire nella vecchia entità

        if( claimsEntityList != null && claimsEntityList.size() > 0 ){
            return false;
        }
        return true;
    }



    @Override
    public Refund setRefundFranchiseAmountFcm (ClaimsEntity claimsEntity, Refund refund) {

        if( claimsEntity.getType() != null && claimsEntity.getType().equals(ClaimsFlowEnum.FCM) && claimsEntity.getDamaged() != null && claimsEntity.getDamaged().getInsuranceCompany() != null &&
                claimsEntity.getDamaged().getInsuranceCompany().getTpl() != null &&
                claimsEntity.getDamaged().getInsuranceCompany().getTpl().getInsurancePolicyId() != null) {
            InsurancePolicyEntity insurancePolicyEntity = insurancePolicyRepository.getOne(claimsEntity.getDamaged().getInsuranceCompany().getTpl().getInsurancePolicyId());
            List<RiskEntity> riskEntityList = insurancePolicyEntity.getRiskEntityList();
            if (riskEntityList != null && !riskEntityList.isEmpty()) {
                Iterator<RiskEntity> itRisk = riskEntityList.iterator();
                Boolean isFound = false;
                while (itRisk.hasNext() && !isFound) {
                    RiskEntity riskEntity = itRisk.next();
                    if (riskEntity.getType().getValue().equalsIgnoreCase(claimsEntity.getComplaint().getDataAccident().getTypeAccident().getValue())) {
                        refund.setFranchiseAmountFcm(riskEntity.getExemption());
                        isFound = true;
                    }
                }
            }
        }
        return refund;
    }


    @Override
    public ClaimsEntity findById(String claimsId){
        Optional<ClaimsNewEntity> claimsNewEntityOptional = claimsNewRepository.findById(claimsId);
        if(!claimsNewEntityOptional.isPresent()){
            LOGGER.debug("Claims with UUID " + claimsId + " not found ");
            throw new NotFoundException("Claims with UUID " + claimsId + " not found ", MessageCode.CLAIMS_1010);
        }
        //recupero della nuova entità
        ClaimsNewEntity claimsNewEntity = claimsNewEntityOptional.get();
        //converto nella vecchia entità
        ClaimsEntity claimsEntity = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntity);

        return claimsEntity;
    }

    @Override
    public ClaimsEntity findByIdPratica(Long practiceId){
        //recupero della nuova entità
        ClaimsNewEntity claimsNewEntity = claimsNewRepository.getClaimsByIdPratica(practiceId);
        if(claimsNewEntity == null){
            LOGGER.debug("Claims with practice id " + practiceId + " not found ");
            throw new NotFoundException("Claims with practice id " + practiceId + " not found ", MessageCode.CLAIMS_1010);
        }
        //converto nella vecchia entità
        ClaimsEntity claimsEntity = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntity);

        return claimsEntity;
    }


    @Override
    public ClaimsNewEntity findByIdNewEntity(String claimsId){
        Optional<ClaimsNewEntity> claimsNewEntityOptional = claimsNewRepository.findById(claimsId);
        if(!claimsNewEntityOptional.isPresent()){
            LOGGER.debug("Claims with UUID " + claimsId + " not found ");
            throw new NotFoundException("Claims with UUID " + claimsId + " not found ", MessageCode.CLAIMS_1010);
        }
        //recupero della nuova entità
        ClaimsNewEntity claimsNewEntity = claimsNewEntityOptional.get();

        return claimsNewEntity;
    }


    @Override
    public void deleteClaimsInBozzaRange(int range){
        claimsNewRepository.deleteClaimsInBozzaRange(range);
    }

    @Override
    public void updateClaimsEvidence(){
        claimsNewRepository.updateClaimsEvidence();
    }

    @Override
    public void saveClaimsNew(ClaimsNewEntity claimsNewEntity){
        claimsNewRepository.save(claimsNewEntity);
    }

    @Override
    public NoteEntity findClaimNoteById(String idNote) {

        NoteEntity note = noteRepository.getOne(idNote);
        return note;
    }

    @Override
    public ClaimsEntity toggleHiddenAttachment(String idClaims, String idAttachments) {

        ClaimsNewEntity claimsEntity = claimsNewRepository.getOne(idClaims);
        List<String> attachmentList = new ArrayList<>();
        String[] attachments = idAttachments.split(",");
        for (String idAttachment : attachments) {
            attachmentList.add(idAttachment);
        }
        if (claimsEntity.getForms() != null) {
            for (Attachment attachment : claimsEntity.getForms().getAttachment()) {
                if (attachmentList.contains(attachment.getFileManagerId())) {
                    attachment.setHidden(!attachment.getHidden());
                }
            }
        }
        if(claimsEntity.getAntiTheftRequestEntities()!=null){
            for(AntiTheftRequestNewEntity current: claimsEntity.getAntiTheftRequestEntities()){
                if(current.getCrashReport()!= null && attachmentList.contains(current.getCrashReport().getFileManagerId())){
                    current.getCrashReport().setHidden(!current.getCrashReport().getHidden());
                }
            }
        }

        claimsNewRepository.save(claimsEntity);
        return converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsEntity);
    }

    public static boolean isAuthority(ClaimsEntity claimsEntity) {
        boolean isAuthority = false;

        if (claimsEntity.getAuthorities() != null && !claimsEntity.getAuthorities().isEmpty()) {
            Iterator<Authority> authIt = claimsEntity.getAuthorities().iterator();

            while (authIt.hasNext() && !isAuthority) {
                Authority authority = authIt.next();
                if (authority.getInvoiced()) {
                    isAuthority = true;
                }
            }
        }

        return isAuthority;
    }


    @Override
    public ClaimsResponseV1 isPending(ClaimsResponseV1 claimsResponseV1){
        if(claimsResponseV1!=null){
            claimsResponseV1.setPending(claimsPendingService.checkIfExistsPendingWorking(claimsResponseV1.getPracticeId()));
        }
        return claimsResponseV1;
    }

    @Override
    public Boolean isPending(Long practiceId) {
        if (practiceId != null){
            return claimsPendingService.checkIfExistsPendingWorking(practiceId);
        }
        return false;
    }

    @Override
    public UploadFileResponseV1 buildCsvPaginationExport(Boolean inEvidence, Long practiceId, String flow, List<Long> clientListId, String plate, String status, String locator, String dateAccidentFrom, String dateAccidentTo, String typeAccident, Boolean asc, String orderBy, int page, int pageSize, Boolean withContinuation, String company, String clientName, Boolean poVariation, Boolean waitingForNumberSx, Long fleetVehicleId, Boolean waitingForAuthority) throws IOException {
        Pagination<ClaimsEntity> pagination = this.getClaimsPaginationWithoutDraftAndTheft(inEvidence, practiceId, flow, clientListId, plate, status, locator, dateAccidentFrom, dateAccidentTo, typeAccident, asc, orderBy, 1, this.csvMaxRows, withContinuation, company, clientName, poVariation, waitingForNumberSx, fleetVehicleId, waitingForAuthority);

        if(pagination!=null && pagination.getStats()!=null && pagination.getStats().getItemCount()!=null && pagination.getStats().getItemCount()>csvMaxRows){
            throw new BadRequestException("The lines returned are greater than the maximum number of lines to export", MessageCode.CLAIMS_1163);
        }

        List<String> csvContentStrings = csvBuilderService.buildClaimsDashboardCSV(pagination.getItems());

        Date date = new Date(System.currentTimeMillis());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String timestamp = simpleDateFormat.format(date);
        UploadFileResponseV1 uploadFileResponse =null;
        uploadFileResponse = csvBuilderService.upldadCsv(csvContentStrings, "ClaimsDashboard_" + timestamp, "claims");
        return uploadFileResponse;
    }


}
