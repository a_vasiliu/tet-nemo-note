package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.enumerated.DamagedEnum.WoundedEnum.WoundedTypeEnum;
import com.doing.nemo.claims.entity.enumerated.EventTypeEnum;
import com.doing.nemo.claims.entity.jsonb.Historical;
import com.doing.nemo.claims.entity.jsonb.Wounded;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import javax.annotation.PostConstruct;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public abstract class ExportMapFiller {

    protected static Logger LOGGER = LoggerFactory.getLogger(ExportMapFiller.class);

    private final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

    private final SimpleDateFormat hourFormat = new SimpleDateFormat("HH:mm");

    private final SimpleDateFormat timestampFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");

    @PostConstruct
    private void setTimeZone() {
        dateFormat.setTimeZone( TimeZone.getTimeZone("Europe/Rome") );
        hourFormat.setTimeZone( TimeZone.getTimeZone("Europe/Rome") );
        timestampFormat.setTimeZone( TimeZone.getTimeZone("Europe/Rome") );
    }

    public SimpleDateFormat getDateFormat() {
        return dateFormat;
    }

    public SimpleDateFormat getHourFormat() {
        return hourFormat;
    }

    public SimpleDateFormat getTimestampFormat() {
        return timestampFormat;
    }

    public String getUser(ClaimsEntity claimsEntity) {
        //UTENTE INSERIMENTO
        if(claimsEntity.getHistorical() != null) {
            for (Historical historical : claimsEntity.getHistorical() ){
                if(historical.getEventType() != null && historical.getEventType().equals(EventTypeEnum.COMPLAINT_INSERTION)) {
                    return historical.getUserName();
                }
            }
        }
        return "";
    }

    public String adptBooleanToString(Boolean flag) {
        return flag == null || !flag ? "NO" : "SI";
    }

    public String adptBooleanHiddenToString(Boolean flag) {
        return flag == null || !flag ? "SI" : "NO";
    }

    public Boolean isDriverInjury(List<Wounded> woundedList) {
        if(woundedList == null || woundedList.isEmpty()) return false;
        for(Wounded wounded : woundedList){
            if(wounded.getType().equals(WoundedTypeEnum.DRIVER))
                return true;
        }
        return false;
    }

    public String formatDate(SimpleDateFormat simpleDateFormat, Instant instant){
        return instant != null ? formatDate(simpleDateFormat, DateUtil.convertIS08601StringToUTCDate(DateUtil.convertUTCInstantToIS08601String(instant))) : "";
    }

    public String formatDate(SimpleDateFormat simpleDateFormat, Date date){
        return date != null ? simpleDateFormat.format(date) : "";
    }

}
