package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.entity.settings.CustomTypeRiskEntity;
import com.doing.nemo.claims.repository.CustomTypeRiskRepository;
import com.doing.nemo.claims.service.CustomTypeRiskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service

public class CustomTypeRiskServiceImpl implements CustomTypeRiskService {

    @Autowired
    private CustomTypeRiskRepository customTypeRiskRepository;

    @Override
    public List<CustomTypeRiskEntity> updateCustomTypeRiskList(List<CustomTypeRiskEntity> customTypeRiskEntityList) {
        if (customTypeRiskEntityList != null)
            for (CustomTypeRiskEntity customTypeRiskEntity : customTypeRiskEntityList) {
                customTypeRiskRepository.save(customTypeRiskEntity);
            }
        return customTypeRiskEntityList;
    }

    @Override
    public void deleteCustomTypeRiskList(List<CustomTypeRiskEntity> customTypeRiskEntityList) {
        if (customTypeRiskEntityList != null) {
            for (CustomTypeRiskEntity customTypeRiskEntity : customTypeRiskEntityList) {
                customTypeRiskRepository.delete(customTypeRiskEntity);
            }
        }
    }
}
