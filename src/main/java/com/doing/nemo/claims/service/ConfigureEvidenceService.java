package com.doing.nemo.claims.service;

import com.doing.nemo.claims.controller.payload.request.ConfigureEvidenceRequestV1;
import com.doing.nemo.claims.controller.payload.response.ConfigureEvidenceResponseV1;
import com.doing.nemo.claims.entity.settings.ConfigureEvidenceEntity;

import java.util.List;
import java.util.UUID;

public interface ConfigureEvidenceService {
    UUID insertConfigureEvidence(ConfigureEvidenceRequestV1 configureEvidenceRequestV1);

    ConfigureEvidenceResponseV1 getConfigureEvidence(UUID configurationId);

    ConfigureEvidenceResponseV1 updateConfigureEvidence(UUID configurationId, ConfigureEvidenceRequestV1 configureEvidenceRequestV1);

    void deleteConfigureEvidence(UUID configurationId);

    ConfigureEvidenceResponseV1 patchStatusConfigureEvidence(UUID uuid);

    List<ConfigureEvidenceResponseV1> getAllConfigureEvidence();

    List<ConfigureEvidenceEntity> findConfigureEvidencesFiltered (Integer page, Integer pageSize, String practicalStateIt, String practicalStateEn, Long daysOfStayInTheState, Boolean isActive, String orderBy, Boolean asc);

    Long countConfigureEvidenceFiltered(String practicalStateIt, String practicalStateEn, Long daysOfStayInTheState, Boolean isActive);
}
