package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.adapter.ContractAdapter;
import com.doing.nemo.claims.controller.payload.request.ClaimsTypeRequestV1;
import com.doing.nemo.claims.controller.payload.request.FlowContractUpdateRequestV1;
import com.doing.nemo.claims.controller.payload.response.ClaimsTypeResponseV1;
import com.doing.nemo.claims.controller.payload.response.ClaimsTypeWithFlowResponseV1;
import com.doing.nemo.claims.controller.payload.response.ContractTypeEntityResponseV1;
import com.doing.nemo.claims.controller.payload.response.PaginationStats;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsFlowEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.settings.*;
import com.doing.nemo.claims.repository.ClaimsTypeRepository;
import com.doing.nemo.claims.repository.ContractTypeRepository;
import com.doing.nemo.claims.repository.ContractTypeRepositoryV1;
import com.doing.nemo.claims.repository.PersonalDataRepository;
import com.doing.nemo.claims.service.ContractService;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.*;

@Service
public class ContractServiceImpl implements ContractService {

    private static Logger LOGGER = LoggerFactory.getLogger(ContractServiceImpl.class);
    @PersistenceContext
    private EntityManager entityManager;
    @Autowired
    private ContractAdapter contractAdapter;
    @Autowired
    private ClaimsTypeRepository claimsTypeRepository;
    @Autowired
    private ContractTypeRepository contractTypeRepository;
    @Autowired
    private ContractTypeRepositoryV1 contractTypeRepositoryV1;
    @Autowired
    private PersonalDataRepository personalDataRepository;

    @Override
    public ClaimsFlowEnum getPersonalRiskFlowType(String contractType, DataAccidentTypeAccidentEnum claimsType, String customerId){
        ClaimsFlowEnum claimsTypeEntity;

        //Verifica aggiunta per vedere se un customer possiede un rischio personalizzato
        if(customerId != null){

            //Recupero i rischi personalizzati del customer che hanno lo stesso AccidentType di quello passato dal front-end
            String q = "SELECT ctr.flow" +
                    " FROM PersonalDataEntity pde INNER JOIN pde.customTypeRiskEntityList ctr" +
                    " WHERE ctr.claimsTypeEntity.claimsAccidentType = :claimsType" +
                    " AND   pde.customerId = :customerId";

            Query query = entityManager.createQuery(q);
            query.setParameter("claimsType", claimsType);
            query.setParameter("customerId", customerId);

            //Anche se un singolo risultato DEVO farmi ritornare una lista, getSingleResult solleva eccezione se non trova risultati
            List queryResult = query.getResultList();

            if(!queryResult.isEmpty()){
                claimsTypeEntity = ClaimsFlowEnum.create((String) queryResult.get(0));
                return claimsTypeEntity;
            }
        }
        return getFlowContractType(contractType, claimsType);
    }

    @Override
    public ClaimsFlowEnum getFlowContractType(String contractType, DataAccidentTypeAccidentEnum claimsType) {

        ContractTypeEntity contractTypeEntity = contractTypeRepository.findByCodContract(contractType);
        ClaimsFlowEnum claimsTypeEntity = null;
        if (contractTypeEntity != null) {
            if (contractTypeEntity.getFlowContractTypeList() == null || contractTypeEntity.getFlowContractTypeList().isEmpty()) {
                claimsTypeEntity = ClaimsFlowEnum.NO;
            } else {

                String s = "SELECT f.flow " +
                        "FROM ContractTypeEntity c inner join c.flowContractTypeList f " +
                        "WHERE c.codContractType = :contractType " +
                        "AND f.claimsTypeEntity.claimsAccidentType = :claimsType";

                Query query = entityManager.createQuery(s);
                query.setParameter("contractType", contractType);
                query.setParameter("claimsType", claimsType);

                claimsTypeEntity = ClaimsFlowEnum.create((String) query.getSingleResult());
            }
        }
        return claimsTypeEntity;
    }

    @Override
    public String getTypeOfChargeBack(String contractType, DataAccidentTypeAccidentEnum claimsType, String customerId){
        //Verifica aggiunta per vedere se un customer possiede un rischio personalizzato
        if(customerId != null) {

            //Recupero i rischi personalizzati del customer che hanno lo stesso AccidentType di quello passato dal front-end
            String q = "SELECT ctr.typeOfChargeback" +
                    " FROM PersonalDataEntity pde INNER JOIN pde.customTypeRiskEntityList ctr" +
                    " WHERE ctr.claimsTypeEntity.claimsAccidentType = :claimsType" +
                    " AND   pde.customerId = :customerId";

            Query query = entityManager.createQuery(q);
            query.setParameter("claimsType", claimsType);
            query.setParameter("customerId", customerId);

            //Anche se un singolo risultato DEVO farmi ritornare una lista, getSingleResult solleva eccezione se non trova risultati
            List queryResult = query.getResultList();

            if (!queryResult.isEmpty()) {
                return (String) queryResult.get(0);
            }
        }
        FlowContractTypeEntity flowContractTypeEntity = getFlowContractTypeEntity(contractType,claimsType);
        if(flowContractTypeEntity != null){
            return flowContractTypeEntity.getRebilling();
        }else {
            return null;
        }

    }

    @Override
    public FlowContractTypeEntity getFlowContractTypeEntity(String contractType, DataAccidentTypeAccidentEnum claimsType) {

        ContractTypeEntity contractTypeEntity = contractTypeRepository.findByCodContract(contractType);
        FlowContractTypeEntity flowContractTypeEntity = null;
        if (contractTypeEntity != null) {

            String s = "SELECT f " +
                    "FROM ContractTypeEntity c inner join c.flowContractTypeList f " +
                    "WHERE c.codContractType = :contractType " +
                    "AND f.claimsTypeEntity.claimsAccidentType = :claimsType";

            Query query = entityManager.createQuery(s, FlowContractTypeEntity.class);
            query.setParameter("contractType", contractType);
            query.setParameter("claimsType", claimsType);

            flowContractTypeEntity = (FlowContractTypeEntity) query.getSingleResult();

        }
        return flowContractTypeEntity;
    }

    @Override
    public ContractTypeEntity getContractType(String contractType) {
        ContractTypeEntity contractTypeEntity = contractTypeRepository.searchContractByCode(contractType);

        return contractTypeEntity;

    }

    @Transactional
    @Override
    public void insertClaimType(List<ClaimsTypeRequestV1.Type> listClaimsType) {

        for (ClaimsTypeRequestV1.Type currentType : listClaimsType) {
            ClaimsTypeEntity c = new ClaimsTypeEntity();
            c.setType(currentType.getType());
            c.setClaimsAccidentType(DataAccidentTypeAccidentEnum.create(currentType.getClaimsAccidentType()));
            entityManager.persist(c);
        }
    }

    @Override
    public UUID insertContract(SettingsRequest settingsRequest) {

        ContractTypeEntity co = new ContractTypeEntity();
        co.setCodContractType(settingsRequest.getCodContractType());
        co.setDescription(settingsRequest.getDescription());
        co.setFlagWS(settingsRequest.getFlagWS());
        co.setDefaultFlow(settingsRequest.getDefaultFlow());
        co.setCtrnote(settingsRequest.getCtrnote());
        co.setRicaricar(settingsRequest.getRicaricar());
        co.setFranchise(settingsRequest.getFranchise());
        co.setActive(settingsRequest.getActive());
        entityManager.persist(co);

        for (FlowContract flowContract : settingsRequest.getFlowContracts()) {

            String flow;
            if (co.getFlagWS() == false)
                flow = "NO";
            else flow = flowContract.getFlow();

            String rebilling = flowContract.getRebilling();


            com.doing.nemo.claims.entity.settings.ClaimsTypeEntity c = claimsTypeRepository.getOne(UUID.fromString(flowContract.getId()));
            c.addContractType(co, flow, rebilling);

            entityManager.persist(c);
        }

        return co.getId();
    }

    private com.doing.nemo.claims.entity.settings.ClaimsTypeEntity getClaimsTypeByType(String typeAccident) {

        String s = "SELECT c FROM ClaimsTypeEntity c WHERE c.type = :typeAccident";

        Query query = entityManager.createQuery(s, ClaimsTypeEntity.class);
        query.setParameter("typeAccident", typeAccident);

        return (ClaimsTypeEntity) query.getSingleResult();

    }

    @Override
    public List<ClaimsTypeResponseV1> getListClaimsType() {
        return ContractAdapter.adptClaimsTypeToClaimsTypeResponse(claimsTypeRepository.findAll());
    }


    @Override
    public List<ContractTypeEntityResponseV1> getListContractType(String contractCode) {

        Query query = null;
        String string = "SELECT * FROM contract_type ct ";

        List<String> contractCodeList = null;

        if (contractCode != null) {
            contractCodeList = Arrays.asList(contractCode.toUpperCase().split(","));

            String where = "WHERE ct.cod_contract_type IN (:contractCodeList) ";
            string += where;

            query = entityManager.createNativeQuery(string, ContractTypeEntity.class).setParameter("contractCodeList", contractCodeList);

        } else {
            query = entityManager.createNativeQuery(string, ContractTypeEntity.class);
        }

        List<ContractTypeEntity> contractTypeEntityList = query.getResultList();
        return ContractAdapter.getContractListAdapter(contractTypeEntityList);

    }

    @Override
    public ContractTypeEntityResponseV1 getContractTypeById(String contractId) {
        Optional<ContractTypeEntity> contractTypeEntityOptional = contractTypeRepository.findById(UUID.fromString(contractId));
        if (!contractTypeEntityOptional.isPresent()) {
            LOGGER.debug("Contract with id " + contractId + " not found");
            throw new NotFoundException("Contract with id " + contractId + " not found", MessageCode.CLAIMS_1010);
        }

        return ContractAdapter.getContractAdapter(contractTypeEntityOptional.get());
    }

    @Override
    public List<ClaimsTypeWithFlowResponseV1> getContractTypeByCodContract(String codContract) {

        String sql = "SELECT cla.type, cla.claims_accident_type, f.flow, Cast(f.claims_type_entity_id as varchar), f.rebilling  " +
                "FROM flow_contract_type f inner join contract_type con ON (contract_type_entity_id = con.id) inner join claims_type cla ON (claims_type_entity_id = cla.id)\n" +
                "WHERE con.cod_contract_type = :codContractType";

        Query query = entityManager.createNativeQuery(sql);
        query.setParameter("codContractType", codContract.toUpperCase());

        List<Object[]> result = query.getResultList();

        return ContractAdapter.adptObjectClaimsTypeWithFlowToClaimsTypeWithFlowResponse(result);
    }


    @Override
    public ContractTypeEntityResponseV1 updateFlowContractList(String contractId, FlowContractUpdateRequestV1 flowContractUpdateRequestV1) {

        List<FlowContract> flowContractList = ContractAdapter.getFlowContractList(flowContractUpdateRequestV1);

        ContractTypeEntity updateContractTypeEntity = entityManager.find(ContractTypeEntity.class, UUID.fromString(contractId));
        updateContractTypeEntity.setFlagWS(flowContractUpdateRequestV1.getFlagWS());
        updateContractTypeEntity.setCodContractType(flowContractUpdateRequestV1.getCodContractType());
        updateContractTypeEntity.setDescription(flowContractUpdateRequestV1.getDescription());
        updateContractTypeEntity.setFranchise(flowContractUpdateRequestV1.getFranchise());
        updateContractTypeEntity.setDefaultFlow(flowContractUpdateRequestV1.getDefaultFlow());
        updateContractTypeEntity.setRicaricar(flowContractUpdateRequestV1.getRicaricar());
        updateContractTypeEntity.setCtrnote(flowContractUpdateRequestV1.getCtrnote());

        List<FlowContractTypeEntity> listaAggiornata = new LinkedList<>();

        if (flowContractList != null && flowContractList.size() > 0) {

            updateContractTypeEntity.getFlowContractTypeList().clear();

            for (FlowContract flowContract : flowContractList) {
                com.doing.nemo.claims.entity.settings.ClaimsTypeEntity c = claimsTypeRepository.getOne(UUID.fromString(flowContract.getId()));

                for (FlowContractTypeEntity fl : c.getFlowContractTypeList()) {
                    if (fl.getClaimsTypeEntity().getId().equals(UUID.fromString(flowContract.getId())) && fl.getContractTypeEntity().getId().equals(UUID.fromString(contractId))) {

                        /*if(updateContractTypeEntity.getFlagWS() == false)
                            fl.setFlow("NO");
                        else*/
                        fl.setFlow(flowContract.getFlow());

                        fl.setRebilling(flowContract.getRebilling());
                        listaAggiornata.add(fl);
                    }
                }

            }

        }
        updateContractTypeEntity.setFlowContractTypeList(listaAggiornata);

        return ContractAdapter.getContractAdapter(entityManager.merge(updateContractTypeEntity));
    }

    @Override
    public ContractTypeEntity patchActive(UUID uuid) {
        ContractTypeEntity updateContractTypeEntity = entityManager.find(ContractTypeEntity.class, uuid);
        if (updateContractTypeEntity != null) {
            if (updateContractTypeEntity.getActive()) {
                updateContractTypeEntity.setActive(false);
            } else updateContractTypeEntity.setActive(true);
        }
        return entityManager.merge(updateContractTypeEntity);
    }

    @Override
    public Pagination<ContractTypeEntity> paginationContractType(int page, int pageSize, String orderBy, Boolean asc, String codContractType, String description, Boolean flagWS, ClaimsFlowEnum defaultFlow, String ctrnote, Boolean ricaricar, Double franchise, Boolean isActive) {
        Pagination<ContractTypeEntity> contractPagination = new Pagination<>();
        List<ContractTypeEntity> contractList = contractTypeRepositoryV1.findPaginationContractType(page, pageSize, orderBy, asc, codContractType, description, flagWS, defaultFlow, ctrnote, ricaricar, franchise, isActive);
        Long itemCount = contractTypeRepositoryV1.countPaginationContractType(codContractType, description, flagWS, defaultFlow, ctrnote, ricaricar, franchise, isActive);
        contractPagination.setItems(contractList);
        contractPagination.setStats(new PaginationStats(itemCount.intValue(), page, pageSize));

        return contractPagination;
    }

}
