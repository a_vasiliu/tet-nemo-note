package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.entity.settings.AutomaticAffiliationRuleLegalEntity;
import com.doing.nemo.claims.entity.settings.ClaimsTypeEntity;
import com.doing.nemo.claims.entity.settings.InsuranceCompanyEntity;
import com.doing.nemo.claims.repository.AutomaticAffiliationRuleLegalRepository;
import com.doing.nemo.claims.repository.ClaimsTypeRepository;
import com.doing.nemo.claims.repository.InsuranceCompanyRepository;
import com.doing.nemo.claims.service.AutomaticAffiliationRuleLegalService;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;


@Service
public class AutomaticAffiliationRuleLegalServiceImpl implements AutomaticAffiliationRuleLegalService {

    private static Logger LOGGER = LoggerFactory.getLogger(AutomaticAffiliationRuleLegalServiceImpl.class);
    @Autowired

    private InsuranceCompanyRepository insuranceCompanyRepository;
    @Autowired

    private AutomaticAffiliationRuleLegalRepository automaticAffiliationRuleLegalRepository;
    @Autowired

    private ClaimsTypeRepository claimsTypeRepository;

    public List<AutomaticAffiliationRuleLegalEntity> updateRuleLegalList(List<AutomaticAffiliationRuleLegalEntity> automaticAffiliationRuleLegalEntityList) {

        Set<String> automaticAffiliationRuleSetForDuplicate = new HashSet<>();
        for (AutomaticAffiliationRuleLegalEntity automaticAffiliationRuleLegalEntity : automaticAffiliationRuleLegalEntityList) {
            if (!automaticAffiliationRuleSetForDuplicate.add(automaticAffiliationRuleLegalEntity.getSelectionName())) {
                LOGGER.debug("Error in Legal Rule, duplicate name not admitted");
                throw new BadRequestException("Error in Legal Rule, duplicate name not admitted", MessageCode.CLAIMS_1009);
            }
        }
        List<AutomaticAffiliationRuleLegalEntity> automaticAffiliationRuleLegalEntityList1 = new ArrayList<>();
        if (automaticAffiliationRuleLegalEntityList != null) {
            for (AutomaticAffiliationRuleLegalEntity automaticAffiliationRuleLegalEntity1 : automaticAffiliationRuleLegalEntityList) {
                updateRuleLegal(automaticAffiliationRuleLegalEntity1);
                automaticAffiliationRuleLegalEntityList1.add(automaticAffiliationRuleLegalEntity1);

            }
        }
        return automaticAffiliationRuleLegalEntityList1;
    }

    public AutomaticAffiliationRuleLegalEntity updateRuleLegal(AutomaticAffiliationRuleLegalEntity automaticAffiliationRuleLegalEntity) {
        List<InsuranceCompanyEntity> insuranceCompanyEntityList = new ArrayList<>();
        if (automaticAffiliationRuleLegalEntity.getCounterpartInsuranceCompanyList() != null) {
            for (InsuranceCompanyEntity insuranceCompanyEntity : automaticAffiliationRuleLegalEntity.getCounterpartInsuranceCompanyList()) {
                Optional<InsuranceCompanyEntity> insuranceCompanyEntity1 = insuranceCompanyRepository.findById(insuranceCompanyEntity.getId());
                if (insuranceCompanyEntity1.isPresent()) {
                    insuranceCompanyEntity = insuranceCompanyEntity1.get();
                    insuranceCompanyRepository.save(insuranceCompanyEntity);
                    insuranceCompanyEntityList.add(insuranceCompanyEntity);
                }

            }
        }


        List<InsuranceCompanyEntity> insuranceCompanyEntityList2 = new ArrayList<>();
        if (automaticAffiliationRuleLegalEntity.getDamagedInsuranceCompanyList() != null) {
            for (InsuranceCompanyEntity insuranceCompanyEntity : automaticAffiliationRuleLegalEntity.getDamagedInsuranceCompanyList()) {
                Optional<InsuranceCompanyEntity> insuranceCompanyEntity1 = insuranceCompanyRepository.findById(insuranceCompanyEntity.getId());
                if (insuranceCompanyEntity1.isPresent()) {
                    insuranceCompanyEntity = insuranceCompanyEntity1.get();
                    insuranceCompanyRepository.save(insuranceCompanyEntity);
                    insuranceCompanyEntityList2.add(insuranceCompanyEntity);
                }

            }
        }

        List<ClaimsTypeEntity> claimsTypeEntityList = new ArrayList<>();
        if (automaticAffiliationRuleLegalEntity.getClaimsType() != null) {
            for (ClaimsTypeEntity claimsTypeEntity : automaticAffiliationRuleLegalEntity.getClaimsType()) {
                Optional<ClaimsTypeEntity> claimsTypeEntity1 = claimsTypeRepository.findById(claimsTypeEntity.getId());
                if (claimsTypeEntity1.isPresent()) {
                    claimsTypeEntity = claimsTypeEntity1.get();
                    claimsTypeRepository.save(claimsTypeEntity);
                    claimsTypeEntityList.add(claimsTypeEntity);
                }
            }
        }

        automaticAffiliationRuleLegalEntity.setDamagedInsuranceCompanyList(insuranceCompanyEntityList2);
        automaticAffiliationRuleLegalEntity.setCounterpartInsuranceCompanyList(insuranceCompanyEntityList);
        automaticAffiliationRuleLegalEntity.setClaimsType(claimsTypeEntityList);
        automaticAffiliationRuleLegalRepository.save(automaticAffiliationRuleLegalEntity);
        return automaticAffiliationRuleLegalEntity;
    }

    public AutomaticAffiliationRuleLegalEntity deleteRuleLegal(List<AutomaticAffiliationRuleLegalEntity> automaticAffiliationRuleLegalEntities) {
        if (automaticAffiliationRuleLegalEntities != null) {
            for (AutomaticAffiliationRuleLegalEntity automaticAffiliationRuleLegalEntity : automaticAffiliationRuleLegalEntities) {
                automaticAffiliationRuleLegalRepository.delete(automaticAffiliationRuleLegalEntity);
            }
        }
        return null;
    }


}
