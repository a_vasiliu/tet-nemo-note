package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.entity.settings.InsurancePolicyPersonalDataEntity;
import com.doing.nemo.claims.repository.InsurancePolicyPersonalDataRepository;
import com.doing.nemo.claims.service.InsurancePolicyPersonalDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service

public class InsurancePolicyPersonalDataServiceImpl implements InsurancePolicyPersonalDataService {

    @Autowired
    private InsurancePolicyPersonalDataRepository insurancePolicyPersonalDataRepository;

    @Override
    public List<InsurancePolicyPersonalDataEntity> updateInsurancePolicyPersonalDataList(List<InsurancePolicyPersonalDataEntity> insurancePolicyPersonalDataEntityList) {
        if (insurancePolicyPersonalDataEntityList != null)
            for (InsurancePolicyPersonalDataEntity insurancePolicyPersonalDataEntity : insurancePolicyPersonalDataEntityList) {
                insurancePolicyPersonalDataRepository.save(insurancePolicyPersonalDataEntity);
            }
        return insurancePolicyPersonalDataEntityList;
    }

    @Override
    public void deleteInsurancePolicyPersonalDataList(List<InsurancePolicyPersonalDataEntity> insurancePolicyPersonalDataEntityList) {
        if (insurancePolicyPersonalDataEntityList != null) {
            for (InsurancePolicyPersonalDataEntity insurancePolicyPersonalDataEntity : insurancePolicyPersonalDataEntityList) {
                insurancePolicyPersonalDataRepository.delete(insurancePolicyPersonalDataEntity);
            }
        }
    }
}