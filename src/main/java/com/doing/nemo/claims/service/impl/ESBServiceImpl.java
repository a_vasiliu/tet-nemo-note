package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.adapter.*;
import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.common.util.ServiceUtil;
import com.doing.nemo.claims.controller.payload.response.*;
import com.doing.nemo.claims.controller.payload.response.damaged.*;
import com.doing.nemo.claims.controller.payload.response.insurancecompany.LegalCostResponse;
import com.doing.nemo.claims.controller.payload.response.insurancecompany.PaiResponse;
import com.doing.nemo.claims.controller.payload.response.insurancecompany.TheftResponse;
import com.doing.nemo.claims.controller.payload.response.insurancecompany.TplResponse;
import com.doing.nemo.claims.controller.payload.response.registration.PlateRegistrationHistoryItem;
import com.doing.nemo.claims.entity.enumerated.PolicyEnum.PolicyTypeEnum;
import com.doing.nemo.claims.entity.esb.ContractESB.ContractEsb;
import com.doing.nemo.claims.entity.esb.CustomerESB.CustomerEsb;
import com.doing.nemo.claims.entity.esb.DriverESB.DriverEsb;
import com.doing.nemo.claims.entity.esb.FleetManagerESB.FleetManagerEsb;
import com.doing.nemo.claims.entity.esb.InsuranceCompanyESB.InsuranceCompanyEsb;
import com.doing.nemo.claims.entity.esb.RegistryESB;
import com.doing.nemo.claims.entity.esb.VehicleESB.VehicleEsb;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbInsuranceCompany.Kasko;
import com.doing.nemo.claims.entity.jsonb.personalData.FleetManagerPersonalData;
import com.doing.nemo.claims.entity.settings.ContractTypeEntity;
import com.doing.nemo.claims.entity.settings.InsurancePolicyEntity;
import com.doing.nemo.claims.entity.settings.PersonalDataEntity;
import com.doing.nemo.claims.exception.ForbiddenException;
import com.doing.nemo.claims.repository.ContractTypeRepository;
import com.doing.nemo.claims.repository.InsurancePolicyRepository;
import com.doing.nemo.claims.repository.PersonalDataRepository;
import com.doing.nemo.claims.service.ESBService;
import com.doing.nemo.claims.service.GoLiveStrategyService;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.AbstractException;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.exception.InternalException;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import com.doing.nemo.commons.util.HttpUtil;
import com.doing.nemo.middleware.client.MiddlewareClient;
import com.doing.nemo.middleware.client.payload.response.MiddlewareClaimInfoResponse;
import com.doing.nemo.middleware.client.payload.response.MiddlewareVehicleRegistrationsResponse;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.Response;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.Collectors;

@Service
public class ESBServiceImpl implements ESBService {

    private static Logger LOGGER = LoggerFactory.getLogger(ESBServiceImpl.class);

    @Value("${middleware.endpoint}")
    private String middlewareEndpoint;

    @Value("${middleware.api.contracts.id}")
    private String middlewareContractByIdUri;

    @Value("${middleware.api.contracts.id.detail}")
    private String middlewareContractDetailByIdUri;

    @Value("${middleware.api.customers.id}")
    private String middlewareCustomerByIdUri;

    @Value("${middleware.api.drivers.id}")
    private String middlewareDriverByIdUri;

    @Value("${middleware.api.vehicles.plate}")
    private String middlewareVehicleByPlateUri;

    @Value("${middleware.api.vehicles.plate.accessories}")
    private String middlewareVehicleAccessoriesByPlateUri;

    @Value("${middleware.api.vehicleExchanges.plate}")
    private String middlewareVehicleExchangeByPlateUri;

    @Value("${middleware.api.customers.id.fleetmanagers}")
    private String middlewareCustomersFleetManagers;

    @Value("${middleware.api.customers.id.fleetmanagers.id}")
    private String middlewareCustomersFleetManagerById;

    @Value("${middleware.api.contracts.id.insurance}")
    private String middlewareCustomersInsuranceByCustomerId;

    @Value("${middleware.api.contracts.id.telematics}")
    private String middlewareCustomersTelematicsByCustomerId;

    @Value("${middleware.api.contracts.id.billingReturn}")
    private String middlewareContractssBillingReturnByCustomerId;

    @Value("${middleware.api.contracts.id.return}")
    private String middlewareContractssReturnByCustomerId;

    @Value("${middleware.conn.timeout}")
    private Integer middlewareConnTimeout;

    @Value("${middleware.read.timeout}")
    private Integer middlewareReadTimeout;

    @Value("${middleware.write.timeout}")
    private Integer middlewareWriteTimeout;

    @Autowired
    private HttpUtil httpUtil;

    @Autowired
    private InsuranceCompanyAdapter insuranceCompanyAdapter;

    @Autowired
    private InsurancePolicyRepository insurancePolicyRepository;

    @Autowired
    private ContractTypeRepository contractTypeRepository;

    @Autowired
    private MiddlewareClient middlewareClient;

    @Autowired
    private GoLiveStrategyService goLiveStrategyService;

    @Autowired
    private PersonalDataRepository personalDataRepository;


    @Autowired
    private VehicleRegistrationResponseAdapter vehicleRegistrationResponseAdapter;

    @Autowired
    private VehicleEsbResponseAdapter vehicleEsbResponseAdapter;

    @Autowired
    private ServiceUtil serviceUtil;

    private <T, P> T callWithoutCert(String url, HttpMethod method, P value, HashMap<String, String> headersparams, Class<T> outclass) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = null;
        Response responseToken = null;
        try {
            if (value != null) {
                jsonInString = mapper.writeValueAsString(value);
            }

            responseToken = httpUtil.callURL(headersparams, url, jsonInString, method,
                    middlewareConnTimeout, middlewareReadTimeout, middlewareWriteTimeout);


            if (responseToken.code() == HttpStatus.NOT_FOUND.value()) {
                LOGGER.debug(responseToken.message() + " with code " + responseToken.code());
                throw new NotFoundException(responseToken.message() + " with code " + responseToken.code(), MessageCode.CLAIMS_1010);
            }

            if (responseToken.code() != HttpStatus.OK.value()) {
                LOGGER.debug(responseToken.message() + " with code " + responseToken.code());
                throw new BadRequestException(responseToken.message() + " with code " + responseToken.code(), MessageCode.CLAIMS_2000);
            }
            String response = responseToken.body().string();
            LOGGER.debug("Response ESB Body: {}", response);
            return mapper.readValue(response, outclass);
        } catch (AbstractException e) {
            LOGGER.error(e.getMessage(), e);
            throw e;
        }  catch (Exception e) {
            String message = String.format(MessageCode.CLAIMS_2012.value(), e.getClass().getSimpleName(), e.getMessage());
            LOGGER.error(message, e);
            throw new InternalException(message, MessageCode.CLAIMS_2012);
        } finally {
            if (responseToken != null && responseToken.body() != null) {
                responseToken.body().close();
            }
        }

    }


    @Override
    public ContractInfoResponseV1 getContractByContractIdOrPlate(String contractOrPlate, String date,String profile) throws IOException {

        String queryDate = date;
        Date date1 = new Date();
        if (queryDate != null && !queryDate.isEmpty()) {
            date1 = DateUtil.convertIS08601StringToUTCDate(queryDate);
        }
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMMdd"); //note: time zone not in format!
        sdf2.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));
        queryDate = sdf2.format(date1);
        LOGGER.debug("Data passata con fuso orario di Roma ESB: "+queryDate);


        SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy-MM-dd"); //note: time zone not in format!
        sdf2.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));
        String checkEndDate = sdf3.format(date1) + "T00:00:00Z";

        ContractInfoResponseV1 result = null;

        result = this.getDriverContractCustomerCustomerInfo(contractOrPlate, queryDate, checkEndDate, profile);
        String formattedDate = queryDate.substring(0, 4) + "-" + queryDate.substring(4, 6) + "-" + queryDate.substring(6, 8);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
        formattedDate += "T00:00:00Z";
        //Date date1 = format.parse(formattedDate);
        Instant dateInstant = DateUtil.convertIS08601StringToUTCInstant(formattedDate);

        if(result != null && result.getCustomerResponse()!= null && result.getCustomerResponse().getCustomerId() != null){
            PersonalDataEntity user = personalDataRepository.findByCustomerId(result.getCustomerResponse().getCustomerId());
            List<FleetManagerResponseV1> newFleetList = new LinkedList<>();
            if(user!=null){
                for(FleetManagerResponseV1 fleetR: result.getFleetManagerResponseList()) {
                    for (FleetManagerPersonalData fleet : user.getFleetManagerPersonalData()) {
                        if (fleetR.getId().equals(fleet.getFleetManagerId())) {
                            if (!StringUtils.isEmpty(fleet.getFleetManagerSecondaryEmail())) {
                                fleetR.setEmail(fleet.getFleetManagerSecondaryEmail());
                            }
                            fleetR.setDisableNotification(fleet.isDisableNotification());
                        }
                    }
                    newFleetList.add(fleetR);
                }
                for(FleetManagerPersonalData fleet: user.getFleetManagerPersonalData()){
                    if(fleet.getIsImported()!= null && !fleet.getIsImported()){
                        FleetManagerResponseV1 fleetR = FleetManagerAdpter.adptFromFleetManagerPersonalDataToFleetManagerResponseV1(fleet);
                        newFleetList.add(fleetR);
                    }
                }
            }else{
                newFleetList.addAll(result.getFleetManagerResponseList());
            }

            result.setFleetManagerResponseList(newFleetList);

        }
        if (result != null && result.getInsuranceCompany() != null && result.getCustomerResponse() != null) {

            InsuranceCompanyResponse insuranceCompany = result.getInsuranceCompany();
            CustomerResponse customerResponse = result.getCustomerResponse();
            String tariffId = null;
            String customerId = null;
            List<InsurancePolicyEntity> insurancePolicyEntityList = null;
            InsurancePolicyEntity insurancePolicyEntity = null;

            if (insuranceCompany.getTpl() != null) {
                tariffId = insuranceCompany.getTpl().getTariffId();
            }

            if (customerResponse.getCustomerId() != null)
                customerId = customerResponse.getCustomerId();

            if (tariffId != null) {
                //caso assicurato ALD
                insurancePolicyEntityList = insurancePolicyRepository.searchInsurancePolicyForGetContract(tariffId, dateInstant, PolicyTypeEnum.RCA);
                if (insurancePolicyEntityList == null || insurancePolicyEntityList.isEmpty()) {
                    LOGGER.debug(MessageCode.CLAIMS_1048.value());
                    throw new NotFoundException(MessageCode.CLAIMS_1048);
                }
                insurancePolicyEntity = insurancePolicyEntityList.get(0);
                if (insurancePolicyEntity == null) {
                    LOGGER.debug(MessageCode.CLAIMS_1048.value());
                    throw new NotFoundException(MessageCode.CLAIMS_1048);
                }
            } else if (customerId != null) {

                //caso autoassicurato
                insurancePolicyEntityList = insurancePolicyRepository.searchInsurancePolicyForGetContractByCustomer(Long.parseLong(customerId), dateInstant, PolicyTypeEnum.RCA);

                if (insurancePolicyEntityList == null || insurancePolicyEntityList.isEmpty()) {
                    LOGGER.debug(MessageCode.CLAIMS_1048.value());
                    throw new NotFoundException(MessageCode.CLAIMS_1048);
                }
                insurancePolicyEntity = insurancePolicyEntityList.get(0);

                if (insurancePolicyEntity == null) {
                    LOGGER.debug(MessageCode.CLAIMS_1048.value());
                    throw new NotFoundException(MessageCode.CLAIMS_1048);
                }
                insuranceCompany.setTpl(new TplResponse());

            }
            insuranceCompany.getTpl().setPolicyNumber(insurancePolicyEntity.getNumberPolicy());

            if (insurancePolicyEntity.getBeginningValidity() != null) {
                String beginningValidity = DateUtil.convertUTCInstantToIS08601String(insurancePolicyEntity.getBeginningValidity());
                beginningValidity = beginningValidity.substring(0, 10);
                insuranceCompany.getTpl().setStartDate(beginningValidity);
            }

            if (insurancePolicyEntity.getEndValidity() != null) {
                String endValidity = DateUtil.convertUTCInstantToIS08601String(insurancePolicyEntity.getEndValidity());
                endValidity = endValidity.substring(0, 10);
                insuranceCompany.getTpl().setEndDate(endValidity);
            }

            insuranceCompany.getTpl().setCompany(insurancePolicyEntity.getInsuranceCompanyEntity().getName());
            insuranceCompany.getTpl().setInsuranceCompanyId(insurancePolicyEntity.getInsuranceCompanyEntity().getId());
            insuranceCompany.getTpl().setInsurancePolicyId(insurancePolicyEntity.getId());

            insurancePolicyEntity = null;
            String tariffIdPai = null;

            if (insuranceCompany.getPai() != null) {
                tariffIdPai = insuranceCompany.getPai().getTariffId();
            }

            if (customerResponse.getCustomerId() != null)
                customerId = customerResponse.getCustomerId();

            if (tariffIdPai != null) {
                //polizza assicurativa PAI per cliente assicurato ALD
                insurancePolicyEntityList = insurancePolicyRepository.searchInsurancePolicyForGetContract(tariffIdPai, dateInstant, PolicyTypeEnum.PAI);
                if (insurancePolicyEntityList != null && !insurancePolicyEntityList.isEmpty())
                    insurancePolicyEntity = insurancePolicyEntityList.get(0);

            } else if (customerId != null) {
                //polizza assicurativa PAI per cliente AUTOassicurato ALD
                insurancePolicyEntityList = insurancePolicyRepository.searchInsurancePolicyForGetContractByCustomer(Long.parseLong(customerId), dateInstant, PolicyTypeEnum.PAI);
                if (insurancePolicyEntityList != null && !insurancePolicyEntityList.isEmpty())
                    insurancePolicyEntity = insurancePolicyEntityList.get(0);

                if (insurancePolicyEntity != null) {

                    insuranceCompany.setPai(new PaiResponse());
                }
            }

            if (insurancePolicyEntity != null) {
                insuranceCompany.getPai().setInsuranceCompanyId(insurancePolicyEntity.getInsuranceCompanyEntity().getId());
                insuranceCompany.getPai().setInsurancePolicyId(insurancePolicyEntity.getId());
                insuranceCompany.getPai().setPolicyNumber(insurancePolicyEntity.getNumberPolicy());

                if (insurancePolicyEntity.getBeginningValidity() != null) {
                    String beginningValidity = DateUtil.convertUTCInstantToIS08601String(insurancePolicyEntity.getBeginningValidity());
                    beginningValidity = beginningValidity.substring(0, 10);
                    insuranceCompany.getPai().setStartDate(beginningValidity);
                }

                if (insurancePolicyEntity.getEndValidity() != null) {
                    String endValidity = DateUtil.convertUTCInstantToIS08601String(insurancePolicyEntity.getEndValidity());
                    endValidity = endValidity.substring(0, 10);
                    insuranceCompany.getPai().setEndDate(endValidity);
                }
            }

            insurancePolicyEntity = null;
            String tariffIdLegalCost = null;
            if (insuranceCompany.getLegalCost() != null) {
                tariffIdLegalCost = insuranceCompany.getLegalCost().getTariffId();
            }
            if (customerResponse.getCustomerId() != null)
                customerId = customerResponse.getCustomerId();
            if (tariffIdLegalCost != null) {

                insurancePolicyEntityList = insurancePolicyRepository.searchInsurancePolicyForGetContract(tariffIdLegalCost, dateInstant, PolicyTypeEnum.LEGAL);
                if (insurancePolicyEntityList != null && !insurancePolicyEntityList.isEmpty())
                    insurancePolicyEntity = insurancePolicyEntityList.get(0);

            } else if (customerId != null) {

                insurancePolicyEntityList = insurancePolicyRepository.searchInsurancePolicyForGetContractByCustomer(Long.parseLong(customerId), dateInstant, PolicyTypeEnum.LEGAL);
                if (insurancePolicyEntityList != null && !insurancePolicyEntityList.isEmpty())
                    insurancePolicyEntity = insurancePolicyEntityList.get(0);

                if (insurancePolicyEntity != null) {

                    insuranceCompany.setLegalCost(new LegalCostResponse());
                }
            }

            if (insurancePolicyEntity != null) {
                insuranceCompany.getLegalCost().setInsuranceCompanyId(insurancePolicyEntity.getInsuranceCompanyEntity().getId());
                insuranceCompany.getLegalCost().setInsurancePolicyId(insurancePolicyEntity.getId());
                insuranceCompany.getLegalCost().setPolicyNumber(insurancePolicyEntity.getNumberPolicy());

                if (insurancePolicyEntity.getBeginningValidity() != null) {
                    String beginningValidity = DateUtil.convertUTCInstantToIS08601String(insurancePolicyEntity.getBeginningValidity());
                    beginningValidity = beginningValidity.substring(0, 10);
                    insuranceCompany.getLegalCost().setStartDate(beginningValidity);
                }

                if (insurancePolicyEntity.getEndValidity() != null) {
                    String endValidity = DateUtil.convertUTCInstantToIS08601String(insurancePolicyEntity.getEndValidity());
                    endValidity = endValidity.substring(0, 10);
                    insuranceCompany.getLegalCost().setEndDate(endValidity);
                }
            }


            result.setInsuranceCompany(insuranceCompany);
        }


        if (result != null) {
            //controllo che per quel contratto ci sia un contratto valido in nemo
            ContractResponse contractResponse = result.getContractResponse();
            if (contractResponse.getContractType() != null) {
                ContractTypeEntity contractTypeEntity = contractTypeRepository.searchContractByCode(contractResponse.getContractType());
                if (contractTypeEntity == null || contractTypeEntity.getFlagWS() == null || !contractTypeEntity.getFlagWS()) {
                    LOGGER.debug(MessageCode.CLAIMS_1042.value());
                    throw new BadRequestException(MessageCode.CLAIMS_1042);
                }
            }
        }

        if (result != null && result.getContractResponse() != null && result.getContractResponse().getLeaseServiceComponentsList() != null) {
            //recupero i serivizi aggiuntivi
            Iterator<LeaseServiceComponentsResponse> iLeaseServiceComponentResponse = result.getContractResponse().getLeaseServiceComponentsList().iterator();


            while (iLeaseServiceComponentResponse.hasNext()) {

                LeaseServiceComponentsResponse leaseServiceComponentsResponse = iLeaseServiceComponentResponse.next();
                if (leaseServiceComponentsResponse.getName() != null && (leaseServiceComponentsResponse.getName().equalsIgnoreCase("Full MD self-insured") || leaseServiceComponentsResponse.getName().equalsIgnoreCase("MD Customer managed"))) {


                    Kasko kasko = null;

                    if (leaseServiceComponentsResponse.getQualifiersList() != null) {
                        Iterator<QualifiersResponse> iQualifierResponse = leaseServiceComponentsResponse.getQualifiersList().iterator();

                        while (iQualifierResponse.hasNext() && kasko == null) {
                            QualifiersResponse qualifiersResponse = iQualifierResponse.next();
                            System.out.println(qualifiersResponse);
                            if (qualifiersResponse.getAttributeDescription().equalsIgnoreCase("Deductible")) {
                                kasko = new Kasko();
                                kasko.setDeductibleId(Integer.parseInt(qualifiersResponse.getValue()));
                                kasko.setDeductibleValue(qualifiersResponse.getValueDescription());
                            }
                        }
                        if (kasko != null)
                            result.getInsuranceCompany().setKasko(KaskoAdapter.adptFromKaskoToKaskoResponse(kasko));

                    }
                } else if (leaseServiceComponentsResponse.getName() != null && leaseServiceComponentsResponse.getName().equalsIgnoreCase("Theft Insurance")) {
                    if (leaseServiceComponentsResponse.getQualifiersList() != null) {
                        Iterator<QualifiersResponse> iQualifierResponse = leaseServiceComponentsResponse.getQualifiersList().iterator();
                        boolean isFound = false;
                        while (iQualifierResponse.hasNext() && !isFound) {
                            QualifiersResponse qualifiersResponse = iQualifierResponse.next();
                            if (qualifiersResponse.getAttributeDescription().equalsIgnoreCase("Deductible")) {
                                isFound = true;
                                TheftResponse theft = result.getInsuranceCompany().getTheft();
                                if (theft == null) {
                                    theft = new TheftResponse();
                                }
                                if (qualifiersResponse.getValue() != null) {
                                    theft.setDeductibleId(Long.parseLong(qualifiersResponse.getValue()));
                                }
                                if (qualifiersResponse.getValueDescription() != null) {
                                    theft.setDeductible(qualifiersResponse.getValueDescription());
                                }
                                result.getInsuranceCompany().setTheft(theft);
                            }
                        }
                    }
                } else if (leaseServiceComponentsResponse.getName() != null && leaseServiceComponentsResponse.getName().equalsIgnoreCase("TPL - Third Party Liability")) {
                    if (leaseServiceComponentsResponse.getQualifiersList() != null) {
                        Iterator<QualifiersResponse> iQualifierResponse = leaseServiceComponentsResponse.getQualifiersList().iterator();
                        boolean isFound = false;
                        while (iQualifierResponse.hasNext() && !isFound) {
                            QualifiersResponse qualifiersResponse = iQualifierResponse.next();
                            if (qualifiersResponse.getAttributeDescription().equalsIgnoreCase("Deductible")) {
                                isFound = true;
                                TplResponse tpl = result.getInsuranceCompany().getTpl();
                                tpl.setDeductible(qualifiersResponse.getValueDescription());
                                result.getInsuranceCompany().setTpl(tpl);
                            }
                        }
                    }
                }
            }
        }

        if (result != null && result.getInsuranceCompany() != null) {
            if (result.getInsuranceCompany().getPai() != null && result.getInsuranceCompany().getPai().getInsuranceCompanyId() == null) {
                result.getInsuranceCompany().setPai(null);
            }
            if (result.getInsuranceCompany().getLegalCost() != null && result.getInsuranceCompany().getLegalCost().getInsuranceCompanyId() == null) {
                result.getInsuranceCompany().setLegalCost(null);
            }
        }

        return result;
    }

    @Override
    public CustomerEsb getCustomer(String customerId) throws IOException {

        String url = getCustomerByIdUrl(customerId);

        return callWithoutCert(url, HttpMethod.GET, null, null, CustomerEsb.class);
    }

    @Override
    public VehicleEsb getVehicle(String plate) throws IOException {

        String url = getVehicleByPlateUrl(plate);

        return callWithoutCert(url, HttpMethod.GET, null, null, VehicleEsb.class);
    }

    @Override
    public ContractEsb getContractDetail(String idcontract, String date) throws IOException {

        String url = getContractDetailByIdUrl(idcontract, date);

        return callWithoutCert(url, HttpMethod.GET, null, null, ContractEsb.class);
    }

    public InsuranceCompanyEsb getInsuranceCompany(String idcontract, String date) throws IOException {

        String url = getInsuranceCompanyByIdUrl(idcontract, date);

        return callWithoutCert(url, HttpMethod.GET, null, null, InsuranceCompanyEsb.class);
    }

    @Override
    public BlockingQueue asynchronousEsbcalls(String idCustomer, String idDriver, String plate, boolean isPlate, String idContract, String date, BlockingQueue responseList, Boolean isAld) {

        BlockingQueue<Integer> fleetManagersId = new LinkedBlockingQueue<>();
        BlockingQueue<FleetManagerEsb> fleetManagersInfoList = new LinkedBlockingQueue<>();
        BlockingQueue<RegistryESB> registryESBBlockingQueue = new ArrayBlockingQueue<>(10000);
        ESBCallThread callDriverESB = null;
        if (idDriver != null) {
            String driverUrl = getDriverByIdUrl(idDriver);

            callDriverESB = new ESBCallThread(driverUrl,
                    "driver", HttpMethod.GET, null, fleetManagersId, fleetManagersInfoList, registryESBBlockingQueue, responseList);
        }
        String customerUrl = getCustomerByIdUrl(idCustomer);
        ESBCallThread callCustomerESB = new ESBCallThread(customerUrl,
                "customer", HttpMethod.GET, null, fleetManagersId, fleetManagersInfoList, registryESBBlockingQueue, responseList);

        String fleetmanagerListUrl = getFleetmanagersListByIdUrl(idCustomer);
        ESBCallThread callFleetManagersIdESB = new ESBCallThread(fleetmanagerListUrl,
                "fleetmanagersId", HttpMethod.GET, null, fleetManagersId, fleetManagersInfoList, registryESBBlockingQueue, responseList);

        String insuranceCompanyUrl = getInsuranceCompanyByIdUrl(idContract, date);
        ESBCallThread callInsuranceCompany = new ESBCallThread(insuranceCompanyUrl,
                "insurancecompany", HttpMethod.GET, null, fleetManagersId, fleetManagersInfoList, registryESBBlockingQueue, responseList);

        String telematicsUrl = getTelematicsByIdUrl(idContract, date);
        ESBCallThread callTelematics = new ESBCallThread(telematicsUrl,
                "telematics", HttpMethod.GET, null, fleetManagersId, fleetManagersInfoList, registryESBBlockingQueue, responseList);

        ESBCallThread callVehicleESB = null;
        if (!isPlate) {
            String vehicleUrl = getVehicleByPlateUrl(plate);
            callVehicleESB = new ESBCallThread(vehicleUrl,
                    "vehicle", HttpMethod.GET, null, fleetManagersId, fleetManagersInfoList, registryESBBlockingQueue, responseList);
        }

        try {
            if (callDriverESB != null)
                callDriverESB.startThread();
            callCustomerESB.startThread();
            callInsuranceCompany.startThread();
            if (!isAld) {
                callFleetManagersIdESB.startThread();
                callTelematics.startThread();
                if (callVehicleESB != null) {
                    callVehicleESB.startThread();
                    callVehicleESB.join();
                }
            }
            if (callDriverESB != null)
                callDriverESB.join();
            callCustomerESB.join();
            callInsuranceCompany.join();
            if (!isAld) {
                callFleetManagersIdESB.join();


                List<Thread> fleetThreads = new LinkedList<>();
                for (Integer currentId : fleetManagersId) {
                    String fleetmanagerUrl = getFleetmanagersByIdUrl(idCustomer, currentId.toString());
                    ESBCallThread fleetThread = new ESBCallThread(fleetmanagerUrl,
                            "fleetmanagers", HttpMethod.GET, null, fleetManagersId, fleetManagersInfoList, registryESBBlockingQueue, responseList);
                    fleetThreads.add(fleetThread);
                    fleetThread.startThread();
                }


                for (Thread currentFM : fleetThreads) {
                    currentFM.join();
                }
                callTelematics.join();
                responseList.add(fleetManagersInfoList);
                responseList.add(registryESBBlockingQueue);
            }


        } catch (InterruptedException e) {
            LOGGER.debug(e.getMessage());
        }

        return responseList;
    }

    private String getFleetmanagersByIdUrl(String idCustomer, String idFleetmanager) {

        return buildUrl(
                middlewareEndpoint,
                middlewareCustomersFleetManagerById,
                new HashMap<String, String>() {{
                    put("customerId", idCustomer);
                    put("fleetManagerId", idFleetmanager);
                }},
                null
        );
    }

    @Override
    public ContractInfoResponseV1 getDriverContractCustomerCustomerInfo(String contractOrPlate, String date, String endDateCheck, String profile) throws IOException {

        BlockingQueue<Object> responseList = new ArrayBlockingQueue<>(7);

        ContractInfoResponseV1 contractInfoResponseV1 = new ContractInfoResponseV1();


        String formattedDate = date.substring(0, 4) + "-" + date.substring(4, 6) + "-" + date.substring(6, 8);

        String regex = "\\d+";
        boolean isPlate = false;
        VehicleEsb vehicle = null;
        try {
            //it's a plate

            if (!contractOrPlate.matches(regex)) {
                isPlate = true;
                try {
                       /*
                    CO-643 ricerca storico delle targhe
                    * */
                    //    vehicle = getVehicle(contractOrPlate);
                    vehicle = this.getPlateFromVehicleRegistrations(contractOrPlate, date);

                } catch (NotFoundException ex) {
                    LOGGER.debug(MessageCode.CLAIMS_1017.value());
                    throw new NotFoundException(MessageCode.CLAIMS_1017, ex);
                }
                Long idContract = vehicle.getIdActiveContract(formattedDate);

                if (idContract == null) {
                    //non esiste alcun contratto attivo associato alla targa
                    LOGGER.debug("Plate " + contractOrPlate + " is not related to an active contract");
                    throw new ForbiddenException("Plate " + contractOrPlate + " is not related to an active contract", MessageCode.CLAIMS_1025);
                }
                contractOrPlate = vehicle.getIdActiveContract(formattedDate).toString();
                contractInfoResponseV1.setVehicleResponse(VehicleAdapter.adptMiddlewareVehicleResponseToVehicleResponse(vehicle));
            }
            ContractEsb contract = getContractDetail(contractOrPlate, date);
            if (contract == null) {
                LOGGER.debug(MessageCode.CLAIMS_1015.value());
                throw new NotFoundException(MessageCode.CLAIMS_1015);
            } else if (contract.getTakeinDate() != null) {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                Date dateSX = DateUtil.convertIS08601StringToUTCDate(endDateCheck);
                String dateSXString = simpleDateFormat.format(dateSX);
                String takeinDate = simpleDateFormat.format(contract.getTakeinDate());
                if (dateSXString.compareTo(takeinDate) > 0) {
                    LOGGER.info(MessageCode.CLAIMS_2009.value());
                    throw new BadRequestException(MessageCode.CLAIMS_2009);
                }
            }

            contractInfoResponseV1.setContractResponse(ContractAdapter.adptMiddlewareContractDetailResponseToContractResponse(contract));
            if (contract.getCustomerId() == null) {
                LOGGER.debug(MessageCode.CLAIMS_1012.value());
                throw new NotFoundException(MessageCode.CLAIMS_1012);
            }
            /*if(contract.getDriverId() == null){
                throw new NotFoundException(MessageCode.CLAIMS_1013);
            }*/
            if (contract.getLicensePlate() == null) {
                LOGGER.debug(MessageCode.CLAIMS_1014.value());
                throw new NotFoundException(MessageCode.CLAIMS_1014);
            }
            String idCustomer = contract.getCustomerId().toString();

            //Controlli strategia di GO LIVE.
            goLiveStrategyService.goLiveStrategyChecksForInsert(idCustomer, null, profile);

            String idDriver = null;
            if (contract.getDriverId() != null)
                idDriver = contract.getDriverId().toString();
            String plate = contract.getLicensePlate();

            responseList = asynchronousEsbcalls(idCustomer, idDriver, plate, isPlate, contract.getContractId().toString(), date, responseList, false);
            while (!responseList.isEmpty()) {
                Object obj = responseList.poll();
                if (obj instanceof DriverEsb) {
                    DriverEsb driverEsb = (DriverEsb) obj;
                    if (driverEsb.getMainAddress() != null)
                        driverEsb.getMainAddress().setFormattedAddress();
                    contractInfoResponseV1.setDriverResponse(DriverAdapter.adptDriverEsbToDriverResponse(driverEsb));
                } else if (obj instanceof CustomerEsb) {
                    CustomerEsb customerEsb = (CustomerEsb) obj;
                    if (customerEsb.getMainAddress() != null)
                        customerEsb.getMainAddress().setFormattedAddress();
                    contractInfoResponseV1.setCustomerResponse(CustomerAdapter.adptMiddlewareCustomerResponseToCustomerResposne(customerEsb));
                } else if (obj instanceof VehicleEsb) {
                    VehicleEsb vehicleEsb = (VehicleEsb) obj;
                    contractInfoResponseV1.setVehicleResponse(VehicleAdapter.adptMiddlewareVehicleResponseToVehicleResponse(vehicleEsb));
                } else if (obj instanceof ContractEsb) {
                    ContractEsb contractEsb = (ContractEsb) obj;
                    contractInfoResponseV1.setContractResponse(ContractAdapter.adptMiddlewareContractDetailResponseToContractResponse(contractEsb));
                } else if (obj instanceof ArrayBlockingQueue) {
                    ArrayBlockingQueue<RegistryESB> registryESBS = (ArrayBlockingQueue<RegistryESB>) obj;
                    contractInfoResponseV1.setRegistryResponseList(RegistryAdapter.adptFromRegistryEsbToRegistryResponseV1List(registryESBS));
                } else if (obj instanceof BlockingQueue) {
                    BlockingQueue<FleetManagerEsb> fleetManagerEsbs = (BlockingQueue<FleetManagerEsb>) obj;
                    contractInfoResponseV1.setFleetManagerResponseList(FleetManagerAdpter.adtFromBlockingQueueFMESBToListFMResponse(fleetManagerEsbs));
                } else if (obj instanceof InsuranceCompanyEsb) {
                    InsuranceCompanyEsb insuranceCompanyEsb = (InsuranceCompanyEsb) obj;
                    contractInfoResponseV1.setInsuranceCompany(InsuranceCompanyAdapter.adptInsuranceCompanyEsbToInsuranceCompanyResponse(insuranceCompanyEsb));
                }
            }

        } catch (IOException e) {
            throw e;
        }

        return contractInfoResponseV1;
    }

    @Override
    public FleetManagerEsb checkFleetManager(String customerId, String fleetManagerId) throws IOException {

        String url = getFleetmanagersByIdUrl(customerId, fleetManagerId);
        return callWithoutCert(url, HttpMethod.GET, null, null, FleetManagerEsb.class);
    }

    @Override
    public List<RegistryESB> getTelematics(String idContract) throws IOException {

        List<RegistryESB> registryESBList = new ArrayList<>();
        Response responseToken = null;

        try {
            String telematicsUrl = getTelematicsByIdUrl(idContract, null);
            responseToken = httpUtil.callURL(null, telematicsUrl, null, HttpMethod.GET,
                    middlewareConnTimeout, middlewareReadTimeout, middlewareWriteTimeout);

            String response = responseToken.body().string();
            LOGGER.debug("Response Esb getTelematics Body: {}", response);
            ObjectMapper mapper = new ObjectMapper();
            Map<String, List<Map<String, Object>>> FMMap = mapper.readValue(response, Map.class);
            List<Map<String, Object>> listTelematics = FMMap.get("services");
            for (Map<String, Object> currentTelematic : listTelematics) {
                String jsonTelematics = mapper.writeValueAsString(currentTelematic);
                RegistryESB registryESB = mapper.readValue(jsonTelematics, RegistryESB.class);
                registryESBList.add(registryESB);
            }
            return registryESBList;
        } catch (AbstractException e) {
            LOGGER.error(e.getMessage(), e);
            throw e;
        }  catch (Exception e) {
            String message = String.format(MessageCode.CLAIMS_2012.value(), e.getClass().getSimpleName(), e.getMessage());
            LOGGER.error(message, e);
            throw new InternalException(message, MessageCode.CLAIMS_2012);
        } finally {
            if (responseToken != null && responseToken.body() != null) {
                responseToken.body().close();
            }
        }
    }

    @Override
    public List<FleetManagerEsb> getFleetManagers(String idCustomer) throws IOException {

        List<Integer> fleetManagerIdList = new ArrayList<>();
        List<FleetManagerEsb> fleetManagerList = new ArrayList<>();
        Response responseToken = null;
        Response responseTokenFleet = null;

        try {
            String fleetmanagerListUrl = getFleetmanagersListByIdUrl(idCustomer);
            responseToken = httpUtil.callURL(null, fleetmanagerListUrl, null, HttpMethod.GET,
                    middlewareConnTimeout, middlewareReadTimeout, middlewareWriteTimeout);

            String response = responseToken.body().string();
            LOGGER.debug("Response Enjoy getFleetManagersList Body: {}", response);
            response = response.replaceAll("city", "locality");
            ObjectMapper mapper = new ObjectMapper();
            Map<String, List<Map<String, Integer>>> FMMap = mapper.readValue(response, Map.class);
            List<Map<String, Integer>> listFleet = FMMap.get("contacts");
            for (Map<String, Integer> currentFM : listFleet) {
                fleetManagerIdList.add(currentFM.get("id"));
            }

            for (Integer currentIds : fleetManagerIdList) {
                String fleetmanagerUrl = getFleetmanagersByIdUrl(idCustomer, currentIds.toString());
                responseTokenFleet = httpUtil.callURL(null, fleetmanagerUrl, null, HttpMethod.GET,
                        middlewareConnTimeout, middlewareReadTimeout, middlewareWriteTimeout);
                String responseFleet = responseTokenFleet.body().string();
                LOGGER.debug("Response Enjoy getFleetManagersById Body: {}", responseFleet);
                responseFleet = responseFleet.replaceAll("city", "locality");
                FleetManagerEsb fleetManagerEsb = mapper.readValue(responseFleet, FleetManagerEsb.class);
                fleetManagerList.add(fleetManagerEsb);
            }


            return fleetManagerList;
        } catch (AbstractException e) {
            LOGGER.error(e.getMessage(), e);
            throw e;
        }  catch (Exception e) {
            String message = String.format(MessageCode.CLAIMS_2012.value(), e.getClass().getSimpleName(), e.getMessage());
            LOGGER.error(message, e);
            throw new InternalException(message, MessageCode.CLAIMS_2012);
        } finally {
            if (responseToken != null && responseToken.body() != null) {
                responseToken.body().close();
            }
            if (responseTokenFleet != null && responseTokenFleet.body() != null) {
                responseTokenFleet.body().close();
            }
        }
    }



    @Override
    public ContractInfoPracticeResponseV1 getContractByContractIdOrPlatePractice(String contractOrPlate, String date) throws IOException {

        String queryDate = date;
        Date date1 = new Date();
        if (queryDate != null && !queryDate.isEmpty()) {
            date1 = DateUtil.convertIS08601StringToUTCDate(queryDate);
        }
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMMdd"); //note: time zone not in format!
        sdf2.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));
        queryDate = sdf2.format(date1);
        LOGGER.debug("Data passata con fuso orario di Roma ESB: "+queryDate);

        ContractInfoPracticeResponseV1 result = null;
        result = this.getDriverContractCustomerCustomerInfoPractice(contractOrPlate, queryDate);
        return result;

    }

    @Override
    public ContractInfoPracticeResponseV1 getDriverContractCustomerCustomerInfoPractice(String contractOrPlate, String date) throws IOException {


        BlockingQueue<Object> responseList = new ArrayBlockingQueue<>(7);

        ContractInfoPracticeResponseV1 contractInfoResponseV1 = new ContractInfoPracticeResponseV1();


        String formattedDate = date.substring(0, 4) + "-" + date.substring(4, 6) + "-" + date.substring(6, 8);

        String regex = "\\d+";
        boolean isPlate = false;
        VehicleEsb vehicle = null;
        try {
            //it's a plate
            if (!contractOrPlate.matches(regex)) {
                isPlate = true;
                try {
                    vehicle = getVehicle(contractOrPlate);

                } catch (NotFoundException ex) {
                    LOGGER.debug(MessageCode.CLAIMS_1017.value());
                    throw new NotFoundException(MessageCode.CLAIMS_1017, ex);
                }
                Long idContract = vehicle.getIdActiveContract(formattedDate);
                if (idContract == null) {
                    //non esiste alcun contratto attivo associato alla targa
                    LOGGER.debug("Plate " + contractOrPlate + " is not related to an active contract");
                    throw new ForbiddenException("Plate " + contractOrPlate + " is not related to an active contract", MessageCode.CLAIMS_1025);
                }
                contractOrPlate = vehicle.getIdActiveContract(formattedDate).toString();
                contractInfoResponseV1.setVehicleResponse(VehicleAdapter.adptMiddlewareVehicleResponseToVehicleResponse(vehicle));
            }
            ContractEsb contract = getContractDetail(contractOrPlate, date);
            if (contract == null) {
                LOGGER.debug(MessageCode.CLAIMS_1015.value());
                throw new NotFoundException(MessageCode.CLAIMS_1015);
            }
            contractInfoResponseV1.setContractResponse(ContractAdapter.adptMiddlewareContractDetailResponseToContractResponse(contract));
            if (contract.getCustomerId() == null) {
                LOGGER.debug(MessageCode.CLAIMS_1012.value());
                throw new NotFoundException(MessageCode.CLAIMS_1012);
            }
            if (contract.getDriverId() == null) {
                LOGGER.debug(MessageCode.CLAIMS_1013.value());
                throw new NotFoundException(MessageCode.CLAIMS_1013);
            }
            if (contract.getLicensePlate() == null) {
                LOGGER.debug(MessageCode.CLAIMS_1014.value());
                throw new NotFoundException(MessageCode.CLAIMS_1014);
            }
            String idCustomer = contract.getCustomerId().toString();
            String idDriver = contract.getDriverId().toString();
            String plate = contract.getLicensePlate();

            responseList = asynchronousEsbcallsPractice(idCustomer, null, plate, isPlate, contract.getContractId().toString(), date, responseList);
            while (!responseList.isEmpty()) {
                Object obj = responseList.poll();
                if (obj instanceof CustomerEsb) {
                    CustomerEsb customerEsb = (CustomerEsb) obj;
                    if (customerEsb.getMainAddress() != null)
                        customerEsb.getMainAddress().setFormattedAddress();
                    contractInfoResponseV1.setCustomerResponse(CustomerAdapter.adptMiddlewareCustomerResponseToCustomerResposne(customerEsb));
                } else if (obj instanceof VehicleEsb) {
                    VehicleEsb vehicleEsb = (VehicleEsb) obj;
                    contractInfoResponseV1.setVehicleResponse(VehicleAdapter.adptMiddlewareVehicleResponseToVehicleResponse(vehicleEsb));
                } else if (obj instanceof ContractEsb) {
                    ContractEsb contractEsb = (ContractEsb) obj;
                    contractInfoResponseV1.setContractResponse(ContractAdapter.adptMiddlewareContractDetailResponseToContractResponse(contractEsb));
                } else if (obj instanceof BlockingQueue) {
                    BlockingQueue<FleetManagerEsb> fleetManagerEsbs = (BlockingQueue<FleetManagerEsb>) obj;
                    contractInfoResponseV1.setFleetManagerResponseList(FleetManagerAdpter.adtFromBlockingQueueFMESBToListFMResponse(fleetManagerEsbs));
                }
            }
        } catch (IOException e) {
            throw e;
        }

        return contractInfoResponseV1;
    }

    @Override
    public BlockingQueue asynchronousEsbcallsPractice(String idCustomer, String idDriver, String plate, boolean isPlate, String idContract, String date, BlockingQueue responseList) {

        BlockingQueue<Integer> fleetManagersId = new LinkedBlockingQueue<>();

        BlockingQueue<FleetManagerEsb> fleetManagersInfoList = new LinkedBlockingQueue<>();

        BlockingQueue<RegistryESB> registryESBBlockingQueue = new ArrayBlockingQueue<>(10000);

        String customerUrl = getCustomerByIdUrl(idCustomer);
        ESBCallThread callCustomerESB = new ESBCallThread(customerUrl,
                "customer", HttpMethod.GET, null, fleetManagersId, fleetManagersInfoList, registryESBBlockingQueue, responseList);

        String fleetmanagerListUrl = getFleetmanagersListByIdUrl(idCustomer);
        ESBCallThread callFleetManagersIdESB = new ESBCallThread(fleetmanagerListUrl,
                "fleetmanagersId", HttpMethod.GET, null, fleetManagersId, fleetManagersInfoList, registryESBBlockingQueue, responseList);

        ESBCallThread callVehicleESB = null;
        if (!isPlate) {
            String vehicleUrl = getVehicleByPlateUrl(plate);
            callVehicleESB = new ESBCallThread(vehicleUrl,
                    "vehicle", HttpMethod.GET, null, fleetManagersId, fleetManagersInfoList, registryESBBlockingQueue, responseList);
        }
        try {
            callCustomerESB.startThread();
            callFleetManagersIdESB.startThread();
            if (callVehicleESB != null) {
                callVehicleESB.startThread();
                callVehicleESB.join();
            }

            callCustomerESB.join();
            callFleetManagersIdESB.join();

            List<Thread> fleetThreads = new LinkedList<>();
            for (Integer currentId : fleetManagersId) {
                String fleetmanagerUrl = getFleetmanagersByIdUrl(idCustomer, currentId.toString());
                ESBCallThread fleetThread = new ESBCallThread(fleetmanagerUrl,
                        "fleetmanagers", HttpMethod.GET, null, fleetManagersId, fleetManagersInfoList, registryESBBlockingQueue, responseList);
                fleetThreads.add(fleetThread);
                fleetThread.startThread();

            }
            for (Thread currentFM : fleetThreads) {
                currentFM.join();
            }
            responseList.add(fleetManagersInfoList);
        } catch (InterruptedException e) {
            LOGGER.debug(e.getMessage());
        }

        return responseList;
    }

    //chiamata all'esb per ALD VS ALD
    @Override
    public ContractInfoResponseAldVSAldV1 getContractByContractIdOrPlateAldVSAld(String contractOrPlate, String date) throws IOException {

        String queryDate = date;
        Date date1 = new Date();
        if (queryDate != null && !queryDate.isEmpty()) {
            date1 = DateUtil.convertIS08601StringToUTCDate(queryDate);
        }
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMMdd"); //note: time zone not in format!
        sdf2.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));
        queryDate = sdf2.format(date1);
        LOGGER.debug("Data passata con fuso orario di Roma ESB: "+queryDate);

        SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy-MM-dd"); //note: time zone not in format!
        sdf2.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));
        String checkEndDate = sdf3.format(date1) + "T00:00:00Z";

        ContractInfoResponseV1 appContractInfo = null;
        ContractInfoResponseAldVSAldV1 result = new ContractInfoResponseAldVSAldV1();

        appContractInfo = this.getDriverContractCustomerCustomerInfoAldVSAld(contractOrPlate, queryDate, checkEndDate);
        String formattedDate = queryDate.substring(0, 4) + "-" + queryDate.substring(4, 6) + "-" + queryDate.substring(6, 8);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
        formattedDate += "T00:00:00Z";
        //Date date1 = format.parse(formattedDate);
        Instant dateInstant = DateUtil.convertIS08601StringToUTCInstant(formattedDate);
        if (appContractInfo != null && appContractInfo.getInsuranceCompany() != null && appContractInfo.getCustomerResponse() != null) {

            InsuranceCompanyResponse insuranceCompany = appContractInfo.getInsuranceCompany();
            CustomerResponse customerResponse = appContractInfo.getCustomerResponse();
            String tariffId = null;
            String customerId = null;
            List<InsurancePolicyEntity> insurancePolicyEntityList = null;
            InsurancePolicyEntity insurancePolicyEntity = null;

            if (insuranceCompany.getTpl() != null) {
                tariffId = insuranceCompany.getTpl().getTariffId();
            }

            if (customerResponse.getCustomerId() != null)
                customerId = customerResponse.getCustomerId();

            if (tariffId != null) {
                //caso assicurato ALD
                insurancePolicyEntityList = insurancePolicyRepository.searchInsurancePolicyForGetContract(tariffId, dateInstant, PolicyTypeEnum.RCA);
                if (insurancePolicyEntityList != null && !insurancePolicyEntityList.isEmpty())
                    insurancePolicyEntity = insurancePolicyEntityList.get(0);
            } else if (customerId != null) {

                //caso autoassicurato
                insurancePolicyEntityList = insurancePolicyRepository.searchInsurancePolicyForGetContractByCustomer(Long.parseLong(customerId), dateInstant, PolicyTypeEnum.RCA);

                if (insurancePolicyEntityList != null && !insurancePolicyEntityList.isEmpty())
                    insurancePolicyEntity = insurancePolicyEntityList.get(0);

            }

            result.setCustomerResponse(appContractInfo.getCustomerResponse());
            //result.setContractResponse(appContractInfo.getContractResponse());
            result.setDriverResponse(appContractInfo.getDriverResponse());
            result.setVehicleResponse(appContractInfo.getVehicleResponse());

            if (insurancePolicyEntity != null) {
                result.setPolicyNumber(insurancePolicyEntity.getNumberPolicy());
                if (insurancePolicyEntity.getEndValidity() != null) {
                    String endValidity = DateUtil.convertUTCInstantToIS08601String(insurancePolicyEntity.getEndValidity());
                    endValidity = endValidity.substring(0, 10);
                    result.setPolicyEndValidity(endValidity);
                }

                if (insurancePolicyEntity.getBeginningValidity() != null) {
                    String beginValidity = DateUtil.convertUTCInstantToIS08601String(insurancePolicyEntity.getBeginningValidity());
                    beginValidity = beginValidity.substring(0, 10);
                    result.setPolicyBeginningValidity(beginValidity);
                }
                result.setInsuranceCompany(insuranceCompanyAdapter.adptFromICompEntityToICompResp(insurancePolicyEntity.getInsuranceCompanyEntity()));
            }

        }

        return result;
    }

    private ContractInfoResponseV1 getDriverContractCustomerCustomerInfoAldVSAld(String contractOrPlate, String date, String endDateCheck) throws IOException {

        BlockingQueue<Object> responseList = new ArrayBlockingQueue<>(7);
        ContractInfoResponseV1 contractInfoResponseV1 = new ContractInfoResponseV1();
        String formattedDate = date.substring(0, 4) + "-" + date.substring(4, 6) + "-" + date.substring(6, 8);

        String regex = "\\d+";
        boolean isPlate = false;
        VehicleEsb vehicle = null;
        try {
            //it's a plate

            if (!contractOrPlate.matches(regex)) {
                isPlate = true;
                try {
              /*
                    CO-643 ricerca storico delle targhe
                    * */
                    //   vehicle = getVehicle(contractOrPlate);
                    vehicle = getPlateFromVehicleRegistrations(contractOrPlate, date);
                } catch (NotFoundException ex) {
                    LOGGER.debug(MessageCode.CLAIMS_1017.value());
                    throw new NotFoundException(MessageCode.CLAIMS_1017, ex);
                }

                //aggiungere controllo


                contractOrPlate = vehicle.getRelatedContractsList().get(0).getId().toString();
                contractInfoResponseV1.setVehicleResponse(VehicleAdapter.adptMiddlewareVehicleResponseToVehicleResponse(vehicle));
            }
            //aggiungere controllo
            ContractEsb contract = getContractDetail(contractOrPlate, date);

            if (contract == null) {
                LOGGER.debug(MessageCode.CLAIMS_1015.value());
                throw new NotFoundException(MessageCode.CLAIMS_1015);
            } else if (contract.getTakeinDate() != null) {
                Date dateSX = DateUtil.convertIS08601StringToUTCDate(endDateCheck);
                if (dateSX.compareTo(contract.getTakeinDate()) > 0) {
                    LOGGER.info(MessageCode.CLAIMS_2009.value());
                    throw new BadRequestException(MessageCode.CLAIMS_2009);
                }
            }
            contractInfoResponseV1.setContractResponse(ContractAdapter.adptMiddlewareContractDetailResponseToContractResponse(contract));
            if (contract.getCustomerId() == null) {
                LOGGER.debug(MessageCode.CLAIMS_1012.value());
                throw new NotFoundException(MessageCode.CLAIMS_1012);
            }


            String idCustomer = contract.getCustomerId().toString();
            String idDriver = null;
            if (contract.getDriverId() != null)
                idDriver = contract.getDriverId().toString();
            String plate = contract.getLicensePlate();

            responseList = asynchronousEsbcalls(idCustomer, idDriver, plate, isPlate, contract.getContractId().toString(), date, responseList, true);
            while (!responseList.isEmpty()) {
                Object obj = responseList.poll();
                if (obj instanceof DriverEsb) {
                    DriverEsb driverEsb = (DriverEsb) obj;
                    if (driverEsb.getMainAddress() != null)
                        driverEsb.getMainAddress().setFormattedAddress();
                    contractInfoResponseV1.setDriverResponse(DriverAdapter.adptDriverEsbToDriverResponse(driverEsb));
                } else if (obj instanceof CustomerEsb) {
                    CustomerEsb customerEsb = (CustomerEsb) obj;
                    if (customerEsb.getMainAddress() != null)
                        customerEsb.getMainAddress().setFormattedAddress();
                    contractInfoResponseV1.setCustomerResponse(CustomerAdapter.adptMiddlewareCustomerResponseToCustomerResposne(customerEsb));
                } else if (obj instanceof VehicleEsb) {
                    VehicleEsb vehicleEsb = (VehicleEsb) obj;
                    contractInfoResponseV1.setVehicleResponse(VehicleAdapter.adptMiddlewareVehicleResponseToVehicleResponse(vehicleEsb));
                } else if (obj instanceof ContractEsb) {
                    ContractEsb contractEsb = (ContractEsb) obj;
                    contractInfoResponseV1.setContractResponse(ContractAdapter.adptMiddlewareContractDetailResponseToContractResponse(contractEsb));
                } else if (obj instanceof InsuranceCompanyEsb) {
                    InsuranceCompanyEsb insuranceCompanyEsb = (InsuranceCompanyEsb) obj;
                    contractInfoResponseV1.setInsuranceCompany(InsuranceCompanyAdapter.adptInsuranceCompanyEsbToInsuranceCompanyResponse(insuranceCompanyEsb));
                }
            }

        } catch (IOException e) {
            throw e;
        }

        return contractInfoResponseV1;
    }


    @Override
    public ContractInfoResponseV1 getContractInfoEnjoy(String plate, String date) throws ParseException {
        String queryDate = date;
        Date date1 = new Date();
        if (queryDate != null && !queryDate.isEmpty()) {
            date1 = DateUtil.convertIS08601StringToUTCDate(queryDate);
        }
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMMdd"); //note: time zone not in format!
        sdf2.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));
        queryDate = sdf2.format(date1);
        String formattedDate = queryDate.substring(0, 4) + "-" + queryDate.substring(4, 6) + "-" + queryDate.substring(6, 8);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
        formattedDate += "T00:00:00Z";
        //Date date1 = format.parse(formattedDate);
        Instant dateInstant = DateUtil.convertIS08601StringToUTCInstant(formattedDate);
        MiddlewareClaimInfoResponse middlewareClaimInfoResponse = null;
        try {
          /*
            CO-643 ricerca storico delle targhe
            * */
            middlewareClaimInfoResponse = getPlateFromEnjoyVehicleRegistrations(plate, queryDate, dateInstant);
            //  middlewareClaimInfoResponse = middlewareClient.getClaimInfoByPlateAndDate(plate, queryDate);
        } catch (NotFoundException e) {
            LOGGER.error(e.getMessage() + " " + e.getCode());
            throw new NotFoundException(MessageCode.CLAIMS_1010);
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
            throw new NotFoundException(MessageCode.CLAIMS_1010);
        }

        SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy-MM-dd"); //note: time zone not in format!
        sdf2.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));
        String checkEndDate = sdf3.format(date1) + "T00:00:00Z";

        ContractInfoResponseV1 result = EnjoyAdapter.adptFromMiddlewareResponseToContractInfoResponse(middlewareClaimInfoResponse);
        if (result != null && result.getContractResponse() != null && result.getContractResponse().getTakeinDate() != null) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date dateSX = DateUtil.convertIS08601StringToUTCDate(checkEndDate);
            String dateSXString = simpleDateFormat.format(dateSX);
            String takeinDateString = simpleDateFormat.format(result.getContractResponse().getTakeinDate());
            if (dateSXString.compareTo(takeinDateString) > 0) {
                LOGGER.info(MessageCode.CLAIMS_2009.value());
                throw new BadRequestException(MessageCode.CLAIMS_2009);
            }
        }

        //effettuare controlli iniziali

        if (result != null && (result.getContractResponse() == null || result.getContractResponse().getContractId() == null)) {
            LOGGER.debug(MessageCode.CLAIMS_1015.value());
            throw new NotFoundException(MessageCode.CLAIMS_1015);
        }

        //controllo della data da fare

        if (result != null && (result.getCustomerResponse() == null || result.getCustomerResponse().getCustomerId() == null)) {
            LOGGER.debug(MessageCode.CLAIMS_1012.value());
            throw new NotFoundException(MessageCode.CLAIMS_1012);
        }

        if (result != null && result.getContractResponse() != null && result.getContractResponse().getLicensePlate() == null) {
            LOGGER.debug(MessageCode.CLAIMS_1014.value());
            throw new NotFoundException(MessageCode.CLAIMS_1014);
        }

        //


        if (result != null && result.getInsuranceCompany() != null && result.getCustomerResponse() != null) {

            InsuranceCompanyResponse insuranceCompany = result.getInsuranceCompany();
            CustomerResponse customerResponse = result.getCustomerResponse();
            String tariffId = null;
            String customerId = null;
            List<InsurancePolicyEntity> insurancePolicyEntityList = null;
            InsurancePolicyEntity insurancePolicyEntity = null;

            if (insuranceCompany.getTpl() != null) {
                tariffId = insuranceCompany.getTpl().getTariffId();
            }

            if (customerResponse.getCustomerId() != null)
                customerId = customerResponse.getCustomerId();

            if (tariffId != null) {
                //caso assicurato ALD
                insurancePolicyEntityList = insurancePolicyRepository.searchInsurancePolicyForGetContract(tariffId, dateInstant, PolicyTypeEnum.RCA);
                if (insurancePolicyEntityList == null || insurancePolicyEntityList.isEmpty()) {
                    LOGGER.debug(MessageCode.CLAIMS_1048.value());
                    throw new NotFoundException(MessageCode.CLAIMS_1048);
                }
                insurancePolicyEntity = insurancePolicyEntityList.get(0);
                if (insurancePolicyEntity == null) {
                    LOGGER.debug(MessageCode.CLAIMS_1048.value());
                    throw new NotFoundException(MessageCode.CLAIMS_1048);
                }
            } else if (customerId != null) {

                //caso autoassicurato
                insurancePolicyEntityList = insurancePolicyRepository.searchInsurancePolicyForGetContractByCustomer(Long.parseLong(customerId), dateInstant, PolicyTypeEnum.RCA);

                if (insurancePolicyEntityList == null || insurancePolicyEntityList.isEmpty()) {
                    LOGGER.debug(MessageCode.CLAIMS_1048.value());
                    throw new NotFoundException(MessageCode.CLAIMS_1048);
                }
                insurancePolicyEntity = insurancePolicyEntityList.get(0);

                if (insurancePolicyEntity == null) {
                    LOGGER.debug(MessageCode.CLAIMS_1048.value());
                    throw new NotFoundException(MessageCode.CLAIMS_1048);
                }
                insuranceCompany.setTpl(new TplResponse());

            }
            insuranceCompany.getTpl().setPolicyNumber(insurancePolicyEntity.getNumberPolicy());

            if (insurancePolicyEntity.getBeginningValidity() != null) {
                String beginningValidity = DateUtil.convertUTCInstantToIS08601String(insurancePolicyEntity.getBeginningValidity());
                beginningValidity = beginningValidity.substring(0, 10);
                insuranceCompany.getTpl().setStartDate(beginningValidity);
            }

            if (insurancePolicyEntity.getEndValidity() != null) {
                String endValidity = DateUtil.convertUTCInstantToIS08601String(insurancePolicyEntity.getEndValidity());
                endValidity = endValidity.substring(0, 10);
                insuranceCompany.getTpl().setEndDate(endValidity);
            }

            insuranceCompany.getTpl().setCompany(insurancePolicyEntity.getInsuranceCompanyEntity().getName());
            insuranceCompany.getTpl().setInsuranceCompanyId(insurancePolicyEntity.getInsuranceCompanyEntity().getId());
            insuranceCompany.getTpl().setInsurancePolicyId(insurancePolicyEntity.getId());

            insurancePolicyEntity = null;
            String tariffIdPai = null;

            if (insuranceCompany.getPai() != null) {
                tariffIdPai = insuranceCompany.getPai().getTariffId();
            }

            if (customerResponse.getCustomerId() != null)
                customerId = customerResponse.getCustomerId();

            if (tariffIdPai != null) {
                //polizza assicurativa PAI per cliente assicurato ALD
                insurancePolicyEntityList = insurancePolicyRepository.searchInsurancePolicyForGetContract(tariffIdPai, dateInstant, PolicyTypeEnum.PAI);
                if (insurancePolicyEntityList != null && !insurancePolicyEntityList.isEmpty())
                    insurancePolicyEntity = insurancePolicyEntityList.get(0);

            } else if (customerId != null) {
                //polizza assicurativa PAI per cliente AUTOassicurato ALD
                insurancePolicyEntityList = insurancePolicyRepository.searchInsurancePolicyForGetContractByCustomer(Long.parseLong(customerId), dateInstant, PolicyTypeEnum.PAI);
                if (insurancePolicyEntityList != null && !insurancePolicyEntityList.isEmpty())
                    insurancePolicyEntity = insurancePolicyEntityList.get(0);

                if (insurancePolicyEntity != null) {

                    insuranceCompany.setPai(new PaiResponse());
                }
            }

            if (insurancePolicyEntity != null) {
                insuranceCompany.getPai().setInsuranceCompanyId(insurancePolicyEntity.getInsuranceCompanyEntity().getId());
                insuranceCompany.getPai().setInsurancePolicyId(insurancePolicyEntity.getId());
                insuranceCompany.getPai().setPolicyNumber(insurancePolicyEntity.getNumberPolicy());

                if (insurancePolicyEntity.getBeginningValidity() != null) {
                    String beginningValidity = DateUtil.convertUTCInstantToIS08601String(insurancePolicyEntity.getBeginningValidity());
                    beginningValidity = beginningValidity.substring(0, 10);
                    insuranceCompany.getPai().setStartDate(beginningValidity);
                }

                if (insurancePolicyEntity.getEndValidity() != null) {
                    String endValidity = DateUtil.convertUTCInstantToIS08601String(insurancePolicyEntity.getEndValidity());
                    endValidity = endValidity.substring(0, 10);
                    insuranceCompany.getPai().setEndDate(endValidity);
                }
            }

            insurancePolicyEntity = null;
            String tariffIdLegalCost = null;
            if (insuranceCompany.getLegalCost() != null) {
                tariffIdLegalCost = insuranceCompany.getLegalCost().getTariffId();
            }
            if (customerResponse.getCustomerId() != null)
                customerId = customerResponse.getCustomerId();
            if (tariffIdLegalCost != null) {

                insurancePolicyEntityList = insurancePolicyRepository.searchInsurancePolicyForGetContract(tariffIdLegalCost, dateInstant, PolicyTypeEnum.LEGAL);
                if (insurancePolicyEntityList != null && !insurancePolicyEntityList.isEmpty())
                    insurancePolicyEntity = insurancePolicyEntityList.get(0);

            } else if (customerId != null) {

                insurancePolicyEntityList = insurancePolicyRepository.searchInsurancePolicyForGetContractByCustomer(Long.parseLong(customerId), dateInstant, PolicyTypeEnum.LEGAL);
                if (insurancePolicyEntityList != null && !insurancePolicyEntityList.isEmpty())
                    insurancePolicyEntity = insurancePolicyEntityList.get(0);

                if (insurancePolicyEntity != null) {

                    insuranceCompany.setLegalCost(new LegalCostResponse());
                }
            }

            if (insurancePolicyEntity != null) {
                insuranceCompany.getLegalCost().setInsuranceCompanyId(insurancePolicyEntity.getInsuranceCompanyEntity().getId());
                insuranceCompany.getLegalCost().setInsurancePolicyId(insurancePolicyEntity.getId());
                insuranceCompany.getLegalCost().setPolicyNumber(insurancePolicyEntity.getNumberPolicy());

                if (insurancePolicyEntity.getBeginningValidity() != null) {
                    String beginningValidity = DateUtil.convertUTCInstantToIS08601String(insurancePolicyEntity.getBeginningValidity());
                    beginningValidity = beginningValidity.substring(0, 10);
                    insuranceCompany.getLegalCost().setStartDate(beginningValidity);
                }

                if (insurancePolicyEntity.getEndValidity() != null) {
                    String endValidity = DateUtil.convertUTCInstantToIS08601String(insurancePolicyEntity.getEndValidity());
                    endValidity = endValidity.substring(0, 10);
                    insuranceCompany.getLegalCost().setEndDate(endValidity);
                }
            }


            result.setInsuranceCompany(insuranceCompany);
        }


        if (result != null) {
            //controllo che per quel contratto ci sia un contratto valido in nemo
            ContractResponse contractResponse = result.getContractResponse();
            if (contractResponse.getContractType() != null) {
                ContractTypeEntity contractTypeEntity = contractTypeRepository.searchContractByCode(contractResponse.getContractType());
                if (contractTypeEntity == null || contractTypeEntity.getFlagWS() == null || !contractTypeEntity.getFlagWS()) {
                    LOGGER.debug(MessageCode.CLAIMS_1042.value());
                    throw new BadRequestException(MessageCode.CLAIMS_1042);
                }
            }
        }

        if (result != null && result.getContractResponse() != null && result.getContractResponse().getLeaseServiceComponentsList() != null) {
            //recupero i serivizi aggiuntivi
            Iterator<LeaseServiceComponentsResponse> iLeaseServiceComponentResponse = result.getContractResponse().getLeaseServiceComponentsList().iterator();


            while (iLeaseServiceComponentResponse.hasNext()) {

                LeaseServiceComponentsResponse leaseServiceComponentsResponse = iLeaseServiceComponentResponse.next();
                if (leaseServiceComponentsResponse.getName() != null && (leaseServiceComponentsResponse.getName().equalsIgnoreCase("Full MD self-insured") || leaseServiceComponentsResponse.getName().equalsIgnoreCase("MD Customer managed"))) {


                    Kasko kasko = null;

                    if (leaseServiceComponentsResponse.getQualifiersList() != null) {
                        Iterator<QualifiersResponse> iQualifierResponse = leaseServiceComponentsResponse.getQualifiersList().iterator();

                        while (iQualifierResponse.hasNext() && kasko == null) {
                            QualifiersResponse qualifiersResponse = iQualifierResponse.next();
                            System.out.println(qualifiersResponse);
                            if (qualifiersResponse.getAttributeDescription().equalsIgnoreCase("Deductible")) {
                                kasko = new Kasko();
                                kasko.setDeductibleId(Integer.parseInt(qualifiersResponse.getValue()));
                                kasko.setDeductibleValue(qualifiersResponse.getValueDescription());
                            }
                        }
                        if (kasko != null)
                            result.getInsuranceCompany().setKasko(KaskoAdapter.adptFromKaskoToKaskoResponse(kasko));

                    }
                } else if (leaseServiceComponentsResponse.getName() != null && leaseServiceComponentsResponse.getName().equalsIgnoreCase("Theft Insurance")) {
                    if (leaseServiceComponentsResponse.getQualifiersList() != null) {
                        Iterator<QualifiersResponse> iQualifierResponse = leaseServiceComponentsResponse.getQualifiersList().iterator();
                        boolean isFound = false;
                        while (iQualifierResponse.hasNext() && !isFound) {
                            QualifiersResponse qualifiersResponse = iQualifierResponse.next();
                            if (qualifiersResponse.getAttributeDescription().equalsIgnoreCase("Deductible")) {
                                isFound = true;
                                TheftResponse theft = result.getInsuranceCompany().getTheft();
                                if (theft == null) {
                                    theft = new TheftResponse();
                                }
                                if (qualifiersResponse.getValue() != null) {
                                    theft.setDeductibleId(Long.parseLong(qualifiersResponse.getValue()));
                                }
                                if (qualifiersResponse.getValueDescription() != null) {
                                    theft.setDeductible(qualifiersResponse.getValueDescription());
                                }
                                result.getInsuranceCompany().setTheft(theft);
                            }
                        }
                    }
                } else if (leaseServiceComponentsResponse.getName() != null && leaseServiceComponentsResponse.getName().equalsIgnoreCase("TPL - Third Party Liability")) {
                    if (leaseServiceComponentsResponse.getQualifiersList() != null) {
                        Iterator<QualifiersResponse> iQualifierResponse = leaseServiceComponentsResponse.getQualifiersList().iterator();
                        boolean isFound = false;
                        while (iQualifierResponse.hasNext() && !isFound) {
                            QualifiersResponse qualifiersResponse = iQualifierResponse.next();
                            if (qualifiersResponse.getAttributeDescription().equalsIgnoreCase("Deductible")) {
                                isFound = true;
                                TplResponse tpl = result.getInsuranceCompany().getTpl();
                                tpl.setDeductible(qualifiersResponse.getValueDescription());
                                result.getInsuranceCompany().setTpl(tpl);
                            }
                        }
                    }
                }
            }
        }

        if (result != null && result.getInsuranceCompany() != null) {
            if (result.getInsuranceCompany().getPai() != null && result.getInsuranceCompany().getPai().getInsuranceCompanyId() == null) {
                result.getInsuranceCompany().setPai(null);
            }
            if (result.getInsuranceCompany().getLegalCost() != null && result.getInsuranceCompany().getLegalCost().getInsuranceCompanyId() == null) {
                result.getInsuranceCompany().setLegalCost(null);
            }
        }

        return result;

    }


    private String getCustomerByIdUrl(String customerId) {

        return buildUrl(
                middlewareEndpoint,
                middlewareCustomerByIdUri,
                new HashMap<String, String>() {{
                    put("id", customerId);
                }},
                null
        );
    }

    private String getVehicleByPlateUrl(String plate) {

        return buildUrl(
                middlewareEndpoint,
                middlewareVehicleByPlateUri,
                new HashMap<String, String>() {{
                    put("plate", plate);
                }},
                null
        );
    }

    private String getContractDetailByIdUrl(String idcontract, String date) {

        return buildUrl(
                middlewareEndpoint,
                middlewareContractDetailByIdUri,
                new HashMap<String, String>() {{
                    put("id", idcontract);
                }},
                new LinkedMultiValueMap<String, String>() {{
                    add("date", date);
                }}
        );
    }

    private String getDriverByIdUrl(String idDriver) {

        return buildUrl(
                middlewareEndpoint,
                middlewareDriverByIdUri,
                new HashMap<String, String>() {{
                    put("id", idDriver);
                }},
                null
        );
    }

    private String getInsuranceCompanyByIdUrl(String idcontract, String date) {

        return buildUrl(
                middlewareEndpoint,
                middlewareCustomersInsuranceByCustomerId,
                new HashMap<String, String>() {{
                    put("id", idcontract);
                }},
                new LinkedMultiValueMap<String, String>() {{
                    add("date", date);
                }}
        );
    }

    private String getFleetmanagersListByIdUrl(String idCustomer) {

        return buildUrl(
                middlewareEndpoint,
                middlewareCustomersFleetManagers,
                new HashMap<String, String>() {{
                    put("id", idCustomer);
                }},
                null
        );
    }

    private String getTelematicsByIdUrl(String idContract, String date) {

        return buildUrl(
                middlewareEndpoint,
                middlewareCustomersTelematicsByCustomerId,
                new HashMap<String, String>() {{
                    put("id", idContract);
                }},
                StringUtils.isBlank(date)
                        ? null
                        : new LinkedMultiValueMap<String, String>() {{
                    add("date", date);
                }}
        );
    }

    private static String buildUrl(String baseUrl, String uri, Map<String, String> pathParams, MultiValueMap<String, String> queryParams) {

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(baseUrl.concat(uri));

        if (MapUtils.isNotEmpty(queryParams)) {
            builder.queryParams(queryParams);
        }

        return builder.buildAndExpand(pathParams).toString();
    }

    private class ESBCallThread extends Thread {
        private String path;
        private String tipo;
        private HttpMethod method;
        private Map<String, String> header;

        private BlockingQueue<Integer> fleetManagersId;

        private BlockingQueue<FleetManagerEsb> fleetManagersInfoList;

        private BlockingQueue<RegistryESB> registryESBBlockingQueue;
        private BlockingQueue<Object> responseList;

        public ESBCallThread(String path, String tipo, HttpMethod method, Map<String, String> header, BlockingQueue<Integer> fleetManagersId, BlockingQueue<FleetManagerEsb> fleetManagersInfoList, BlockingQueue<RegistryESB> registryESBBlockingQueue, BlockingQueue<Object> responseList) {
            this.path = path;
            this.tipo = tipo;
            this.method = method;
            this.header = header;
            this.fleetManagersId = fleetManagersId;
            this.fleetManagersInfoList = fleetManagersInfoList;
            this.registryESBBlockingQueue = registryESBBlockingQueue;
            this.responseList = responseList;
        }

        @Override
        public void run() {
            Response responseToken = null;
            try {
                 responseToken = httpUtil.callURL(this.header, this.path, null, this.method,
                        middlewareConnTimeout, middlewareReadTimeout, middlewareWriteTimeout);


                String response = responseToken.body().string();
                response = response.replaceAll("city", "locality");
                ObjectMapper mapper = new ObjectMapper();

                Object obj = null;
                if (this.tipo.equalsIgnoreCase("vehicle")) {
                    obj = mapper.readValue(response, VehicleEsb.class);
                } else if (this.tipo.equalsIgnoreCase("driver")) {
                    obj = mapper.readValue(response, DriverEsb.class);
                } else if (this.tipo.equalsIgnoreCase("customer")) {
                    obj = mapper.readValue(response, CustomerEsb.class);
                } else if (this.tipo.equalsIgnoreCase("fleetmanagersId")) {
                    Map<String, List<Map<String, Integer>>> FMMap = mapper.readValue(response, Map.class);
                    List<Map<String, Integer>> listFleet = FMMap.get("contacts");
                    for (Map<String, Integer> currentFM : listFleet) {
                        fleetManagersId.add(currentFM.get("id"));
                    }
                } else if (this.tipo.equalsIgnoreCase("fleetmanagers")) {
                    FleetManagerEsb fleetManagerEsb = mapper.readValue(response, FleetManagerEsb.class);
                    fleetManagersInfoList.add(fleetManagerEsb);
                } else if (this.tipo.equalsIgnoreCase("insurancecompany")) {
                    obj = mapper.readValue(response, InsuranceCompanyEsb.class);
                } else if (this.tipo.equalsIgnoreCase("telematics")) {
                    Map<String, List<Map<String, Object>>> FMMap = mapper.readValue(response, Map.class);
                    List<Map<String, Object>> listTelematics = FMMap.get("services");

                    if (listTelematics != null) {

                        for (Map<String, Object> currentTelematic : listTelematics) {
                            String jsonTelematics = mapper.writeValueAsString(currentTelematic);
                            RegistryESB registryESB = mapper.readValue(jsonTelematics, RegistryESB.class);
                            registryESBBlockingQueue.add(registryESB);
                        }
                    }
                }

                if (obj != null) {
                    responseList.add(obj);
                }

            } catch (JsonParseException e) {
                LOGGER.debug(MessageCode.CLAIMS_1047.value());
                throw new InternalException(MessageCode.CLAIMS_1047, e);
            } catch (IOException e) {
                LOGGER.debug(e.getMessage());
            } finally {
                if (responseToken != null && responseToken.body() != null) {
                    responseToken.body().close();
                }
            }
        }


        public BlockingQueue<Object> startThread() {
            BlockingQueue<Object> result = new ArrayBlockingQueue<>(7);

            this.start();
            result.add(fleetManagersId);
            result.add(fleetManagersInfoList);
            result.add(registryESBBlockingQueue);
            result.add(responseList);
            return result;
        }

    }


    @Override
    public Boolean getContractByContractIdOrPlateGoLiveMSA(String contractOrPlate, String date) {

        try{
            String queryDate = date;
            Date date1 = new Date();
            if (queryDate != null && !queryDate.isEmpty()) {
                date1 = DateUtil.convertIS08601StringToUTCDate(queryDate);
            }
            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMMdd"); //note: time zone not in format!
            sdf2.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));
            queryDate = sdf2.format(date1);
            LOGGER.info("Data passata con fuso orario di Roma ESB: " + queryDate);


            SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy-MM-dd"); //note: time zone not in format!
            sdf2.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));
            String checkEndDate = sdf3.format(date1) + "T00:00:00Z";

            ContractInfoResponseV1 result = null;

            String idCustomer = this.getDriverContractCustomerCustomerInfoGoLiveMSA(contractOrPlate, queryDate, checkEndDate);
            System.out.println(idCustomer);
            goLiveStrategyService.goLiveStrategyChecksForMSA(idCustomer);
        } catch (Exception e){
            System.out.println(e);
            return false;
        }
        return true;
    }


    @Override
    public MiddlewareClaimInfoResponse getPlateFromEnjoyVehicleRegistrations(String plate, String queryDate, Instant instantRif) throws IOException,NotFoundException {
        MiddlewareClaimInfoResponse middlewareClaimInfoResponse =null ;

        VehicleRegistrationResponse  platesRegistrationHistory = null;
        try {
            platesRegistrationHistory = getVehicleRegistrationsByPlate(plate);
        } catch (IOException e) {
            LOGGER.info(MessageCode.CLAIMS_1017.value());
            throw new NotFoundException(MessageCode.CLAIMS_1017, e);
        }

        if (platesRegistrationHistory!=null) {

            List<PlateRegistrationHistoryItem> listPlates =  platesRegistrationHistory.getPlateHistoryItems().stream()
                    .filter(h -> h.getValidFrom().isBefore(instantRif) && h.getValidTo().isAfter(instantRif) && h.getPlate().equalsIgnoreCase(plate))
                    .collect(Collectors.toList());


            if (!listPlates.isEmpty()){
                try {
                    middlewareClaimInfoResponse = middlewareClient.getClaimInfoByPlateAndDate(platesRegistrationHistory.getCurrentPlate(), queryDate);
                } catch (NotFoundException e) {
                    LOGGER.info(MessageCode.CLAIMS_1017.value());
                    throw new NotFoundException(MessageCode.CLAIMS_1017, e);
                }
                middlewareClaimInfoResponse.getVehicle().setLicensePlate(plate);
            }else{
                throw new NotFoundException(MessageCode.CLAIMS_2014);
            }

        }


        return middlewareClaimInfoResponse;
    }


    @Override
    public VehicleEsb getPlateFromVehicleRegistrations(String plate, String queryDate) throws IOException {

        VehicleEsb vehicleEsb = null;

        String plateOldRegistration = StringUtils.EMPTY;

        Instant instantRif = Instant.parse(queryDate.substring(0, 4) + "-" + queryDate.substring(4, 6) + "-" + queryDate.substring(6, 8) + "T00:00:00Z");


        VehicleRegistrationResponse  platesRegistrationHistory = null;
        try {
            platesRegistrationHistory = getVehicleRegistrationsByPlate(plate);
        } catch (IOException e) {
            LOGGER.info(MessageCode.CLAIMS_1017.value());
            throw new NotFoundException(MessageCode.CLAIMS_1017, e);
        }

        if (platesRegistrationHistory!=null) {

            List<PlateRegistrationHistoryItem> listPlates =  platesRegistrationHistory.getPlateHistoryItems().stream()
                    .filter(h -> h.getValidFrom().isBefore(instantRif) && h.getValidTo().isAfter(instantRif) && h.getPlate().equalsIgnoreCase(plate))
                    .collect(Collectors.toList());


            if (!listPlates.isEmpty()){
                try {
                    vehicleEsb = getVehicle(platesRegistrationHistory.getCurrentPlate());
                } catch (IOException e) {
                    LOGGER.info(MessageCode.CLAIMS_1017.value());
                    throw new NotFoundException(MessageCode.CLAIMS_1017, e);
                }
                vehicleEsb.setLicensePlate(plate);
            }else{
                throw new NotFoundException(MessageCode.CLAIMS_2014);
            }

        }


        return vehicleEsb;
    }

    @Override
    public VehicleRegistrationResponse getVehicleRegistrationsByPlate(String plate) throws IOException {
        VehicleRegistrationResponse response  = null;

        Future<MiddlewareVehicleRegistrationsResponse> vehicleRegistrationsFuture = null;

        MiddlewareVehicleRegistrationsResponse vehicleRegistrations = null;

        try {
            vehicleRegistrationsFuture =  middlewareClient.getFutureVehicleRegistrationsByPlate(plate);

            if (vehicleRegistrationsFuture != null) {
                vehicleRegistrations = serviceUtil.getCompletedFromFuture(vehicleRegistrationsFuture);
            }

            response = vehicleRegistrationResponseAdapter.adaptToDTO(vehicleRegistrations, null);
        } catch (Exception e) {
            LOGGER.info(MessageCode.CLAIMS_1017.value());
            throw new NotFoundException(MessageCode.CLAIMS_1017, e);
        }


        return response;
    }


    private String getDriverContractCustomerCustomerInfoGoLiveMSA(String contractOrPlate, String date, String endDateCheck) throws IOException {

        BlockingQueue<Object> responseList = new ArrayBlockingQueue<>(7);

        ContractInfoResponseV1 contractInfoResponseV1 = new ContractInfoResponseV1();


        String formattedDate = date.substring(0, 4) + "-" + date.substring(4, 6) + "-" + date.substring(6, 8);

        String regex = "\\d+";
        boolean isPlate = false;
        VehicleEsb vehicle = null;
        //it's a plate

        if (!contractOrPlate.matches(regex)) {
            isPlate = true;
            try {
                    /*
                    CO-643 ricerca storico delle targhe
                    * */
                //  vehicle = getVehicle(contractOrPlate);
                vehicle = getPlateFromVehicleRegistrations(contractOrPlate, date);

            } catch (NotFoundException ex) {
                LOGGER.info(MessageCode.CLAIMS_1017.value());
                throw new NotFoundException(MessageCode.CLAIMS_1017, ex);
            }
            Long idContract = vehicle.getIdActiveContract(formattedDate);

            if (idContract == null) {
                //non esiste alcun contratto attivo associato alla targa
                LOGGER.info("Plate " + contractOrPlate + " is not related to an active contract");
                throw new ForbiddenException("Plate " + contractOrPlate + " is not related to an active contract", MessageCode.CLAIMS_1025);
            }
            contractOrPlate = vehicle.getIdActiveContract(formattedDate).toString();
            contractInfoResponseV1.setVehicleResponse(VehicleAdapter.adptMiddlewareVehicleResponseToVehicleResponse(vehicle));
        }
        ContractEsb contract = getContractDetail(contractOrPlate, date);
        if (contract == null) {
            LOGGER.info(MessageCode.CLAIMS_1015.value());
            throw new NotFoundException(MessageCode.CLAIMS_1015);
        } else if (contract.getTakeinDate() != null) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date dateSX = DateUtil.convertIS08601StringToUTCDate(endDateCheck);
            String dateSXString = simpleDateFormat.format(dateSX);
            String takeinDate = simpleDateFormat.format(contract.getTakeinDate());
            if (dateSXString.compareTo(takeinDate) > 0) {
                LOGGER.info(MessageCode.CLAIMS_2009.value());
                throw new BadRequestException(MessageCode.CLAIMS_2009);
            }
        }

        contractInfoResponseV1.setContractResponse(ContractAdapter.adptMiddlewareContractDetailResponseToContractResponse(contract));
        if (contract.getCustomerId() == null) {
            LOGGER.info(MessageCode.CLAIMS_1012.value());
            throw new NotFoundException(MessageCode.CLAIMS_1012);
        }
            /*if(contract.getDriverId() == null){
                throw new NotFoundException(MessageCode.CLAIMS_1013);
            }*/
        if (contract.getLicensePlate() == null) {
            LOGGER.info(MessageCode.CLAIMS_1014.value());
            throw new NotFoundException(MessageCode.CLAIMS_1014);
        }
        return contract.getCustomerId().toString();
    }
}
