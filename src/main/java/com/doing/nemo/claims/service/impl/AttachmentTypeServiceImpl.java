package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.entity.enumerated.AttachmentEnum.GroupsEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsRepairEnum;
import com.doing.nemo.claims.entity.settings.AttachmentTypeEntity;
import com.doing.nemo.claims.repository.AttachmentTypeRepository;
import com.doing.nemo.claims.repository.AttachmentTypeRepositoryV1;
import com.doing.nemo.claims.service.AttachmentTypeService;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class AttachmentTypeServiceImpl implements AttachmentTypeService {

    private static Logger LOGGER = LoggerFactory.getLogger(AttachmentTypeServiceImpl.class);
    @Autowired
    private AttachmentTypeRepository attachmentTypeRepository;
    @Autowired
    private AttachmentTypeRepositoryV1 attachmentTypeRepositoryV1;

    @Override
    public AttachmentTypeEntity insertAttachmentType(AttachmentTypeEntity attachmentTypeEntity) {

        attachmentTypeRepository.save(attachmentTypeEntity);
        return attachmentTypeEntity;
    }

    @Override
    public AttachmentTypeEntity updateAttachmentType(AttachmentTypeEntity attachmentTypeEntity, UUID uuid) {

        Optional<AttachmentTypeEntity> attachmentTypeEntityOld = attachmentTypeRepository.findById(uuid);
        if (!attachmentTypeEntityOld.isPresent()) {
            LOGGER.debug("AttachmentType with id " + uuid + " not found");
            throw new NotFoundException("AttachmentType with id " + uuid + " not found", MessageCode.CLAIMS_1010);
        }

        attachmentTypeEntity.setId(attachmentTypeEntityOld.get().getId());
        attachmentTypeEntity.setTypeComplaint(attachmentTypeEntityOld.get().getTypeComplaint());
        attachmentTypeRepository.save(attachmentTypeEntity);
        return attachmentTypeEntity;
    }

    @Override
    public AttachmentTypeEntity getAttachmentType(UUID uuid) {
        Optional<AttachmentTypeEntity> attachmentTypeEntity = attachmentTypeRepository.findById(uuid);
        if (!attachmentTypeEntity.isPresent()) {
            LOGGER.debug("AttachmentType with id " + uuid + " not found");
            throw new NotFoundException("AttachmentType with id " + uuid + " not found", MessageCode.CLAIMS_1010);
        }

        return attachmentTypeEntity.get();
    }

    @Override
    public List<AttachmentTypeEntity> getAllAttachmentType(ClaimsRepairEnum claimsRepairEnum) {
        List<AttachmentTypeEntity> attachmentTypeEntities = attachmentTypeRepository.searchAttachmentTypeByTypeComplaint(claimsRepairEnum);


        return attachmentTypeEntities;
    }


    @Override
    public AttachmentTypeEntity deleteAttachmentType(UUID uuid) {
        Optional<AttachmentTypeEntity> attachmentTypeEntity = attachmentTypeRepository.findById(uuid);
        if (!attachmentTypeEntity.isPresent()) {
            LOGGER.debug("AttachmentType with id " + uuid + " not found");
            throw new NotFoundException("AttachmentType with id " + uuid + " not found", MessageCode.CLAIMS_1010);
        }

        attachmentTypeRepository.delete(attachmentTypeEntity.get());

        return null;
    }


    @Override
    public AttachmentTypeEntity updateStatus(UUID uuid) {
        AttachmentTypeEntity attachmentTypeEntity = getAttachmentType(uuid);
        if (attachmentTypeEntity.getActive()) {
            attachmentTypeEntity.setActive(false);
        } else {
            attachmentTypeEntity.setActive(true);
        }

        attachmentTypeRepository.save(attachmentTypeEntity);
        return attachmentTypeEntity;
    }


    @Override
    public List<AttachmentTypeEntity> findAttachmentTypesFiltered(Integer page, Integer pageSize, ClaimsRepairEnum claimsRepairEnum, String name, GroupsEnum groupsEnum, Boolean isActive, String orderBy, Boolean asc) {
        return attachmentTypeRepositoryV1.findAttachmentTypesFiltered(page, pageSize, claimsRepairEnum, name, groupsEnum, isActive, orderBy, asc);
    }

    @Override
    public Long countAttachmentTypeFiltered(ClaimsRepairEnum claimsRepairEnum, String name, GroupsEnum groupsEnum, Boolean isActive) {
        return attachmentTypeRepositoryV1.countAttachmentTypeFiltered(claimsRepairEnum, name, groupsEnum, isActive);
    }
}
