package com.doing.nemo.claims.service;

import com.doing.nemo.claims.entity.enumerated.AttachmentEnum.GroupsEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsRepairEnum;
import com.doing.nemo.claims.entity.settings.AttachmentTypeEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public interface AttachmentTypeService {

    AttachmentTypeEntity insertAttachmentType(AttachmentTypeEntity attachmentTypeEntity);

    AttachmentTypeEntity updateAttachmentType(AttachmentTypeEntity attachmentTypeEntity, UUID uuid);

    AttachmentTypeEntity getAttachmentType(UUID uuid);

    List<AttachmentTypeEntity> getAllAttachmentType(ClaimsRepairEnum claimsRepairEnum);

    AttachmentTypeEntity deleteAttachmentType(UUID uuid);

    AttachmentTypeEntity updateStatus(UUID uuid);

    List<AttachmentTypeEntity> findAttachmentTypesFiltered (Integer page, Integer pageSize, ClaimsRepairEnum claimsRepairEnum, String name, GroupsEnum groupsEnum, Boolean isActive, String orderBy, Boolean asc);

    Long countAttachmentTypeFiltered(ClaimsRepairEnum claimsRepairEnum,String name, GroupsEnum groupsEnum, Boolean isActive);

}
