package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.adapter.ClaimsAdapter;
import com.doing.nemo.claims.adapter.StatusAdapter;
import com.doing.nemo.claims.controller.payload.request.myald.MyAldLinkObjectRequestV1;
import com.doing.nemo.claims.controller.payload.request.myald.MyAldLinkRequestV1;
import com.doing.nemo.claims.controller.payload.response.ClaimsMyAldResponseV1;
import com.doing.nemo.claims.controller.payload.response.ClaimsResponseMyAldPaginationV1;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.ClaimsNewEntity;
import com.doing.nemo.claims.entity.enumerated.myAld.MyAldInsertedByEnum;
import com.doing.nemo.claims.repository.ClaimsNewRepository;
import com.doing.nemo.claims.repository.ClaimsRepositoryV1;
import com.doing.nemo.claims.service.ConverterClaimsService;
import com.doing.nemo.claims.service.MyAldService;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.AbstractException;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.exception.InternalException;
import com.doing.nemo.commons.util.HttpUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.Response;
import org.apache.commons.collections4.MapUtils;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.lang.reflect.Type;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MyAldServiceImpl implements MyAldService {

    private static Logger LOGGER = LoggerFactory.getLogger(MyAldServiceImpl.class);

    @Autowired
    private ClaimsRepositoryV1 claimsRepository;

    @Autowired
    private ClaimsNewRepository claimsNewRepository;

    @Autowired
    private ConverterClaimsService converterClaimsService;

    @Autowired
    private StatusAdapter statusAdapter;

    @Autowired
    private HttpUtil httpUtil;

    @Value("${myald.base.endpoint.url}")
    private String endpoint;

    @Value("${myald.base.path}")
    private String path;

    public class CustomTypeReference extends TypeReference<Object> {
        private final Type type;

        public CustomTypeReference(ParameterizedTypeReference pt) {
            this.type = pt.getType();
        }

        @Override
        public Type getType() {
            return type;
        }
    }



    private <T, P> T callWithoutCert(String url, HttpMethod method, P value, HashMap<String, String> headersparams, ParameterizedTypeReference<T> typeReference) throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = null;
        Response responseToken = null;
        try {
            if (value != null) {
                jsonInString = mapper.writeValueAsString(value);
            }

            responseToken = httpUtil.callURL(headersparams, url, jsonInString, method);

            if (responseToken.code() != HttpStatus.OK.value() && responseToken.code() != HttpStatus.CREATED.value() && responseToken.code() != HttpStatus.ACCEPTED.value()) {
                LOGGER.debug(responseToken.message() + " with code " + responseToken.code());
                throw new BadRequestException(responseToken.message() + " with code " + responseToken.code(), MessageCode.CLAIMS_1157);
            }

            if (typeReference == null)
                return null;

            String response = responseToken.body().string();
            LOGGER.debug("Response MyAld Body: {}", response);
            TypeReference tr = new CustomTypeReference(typeReference);
            return mapper.readValue(response, tr);
        } catch (AbstractException e) {
            LOGGER.error(e.getMessage(), e);
            throw e;
        }  catch (Exception e) {
            String message = String.format(MessageCode.CLAIMS_2012.value(), e.getClass().getSimpleName(), e.getMessage());
            LOGGER.error(message, e);
            throw new InternalException(message, MessageCode.CLAIMS_2012);
        } finally {
            if (responseToken != null && responseToken.body() != null) {
                responseToken.close();
            }
        }
    }


    /* FILTRI
        1) contratto, targa e n° sinistro
        2) data del sinistro dal - data del sinistro al
        3) stato denuncia
        4) data inserimento sinistro dal - data inserimento sinistro al
        5) inserita da: me, altri utenti, operatore ald
     */

    @Override
    public ClaimsResponseMyAldPaginationV1 getPaginationFmMyAld(
            String contractPlateNumberSx,
            List<String> clientIdList,
            String dateAccidentFrom,
            String dateAccidentTo,
            List<String> statusList,
            String dateCreatedFrom,
            String dateCreatedTo,
            List<MyAldInsertedByEnum> insertedByList,
            Boolean asc,
            String orderBy,
            Integer page,
            Integer pageSize,
            String userId,
            String clientId
    ) {
        ClaimsResponseMyAldPaginationV1 pagination = new ClaimsResponseMyAldPaginationV1();

        if(statusList != null && !statusList.isEmpty()){
            statusList = StatusAdapter.adptFromMyAldStatusListToClaimsStatusListString(statusList);
        }
        //recupera vecchia entità
        List<ClaimsNewEntity> claimsNewEntityList = claimsNewRepository.findClaimsForPaginationWithoutDraftMyAldFleetManager(contractPlateNumberSx,clientIdList,clientId,statusList,dateAccidentFrom,dateAccidentTo, dateCreatedFrom, dateCreatedTo, insertedByList, userId, asc,orderBy,page,pageSize);
        //converti nella vecchia entità
        List<ClaimsEntity> claimsEntityList =converterClaimsService.wrapFromClaimsNewEntityListToClaimsEntityList(claimsNewEntityList);
        BigInteger count = claimsNewRepository.countClaimsForPaginationWithoutDraftMyAldFleetManager(contractPlateNumberSx,clientIdList,clientId,statusList,dateAccidentFrom,dateAccidentTo, dateCreatedFrom, dateCreatedTo,insertedByList, userId);

        pagination.setStats(new ClaimsResponseMyAldPaginationV1.Pagination(count.intValue(), page, pageSize));
        List<ClaimsMyAldResponseV1> claimsMyAldResponseV1s = ClaimsAdapter.adptFromClaimsToMyAldResponse(claimsEntityList,userId);
        pagination.setItems(claimsMyAldResponseV1s);

        return pagination;
    }

    public ClaimsResponseMyAldPaginationV1 getPaginationDriverMyAld(
            List<String> plates,
            String contractPlateNumberSx,
            String dateAccidentFrom,
            String dateAccidentTo,
            List<String> statusList,
            String dateCreatedFrom,
            String dateCreatedTo,
            List<MyAldInsertedByEnum> insertedByList,
            Boolean asc,
            String orderBy,
            Integer page,
            Integer pageSize,
            String userId,
            Boolean isInclusive
    ) {
        ClaimsResponseMyAldPaginationV1 pagination = new ClaimsResponseMyAldPaginationV1();
        if(statusList != null && !statusList.isEmpty()){
            statusList = StatusAdapter.adptFromMyAldStatusListToClaimsStatusListString(statusList);
        }
        if(plates == null || plates.isEmpty()){
            throw new BadRequestException(MessageCode.CLAIMS_2007);
        }

        //recupero della nuova entità
        List<ClaimsNewEntity> claimsNewEntityList = claimsNewRepository.findClaimsForPaginationWithoutDraftMyAldDriver(plates,contractPlateNumberSx,statusList,dateAccidentFrom,dateAccidentTo, dateCreatedFrom, dateCreatedTo,asc,orderBy,page,pageSize, userId,insertedByList, isInclusive);
        //conversione nella vecchia entità
        List<ClaimsEntity> claimsEntityList = converterClaimsService.wrapFromClaimsNewEntityListToClaimsEntityList(claimsNewEntityList);
        BigInteger count = claimsNewRepository.countClaimsForPaginationWithoutDraftMyAldDriver(plates,contractPlateNumberSx,statusList,dateAccidentFrom,dateAccidentTo, dateCreatedFrom, dateCreatedTo, userId,insertedByList, isInclusive);


        pagination.setStats(new ClaimsResponseMyAldPaginationV1.Pagination(count.intValue(), page, pageSize));
        List<ClaimsMyAldResponseV1> claimsMyAldResponseV1s = ClaimsAdapter.adptFromClaimsToMyAldResponse(claimsEntityList, userId);
        pagination.setItems(claimsMyAldResponseV1s);
        return pagination;
    }


    public List<String> getMyaldLink(ClaimsEntity claimsEntity) throws IOException {

        String customerId = claimsEntity != null && claimsEntity.getDamaged() != null && claimsEntity.getDamaged().getCustomer() != null && claimsEntity.getDamaged().getCustomer().getCustomerId() != null ? claimsEntity.getDamaged().getCustomer().getCustomerId() : null;


        String url = buildUrl(
                endpoint,
                path,
                new HashMap<String, String>() {{
                    put("customer_id", customerId);
                }},
                null
        );
        MyAldLinkObjectRequestV1 myAldLinkObjectRequest = new MyAldLinkObjectRequestV1();
        if(claimsEntity != null){
            myAldLinkObjectRequest.setClaimId(claimsEntity.getId());
            myAldLinkObjectRequest.setClaimStatus(StatusAdapter.adptFromClaimsStatusToMyAldStatus(claimsEntity.getStatus()).getValue());
            if(customerId != null){
                myAldLinkObjectRequest.setIdCustomer(Long.parseLong(customerId));
            }
            if(claimsEntity.getMetadata() != null && claimsEntity.getMetadata().getMetadata() != null) {
                myAldLinkObjectRequest.setUserCreated((Integer) claimsEntity.getMetadata().getMetadata().get("user_id"));
            }
        }

        MyAldLinkRequestV1 myAldLinkRequestV1 = new MyAldLinkRequestV1();
        myAldLinkRequestV1.setClaimDetail(myAldLinkObjectRequest);
        return callWithoutCert(url, HttpMethod.POST, myAldLinkRequestV1, null, new ParameterizedTypeReference<List<String>>() {
        });

    }

    private static String buildUrl(String baseUrl, String uri, Map<String, String> pathParams, MultiValueMap<String, String> queryParams) {

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(baseUrl.concat(uri));

        if (MapUtils.isNotEmpty(queryParams)) {
            builder.queryParams(queryParams);
        }

        return builder.buildAndExpand(pathParams).toString();
    }
}
