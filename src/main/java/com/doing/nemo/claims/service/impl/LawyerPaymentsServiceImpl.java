package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.response.PaginationStats;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.ClaimsNewEntity;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsFlowEnum;
import com.doing.nemo.claims.entity.enumerated.LawyerPaymentsStatusEnum;
import com.doing.nemo.claims.entity.jsonb.refund.Refund;
import com.doing.nemo.claims.entity.jsonb.refund.Split;
import com.doing.nemo.claims.entity.settings.LawyerPaymentsEntity;
import com.doing.nemo.claims.repository.*;
import com.doing.nemo.claims.service.ClaimsService;
import com.doing.nemo.claims.service.ConverterClaimsService;
import com.doing.nemo.claims.service.LawyerPaymentsService;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.BadParametersException;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class LawyerPaymentsServiceImpl implements LawyerPaymentsService {

    @Autowired
    private LawyerPaymentsEntityRepositoryV1 lawyerPaymentsEntityRepositoryV1;

    @Autowired
    private LawyerPaymentsRepository lawyerPaymentsRepository;

    @Autowired
    private ClaimsNewRepository claimsNewRepository;

    @Autowired
    private ConverterClaimsService converterClaimsService;

    @Autowired
    private ClaimsService claimsService;
    

    private static Logger LOGGER = LoggerFactory.getLogger(LawyerPaymentsServiceImpl.class);

    @Override
    public Pagination<LawyerPaymentsEntity> getLawyerPaymentPagination(String plate, Long practiceId, String dateAccidentFrom, String dateAccidentTo,
                                                                String paymentType, String bank, String lawyerCode, String accSale, String status, int page, int pageSize ) {
        try {

            Pagination<LawyerPaymentsEntity> lawyerPaymentsPagination = new Pagination<>();

            LocalDateTime localDateTimeDateAccidentFrom = null;
            LocalDateTime localDateTimeDateAccidentTo = null;
            if(dateAccidentFrom != null && dateAccidentTo != null){

                localDateTimeDateAccidentFrom = LocalDateTime.parse(dateAccidentFrom, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
                localDateTimeDateAccidentTo = LocalDateTime.parse(dateAccidentTo, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
            }

            List<LawyerPaymentsEntity> claimsList = lawyerPaymentsEntityRepositoryV1.findLawyerPaymentsForPagination(plate, practiceId, localDateTimeDateAccidentFrom, localDateTimeDateAccidentTo, paymentType, bank, lawyerCode, accSale, status, page, pageSize );
            BigInteger itemCount = lawyerPaymentsEntityRepositoryV1.countLawyerPaymentsForPagination(plate, practiceId, localDateTimeDateAccidentFrom, localDateTimeDateAccidentTo, paymentType, bank, lawyerCode, accSale, status );

            //Mi serve necessariemante per calcolare la pagina di start
            lawyerPaymentsPagination.setStats( new PaginationStats(itemCount.intValue(), page, pageSize));
            lawyerPaymentsPagination.setItems(claimsList);

            return lawyerPaymentsPagination;

        } catch (BadParametersException ex) {
            LOGGER.debug(ex.getMessage());
            throw ex;
        } catch (BadRequestException ex) {
            LOGGER.debug("There are problems with the incoming Date_accident, the correct format is DD-MM-YYYY");
            throw new BadRequestException(MessageCode.CLAIMS_1011, ex);
        }
    }


    //REFACTOR
    @Override
    public void postAssignedLawyerPayments(List<UUID> lawyerPaymentsList){

        for (UUID att : lawyerPaymentsList){

            Optional<LawyerPaymentsEntity> lawyerPaymentsOpt = lawyerPaymentsRepository.findById(att);
            if(lawyerPaymentsOpt.isPresent()){

                LawyerPaymentsEntity lawyerPayments = lawyerPaymentsOpt.get();

                String dateAccidentISO = DateUtil.convertUTCInstantToIS08601String(lawyerPayments.getDateAccident());

                //recupero della lista dei nuovi oggetti
                //Utilizzo la paginazione dei claims per recuperare il clamis con il pratics_id, la targa e la data sinistro individuata nel lawyer payments individuato
                List<ClaimsNewEntity> claimsNewList = claimsNewRepository.findClaimsForPagination(null,lawyerPayments.getPracticeId(), null, null,
                lawyerPayments.getPlate(),null, null, dateAccidentISO, dateAccidentISO, null, null, null, 1
                ,20,null,null, null, null, null, null);

                List<ClaimsEntity> claimsList = converterClaimsService.wrapFromClaimsNewEntityListToClaimsEntityList(claimsNewList);

                //Controllo che la query torni un risultato, in quanto potrebbe essere stato  modificato uno dei tre parametri sopracitati, e di conseguenza
                //potrei non avere più una corrispondenza
                if(claimsList != null && !claimsList.isEmpty()){

                    ClaimsEntity claimsEntity = claimsList.get(0);

                    Refund refund = null;

                    if(claimsEntity.getRefund() == null){
                        claimsEntity.setRefund(new Refund());
                    }

                    refund = claimsEntity.getRefund();
                    List<Split> splitList = refund.getSplitList();

                    if(splitList == null){
                        splitList = new LinkedList<>();
                    }

                    Split split = new Split();

                    split.setId(lawyerPayments.getId());
                    split.setTechnicalStop(lawyerPayments.getTechnicalShutdown());
                    split.setMaterialAmount(lawyerPayments.getDamage());
                    split.setLegalFees(lawyerPayments.getFee());
                    split.setType(lawyerPayments.getAccSale());
                    split.setRefundType(lawyerPayments.getPaymentType());

                    splitList.add(split);
                    refund.setSplitList(splitList);


                    Double totalLiquidationReceived = 0.0;
                    for(Split currentSplit : refund.getSplitList()){
                        if(currentSplit.getTotalPaid() != null) {
                            totalLiquidationReceived += currentSplit.getTotalPaid();
                        }
                    }

                    refund.setTotalLiquidationReceived(totalLiquidationReceived);

                    if(claimsEntity.getType() != null && claimsEntity.getType().equals(ClaimsFlowEnum.FCM) && refund.getPoSum() != null && refund.getTotalLiquidationReceived()!= null){
                        refund.setAmountToBeDebited(refund.getPoSum() - refund.getTotalLiquidationReceived());
                    }
                    refund = claimsService.setRefundFranchiseAmountFcm(claimsEntity,refund);

                    claimsEntity.setRefund(refund);

                    //conversione nella nuova entità
                    ClaimsNewEntity claimsNewEntity = converterClaimsService.convertOldEntityToNewClaimsEntity(claimsEntity);

                    claimsNewRepository.save(claimsNewEntity);
                    lawyerPayments.setStatus(LawyerPaymentsStatusEnum.ASSIGNED);

                }else{

                    //Se non esiste una corrispondenza nei claims presenti nel database, allora il record corrispondente nei lawyerPayments
                    //verrà salvato in uno stato di errore
                    lawyerPayments.setStatus(LawyerPaymentsStatusEnum.ERROR);
                }

                lawyerPaymentsRepository.save(lawyerPayments);
            }
        }
    }
}
