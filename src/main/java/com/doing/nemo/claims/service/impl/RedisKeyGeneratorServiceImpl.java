package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.service.RedisKeyGeneratorService;
import org.springframework.stereotype.Service;

@Service
public class RedisKeyGeneratorServiceImpl implements RedisKeyGeneratorService {

    private String getKeyForRedis(String base, Boolean value) {
        if( value == null ) {
            return base + ClaimsServiceImpl.REDIS_KEY_NULL;
        } else if( value ) {
            return base + ClaimsServiceImpl.REDIS_KEY_TRUE;
        } else {
            return base + ClaimsServiceImpl.REDIS_KEY_FALSE;
        }
    }

    public String getFullKeyForRedis(Boolean inEvidence, Boolean withContinuation) {
        return new StringBuffer()
                .append(this.getKeyForRedis( ClaimsServiceImpl.REDIS_KEY_IN_EVIDENCE, inEvidence))
                .append(ClaimsServiceImpl.SEPARATOR)
                .append(this.getKeyForRedis( ClaimsServiceImpl.REDIS_KEY_WITH_CONTINUATION, withContinuation))
                .append(ClaimsServiceImpl.EOL)
                .toString()
        ;
    }

}
