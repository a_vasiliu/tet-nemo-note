package com.doing.nemo.claims.service;

public interface RedisKeyGeneratorService {

    String getFullKeyForRedis(Boolean inEvidence, Boolean withContinuation);
    
}