package com.doing.nemo.claims.service;


import com.doing.nemo.claims.controller.payload.response.ClosingResponseV1;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public interface ClosingService {
    List<ClosingResponseV1> findClosing(String idContract);
}
