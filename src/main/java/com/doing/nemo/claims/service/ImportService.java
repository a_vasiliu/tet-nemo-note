package com.doing.nemo.claims.service;

import com.doing.nemo.claims.controller.payload.response.DataManagementImportResponseV1;
import com.doing.nemo.claims.controller.payload.response.UploadFileResponseV1;
import com.doing.nemo.claims.entity.enumerated.FileImportTypeEnum;
import org.springframework.web.multipart.MultipartFile;

import java.rmi.ConnectException;

public interface ImportService {


    DataManagementImportResponseV1 importFileWithWasteFile(MultipartFile fileToBeImport, String fileDescription, FileImportTypeEnum fileType) throws ConnectException;
    void startJob(FileImportTypeEnum fileType);
    UploadFileResponseV1 importFile(MultipartFile fileToBeImport, String fileDescription, FileImportTypeEnum fileType, String staus);
    void massiveEntrust(FileImportTypeEnum fileImportType);

}
