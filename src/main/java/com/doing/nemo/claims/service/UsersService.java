package com.doing.nemo.claims.service;

import java.io.IOException;

public interface UsersService {

    String getUserEmailByUserId(String userId, String authorization) throws IOException;

}
