package com.doing.nemo.claims.service;

import com.doing.nemo.claims.controller.payload.response.MotivationResponseV1;
import com.doing.nemo.claims.entity.enumerated.ClaimsRepairEnum;
import com.doing.nemo.claims.entity.settings.MotivationEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public interface MotivationService {
    MotivationEntity insertMotivation(MotivationEntity motivationEntity);

    MotivationEntity updateMotivation(MotivationEntity motivationEntity);

    MotivationEntity selectMotivation(UUID id);

    List<MotivationEntity> selectAllMotivation(ClaimsRepairEnum claimsRepairEnum);

    List<MotivationEntity> selectAllMotivationFilteredByIsActive(ClaimsRepairEnum claimsRepairEnum, Boolean isActive);

    MotivationResponseV1 deleteMotivation(UUID id);

    MotivationEntity updateStatus(UUID uuid);


    List<MotivationEntity> findMotivationsFiltered (Integer page, Integer pageSize, ClaimsRepairEnum claimsRepairEnum, String answerType, String description, Boolean isActive,Boolean asc, String orderBy);

    Long countMotivationFiltered(ClaimsRepairEnum claimsRepairEnum, String answerType, String description, Boolean isActive);
}