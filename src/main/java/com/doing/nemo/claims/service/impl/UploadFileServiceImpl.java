package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.UploadFileRequestV1;
import com.doing.nemo.claims.controller.payload.response.UploadFileResponseV1;
import com.doing.nemo.claims.entity.enumerated.FileImportTypeEnum;
import com.doing.nemo.claims.entity.settings.DataManagementExportEntity;
import com.doing.nemo.claims.entity.settings.DataManagementImportEntity;
import com.doing.nemo.claims.entity.settings.ExportingFile;
import com.doing.nemo.claims.entity.settings.ProcessingFile;
import com.doing.nemo.claims.repository.DataManagementImportRepository;
import com.doing.nemo.claims.service.FileManagerService;
import com.doing.nemo.claims.service.UploadFileService;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayOutputStream;
import java.rmi.ConnectException;
import java.time.Instant;
import java.util.*;

@Service
public class UploadFileServiceImpl implements UploadFileService {

    @Autowired
    private FileManagerService fileManagerService;

    @Autowired
    private DataManagementImportRepository dataManagementImportRepository;


    private static Logger LOGGER = LoggerFactory.getLogger(UploadFileServiceImpl.class);

    @Override
    public List<List<String>> decodeFileFromBase64(String fileBase64){

        byte[] decodeFileBase64 = Base64.getDecoder().decode(fileBase64);
        String stringFileBase64 = new String(decodeFileBase64);

        List<String> listProva = Arrays.asList(stringFileBase64.split("\\r?\\n"));
        List<List<String>> result = new ArrayList<>();
        for (String att : listProva ) {
            result.add(Arrays.asList(att.split(",")));
        }
        return result;
    }

    @Override
    public String encodeFileInBase64(ByteArrayOutputStream baos) {

        byte[] encodeFileBase64 = Base64.getEncoder().encode(baos.toByteArray());
        return new String(encodeFileBase64);
    }
    @Override
    public String encodeFileInBase64(String fileString) {

        byte[] encodeFileBase64 = Base64.getEncoder().encode(fileString.getBytes());
        return new String(encodeFileBase64);
    }

    @Override
    public UploadFileResponseV1 uploadDeductibleFile(MultipartFile fileToBeImport, String fileContent, String fileDescription, FileImportTypeEnum fileType) throws ConnectException {

        try {

            UploadFileRequestV1 uploadFileRequest = new UploadFileRequestV1();

            String filename =  fileToBeImport.getOriginalFilename().replace(".csv", "_"+DateUtil.getDateStringWithoutSeparationCharacters(Instant.now())+".csv");
            String blobType  = filename.substring((filename.lastIndexOf("."))+1, filename.length());

            UUID dataManagmentImportUUID = UUID.randomUUID();

            uploadFileRequest.setFileName(filename);
            uploadFileRequest.setDescription(fileDescription);
            uploadFileRequest.setBlobType(blobType);
            uploadFileRequest.setResourceId(dataManagmentImportUUID.toString());

            uploadFileRequest.setFileContent(this.encodeFileInBase64(fileContent));
            //COME TIPO DI RISORSA METTO CLAIMS
            uploadFileRequest.setResourceType("claims");

            //Carico il file su FILE MANAGER
            UploadFileResponseV1 uploadFileResponse = null;
            uploadFileResponse = fileManagerService.uploadSingleFile(uploadFileRequest);

            //ALLEGO L'UUID DEL FILE OTTENUTO DA FILE MANAGER NEL RECORD DA INSERIRE IN PROCESSING_FILE
            ProcessingFile processingFile = new ProcessingFile();
            processingFile.setAttachmentId(UUID.fromString(uploadFileResponse.getUuid()));
            processingFile.setFileName(uploadFileResponse.getFileName());
            processingFile.setFileSize(uploadFileResponse.getFileSize());

            //INSERISCO IL RECORD NELLA TABELLA DATA_MANAGMENT_IMPORT IMPOSTANDO LO STATO IN "WAITING_FOR_PROCCESSING"
            DataManagementImportEntity dataManagementImport = new DataManagementImportEntity();
            dataManagementImport.setId(dataManagmentImportUUID);
            dataManagementImport.setDate(DateUtil.getNowInstant());
            dataManagementImport.setUser("User");
            dataManagementImport.setDescription(uploadFileRequest.getDescription());
            dataManagementImport.setFileType(fileType);
            dataManagementImport.setResult("WAITING_FOR_PROCESSING");
            dataManagementImport.getProcessingFiles().add(processingFile);
            dataManagementImportRepository.save(dataManagementImport);

            //CARICO IL FILE SUL SERVER SFTP
            //sftpUtils.uploadFileToFTP(fileToBeImport);

            return uploadFileResponse;

        } catch (ConnectException e) {
            LOGGER.debug(e.getMessage());
            throw e;
        }
    }

    @Override
    public DataManagementExportEntity uploadExportFile(DataManagementExportEntity dataManagementExport, String fileName, String fileContent, String fileDescription, String user, UUID dataManagmentExportUUID, String typeExport) throws ConnectException{

        if(dataManagementExport == null){

            dataManagementExport = new DataManagementExportEntity();
            dataManagementExport.setId(dataManagmentExportUUID);
            dataManagementExport.setDate(DateUtil.getNowInstant());
            dataManagementExport.setUser(user);
            dataManagementExport.setDescription(typeExport);
            dataManagementExport.setResult("PROCESSED");
        }

        ExportingFile exportingFile = null;

        UploadFileRequestV1 uploadFileExportRequest = new UploadFileRequestV1();
        uploadFileExportRequest.setBlobType("csv");
        uploadFileExportRequest.setDescription(fileDescription);
        uploadFileExportRequest.setFileName(fileName + ".csv");
        uploadFileExportRequest.setResourceType("export");
        uploadFileExportRequest.setResourceId(dataManagmentExportUUID.toString());
        uploadFileExportRequest.setFileContent(this.encodeFileInBase64(fileContent));

        UploadFileResponseV1 uploadExportFileResponse = fileManagerService.uploadSingleFile(uploadFileExportRequest);

        exportingFile = new ExportingFile();
        exportingFile.setId(UUID.randomUUID());
        exportingFile.setAttachmentId(UUID.fromString(uploadExportFileResponse.getUuid()));
        exportingFile.setFileName(uploadExportFileResponse.getFileName());
        exportingFile.setFileSize(uploadExportFileResponse.getFileSize());

        dataManagementExport.getProcessingFiles().add(exportingFile);

        return dataManagementExport;
    }
}
