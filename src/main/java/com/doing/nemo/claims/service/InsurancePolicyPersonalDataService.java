package com.doing.nemo.claims.service;

import com.doing.nemo.claims.entity.settings.InsurancePolicyPersonalDataEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface InsurancePolicyPersonalDataService {
    List<InsurancePolicyPersonalDataEntity> updateInsurancePolicyPersonalDataList(List<InsurancePolicyPersonalDataEntity> insurancePolicyPersonalDataEntityList);

    void deleteInsurancePolicyPersonalDataList(List<InsurancePolicyPersonalDataEntity> insurancePolicyPersonalDataEntityList);
}
