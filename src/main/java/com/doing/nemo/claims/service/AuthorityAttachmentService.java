package com.doing.nemo.claims.service;

import com.doing.nemo.claims.controller.payload.request.authority.claims.AuthorityRequestV1;
import com.doing.nemo.claims.controller.payload.request.authority.claims.AuthorityTotalRequestV1;
import com.doing.nemo.claims.controller.payload.request.authority.practice.AuthorityPracticeRequestV1;
import com.doing.nemo.claims.controller.payload.request.authority.practice.AuthorityPracticeUpdateRequestV1;
import com.doing.nemo.claims.controller.payload.request.authority.practice.ProcessingRequestV1;
import com.doing.nemo.claims.controller.payload.request.authority.repair.AuthorityRepairRequestV1;
import com.doing.nemo.claims.controller.payload.request.authority.repair.AuthorityRepairStatusRequestV1;
import com.doing.nemo.claims.controller.payload.request.authority.repair.AuthorityRepairTotalRequestV1;
import com.doing.nemo.claims.controller.payload.response.ClaimsResponseIdV1;
import com.doing.nemo.claims.controller.payload.response.CounterpartyResponseV1;
import com.doing.nemo.claims.controller.payload.response.PaginationResponseV1;
import com.doing.nemo.claims.controller.payload.response.authority.AuthorityWorkingsAttachmentResponseV1;
import com.doing.nemo.claims.controller.payload.response.authority.claims.AuthorityClaimsResponseV1;
import com.doing.nemo.claims.controller.payload.response.authority.claims.AuthorityOldestResponseV1;
import com.doing.nemo.claims.controller.payload.response.authority.claims.AuthorityPaginationResponseV1;
import com.doing.nemo.claims.controller.payload.response.authority.claims.VehicleStatusAuthorityResponseV1;
import com.doing.nemo.claims.controller.payload.response.authority.practice.AuthorityPracticeAssociationListResponseV1;
import com.doing.nemo.claims.controller.payload.response.authority.practice.AuthorityPracticePaginationResponseV1;
import com.doing.nemo.claims.controller.payload.response.authority.practice.WorkingAdministrationResponseV1;
import com.doing.nemo.claims.controller.payload.response.authority.repair.AuthorityPaginationRepairResponseV1;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.CounterpartyEntity;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.enumerated.AuthorityEnum.AdministrativePracticeTypeEnum;
import com.doing.nemo.claims.entity.enumerated.AuthorityEnum.AuthorityPracticeTypeEnum;
import com.doing.nemo.claims.entity.enumerated.WorkingStatusEnum;
import com.doing.nemo.claims.entity.jsonb.Authority;
import com.doing.nemo.claims.entity.settings.PracticeEntity;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.UUID;


@Service
public interface AuthorityAttachmentService {
    AuthorityWorkingsAttachmentResponseV1 getWorkingsAttachment(List<String> workingsList) throws IOException;
    ClaimsEntity addAuthorityAttachment(ClaimsEntity claimsEntity);
    Pagination<ClaimsEntity> addAuthorityAttachment(Pagination<ClaimsEntity> claimsEntityPagination);
    //REFACTOR
    AuthorityWorkingsAttachmentResponseV1 getWorkingsAttachmentByClaimId(String claimsId) throws IOException;
    AuthorityWorkingsAttachmentResponseV1 getWorkingsAttachmentByRepairId(String repairId) throws IOException;
    List<CounterpartyEntity> addAuthorityAttachmentRepair(List<CounterpartyEntity> counterpartyList);
    CounterpartyEntity addAuthorityAttachmentRepair(CounterpartyEntity counterpartyResponseV1);
}


