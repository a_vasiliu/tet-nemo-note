package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.response.PersonalDataResponseV1;
import com.doing.nemo.claims.entity.ClaimsPendingEntity;
import com.doing.nemo.claims.entity.esb.CustomerESB.CustomerEsb;
import com.doing.nemo.claims.entity.settings.ContractEntity;
import com.doing.nemo.claims.entity.settings.CustomTypeRiskEntity;
import com.doing.nemo.claims.entity.settings.InsurancePolicyPersonalDataEntity;
import com.doing.nemo.claims.entity.settings.PersonalDataEntity;
import com.doing.nemo.claims.repository.ClaimsPendingRepository;
import com.doing.nemo.claims.repository.PersonalDataRepository;
import com.doing.nemo.claims.repository.PersonalDataRepositoryV1;
import com.doing.nemo.claims.service.*;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.exception.InternalException;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ClaimsPendingServiceImpl implements ClaimsPendingService {

    private static Logger LOGGER = LoggerFactory.getLogger(ClaimsPendingServiceImpl.class);
    @Autowired
    private ClaimsPendingRepository claimsPendingRepository;
    @Autowired
    private GoLiveStrategyService goLiveStrategyService;

    @Override
    public Boolean checkIfExistsPendingWorking(String plate, String dateAccident){
        /*Instant dateAccidentInst = DateUtil.getNowInstant();
        if(dateAccident!=null){
            dateAccidentInst = DateUtil.convertIS08601StringToUTCInstant(dateAccident);
        }*/
        List<ClaimsPendingEntity> claimsPendingEntityList = claimsPendingRepository.findByPlate(plate);
        return !CollectionUtils.isEmpty(claimsPendingEntityList);
    }

    public Boolean checkIfExistsPendingWorking(Long practiceId){
        if(goLiveStrategyService.getGoLivePendingCheck()){
            List<ClaimsPendingEntity> claimsPendingEntityList = claimsPendingRepository.findByPracticeId(practiceId);
            return !CollectionUtils.isEmpty(claimsPendingEntityList);
        }
        return false;
    }

}
