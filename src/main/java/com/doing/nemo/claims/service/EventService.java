package com.doing.nemo.claims.service;

import com.doing.nemo.claims.entity.settings.EventEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface EventService {
    List<EventEntity> updateEventEntityList(List<EventEntity> eventEntities);

    void deleteEventEntityList(List<EventEntity> eventEntities);
}
