package com.doing.nemo.claims.service;

import com.doing.nemo.claims.controller.payload.UploadFileClaimsMigrationRequestV1;
import com.doing.nemo.claims.controller.payload.UploadFileClaimsRequestV1;
import com.doing.nemo.claims.controller.payload.request.UploadFileRequestV1;
import com.doing.nemo.claims.controller.payload.response.AddTokenResponseV1;
import com.doing.nemo.claims.controller.payload.response.DownloadFileBase64ResponseV1;
import com.doing.nemo.claims.controller.payload.response.UploadFileResponseV1;
import com.doing.nemo.claims.controller.payload.response.filemanager.BlobExistsResponseV1;
import com.doing.nemo.claims.entity.jsonb.forms.Forms;
import com.doing.nemo.claims.entity.jsonb.forms.jsonbForms.Attachment;
import com.doing.nemo.claims.exception.MigrationException;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.exception.NotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.rmi.ConnectException;
import java.text.ParseException;
import java.util.List;
import java.util.UUID;

@Service
public interface FileManagerService {

    //REFACTOR NON NECESSARIO
    UploadFileResponseV1 uploadSingleFile(UploadFileRequestV1 uploadFileRequest) throws ConnectException;

    //REFACTOR
    List<UploadFileResponseV1> uploadFileByIdPractice(List<UploadFileClaimsRequestV1> uploadFileRequest, String idPractice, String idCounterparty, String user, String userName, Boolean isValidate) throws IOException;

    //REFACTOR
    List<UploadFileResponseV1> uploadFile(List<UploadFileClaimsRequestV1> uploadFileRequest, String idClaims, String idCounterparty, String user, String userName, Boolean isValidate) throws IOException;

    //REFACTOR NON NECESSARIO
    UploadFileResponseV1 octoFileManager(UploadFileRequestV1 uploadFile, String idClaims) throws IOException;

    //REFACTOR
    List<DownloadFileBase64ResponseV1> downloadAllFileAttachment(String idClaims) throws IOException;

    //REFACTOR NON NECESSARIO
    DownloadFileBase64ResponseV1 downloadFile(String idFileManager) throws IOException;

    //REFACTOR
    List<Attachment> deleteFile(String idAttachment, String idClaims, String idCounterparty);

    //REFACTOR
    List<Attachment> deleteFile(String idAttachment, String idClaims) throws ConnectException;

    //REFACTOR
    List<Attachment> deleteFileList(List<String> attachmentsList, String idClaims, String idCounterparty) throws ConnectException;

    //REFACTOR
    List<Attachment> deleteFileList(List<String> attachmentsList, String idClaims) throws ConnectException;

    //REFACTOR
    void attachPdfFileClaim(String idClaims, String idFileManager, String userId, String userName, Boolean isPai, Boolean isLegal) throws IOException;

    //REFACTOR NON NECESSARIO
    List<UploadFileResponseV1> uploadFileInsurancePolicy(List<UploadFileRequestV1> uploadFileRequest, String id, String user, String userName) throws IOException;

    //REFACTOR NON NECESSARIO
    List<Attachment> deleteFileInsurancePolicy(List<String> idAttachmentList, UUID idPolicy) throws ConnectException;

    //REFACTOR NON NECESSARIO
    List<Attachment> deleteFileListForPractice(List<String> attachmentsList, String idPractice) throws ConnectException;

    //REFACTOR NON NECESSARIO
    List<UploadFileResponseV1> uploadFilePractice(List<UploadFileClaimsRequestV1> uploadFileRequest, String id, String user, String userName) throws IOException;

    List<AddTokenResponseV1> getTokenList(String uuidFileManager) throws IOException;

    //REFACTOR
    List<UploadFileResponseV1> uploadFilePractice(List<UploadFileClaimsRequestV1> uploadFileRequest, Long idClaims, String user, String userName, Boolean isValidate) throws MigrationException, IOException;

    //REFACTOR
    List<UploadFileResponseV1> uploadFilePracticeNew(List<UploadFileClaimsMigrationRequestV1> uploadFileRequests, Long idPractice, String user, String userName, Boolean isValidate) throws MigrationException, IOException, ParseException;

    //REFACTOR
    Forms attachPdfFileClaimEnjoy(String idClaims, String idFileManager, String userId, String userName, Boolean isPai, Boolean isLegal) throws IOException ;
}
