package com.doing.nemo.claims.service;

import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.enumerated.WorkabilitySystemEnum;
import com.doing.nemo.claims.entity.settings.RiskEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface GoLiveStrategyService {
    void goLiveStrategyChecksForInsert (String customerId, DataAccidentTypeAccidentEnum typeAccidentEnum);
    void goLiveStrategyChecksForInsert(String customerId, DataAccidentTypeAccidentEnum typeAccidentEnum, String profile);
    void goLiveStrategyChecksForMSA(String customerId);
    List<ClaimsEntity> goLiveStrategyChecksForAuthorityAssociation (List<ClaimsEntity> initialClaimsEntityList);
    WorkabilitySystemEnum goLiveStrategyChecksSystemForWorking (ClaimsEntity claimsEntity);
    String getGoLiveStepZeroDate ();
    Boolean getGoLivePendingCheck();
    Boolean checkIfStepMinusOne();
    Boolean checkIfStepZero();
    Boolean checkIfStepOne();
    Boolean checkIfStepTwo();
    Boolean checkIfStepThree();
    List<String> getCustomerIdListToFilterStepMinusOneAndHigher();
    List<String> getTypeAccidentListToFilterStepZero();
    List<String> getTypeAccidentListToFilterStepTwoOne();
}
