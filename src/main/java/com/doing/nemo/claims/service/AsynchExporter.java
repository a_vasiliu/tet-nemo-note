package com.doing.nemo.claims.service;

import com.doing.nemo.claims.adapter.KafkaAdapter;
import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.dto.AbstractExportFilter;
import com.doing.nemo.claims.dto.KafkaEventExportClaimsDTO;
import com.doing.nemo.claims.entity.enumerated.KafkaEventTypeEnum;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.TimeZone;

public abstract class AsynchExporter<T extends AbstractExportFilter> {

    @Autowired
    protected ClaimsExportSheetsBuilder sheetsBuilder;

    @Autowired(required = false)
    private NemoEventProducerService nemoEventProducerService;

    private static Logger LOGGER = LoggerFactory.getLogger(AsynchExporter.class);

    public String exportClaimsFileNameGenerator () {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));
        String now = simpleDateFormat.format(DateUtil.getNowDate());
        return "Estrazione_"+now;
    }

    public void uploadOnKafka(
            Map<String, Object> requestMap,
            int part,
            int partsCount,
            String topic,
            String userEmail,
            String headers,
            String templateName,
            String processId
    ) throws IOException {
        // MAKE REQUESTMAP A JSON IN BASE 64
        ObjectMapper mapper = new ObjectMapper();
        String requestMapAsJsonString = mapper.writeValueAsString(requestMap);
        // UPLOAD JSON REQUESTMAP ON FILEMANAGER
        KafkaEventExportClaimsDTO kafkaEventExportClaimsDTO;
        /*
         * il controllo per verificare che il record sia l'ultimo è necessario in quanto dev'essere specificato
         * all'endpoint di generazione file che quella è l'ultima riga, e quindi può chiudere il file
         */
        if(part >= partsCount) {
            kafkaEventExportClaimsDTO = KafkaAdapter.adptFromExportPropertiesToKafkaEventExportClaimsDTO("last", processId, requestMapAsJsonString, userEmail, headers, templateName);
        }
        else {
            kafkaEventExportClaimsDTO = KafkaAdapter.adptFromExportPropertiesToKafkaEventExportClaimsDTO("" + part, processId, requestMapAsJsonString, null, null, null);
        }
        LOGGER.info("[kafkaEventExportClaimsDTO] " + kafkaEventExportClaimsDTO.toString());
        nemoEventProducerService.produceEvent(kafkaEventExportClaimsDTO, topic, KafkaEventTypeEnum.EXPORT_CLAIMS);
    }

    public abstract  void export(T filter, int pagesNumber, int exportCount, String userEmail,String nemoUserId) throws IOException;

}
