package com.doing.nemo.claims.service;

import com.doing.nemo.claims.controller.payload.request.IncidentRequestV1;
import com.doing.nemo.claims.controller.payload.response.IncidentResponseV1;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.doing.nemo.claims.entity.jsonb.Historical;
import com.doing.nemo.claims.exception.ExternalCommunicationException;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public interface IncidentService {

    //REFACTOR
    /*@Retryable(
            maxAttempts = 3,
            backoff = @Backoff(delay = 1))*/
    IncidentResponseV1 insertIncident(String claimsId, ClaimsStatusEnum oldStatus) throws IOException,ExternalCommunicationException;

    ClaimsEntity insertIncidentEnjoy(ClaimsEntity claimsEntity, ClaimsStatusEnum oldStatus) throws IOException,ExternalCommunicationException;

    //REFACTOR
    IncidentResponseV1 modifyIncident(String claimsId, ClaimsStatusEnum oldStatus,Boolean franchise, IncidentRequestV1 oldValue, Boolean lastPayment, ClaimsEntity oldClaims) throws IOException, ExternalCommunicationException;

    Historical addIncidentHistoricalInsert (IncidentRequestV1 requestV1, ClaimsEntity claimsEntity, Boolean error, String messageError);
}
