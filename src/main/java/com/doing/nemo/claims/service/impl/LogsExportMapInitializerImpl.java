package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.service.ExportMapInitializer;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.Map;

@Service("LogsExportMapInitializerImpl")
public class LogsExportMapInitializerImpl implements ExportMapInitializer {

    public Map<String, Object> log() {
        Map<String,Object> innerMap = new HashMap<>();
        innerMap.put("practice_id", "");
        innerMap.put("type", "");
        innerMap.put("flow", "");
        innerMap.put("event_type", "");
        innerMap.put("old_status", "");
        innerMap.put("new_status", "");
        innerMap.put("old_type", "");
        innerMap.put("new_type", "");
        innerMap.put("update_at", "");
        innerMap.put("user_name", "");
        innerMap.put("old_flow", "");
        innerMap.put("new_flow", "");
        innerMap.put("targa_danneggiato", "");
        innerMap.put("communication_description", "");
        innerMap.put("data_sx", "");
        innerMap.put("inserita_il","");
        innerMap.put("cliente_nome","");
        return innerMap;
    }

}