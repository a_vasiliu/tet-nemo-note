package com.doing.nemo.claims.service;

import com.doing.nemo.claims.dto.KafkaEventDTO;
import com.doing.nemo.claims.dto.KafkaEventExportClaimsDTO;
import com.doing.nemo.claims.entity.enumerated.KafkaEventTypeEnum;
import org.springframework.stereotype.Service;

@Service
public interface NemoEventProducerService {

    void produceEvent(KafkaEventDTO request, String topic, KafkaEventTypeEnum type);

    void produceEvent(KafkaEventExportClaimsDTO request, String topic, KafkaEventTypeEnum type);

}
