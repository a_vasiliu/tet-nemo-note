package com.doing.nemo.claims.service;

import com.doing.nemo.claims.dto.ClaimsExportBaseData;
import com.doing.nemo.claims.entity.ClaimsEntity;

public interface ClaimsExportBaseDataBuilder {

    ClaimsExportBaseData build(ClaimsEntity claimsEntity);

}
