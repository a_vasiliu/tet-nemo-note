package com.doing.nemo.claims.service;

import com.doing.nemo.claims.controller.payload.request.*;
import com.doing.nemo.claims.controller.payload.response.EmailTemplateInternalMessagingResponseV1;
import com.doing.nemo.claims.controller.payload.response.EmailTemplateMessagingResponseV1;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.doing.nemo.claims.entity.enumerated.EventTypeEnum;
import com.doing.nemo.claims.entity.enumerated.LinkEmailTypeEnum;
import com.doing.nemo.claims.entity.jsonb.complaint.jsonbComplaint.Entrusted;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;

import java.io.IOException;
import java.time.Instant;
import java.util.Date;
import java.util.List;

public interface MessagingService {

    HttpStatus sendMail(@RequestBody SendMailRequestV1 sendMailRequest) throws IOException;

    HttpStatus sendMailWithTemplate(@RequestBody SendMailTemplateRequestV1 sendMailTemplateRequestV1) throws IOException;

    //REFACTOR
    List<EmailTemplateMessagingResponseV1> getMailTemplateByTypeEvent(EventTypeEnum typeEvent, String claimsId);
    //REFACTOR
    List<EmailTemplateMessagingResponseV1> getRepairMailTemplateByTypeEvent(EventTypeEnum typeEvent, String claimsId, String counterpartyId);

    HttpStatus sendEmail(List<EmailTemplateMessagingRequestV1> sendMailRequestList, String motivation, Instant createdAt);
    HttpStatus sendEmailEnjoy(List<EmailTemplateMessagingRequestV1> emailTemplateMessagingRequestList, String motivation, String claimsId);
    //REFACTOR
    HttpStatus sendEmailEntrustPaiEnjoy(List<EmailTemplateMessagingRequestV1> emailTemplateMessagingRequestList, String claimsId, String userId, String userName) throws IOException;
    //REFACTOR
    HttpStatus sendEmailEntrustPai(List<EmailTemplateMessagingRequestV1> sendMailRequestList, String claimsId, String userId,String userName) throws IOException;
    //REFACTOR
    HttpStatus sendEmailPoVariation(List<EmailTemplateMessagingRequestV1> sendMailRequestList, String claimsId, String userId,String userName) throws IOException;
    //REFACTOR
    void sendEmailConvertionURLAndLog (MessagingSendMailRequestV1 messagingSendMailRequestV1, String idCounterparty, String idClaim, String userId, String userName );
    HttpStatus sendEmailConfirmSequelNotify(SequelWithNotifyMessagingRequestV1 requestSequelMessaging, String claimsId, String userId,  String userName, ClaimsEntity claimsEntity);
    List<EmailTemplateMessagingResponseV1> getMailTemplateByTypeEvent(EventTypeEnum typeEvent, ClaimsEntity claims);
    String sendMailAndCreateLogsEnjoy(List<EmailTemplateMessagingRequestV1> listEmailTemplateMessagingRequestV1, String motivation, String claimsId);
    String sendMailAndCreateLogs(List<EmailTemplateMessagingRequestV1> listEmailTemplateMessagingRequestV1, String motivation, Instant createdAt);
    String createLogs(List<EmailTemplateMessagingRequestV1> listEmailTemplateMessagingRequestV1, String motivation);
    List<EmailTemplateMessagingResponseV1> getMailTemplateByTypeEventAndClaimsInternal(EventTypeEnum typeEvent, ClaimsEntity claims);
    List<EmailTemplateInternalMessagingResponseV1> getMailTemplateByTypeEventListAndClaimsInternal(List<EventTypeEnum> typeEventList, ClaimsEntity claims);
    ClaimsEntity sendEmailEntrustPaiInternal(List<EmailTemplateMessagingRequestV1> emailTemplateMessagingRequestList, ClaimsEntity claimsEntity, String userName, ClaimsStatusEnum statusEnum) throws IOException;
    //REFACTOR
    HttpStatus sendAndLogEmail(MessagingSendMailRequestV1 messagingSendMailRequestV1, String motivation, String idClaim, String idCounterparty, String user, String username);
    //REFACTOR
    HttpStatus sendEmailLegalComunication(List<EmailTemplateMessagingRequestV1> emailTemplateMessagingRequestList, String claimsId, String userId, String userName) throws IOException;
    //REFACTOR
    List<EmailTemplateMessagingRequestV1> splitEmailIfContainsUrl(List<EmailTemplateMessagingRequestV1> emailTemplateRequestList, String idClaims);
    List<EmailTemplateMessagingRequestV1> splitEmailIfContainsUrl(List<EmailTemplateMessagingRequestV1> emailTemplateRequestList, String idClaims, boolean isIncomplete);
    //REFACTOR
    List<EmailTemplateMessagingResponseV1> getMailTemplateByTypeEventManualEntrust(Entrusted entrusted, EventTypeEnum typeEvent, String claimsId);
}
