package com.doing.nemo.claims.service;

import com.doing.nemo.claims.controller.payload.request.authority.claims.AuthorityRequestV1;
import com.doing.nemo.claims.controller.payload.request.authority.claims.AuthorityTotalRequestV1;
import com.doing.nemo.claims.controller.payload.request.authority.practice.AuthorityPracticeRequestV1;
import com.doing.nemo.claims.controller.payload.request.authority.practice.AuthorityPracticeUpdateRequestV1;
import com.doing.nemo.claims.controller.payload.request.authority.practice.ProcessingRequestV1;
import com.doing.nemo.claims.controller.payload.request.authority.repair.AuthorityRepairRequestV1;
import com.doing.nemo.claims.controller.payload.request.authority.repair.AuthorityRepairStatusRequestV1;
import com.doing.nemo.claims.controller.payload.request.authority.repair.AuthorityRepairTotalRequestV1;
import com.doing.nemo.claims.controller.payload.response.*;
import com.doing.nemo.claims.controller.payload.response.authority.*;
import com.doing.nemo.claims.controller.payload.response.authority.claims.AuthorityClaimsResponseV1;
import com.doing.nemo.claims.controller.payload.response.authority.claims.AuthorityOldestResponseV1;
import com.doing.nemo.claims.controller.payload.response.authority.claims.AuthorityPaginationResponseV1;
import com.doing.nemo.claims.controller.payload.response.authority.claims.VehicleStatusAuthorityResponseV1;
import com.doing.nemo.claims.controller.payload.response.authority.practice.AuthorityPracticeAssociationListResponseV1;
import com.doing.nemo.claims.controller.payload.response.authority.practice.AuthorityPracticePaginationResponseV1;
import com.doing.nemo.claims.controller.payload.response.authority.practice.WorkingAdministrationResponseV1;
import com.doing.nemo.claims.controller.payload.response.authority.repair.AuthorityPaginationRepairResponseV1;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.CounterpartyEntity;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.PaginationAuthority;
import com.doing.nemo.claims.entity.enumerated.AuthorityEnum.AdministrativePracticeTypeEnum;
import com.doing.nemo.claims.entity.enumerated.AuthorityEnum.AuthorityPracticeTypeEnum;
import com.doing.nemo.claims.entity.enumerated.WorkingStatusEnum;
import com.doing.nemo.claims.entity.jsonb.Authority;
import com.doing.nemo.claims.entity.settings.PracticeEntity;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.UUID;


@Service
public interface AuthorityService {
    //REFACTOR
    void linkUnlinkAuthority(AuthorityRequestV1 authorityRequestV1);
    //List<ClaimsResponseIdV1> checkClaims(String idClaims);
    //REFACTOR
    Map<String,String> associateClaims(AuthorityTotalRequestV1 authorityTotalRequestV1);
    Pagination<AuthorityPaginationResponseV1> paginationAuthorityResponseListFirstCheck(int page, int pageSize, String orderBy, Boolean asc, List<String> plates, String chassis);
    Pagination<AuthorityPaginationResponseV1> paginationAuthorityResponseListSecondCheck(int page, int pageSize, String orderBy, Boolean asc, List<String> plates, String chassis);
    List<AuthorityOldestResponseV1> getOldestClaimsByDossierAndWorking(String dossierId, String workingId);
    //REFACTOR
    AuthorityClaimsResponseV1 getAuthorityPracticeByWorkingId(String idClaims, String workingId) throws IOException;
    VehicleStatusAuthorityResponseV1 getVehicleStatus(String chassis, String dateAccident) throws IOException;
    //void resetAuthority();
    //REFACTOR
    List<ClaimsEntity> getClaimsToInvoce() throws IOException;
    //REFACTOR
    void getSingleClaimToInvoice(String idClaims, String workingId) throws IOException;

    //REFACTOR
    List<CounterpartyEntity> getCounterpartyByPlate(String plate);
    //REFACTOR
    Pagination<AuthorityPaginationRepairResponseV1> paginationRepairAuthorityResponseList(int page, int pageSize, String orderBy, Boolean asc, String plate, String chassis);
    //REFACTOR
    void linkUnlinkAuthorityRepair(AuthorityRepairRequestV1 authorityRepairRequestV1);
    //REFACTOR
    void associateCounterparty(AuthorityRepairTotalRequestV1 authorityTotalRequestV1);
    void setAuthorityRepairStatus(String dossier, String working, AuthorityRepairStatusRequestV1 requestV1, String userId, String firstname, String lastname, String profilesMap) throws IOException;
    Authority addHistoricalRepairClosedStatus(Authority currentAuthority, WorkingStatusEnum workingStatusEnum);
    Pagination<AuthorityPracticePaginationResponseV1> paginationPracticeAuthorityResponseList(int page, int pageSize, String plate, String chassis, List<AuthorityPracticeTypeEnum> type);
    AuthorityPracticeAssociationListResponseV1 linkUnlinkAuthorityPractice(AuthorityPracticeRequestV1 authorityRequestV1);
    void unlinkPracticeToAuthority( AuthorityPracticeRequestV1 authorityRequestV1, List<String> practiceListID);
    void associatePractice(AuthorityPracticeUpdateRequestV1 authorityPracticeUpdateRequestV1);
    PaginationResponseV1<WorkingAdministrationResponseV1> paginationPracticeAdministrative(int page, int pageSize, String orderBy, Boolean asc, String plateOrChassis, AdministrativePracticeTypeEnum type, String nemoUserId, String nemoProfileTree) throws IOException;
    PracticeEntity associateProcessingAdministrative(UUID uuid, List<ProcessingRequestV1> processingRequestV1s) throws IOException;
    PracticeEntity disassociateProcessingAdministrative(UUID uuid, List<ProcessingRequestV1> processingRequestV1s) throws IOException;
}


