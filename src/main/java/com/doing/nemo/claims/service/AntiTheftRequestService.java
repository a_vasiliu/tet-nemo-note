package com.doing.nemo.claims.service;


import com.doing.nemo.claims.crashservices.texa.org.datacontract.schemas._2004._07.texa_tmd_bol_m2mtrack_ald_webservice_service.GetVoucherInfoResponse;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.jsonb.forms.jsonbForms.Attachment;
import com.doing.nemo.claims.entity.ClaimsNewEntity;
import org.springframework.stereotype.Service;

@Service
public interface AntiTheftRequestService {

    void insertAntiTheftRequest(String idAntiTheftRequest, ClaimsNewEntity claimsEntity, String providerType, String idTransaction);
    void updateAntiTheftRequest(String idAntiTheftRequest, GetVoucherInfoResponse responseTexa, com.doing.nemo.claims.crashservices.octo.GetVoucherInfoResponse responseOCTO, String responseType, Attachment pdf, String responseMessage );
}
