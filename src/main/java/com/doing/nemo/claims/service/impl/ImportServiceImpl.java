package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.adapter.ClaimsAdapter;
import com.doing.nemo.claims.adapter.DataManagementImportAdapter;
import com.doing.nemo.claims.adapter.IncidentAdapter;
import com.doing.nemo.claims.adapter.MessagingAdapter;
import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.EmailTemplateMessagingRequestV1;
import com.doing.nemo.claims.controller.payload.request.IncidentRequestV1;
import com.doing.nemo.claims.controller.payload.request.UploadFileRequestV1;
import com.doing.nemo.claims.controller.payload.response.DataManagementImportResponseV1;
import com.doing.nemo.claims.controller.payload.response.DownloadFileBase64ResponseV1;
import com.doing.nemo.claims.controller.payload.response.EmailTemplateMessagingResponseV1;
import com.doing.nemo.claims.controller.payload.response.UploadFileResponseV1;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.ClaimsNewEntity;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsFlowEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsHistoricalUserEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.EntrustedEnum.EntrustedEnum;
import com.doing.nemo.claims.entity.enumerated.DamagedEnum.VehicleEnum.VehicleNatureEnum;
import com.doing.nemo.claims.entity.enumerated.EventTypeEnum;
import com.doing.nemo.claims.entity.enumerated.FileImportTypeEnum;
import com.doing.nemo.claims.entity.enumerated.LawyerPaymentsStatusEnum;
import com.doing.nemo.claims.entity.enumerated.PolicyEnum.PolicySuffixEnum;
import com.doing.nemo.claims.entity.enumerated.RefundEnum.RefundTypeEnum;
import com.doing.nemo.claims.entity.enumerated.RefundEnum.TypeEnum;
import com.doing.nemo.claims.entity.jsonb.Exemption;
import com.doing.nemo.claims.entity.jsonb.Historical;
import com.doing.nemo.claims.entity.jsonb.complaint.Complaint;
import com.doing.nemo.claims.entity.jsonb.complaint.jsonbComplaint.Entrusted;
import com.doing.nemo.claims.entity.jsonb.complaint.jsonbComplaint.FromCompany;
import com.doing.nemo.claims.entity.jsonb.damaged.Damaged;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.InsuranceCompany;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbInsuranceCompany.Tpl;
import com.doing.nemo.claims.entity.settings.*;
import com.doing.nemo.claims.repository.*;
import com.doing.nemo.claims.service.*;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.BadParametersException;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.exception.InternalException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.rmi.ConnectException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.*;

@Transactional
@Service
public class ImportServiceImpl implements ImportService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ImportServiceImpl.class);
    private static final String UTF8_BOM = "\uFEFF";
    @Autowired
    private UploadFileService uploadFileService;
    @Autowired
    private FileManagerService fileManagerService;
    /*@Autowired
    private ClaimsRepository claimsRepository;*/
    @Autowired
    private ClaimsNewRepository claimsNewRepository;
    @Autowired
    private ConverterClaimsService converterClaimsService;
    /*@Autowired
    private ClaimsRepositoryV1 claimsRepositoryV1;*/
    @Autowired
    private DataManagementImportRepository dataManagementImportRepository;
    @Autowired
    private ContractTypeRepository contractTypeRepository;
    @Autowired
    private LegalRepository legalRepository;
    @Autowired
    private InspectorateRepository inspectorateRepository;
    @Autowired
    private InsuranceManagerRepository insuranceManagerRepository;
    @Autowired
    private InsurancePolicyRepository insurancePolicyRepository;
    @Autowired
    private LawyerPaymentsRepository lawyerPaymentsRepository;
    @Autowired
    private MessagingService messagingService;
    @Autowired
    private IncidentService incidentService;
    @Autowired
    private IncidentAdapter incidentAdapter;

    @Autowired
    private DwhClaimsService dwhClaimsService;

    @Autowired
    private ExternalCommunicationService externalCommunicationService;

    @Value("${time.zone}")
    private String timeZone;

    @Value("${dwh.call}")
    private Boolean dwhCall;

    private static String removeUTF8BOM(String s) {
        if (s.startsWith(UTF8_BOM)) {
            s = s.substring(1);
        }
        return s;
    }

    /**
     * ------------------------------ CREAZIONE INTESTAZIONE DEI FILE DI SCARTO ------------------------------
     **/

    /*FRANCHIGIA 1*/
    private static String creationDeduction1WasteFile() {

        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("SX Fleet");
        stringBuilder.append(';');
        stringBuilder.append("Data del Sx.");
        stringBuilder.append(';');
        stringBuilder.append("Targa Veicolo Assicurato");
        stringBuilder.append(';');
        stringBuilder.append("Sx. Dcs Italia");
        stringBuilder.append(';');
        stringBuilder.append("Tipologia di Sinistro");
        stringBuilder.append(';');
        stringBuilder.append("Data di Chiusura");
        stringBuilder.append(';');
        stringBuilder.append("Importo FF Pagamento Sx.");
        stringBuilder.append(';');
        stringBuilder.append("Totale Pagato");
        stringBuilder.append(';');
        stringBuilder.append("Riaperture");
        stringBuilder.append(';');
        stringBuilder.append("Nr. Polizza");
        stringBuilder.append(';');
        stringBuilder.append("Suffisso Polizza");
        stringBuilder.append(';');
        stringBuilder.append("Note Marsh");
        stringBuilder.append('\n');

        return stringBuilder.toString();

    }

    /*FRANCHIGIA 2*/
    private static String creationDeduction2WasteFile() {

        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("SX Fleet");
        stringBuilder.append(';');
        stringBuilder.append("Data del Sx.");
        stringBuilder.append(';');
        stringBuilder.append("Targa Veicolo Assicurato");
        stringBuilder.append(';');
        stringBuilder.append("Sx. Dcs Italia");
        stringBuilder.append(';');
        stringBuilder.append("Tipologia di Sinistro");
        stringBuilder.append(';');
        stringBuilder.append("Data di Chiusura");
        stringBuilder.append(';');
        stringBuilder.append("Importo Extra FF Pagamento Sx");
        stringBuilder.append(';');
        stringBuilder.append("Importo FF Pagamento Sx.");
        stringBuilder.append(';');
        stringBuilder.append("Importo FF precedente fatturato Pagamento Sx");
        stringBuilder.append(';');
        stringBuilder.append("Totale Pagato");
        stringBuilder.append(';');
        stringBuilder.append("Riaperture");
        stringBuilder.append(';');
        stringBuilder.append("Nr. Polizza");
        stringBuilder.append(';');
        stringBuilder.append("Suffisso Polizza");
        stringBuilder.append(';');
        stringBuilder.append("Note Marsh");
        stringBuilder.append('\n');

        return stringBuilder.toString();

    }

    /*NUMERO SX*/
    private static String creationNumberSxWasteFile() {

        StringBuilder sb = new StringBuilder();

        sb.append("Rif Axus-Ald");
        sb.append(';');
        sb.append("Rif Compagnia");
        sb.append(';');
        sb.append("Numero Polizza");
        sb.append(';');
        sb.append("Data Sinistro");
        sb.append(';');
        sb.append("Tipo Danno");
        sb.append(';');
        sb.append("Targa  Assicurato");
        sb.append(';');
        sb.append("Ispettorato competente");
        sb.append(';');
        sb.append("Perito incaricato");
        sb.append(';');
        sb.append("Note Marsh");
        sb.append('\n');

        return sb.toString();

    }

    /*AFFIDO MASSIVO*/
    private static String creationMassiveEntrustWasteFile() {

        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("SX Fleet");
        stringBuilder.append(';');
        stringBuilder.append("Data del Sx.");
        stringBuilder.append(';');
        stringBuilder.append("Targa Veicolo Assicurato");
        stringBuilder.append(';');
        stringBuilder.append("Sx. Dcs Italia");
        stringBuilder.append(';');
        stringBuilder.append("Tipologia Affidatario");
        stringBuilder.append(';');
        stringBuilder.append("Cod. Affidatario");
        stringBuilder.append(';');
        stringBuilder.append("Note");
        stringBuilder.append('\n');

        return stringBuilder.toString();

    }

    /*PAGAMENTI LEGALE*/
    private static String creationLawyerPaymentsWasteFile(FileImportTypeEnum fileImportType) {

        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("n°");
        stringBuilder.append(';');
        stringBuilder.append("TARGA");
        stringBuilder.append(';');
        stringBuilder.append("IDENTIFICATIVO SINISTRO");
        stringBuilder.append(';');
        stringBuilder.append("DATA SINISTRO");
        stringBuilder.append(';');
        stringBuilder.append("DANNO");
        stringBuilder.append(';');
        stringBuilder.append("FERMO TECNICO");
        stringBuilder.append(';');
        stringBuilder.append("ONORARI");
        stringBuilder.append(';');
        stringBuilder.append("Spesa causa");
        stringBuilder.append(';');
        stringBuilder.append("ACC/SALDO");
        stringBuilder.append(';');

        if (fileImportType.equals(FileImportTypeEnum.ALLOWANCE)) {

            stringBuilder.append("IMPORTO ASSEGNO");
            stringBuilder.append(';');
            stringBuilder.append("NUMERO ASSEGNO");
            stringBuilder.append(';');

        } else {

            stringBuilder.append("IMPORTO BONIFICO");
            stringBuilder.append(';');
            stringBuilder.append("NUMERO BONIFICO");
            stringBuilder.append(';');
        }

        stringBuilder.append("BANCA");
        stringBuilder.append(';');
        stringBuilder.append("NOTE");
        stringBuilder.append(';');
        stringBuilder.append("Codice Avvocato");
        stringBuilder.append(';');
        stringBuilder.append("NOTE SCARTO");
        stringBuilder.append('\n');

        return stringBuilder.toString();

    }

    /**
     * ------------------------------ METODI DI INSERIMENTO NEL FILE DI SCARTO ------------------------------
     **/

    /*FRANCHIGIA 1*/
    private String insertInDeduction1WasteFile(WasteFileDeduction1Attribute element, String stringaScarto) {

        if (stringaScarto == null)
            stringaScarto = creationDeduction1WasteFile();

        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(element.getSxFleet());
        stringBuilder.append(';');
        stringBuilder.append(element.getDataDelSx());
        stringBuilder.append(';');
        stringBuilder.append(element.getTargaVeicoloAssicurato());
        stringBuilder.append(';');
        stringBuilder.append(element.getSxDcsItalia());
        stringBuilder.append(';');
        stringBuilder.append(element.getTipologiaDelSinistro());
        stringBuilder.append(';');
        stringBuilder.append(element.getDataDiChiusura());
        stringBuilder.append(';');
        stringBuilder.append(element.getImportoFFPagamentoSx());
        stringBuilder.append(';');
        stringBuilder.append(element.getTotalePagato());
        stringBuilder.append(';');
        stringBuilder.append(element.getRiaperture());
        stringBuilder.append(';');
        stringBuilder.append(element.getNrPolizza());
        stringBuilder.append(';');
        stringBuilder.append(element.getSuffisoPolizza());
        stringBuilder.append(';');
        stringBuilder.append("RESTITUITO: " + element.getNote());
        stringBuilder.append('\n');

        return stringaScarto + stringBuilder.toString();
    }

    /*FRANCHIGIA 2*/
    private String insertInDeduction2WasteFile(WasteFileDeduction2Attribute element, String stringaScarto) {

        if (stringaScarto == null)
            stringaScarto = creationDeduction2WasteFile();

        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(element.getSxFleet());
        stringBuilder.append(';');
        stringBuilder.append(element.getDataDelSx());
        stringBuilder.append(';');
        stringBuilder.append(element.getTargaVeicoloAssicurato());
        stringBuilder.append(';');
        stringBuilder.append(element.getSxDcsItalia());
        stringBuilder.append(';');
        stringBuilder.append(element.getTipologiaDelSinistro());
        stringBuilder.append(';');
        stringBuilder.append(element.getDataDiChiusura());
        stringBuilder.append(';');
        stringBuilder.append(element.getImportoExtraFFPagamentoSx());
        stringBuilder.append(';');
        stringBuilder.append(element.getImportoFFPagamentoSx());
        stringBuilder.append(';');
        stringBuilder.append(element.getImportoFFPrecedenteFatturatoPagamentoSx());
        stringBuilder.append(';');
        stringBuilder.append(element.getTotalePagato());
        stringBuilder.append(';');
        stringBuilder.append(element.getRiaperture());
        stringBuilder.append(';');
        stringBuilder.append(element.getNrPolizza());
        stringBuilder.append(';');
        stringBuilder.append(element.getSuffisoPolizza());
        stringBuilder.append(';');
        stringBuilder.append("RESTITUITO: " + element.getNote());
        stringBuilder.append('\n');

        return stringaScarto + stringBuilder.toString();
    }

    /*NUMERO SX*/
    private String insertInNumberSxWasteFile(WasteFileNumberSxAttribute element, String stringaScarto) {

        if (stringaScarto == null)
            stringaScarto = creationNumberSxWasteFile();

        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(element.getRifAxusAld());
        stringBuilder.append(';');
        stringBuilder.append(element.getRifCompagnia());
        stringBuilder.append(';');
        stringBuilder.append(element.getNumeroPolizza());
        stringBuilder.append(';');
        stringBuilder.append(element.getDataSinistro());
        stringBuilder.append(';');
        stringBuilder.append(element.getTipoDanno());
        stringBuilder.append(';');
        stringBuilder.append(element.getTargaAssicurato());
        stringBuilder.append(';');
        stringBuilder.append(element.getIspettorato());
        stringBuilder.append(';');
        stringBuilder.append(element.getPeritoIncaricato());
        stringBuilder.append(';');
        stringBuilder.append("RESTITUITO: " + element.getNote());
        stringBuilder.append('\n');

        return stringaScarto + stringBuilder.toString();
    }

    /*AFFIDO MASSIVO*/
    private String insertInMassiveEntrustWasteFile(WasteFileMassiveEntrustAttribute element, String stringaScarto) {

        if (stringaScarto == null)
            stringaScarto = creationMassiveEntrustWasteFile();

        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(element.getSxFleet());
        stringBuilder.append(';');
        stringBuilder.append(element.getDataDelSx());
        stringBuilder.append(';');
        stringBuilder.append(element.getTargaVeicoloAssicurato());
        stringBuilder.append(';');
        stringBuilder.append(element.getTipologiaAffidatario());
        stringBuilder.append(';');
        stringBuilder.append(element.getCodAffidatario());
        stringBuilder.append(';');
        stringBuilder.append("RESTITUITO: " + element.getNote());
        stringBuilder.append('\n');

        return stringaScarto + stringBuilder.toString();
    }

    /*PAGAMENTI LEGALE*/
    private String insertInLawyerPaymentsWasteFile(WasteFileLawyerPaymentsAttribute element, String stringaScarto, FileImportTypeEnum fileImportType) {

        if (stringaScarto == null)
            stringaScarto = creationLawyerPaymentsWasteFile(fileImportType);

        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(element.getNum());
        stringBuilder.append(';');
        stringBuilder.append(element.getTarga());
        stringBuilder.append(';');
        stringBuilder.append(element.getIdentificativoSinistro());
        stringBuilder.append(';');
        stringBuilder.append(element.getDataSinistro());
        stringBuilder.append(';');
        stringBuilder.append(element.getDanno());
        stringBuilder.append(';');
        stringBuilder.append(element.getFermoTecnico());
        stringBuilder.append(';');
        stringBuilder.append(element.getOnorari());
        stringBuilder.append(';');
        stringBuilder.append(element.getSpeseCausa());
        stringBuilder.append(';');
        stringBuilder.append(element.getAccSaldo());
        stringBuilder.append(';');
        stringBuilder.append(element.getImporto());
        stringBuilder.append(';');
        stringBuilder.append(element.getNumeroTransazione());
        stringBuilder.append(';');
        stringBuilder.append(element.getBanca());
        stringBuilder.append(';');
        stringBuilder.append(element.getNote());
        stringBuilder.append(';');
        stringBuilder.append(element.getCodiceAvvocato());
        stringBuilder.append(';');
        stringBuilder.append("RESTITUITO: " + element.getNoteScarto());
        stringBuilder.append('\n');

        return stringaScarto + stringBuilder.toString();
    }

    /**
     * ----------------- METODI PER CONTROLLARE LA CONFORMITA' DELLE INTESTAZIONI DEI FILE IMPORTATI -----------------
     **/

    /*FRANCHIGIA 1*/
    private String checkDeduction1Attributes(List<String> attributeColumn) {

        String error = "";

        if (attributeColumn.size() != 11) {
            return "PER LA FRANCHIGIA 1 IL NUMERO DEGLI ATTRIBUTI E' DIVERSO DA 11";
        }
        if (!(this.removeUTF8BOM(attributeColumn.get(0)).trim()).equalsIgnoreCase("SX Fleet")) {
            error += attributeColumn.get(0) + ", ";
        }
        if (!(this.removeUTF8BOM(attributeColumn.get(1)).trim()).equalsIgnoreCase("Data del Sx.")) {
            error += attributeColumn.get(1) + ", ";
        }
        if (!(this.removeUTF8BOM(attributeColumn.get(2)).trim()).equalsIgnoreCase("Targa Veicolo Assicurato")) {
            error += attributeColumn.get(2) + ", ";
        }
        if (!(this.removeUTF8BOM(attributeColumn.get(3)).trim()).equalsIgnoreCase("Sx. Dcs Italia")) {
            error += attributeColumn.get(3) + ", ";
        }
        if (!(this.removeUTF8BOM(attributeColumn.get(4)).trim()).equalsIgnoreCase("Tipologia di Sinistro")) {
            error += attributeColumn.get(4) + ", ";
        }
        if (!(this.removeUTF8BOM(attributeColumn.get(5)).trim()).equalsIgnoreCase("Data di Chiusura")) {
            error += attributeColumn.get(5) + ", ";
        }
        if (!(this.removeUTF8BOM(attributeColumn.get(6)).trim()).equalsIgnoreCase("Importo FF Pagamento Sx.")) {
            error += attributeColumn.get(6) + ", ";
        }
        if (!(this.removeUTF8BOM(attributeColumn.get(7)).trim()).equalsIgnoreCase("Totale Pagato")) {
            error += attributeColumn.get(7) + ", ";
        }
        if (!(this.removeUTF8BOM(attributeColumn.get(8)).trim()).equalsIgnoreCase("Riaperture")) {
            error += attributeColumn.get(8) + ", ";
        }
        if (!(this.removeUTF8BOM(attributeColumn.get(9)).trim()).equalsIgnoreCase("Nr. Polizza")) {
            error += attributeColumn.get(9) + ", ";
        }
        if (!(this.removeUTF8BOM(attributeColumn.get(10)).trim()).equalsIgnoreCase("Suffisso Polizza")) {
            error += attributeColumn.get(10) + ", ";
        }

        if (!error.isEmpty())
            error += " NOME DEI CAMPI DIVERSO DA QUELLO DEFINITO ";

        return error;
    }

    /*FRANCHIGIA 2*/
    private String checkDeduction2Attributes(List<String> attributeColumn) {

        String error = "";

        if (attributeColumn.size() != 13) {
            return "PER LA FRANCHIGIA 2 IL NUMERO DEGLI ATTRIBUTI E' DIVERSO DA 13";
        }

        if (!(this.removeUTF8BOM(attributeColumn.get(0)).trim()).equalsIgnoreCase("SX Fleet")) {
            error += attributeColumn.get(0) + ", ";
        }
        if (!(this.removeUTF8BOM(attributeColumn.get(1)).trim()).equalsIgnoreCase("Data del Sx.")) {
            error += attributeColumn.get(1) + ", ";
        }
        if (!(this.removeUTF8BOM(attributeColumn.get(2)).trim()).equalsIgnoreCase("Targa Veicolo Assicurato")) {
            error += attributeColumn.get(2) + ", ";
        }
        if (!(this.removeUTF8BOM(attributeColumn.get(3)).trim()).equalsIgnoreCase("Sx. Dcs Italia")) {
            error += attributeColumn.get(3) + ", ";
        }
        if (!(this.removeUTF8BOM(attributeColumn.get(4)).trim()).equalsIgnoreCase("Tipologia di Sinistro")) {
            error += attributeColumn.get(4) + ", ";
        }
        if (!(this.removeUTF8BOM(attributeColumn.get(5)).trim()).equalsIgnoreCase("Data di Chiusura")) {
            error += attributeColumn.get(5) + ", ";
        }
        if (!(this.removeUTF8BOM(attributeColumn.get(6)).trim()).equalsIgnoreCase("Importo Extra FF Pagamento Sx")) {
            error += attributeColumn.get(6) + ", ";
        }
        if (!(this.removeUTF8BOM(attributeColumn.get(7)).trim()).equalsIgnoreCase("Importo FF Pagamento Sx.")) {
            error += attributeColumn.get(7) + ", ";
        }
        if (!(this.removeUTF8BOM(attributeColumn.get(8)).trim()).equalsIgnoreCase("Importo FF precedente fatturato Pagamento Sx")) {
            error += attributeColumn.get(8) + ", ";
        }
        if (!(this.removeUTF8BOM(attributeColumn.get(9)).trim()).equalsIgnoreCase("Totale Pagato")) {
            error += attributeColumn.get(9) + ", ";
        }
        if (!(this.removeUTF8BOM(attributeColumn.get(10)).trim()).equalsIgnoreCase("Riaperture")) {
            error += attributeColumn.get(10) + ", ";
        }
        if (!(this.removeUTF8BOM(attributeColumn.get(11)).trim()).equalsIgnoreCase("Nr. Polizza")) {
            error += attributeColumn.get(11) + ", ";
        }
        if (!(this.removeUTF8BOM(attributeColumn.get(12)).trim()).equalsIgnoreCase("Suffisso Polizza")) {
            error += attributeColumn.get(12) + ", ";
        }

        if (!error.isEmpty())
            error += " NOME DEI CAMPI DIVERSO DA QUELLO DEFINITO ";

        return error;
    }

    /*NUMERO SX*/
    private String checkNumberSxAttributes(List<String> attributeColumn) {

        String error = "";

        if (attributeColumn.size() != 9) {
            return "PER NUMERO SX IL NUMERO DEGLI ATTRIBUTI E' DIVERSO DA 9";
        }

        if (!(this.removeUTF8BOM(attributeColumn.get(0)).trim()).equalsIgnoreCase("Rif Axus-Ald")) {
            error += attributeColumn.get(0) + ", ";
        }
        if (!(this.removeUTF8BOM(attributeColumn.get(1)).trim()).equalsIgnoreCase("Rif Compagnia")) {
            error += attributeColumn.get(1) + ", ";
        }
        if (!(this.removeUTF8BOM(attributeColumn.get(2)).trim()).equalsIgnoreCase("Numero Polizza")) {
            error += attributeColumn.get(2) + ", ";
        }
        if (!(this.removeUTF8BOM(attributeColumn.get(3)).trim()).equalsIgnoreCase("Data Sinistro")) {
            error += attributeColumn.get(3) + ", ";
        }
        if (!(this.removeUTF8BOM(attributeColumn.get(4)).trim()).equalsIgnoreCase("Tipo Danno")) {
            error += attributeColumn.get(4) + ", ";
        }
        if (!(this.removeUTF8BOM(attributeColumn.get(5)).trim()).equalsIgnoreCase("Targa  Assicurato")) {
            error += attributeColumn.get(5) + ", ";
        }
        if (!(this.removeUTF8BOM(attributeColumn.get(6)).trim()).equalsIgnoreCase("Ispettorato competente")) {
            error += attributeColumn.get(6) + ", ";
        }
        if (!(this.removeUTF8BOM(attributeColumn.get(7)).trim()).equalsIgnoreCase("Perito incaricato")) {
            error += attributeColumn.get(7) + ", ";
        }
        if (!(this.removeUTF8BOM(attributeColumn.get(8)).trim()).equalsIgnoreCase("Note Marsh")) {
            error += attributeColumn.get(8) + ", ";
        }

        if (!error.isEmpty())
            error += " NOME DEI CAMPI DIVERSO DA QUELLO DEFINITO ";

        return error;
    }

    /*FURTO*/
    private String checkTefthAttributes(List<String> attributeColumn) {

        String error = "";

        if (attributeColumn.size() != 9) {
            return "PER FURTO IL NUMERO DEGLI ATTRIBUTI E' DIVERSO DA 9";
        }

        if (!(this.removeUTF8BOM(attributeColumn.get(0)).trim()).equalsIgnoreCase("EXTARGA_DANNEGGIATO"))
            error += attributeColumn.get(0) + ", ";

        if (!(this.removeUTF8BOM(attributeColumn.get(1)).trim()).equalsIgnoreCase("CLIENTE_ID"))
            error += attributeColumn.get(1) + ", ";

        if (!(this.removeUTF8BOM(attributeColumn.get(2)).trim()).equalsIgnoreCase("CLIENTE_NOME"))
            error += attributeColumn.get(2) + ", ";

        if (!(this.removeUTF8BOM(attributeColumn.get(3)).trim()).equalsIgnoreCase("TIPOSX_ID"))
            error += attributeColumn.get(3) + ", ";

        if (!(this.removeUTF8BOM(attributeColumn.get(4)).trim()).equalsIgnoreCase("TIPOSX_DES"))
            error += attributeColumn.get(4) + ", ";

        if (!(this.removeUTF8BOM(attributeColumn.get(5)).trim()).equalsIgnoreCase("DATASX"))
            error += attributeColumn.get(5) + ", ";

        if (!(this.removeUTF8BOM(attributeColumn.get(6)).trim()).equalsIgnoreCase("Chiudere al"))
            error += attributeColumn.get(6) + ", ";

        if (!(this.removeUTF8BOM(attributeColumn.get(7)).trim()).equalsIgnoreCase("Moto_Auto"))
            error += attributeColumn.get(7) + ", ";

        if (!(this.removeUTF8BOM(attributeColumn.get(8)).trim()).equalsIgnoreCase("Contract_ID"))
            error += attributeColumn.get(8) + ", ";

        if (!error.isEmpty())
            error = "Gli attributi: " + error + " non corrispondono alla formattazione prevista.";

        return error;
    }

    /*PRATICHE AUTO*/
    private String checkPracticeCarAttributes(List<String> attributeColumn) {

        String error = "";

        if (attributeColumn.size() != 9) {
            return "PER PRATICHE IL NUMERO DEGLI ATTRIBUTI E' DIVERSO DA 9";
        }

        if (!(this.removeUTF8BOM(attributeColumn.get(0)).trim()).equalsIgnoreCase("EXTARGA_DANNEGGIATO"))
            error += attributeColumn.get(0) + ", ";

        if (!(this.removeUTF8BOM(attributeColumn.get(1)).trim()).equalsIgnoreCase("CLIENTE_ID"))
            error += attributeColumn.get(1) + ", ";

        if (!(this.removeUTF8BOM(attributeColumn.get(2)).trim()).equalsIgnoreCase("CLIENTE_NOME"))
            error += attributeColumn.get(2) + ", ";

        if (!(this.removeUTF8BOM(attributeColumn.get(3)).trim()).equalsIgnoreCase("TIPOSX_ID"))
            error += attributeColumn.get(3) + ", ";

        if (!(this.removeUTF8BOM(attributeColumn.get(4)).trim()).equalsIgnoreCase("TIPOSX_DES"))
            error += attributeColumn.get(4) + ", ";

        if (!(this.removeUTF8BOM(attributeColumn.get(5)).trim()).equalsIgnoreCase("DATASX"))
            error += attributeColumn.get(5) + ", ";

        if (!(this.removeUTF8BOM(attributeColumn.get(6)).trim()).equalsIgnoreCase("Chiudere al"))
            error += attributeColumn.get(6) + ", ";

        if (!(this.removeUTF8BOM(attributeColumn.get(7)).trim()).equalsIgnoreCase("Moto_Auto"))
            error += attributeColumn.get(7) + ", ";

        if (!(this.removeUTF8BOM(attributeColumn.get(8)).trim()).equalsIgnoreCase("Contract_ID"))
            error += attributeColumn.get(8) + ", ";

        if (!error.isEmpty())
            error = "Gli attributi: " + error + " non corrispondono alla formattazione prevista.";

        return error;
    }

    /*SEQUESTRI*/
    private String checkAppropriationAttributes(List<String> attributeColumn) {

        String error = "";

        if (attributeColumn.size() != 9) {
            return "PER SEQUESTRI IL NUMERO DEGLI ATTRIBUTI E' DIVERSO DA 9";
        }

        if (!(this.removeUTF8BOM(attributeColumn.get(0)).trim()).equalsIgnoreCase("EXTARGA_DANNEGGIATO"))
            error += attributeColumn.get(0) + ", ";

        if (!(this.removeUTF8BOM(attributeColumn.get(1)).trim()).equalsIgnoreCase("CLIENTE_ID"))
            error += attributeColumn.get(1) + ", ";

        if (!(this.removeUTF8BOM(attributeColumn.get(2)).trim()).equalsIgnoreCase("CLIENTE_NOME"))
            error += attributeColumn.get(2) + ", ";

        if (!(this.removeUTF8BOM(attributeColumn.get(3)).trim()).equalsIgnoreCase("TIPOSX_ID"))
            error += attributeColumn.get(3) + ", ";

        if (!(this.removeUTF8BOM(attributeColumn.get(4)).trim()).equalsIgnoreCase("TIPOSX_DES"))
            error += attributeColumn.get(4) + ", ";

        if (!(this.removeUTF8BOM(attributeColumn.get(5)).trim()).equalsIgnoreCase("DATASX"))
            error += attributeColumn.get(5) + ", ";

        if (!(this.removeUTF8BOM(attributeColumn.get(6)).trim()).equalsIgnoreCase("Chiudere al"))
            error += attributeColumn.get(6) + ", ";

        if (!(this.removeUTF8BOM(attributeColumn.get(7)).trim()).equalsIgnoreCase("Moto_Auto"))
            error += attributeColumn.get(7) + ", ";

        if (!(this.removeUTF8BOM(attributeColumn.get(8)).trim()).equalsIgnoreCase("Contract_ID"))
            error += attributeColumn.get(8) + ", ";

        if (!error.isEmpty())
            error = "Gli attributi: " + error + " non corrispondono alla formattazione prevista.";

        return error;
    }

    /*AFFIDO MASSIVO*/
    private String checkMassiveEntrustAttributes(List<String> attributeColumn) {

        String error = "";

        if (attributeColumn.size() != 5) {
            return "PER L'AFFIDO MASSIVO IL NUMERO DEGLI ATTRIBUTI E' DIVERSO DA 5";
        }
        if (!(this.removeUTF8BOM(attributeColumn.get(0)).trim()).equalsIgnoreCase("SX Fleet")) {
            error += attributeColumn.get(0) + ", ";
        }
        if (!(this.removeUTF8BOM(attributeColumn.get(1)).trim()).equalsIgnoreCase("Data del Sx.")) {
            error += attributeColumn.get(1) + ", ";
        }
        if (!(this.removeUTF8BOM(attributeColumn.get(2)).trim()).equalsIgnoreCase("Targa Veicolo Assicurato")) {
            error += attributeColumn.get(2) + ", ";
        }
        if (!(this.removeUTF8BOM(attributeColumn.get(3)).trim()).equalsIgnoreCase("Tipologia Affidatario")) {
            error += attributeColumn.get(3) + ", ";
        }
        if (!(this.removeUTF8BOM(attributeColumn.get(4)).trim()).equalsIgnoreCase("Cod. Affidatario")) {
            error += attributeColumn.get(4) + ", ";
        }

        if (!error.isEmpty())
            error += " NOME DEI CAMPI DIVERSO DA QUELLO DEFINITO ";

        return error;

    }

    /*PAGAMENTI LEGALE*/
    private String checkLawyerPaymentsAttributes(List<String> attributeColumn, FileImportTypeEnum fileImportType) {

        String error = "";

        if (attributeColumn.size() != 14) {
            return "PER PAGAMENTI LEGALE IL NUMERO DEGLI ATTRIBUTI E' DIVERSO DA 14";
        }
        if (!(this.removeUTF8BOM(attributeColumn.get(0)).trim()).equalsIgnoreCase("n°")) {
            error += attributeColumn.get(0) + ", ";
        }
        if (!(this.removeUTF8BOM(attributeColumn.get(1)).trim()).equalsIgnoreCase("TARGA")) {
            error += attributeColumn.get(1) + ", ";
        }
        if (!(this.removeUTF8BOM(attributeColumn.get(2)).trim()).equalsIgnoreCase("IDENTIFICATIVO SINISTRO")) {
            error += attributeColumn.get(2) + ", ";
        }
        if (!(this.removeUTF8BOM(attributeColumn.get(3)).trim()).equalsIgnoreCase("DATA SINISTRO")) {
            error += attributeColumn.get(3) + ", ";
        }
        if (!(this.removeUTF8BOM(attributeColumn.get(4)).trim()).equalsIgnoreCase("DANNO")) {
            error += attributeColumn.get(4) + ", ";
        }
        if (!(this.removeUTF8BOM(attributeColumn.get(5)).trim()).equalsIgnoreCase("FERMO TECNICO")) {
            error += attributeColumn.get(5) + ", ";
        }
        if (!(this.removeUTF8BOM(attributeColumn.get(6)).trim()).equalsIgnoreCase("ONORARI")) {
            error += attributeColumn.get(6) + ", ";
        }
        if (!(this.removeUTF8BOM(attributeColumn.get(7)).trim()).equalsIgnoreCase("Spese causa")) {
            error += attributeColumn.get(7) + ", ";
        }
        if (!(this.removeUTF8BOM(attributeColumn.get(8)).trim()).equalsIgnoreCase("ACC/SALDO")) {
            error += attributeColumn.get(8) + ", ";
        }

        if (fileImportType.equals(FileImportTypeEnum.ALLOWANCE)) {

            if (!(this.removeUTF8BOM(attributeColumn.get(9)).trim()).equalsIgnoreCase("IMPORTO ASSEGNO")) {
                error += attributeColumn.get(9) + ", ";
            }
            if (!(this.removeUTF8BOM(attributeColumn.get(10)).trim()).equalsIgnoreCase("NUMERO ASSEGNO")) {
                error += attributeColumn.get(10) + ", ";
            }
        } else {

            if (!(this.removeUTF8BOM(attributeColumn.get(9)).trim()).equalsIgnoreCase("IMPORTO BONIFICO")) {
                error += attributeColumn.get(9) + ", ";
            }
            if (!(this.removeUTF8BOM(attributeColumn.get(10)).trim()).equalsIgnoreCase("NUMERO BONIFICO")) {
                error += attributeColumn.get(10) + ", ";
            }
        }

        if (!(this.removeUTF8BOM(attributeColumn.get(11)).trim()).equalsIgnoreCase("BANCA")) {
            error += attributeColumn.get(11) + ", ";
        }
        if (!(this.removeUTF8BOM(attributeColumn.get(12)).trim()).equalsIgnoreCase("NOTE")) {
            error += attributeColumn.get(12) + ", ";
        }
        if (!(this.removeUTF8BOM(attributeColumn.get(13)).trim()).equalsIgnoreCase("Codice Avvocato")) {
            error += attributeColumn.get(13) + ", ";
        }

        if (!error.isEmpty())
            error += " NOME DEI CAMPI DIVERSO DA QUELLO DEFINITO ";

        return error;

    }

    /**
     * ----------------------------- METODI RICHIAMATI PER ESEGUIRE L'IMPORT DEI FILE -----------------------------
     **/

    //REFACTOR
    // I FILE GESTITI IN QUESTO METODO SONO:
    //  - FRANCHIGIA_1
    //  - FRANCHIGIA_2
    //  - NUMBER_SX
    //  - AFFIDO_MASSIVO
    @Override
    public DataManagementImportResponseV1 importFileWithWasteFile(MultipartFile fileToBeImport, String fileDescription, FileImportTypeEnum fileImportType) {

        UploadFileResponseV1 uploadDataManagementImport = null;
        DataManagementImportResponseV1 response = null;
        try {

            BufferedReader br = null;
            String line = "";

            br = new BufferedReader(new InputStreamReader(fileToBeImport.getInputStream()));

            StringBuffer finaleToimport = new StringBuffer();

            List<List<String>> fileCsv = new ArrayList<>();

            while ((line = br.readLine()) != null) {

                List<String> attribute = Arrays.asList(line.split(";", -1));

                List<String> columnFinal = new LinkedList<>();

                for (int i = 0; i < attribute.size(); i++) {

                    //PER LE COLONNE CHE PREVEDONO VALORI NUMERICI CON LA VIRGOLA, ANDIAMO A SOSTITUIRE LA ',' CON IL '.'
                    if (((i == 6 || i == 7) && fileImportType.equals(FileImportTypeEnum.FRANCHISE_1)) ||
                            ((i == 6 || i == 7 || i == 8 || i == 9) && fileImportType.equals(FileImportTypeEnum.FRANCHISE_2))
                    ) {

                        finaleToimport.append(attribute.get(i).replaceAll(",", "."));
                        columnFinal.add(attribute.get(i).replaceAll(",", "."));
                    } else {

                        finaleToimport.append(attribute.get(i));
                        columnFinal.add(attribute.get(i));
                    }
                    finaleToimport.append(";");

                }

                finaleToimport = new StringBuffer(finaleToimport.substring(0, finaleToimport.length() - 1));
                finaleToimport.append("\n");
                fileCsv.add(columnFinal);
            }
            br.close();
            uploadDataManagementImport = uploadFileService.uploadDeductibleFile(fileToBeImport, finaleToimport.toString(), fileDescription, fileImportType);

            DataManagementImportEntity dataManagement = dataManagementImportRepository.getOne(UUID.fromString(uploadDataManagementImport.getResourceId()));

            String error = null;
            String result = null;
            UploadFileRequestV1 uploadFileScartoRequest;

            if (fileImportType.equals(FileImportTypeEnum.FRANCHISE_1)) {

                error = this.checkDeduction1Attributes(fileCsv.get(0));
                fileCsv.remove(0);

                if (!error.isEmpty()) {
                    LOGGER.debug("RESTITUITO: " + error);
                    throw new BadRequestException(error, MessageCode.CLAIMS_1156);
                }

                result = this.deduction1Import(fileCsv);
            } else if (fileImportType.equals(FileImportTypeEnum.FRANCHISE_2)) {

                error = this.checkDeduction2Attributes(fileCsv.get(0));
                fileCsv.remove(0);


                if (!error.isEmpty()) {
                    LOGGER.debug("RESTITUITO: " + error);
                    throw new BadRequestException(error, MessageCode.CLAIMS_1156);
                }
                result = this.deduction2Import(fileCsv);

            } else if (fileImportType.equals(FileImportTypeEnum.NUMBER_SX)) {

                error = this.checkNumberSxAttributes(fileCsv.get(0));
                fileCsv.remove(0);

                if (!error.isEmpty()) {
                    LOGGER.debug("RESTITUITO: " + error);
                    throw new BadRequestException(error, MessageCode.CLAIMS_1156);
                }

                result = this.numberSxImport(fileCsv);

            } else if (fileImportType.equals(FileImportTypeEnum.MASSIVE_ENTRUST)) {

                error = this.checkMassiveEntrustAttributes(fileCsv.get(0));
                fileCsv.remove(0);

                if (!error.isEmpty()) {
                    LOGGER.debug("RESTITUITO: " + error);
                    throw new BadRequestException(error, MessageCode.CLAIMS_1156);
                }

                result = this.massiveEntrustImport(fileCsv);

            } else if (fileImportType.equals(FileImportTypeEnum.ALLOWANCE) || fileImportType.equals(FileImportTypeEnum.BANK_TRANSFER)) {

                error = this.checkLawyerPaymentsAttributes(fileCsv.get(0), fileImportType);
                fileCsv.remove(0);

                if (!error.isEmpty()) {
                    LOGGER.debug("RESTITUITO: " + error);
                    throw new BadRequestException("RESTITUITO: " + error, MessageCode.CLAIMS_2000);
                }

                result = this.lawyerPaymentsImport(fileCsv, fileImportType);
            }

            if (result != null) {

                UploadFileResponseV1 uploadFileResponse = null;

                uploadFileScartoRequest = new UploadFileRequestV1();
                uploadFileScartoRequest.setBlobType(uploadDataManagementImport.getBlobType());
                uploadFileScartoRequest.setDescription("file di scarto");
                uploadFileScartoRequest.setFileName(uploadDataManagementImport.getFileName().replace(".csv", "_Scarto.csv"));
                uploadFileScartoRequest.setFileContent(uploadFileService.encodeFileInBase64(result));
                uploadFileScartoRequest.setResourceType("scarto");
                uploadFileScartoRequest.setResourceId(uploadDataManagementImport.getResourceId());

                try {
                    uploadFileResponse = fileManagerService.uploadSingleFile(uploadFileScartoRequest);

                    ProcessingFile processingFile = new ProcessingFile();
                    processingFile.setAttachmentId(UUID.fromString(uploadFileResponse.getUuid()));
                    processingFile.setFileName(uploadFileResponse.getFileName());
                    processingFile.setFileSize(uploadFileResponse.getFileSize());

                    dataManagement.getProcessingFiles().add(processingFile);

                    dataManagement.setResult("WAITING_FOR_PROCESSING_WITH_WASTE");

                } catch (ConnectException e) {
                    dataManagement.setDescription("Caricamento del file di scarto fallito");
                }
            }

            dataManagementImportRepository.save(dataManagement);
            response = DataManagementImportAdapter.adptDataManagementImportToDataManagementImportResponse(dataManagement);

        } catch (IOException e) {
            LOGGER.debug(e.getMessage());
            throw new InternalError(e);
        }

        return response;
    }

    // I FILE GESTITI IN QUESTO METODO SONO:
    //  - FURTI
    //  - PRATICHE_AUTO
    //  - SEQUESTRI
    @Override
    public UploadFileResponseV1 importFile(MultipartFile fileToBeImport, String fileDescription, FileImportTypeEnum fileImportType, String status) {

        try {

            BufferedReader br = null;
            String line = "";

            br = new BufferedReader(new InputStreamReader(fileToBeImport.getInputStream()));

            StringBuffer finaleToimport = new StringBuffer();

            List<List<String>> fileCsv = new ArrayList<>();

            while ((line = br.readLine()) != null) {

                List<String> attribute = Arrays.asList(line.split(";", -1));
                List<String> columnFinal = new LinkedList<>();

                for (int i = 0; i < attribute.size(); i++) {

                    finaleToimport.append(attribute.get(i));
                    columnFinal.add(attribute.get(i));
                    finaleToimport.append(";");

                }

                finaleToimport = new StringBuffer(finaleToimport.substring(0, finaleToimport.length() - 1));
                finaleToimport.append("\n");
                fileCsv.add(columnFinal);
            }
            br.close();
            DataManagementImportEntity dataManagement = null;
            UploadFileResponseV1 uploadDataManagementImport = uploadFileService.uploadDeductibleFile(fileToBeImport, finaleToimport.toString(), fileDescription, fileImportType);
            dataManagement = dataManagementImportRepository.getOne(UUID.fromString(uploadDataManagementImport.getResourceId()));

            String error = null;

            try {

                if (fileImportType.equals(FileImportTypeEnum.THEFT)) {

                    error = this.checkTefthAttributes(fileCsv.get(0));
                    fileCsv.remove(0);

                    if (!error.equalsIgnoreCase("")) {
                        LOGGER.debug(error);
                        throw new BadRequestException(error);
                    }

                    this.tefthImport(fileCsv, status);

                }

                dataManagement.setResult("PROCESSED");
                dataManagementImportRepository.save(dataManagement);
                //return DataManagementImportAdapter.adptDataManagementImportToDataManagementImportResponse(dataManagement);
                return uploadDataManagementImport;

            } catch (Exception ex) {

                dataManagement.setResult("ERROR");
                dataManagementImportRepository.save(dataManagement);

                if (ex instanceof BadRequestException) {
                    LOGGER.debug(ex.getMessage());
                    throw new BadRequestException(ex.getMessage(), ex);
                }

                LOGGER.debug(ex.getMessage());
                throw new InternalException(ex.getMessage(), ex);
            }

        } catch (BadRequestException | InternalException e) {
            LOGGER.debug(e.getMessage());
            throw e;
        } catch (IOException e) {
            LOGGER.debug(e.getMessage());
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    /**
     * ----------------- METODI CHE ESEGUONO IL CONTROLLO DEL CONTENUTO DEI FILE IMPORTATI -----------------
     **/

    /*FRANCHIGIA 1*/
    //REFACTOR
    private String deduction1Import(List<List<String>> elementList) {

        StringBuilder error = new StringBuilder();
        String stringaFileDiScarto = null;

        elementList.removeIf(att -> att.size() != 11);

        for (List<String> element : elementList) {

            if (element.size() > 1 && element.size() < 12) {

                ClaimsNewEntity claimsNewEntity = null;
                ClaimsEntity claimsEntity = null;
                ContractTypeEntity contractType = null;
                WasteFileDeduction1Attribute wasteFileDeduction1Attributes = new WasteFileDeduction1Attribute();

                //CONTROLLO DELL'ID PRATICA
                String practiceId = element.get(0);
                if (practiceId.equals("")) {
                    error.append("Il campo SX Fleet e' vuoto. ");
                } else {

                    //RECUPERO CLAIMS TRAMITE ID PARTICA PRESENTE SUL FILE DI IMPORT
                    claimsNewEntity = claimsNewRepository.getClaimsByIdPratica(Long.valueOf(practiceId));
                    if (claimsNewEntity == null) {
                        error.append("SX Fleet non presente. ");

                    } else {
                        //funzione di conversione
                        claimsEntity = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntity);

                        if (claimsEntity.getStatus() != null && claimsEntity.getStatus().equals(ClaimsStatusEnum.DRAFT) ||
                                claimsEntity.getStatus().equals(ClaimsStatusEnum.INCOMPLETE) ||
                                claimsEntity.getStatus().equals(ClaimsStatusEnum.WAITING_FOR_VALIDATION)
                        )
                            error.append("Il sinistro non e' stato validato. ");


                        Damaged damaged = claimsEntity.getDamaged();

                        if (damaged != null && damaged.getContract() != null && damaged.getContract().getContractType() != null)
                            contractType = contractTypeRepository.findByCodContract(claimsEntity.getDamaged().getContract().getContractType());

                        //Controllo che se l'exemption è diversa da null, siano settati tutti i valori riferiti alla franchigia 1
                        if (claimsEntity.getExemption() != null &&
                                claimsEntity.getExemption().getDeductPaid1() != null &&
                                claimsEntity.getExemption().getDeductImported1() != null &&
                                claimsEntity.getExemption().getDeductEnd1() != null

                        )
                            error.append("E' stato gia importata la franchigia 1 per questo claim. ");

                            //Controllo che exemption sia null o che sia diversa da null ma con tutti i campi null
                        else if (claimsEntity.getExemption() == null || (claimsEntity.getExemption() != null &&
                                claimsEntity.getExemption().getDeductPaid1() == null &&
                                claimsEntity.getExemption().getDeductImported1() == null &&
                                claimsEntity.getExemption().getDeductEnd1() == null &&
                                claimsEntity.getExemption().getDeductPaid2() == null &&
                                claimsEntity.getExemption().getDeductImported2() == null &&
                                claimsEntity.getExemption().getDeductEnd2() == null &&
                                claimsEntity.getExemption().getDeductTotalPaid() == null &&
                                claimsEntity.getExemption().getCompanyReference() == null &&
                                claimsEntity.getExemption().getPolicyNumber() == null)
                        ) {

                            //CONTROLLO DELLA TARGA DA DAMAGED -> VEHICLE
                            String plate = element.get(2);
                            if (plate.equals(""))
                                error.append("Il campo Targa Veicolo Assicurato e' vuoto. ");
                            else if (damaged != null && damaged.getVehicle() != null && !damaged.getVehicle().getLicensePlate().equalsIgnoreCase(plate))
                                error.append("La targa non corrisponde con quella individuata nel claim. ");

                            //CONTROLLO DELLA DATE DEL SINISTRO
                            String dateAccident = element.get(1);
                            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy").withZone( ZoneId.of(timeZone));


                            LocalDateTime dataSinistro = null;

                            if (claimsEntity.getComplaint() == null || claimsEntity.getComplaint().getDataAccident() == null) {
                                error.append("La data del sinistro nel claim non è valorizzata. ");
                            } else {

                                dataSinistro = LocalDateTime.ofInstant(claimsEntity.getComplaint().getDataAccident().getDateAccident().toInstant(), ZoneId.of(timeZone));



                                if (dateAccident.equals(""))
                                    error.append("Il campo Data del Sx e' vuoto. ");
                                else {
                                    LocalDate dataAccidentCal = null;
                                    try {
                                        dataAccidentCal = LocalDate.from(formatter.parse(dateAccident));


                                        if (!dataAccidentCal.isEqual(dataSinistro.toLocalDate()))
                                            error.append("La data del sinistro non concide con la data individuata nel claim.");

                                    } catch (DateTimeParseException e) {
                                        error.append("La data del sinistro non rispetta il formato corretto (dd-MM-yyyy). ");
                                    }
                                }
                            }

                            //CONTROLLO SE E' PRESENTE IL RIFERIMENTO ALLA COMPAGNIA
                            String companyReference = element.get(3);
                            if (companyReference.equals(""))
                                error.append("L'attributo SX Dcs Italia non e' valorizzato. ");

                            //CONTROLLO SE E' PRESENTE LA DATA DI CHIUSURA
                            String dataChiusura = element.get(5);
                            //try {
                            if (dataChiusura == null || dataChiusura.equals(""))
                                error.append("Il campo Data di chiusura non e' valorizzato. ");
                            else {

                                LocalDate oggi = LocalDate.now();
                                LocalDate dataChiusuraCal = null;
                                try {
                                    dataChiusuraCal = LocalDate.from(formatter.parse(dataChiusura));

                                    if ((dataSinistro != null && (dataSinistro.toLocalDate().isAfter(dataChiusuraCal) || dataSinistro.toLocalDate().isEqual(dataChiusuraCal))) || (oggi.isBefore(dataChiusuraCal) || oggi.isEqual(dataChiusuraCal)))
                                        error.append("La data di chiusura non puo' essere minore della data del sinistro oppure maggiore o uguale di quella odierna. ");

                                } catch (DateTimeParseException e) {
                                    error.append("La data chiusura non rispetta il formato corretto (dd-MM-yyyy). ");
                                }
                            }

                            //CONTROLLO SE E' PRESENTE L'IMPORTO DELLA FRANCHIGIA
                            String importoFF = element.get(6);
                            Double importoFFDouble = null;
                            if (importoFF == null || importoFF.equals(""))
                                error.append("Il campo Importo FF Pagamento Sx non e' valorizzato. ");
                            else {
                                importoFFDouble = Double.valueOf(importoFF);
                            }

                            //CONTROLLO SE IL SUFFISSO POLIZZA NEL FILE NON E' VUOTO
                            String policySufix = element.get(10);
                            Double checkFranchise = null;
                            if (policySufix == null || policySufix.equalsIgnoreCase("")) {
                                error.append("Il campo Suffisso Polizza e' vuoto. ");
                            } else {

                                //Nel caso debba considerare il campo Franchigia della policy, controllo che questo sia valorizzato
                                if (PolicySuffixEnum.create(policySufix).equals(PolicySuffixEnum.P) && damaged.getInsuranceCompany() != null) {

                                    InsuranceCompany insuranceCompany = damaged.getInsuranceCompany();

                                    if (insuranceCompany != null && insuranceCompany.getTpl() != null) {

                                        Tpl tpl = insuranceCompany.getTpl();

                                        if (tpl.getInsurancePolicyId() != null) {

                                            InsurancePolicyEntity policy = insurancePolicyRepository.getOne(tpl.getInsurancePolicyId());

                                            if (policy.getFranchise() == null)
                                                error.append("La franchigia relativa alla polizza con id: " + tpl.getInsurancePolicyId() + " non è presente");
                                            else {
                                                checkFranchise = policy.getFranchise();
                                            }

                                        } else {
                                            error.append("Non risulta nessuna polizza associata a questo Claims");
                                        }
                                    }
                                } else {
                                    if (contractType.getFranchise() != null)
                                        checkFranchise = contractType.getFranchise();
                                }
                            }

                            //Se ho tutte le informazioni per le franchigie
                            if (importoFFDouble != null && checkFranchise != null) {

                                if (importoFFDouble < 0)
                                    error.append(" Importo franghigia da pagare e' negativo. ");

                                else if (importoFFDouble > checkFranchise)
                                    error.append(" Importo franghigia da pagare e' maggiore di ").append(checkFranchise).append(". ");

                            }

                            //CONTROLO SE IL TOTALE E' PRESENTE SUL FILE, NON SIA NEGATIVO E NEMMENO MAGGIORE DELLA FRANCHIGIA ASSOCIATA AL CONTRATTO
                            String totalPaid = element.get(7);
                            if (totalPaid == null || totalPaid.equals(""))
                                error.append("Il campo Totale Pagato non e' valorizzato. ");

                            //Da richiesta rimuovo il controllo sul Totale pagato
                            /*else {

                                Double totaleDouble = Double.valueOf(totalPaid);
                                if (totaleDouble < 0)
                                    error.append("Il campo Totale Pagato non puo' essere negativo. ");
                                else if (contractType != null && totaleDouble > contractType.getFranchise())
                                    error.append("Il campo Totale Pagato è' maggiore di ").append(contractType.getFranchise()).append(". ");
                            }*/

                            //CONTROLLO SE IL CAMPO NR POLIZZA NEL FILE NON SIA VUOTO
                            String policyNumber = element.get(9);
                            if (policyNumber == null ||
                                    policyNumber.equalsIgnoreCase("")) {
                                error.append("Il campo Nr. Polizza e' vuoto. ");
                            }

                        } else {
                            LOGGER.error("Exemption presenta delle informazioni non sufficienti per la franchigia 1. ");
                        }
                    }
                }

                if (!error.toString().equalsIgnoreCase("")) {

                    wasteFileDeduction1Attributes.setSxFleet(element.get(0));
                    wasteFileDeduction1Attributes.setDataDelSx(element.get(1));
                    wasteFileDeduction1Attributes.setTargaVeicoloAssicurato(element.get(2));
                    wasteFileDeduction1Attributes.setSxDcsItalia(element.get(3));
                    wasteFileDeduction1Attributes.setTipologiaDelSinistro(element.get(4));
                    wasteFileDeduction1Attributes.setDataDiChiusura(element.get(5));
                    wasteFileDeduction1Attributes.setImportoFFPagamentoSx(element.get(6).replaceAll(",", "."));
                    wasteFileDeduction1Attributes.setTotalePagato(element.get(7).replaceAll(",", "."));
                    wasteFileDeduction1Attributes.setRiaperture(element.get(8));
                    wasteFileDeduction1Attributes.setNrPolizza(element.get(9));
                    wasteFileDeduction1Attributes.setSuffisoPolizza(element.get(10));
                    wasteFileDeduction1Attributes.setNote(error.toString());

                    stringaFileDiScarto = this.insertInDeduction1WasteFile(wasteFileDeduction1Attributes, stringaFileDiScarto);
                    error = new StringBuilder();

                }

            } else {

                LOGGER.error("il numero degli elementi della riga non rispetta il formato corretto. ");
            }


        }

        return stringaFileDiScarto;
    }


    //REFACTOR
    /*FRANCHIGIA 2*/
    private String deduction2Import(List<List<String>> elementList) {

        StringBuilder error = new StringBuilder();
        String stringaFileDiScarto = null;

        elementList.removeIf(att -> att.size() != 13);

        for (List<String> element : elementList) {

            if (element.size() > 1 && element.size() < 14) {

                ClaimsNewEntity claimsNewEntity = null;
                ClaimsEntity claimsEntity = null;
                ContractTypeEntity contractType = null;
                WasteFileDeduction2Attribute wasteFileDeduction2Attributes = new WasteFileDeduction2Attribute();

                String practiceId = element.get(0);
                if (practiceId.equals(""))
                    error.append("Il campo SX Fleet è vuoto. ");
                else {

                    //RECUPERO CLAIMS TRAMITE ID PARTICA PRESENTE SUL FILE DI IMPORT
                    claimsNewEntity = claimsNewRepository.getClaimsByIdPratica(Long.valueOf(practiceId));

                    if (claimsNewEntity == null) {
                        error.append("SX Fleet non presente. ");

                    } else {
                        //funzione di conversione
                        claimsEntity = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntity);

                        if (claimsEntity.getStatus() != null && claimsEntity.getStatus().equals(ClaimsStatusEnum.DRAFT) ||
                                claimsEntity.getStatus().equals(ClaimsStatusEnum.INCOMPLETE) ||
                                claimsEntity.getStatus().equals(ClaimsStatusEnum.WAITING_FOR_VALIDATION)
                        )
                            error.append("Il sinistro non e' stato validato. ");

                        if (claimsEntity.getExemption() == null)

                            error.append("Non sono presenti i dati riguardanti la franchigia 1. ");

                        else {

                            Damaged damaged = claimsEntity.getDamaged();

                            if (damaged != null && damaged.getContract() != null && damaged.getContract().getContractType() != null)
                                contractType = contractTypeRepository.findByCodContract(claimsEntity.getDamaged().getContract().getContractType());

                            String plate = element.get(2);
                            if (plate.equals(""))
                                error.append("Il campo Targa Veicolo Assicurato è vuoto. ");
                            else if (damaged != null && damaged.getVehicle() != null && !damaged.getVehicle().getLicensePlate().equalsIgnoreCase(plate))
                                error.append("La targa non corrisponde con quella individuata nel claim. ");

                            String dateAccident = element.get(1);
                            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy").withZone(ZoneId.of(timeZone));

                            LocalDateTime dataSinistro = null;

                            if (claimsEntity.getComplaint() == null || claimsEntity.getComplaint().getDataAccident() == null) {
                                error.append("La data del sinistro nel claim non è valorizzata. ");
                            } else {

                                dataSinistro = LocalDateTime.ofInstant(claimsEntity.getComplaint().getDataAccident().getDateAccident().toInstant(), ZoneId.of(timeZone));


                                if (dateAccident.equals(""))
                                    error.append("Il campo Data del Sx e' vuoto. ");
                                else {

                                    LocalDate dataAccidentCal = null;
                                    try {
                                        dataAccidentCal = LocalDate.from(formatter.parse(dateAccident));

                                        if (!dataAccidentCal.isEqual(dataSinistro.toLocalDate()))
                                            error.append("La data del sinistro non concide con la data individuata nel claim. ");
                                    } catch (DateTimeParseException e) {
                                        error.append("La data del sinistro non rispetta il formato corretto (dd-MM-yyyy). ");
                                    }

                                }
                            }

                            String dataChiusura = element.get(5);
                            if (claimsEntity.getExemption().getDeductImported2() != null)
                                error.append("Deduct Imported2 è già valorizzato. ");

                            if (dataChiusura == null || dataChiusura.equals(""))
                                error.append("Il campo Data di chiusura non e' valorizzato. ");
                            else {

                                LocalDate oggi = LocalDate.now();
                                LocalDate dataChiusuraCal = null;
                                try {
                                    dataChiusuraCal = LocalDate.from(formatter.parse(dataChiusura));

                                    if ((dataSinistro != null && (dataSinistro.toLocalDate().isAfter(dataChiusuraCal) || dataSinistro.toLocalDate().isEqual(dataChiusuraCal))) || (oggi.isBefore(dataChiusuraCal) || oggi.isEqual(dataChiusuraCal)))
                                        error.append("La data di chiusura non puo' essere minore della data del sinistro oppure maggiore o uguale di quella odierna. ");

                                } catch (DateTimeParseException e) {
                                    error.append("La data di chiusura non rispetta il formato corretto (dd-MM-yyyy). ");
                                }
                            }

                            String importoExtraFF = element.get(6);
                            Double importoExtraFFDouble = null;
                            if (importoExtraFF.equals(""))
                                error.append("Il campo Importo Extra FF Pagamento Sx non e' valorizzato. ");
                            else if (claimsEntity.getExemption().getDeductImported2() != null)
                                error.append("Il campo Importo Extra FF Pagamento Sx e' gia' stato importato. ");
                            else {
                                importoExtraFFDouble = Double.valueOf(importoExtraFF);
                            }

                            if (claimsEntity.getExemption().getDeductImported1() == null)
                                error.append("La data di prima importazione non e' valorizzata. ");

                            if (claimsEntity.getExemption().getDeductImported2() != null)
                                error.append("La data di seconda importazione e' gia' valorizzata. ");

                            String importoFFPagamentoSx = element.get(7);
                            if (importoFFPagamentoSx.equalsIgnoreCase(""))
                                error.append("Il campo Importo FF Pagamento Sx non e' valorizzato. ");
                            else {

                                Double importoFFPagamentoSxDouble = Double.valueOf(importoFFPagamentoSx);
                                if (contractType != null && importoFFPagamentoSxDouble > contractType.getFranchise())
                                    error.append("Importo FF Pagamento Sx. e' maggiore di ").append(contractType.getFranchise()).append(". ");
                            }

                            String ImportoFFPrecedenteFatturatoPagamentoSx = element.get(8);
                            if (ImportoFFPrecedenteFatturatoPagamentoSx.equalsIgnoreCase(""))
                                error.append("Importo FF precedente fatturato Pagamento Sx non e' valorizzato. ");
                            else {

                                Double ImportoFFPrecedenteFatturatoPagamentoSxDouble = Double.valueOf(ImportoFFPrecedenteFatturatoPagamentoSx);
                                BigDecimal ImportoFFPrecedenteFatturatoPagamentoSxBigDecimal = BigDecimal.valueOf(ImportoFFPrecedenteFatturatoPagamentoSxDouble);
                                if (claimsEntity.getExemption() != null && claimsEntity.getExemption().getDeductPaid1() != null) {
                                    BigDecimal deductPaid1BigDecimal = BigDecimal.valueOf(claimsEntity.getExemption().getDeductPaid1());
                                    if (!deductPaid1BigDecimal.equals(ImportoFFPrecedenteFatturatoPagamentoSxBigDecimal)) {
                                        error.append("Importo FF precedente fatturato Pagamento Sx non corrisponde all'importo franchigia presente sul primo tracciato. ");
                                    }
                                }
                            }


                            //CONTROLLO SE IL SUFFISSO POLIZZA NEL FILE NON E' VUOTO
                            String policySufix = element.get(12);
                            Double checkFranchise = null;
                            if (policySufix == null || policySufix.equalsIgnoreCase("")) {
                                error.append("Il campo Suffisso Polizza e' vuoto. ");
                            } else {

                                //Nel caso debba considerare il campo Franchigia della policy, controllo che questo sia valorizzato
                                if (PolicySuffixEnum.create(policySufix).equals(PolicySuffixEnum.P) && damaged.getInsuranceCompany() != null) {

                                    InsuranceCompany insuranceCompany = damaged.getInsuranceCompany();

                                    if (insuranceCompany != null && insuranceCompany.getTpl() != null) {

                                        Tpl tpl = insuranceCompany.getTpl();

                                        if (tpl.getInsurancePolicyId() != null) {

                                            InsurancePolicyEntity policy = insurancePolicyRepository.getOne(tpl.getInsurancePolicyId());

                                            if (policy.getFranchise() == null)
                                                error.append("La franchigia relativa alla polizza con id: " + tpl.getInsurancePolicyId() + " non è presente");
                                            else {
                                                checkFranchise = policy.getFranchise();
                                            }

                                        } else {
                                            error.append("Non risulta nessuna polizza associata a questo Claims");
                                        }
                                    }
                                } else {
                                    if (contractType.getFranchise() != null)
                                        checkFranchise = contractType.getFranchise();
                                }
                            }


                            if (!importoExtraFF.equalsIgnoreCase("") && !ImportoFFPrecedenteFatturatoPagamentoSx.equalsIgnoreCase("") && !importoFFPagamentoSx.equalsIgnoreCase("") && checkFranchise != null) {

                                Double importoFFPagamentoSxDouble = Double.valueOf(importoFFPagamentoSx);
                                Double importoFFPrecedenteFatturatoPagamentoSxDouble = Double.valueOf(ImportoFFPrecedenteFatturatoPagamentoSx);
                                importoExtraFFDouble = Double.valueOf(importoExtraFF);


                                //CONVERSIONE IN BIG DECIMAL
                                BigDecimal importoFFPagamentoSxBigDecimal = BigDecimal.valueOf(importoFFPagamentoSxDouble);
                                BigDecimal importoFFPrecedenteFatturatoPagamentoSxBigDecimal = BigDecimal.valueOf(importoFFPrecedenteFatturatoPagamentoSxDouble);
                                BigDecimal importoExtraFFBigDecimal = BigDecimal.valueOf(importoExtraFFDouble);

                                if (contractType != null && importoExtraFFDouble > checkFranchise)
                                    error.append("Importo Extra FF Pagamento Sx. e' maggiore di ").append(checkFranchise).append(". ");

                                if (!importoFFPagamentoSxBigDecimal.equals(importoFFPrecedenteFatturatoPagamentoSxBigDecimal.add(importoExtraFFBigDecimal)))
                                    error.append("Importo FF Pagamento Sx. non corrisponde alla somma delle franchigie importate. ");

                                else if (contractType != null && importoFFPrecedenteFatturatoPagamentoSxDouble + importoExtraFFDouble > checkFranchise)
                                    error.append("Importo FF Pagamento Sx. e' maggiore di ").append(checkFranchise).append(". ");
                            }

                            String totalePagato = element.get(9);
                            if (totalePagato.equalsIgnoreCase("")) {
                                error.append("Il Totale Pagato non e' valorizzato. ");
                            }
                        }
                    }
                }

                if (!error.toString().equalsIgnoreCase("")) {

                    wasteFileDeduction2Attributes.setSxFleet(element.get(0));
                    wasteFileDeduction2Attributes.setDataDelSx(element.get(1));
                    wasteFileDeduction2Attributes.setTargaVeicoloAssicurato(element.get(2));
                    wasteFileDeduction2Attributes.setSxDcsItalia(element.get(3));
                    wasteFileDeduction2Attributes.setTipologiaDelSinistro(element.get(4));
                    wasteFileDeduction2Attributes.setDataDiChiusura(element.get(5));
                    wasteFileDeduction2Attributes.setImportoExtraFFPagamentoSx(element.get(6));
                    wasteFileDeduction2Attributes.setImportoFFPagamentoSx(element.get(7));
                    wasteFileDeduction2Attributes.setImportoFFPrecedenteFatturatoPagamentoSx(element.get(8));
                    wasteFileDeduction2Attributes.setTotalePagato(element.get(9));
                    wasteFileDeduction2Attributes.setRiaperture(element.get(10));
                    wasteFileDeduction2Attributes.setNrPolizza(element.get(11));
                    wasteFileDeduction2Attributes.setSuffisoPolizza(element.get(12));
                    wasteFileDeduction2Attributes.setNote(error.toString());

                    stringaFileDiScarto = this.insertInDeduction2WasteFile(wasteFileDeduction2Attributes, stringaFileDiScarto);
                    error = new StringBuilder();

                }
            } else {
                LOGGER.error("il numero degli elementi della riga non rispetta il formato corretto. ");
            }
        }
        return stringaFileDiScarto;
    }

    //REFACTOR
    /*NUMERO SX*/
    private String numberSxImport(List<List<String>> elementList) {

        StringBuilder error = new StringBuilder();
        String stringaFileDiScarto = null;

        elementList.removeIf(att -> att.size() != 9);

        for (List<String> element : elementList) {

            ClaimsNewEntity claimsNewEntity = null;
            ClaimsEntity claimsEntity = null;
            ContractTypeEntity contractType = null;
            WasteFileNumberSxAttribute wasteFileNumberSxAttribute = new WasteFileNumberSxAttribute();

            String practiceId = element.get(0);
            if (practiceId.equals(""))
                error.append("Il campo Rif Axus-Ald e' vuoto. ");
            else {
                //RECUPERO CLAIMS TRAMITE ID PARTICA PRESENTE SUL FILE DI IMPORT
                claimsNewEntity = claimsNewRepository.getClaimsByIdPratica(Long.valueOf(practiceId));

                if (claimsNewEntity == null) {
                    error.append("Rif Axus-Ald non presente. ");

                } else {

                    //funzione di conversione
                    claimsEntity = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntity);

                    if (claimsEntity.getStatus() != null && claimsEntity.getStatus().equals(ClaimsStatusEnum.DRAFT) ||
                            claimsEntity.getStatus().equals(ClaimsStatusEnum.INCOMPLETE) ||
                            claimsEntity.getStatus().equals(ClaimsStatusEnum.WAITING_FOR_VALIDATION)
                    )
                        error.append("Il sinistro non e' stato validato. ");

                    //CONTROLLO DELLA TARGA
                    if (element.get(5).equalsIgnoreCase(""))
                        error.append("Il campo Targa Veicolo Assicurato e' vuoto. ");
                    else {

                        String plate = element.get(5);
                        Damaged damaged = claimsEntity.getDamaged();
                        if (damaged.getVehicle() != null && damaged.getVehicle().getLicensePlate() != null) {
                            if (!claimsEntity.getDamaged().getVehicle().getLicensePlate().equalsIgnoreCase(plate))
                                error.append("La targa non corrisponde con quella individuata nel claim. ");
                        } else
                            error.append("Nel claims non e' presente il veicolo o la targa ad esso associata. ");
                    }

                    //CONTROLLO DATA SINISTRO
                    if (element.get(3).equalsIgnoreCase(""))
                        error.append("Il campo Data Sinistro e' vuoto. ");
                    else {

                        String dateAccident = element.get(3);
                        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy").withZone(ZoneId.of(timeZone));
                        LocalDateTime dataSinistro = null;

                        if (dateAccident.equals(""))
                            error.append("Il campo Data del Sx e' vuoto. ");
                        else {

                            dataSinistro = LocalDateTime.ofInstant(claimsEntity.getComplaint().getDataAccident().getDateAccident().toInstant(),ZoneId.of(timeZone));


                            LocalDate dataAccidentCal = null;
                            try {
                                dataAccidentCal = LocalDate.from(formatter.parse(dateAccident));

                                if (!dataAccidentCal.isEqual(dataSinistro.toLocalDate()))
                                    error.append("La data del sinistro non concide con la data individuata nel claim. ");

                            } catch (DateTimeParseException e) {
                                error.append("La data del sinistro non rispetta il formato corretto (dd-MM-yyyy). ");
                            }
                        }
                    }

                    String tipoDanno = element.get(4);

                    if (tipoDanno != null && !tipoDanno.equalsIgnoreCase("")) {

                        try {

                            DataAccidentTypeAccidentEnum accidentType = DataAccidentTypeAccidentEnum.create(tipoDanno.toLowerCase());
                        } catch (BadParametersException ex) {
                            error.append("Tipo sinistro non esistente a sistema. ");
                        }
                    } else {
                        error.append("Tipo sinistro non presente. ");
                    }


                    //CONTROLLO RIFERIMENTO COMPAGNIA
                    if (element.get(1).equalsIgnoreCase(""))
                        error.append("Il riferimento alla compagnia e' vuoto. ");
                    else {
                        String rifCompagnia = element.get(1);

                        Complaint complaint = claimsEntity.getComplaint();
                        if (complaint != null) {
                            FromCompany fromCompany = claimsEntity.getComplaint().getFromCompany();
                            if (fromCompany != null) {
                                if ((fromCompany.getNumberSx() != null &&
                                        !fromCompany.getNumberSx().isEmpty() &&
                                !rifCompagnia.equalsIgnoreCase(fromCompany.getNumberSx())))
                                    error.append("Il riferimento alla compagnia e' gia' presente e non corrisponde a quello presente nel file. ");

                            }
                        } else
                            error.append("Nel claims non e' presente il complaint. ");
                    }
                }
            }

            if (!error.toString().equalsIgnoreCase("")) {

                wasteFileNumberSxAttribute.setRifAxusAld(element.get(0));
                wasteFileNumberSxAttribute.setRifCompagnia(element.get(1));
                wasteFileNumberSxAttribute.setNumeroPolizza(element.get(2));
                wasteFileNumberSxAttribute.setDataSinistro(element.get(3));
                wasteFileNumberSxAttribute.setTipoDanno(element.get(4));
                wasteFileNumberSxAttribute.setTargaAssicurato(element.get(5));
                wasteFileNumberSxAttribute.setIspettorato(element.get(6));
                wasteFileNumberSxAttribute.setPeritoIncaricato(element.get(7));
                wasteFileNumberSxAttribute.setNote(error.toString());

                stringaFileDiScarto = this.insertInNumberSxWasteFile(wasteFileNumberSxAttribute, stringaFileDiScarto);
                error = new StringBuilder();

            }
        }
        return stringaFileDiScarto;
    }

    //REFACTOR
    /*AFFIDO MASSIVO*/
    private String massiveEntrustImport(List<List<String>> elementList) {

        StringBuilder error = new StringBuilder();
        String stringaFileDiScarto = null;

        elementList.removeIf(att -> att.size() != 5);

        for (List<String> element : elementList) {

            if (element.size() > 1 && element.size() < 6) {

                ClaimsNewEntity claimsNewEntity = null;
                ClaimsEntity claimsEntity = null;
                ContractTypeEntity contractType = null;

                WasteFileMassiveEntrustAttribute wasteFileMassiveEntrustAttributes = new WasteFileMassiveEntrustAttribute();

                //CONTROLLO DELL'ID PRATICA
                String practiceId = element.get(0);

                if (practiceId.equals("")) {
                    error.append("Il campo SX Fleet e' vuoto. ");
                } else {
                    Long practiceIdL = null;
                    //RECUPERO CLAIMS TRAMITE ID PARTICA PRESENTE SUL FILE DI IMPORT
                    try {
                        practiceIdL = Long.valueOf(practiceId);
                    } catch (NumberFormatException e) {
                        error.append("Il campo SX Fleet contiene un formato non valido. ");
                    }
                    if (practiceIdL != null) {
                        //RECUPERO CLAIMS TRAMITE ID PARTICA PRESENTE SUL FILE DI IMPORT
                        claimsNewEntity = claimsNewRepository.getClaimsByIdPratica(Long.valueOf(practiceId));
                        if (claimsNewEntity == null) {
                            error.append("SX Fleet non presente. ");

                        } else {
                            //funzione di conversione
                            claimsEntity = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntity);

                            if (claimsEntity.getStatus() != null && claimsEntity.getStatus().equals(ClaimsStatusEnum.DRAFT) ||
                                    claimsEntity.getStatus().equals(ClaimsStatusEnum.INCOMPLETE) ||
                                    claimsEntity.getStatus().equals(ClaimsStatusEnum.WAITING_FOR_VALIDATION) ||
                                    claimsEntity.getStatus().equals(ClaimsStatusEnum.REJECTED)
                            ) {
                                error.append("Il sinistro non e' stato validato. ");
                            } else if (claimsEntity.getStatus() != null && claimsEntity.getStatus().equals(ClaimsStatusEnum.DELETED)) {
                                error.append("Il sinistro e' stato cancellato. ");
                            } else if (claimsEntity.getStatus() != null && (claimsEntity.getStatus().equals(ClaimsStatusEnum.WAITING_FOR_REFUND) ||
                                    claimsEntity.getStatus().equals(ClaimsStatusEnum.CLOSED_PARTIAL_REFUND) || claimsEntity.getStatus().equals(ClaimsStatusEnum.CLOSED_TOTAL_REFUND))) {
                                error.append("Il sinistro e' già stato affidato. ");
                            }

                            Damaged damaged = claimsEntity.getDamaged();

                            if (damaged != null && damaged.getContract() != null && damaged.getContract().getContractType() != null) {

                                contractType = contractTypeRepository.findByCodContract(damaged.getContract().getContractType());
                            }

                            //CONTROLLO DELLA TARGA DA DAMAGED -> VEHICLE
                            String plate = element.get(2);
                            if (plate.equals(""))
                                error.append("Il campo Targa Veicolo Assicurato e' vuoto. ");
                            else if (damaged != null && damaged.getVehicle() != null && !damaged.getVehicle().getLicensePlate().equalsIgnoreCase(plate))
                                error.append("La targa non corrisponde con quella individuata nel claim. ");

                        //CONTROLLO DELLA DATE DEL SINISTRO
                        String dateAccident = element.get(1);
                        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                        formatter.setTimeZone(TimeZone.getTimeZone(timeZone));

                        String dataSinistro = null;
                        if (claimsEntity.getComplaint() == null || claimsEntity.getComplaint().getDataAccident() == null) {
                            error.append("La data del sinistro nel claim non è valorizzata. ");
                        } else {


                            dataSinistro = formatter.format(claimsEntity.getComplaint().getDataAccident().getDateAccident());


                            if (dateAccident.equals(""))
                                error.append("Il campo Data del Sx e' vuoto. ");
                            else {
                                try {
                                    Date checkFileDate = formatter.parse(dateAccident);

                                    if (!dataSinistro.equals(dateAccident))
                                        error.append("La data del sinistro non concide con la data individuata nel claim.");

                                } catch (ParseException e) {
                                    error.append("La data del sinistro non rispetta il formato previsto dd/mm/yyyy.");
                                }
                            }
                        }

                            //CONTROLLO SE IL CAMPO TIPOLOGIA AFFIDATARIO NON SIA VUOTO
                            String entrusted = element.get(3);
                            if (entrusted == null ||
                                    entrusted.equalsIgnoreCase("")) {
                                error.append("Il campo Tipologia Affidatario è vuoto. ");
                            } else {

                                try {

                                    Long codAffidatario = Long.parseLong(element.get(4));
                                    EntrustedEnum entrustedEnum = EntrustedEnum.create(entrusted);
                                    if (codAffidatario == null) {

                                        error.append("Il campo Cod. Affidatario è vuoto. ");
                                    } else {

                                        switch (entrustedEnum) {

                                            case LEGAL:
                                                LegalEntity legalEntity = legalRepository.searchLegalbyCode(codAffidatario);
                                                if (legalEntity == null)
                                                    error.append("Il campo Cod. Affidatario non corrisponde a nessun LEGALE. ");
                                                else if (legalEntity.getActive() == null || !legalEntity.getActive())
                                                    error.append("Il campo Cod. Affidatario corrisponde ad un LEGALE non attivo. ");
                                                break;

                                            case INSPECTORATE:
                                                InspectorateEntity inspectorateEntity = inspectorateRepository.searchInspectoratebyCode(codAffidatario);
                                                if (inspectorateEntity == null)
                                                    error.append("Il campo Cod. Affidatario non corrisponde a nessun ISPETTORATO. ");
                                                else if (inspectorateEntity.getActive() == null || !inspectorateEntity.getActive())
                                                    error.append("Il campo Cod. Affidatario corrisponde ad un INSPETTORATO non attivo. ");
                                                break;

                                            case MANAGER:
                                                InsuranceManagerEntity insuranceManagerEntity = insuranceManagerRepository.searchManagerByCode(codAffidatario);
                                                if (insuranceManagerEntity == null)
                                                    error.append("Il campo Cod. Affidatario non corrisponde a nessun MANAGER. ");
                                                else if (insuranceManagerEntity.getActive() == null || !insuranceManagerEntity.getActive())
                                                    error.append("Il campo Cod. Affidatario corrisponde ad un MANAGER non attivo. ");
                                                break;
                                            default:
                                                LOGGER.debug(MessageCode.CLAIMS_1114.value());
                                                throw new BadRequestException(MessageCode.CLAIMS_1114);
                                        }
                                    }

                                } catch (NumberFormatException e) {
                                    error.append("Il campo Tipologia Affidatario contiene un valore errato. ");
                                }
                            }
                        }
                    }
                }
                if (!error.toString().equalsIgnoreCase("")) {

                    wasteFileMassiveEntrustAttributes.setSxFleet(element.get(0));
                    wasteFileMassiveEntrustAttributes.setDataDelSx(element.get(1));
                    wasteFileMassiveEntrustAttributes.setTargaVeicoloAssicurato(element.get(2));
                    wasteFileMassiveEntrustAttributes.setTipologiaAffidatario(element.get(3));
                    wasteFileMassiveEntrustAttributes.setCodAffidatario(element.get(4));
                    wasteFileMassiveEntrustAttributes.setNote(error.toString());

                    stringaFileDiScarto = this.insertInMassiveEntrustWasteFile(wasteFileMassiveEntrustAttributes, stringaFileDiScarto);
                    error = new StringBuilder();

                }

            } else {

                LOGGER.error("il numero degli elementi della riga non rispetta il formato corretto. ");
            }


        }

        return stringaFileDiScarto;
    }

    //TO da testara
    //REFACTOR
    /*PAGAMENTI LEGALE*/
    private String lawyerPaymentsImport(List<List<String>> elementList, FileImportTypeEnum fileImportType) {

        StringBuilder error = new StringBuilder();
        String stringaFileDiScarto = null;

        elementList.removeIf(att -> att.size() != 14);

        for (List<String> element : elementList) {

            if (element.size() > 1 && element.size() < 15) {

                ClaimsNewEntity claimsNewEntity = null;
                ClaimsEntity claimsEntity = null;

                WasteFileLawyerPaymentsAttribute wasteFileLawyerPaymentsAttribute = new WasteFileLawyerPaymentsAttribute();

                //CONTROLLO DELL'ID PRATICA
                String practiceId = element.get(2);

                if (practiceId.equals("")) {
                    error.append("Il campo IDENTIFICATIVO SINISTRO e' vuoto. ");
                } else {

                    //RECUPERO CLAIMS TRAMITE ID PARTICA PRESENTE SUL FILE DI IMPORT
                    claimsNewEntity = claimsNewRepository.getClaimsByIdPratica(Long.valueOf(practiceId));

                    if (claimsNewEntity == null) {
                        error.append("IDENTIFICATIVO SINISTRO non presente. ");

                    } else {
                        //funzione di conversione
                        claimsEntity = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntity);

                        //CONSIDERO SEMPRE SINISTRI VALIDATI?
                        if (claimsEntity.getStatus() != null && claimsEntity.getStatus().equals(ClaimsStatusEnum.DRAFT) ||
                                claimsEntity.getStatus().equals(ClaimsStatusEnum.INCOMPLETE) ||
                                claimsEntity.getStatus().equals(ClaimsStatusEnum.WAITING_FOR_VALIDATION) ||
                                claimsEntity.getStatus().equals(ClaimsStatusEnum.REJECTED)
                        ) {
                            error.append("Il sinistro non e' stato validato. ");
                        }

                        Damaged damaged = claimsEntity.getDamaged();


                        //CONTROLLO DELLA TARGA DA DAMAGED -> VEHICLE
                        String plate = element.get(1);
                        if (plate.equals(""))
                            error.append("Il campo TARGA e' vuoto. ");
                        else if (damaged != null && damaged.getVehicle() != null && !damaged.getVehicle().getLicensePlate().equalsIgnoreCase(plate))
                            error.append("La targa non corrisponde con quella individuata nel claim. ");

                        //CONTROLLO DELLA DATE DEL SINISTRO
                        String dateAccident = element.get(3);
                        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                        formatter.setTimeZone(TimeZone.getTimeZone(timeZone));
                        String dataSinistro = null
                                ;
                        if (claimsEntity.getComplaint() == null || claimsEntity.getComplaint().getDataAccident() == null) {
                            error.append("La data del sinistro nel claim non è valorizzata. ");
                        } else {

                            dataSinistro = formatter.format(claimsEntity.getComplaint().getDataAccident().getDateAccident());

                            if (dateAccident.equals(""))
                                error.append("Il campo DATA SINISTRO e' vuoto. ");
                            else {
                                try {
                                    Date checkFileDate = formatter.parse(dateAccident);

                                    if (!dataSinistro.equals(dateAccident))
                                        error.append("La data del sinistro non concide con la data individuata nel claim. ");

                                } catch (ParseException e) {
                                    error.append("La data del sinistro non rispetta il formato previsto. ");
                                }
                            }
                        }

                        /**CONTROLLI SU TUTTI I CAMPI DEL DOCUMENTO**/
                        /*
                        //CONTROLLO IL CAMPO DANNO
                        String dannoString = element.get(4);
                        if(dataSinistro == null)
                            error.append("Il campo DANNO SINISTRO e' vuoto. ");

                        //CONTROLLO IL CAMPO FERMO TECNICO
                        String fermoTecnicoString = element.get(5);
                        if(fermoTecnicoString == null)
                            error.append("Il campo FERMO TECNICO SINISTRO e' vuoto. ");

                        //CONTROLLO IL CAMPO ONORARI TECNICO
                        String onorariString = element.get(6);
                        if(onorariString == null)
                            error.append("Il campo ONORARI SINISTRO e' vuoto. ");

                        //CONTROLLO IL CAMPO Spese Causa
                        String speseCausaString = element.get(7);
                        if(speseCausaString == null)
                            error.append("Il campo Spese Causa e' vuoto. ");

                        //CONTROLLO IL CAMPO ACC/SALDO
                        String accSaldoString = element.get(8);
                        if(accSaldoString == null)
                            error.append("Il campo ACC/SALDO e' vuoto. ");

                        //CONTROLLO IL CAMPO IMPORTO
                        String importoString = element.get(9);
                        if(importoString == null)
                            error.append("Il campo IMPORTO " + fileImportType.getValue().toUpperCase() + " e' vuoto. ");

                        //CONTROLLO IL CAMPO NUMERO
                        String numeroTransazioneString = element.get(10);
                        if(numeroTransazioneString == null)
                            error.append("Il campo NUMERO " + fileImportType.getValue().toUpperCase() + " e' vuoto. ");

                        //CONTROLLO IL CAMPO BANCA
                        String bancaString = element.get(11);
                        if(bancaString == null)
                            error.append("Il campo BANCA e' vuoto. ");

                        //CONTROLLO SE IL CAMPO TIPOLOGIA AFFIDATARIO NON SIA VUOTO
                        String codiceAvvocato = element.get(13);
                        if (codiceAvvocato == null ||
                                codiceAvvocato.equalsIgnoreCase("")) {
                            error.append("Il campo Codice Avvocato è vuoto. ");
                        }else{

                            Long codAffidatario = Long.parseLong(codiceAvvocato);

                            if(legalRepository.searchLegalbyCode(codAffidatario) == null)
                                error.append("Il campo Codice Avvocato non corrisponde a nessun LEGALE. ");
                        }

                         */
                    }
                }

                if (!error.toString().equalsIgnoreCase("")) {

                    wasteFileLawyerPaymentsAttribute.setNum(element.get(0));
                    wasteFileLawyerPaymentsAttribute.setTarga(element.get(1));
                    wasteFileLawyerPaymentsAttribute.setIdentificativoSinistro(element.get(2));
                    wasteFileLawyerPaymentsAttribute.setDataSinistro(element.get(3));
                    wasteFileLawyerPaymentsAttribute.setDanno(element.get(4));
                    wasteFileLawyerPaymentsAttribute.setFermoTecnico(element.get(5));
                    wasteFileLawyerPaymentsAttribute.setOnorari(element.get(6));
                    wasteFileLawyerPaymentsAttribute.setSpeseCausa(element.get(7));
                    wasteFileLawyerPaymentsAttribute.setAccSaldo(element.get(8));
                    wasteFileLawyerPaymentsAttribute.setImporto(element.get(9));
                    wasteFileLawyerPaymentsAttribute.setNumeroTransazione(element.get(10));
                    wasteFileLawyerPaymentsAttribute.setBanca(element.get(11));
                    wasteFileLawyerPaymentsAttribute.setNote(element.get(12));
                    wasteFileLawyerPaymentsAttribute.setCodiceAvvocato(element.get(13));
                    wasteFileLawyerPaymentsAttribute.setNoteScarto(error.toString());

                    stringaFileDiScarto = this.insertInLawyerPaymentsWasteFile(wasteFileLawyerPaymentsAttribute, stringaFileDiScarto, fileImportType);
                    error = new StringBuilder();

                }
            } else {
                LOGGER.error("il numero degli elementi della riga non rispetta il formato corretto. ");
            }
        }
        return stringaFileDiScarto;
    }

    /**
     * ------ METODI RICHIAMATI DAL JOB PER ESEGUIRE L'INSERIMENTO DELLE INFORMAZIONI IMPORTATE TRAMITE FILE ------
     **/

    @Override
    public void startJob(FileImportTypeEnum fileImportType) {

        List<DataManagementImportEntity> dataManagementImportList = dataManagementImportRepository.getDataManagement(fileImportType);

        for (DataManagementImportEntity dataManagement : dataManagementImportList) {

            try {
                if (dataManagement.getResult().equalsIgnoreCase("WAITING_FOR_PROCESSING") || dataManagement.getResult().equalsIgnoreCase("WAITING_FOR_PROCESSING_WITH_WASTE")) {

                    try {

                        String uuidAttachmentProcessingFile = null;

                        //Filtro i file processati, andando a considerare il file cariato e non quello generato come scarto
                        for (ProcessingFile processingFile : dataManagement.getProcessingFiles()) {

                            if (processingFile.getFileName().indexOf("Scarto") < 0) {
                                uuidAttachmentProcessingFile = processingFile.getAttachmentId().toString();
                            }
                        }

                        DownloadFileBase64ResponseV1 downloadFileBase64Response = fileManagerService.downloadFile(uuidAttachmentProcessingFile);

                        //recupero file da base64 e lo decodifico in stringa
                        List<List<String>> fileCsv = uploadFileService.decodeFileFromBase64(downloadFileBase64Response.getFileContent());
                        fileCsv.remove(0);

                        switch (fileImportType) {

                            case FRANCHISE_1:
                                claimsAttachFranchigia1(fileCsv);
                                break;

                            case FRANCHISE_2:
                                claimsAttachFranchigia2(fileCsv);
                                break;

                            case NUMBER_SX:
                                claimsAttachNumeroSinistro(fileCsv);
                                break;

                            case MASSIVE_ENTRUST:
                                claimsAttachMassiveEntrust(fileCsv);
                                break;

                            case ALLOWANCE:
                            case BANK_TRANSFER:
                                paymentLawyerAttach(fileCsv, fileImportType);
                                break;
                            default:
                                LOGGER.debug(MessageCode.CLAIMS_1113.value());
                                throw new BadRequestException(MessageCode.CLAIMS_1113);
                        }

                        /*if (dataManagement.getFileType().equals(FileImportTypeEnum.FRANCHIGIA_1)) {
                            claimsAttachFranchigia1(fileCsv);
                        } else if (dataManagement.getFileType().equals(FileImportTypeEnum.FRANCHIGIA_2)) {
                            claimsAttachFranchigia2(fileCsv);
                        } else if (dataManagement.getFileType().equals(FileImportTypeEnum.NUMERO_SX)) {
                            claimsAttachNumeroSinistro(fileCsv);
                        }*/

                    } catch (IOException e) {
                        LOGGER.debug(e.getMessage());
                        throw new InternalException(e.getMessage(), e);
                    }
                }

                if (dataManagement.getResult().equalsIgnoreCase("WAITING_FOR_PROCESSING")) {
                    dataManagement.setResult("PROCESSED");
                } else if (dataManagement.getResult().equalsIgnoreCase("WAITING_FOR_PROCESSING_WITH_WASTE")) {
                    dataManagement.setResult("PROCESSED_WITH_WASTE");
                }
                dataManagementImportRepository.save(dataManagement);

            } catch (InternalException e) {

                dataManagement.setResult("ERROR");
                dataManagementImportRepository.save(dataManagement);
            }
        }
    }

    @Override
    public void massiveEntrust(FileImportTypeEnum fileImportType) {

        List<DataManagementImportEntity> dataManagementImportList = dataManagementImportRepository.getDataManagement(fileImportType);

        for (DataManagementImportEntity dataManagement : dataManagementImportList) {

            try {
                if (dataManagement.getResult().equalsIgnoreCase("WAITING_FOR_PROCESSING") || dataManagement.getResult().equalsIgnoreCase("WAITING_FOR_PROCESSING_WITH_WASTE")) {

                    try {

                        String uuidAttachmentProcessingFile = null;

                        //Filtro i file processati, andando a considerare il file cariato e non quello generato come scarto
                        for (ProcessingFile processingFile : dataManagement.getProcessingFiles()) {

                            if (processingFile.getFileName().indexOf("Scarto") < 0) {
                                uuidAttachmentProcessingFile = processingFile.getAttachmentId().toString();
                            }
                        }

                        DownloadFileBase64ResponseV1 downloadFileBase64Response = fileManagerService.downloadFile(uuidAttachmentProcessingFile);

                        //recupero file da base64 e lo decodifico in stringa
                        List<List<String>> fileCsv = uploadFileService.decodeFileFromBase64(downloadFileBase64Response.getFileContent());
                        fileCsv.remove(0);

                        //Importo il contenuto del file
                        claimsAttachMassiveEntrust(fileCsv);

                    } catch (IOException e) {
                        LOGGER.debug(e.getMessage());
                        throw new InternalException(e.getMessage(), e);
                    }
                }

                if (dataManagement.getResult().equalsIgnoreCase("WAITING_FOR_PROCESSING")) {
                    dataManagement.setResult("PROCESSED");
                } else if (dataManagement.getResult().equalsIgnoreCase("WAITING_FOR_PROCESSING_WITH_WASTE")) {
                    dataManagement.setResult("PROCESSED_WITH_WASTE");
                }
                dataManagementImportRepository.save(dataManagement);

            } catch (InternalException e) {
                dataManagement.setResult("ERROR");
                dataManagementImportRepository.save(dataManagement);
            }
        }
    }

    /**
     * ------------- METODI CHE, A SEGUITO DEL JOB, ALLEGANO IL RISULTATO DEL FILE DI IMPORT AL CLAIMS -------------
     **/

    //REFACTOR
    /*FRANCHIGIA 1*/
    private void claimsAttachFranchigia1(List<List<String>> fileCsv) {


        fileCsv.removeIf(att -> Arrays.asList(att.get(0).split(";", -1)).size() != 11);

        for (List<String> currentoRow : fileCsv) {

            List<String> column = Arrays.asList(currentoRow.get(0).split(";", -1));
            String idPratica = column.get(0);
            ContractTypeEntity contractType = null;
            ClaimsNewEntity claimsNewEntity = null;
            ClaimsEntity claimsEntity = null;

            try {

                if (!idPratica.equals("")) {

                //RECUPERO CLAIMS TRAMITE ID PARTICA PRESENTE SUL FILE DI IMPORT
                claimsNewEntity = claimsNewRepository.getClaimsByIdPratica(Long.valueOf(idPratica));
                //funzione di conversione
                claimsEntity = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntity);

                if (claimsEntity != null &&
                        claimsEntity.getStatus() != null &&
                        !claimsEntity.getStatus().equals(ClaimsStatusEnum.DRAFT) &&
                        !claimsEntity.getStatus().equals(ClaimsStatusEnum.INCOMPLETE) &&
                        !claimsEntity.getStatus().equals(ClaimsStatusEnum.WAITING_FOR_VALIDATION)
                ) {

                    Damaged damaged = claimsEntity.getDamaged();

                    if (damaged != null && damaged.getContract() != null && damaged.getContract().getContractType() != null)
                        contractType = contractTypeRepository.findByCodContract(claimsEntity.getDamaged().getContract().getContractType());

                    //CONTROLLO DELLA TARGA DA DAMAGED -> VEHICLE
                    String plate = column.get(2);
                    if (contractType != null &&
                            contractType.getFranchise() != null &&
                            !plate.equals("") &&
                            damaged.getVehicle() != null &&
                            damaged.getVehicle().getLicensePlate().equalsIgnoreCase(plate)) {

                        String dcsItalia = column.get(3);
                        if (dcsItalia != null && !dcsItalia.equalsIgnoreCase("")) {

                            //CONTROLLO SE IL CAMPO DATE DEL SINISTRO NEL FILE NON SIA VUOTO
                            String dateAccident = column.get(1);
                            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
                            LocalDateTime dataSinistro = null;

                            if (claimsEntity.getComplaint() != null && claimsEntity.getComplaint().getDataAccident() != null) {

                                dataSinistro = LocalDateTime.ofInstant(claimsEntity.getComplaint().getDataAccident().getDateAccident().toInstant(), ZoneOffset.UTC);
                                dataSinistro = dataSinistro.plusHours(2);

                                if (!dateAccident.equals("") && dataSinistro.toLocalDate().isEqual(LocalDate.from(formatter.parse(dateAccident)))) {

                                    //CONTROLLO SE IL CAMPO RIFERIMENTO ALLA COMPAGNIA NEL FILE NON SIA VUOTO
                                    String companyReference = column.get(3);
                                    if (!companyReference.equals("")) {

                                        //CONTROLLO SE IL CAMPO DATA DI CHIUSURA NEL FILE NON SIA VUOTO E CHE SIA MINORE DI OGGI
                                        String dataChiusura = column.get(5);

                                            LocalDate oggi = LocalDate.now();
                                            LocalDate dataChiusuraCal = LocalDate.from(formatter.parse(dataChiusura));

                                        //Data chiusura sempre maggiore della data del sinistro
                                        //Datat chiusura sempre minore di oggi
                                        if (dataChiusura != null && !dataChiusura.equals("") && dataChiusuraCal.compareTo(dataSinistro.toLocalDate()) > 0 && dataChiusuraCal.compareTo(oggi) < 0) {

                                            String policySuffix = column.get(10);

                                            if (policySuffix != null) {

                                                PolicySuffixEnum policySuffixEnum = PolicySuffixEnum.create(policySuffix);
                                                Double checkFranchise = null;

                                                //Controllo la franchigia relativa al contratto
                                                if (policySuffixEnum.equals(PolicySuffixEnum.C)) {
                                                    checkFranchise = contractType.getFranchise();

                                                }
                                                //Controllo la franchigia presente nella polizza relativa al contratto
                                                else if (policySuffixEnum.equals(PolicySuffixEnum.P)) {

                                                    InsuranceCompany insuranceCompany = damaged.getInsuranceCompany();

                                                    if (insuranceCompany != null && insuranceCompany.getTpl() != null && insuranceCompany.getTpl().getInsurancePolicyId() != null) {

                                                        Tpl tpl = insuranceCompany.getTpl();
                                                        InsurancePolicyEntity policy = insurancePolicyRepository.getOne(tpl.getInsurancePolicyId());
                                                        checkFranchise = policy.getFranchise();
                                                    }
                                                }

                                                if (checkFranchise != null) {

                                                    //CONTROLLO SE IL CAMPO IMPORTO FF PAGAMENTO SX NEL FILE NON SIA VUOTO,
                                                    //E CHE SIA MINORE DELLA FRANCHIGIA PREVISTA DAL CONTRATTO
                                                    String importoFF = column.get(6);
                                                    if (importoFF != null &&
                                                            !importoFF.equals("") &&
                                                            (Double.parseDouble(importoFF)) >= 0 &&
                                                            (Double.parseDouble(importoFF)) <= checkFranchise) {

                                                        //CONTROLLO SE IL CAMPO NR POLIZZA NEL FILE NON SIA VUOTO
                                                        String policyNumber = column.get(9);
                                                        if (!policyNumber.equals("")) {

                                                            //CONTROLO SE IL CAMPO TOTALE PAGATO NEL FILE NON SIA VUOTO
                                                            String totalPaid = column.get(7);
                                                            if (!totalPaid.equals("") /*&& Double.valueOf(totalPaid) >= 0*/) {

                                                                Exemption exemption = new Exemption();
                                                                exemption.setCompanyReference(companyReference);
                                                                exemption.setDeductEnd1(Date.from(dataChiusuraCal.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()));
                                                                exemption.setDeductPaid1(Double.valueOf(importoFF));
                                                                exemption.setPolicyNumber(policyNumber);
                                                                exemption.setDeductTotalPaid(Double.valueOf(importoFF));
                                                                exemption.setDeductImported1(DateUtil.getNowDate());

                                                                claimsEntity.setExemption(exemption);

                                                                if (claimsEntity.getHistorical() == null)
                                                                    claimsEntity.setHistorical(new LinkedList<>());

                                                                Historical historical = new Historical();

                                                                historical.setEventType(EventTypeEnum.IMPORT_FRANCHISE_1);
                                                                historical.setNewFlow(claimsEntity.getType().toValue());
                                                                historical.setOldFlow(claimsEntity.getType().toValue());
                                                                historical.setNewType(claimsEntity.getComplaint().getDataAccident().getTypeAccident());
                                                                historical.setOldType(claimsEntity.getComplaint().getDataAccident().getTypeAccident());
                                                                historical.setStatusEntityNew(claimsEntity.getStatus());
                                                                historical.setStatusEntityOld(claimsEntity.getStatus());
                                                                historical.setUserName(ClaimsHistoricalUserEnum.SYSTEM_IMPORT_EXEMPTION_1.getValue());
                                                                historical.setComunicationDescription("Import file franchigia 1");
                                                                historical.setUpdateAt(DateUtil.getNowDate());

                                                                    List<Historical> historicalList = claimsEntity.getHistorical();
                                                                    historicalList.add(historical);
                                                                    claimsEntity.setHistorical(historicalList);
                                                                    claimsNewEntity = converterClaimsService.convertOldEntityToNewClaimsEntity(claimsEntity);
                                                                    claimsNewRepository.save(claimsNewEntity);
                                                                    if (dwhCall) {
                                                                        dwhClaimsService.sendMessage(claimsEntity, com.doing.nemo.dwh.client.payload.request.EventTypeEnum.UPDATED);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (Exception ex) {
                LOGGER.debug(ex.getMessage());
            }
        }
    }

    //REFACTOR
    /*FRANCHIGIA 2*/
    private void claimsAttachFranchigia2(List<List<String>> fileCsv) {

        fileCsv.removeIf(att -> Arrays.asList(att.get(0).split(";", -1)).size() != 13);

        for (List<String> currentoRow : fileCsv) {

            List<String> column = Arrays.asList(currentoRow.get(0).split(";", -1));
            String idPratica = column.get(0);
            ContractTypeEntity contractType = null;
            ClaimsNewEntity claimsNewEntity = null;
            ClaimsEntity claimsEntity = null;

            try{

            if (!idPratica.equals("")) {

                //RECUPERO CLAIMS TRAMITE ID PARTICA PRESENTE SUL FILE DI IMPORT
                claimsNewEntity = claimsNewRepository.getClaimsByIdPratica(Long.valueOf(idPratica));
                //funzione di conversione
                claimsEntity = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntity);

                if (claimsEntity != null &&
                        claimsEntity.getStatus() != null &&
                        !claimsEntity.getStatus().equals(ClaimsStatusEnum.DRAFT) &&
                        !claimsEntity.getStatus().equals(ClaimsStatusEnum.INCOMPLETE) &&
                        !claimsEntity.getStatus().equals(ClaimsStatusEnum.WAITING_FOR_VALIDATION)
                ) {

                    Damaged damaged = claimsEntity.getDamaged();

                    if (damaged != null && damaged.getContract() != null && damaged.getContract().getContractType() != null)
                        contractType = contractTypeRepository.findByCodContract(claimsEntity.getDamaged().getContract().getContractType());

                    //CONTROLLO SE SONO PRESENTI I DATI RIGUARDANTI LA FRANCHIGIA1
                    if (contractType != null &&
                            contractType.getFranchise() != null &&
                            claimsEntity.getExemption() != null &&
                            claimsEntity.getExemption().getDeductPaid1() != null &&
                            claimsEntity.getExemption().getDeductEnd1() != null &&
                            claimsEntity.getExemption().getDeductImported1() != null
                    ) {

                        //CONTROLLO DELLA TARGA DA DAMAGED -> VEHICLE
                        String plate = column.get(2);
                        if (!plate.equals("") &&
                                damaged.getVehicle() != null &&
                                damaged.getVehicle().getLicensePlate().equalsIgnoreCase(plate)) {

                            //CONTROLLO DELLA DATE DEL SINISTRO
                            String dateAccident = column.get(1);
                            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy").withZone(ZoneId.of(timeZone));
                            LocalDateTime dataSinistro = null;

                            if (claimsEntity.getComplaint() != null && claimsEntity.getComplaint().getDataAccident() != null) {

                                dataSinistro = LocalDateTime.ofInstant(claimsEntity.getComplaint().getDataAccident().getDateAccident().toInstant(), ZoneId.of(timeZone));



                                if (dateAccident != null &&
                                        !dateAccident.equals("") &&
                                        dataSinistro.toLocalDate().isEqual(LocalDate.from(formatter.parse(dateAccident)))) {

                                    //CONTROLLO CHE L'IMPORTED2 NON SIA GIA' VALORIZZATO
                                    if (claimsEntity.getExemption().getDeductImported2() == null) {

                                        //CONTROLLO SE IL CAMPO DATA DI CHIUSURA NEL FILE NON SIA VUOTO
                                        String dataChiusura = column.get(5);
                                        LocalDate oggi = LocalDate.now();
                                        LocalDate dataChiusuraCal = LocalDate.from(formatter.parse(dataChiusura));

                                        if (dataChiusura != null && !dataChiusura.equals("") && dataChiusuraCal.compareTo(dataSinistro.toLocalDate()) > 0 && dataChiusuraCal.compareTo(oggi) < 0) {

                                            formatter.parse(dataChiusura);

                                            String importoExtraSS = column.get(6);
                                            if (importoExtraSS != null &&
                                                    !importoExtraSS.equalsIgnoreCase("") &&
                                                    claimsEntity.getExemption().getDeductImported2() == null) {


                                                String policySuffix = column.get(12);

                                                if (policySuffix != null) {

                                                    PolicySuffixEnum policySuffixEnum = PolicySuffixEnum.create(policySuffix);
                                                    Double checkFranchise = null;

                                                    //Controllo la franchigia relativa al contratto
                                                    if (policySuffixEnum.equals(PolicySuffixEnum.C)) {
                                                        checkFranchise = contractType.getFranchise();

                                                    }
                                                    //Controllo la franchigia presente nella polizza relativa al contratto
                                                    else if (policySuffixEnum.equals(PolicySuffixEnum.P)) {

                                                        InsuranceCompany insuranceCompany = damaged.getInsuranceCompany();

                                                        if (insuranceCompany != null && insuranceCompany.getTpl() != null && insuranceCompany.getTpl().getInsurancePolicyId() != null) {

                                                            Tpl tpl = insuranceCompany.getTpl();
                                                            InsurancePolicyEntity policy = insurancePolicyRepository.getOne(tpl.getInsurancePolicyId());
                                                            checkFranchise = policy.getFranchise();
                                                        }
                                                    }

                                                    if (checkFranchise != null) {

                                                        //CONTROLLO SE IL CAMPO IMPORTO EXTRA FF PAGAMENTO SX NEL FILE NON SIA VUOTO
                                                        String importoFF = column.get(8);
                                                        String importFFpagamentoSx = column.get(7);
                                                        if (importoFF != null &&
                                                                !importoFF.equals("") &&
                                                                claimsEntity.getExemption().getDeductPaid1() != null &&
                                                                (BigDecimal.valueOf(Double.parseDouble(importoFF)).equals(BigDecimal.valueOf(claimsEntity.getExemption().getDeductPaid1()))) &&
                                                                (Double.parseDouble(importoFF) + Double.parseDouble(importoExtraSS)) <= checkFranchise &&
                                                                importFFpagamentoSx != null && !importFFpagamentoSx.equalsIgnoreCase("") &&
                                                                (BigDecimal.valueOf(Double.parseDouble(importoFF)).add(BigDecimal.valueOf(Double.parseDouble(importoExtraSS))).equals(BigDecimal.valueOf(Double.parseDouble(importFFpagamentoSx))))
                                                        ) {

                                                            //CONTROLO SE IL CAMPO TOTALE NEL FILE NON SIA VUOTO
                                                            String totalPaid = column.get(7);
                                                            if (totalPaid != null &&
                                                                    !totalPaid.equals("") /*&&
                                                            Double.valueOf(totalPaid) <= contractType.getFranchise()*/) {

                                                                claimsEntity.getExemption().setDeductPaid2(Double.parseDouble(importoExtraSS));
                                                                claimsEntity.getExemption().setDeductEnd2(Date.from(dataChiusuraCal.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()));
                                                                claimsEntity.getExemption().setDeductTotalPaid(claimsEntity.getExemption().getDeductPaid1() + claimsEntity.getExemption().getDeductPaid2());
                                                                claimsEntity.getExemption().setDeductImported2(DateUtil.getNowDate());

                                                                if (claimsEntity.getHistorical() == null)
                                                                    claimsEntity.setHistorical(new LinkedList<>());

                                                                Historical historical = new Historical();

                                                                historical.setEventType(EventTypeEnum.IMPORT_FRANCHISE_2);
                                                                historical.setNewFlow(claimsEntity.getType().toValue());
                                                                historical.setOldFlow(claimsEntity.getType().toValue());
                                                                historical.setNewType(claimsEntity.getComplaint().getDataAccident().getTypeAccident());
                                                                historical.setOldType(claimsEntity.getComplaint().getDataAccident().getTypeAccident());
                                                                historical.setStatusEntityNew(claimsEntity.getStatus());
                                                                historical.setStatusEntityOld(claimsEntity.getStatus());
                                                                historical.setUserName(ClaimsHistoricalUserEnum.SYSTEM_IMPORT_EXEMPTION_2.getValue());
                                                                historical.setComunicationDescription("Import file franchigia 2");
                                                                historical.setUpdateAt(DateUtil.getNowDate());

                                                                List<Historical> historicalList = claimsEntity.getHistorical();
                                                                historicalList.add(historical);
                                                                claimsEntity.setHistorical(historicalList);

                                                                claimsNewEntity = converterClaimsService.convertOldEntityToNewClaimsEntity(claimsEntity);
                                                                claimsNewRepository.save(claimsNewEntity);
                                                                if (dwhCall) {
                                                                    dwhClaimsService.sendMessage(claimsEntity, com.doing.nemo.dwh.client.payload.request.EventTypeEnum.UPDATED);
                                                                }
                                                            }

                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            LOGGER.debug(ex.getMessage());
        }
        }
    }

    /*NUMERO SX*/
    private void claimsAttachNumeroSinistro(List<List<String>> fileCsv) {

        fileCsv.removeIf(att -> Arrays.asList(att.get(0).split(";", -1)).size() != 9);

        for (List<String> currentRow : fileCsv) {

            List<String> column = Arrays.asList(currentRow.get(0).split(";", -1));
            String idPratica = column.get(0);
            ClaimsNewEntity claimsNewEntity = null;
            ClaimsEntity claimsEntity = null;

            try{

            if (idPratica != null && !idPratica.equals("")) {

                //RECUPERO CLAIMS TRAMITE ID PARTICA PRESENTE SUL FILE DI IMPORT
                claimsNewEntity = claimsNewRepository.getClaimsByIdPratica(Long.valueOf(idPratica));
                //funzione di conversione
                claimsEntity = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntity);

                if (claimsEntity != null &&
                        claimsEntity.getStatus() != null &&
                        !claimsEntity.getStatus().equals(ClaimsStatusEnum.DRAFT) &&
                        !claimsEntity.getStatus().equals(ClaimsStatusEnum.INCOMPLETE) &&
                        !claimsEntity.getStatus().equals(ClaimsStatusEnum.WAITING_FOR_VALIDATION)
                ) {
                    String plate = column.get(5);

                    //CONTROLLO LA TARGA
                    if (!plate.equals("") &&
                            claimsEntity.getDamaged().getVehicle() != null &&
                            claimsEntity.getDamaged().getVehicle().getLicensePlate().equalsIgnoreCase(plate)) {

                        String dateAccident = column.get(3);
                        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy").withZone(ZoneId.of(timeZone));
                        LocalDateTime dataSinistro = null;

                        if (claimsEntity.getComplaint() != null && claimsEntity.getComplaint().getDataAccident() != null) {

                            dataSinistro = LocalDateTime.ofInstant(claimsEntity.getComplaint().getDataAccident().getDateAccident().toInstant(), ZoneId.of(timeZone));


                            LocalDate dataAccidentCal = LocalDate.from(formatter.parse(dateAccident));


                            //CONTROLLO SE LA DATA DELL'INCIDENTE COMBACIA
                            if (dateAccident != null &&
                                    !dateAccident.equals("") &&
                                    dataAccidentCal.isEqual(dataSinistro.toLocalDate())) {

                                String numeroCompagnia = column.get(1);
                                if (!numeroCompagnia.equals("")) {

                                    FromCompany fromCompany = claimsEntity.getComplaint().getFromCompany();
                                    if (fromCompany == null) {
                                        fromCompany = new FromCompany();
                                    }

                                    if ((fromCompany.getNumberSx() == null)  || ( fromCompany.getNumberSx().isEmpty() ) ||
                                            (
                                                    claimsEntity.getComplaint().getFromCompany().getNumberSx() != null &&
                                                            claimsEntity.getComplaint().getFromCompany().getNumberSx().equalsIgnoreCase(numeroCompagnia)
                                            )
                                    ) {

                                        String numeroPolizza = column.get(2);
                                        String tipoDanno = column.get(4);

                                        if (tipoDanno != null && !tipoDanno.equalsIgnoreCase("")) {

                                            try {

                                                DataAccidentTypeAccidentEnum accidentType = DataAccidentTypeAccidentEnum.create(tipoDanno.toLowerCase());

                                                String ispettoratoCompetente = column.get(6);
                                                String peritoIncaricato = column.get(7);
                                                String noteMarsh = column.get(8);

                                                if (peritoIncaricato != null && !peritoIncaricato.equalsIgnoreCase("")) {
                                                    String result = fromCompany.getExpert();
                                                    if (result != null && !result.equals("")) {
                                                        fromCompany.setExpert(result + ";" + peritoIncaricato);
                                                    } else {
                                                        fromCompany.setExpert(peritoIncaricato);
                                                    }

                                                }

                                                if (ispettoratoCompetente != null && !ispettoratoCompetente.equalsIgnoreCase("")) {

                                                    String result = fromCompany.getInspectorate();
                                                    if (result != null && !result.equals("")) {
                                                        fromCompany.setInspectorate(result + ";" + ispettoratoCompetente);
                                                    } else {
                                                        fromCompany.setInspectorate(ispettoratoCompetente);
                                                    }

                                                }

                                                if (noteMarsh != null && !noteMarsh.equalsIgnoreCase("")) {

                                                    String result = fromCompany.getNote();
                                                    if (result != null && !result.equals("")) {
                                                        fromCompany.setNote(result + ";" + noteMarsh);
                                                    } else {
                                                        fromCompany.setNote(noteMarsh);
                                                    }

                                                }

                                                fromCompany.setTypeSx(accidentType.getValue());
                                                fromCompany.setNumberSx(numeroCompagnia);
                                                fromCompany.setLastUpdate(DateUtil.getNowDate());

                                                claimsEntity.getComplaint().setFromCompany(fromCompany);

                                                if (claimsEntity.getHistorical() == null)
                                                    claimsEntity.setHistorical(new LinkedList<>());

                                                Historical historical = new Historical();

                                                historical.setEventType(EventTypeEnum.IMPORT_NUMBER_SX);
                                                historical.setNewFlow(claimsEntity.getType().toValue());
                                                historical.setOldFlow(claimsEntity.getType().toValue());
                                                historical.setNewType(claimsEntity.getComplaint().getDataAccident().getTypeAccident());
                                                historical.setOldType(claimsEntity.getComplaint().getDataAccident().getTypeAccident());
                                                historical.setStatusEntityNew(claimsEntity.getStatus());
                                                historical.setStatusEntityOld(claimsEntity.getStatus());
                                                historical.setUserName(ClaimsHistoricalUserEnum.SYSTEM_IMPORT_NUM_SX.getValue());
                                                historical.setComunicationDescription("Import file numero SX ");
                                                historical.setUpdateAt(DateUtil.getNowDate());

                                                List<Historical> historicalList = claimsEntity.getHistorical();
                                                historicalList.add(historical);
                                                claimsEntity.setHistorical(historicalList);
                                                if (claimsEntity.getStatus().equals(ClaimsStatusEnum.WAITING_FOR_AUTHORITY) &&  ClaimsServiceImpl.isAuthorityAndSxNumber(claimsEntity)) {
                                                    ClaimsStatusEnum statusOld = claimsEntity.getStatus();
                                                    ClaimsStatusEnum nextStatus = null;
                                                    if (claimsEntity.getType().equals(ClaimsFlowEnum.FNI)) {
                                                        nextStatus = ClaimsStatusEnum.TO_SEND_TO_CUSTOMER;
                                                    } else {
                                                        nextStatus = ClaimsStatusEnum.TO_ENTRUST;
                                                    }
                                                    claimsEntity.setStatus(nextStatus);
                                                    LOGGER.debug("[IMPORT NUM SX]  change status in " + claimsEntity.getStatus());
                                                    String description = "Lo stato è cambiato da " + ClaimsAdapter.adptClaimsStatusEnumToItalian(statusOld) + " a " + ClaimsAdapter.adptClaimsStatusEnumToItalian(claimsEntity.getStatus());
                                                    Historical historicalChangeStatus = new Historical(EventTypeEnum.EDIT_PRACTICE_DATA, statusOld, claimsEntity.getStatus(), claimsEntity.getComplaint().getDataAccident().getTypeAccident(), claimsEntity.getComplaint().getDataAccident().getTypeAccident(), DateUtil.getNowDate(), null, ClaimsHistoricalUserEnum.SYSTEM_TO_ENTRUST.getValue(), claimsEntity.getType().getValue(), claimsEntity.getType().getValue(), description);
                                                    claimsEntity.addHistorical(historicalChangeStatus);
                                                }

                                                claimsNewEntity = converterClaimsService.convertOldEntityToNewClaimsEntity(claimsEntity);
                                                claimsNewRepository.save(claimsNewEntity);
                                                if (dwhCall) {
                                                    dwhClaimsService.sendMessage(claimsEntity, com.doing.nemo.dwh.client.payload.request.EventTypeEnum.UPDATED);
                                                }


                                            } catch (BadParametersException ex) {
                                                LOGGER.error("ERROR: " + ex.getMessage() + ", the type accident present into file is " + tipoDanno);

                                            }

                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            LOGGER.debug(ex.getMessage());
        }
        }
    }

    //REFACTOR
    /*AFFIDO MASSIVO*/
    private void claimsAttachMassiveEntrust(List<List<String>> fileCsv) {

        fileCsv.removeIf(att -> Arrays.asList(att.get(0).split(";", -1)).size() != 5);

        for (List<String> currentoRow : fileCsv) {

            List<String> column = Arrays.asList(currentoRow.get(0).split(";", -1));
            ClaimsEntity claimsEntity = null;
            ClaimsNewEntity claimsNewEntity = null;

            //CONTROLLO DELL'ID PRATICA
            String practiceId = column.get(0);
            if (!practiceId.equals("")) {
                try {
                    //RECUPERO CLAIMS TRAMITE ID PARTICA PRESENTE SUL FILE DI IMPORT
                    claimsNewEntity = claimsNewRepository.getClaimsByIdPratica(Long.valueOf(practiceId));

                    claimsEntity = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntity);
                    ClaimsEntity oldClaims = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntity);
                    ClaimsStatusEnum oldStatus = claimsEntity.getStatus();
                    IncidentRequestV1 oldIncident = incidentAdapter.adptFromClaimsEntityToIncidentRequest(claimsEntity, oldStatus, false, false);
                    if (claimsEntity != null) {

                        if (claimsEntity.getStatus() != null && !claimsEntity.getStatus().equals(ClaimsStatusEnum.DRAFT) &&
                                !claimsEntity.getStatus().equals(ClaimsStatusEnum.INCOMPLETE) &&
                                !claimsEntity.getStatus().equals(ClaimsStatusEnum.WAITING_FOR_VALIDATION) &&
                                !claimsEntity.getStatus().equals(ClaimsStatusEnum.REJECTED) &&
                                !claimsEntity.getStatus().equals(ClaimsStatusEnum.DELETED) &&
                                !claimsEntity.getStatus().equals(ClaimsStatusEnum.WAITING_FOR_REFUND) &&
                                !claimsEntity.getStatus().equals(ClaimsStatusEnum.CLOSED_PARTIAL_REFUND) &&
                                !claimsEntity.getStatus().equals(ClaimsStatusEnum.CLOSED_TOTAL_REFUND)
                        ) {

                            Damaged damaged = claimsEntity.getDamaged();

                            //CONTROLLO DELLA TARGA DA DAMAGED -> VEHICLE
                            String plate = column.get(2);

                            if (!plate.equals("") && damaged != null && damaged.getVehicle() != null && damaged.getVehicle().getLicensePlate().equalsIgnoreCase(plate)) {

                            //CONTROLLO DELLA DATE DEL SINISTRO
                            String dateAccident = column.get(1);
                            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                            formatter.setTimeZone(TimeZone.getTimeZone(timeZone));
                            String dataSinistro = null;
                            if (claimsEntity.getComplaint() != null && claimsEntity.getComplaint().getDataAccident() != null) {

                                dataSinistro = formatter.format(claimsEntity.getComplaint().getDataAccident().getDateAccident());


                                    try {

                                    Date checkFileDate = formatter.parse(dateAccident);

                                    if (!dateAccident.equals("") && dataSinistro.equals(dateAccident)) {

                                        //CONTROLLO SE IL CAMPO TIPOLOGIA AFFIDATARIO NON SIA VUOTO
                                        String entrusted = column.get(3);
                                        if (entrusted != null &&
                                                !entrusted.equalsIgnoreCase("")
                                        ) {

                                            try {

                                                long codAffidatario = Long.parseLong(column.get(4));
                                                EntrustedEnum entrustedEnum = EntrustedEnum.create(entrusted);

                                                Entrusted entrustedClaims = claimsEntity.getComplaint().getEntrusted();
                                                if (entrustedClaims == null)
                                                    entrustedClaims = new Entrusted();

                                                if (column.get(4) != null) {

                                                    Boolean affidato = false;
                                                    EventTypeEnum entrustedTo = null;

                                                    switch (entrustedEnum) {

                                                        case LEGAL:

                                                            LegalEntity legalEntity = legalRepository.searchLegalbyCode(codAffidatario);
                                                            if (legalEntity != null) {
                                                                if (legalEntity.getActive() != null && legalEntity.getActive()) {
                                                                    entrustedClaims.setIdEntrustedTo(legalEntity.getId());
                                                                    entrustedClaims.setEntrustedTo(legalEntity.getName());
                                                                    entrustedClaims.setEntrustedEmail(legalEntity.getEmail());
                                                                    entrustedClaims.setEntrustedType(EntrustedEnum.LEGAL);
                                                                    entrustedClaims.setDescription("Affido Massivo");
                                                                    affidato = true;
                                                                    entrustedTo = EventTypeEnum.ENTRUST_TO_THE_LEGAL;
                                                                }
                                                            }
                                                            break;

                                                        case INSPECTORATE:

                                                            InspectorateEntity inspectorateEntity = inspectorateRepository.searchInspectoratebyCode(codAffidatario);
                                                            if (inspectorateEntity != null) {
                                                                if (inspectorateEntity.getActive() != null && inspectorateEntity.getActive()) {
                                                                    entrustedClaims.setIdEntrustedTo(inspectorateEntity.getId());
                                                                    entrustedClaims.setEntrustedTo(inspectorateEntity.getName());
                                                                    entrustedClaims.setEntrustedEmail(inspectorateEntity.getEmail());
                                                                    entrustedClaims.setEntrustedType(EntrustedEnum.INSPECTORATE);
                                                                    entrustedClaims.setDescription("Affido Massivo");
                                                                    affidato = true;
                                                                    entrustedTo = EventTypeEnum.ENTRUST_TO_THE_COMPANY;
                                                                }
                                                            }
                                                            break;

                                                        case MANAGER:

                                                            InsuranceManagerEntity managerEntity = insuranceManagerRepository.searchManagerByCode(codAffidatario);
                                                            if (managerEntity != null) {
                                                                if (managerEntity.getActive() != null && managerEntity.getActive()) {
                                                                    entrustedClaims.setIdEntrustedTo(managerEntity.getId());
                                                                    entrustedClaims.setEntrustedTo(managerEntity.getName());
                                                                    entrustedClaims.setEntrustedEmail(managerEntity.getEmail());
                                                                    entrustedClaims.setEntrustedType(EntrustedEnum.MANAGER);
                                                                    entrustedClaims.setDescription("Affido Massivo");
                                                                    affidato = true;
                                                                    entrustedTo = EventTypeEnum.ENTRUST_TO_THE_CLAIMS_MANAGER;
                                                                }
                                                            }
                                                            break;
                                                        default:
                                                            LOGGER.debug(MessageCode.CLAIMS_1114.value());
                                                            throw new BadRequestException(MessageCode.CLAIMS_1114);
                                                    }

                                                    entrustedClaims.setEntrustedDay(DateUtil.getNowDate());
                                                    claimsEntity.getComplaint().setEntrusted(entrustedClaims);

                                                    //bisogna prima salvare altrimenti l'email non potrà essere mandata

                                                    claimsNewEntity = converterClaimsService.convertOldEntityToNewClaimsEntity(claimsEntity);
                                                    claimsNewRepository.save(claimsNewEntity);

                                                    //Nel caso in cui riesca ad affidare
                                                    if (affidato) {

                                                        List<EmailTemplateMessagingResponseV1> emailTemplateEntity = messagingService.getMailTemplateByTypeEvent(entrustedTo, claimsEntity.getId());

                                                        String description = "Import file per eseguire un affido massivo \n";

                                                        if (emailTemplateEntity != null && !emailTemplateEntity.isEmpty()) {

                                                            List<EmailTemplateMessagingRequestV1> listRequestMail = MessagingAdapter.adptEmailTemplateMessagingRequestToEmailTemplateMessagingResponseList(emailTemplateEntity);

                                                            List<EmailTemplateMessagingRequestV1> listTos = new LinkedList<>();
                                                            List<EmailTemplateMessagingRequestV1> listNotTos = new LinkedList<>();
                                                            for (EmailTemplateMessagingRequestV1 currentTemplate : listRequestMail) {
                                                                if (currentTemplate.getTos() != null && !currentTemplate.getTos().isEmpty()) {
                                                                    listTos.add(currentTemplate);
                                                                } else {
                                                                    listNotTos.add(currentTemplate);
                                                                }
                                                            }
                                                            List<EmailTemplateMessagingRequestV1> emailTemplateMessaging = messagingService.splitEmailIfContainsUrl(listTos, claimsEntity.getId());
                                                            emailTemplateMessaging.addAll(listNotTos);

                                                            description = messagingService.sendMailAndCreateLogs(emailTemplateMessaging, null, claimsEntity.getCreatedAt());
                                                        }

                                                        Historical historical = new Historical(entrustedTo, claimsEntity.getStatus(), ClaimsStatusEnum.WAITING_FOR_REFUND, claimsEntity.getComplaint().getDataAccident().getTypeAccident(), claimsEntity.getComplaint().getDataAccident().getTypeAccident(), DateUtil.getNowDate(), null, ClaimsHistoricalUserEnum.SYSTEM_MASSIVE_ENTRUST.getValue(), claimsEntity.getType().getValue(), claimsEntity.getType().getValue(), description);

                                                        if (claimsEntity.getHistorical() == null)
                                                            claimsEntity.setHistorical(new LinkedList<>());

                                                        List<Historical> historicalList = claimsEntity.getHistorical();
                                                        historicalList.add(historical);
                                                        claimsEntity.setHistorical(historicalList);
                                                        claimsEntity.setStatus(ClaimsStatusEnum.WAITING_FOR_REFUND);

                                                        }

                                                        claimsNewEntity = converterClaimsService.convertOldEntityToNewClaimsEntity(claimsEntity);
                                                        claimsNewRepository.save(claimsNewEntity);
                                                        if (dwhCall) {
                                                            dwhClaimsService.sendMessage(claimsEntity, com.doing.nemo.dwh.client.payload.request.EventTypeEnum.UPDATED);
                                                        }
                                                        //Intervento 6 - C
                                                        externalCommunicationService.modifyIncidentAsync(claimsEntity.getId(), oldStatus, false, oldIncident, false, oldClaims);

                                                    }
                                                } catch (NumberFormatException | IOException e) {
                                                    LOGGER.error(e.getMessage());
                                                }
                                            }
                                        }
                                    } catch (ParseException e) {
                                        LOGGER.error(e.getMessage());
                                    }
                                }
                            }
                        }
                    }
                }catch (NumberFormatException e){
                    LOGGER.error(e.getMessage());
                }
            }
        }
    }


    //REFACTOR
    /*PAGAMENTI LEGALE*/
    private void paymentLawyerAttach(List<List<String>> fileCsv, FileImportTypeEnum fileImportType) {

        fileCsv.removeIf(att -> Arrays.asList(att.get(0).split(";", -1)).size() != 14);

        for (List<String> currentoRow : fileCsv) {

            List<String> column = Arrays.asList(currentoRow.get(0).split(";", -1));
            ClaimsEntity claimsEntity = null;
            ClaimsNewEntity claimsNewEntity = null;

            //CONTROLLO DELL'ID PRATICA
            String practiceId = column.get(2);
            if (!practiceId.equals("")) {
                //RECUPERO CLAIMS TRAMITE ID PARTICA PRESENTE SUL FILE DI IMPORT
                claimsNewEntity = claimsNewRepository.getClaimsByIdPratica(Long.valueOf(practiceId));
                //funzione di conversione
                claimsEntity = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntity);

                if (claimsEntity != null) {

                    if (claimsEntity.getStatus() != null && !claimsEntity.getStatus().equals(ClaimsStatusEnum.DRAFT) &&
                            !claimsEntity.getStatus().equals(ClaimsStatusEnum.INCOMPLETE) &&
                            !claimsEntity.getStatus().equals(ClaimsStatusEnum.WAITING_FOR_VALIDATION) &&
                            !claimsEntity.getStatus().equals(ClaimsStatusEnum.REJECTED)
                    ) {

                        Damaged damaged = claimsEntity.getDamaged();

                        //CONTROLLO DELLA TARGA DA DAMAGED -> VEHICLE
                        String plate = column.get(1);

                        if (!plate.equals("") && damaged != null && damaged.getVehicle() != null && damaged.getVehicle().getLicensePlate().equalsIgnoreCase(plate)) {

                            //CONTROLLO DELLA DATE DEL SINISTRO
                            String dateAccident = column.get(3);
                            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                            formatter.setTimeZone(TimeZone.getTimeZone(timeZone));
                            String dataSinistro = null;
                            if (claimsEntity.getComplaint() != null && claimsEntity.getComplaint().getDataAccident() != null) {

                                dataSinistro= formatter.format(claimsEntity.getComplaint().getDataAccident().getDateAccident());

                                try {

                                    Date checkFiledate = formatter.parse(dateAccident);

                                    if (!dateAccident.equals("") && dataSinistro.equals(dateAccident)) {

                                        LawyerPaymentsEntity lawyerPaymentsEntity = new LawyerPaymentsEntity();

                                        lawyerPaymentsEntity.setPlate(plate);
                                        lawyerPaymentsEntity.setDateAccident(claimsEntity.getComplaint().getDataAccident().getDateAccident().toInstant());
                                        lawyerPaymentsEntity.setPracticeId(Long.parseLong(practiceId));

                                        //CONTROLLO IL CAMPO DANNO
                                        String dannoString = column.get(4);
                                        if (dannoString != null && !dannoString.equalsIgnoreCase(""))
                                            lawyerPaymentsEntity.setDamage(Double.parseDouble(dannoString));

                                        //CONTROLLO IL CAMPO FERMO TECNICO
                                        String fermoTecnicoString = column.get(5);
                                        if (fermoTecnicoString != null && !fermoTecnicoString.equalsIgnoreCase(""))
                                            lawyerPaymentsEntity.setTechnicalShutdown(Double.parseDouble(fermoTecnicoString));

                                        //CONTROLLO IL CAMPO ONORARI
                                        String onorariString = column.get(6);
                                        if (onorariString != null && !onorariString.equalsIgnoreCase(""))
                                            lawyerPaymentsEntity.setFee(Double.parseDouble(onorariString));

                                        //CONTROLLO IL CAMPO Spese Causa
                                        String speseCausaString = column.get(7);
                                        if (speseCausaString != null && !speseCausaString.equalsIgnoreCase(""))
                                            lawyerPaymentsEntity.setExpensesSuit(Double.parseDouble(speseCausaString));

                                        //CONTROLLO IL CAMPO ACC/SALDO
                                        String accSaldoString = column.get(8);
                                        if (accSaldoString != null && !accSaldoString.equalsIgnoreCase("")) {

                                            if (accSaldoString.equalsIgnoreCase("saldo"))
                                                accSaldoString = "BALANCE";
                                            lawyerPaymentsEntity.setAccSale(TypeEnum.create(accSaldoString));
                                        }

                                        //CONTROLLO IL CAMPO IMPORTO
                                        String importoString = column.get(9);
                                        if (importoString != null && !importoString.equalsIgnoreCase(""))
                                            lawyerPaymentsEntity.setAmount(Double.parseDouble(importoString));

                                        //CONTROLLO IL CAMPO NUMERO TRANSAZIONE
                                        String numeroTransazioneString = column.get(10);
                                        if (numeroTransazioneString != null && !numeroTransazioneString.equalsIgnoreCase(""))
                                            lawyerPaymentsEntity.setTransactionNumber(numeroTransazioneString);

                                        //CONTROLLO IL CAMPO BANCA
                                        String bancaString = column.get(11);
                                        if (bancaString != null && !bancaString.equalsIgnoreCase(""))
                                            lawyerPaymentsEntity.setBank(bancaString);

                                        //CONTROLLO IL CAMPO NOTE
                                        String noteString = column.get(12);
                                        if (noteString != null && !noteString.equalsIgnoreCase(""))
                                            lawyerPaymentsEntity.setNote(noteString);

                                        //CONTROLLO SE IL CAMPO TIPOLOGIA AFFIDATARIO NON SIA VUOTO
                                        String codiceAvvocato = column.get(13);
                                        if (codiceAvvocato != null && !codiceAvvocato.equalsIgnoreCase(""))
                                            lawyerPaymentsEntity.setLawyerCode(Long.parseLong(codiceAvvocato));


                                        lawyerPaymentsEntity.setPaymentType(RefundTypeEnum.create(fileImportType.getValue()));

                                        lawyerPaymentsEntity.setStatus(LawyerPaymentsStatusEnum.WAITING_TO_ASSIGNED);

                                        lawyerPaymentsRepository.save(lawyerPaymentsEntity);
                                    }

                                } catch (ParseException e) {
                                    LOGGER.error(e.getMessage());
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /*FURTI*/
    //VERIFICARE SE SI PUò eliminare
   private void tefthImport(List<List<String>> fileTefthCsv, String status) {

        ClaimsStatusEnum statusEnum = null;
        if (status != null) {

            statusEnum = ClaimsStatusEnum.create(status);
            if (!statusEnum.equals(ClaimsStatusEnum.WAIT_TO_RETURN_POSSESSION) && !statusEnum.equals(ClaimsStatusEnum.WAIT_LOST_POSSESSION)) {
                LOGGER.debug("Don't can import file for practice with " + statusEnum.getValue() + " status");
                throw new BadRequestException("Don't can import file for practice with " + statusEnum.getValue() + " status");
            }
        }

        for (List<String> column : fileTefthCsv) {

            String plate = null;
            if (!column.get(0).equalsIgnoreCase(""))
                plate = column.get(0);

            String dateAccident = null;
            if (!column.get(5).equalsIgnoreCase(""))
                dateAccident = column.get(5);

            try {

                List<ClaimsEntity> claimsList = null;
                ClaimsEntity claimsEntity = null;
                if (statusEnum != null && dateAccident != null && plate != null && !plate.equals("")) {

                    DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                    formatter.setTimeZone(TimeZone.getTimeZone(timeZone));

                    //claimsList = claimsRepositoryV1.getClaimsByDamagedVehiclePlateAndDateAccident(plate, statusEnum.getValue());
                    String data = null;
                    if (claimsList != null) {

                        for (ClaimsEntity att : claimsList) {

                            if (att.getComplaint() != null && att.getComplaint().getDataAccident() != null &&
                                    att.getComplaint().getDataAccident().getDateAccident() != null) {

                                data = formatter.format(att.getComplaint().getDataAccident().getDateAccident());


                                Date checkFileDate = formatter.parse(dateAccident);

                                if (!dateAccident.equals("") && data.equals(dateAccident))
                                    claimsEntity = att;
                            }

                        }
                    }


                }
                if (claimsEntity != null) {

                    String customerId = null;
                    if (!column.get(1).equalsIgnoreCase(""))
                        customerId = column.get(1);

                    if (claimsEntity.getDamaged() != null &&
                            claimsEntity.getDamaged().getCustomer() != null &&
                            claimsEntity.getDamaged().getCustomer().getCustomerId() != null &&
                            customerId != null &&
                            customerId.equalsIgnoreCase(claimsEntity.getDamaged().getCustomer().getCustomerId())
                    ) {

                        String customerName = null;
                        if (!column.get(2).equalsIgnoreCase(""))
                            customerName = column.get(2);

                        if (claimsEntity.getDamaged().getCustomer().getLegalName() != null &&
                                customerName != null &&
                                customerName.equalsIgnoreCase(claimsEntity.getDamaged().getCustomer().getLegalName())
                        ) {

                            String accident = null;
                            if (!column.get(4).equalsIgnoreCase(""))
                                accident = column.get(4);

                            DataAccidentTypeAccidentEnum accidentType = null;
                            if (accident != null)
                                accidentType = DataAccidentTypeAccidentEnum.create(accident.toLowerCase());

                            if (claimsEntity.getComplaint() != null &&
                                    claimsEntity.getComplaint().getDataAccident() != null &&
                                    claimsEntity.getComplaint().getDataAccident().getTypeAccident() != null &&
                                    accidentType != null &&
                                    accidentType.equals(claimsEntity.getComplaint().getDataAccident().getTypeAccident())
                            ) {

                                String vehcleNature = null;
                                if (!column.get(7).equalsIgnoreCase(""))
                                    vehcleNature = column.get(7);

                                VehicleNatureEnum nature = null;
                                if (vehcleNature != null && !vehcleNature.equalsIgnoreCase("")) {

                                    if (vehcleNature.equalsIgnoreCase("M")) {
                                        nature = VehicleNatureEnum.MOTOR_CYCLE;
                                    } else if (vehcleNature.equalsIgnoreCase("A"))
                                        nature = VehicleNatureEnum.MOTOR_VEHICLE;

                                    if (claimsEntity.getDamaged() != null &&
                                            claimsEntity.getDamaged().getVehicle() != null &&
                                            claimsEntity.getDamaged().getVehicle().getNature() != null &&
                                            nature != null &&
                                            nature.equals(claimsEntity.getDamaged().getVehicle().getNature())
                                    ) {

                                        if (claimsEntity.getStatus().equals(ClaimsStatusEnum.WAIT_LOST_POSSESSION))
                                            claimsEntity.setStatus(ClaimsStatusEnum.LOST_POSSESSION);
                                        else
                                            claimsEntity.setStatus(ClaimsStatusEnum.RETURN_TO_POSSESSION);

                                        //claimsRepository.save(claimsEntity);
                                        if (dwhCall) {
                                            dwhClaimsService.sendMessage(claimsEntity, com.doing.nemo.dwh.client.payload.request.EventTypeEnum.UPDATED);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            } catch (ParseException ex) {
                LOGGER.error("ERROR: " + ex.getMessage() + ", the wrong date is " + dateAccident);

            } catch (Exception ex) {
                LOGGER.error("ERROR: " + ex.getMessage() + ", the informations that generate an exception are: " + plate + ", " + dateAccident);
            }

        }

        /*while (infoTefth.hasNext()) {

            Row infoRow = infoTefth.next();

            String plate = null;
            if(infoRow.getCell(0).getCellTypeEnum().equals(CellType.STRING))
                plate = infoRow.getCell(0).getStringCellValue();

            if(infoRow.getCell(0).getCellTypeEnum().equals(CellType.NUMERIC))
                plate = Double.toString(infoRow.getCell(0).getNumericCellValue());

            String dateAccident = null;
            if(infoRow.getCell(5).getCellTypeEnum().equals(CellType.STRING))
                dateAccident = infoRow.getCell(5).getStringCellValue();
            if(infoRow.getCell(5).getCellTypeEnum().equals(CellType.NUMERIC))
                dateAccident = Double.toString(infoRow.getCell(5).getNumericCellValue());

            ClaimsEntity claimsEntity = null;

            if (!plate.equals(""))
                claimsEntity = claimsRepository.getClaimsByDamagedVehiclePlateAndDateAccident(plate, dateAccident, statusEnum.getValue());

            if (claimsEntity != null) {

                String customerId = null;
                if(infoRow.getCell(1).getCellTypeEnum().equals(CellType.STRING))
                    customerId = infoRow.getCell(1).getStringCellValue();
                if(infoRow.getCell(1).getCellTypeEnum().equals(CellType.NUMERIC))
                    customerId = Double.toString(infoRow.getCell(1).getNumericCellValue());
                if (claimsEntity.getDamaged() != null &&
                        claimsEntity.getDamaged().getCustomer() != null &&
                        claimsEntity.getDamaged().getCustomer().getCustomerId() != null &&
                        customerId.equalsIgnoreCase(claimsEntity.getDamaged().getCustomer().getCustomerId())
                ) {

                    String customerName = null;
                    if(infoRow.getCell(2).getCellTypeEnum().equals(CellType.STRING))
                        customerName = infoRow.getCell(2).getStringCellValue();
                    if(infoRow.getCell(2).getCellTypeEnum().equals(CellType.NUMERIC))
                        customerName = Double.toString(infoRow.getCell(2).getNumericCellValue());
                    if (claimsEntity.getDamaged().getCustomer().getLegalName() != null &&
                            customerName.equalsIgnoreCase(claimsEntity.getDamaged().getCustomer().getLegalName())
                    ) {

                        String accident = null;
                        if(infoRow.getCell(4).getCellTypeEnum().equals(CellType.STRING))
                            accident = infoRow.getCell(4).getStringCellValue();
                        if(infoRow.getCell(4).getCellTypeEnum().equals(CellType.NUMERIC))
                            accident = Double.toString(infoRow.getCell(4).getNumericCellValue());
                        DataAccidentTypeAccidentEnum accidentType = DataAccidentTypeAccidentEnum.create(accident.toLowerCase());

                        if (claimsEntity.getComplaint() != null &&
                                claimsEntity.getComplaint().getDataAccident() != null &&
                                claimsEntity.getComplaint().getDataAccident().getTypeAccident() != null &&
                                accidentType.equals(claimsEntity.getComplaint().getDataAccident().getTypeAccident())
                        ) {

                            String vehcleNature = null;
                            if(infoRow.getCell(7).getCellTypeEnum().equals(CellType.STRING))
                                vehcleNature = infoRow.getCell(7).getStringCellValue();
                            if(infoRow.getCell(7).getCellTypeEnum().equals(CellType.NUMERIC))
                                vehcleNature = Double.toString(infoRow.getCell(7).getNumericCellValue());
                            VehicleNatureEnum nature = null;
                            if (vehcleNature.equalsIgnoreCase("M")) {
                                nature = VehicleNatureEnum.MOTOR_CYCLE;
                            } else
                                nature = VehicleNatureEnum.MOTOR_VEHICLE;

                            if (claimsEntity.getDamaged() != null &&
                                    claimsEntity.getDamaged().getVehicle() != null &&
                                    claimsEntity.getDamaged().getVehicle().getNature() != null &&
                                    nature.equals(claimsEntity.getDamaged().getVehicle().getNature())
                            ) {

                                if (claimsEntity.getStatus().equals(ClaimsStatusEnum.WAIT_LOST_POSSESSION))
                                    claimsEntity.setStatus(ClaimsStatusEnum.LOST_POSSESSION);
                                else
                                    claimsEntity.setStatus(ClaimsStatusEnum.RETURN_TO_POSSESSION);

                                claimsRepository.save(claimsEntity);

                            }
                        }
                    }
                }
            }
        }*/
    }

    /**
     * ------------------------------ CLASSI CUSTOMIZZATE PER I FILE DI SCARTO ------------------------------
     **/

    /*FRANCHIGIA 1*/
    private static class WasteFileDeduction1Attribute {

        private String sxFleet;
        private String dataDelSx;
        private String targaVeicoloAssicurato;
        private String sxDcsItalia;
        private String tipologiaDelSinistro;
        private String dataDiChiusura;
        private String importoFFPagamentoSx;
        private String totalePagato;
        private String riaperture;
        private String nrPolizza;
        private String suffisoPolizza;
        private String note;

        public WasteFileDeduction1Attribute() {
        }

        public WasteFileDeduction1Attribute(String sxFleet, String dataDelSx, String targaVeicoloAssicurato, String sxDcsItalia, String tipologiaDelSinistro, String dataDiChiusura, String importoFFPagamentoSx, String totalePagato, String riaperture, String nrPolizza, String suffisoPolizza, String note) {
            this.sxFleet = sxFleet;
            this.dataDelSx = dataDelSx;
            this.targaVeicoloAssicurato = targaVeicoloAssicurato;
            this.sxDcsItalia = sxDcsItalia;
            this.tipologiaDelSinistro = tipologiaDelSinistro;
            this.dataDiChiusura = dataDiChiusura;
            this.importoFFPagamentoSx = importoFFPagamentoSx;
            this.totalePagato = totalePagato;
            this.riaperture = riaperture;
            this.nrPolizza = nrPolizza;
            this.suffisoPolizza = suffisoPolizza;
            this.note = note;
        }

        public String getSxFleet() {
            return sxFleet;
        }

        public void setSxFleet(String sxFleet) {
            this.sxFleet = sxFleet;
        }

        public String getDataDelSx() {
            return dataDelSx;
        }

        public void setDataDelSx(String dataDelSx) {
            this.dataDelSx = dataDelSx;
        }

        public String getTargaVeicoloAssicurato() {
            return targaVeicoloAssicurato;
        }

        public void setTargaVeicoloAssicurato(String targaVeicoloAssicurato) {
            this.targaVeicoloAssicurato = targaVeicoloAssicurato;
        }

        public String getSxDcsItalia() {
            return sxDcsItalia;
        }

        public void setSxDcsItalia(String sxDcsItalia) {
            this.sxDcsItalia = sxDcsItalia;
        }

        public String getTipologiaDelSinistro() {
            return tipologiaDelSinistro;
        }

        public void setTipologiaDelSinistro(String tipologiaDelSinistro) {
            this.tipologiaDelSinistro = tipologiaDelSinistro;
        }

        public String getDataDiChiusura() {
            return dataDiChiusura;
        }

        public void setDataDiChiusura(String dataDiChiusura) {
            this.dataDiChiusura = dataDiChiusura;
        }

        public String getImportoFFPagamentoSx() {
            return importoFFPagamentoSx;
        }

        public void setImportoFFPagamentoSx(String importoFFPagamentoSx) {
            this.importoFFPagamentoSx = importoFFPagamentoSx;
        }

        public String getTotalePagato() {
            return totalePagato;
        }

        public void setTotalePagato(String totalePagato) {
            this.totalePagato = totalePagato;
        }

        public String getRiaperture() {
            return riaperture;
        }

        public void setRiaperture(String riaperture) {
            this.riaperture = riaperture;
        }

        public String getNrPolizza() {
            return nrPolizza;
        }

        public void setNrPolizza(String nrPolizza) {
            this.nrPolizza = nrPolizza;
        }

        public String getSuffisoPolizza() {
            return suffisoPolizza;
        }

        public void setSuffisoPolizza(String suffisoPolizza) {
            this.suffisoPolizza = suffisoPolizza;
        }

        public String getNote() {
            return note;
        }

        public void setNote(String note) {
            this.note = note;
        }

        @Override
        public String toString() {
            return "WasteFileDeduction1Attribute{" +
                    "sxFleet='" + sxFleet + '\'' +
                    ", dataDelSx='" + dataDelSx + '\'' +
                    ", targaVeicoloAssicurato='" + targaVeicoloAssicurato + '\'' +
                    ", sxDcsItalia='" + sxDcsItalia + '\'' +
                    ", tipologiaDelSinistro='" + tipologiaDelSinistro + '\'' +
                    ", dataDiChiusura='" + dataDiChiusura + '\'' +
                    ", importoFFPagamentoSx='" + importoFFPagamentoSx + '\'' +
                    ", totalePagato='" + totalePagato + '\'' +
                    ", riaperture='" + riaperture + '\'' +
                    ", nrPolizza='" + nrPolizza + '\'' +
                    ", suffisoPolizza='" + suffisoPolizza + '\'' +
                    ", note='" + note + '\'' +
                    '}';
        }
    }

    /*FRANCHIGIA 2*/
    private static class WasteFileDeduction2Attribute {

        private String sxFleet;
        private String dataDelSx;
        private String targaVeicoloAssicurato;
        private String sxDcsItalia;
        private String tipologiaDelSinistro;
        private String dataDiChiusura;
        private String importoExtraFFPagamentoSx;
        private String importoFFPagamentoSx;
        private String importoFFPrecedenteFatturatoPagamentoSx;
        private String totalePagato;
        private String riaperture;
        private String nrPolizza;
        private String suffisoPolizza;
        private String note;

        public WasteFileDeduction2Attribute() {
        }

        public WasteFileDeduction2Attribute(String sxFleet, String dataDelSx, String targaVeicoloAssicurato, String sxDcsItalia, String tipologiaDelSinistro, String dataDiChiusura, String importoExtraFFPagamentoSx, String importoFFPagamentoSx, String importoFFPrecedenteFatturatoPagamentoSx, String totalePagato, String riaperture, String nrPolizza, String suffisoPolizza, String note) {
            this.sxFleet = sxFleet;
            this.dataDelSx = dataDelSx;
            this.targaVeicoloAssicurato = targaVeicoloAssicurato;
            this.sxDcsItalia = sxDcsItalia;
            this.tipologiaDelSinistro = tipologiaDelSinistro;
            this.dataDiChiusura = dataDiChiusura;
            this.importoExtraFFPagamentoSx = importoExtraFFPagamentoSx;
            this.importoFFPagamentoSx = importoFFPagamentoSx;
            this.importoFFPrecedenteFatturatoPagamentoSx = importoFFPrecedenteFatturatoPagamentoSx;
            this.totalePagato = totalePagato;
            this.riaperture = riaperture;
            this.nrPolizza = nrPolizza;
            this.suffisoPolizza = suffisoPolizza;
            this.note = note;
        }

        public String getSxFleet() {
            return sxFleet;
        }

        public void setSxFleet(String sxFleet) {
            this.sxFleet = sxFleet;
        }

        public String getDataDelSx() {
            return dataDelSx;
        }

        public void setDataDelSx(String dataDelSx) {
            this.dataDelSx = dataDelSx;
        }

        public String getTargaVeicoloAssicurato() {
            return targaVeicoloAssicurato;
        }

        public void setTargaVeicoloAssicurato(String targaVeicoloAssicurato) {
            this.targaVeicoloAssicurato = targaVeicoloAssicurato;
        }

        public String getSxDcsItalia() {
            return sxDcsItalia;
        }

        public void setSxDcsItalia(String sxDcsItalia) {
            this.sxDcsItalia = sxDcsItalia;
        }

        public String getTipologiaDelSinistro() {
            return tipologiaDelSinistro;
        }

        public void setTipologiaDelSinistro(String tipologiaDelSinistro) {
            this.tipologiaDelSinistro = tipologiaDelSinistro;
        }

        public String getDataDiChiusura() {
            return dataDiChiusura;
        }

        public void setDataDiChiusura(String dataDiChiusura) {
            this.dataDiChiusura = dataDiChiusura;
        }

        public String getImportoExtraFFPagamentoSx() {
            return importoExtraFFPagamentoSx;
        }

        public void setImportoExtraFFPagamentoSx(String importoExtraFFPagamentoSx) {
            this.importoExtraFFPagamentoSx = importoExtraFFPagamentoSx;
        }

        public String getImportoFFPagamentoSx() {
            return importoFFPagamentoSx;
        }

        public void setImportoFFPagamentoSx(String importoFFPagamentoSx) {
            this.importoFFPagamentoSx = importoFFPagamentoSx;
        }

        public String getImportoFFPrecedenteFatturatoPagamentoSx() {
            return importoFFPrecedenteFatturatoPagamentoSx;
        }

        public void setImportoFFPrecedenteFatturatoPagamentoSx(String importoFFPrecedenteFatturatoPagamentoSx) {
            this.importoFFPrecedenteFatturatoPagamentoSx = importoFFPrecedenteFatturatoPagamentoSx;
        }

        public String getTotalePagato() {
            return totalePagato;
        }

        public void setTotalePagato(String totalePagato) {
            this.totalePagato = totalePagato;
        }

        public String getRiaperture() {
            return riaperture;
        }

        public void setRiaperture(String riaperture) {
            this.riaperture = riaperture;
        }

        public String getNrPolizza() {
            return nrPolizza;
        }

        public void setNrPolizza(String nrPolizza) {
            this.nrPolizza = nrPolizza;
        }

        public String getSuffisoPolizza() {
            return suffisoPolizza;
        }

        public void setSuffisoPolizza(String suffisoPolizza) {
            this.suffisoPolizza = suffisoPolizza;
        }

        public String getNote() {
            return note;
        }

        public void setNote(String note) {
            this.note = note;
        }

        @Override
        public String toString() {
            return "WasteFileDeduction2Attribute{" +
                    "sxFleet='" + sxFleet + '\'' +
                    ", dataDelSx='" + dataDelSx + '\'' +
                    ", targaVeicoloAssicurato='" + targaVeicoloAssicurato + '\'' +
                    ", sxDcsItalia='" + sxDcsItalia + '\'' +
                    ", tipologiaDelSinistro='" + tipologiaDelSinistro + '\'' +
                    ", dataDiChiusura='" + dataDiChiusura + '\'' +
                    ", importoExtraFFPagamentoSx='" + importoExtraFFPagamentoSx + '\'' +
                    ", importoFFPagamentoSx='" + importoFFPagamentoSx + '\'' +
                    ", importoFFPrecedenteFatturatoPagamentoSx='" + importoFFPrecedenteFatturatoPagamentoSx + '\'' +
                    ", totalePagato='" + totalePagato + '\'' +
                    ", riaperture='" + riaperture + '\'' +
                    ", nrPolizza='" + nrPolizza + '\'' +
                    ", suffisoPolizza='" + suffisoPolizza + '\'' +
                    ", note='" + note + '\'' +
                    '}';
        }
    }

    /*NUMERO SX*/
    private static class WasteFileNumberSxAttribute {

        private String rifAxusAld;
        private String rifCompagnia;
        private String numeroPolizza;
        private String dataSinistro;
        private String tipoDanno;
        private String targaAssicurato;
        private String ispettorato;
        private String peritoIncaricato;
        private String note;

        public WasteFileNumberSxAttribute() {
        }

        public WasteFileNumberSxAttribute(String rifAxusAld, String rifCompagnia, String numeroPolizza, String dataSinistro, String tipoDanno, String targaAssicurato, String ispettorato, String peritoIncaricato, String note) {
            this.rifAxusAld = rifAxusAld;
            this.rifCompagnia = rifCompagnia;
            this.numeroPolizza = numeroPolizza;
            this.dataSinistro = dataSinistro;
            this.tipoDanno = tipoDanno;
            this.targaAssicurato = targaAssicurato;
            this.ispettorato = ispettorato;
            this.peritoIncaricato = peritoIncaricato;
            this.note = note;
        }

        public String getRifAxusAld() {
            return rifAxusAld;
        }

        public void setRifAxusAld(String rifAxusAld) {
            this.rifAxusAld = rifAxusAld;
        }

        public String getRifCompagnia() {
            return rifCompagnia;
        }

        public void setRifCompagnia(String rifCompagnia) {
            this.rifCompagnia = rifCompagnia;
        }

        public String getNumeroPolizza() {
            return numeroPolizza;
        }

        public void setNumeroPolizza(String numeroPolizza) {
            this.numeroPolizza = numeroPolizza;
        }

        public String getDataSinistro() {
            return dataSinistro;
        }

        public void setDataSinistro(String dataSinistro) {
            this.dataSinistro = dataSinistro;
        }

        public String getTipoDanno() {
            return tipoDanno;
        }

        public void setTipoDanno(String tipoDanno) {
            this.tipoDanno = tipoDanno;
        }

        public String getTargaAssicurato() {
            return targaAssicurato;
        }

        public void setTargaAssicurato(String targaAssicurato) {
            this.targaAssicurato = targaAssicurato;
        }

        public String getIspettorato() {
            return ispettorato;
        }

        public void setIspettorato(String ispettorato) {
            this.ispettorato = ispettorato;
        }

        public String getPeritoIncaricato() {
            return peritoIncaricato;
        }

        public void setPeritoIncaricato(String peritoIncaricato) {
            this.peritoIncaricato = peritoIncaricato;
        }

        public String getNote() {
            return note;
        }

        public void setNote(String note) {
            this.note = note;
        }

        @Override
        public String toString() {
            return "WasteFileNumberSxAttribute{" +
                    "rifAxusAld='" + rifAxusAld + '\'' +
                    ", rifCompagnia='" + rifCompagnia + '\'' +
                    ", numeroPolizza='" + numeroPolizza + '\'' +
                    ", dataSinistro='" + dataSinistro + '\'' +
                    ", tipoDanno='" + tipoDanno + '\'' +
                    ", targaAssicurato='" + targaAssicurato + '\'' +
                    ", ispettorato='" + ispettorato + '\'' +
                    ", peritoIncaricato='" + peritoIncaricato + '\'' +
                    ", note='" + note + '\'' +
                    '}';
        }
    }

    /*AFFIDO MASSIVO*/
    private static class WasteFileMassiveEntrustAttribute {

        private String sxFleet;
        private String dataDelSx;
        private String targaVeicoloAssicurato;
        private String tipologiaAffidatario;
        private String codAffidatario;
        private String note;

        public WasteFileMassiveEntrustAttribute() {
        }

        public String getSxFleet() {
            return sxFleet;
        }

        public void setSxFleet(String sxFleet) {
            this.sxFleet = sxFleet;
        }

        public String getDataDelSx() {
            return dataDelSx;
        }

        public void setDataDelSx(String dataDelSx) {
            this.dataDelSx = dataDelSx;
        }

        public String getTargaVeicoloAssicurato() {
            return targaVeicoloAssicurato;
        }

        public void setTargaVeicoloAssicurato(String targaVeicoloAssicurato) {
            this.targaVeicoloAssicurato = targaVeicoloAssicurato;
        }

        public String getTipologiaAffidatario() {
            return tipologiaAffidatario;
        }

        public void setTipologiaAffidatario(String tipologiaAffidatario) {
            this.tipologiaAffidatario = tipologiaAffidatario;
        }

        public String getCodAffidatario() {
            return codAffidatario;
        }

        public void setCodAffidatario(String codAffidatario) {
            this.codAffidatario = codAffidatario;
        }

        public String getNote() {
            return note;
        }

        public void setNote(String note) {
            this.note = note;
        }
    }

    /*PAGAMENTI LEGALE*/
    private static class WasteFileLawyerPaymentsAttribute {

        private String num;
        private String targa;
        private String identificativoSinistro;
        private String dataSinistro;
        private String danno;
        private String fermoTecnico;
        private String onorari;
        private String speseCausa;
        private String accSaldo;
        private String importo;
        private String numeroTransazione;
        private String banca;
        private String note;
        private String codiceAvvocato;
        private String noteScarto;

        public WasteFileLawyerPaymentsAttribute() {
        }

        public String getNum() {
            return num;
        }

        public void setNum(String num) {
            this.num = num;
        }

        public String getTarga() {
            return targa;
        }

        public void setTarga(String targa) {
            this.targa = targa;
        }

        public String getIdentificativoSinistro() {
            return identificativoSinistro;
        }

        public void setIdentificativoSinistro(String identificativoSinistro) {
            this.identificativoSinistro = identificativoSinistro;
        }

        public String getDataSinistro() {
            return dataSinistro;
        }

        public void setDataSinistro(String dataSinistro) {
            this.dataSinistro = dataSinistro;
        }

        public String getDanno() {
            return danno;
        }

        public void setDanno(String danno) {
            this.danno = danno;
        }

        public String getFermoTecnico() {
            return fermoTecnico;
        }

        public void setFermoTecnico(String fermoTecnico) {
            this.fermoTecnico = fermoTecnico;
        }

        public String getOnorari() {
            return onorari;
        }

        public void setOnorari(String onorari) {
            this.onorari = onorari;
        }

        public String getSpeseCausa() {
            return speseCausa;
        }

        public void setSpeseCausa(String speseCausa) {
            this.speseCausa = speseCausa;
        }

        public String getAccSaldo() {
            return accSaldo;
        }

        public void setAccSaldo(String accSaldo) {
            this.accSaldo = accSaldo;
        }

        public String getImporto() {
            return importo;
        }

        public void setImporto(String importo) {
            this.importo = importo;
        }

        public String getNumeroTransazione() {
            return numeroTransazione;
        }

        public void setNumeroTransazione(String numeroTransazione) {
            this.numeroTransazione = numeroTransazione;
        }

        public String getBanca() {
            return banca;
        }

        public void setBanca(String banca) {
            this.banca = banca;
        }

        public String getNote() {
            return note;
        }

        public void setNote(String note) {
            this.note = note;
        }

        public String getCodiceAvvocato() {
            return codiceAvvocato;
        }

        public void setCodiceAvvocato(String codiceAvvocato) {
            this.codiceAvvocato = codiceAvvocato;
        }

        public String getNoteScarto() {
            return noteScarto;
        }

        public void setNoteScarto(String noteScarto) {
            this.noteScarto = noteScarto;
        }
    }

}
