package com.doing.nemo.claims.service;

import com.doing.nemo.claims.dto.AbstractExportFilter;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.exception.InternalException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class ExportHelper<T extends AbstractExportFilter> {

    @Autowired
    private UsersService usersService;

    private static Logger LOGGER = LoggerFactory.getLogger(ExportHelper.class);

    public abstract Integer getExportCount(T filter);

    public String getUserEmail(String nemoUserId, String authorization) {
        try {
            String userEmail = usersService.getUserEmailByUserId(nemoUserId, authorization);
            if(userEmail == null){
                throw new BadRequestException(MessageCode.CLAIMS_1190);
            }
            return userEmail;
        }
        catch(Exception e){
            LOGGER.error(MessageCode.CLAIMS_1191.value(), e);
            throw new InternalException(MessageCode.CLAIMS_1191, e);
        }
    }

}
