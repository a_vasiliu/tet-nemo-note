package com.doing.nemo.claims.service;

import com.doing.nemo.claims.common.commandBus.Command;
import com.doing.nemo.claims.controller.payload.request.AssignationRequestV1;
import com.doing.nemo.claims.controller.payload.request.CounterpartyProcedureRequestV1;
import com.doing.nemo.claims.controller.payload.request.IdRequestV1;
import com.doing.nemo.claims.controller.payload.response.CounterpartyResponseV1;
import com.doing.nemo.claims.controller.payload.response.external.CounterpartyExternalResponse;
import com.doing.nemo.claims.dto.CtpExportFilter;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.CounterpartyEntity;
import com.doing.nemo.claims.entity.CounterpartyNewEntity;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.enumerated.DTO.CounterpartyStats;
import com.doing.nemo.claims.entity.enumerated.RepairEnum.RepairProcedureEnum;
import com.doing.nemo.claims.entity.jsonb.Historical;
import com.doing.nemo.claims.entity.jsonb.repair.HistoricalCounterparty;

import java.math.BigInteger;
import java.util.List;

public interface CounterpartyService {

    //REFACTOR
    CounterpartyStats getClaimsCounterpartyStats(String nemoUserId);

    //REFACTOR
   // void EligibilityCriterion(ClaimsEntity claims);
    void EligibilityCriterion(ClaimsEntity claims, String userId, String userName);


    //REFACTOR
    List<CounterpartyEntity> getCounterpartyPaginationFromDB (String denomination, String plate, Long practiceIdCounterparty, String statusRepair, String dateCreatedFrom,
                                                              String dateCreatedTo, Boolean asc, String orderBy, int page, int pageSize, String addressSx, String fromCompany, String dateSxFrom, String dateSxTo, String region, String province, String lastContact, String assignedTo, String locality, String recall, String centerEmail, Boolean msaPagination);



    List<CounterpartyResponseV1> getCounterpartyMsaPaginationFromDB (String denomination, String plate, Long practiceIdCounterparty, String statusRepair, String dateCreatedFrom,
                                                                     String dateCreatedTo, Boolean asc, String orderBy, int page, int pageSize, String addressSx, String fromCompany, String dateSxFrom, String dateSxTo, String region, String province, String lastContact, String assignedTo, String locality, String recall, String centerEmail, Boolean msaPagination);


    //REFACTOR
    BigInteger countCounterpartyPagination(String denomination, String plate, Long practiceIdCounterparty, String statusRepair, String dateCreatedFrom,
                                           String dateCreatedTo, Boolean asc, String orderBy, int page, int pageSize, String addressSx, String fromCompany, String dateSxFrom, String dateSxTo, String region, String province, String lastContact, String assignedTo, String locality, String recall,String centerEmail, Boolean msaPagination);

    //REFACTOR
    List<CounterpartyEntity> getCounterpartyPaginationWithoutToSort (String denomination, String plate, Long practiceIdCounterparty, String statusRepair, String dateCreatedFrom,
                                                                     String dateCreatedTo, Boolean asc, String orderBy, int page, int pageSize, String addressSx, String fromCompany, String dateSxFrom, String dateSxTo, String region, String province, String lastContact, String assignedTo, String locality, String recall);
    //REFACTOR
    BigInteger countCounterpartyPaginationWithoutToSort(String denomination, String plate, Long practiceIdCounterparty, String statusRepair, String dateCreatedFrom,
                                                               String dateCreatedTo, Boolean asc, String orderBy, int page, int pageSize, String addressSx, String fromCompany, String dateSxFrom, String dateSxTo, String region, String province, String lastContact, String assignedTo, String locality, String recall);

    //REFACTOR NON NECESSARIO
    Pagination<CounterpartyResponseV1> generateCounterpartyPagination(List<CounterpartyResponseV1> counterpartyResponseV1s, BigInteger count, int page, int pageSize);
    //REFACTOR
    void assignsCounterparty(List<AssignationRequestV1> assignationRequestV1s, String userid, String username);
    //REFACTOR
    CounterpartyEntity findById(String countepartyId);
    //REFACTOR
    CounterpartyNewEntity updateCounterparts(CounterpartyNewEntity counterparty);
    //REFACTOR
    Pagination<CounterpartyEntity> getCounterpartyPaginationAuthority(String plate, String chassis, int page, int pageSize);
    //REFACTOR
    CounterpartyEntity patchProcedureRepair(RepairProcedureEnum procedure, String counterpartyId, CounterpartyProcedureRequestV1 motivationRequest);
    void isWorkable (CounterpartyNewEntity counterpartyEntity);
    //REFACTOR
    ClaimsEntity patchStatusHiddenAttachmentCounterpart (String idCounterpart, String idAttachments);
    //REFACTOR
    List<CounterpartyExternalResponse> getCounterpartySetMsaReadResponse ( List<IdRequestV1> counterpartyId);

    BigInteger countCounterpartyForExport (CtpExportFilter filter);

    List<CounterpartyEntity> getCounterpartyForExportByPages (CtpExportFilter filter, int page, int pageSize);

    List<HistoricalCounterparty> getCounterpartyHistorical(String counterpartyId);

}
