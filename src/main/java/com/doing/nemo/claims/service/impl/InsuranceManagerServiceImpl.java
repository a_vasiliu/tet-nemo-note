package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.controller.payload.response.InsuranceManagerResponseV1;
import com.doing.nemo.claims.controller.payload.response.PaginationStats;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.settings.InsuranceManagerEntity;
import com.doing.nemo.claims.repository.InsuranceManagerRepository;
import com.doing.nemo.claims.repository.InsuranceManagerRepositoryV1;
import com.doing.nemo.claims.service.InsuranceManagerService;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class InsuranceManagerServiceImpl implements InsuranceManagerService {

    private static Logger LOGGER = LoggerFactory.getLogger(InsuranceManagerServiceImpl.class);
    @Autowired
    private InsuranceManagerRepository insuranceManagerRepository;
    @Autowired
    private InsuranceManagerRepositoryV1 insuranceManagerRepositoryV1;

    @Override
    public InsuranceManagerEntity insertManager(InsuranceManagerEntity insuranceManagerEntity) {
        if (insuranceManagerRepository.searchInsuranceManagerbyName(insuranceManagerEntity.getName()) != null) {
            LOGGER.debug("Insurance manager with name '" + insuranceManagerEntity.getName() + "' already exists");
            throw new BadRequestException("Insurance manager with name '" + insuranceManagerEntity.getName() + "' already exists", MessageCode.CLAIMS_1009);
        }
        insuranceManagerRepository.save(insuranceManagerEntity);
        return insuranceManagerEntity;
    }

    @Override

    public InsuranceManagerEntity updateManager(InsuranceManagerEntity insuranceManagerEntity) {
        if (insuranceManagerRepository.searchInsuranceManagerbyNameWithDifferentId(insuranceManagerEntity.getName(), insuranceManagerEntity.getId()) != null) {
            LOGGER.debug("Insurance manager with name '" + insuranceManagerEntity.getName() + "' already exists");
            throw new BadRequestException("Insurance manager with name '" + insuranceManagerEntity.getName() + "' already exists", MessageCode.CLAIMS_1009);
        }

        insuranceManagerRepository.save(insuranceManagerEntity);
        return insuranceManagerEntity;
    }

    @Override
    public InsuranceManagerResponseV1 deleteManager(UUID uuid) {
        Optional<InsuranceManagerEntity> insuranceManagerEntity2 = insuranceManagerRepository.findById(uuid);
        if (!insuranceManagerEntity2.isPresent()) {
            LOGGER.debug("Insurance manager with id " + uuid + " not found");
            throw new NotFoundException("Insurance manager with id " + uuid + " not found", MessageCode.CLAIMS_1010);
        }
        //if(insuranceManagerRepository.searchInsurancePolicyForeignKey(uuid) != null)
        //throw new NotFoundException("Foreign key in InsurancePolicy. Insurance Manager impossible to delete");
        InsuranceManagerEntity insuranceManagerEntity = insuranceManagerEntity2.get();
        insuranceManagerRepository.delete(insuranceManagerEntity);
        return null;
    }

    @Override
    public InsuranceManagerEntity selectManager(UUID uuid) {
        Optional<InsuranceManagerEntity> insuranceManagerEntity = insuranceManagerRepository.findById(uuid);
        if (!insuranceManagerEntity.isPresent()) {
            LOGGER.debug("Insurance manager with id " + uuid + " not found");
            throw new NotFoundException("Insurance manager with id " + uuid + " not found", MessageCode.CLAIMS_1010);
        }

        return insuranceManagerEntity.get();
    }

    @Override
    public List<InsuranceManagerEntity> selectAllManager() {
        List<InsuranceManagerEntity> insuranceManagerEntityList = insuranceManagerRepository.findAll();


        return insuranceManagerEntityList;
    }

    @Override
    public InsuranceManagerEntity updateStatusManager(UUID uuid) {
        InsuranceManagerEntity insuranceManagerEntity = selectManager(uuid);

        if(insuranceManagerEntity.getActive()==null){
            insuranceManagerEntity.setActive(true);
            return insuranceManagerEntity;
        }

        insuranceManagerEntity.setActive(!insuranceManagerEntity.getActive());

        insuranceManagerRepository.save(insuranceManagerEntity);
        return insuranceManagerEntity;
    }

    @Override
    public Pagination<InsuranceManagerEntity> paginationInsuranceManager(int page, int pageSize, String orderBy, Boolean asc, String name, String province, String telephone, String fax, String email, Boolean isActive, String address, String zipCode, String city, String state, Long rifCode) {
        Pagination<InsuranceManagerEntity> insuranceManagerPagination = new Pagination<>();
        List<InsuranceManagerEntity> insuranceManagerEntityList = insuranceManagerRepositoryV1.findPaginationInsuranceManager(page, pageSize, orderBy, asc, name, province, telephone, fax, email, isActive, address, zipCode, city, state, rifCode);
        Long itemCount = insuranceManagerRepositoryV1.countPaginationInsuranceManager(name, province, telephone, fax, email, isActive, address, zipCode, city, state, rifCode);
        insuranceManagerPagination.setItems(insuranceManagerEntityList);
        insuranceManagerPagination.setStats(new PaginationStats(itemCount.intValue(), page, pageSize));

        return insuranceManagerPagination;
    }
}
