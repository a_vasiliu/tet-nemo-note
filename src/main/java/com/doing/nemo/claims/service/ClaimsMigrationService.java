package com.doing.nemo.claims.service;

import com.doing.nemo.claims.controller.payload.request.CounterpartyMigrationRequestV1;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.CounterpartyEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ClaimsMigrationService {
    void  importClaimsFromMigration(List<ClaimsEntity> claimsEntityList);
    void importCounterpartyFromMigration(List<CounterpartyMigrationRequestV1> counterpartyMigrationRequestV1List);
    }
