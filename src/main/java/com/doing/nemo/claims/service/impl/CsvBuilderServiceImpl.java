package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.controller.payload.request.UploadFileRequestV1;
import com.doing.nemo.claims.controller.payload.response.UploadFileResponseV1;
import com.doing.nemo.claims.controller.payload.response.claims.Claims;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.doing.nemo.claims.entity.jsonb.forms.jsonbForms.Attachment;
import com.doing.nemo.claims.service.CsvBuilderService;
import com.doing.nemo.claims.service.FileManagerService;
import com.doing.nemo.claims.service.UploadFileService;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.rmi.ConnectException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class CsvBuilderServiceImpl implements CsvBuilderService {
    private static Logger LOGGER = LoggerFactory.getLogger(CsvBuilderServiceImpl.class);

    @Autowired
    UploadFileService uploadFileService;

    @Autowired
    FileManagerService fileManagerService;


    private HashMap<ClaimsStatusEnum,String> claimsStatusEnumTranslateMap =new HashMap<ClaimsStatusEnum,String>(){{
        put(ClaimsStatusEnum.WAITING_FOR_VALIDATION, "Da Validare");
        put(ClaimsStatusEnum.WAITING_FOR_AUTHORITY, "In Attesa");
        put(ClaimsStatusEnum.DELETED, "Cancellata");
        put(ClaimsStatusEnum.TO_ENTRUST, "Da affidare");
        put(ClaimsStatusEnum.CLOSED, "Chiusa senza seguito");
        put(ClaimsStatusEnum.INCOMPLETE, "Incompleta");
        put(ClaimsStatusEnum.CLOSED_PRACTICE, "Pratica chiusa");
        put(ClaimsStatusEnum.REJECTED, "Rifiutata");
        put(ClaimsStatusEnum.MANAGED, "Autogestita");
        put(ClaimsStatusEnum.WAITING_FOR_REFUND, "In attesa di rimborso");
        put(ClaimsStatusEnum.TO_SEND_TO_CUSTOMER, "Da inviare al cliente");
        put(ClaimsStatusEnum.CLOSED_PARTIAL_REFUND, "Chiusa con rimborso parziale");
        put(ClaimsStatusEnum.CLOSED_TOTAL_REFUND, "Chiusa con rimborso totale");
    }};

    /**
     * Costruisce le righe del CSV comprensive di Header a partire da una lista di {@link ClaimsEntity}
     * @param claimsEntityList
     *      Lista di claims da cui estrarre i valori
     * @return
     *      {@link List} di {@link String} ognuna delle quali rappresenta una riga del CSV, comprensiva di HEADER.
     *      ogni riga termina con il carattere di fine riga '\n'
     */
    @Override
    public List<String> buildClaimsDashboardCSV(List<ClaimsEntity> claimsEntityList) {
        List<String> claimsDashboardCSV = new LinkedList<>();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        //Header
        claimsDashboardCSV.add("TIPO;ID;DATA SINISTRO;TARGA;TIPO DENUNCIA;DATA INSERIMENTO;APP. TELEMATICO;STATO DENUNCIA;IN EVIDENZA;VARIAZIONE PO;AFFIDO AUTOMATICO;CRASH REPORT\n");

        //Rows
        for(ClaimsEntity claimsEntity : claimsEntityList){
            StringBuilder csvRow = new StringBuilder();
            csvRow.append(claimsEntity.getType().getValue());
            csvRow.append(";");
            csvRow.append(claimsEntity.getPracticeId());
            csvRow.append(";");
            if(claimsEntity.getComplaint()!=null && claimsEntity.getComplaint().getDataAccident()!= null && claimsEntity.getComplaint().getDataAccident().getDateAccident()!=null) {
                csvRow.append(dateFormat.format(claimsEntity.getComplaint().getDataAccident().getDateAccident()));
            }
            csvRow.append(";");
            if(claimsEntity.getComplaint()!=null ) {
                csvRow.append(claimsEntity.getComplaint().getPlate());
            }
            csvRow.append(";");
            if(claimsEntity.getComplaint()!=null && claimsEntity.getComplaint().getDataAccident()!=null && claimsEntity.getComplaint().getDataAccident().getTypeAccident()!=null){
                csvRow.append(claimsEntity.getComplaint().getDataAccident().getTypeAccident().getValue());
            }
            csvRow.append(";");
            if(claimsEntity.getCreatedAt()!=null){
                Date cratedAt = Date.from(claimsEntity.getCreatedAt());
                csvRow.append(dateFormat.format(cratedAt));
            }
            csvRow.append(";");

            if(claimsEntity.getDamaged()!=null && claimsEntity.getDamaged().getAntiTheftService()!=null && claimsEntity.getDamaged().getAntiTheftService().getName()!=null) {
                csvRow.append(claimsEntity.getDamaged().getAntiTheftService().getName());
            }
            csvRow.append(";");
            if(claimsEntity.getStatus()!=null){
                csvRow.append(translateClaimsStatusEnum(claimsEntity.getStatus()));
            }
            csvRow.append(";");
            if(claimsEntity.getInEvidence()!=null && claimsEntity.getInEvidence()){
                csvRow.append("Si");
            }else{
                csvRow.append("No");
            }
            csvRow.append(";");
            if(claimsEntity.getPoVariation()!=null && claimsEntity.getPoVariation()){
                csvRow.append("Si");
            }else{
                csvRow.append("No");
            }
            csvRow.append(";");
            if(claimsEntity.getComplaint()!=null && claimsEntity.getComplaint().getEntrusted()!=null){
                //Il Flag a SI va innalzato solo se l'AutoEntrust è FALSE
                if(claimsEntity.getComplaint().getEntrusted().getAutoEntrust()!=null && !claimsEntity.getComplaint().getEntrusted().getAutoEntrust()){
                    csvRow.append("Si");
                }else{
                    csvRow.append("No");
                }
            }else{
                csvRow.append("No");
            }
            csvRow.append(";");
            // Check Flag CrashReport
            if(claimsEntity.getDamaged()!=null
                    && claimsEntity.getDamaged().getAntiTheftService()!=null
                    && claimsEntity.getAntiTheftRequestEntities()!=null
                    && !claimsEntity.getAntiTheftRequestEntities().isEmpty()
                    ){
                //Verifico che l'ultimo elemento abbia response_type != 1
                int lastIdx = claimsEntity.getAntiTheftRequestEntities().size()-1;
                if(claimsEntity.getAntiTheftRequestEntities().get(lastIdx)!=null
                        && claimsEntity.getAntiTheftRequestEntities().get(lastIdx).getResponseType()!=null
                        && !claimsEntity.getAntiTheftRequestEntities().get(lastIdx).getResponseType().trim().equalsIgnoreCase("1")
                ){
                    csvRow.append("Si");
                }else{
                    csvRow.append("No");
                }
            }else{
                csvRow.append("No");
            }
            csvRow.append('\n');
            claimsDashboardCSV.add(csvRow.toString());
        }

        return claimsDashboardCSV;
    }

    /**
     * Crea il CSV su FileManager.
     * @param csvRowList
     *      Lista di righe del csv comprensive di HEADER
     * @param fileName
     *      Nome del file da salvare su FileManager
     * @param description
     *      Descrizione del file da salvare su FileManager
     * @return
     *      Response dell'esito dell'elaborazione da parte di FileManager.
     * @throws IOException
     */
    @Override
    public UploadFileResponseV1 upldadCsv(List<String> csvRowList, String fileName, String description) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(baos);
        BufferedWriter writer = new BufferedWriter(outputStreamWriter);

        for (String row : csvRowList){
            writer.write(row);
        }

        writer.close();
        outputStreamWriter.close();
        baos.close();

        UploadFileResponseV1 uploadFileResponse = null;
        UploadFileRequestV1 uploadFileCSV = null;

        uploadFileCSV = new UploadFileRequestV1();
        uploadFileCSV.setBlobType("csv");
        uploadFileCSV.setDescription(description);

        uploadFileCSV.setFileName(fileName+".csv");
        uploadFileCSV.setFileContent(uploadFileService.encodeFileInBase64(baos));
        uploadFileCSV.setResourceType("invoicing");
        uploadFileCSV.setResourceId("1");

        try {
            uploadFileResponse = fileManagerService.uploadSingleFile(uploadFileCSV);
        } catch (ConnectException e) {
            LOGGER.info(e.getMessage());
        }
        return uploadFileResponse;
    }

    /**
     * Traduce lo stato da Enum a stringa in italiano come presentato a FE.
     * La traduzione avviene tramita l'hashmap <b>claimsStatusEnumTranslateMap</b>
     * presente nella classe.
     * </br>
     * Nel caso in cui l'enum non è presente nell'hashmap viene tornata la
     * stringa della stessa
     * @param statusEnum
     * @return
     *      -Valore tradotto se lo statusEnum è presente in  <b>claimsStatusEnumTranslateMap</b>
     *      -Stringa dell'ENUM se lo statusEnum non è presente in <b>claimsStatusEnumTranslateMap</b>
     *      -Stringa vuota se lo statusEnum è null <b>claimsStatusEnumTranslateMap</b>
     */
    private String translateClaimsStatusEnum(ClaimsStatusEnum statusEnum){
        String translatedValue="";
        if(this.claimsStatusEnumTranslateMap!=null){
            if(this.claimsStatusEnumTranslateMap.containsKey(statusEnum)) {
                translatedValue = this.claimsStatusEnumTranslateMap.get(statusEnum);
            }else{
                translatedValue = statusEnum.getValue();
            }
        }
        return translatedValue;
    }

}
