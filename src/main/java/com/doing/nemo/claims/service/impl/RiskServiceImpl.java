package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.entity.settings.RiskEntity;
import com.doing.nemo.claims.repository.RiskRepository;
import com.doing.nemo.claims.service.RiskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RiskServiceImpl implements RiskService {

    @Autowired
    private RiskRepository riskRepository;

    @Override
    public List<RiskEntity> updateRiskEntityList(List<RiskEntity> riskEntities) {

        List<RiskEntity> riskEntityList = new ArrayList<>();

        if (riskEntities != null) {

            for (RiskEntity riskEntity : riskEntities) {

                riskRepository.save(riskEntity);

                RiskEntity riskEntity1 = riskRepository.getOne(riskEntity.getId());

                riskEntityList.add(riskEntity1);
            }
        }
        return riskEntityList;
    }

    @Override
    public void deleteRiskEntityList(List<RiskEntity> riskEntities) {

        if (riskEntities != null) {

            for (RiskEntity riskEntity : riskEntities) {

                riskRepository.delete(riskEntity);
            }
        }
    }
}
