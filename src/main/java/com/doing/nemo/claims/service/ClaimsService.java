package com.doing.nemo.claims.service;

import com.doing.nemo.claims.controller.payload.request.IdRequestV1;
import com.doing.nemo.claims.controller.payload.request.ClaimsInsertRequestV1;
import com.doing.nemo.claims.controller.payload.request.ClaimsUpdateRequestV1;
import com.doing.nemo.claims.controller.payload.request.EmailTemplateMessagingValidatedRequestV1;
import com.doing.nemo.claims.controller.payload.request.claims.DamagedRequest;
import com.doing.nemo.claims.controller.payload.request.refund.SplitRequest;
import com.doing.nemo.claims.controller.payload.response.*;
import com.doing.nemo.claims.dto.FilterView;
import com.doing.nemo.claims.dto.ClaimsViewSearchDTO;
import com.doing.nemo.claims.entity.*;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsFlowEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsUpdateProcedureEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.enumerated.DTO.ClaimsStats;
import com.doing.nemo.claims.entity.enumerated.EventTypeEnum;
import com.doing.nemo.claims.entity.jsonb.Historical;
import com.doing.nemo.claims.entity.jsonb.cai.Cai;
import com.doing.nemo.claims.entity.jsonb.damaged.Damaged;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.Customer;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.InsuranceCompany;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbAntiTheftService.AntiTheftRequest;
import com.doing.nemo.claims.entity.jsonb.refund.Refund;
import com.doing.nemo.claims.entity.settings.AntiTheftServiceEntity;
import com.doing.nemo.claims.entity.settings.ContractTypeEntity;
import com.doing.nemo.commons.exception.InternalException;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.scheduling.annotation.Async;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

public interface ClaimsService {


    Map<String,Object> getFlowDetailsByCai(Cai cai, String contractType, Damaged damaged, List<CounterpartyEntity> counterpartyList, Boolean isCaiSignedA, Boolean isCaiSignedB);

    Stats getClaimsStats(Boolean isInEvidence);

    ClaimsResponseStatsEvidenceV2 getClaimsStatsV2(Boolean inEvidence, Boolean withContinuation);

    ClaimsStats getClaimsStatsAllStatus();

    Pagination<ClaimsEntity> getClaimsPagination(Boolean inEvidence, Long practiceId, String flow, List<Long> clientIdList, String plate, String status,
                                                 String locator, String dateAccidentFrom, String dateAccidentTo,
                                                 String typeAccident, Boolean asc, String orderBy, int page, int pageSize, Boolean withContinuation, String company,  Boolean withReceptions, String clientName, Boolean poVariation, Long fleetVehicleId);

    Pagination<ClaimsEntity> getClaimsPaginationWithoutDraftAndTheft(Boolean inEvidence, Long practiceId, String flow, List<Long> clientListId, String plate, String status,
                                                                     String locator, String dateAccidentFrom, String dateAccidentTo,
                                                                     String typeAccident, Boolean asc, String orderBy, int page, int pageSize, Boolean withContinuation, String company, String clientName, Boolean poVariation, Boolean waitingForNumberSx, Long fleetVehicleId, Boolean waitingForAuthority);

    ClaimsViewSearchDTO getClaimsViewPaginationWithoutDraftAndTheft(FilterView claimsRequestFilterViewDTO,
                                                                    Boolean asc, String orderBy, int page, int pageSize);

    Pagination<ClaimsEntity> getClaimsPaginationTheftOperator(Boolean inEvidence, Long practiceId, String flow, String clientId, String plate, String status,
                                                              String locator, String dateAccidentFrom, String dateAccidentTo,
                                                              String typeAccident, Boolean asc, String orderBy, int page, int pageSize, Boolean withContinuation, String company, String clientName, Boolean poVariation, Boolean waitingForNumberSx, Long fleetVehicleId, Boolean waitingForAuthority);

    Pagination<ClaimsEntity> getClaimsPaginationOnlyTheft(Boolean inEvidence, Long practiceId, String flow, String clientId, String plate, String status,
                                                          String locator, String dateAccidentFrom, String dateAccidentTo, Boolean asc, String orderBy, int page, int pageSize, Boolean withContinuation, String company,  Boolean withReceptions, String clientName);
    //REFACTOR
    List<Historical> getClaimsHistorical(String idClaims);
    /*Pagination<CounterpartyResponseV1> getCounterpartyPagination(String denomination, String plate, Long practiceId, String statusRepair, String dateCreatedFrom,
                                                                 String dateCreatedTo, Boolean asc, String orderBy, int page, int pageSize, String addressSx, String fromCompany, String dateSxFrom, String dateSxTo, String region, String province, String lastContact, String assignedTo, String locality, String recall) throws IOException;
    */
    void deleteClaims(String idClaims);
    //REFACTOR
    void setAutomaticEntrust() throws IOException;

    UploadFileResponseV1 setPdfOnFileManager (String claimsId, String pdfBase64, String fileManagerId);
    //REFACTOR
    ClaimsEntity patchValidateAttachments(String claimsId, List<IdRequestV1> idAttachments);
    ClaimsEntity patchValidateAttachmentsCounterparty(String claimsId, String counterpartyId, List<IdRequestV1> idAttachments);
    void sendEmailTheft ();
    void sendEmailTheftChangeStatus () throws IOException;
    void sendToClient() throws IOException;
    List<AntiTheftRequestNewEntity> getLogForClaim(String claimsId);
    //List<ClaimsEntity> patchIsWithReceptionsTheft(String claimsId);
    boolean checkIsCard(Damaged damaged, List<CounterpartyEntity> counterpartyList);
    Pagination<ClaimsEntity> getClaimsPaginationAuthority(List<String> plate, String chassis,  int page, int pageSize, Boolean isFirstCheck);
    ClaimsEntity updateLinkable(String idClaims);
    Damaged getInfoChangeDate(ClaimsEntity claimsEntity, Date newDate) throws IOException;
    ClaimsStatusEnum getStatusChangeFlow(ClaimsEntity claimsEntity, ClaimsFlowEnum claimsFlowOld, Boolean oldWithCounterparty ) throws NoSuchMethodException;

    boolean checkPolicy(InsuranceCompany insuranceCompany, Date dateAccident, Customer customer);
    ClaimsEntity checkInfoClaimsForWaitingForValidation(ClaimsEntity claimsDraft, String username) ;
    //ClaimsEntity checkStatusByFlowExternalAndDraftLog (ClaimsEntity claimsEntity, String userName);
    ClaimsEntity checkStatusByFlowInternal (ClaimsEntity claimsEntity, String userName, List<EmailTemplateMessagingValidatedRequestV1> emailTemplateListRequestV1List, boolean isIncomplete);
    List<EventTypeEnum> checkEventTypeByFlowInternal (ClaimsEntity claimsEntity, boolean isIncomplete);
    ClaimsEntity checkStatusByFlowExternalAndDraft (ClaimsEntity claimsEntity);
    void checkCaiWithMoreCounterparty (ClaimsEntity claimsEntity);
    void checkWoundedAttachments (ClaimsEntity claimsEntity);
    void checkDeponentAttachments (ClaimsEntity claimsEntity);
    Boolean isPai (ClaimsEntity claimsEntity);

    //REFACTOR
    @Retryable(
            maxAttempts = 3,
            backoff = @Backoff(delay = 1), value = { InternalException.class })
    @Async
    void callToOctoAsync(ClaimsNewEntity claimsNewEntity, ClaimsStatusEnum previousStatus, AntiTheftRequest antiTheftRequest, String userId, String userName, String idAntiTheftRequest);

    //REFACTOR
    @Recover
    void recoverCallToOcto(ClaimsNewEntity claimsEntity, ClaimsStatusEnum previousStatus, AntiTheftRequest antiTheftRequest,String userId, String userName, String recoverCallToOcto);

    //REFACTOR
    @Retryable(
            maxAttempts = 3,
            backoff = @Backoff(delay = 1))
    void callToOcto(ClaimsNewEntity claimsNewEntity, ClaimsStatusEnum previousStatus, AntiTheftRequest antiTheftRequest, String user, String userName,  String idAntiTheftRequest);
    AntiTheftServiceEntity getAntiTheftServiceByRegistry (ClaimsNewEntity claimsEntityNew);

    ClaimsEntity checkALDvsALD(ClaimsEntity claimsEntity);

    ContractTypeEntity getContractType(String contractType);

    ClaimsFlowEnum getFlowContractType(String contractType, DataAccidentTypeAccidentEnum claimsType);
    ClaimsFlowEnum getPersonalRiskFlowType(String contractType, DataAccidentTypeAccidentEnum claimsType, String customerId);

    ClaimsEntity adptClaimsInsertRequestToClaimsEntityWithFlow (ClaimsInsertRequestV1 claimsInsertRequestV1);
    ClaimsStatusEnum checkStatusByFlowValidated (ClaimsEntity claimsEntity);
    Boolean isInsuranceChanged(Damaged damaged, ClaimsEntity claimsEntity);

    //REFACTOR
    ClaimsUpdateProcedureEnum checkModifyClaims(ClaimsUpdateRequestV1 claimsUpdateRequest) throws IOException;

    //REFACTOR /*SPLIT*/
    ClaimsEntity addSplit(String claimsId, List<SplitRequest> splitList);
    //REFACTOR
    ClaimsEntity deleteSplit(String claimsId, String splitId);
    //REFACTOR
    ClaimsEntity updateSplit(String claimsId, String splitId, SplitRequest splitUpdateRequest);

    void checkClosureByTypeAndStatus(ClaimsEntity claimsEntity, ClaimsStatusEnum newClosureStatus, EventTypeEnum nextEvent);
    /*PATCH CONTRACT*/
    ClaimsEntity patchContractInfo(String claimsId, DamagedRequest damagedRequest, String userId, String userName);
    AlertCodeListResponseV1 checkSameCustomerAldVSAld(ClaimsInsertRequestV1 claimsInsertRequestV1);
    String createChangeInfoDescriptinForLog(String description , String attribute, String oldValue, String newValue );

    Refund setRefundFranchiseAmountFcm (ClaimsEntity claimsEntity, Refund refund);
    boolean isNotClosedTheft(String plate, String date);


    //funzioni che recuperano dal nuovo e trasformano direttamente nel vecchio
    ClaimsEntity findById(String claimsId);
    ClaimsNewEntity findByIdNewEntity(String claimsId);
    ClaimsEntity findByIdPratica(Long practiceId);

    void deleteClaimsInBozzaRange(int range);
    void updateClaimsEvidence();
    void saveClaimsNew(ClaimsNewEntity claimsNewEntity);

    NoteEntity findClaimNoteById(String idNote);

    ClaimsEntity toggleHiddenAttachment(String idClaims, String idAttachments);


    ClaimsResponseV1 isPending(ClaimsResponseV1 claimsResponseV1);
    Boolean isPending(Long practiceId);
    UploadFileResponseV1 buildCsvPaginationExport(Boolean inEvidence, Long practiceId, String flow, List<Long> clientListId, String plate,
                                                  String status, String locator, String dateAccidentFrom,
                                                  String dateAccidentTo, String typeAccident, Boolean asc, String orderBy, int page, int pageSize, Boolean withContinuation, String company, String clientName, Boolean poVariation, Boolean waitingForNumberSx, Long fleetVehicleId, Boolean waitingForAuthority) throws IOException;


}
