package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.dto.*;
import com.doing.nemo.claims.entity.CenterEntity;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.CounterpartyEntity;
import com.doing.nemo.claims.entity.enumerated.AuthorityEnum.AuthorityEventTypeEnum;
import com.doing.nemo.claims.entity.jsonb.Address;
import com.doing.nemo.claims.entity.jsonb.Authority;
import com.doing.nemo.claims.entity.jsonb.HistoricalAuthority;
import com.doing.nemo.claims.entity.jsonb.complaint.Complaint;
import com.doing.nemo.claims.entity.jsonb.complaint.jsonbComplaint.Entrusted;
import com.doing.nemo.claims.entity.jsonb.complaint.jsonbComplaint.jsonbDataAccidents.DataAccident;
import com.doing.nemo.claims.entity.jsonb.counterparty.InsuranceCompanyCounterparty;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.Insured;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbDriver.Driver;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbDriver.DrivingLicense;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbVehicle.Vehicle;
import com.doing.nemo.claims.entity.jsonb.forms.jsonbForms.Attachment;
import com.doing.nemo.claims.entity.jsonb.repair.Canalization;
import com.doing.nemo.claims.entity.jsonb.repair.HistoricalCounterparty;
import com.doing.nemo.claims.entity.jsonb.repair.LastContact;
import com.doing.nemo.claims.entity.settings.ManagerEntity;
import com.doing.nemo.claims.service.ExportFieldTranslator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service("CtpExportMapFillerImpl")
public class CtpExportMapFillerImpl extends ExportMapFiller {

    @Autowired
    private ExportFieldTranslator fieldTranslator;

    public Map<String, Object> ctpRepair(
            Map<String, Object> map,
            CounterpartyEntity counterParty,
            LastContact lastContactRealLastByDate,
            Attachment lastAttachmentRealLastByDate,
            CtpExportFilter filter) {
        // dd-MM-yyyy
        SimpleDateFormat simpleDateFormatDate = getDateFormat();
        // HH:mm
        SimpleDateFormat simpleDateFormatHour = getHourFormat();
        // dd-MM-yyyy HH:mm
        SimpleDateFormat simpleDateFormatFull = getTimestampFormat();

        map.put("download", "MSA"); // BQ
        map.put("counterparty_id", counterParty.getPracticeIdCounterparty()); // A
        map.put("user_created_at", formatDate(simpleDateFormatFull, counterParty.getCreatedAt())); // E
        map.put("update_at", formatDate(simpleDateFormatFull, counterParty.getUpdateAt())); // G
        map.put("policy_number", counterParty.getPolicyNumber()); // X
        if(counterParty.getAskedForDamages() != null) {
            map.put("asked_for_damages", counterParty.getAskedForDamages() ? "SI" : "NO"); // AB
        }
        if(counterParty.getLegalOrConsultant() != null) {
            map.put("legal_or_consultant", counterParty.getLegalOrConsultant() ? "SI" : "NO"); // AC
        }
        map.put("date_request_damages", formatDate(simpleDateFormatFull, counterParty.getInstantRequestDamages())); // AD
        map.put("expiration_date", formatDate(simpleDateFormatFull, counterParty.getExpirationInstant())); // AE
        map.put("legal_name", counterParty.getLegal()); // AG
        map.put("legal_email", counterParty.getEmailLegal()); // AH
        if(counterParty.getReplacementCar() != null) {
            map.put("replacement", counterParty.getReplacementCar() ? "SI" : "NO"); // AI
        }
        map.put("replacement_plate", counterParty.getReplacementPlate()); // AJ
        map.put("replacement_assigned_at", counterParty.getAssignedAt()); //  AK
        map.put("motivation", counterParty.getMotivation()); // BE
        map.put("practice_id", counterParty.getPracticeIdCounterparty()); // BP
        // status
        map.put("status", fieldTranslator.getRepairStatus(counterParty)); // B
        // PROCEDURE
        if(counterParty.getRepairProcedure() != null) {
            map.put("procedure", fieldTranslator.getProcedure(counterParty)); // C
        }
        // TYPE
        if(counterParty.getType() != null) {
            map.put("type", fieldTranslator.getType(counterParty)); // O
        }
        // MANAGEMENT TYPE
        if(counterParty.getManagementType() != null) {
            map.put("management_type", fieldTranslator.getManagementType(counterParty)); // AA
        }
        // CLAIM
        ClaimsEntity claim = counterParty.getClaims();
        if(claim != null) {
            map.put("claim_id", claim.getPracticeId()); // H
            map.put("claim_created_at", formatDate(simpleDateFormatFull, claim.getCreatedAt())); // I
            map.put("documentation_complete", claim.getCompleteDocumentation()); // AF
            String driverCaiPart = "";
            if(claim.getCaiDetails() != null && claim.getCaiDetails().getDriverSide() != null) {
                if(claim.getCaiDetails().getDriverSide().compareToIgnoreCase("A") == 0) { driverCaiPart = "B"; }
                else if(claim.getCaiDetails().getDriverSide().compareToIgnoreCase("B") == 0) { driverCaiPart = "A"; }
                map.put("driver_cai_part", driverCaiPart); // AY claim.getCaiDetails().getDriverSide() = "A" metto B, se = "B" metto A
            }
            String driverCaiSignType = "";
            Boolean isClaimCaiSigned = claim.getDamaged() != null && claim.getDamaged().getCaiSigned() != null && claim.getDamaged().getCaiSigned();
            Boolean isCounterpartyCaiSigned = counterParty.getCaiSigned() != null && counterParty.getCaiSigned();
            if(isClaimCaiSigned ^ isCounterpartyCaiSigned) { driverCaiSignType = "Firma singola"; }
            else if(isClaimCaiSigned && isCounterpartyCaiSigned) { driverCaiSignType = "Firma doppia"; }
            map.put("driver_cai_sign_type", driverCaiSignType); // BA che è MONO? è mono se ho claim.getDamaged().getCaiSigned() oppure ce l'ho in counterpaerty, altrimento DOUBLE
            // COMPLAINT
            Complaint claimComplaint = claim.getComplaint();
            if(claimComplaint != null) {
                // ENTRUSTED
                Entrusted claimComplaintEntrusted = claimComplaint.getEntrusted();
                if(claimComplaintEntrusted != null) {
                    map.put("sx_number_ext", claimComplaintEntrusted.getNumberSxCounterparty()); // Z
                }
                // DATA ACCIDENT
                DataAccident claimComplaintDataAccident = claimComplaint.getDataAccident();
                if(claimComplaintDataAccident != null) {
                    map.put("claim_date", formatDate(simpleDateFormatDate, claimComplaintDataAccident.getDateAccident())); // J
                    map.put("claim_hour", formatDate(simpleDateFormatHour, claimComplaintDataAccident.getDateAccident())); // K
                    // ADDRESS
                    Address claimComplaintDataAccidentAddress = claimComplaintDataAccident.getAddress();
                    if(claimComplaintDataAccidentAddress != null) {
                        map.put("claim_location", claimComplaintDataAccidentAddress.getLocality()); // L
                        map.put("claim_region", claimComplaintDataAccidentAddress.getRegion()); // M
                        map.put("claim_province", claimComplaintDataAccidentAddress.getProvince()); // N
                    }
                }
            }

            // HISTORICAL FETCH
            ClaimHistoricalEvents claimHistoricalEvents = findInHistoricalClaim(claim);
            // CLAIM CREATION
            if(claimHistoricalEvents.getClaimCreation() != null) {
                map.put("user_create", claimHistoricalEvents.getClaimCreation().getUsername()); // D
            }
        }
        // VEHICLE
        Vehicle vehicle = counterParty.getVehicle();
        if(vehicle != null) {
            map.put("vehicle_plate", vehicle.getLicensePlate()); // P
            map.put("vehicle_model", vehicle.getModel()); // Q
        }
        // INSURED
        Insured insured = counterParty.getInsured();
        if(insured != null) {
            map.put("insured_last_name", insured.getLastname()); // R
            map.put("insured_first_name", insured.getFirstname()); // S
            map.put("insured_telephone", insured.getPhone()); // T
        }
        // INSURANCE COMPANY
        InsuranceCompanyCounterparty insuranceCompanyCounterparty = counterParty.getInsuranceCompany();
        if(insuranceCompanyCounterparty != null) {
            map.put("insured_company_name", insuranceCompanyCounterparty.getName()); // V
            map.put("insured_company_id", insuranceCompanyCounterparty.getEntity().getId());

            // ENTITY
            if(insuranceCompanyCounterparty.getEntity() != null) {
                map.put("insurance_company_id", insuranceCompanyCounterparty.getEntity().getCode()); // W
            }
        }
        // MANAGER
        ManagerEntity manager = counterParty.getManager();
        if(manager != null) {
            map.put("manager_name", manager.getName()); // Y
        }
        // CANALIZATION
        Canalization canalization = counterParty.getCanalization();
        if(canalization != null) {
            // CENTER ENTITY
            CenterEntity canalizationCenterEntity = canalization.getCenterEntity();
            if(canalizationCenterEntity != null) {
                map.put("repairer_name", canalizationCenterEntity.getName()); // BH
                map.put("repairer_id", canalizationCenterEntity.getPlaceId()); // BG
                map.put("repairer_email", canalizationCenterEntity.getEmail()); // BI
            }
        }
        // ASSIGNEE REPAIR
        AssigneeRepair assigneeRepair = counterParty.getAssignedTo();
        if(assigneeRepair != null) {
            // PERSONAL DETAILS
            PersonalDetailsRepair assigneeRepairPersonalDetailsRepair = assigneeRepair.getPersonalDetails();
            String assignedTo = "";
            if(assigneeRepairPersonalDetailsRepair != null) {
                assignedTo += assigneeRepairPersonalDetailsRepair.getLastname() + " " + assigneeRepairPersonalDetailsRepair.getFirstname() + " ";
            }
            assignedTo += "(" + assigneeRepair.getId() + ")";
            map.put("replacement_assigned_to", assignedTo); // AL
        }
        // DRIVER
        Driver driver = counterParty.getDriver();
        if(driver != null) {
            map.put("driver_surname", driver.getLastname()); // AM
            map.put("driver_name", driver.getFirstname()); // AN
            map.put("driver_telephone", driver.getPhone()); // AS
            map.put("driver_email", driver.getEmail()); // AT
            map.put("driver_fiscal_code", driver.getFiscalCode()); // AU
            map.put("driver_birth_date", formatDate(simpleDateFormatDate, driver.getDateOfBirth())); // AV
            if(counterParty.getCaiSigned() != null) {
                map.put("driver_cai_signed", counterParty.getCaiSigned() ? "SI" : "NO"); // AZ
            }
            // ADDRESS
            Address driverAddress = driver.getMainAddress();
            if(driverAddress != null) {
                map.put("driver_address", driverAddress.getFormattedAddress()); // AO
                map.put("driver_cap", driverAddress.getZip()); // AP
                map.put("driver_locality", driverAddress.getLocality()); // AQ
                map.put("driver_province", driverAddress.getProvince()); // AR
            }
            // DRIVING LICENSE
            DrivingLicense driverDrivingLicense = driver.getDrivingLicense();
            if(driverDrivingLicense != null) {
                map.put("driver_license", driverDrivingLicense.getNumber()); // AW
                map.put("driver_license_category", driverDrivingLicense.getCategory()); // AX
            }
        }
        // UPDATING LAST CONTACT CTP FIELDS WITH VALUES FOUND POPULATING THE CALLS SHEET
        if(lastContactRealLastByDate != null) {
            map.put("contact_last_call_note", lastContactRealLastByDate.getNote()); // BF
            map.put("contact_last_call_date", formatDate(simpleDateFormatFull, lastContactRealLastByDate.getLastCall())); // BB
            // RESULT
            if(lastContactRealLastByDate.getResult() != null) {
                map.put("contact_result", fieldTranslator.getContactResult(lastContactRealLastByDate)); // BC
            }
            map.put("contact_recall_date", formatDate(simpleDateFormatFull, lastContactRealLastByDate.getRecall())); // BD
        }
        // UPDATING LAST ATTACHMENT
        if(lastAttachmentRealLastByDate != null) {
            map.put("last_receive_attachments", formatDate(simpleDateFormatFull, lastAttachmentRealLastByDate.getCreatedAt())); // BI
        }
        // HISTORICAL FETCH
        CounterPartyHistoricalEvents counterpartyHistoricalEvents = findInHistoricalCounterparty(counterParty, map);
        // IF USER_UPDATE IS STILL EMPTY
        map.putIfAbsent("user_update", "");
        if(map.get("user_update").toString().isEmpty() && claim != null && claim.getHistorical() != null && !claim.getHistorical().isEmpty()) {
            map.put("user_update", claim.getHistorical().get(claim.getHistorical().size() - 1).getUserName()); // F
        }
        // LEGAL
        if(counterpartyHistoricalEvents.getLegal() != null) {
            map.put("communicated_legal_or_consultant", counterpartyHistoricalEvents.getLegal() != null ? "SI" : "NO"); // BU se esiste l'historical
            map.put("communicated_legal_or_consultant_date", formatDate(simpleDateFormatFull, counterpartyHistoricalEvents.getLegal().getDate())); // BV "event_type": "GENERIC_LEGAL_COMUNICATION"
        }
        else {
            map.put("communicated_legal_or_consultant", "NO"); // BU
        }
        // REPAIRER
        if(counterpartyHistoricalEvents.getRepairer() != null) {
            map.put("repairer_sent_to", counterpartyHistoricalEvents.getRepairer().getDate()); // BJ
        }
        // COUNTERPARTY
        map.put("communicated", (counterpartyHistoricalEvents.getCounterpartyDriver() != null || counterpartyHistoricalEvents.getCounterpartyInsured() != null) ? "SI" : "NO"); // BR negli historical "comunicazione controparte"
        if(counterpartyHistoricalEvents.getCounterpartyDriver() != null) {
            map.put("communicated_type", "CONDUCENTE"); // BS
            map.put("communicated_date", formatDate(simpleDateFormatFull, counterpartyHistoricalEvents.getCounterpartyDriver().getDate())); // BT
        }
        if(counterpartyHistoricalEvents.getCounterpartyInsured() != null) {
            map.put("communicated_type", "ASSICURATO"); // BS
            map.put("communicated_date", formatDate(simpleDateFormatFull, counterpartyHistoricalEvents.getCounterpartyInsured().getDate())); // BT
        }

        //PLATE ALD
        map.put("plate_ald",filter.getPlateAld());

        return map;
    }

    public Map<String, Object> attachmentRepair(
            Map<String, Object> map,
            CounterpartyEntity counterParty,
            Attachment attachment
    ) {
        map.put("counterparty_id", counterParty.getPracticeIdCounterparty());
        if(counterParty.getRepairStatus() != null) {
            map.put("status", fieldTranslator.getRepairStatus(counterParty));
        }
        if(attachment.getFileManagerName() != null) {
            if(attachment.getFileManagerName().startsWith("AUTH_")) {
                map.put("tipologia_denuncia", attachment.getDescription());
                map.put("nomefile_denuncia", attachment.getFileManagerName());
            }
            else {
                map.put("tipologia_manutenzione", attachment.getDescription());
                map.put("nomefile_manutenzione", attachment.getFileManagerName());
            }
        }
        return map;
    }

    public Map<String,Object> calls(
            Map<String, Object> lastContactMap,
            CounterpartyEntity counterParty,
            LastContact lastContact
    ) {
        // dd-MM-yyyy HH:mm
        SimpleDateFormat simpleDateFormatFull = this.getTimestampFormat();

        lastContactMap.put("counterparty_id", counterParty.getPracticeIdCounterparty());
        // REPAIR STATUS
        if(counterParty.getRepairStatus() != null) {
            lastContactMap.put("status", fieldTranslator.getRepairStatus(counterParty));
        }
        // RESULT
        if(lastContact.getResult() != null) {
            lastContactMap.put("contact_result", fieldTranslator.getContactResult(lastContact));
        }
        lastContactMap.put("contact_date", formatDate(simpleDateFormatFull, lastContact.getLastCall()));
        if(lastContact.getMotivation() != null) {
            lastContactMap.put("motivation", lastContact.getMotivation().getDescription());
        }
        lastContactMap.put("contact_last_call_note", lastContact.getNote());
        return lastContactMap;
    }

    public LinkedList<Map<String, Object>> ctpListRepair(CounterpartyEntity cunterParty, LinkedList<Map<String, Object>> ctpList, Map<String,Object> ctpMap) {
        SimpleDateFormat simpleDateFormatFull = this.getTimestampFormat();
        simpleDateFormatFull.setTimeZone( TimeZone.getTimeZone("Europe/Rome") );

        List<Authority> authorities = cunterParty.getAuthorities();
        if(authorities != null) {
            for (Authority authority: authorities) {
                Map<String, Object> ctpMapClone = new HashMap<>(ctpMap);
                ctpMapClone.put("quotation_amount", authority.getTotal()); // BN n righe per n authorities
                ctpMapClone.put("date_result_authorization", formatDate(simpleDateFormatFull, authority.getAuthorizationDate())); // BP n righe per n autorities
                // STATUS
                if(authority.getStatus() != null) {
                    ctpMapClone.put("repair_state", fieldTranslator.getRepairState(authority)); // BO
                }
                // FETCH HISTORICALS
                AuthorityHistoricalEvents authorityHistoricalEvents = findInHistoricalAuthorities(authority);
                if(authorityHistoricalEvents.getRepairSend() != null) {
                    ctpMapClone.put("quotation_sent_date", formatDate(simpleDateFormatFull, authorityHistoricalEvents.getRepairSend().getDate())); // BM
                }
                // REPAIRER
                if(authority.getAcceptingDate() != null) {
                    ctpMapClone.put("repairer_sent_to", formatDate(simpleDateFormatFull, authority.getAcceptingDate())); // BK
                }

                ctpList.add(ctpMapClone);
            }
        }
        else {
            ctpList.add(ctpMap);
        }
        return ctpList;

    }

    private ClaimHistoricalEvents findInHistoricalClaim(ClaimsEntity claim) {
        ClaimHistoricalEvents claimHistoricalEvents = new ClaimHistoricalEvents();
        if(claim.getHistorical() != null && !claim.getHistorical().isEmpty()) {
            claimHistoricalEvents.setClaimCreation(new HistoricalEventObject(claim.getHistorical().get(0).getUserName()));
        }
        return claimHistoricalEvents;
    }

    private CounterPartyHistoricalEvents findInHistoricalCounterparty(CounterpartyEntity counterparty, Map<String,Object> ctpMap) {
        SimpleDateFormat simpleDateFormatFull = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        simpleDateFormatFull.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));

        CounterPartyHistoricalEvents counterpartyHistoricalEvents = new CounterPartyHistoricalEvents();
        if(counterparty.getHistoricals() != null) {
            HistoricalCounterparty lastHistorical = null;
            for (HistoricalCounterparty historical: counterparty.getHistoricals()) {
                // LOOKING FOR LAST HISTORICAL
                if(lastHistorical == null || (lastHistorical.getUpdateAt() != null && lastHistorical.getUpdateAt().before(historical.getUpdateAt()))) {
                    lastHistorical = historical;
                }
                // SWITCH EVENT TYPES
                if(historical.getEventType() != null) {
                    switch (historical.getEventType()) {
                        case GENERIC_LEGAL_COMUNICATION:
                            counterpartyHistoricalEvents.setLegal(new HistoricalEventObject(historical.getUpdateAt()));
                            break;
                        case GENERIC_COUNTERPARTY_INSURED_COMUNICATION:
                            counterpartyHistoricalEvents.setCounterpartyInsured(new HistoricalEventObject(historical.getUpdateAt()));
                            break;
                        case GENERIC_COUNTERPARTY_DRIVER_COMUNICATION:
                            counterpartyHistoricalEvents.setCounterpartyDriver(new HistoricalEventObject(historical.getUpdateAt()));
                            break;
                    }
                }
            }
            if(lastHistorical != null) {
                ctpMap.put("user_update", lastHistorical.getUserName()); // F
            }
        }
        return counterpartyHistoricalEvents;
    }

    private AuthorityHistoricalEvents findInHistoricalAuthorities(Authority authority) {
        AuthorityHistoricalEvents authorityHistoricalEvents = new AuthorityHistoricalEvents();
        if(authority.getHistorical() != null) {
            HistoricalAuthority lastHistorical = null;
            List<HistoricalAuthority> historicalAuthoritiesOnlySend = authority.getHistorical().stream().filter(h -> h.getEventType().equals(AuthorityEventTypeEnum.SEND)).collect(Collectors.toList());
            for (HistoricalAuthority historical: historicalAuthoritiesOnlySend) {
                // LOOKING FOR LAST HISTORICAL
                if(lastHistorical == null || lastHistorical.getUpdateAt().before(historical.getUpdateAt())) {
                    lastHistorical = historical;
                }
            }
            if(lastHistorical != null) {
                authorityHistoricalEvents.setRepairSend(new HistoricalEventObject(lastHistorical.getUpdateAt()));
            }
        }
        return authorityHistoricalEvents;
    }

}