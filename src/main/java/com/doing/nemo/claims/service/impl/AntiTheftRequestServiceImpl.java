package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.adapter.OctoAdapter;
import com.doing.nemo.claims.adapter.TexaAdapter;
import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.crashservices.texa.org.datacontract.schemas._2004._07.texa_tmd_bol_m2mtrack_ald_webservice_service.GetVoucherInfoResponse;
import com.doing.nemo.claims.entity.AntiTheftRequestEntity;
import com.doing.nemo.claims.entity.AntiTheftRequestNewEntity;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.ClaimsNewEntity;
import com.doing.nemo.claims.repository.AntiTheftRequestNewRepository;
import com.doing.nemo.claims.entity.jsonb.forms.jsonbForms.Attachment;
import com.doing.nemo.claims.repository.AntiTheftRequestRepository;
import com.doing.nemo.claims.service.AntiTheftRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AntiTheftRequestServiceImpl implements AntiTheftRequestService {

    @Autowired
    private AntiTheftRequestRepository antiTheftRequestRepository;

    @Autowired
    private TexaAdapter texaAdapter;

    @Autowired
    private OctoAdapter octoAdapter;


    @Autowired
    private AntiTheftRequestNewRepository antiTheftRequestNewRepository;



    @Override
    public void insertAntiTheftRequest(String idAntiTheftRequest, ClaimsNewEntity claimsNewEntity, String providerType, String idTransaction) {

        AntiTheftRequestNewEntity antiTheftRequestEntity = new AntiTheftRequestNewEntity();

        if(idTransaction == null){
            //recupero il numero di storico di quel claims
            List<AntiTheftRequestNewEntity> antiTheftRequestEntityList = antiTheftRequestNewRepository.getAllCrashByClaim(claimsNewEntity.getId());
            Integer countCrashHistorical = antiTheftRequestEntityList != null ?  antiTheftRequestEntityList.size()+1 : null;

            antiTheftRequestEntity.setIdTransaction(countCrashHistorical != null ? countCrashHistorical.toString() : "1");

        }else {
            antiTheftRequestEntity.setIdTransaction(idTransaction);
        }
        antiTheftRequestEntity.setId(idAntiTheftRequest);
        antiTheftRequestEntity.setClaim(claimsNewEntity);
        antiTheftRequestEntity.setPracticeId(claimsNewEntity.getPracticeId().toString());
        antiTheftRequestEntity.setProviderType(providerType);
        antiTheftRequestEntity.setResponseMessage("Pending");


        antiTheftRequestEntity.setResponseType("0");
        antiTheftRequestEntity.setCreatedAt(DateUtil.getNowDate());

        antiTheftRequestNewRepository.save(antiTheftRequestEntity);
    }

    @Override
    public void updateAntiTheftRequest(String idAntiTheftRequest, GetVoucherInfoResponse responseTexa, com.doing.nemo.claims.crashservices.octo.GetVoucherInfoResponse responseOCTO, String responseType, Attachment crashReport, String responseMessage ) {

        Optional<AntiTheftRequestNewEntity> antiTheftRequestEntityOptional = antiTheftRequestNewRepository.findById(idAntiTheftRequest);
        if(!antiTheftRequestEntityOptional.isPresent()){
            //loggata errore qualcosa è andato storto
        }
        AntiTheftRequestNewEntity antiTheftRequestEntity = antiTheftRequestEntityOptional.get();

        //fare 2 adapter oer octo/texa e poi settare dentro gli altri campi


        if(responseTexa != null) {
            antiTheftRequestEntity = texaAdapter.adptFromTexaResponseToAntiTheftServiceEntity(responseTexa,antiTheftRequestEntity );
        }else if(responseOCTO != null){
            antiTheftRequestEntity = octoAdapter.adptFromOctoResponseToAntiTheftServiceEntity(responseOCTO, antiTheftRequestEntity);
        }


        if(responseType != null){
            antiTheftRequestEntity.setResponseType(responseType);
        }

        antiTheftRequestEntity.setCrashReport(crashReport);
        if(crashReport!=null){
            antiTheftRequestEntity.setPdf(crashReport.getFileManagerId());
        }
        if(responseMessage != null){
            antiTheftRequestEntity.setResponseMessage(responseMessage);
        }

        antiTheftRequestEntity.setUpdatedAt(DateUtil.getNowDate());

        antiTheftRequestNewRepository.save(antiTheftRequestEntity);

    }
}
