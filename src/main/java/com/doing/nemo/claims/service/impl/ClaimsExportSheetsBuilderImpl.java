package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.service.ClaimsExportSheetsBuilder;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.TimeZone;

@Service
public class ClaimsExportSheetsBuilderImpl implements ClaimsExportSheetsBuilder {

    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

    public Map<String, Object> build() {
        simpleDateFormat.setTimeZone( TimeZone.getTimeZone("Europe/Rome") );

        Map<String, Object> requestMap = new HashMap<>();

        requestMap.put("claims", new LinkedList<Map<String, Object>>()); // sinistri
        requestMap.put("ctp", new LinkedList<Map<String, Object>>()); // ctp
        requestMap.put("authority", new LinkedList<Map<String, Object>>()); // authority
        requestMap.put("refund", new LinkedList<Map<String, Object>>()); // rimborso
        requestMap.put("wounded", new LinkedList<Map<String, Object>>()); // feriti
        requestMap.put("deponent", new LinkedList<Map<String, Object>>()); // testimoni
        requestMap.put("attachment", new LinkedList<Map<String, Object>>()); // allegato
        requestMap.put("additional_costs", new LinkedList<Map<String, Object>>()); // spese accessorie
        requestMap.put("log", new LinkedList<Map<String, Object>>()); // Log
        requestMap.put("ctp_repair", new LinkedList<Map<String, Object>>()); // ctp repair
        requestMap.put("calls", new LinkedList<Map<String, Object>>()); // ?
        requestMap.put("attachment_repair", new LinkedList<Map<String, Object>>()); // allegato rapair
        requestMap.put("export_date", "Data: " + simpleDateFormat.format(DateUtil.getNowDate())); // data di export

        return requestMap;
    }

    @Override
    public LinkedList<Map<String, Object>> get(Map<String, Object> map, String sheetName) {
        if(
            sheetName != null &&
            !sheetName.equals("") &&
            map.get(sheetName) != null &&
            map.get(sheetName) instanceof LinkedList
        ) {
            return (LinkedList<Map<String, Object>>)map.get(sheetName);
        }
        else {
            return null;
        }
    }

}
