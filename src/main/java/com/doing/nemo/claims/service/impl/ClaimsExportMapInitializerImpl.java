package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.service.ExportMapInitializer;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.Map;

@Service("ClaimsExportMapInitializerImpl")
public class ClaimsExportMapInitializerImpl implements ExportMapInitializer {

    private Map<String, Object> baseInitializer() {
        Map<String, Object> innerMap = new HashMap<>();
        innerMap.put("idcomplaint", ""); // ID PRATICA
        innerMap.put("targa_danneggiato", ""); // TARGA DANNEGGIATO
        innerMap.put("data_sx", ""); // DATA SINISTRO
        return innerMap;
    }

    private Map<String, Object> personInitializer(Map<String, Object> innerMap) {
        innerMap.put("cognome", ""); // COGNOME
        innerMap.put("nome", ""); // NOME
        innerMap.put("indirizzo", ""); // INDIRIZZO
        return innerMap;
    }

    public Map<String, Object> claims() {
        Map<String, Object> innerMap = this.baseInitializer();

        //STATO PRATICA
        innerMap.put("status", "");
        //DATA INSERIMENTO
        innerMap.put("inserita_il", "");
        //UTENTE INSERIMENTO
        innerMap.put("utente", "");
        //TIPOLOGIA FLUSSO
        innerMap.put("tipo_flusso", "");
        //MOTIVAZIONE
        innerMap.put("motivincomp_des", "");
        //EVIDENZA
        innerMap.put("evidenza", "");
        //RICEZIONE DENUNCIA
        innerMap.put("ricev_denuncia", "");
        //TIPO MODULO
        innerMap.put("tipo_modulo", "");
        //DESCRIZIONE NOTIFICA
        innerMap.put("notifica_des", "");
        //TIPOLOGIA SINISTRO
        innerMap.put("tiposx_des", "");
        //ORA SINISTRO
        innerMap.put("ora_sx", "");
        //INDIRIZZO SINISTRO
        innerMap.put("indirizzo_sx", "");
        //LOCALITA SINISTRO
        innerMap.put("localita_sx", "");
        //REGIONE SINISTRO
        innerMap.put("regione_sx", "");
        //PROV SINISTRO
        innerMap.put("prov_sx", "");
        //RECUPERABILITA
        innerMap.put("recuperabilita", "");
        //PERC RECUPERABILITA
        innerMap.put("recuperabilita_perc", "");
        //CONTROPARTI
        innerMap.put("controparti", "");
        //AUTORITA PS
        innerMap.put("autorita_ps", "");
        //AUTORITA VVUU
        innerMap.put("autorita_vvuu", "");
        //AUTORITA DESC
        innerMap.put("autorita_desc", "");
        //CITAZIONE
        innerMap.put("citazione", "");
        //CAI FIRMATO A
        innerMap.put("cai_firmato_a", "");
        //CAI FIRMATO B
        innerMap.put("cai_firmato_b", "");
        //DANNI VEICOLO
        innerMap.put("veicolo_danni", "");
        //OSSERVAZIONI VEICOLO
        innerMap.put("veicolo_osservazioni", "");
        //COMUNICAZIONE PAI
        innerMap.put("comunicazione_pai", "");
        //COMUNICAZIONE LEGAL
        innerMap.put("comunicazione_legal", "");
        //SEGUITI DA CONFERMARE
        innerMap.put("seguiti_confermare", "");
        //DRIVER COGNOME
        innerMap.put("driver_cognome", "");
        //DRIVER NOME
        innerMap.put("driver_nome", "");
        //DRIVER TEL
        innerMap.put("driver_tel", "");
        //DRIVER EMAIL
        innerMap.put("driver_email", "");
        //DRIVER LESIONI
        innerMap.put("driver_lesioni", "");
        //ELEGGIBILITA
        innerMap.put("eleggibilita", "");
        //CANALIZZAZIONE
        innerMap.put("canalizzazione", "");
        //TARGA DANNEGGIATO
        innerMap.put("targa_danneggiato", "");
        //VEICOLO DESCRIZIONE
        innerMap.put("veicolo_desc", "");
        //ID CONTRATTO
        innerMap.put("contratto_id", ""); //da tradurre in italiano
        //TIPOLOGIA CONTRATTO
        innerMap.put("contratto_type", "");
        //DATA INIZIO CONTRATTO
        innerMap.put("contratto_begin", "");
        //DATA FINE CONTRATTO
        innerMap.put("contratto_end", "");
        //DATA RITORNO CONTRATTO
        innerMap.put("contratto_return", "");
        //PENALE FURTO
        innerMap.put("penale_furto", "");
        //PENALE RCA
        innerMap.put("penale_rca", "");
        //PENALE DANNI
        innerMap.put("penale_danni", "");
        //CLIENTE ID
        innerMap.put("cliente_id", "");
        //CLIENTE NOME
        innerMap.put("cliente_nome", "");
        //STATUS CLIENTE
        innerMap.put("status_cliente", "");
        //POLIZZA NUM
        innerMap.put("polizza_num", "");
        //COMPAGNIA ASSICURATIVA NOME
        innerMap.put("compass_nome", ""); //sul doc dice non pervenuto -> modulo casagrande
        //COMPAGNIA PAI
        innerMap.put("compagnia_pai", "");
        //POLIZZA PAI
        innerMap.put("polizza_pai", "");
        //COMPAGNIA LEGAL
        innerMap.put("compagnia_legal", "");
        //POLIZZA LEGAL
        innerMap.put("polizza_legal", "");
        //DATI FROM COMPANY
        innerMap.put("ext_numsx", "");
        innerMap.put("ext_tiposxdes", "");
        innerMap.put("ext_ispettorato", "");
        innerMap.put("ext_perito", "");
        innerMap.put("ext_note", "");
        innerMap.put("riserva_globale", "");
        innerMap.put("totale_pagato", "");
        innerMap.put("status_compagnia", "");
        innerMap.put("data_dwl_sx", "");
        //DATA AGGIORNAMENTO SINISTRO - DA VEDERE SE IN FROM COMPANY
        innerMap.put("data_update_sx", "");
        //DATI ENTRUSTED
        innerMap.put("affidata_a", ""); //da tradurre in italiano
        innerMap.put("affidata_il", "");
        innerMap.put("legale_des", "");
        innerMap.put("ispettorato_des", "");
        innerMap.put("manager_des", "");
        innerMap.put("number_sx_ctp", "");
        innerMap.put("data_dwl_affido", "");
        innerMap.put("status_richiestadanni", "");
        //FURTO
        innerMap.put("furto_telefono_autorita", "");
        innerMap.put("avviso_centrale_operativa", "");
        innerMap.put("furto_centrale_ald", "");
        innerMap.put("codice_fornitore", "");
        innerMap.put("ritrov_citta", "");
        innerMap.put("gestione_contabile_furti", "");
        innerMap.put("pos_amm_definita_furti", "");
        innerMap.put("gestione_chiavi", "");
        innerMap.put("ritrov_regione", "");
        innerMap.put("ritrov_prov", "");
        innerMap.put("ritrov_indirizzo", "");
        innerMap.put("sottosequestro", "");
        innerMap.put("ritrov_estero", "");
        innerMap.put("ritrov_autorita_ps", "");
        innerMap.put("ritrov_autorita_cc", "");
        innerMap.put("ritrov_autorita_vvuu", "");
        innerMap.put("ritrov_autorita_desc", "");
        innerMap.put("ritrov_autorita_tel", "");
        innerMap.put("ritrov_veicolo", "");
        innerMap.put("rx_chiave_1", "");
        innerMap.put("rx_chiave_2", "");
        innerMap.put("rx_ori_denuncia", "");
        innerMap.put("rx_cpy_denuncia", "");
        innerMap.put("rapina", "");
        innerMap.put("rx_ori_verbritr", "");
        innerMap.put("rx_cpy_verbritr", "");
        //APPARATO TELEMATICO
        innerMap.put("apparato_telematico", "");
        innerMap.put("anttheft_active", "");
        innerMap.put("atf_lastcheck", "");
        innerMap.put("esito", "");
        innerMap.put("atf_voucherid", "");
        innerMap.put("atf_provider", "");
        innerMap.put("atf_codice_risposta", "");
        innerMap.put("atf_messaggio_risposta", "");
        innerMap.put("atf_numrep", "");
        innerMap.put("atf_forzag", "");
        innerMap.put("atf_numcrash", "");
        innerMap.put("atf_anomalia", "");
        //FRANCHIGIA
        innerMap.put("importo1", "");
        innerMap.put("data1", "");
        innerMap.put("chiusura1", "");
        innerMap.put("importo2", "");
        innerMap.put("data2", "");
        innerMap.put("chiusura2", "");
        innerMap.put("totale_franchigia_pagato", "");
        innerMap.put("numero_polizza", "");
        innerMap.put("riferimento_compagnia", "");
        return innerMap;
    }

    public Map<String, Object> ctp() {
        Map<String, Object> innerMap = this.baseInitializer();
        //TARGA
        innerMap.put("targa_danneggiato", "");
        //COGNOME ASSICURATO
        innerMap.put("ctp_cognome", "");
        //NOME ASSICURATO
        innerMap.put("ctp_nome", "");
        //ASSICURAZIONE
        innerMap.put("ctp_assicurazione", "");
        //TIPOLOGIA VEICOLO
        innerMap.put("ctp_veicolo", "");
        //RESPONSABILE
        innerMap.put("ctp_responsabile", "");
        //DRIVER COGNOME
        innerMap.put("ctp_driver_cognome", "");
        //DRIVER NOME
        innerMap.put("ctp_driver_nome", "");
        //DRIVER INDIRIZZO
        innerMap.put("ctp_driver_indirizzo", "");
        //DRIVER LUOGO DI NASCITA
        innerMap.put("ctp_driver_nascita", "");
        //DRIVER TEL
        innerMap.put("ctp_driver_tel", "");
        //DRIVER EMAIL
        innerMap.put("ctp_driver_email", "");
        //DRIVER PATENTE
        innerMap.put("ctp_driver_patente", "");
        //TIPOLOGIA VEICOLO
        innerMap.put("ctp_tip_veicolo", "");
        //CODICE FISCALE DRIVER
        innerMap.put("ctp_cf", "");
        //CATEGORIA DRIVER
        innerMap.put("ctp_categoria", "");
        //DRIVER VALIDITA
        innerMap.put("ctp_validita_driver", "");
        return innerMap;
    }

    public Map<String, Object> authority() {
        Map<String, Object> innerMap = this.baseInitializer();
        //NOME RIPARATORE
        innerMap.put("riparat_nome", "");
        //STATO PRATICAN AUTHORITY
        innerMap.put("repstatusdes", "");
        //BLOCCO RIPARAZIONE
        innerMap.put("replock", "");
        //NON RIPARABILE - NON DEVE ESSERE VALORIZZATO
        innerMap.put("noriparabile", "");
        //VALORE VEICOLO
        innerMap.put("valveicolo", "");
        //NUM PRATICA
        innerMap.put("practice_number", "");
        //PO NUMBER
        innerMap.put("po_number", "");
        //PO TIPOLOGIA
        innerMap.put("po_tipo", "");
        //PO IMPORTO
        innerMap.put("po_amount", "");
        //PREVENTIVO
        innerMap.put("preventivo", "");
        //DATA AUTORIZZAZIONE PREVENTIVO
        innerMap.put("po_autorizzato", "");
        //DATA RIFIUTO PREVENTIVO
        innerMap.put("po_no_autorizzato", "");
        //WRECK
        innerMap.put("po_totalloss", "");
        //NUMERO PENALI PRATICA
        innerMap.put("penali_practice", "");
        //MOTIVAZIONE RIFIUTO
        innerMap.put("reject_motivation", "");
        //PENALI MILES
        innerMap.put("penali_miles", "");
        //IS INVOICED
        innerMap.put("fatturata", "");
        //SE E' OLDEST
        innerMap.put("oldest", "");
        //LISTA DEI SINISTRI ASSOCIATI
        innerMap.put("sx_associati", "");
        //TIPO PRATICA
        innerMap.put("tipo_pratica", "");
        return innerMap;
    }

    public Map<String, Object> refund() {
        Map<String, Object> innerMap = this.baseInitializer();
        //DATA RICEZIONE SPLIT
        innerMap.put("liq1_dataval", "");
        //TIPOLOGIA PAGAMENTO
        innerMap.put("liq1_tipo", "");
        //IMPORTO VALORE MATERIALE
        innerMap.put("liq1_materiale", "");
        //IMPORTO SPESE LEGALI
        innerMap.put("liq1_legali", "");
        //IMPORTO FERMO TECNICO
        innerMap.put("liq1_fermtec", "");
        //ASSEGNO - BONIFICO
        innerMap.put("liq1_assbon", "");
        //IMPORTO PAGAMENTO SPLIT
        innerMap.put("liq1_pagato", "");
        //DATA DEFINIZIONE
        innerMap.put("liq_datadef", "");
        //TOTALE PAGAMENTI RICEVUTI
        innerMap.put("liq_totliq", "");
        //NOTE PAGAMENTO
        innerMap.put("liq_notepag", "");
        //TOTALE LIQUIDAZIONE RICEVUTA
        innerMap.put("somma_ricevuta", "");
        //WRECK PRE
        innerMap.put("valorerelitto", "");
        //SOMMA PO
        innerMap.put("sommapo", "");
        //WRECK
        innerMap.put("wreck", "");
        //IMPORTO FRANCHIGIA FCM
        innerMap.put("impfranfcm", "");
        //NBV
        innerMap.put("nbv", "");
        //WRECK POST
        innerMap.put("wreckvalue", "");
        //IMPORTO DA ADDEBITARE
        innerMap.put("impdaaddebitare", "");
        //EUROTAX BLU
        innerMap.put("eurotax", "");
        //DATA ADDEBITO DELTA
        innerMap.put("dataaddebitodelta", "");
        //TOTALE RIMBORSO ATTESO
        innerMap.put("totalerimborsoatteso", "");

        return innerMap;
    }

    public Map<String, Object> wounded() {
        Map<String,Object> innerMap = this.baseInitializer();
        innerMap = this.personInitializer(innerMap);
        innerMap.put("tipferito", ""); // TIPOLOGIA FERITO
        innerMap.put("prontosoccorso", ""); // PRONTO SOCCORSO
        innerMap.put("ferite", ""); // GRAVITA FERITE
        innerMap.put("privacy",""); //PRIVACY
        return innerMap;
    }

    public Map<String, Object> deponent() {
        Map<String,Object> innerMap = this.baseInitializer();
        innerMap = this.personInitializer(innerMap);
        innerMap.put("cf", ""); // CODICE FISCALE
        innerMap.put("telefono", ""); // TELEFONO
        innerMap.put("email", ""); // EMAIL
        return innerMap;
    }

    public Map<String, Object> attachment() {
        Map<String,Object> innerMap = this.baseInitializer();
        innerMap.put("tipologia", ""); // TIPOLOGIA
        innerMap.put("nomefile", ""); //NOME FILE
        innerMap.put("visibilita", ""); //VISIBILITA
        return innerMap;
    }

    public Map<String, Object> additionalCosts() {
        Map<String,Object> innerMap = this.baseInitializer();
        innerMap.put("additional_costs_typology", "");
        innerMap.put("additional_costs_amount", "");
        innerMap.put("additional_costs_date", "");
        return innerMap;
    }

}