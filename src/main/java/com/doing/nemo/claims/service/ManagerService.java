package com.doing.nemo.claims.service;

import com.doing.nemo.claims.entity.enumerated.ClaimsRepairEnum;
import com.doing.nemo.claims.entity.settings.ManagerEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public interface ManagerService {

    ManagerEntity insertManager(ManagerEntity managerEntity);

    ManagerEntity updateManager(ManagerEntity managerEntity, UUID uuid);

    ManagerEntity getManager(UUID uuid);

    List<ManagerEntity> getAllManager(ClaimsRepairEnum claimsRepairEnum);

    ManagerEntity deleteManager(UUID uuid);

    ManagerEntity patchActive(UUID uuid);

    List<ManagerEntity> findManagersFiltered (Integer page, Integer pageSize, ClaimsRepairEnum claimsRepairEnum, String name, String prov, String phone, String fax, String email, String externalExportId, Boolean isActive, String orderBy, Boolean asc, String address, String zipCode, String locality, String country, Long code);

    Long countManagerFiltered(ClaimsRepairEnum claimsRepairEnum, String name, String prov, String phone, String fax, String email, String externalExportId, Boolean isActive, String address, String zipCode, String locality, String country, Long code);

}
