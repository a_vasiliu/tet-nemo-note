package com.doing.nemo.claims.service;

import com.doing.nemo.claims.controller.payload.response.UploadFileResponseV1;
import com.doing.nemo.claims.entity.enumerated.FileImportTypeEnum;
import com.doing.nemo.claims.entity.settings.DataManagementExportEntity;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayOutputStream;
import java.rmi.ConnectException;
import java.util.List;
import java.util.UUID;

public interface UploadFileService {

    List<List<String>> decodeFileFromBase64(String fileBase64);
    String encodeFileInBase64(String fileString);
    public String encodeFileInBase64(ByteArrayOutputStream baos);
    UploadFileResponseV1 uploadDeductibleFile(MultipartFile fileToBeImport, String fileContent, String fileDescription, FileImportTypeEnum fileType) throws ConnectException;
    DataManagementExportEntity uploadExportFile(DataManagementExportEntity dataManagementExport, String fileName, String fileContent, String fileDescription, String user, UUID dataManagmentExportUUID, String typeExport) throws ConnectException;
}
