package com.doing.nemo.claims.service;

import com.doing.nemo.claims.controller.payload.request.AlertRequestV1;
import com.doing.nemo.claims.controller.payload.response.AlertResponseV1;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public interface AlertService {
    AlertResponseV1 createAlert(AlertRequestV1 alertRequestV1) throws IOException;
}
