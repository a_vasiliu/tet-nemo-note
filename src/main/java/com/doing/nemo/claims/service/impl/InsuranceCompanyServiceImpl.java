package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.controller.payload.response.InsuranceCompanyResponseV1;
import com.doing.nemo.claims.controller.payload.response.PaginationStats;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.settings.InsuranceCompanyEntity;
import com.doing.nemo.claims.repository.InsuranceCompanyRepository;
import com.doing.nemo.claims.repository.InsuranceCompanyRepositoryV1;
import com.doing.nemo.claims.service.InsuranceCompanyService;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class InsuranceCompanyServiceImpl implements InsuranceCompanyService {


    private static Logger LOGGER = LoggerFactory.getLogger(InsuranceCompanyServiceImpl.class);
    @Autowired
    private InsuranceCompanyRepository insuranceCompanyRepository;
    @Autowired
    private InsuranceCompanyRepositoryV1 insuranceCompanyRepositoryV1;

    @Override
    public InsuranceCompanyEntity insertCompany(InsuranceCompanyEntity insuranceCompanyEntity) {
        if (insuranceCompanyRepository.searchInsuranceCompanybyName(insuranceCompanyEntity.getName()) != null) {
            LOGGER.debug("Insurance company with name '" + insuranceCompanyEntity.getName() + "' already exists");
            throw new BadRequestException("Insurance company with name '" + insuranceCompanyEntity.getName() + "' already exists", MessageCode.CLAIMS_1009);
        }
        insuranceCompanyRepository.save(insuranceCompanyEntity);
        return insuranceCompanyEntity;
    }

    @Override
    public InsuranceCompanyEntity updateCompany(InsuranceCompanyEntity insuranceCompanyEntity) {
        if (insuranceCompanyRepository.searchInsuranceCompanybyNameWithDifferentId(insuranceCompanyEntity.getName(), insuranceCompanyEntity.getId()) != null) {
            LOGGER.debug("Insurance company with name '" + insuranceCompanyEntity.getName() + "' already exists");
            throw new BadRequestException("Insurance company with name '" + insuranceCompanyEntity.getName() + "' already exists", MessageCode.CLAIMS_1009);
        }

        insuranceCompanyRepository.save(insuranceCompanyEntity);
        return insuranceCompanyEntity;
    }

    @Override
    public InsuranceCompanyResponseV1 deleteCompany(UUID uuid) {
        Optional<InsuranceCompanyEntity> insuranceCompanyEntity2 = insuranceCompanyRepository.findById(uuid);
        if (!insuranceCompanyEntity2.isPresent()) {
            LOGGER.debug("Insurance company with id " + uuid + " not found");
            throw new NotFoundException("Insurance company with id " + uuid + " not found", MessageCode.CLAIMS_1010);
        }
        InsuranceCompanyEntity insuranceCompanyEntity = insuranceCompanyEntity2.get();
        insuranceCompanyRepositoryV1.deleteAllRules(insuranceCompanyEntity.getId());
        insuranceCompanyRepository.delete(insuranceCompanyEntity);
        return null;

    }

    @Override
    public InsuranceCompanyEntity selectCompany(UUID uuid) {

        Optional<InsuranceCompanyEntity> insuranceCompanyEntity = insuranceCompanyRepository.findById(uuid);
        if (!insuranceCompanyEntity.isPresent()) {
            LOGGER.debug("Insurance company with id " + uuid + " not found");
            throw new NotFoundException("Insurance company with id " + uuid + " not found", MessageCode.CLAIMS_1010);
        }
        return insuranceCompanyEntity.get();
    }

    @Override
    public List<InsuranceCompanyEntity> selectAllCompany() {
        List<InsuranceCompanyEntity> insuranceCompanyEntityList = insuranceCompanyRepository.selectAllByOrderAsc();

        return insuranceCompanyEntityList;

    }

    @Override
    public Boolean getCardCompanyByName(String name) {


        /*Query query = insuranceCompanyRepository.createQuery("select i from InsuranceCompanyEntity i where i.name = :name", InsuranceCompanyEntity.class);
        query.setParameter("name", name);

        InsuranceCompanyEntity insuranceCompany = (InsuranceCompanyEntity)query.getSingleResult();*/

        InsuranceCompanyEntity insuranceCompany = insuranceCompanyRepository.findByCardCompanyByName(name);

        if (insuranceCompany == null) {
            LOGGER.debug("Insurance Company with denomination " + name + " not found");
            throw new NotFoundException("Insurance Company with denomination " + name + " not found", MessageCode.CLAIMS_1010);
        }
        return insuranceCompany.getJoinCard();
    }

    @Override
    public InsuranceCompanyEntity updateStatusCompany(UUID uuid) {
        InsuranceCompanyEntity insuranceCompanyEntity = selectCompany(uuid);

        if(insuranceCompanyEntity.getActive()==null){
            insuranceCompanyEntity.setActive(true);
            return insuranceCompanyEntity;
        }

        insuranceCompanyEntity.setActive(!insuranceCompanyEntity.getActive());

        insuranceCompanyRepository.save(insuranceCompanyEntity);
        return insuranceCompanyEntity;
    }

    @Override
    public Pagination<InsuranceCompanyEntity> paginationInsuranceCompany(int page, int pageSize, String orderBy, Boolean asc, String name, String zipCode, String city, String province, String telephone, Boolean isActive, Long code) {
        Pagination<InsuranceCompanyEntity> insuranceCompanyPagination = new Pagination<>();
        List<InsuranceCompanyEntity> insuranceCompanyEntityList = insuranceCompanyRepositoryV1.findPaginationInsuranceCompany(page, pageSize, orderBy, asc, name, zipCode, city, province, telephone, isActive, code);
        Long itemCount = insuranceCompanyRepositoryV1.countPaginationInsuranceCompany(name, zipCode, city, province, telephone, isActive, code);
        insuranceCompanyPagination.setItems(insuranceCompanyEntityList);
        insuranceCompanyPagination.setStats(new PaginationStats(itemCount.intValue(), page, pageSize));

        return insuranceCompanyPagination;
    }
}
