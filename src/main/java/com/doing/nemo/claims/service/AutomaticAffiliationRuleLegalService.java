package com.doing.nemo.claims.service;

import com.doing.nemo.claims.entity.settings.AutomaticAffiliationRuleLegalEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface AutomaticAffiliationRuleLegalService {
    AutomaticAffiliationRuleLegalEntity deleteRuleLegal(List<AutomaticAffiliationRuleLegalEntity> automaticAffiliationRuleLegalEntities);

    AutomaticAffiliationRuleLegalEntity updateRuleLegal(AutomaticAffiliationRuleLegalEntity automaticAffiliationRuleLegalEntity);

    List<AutomaticAffiliationRuleLegalEntity> updateRuleLegalList(List<AutomaticAffiliationRuleLegalEntity> automaticAffiliationRuleLegalEntityList);
}
