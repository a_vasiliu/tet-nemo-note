package com.doing.nemo.claims.service;

import com.doing.nemo.claims.command.ClaimsInsertEnjoyCommand;
import com.doing.nemo.claims.controller.payload.response.ContractInfoPracticeResponseV1;
import com.doing.nemo.claims.controller.payload.response.ContractInfoResponseAldVSAldV1;
import com.doing.nemo.claims.controller.payload.response.ContractInfoResponseV1;
import com.doing.nemo.claims.entity.esb.ContractESB.ContractEsb;
import com.doing.nemo.claims.entity.esb.CustomerESB.CustomerEsb;
import com.doing.nemo.claims.entity.esb.FleetManagerESB.FleetManagerEsb;
import com.doing.nemo.claims.entity.esb.InsuranceCompanyESB.InsuranceCompanyEsb;
import com.doing.nemo.claims.entity.esb.RegistryESB;
import com.doing.nemo.claims.entity.esb.VehicleESB.VehicleEsb;
import org.springframework.scheduling.annotation.Async;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.BlockingQueue;

public interface EnjoyService {

    ContractInfoResponseV1 getDriverRegistryFlettManagerInfo(String idDriver,String idCustomer, String idContract, String date);

    BlockingQueue asynchronousEsbcalls(String idCustomer, String idDriver, String idContract, String date, BlockingQueue responseList);

    ContractInfoResponseV1 getDriverRegistryAndFleetFromESB(String idContract, String idCustomer, String idDriver, String date) ;
    FleetManagerEsb checkFleetManager(String customerId, String fleetManagerId) throws IOException;

    List<RegistryESB> getTelematics(String idContract) throws IOException;

    List<FleetManagerEsb> getFleetManagers(String idCustomer) throws IOException;

    @Async
    void enjoyFlow (ClaimsInsertEnjoyCommand command, String uuid, String driverId, String customerId, String contractId, String date) throws IOException;
    //ContractInfoResponseV1 getContractInfoEnjoy (String plate, String date);

    }

