package com.doing.nemo.claims.service;

import com.doing.nemo.claims.controller.payload.response.InsuranceCompanyResponseV1;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.settings.InsuranceCompanyEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public interface InsuranceCompanyService {
    InsuranceCompanyEntity insertCompany(InsuranceCompanyEntity InsuranceCompanyEntity);

    InsuranceCompanyEntity updateCompany(InsuranceCompanyEntity insuranceCompaniesRequest);

    InsuranceCompanyResponseV1 deleteCompany(UUID uuid);

    InsuranceCompanyEntity selectCompany(UUID uuid);

    List<InsuranceCompanyEntity> selectAllCompany();

    Boolean getCardCompanyByName(String name);

    InsuranceCompanyEntity updateStatusCompany(UUID uuid);

    Pagination<InsuranceCompanyEntity> paginationInsuranceCompany(int page, int pageSize, String orderBy, Boolean asc, String name, String zipCode, String city, String province, String telephone, Boolean isActive, Long code);
}
