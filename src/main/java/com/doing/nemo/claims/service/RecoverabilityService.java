package com.doing.nemo.claims.service;

import com.doing.nemo.claims.controller.payload.response.RecoverabilityResponseV1;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.settings.RecoverabilityEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public interface RecoverabilityService {

    RecoverabilityEntity insertRecoverability(RecoverabilityEntity recoverabilityEntity);

    RecoverabilityEntity updateRecoverability(RecoverabilityEntity recoverabilityEntity);

    RecoverabilityEntity selectRecoverability(UUID id);

    List<RecoverabilityEntity> selectAllRecoverability();

    RecoverabilityResponseV1 deleteRecoverability(UUID id);

    RecoverabilityEntity updateStatus(UUID uuid);

    RecoverabilityEntity getRecoverabilityByFlowAndTypeClaim(String flow, DataAccidentTypeAccidentEnum typeClaim);

    Pagination<RecoverabilityEntity> paginationRecoverability(int page, int pageSize, String management, String claimsType, Boolean counterpart, Boolean recoverability, Double percentRecoverability, Boolean isActive, String orderBy, Boolean asc);
}
