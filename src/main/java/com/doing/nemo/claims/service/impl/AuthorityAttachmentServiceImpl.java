package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.adapter.AttachmentAdapter;
import com.doing.nemo.claims.adapter.CounterpartyAdapter;
import com.doing.nemo.claims.controller.payload.response.authority.AuthorityWorkingAttachmentResponseV1;
import com.doing.nemo.claims.controller.payload.response.authority.AuthorityWorkingsAttachmentResponseV1;
import com.doing.nemo.claims.entity.*;
import com.doing.nemo.claims.entity.enumerated.WorkingStatusEnum;
import com.doing.nemo.claims.entity.jsonb.Authority;
import com.doing.nemo.claims.entity.jsonb.forms.Forms;
import com.doing.nemo.claims.entity.jsonb.forms.jsonbForms.Attachment;
import com.doing.nemo.claims.repository.ClaimsNewRepository;
import com.doing.nemo.claims.repository.CounterpartyNewRepository;
import com.doing.nemo.claims.service.AuthorityAttachmentService;
import com.doing.nemo.claims.service.ConverterClaimsService;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.AbstractException;
import com.doing.nemo.commons.exception.InternalException;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.commons.util.HttpUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.Response;
import org.apache.commons.collections4.MapUtils;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.util.*;

@Service
public class AuthorityAttachmentServiceImpl implements AuthorityAttachmentService {

    private static Logger LOGGER = LoggerFactory.getLogger(AuthorityAttachmentServiceImpl.class);

    @Value("${authority.endpoint}")
    private String authorityEndpoint;

    @Value("${authority.api.working.attachment}")
    private String authorityWorkingAttachment;

    @Autowired
    private HttpUtil httpUtil;

    @Autowired
    private ClaimsNewRepository claimsNewRepository;

    @Autowired
    private CounterpartyNewRepository counterpartyNewRepository;

    @Autowired
    private ConverterClaimsService converterClaimsService;

    private <T, P> T callWithoutCert(String url, HttpMethod method, P value, HashMap<String, String> headersparams, ParameterizedTypeReference<T> typeReference) throws AbstractException, IOException {

        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = null;
        Response responseToken = null;
        try {
            if (value != null) {
                jsonInString = mapper.writeValueAsString(value);
            }

            responseToken = httpUtil.callURL(headersparams, url, jsonInString, method);

            if (!responseToken.isSuccessful() && responseToken.code() == HttpStatus.NOT_FOUND.value()) {
                LOGGER.debug(responseToken.message() + " with code " + responseToken.code());
                throw new NotFoundException(responseToken.message() + " with code " + responseToken.code(), MessageCode.CLAIMS_1101);
            } else if(!responseToken.isSuccessful()){
                LOGGER.debug(responseToken.message() + " with code " + responseToken.code());
                throw new InternalException(responseToken.message() + " with code " + responseToken.code(), MessageCode.CLAIMS_1103);
            }

            if (typeReference == null)
                return null;

            String response = responseToken.body().string();
            LOGGER.debug("Response AuthorityWorkingAttachment Body: {}", response);
            TypeReference tr = new FileManagerServiceImpl.CustomTypeReference(typeReference);
            if(response == null || response.isEmpty()) return null;
            return mapper.readValue(response, tr);
        } catch (AbstractException e) {
            LOGGER.error(e.getMessage(), e);
            throw e;
        }  catch (Exception e) {
            String message = String.format(MessageCode.CLAIMS_2012.value(), e.getClass().getSimpleName(), e.getMessage());
            LOGGER.error(message, e);
            throw new InternalException(message, MessageCode.CLAIMS_2012);
        } finally {
            if (responseToken != null && responseToken.body() != null) {
                responseToken.body().close();
            }
        }
    }

    private String buildUrl(String baseUrl, String uri, Map<String, String> pathParams, MultiValueMap<String, String> queryParams) {

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(baseUrl.concat(uri));

        if (MapUtils.isNotEmpty(queryParams)) {
            builder.queryParams(queryParams);
        }

        return builder.buildAndExpand(pathParams).toString();
    }


    public AuthorityWorkingsAttachmentResponseV1 getWorkingsAttachment(List<String> workingsList) throws IOException {

        String workings = "";
        for (String working : workingsList) {
            workings += "" + working + ",";
        }

        if (!workings.equals("")) {
            workings = workings.substring(0, workings.length() - 1);
        }

        String url = getAttachmentsByWorkings(workings);

        return  callWithoutCert(url, HttpMethod.GET, null, null, new ParameterizedTypeReference<AuthorityWorkingsAttachmentResponseV1>() {
        });
    }

    public ClaimsEntity addAuthorityAttachment(ClaimsEntity claimsEntity){
        List<Authority> authorityList = claimsEntity.getAuthorities();
        Forms forms = claimsEntity.getForms();
        List<Attachment> attachments = null;
        if(forms != null)
            attachments = claimsEntity.getForms().getAttachment();
        else
            claimsEntity.setForms(new Forms());
        if(authorityList != null && !authorityList.isEmpty()){
            if(attachments == null)
                attachments = new ArrayList<>();
            List<String> workingsList = new ArrayList<>();
            for(Authority authority :authorityList){
                if(authority.getAuthorityWorkingId() != null) {
                    workingsList.add(authority.getAuthorityWorkingId());
                }
            }
            try {
                if (workingsList != null && !workingsList.isEmpty()) {
                    AuthorityWorkingsAttachmentResponseV1 authorityWorkingAttachmentResponseV1 = getWorkingsAttachment(workingsList);
                    for (AuthorityWorkingAttachmentResponseV1 authorityWorkingAttachmentResponseV11 : authorityWorkingAttachmentResponseV1.getWorkingsAttachments()) {
                        attachments.addAll(AttachmentAdapter.adptAuthorityAttachmentToAttachment(authorityWorkingAttachmentResponseV11.getAttachments()));
                    }

                    claimsEntity.getForms().setAttachment(attachments);
                }
            } catch (IOException e) {
                LOGGER.debug("Errore chiamata authority");
            }
        }

        return claimsEntity;
    }

    public CounterpartyEntity addAuthorityAttachmentRepair(CounterpartyEntity counterpartyEntity){
        List<Authority> authorityList = counterpartyEntity.getAuthorities();
        List<Attachment> attachments = counterpartyEntity.getAttachments();
        if(authorityList != null && !authorityList.isEmpty()){
            if(attachments == null)
                attachments = new ArrayList<>();
            List<String> workingsList = new ArrayList<>();
            for(Authority authority :authorityList){
                if((authority.getWorkingStatus() != null && (authority.getWorkingStatus().equals(WorkingStatusEnum.CLOSED) || authority.getWorkingStatus().equals(WorkingStatusEnum.CLOSED_WITH_LIQUIDATION) || authority.getWorkingStatus().equals(WorkingStatusEnum.CLOSED_WITH_REJECTION))) && (authority.getMsaDownloaded() == null || !authority.getMsaDownloaded()))
                {
                    workingsList.add(authority.getAuthorityWorkingId());
                }
            }
            if(!workingsList.isEmpty()) {
                try {
                    AuthorityWorkingsAttachmentResponseV1 authorityWorkingAttachmentResponseV1 = getWorkingsAttachment(workingsList);
                    for (AuthorityWorkingAttachmentResponseV1 authorityWorkingAttachmentResponseV11 : authorityWorkingAttachmentResponseV1.getWorkingsAttachments()) {
                        attachments.addAll(AttachmentAdapter.adptAuthorityAttachmentToAttachment(authorityWorkingAttachmentResponseV11.getAttachments()));
                    }
                    counterpartyEntity.setAttachments(attachments);
                } catch (IOException e) {
                    LOGGER.debug("Errore chiamata authority");
                }
            }
        }

        return counterpartyEntity;
    }

    public Pagination<ClaimsEntity> addAuthorityAttachment(Pagination<ClaimsEntity> claimsEntityPagination){
        List<ClaimsEntity> claimsEntityList = claimsEntityPagination.getItems();

        if(claimsEntityList != null && !claimsEntityList.isEmpty()){
            List<ClaimsEntity> claimsEntityListNew = new ArrayList<>();
            for(ClaimsEntity claimsEntity : claimsEntityList){
                claimsEntityListNew.add(addAuthorityAttachment(claimsEntity));
            }
            claimsEntityPagination.setItems(claimsEntityListNew);
        }

        return claimsEntityPagination;
    }

    public List<CounterpartyEntity> addAuthorityAttachmentRepair(List<CounterpartyEntity> counterpartyList){
        List<CounterpartyEntity> counterpartyResponseV1ListNew = null;

        if(counterpartyList != null && !counterpartyList.isEmpty()){
            counterpartyResponseV1ListNew = new ArrayList<>();
            for(CounterpartyEntity responseV1 : counterpartyList){
                counterpartyResponseV1ListNew.add(addAuthorityAttachmentRepair(responseV1));
            }

        }

        return counterpartyResponseV1ListNew;
    }

    //REFACTOR
    public AuthorityWorkingsAttachmentResponseV1 getWorkingsAttachmentByClaimId(String claimsId) throws IOException {
        //recupero della nuova entità
        Optional<ClaimsNewEntity> claimsNewEntityOptional = claimsNewRepository.findById(claimsId);
        if(claimsNewEntityOptional.isPresent()) {
            //conversione della nuova entità
            ClaimsEntity claimsEntity = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntityOptional.get());
            List<Authority> authorities = claimsEntity.getAuthorities();
            List<String> workingId;
            if (authorities != null && !authorities.isEmpty()) {
                workingId = new ArrayList<>();
                for (Authority authority : authorities) {
                    workingId.add(authority.getAuthorityWorkingId());
                }

                return getWorkingsAttachment(workingId);
            }
        }

        return null;
    }

    //REFACTOR
    public AuthorityWorkingsAttachmentResponseV1 getWorkingsAttachmentByRepairId(String repairId) throws IOException {
        Optional<CounterpartyNewEntity> counterpartyNewEntityOptional = counterpartyNewRepository.findById(repairId);
        if(counterpartyNewEntityOptional.isPresent()) {
            CounterpartyEntity counterpartyEntity = CounterpartyAdapter.adptCounterpartyNewToCounterpartyOld(counterpartyNewEntityOptional.get());
            List<Authority> authorities = counterpartyEntity.getAuthorities();
            List<String> workingId;
            if (authorities != null && !authorities.isEmpty()) {
                workingId = new ArrayList<>();
                for (Authority authority : authorities) {
                    workingId.add(authority.getAuthorityWorkingId());
                }

                return getWorkingsAttachment(workingId);
            }
        }
        return null;
    }


    private String getAttachmentsByWorkings(String workings) {

        return this.buildUrl(
                authorityEndpoint,
                authorityWorkingAttachment,
                new HashMap<>(),
                new LinkedMultiValueMap<String, String>() {{
                    add("working_id", workings);
                }}
        );
    }
}
