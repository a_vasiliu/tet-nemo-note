package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.controller.payload.response.PaginationStats;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.enumerated.ClaimsRepairEnum;
import com.doing.nemo.claims.entity.enumerated.EmailTemplateEnum;
import com.doing.nemo.claims.entity.enumerated.EventTypeEnum;
import com.doing.nemo.claims.entity.settings.EmailTemplateEntity;
import com.doing.nemo.claims.entity.settings.EventTypeEntity;
import com.doing.nemo.claims.entity.settings.RecipientTypeEntity;
import com.doing.nemo.claims.repository.EmailTemplateRepository;
import com.doing.nemo.claims.repository.EmailTemplateRepositoryV1;
import com.doing.nemo.claims.repository.EventTypeRepository;
import com.doing.nemo.claims.repository.RecipientTypeRepository;
import com.doing.nemo.claims.service.EmailTemplateService;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class EmailTemplateServiceImpl implements EmailTemplateService {

    private static Logger LOGGER = LoggerFactory.getLogger(EmailTemplateServiceImpl.class);
    @Autowired
    private EmailTemplateRepository emailTemplateRepository;
    @Autowired
    private EmailTemplateRepositoryV1 emailTemplateRepositoryV1;
    @Autowired
    private EventTypeRepository eventTypeRepository;
    @Autowired
    private RecipientTypeRepository recipientTypeRepository;

    @Override
    public EmailTemplateEntity insertEmailTemplate(EmailTemplateEntity emailTemplateEntity) {

        if (emailTemplateEntity.getApplicationEvent() != null) {
            Optional<EventTypeEntity> eventTypeEntity = eventTypeRepository.findById(emailTemplateEntity.getApplicationEvent().getId());
            if (!eventTypeEntity.isPresent()) {
                LOGGER.debug("Application event with event type " + emailTemplateEntity.getApplicationEvent().getEventType().getValue() + " not found");
                throw new BadRequestException("Application event with event type " + emailTemplateEntity.getApplicationEvent().getEventType().getValue() + " not found", MessageCode.CLAIMS_1010);
            }
            emailTemplateEntity.setApplicationEvent(eventTypeEntity.get());
        }

        if (emailTemplateEntity.getCustomers() != null) {
            List<RecipientTypeEntity> recipientTypeEntities = new ArrayList<>();
            for (RecipientTypeEntity recipientTypeEntity : emailTemplateEntity.getCustomers()) {
                Optional<RecipientTypeEntity> recipientTypeEntity1 = recipientTypeRepository.findById(recipientTypeEntity.getId());
                if (!recipientTypeEntity1.isPresent()) {
                    LOGGER.debug("Customer with recipient id " + recipientTypeEntity.getId() + " not found");
                    throw new BadRequestException("Customer with recipient id " + recipientTypeEntity.getId() + " not found", MessageCode.CLAIMS_1010);
                }
                recipientTypeEntities.add(recipientTypeEntity1.get());
            }
            emailTemplateEntity.setCustomers(recipientTypeEntities);
        }

        emailTemplateRepository.save(emailTemplateEntity);
        return emailTemplateEntity;
    }

    @Override
    @Transactional
    public EmailTemplateEntity updateEmailTemplate(EmailTemplateEntity emailTemplateEntity, UUID uuid) {
        Optional<EmailTemplateEntity> emailTemplateEntityOld = emailTemplateRepository.findById(uuid);
        if (!emailTemplateEntityOld.isPresent()) {
            LOGGER.debug("EmailTemplate with id " + uuid + " not found");
            throw new NotFoundException("EmailTemplate with id " + uuid + " not found", MessageCode.CLAIMS_1010);
        }

        emailTemplateEntity.setId(emailTemplateEntityOld.get().getId());
        emailTemplateEntity.setTypeComplaint(emailTemplateEntityOld.get().getTypeComplaint());
        if (emailTemplateEntity.getApplicationEvent() != null) {
            if (emailTemplateEntity.getApplicationEvent().getId() == null) {
                eventTypeRepository.save(emailTemplateEntity.getApplicationEvent());
            } else {
                Optional<EventTypeEntity> eventTypeEntityOptional = eventTypeRepository.findById(emailTemplateEntity.getApplicationEvent().getId());
                if (!eventTypeEntityOptional.isPresent()) {
                    LOGGER.debug("EventType with id " + emailTemplateEntity.getApplicationEvent().getId() + " not found");
                    throw new NotFoundException("EventType with id " + emailTemplateEntity.getApplicationEvent().getId() + " not found", MessageCode.CLAIMS_1010);
                }
                eventTypeRepository.save(eventTypeEntityOptional.get());
            }
        }

        if (emailTemplateEntity.getCustomers() != null) {
            for (RecipientTypeEntity recipientTypeEntity : emailTemplateEntity.getCustomers()) {
                if (recipientTypeEntity.getId() == null)
                    recipientTypeRepository.save(recipientTypeEntity);
                else {
                    Optional<RecipientTypeEntity> recipientTypeEntityOptional = recipientTypeRepository.findById(recipientTypeEntity.getId());
                    if (!recipientTypeEntityOptional.isPresent()) {
                        LOGGER.debug("RecipientType with id " + recipientTypeEntity.getId() + " not found");
                        throw new NotFoundException("RecipientType with id " + recipientTypeEntity.getId() + " not found", MessageCode.CLAIMS_1010);
                    }
                    recipientTypeRepository.save(recipientTypeEntityOptional.get());
                }
            }
        }

        emailTemplateRepository.save(emailTemplateEntity);
        return emailTemplateEntity;
    }

    @Override
    public EmailTemplateEntity getEmailTemplate(UUID uuid) {
        Optional<EmailTemplateEntity> emailTemplateEntity = emailTemplateRepository.findById(uuid);
        if (!emailTemplateEntity.isPresent()) {
            LOGGER.debug("EmailTemplate with id " + uuid + " not found");
            throw new NotFoundException("EmailTemplate with id " + uuid + " not found", MessageCode.CLAIMS_1010);
        }

        return emailTemplateEntity.get();
    }

    @Override
    public List<EmailTemplateEntity> getAllEmailTemplate(ClaimsRepairEnum claimsRepairEnum) {
        List<EmailTemplateEntity> emailTemplateEntities = emailTemplateRepository.searchEmailTemplateByTypeComplaint(claimsRepairEnum);

        return emailTemplateEntities;
    }

    @Override
    public EmailTemplateEntity deleteEmailTemplate(UUID uuid) {
        Optional<EmailTemplateEntity> emailTemplateEntity = emailTemplateRepository.findById(uuid);
        if (!emailTemplateEntity.isPresent()) {
            LOGGER.debug("EmailTemplate with id " + uuid + " not found");
            throw new NotFoundException("EmailTemplate with id " + uuid + " not found", MessageCode.CLAIMS_1010);
        }

        emailTemplateRepository.delete(emailTemplateEntity.get());

        return null;
    }

    @Override
    public EmailTemplateEntity patchStatusEmailTemplate(UUID uuid) {
        EmailTemplateEntity emailTemplateEntity = getEmailTemplate(uuid);
        if (emailTemplateEntity.getActive()) {
            emailTemplateEntity.setActive(false);
        } else {
            emailTemplateEntity.setActive(true);
        }

        emailTemplateRepository.save(emailTemplateEntity);
        return emailTemplateEntity;
    }

    @Override
    public Pagination<EmailTemplateEntity> paginationEmailTemplate(int page, int pageSize, String flows, String claimsRepairEnum, String orderBy, Boolean asc, String applicationEventType, String type, String description, String object, String heading, String body, String foot, Boolean attachFile, Boolean splittingRecipientsEmail, String clientCode, Boolean isActive) {
        Pagination<EmailTemplateEntity> emailTemplatePagination = new Pagination<>();

        ClaimsRepairEnum claimsRepair = null;
        if (claimsRepairEnum != null)
            claimsRepair = ClaimsRepairEnum.create(claimsRepairEnum);

        EventTypeEnum eventType = null;
        if (applicationEventType != null)
            eventType = EventTypeEnum.create(applicationEventType);

        EmailTemplateEnum emailTemplate = null;
        if (type != null)
            emailTemplate = EmailTemplateEnum.create(type);

        List<EmailTemplateEntity> emailTemplateList = emailTemplateRepositoryV1.findPaginationEmailTemplate(page, pageSize, flows, claimsRepair, orderBy, asc, eventType, emailTemplate, description, object, heading, body, foot, attachFile, splittingRecipientsEmail, clientCode, isActive);
        BigInteger itemCount = emailTemplateRepositoryV1.countPaginationEmailTemplate(claimsRepair, flows, eventType, emailTemplate, description, object, heading, body, foot, attachFile, splittingRecipientsEmail, clientCode, isActive);
        emailTemplatePagination.setItems(emailTemplateList);
        emailTemplatePagination.setStats(new PaginationStats(itemCount.intValue(), page, pageSize));

        return emailTemplatePagination;
    }

}
