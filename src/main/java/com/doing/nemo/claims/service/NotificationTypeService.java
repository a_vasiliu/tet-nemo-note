package com.doing.nemo.claims.service;

import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.settings.NotificationTypeEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public interface NotificationTypeService {

    NotificationTypeEntity insertNotificationType(NotificationTypeEntity notificationTypeEntity);

    NotificationTypeEntity updateNotificationType(NotificationTypeEntity notificationTypeEntity, UUID uuid);

    NotificationTypeEntity getNotificationType(UUID uuid);

    List<NotificationTypeEntity> getAllNotificationType();

    NotificationTypeEntity deleteNotificationType(UUID uuid);

    NotificationTypeEntity updateStatus(UUID uuid);

    Pagination<NotificationTypeEntity> paginationNotificationType(int page, int pageSize, String description, Boolean isActive, Integer orderId, String orderBy, Boolean asc);


}
