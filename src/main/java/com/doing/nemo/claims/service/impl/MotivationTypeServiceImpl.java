package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.entity.settings.MotivationTypeEntity;
import com.doing.nemo.claims.repository.MotivationTypeRepository;
import com.doing.nemo.claims.repository.MotivationTypeRepositoryV1;
import com.doing.nemo.claims.service.MotivationTypeService;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class MotivationTypeServiceImpl implements MotivationTypeService {

    @Autowired
    private MotivationTypeRepository motivationTypeRepository;

    @Autowired
    private MotivationTypeRepositoryV1 motivationTypeRepositoryV1;

    private static Logger LOGGER = LoggerFactory.getLogger(MotivationTypeServiceImpl.class);

    public MotivationTypeEntity insertMotivationType(MotivationTypeEntity motivationTypeEntity) {
        if (motivationTypeRepository.searchMotivationTypebyDescription(motivationTypeEntity.getDescription()) != null)
        {
            LOGGER.debug("Motivation type with description '" + motivationTypeEntity.getDescription() + "' already exists");
            throw new BadRequestException("Motivation type with description '" + motivationTypeEntity.getDescription() + "' already exists", MessageCode.CLAIMS_1009);
        }
        motivationTypeRepository.save(motivationTypeEntity);
        return motivationTypeEntity;
    }

    public MotivationTypeEntity updateMotivationType(MotivationTypeEntity motivationTypeEntity) {
        if (motivationTypeRepository.searchMotivationTypebyDescriptionWithDifferentId(motivationTypeEntity.getDescription(), motivationTypeEntity.getId()) != null)
        {
            LOGGER.debug("Motivation type with description '" + motivationTypeEntity.getDescription() + "' already exists");
            throw new BadRequestException("Motivation type with description '" + motivationTypeEntity.getDescription() + "' already exists", MessageCode.CLAIMS_1009);
        }
        motivationTypeRepository.save(motivationTypeEntity);
        return motivationTypeEntity;
    }

    public MotivationTypeEntity selectMotivationType(UUID uuid) {
        Optional<MotivationTypeEntity> motivationTypeEntityOptional = motivationTypeRepository.findById(uuid);
        if (!motivationTypeEntityOptional.isPresent())
        {
            LOGGER.debug("Motivation type with id " + uuid + "not found");
            throw new NotFoundException("Motivation type with id " + uuid + "not found", MessageCode.CLAIMS_1010);
        }
        return motivationTypeEntityOptional.get();
    }

    public List<MotivationTypeEntity> selectAllMotivationType() {
        List<MotivationTypeEntity> motivationTypeEntities = motivationTypeRepository.findAllOrderedByOrderId();

        return motivationTypeEntities;
    }

    public MotivationTypeEntity deleteMotivationType(UUID uuid) {
        Optional<MotivationTypeEntity> motivationTypeEntityOptional = motivationTypeRepository.findById(uuid);
        if (!motivationTypeEntityOptional.isPresent())
        {
            LOGGER.debug("Motivation type with id " + uuid + "not found");
            throw new NotFoundException("Motivation type with id " + uuid + "not found", MessageCode.CLAIMS_1010);
        }
        motivationTypeRepository.delete(motivationTypeEntityOptional.get());

        return null;
    }


    @Override
    public MotivationTypeEntity updateStatusMotivationType(UUID uuid) {
        MotivationTypeEntity motivationTypeEntity = selectMotivationType(uuid);
        if (motivationTypeEntity.getActive()) {
            motivationTypeEntity.setActive(false);
        } else {
            motivationTypeEntity.setActive(true);
        }

        motivationTypeRepository.save(motivationTypeEntity);
        return motivationTypeEntity;
    }

    @Override
    public List<MotivationTypeEntity> findMotivationTypesFiltered (Integer page, Integer pageSize, String orderBy, Boolean asc, Long motivationTypeId, String description, Integer orderId, Boolean isActive){
        return motivationTypeRepositoryV1.findMotivationTypesFiltered(page,pageSize, orderBy, asc, motivationTypeId, description, orderId, isActive);
    }

    @Override
    public Long countMotivationTypeFiltered(Long motivationTypeId, String description, Integer orderId, Boolean isActive){
        return motivationTypeRepositoryV1.countMotivationTypeFiltered(motivationTypeId, description, orderId, isActive);
    }
}
