package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.adapter.*;
import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.DogeRequestV1;
import com.doing.nemo.claims.controller.payload.response.DogeResponseV1;
import com.doing.nemo.claims.dto.*;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.ClaimsNewEntity;
import com.doing.nemo.claims.entity.CounterpartyEntity;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.EntrustedEnum.EntrustedEnum;
import com.doing.nemo.claims.entity.enumerated.DamagedEnum.ImpactPointEnum.ImpactPointDetectionImpactPointEnum;
import com.doing.nemo.claims.entity.enumerated.DamagedEnum.VehicleEnum.VehicleNatureEnum;
import com.doing.nemo.claims.entity.enumerated.DamagedEnum.VehicleEnum.VehicleTypeEnum;
import com.doing.nemo.claims.entity.enumerated.DamagedEnum.WoundedEnum.WoundedTypeEnum;
import com.doing.nemo.claims.entity.jsonb.*;
import com.doing.nemo.claims.entity.jsonb.cai.CaiDetails;
import com.doing.nemo.claims.entity.jsonb.complaint.Complaint;
import com.doing.nemo.claims.entity.jsonb.complaint.jsonbComplaint.Entrusted;
import com.doing.nemo.claims.entity.jsonb.complaint.jsonbComplaint.jsonbDataAccidents.DataAccident;
import com.doing.nemo.claims.entity.jsonb.counterparty.InsuranceCompanyCounterparty;
import com.doing.nemo.claims.entity.jsonb.damaged.Damaged;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.*;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbDriver.Driver;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbInsuranceCompany.Pai;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbVehicle.Vehicle;
import com.doing.nemo.claims.entity.settings.*;
import com.doing.nemo.claims.repository.*;
import com.doing.nemo.claims.service.*;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.AbstractException;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.exception.InternalException;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.commons.util.HttpUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.Response;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.*;

@Service
public class DogeEnquServiceImpl implements DogeEnquService {

    private static Logger LOGGER = LoggerFactory.getLogger(DogeEnquServiceImpl.class);

    @Value("${doge.api.endpoint}")
    private String dogeAPIEndpoint;

    @Value("${doge.api.enqueue}")
    private String dogeAPIEnqueue;

    @Value("${export.claims.blocksize}")
    private Integer exportClaimsBlocksize;

    @Value("${export.repair.blocksize}")
    private Integer exportRepairBlocksize;

    @Autowired
    private HttpUtil httpUtil;
    @Autowired
    private InsuranceCompanyRepository insuranceCompanyRepository;

    @Autowired
    private InsurancePolicyRepository insurancePolicyRepository;

    @Autowired
    private PracticeRepositoryV1 practiceRepositoryV1;

    @Autowired
    private ConverterClaimsService converterClaimsService;

    @Autowired
    private ClaimsExportHelper claimsExportHelper;

    @Autowired
    private CtpExportHelper ctpExportHelper;

    @Autowired
    private ClaimsNewRepository claimsNewRepository;

    @Autowired
    private ClaimsAsynchExporterImpl claimsAsynchExporter;

    @Autowired
    private LogsAsynchExporterImpl logsAsynchExporter;

    @Autowired
    private CtpAsynchExporterImpl ctpAsynchExporter;

    private <T, P> T callWithoutCert(String url, HttpMethod method, P value, HashMap<String, String> headersparams, Class<T> outclass) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = null;
        Response responseToken = null;
        try {
            if (value != null) {
                jsonInString = mapper.writeValueAsString(value);
            }

            responseToken = httpUtil.callURL(headersparams, url, jsonInString, method);

            LOGGER.debug(responseToken.toString());

            if (responseToken.code() != HttpStatus.OK.value() && responseToken.code() != HttpStatus.CREATED.value()) {
                LOGGER.debug(responseToken.message() + " with code " + responseToken.code());
                throw new BadRequestException(responseToken.message() + " with code " + responseToken.code(), MessageCode.CLAIMS_2000);
            }
            String response = responseToken.body().string();
            LOGGER.debug("Response DogeEnqu Body: {}", response);
            return mapper.readValue(response, outclass);
        } catch (AbstractException e) {
            LOGGER.error(e.getMessage(), e);
            throw e;
        }  catch (Exception e) {
            String message = String.format(MessageCode.CLAIMS_2012.value(), e.getClass().getSimpleName(), e.getMessage());
            LOGGER.error(message, e);
            throw new InternalException(message, MessageCode.CLAIMS_2012);
        } finally {
            if (responseToken != null && responseToken.body() != null) {
                responseToken.body().close();
            }
        }

    }

    @Override
    public DogeResponseV1 enqueueClaimsWithoutCounterpart(String id) throws IOException {
        DogeRequestV1 requestOut = new DogeRequestV1();
        //recupero della nuova entità
        Optional<ClaimsNewEntity> claimsNewEntityOptional = claimsNewRepository.findById(id);
        if (!claimsNewEntityOptional.isPresent()) {
            throw new BadRequestException("Claims with id " + id + "not found.", MessageCode.CLAIMS_1010);
        }
        //conversione nella vecchia entità
        ClaimsEntity claimsEntity = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntityOptional.get());
        //Map<String, Object> requestMap = new HashMap<>();

        List<Map<String, String>> request = new ArrayList<>();

        Map<String, Object> requestInnerMap = new HashMap<>();
        if (claimsEntity.getCreatedAt() != null) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
            String dateCreated = simpleDateFormat.format(DateUtil.convertIS08601StringToUTCDate(DateUtil.convertUTCInstantToIS08601String(claimsEntity.getCreatedAt())));
            requestInnerMap.put("date", dateCreated);
        }

        if (claimsEntity.getPracticeId() != null)
            requestInnerMap.put("claim_number", claimsEntity.getPracticeId().toString().toUpperCase());
        if (claimsEntity.getComplaint() != null) {
            if (claimsEntity.getComplaint().getPlate() != null)
                requestInnerMap.put("plate", claimsEntity.getComplaint().getPlate().toUpperCase());
            if (claimsEntity.getComplaint().getDataAccident() != null) {
                if (claimsEntity.getComplaint().getDataAccident().getDateAccident() != null) {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                    simpleDateFormat.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));
                    Date claimDate = claimsEntity.getComplaint().getDataAccident().getDateAccident();
                    String dateAccident = simpleDateFormat.format(claimDate);
                    requestInnerMap.put("claim_date", dateAccident);
                }
                if (claimsEntity.getComplaint().getDataAccident().getAddress() != null) {
                    if (claimsEntity.getComplaint().getDataAccident().getAddress().getLocality() != null && claimsEntity.getComplaint().getDataAccident().getAddress().getProvince() != null)
                        requestInnerMap.put("city", claimsEntity.getComplaint().getDataAccident().getAddress().getLocality().toUpperCase() + " (" + claimsEntity.getComplaint().getDataAccident().getAddress().getProvince().toUpperCase() + ")");


                }
                if (claimsEntity.getComplaint().getDataAccident().getTypeAccident() != null) {
                    String typeAccident = claimsEntity.getComplaint().getDataAccident().getTypeAccident().getValue();
                    typeAccident = typeAccident.replaceAll("_", " ");
                    requestInnerMap.put("claim_type", typeAccident.toUpperCase());
                }
            }
        }
        if (claimsEntity.getDamaged() != null) {
            if (claimsEntity.getDamaged().getImpactPoint() != null) {
                if (claimsEntity.getDamaged().getImpactPoint().getIncidentDescription() != null)
                    requestInnerMap.put("claim_description", claimsEntity.getDamaged().getImpactPoint().getIncidentDescription().toUpperCase());
                if (claimsEntity.getDamaged().getImpactPoint().getDamageToVehicle() != null)
                    requestInnerMap.put("vehicle_damages", claimsEntity.getDamaged().getImpactPoint().getDamageToVehicle().toUpperCase());
            }
            if (claimsEntity.getDamaged().getDriver() != null) {
                if (claimsEntity.getDamaged().getDriver().getFirstname() != null)
                    requestInnerMap.put("driver_name", claimsEntity.getDamaged().getDriver().getFirstname().toUpperCase());
                if (claimsEntity.getDamaged().getDriver().getLastname() != null)
                    requestInnerMap.put("driver_surname", claimsEntity.getDamaged().getDriver().getLastname().toUpperCase());
                if (claimsEntity.getDamaged().getDriver().getEmail() != null)
                    requestInnerMap.put("driver_email", claimsEntity.getDamaged().getDriver().getEmail());
            }
        }


        requestOut.setData(requestInnerMap);
        String idPractice = null;
        if (claimsEntity.getPracticeId() != null)
            idPractice = claimsEntity.getPracticeId().toString();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String dateCreated = simpleDateFormat.format(new Date());
        requestOut.setFilename("Riepilogo_" + claimsEntity.getPracticeId() + ".pdf");
        requestOut.setTemplate("ALD-Denuncia-senza-controparte");
        DogeRequestV1.FileManager fileManager = new DogeRequestV1.FileManager();
        String uuid = UUID.randomUUID().toString();
        fileManager.setUuid(uuid);
        fileManager.setResourceId(claimsEntity.getId());
        fileManager.setResourceType("claims");
        fileManager.setBlobType("pdf");
        requestOut.setFileManager(fileManager);

        DogeResponseV1.ActionResult response = callWithoutCert(dogeAPIEndpoint + dogeAPIEnqueue, HttpMethod.POST, requestOut, new HashMap<>(), DogeResponseV1.ActionResult.class);
        if (response.getCode() == 1) {
            response = new DogeResponseV1.ActionResult(response.getCode(), response.getMessage(), response.getDetails());
        }
        DogeResponseV1 dogeResponse = new DogeResponseV1();
        dogeResponse.setDocumentId(uuid);
        dogeResponse.setActionResult(response);

        return dogeResponse;
    }

    // TODO REFACTOR
    @Override
    public DogeResponseV1 enqueueClaimsWithCounterpart(String idClaims) throws IOException {
        DogeRequestV1 requestOut = new DogeRequestV1();
        //Recupero della nuova entità
        Optional<ClaimsNewEntity> claimsNewEntityOptional = claimsNewRepository.findById(idClaims);
        if (!claimsNewEntityOptional.isPresent()) {
            throw new BadRequestException("Claims with id " + idClaims + "not found", MessageCode.CLAIMS_1010);
        }
        //conversione nella vecchià entità
        ClaimsEntity claimsEntity = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntityOptional.get());

        Map<String, Object> requestMap = new HashMap<>();
        if (claimsEntity.getDeponentList() != null) {
            if (claimsEntity.getDeponentList().isEmpty()) requestMap.put("witness", "NO");
            else requestMap.put("witness", "SI");
        } else {
            requestMap.put("witness", "NO");
        }

        if (claimsEntity.getWoundedList() != null) {
            if (claimsEntity.getWoundedList().isEmpty())
                requestMap.put("transported_accident", "NO");
            else {
                boolean check = false;
                int i = 0;
                while (!check && i < claimsEntity.getWoundedList().size()) {
                    Wounded wounded = claimsEntity.getWoundedList().get(i);
                    if (wounded.getType() != null) {
                        if (wounded.getType().getValue().equalsIgnoreCase("PASSENGER")) check = true;
                    }
                    i++;
                }

                if (check) requestMap.put("transported_accident", "SI");
                else requestMap.put("transported_accident", "NO");
            }
        } else {
            requestMap.put("transported_accident", "NO");
        }
        if (claimsEntity.getCaiDetails() != null) {
            CaiDetails vehicleA = claimsEntity.getCaiDetails().getVehicleA();
            CaiDetails vehicleB = claimsEntity.getCaiDetails().getVehicleB();

            if (vehicleA != null) {
                Integer count = 0;
                Map<String, Boolean> requestInnerMapA = new HashMap<>();
                List<Map<String, Boolean>> requestA = new ArrayList<>();
                if (vehicleA.getCondition1().getValue()) count++;
                requestInnerMapA.put("check", vehicleA.getCondition1().getValue());
                requestA.add(requestInnerMapA);

                if (vehicleA.getCondition2().getValue()) count++;
                requestInnerMapA = new HashMap<>();
                requestInnerMapA.put("check", vehicleA.getCondition2().getValue());
                requestA.add(requestInnerMapA);

                if (vehicleA.getCondition3().getValue()) count++;
                requestInnerMapA = new HashMap<>();
                requestInnerMapA.put("check", vehicleA.getCondition3().getValue());
                requestA.add(requestInnerMapA);

                if (vehicleA.getCondition4().getValue()) count++;
                requestInnerMapA = new HashMap<>();
                requestInnerMapA.put("check", vehicleA.getCondition4().getValue());
                requestA.add(requestInnerMapA);

                if (vehicleA.getCondition5().getValue()) count++;
                requestInnerMapA = new HashMap<>();
                requestInnerMapA.put("check", vehicleA.getCondition5().getValue());
                requestA.add(requestInnerMapA);

                if (vehicleA.getCondition6().getValue()) count++;
                requestInnerMapA = new HashMap<>();
                requestInnerMapA.put("check", vehicleA.getCondition6().getValue());
                requestA.add(requestInnerMapA);

                if (vehicleA.getCondition7().getValue()) count++;
                requestInnerMapA = new HashMap<>();
                requestInnerMapA.put("check", vehicleA.getCondition7().getValue());
                requestA.add(requestInnerMapA);

                if (vehicleA.getCondition8().getValue()) count++;
                requestInnerMapA = new HashMap<>();
                requestInnerMapA.put("check", vehicleA.getCondition8().getValue());
                requestA.add(requestInnerMapA);

                if (vehicleA.getCondition9().getValue()) count++;
                requestInnerMapA = new HashMap<>();
                requestInnerMapA.put("check", vehicleA.getCondition9().getValue());
                requestA.add(requestInnerMapA);

                if (vehicleA.getCondition10().getValue()) count++;
                requestInnerMapA = new HashMap<>();
                requestInnerMapA.put("check", vehicleA.getCondition10().getValue());
                requestA.add(requestInnerMapA);

                if (vehicleA.getCondition11().getValue()) count++;
                requestInnerMapA = new HashMap<>();
                requestInnerMapA.put("check", vehicleA.getCondition11().getValue());
                requestA.add(requestInnerMapA);

                if (vehicleA.getCondition12().getValue()) count++;
                requestInnerMapA = new HashMap<>();
                requestInnerMapA.put("check", vehicleA.getCondition12().getValue());
                requestA.add(requestInnerMapA);

                if (vehicleA.getCondition13().getValue()) count++;
                requestInnerMapA = new HashMap<>();
                requestInnerMapA.put("check", vehicleA.getCondition13().getValue());
                requestA.add(requestInnerMapA);

                if (vehicleA.getCondition14().getValue()) count++;
                requestInnerMapA = new HashMap<>();
                requestInnerMapA.put("check", vehicleA.getCondition14().getValue());
                requestA.add(requestInnerMapA);

                if (vehicleA.getCondition15().getValue()) count++;
                requestInnerMapA = new HashMap<>();
                requestInnerMapA.put("check", vehicleA.getCondition15().getValue());
                requestA.add(requestInnerMapA);

                if (vehicleA.getCondition16().getValue()) count++;
                requestInnerMapA = new HashMap<>();
                requestInnerMapA.put("check", vehicleA.getCondition16().getValue());
                requestA.add(requestInnerMapA);

                if (vehicleA.getCondition17().getValue()) count++;
                requestInnerMapA = new HashMap<>();
                requestInnerMapA.put("check", vehicleA.getCondition17().getValue());
                requestA.add(requestInnerMapA);

                requestMap.put("a_insured_check", requestA);
                requestMap.put("a_insured_total_check", count.toString());
            }
            if (vehicleB != null) {
                List<Map<String, Boolean>> requestB = new ArrayList<>();
                Map<String, Boolean> requestInnerMapB = new HashMap<>();
                Integer count = 0;

                if (vehicleB.getCondition1().getValue()) count++;
                requestInnerMapB.put("check", vehicleB.getCondition1().getValue());

                requestB.add(requestInnerMapB);

                if (vehicleB.getCondition2().getValue()) count++;
                requestInnerMapB = new HashMap<>();
                requestInnerMapB.put("check", vehicleB.getCondition2().getValue());
                requestB.add(requestInnerMapB);

                if (vehicleB.getCondition3().getValue()) count++;
                requestInnerMapB = new HashMap<>();
                requestInnerMapB.put("check", vehicleB.getCondition3().getValue());
                requestB.add(requestInnerMapB);

                if (vehicleB.getCondition4().getValue()) count++;
                requestInnerMapB = new HashMap<>();
                requestInnerMapB.put("check", vehicleB.getCondition4().getValue());
                requestB.add(requestInnerMapB);

                if (vehicleB.getCondition5().getValue()) count++;
                requestInnerMapB = new HashMap<>();
                requestInnerMapB.put("check", vehicleB.getCondition5().getValue());
                requestB.add(requestInnerMapB);

                if (vehicleB.getCondition6().getValue()) count++;
                requestInnerMapB = new HashMap<>();
                requestInnerMapB.put("check", vehicleB.getCondition6().getValue());
                requestB.add(requestInnerMapB);


                if (vehicleB.getCondition7().getValue()) count++;
                requestInnerMapB = new HashMap<>();
                requestInnerMapB.put("check", vehicleB.getCondition7().getValue());
                requestB.add(requestInnerMapB);

                if (vehicleB.getCondition8().getValue()) count++;
                requestInnerMapB = new HashMap<>();
                requestInnerMapB.put("check", vehicleB.getCondition8().getValue());
                requestB.add(requestInnerMapB);

                if (vehicleB.getCondition9().getValue()) count++;
                requestInnerMapB = new HashMap<>();
                requestInnerMapB.put("check", vehicleB.getCondition9().getValue());
                requestB.add(requestInnerMapB);

                if (vehicleB.getCondition10().getValue()) count++;
                requestInnerMapB = new HashMap<>();
                requestInnerMapB.put("check", vehicleB.getCondition10().getValue());
                requestB.add(requestInnerMapB);

                if (vehicleB.getCondition11().getValue()) count++;
                requestInnerMapB = new HashMap<>();
                requestInnerMapB.put("check", vehicleB.getCondition11().getValue());
                requestB.add(requestInnerMapB);

                if (vehicleB.getCondition12().getValue()) count++;
                requestInnerMapB = new HashMap<>();
                requestInnerMapB.put("check", vehicleB.getCondition12().getValue());
                requestB.add(requestInnerMapB);

                if (vehicleB.getCondition13().getValue()) count++;
                requestInnerMapB = new HashMap<>();
                requestInnerMapB.put("check", vehicleB.getCondition13().getValue());
                requestB.add(requestInnerMapB);

                if (vehicleB.getCondition14().getValue()) count++;
                requestInnerMapB = new HashMap<>();
                requestInnerMapB.put("check", vehicleB.getCondition14().getValue());
                requestB.add(requestInnerMapB);

                if (vehicleB.getCondition15().getValue()) count++;
                requestInnerMapB = new HashMap<>();
                requestInnerMapB.put("check", vehicleB.getCondition15().getValue());
                requestB.add(requestInnerMapB);

                if (vehicleB.getCondition16().getValue()) count++;
                requestInnerMapB = new HashMap<>();
                requestInnerMapB.put("check", vehicleB.getCondition16().getValue());
                requestB.add(requestInnerMapB);

                if (vehicleB.getCondition17().getValue()) count++;
                requestInnerMapB = new HashMap<>();
                requestInnerMapB.put("check", vehicleB.getCondition17().getValue());
                requestB.add(requestInnerMapB);

                requestMap.put("b_insured_check", requestB);
                requestMap.put("b_insured_total_check", count.toString());
            }
        }

        requestMap.put("claims_number", claimsEntity.getPracticeId().toString());
        if(claimsEntity.getCreatedAt()!=null){
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            simpleDateFormat.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));
            Date createdAtDate = DateUtil.convertIS08601StringToUTCDate(DateUtil.convertUTCInstantToIS08601String(claimsEntity.getCreatedAt()));
            requestMap.put("datetime_ins", simpleDateFormat.format(createdAtDate));
        }
        if (claimsEntity.getComplaint() != null) {
            DataAccident dataAccident = claimsEntity.getComplaint().getDataAccident();
            if (dataAccident != null) {
                Date datetime = dataAccident.getDateAccident();
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                simpleDateFormat.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));
                String date = simpleDateFormat.format(datetime);
                requestMap.put("date", date);
                SimpleDateFormat simpleHourFormat = new SimpleDateFormat("HH:mm");
                simpleHourFormat.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));
                String hour = simpleHourFormat.format(datetime);
                requestMap.put("time", hour);

                if (dataAccident.getAddress() != null) {
                    requestMap.put("region", dataAccident.getAddress().getRegion());
                    requestMap.put("province", dataAccident.getAddress().getProvince());
                    requestMap.put("city", dataAccident.getAddress().getLocality());
                    requestMap.put("address", dataAccident.getAddress().getStreet());
                    requestMap.put("address_number", dataAccident.getAddress().getStreetNr());
                }
                if (dataAccident.getInterventionAuthority())
                    requestMap.put("police", "SI");
                else requestMap.put("police", "NO");
            }
        }
        Damaged damaged = claimsEntity.getDamaged();
        Map<String, String> innerDriver1 = new HashMap<>();
        Map<String, String> innerDriver2 = new HashMap<>();
        Map<String, String> innerInsurance1 = new HashMap<>();
        Map<String, String> innerInsurance2 = new HashMap<>();
        Map<String, String> innerVehicle1 = new HashMap<>();
        Map<String, String> innerVehicle2 = new HashMap<>();
        Map<String, String> insured1 = new HashMap<>();
        Map<String, String> insured2 = new HashMap<>();
        List<CounterpartyEntity> counterparties = claimsEntity.getCounterparts();
        CounterpartyEntity counterparty = null;
        if (counterparties != null && !counterparties.isEmpty()) {
            counterparty = counterparties.get(0);
        }
        if (damaged != null) {
            Driver driver = damaged.getDriver();
            if (driver != null) {
                innerDriver1.put("email", driver.getEmail());
                innerDriver1.put("driver_phone", driver.getPhone());
                innerDriver1.put("name", driver.getFirstname());
                innerDriver1.put("surname", driver.getLastname());
            }

            InsuranceCompany insuranceCompany = damaged.getInsuranceCompany();
            if (insuranceCompany != null && insuranceCompany.getTpl() != null && insuranceCompany.getTpl().getInsuranceCompanyId() != null) {
                UUID companyEntity = insuranceCompany.getTpl().getInsuranceCompanyId();
                Optional<InsuranceCompanyEntity> entityOptional = insuranceCompanyRepository.findById(companyEntity);
                if (entityOptional.isPresent()) {
                    InsuranceCompanyEntity companyEntity1 = entityOptional.get();
                    innerInsurance1.put("policy_number", insuranceCompany.getTpl().getPolicyNumber());
                    innerInsurance1.put("company_name", companyEntity1.getName());
                }
            } else {
                LOGGER.debug("Company not present into cai details pdf");
            }

            Vehicle vehicle = damaged.getVehicle();
            if (vehicle != null) {
                innerVehicle1.put("plate", vehicle.getLicensePlate());
                innerVehicle1.put("model", vehicle.getModel());
            }

            Customer customer = damaged.getCustomer();
            if (customer != null) {
                insured1.put("name", "--");
                insured1.put("surname", customer.getLegalName());
            }
        }
        if (counterparty != null) {
            Driver driver = counterparty.getDriver();
            if (driver != null) {
                innerDriver2.put("email", driver.getEmail());
                innerDriver2.put("driver_phone", driver.getPhone());
                innerDriver2.put("name", driver.getFirstname());
                innerDriver2.put("surname", driver.getLastname());
            }

            InsuranceCompanyCounterparty insuranceCompany = counterparty.getInsuranceCompany();
            if (insuranceCompany != null && insuranceCompany.getEntity() != null) {
                innerInsurance2.put("policy_number", counterparty.getPolicyNumber());
                innerInsurance2.put("company_name", insuranceCompany.getEntity().getName());
            }

            Vehicle vehicle = counterparty.getVehicle();
            if (vehicle != null) {
                innerVehicle2.put("plate", vehicle.getLicensePlate());
                innerVehicle2.put("model", vehicle.getModel());

            }

            Insured insured = counterparty.getInsured();
            if (insured != null) {
                insured2.put("name", insured.getFirstname());
                insured2.put("surname", insured.getLastname());
            }
        }
        if (claimsEntity.getCaiDetails() != null && claimsEntity.getCaiDetails().getDriverSide() != null) {
            if (claimsEntity.getCaiDetails().getDriverSide().equalsIgnoreCase("A")) {
                requestMap.put("a_driver", innerDriver1);
                requestMap.put("a_insurance", innerInsurance1);
                requestMap.put("a_vehicle", innerVehicle1);
                requestMap.put("a_insured", insured1);
                requestMap.put("b_driver", innerDriver2);
                requestMap.put("b_insurance", innerInsurance2);
                requestMap.put("b_vehicle", innerVehicle2);
                requestMap.put("b_insured", insured2);
                if (damaged != null) {
                    if (damaged.getCaiSigned() == null || !damaged.getCaiSigned())
                        requestMap.put("driver_signature_a_no", "U");
                    else requestMap.put("driver_signature_a_yes", "U");
                    ImpactPoint impactPoint = damaged.getImpactPoint();
                    if (impactPoint != null) {
                        requestMap.put("a_visible_damages", impactPoint.getDamageToVehicle());
                    }
                    if (damaged.getImpactPoint() != null) {
                        List<ImpactPointDetectionImpactPointEnum> impactPointDetectionImpactPointEntities = damaged.getImpactPoint().getDetectionsImpactPoint();
                        Map<String, Object> map = new HashMap<>();
                        if (damaged.getVehicle().getNature() == VehicleNatureEnum.MOTOR_VEHICLE) {
                            for (ImpactPointDetectionImpactPointEnum impactPointDetectionImpactPointEnum : impactPointDetectionImpactPointEntities) {
                                String impactPointValue = "a_" + ImpactPointDetectionImpactPointAdapter.adptFromImPointDetectionImpactPointEntityToJsonKeyCar(impactPointDetectionImpactPointEnum.getValue());
                                map.put(impactPointValue, true);
                            }
                        } else if (damaged.getVehicle().getNature() == VehicleNatureEnum.MOTOR_CYCLE) {
                            for (ImpactPointDetectionImpactPointEnum impactPointDetectionImpactPointEnum : impactPointDetectionImpactPointEntities) {
                                String impactPointValue = "a_" + ImpactPointDetectionImpactPointAdapter.adptFromImPointDetectionImpactPointEntityToJsonKeyMoto(impactPointDetectionImpactPointEnum.getValue());
                                map.put(impactPointValue, true);
                            }
                        } else {
                            for (ImpactPointDetectionImpactPointEnum impactPointDetectionImpactPointEnum : impactPointDetectionImpactPointEntities) {
                                String impactPointValue = "a_" + ImpactPointDetectionImpactPointAdapter.adptFromImPointDetectionImpactPointEntityToJsonKeyVan(impactPointDetectionImpactPointEnum.getValue());
                                map.put(impactPointValue, true);
                            }
                        }
                        requestMap.put("a_check", map);
                    }

                    if (damaged.getVehicle() != null) {
                        requestMap.put("a_vehicle_type", damaged.getVehicle().getNature().name());
                    }
                }
                if (isDriverInjury(claimsEntity.getWoundedList()))
                    requestMap.put("vehicle_accident", "SI");
                else
                    requestMap.put("vehicle_accident", "NO");
                if (counterparty != null) {
                    if (counterparty.getCaiSigned() == null || !counterparty.getCaiSigned())
                        requestMap.put("driver_signature_b_no", "U");
                    else requestMap.put("driver_signature_b_yes", "U");
                    ImpactPoint impactPoint = counterparty.getImpactPoint();
                    if (impactPoint != null) {
                        requestMap.put("b_visible_damages", impactPoint.getDamageToVehicle());
                    }
                    if (counterparty.getImpactPoint() != null) {
                        List<ImpactPointDetectionImpactPointEnum> impactPointDetectionImpactPointEntities = counterparty.getImpactPoint().getDetectionsImpactPoint();
                        Map<String, Object> map = new HashMap<>();
                        if (counterparty.getVehicle().getNature().equals(VehicleNatureEnum.MOTOR_VEHICLE)) {
                            for (ImpactPointDetectionImpactPointEnum impactPointDetectionImpactPointEnum : impactPointDetectionImpactPointEntities) {
                                String impactPointValue = "b_" + ImpactPointDetectionImpactPointAdapter.adptFromImPointDetectionImpactPointEntityToJsonKeyCar(impactPointDetectionImpactPointEnum.getValue());
                                map.put(impactPointValue, true);
                            }
                        } else if (counterparty.getVehicle().getNature().equals(VehicleNatureEnum.MOTOR_CYCLE)) {
                            for (ImpactPointDetectionImpactPointEnum impactPointDetectionImpactPointEnum : impactPointDetectionImpactPointEntities) {
                                String impactPointValue = "b_" + ImpactPointDetectionImpactPointAdapter.adptFromImPointDetectionImpactPointEntityToJsonKeyMoto(impactPointDetectionImpactPointEnum.getValue());
                                map.put(impactPointValue, true);
                            }
                        } else {
                            for (ImpactPointDetectionImpactPointEnum impactPointDetectionImpactPointEnum : impactPointDetectionImpactPointEntities) {
                                String impactPointValue = "b_" + ImpactPointDetectionImpactPointAdapter.adptFromImPointDetectionImpactPointEntityToJsonKeyVan(impactPointDetectionImpactPointEnum.getValue());
                                map.put(impactPointValue, true);
                            }
                        }
                        requestMap.put("b_check", map);
                    }
                    if (counterparty.getVehicle() != null) {
                        requestMap.put("b_vehicle_type", counterparty.getVehicle().getNature().name());
                    }
                }
            } else {
                requestMap.put("b_driver", innerDriver1);
                requestMap.put("b_insurance", innerInsurance1);
                requestMap.put("b_vehicle", innerVehicle1);
                requestMap.put("b_insured", insured1);
                requestMap.put("a_driver", innerDriver2);
                requestMap.put("a_insurance", innerInsurance2);
                requestMap.put("a_vehicle", innerVehicle2);
                requestMap.put("a_insured", insured2);
                if (damaged != null) {
                    if (damaged.getCaiSigned() == null || !damaged.getCaiSigned())
                        requestMap.put("driver_signature_b_no", "U");
                    else requestMap.put("driver_signature_b_yes", "U");
                    ImpactPoint impactPoint = damaged.getImpactPoint();
                    if (impactPoint != null) {
                        requestMap.put("b_visible_damages", impactPoint.getDamageToVehicle());

                        List<ImpactPointDetectionImpactPointEnum> impactPointDetectionImpactPointEntities = damaged.getImpactPoint().getDetectionsImpactPoint();
                        Map<String, Object> map = new HashMap<>();
                        if (damaged.getVehicle().getNature() == VehicleNatureEnum.MOTOR_VEHICLE) {
                            for (ImpactPointDetectionImpactPointEnum impactPointDetectionImpactPointEnum : impactPointDetectionImpactPointEntities) {
                                String impactPointValue = "b_" + ImpactPointDetectionImpactPointAdapter.adptFromImPointDetectionImpactPointEntityToJsonKeyCar(impactPointDetectionImpactPointEnum.getValue());
                                map.put(impactPointValue, true);
                            }
                        } else if (damaged.getVehicle().getNature() == VehicleNatureEnum.MOTOR_CYCLE) {
                            for (ImpactPointDetectionImpactPointEnum impactPointDetectionImpactPointEnum : impactPointDetectionImpactPointEntities) {
                                String impactPointValue = "b_" + ImpactPointDetectionImpactPointAdapter.adptFromImPointDetectionImpactPointEntityToJsonKeyMoto(impactPointDetectionImpactPointEnum.getValue());
                                map.put(impactPointValue, true);
                            }
                        } else {
                            for (ImpactPointDetectionImpactPointEnum impactPointDetectionImpactPointEnum : impactPointDetectionImpactPointEntities) {
                                String impactPointValue = "b_" + ImpactPointDetectionImpactPointAdapter.adptFromImPointDetectionImpactPointEntityToJsonKeyVan(impactPointDetectionImpactPointEnum.getValue());
                                map.put(impactPointValue, true);
                            }

                        }
                        requestMap.put("b_check", map);
                    }

                    if (damaged.getVehicle() != null) {
                        requestMap.put("b_vehicle_type", damaged.getVehicle().getNature().name());
                    }
                }
                if (isDriverInjury(claimsEntity.getWoundedList()))
                    requestMap.put("vehicle_accident", "SI");
                else
                    requestMap.put("vehicle_accident", "NO");
                if (counterparty != null) {
                    if (counterparty.getCaiSigned() == null || !counterparty.getCaiSigned())
                        requestMap.put("driver_signature_a_no", "U");
                    else requestMap.put("driver_signature_a_yes", "U");
                    ImpactPoint impactPoint = counterparty.getImpactPoint();
                    if (impactPoint != null) {
                        requestMap.put("a_visible_damages", impactPoint.getDamageToVehicle());

                        List<ImpactPointDetectionImpactPointEnum> impactPointDetectionImpactPointEntities = counterparty.getImpactPoint().getDetectionsImpactPoint();
                        Map<String, Object> map = new HashMap<>();
                        if (counterparty.getVehicle().getNature() == VehicleNatureEnum.MOTOR_VEHICLE) {
                            for (ImpactPointDetectionImpactPointEnum impactPointDetectionImpactPointEnum : impactPointDetectionImpactPointEntities) {
                                String impactPointValue = "a_" + ImpactPointDetectionImpactPointAdapter.adptFromImPointDetectionImpactPointEntityToJsonKeyCar(impactPointDetectionImpactPointEnum.getValue());
                                map.put(impactPointValue, true);
                            }
                        } else if (counterparty.getVehicle().getNature() == VehicleNatureEnum.MOTOR_CYCLE) {
                            for (ImpactPointDetectionImpactPointEnum impactPointDetectionImpactPointEnum : impactPointDetectionImpactPointEntities) {
                                String impactPointValue = "a_" + ImpactPointDetectionImpactPointAdapter.adptFromImPointDetectionImpactPointEntityToJsonKeyMoto(impactPointDetectionImpactPointEnum.getValue());
                                map.put(impactPointValue, true);
                            }
                        } else {
                            for (ImpactPointDetectionImpactPointEnum impactPointDetectionImpactPointEnum : impactPointDetectionImpactPointEntities) {
                                String impactPointValue = "a_" + ImpactPointDetectionImpactPointAdapter.adptFromImPointDetectionImpactPointEntityToJsonKeyVan(impactPointDetectionImpactPointEnum.getValue());
                                map.put(impactPointValue, true);
                            }
                        }
                        requestMap.put("a_check", map);
                    }
                    if (counterparty.getVehicle() != null) {
                        requestMap.put("a_vehicle_type", counterparty.getVehicle().getNature().name());
                    }
                }
            }
        }
        if (claimsEntity.getDeponentList() != null) {
            int numDeponent = 0;
            Iterator<Deponent> deponentIterator = claimsEntity.getDeponentList().iterator();
            while (numDeponent < 2 && deponentIterator.hasNext()) {
                numDeponent++;
                Deponent deponent = deponentIterator.next();
                if (deponent.getLastname() != null) {
                    String labelSurname = "txt_witnesses_" + numDeponent + "_surname";
                    String surname = "witnesses_" + numDeponent + "_surname";
                    requestMap.put(surname, deponent.getLastname());
                    requestMap.put(labelSurname, "Cognome");
                }
                if (deponent.getFirstname() != null) {
                    String labelName = "txt_witnesses_" + numDeponent + "_name";
                    String name = "witnesses_" + numDeponent + "_name";
                    requestMap.put(name, deponent.getFirstname());
                    requestMap.put(labelName, "Nome");
                }
                if (deponent.getAddress() != null) {
                    if (deponent.getAddress().getFormattedAddress() != null) {
                        String labelAddress = "txt_witnesses_" + numDeponent + "_address";
                        String address = "witnesses_" + numDeponent + "_address";
                        requestMap.put(address, deponent.getAddress().getFormattedAddress());
                        requestMap.put(labelAddress, "Indirizzo");
                    } else if (deponent.getAddress().getStreet() != null && deponent.getAddress().getStreetNr() != null) {
                        String labelAddress = "txt_witnesses_" + numDeponent + "_address";
                        String address = deponent.getAddress().getStreet() + ", " + deponent.getAddress().getStreetNr();
                        String address1 = "witnesses_" + numDeponent + "_address";
                        requestMap.put(labelAddress, "Indirizzo");
                        requestMap.put(address1, address);
                    }
                }
                if (deponent.getFiscalCode() != null) {
                    String labelFiscalCode = "txt_witnesses_" + numDeponent + "_fiscal_code";
                    String fiscalCode = "witnesses_" + numDeponent + "_fiscal_code";
                    requestMap.put(fiscalCode, deponent.getFiscalCode());
                    requestMap.put(labelFiscalCode, "Codice fiscale");
                }
                if (deponent.getPhone() != null) {
                    String labelPhone = "txt_witnesses_" + numDeponent + "_phone";
                    String phone = "witnesses_" + numDeponent + "_phone";
                    requestMap.put(phone, deponent.getPhone());
                    requestMap.put(labelPhone, "Telefono");
                }
                if (deponent.getEmail() != null) {
                    String labelEmail = "txt_witnesses_" + numDeponent + "_email";
                    String email = "witnesses_" + numDeponent + "_email";
                    requestMap.put(email, deponent.getEmail());
                    requestMap.put(labelEmail, "Email");
                }
            }
        }
        if (claimsEntity.getWoundedList() != null) {
            int numWounded = 0;
            Iterator<Wounded> woundedIterator = claimsEntity.getWoundedList().iterator();
            while (numWounded < 4 && woundedIterator.hasNext()) {
                numWounded++;
                Wounded wounded = woundedIterator.next();
                if (wounded.getType() != null) {
                    String labelType = "txt_injured_type_" + numWounded;
                    String type = "injured_type_" + numWounded;
                    requestMap.put(type, WoundedAdapter.adptFromWoundedTypeEnumToItalian(wounded.getType()));
                    requestMap.put(labelType, "Tipologia");
                }
                if (wounded.getLastname() != null) {
                    String labelSurname = "txt_injured_" + numWounded + "_surname";
                    String surname = "injured_" + numWounded + "_surname";
                    requestMap.put(surname, wounded.getLastname());
                    requestMap.put(labelSurname, "Cognome");
                }
                if (wounded.getFirstname() != null) {
                    String labelName = "txt_injured_" + numWounded + "_name";
                    String name = "injured_" + numWounded + "_name";
                    requestMap.put(name, wounded.getFirstname());
                    requestMap.put(labelName, "Nome");
                }
                if (wounded.getAddress() != null) {
                    if (wounded.getAddress().getFormattedAddress() != null) {
                        String labelAddress = "txt_injured_" + numWounded + "_address";
                        String address = "injured_" + numWounded + "_address";
                        requestMap.put(address, wounded.getAddress().getFormattedAddress());
                        requestMap.put(labelAddress, "Indirizzo");
                    } else if (wounded.getAddress().getStreet() != null && wounded.getAddress().getStreetNr() != null) {
                        String labelAddress = "txt_injured_" + numWounded + "_address";
                        String address = wounded.getAddress().getStreet() + ", " + wounded.getAddress().getStreetNr();
                        String address1 = "injured_" + numWounded + "_address";
                        requestMap.put(labelAddress, "Indirizzo");
                        requestMap.put(address1, address);
                    }
                }
                if (wounded.getEmergencyRoom() != null) {
                    String labelRoom = "txt_injured_" + numWounded + "_emergency_room";
                    requestMap.put(labelRoom, "Pronto soccorso");
                    String room = "injured_" + numWounded + "_emergency_room";
                    if (wounded.getEmergencyRoom()) {
                        requestMap.put(room, "SI");
                    } else {
                        requestMap.put(room, "NO");
                    }
                }
                if (wounded.getWound() != null) {
                    String labelWounds = "txt_injured_" + numWounded + "_wounds";
                    String wounds = "injured_" + numWounded + "_wounds";
                    requestMap.put(labelWounds, "Ferite");
                    requestMap.put(wounds, WoundedAdapter.adptFromWoundedWoundEnumToItalian(wounded.getWound()));

                }
            }
        }
        requestMap.put("title", "MODULO RIEPILOGO INSERIMENTO DENUNCIA");
        requestOut.setData(requestMap);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy_MM_dd");
        String date = simpleDateFormat.format(new Date());
        requestOut.setFilename("CAI_" + claimsEntity.getPracticeId() + ".pdf");
        requestOut.setTemplate("DenunciaControparte");
        DogeRequestV1.FileManager fileManager = new DogeRequestV1.FileManager();
        String uuid = UUID.randomUUID().toString();
        fileManager.setUuid(uuid);
        fileManager.setResourceId("1");
        fileManager.setResourceType("claims");
        fileManager.setBlobType("pdf");
        requestOut.setFileManager(fileManager);

        DogeResponseV1.ActionResult response = callWithoutCert(dogeAPIEndpoint + dogeAPIEnqueue, HttpMethod.POST, requestOut, new HashMap<>(), DogeResponseV1.ActionResult.class);
        if (response.getCode() == 1) {
            response = new DogeResponseV1.ActionResult(response.getCode(), response.getMessage(), response.getDetails());
        }
        DogeResponseV1 dogeResponse = new DogeResponseV1();
        dogeResponse.setDocumentId(uuid);
        dogeResponse.setActionResult(response);
        return dogeResponse;
    }

    // TODO REFACTOR
    @Override
    public DogeResponseV1 enqueueClaimsWithoutCai(String idClaims) throws IOException {
        DogeRequestV1 requestOut = new DogeRequestV1();

        Map<String, Object> requestMap = new HashMap<>();

        Optional<ClaimsNewEntity> claimsNewEntityOptional = claimsNewRepository.findById(idClaims);
        if (!claimsNewEntityOptional.isPresent()) {
            throw new BadRequestException("Claims with id " + idClaims + "not found", MessageCode.CLAIMS_1010);
        }
        ClaimsEntity claimsEntity = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntityOptional.get());

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));
        SimpleDateFormat simpleHourFormat = new SimpleDateFormat("HH:mm");
        simpleHourFormat.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));
        if (claimsEntity.getWoundedList() != null) {
            if (claimsEntity.getWoundedList().isEmpty()) requestMap.put("transported_accident", "NO");
            else {
                boolean check = false;
                int i = 0;
                while (!check && i < claimsEntity.getWoundedList().size()) {
                    Wounded wounded = claimsEntity.getWoundedList().get(i);
                    if (wounded.getType() != null) {
                        if (wounded.getType().getValue().equalsIgnoreCase("PASSENGER")) check = true;
                    }
                    i++;
                }

                if (check) requestMap.put("transported_accident", "SI");
                else requestMap.put("transported_accident", "NO");
            }
        } else {
            requestMap.put("transported_accident", "NO");
        }
        requestMap.put("title", "MODULO RIEPILOGO INSERIMENTO DENUNCIA");
        requestMap.put("claim_number", claimsEntity.getPracticeId().toString());

        Date dateAndHourInsertion = DateUtil.convertIS08601StringToUTCDate(DateUtil.convertUTCInstantToIS08601String(claimsEntity.getCreatedAt()));
        String dateInsertion = simpleDateFormat.format(dateAndHourInsertion);
        String hourInsertion = simpleHourFormat.format(dateAndHourInsertion);

        requestMap.put("datetime_ins", dateInsertion + " " + hourInsertion);

        if (claimsEntity.getDeponentList() != null) {
            if (claimsEntity.getDeponentList().isEmpty()) requestMap.put("witness", "NO");
            else requestMap.put("witness", "SI");
        } else {
            requestMap.put("witness", "NO");
        }

        Damaged damaged = claimsEntity.getDamaged();
        if (damaged != null) {
            Driver driver = damaged.getDriver();
            if (driver != null) {
                requestMap.put("ald_driver_email", driver.getEmail());
                requestMap.put("ald_driver_phone", driver.getPhone());
                requestMap.put("ald_driver_name", driver.getFirstname());
                requestMap.put("ald_driver_surname", driver.getLastname());

            }

            InsuranceCompany insuranceCompany = damaged.getInsuranceCompany();
            if (insuranceCompany != null && insuranceCompany.getTpl() != null && insuranceCompany.getTpl().getInsuranceCompanyId() != null) {
                UUID companyEntity = insuranceCompany.getTpl().getInsuranceCompanyId();
                Optional<InsuranceCompanyEntity> entityOptional = insuranceCompanyRepository.findById(companyEntity);
                if (entityOptional.isPresent()) {
                    InsuranceCompanyEntity companyEntity1 = entityOptional.get();
                    requestMap.put("ald_insurance_policy_number", insuranceCompany.getTpl().getPolicyNumber());
                    requestMap.put("ald_insurance_company_name", companyEntity1.getName());
                }
            } else {
                LOGGER.debug("Company not present into cai details pdf");
            }

            Vehicle vehicle = damaged.getVehicle();
            if (vehicle != null) {
                requestMap.put("ald_plate", vehicle.getLicensePlate());
                requestMap.put("ald_model", vehicle.getModel());
            }

            Customer customer = damaged.getCustomer();
            if (customer != null) {
                requestMap.put("ald_insured", customer.getLegalName());
            }

            ImpactPoint impactPoint = damaged.getImpactPoint();
            if (impactPoint != null) {
                requestMap.put("visible_damages", impactPoint.getDamageToVehicle());
                requestMap.put("claim_description", impactPoint.getIncidentDescription());
            }
            if (damaged.getImpactPoint() != null) {
                List<ImpactPointDetectionImpactPointEnum> impactPointDetectionImpactPointEntities = damaged.getImpactPoint().getDetectionsImpactPoint();
                requestMap.put("ald_vehicle_type", damaged.getVehicle().getNature().name());
                if (damaged.getVehicle().getNature() == VehicleNatureEnum.MOTOR_VEHICLE) {
                    for (ImpactPointDetectionImpactPointEnum impactPointDetectionImpactPointEnum : impactPointDetectionImpactPointEntities) {
                        String impactPointValue = "ald_" + ImpactPointDetectionImpactPointAdapter.adptFromImPointDetectionImpactPointEntityToJsonKeyCar(impactPointDetectionImpactPointEnum.getValue());
                        requestMap.put(impactPointValue, true);
                    }
                } else if (damaged.getVehicle().getNature() == VehicleNatureEnum.MOTOR_CYCLE) {
                    for (ImpactPointDetectionImpactPointEnum impactPointDetectionImpactPointEnum : impactPointDetectionImpactPointEntities) {
                        String impactPointValue = "ald_" + ImpactPointDetectionImpactPointAdapter.adptFromImPointDetectionImpactPointEntityToJsonKeyMoto(impactPointDetectionImpactPointEnum.getValue());
                        requestMap.put(impactPointValue, true);
                    }
                } else {
                    for (ImpactPointDetectionImpactPointEnum impactPointDetectionImpactPointEnum : impactPointDetectionImpactPointEntities) {
                        String impactPointValue = "ald_" + ImpactPointDetectionImpactPointAdapter.adptFromImPointDetectionImpactPointEntityToJsonKeyVan(impactPointDetectionImpactPointEnum.getValue());
                        requestMap.put(impactPointValue, true);
                    }
                }
            }
        }

        if (claimsEntity.getComplaint() != null) {

            DataAccident dataAccident = claimsEntity.getComplaint().getDataAccident();
            if (dataAccident != null) {

                Date datetime = dataAccident.getDateAccident();
                String date = simpleDateFormat.format(datetime);
                requestMap.put("date", date);

                String hour = simpleHourFormat.format(datetime);
                requestMap.put("time", hour);

                if (dataAccident.getAddress() != null) {
                    requestMap.put("region", dataAccident.getAddress().getRegion());
                    requestMap.put("province", dataAccident.getAddress().getProvince());
                    requestMap.put("city", dataAccident.getAddress().getLocality());
                    requestMap.put("address", dataAccident.getAddress().getStreet());
                    requestMap.put("address_number", dataAccident.getAddress().getStreetNr());
                }
                if (dataAccident.getInterventionAuthority())
                    requestMap.put("police", "SI");
                else requestMap.put("police", "NO");
            }
        }

        List<CounterpartyEntity> counterpartyList = claimsEntity.getCounterparts();
        if (counterpartyList != null) {
            int count = 0;
            for (CounterpartyEntity counterparty : counterpartyList) {
                count++;
                if (count > 4) {
                    break;
                }
                if (counterparty.getType() != null) {
                    if (counterparty.getType().getValue().equalsIgnoreCase("vehicle")) {
                        if (counterparty.getVehicle() != null) {
                            String vehicle = "counterpart_" + count + "_model";
                            String vehicleLabel = "txt_counterpart_" + count + "_model";
                            String plate = "counterpart_" + count + "_plate";

                            requestMap.put(vehicle, counterparty.getVehicle().getModel());
                            requestMap.put(plate, counterparty.getVehicle().getLicensePlate());
                            requestMap.put(vehicleLabel, "Veicolo ");
                            if (counterparty.getDriver() != null) {
                                String driver = "counterpart_" + count + "_driver_name_and_surname";
                                String driverLabel = "txt_counterpart_" + count + "_driver_name_and_surname";
                                String email = "counterpart_" + count + "_driver_email";
                                String phone = "counterpart_" + count + "_driver_phone";
                                requestMap.put(driverLabel, "Conducente ");
                                requestMap.put(driver, counterparty.getDriver().getLastname() + " " + counterparty.getDriver().getFirstname());
                                requestMap.put(email, counterparty.getDriver().getEmail());
                                requestMap.put(phone, counterparty.getDriver().getPhone());
                            }

                            String type = "c" + count + "_vehicle_type";
                            requestMap.put(type, counterparty.getVehicle().getNature().name());
                            if (counterparty.getImpactPoint() != null) {
                                List<ImpactPointDetectionImpactPointEnum> impactPointDetectionImpactPointEntities = counterparty.getImpactPoint().getDetectionsImpactPoint();
                                if (counterparty.getVehicle().getNature() == VehicleNatureEnum.MOTOR_VEHICLE) {
                                    for (ImpactPointDetectionImpactPointEnum impactPointDetectionImpactPointEnum : impactPointDetectionImpactPointEntities) {
                                        String impactPointValue = "counterpart_" + count + "_" + ImpactPointDetectionImpactPointAdapter.adptFromImPointDetectionImpactPointEntityToJsonKeyCar(impactPointDetectionImpactPointEnum.getValue());
                                        requestMap.put(impactPointValue, true);
                                    }
                                } else if (counterparty.getVehicle().getNature() == VehicleNatureEnum.MOTOR_CYCLE) {

                                    for (ImpactPointDetectionImpactPointEnum impactPointDetectionImpactPointEnum : impactPointDetectionImpactPointEntities) {
                                        String impactPointValue = "counterpart_" + count + "_" + ImpactPointDetectionImpactPointAdapter.adptFromImPointDetectionImpactPointEntityToJsonKeyMoto(impactPointDetectionImpactPointEnum.getValue());
                                        requestMap.put(impactPointValue, true);
                                    }
                                } else {
                                    for (ImpactPointDetectionImpactPointEnum impactPointDetectionImpactPointEnum : impactPointDetectionImpactPointEntities) {
                                        String impactPointValue = "counterpart_" + count + "_" + ImpactPointDetectionImpactPointAdapter.adptFromImPointDetectionImpactPointEntityToJsonKeyVan(impactPointDetectionImpactPointEnum.getValue());
                                        requestMap.put(impactPointValue, true);
                                    }
                                }
                            }
                        }
                    } else {
                        String type = "c" + count + "_vehicle_type";
                        requestMap.put(type, "DEFAULT");
                        String descriptionLabel = "txt_counterpart_" + count + "_description";
                        String description = "counterpart_" + count + "_description";
                        requestMap.put(description, counterparty.getDescription());
                        requestMap.put(descriptionLabel, "Descrizione controparte ");
                        if (counterparty.getDriver() != null) {
                            String referencePerson = "counterpart_" + count + "_reference_person";
                            String referencePersonLabel = "txt_counterpart_" + count + "_reference_person";
                            String email = "counterpart_" + count + "_driver_email";
                            String phone = "counterpart_" + count + "_driver_phone";
                            requestMap.put(referencePersonLabel, "Persona di riferimento ");
                            requestMap.put(referencePerson, counterparty.getDriver().getLastname() + " " + counterparty.getDriver().getFirstname());
                            requestMap.put(email, counterparty.getDriver().getEmail());
                            requestMap.put(phone, counterparty.getDriver().getPhone());
                        }
                    }

                }
                if (counterparty.getInsured() != null) {
                    String insured = "counterpart_" + count + "_insured";
                    requestMap.put(insured, counterparty.getInsured().getLastname() + " " + counterparty.getInsured().getFirstname());
                }

                if (counterparty.getInsuranceCompany() != null) {
                    String company = "counterpart_" + count + "_insurance_company_name";
                    String number = "counterpart_" + count + "_insurance_policy_number";
                    if (counterparty.getInsuranceCompany() != null) {
                        requestMap.put(company, counterparty.getInsuranceCompany().getName());
                        requestMap.put(number, counterparty.getPolicyNumber());
                    }
                }


            }
            int size = counterpartyList.size() + 1;
            for (int i = size; i <= 4; i++) {
                String type = "c" + i + "_vehicle_type";
                requestMap.put(type, "DEFAULT");
            }
        }
        if (claimsEntity.getDeponentList() != null) {
            int numDeponent = 0;
            Iterator<Deponent> deponentIterator = claimsEntity.getDeponentList().iterator();
            while (numDeponent < 2 && deponentIterator.hasNext()) {
                numDeponent++;
                Deponent deponent = deponentIterator.next();
                if (deponent.getLastname() != null) {
                    String labelSurname = "txt_witnesses_" + numDeponent + "_surname";
                    String surname = "witnesses_" + numDeponent + "_surname";
                    requestMap.put(surname, deponent.getLastname());
                    requestMap.put(labelSurname, "Cognome");
                }
                if (deponent.getFirstname() != null) {
                    String labelName = "txt_witnesses_" + numDeponent + "_name";
                    String name = "witnesses_" + numDeponent + "_name";
                    requestMap.put(name, deponent.getFirstname());
                    requestMap.put(labelName, "Nome");
                }
                if (deponent.getAddress() != null) {
                    if (deponent.getAddress().getFormattedAddress() != null) {
                        String labelAddress = "txt_witnesses_" + numDeponent + "_address";
                        String address = "witnesses_" + numDeponent + "_address";
                        requestMap.put(address, deponent.getAddress().getFormattedAddress());
                        requestMap.put(labelAddress, "Indirizzo");
                    } else if (deponent.getAddress().getStreet() != null && deponent.getAddress().getStreetNr() != null) {
                        String labelAddress = "txt_witnesses_" + numDeponent + "_address";
                        String address = deponent.getAddress().getStreet() + ", " + deponent.getAddress().getStreetNr();
                        String address1 = "witnesses_" + numDeponent + "_address";
                        requestMap.put(labelAddress, "Indirizzo");
                        requestMap.put(address1, address);
                    }
                }
                if (deponent.getFiscalCode() != null) {
                    String labelFiscalCode = "txt_witnesses_" + numDeponent + "_fiscal_code";
                    String fiscalCode = "witnesses_" + numDeponent + "_fiscal_code";
                    requestMap.put(fiscalCode, deponent.getFiscalCode());
                    requestMap.put(labelFiscalCode, "Codice fiscale");
                }
                if (deponent.getPhone() != null) {
                    String labelPhone = "txt_witnesses_" + numDeponent + "_phone";
                    String phone = "witnesses_" + numDeponent + "_phone";
                    requestMap.put(phone, deponent.getPhone());
                    requestMap.put(labelPhone, "Telefono");
                }
                if (deponent.getEmail() != null) {
                    String labelEmail = "txt_witnesses_" + numDeponent + "_email";
                    String email = "witnesses_" + numDeponent + "_email";
                    requestMap.put(email, deponent.getEmail());
                    requestMap.put(labelEmail, "Email");
                }
            }
        }

        if (isDriverInjury(claimsEntity.getWoundedList()))
            requestMap.put("vehicle_accident", "SI");
        else
            requestMap.put("vehicle_accident", "NO");

        if (claimsEntity.getWoundedList() != null) {
            int numWounded = 0;
            Iterator<Wounded> woundedIterator = claimsEntity.getWoundedList().iterator();
            while (numWounded < 4 && woundedIterator.hasNext()) {
                numWounded++;
                Wounded wounded = woundedIterator.next();
                if (wounded.getType() != null) {
                    String labelType = "txt_injured_type_" + numWounded;
                    String type = "injured_type_" + numWounded;
                    requestMap.put(type, WoundedAdapter.adptFromWoundedTypeEnumToItalian(wounded.getType()));
                    requestMap.put(labelType, "Tipologia");
                }
                if (wounded.getLastname() != null) {
                    String labelSurname = "txt_injured_" + numWounded + "_surname";
                    String surname = "injured_" + numWounded + "_surname";
                    requestMap.put(surname, wounded.getLastname());
                    requestMap.put(labelSurname, "Cognome");
                }
                if (wounded.getFirstname() != null) {
                    String labelName = "txt_injured_" + numWounded + "_name";
                    String name = "injured_" + numWounded + "_name";
                    requestMap.put(name, wounded.getFirstname());
                    requestMap.put(labelName, "Nome");
                }
                if (wounded.getAddress() != null) {
                    if (wounded.getAddress().getFormattedAddress() != null) {
                        String labelAddress = "txt_injured_" + numWounded + "_address";
                        String address = "injured_" + numWounded + "_address";
                        requestMap.put(address, wounded.getAddress().getFormattedAddress());
                        requestMap.put(labelAddress, "Indirizzo");
                    } else if (wounded.getAddress().getStreet() != null && wounded.getAddress().getStreetNr() != null) {
                        String labelAddress = "txt_injured_" + numWounded + "_address";
                        String address = wounded.getAddress().getStreet() + ", " + wounded.getAddress().getStreetNr();
                        String address1 = "injured_" + numWounded + "_address";
                        requestMap.put(labelAddress, "Indirizzo");
                        requestMap.put(address1, address);
                    }
                }
                if (wounded.getEmergencyRoom() != null) {
                    String labelRoom = "txt_injured_" + numWounded + "_emergency_room";
                    requestMap.put(labelRoom, "Pronto soccorso");
                    String room = "injured_" + numWounded + "_emergency_room";
                    if (wounded.getEmergencyRoom()) {
                        requestMap.put(room, "SI");
                    } else {
                        requestMap.put(room, "NO");
                    }
                }
                if (wounded.getWound() != null) {
                    String labelWounds = "txt_injured_" + numWounded + "_wounds";
                    String wounds = "injured_" + numWounded + "_wounds";
                    requestMap.put(labelWounds, "Ferite");
                    requestMap.put(wounds, WoundedAdapter.adptFromWoundedWoundEnumToItalian(wounded.getWound()));

                }
            }
        }
        requestOut.setData(requestMap);
        simpleDateFormat = new SimpleDateFormat("yyyy_MM_dd");
        String date = simpleDateFormat.format(new Date());
        requestOut.setFilename("Denuncia_" + claimsEntity.getPracticeId() + ".pdf");
        requestOut.setTemplate("ALDDenunciaControparteSenzaCAI");
        DogeRequestV1.FileManager fileManager = new DogeRequestV1.FileManager();
        String uuid = UUID.randomUUID().toString();
        fileManager.setUuid(uuid);
        fileManager.setResourceId("1");
        fileManager.setResourceType("claims");
        fileManager.setBlobType("pdf");
        requestOut.setFileManager(fileManager);

        DogeResponseV1.ActionResult response = callWithoutCert(dogeAPIEndpoint + dogeAPIEnqueue, HttpMethod.POST, requestOut, new HashMap<>(), DogeResponseV1.ActionResult.class);
        if (response.getCode() == 1) {
            response = new DogeResponseV1.ActionResult(response.getCode(), response.getMessage(), response.getDetails());
        }
        DogeResponseV1 dogeResponse = new DogeResponseV1();
        dogeResponse.setDocumentId(uuid);
        dogeResponse.setActionResult(response);
        return dogeResponse;

    }

    // TODO REFACTOR
    @Override
    public DogeResponseV1 enqueueClaimsPAI(String idClaims) throws IOException {
        DogeRequestV1 requestOut = new DogeRequestV1();
        Map<String, Object> requestMap = new HashMap<>();
        //recupero della nuova entità
        Optional<ClaimsNewEntity> claimsNewEntityOptional = claimsNewRepository.findById(idClaims);

        if (!claimsNewEntityOptional.isPresent()) {
            throw new NotFoundException("Claims with id " + idClaims + "not found", MessageCode.CLAIMS_1010);
        }

        //conversione nella vecchia entità
        ClaimsEntity claimsEntity = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntityOptional.get());
        List<Wounded> woundedList = claimsEntity.getWoundedList();
        if (woundedList == null || woundedList.isEmpty()) throw new BadRequestException(MessageCode.CLAIMS_1052);
        Wounded wounded = null;
        Boolean isFound = false;
        Iterator<Wounded> iterator = woundedList.iterator();
        while (iterator.hasNext() && !isFound) {
            Wounded currentWounded = iterator.next();
            if (currentWounded.getType().equals(WoundedTypeEnum.DRIVER)) {
                isFound = true;
                wounded = currentWounded;
            }
        }
        //DATI DRIVER FERITO ALD
        if (!isFound){
            throw new BadRequestException(MessageCode.CLAIMS_1053);
        }
        requestMap.put("customer_name", wounded.getFirstname() + " " + wounded.getLastname());
        requestMap.put("claim_injured", wounded.getFirstname() + " " + wounded.getLastname());
        if (wounded.getAddress() != null) {
            Address address = wounded.getAddress();
            requestMap.put("address", address.getFormattedAddress());
            if(address.getZip()!=null)
                requestMap.put("cap", address.getZip());
            String cityProvince = "";
            if(address.getLocality()!=null)
                cityProvince += address.getLocality();
            if(address.getProvince()!=null)
                cityProvince += " (" + address.getProvince() + ")";
            requestMap.put("city_province", cityProvince);
        }

        //ID PRATICA
        requestMap.put("claim_number", claimsEntity.getPracticeId().toString());
        //OGGETTO FILE
        requestMap.put("claim_object", "apertura posizione sinistro polizza infortuni conducente");

        //TARGA
        if (claimsEntity.getDamaged() != null && claimsEntity.getDamaged().getVehicle() != null) {
            String plate = claimsEntity.getDamaged().getVehicle().getLicensePlate();
            requestMap.put("plate", plate);
        }

        //DATA SINISTRO
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        formatter.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));
        if (claimsEntity.getComplaint() != null && claimsEntity.getComplaint().getDataAccident() != null && claimsEntity.getComplaint().getDataAccident().getDateAccident() != null) {
            requestMap.put("accident_date", formatter.format(claimsEntity.getComplaint().getDataAccident().getDateAccident()));
            //DATI MANAGER PAI
            if (claimsEntity.getDamaged() != null && claimsEntity.getDamaged().getInsuranceCompany() != null && claimsEntity.getDamaged().getInsuranceCompany().getPai() != null) {

                Pai pai = claimsEntity.getDamaged().getInsuranceCompany().getPai();
                if (pai.getInsurancePolicyId() != null) {
                    InsurancePolicyEntity insurancePolicyEntity = insurancePolicyRepository.getOne(pai.getInsurancePolicyId());
                    InsuranceManagerEntity insuranceManagerEntity = insurancePolicyEntity.getInsuranceManagerEntity();
                    if (insuranceManagerEntity != null && insuranceManagerEntity.getActive()!=null && insuranceManagerEntity.getActive()) {
                        requestMap.put("manager_sx_PAI", insuranceManagerEntity.getName());
                        requestMap.put("contact_manager_sx_PAI", insuranceManagerEntity.getReferencePerson());
                        requestMap.put("phone_manager_sx_PAI", insuranceManagerEntity.getTelephone());
                        requestMap.put("address_manager_sx_PAI", insuranceManagerEntity.getAddress());
                        if (insuranceManagerEntity.getWebSite() != null && insuranceManagerEntity.getWebSite().contains("@")) {
                            requestMap.put("pec_manager_sx_PAI", insuranceManagerEntity.getWebSite());
                        }
                    }
                }

                requestMap.put("death_insured_capital", pai.getMaxCoverage());
                requestMap.put("permanent_disability", pai.getMaxCoverage());
                requestMap.put("fixed_deductible_PAI", pai.getPercentage());
                requestMap.put("tarifcode", pai.getTariffCode());
                requestMap.put("spese_cura", pai.getTariff());
                requestMap.put("policy_number", pai.getPolicyNumber());

                //DATI COMPAGNIA ASSICURATIVA
                if (claimsEntity.getDamaged().getInsuranceCompany().getPai().getInsuranceCompanyId() != null) {
                    Optional<InsuranceCompanyEntity> insuranceCompanyEntityOptional = insuranceCompanyRepository.findById(claimsEntity.getDamaged().getInsuranceCompany().getPai().getInsuranceCompanyId());
                    if (insuranceCompanyEntityOptional.isPresent()) {
                        InsuranceCompanyEntity insuranceCompanyEntity = insuranceCompanyEntityOptional.get();
                        requestMap.put("company_policy", insuranceCompanyEntity.getName());
                    }
                }
            }
        }

        requestMap.put("insurance_department", "ALD Automotive Italia " +
                "Insurance Department");
        requestOut.setData(requestMap);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy_MM_dd");
     //   String date = simpleDateFormat.format(new Date());
     //   requestOut.setFilename("PAI_" + claimsEntity.getPracticeId() + "_" + date + ".pdf");


        requestOut.setFilename("PAI_" + claimsEntity.getPracticeId() + "_" +  Instant.now().toEpochMilli() + ".pdf");

        requestOut.setTemplate("ALD-Claims-PAI");
        DogeRequestV1.FileManager fileManager = new DogeRequestV1.FileManager();
        String uuid = UUID.randomUUID().toString();
        fileManager.setUuid(uuid);
        fileManager.setResourceId("1");
        fileManager.setResourceType("claims");
        fileManager.setBlobType("pdf");
        requestOut.setFileManager(fileManager);

        DogeResponseV1.ActionResult response = callWithoutCert(dogeAPIEndpoint + dogeAPIEnqueue, HttpMethod.POST, requestOut, new HashMap<>(), DogeResponseV1.ActionResult.class);
        if (response.getCode() == 1) {
            response = new DogeResponseV1.ActionResult(response.getCode(), response.getMessage(), response.getDetails());
        }
        DogeResponseV1 dogeResponse = new DogeResponseV1();
        dogeResponse.setDocumentId(uuid);
        dogeResponse.setActionResult(response);
        return dogeResponse;
    }

    // TODO REFACTOR
    public DogeResponseV1 enqueueFrontespizio(String idClaims, String user) throws IOException {
        DogeRequestV1 requestOut = new DogeRequestV1();
        Map<String, Object> requestMap = new HashMap<>();
        Optional<ClaimsNewEntity> claimsEntityOptional = claimsNewRepository.findById(idClaims);

        if (!claimsEntityOptional.isPresent()) {
            throw new BadRequestException("Claims with id " + idClaims + "not found", MessageCode.CLAIMS_1010);
        }
        ClaimsEntity claimsEntity = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsEntityOptional.get());

        //UTENTE
        requestMap.put("printed_by", user);

        //FLUSSO
        requestMap.put("tipologia_gestione", claimsEntity.getType().getValue());

        //NUMERO PRATICA
        requestMap.put("sx_numero", claimsEntity.getPracticeId().toString());

        if (claimsEntity.getDamaged() != null) {
            Damaged damaged = claimsEntity.getDamaged();
            //TARGA
            if (damaged.getVehicle() != null) {
                String plate = damaged.getVehicle().getLicensePlate();
                requestMap.put("targa", plate);

                Boolean wreck = damaged.getVehicle().getWreck();
                if (wreck != null && wreck) {
                    requestMap.put("recuperabile", "SI");
                } else {
                    requestMap.put("recuperabile", "NO");
                }
            }

            //DATA SINISTRO
            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            formatter.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));
            if (claimsEntity.getComplaint() != null && claimsEntity.getComplaint().getDataAccident() != null && claimsEntity.getComplaint().getDataAccident().getDateAccident() != null) {
                requestMap.put("data_sinistro", formatter.format(claimsEntity.getComplaint().getDataAccident().getDateAccident()));
            }

            //POLIZZA
            if (damaged.getInsuranceCompany() != null && damaged.getInsuranceCompany().getTpl() != null) {

                if (damaged.getInsuranceCompany().getTpl().getInsurancePolicyId() != null) {
                    InsurancePolicyEntity insurancePolicyEntity = insurancePolicyRepository.getOne(damaged.getInsuranceCompany().getTpl().getInsurancePolicyId());
                    InsuranceManagerEntity insuranceManagerEntity = insurancePolicyEntity.getInsuranceManagerEntity();
                    if (insuranceManagerEntity != null) {
                        requestMap.put("gestore_sinistri", insuranceManagerEntity.getName());
                    }
                }
                //DATI COMPAGNIA ASSICURATIVA
                if (damaged.getInsuranceCompany().getTpl().getInsuranceCompanyId() != null) {
                    Optional<InsuranceCompanyEntity> insuranceCompanyEntityOptional = insuranceCompanyRepository.findById(damaged.getInsuranceCompany().getTpl().getInsuranceCompanyId());
                    if (insuranceCompanyEntityOptional.isPresent()) {
                        InsuranceCompanyEntity insuranceCompanyEntity = insuranceCompanyEntityOptional.get();
                        requestMap.put("compagnia_assicurativa", insuranceCompanyEntity.getName());
                    }
                }
            }

            //DATI CLIENTE
            if (damaged.getCustomer() != null) {
                Customer customer = damaged.getCustomer();
                requestMap.put("cliente", customer.getLegalName());
                if (customer.getMainAddress() != null) {
                    Address customerAddress = customer.getMainAddress();
                    requestMap.put("indirizzo_cliente", customerAddress.getFormattedAddress());
                }
            }

            //DATI VEICOLO
            if (damaged.getVehicle() != null) {
                Vehicle vehicle = damaged.getVehicle();
                requestMap.put("marca_modello", vehicle.getMake() + " " + vehicle.getModel());
            }

            //DATI DRIVER
            if (damaged.getDriver() != null) {
                Driver driver = damaged.getDriver();
                requestMap.put("conducente", driver.getLastname());
            }

        }

        if (claimsEntity.getComplaint() != null) {
            Complaint complaint = claimsEntity.getComplaint();
            //NUMERO SINISTRO
            if (complaint.getFromCompany() != null) {
                requestMap.put("num_sinistro_compagnia", complaint.getFromCompany().getNumberSx());
            }

            //LEGALE O ISPETTORATO
            if (complaint.getEntrusted() != null) {
                Entrusted entrusted = complaint.getEntrusted();
                if (entrusted.getEntrustedType().equals(EntrustedEnum.LEGAL)) {
                    requestMap.put("legale", entrusted.getEntrustedTo());
                } else if (entrusted.getEntrustedType().equals(EntrustedEnum.INSPECTORATE)) {
                    requestMap.put("ispettorato", entrusted.getEntrustedTo());
                }
            }

            //TIPOLOGIA SINISTRO
            if (complaint.getDataAccident() != null) {
                DataAccident dataAccident = complaint.getDataAccident();
                String typeAccident = dataAccident.getTypeAccident().getValue().replaceAll("_", " ");
                requestMap.put("tipologia_di_sinistro", typeAccident);
            }
        }
        //MANCANO DATI SUL SUPPLIER, DAMAGE FORM, CONTROPARTE

        requestMap.put("stato_del_sinistro", ClaimsAdapter.adptClaimsStatusEnumToItalian(claimsEntity.getStatus()));

        if(claimsEntity.getCounterparts() != null && !claimsEntity.getCounterparts().isEmpty()){
            requestMap.put("con_controparte", "SI");
            List<CounterpartyEntity> counterparties = claimsEntity.getCounterparts();
            int i = 1;
            for (CounterpartyEntity counterparty : counterparties) {
                String controparte = i + "controparte";
                if (counterparty.getType().equals(VehicleTypeEnum.VEHICLE)) {
                    requestMap.put(controparte, "Veicolo");
                } else {
                    requestMap.put(controparte, "Altro");
                }
                if (counterparty.getInsured() != null) {
                    String nomeControparte = "nome_" + i + "controparte";
                    requestMap.put(nomeControparte, counterparty.getInsured().getLastname());
                }
                if (counterparty.getInsuranceCompany() != null && counterparty.getInsuranceCompany().getEntity() != null) {
                    String compagniaControparte = "compagnia_" + i + "controparte";
                    requestMap.put(compagniaControparte, counterparty.getInsuranceCompany().getEntity().getName());
                }
                if (counterparty.getVehicle() != null) {
                    String targaControparte;
                    if (i == 2) {
                        targaControparte = "tarag_" + i + "controparte";
                    } else {
                        targaControparte = "targa_" + i + "controparte";
                    }

                    requestMap.put(targaControparte, counterparty.getVehicle().getLicensePlate());
                }
                i++;
            }
        }

        requestOut.setData(requestMap);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy_MM_dd");
        String date = simpleDateFormat.format(new Date());
        requestOut.setFilename("Frontespizio_" + claimsEntity.getPracticeId() + "_" + date + ".pdf");
        requestOut.setTemplate("ALD-Claims-Frontespizio");
        DogeRequestV1.FileManager fileManager = new DogeRequestV1.FileManager();
        String uuid = UUID.randomUUID().toString();
        fileManager.setUuid(uuid);
        fileManager.setResourceId("1");
        fileManager.setResourceType("claims");
        fileManager.setBlobType("pdf");
        requestOut.setFileManager(fileManager);

        DogeResponseV1.ActionResult response = callWithoutCert(dogeAPIEndpoint + dogeAPIEnqueue, HttpMethod.POST, requestOut, new HashMap<>(), DogeResponseV1.ActionResult.class);
        if (response.getCode() == 1) {
            response = new DogeResponseV1.ActionResult(response.getCode(), response.getMessage(), response.getDetails());
        }
        DogeResponseV1 dogeResponse = new DogeResponseV1();
        dogeResponse.setDocumentId(uuid);
        dogeResponse.setActionResult(response);
        return dogeResponse;
    }

    @Override
    public void exportClaims(ClaimsExportFilter filter, String nemoUserId, String authorization) {
        // calcola il numero di elementi da mostrare
        int pageSize = exportClaimsBlocksize;
        int exportCount = claimsExportHelper.getExportCount(filter);
        double pagesNumberDiv = (double) exportCount / (double) pageSize;
        int pagesNumber = (int) Math.ceil(pagesNumberDiv);
        LOGGER.info("[exportClaims] There are \"" + exportCount + "\" claims to export");
        // utente a cui verrà inviato il file, una volta completato
        String userEmail = claimsExportHelper.getUserEmail(nemoUserId, authorization);
        // export tramite thread
        new Thread(() -> {
            try {
                claimsAsynchExporter.export(filter, pagesNumber, exportCount, userEmail,nemoUserId);
            } catch (IOException e) {
                LOGGER.error("[exportClaims] Exception in enqueueExportClaimsPassingThroughFileManagerUsingKafka", e);
            }
        }).start();
    }

    @Override
    public void exportLogs(ClaimsExportFilter filter, String nemoUserId, String authorization) {
        // calcola il numero di elementi da mostrare
        int pageSize = exportClaimsBlocksize;
        int exportCount = claimsExportHelper.getExportCount(filter);
        double pagesNumberDiv = (double) exportCount / (double)pageSize;
        int pagesNumber = (int) Math.ceil(pagesNumberDiv);
        LOGGER.info("[exportClaims] There are \"" + exportCount + "\" claims to export");
        // utente a cui verrà inviato il file, una volta completato
        String userEmail = claimsExportHelper.getUserEmail(nemoUserId, authorization);
        new Thread(() -> {
            try {
                logsAsynchExporter.export(filter, pagesNumber, exportCount, userEmail,nemoUserId);
            } catch (IOException e) {
                LOGGER.error("[exportClaims] Exception in enqueueExportClaimsPassingThroughFileManagerUsingKafka", e);
            }
        }).start();
    }

    @Override
    public void exportCtp(CtpExportFilter filter, String nemoUserId, String authorization) throws InternalException {
        // calcola il numero di elementi da mostrare
        int pageSize = exportRepairBlocksize;
        int exportCount = ctpExportHelper.getExportCount(filter);
        double pagesNumberDiv = (double)exportCount / (double)pageSize;
        int pagesNumber = (int) Math.ceil(pagesNumberDiv);
        LOGGER.info("[exportRepair] There are \"" + exportCount + "\" repair to export");
        // utente a cui verrà inviato il file, una volta completato
        String userEmail = ctpExportHelper.getUserEmail(nemoUserId, authorization);
        new Thread(() -> {
            try {
                ctpAsynchExporter.export(filter, pagesNumber, exportCount, userEmail,nemoUserId);
            }
            catch (IOException e) {
                LOGGER.error("[exportRepair] Exception in exportCounterpartyPassingThroughFileManagerUsingKafka", e);
            }
        }).start();
    }

    private static Map<String, Object> inizializeMapExportPracticeCar() {
        Map<String,Object> innerMap;
        innerMap = new HashMap<>();

        innerMap.put("practice_id", "");
        innerMap.put("plate", "");
        innerMap.put("contract_type", "");
        innerMap.put("created_at", "");
        innerMap.put("status", "");
        innerMap.put("date_accident", "");
        innerMap.put("id_cliente", "");
        innerMap.put("denominazione_cliente", "");
        return innerMap;
    }

    private static Map<String, Object> inizializeMapHeaderExportPracticeCar() {
        Map<String,Object> innerMap;
        innerMap = new HashMap<>();

        innerMap.put("practice_id", "ID");
        innerMap.put("plate", "TARGA");
        innerMap.put("contract_type", "CONTRATTO");
        innerMap.put("created_at", "INSERITA IL");
        innerMap.put("status", "STATO");
        innerMap.put("date_accident", "DATA SINISTRO");
        innerMap.put("id_cliente", "ID CLIENTE");
        innerMap.put("denominazione_cliente", "DENOMINAZIONE CLIENTE");
        return innerMap;
    }

    private static Boolean isDriverInjury(List<Wounded> woundedList) {
        if(woundedList == null || woundedList.isEmpty()) return false;
        for(Wounded wounded : woundedList){
            if(wounded.getType().equals(WoundedTypeEnum.DRIVER))
                return true;
        }
        return false;
    }

    //ESTRAZIONE PRATICHE AUTO PERDITA DI POSSESSO
    public DogeResponseV1 enqueueExportPracticeCar(Long practiceIdFrom, Long practiceIdTo, String licensePlate, String contractId, String dateCreatedFrom, String dateCreatedTo, String status, String fleetVehicleId, Boolean asc, String orderBy) throws IOException {
        DogeRequestV1 requestOut = new DogeRequestV1();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));
        SimpleDateFormat simpleDateFormatHour = new SimpleDateFormat("HH:mm");
        simpleDateFormatHour.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));
        Map<String, Object> requestMap = new HashMap<>();
        LinkedList<Map<String, Object>> itemList = new LinkedList<>();
        Map<String,Object> innerMap;
        Map<String,Object> headerMap = inizializeMapHeaderExportPracticeCar();

        //RECUPERO PRATICHE PERDITA DI POSSESSO IN BASE AI FILTRI PASSATI
        List<PracticeEntity> practiceEntityList = practiceRepositoryV1.findPracticeToExport(practiceIdFrom, practiceIdTo, licensePlate, contractId, dateCreatedFrom, dateCreatedTo, status, fleetVehicleId, asc, orderBy);

        if(practiceEntityList != null && !practiceEntityList.isEmpty()){
            for(PracticeEntity practiceEntity : practiceEntityList){
                innerMap = inizializeMapExportPracticeCar();
                innerMap.put("practice_id", practiceEntity.getPracticeId().toString());
                if(practiceEntity.getVehicle() != null){
                    innerMap.put("plate", practiceEntity.getVehicle().getLicensePlate());
                }
                if(practiceEntity.getContract() != null){
                    innerMap.put("contract_type", practiceEntity.getContract().getContractType());
                }
                if(practiceEntity.getCreatedAt() != null){
                    Date createdAt = DateUtil.convertIS08601StringToUTCDate(DateUtil.convertUTCInstantToIS08601String(practiceEntity.getCreatedAt()));
                    innerMap.put("created_at", simpleDateFormat.format(createdAt)+" "+simpleDateFormatHour.format(createdAt));
                }
                innerMap.put("status", EnumAdapter.adptFromPracticeStatusToItalianString(practiceEntity.getStatus()));

                if(practiceEntity.getClaimsId() != null){
                    Optional<ClaimsNewEntity> claimsEntityOptional = claimsNewRepository.findById(practiceEntity.getClaimsId().toString());
                    if(claimsEntityOptional.isPresent()){
                        ClaimsEntity claimsEntity = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsEntityOptional.get());
                        Date dateAccident = claimsEntity.getComplaint().getDataAccident().getDateAccident();

                        innerMap.put("date_accident", simpleDateFormat.format(dateAccident)+" "+simpleDateFormatHour.format(dateAccident));
                    }
                }

                if(practiceEntity.getCustomer()!=null && practiceEntity.getCustomer().getCustomerId()!=null) {
                    innerMap.put("id_cliente", practiceEntity.getCustomer().getCustomerId());
                }

                if(practiceEntity.getCustomer()!=null && practiceEntity.getCustomer().getLegalName()!=null){
                    innerMap.put("denominazione_cliente", practiceEntity.getCustomer().getLegalName());
                }

                itemList.add(innerMap);
            }
        }

        requestMap.put("header", headerMap);
        requestMap.put("export_date", "Data: "+simpleDateFormat.format(DateUtil.getNowDate()));
        requestMap.put("items", itemList);
        requestOut.setData(requestMap);
        SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("yyyy_MM_dd");
        String date = simpleDateFormat2.format(new Date());
        requestOut.setFilename("Perdita_Possesso_" + date + ".xlsx");
        requestOut.setTemplate("PracticeCar");
        DogeRequestV1.FileManager fileManager = new DogeRequestV1.FileManager();
        String uuid = UUID.randomUUID().toString();
        fileManager.setUuid(uuid);
        fileManager.setResourceId("1");
        fileManager.setResourceType("claims");
        fileManager.setBlobType("pdf");
        requestOut.setFileManager(fileManager);

        DogeResponseV1.ActionResult response = callWithoutCert(dogeAPIEndpoint + dogeAPIEnqueue, HttpMethod.POST, requestOut, new HashMap<>(), DogeResponseV1.ActionResult.class);
        if (response.getCode() == 1) {
            response = new DogeResponseV1.ActionResult(response.getCode(), response.getMessage(), response.getDetails());
        }
        DogeResponseV1 dogeResponse = new DogeResponseV1();
        dogeResponse.setDocumentId(uuid);
        dogeResponse.setActionResult(response);
        return dogeResponse;
    }

    @Override
    public DogeResponseV1 enqueueFileTheftSummary(String id, String userName) throws IOException {
        DogeRequestV1 requestOut = new DogeRequestV1();
        //recupero nuova entità

        Optional<ClaimsNewEntity> claimsNewEntityOptional = claimsNewRepository.findById(id);
        if (!claimsNewEntityOptional.isPresent()) {
            throw new BadRequestException("Claims with id " + id + "not found.", MessageCode.CLAIMS_1010);
        }
        //converto nella vecchia entità
        ClaimsEntity claimsEntity = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntityOptional.get());

        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        formatter.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));

        Map<String, Object> requestInnerMap = new HashMap<>();
        requestInnerMap.put("id_pratica",claimsEntity.getPracticeId().toString());

        if(claimsEntity.getDamaged() != null ) {
            Damaged damaged = claimsEntity.getDamaged();
            if (damaged.getVehicle() != null) {
                if (damaged.getVehicle().getModel() != null) {
                    requestInnerMap.put("modello", damaged.getVehicle().getModel());
                }
                if (damaged.getVehicle().getLicensePlate() != null) {
                    requestInnerMap.put("targa", damaged.getVehicle().getLicensePlate());
                }
            }
            if(damaged.getCustomer() != null) {
                if(damaged.getCustomer().getLegalName() != null) {
                    requestInnerMap.put("società", damaged.getCustomer().getLegalName());
                }
                if(damaged.getCustomer().getPhonenr() != null){
                    requestInnerMap.put("telefono",damaged.getCustomer().getPhonenr());
                }
            }

            if(damaged.getDriver() != null){
                if(damaged.getDriver().getFirstname() != null && damaged.getDriver().getLastname() != null){
                    requestInnerMap.put("utilizz",damaged.getDriver().getFirstname() +" "+damaged.getDriver().getLastname() );
                }
                if(damaged.getDriver().getPhone() != null ){
                    requestInnerMap.put("tel_2",damaged.getDriver().getPhone() );
                }
            }
            if(damaged.getImpactPoint() != null && damaged.getImpactPoint().getIncidentDescription() != null){
                requestInnerMap.put("furto_dichiara",damaged.getImpactPoint().getIncidentDescription() );
            }

        }

        Complaint complaint = claimsEntity.getComplaint();
        if(complaint != null && complaint.getDataAccident() != null){
            DataAccident dataAccident =  complaint.getDataAccident();
            if(dataAccident.getDateAccident() != null){
                requestInnerMap.put("furto_data",formatter.format(dataAccident.getDateAccident()));
            }
            if(dataAccident.getAddress() != null && dataAccident.getAddress().getFormattedAddress() != null){
                requestInnerMap.put("furto_luogo",dataAccident.getAddress().getFormattedAddress());
            }

        }

        DataAccidentTypeAccidentEnum dataAccidentTypeAccidentEnum = null;
        if(complaint != null && complaint.getDataAccident() != null && complaint.getDataAccident().getTypeAccident() != null){
            dataAccidentTypeAccidentEnum = complaint.getDataAccident().getTypeAccident();
        }

        if(claimsEntity.getTheft() != null ){
            Theft theft = claimsEntity.getTheft() ;
            if(theft.getTheftNotes()!= null){
                requestInnerMap.put("note_furto", theft.getTheftNotes());
            }
            if(theft.getAuthorityTelephone() != null){
                requestInnerMap.put("furto_tel",theft.getAuthorityTelephone());
            }
            if(theft.getFindDate() != null){
                requestInnerMap.put("ritrovamento_data",formatter.format(theft.getFindDate()));
            }
            if(theft.getAddress() != null){
              if(theft.getAddress().getFormattedAddress() != null){
                  requestInnerMap.put("ritrovamento_veicolo",theft.getAddress().getFormattedAddress());
              }
              if(theft.getAddress().getLocality() != null){
                  requestInnerMap.put("ritrovamento_luogo",theft.getAddress().getLocality());
              }
            }

            if(theft.getFindAuthorityCc() != null && theft.getFindAuthorityCc() && theft.getAddress() != null && theft.getAddress().getLocality()!= null){
                requestInnerMap.put("ritrovamento_effettuato","CC - " +theft.getAddress().getLocality());
            }else if(theft.getFindAuthorityPolice() != null && theft.getFindAuthorityPolice() && theft.getAddress() != null && theft.getAddress().getLocality()!= null){
                requestInnerMap.put("ritrovamento_effettuato","PS - " +theft.getAddress().getLocality());
            }else if(theft.getFindAuthorityVvuu() != null && theft.getFindAuthorityVvuu() && theft.getAddress() != null && theft.getAddress().getLocality()!= null){
                requestInnerMap.put("ritrovamento_effettuato","VU - " +theft.getAddress().getLocality());
            }
            if(theft.getFindNotes() != null){
                requestInnerMap.put("note_ritrovamento",theft.getFindNotes());
            }
            if(theft.getFindAuthorityTelephone() != null){
                requestInnerMap.put("ritrovamento_tel",theft.getFindAuthorityTelephone());
            }
            if(theft.getOperationsCenterNotified() != null){
                    requestInnerMap.put("SI_centrale",theft.getOperationsCenterNotified());
                    requestInnerMap.put("NO_centrale",!theft.getOperationsCenterNotified());
            }
            if(theft.getSequestered() != null) {
                if(dataAccidentTypeAccidentEnum != null && dataAccidentTypeAccidentEnum.equals(DataAccidentTypeAccidentEnum.FURTO_E_RITROVAMENTO)){
                    requestInnerMap.put("SI_sequestro",theft.getSequestered());
                    requestInnerMap.put("NO_sequestro",!theft.getSequestered());
                }
                else {
                    requestInnerMap.put("SI_sequestro",false);
                    requestInnerMap.put("NO_sequestro",false);
                }
            }
            else {
                requestInnerMap.put("SI_sequestro",false);
                requestInnerMap.put("NO_sequestro",false);
            }
        }


        requestInnerMap.put("data_compilazione",formatter.format(DateUtil.getNowDate()));

        if(userName != null){
            requestInnerMap.put("compilatore",userName);
        }

        requestInnerMap.put("SI_antifurto",false);
        requestInnerMap.put("NO_antifurto",true);


        requestOut.setData(requestInnerMap);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy-HH:mm");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));
        requestOut.setFilename("Riepilogo_Furto_" + claimsEntity.getPracticeId()+ "_" + simpleDateFormat.format(DateUtil.getNowDate())+".pdf");
        requestOut.setTemplate("ALDModuloFurtoV2");
        DogeRequestV1.FileManager fileManager = new DogeRequestV1.FileManager();
        String uuid = UUID.randomUUID().toString();
        fileManager.setUuid(uuid);
        fileManager.setResourceId(claimsEntity.getId());
        fileManager.setResourceType("claims");
        fileManager.setBlobType("pdf");
        requestOut.setFileManager(fileManager);
        DogeResponseV1.ActionResult response = callWithoutCert(dogeAPIEndpoint + dogeAPIEnqueue, HttpMethod.POST, requestOut, new HashMap<>(), DogeResponseV1.ActionResult.class);
        if (response.getCode() == 1) {
            response = new DogeResponseV1.ActionResult(response.getCode(), response.getMessage(), response.getDetails());
        }
        DogeResponseV1 dogeResponse = new DogeResponseV1();
        dogeResponse.setDocumentId(uuid);
        dogeResponse.setActionResult(response);

        return dogeResponse;
    }

}
