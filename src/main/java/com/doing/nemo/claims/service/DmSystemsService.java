package com.doing.nemo.claims.service;

import com.doing.nemo.claims.entity.settings.DmSystemsEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public interface DmSystemsService {
    DmSystemsEntity saveDmSystems(DmSystemsEntity dmSystemsEntity);
    DmSystemsEntity updateDmSystems(DmSystemsEntity dmSystemsEntity);
    void deleteDmSystems(UUID uuid);
    DmSystemsEntity getDmSystems(UUID uuid);
    List<DmSystemsEntity> getAllDmSystems();
}
