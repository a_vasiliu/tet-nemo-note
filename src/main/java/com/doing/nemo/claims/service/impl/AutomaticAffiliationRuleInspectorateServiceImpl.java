package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.entity.settings.AutomaticAffiliationRuleInspectorateEntity;
import com.doing.nemo.claims.entity.settings.ClaimsTypeEntity;
import com.doing.nemo.claims.entity.settings.InsuranceCompanyEntity;
import com.doing.nemo.claims.repository.AutomaticAffiliationRuleInspectorateRepository;
import com.doing.nemo.claims.repository.ClaimsTypeRepository;
import com.doing.nemo.claims.repository.InsuranceCompanyRepository;
import com.doing.nemo.claims.service.AutomaticAffiliationRuleInspectorateService;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;


@Service
public class AutomaticAffiliationRuleInspectorateServiceImpl implements AutomaticAffiliationRuleInspectorateService {

    private static Logger LOGGER = LoggerFactory.getLogger(AutomaticAffiliationRuleInspectorateServiceImpl.class);
    @Autowired

    private InsuranceCompanyRepository insuranceCompanyRepository;
    @Autowired

    private AutomaticAffiliationRuleInspectorateRepository automaticAffiliationRuleInspectorateRepository;
    @Autowired

    private ClaimsTypeRepository claimsTypeRepository;

    @Override
    public List<AutomaticAffiliationRuleInspectorateEntity> updateRuleInspectorateList(List<AutomaticAffiliationRuleInspectorateEntity> automaticAffiliationRuleInspectorateEntityList) {
        Set<String> automaticAffiliationRuleSetForDuplicate = new HashSet<>();
        for (AutomaticAffiliationRuleInspectorateEntity automaticAffiliationRuleInspectorateEntity : automaticAffiliationRuleInspectorateEntityList) {
            if (!automaticAffiliationRuleSetForDuplicate.add(automaticAffiliationRuleInspectorateEntity.getSelectionName())) {
                LOGGER.debug("Error in Inspectorate Rule, duplicate name not admitted");
                throw new BadRequestException("Error in Inspectorate Rule, duplicate name not admitted", MessageCode.CLAIMS_1009);
            }
        }

        List<AutomaticAffiliationRuleInspectorateEntity> automaticAffiliationRuleInspectorateEntityList1 = new ArrayList<>();
        if (automaticAffiliationRuleInspectorateEntityList != null) {
            for (AutomaticAffiliationRuleInspectorateEntity automaticAffiliationRuleInspectorateEntity1 : automaticAffiliationRuleInspectorateEntityList) {
                //if (automaticAffiliationRuleInspectorateEntity1.getCurrentVolume() <= automaticAffiliationRuleInspectorateEntity1.getMonthlyVolume()) {
                    updateRuleInspectorate(automaticAffiliationRuleInspectorateEntity1);
                    automaticAffiliationRuleInspectorateEntityList1.add(automaticAffiliationRuleInspectorateEntity1);
               //}

            }
        }
        return automaticAffiliationRuleInspectorateEntityList1;
    }

    @Override
    public AutomaticAffiliationRuleInspectorateEntity updateRuleInspectorate(AutomaticAffiliationRuleInspectorateEntity automaticAffiliationRuleInspectorateEntity) {
        List<InsuranceCompanyEntity> insuranceCompanyEntityList = new ArrayList<>();
        if (automaticAffiliationRuleInspectorateEntity.getCounterParterInsuranceCompanyList() != null) {
            for (InsuranceCompanyEntity insuranceCompanyEntity : automaticAffiliationRuleInspectorateEntity.getCounterParterInsuranceCompanyList()) {
                Optional<InsuranceCompanyEntity> insuranceCompanyEntity1 = insuranceCompanyRepository.findById(insuranceCompanyEntity.getId());
                if (insuranceCompanyEntity1.isPresent()) {
                    insuranceCompanyEntity = insuranceCompanyEntity1.get();
                    insuranceCompanyRepository.save(insuranceCompanyEntity);
                    insuranceCompanyEntityList.add(insuranceCompanyEntity);
                }

            }
        }


        List<InsuranceCompanyEntity> insuranceCompanyEntityList2 = new ArrayList<>();
        if (automaticAffiliationRuleInspectorateEntity.getDamagedInsuranceCompanyList() != null) {
            for (InsuranceCompanyEntity insuranceCompanyEntity : automaticAffiliationRuleInspectorateEntity.getDamagedInsuranceCompanyList()) {
                Optional<InsuranceCompanyEntity> insuranceCompanyEntity1 = insuranceCompanyRepository.findById(insuranceCompanyEntity.getId());
                if (insuranceCompanyEntity1.isPresent()) {
                    insuranceCompanyEntity = insuranceCompanyEntity1.get();
                    insuranceCompanyRepository.save(insuranceCompanyEntity);
                    insuranceCompanyEntityList2.add(insuranceCompanyEntity);
                }

            }
        }

        List<ClaimsTypeEntity> claimsTypeEntityList = new ArrayList<>();
        if (automaticAffiliationRuleInspectorateEntity.getClaimsTypeEntityList() != null) {
            for (ClaimsTypeEntity claimsTypeEntity : automaticAffiliationRuleInspectorateEntity.getClaimsTypeEntityList()) {
                Optional<ClaimsTypeEntity> claimsTypeEntity1 = claimsTypeRepository.findById(claimsTypeEntity.getId());
                if (claimsTypeEntity1.isPresent()) {
                    claimsTypeEntity = claimsTypeEntity1.get();
                    claimsTypeRepository.save(claimsTypeEntity);
                    claimsTypeEntityList.add(claimsTypeEntity);
                }
            }
        }


        automaticAffiliationRuleInspectorateEntity.setCounterParterInsuranceCompanyList(insuranceCompanyEntityList);
        automaticAffiliationRuleInspectorateEntity.setDamagedInsuranceCompanyList(insuranceCompanyEntityList2);
        automaticAffiliationRuleInspectorateEntity.setClaimsTypeEntityList(claimsTypeEntityList);
        automaticAffiliationRuleInspectorateRepository.save(automaticAffiliationRuleInspectorateEntity);
        return automaticAffiliationRuleInspectorateEntity;
    }

    public AutomaticAffiliationRuleInspectorateEntity deleteRuleInspectorate(List<AutomaticAffiliationRuleInspectorateEntity> automaticAffiliationRuleInspectorateEntities) {
        if (automaticAffiliationRuleInspectorateEntities != null) {
            for (AutomaticAffiliationRuleInspectorateEntity automaticAffiliationRuleInspectorateEntity : automaticAffiliationRuleInspectorateEntities) {
                automaticAffiliationRuleInspectorateRepository.delete(automaticAffiliationRuleInspectorateEntity);
            }
        }
        return null;
    }

}
