package com.doing.nemo.claims.service;

import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.enumerated.ClaimsRepairEnum;
import com.doing.nemo.claims.entity.settings.RecipientTypeEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public interface RecipientTypeService {

    RecipientTypeEntity insertRecipientType(RecipientTypeEntity recipientTypeEntity);

    RecipientTypeEntity updateRecipientType(RecipientTypeEntity recipientTypeEntity, UUID uuid);

    RecipientTypeEntity getRecipientType(UUID uuid);

    List<RecipientTypeEntity> getAllRecipientType(ClaimsRepairEnum claimsRepairEnum);

    RecipientTypeEntity deleteRecipientType(UUID uuid);

    RecipientTypeEntity patchStatusRecipientType(UUID uuid);

    Pagination<RecipientTypeEntity> paginationRecipientType(int page, int pageSize, String claimsRepairEnum, String description, Boolean fixed, String email, String dbFieldEmail, Boolean isActive, String recipientType, String orderBy, Boolean asc);
}
