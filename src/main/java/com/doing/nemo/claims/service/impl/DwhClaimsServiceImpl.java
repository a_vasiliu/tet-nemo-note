package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.adapter.DwhClaimsDTOAdapter;
import com.doing.nemo.claims.adapter.DwhCounterpartyDTOAdapter;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.CounterpartyEntity;
import com.doing.nemo.claims.service.DwhClaimsService;
import com.doing.nemo.dwh.client.DwhClient;
import com.doing.nemo.dwh.client.exception.DwhEventBuildException;
import com.doing.nemo.dwh.client.payload.request.DwhEventRequestBuilder;
import com.doing.nemo.dwh.client.payload.request.EventTypeEnum;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DwhClaimsServiceImpl implements DwhClaimsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DwhClaimsServiceImpl.class);


    @Autowired
    private DwhClient dwhClient;

    @Override
    public void sendMessage(ClaimsEntity claimsEntity, EventTypeEnum eventTypeEnum) {


        try {

            dwhClient.postDwhEvent(new DwhEventRequestBuilder(DwhClaimsDTOAdapter.adptFromClaimsEntityToDwhClaimsDTO(claimsEntity), eventTypeEnum).build());

        } catch ( Exception e) {
            LOGGER.debug("[DEBUG] DWH error: "+ e.getMessage() );
            LOGGER.error(e.getMessage(), e);
        }
        /*try {
            EventTypeEnum eventType = ActivityOperation.CREATE == quotationActivityEntity.getOperation()
                    ? EventTypeEnum.CREATED
                    : EventTypeEnum.UPDATED;

            Object specificQuotationEntity = (QuotationType.CARBODY == quotationActivityEntity.getQuotation().getType())
                    ? carbodyQuotationRepository.findByQuotationId(quotationActivityEntity.getQuotation().getId())
                    : mechanicQuotationRepository.findByQuotationId(quotationActivityEntity.getQuotation().getId());

            Object dwhQuotationDTO = (QuotationType.CARBODY == quotationActivityEntity.getQuotation().getType())
                    ? new DwhCarbodyQuotationDTOAdapter().adaptCarbodyQuotationEntityToDTO((CarbodyQuotationEntity) specificQuotationEntity)
                    : new DwhMechanicQuotationDTOAdapter().adaptMechanicQuotationEntityToDTO((MechanicQuotationEntity) specificQuotationEntity);

            if (eventType == EventTypeEnum.CREATED) {
                dwhClient.postDwhEvent(new DwhEventRequestBuilder(new DwhQuotationDTOAdapter().adaptQuotationEntityToDTO(quotationActivityEntity.getQuotation()), eventType).build());
            }



            LOGGER.debug(String.format("START postDwhEventDetailQuotation '%s'", quotationActivityEntity.getQuotation().getId()));
            dwhClient.postDwhEvent(new DwhEventRequestBuilder(dwhQuotationDTO, eventType).build());
            LOGGER.debug(String.format("END postDwhEventDetailQuotation '%s'", quotationActivityEntity.getQuotation().getId()));

            LOGGER.debug(String.format("START postDwhEventDetailWorking '%s'", quotationActivityEntity.getQuotation().getWorking().getId()));
            dwhClient.postDwhEvent(new DwhEventRequestBuilder(new DwhWorkingDTOAdapter().adaptWorkingEntityToDTO(quotationActivityEntity.getQuotation().getWorking()), EventTypeEnum.UPDATED).build());
            LOGGER.debug(String.format("END postDwhEventDetailWorking '%s'", quotationActivityEntity.getQuotation().getWorking().getId()));

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }*/
    }

    @Override
    public void sendMessage(CounterpartyEntity counterparty, EventTypeEnum eventTypeEnum){
        try {

            dwhClient.postDwhEvent(new DwhEventRequestBuilder(DwhCounterpartyDTOAdapter.adptFromCounterpartyToDwhCounterpartyDTO(counterparty), eventTypeEnum).build());

        } catch (DwhEventBuildException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }
}
