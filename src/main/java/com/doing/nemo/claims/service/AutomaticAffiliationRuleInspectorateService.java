package com.doing.nemo.claims.service;

import com.doing.nemo.claims.entity.settings.AutomaticAffiliationRuleInspectorateEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface AutomaticAffiliationRuleInspectorateService {

    AutomaticAffiliationRuleInspectorateEntity updateRuleInspectorate(AutomaticAffiliationRuleInspectorateEntity automaticAffiliationRuleInspectorateEntity);

    List<AutomaticAffiliationRuleInspectorateEntity> updateRuleInspectorateList(List<AutomaticAffiliationRuleInspectorateEntity> automaticAffiliationRuleInspectorateEntityList);
}
