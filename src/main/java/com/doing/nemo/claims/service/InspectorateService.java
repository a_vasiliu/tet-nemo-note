package com.doing.nemo.claims.service;


import com.doing.nemo.claims.controller.payload.response.InspectorateResponseV1;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.enumerated.DamagedEnum.VehicleEnum.VehicleTypeEnum;
import com.doing.nemo.claims.entity.settings.AutomaticAffiliationRuleInspectorateEntity;
import com.doing.nemo.claims.entity.settings.InspectorateEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public interface InspectorateService {
    InspectorateEntity updateInspectorate(InspectorateEntity inspectorateEntity, UUID uuid);

    InspectorateEntity updateInspectorateRule(List<AutomaticAffiliationRuleInspectorateEntity> automaticAffiliationRuleInspectorateEntityList, InspectorateEntity inspectorateEntity);

    InspectorateEntity insertInspectorateRule(List<AutomaticAffiliationRuleInspectorateEntity> automaticAffiliationRuleInspectorateEntityList, UUID uuid);

    List<InspectorateEntity> selectAllInspectorate();

    InspectorateEntity selectInspectorate(UUID uuid);

    InspectorateResponseV1 deleteInspectorate(UUID uuid);

    InspectorateEntity updateInspectorateWithoutRule(InspectorateEntity inspectorateEntity);

    InspectorateEntity insertInspectorate(InspectorateEntity inspectorateEntity);

    InspectorateEntity updateStatus(UUID uuid);
    List<InspectorateEntity> findInspectoratesFiltered (Integer page, Integer pageSize, String orderBy, Boolean asc, String name, String email, String province, String telephone, String fax, Boolean isActive, String address, String zipCode, String city, String state, Long code);
    InspectorateEntity selectInspectorateWithMaxVolume(Boolean isNotCompanyCounterParty, Boolean isAbroad, Boolean isWounded, Boolean isRecoverability, UUID counterparty, DataAccidentTypeAccidentEnum typeClaim, VehicleTypeEnum counterpartType, UUID damaged, Double amount,Long practiceId);
    Long countInspectorateFiltered(String name, String email, String province, String telephone, String fax, Boolean isActive, String address, String zipCode, String city, String state, Long code);
    void resetRuleJob();
}
