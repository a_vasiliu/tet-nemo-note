package com.doing.nemo.claims.service;

import com.doing.nemo.claims.controller.payload.request.ClaimsTypeRequestV1;
import com.doing.nemo.claims.controller.payload.request.FlowContractUpdateRequestV1;
import com.doing.nemo.claims.controller.payload.response.ClaimsTypeResponseV1;
import com.doing.nemo.claims.controller.payload.response.ClaimsTypeWithFlowResponseV1;
import com.doing.nemo.claims.controller.payload.response.ContractTypeEntityResponseV1;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsFlowEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.settings.ContractTypeEntity;
import com.doing.nemo.claims.entity.settings.FlowContractTypeEntity;
import com.doing.nemo.claims.entity.settings.SettingsRequest;

import java.util.List;
import java.util.UUID;

public interface ContractService {

    void insertClaimType(List<ClaimsTypeRequestV1.Type> listClaimsType);

    UUID insertContract(SettingsRequest settingsRequest);

    ClaimsFlowEnum getFlowContractType(String contractType, DataAccidentTypeAccidentEnum claimsType);

    ClaimsFlowEnum getPersonalRiskFlowType(String contractType, DataAccidentTypeAccidentEnum claimsType, String customerId);

    List<ContractTypeEntityResponseV1> getListContractType(String contractCode);

    ContractTypeEntityResponseV1 getContractTypeById(String contractId);



    ContractTypeEntityResponseV1 updateFlowContractList(String contractId, FlowContractUpdateRequestV1 flowContractUpdateRequestV1);

    List<ClaimsTypeWithFlowResponseV1> getContractTypeByCodContract(String codContract);

    List<ClaimsTypeResponseV1> getListClaimsType();

    ContractTypeEntity getContractType(String contractType);

    ContractTypeEntity patchActive(UUID uuid);

    Pagination<ContractTypeEntity> paginationContractType(int page, int pageSize, String orderBy, Boolean asc, String codContractType, String description, Boolean flagWS, ClaimsFlowEnum defaultFlow, String ctrnote, Boolean ricaricar, Double franchise, Boolean isActive);

    FlowContractTypeEntity getFlowContractTypeEntity(String contractType, DataAccidentTypeAccidentEnum claimsType);

    String getTypeOfChargeBack(String contractType, DataAccidentTypeAccidentEnum claimsType, String customerId);
    }
