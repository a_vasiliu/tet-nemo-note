package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.CounterpartyEntity;
import com.doing.nemo.claims.entity.enumerated.DamagedEnum.VehicleEnum.VehicleNatureEnum;
import com.doing.nemo.claims.entity.enumerated.DamagedEnum.VehicleEnum.VehicleTypeEnum;
import com.doing.nemo.claims.entity.enumerated.DamagedEnum.WoundedEnum.WoundedTypeEnum;
import com.doing.nemo.claims.service.EligibilityCriterionChecker;
import com.doing.nemo.claims.service.GoLiveStrategyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EligibilityCriterionCheckerImpl implements EligibilityCriterionChecker {

    @Autowired
    private GoLiveStrategyService goLiveStrategyService;

    public Boolean isClaimsEligible(ClaimsEntity claims) {
        if( claims == null || claims.getCounterparts() == null || claims.getCounterparts().isEmpty() || !goLiveStrategyService.checkIfStepThree()) {
            return false;
        } else {
            CounterpartyEntity counterparty = claims.getCounterparts().get(0);
            return
           //     !claims.isMigrated() && // il sinistro non è migrato
                !counterparty.isAld() && // la controparte NON è ALD
                ( claims.isRCPassive() ) && // il tipo di sinistro è RC passivo
                claims.isInsuranceCompanySogessour() && // la compagnia assicuratrice del danneggiato sia sogessur
                claims.getWoundedByType(WoundedTypeEnum.OTHER) == null && // non ci sono feriti nella controparte
                counterparty.getType() != null && counterparty.getType().equals(VehicleTypeEnum.VEHICLE) && // la controparte è un veicolo
                (counterparty.isCounterPartyNatureByType(VehicleNatureEnum.MOTOR_VEHICLE) || counterparty.isCounterPartyNatureByType(VehicleNatureEnum.VAN)) && // la controparte è un'auto oppure un furgone
                !counterparty.isAlreadyEligible() && //la controparte non è già eleggibile
                !counterparty.isWebsinRepair() //la controparte non è stata già lavorata su websin repair
            ;
        }
    }

}
