package com.doing.nemo.claims.service;

import com.doing.nemo.claims.controller.payload.response.DogeResponseV1;
import com.doing.nemo.claims.dto.ClaimsExportFilter;
import com.doing.nemo.claims.dto.CtpExportFilter;
import org.springframework.stereotype.Service;
import java.io.IOException;

@Service
public interface DogeEnquService {

    // TODO REFACTOR
    DogeResponseV1 enqueueClaimsWithCounterpart(String idClaims) throws IOException;

    // TODO REFACTOR
    DogeResponseV1 enqueueClaimsWithoutCai(String idClaims) throws IOException;

    // TODO REFACTOR
    DogeResponseV1 enqueueClaimsWithoutCounterpart(String id) throws IOException;

    // TODO REFACTOR
    DogeResponseV1 enqueueClaimsPAI(String idClaims) throws IOException;

    // TODO REFACTOR
    DogeResponseV1 enqueueFrontespizio(String idClaims, String user) throws IOException;

    void exportClaims(ClaimsExportFilter filter, String nemoUserId, String authorization) throws IOException;

    void exportLogs(ClaimsExportFilter filter, String nemoUserId, String authorization);

    DogeResponseV1 enqueueExportPracticeCar(Long practiceIdFrom, Long practiceIdTo, String licensePlate, String contractId, String dateCreatedFrom, String dateCreatedTo, String status, String fleetVehicleId, Boolean asc, String orderBy) throws IOException;

    // TODO REFACTOR
    DogeResponseV1 enqueueFileTheftSummary(String id, String userName) throws IOException;

    void exportCtp(CtpExportFilter exportFilter, String nemoUserId, String authorization) throws IOException;

}
