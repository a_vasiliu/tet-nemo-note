package com.doing.nemo.claims.service;

import com.doing.nemo.claims.controller.payload.response.UploadFileResponseV1;
import com.doing.nemo.claims.entity.ClaimsEntity;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public interface CsvBuilderService {
    List<String> buildClaimsDashboardCSV(List<ClaimsEntity> claimsEntityList);
    UploadFileResponseV1 upldadCsv(List<String> csvRowList, String fileName, String description) throws IOException;
}