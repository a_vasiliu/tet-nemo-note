package com.doing.nemo.claims.service;

import com.doing.nemo.claims.controller.payload.response.ClaimsResponseMyAldPaginationV1;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.enumerated.myAld.MyAldInsertedByEnum;

import java.io.IOException;
import java.util.List;

public interface MyAldService {

    //REFACTOR
    ClaimsResponseMyAldPaginationV1 getPaginationFmMyAld(String contractPlateNumberSx,
                                                         List<String> clientIdList,
                                                         String dateAccidentFrom,
                                                         String dateAccidentTo,
                                                         List<String> statusList,
                                                         String dateCreatedFrom,
                                                         String dateCreatedTo,
                                                         List<MyAldInsertedByEnum> insertedByList,
                                                         Boolean asc,
                                                         String orderBy,
                                                         Integer page,
                                                         Integer pageSize,
                                                         String userId, String clientId);

    //REFACTOR
    ClaimsResponseMyAldPaginationV1 getPaginationDriverMyAld(
            List<String> plates,
            String contractPlateNumberSx,
            String dateAccidentFrom,
            String dateAccidentTo,
            List<String> statusList,
            String dateCreatedFrom,
            String dateCreatedTo,
            List<MyAldInsertedByEnum> insertedByList,
            Boolean asc,
            String orderBy,
            Integer page,
            Integer pageSize,
            String userId,
            Boolean isInclusive
    );

    List<String> getMyaldLink(ClaimsEntity claimsEntity) throws IOException;
}
