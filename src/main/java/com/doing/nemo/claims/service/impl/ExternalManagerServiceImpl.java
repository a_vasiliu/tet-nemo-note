package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.controller.payload.response.PaginationStats;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.ClaimsNewEntity;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.repository.ClaimsNewRepository;
import com.doing.nemo.claims.repository.ClaimsNewRepositoryImpl;
import com.doing.nemo.claims.repository.ClaimsRepository;
import com.doing.nemo.claims.repository.ClaimsRepositoryV1;
import com.doing.nemo.claims.service.AuthorityAttachmentService;
import com.doing.nemo.claims.service.ConverterClaimsService;
import com.doing.nemo.claims.service.ExternalManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigInteger;
import java.util.List;

@Service
public class ExternalManagerServiceImpl implements ExternalManagerService {

    @Autowired
    private ClaimsRepositoryV1 claimsRepositoryV1;
    @Autowired
    private ClaimsRepository claimsRepository;

    @Autowired
    private ClaimsNewRepository claimsNewRepository;

    @Autowired
    private ClaimsNewRepositoryImpl claimsNewRepositoryImpl;

    @Autowired
    private ConverterClaimsService converterClaimsService;

    @Autowired
    private AuthorityAttachmentService authorityAttachmentService;

    //REFACTOR
    @Override
    @Transactional
    public Pagination<ClaimsEntity> getAcclaimsClaimsEntity(Long practiceId, String plate, String dateAccident,  Boolean asc, String orderBy, int page, int pageSize) {

        //acclaims = a4f522b2-59fb-11e9-8647-d663bd873d93
        //acclaims sr = a4f526cc-59fb-11e9-8647-d663bd873d93
        Pagination<ClaimsEntity> claimsPagination = new Pagination<>();

        List<ClaimsNewEntity> getClaimsNewEntitities = claimsNewRepositoryImpl.getAcclaimsClaimsEntity(null, practiceId, null, null, plate, null, null, null, dateAccident, null, asc, orderBy, page, pageSize, null, null, null, null);
        List<ClaimsEntity> getClaimsEntitities = converterClaimsService.wrapFromClaimsNewEntityListToClaimsEntityList(getClaimsNewEntitities);

        BigInteger itemCount = claimsNewRepositoryImpl.countClaimsForAcclaimsPagination(null, practiceId, null, null, plate, null, null, null, dateAccident, null, asc, orderBy, page, pageSize, null, null, null, null);
                claimsPagination.setStats(new PaginationStats(itemCount.intValue(), page, pageSize));

        claimsPagination.setStats(new PaginationStats(itemCount.intValue(), page, pageSize));
        claimsPagination.setItems(getClaimsEntitities);

        return claimsPagination;

    }

    @Override
    public Pagination<ClaimsEntity> getMsaClaimsEntity(Long practiceId,  String plate, String dateAccident,  Boolean asc, String orderBy, int page, int pageSize) {

        Pagination<ClaimsEntity> claimsPagination = new Pagination<>();
        List<ClaimsNewEntity> getClaimsNewEntitities = claimsNewRepositoryImpl.getMsaClaimsEntity(null, practiceId, null, null, plate, null, null, null, dateAccident, null, asc, orderBy, page, pageSize, null, null, null, null);
        List<ClaimsEntity> getClaimsEntitities = converterClaimsService.wrapFromClaimsNewEntityListToClaimsEntityList(getClaimsNewEntitities);
        BigInteger itemCount = claimsNewRepositoryImpl.countClaimsForMsaPagination(null, practiceId, null, null, plate, null, null, null, dateAccident, null, asc, orderBy, page, pageSize, null, null, null, null);

        claimsPagination.setStats(new PaginationStats(itemCount.intValue(), page, pageSize));
        claimsPagination.setItems(getClaimsEntitities);

        return claimsPagination;

    }


}
