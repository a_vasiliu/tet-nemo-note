package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.adapter.*;
import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.*;
import com.doing.nemo.claims.controller.payload.request.authority.claims.AuthorityRequestV1;
import com.doing.nemo.claims.controller.payload.request.authority.claims.AuthorityTotalRequestV1;
import com.doing.nemo.claims.controller.payload.request.authority.claims.ClaimsAuthorityRequestV1;
import com.doing.nemo.claims.controller.payload.request.authority.claims.UserDetailsRequestV1;
import com.doing.nemo.claims.controller.payload.request.authority.practice.*;
import com.doing.nemo.claims.controller.payload.request.authority.repair.AuthorityRepairRequestV1;
import com.doing.nemo.claims.controller.payload.request.authority.repair.AuthorityRepairStatusRequestV1;
import com.doing.nemo.claims.controller.payload.request.authority.repair.AuthorityRepairTotalRequestV1;
import com.doing.nemo.claims.controller.payload.response.*;
import com.doing.nemo.claims.controller.payload.response.authority.*;
import com.doing.nemo.claims.controller.payload.response.authority.claims.*;
import com.doing.nemo.claims.controller.payload.response.authority.practice.AuthorityPracticeAssociationListResponseV1;
import com.doing.nemo.claims.controller.payload.response.authority.practice.AuthorityPracticeAssociationResponseV1;
import com.doing.nemo.claims.controller.payload.response.authority.practice.AuthorityPracticePaginationResponseV1;
import com.doing.nemo.claims.controller.payload.response.authority.practice.WorkingAdministrationResponseV1;
import com.doing.nemo.claims.controller.payload.response.authority.repair.AuthorityPaginationRepairResponseV1;
import com.doing.nemo.claims.entity.*;
import com.doing.nemo.claims.entity.claims.ClaimsAuthorityEntity;
import com.doing.nemo.claims.entity.enumerated.AuthorityEnum.*;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsFlowEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsHistoricalUserEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.doing.nemo.claims.entity.enumerated.EventTypeEnum;
import com.doing.nemo.claims.entity.enumerated.RepairEnum.RepairStatusEnum;
import com.doing.nemo.claims.entity.enumerated.WorkingStatusEnum;
import com.doing.nemo.claims.entity.jsonb.Authority;
import com.doing.nemo.claims.entity.jsonb.Historical;
import com.doing.nemo.claims.entity.jsonb.HistoricalAuthority;
import com.doing.nemo.claims.entity.jsonb.PODetails;
import com.doing.nemo.claims.entity.enumerated.ProcessingTypeEnum;
import com.doing.nemo.claims.entity.jsonb.*;
import com.doing.nemo.claims.entity.jsonb.complaint.Complaint;
import com.doing.nemo.claims.entity.jsonb.complaint.jsonbComplaint.jsonbDataAccidents.DataAccident;
import com.doing.nemo.claims.entity.jsonb.forms.Forms;
import com.doing.nemo.claims.entity.jsonb.forms.jsonbForms.Attachment;
import com.doing.nemo.claims.entity.jsonb.refund.Refund;
import com.doing.nemo.claims.entity.jsonb.repair.HistoricalCounterparty;
import com.doing.nemo.claims.entity.settings.PracticeEntity;
import com.doing.nemo.claims.repository.*;
import com.doing.nemo.claims.service.*;
import com.doing.nemo.claims.util.Util;
import com.doing.nemo.claims.validation.AuthorityErrorCode;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.AbstractException;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.exception.InternalException;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.commons.util.HttpUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.Response;
import org.apache.commons.collections4.MapUtils;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.math.BigInteger;
import java.util.*;

@Service
public class AuthorityServiceImpl implements AuthorityService {

    private static Logger LOGGER = LoggerFactory.getLogger(AuthorityServiceImpl.class);

    @Value("${authority.endpoint}")
    private String authorityEndpoint;

    @Value("${authority.api.vehicle.status}")
    private String authorityVehicleStatus;

    @Value("${authority.api.working.attachment}")
    private String authorityWorkingAttachment;

    @Value("${authority.api.repair.status}")
    private String authorityRepairStatus;

    @Value("${authority.api.administrative}")
    private String authorityAdministrative;

    @Value("${authority.api.administrative.association}")
    private String authorityAdministrativeAssociation;

    @Value("${nemo.user.id}")
    private String nemoUserId;

    @Value("${nemo.user.firstname}")
    private String nemoUserFirstname;

    @Value("${nemo.user.lastname}")
    private String nemoUserLastname;

    @Value("${nemo.profiles.tree}")
    private String nemoProfilesTree;

    @Autowired
    private HttpUtil httpUtil;

    @Autowired
    private ClaimsNewRepository claimsNewRepository;

    @Autowired
    private ClaimsAuthorityRepositorty claimsAuthorityRepositorty;

    @Autowired
    private CounterpartyNewRepository counterpartyNewRepository;

    @Autowired
    private CounterpartyNewRepositoryImpl counterpartyNewRepositoryImpl;

    @Autowired
    private ClaimsService claimsService;

    @Autowired
    private InvoicingService invocingService;

    @Autowired
    private IncidentService incidentService;

    @Autowired
    private IncidentAdapter incidentAdapter;

    @Autowired
    private CounterpartyService counterpartyService;

    @Autowired
    private InsurancePolicyRepository insurancePolicyRepository;

    @Autowired
    private AuthorityAdapter authorityAdapter;

    @Autowired
    private GoLiveStrategyService goLiveStrategyService;

    private  List<AuthorityEventTypeEnum> acceptableEventToPoInvoicePracticeList = new ArrayList(){{
        add(AuthorityEventTypeEnum.END_WORKING);
        add(AuthorityEventTypeEnum.RETURN_VEHICLE);
    }};

    @Autowired
    private DwhClaimsService dwhClaimsService;

    @Value("${dwh.call}")
    private Boolean dwhCall;

    @Autowired
    private PracticeService practiceService;

    @Autowired
    private PracticeRepositoryV1 practiceRepositoryV1;

    @Autowired
    private PracticeRepository practiceRepository;

    @Autowired
    private ObjectMapper mapper;


    @Autowired
    private ConverterClaimsService converterClaimsService;

    @Autowired
    private ClaimsAuthorityRepositoryV1 claimsAuthorityRepositoryV1;

    @Autowired
    private ExternalCommunicationService externalCommunicationService;

    private <T, P> T call(String url, HttpMethod method, P value, HashMap<String, String> headersparams, Class<T> outclass) {
        try {

            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            for (String k : headersparams.keySet()) {
                headers.add(k, headersparams.get(k));
            }

            HttpEntity entity = new HttpEntity(value, headers);

            return restTemplate.exchange(url, method, entity, outclass).getBody();
        } catch (Exception e) {
            LOGGER.debug(e.getMessage());
            throw e;
        }
    }

    private <T, P> T callWithoutCert(String url, HttpMethod method, P value, HashMap<String, String> headersparams, ParameterizedTypeReference<T> typeReference) throws InternalException, IOException {

        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = null;
        Response responseToken = null;
        try {
            if (value != null) {
                jsonInString = mapper.writeValueAsString(value);
            }

            responseToken = httpUtil.callURL(headersparams, url, jsonInString, method);

            AuthorityErrorCode.CheckResponseAndRaiseException(responseToken, true);

        /*
        if (!responseToken.isSuccessful() && responseToken.code() == HttpStatus.NOT_FOUND.value()) {
            LOGGER.debug(responseToken.message() + " with code " + responseToken.code());
            throw new NotFoundException(responseToken.message() + " with code " + responseToken.code(), MessageCode.CLAIMS_1101);
        } else if(!responseToken.isSuccessful()){
            LOGGER.debug(responseToken.message() + " with code " + responseToken.code());
            throw new InternalException(responseToken.message() + " with code " + responseToken.code(), MessageCode.CLAIMS_1103);
        }
        */

            if (typeReference == null)
                return null;

            String response = responseToken.body().string();
            LOGGER.debug("Response Authority Body: {}", response);
            TypeReference tr = new FileManagerServiceImpl.CustomTypeReference(typeReference);
            if(response == null || response.isEmpty()) return null;
            return mapper.readValue(response, tr);
        } catch (AbstractException e) {
            LOGGER.error(e.getMessage(), e);
            throw e;
        }  catch (Exception e) {
            String message = String.format(MessageCode.CLAIMS_2012.value(), e.getClass().getSimpleName(), e.getMessage());
            LOGGER.error(message, e);
            throw new InternalException(message, MessageCode.CLAIMS_2012);
        } finally {
            if (responseToken != null && responseToken.body() != null) {
                responseToken.body().close();
            }
        }
    }

    private Boolean checkOtherAuthority(List<Authority> authorityClaimList, Authority authority) {
        if (authorityClaimList == null || authorityClaimList.isEmpty()) return false;
        boolean isFound = false;
        Iterator<Authority> authorityIterator = authorityClaimList.iterator();
        while (authorityIterator.hasNext() && !isFound) {
            Authority authority1 = authorityIterator.next();
            if (!authority1.getAuthorityDossierId().equalsIgnoreCase(authority.getAuthorityDossierId()))
                isFound = true;
        }
        return isFound;
    }


    //REFACTOR
    @Override
    @Transactional
    public void linkUnlinkAuthority(AuthorityRequestV1 authorityRequestV1) {

        Authority authority = new Authority();
        authority.setAuthorityDossierId(authorityRequestV1.getAuthorityDossierId());
        authority.setAuthorityWorkingId(authorityRequestV1.getAuthorityWorkingId());
        authority.setType(authorityRequestV1.getType());
        authority.setCommodityDetails(CommodityDetailsAdapter.adptFromCommodityDetailsRequestToCommodityDetails(authorityRequestV1.getCommodityDetails()));


        List<String> claimsListId = new LinkedList<>();

        /* COLLEGO AUTHORITY AL CLAIMS */
        if (authorityRequestV1.getClaimsList() != null && !authorityRequestV1.getClaimsList().isEmpty()) {

            for (ClaimsAuthorityRequestV1 att : authorityRequestV1.getClaimsList()) {
                claimsListId.add(att.getId());
            }

            //REFACTOR

            List<ClaimsNewEntity> claimsNewAlreadyAssociated = claimsNewRepository.getClaimsAttachedToAuthorityPractice(authorityRequestV1.getAuthorityDossierId(), authorityRequestV1.getAuthorityWorkingId(), claimsListId);
            List<ClaimsNewEntity> claimsNewEntities = claimsNewRepository.getClaimsToAttachAuthority(authorityRequestV1.getAuthorityDossierId(), authorityRequestV1.getAuthorityWorkingId(), claimsListId);

            List<ClaimsEntity> claimsAlreadyAssociated = converterClaimsService.wrapFromClaimsNewEntityListToClaimsEntityList(claimsNewAlreadyAssociated);
            List<ClaimsEntity> claimsEntities = converterClaimsService.wrapFromClaimsNewEntityListToClaimsEntityList(claimsNewEntities);

            //Controllo che i sinistri già associati siano senza controparte
            if(claimsAlreadyAssociated != null && !claimsAlreadyAssociated.isEmpty()){
                if(claimsAlreadyAssociated.size() == 1){
                    ClaimsEntity c = claimsAlreadyAssociated.get(0);
                    if(c.getWithCounterparty() != null && c.getWithCounterparty()) {
                        if(!claimsEntities.isEmpty()) {
                            String message = String.format(MessageCode.CLAIMS_1106.value(), c.getId());
                            LOGGER.debug("[AUTHORITY] " + MessageCode.CLAIMS_1106.value());
                            throw new BadRequestException(message, MessageCode.CLAIMS_1106);
                        }
                    } else {
                        if(!c.getType().equals(ClaimsFlowEnum.FUL)){
                            if(!claimsEntities.isEmpty()) {
                                String message = String.format(MessageCode.CLAIMS_1108.value(), c.getId());
                                LOGGER.debug("[AUTHORITY] " +MessageCode.CLAIMS_1108.value());
                                throw new BadRequestException(message, MessageCode.CLAIMS_1108);
                            }
                        }
                    }
                }
            }

            if( (claimsEntities != null && claimsEntities.size() >1)  || (claimsAlreadyAssociated != null && !claimsAlreadyAssociated.isEmpty())){
                Iterator<ClaimsEntity> claimsEntityIterator = claimsEntities.iterator();
                while(claimsEntityIterator.hasNext()){
                    ClaimsEntity claimsEntity = claimsEntityIterator.next();
                    if(claimsEntity.getWithCounterparty() != null && claimsEntity.getWithCounterparty()) {
                        String message = String.format(MessageCode.CLAIMS_1107.value(), claimsEntity.getId());
                        LOGGER.debug("[AUTHORITY] " + MessageCode.CLAIMS_1107.value());
                        throw new BadRequestException(message, MessageCode.CLAIMS_1107);
                    } else {
                        if(!claimsEntity.getType().equals(ClaimsFlowEnum.FUL)){
                            String message = String.format(MessageCode.CLAIMS_1109.value(), claimsEntity.getId());
                            LOGGER.debug("[AUTHORITY] " +MessageCode.CLAIMS_1109.value());
                            throw new BadRequestException(message, MessageCode.CLAIMS_1109);
                        }
                    }
                }
            }

            //recupero i claims da associare
            //claimsEntities = claimsRepositoryV1.getClaimsToAttachAuthority(authorityRequestV1.getAuthorityDossierId(), authorityRequestV1.getAuthorityWorkingId(), claimsListId);

            //controllo se ci sono claims non associabili per stato, controparte o tipo
            for (ClaimsEntity currentClaim : claimsEntities) {

                if (currentClaim.getAuthorityLinkable() == null || !currentClaim.getAuthorityLinkable()) {
                    String message = String.format(MessageCode.CLAIMS_1100.value(), currentClaim.getId());
                    //claimsNotValid.addGenericError(currentClaim.getId(), "Claims with id " + currentClaim.getId() + " not associable because it's already associated and it's with counterparty");
                    LOGGER.debug("[AUTHORITY] " +MessageCode.CLAIMS_1100.value());
                    throw new BadRequestException(message, MessageCode.CLAIMS_1100);
                }

                if (currentClaim.getDamaged() != null && currentClaim.getDamaged().getVehicle() != null && currentClaim.getDamaged().getVehicle().getLicensePlate() != null) {

                    /************************** questo controllo è stato eliminato con la CO-643 - gestione delle reimmatricolazioni
                     Attenzione questo if (currentClaim.getDamaged().getVehicle().getLicensePlate().equalsIgnoreCase(authorityRequestV1.getPalte())) {*/

                    if (currentClaim.getType().equals(ClaimsFlowEnum.FNI) || currentClaim.getType().equals(ClaimsFlowEnum.FCM) ||
                            (currentClaim.getType().equals(ClaimsFlowEnum.FUL) && currentClaim.getWithCounterparty()!= null && currentClaim.getWithCounterparty())) {

                        LOGGER.debug("[AUTHORITY] :" +currentClaim.getType());
                        LOGGER.debug("[AUTHORITY] :" + currentClaim.getWithCounterparty());
                        List<Authority> authorityList = currentClaim.getAuthorities();

                        if (authorityList != null && !authorityList.isEmpty()) {
                                /*if (this.checkOtherAuthority(authorityList, authority)) {
                                    //claimsNotValid.addGenericError(currentClaim.getId(), "Claims with id " + currentClaim.getId() + " not associable because it's already associated and his status is type is " + currentClaim.getType());
                                    String message = String.format(MessageCode.CLAIMS_1085.value(), currentClaim.getId(), currentClaim.getType());
                                    LOGGER.debug(message);
                                    throw new BadRequestException(message, MessageCode.CLAIMS_1085);
                                }*/


                            for (Authority authority1 : authorityList) {
                                //se la pratica è della stessa tipologia all'interno dello stesso intervento scatta il controllo
                                if (authority1.getType().equals(authority.getType()) && authority1.getAuthorityDossierId().equalsIgnoreCase(authority.getAuthorityDossierId())) {
                                    String message = String.format(MessageCode.CLAIMS_1099.value(), currentClaim.getId(), authority.getType().getValue());
                                    LOGGER.debug("[AUTHORITY] working type: " + authority1.getType() );
                                    LOGGER.debug("[AUTHORITY] dossier id: " + authority1.getAuthorityDossierId() );
                                    LOGGER.debug("[AUTHORITY] " +message);
                                    throw new BadRequestException(message, MessageCode.CLAIMS_1099);
                                }
                            }
                        }
                    }

                    /************************** questo controllo è stato eliminato con la CO-643 - gestione delle reimmatricolazioni
                     } else {
                     //claimsNotValid.addPlateError(currentClaim.getId(), "Claims with id " + currentClaim.getId() + " is not associable because his plate does not match");
                     String message = String.format(MessageCode.CLAIMS_1097.value(), currentClaim.getId());
                     LOGGER.debug("[AUTHORITY] " +message);
                     throw new BadRequestException(message, MessageCode.CLAIMS_1097);
                     }*/
                } else {
                    //claimsNotValid.addPlateError(currentClaim.getId(), "Claims with id " + currentClaim.getId() + " is not associable because his plate is NULL");
                    String message = String.format(MessageCode.CLAIMS_1096.value(), currentClaim.getId());
                    LOGGER.debug("[AUTHORITY] " +message);
                    throw new BadRequestException(message, MessageCode.CLAIMS_1096);
                }

            }


            for (ClaimsEntity currentClaim : claimsEntities) {
                authority.setStatus(AuthorityStatusEnum.WAITING_FOR_AUTHORIZATION);
                authority.setWorkingNumber(authorityRequestV1.getWorkingNumber());
                authority.setAuthorityDossierNumber(authorityRequestV1.getAuthorityDossierNumber());
                List<Authority> authorityList = currentClaim.getAuthorities();
                if (authorityList == null)
                    authorityList = new ArrayList<>();

                authorityList.add(authority);
                currentClaim.setAuthorities(authorityList);
                Historical historical;
                if(authority.getType().equals(AuthorityTypeEnum.GLASS)){
                    historical = new Historical(EventTypeEnum.AUTHORITY_GLASS_ASSOCIATION, currentClaim.getStatus(), currentClaim.getStatus(), currentClaim.getComplaint().getDataAccident().getTypeAccident(), currentClaim.getComplaint().getDataAccident().getTypeAccident(), DateUtil.getNowDate(), null, ClaimsHistoricalUserEnum.SYSTEM_AUTHORITY.getValue(), currentClaim.getType().getValue(), currentClaim.getType().getValue(), "\n" +
                            "Autorizzazione collegata con id " + authorityRequestV1.getAuthorityDossierNumber());
                } else {
                    historical = new Historical(EventTypeEnum.AUTHORITY_ASSOCIATION, currentClaim.getStatus(), currentClaim.getStatus(), currentClaim.getComplaint().getDataAccident().getTypeAccident(), currentClaim.getComplaint().getDataAccident().getTypeAccident(), DateUtil.getNowDate(), null, ClaimsHistoricalUserEnum.SYSTEM_AUTHORITY.getValue(), currentClaim.getType().getValue(), currentClaim.getType().getValue(), "\n" +
                            "Autorizzazione collegata con dossier id " + authorityRequestV1.getAuthorityDossierNumber() + " e working id " + authorityRequestV1.getWorkingNumber());
                }
                if(currentClaim.getHistorical() == null ){
                    currentClaim.setHistorical(new ArrayList<>());
                }
                List<Historical> historicalList = currentClaim.getHistorical();
                historicalList.add(historical);
                currentClaim.setHistorical(historicalList);




                //conversione dalla vecchia alla nuova entità
                ClaimsNewEntity claimsNewEntity = converterClaimsService.convertOldEntityToNewClaimsEntity(currentClaim);
                claimsNewRepository.save(claimsNewEntity);

                if(dwhCall){
                    dwhClaimsService.sendMessage(currentClaim, com.doing.nemo.dwh.client.payload.request.EventTypeEnum.UPDATED);
                }
            }
        }else{
            //SCOLLEGO I CLAIMS
            //REFACTOR
            unlinkClaimsToAuthority(authority, authorityRequestV1, claimsListId);
        }


        //SETTAGGIO DI OLDEST, NOT DUPLICATE E DELLE FRANCHIGIE.
        List<AuthorityOldestResponseV1> responseV1s;
        // System.out.println(authorityRequestV1.getClaimsList());
        if ((authorityRequestV1.getClaimsList() != null && !authorityRequestV1.getClaimsList().isEmpty())) {
            responseV1s = new ArrayList<>();
            //REFACTOR
            List<ClaimsNewEntity> claimsNewEntityList = claimsNewRepository.getClaimsToCheckDuplicateAuthority(authorityRequestV1.getAuthorityDossierId(), authorityRequestV1.getAuthorityWorkingId());
            List<ClaimsEntity> claimsEntityList = converterClaimsService.wrapFromClaimsNewEntityListToClaimsEntityList(claimsNewEntityList);
            BigInteger count = claimsNewRepository.countClaimsToCheckDuplicateAuthority(authorityRequestV1.getAuthorityDossierId(), authorityRequestV1.getAuthorityWorkingId());

            if (count != null) {
                System.out.println(count);
                if (count.intValue() > 1) {
                    ClaimsEntity claimsOldest = claimsEntityList.get(0);
                    for (ClaimsEntity claimsEntity : claimsEntityList) {
                        if (claimsOldest.getPracticeId().compareTo(claimsEntity.getPracticeId()) > 0)
                            claimsOldest = claimsEntity;
                    }
                    for (ClaimsEntity claimsEntity : claimsEntityList) {
                        List<Authority> authorities = claimsEntity.getAuthorities();
                        if(authorities != null) {

                            Authority authorityToChange = getAuthoritySearched(authority, authorities);
                            if (authorityToChange==null){
                                String message = String.format(MessageCode.CLAIMS_1129.value(), claimsEntity.getId());
                                LOGGER.debug("[AUTHORITY] " +message);
                                throw new BadRequestException(message, MessageCode.CLAIMS_1129);
                            }
                            List<Authority> authoritiesOld = claimsEntity.getAuthorities();
                            authoritiesOld.remove(authorityToChange);
                            claimsEntity.setAuthorities(authoritiesOld);
                            AuthorityOldestResponseV1 authorityClaimsResponseV1 = new AuthorityOldestResponseV1();
                            authorityClaimsResponseV1.setId(claimsEntity.getId());
                            authorityClaimsResponseV1.setOldest(false);
                            authorityToChange.setOldest(false);
                            authorityToChange.setClaimsLinkedSize((long) claimsEntityList.size());
                            if (claimsOldest.getId().equalsIgnoreCase(claimsEntity.getId())) {
                                authorityToChange.setOldest(true);
                                authorityClaimsResponseV1.setOldest(true);
                            }
                            if(authorityRequestV1.getClaimsList() != null) {
                                authorityToChange = setNumberFranchiseAuthority(authorityToChange, claimsEntity.getId(), authorityRequestV1.getClaimsList());
                            }

                            authorityToChange.setNotDuplicate(false);
                            List<Authority> authoritiesList = claimsEntity.getAuthorities();
                            authoritiesList.add(authorityToChange);
                            claimsEntity.setAuthorities(authoritiesList);


                            //REFACTOR
                            ClaimsNewEntity claimsNewEntity = converterClaimsService.convertOldEntityToNewClaimsEntity(claimsEntity);
                            claimsNewRepository.save(claimsNewEntity);



                            if(dwhCall){
                                dwhClaimsService.sendMessage(claimsEntity, com.doing.nemo.dwh.client.payload.request.EventTypeEnum.UPDATED);
                            }
                            responseV1s.add(authorityClaimsResponseV1);
                        }
                    }
                }
                else if (count.intValue() == 1) {
                    ClaimsEntity claimsEntity = claimsEntityList.get(0);
                    List<Authority> authorities = claimsEntity.getAuthorities();
                    if(authorities != null) {

                        Authority authorityToChange = getAuthoritySearched(authority, authorities);

                        if (authorityToChange==null){
                            String message = String.format(MessageCode.CLAIMS_1129.value(), claimsEntity.getId());
                            LOGGER.debug("[AUTHORITY] " +message);
                            throw new BadRequestException(message, MessageCode.CLAIMS_1129);
                        }

                        List<Authority> authoritiesList = claimsEntity.getAuthorities();
                        authoritiesList.remove(authorityToChange);
                        claimsEntity.setAuthorities(authoritiesList);
                        AuthorityOldestResponseV1 authorityClaimsResponseV1 = new AuthorityOldestResponseV1();
                        authorityClaimsResponseV1.setId(claimsEntity.getId());
                        authorityClaimsResponseV1.setOldest(true);
                        authorityToChange.setNotDuplicate(true);
                        authorityToChange.setOldest(true);
                        authorityToChange.setClaimsLinkedSize(1L);

                        authorityToChange = setNumberFranchiseAuthority(authorityToChange, claimsEntity.getId(), authorityRequestV1.getClaimsList());

                        List<Authority> authorityList = claimsEntity.getAuthorities();
                        authorityList.add(authorityToChange);
                        claimsEntity.setAuthorities(authorityList);

                        //REFACTOR
                        ClaimsNewEntity claimsNewEntity = converterClaimsService.convertOldEntityToNewClaimsEntity(claimsEntity);
                        claimsNewRepository.save(claimsNewEntity);

                        if(dwhCall){
                            dwhClaimsService.sendMessage(claimsEntity, com.doing.nemo.dwh.client.payload.request.EventTypeEnum.UPDATED);
                        }
                        responseV1s.add(authorityClaimsResponseV1);
                    }
                }
            }
        }

        //claimsAuthorityRepositoryV1.deleteAuthorityOrphanal();
    }

    private Authority setNumberFranchiseAuthority(Authority authority, String claimsId, List<ClaimsAuthorityRequestV1> authorityRequestV1s){
        Iterator<ClaimsAuthorityRequestV1> iteratorFranchise = authorityRequestV1s.iterator();
        boolean isFoundFranchise = false;
        while (iteratorFranchise.hasNext() && !isFoundFranchise) {
            ClaimsAuthorityRequestV1 claimsAuthorityRequestV1 = iteratorFranchise.next();
            if (claimsAuthorityRequestV1.getId().equals(claimsId)) {
                isFoundFranchise = true;
                if (claimsAuthorityRequestV1.getFranchiseNumber() != null)
                    authority.setNumberFranchise(claimsAuthorityRequestV1.getFranchiseNumber());
            }
        }
        return authority;
    }



    private Authority getAuthoritySearched(Authority authority, List<Authority> authorities){
        Iterator<Authority> iterator = authorities.iterator();
        boolean isFound = false;
        Authority authorityToChange = new Authority();
        while (iterator.hasNext()) {
            authorityToChange = iterator.next();
            if (authorityToChange.getAuthorityDossierId().equalsIgnoreCase(authority.getAuthorityDossierId())&&
                    authorityToChange.getAuthorityWorkingId().equalsIgnoreCase(authority.getAuthorityWorkingId())) {
                isFound = true;
                break;
            }
        }
        if (isFound)  return authorityToChange;
        else return null;
    }

    //REFACTOR
    @Transactional
    public void unlinkClaimsToAuthority(Authority authority, AuthorityRequestV1 authorityRequestV1, List<String> claimsListId){

        List<ClaimsNewEntity> claimsNewEntities = claimsNewRepository.getClaimsToDetattachAuthority(authorityRequestV1.getAuthorityDossierId(), authorityRequestV1.getAuthorityWorkingId(), claimsListId);
        List<ClaimsEntity> claimsEntities = converterClaimsService.wrapFromClaimsNewEntityListToClaimsEntityList(claimsNewEntities);

        System.out.println("IN UNLINK "+ claimsEntities.size());
        if(claimsEntities != null) {
            for (ClaimsEntity currentClaim : claimsEntities) {
                if(currentClaim.getAuthorities() != null) {
                    Authority authorityToDelete = getAuthoritySearched(authority, currentClaim.getAuthorities());
                    if (authorityToDelete!=null){
                        List<ClaimsNewEntity> listaSxAssociati = claimsNewRepository.getClaimsToCheckDuplicateAuthority(authorityToDelete.getAuthorityDossierId(), authorityToDelete.getAuthorityWorkingId());

                        List<Authority> authorityList = currentClaim.getAuthorities();
                        authorityList.remove(authorityToDelete);
                        currentClaim.setAuthorities(authorityList);
                        Historical historical;
                        if (authorityToDelete.getType().equals(AuthorityTypeEnum.GLASS)) {
                            historical = new Historical(EventTypeEnum.AUTHORITY_GLASS_DISASSOCIATION, currentClaim.getStatus(), currentClaim.getStatus(), currentClaim.getComplaint().getDataAccident().getTypeAccident(), currentClaim.getComplaint().getDataAccident().getTypeAccident(), DateUtil.getNowDate(), null, ClaimsHistoricalUserEnum.SYSTEM_AUTHORITY.getValue(), currentClaim.getType().getValue(), currentClaim.getType().getValue(), "\n" +
                                    "Disassociazione collegata a id " + authorityToDelete.getAuthorityDossierNumber());
                        } else {
                            historical = new Historical(EventTypeEnum.AUTHORITY_DISASSOCIATION, currentClaim.getStatus(), currentClaim.getStatus(), currentClaim.getComplaint().getDataAccident().getTypeAccident(), currentClaim.getComplaint().getDataAccident().getTypeAccident(), DateUtil.getNowDate(), null, ClaimsHistoricalUserEnum.SYSTEM_AUTHORITY.getValue(), currentClaim.getType().getValue(), currentClaim.getType().getValue(), "\n" +
                                    "Disassociazione collegata con dossier id " + authorityToDelete.getAuthorityDossierNumber() + " e working id " + authorityToDelete.getWorkingNumber());
                        }
                        if (currentClaim.getHistorical() == null) {
                            currentClaim.setHistorical(new ArrayList<>());
                        }
                        List<Historical> historicalList = currentClaim.getHistorical();
                        historicalList.add(historical);
                        currentClaim.setHistorical(historicalList);


                        ClaimsNewEntity claimsNewEntity = converterClaimsService.convertOldEntityToNewClaimsEntity(currentClaim);
                        claimsNewRepository.save(claimsNewEntity);

                        if (!listaSxAssociati.isEmpty() && listaSxAssociati.size() == 1) {
                            // elimino le occorrenze dalla tabella Claims_Authority se esiste l'associazione con un solo sx
                            ClaimsAuthorityEntity claimsAuthority = claimsAuthorityRepositorty.searchByWorkingIdAuthorityDossierId(authorityToDelete.getAuthorityWorkingId(), authorityToDelete.getAuthorityDossierId());
                            claimsAuthorityRepositorty.delete(claimsAuthority);
                        }

                        if (dwhCall) {
                            dwhClaimsService.sendMessage(currentClaim, com.doing.nemo.dwh.client.payload.request.EventTypeEnum.UPDATED);
                        }
                    }
                }
            }
        }
    }

    /*public List<ClaimsResponseIdV1> checkClaims(String idClaims) {
        String[] claimsList = idClaims.split(",");
        List<ClaimsResponseIdV1> claimsResponseIdV1s = new ArrayList<>();
        for (String claims : claimsList) {
            Optional<ClaimsEntity> claimsEntityOptional = claimsRepository.findById(claims);
            if (claimsEntityOptional.isPresent()) {
                ClaimsEntity claimsEntity = claimsEntityOptional.get();
                if (claimsEntity.getAuthorityLinkable() != null && claimsEntity.getAuthorityLinkable() == true) {
                    claimsResponseIdV1s.add(new ClaimsResponseIdV1(claimsEntityOptional.get().getId()));
                }
            }
        }
        return claimsResponseIdV1s;
    }*/

    //REFACTOR
    @Transactional
    public Map<String, String> associateClaims(AuthorityTotalRequestV1 authorityTotalRequestV1) {
        Map<String, String> claimsNotValid = new HashMap<>();
        if (authorityTotalRequestV1 != null && authorityTotalRequestV1.getClaimsList() != null) {

            if (authorityTotalRequestV1.getClaimsList().isEmpty()) {
                LOGGER.debug(MessageCode.CLAIMS_1038.value());
                throw new BadRequestException(MessageCode.CLAIMS_1038);
            }
            List<String> claimsIdList = new ArrayList<>();
            for (ClaimsAuthorityRequestV1 claimsAuthorityRequestV1 : authorityTotalRequestV1.getClaimsList()) {
                claimsIdList.add(claimsAuthorityRequestV1.getId());
            }
            //recupero della nuova entità
            List<ClaimsNewEntity> claimsNewEntities = claimsNewRepository.getClaimsToCheckDuplicateAuthority(authorityTotalRequestV1.getAuthorityDossierId(), authorityTotalRequestV1.getAuthorityWorkingId());
            List<ClaimsEntity> claimsEntities= converterClaimsService.wrapFromClaimsNewEntityListToClaimsEntityList(claimsNewEntities);

            if (claimsEntities == null || claimsEntities.isEmpty()) {
                LOGGER.debug(MessageCode.CLAIMS_1039.value());
                throw new NotFoundException(MessageCode.CLAIMS_1039);
            }
            if ((claimsEntities.size() != authorityTotalRequestV1.getClaimsList().size())) {
                LOGGER.debug(MessageCode.CLAIMS_1086.value());
                throw new BadRequestException(MessageCode.CLAIMS_1086);

            } else {
                Long totalMilesFranchise = null;
                ClaimsEntity oldest = null;
                ClaimsStatusEnum oldStatus = null;
                IncidentRequestV1 oldIncident = null;
                ClaimsEntity oldClaims = new ClaimsEntity();
                for (ClaimsEntity claimsEntity : claimsEntities) {
                    Double poSum = 0.0;

                    List<Authority> authorities = claimsEntity.getAuthorities();
                    Refund refund = claimsEntity.getRefund();
                    if(refund == null)
                        refund = new Refund();
                    Authority currentAuthority = null;
                    if (authorities.size() > 1) {
                        Authority authority = new Authority();
                        authority.setAuthorityDossierId(authorityTotalRequestV1.getAuthorityDossierId());
                        authority.setAuthorityWorkingId(authorityTotalRequestV1.getAuthorityWorkingId());

                        currentAuthority = getAuthoritySearched(authority, authorities);
                        if (currentAuthority==null){
                            String message = String.format(MessageCode.CLAIMS_1129.value(), claimsEntity.getId());
                            LOGGER.debug("[AUTHORITY] " +message);
                            throw new BadRequestException(message, MessageCode.CLAIMS_1129);
                        }
                        authorities.remove(currentAuthority);
                        for (Authority currentAuthority1 : authorities) {
                            if(currentAuthority1.getTotal() != null)
                                poSum += currentAuthority1.getTotal();
                        }
                    }
                    else if(!authorities.isEmpty()){
                        currentAuthority = authorities.get(0);
                        authorities.remove(currentAuthority);
                    }

                    if(currentAuthority != null) {
                        //currentAuthority = addHistorical(currentAuthority, authorityTotalRequestV1);
                        if (currentAuthority.getOldest()) {
                            oldest = claimsEntity;
                            oldStatus = oldest.getStatus();
                            oldClaims = new ClaimsEntity();
                            oldClaims.setStatus(oldest.getStatus());
                            oldClaims.setType(oldest.getType());
                            oldClaims.setComplaint(new Complaint());
                            oldClaims.getComplaint().setDataAccident(new DataAccident());
                            oldClaims.getComplaint().getDataAccident().setTypeAccident(oldest.getComplaint().getDataAccident().getTypeAccident());
                            oldIncident = incidentAdapter.adptFromClaimsEntityToIncidentRequest(oldest, oldStatus, false,false);
                        }

                        currentAuthority = addHistorical(currentAuthority, authorityTotalRequestV1);
                        currentAuthority = setAllAuthorityFieldFromApprovingMethod(currentAuthority, authorityTotalRequestV1);

                        if (currentAuthority.getNbv() != null) {
                            refund.setNBV(currentAuthority.getNbv().toString());
                        }
                        if (currentAuthority.getWreckValue() != null && !currentAuthority.getWreckValue().equals("")) {
                            refund.setWreckValuePre(Double.parseDouble(currentAuthority.getWreckValue()));
                        }

                        if(currentAuthority.getWreck() != null){
                            claimsEntity.getDamaged().getVehicle().setWreck(currentAuthority.getWreck());
                            if(currentAuthority.getWreck()){
                                claimsEntity.getDamaged().getVehicle().setWreckDate(DateUtil.getNowDate());
                            }
                        }

                        authorities.add(currentAuthority);


                        /*for(Authority authority : authorities) {
                            if (authority.getEventType() != null && authority.getEventType().equals(AuthorityEventTypeEnum.END_WORKING)) {
                                if (authority.getNumberFranchise() != null) {
                                    if (totalMilesFranchise == null)
                                        totalMilesFranchise = authority.getNumberFranchise();
                                    else
                                        totalMilesFranchise += authority.getNumberFranchise();
                                }
                            }
                        }*/

                        refund.setWreck(currentAuthority.getWreck());
                        poSum += currentAuthority.getTotal();
                        Double poSumFinal = null;
                        if(poSum != null) {
                            poSumFinal =  Double.parseDouble(String.format("%.2f", poSum).replace(",", "."));
                        };
                        refund.setPoSum(poSumFinal);

                        refund = claimsService.setRefundFranchiseAmountFcm(claimsEntity,refund);
                    }

                    claimsEntity.setRefund(refund);
                    claimsEntity.setAuthorities(authorities);
                    ClaimsStatusEnum nextStatus = claimsEntity.getStatus();
                    /*if (ClaimsServiceImpl.isAuthorityAndSxNumber(claimsEntity)) {
                        ClaimsStatusEnum statusOld = claimsEntity.getStatus();
                        if (claimsEntity.getType().equals(ClaimsFlowEnum.FNI)) {
                            nextStatus = ClaimsStatusEnum.TO_SEND_TO_CUSTOMER;
                        } else {
                            nextStatus = ClaimsStatusEnum.TO_ENTRUST;
                        }
                        claimsEntity.setStatus(nextStatus);

                        String description = "Lo stato è cambiato da  " + ClaimsAdapter.adptClaimsStatusEnumToItalian(statusOld) + " a " + ClaimsAdapter.adptClaimsStatusEnumToItalian(claimsEntity.getStatus());
                        Historical historical = new Historical(EventTypeEnum.EDIT_PRACTICE_DATA, statusOld, claimsEntity.getStatus(), claimsEntity.getComplaint().getDataAccident().getTypeAccident(), claimsEntity.getComplaint().getDataAccident().getTypeAccident(), DateUtil.getNowDate(), null, null, claimsEntity.getType().getValue(), claimsEntity.getType().getValue(), description);
                        claimsEntity.addHistorical(historical);
                    }*/


                    if(claimsEntity.getStatus().equals(ClaimsStatusEnum.WAITING_FOR_AUTHORITY) && ClaimsServiceImpl.isAuthorityAndSxNumber(claimsEntity)) {
                        if ( claimsEntity.getType().equals(ClaimsFlowEnum.FNI)) {
                            nextStatus = ClaimsStatusEnum.TO_SEND_TO_CUSTOMER;
                        } else {
                            nextStatus = ClaimsStatusEnum.TO_ENTRUST;
                        }

                        LOGGER.debug("[AUTHORITY] approved call - rejected Event - change status in " + nextStatus);
                        String description = "Lo stato è cambiato da  " + ClaimsAdapter.adptClaimsStatusEnumToItalian(claimsEntity.getStatus()) + " a " + ClaimsAdapter.adptClaimsStatusEnumToItalian(nextStatus);
                        Historical historical = new Historical(EventTypeEnum.EDIT_PRACTICE_DATA, claimsEntity.getStatus(), nextStatus, claimsEntity.getComplaint().getDataAccident().getTypeAccident(), claimsEntity.getComplaint().getDataAccident().getTypeAccident(), DateUtil.getNowDate(), null, ClaimsHistoricalUserEnum.SYSTEM_AUTHORITY.getValue(), claimsEntity.getType().getValue(), claimsEntity.getType().getValue(), description);
                        claimsEntity.addHistorical(historical);
                        claimsEntity.setStatus(nextStatus);
                    }

                    ClaimsNewEntity claimsNewEntity = converterClaimsService.convertOldEntityToNewClaimsEntity(claimsEntity);
                    claimsNewRepository.save(claimsNewEntity);

                    if(dwhCall){
                        dwhClaimsService.sendMessage(claimsEntity, com.doing.nemo.dwh.client.payload.request.EventTypeEnum.UPDATED);
                    }
                }

                if(oldest != null) {

                    for (Authority authority : oldest.getAuthorities()) {
                        if(authority.getOldest() != null && authority.getOldest()){
                            //vengono conteggiate solo le penali di autorizzazioni il cui sinistro e oldest
                            List<ClaimsNewEntity> claimsNewEntityList = claimsNewRepository.getClaimsToCheckDuplicateAuthority(authority.getAuthorityDossierId(), authority.getAuthorityWorkingId());
                            //funzione di conversione
                            List<ClaimsEntity> claimsEntityList = converterClaimsService.wrapFromClaimsNewEntityListToClaimsEntityList(claimsNewEntityList);

                            //vengono recuperati i sinistri collegati all'autorizzazione di cui il sinistro è oldest compreso se stesso
                            for (ClaimsEntity currentClaims : claimsEntityList) {
                                Authority authority1 = getAuthoritySearched(authority, currentClaims.getAuthorities());
                                if (authority1==null){
                                    String message = String.format(MessageCode.CLAIMS_1129.value(), currentClaims.getId());
                                    LOGGER.debug("[AUTHORITY] " +message);
                                    throw new BadRequestException(message, MessageCode.CLAIMS_1129);
                                }

                                //vengono recuperate solo le penali delle pratiche che sono end working oppure glass autorizzate
                                if ((authority1.getEventType() != null && authority1.getEventType().equals(AuthorityEventTypeEnum.END_WORKING)) || (WorkingStatusEnum.AUTHORIZED == authority1.getWorkingStatus() && AuthorityTypeEnum.GLASS == authority1.getType())) {
                                    if (totalMilesFranchise == null)
                                        totalMilesFranchise = authority1.getNumberFranchise();
                                    else
                                        totalMilesFranchise += authority1.getNumberFranchise();
                                }
                            }
                        }
                    }

                    if (totalMilesFranchise != null) {
                        oldest.setTotalPenalty(totalMilesFranchise);
                        ClaimsNewEntity oldestNewEntity = converterClaimsService.convertOldEntityToNewClaimsEntity(oldest);
                        claimsNewRepository.save(oldestNewEntity);
                        if(dwhCall){
                            dwhClaimsService.sendMessage(oldest, com.doing.nemo.dwh.client.payload.request.EventTypeEnum.UPDATED);
                        }

                        //Intervento 6 - C
                        try {
                            LOGGER.debug("oldStatus " + oldStatus);
                            LOGGER.debug("oldIncident " + oldIncident);
                            externalCommunicationService.modifyIncidentSync(oldest.getId(), oldStatus, true, oldIncident,false, oldClaims);
                        } catch (IOException e) {
                            throw new BadRequestException(MessageCode.CLAIMS_1112);
                        }

                    }
                }
            }
        }
        return claimsNotValid;
    }

    private Authority setAllAuthorityFieldFromApprovingMethod(Authority authority, AuthorityTotalRequestV1 authorityTotalRequestV1){
        if (authorityTotalRequestV1.getTotal() != null)
            authority.setTotal(authorityTotalRequestV1.getTotal());
        if (authorityTotalRequestV1.getWorkingNumber() != null)
            authority.setWorkingNumber(authorityTotalRequestV1.getWorkingNumber());
        if (Util.isNotEmpty(authorityTotalRequestV1.getAcceptingDate()))
            if(authorityTotalRequestV1.getAuthorityDossierNumber() != null)
                authority.setAuthorityDossierNumber(authorityTotalRequestV1.getAuthorityDossierNumber());
        if(authorityTotalRequestV1.getWorkingStatus() != null)
            authority.setWorkingStatus(authorityTotalRequestV1.getWorkingStatus());
        if(authorityTotalRequestV1.getUserDetails() != null)
            authority.setUserDetails(UserDetailsAdapter.adptFromUserDetailsRequestToUserDetails(authorityTotalRequestV1.getUserDetails()));
        if(Util.isNotEmpty(authorityTotalRequestV1.getAcceptingDate()))
            authority.setAcceptingDate(DateUtil.convertIS08601StringToUTCDate(authorityTotalRequestV1.getAcceptingDate()));
        if (Util.isNotEmpty(authorityTotalRequestV1.getAuthorizationDate())) {
            authority.setAuthorizationDate(DateUtil.convertIS08601StringToUTCDate(authorityTotalRequestV1.getAuthorizationDate()));
            authority.setStatus(AuthorityStatusEnum.AUTHORIZED);
        } else if (Util.isNotEmpty(authorityTotalRequestV1.getRejectionDate())) {
            authority.setRejectionDate(DateUtil.convertIS08601StringToUTCDate(authorityTotalRequestV1.getRejectionDate()));
            authority.setStatus(AuthorityStatusEnum.UNAUTHORIZED);
        }
        if (authorityTotalRequestV1.getRejection() != null)
            authority.setRejection(authorityTotalRequestV1.getRejection());

        if(Util.isNotEmpty(authorityTotalRequestV1.getNoteRejection())){
            authority.setNoteRejection(authorityTotalRequestV1.getNoteRejection());
        }

        if (Util.isNotEmpty(authorityTotalRequestV1.getWorkingCreatedAt()))
            authority.setWorkingCreatedAt(DateUtil.convertIS08601StringToUTCDate(authorityTotalRequestV1.getWorkingCreatedAt()));
        if (authorityTotalRequestV1.getWreck() != null) {
            authority.setWreck(authorityTotalRequestV1.getWreck());
        }
        if (authorityTotalRequestV1.getWreckCasual() != null)
            authority.setWreckCasual(authorityTotalRequestV1.getWreckCasual());
        if (Util.isNotEmpty(authorityTotalRequestV1.getEventDate()))
            authority.setEventDate(DateUtil.convertIS08601StringToUTCDate(authorityTotalRequestV1.getEventDate()));
        if (authorityTotalRequestV1.getEventType() != null) {
            authority.setEventType(authorityTotalRequestV1.getEventType());
        }
        if (authorityTotalRequestV1.getPoDetails() != null) {
            authority.setPoDetails(PODetailsAdapter.adptFromPODetailsRequestToPODetails(authorityTotalRequestV1.getPoDetails()));
            authority.setInvoiced(false);
        }if(authorityTotalRequestV1.getNbv() != null)
            authority.setNbv(authorityTotalRequestV1.getNbv());
        if(authorityTotalRequestV1.getWreckValue() != null && !authorityTotalRequestV1.getWreckValue().equals("")) {
            authority.setWreckValue(authorityTotalRequestV1.getWreckValue());

        }
        return authority;
    }


    //REFACTOR
    @Override
    public Pagination<AuthorityPaginationResponseV1> paginationAuthorityResponseListFirstCheck(int page, int pageSize, String orderBy, Boolean asc, List<String> plates, String chassis) {
        Pagination<ClaimsEntity> claimsPagination = claimsService.getClaimsPaginationAuthority(plates, chassis, page, pageSize, true);
        List<ClaimsEntity> claimsEntityList = claimsPagination.getItems();
        Map<String, List<String>> claimsWithLinkedAssociation = this.authorityWithLinkedAssociation(claimsEntityList);
        return authorityAdapter.adptFromPaginationClaimsToPaginationAuthority(claimsPagination, claimsWithLinkedAssociation,true);
    }

    @Override
    public Pagination<AuthorityPaginationResponseV1> paginationAuthorityResponseListSecondCheck(int page, int pageSize, String orderBy, Boolean asc, List<String> plates, String chassis) {
        Pagination<ClaimsEntity> claimsPagination = claimsService.getClaimsPaginationAuthority(plates, chassis, page, pageSize, false);
        Map<String, List<String>> claimsWithLinkedAssociation = this.authorityWithLinkedAssociation(claimsPagination.getItems());
        return authorityAdapter.adptFromPaginationClaimsToPaginationAuthority(claimsPagination, claimsWithLinkedAssociation,false);
    }

    private Map<String,List<String>> authorityWithLinkedAssociation (List<ClaimsEntity> claimsEntities) {
        Map<String, List<String>> claimsWithLinkedAssociation = new HashMap<>();
        if(!CollectionUtils.isEmpty(claimsEntities)){
            for (ClaimsEntity claimsEntity : claimsEntities) {
                List<String> authorityLinked = new ArrayList<>();
                if (claimsEntity.getAuthorities() != null) {
                    for (Authority authority : claimsEntity.getAuthorities()) {
                        authorityLinked.add(authority.getAuthorityDossierId());
                    }
                }
                if (authorityLinked.isEmpty())
                    claimsWithLinkedAssociation.put(claimsEntity.getId(), null);
                else
                    claimsWithLinkedAssociation.put(claimsEntity.getId(), authorityLinked);
            }
        }
        return claimsWithLinkedAssociation;
    }



    //REFACTOR
    @Override
    public List<AuthorityOldestResponseV1> getOldestClaimsByDossierAndWorking(String dossierId, String workingId) {
        //recupero nuova entità
        List<ClaimsNewEntity> claimsNewEntityList = claimsNewRepository.getClaimsToCheckDuplicateAuthority(dossierId, workingId);
        //conversione alla vecchia entità
        List<ClaimsEntity> claimsEntityList = converterClaimsService.wrapFromClaimsNewEntityListToClaimsEntityList(claimsNewEntityList);


        List<AuthorityOldestResponseV1> authorityClaimsResponseV1s = null;
        Authority authority = new Authority();
        authority.setAuthorityDossierId(dossierId);
        authority.setAuthorityWorkingId(workingId);
        if (claimsEntityList != null && !claimsEntityList.isEmpty()) {
            authorityClaimsResponseV1s = new ArrayList<>();
            for (ClaimsEntity claimsEntity : claimsEntityList) {
                AuthorityOldestResponseV1 authorityClaimsResponseV1 = new AuthorityOldestResponseV1();
                authorityClaimsResponseV1.setId(claimsEntity.getId());
                authorityClaimsResponseV1.setFlowType(claimsEntity.getType());
                authorityClaimsResponseV1.setPracticeId(claimsEntity.getPracticeId());
                authorityClaimsResponseV1.setCreatedAt(DateUtil.convertUTCInstantToIS08601String(claimsEntity.getCreatedAt()));
                authorityClaimsResponseV1.setClaimType(claimsEntity.getComplaint().getDataAccident().getTypeAccident());
                authorityClaimsResponseV1.setDateAccident(DateUtil.convertUTCDateToIS08601String(claimsEntity.getComplaint().getDataAccident().getDateAccident()));
                for(Authority activity: claimsEntity.getAuthorities()){
                    if(activity.getAuthorityWorkingId().equals(workingId)){
                        authorityClaimsResponseV1.setNumberFranchise(activity.getNumberFranchise());
                    }
                }
                authorityClaimsResponseV1.setStatus(claimsEntity.getStatus());
                List<Authority> authorities = claimsEntity.getAuthorities();
                Iterator<Authority> iterator = authorities.iterator();
                boolean isFound = false;
                Authority authorityToChange = new Authority();
                while (iterator.hasNext() && !isFound) {
                    authorityToChange = iterator.next();
                    if (authorityToChange.equals(authority)) {
                        isFound = true;
                    }
                }
                authorityClaimsResponseV1.setOldest(authorityToChange.getOldest());
                authorityClaimsResponseV1s.add(authorityClaimsResponseV1);
            }
        } else {
            LOGGER.debug(MessageCode.CLAIMS_2006.value());
            throw new NotFoundException(MessageCode.CLAIMS_2006);
        }

        return authorityClaimsResponseV1s;
    }

    public VehicleStatusAuthorityResponseV1 getVehicleStatus(String chassis, String dateAccident) throws IOException {

        String url = getVehicleStatusByChassis(chassis, dateAccident);

        return callWithoutCert(url, HttpMethod.GET, null, null, new ParameterizedTypeReference<VehicleStatusAuthorityResponseV1>() {
        });
    }


    private String buildUrl(String baseUrl, String uri, Map<String, String> pathParams, MultiValueMap<String, String> queryParams) {

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(baseUrl.concat(uri));

        if (MapUtils.isNotEmpty(queryParams)) {
            builder.queryParams(queryParams);
        }

        return builder.buildAndExpand(pathParams).toString();
    }

    private String getVehicleStatusByChassis(String chassis, String dateAccident) {

        return this.buildUrl(
                authorityEndpoint,
                authorityVehicleStatus,
                new HashMap<String, String>() {{
                    put("chassis", chassis);
                }},
                new LinkedMultiValueMap<String, String>() {{
                    add("date", DateUtil.convertIS08601StringToUTCInstant(dateAccident).toString());
                }}
        );
    }

    private Authority addHistorical(Authority currentAuthority, AuthorityTotalRequestV1 authorityToBe){
        String description = "";
        Long idHistorical;
        List<HistoricalAuthority> historicalAuthorities = currentAuthority.getHistorical();
        if(historicalAuthorities == null)
            idHistorical = 1L;
        else
            idHistorical = historicalAuthorities.size()+1L;

        HistoricalAuthority historicalAuthority = new HistoricalAuthority();
        historicalAuthority.setId(idHistorical);
        historicalAuthority.setEventType(authorityToBe.getEventType());
        historicalAuthority.setUpdateAt(DateUtil.getNowDate());
        historicalAuthority.setOldTotal(currentAuthority.getTotal());
        historicalAuthority.setNewTotal(authorityToBe.getTotal());
        if(authorityToBe.getUserDetails() != null){
            UserDetailsRequestV1 userDetailsRequestV1 = authorityToBe.getUserDetails();
            historicalAuthority.setUserId(userDetailsRequestV1.getUserId());
            historicalAuthority.setUserName(userDetailsRequestV1.getFirstname()+" "+userDetailsRequestV1.getLastname());
        }

        if(currentAuthority.getWreckCasual() !=null && authorityToBe.getWreckCasual() != null && !currentAuthority.getWreckCasual().equals(authorityToBe.getWreckCasual())){
            description += "Campo: Causale wreck. " + "Valore precedente: "+currentAuthority.getWreckCasual() + "." + " Valore attuale: "+authorityToBe.getWreckCasual()+". <br>";
        } else if (currentAuthority.getWreckCasual() == null && authorityToBe.getWreckCasual()!=null) {
            description += "Campo: Causale wreck. " + "Valore precedente: Nessuno." + " Valore attuale: "+authorityToBe.getWreckCasual()+". <br>";
        } else if (authorityToBe.getWreckCasual()==null && currentAuthority.getWreckCasual() != null){
            description += "Campo: Causale wreck. " + "Valore precedente: "+currentAuthority.getWreckCasual() + "." + "Valore attuale: Nessuno. <br>";
        }

        if(currentAuthority.getNbv() !=null && authorityToBe.getNbv() != null && !currentAuthority.getNbv().equals(authorityToBe.getNbv())){
            description += "Campo: NBV. " + "Valore precedente: "+currentAuthority.getNbv() + "." + " Valore attuale: "+authorityToBe.getNbv()+". <br>";
        } else if (currentAuthority.getNbv() == null && authorityToBe.getNbv()!=null) {
            description += "Campo: NBV. " + "Valore precedente: Nessuno." + " Valore attuale: "+authorityToBe.getNbv()+". <br>";
        } else if(currentAuthority.getNbv() != null && authorityToBe.getNbv()==null){
            description += "Campo: NBV. Valore precedente: "+currentAuthority.getNbv() + "." + " Valore attuale: Nessuno. <br>";
        }

        if(currentAuthority.getWorkingStatus() !=null && authorityToBe.getWorkingStatus() != null && !currentAuthority.getWorkingStatus().equals(authorityToBe.getWorkingStatus())){
            description += "Campo: Stato pratica. " + "Valore precedente: "+AuthorityAdapter.adptWorkingStatusFromEnglishToItalian(currentAuthority.getWorkingStatus()) + "." + " Valore attuale: "+AuthorityAdapter.adptWorkingStatusFromEnglishToItalian(authorityToBe.getWorkingStatus())+". <br>";
        } else if (currentAuthority.getWorkingStatus() == null && authorityToBe.getWorkingStatus()!=null) {
            description += "Campo: Stato pratica. " + "Valore precedente: Nessuno." + " Valore attuale: "+AuthorityAdapter.adptWorkingStatusFromEnglishToItalian(authorityToBe.getWorkingStatus())+". <br>";
        }else if (currentAuthority.getWorkingStatus() != null && authorityToBe.getWorkingStatus()==null) {
            description += "Campo: Stato pratica. " + "Valore precedente: "+AuthorityAdapter.adptWorkingStatusFromEnglishToItalian(currentAuthority.getWorkingStatus()) + "." + " Valore attuale: Nessuno. <br>";
        }

        if(currentAuthority.getWreckValue() !=null && authorityToBe.getWreckValue() != null && !currentAuthority.getWreckValue().equals(authorityToBe.getWreckValue())){
            description += "Campo: Valore wreck. " + "Valore precedente: "+currentAuthority.getWreckValue() + "." + " Valore attuale: "+authorityToBe.getWreckValue()+". <br>";
        } else if (currentAuthority.getWreckValue() == null && authorityToBe.getWreckValue()!=null) {
            description += "Campo: Valore wreck. " + "Valore precedente: Nessuno." + " Valore attuale: "+authorityToBe.getWreckValue()+". <br>";
        } else if (currentAuthority.getWreckValue() != null && authorityToBe.getWreckValue()==null) {
            description += "Campo: Valore wreck. " + "Valore precedente: "+currentAuthority.getWreckValue() + "." + " Valore attuale: Nessuno. <br>";
        }

        if(currentAuthority.getWreck() !=null && authorityToBe.getWreck() != null && !currentAuthority.getWreck().equals(authorityToBe.getWreck())){
            description += "Campo: Wreck. " + "Valore precedente: "+adptBoolean(currentAuthority.getWreck()) + "." + " Valore attuale: "+adptBoolean(authorityToBe.getWreck())+". <br>";
        } else if (currentAuthority.getWreck() == null && authorityToBe.getWreck()!=null) {
            description += "Campo: Wreck. " + "Valore precedente: Nessuno." + " Valore attuale: "+adptBoolean(authorityToBe.getWreck())+". <br>";
        } else if (currentAuthority.getWreck() != null && authorityToBe.getWreck()==null) {
            description += "Campo: Wreck. " + "Valore precedente: "+adptBoolean(currentAuthority.getWreck()) + "." + " Valore attuale: Nessuno. <br>";
        }

        historicalAuthority.setDescription(description);
        currentAuthority.addHistorical(historicalAuthority);



        return currentAuthority;
    }

    private String adptBoolean(Boolean bool){
        if(bool == null) return "";
        if(bool)
            return "SI";
        return "NO";
    }

    //REFACTOR
    public AuthorityClaimsResponseV1 getAuthorityPracticeByWorkingId(String idClaims, String workingId) {
        //recupero della nuova entità
        Optional<ClaimsNewEntity> claimsNewEntityOptional = claimsNewRepository.findById(idClaims);

        if(!claimsNewEntityOptional.isPresent()){
            LOGGER.debug("[AUTHORITY] detail not present claimId = {}, workingId {} MessageCode_1010", idClaims, workingId);
            throw  new BadRequestException(MessageCode.CLAIMS_1010);
        }
        //conversione nella vecchia entità
        ClaimsEntity claimsEntity = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntityOptional.get());
        List<Authority> authorities = claimsEntity.getAuthorities();
        Iterator<Authority> iterator = authorities.iterator();
        boolean isFound = false;
        Authority authorityToChange = null;
        while (iterator.hasNext() && !isFound) {
            authorityToChange = iterator.next();
            if (authorityToChange.getAuthorityWorkingId().equals(workingId)) {
                isFound = true;
            }
        }
        AuthorityClaimsResponseV1 authorityResponseV1 = AuthorityAdapter.adptFromAuthorityToAuthorityClaimsResponse(authorityToChange);
        if(authorityResponseV1 != null){
            List<AuthorityOldestResponseV1> authorityClaimsResponseV1s = this.getOldestClaimsByDossierAndWorking(authorityResponseV1.getAuthorityDossierId(), authorityResponseV1.getAuthorityWorkingId());
            authorityResponseV1.setClaimsLinked(authorityClaimsResponseV1s);
        }

        return authorityResponseV1;
    }

    /*public void resetAuthority(){
        claimsRepositoryV1.resetAuthority();
    }*/

    private List<String> getPoToInvoice(Authority authority){
        List<String> requestedPoInvoicingList = new LinkedList<>();
        //se la pratica non ha ancora settato invoiced a true, devono essere recuperate le fatture degli altri po

        if((authority.getInvoiced() == null || !authority.getInvoiced()) &&
                (acceptableEventToPoInvoicePracticeList.contains(authority.getEventType()) ||
                        (WorkingStatusEnum.AUTHORIZED == authority.getWorkingStatus() && AuthorityTypeEnum.GLASS == authority.getType())) &&
                authority.getPoDetails() != null
        ){
            for(PODetails currentPODetail: authority.getPoDetails()){
                //controllo che non sia stato già recuperato
                if(currentPODetail.getIdFilemanager() == null){
                    requestedPoInvoicingList.add(currentPODetail.getPoNumber());
                }
            }

        }
        return requestedPoInvoicingList;

    }

    private Boolean checkAuthorityInvoiced(Authority authority){
        Iterator<PODetails> poDetailsIt = authority.getPoDetails().iterator();
        while(poDetailsIt.hasNext()){
            if(poDetailsIt.next().getIdFilemanager() == null){
                return false;
            }
        }
        return true;
    }


    //REFACTOR
    @Transactional
    public void checkAttachPoInvoicing(String claimsId, Authority authority,OlsaInvoicingResponseV1 invocingOlsaResponse ){
        //recupero della nuova entità
        Optional<ClaimsNewEntity> claimsNewEntityOptional = claimsNewRepository.findById(claimsId);

        if(claimsNewEntityOptional.isPresent()) {
            //conversione nella vecchia entità
            ClaimsEntity claimsEntity = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntityOptional.get());

            LOGGER.debug("[INVOICING PO] lista di authority");
            LOGGER.debug((claimsEntity.getAuthorities() != null ? claimsEntity.getAuthorities().toString() : "lista vuota"));

            Iterator<Authority> authorityIt = claimsEntity.getAuthorities().iterator();

            Boolean isAuthorityFound = false;
            Authority authorityCandidateApp = null;
            PODetails poDetails = null;
            PoNumberResponseV1 poInvoiced = null;

            while (authorityIt.hasNext() && !isAuthorityFound) {
                Authority authorityCandidate = authorityIt.next();
                if ((authorityCandidate.getAuthorityDossierId() != null && authorityCandidate.getAuthorityDossierId().equals(authority.getAuthorityDossierId()) &&
                        authorityCandidate.getAuthorityWorkingId().equals(authority.getAuthorityWorkingId()))
                        || (authorityCandidate.getAuthorityDossierId() == null && authority.getAuthorityDossierId() == null)) {
                    isAuthorityFound = true;
                    authorityCandidateApp = authorityCandidate;
                    LOGGER.debug("[INVOICING PO] authorityCandidateApp");
                    LOGGER.debug((authorityCandidateApp != null ? authorityCandidateApp.toString() : "nessun oggetto candidato"));

                }
            }
            if (authorityCandidateApp != null) {
                LOGGER.debug("[INVOICING PO] authority candidate app trovato");

                if (authorityCandidateApp.getPoDetails() != null) {

                    List<PODetails> listPODetailsNew = new ArrayList<>();
                    listPODetailsNew.addAll(authorityCandidateApp.getPoDetails());

                    for (PODetails currentPO : listPODetailsNew) {
                        LOGGER.debug("[INVOICING PO] Po corrente: {}" , currentPO.getPoNumber());


                        Iterator<PoNumberResponseV1> olsaPoFoundIt = invocingOlsaResponse.getFoundPoList().iterator();
                        Boolean isPoInvoicedOlsaFound = false;

                        while (olsaPoFoundIt.hasNext() && !isPoInvoicedOlsaFound) {
                            poInvoiced = olsaPoFoundIt.next();
                            if (currentPO.getPoNumber().equals(poInvoiced.getPoNumber())) {
                                poDetails = currentPO;
                                isPoInvoicedOlsaFound = true;
                            }
                        }

                        if (isPoInvoicedOlsaFound) {
                            LOGGER.debug("[INVOICING PO] Po corrente {} trovato nella lista di invoicing ", currentPO.getPoNumber() );

                            List<Authority> authorityList = claimsEntity.getAuthorities();
                            LOGGER.debug("[INVOICING PO] Rimuovo dalla lista {} " , authorityCandidateApp.toString());
                            authorityList.remove(authorityCandidateApp);
                            claimsEntity.setAuthorities(authorityList);

                            LOGGER.debug("[INVOICING PO] Lista authority dopo la rimozione {} " , (claimsEntity.getAuthorities() != null ? claimsEntity.getAuthorities().toString() : "null"));

                            List<PODetails> poDetailsList = authorityCandidateApp.getPoDetails();
                            boolean removed = poDetailsList.remove(poDetails);
                            authorityCandidateApp.setPoDetails(poDetailsList);

                            String filemanagerid = null;
                            if (poInvoiced.getFileManagerResponseV1() != null && poInvoiced.getFileManagerResponseV1().getUuid() != null) {
                                filemanagerid = poInvoiced.getFileManagerResponseV1().getUuid();
                            } else if (poInvoiced.getFileManagerResponseV1() != null && poInvoiced.getFileManagerResponseV1().getId() != null) {
                                filemanagerid = poInvoiced.getFileManagerResponseV1().getId();
                            }
                            poDetails.setIdFilemanager(filemanagerid);
                            poDetailsList = authorityCandidateApp.getPoDetails();
                            poDetailsList.add(poDetails);
                            authorityCandidateApp.setPoDetails(poDetailsList);
                            if (claimsEntity.getForms() != null && claimsEntity.getForms().getAttachment() == null) {
                                claimsEntity.getForms().setAttachment(new ArrayList<>());
                            } else if (claimsEntity.getForms() == null) {
                                claimsEntity.setForms(new Forms());
                                claimsEntity.getForms().setAttachment(new ArrayList<>());
                            }
                            List<Attachment> attachments = claimsEntity.getForms().getAttachment();
                            attachments.add(AttachmentAdapter.adptFromFileManagerInvocingToAttachment(poInvoiced.getFileManagerResponseV1()));
                            claimsEntity.getForms().setAttachment(attachments);
                            authorityCandidateApp.setInvoiced(checkAuthorityInvoiced(authorityCandidateApp));
                            authorityList = claimsEntity.getAuthorities();
                            authorityList.add(authorityCandidateApp);
                            claimsEntity.setAuthorities(authorityList);

                            LOGGER.debug("[INVOICING PO] Lista authority prima del cambio stato {} " , (claimsEntity.getAuthorities() != null ? claimsEntity.getAuthorities().toString() : "nulla"));

                            if (claimsEntity.getStatus().equals(ClaimsStatusEnum.WAITING_FOR_AUTHORITY) && ClaimsServiceImpl.isAuthorityAndSxNumber(claimsEntity)) {
                                LOGGER.info("[INVOICING PO] isAuthorityAndSxNumber true and claims status WAITING FOR AUTHORITY");
                                if (claimsEntity.getType().equals(ClaimsFlowEnum.FNI)) {
                                    LOGGER.info("[INVOICING PO] cambio stato sinistro {} in {} ", claimsEntity.getPracticeId(), ClaimsStatusEnum.TO_SEND_TO_CUSTOMER );
                                    claimsEntity.setStatus(ClaimsStatusEnum.TO_SEND_TO_CUSTOMER);
                                } else {
                                    LOGGER.info("[INVOICING PO] cambio stato sinistro {} in {} ", claimsEntity.getPracticeId(), ClaimsStatusEnum.TO_ENTRUST );
                                    claimsEntity.setStatus(ClaimsStatusEnum.TO_ENTRUST);
                                }
                                String description = "Lo stato è cambiato da  " + ClaimsAdapter.adptClaimsStatusEnumToItalian(ClaimsStatusEnum.WAITING_FOR_AUTHORITY) + " a " + ClaimsAdapter.adptClaimsStatusEnumToItalian(ClaimsStatusEnum.TO_ENTRUST);
                                Historical historical = new Historical(EventTypeEnum.EDIT_PRACTICE_DATA, ClaimsStatusEnum.WAITING_FOR_AUTHORITY, claimsEntity.getStatus(), claimsEntity.getComplaint().getDataAccident().getTypeAccident(), claimsEntity.getComplaint().getDataAccident().getTypeAccident(), DateUtil.getNowDate(), null, ClaimsHistoricalUserEnum.SYSTEM_TO_ENTRUST_PO.getValue(), claimsEntity.getType().getValue(), claimsEntity.getType().getValue(), description);
                                claimsEntity.addHistorical(historical);
                            } else if ((claimsEntity.getStatus().equals(ClaimsStatusEnum.WAITING_FOR_REFUND) ||
                                    claimsEntity.getStatus().equals(ClaimsStatusEnum.CLOSED_TOTAL_REFUND)) && ClaimsServiceImpl.isAuthority(claimsEntity)
                            ) {
                                LOGGER.info("[INVOICING PO] isAuthority true and claims status is "+ claimsEntity.getStatus());
                                LOGGER.info("[INVOICING PO] cambio stato sinistro {} in po variation ", claimsEntity.getPracticeId() );
                                claimsEntity.setPoVariation(true);
                            }


                            if (dwhCall) {
                                dwhClaimsService.sendMessage(claimsEntity, com.doing.nemo.dwh.client.payload.request.EventTypeEnum.UPDATED);
                            }
                        }else{
                            if(currentPO.getPoNumber() != null && currentPO.getIdFilemanager() != null){
                                LOGGER.debug("[INVOICING PO] Po corrente {} già fatturato ", currentPO.getPoNumber());
                            }else {
                                LOGGER.debug("[INVOICING PO] Po corrente {} non trovato nella lista di invoicing ", currentPO.getPoNumber());
                            }
                        }
                    }
                }
            }
            LOGGER.debug("[INVOICING PO] salvataggio del sinistro {} ", claimsEntity.getPracticeId() );
            //conversione nella nuova entità
            ClaimsNewEntity claimsNewEntity = converterClaimsService.convertOldEntityToNewClaimsEntity(claimsEntity);
            claimsNewRepository.save(claimsNewEntity);
        }

    }

    @Override
    @Transactional
    public List<ClaimsEntity> getClaimsToInvoce() throws IOException {
        //recupero nuova entità
        List<ClaimsNewEntity> claimsNewEntityList = claimsNewRepository.getClaimsToInvoce();
        //conversione nella vecchia entità
        List<ClaimsEntity> claimsEntityList = converterClaimsService.wrapFromClaimsNewEntityListToClaimsEntityList(claimsNewEntityList);
        //per ogni claims vado a recuperare la sua lista di autorizzazioni
        if(claimsEntityList != null){
            for(ClaimsEntity currentClaim : claimsEntityList){
                LOGGER.debug("[INVOICING PO] {} " , currentClaim.getPracticeId());
                if(currentClaim.getAuthorities()!= null){
                    //scorro la lista
                    for(Authority currentAuthority: currentClaim.getAuthorities() ) {
                        LOGGER.debug("[INVOICING PO] lista authority del sinistro {} ", currentClaim.getPracticeId());
                        LOGGER.debug(currentClaim.getAuthorities().toString());

                        List<String> requestedPoInvoicing = getPoToInvoice(currentAuthority);
                        LOGGER.debug("[INVOICING PO] {} " , requestedPoInvoicing);
                        //chiamo rotta di invoicing per recuperare le fatture
                        if (!requestedPoInvoicing.isEmpty()){

                            OlsaInvoicingResponseV1 invocingOlsaResponse = invocingService.getAttachmentInvoicing(requestedPoInvoicing);
                            if (invocingOlsaResponse != null &&
                                    invocingOlsaResponse.getFoundPoList() != null &&
                                    !invocingOlsaResponse.getFoundPoList().isEmpty()) {
                                this.checkAttachPoInvoicing(currentClaim.getId(), currentAuthority, invocingOlsaResponse);
                            }
                        }
                    }
                }
            }
        }

        return null;
    }


    //REFACTOR
    @Override
    @Transactional
    public void getSingleClaimToInvoice(String idClaims, String workingId) throws IOException {
        //recupero nuova entità
        Optional<ClaimsNewEntity> claimsNewEntityOptional = claimsNewRepository.findById(idClaims);
        if(!claimsNewEntityOptional.isPresent()){
            LOGGER.debug("Claims with UUID " + idClaims + " not found ");
            throw new NotFoundException("Claims with UUID " + idClaims + " not found ", MessageCode.CLAIMS_1010);
        }

        //converto nella vecchia entità
        ClaimsEntity claimsEntity = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntityOptional.get());

        if(claimsEntity.getAuthorities() != null){
            LOGGER.debug("[INVOICING PO] {} " , claimsEntity.getPracticeId());
            LOGGER.debug(claimsEntity.getAuthorities().toString());
            for(Authority currentAuthority: claimsEntity.getAuthorities()){
                if(currentAuthority.getAuthorityWorkingId().equals(workingId)){
                    List<String> requestedPoInvoicing = getPoToInvoice(currentAuthority);
                    LOGGER.debug("[INVOICING PO] {} ",  requestedPoInvoicing);
                    if (requestedPoInvoicing != null && !requestedPoInvoicing.isEmpty()){
                        OlsaInvoicingResponseV1 invocingOlsaResponse = invocingService.getAttachmentInvoicing(requestedPoInvoicing);
                        LOGGER.debug("[INVOICING PO] response di invoicing; {} " , invocingOlsaResponse);
                        if (invocingOlsaResponse != null &&
                                invocingOlsaResponse.getFoundPoList() != null &&
                                !invocingOlsaResponse.getFoundPoList().isEmpty()) {
                            //this.checkAttachPoInvoicing(claimsEntity.getId(), currentAuthority, invocingOlsaResponse);
                            //recupero della nuova entità
                            List<ClaimsNewEntity> claimsNewEntityList = claimsNewRepository.getClaimsToCheckDuplicateAuthority(currentAuthority.getAuthorityDossierId(), currentAuthority.getAuthorityWorkingId());
                            //conversione nella vecchia entità
                            List<ClaimsEntity> claimsEntityList = converterClaimsService.wrapFromClaimsNewEntityListToClaimsEntityList(claimsNewEntityList);
                            LOGGER.debug("[INVOICING PO] lista sinistri size(): " + (claimsEntityList != null ? claimsEntityList.size() : "null") );
                            if( claimsEntityList != null && !claimsEntityList.isEmpty()){
                                for(ClaimsEntity currentLinkedClaims : claimsEntityList){
                                    LOGGER.debug("[INVOICING PO] sinistro corrente: " + currentLinkedClaims.getPracticeId());
                                    this.checkAttachPoInvoicing(currentLinkedClaims.getId(), currentAuthority, invocingOlsaResponse);
                                }
                            }

                        }
                    }
                }
            }
        }


    }

    //INTEGRAZIONE AUTHORITY/REPAIR

    //REFACTOR
    @Override
    public List<CounterpartyEntity> getCounterpartyByPlate(String plate){
        List<CounterpartyNewEntity> counterpartyNewEntityList = counterpartyNewRepositoryImpl.getCounterpartyByPlate(plate);
        return CounterpartyAdapter.adptCounterpartyNewToCounterpartyOldList(counterpartyNewEntityList);
    }

    //REFACTOR
    public Pagination<AuthorityPaginationRepairResponseV1> paginationRepairAuthorityResponseList(int page, int pageSize, String orderBy, Boolean asc, String plate, String chassis) {


        //targa telaio
        //Recupero della nuova entità
        Pagination<CounterpartyEntity> counterpartyPagination = counterpartyService.getCounterpartyPaginationAuthority(plate, chassis, page, pageSize);
        List<CounterpartyEntity> counterpartyEntityList = counterpartyPagination.getItems();
        Map<String, List<String>> claimsWithLinkedAssociation = new HashMap<>();
        for (CounterpartyEntity counterpartyEntity : counterpartyEntityList) {
            List<String> authorityLinked = new ArrayList<>();
            if (counterpartyEntity.getAuthorities() != null) {
                for (Authority authority : counterpartyEntity.getAuthorities()) {
                    authorityLinked.add(authority.getAuthorityDossierId());
                }
            }
            if (authorityLinked.isEmpty())
                claimsWithLinkedAssociation.put(counterpartyEntity.getCounterpartyId(), null);
            else
                claimsWithLinkedAssociation.put(counterpartyEntity.getCounterpartyId(), authorityLinked);
        }

        Pagination<AuthorityPaginationRepairResponseV1> responsePaginationAuthority = AuthorityAdapter.adptFromPaginationCounterpartyToPaginationAuthority(counterpartyPagination, claimsWithLinkedAssociation);

        return responsePaginationAuthority;
    }

    //REFACTOR
    @Transactional
    public void linkUnlinkAuthorityRepair(AuthorityRepairRequestV1 authorityRepairRequestV1) {
        Authority authority = new Authority();
        authority.setAuthorityDossierId(authorityRepairRequestV1.getAuthorityDossierId());
        authority.setAuthorityWorkingId(authorityRepairRequestV1.getAuthorityWorkingId());
        authority.setType(authorityRepairRequestV1.getType());
        authority.setCommodityDetails(CommodityDetailsAdapter.adptFromCommodityDetailsRequestToCommodityDetails(authorityRepairRequestV1.getCommodityDetails()));
        List<String> counterpartyListId = new ArrayList<>();
        if (authorityRepairRequestV1.getRepairList() != null && !authorityRepairRequestV1.getRepairList().isEmpty()) {

            for (IdRequestV1 att : authorityRepairRequestV1.getRepairList()) {
                counterpartyListId.add(att.getId());
            }

            //Recupero della nuova entità
            List<CounterpartyNewEntity> counterpartyNewEntities = counterpartyNewRepositoryImpl.getCounterpartyToAttachAuthority(authorityRepairRequestV1.getAuthorityDossierId(), authorityRepairRequestV1.getAuthorityWorkingId(), counterpartyListId);
            //conversione nella vecchia entità
            List<CounterpartyEntity> counterpartyEntities = CounterpartyAdapter.adptCounterpartyNewToCounterpartyOldList(counterpartyNewEntities);
            if(counterpartyEntities != null) {
                for (CounterpartyEntity counterpartyEntity : counterpartyEntities) {
                    if (counterpartyEntity.getVehicle() != null && counterpartyEntity.getVehicle().getLicensePlate() != null) {
                        if (!counterpartyEntity.getVehicle().getLicensePlate().equalsIgnoreCase(authorityRepairRequestV1.getPlate())) {
                            //claimsNotValid.addPlateError(currentClaim.getId(), "Counterparty with id " + currentClaim.getId() + " is not associable because his plate does not match");
                            String message = String.format(MessageCode.CLAIMS_1119.value(), counterpartyEntity.getCounterpartyId());
                            LOGGER.debug(message);
                            throw new BadRequestException(message, MessageCode.CLAIMS_1119);
                        }
                    } else {
                        //claimsNotValid.addPlateError(currentClaim.getId(), "Counterparty with id " + currentClaim.getId() + " is not associable because his plate is NULL");
                        String message = String.format(MessageCode.CLAIMS_1118.value(), counterpartyEntity.getCounterpartyId());
                        LOGGER.debug(message);
                        throw new BadRequestException(message, MessageCode.CLAIMS_1118);
                    }
                }

                //se non vi sono COUNTERPARTY non validi
                for (CounterpartyEntity currentCounterparty : counterpartyEntities) {
                    authority.setStatus(AuthorityStatusEnum.WAITING_FOR_AUTHORIZATION);
                    authority.setWorkingNumber(authorityRepairRequestV1.getWorkingNumber());
                    authority.setAuthorityDossierNumber(authorityRepairRequestV1.getAuthorityDossierNumber());
                    authority.setMsaDownloaded(false);
                    List<Authority> authorityList = currentCounterparty.getAuthorities();
                    if (authorityList == null)
                        authorityList = new ArrayList<>();

                    authorityList.add(authority);
                    currentCounterparty.setAuthorities(authorityList);
                    HistoricalCounterparty historical = new HistoricalCounterparty(currentCounterparty.getRepairStatus(), currentCounterparty.getRepairStatus(), "", DateUtil.getNowDate(), null, null, "\n" +
                            "Autorizzazione collegata con dossier id " + authorityRepairRequestV1.getAuthorityDossierNumber() + " e working id " + authorityRepairRequestV1.getWorkingNumber(), EventTypeEnum.AUTHORITY_ASSOCIATION);
                    if(currentCounterparty.getHistoricals() == null ){
                        currentCounterparty.setHistoricals(new ArrayList<>());
                    }
                    List<HistoricalCounterparty> historicalCounterparties = currentCounterparty.getHistoricals();
                    historicalCounterparties.add(historical);
                    currentCounterparty.setHistoricals(historicalCounterparties);



                    //Converto la nuova entità
                    CounterpartyNewEntity counterpartyNewEntity = CounterpartyAdapter.adptCounterpartyOldToCounterpartyNew(currentCounterparty);
                    //salvataggio della nuova entità
                    counterpartyNewRepository.save(counterpartyNewEntity);
                    //AGGIUNGERE CHIAMATA DWH
                    if(dwhCall){
                        dwhClaimsService.sendMessage(currentCounterparty, com.doing.nemo.dwh.client.payload.request.EventTypeEnum.UPDATED);
                    }
                }
            }
        }

        //REFACTOR
        unlinkCounterpartyToAuthority(authority, authorityRepairRequestV1, counterpartyListId);
    }

    //REFACOTR
    @Transactional
    public void unlinkCounterpartyToAuthority(Authority authority, AuthorityRepairRequestV1 authorityRequestV1, List<String> counterpartyListID){
        //recupero vecchia entità
        List<CounterpartyNewEntity> counterpartyNewEntities = counterpartyNewRepositoryImpl.getCounterpartyToDetattachAuthority(authorityRequestV1.getAuthorityDossierId(), authorityRequestV1.getAuthorityWorkingId(), counterpartyListID);
        //conversione nella nuova entità
        List<CounterpartyEntity> counterpartyEntities = CounterpartyAdapter.adptCounterpartyNewToCounterpartyOldList(counterpartyNewEntities);
        System.out.println("IN UNLINK Counterparty "+ (counterpartyEntities != null ? counterpartyEntities.size() : ""));
        if(counterpartyEntities != null) {
            for (CounterpartyEntity currentCounterparty : counterpartyEntities) {
                if(currentCounterparty.getAuthorities() != null) {
                    Authority authorityToDelete = getAuthoritySearched(authority, currentCounterparty.getAuthorities());
                    if (authorityToDelete==null){
                        String message = String.format(MessageCode.CLAIMS_1129.value(), currentCounterparty.getClaims().getId());
                        LOGGER.debug("[AUTHORITY] " +message);
                        throw new BadRequestException(message, MessageCode.CLAIMS_1129);
                    }

                    List<Authority> authorities = currentCounterparty.getAuthorities();
                    authorities.remove(authorityToDelete);
                    currentCounterparty.setAuthorities(authorities);
                    //conversione nella nuova entità
                    CounterpartyNewEntity counterpartyNewEntity = CounterpartyAdapter.adptCounterpartyOldToCounterpartyNew(currentCounterparty);
                    counterpartyNewRepository.save(counterpartyNewEntity);
                    if(dwhCall){
                        dwhClaimsService.sendMessage(currentCounterparty, com.doing.nemo.dwh.client.payload.request.EventTypeEnum.UPDATED);
                    }
                }
            }
        }
    }

    //REFACTOR
    @Transactional
    public void associateCounterparty(AuthorityRepairTotalRequestV1 authorityTotalRequestV1) {
        if (authorityTotalRequestV1 != null && authorityTotalRequestV1.getRepairList() != null) {
            if (authorityTotalRequestV1.getRepairList().isEmpty()) {
                LOGGER.debug(MessageCode.CLAIMS_1120.value());
                throw new BadRequestException(MessageCode.CLAIMS_1120);
            }
            List<String> counterpartyIdList = new ArrayList<>();
            for (IdRequestV1 repairAuthorityRequestV1 : authorityTotalRequestV1.getRepairList()) {
                counterpartyIdList.add(repairAuthorityRequestV1.getId());
            }
            //recupero della nuova entità
            List<CounterpartyNewEntity> counterpartyNewEntities = counterpartyNewRepositoryImpl.getCounterpartyToApproveAuthority(authorityTotalRequestV1.getAuthorityDossierId(), authorityTotalRequestV1.getAuthorityWorkingId());
            List<CounterpartyEntity> counterpartyEntities = CounterpartyAdapter.adptCounterpartyNewToCounterpartyOldList(counterpartyNewEntities);

            if (counterpartyEntities == null || counterpartyEntities.isEmpty()) {
                LOGGER.debug(MessageCode.CLAIMS_1121.value());
                throw new NotFoundException(MessageCode.CLAIMS_1121);
            }
            if ((counterpartyEntities.size() != authorityTotalRequestV1.getRepairList().size())) {

                LOGGER.debug(MessageCode.CLAIMS_1122.value());
                throw new BadRequestException(MessageCode.CLAIMS_1122);

            } else {
                for (CounterpartyEntity counterpartyEntity : counterpartyEntities) {

                    List<Authority> authorities = counterpartyEntity.getAuthorities();
                    Authority currentAuthority = null;
                    if (authorities.size() > 1) {
                        Authority authority = new Authority();
                        authority.setAuthorityDossierId(authorityTotalRequestV1.getAuthorityDossierId());
                        authority.setAuthorityWorkingId(authorityTotalRequestV1.getAuthorityWorkingId());

                        currentAuthority = getAuthoritySearched(authority, authorities);
                        if (currentAuthority==null){
                            String message = String.format(MessageCode.CLAIMS_1129.value(), counterpartyEntity.getClaims().getId());
                            LOGGER.debug("[AUTHORITY] " +message);
                            throw new BadRequestException(message, MessageCode.CLAIMS_1129);
                        }

                        authorities.remove(currentAuthority);
                    }
                    else if(!authorities.isEmpty()){
                        currentAuthority = authorities.get(0);
                        authorities.remove(currentAuthority);
                    }

                    RepairStatusEnum statusOld = counterpartyEntity.getRepairStatus();

                    if(currentAuthority != null) {
                        currentAuthority = addHistoricalRepair(currentAuthority, authorityTotalRequestV1);
                        currentAuthority = setAllAuthorityFieldFromApprovingRepairMethod(currentAuthority, authorityTotalRequestV1);
                        authorities.add(currentAuthority);

                        if(currentAuthority.getEventType() != null && currentAuthority.getEventType().equals(AuthorityEventTypeEnum.APPROVED)) {
                            counterpartyEntity.setRepairStatus(RepairStatusEnum.QUOTATION_APPROVED);
                            if(counterpartyEntity.getIsReadMsa() != null && counterpartyEntity.getIsReadMsa()){
                                counterpartyEntity.setIsReadMsa(false);
                            }
                        }
                        if(currentAuthority.getEventType() != null && currentAuthority.getEventType().equals(AuthorityEventTypeEnum.END_WORKING)){
                            counterpartyEntity.setRepairStatus(RepairStatusEnum.REPAIRED);
                            if(counterpartyEntity.getIsReadMsa() != null && counterpartyEntity.getIsReadMsa()){
                                counterpartyEntity.setIsReadMsa(false);
                            }
                        }
                    }

                    counterpartyEntity.setAuthorities(authorities);

                    // CREATE HISTORICAL
                    //HistoricalCounterparty historicalCounterparty = new HistoricalCounterparty(statusOld, counterpartyEntity.getRepairStatus(), "Riparazione accettata", DateUtil.getNowDate(), null, "Authority", "", EventTypeEnum.EDIT_PRACTICE_DATA);
                    //counterpartyEntity.addHistorical(historicalCounterparty);

                    //MEttere qui gli if sullo stato del working e sull'event type
                    CounterpartyNewEntity counterpartyNewEntity = CounterpartyAdapter.adptCounterpartyOldToCounterpartyNew(counterpartyEntity);
                    counterpartyNewRepository.save(counterpartyNewEntity);
                    //AGGIUNGERE CHIAMATA DWH
                    if(dwhCall){
                        dwhClaimsService.sendMessage(counterpartyEntity, com.doing.nemo.dwh.client.payload.request.EventTypeEnum.UPDATED);
                    }
                }
            }
        }
    }

    private Authority addHistoricalRepair(Authority currentAuthority, AuthorityRepairTotalRequestV1 authorityToBe){
        String description = "";
        Long idHistorical;
        List<HistoricalAuthority> historicalAuthorities = currentAuthority.getHistorical();
        if(historicalAuthorities == null)
            idHistorical = 1L;
        else
            idHistorical = historicalAuthorities.size()+1L;

        HistoricalAuthority historicalAuthority = new HistoricalAuthority();
        historicalAuthority.setId(idHistorical);
        historicalAuthority.setEventType(authorityToBe.getEventType());
        historicalAuthority.setUpdateAt(DateUtil.getNowDate());
        historicalAuthority.setOldTotal(currentAuthority.getTotal());
        historicalAuthority.setNewTotal(authorityToBe.getTotal());
        if(authorityToBe.getUserDetails() != null){
            UserDetailsRequestV1 userDetailsRequestV1 = authorityToBe.getUserDetails();
            historicalAuthority.setUserId(userDetailsRequestV1.getUserId());
            historicalAuthority.setUserName(userDetailsRequestV1.getFirstname()+" "+userDetailsRequestV1.getLastname());
        }

        if(currentAuthority.getWorkingStatus() !=null && !currentAuthority.getWorkingStatus().equals(authorityToBe.getWorkingStatus())){
            if(authorityToBe.getWorkingStatus() != null)
                description += "Campo: Stato pratica. " + "Valore precedente: "+AuthorityAdapter.adptWorkingStatusFromEnglishToItalian(currentAuthority.getWorkingStatus()) + "." + " Valore attuale: "+AuthorityAdapter.adptWorkingStatusFromEnglishToItalian(authorityToBe.getWorkingStatus())+". <br>";
        } else if (currentAuthority.getWorkingStatus() == null && authorityToBe.getWorkingStatus()!=null) {
            description += "Campo: Stato pratica. " + "Valore precedente: Nessuno." + " Valore attuale: "+AuthorityAdapter.adptWorkingStatusFromEnglishToItalian(authorityToBe.getWorkingStatus())+". <br>";
        }

        historicalAuthority.setDescription(description);
        currentAuthority.addHistorical(historicalAuthority);

        return currentAuthority;
    }

    public Authority addHistoricalRepairClosedStatus(Authority currentAuthority, WorkingStatusEnum workingStatusEnum){
        String description = "";
        Long idHistorical;
        List<HistoricalAuthority> historicalAuthorities = currentAuthority.getHistorical();
        if(historicalAuthorities == null)
            idHistorical = 1L;
        else
            idHistorical = historicalAuthorities.size()+1L;

        HistoricalAuthority historicalAuthority = new HistoricalAuthority();
        historicalAuthority.setId(idHistorical);
        historicalAuthority.setEventType(currentAuthority.getEventType());
        historicalAuthority.setUpdateAt(DateUtil.getNowDate());
        historicalAuthority.setOldTotal(currentAuthority.getTotal());
        historicalAuthority.setNewTotal(currentAuthority.getTotal());
        if(currentAuthority.getUserDetails() != null){
            UserDetails userDetailsRequestV1 = currentAuthority.getUserDetails();
            historicalAuthority.setUserId(userDetailsRequestV1.getUserId());
            historicalAuthority.setUserName(userDetailsRequestV1.getFirstname()+" "+userDetailsRequestV1.getLastname());
        }

        description += "Campo: Stato pratica. " + "Valore precedente: "+AuthorityAdapter.adptWorkingStatusFromEnglishToItalian(currentAuthority.getWorkingStatus()) + "." + " Valore attuale: "+AuthorityAdapter.adptWorkingStatusFromEnglishToItalian(workingStatusEnum)+". <br>";

        historicalAuthority.setDescription(description);
        currentAuthority.addHistorical(historicalAuthority);

        return currentAuthority;
    }

    private Authority setAllAuthorityFieldFromApprovingRepairMethod(Authority authority, AuthorityRepairTotalRequestV1 authorityTotalRequestV1){
        if (authorityTotalRequestV1.getTotal() != null)
            authority.setTotal(authorityTotalRequestV1.getTotal());
        if (authorityTotalRequestV1.getWorkingNumber() != null)
            authority.setWorkingNumber(authorityTotalRequestV1.getWorkingNumber());
        if (Util.isNotEmpty(authorityTotalRequestV1.getAcceptingDate()))
            if(authorityTotalRequestV1.getAuthorityDossierNumber() != null)
                authority.setAuthorityDossierNumber(authorityTotalRequestV1.getAuthorityDossierNumber());
        if(authorityTotalRequestV1.getWorkingStatus() != null)
            authority.setWorkingStatus(authorityTotalRequestV1.getWorkingStatus());
        if(authorityTotalRequestV1.getUserDetails() != null)
            authority.setUserDetails(UserDetailsAdapter.adptFromUserDetailsRequestToUserDetails(authorityTotalRequestV1.getUserDetails()));
        if(Util.isNotEmpty(authorityTotalRequestV1.getAcceptingDate()))
            authority.setAcceptingDate(DateUtil.convertIS08601StringToUTCDate(authorityTotalRequestV1.getAcceptingDate()));
        if (Util.isNotEmpty(authorityTotalRequestV1.getAuthorizationDate())) {
            authority.setAuthorizationDate(DateUtil.convertIS08601StringToUTCDate(authorityTotalRequestV1.getAuthorizationDate()));
            authority.setStatus(AuthorityStatusEnum.AUTHORIZED);
        } else if (Util.isNotEmpty(authorityTotalRequestV1.getRejectionDate())) {
            authority.setRejectionDate(DateUtil.convertIS08601StringToUTCDate(authorityTotalRequestV1.getRejectionDate()));
            authority.setStatus(AuthorityStatusEnum.UNAUTHORIZED);
        }
        if (authorityTotalRequestV1.getRejection() != null)
            authority.setRejection(authorityTotalRequestV1.getRejection());

        if(Util.isNotEmpty(authorityTotalRequestV1.getNoteRejection())){
            authority.setNoteRejection(authorityTotalRequestV1.getNoteRejection());
        }
        if (Util.isNotEmpty(authorityTotalRequestV1.getWorkingCreatedAt()))
            authority.setWorkingCreatedAt(DateUtil.convertIS08601StringToUTCDate(authorityTotalRequestV1.getWorkingCreatedAt()));
        if (Util.isNotEmpty(authorityTotalRequestV1.getEventDate()))
            authority.setEventDate(DateUtil.convertIS08601StringToUTCDate(authorityTotalRequestV1.getEventDate()));
        if (authorityTotalRequestV1.getEventType() != null) {
            authority.setEventType(authorityTotalRequestV1.getEventType());
        }
        return authority;
    }

    private String getURLSetRepairStatus(String dossier, String working) {

        return this.buildUrl(
                authorityEndpoint,
                authorityRepairStatus,
                new HashMap<String, String>() {{
                    put("dossier_id", dossier);
                    put("working_id", working);
                }},
                null
        );
    }

    public void setAuthorityRepairStatus(String dossier, String working, AuthorityRepairStatusRequestV1 requestV1, String userId, String firstname, String lastname, String gatewayProfilesMap) throws IOException {

        String url = getURLSetRepairStatus(dossier, working);
        HashMap<String, String> header = new HashMap<String, String>() {{
            put(nemoUserId, userId);
            put(nemoUserFirstname, firstname);
            put(nemoUserLastname, lastname);
            put(nemoProfilesTree, gatewayProfilesMap);
        }};
        callWithoutCert(url, HttpMethod.PATCH, requestV1, header, null);
    }


    //INTEGRAZIONE PRATICHE AUTO
    public Pagination<AuthorityPracticePaginationResponseV1> paginationPracticeAuthorityResponseList(int page, int pageSize, String plate, String chassis, List<AuthorityPracticeTypeEnum> type) {
        //targa telaio
        Pagination<PracticeEntity> practiceEntityPagination = practiceService.getPracticePaginationAuthority(plate, chassis, page, pageSize, type);
        List<PracticeEntity> practiceEntities = practiceEntityPagination.getItems();
        Map<String, List<String>> claimsWithLinkedAssociation = new HashMap<>();
        for (PracticeEntity practiceEntity : practiceEntities) {
            List<String> authorityLinked = new ArrayList<>();
            if (practiceEntity.getProcessingList() != null) {
                for (Processing authority : practiceEntity.getProcessingList()) {
                    if(authority.getDossierId() != null && !authorityLinked.contains(authority.getDossierId())){

                        authorityLinked.add(authority.getDossierId());
                    }
                }
            }
            if (authorityLinked.isEmpty())
                claimsWithLinkedAssociation.put(practiceEntity.getId().toString(), null);
            else
                claimsWithLinkedAssociation.put(practiceEntity.getId().toString(), authorityLinked);
        }

        Pagination<AuthorityPracticePaginationResponseV1> responsePaginationAuthority = AuthorityAdapter.adptFromPaginationPracticeToPaginationAuthority(practiceEntityPagination, claimsWithLinkedAssociation);

        return responsePaginationAuthority;
    }

    @Transactional
    public AuthorityPracticeAssociationListResponseV1 linkUnlinkAuthorityPractice(AuthorityPracticeRequestV1 authorityRequestV1){
        List<String> practiceIdList = new ArrayList<>();
        AuthorityPracticeAssociationListResponseV1 responseV1 = new AuthorityPracticeAssociationListResponseV1();
        if (authorityRequestV1.getPracticeList()!= null && !authorityRequestV1.getPracticeList().isEmpty()) {

            for (IdRequestV1 att : authorityRequestV1.getPracticeList()) {
                practiceIdList.add(att.getId());
            }
            List<ProcessingTypeEnum> processingTypeEnums = new ArrayList<>();
            if(authorityRequestV1.getType() != null && !authorityRequestV1.getType().isEmpty()){
                for(AuthorityPracticeTypeEnum administrativePracticeTypeEnum : authorityRequestV1.getType()){
                    processingTypeEnums.add(EnumAdapter.adptFromAuthorityPracticeTypeToProcessingType(administrativePracticeTypeEnum));
                }
            }

            List<PracticeEntity> practiceEntities = practiceRepositoryV1.getPracticeToAttachAuthority(authorityRequestV1.getAuthorityDossierId(), authorityRequestV1.getAuthorityWorkingId(), practiceIdList);
            if(practiceEntities != null) {
                for (PracticeEntity practiceEntity : practiceEntities) {
                    if (practiceEntity.getVehicle() != null && practiceEntity.getVehicle().getLicensePlate() != null) {
                        if (!practiceEntity.getVehicle().getLicensePlate().equalsIgnoreCase(authorityRequestV1.getPlate())) {
                            //claimsNotValid.addPlateError(currentClaim.getId(), "Counterparty with id " + currentClaim.getId() + " is not associable because his plate does not match");

                            //CAMBIARE MESSAGE CODE
                            String message = String.format(MessageCode.CLAIMS_1119.value(), practiceEntity.getId());
                            LOGGER.debug(message);
                            throw new BadRequestException(message, MessageCode.CLAIMS_1119);
                        }
                    }
                    else {
                        //claimsNotValid.addPlateError(currentClaim.getId(), "Counterparty with id " + currentClaim.getId() + " is not associable because his plate is NULL");

                        //CAMBIARE MESSAGE CODE
                        String message = String.format(MessageCode.CLAIMS_1118.value(), practiceEntity.getId());
                        LOGGER.debug(message);
                        throw new BadRequestException(message, MessageCode.CLAIMS_1118);
                    }

                    List<Processing> processingList = practiceEntity.getProcessingList();
                    if(processingList == null){
                        throw new BadRequestException("Cannot associate practice because it hasn't processing with the same type");
                    }
                    else {
                        Iterator<Processing> processingIterator = processingList.iterator();
                        boolean isFound = false;
                        while(processingIterator.hasNext() && !isFound){
                            Processing processing1 = processingIterator.next();
                            if(processingTypeEnums.contains(processing1.getProcessingType())){
                                isFound = true;
                            }
                        }
                        if(!isFound){
                            throw new BadRequestException("Cannot associate practice because it hasn't processing with the same type");
                        }
                    }
                }

                //se non vi sono PRATICHE non validi

                List<AuthorityPracticeAssociationResponseV1> authorityPracticeAssociationResponseV1s = new ArrayList<>();
                for (PracticeEntity practiceEntity : practiceEntities) {
                    List<Processing> processingList = setAssociationProcessingList(practiceEntity, authorityRequestV1, processingTypeEnums);
                    AuthorityPracticeAssociationResponseV1 authorityPracticeAssociationResponseV1 = new AuthorityPracticeAssociationResponseV1();
                    authorityPracticeAssociationResponseV1.setId(practiceEntity.getId().toString());
                    authorityPracticeAssociationResponseV1.setPracticeId(practiceEntity.getPracticeId());
                    authorityPracticeAssociationResponseV1.setPracticeType(practiceEntity.getPracticeType());
                    authorityPracticeAssociationResponseV1.setProcessingList(processingList);
                    authorityPracticeAssociationResponseV1s.add(authorityPracticeAssociationResponseV1);
                }
                responseV1.setItems(authorityPracticeAssociationResponseV1s);
            }
        }

        unlinkPracticeToAuthority(authorityRequestV1, practiceIdList);
        return responseV1;
    }

    @Transactional
    public void unlinkPracticeToAuthority(AuthorityPracticeRequestV1 authorityRequestV1, List<String> practiceListID){
        List<PracticeEntity> practiceEntities = practiceRepositoryV1.getPracticeToDetattachAuthority(authorityRequestV1.getAuthorityDossierId(), authorityRequestV1.getAuthorityWorkingId(), practiceListID);
        System.out.println("IN UNLINK "+ practiceEntities.size());
        for (PracticeEntity currentPractice : practiceEntities) {
            List<Processing> processings = currentPractice.getProcessingList();
            if(processings != null) {
                //SE WORKING E DOSSIER COINCIDONO SBIANCARE I CAMPI
                for(Processing processing : processings){
                    if(authorityRequestV1.getAuthorityDossierId().equalsIgnoreCase(processing.getDossierId()) && authorityRequestV1.getAuthorityWorkingId().equalsIgnoreCase(processing.getWorkingId())){
                        processing.setProcessingAuthorityDate(null);
                        processing.setCommodityDetails(null);
                        processing.setWorkingNumber(null);
                        processing.setDossierNumber(null);
                        processing.setWorkingId(null);
                        processing.setDossierId(null);
                        processing.setWorkingType(null);
                    }
                }
                practiceRepository.save(currentPractice);
            }
        }
    }

    @Transactional
    public void associatePractice(AuthorityPracticeUpdateRequestV1 authorityPracticeUpdateRequestV1) {
        if (authorityPracticeUpdateRequestV1 != null && authorityPracticeUpdateRequestV1.getPracticeList() != null) {
            if (authorityPracticeUpdateRequestV1.getPracticeList().isEmpty()) {
                LOGGER.debug(MessageCode.CLAIMS_1120.value());
                throw new BadRequestException(MessageCode.CLAIMS_1120);
            }
            List<String> practiceIdList = new ArrayList<>();
            for (IdRequestV1 practiceAuthorityRequestV1 : authorityPracticeUpdateRequestV1.getPracticeList()) {
                practiceIdList.add(practiceAuthorityRequestV1.getId());
            }
            List<PracticeEntity> practiceEntities = practiceRepositoryV1.getPracticeToApproveAuthority(authorityPracticeUpdateRequestV1.getAuthorityDossierId(), authorityPracticeUpdateRequestV1.getAuthorityWorkingId());
            System.out.println(practiceEntities.size());
            if (practiceEntities == null || practiceEntities.isEmpty()) {
                LOGGER.debug(MessageCode.CLAIMS_1121.value());
                throw new NotFoundException(MessageCode.CLAIMS_1121);
            }
            if (practiceEntities.size() != authorityPracticeUpdateRequestV1.getPracticeList().size()) {
                LOGGER.debug(MessageCode.CLAIMS_1128.value());
                throw new BadRequestException(MessageCode.CLAIMS_1128);
            }
            else {
                for (PracticeEntity practiceEntity : practiceEntities) {
                    getAuthorityPracticeSearched(practiceEntity, authorityPracticeUpdateRequestV1);
                }
            }
        }
    }

    private List<Processing> setAssociationProcessingList(PracticeEntity practiceEntity, AuthorityPracticeRequestV1 authorityPracticeRequestV1, List<ProcessingTypeEnum> processingTypeEnums){
        List<Processing> processingList = practiceEntity.getProcessingList();
        List<Processing> processings = new ArrayList<>();
        if(processingList != null) {
            for(Processing processing : processingList) {
                if (processingTypeEnums.contains(processing.getProcessingType())) {
                    processing.setDossierId(authorityPracticeRequestV1.getAuthorityDossierId());
                    processing.setWorkingId(authorityPracticeRequestV1.getAuthorityWorkingId());
                    processing.setDossierNumber(authorityPracticeRequestV1.getAuthorityDossierNumber());
                    processing.setWorkingNumber(authorityPracticeRequestV1.getWorkingNumber());
                    processing.setCommodityDetails(CommodityDetailsAdapter.adptFromCommodityDetailsRequestToCommodityDetails(authorityPracticeRequestV1.getCommodityDetails()));
                    processing.setWorkingType(authorityPracticeRequestV1.getWorkingType());
                    //processing.setProcessingAuthorityDate(DateUtil.convertIS08601StringToUTCDate(authorityPracticeRequestV1.getProcessingAuthorityDate()));
                    processings.add(processing);
                }
            }
            practiceEntity.setProcessingList(processingList);
            practiceRepository.save(practiceEntity);
        }
        return processings;
    }

    private void getAuthorityPracticeSearched(PracticeEntity practiceEntity, AuthorityPracticeUpdateRequestV1 authorityPracticeRequestV1){
        List<Processing> processingList = practiceEntity.getProcessingList();
        if(processingList != null) {
            for(Processing processing : processingList) {
                if (authorityPracticeRequestV1.getAuthorityDossierId().equals(processing.getDossierId()) && authorityPracticeRequestV1.getAuthorityWorkingId().equals(processing.getWorkingId())) {
                    if (authorityPracticeRequestV1.getProcessingAuthorityDate() != null) {
                        processing.setProcessingAuthorityDate(DateUtil.convertIS08601StringToUTCDate(authorityPracticeRequestV1.getProcessingAuthorityDate()));
                    }
                    if(authorityPracticeRequestV1.getStatus() != null){
                        processing.setStatus(authorityPracticeRequestV1.getStatus());
                    }
                }
            }
            practiceEntity.setProcessingList(processingList);
            practiceRepository.save(practiceEntity);
        }

    }


    public PaginationResponseV1<WorkingAdministrationResponseV1> paginationPracticeAdministrative(int page, int pageSize, String orderBy, Boolean asc, String plateOrChassis, AdministrativePracticeTypeEnum type, String nemoUserId, String nemoProfileTree) throws IOException {
        PaginationAuthority<WorkingAdministrationResponseV1> paginationAuthority = getPracticeAdministrative(plateOrChassis, orderBy, asc, page, pageSize, nemoUserId, nemoProfileTree, type);
        PaginationResponseV1<WorkingAdministrationResponseV1> responseV1 = new PaginationResponseV1<>();
        responseV1.setStats(paginationAuthority.getStats());
        responseV1.setItems(paginationAuthority.getItems());
        return responseV1;
    }

    private String getURLPracticeAdministrative(String plateOrChassis, String orderBy, String asc, String page, String pageSize, AdministrativePracticeTypeEnum type ) {
        LinkedMultiValueMap<String, String> queryParam = new LinkedMultiValueMap<String, String>();
        if(type != null){
            queryParam.add("type", type.getValue());
        }
        return this.buildUrl(
                authorityEndpoint,
                authorityAdministrative,
                new HashMap<String, String>() {{
                    put("plate_chassis", plateOrChassis);
                }},
                queryParam
        );
    }

    public PaginationAuthority<WorkingAdministrationResponseV1> getPracticeAdministrative(String plateOrChassis, String orderBy, Boolean asc, Integer page, Integer pageSize, String userId, String nemoProfiles, AdministrativePracticeTypeEnum type) throws IOException {

        String url = getURLPracticeAdministrative(plateOrChassis, orderBy, asc.toString(), page.toString(), pageSize.toString(), type);
        HashMap<String, String> header = new HashMap<String, String>() {{
            put(nemoUserId, userId);
            put(nemoProfilesTree, nemoProfiles);
        }};

        System.out.println(url);
        return callWithoutCert(url, HttpMethod.GET, null, header, new ParameterizedTypeReference<PaginationAuthority<WorkingAdministrationResponseV1>>() {
        });
    }


    public PracticeEntity associateProcessingAdministrative(UUID uuid, List<ProcessingRequestV1> processingRequestV1s) throws IOException {
        PracticeEntity practiceEntity = practiceRepository.getOne(uuid);
        List<Processing> processings = ProcessingAdapter.adptProcessingRequestToProcessing(processingRequestV1s);

        List<ProcessingTypeEnum> processingTypeEnums = new ArrayList<>();
        List<Processing> processingPractice = practiceEntity.getProcessingList();
        if(processingPractice != null && !processingPractice.isEmpty()){
            for(Processing processing : processingPractice){
                processingTypeEnums.add(processing.getProcessingType());
            }
        } else {
            processingPractice = new ArrayList<>();
        }

        //CREAZIONE REQUEST PER AUTHORITY
        AdministrativeAssociationRequestV1 administrativeAssociationRequestV1 = new AdministrativeAssociationRequestV1();
        administrativeAssociationRequestV1.setItems(new ArrayList<>());

        //CREAZIONE REQUEST DELLA PRATICA AUTO
        CarPracticeRequestV1 carPracticeRequestV1 = new CarPracticeRequestV1();
        carPracticeRequestV1.setId(practiceEntity.getId().toString());
        carPracticeRequestV1.setPracticeId(practiceEntity.getPracticeId());

        //CREARE ADAPTER PER I TYPE
        carPracticeRequestV1.setPracticeType(EnumAdapter.adptFromPracticeTypeToCarPracticeType(practiceEntity.getPracticeType()));
        carPracticeRequestV1.setWorkingsList(new ArrayList<>());
        for(Processing processing : processings){
            if(processing.getWorkingId() != null) {
                //CREAZIONE DELLA LISTA DI WORKING DA AGGIUNGERE ALLA RICHIESTA DELLA PRATICA AUTO "CAR PRACTICE REQUEST"
                AuthorityWorkingListRequestV1 authorityWorkingListRequestV1 = new AuthorityWorkingListRequestV1();
                authorityWorkingListRequestV1.setType(new ArrayList<>());
                //CONTROLLO SE QUEL PRACTICE E' GIA' STATO INSERITO
                if (!carPracticeRequestV1.getWorkingsList().isEmpty()) {
                    for (AuthorityWorkingListRequestV1 authorityWorkingListRequestV11 : carPracticeRequestV1.getWorkingsList()) {
                        if (authorityWorkingListRequestV11.getWorkingId().equals(processing.getWorkingId())) {
                            authorityWorkingListRequestV1 = authorityWorkingListRequestV11;
                        }
                    }
                    List<AuthorityWorkingListRequestV1> authorityWorkingListRequestV1s = carPracticeRequestV1.getWorkingsList();
                    authorityWorkingListRequestV1s.removeIf(authorityWorkingListRequestV11 -> authorityWorkingListRequestV11.getWorkingId().equals(processing.getWorkingId()));
                    carPracticeRequestV1.setWorkingsList(authorityWorkingListRequestV1s);
                }
                //SET DEI CAMPI DELLA PRATICA AMMINISTRATIVA
                authorityWorkingListRequestV1.setWorkingId(processing.getWorkingId());
                authorityWorkingListRequestV1.setWorkingType(processing.getWorkingType().getValue());
                List<AdministrativePracticeTypeEnum> practiceTypeEnums = authorityWorkingListRequestV1.getType();
                practiceTypeEnums.add(EnumAdapter.adptFromProcessingTypeToAdministrativePracticeType(processing.getProcessingType()));
                authorityWorkingListRequestV1.setType(practiceTypeEnums);
                //AGGIUNTA DEL WORKING NELLA RICHIESTA DEI DETTAGLI DELLE PRATICHE
                List<AuthorityWorkingListRequestV1> authorityWorkingListRequestV1s = carPracticeRequestV1.getWorkingsList();
                authorityWorkingListRequestV1s.add(authorityWorkingListRequestV1);
                carPracticeRequestV1.setWorkingsList(authorityWorkingListRequestV1s);

            }

            if(!processingTypeEnums.contains(processing.getProcessingType())){
                processingPractice.add(processing);
            } else {
                Processing processing1 = null;
                Iterator<Processing> processingIterator = processingPractice.iterator();
                boolean isFound = false;
                while (processingIterator.hasNext() && !isFound){
                    Processing processing2 = processingIterator.next();
                    if(processing2.getProcessingType().equals(processing.getProcessingType())){
                        processing1 = processing2;
                        isFound = true;
                    }
                }

                if(isFound){
                    processingPractice.removeIf(processing2 -> processing2.getProcessingType().equals(processing.getProcessingType()));
                    processing1.setProcessingDate(processing.getProcessingDate());
                    processing1.setProcessingType(processing.getProcessingType());
                    processing1.setProcessingAuthorityDate(processing.getProcessingAuthorityDate());
                    processing1.setCommodityDetails(processing.getCommodityDetails());
                    processing1.setWorkingNumber(processing.getWorkingNumber());
                    processing1.setDossierNumber(processing.getDossierNumber());
                    processing1.setWorkingId(processing.getWorkingId());
                    processing1.setDossierId(processing.getDossierId());
                    processing1.setWorkingType(processing.getWorkingType());
                    processing1.setStatus(processing.getStatus());
                    processingPractice.add(processing1);
                }
            }

            List<PracticeEntity> practiceAlreadyAssociated = practiceRepositoryV1.getPracticeAlreadyAssociatedWithoutOne(processing.getDossierId(), processing.getWorkingId(), practiceEntity.getId().toString());
            if(practiceAlreadyAssociated != null && !practiceAlreadyAssociated.isEmpty()){
                for(PracticeEntity practiceEntity1 : practiceAlreadyAssociated){
                    Boolean alreadyAdded = false;
                    List<CarPracticeRequestV1> carPracticeRequestV1List = administrativeAssociationRequestV1.getItems();
                    CarPracticeRequestV1 carPracticeRequestV11 = new CarPracticeRequestV1();
                    carPracticeRequestV11.setId(practiceEntity1.getId().toString());
                    carPracticeRequestV11.setPracticeId(practiceEntity1.getPracticeId());

                    //CREARE ADAPTER PER I TYPE
                    carPracticeRequestV11.setPracticeType(EnumAdapter.adptFromPracticeTypeToCarPracticeType(practiceEntity1.getPracticeType()));
                    carPracticeRequestV11.setWorkingsList(new ArrayList<>());

                    if(carPracticeRequestV1List != null && !carPracticeRequestV1List.isEmpty()){
                        for(CarPracticeRequestV1 carPracticeRequestV12 : carPracticeRequestV1List){
                            if(carPracticeRequestV12.getId().equals(practiceEntity1.getId().toString())){
                                carPracticeRequestV11 = carPracticeRequestV12;
                                alreadyAdded = true;
                            }
                        }
                    }

                    List<Processing> processingsList = practiceEntity1.getProcessingList();

                    for(Processing processing1 : processingsList){
                        //CREAZIONE DELLA LISTA DI WORKING DA AGGIUNGERE ALLA RICHIESTA DELLA PRATICA AUTO "CAR PRACTICE REQUEST"
                        if(processing1.getWorkingId() != null && processing1.getWorkingId().equals(processing.getWorkingId())){
                            AuthorityWorkingListRequestV1 authorityWorkingListRequestV11 = new AuthorityWorkingListRequestV1();
                            authorityWorkingListRequestV11.setType(new ArrayList<>());
                            if(!carPracticeRequestV11.getWorkingsList().isEmpty()){
                                for(AuthorityWorkingListRequestV1 authorityWorkingListRequestV111 : carPracticeRequestV11.getWorkingsList()){
                                    if(authorityWorkingListRequestV111.getWorkingId().equals(processing1.getWorkingId())){
                                        authorityWorkingListRequestV11 = authorityWorkingListRequestV111;
                                    }
                                }
                                List<AuthorityWorkingListRequestV1> authorityWorkingListRequestV1s = carPracticeRequestV11.getWorkingsList();
                                authorityWorkingListRequestV1s.removeIf(authorityWorkingListRequestV111 -> authorityWorkingListRequestV111.getWorkingId().equals(processing1.getWorkingId()));
                                carPracticeRequestV11.setWorkingsList(authorityWorkingListRequestV1s);
                            }
                            //SET DEI CAMPI DELLA PRATICA AMMINISTRATIVA
                            authorityWorkingListRequestV11.setWorkingId(processing1.getWorkingId());
                            authorityWorkingListRequestV11.setWorkingType(processing1.getWorkingType().getValue());
                            List<AdministrativePracticeTypeEnum> practiceTypeEnums = authorityWorkingListRequestV11.getType();
                            if(!practiceTypeEnums.contains(EnumAdapter.adptFromProcessingTypeToAdministrativePracticeType(processing1.getProcessingType()))) {
                                practiceTypeEnums.add(EnumAdapter.adptFromProcessingTypeToAdministrativePracticeType(processing1.getProcessingType()));
                                authorityWorkingListRequestV11.setType(practiceTypeEnums);
                            }

                            //AGGIUNTA DEL WORKING NELLA RICHIESTA DEI DETTAGLI DELLE PRATICHE
                            List<AuthorityWorkingListRequestV1> authorityWorkingListRequestV1s = carPracticeRequestV11.getWorkingsList();
                            authorityWorkingListRequestV1s.add(authorityWorkingListRequestV11);
                            carPracticeRequestV11.setWorkingsList(authorityWorkingListRequestV1s);
                        }
                    }
                    if(!alreadyAdded){
                        List<CarPracticeRequestV1> carPracticeRequestV1List1 = administrativeAssociationRequestV1.getItems();
                        carPracticeRequestV1List1.add(carPracticeRequestV11);
                        administrativeAssociationRequestV1.setItems(carPracticeRequestV1List1);
                    }
                }
            }
        }
        if(!carPracticeRequestV1.getWorkingsList().isEmpty()){
            List<CarPracticeRequestV1> carPracticeRequestV1s = administrativeAssociationRequestV1.getItems();
            carPracticeRequestV1s.add(carPracticeRequestV1);
            administrativeAssociationRequestV1.setItems(carPracticeRequestV1s);
        }
        //CHIAMARE AUTHORITY
        ObjectMapper objectMapper = new ObjectMapper();
        //System.out.println("Request:" +objectMapper.writeValueAsString(administrativeAssociationRequestV1));

        if(!administrativeAssociationRequestV1.getItems().isEmpty()) {
            associateProcessingWithAdministrative(administrativeAssociationRequestV1);
        }
        //SE AUTHORITY VA A BUON FINE SALVO LA LISTA
        practiceEntity.setProcessingList(processingPractice);
        practiceRepository.save(practiceEntity);
        return practiceRepository.getOne(uuid);
    }

    public AdministrativeResponseV1 associateProcessingWithAdministrative(AdministrativeAssociationRequestV1 requestV1) throws IOException {

        String url = getURLAssociationAdministrative();

        return callWithoutCert(url, HttpMethod.POST, requestV1, null,new ParameterizedTypeReference<AdministrativeResponseV1>() {
        });
    }

    public String getURLAssociationAdministrative(){
        return this.buildUrl(
                authorityEndpoint,
                authorityAdministrativeAssociation,
                new HashMap<String, String>() ,
                null
        );
    }

    public PracticeEntity disassociateProcessingAdministrative(UUID uuid, List<ProcessingRequestV1> processingRequestV1s) throws IOException {
        PracticeEntity practiceEntity = practiceRepository.getOne(uuid);
        List<Processing> processings = ProcessingAdapter.adptProcessingRequestToProcessing(processingRequestV1s);
        Map<String, List<ProcessingTypeEnum>> map = new HashMap<>();
        List<ProcessingTypeEnum> processingTypeEnums;
        List<Processing> processingPractice = practiceEntity.getProcessingList();
        for(Processing processing : processings){
            if(map.get(processing.getWorkingId()) == null){
                processingTypeEnums = new ArrayList<>();
            }
            else {
                processingTypeEnums = map.get(processing.getWorkingId());
            }
            processingTypeEnums.add(processing.getProcessingType());
            map.put(processing.getWorkingId(), processingTypeEnums);
        }

        //CREAZIONE REQUEST PER AUTHORITY
        AdministrativeAssociationRequestV1 administrativeAssociationRequestV1 = new AdministrativeAssociationRequestV1();
        administrativeAssociationRequestV1.setItems(new ArrayList<>());

        //CREAZIONE REQUEST DELLA PRATICA AUTO
        CarPracticeRequestV1 carPracticeRequestV1 = new CarPracticeRequestV1();
        carPracticeRequestV1.setId(practiceEntity.getId().toString());
        carPracticeRequestV1.setPracticeId(practiceEntity.getPracticeId());

        //CREARE ADAPTER PER I TYPE
        carPracticeRequestV1.setPracticeType(EnumAdapter.adptFromPracticeTypeToCarPracticeType(practiceEntity.getPracticeType()));
        carPracticeRequestV1.setWorkingsList(new ArrayList<>());


        for(Processing processing : processingPractice){
            if(processing.getWorkingId() != null){
                //CREAZIONE DELLA LISTA DI WORKING DA AGGIUNGERE ALLA RICHIESTA DELLA PRATICA AUTO "CAR PRACTICE REQUEST"
                AuthorityWorkingListRequestV1 authorityWorkingListRequestV1 = new AuthorityWorkingListRequestV1();
                //SET DEI CAMPI DELLA PRATICA AMMINISTRATIVA
                authorityWorkingListRequestV1.setWorkingId(processing.getWorkingId());
                authorityWorkingListRequestV1.setWorkingType(processing.getWorkingType().getValue());
                if(!carPracticeRequestV1.getWorkingsList().isEmpty()){
                    for(AuthorityWorkingListRequestV1 authorityWorkingListRequestV11 : carPracticeRequestV1.getWorkingsList()){
                        if(authorityWorkingListRequestV11.getWorkingId().equals(processing.getWorkingId())){
                            authorityWorkingListRequestV1 = authorityWorkingListRequestV11;
                        }
                    }
                    List<AuthorityWorkingListRequestV1> authorityWorkingListRequestV1s = carPracticeRequestV1.getWorkingsList();
                    authorityWorkingListRequestV1s.removeIf(authorityWorkingListRequestV11 -> authorityWorkingListRequestV11.getWorkingId().equals(processing.getWorkingId()));
                    carPracticeRequestV1.setWorkingsList(authorityWorkingListRequestV1s);
                }
                if(authorityWorkingListRequestV1.getType() == null){
                    authorityWorkingListRequestV1.setType(new ArrayList<>());
                }
                if(map.get(processing.getWorkingId()) != null && !(map.get(processing.getWorkingId()).contains(processing.getProcessingType()))){
                    List<AdministrativePracticeTypeEnum> practiceTypeEnums = authorityWorkingListRequestV1.getType();
                    practiceTypeEnums.add(EnumAdapter.adptFromProcessingTypeToAdministrativePracticeType(processing.getProcessingType()));
                    authorityWorkingListRequestV1.setType(practiceTypeEnums);
                }
                if(!authorityWorkingListRequestV1.getType().isEmpty()){
                    List<AuthorityWorkingListRequestV1> authorityWorkingListRequestV1s = carPracticeRequestV1.getWorkingsList();
                    authorityWorkingListRequestV1s.add(authorityWorkingListRequestV1);
                    carPracticeRequestV1.setWorkingsList(authorityWorkingListRequestV1s);
                }

            }
        }


        //LAVORAZIONI DA ELIMINARE
        for(Processing processing : processings){
            if(processing.getWorkingId() == null){
                LOGGER.debug(MessageCode.CLAIMS_1129.value());
                throw new BadRequestException(MessageCode.CLAIMS_1129);
            }
            processingPractice.removeIf(processing1 -> processing.getWorkingId().equals(processing1.getWorkingId()) && processing.getProcessingType().equals(processing1.getProcessingType()));

            List<PracticeEntity> practiceAlreadyAssociated = practiceRepositoryV1.getPracticeAlreadyAssociatedWithoutOne(processing.getDossierId(), processing.getWorkingId(), practiceEntity.getId().toString());
            if(practiceAlreadyAssociated != null && !practiceAlreadyAssociated.isEmpty()){
                for(PracticeEntity practiceEntity1 : practiceAlreadyAssociated){
                    Boolean alreadyAdded = false;
                    List<CarPracticeRequestV1> carPracticeRequestV1List = administrativeAssociationRequestV1.getItems();
                    CarPracticeRequestV1 carPracticeRequestV11 = new CarPracticeRequestV1();
                    carPracticeRequestV11.setId(practiceEntity1.getId().toString());
                    carPracticeRequestV11.setPracticeId(practiceEntity1.getPracticeId());

                    //CREARE ADAPTER PER I TYPE
                    carPracticeRequestV11.setPracticeType(EnumAdapter.adptFromPracticeTypeToCarPracticeType(practiceEntity1.getPracticeType()));
                    carPracticeRequestV11.setWorkingsList(new ArrayList<>());

                    if(carPracticeRequestV1List != null && !carPracticeRequestV1List.isEmpty()){
                        for(CarPracticeRequestV1 carPracticeRequestV12 : carPracticeRequestV1List){
                            if(carPracticeRequestV12.getId().equals(practiceEntity1.getId().toString())){
                                carPracticeRequestV11 = carPracticeRequestV12;
                                alreadyAdded = true;
                            }
                        }
                    }

                    List<Processing> processingsList = practiceEntity1.getProcessingList();

                    for(Processing processing1 : processingsList){
                        //CREAZIONE DELLA LISTA DI WORKING DA AGGIUNGERE ALLA RICHIESTA DELLA PRATICA AUTO "CAR PRACTICE REQUEST"
                        if(processing1.getWorkingId() != null && processing1.getWorkingId().equals(processing.getWorkingId())){
                            AuthorityWorkingListRequestV1 authorityWorkingListRequestV11 = new AuthorityWorkingListRequestV1();
                            authorityWorkingListRequestV11.setType(new ArrayList<>());
                            if(!carPracticeRequestV11.getWorkingsList().isEmpty()){
                                for(AuthorityWorkingListRequestV1 authorityWorkingListRequestV111 : carPracticeRequestV11.getWorkingsList()){
                                    if(authorityWorkingListRequestV111.getWorkingId().equals(processing1.getWorkingId())){
                                        authorityWorkingListRequestV11 = authorityWorkingListRequestV111;
                                    }
                                }
                                List<AuthorityWorkingListRequestV1> authorityWorkingListRequestV1s = carPracticeRequestV11.getWorkingsList();
                                authorityWorkingListRequestV1s.removeIf(authorityWorkingListRequestV111 -> authorityWorkingListRequestV111.getWorkingId().equals(processing1.getWorkingId()));
                                carPracticeRequestV11.setWorkingsList(authorityWorkingListRequestV1s);
                            }
                            //SET DEI CAMPI DELLA PRATICA AMMINISTRATIVA
                            authorityWorkingListRequestV11.setWorkingId(processing1.getWorkingId());
                            authorityWorkingListRequestV11.setWorkingType(processing1.getWorkingType().getValue());
                            List<AdministrativePracticeTypeEnum> practiceTypeEnums = authorityWorkingListRequestV11.getType();
                            if(!practiceTypeEnums.contains(EnumAdapter.adptFromProcessingTypeToAdministrativePracticeType(processing1.getProcessingType()))) {
                                practiceTypeEnums.add(EnumAdapter.adptFromProcessingTypeToAdministrativePracticeType(processing1.getProcessingType()));
                                authorityWorkingListRequestV11.setType(practiceTypeEnums);
                            }

                            //AGGIUNTA DEL WORKING NELLA RICHIESTA DEI DETTAGLI DELLE PRATICHE
                            List<AuthorityWorkingListRequestV1> authorityWorkingListRequestV1s = carPracticeRequestV11.getWorkingsList();
                            authorityWorkingListRequestV1s.add(authorityWorkingListRequestV11);
                            carPracticeRequestV11.setWorkingsList(authorityWorkingListRequestV1s);
                        }
                    }
                    if(!alreadyAdded ){
                        List<CarPracticeRequestV1> carPracticeRequestV1s = administrativeAssociationRequestV1.getItems();
                        carPracticeRequestV1s.add(carPracticeRequestV11);
                        administrativeAssociationRequestV1.setItems(carPracticeRequestV1s);
                    }

                }
            } else {
                boolean isFound = false;
                for(AuthorityWorkingListRequestV1 workingListRequestV1 : carPracticeRequestV1.getWorkingsList()){
                    if(workingListRequestV1.getWorkingId().equals(processing.getWorkingId())){
                        isFound = true;
                    }
                }
                if(!isFound){
                    CarPracticeRequestV1 practiceDeleted = new CarPracticeRequestV1();
                    practiceDeleted.setId(null);
                    practiceDeleted.setPracticeId(0l);

                    //CREARE ADAPTER PER I TYPE
                    practiceDeleted.setPracticeType(EnumAdapter.adptFromPracticeTypeToCarPracticeType(practiceEntity.getPracticeType()));
                    practiceDeleted.setWorkingsList(new ArrayList<>());
                    for(String working : map.keySet()){
                        if(working.equals(processing.getWorkingId())) {
                            List<ProcessingTypeEnum> processingTypeEnums1 = map.get(working);
                            List<CarPracticeRequestV1> carPracticeRequestV1List = administrativeAssociationRequestV1.getItems();
                            if(carPracticeRequestV1List != null && !carPracticeRequestV1List.isEmpty()){
                                for(CarPracticeRequestV1 carPracticeRequestV12 : carPracticeRequestV1List){
                                    if(carPracticeRequestV12.getId() == null){
                                        practiceDeleted = carPracticeRequestV12;
                                    }
                                }
                            }
                            AuthorityWorkingListRequestV1 authorityWorkingListRequestV11 = new AuthorityWorkingListRequestV1();
                            authorityWorkingListRequestV11.setWorkingType("ADMINISTRATION");
                            authorityWorkingListRequestV11.setWorkingId(working);
                            authorityWorkingListRequestV11.setType(EnumAdapter.adptFromProcessingTypeToAdministrativePracticeType(processingTypeEnums1));
                            List<AuthorityWorkingListRequestV1> authorityWorkingListRequestV1s =  practiceDeleted.getWorkingsList();
                            authorityWorkingListRequestV1s.add(authorityWorkingListRequestV11);
                            practiceDeleted.setWorkingsList(authorityWorkingListRequestV1s);
                        }
                    }
                    if(!practiceDeleted.getWorkingsList().isEmpty() && !administrativeAssociationRequestV1.getItems().contains(practiceDeleted)){
                        List<CarPracticeRequestV1> carPracticeRequestV1s = administrativeAssociationRequestV1.getItems();
                        carPracticeRequestV1s.add(practiceDeleted);
                        administrativeAssociationRequestV1.setItems(carPracticeRequestV1s);
                    }
                }
            }
        }

        if(!carPracticeRequestV1.getWorkingsList().isEmpty()){
            List<CarPracticeRequestV1> carPracticeRequestV1s = administrativeAssociationRequestV1.getItems();
            carPracticeRequestV1s.add(carPracticeRequestV1);
            administrativeAssociationRequestV1.setItems(carPracticeRequestV1s);
        }

        ObjectMapper objectMapper = new ObjectMapper();
        //System.out.println("Request:" +objectMapper.writeValueAsString(administrativeAssociationRequestV1));
        if(!administrativeAssociationRequestV1.getItems().isEmpty()){

            associateProcessingWithAdministrative(administrativeAssociationRequestV1);
        }

        //SE AUTHORITY VA A BUON FINE SALVO LA LISTA
        practiceEntity.setProcessingList(processingPractice);
        practiceRepository.save(practiceEntity);
        return practiceRepository.getOne(uuid);
    }
}
