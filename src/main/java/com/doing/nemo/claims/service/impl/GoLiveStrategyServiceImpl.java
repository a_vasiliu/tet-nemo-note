package com.doing.nemo.claims.service.impl;


import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.enumerated.WorkabilitySystemEnum;
import com.doing.nemo.claims.entity.jsonb.Authority;
import com.doing.nemo.claims.service.ClaimsPendingService;
import com.doing.nemo.claims.service.GoLiveStrategyService;

import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.LinkedList;
import java.util.List;


@Service
public class GoLiveStrategyServiceImpl implements GoLiveStrategyService {

    private static Logger LOGGER = LoggerFactory.getLogger(GoLiveStrategyServiceImpl.class);


    @Value("#{T(java.util.Arrays).asList('${go_live_step_minus_one:}')}")
    private List<String> stepMinusOne;

    @Value("#{T(java.util.Arrays).asList('${go_live_step_zero:}')}")
    private List<String> stepZero;
    @Value("#{T(java.util.Arrays).asList('${go_live_step_one:}')}")
    private List<String> stepOne;
    @Value("#{T(java.util.Arrays).asList('${go_live_step_two:}')}")
    private List<String> stepTwo;
    @Value("${go_live_step_zero_date}")
    private String goLiveStepZeroDate;
    @Value("${go_live_pending_check}")
    private Boolean goLivePendingResponse;

    @Value("#{T(java.util.Arrays).asList('${claims.external.nemo-profiles-tree:}')}")
    private List<String> claimsExternalProfiles;

    @Autowired
    private ClaimsPendingService claimsPendingService;

    @Override
    public void goLiveStrategyChecksForInsert(String customerId, DataAccidentTypeAccidentEnum typeAccidentEnum) {
        goLiveStrategyChecksForInsert( customerId,  typeAccidentEnum, null);
    }

    @Override
    public void goLiveStrategyChecksForInsert(String customerId, DataAccidentTypeAccidentEnum typeAccidentEnum, String profile) {
        if (this.checkIfStepMinusOneOrZero()) {
            Boolean isAuthorized = this.checkIfCustomerIsAuthorized(customerId);
            if(this.checkIfStepMinusOne()){
                LOGGER.info("[GO LIVE STRATEGY] Step -1: It is possible to enter claims on NEMO only for authorized customers.");
                if(isAuthorized){
                    LOGGER.info("[GO LIVE STRATEGY] Step -1: Customer "+customerId+" it's authorized to insert claims on NEMO.");
                } else {
                    LOGGER.info("[GO LIVE STRATEGY] Step -1: Customer "+customerId+" it's not authorized to insert claims on NEMO.");
                    throw new BadRequestException("[GO LIVE STRATEGY] Step -1: Customer "+customerId+" it's not authorized to insert claims on NEMO.", MessageCode.CLAIMS_2010);
                }
            } else if(this.checkIfStepZero()){
                if(isAuthorized){
                    LOGGER.info("[GO LIVE STRATEGY] Step -0: Customer "+customerId+" it's authorized to insert claims on NEMO.");
                } else{
                    if(profile!=null && claimsExternalProfiles.contains(profile)){
                        LOGGER.info("[GO LIVE STRATEGY] Step -1: Customer "+customerId+" it's not authorized to insert claims on NEMO.");
                        throw new BadRequestException("[GO LIVE STRATEGY] Step -1: Customer "+customerId+" it's not authorized to insert claims on NEMO.", MessageCode.CLAIMS_2010);
                    }
                    this.checkIfThrowExceptionForStepZeroInsert(typeAccidentEnum,customerId);
                }
            }
        } else {
            LOGGER.info("[GO LIVE STRATEGY] The controls of step -1 and 0 are disabled. It's possibile to enter claims on NEMO for all customers and type accident.");
        }

    }

    @Override
    public void goLiveStrategyChecksForMSA(String customerId) {
        if (this.checkIfStepMinusOneOrZero()
            //&& this.checkIfStepMinusOne() Per MSA i controlli sui customer abilitati sono validi sia nello step -1 che nello step 0. Non piu validi da 1 in poi.
        ) {
            Boolean isAuthorized = this.checkIfCustomerIsAuthorized(customerId);
            LOGGER.info("[GO LIVE STRATEGY] Step -1: It is possible to enter claims on NEMO only for authorized customers.");
            if (isAuthorized) {
                LOGGER.info("[GO LIVE STRATEGY] Step -1: Customer " + customerId + " it's authorized to insert claims on NEMO.");
            } else {
                LOGGER.info("[GO LIVE STRATEGY] Step -1: Customer " + customerId + " it's not authorized to insert claims on NEMO.");
                throw new BadRequestException("[GO LIVE STRATEGY] Step -1: Customer " + customerId + " it's not authorized to insert claims on NEMO.", MessageCode.CLAIMS_2010);
            }
        } else {
            LOGGER.info("[GO LIVE STRATEGY] The controls of step -1 is disabled. It's possibile to enter claims on NEMO for all customers and type accident.");
        }

    }

    /*
     * Se la lista dei customer è diversa dal vuoto e le liste degli step 1 e 2 sono vuote. Significa che sto nello step -1 oppure nello step 0.
     */
    private Boolean checkIfStepMinusOneOrZero() {
        return  (!CollectionUtils.isEmpty(stepMinusOne)) &&
                CollectionUtils.isEmpty(stepOne) &&
                CollectionUtils.isEmpty(stepTwo);
    }

    /*
     * Se la lista dei customer è diversa dal vuoto ma tutte le altre sono vuote allora sto nello step -1.
     */
    @Override
    public Boolean checkIfStepMinusOne(){
        return  (!CollectionUtils.isEmpty(stepMinusOne)) &&
                CollectionUtils.isEmpty(stepZero) &&
                CollectionUtils.isEmpty(stepOne) &&
                CollectionUtils.isEmpty(stepTwo);
    }

    /*
     * Se la lista dei customer e la lista dello step 0 sono diversw dal vuoto ma tutte le altre sono vuote allora sto nello step -0.
     */
    @Override
    public Boolean checkIfStepZero(){
        return  (!CollectionUtils.isEmpty(stepMinusOne)) &&
                (!CollectionUtils.isEmpty(stepZero)) &&
                CollectionUtils.isEmpty(stepOne) &&
                CollectionUtils.isEmpty(stepTwo);
    }

    /*
     * Se la lista dei customer,step 0 e step 1 sono diverse dal vuoto e quella dello step 2 è vuota, sto nello step 1.
     */
    @Override
    public Boolean checkIfStepOne(){
        return  (!CollectionUtils.isEmpty(stepMinusOne)) &&
                (!CollectionUtils.isEmpty(stepZero)) &&
                (!CollectionUtils.isEmpty(stepOne)) &&
                CollectionUtils.isEmpty(stepTwo);
    }

    /*
     * Se le liste sono tutte piene sto nello step 2.
     */
    @Override
    public Boolean checkIfStepTwo(){
        return  (!CollectionUtils.isEmpty(stepMinusOne)) &&
                (!CollectionUtils.isEmpty(stepZero)) &&
                (!CollectionUtils.isEmpty(stepOne)) &&
                (!CollectionUtils.isEmpty(stepTwo));
    }

    /*
     * Se le liste sono tutte vuote sto nello step 3.
     */
    @Override
    public Boolean checkIfStepThree(){
        return  CollectionUtils.isEmpty(stepMinusOne) &&
                CollectionUtils.isEmpty(stepZero) &&
                CollectionUtils.isEmpty(stepOne) &&
                CollectionUtils.isEmpty(stepTwo);
    }

    /*
     *Verifico se il customer è abilitato o meno.
     */
    private Boolean checkIfCustomerIsAuthorized (String customerId){
        return stepMinusOne.contains(customerId);
    }

    /*
     *Se stiamo nello step 0 e l'utente non è abilitato e sta tentando di inserire un sinistro della tipologia non consentita,
     * allora è necessario sollevare un eccezione al chiamante. Se invece stiamo verificando lo step durante la chiamata ad ESB,
     * logghiamo soltanto che il cliente non sarebbe abilitato ma non lanceremo alcuna eccezione, altrimenti il chiamante non potrebbe
     * mai ricevere la response di ESB.
     */
    private void checkIfThrowExceptionForStepZeroInsert(DataAccidentTypeAccidentEnum typeAccidentEnum, String customerId){
        if(this.checkIfTypeAccidentIsAuthorized(stepZero,typeAccidentEnum)){
            LOGGER.info("[GO LIVE STRATEGY] Step 0: Customer "+customerId+" it's authorized to insert only "+ stepZero +"  claims on NEMO.");
        } else {
            throw new BadRequestException("[GO LIVE STRATEGY] Step -0: Customer "+customerId+"  it's authorized to insert only "+ stepZero +" claims on NEMO.",MessageCode.CLAIMS_2011);
        }
    }

    /*
     *Verifico se la tipologia di sinistro è abilitata o meno.
     */
    private Boolean checkIfTypeAccidentIsAuthorized(List<String> stepList, DataAccidentTypeAccidentEnum typeAccidentEnum){
        if(typeAccidentEnum!=null && !CollectionUtils.isEmpty(stepList)){
            return stepList.contains(typeAccidentEnum.getValue());
        } else return true;
    }


    @Override
    public List<ClaimsEntity> goLiveStrategyChecksForAuthorityAssociation (List<ClaimsEntity> initialClaimsEntityList){
        List<ClaimsEntity> finalList = new LinkedList<>();
        if(!CollectionUtils.isEmpty(initialClaimsEntityList)){
            for(ClaimsEntity currentClaim: initialClaimsEntityList){
                String plate = currentClaim.getDamaged().getVehicle().getLicensePlate();
                String customerId = currentClaim.getDamaged().getCustomer().getCustomerId();
                DataAccidentTypeAccidentEnum typeAccident = currentClaim.getComplaint().getDataAccident().getTypeAccident();
                List<Authority> authorities = currentClaim.getAuthorities();
                if(this.checkIfIsAssociableForAuthority(authorities,currentClaim.getPracticeId(),customerId,typeAccident)){
                    finalList.add(currentClaim);
                }
            }
        }
        return finalList;
    }


    /*
     * Verifico dove il sinistro deve essere lavorato, assegnandogli un valore enumerato.
     * Intervento NE-1105 : STEP 2. se la classificazione è D, è necessaria una ulteriore verifica:
     *      1) se il sx fa riferimento ad un cliente in whitelist la classificazione passa da D a N
     *      2) se il sx è di tipo "furto e ritrovamento" la classificazzione deve passare da D a N
     *      3) in tutti gli altri la classificazione rimane D
     *
     * Se ci sono lavorazioni pending/pregresse su Winity, a prescindere dello step ritorno sempre W.
     * Se NON ci sono lavorazioni pending/pregresse su Winity, ci sono lavorazioni su NEMO-Authority e lo step permette la lavorazione ritorno N.
     * Se NON ci sono lavorazioni pending/pregresse su Winity e ci sono lavorazioni su NEMO-Authority ma lo step NON lo permette ritorno W ma è un caso che non si può verificare.
     * Se NON ci sono lavorazioni pending/pregresse su Winity, NON ci sono lavorazioni su NEMO-Authority e lo step lo permette ritorno D.
     * Se NON ci sono lavorazioni pending/pregresse su Winity, NON ci sono lavorazioni su NEMO-Authority e lo step NON lo permette ritorno W.
     * */
    @Override
    public WorkabilitySystemEnum goLiveStrategyChecksSystemForWorking (ClaimsEntity claimsEntity){
        String plate = claimsEntity.getDamaged().getVehicle().getLicensePlate();
        String customerId = claimsEntity.getDamaged().getCustomer().getCustomerId();
        DataAccidentTypeAccidentEnum typeAccident = claimsEntity.getComplaint().getDataAccident().getTypeAccident();
        List<Authority> authorities = claimsEntity.getAuthorities();
        return this.returnSystemForWorking(claimsEntity.getPracticeId(),customerId,typeAccident,authorities);
    }

    private WorkabilitySystemEnum returnSystemForWorking (Long practiceId, String customerId, DataAccidentTypeAccidentEnum typeAccident,List<Authority> authorities){
        WorkabilitySystemEnum workabilityResult = WorkabilitySystemEnum.D; //DEFAULT

        if (!this.isWorkableIntoNemoByPreviousWorkingOnWinity(authorities, practiceId)) {
            workabilityResult = WorkabilitySystemEnum.W; //WINITY
        } else if (!this.thereAreNotPreviousWorkingOnAuthority(authorities)) {
            if (this.isWorkableIntoNemoByStep(customerId, typeAccident)) {
                workabilityResult = WorkabilitySystemEnum.N; //NEMO
            } else {
                workabilityResult = WorkabilitySystemEnum.W; //WINITY
            }
        } else if (!this.isWorkableIntoNemoByStep(customerId, typeAccident)) {
            workabilityResult = WorkabilitySystemEnum.W; //WINITY
        }

        /*NE-1105*/
        if (workabilityResult.equals(WorkabilitySystemEnum.D) && this.checkIfStepTwo()) {
            if (this.getCustomerIdListToFilterStepMinusOneAndHigher().contains(customerId)){
                workabilityResult = WorkabilitySystemEnum.N; //NEMO
            }else if ((typeAccident.getValue().equalsIgnoreCase(DataAccidentTypeAccidentEnum.FURTO_E_RITROVAMENTO.getValue())||(typeAccident.getValue().equalsIgnoreCase(DataAccidentTypeAccidentEnum.FURTO_TOTALE.getValue())))){
                workabilityResult = WorkabilitySystemEnum.N; //NEMO
            }
        }
        /*NE-1105*/

        return workabilityResult;
    }

    private Boolean checkIfIsAssociableForAuthority (List<Authority> authorities, Long practiceId, String customerId,DataAccidentTypeAccidentEnum typeAccident){
        return
                this.isWorkableIntoNemoByPreviousWorkingOnWinity(authorities, practiceId) && //non ci sono lavorazioni pregresse o pending
                        this.isWorkableIntoNemoByStep(customerId,typeAccident) //è lavorabile in NEMO nello step corrente
                ;
    }

    private Boolean isWorkableIntoNemoByPreviousWorkingOnWinity(List<Authority> authorities, Long practiceId){
        return
                this.thereAreNotPreviousWorkingOnWinity(authorities) && //non ci sono lavorazioni pregresse in winity
                        this.thereAreNotPendingWorking(practiceId)  //non ci sono lavorazioni pending in winity
                ;
    }

    private Boolean thereAreNotPreviousWorkingOnWinity(List<Authority> authorities){
        if(authorities!=null) {
            for (Authority authority : authorities) {
                if (authority.getAuthorityWorkingId() == null) {
                    return false;
                }
            }
        }
        return true;
    }

    private Boolean thereAreNotPreviousWorkingOnAuthority(List<Authority> authorities){
        if(authorities!=null) {
            for (Authority authority : authorities) {
                if (authority.getAuthorityWorkingId() != null) {
                    return false;
                }
            }
        }
        return true;
    }

    private Boolean thereAreNotPendingWorking (Long practiceId){
        return !claimsPendingService.checkIfExistsPendingWorking(practiceId);
    }

    private Boolean isWorkableIntoNemoByStep(String customerId, DataAccidentTypeAccidentEnum typeAccident){
        return
                this.checkIfStepThree() || //step 3 controlli non necessari
                        this.checkIfCustomerIsAuthorized(customerId) || //se il cliente è abilitato può modificare a prescindere dei prossimi step. Include controllo step -1.
                        (this.checkIfStepTwo() && this.checkIfTypeAccidentIsAuthorized(stepTwo,typeAccident)) || //step 2 e tipologia abilitata
                        (this.checkIfStepOne() && this.checkIfTypeAccidentIsAuthorized(stepOne,typeAccident)) || //step 1 e tipologia abilitata
                        (this.checkIfStepZero() && this.checkIfTypeAccidentIsAuthorized(stepZero,typeAccident)) //step 0 e tipologia abilitata
                ;
    }

    @Override
    public String getGoLiveStepZeroDate (){
        return goLiveStepZeroDate;
    }

    @Override
    public Boolean getGoLivePendingCheck(){
        return goLivePendingResponse;
    }

    @Override
    public List<String> getTypeAccidentListToFilterStepZero(){
        return stepZero;
    }

    @Override
    public List<String> getTypeAccidentListToFilterStepTwoOne(){
        List<String> typeAccidentList = null;
        if(this.checkIfStepTwo()){
            typeAccidentList = this.stepTwo;
        } else if(this.checkIfStepOne()){
            typeAccidentList = this.stepOne;
        }
        return typeAccidentList;
    }

    @Override
    public List<String> getCustomerIdListToFilterStepMinusOneAndHigher() {
        return stepMinusOne;
    }

}
