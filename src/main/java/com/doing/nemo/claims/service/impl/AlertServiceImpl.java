package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.controller.payload.request.AlertRequestV1;
import com.doing.nemo.claims.controller.payload.response.AlertResponseV1;
import com.doing.nemo.claims.service.AlertService;
import com.doing.nemo.commons.exception.AbstractException;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.exception.InternalException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import com.doing.nemo.commons.util.HttpUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashMap;

@Service
public class AlertServiceImpl implements AlertService {

    private static Logger LOGGER = LoggerFactory.getLogger(AntiTheftServiceServiceImpl.class);
    @Value("${alert.api.endpoint}")
    private String endpointUrl;
    @Autowired
    private HttpUtil httpUtil;

    private <T, P> T callWithoutCert(String url, HttpMethod method, P value, HashMap<String, String> headersparams, Class<T> outclass) throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = null;

        Response responseToken = null;
        try {

            if (value != null) {
                jsonInString = mapper.writeValueAsString(value);
            }
            responseToken = httpUtil.callURL(headersparams, url, jsonInString, method);

            if (responseToken.code() != HttpStatus.OK.value() && responseToken.code() != HttpStatus.CREATED.value()) {
                LOGGER.debug(responseToken.message() + " with code " + responseToken.code());
                throw new BadRequestException(responseToken.message() + " with code " + responseToken.code());
            }

            String response = responseToken.body().string();
            LOGGER.debug("Response Alert Body: {}", response);
            return mapper.readValue(response, outclass);
        } catch (AbstractException e) {
            LOGGER.error(e.getMessage(), e);
            throw e;
        }  catch (Exception e) {
            String message = String.format(MessageCode.CLAIMS_2012.value(), e.getClass().getSimpleName(), e.getMessage());
            LOGGER.error(message, e);
            throw new InternalException(message, MessageCode.CLAIMS_2012);
        } finally {
            if (responseToken != null && responseToken.body() != null) {
                responseToken.body().close();
            }
        }
    }

    @Override
    public AlertResponseV1 createAlert(AlertRequestV1 alertRequestV1) throws IOException {

        return callWithoutCert(endpointUrl, HttpMethod.POST, alertRequestV1, null, AlertResponseV1.class );
    }
}
