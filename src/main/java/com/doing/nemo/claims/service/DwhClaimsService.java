package com.doing.nemo.claims.service;

import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.CounterpartyEntity;
import com.doing.nemo.dwh.client.payload.request.EventTypeEnum;

public interface DwhClaimsService {
    void sendMessage(ClaimsEntity claimsEntity, EventTypeEnum eventTypeEnum);
    void sendMessage(CounterpartyEntity counterparty, EventTypeEnum eventTypeEnum);
}
