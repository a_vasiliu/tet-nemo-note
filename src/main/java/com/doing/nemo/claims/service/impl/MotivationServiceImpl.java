package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.controller.payload.response.MotivationResponseV1;
import com.doing.nemo.claims.entity.enumerated.ClaimsRepairEnum;
import com.doing.nemo.claims.entity.settings.MotivationEntity;
import com.doing.nemo.claims.repository.MotivationRepository;
import com.doing.nemo.claims.repository.MotivationRepositoryV1;
import com.doing.nemo.claims.service.MotivationService;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class MotivationServiceImpl implements MotivationService {

    private static Logger LOGGER = LoggerFactory.getLogger(MotivationServiceImpl.class);
    @Autowired
    private MotivationRepository motivationRepository;
    @Autowired
    private MotivationRepositoryV1 motivationRepositoryV1;

    @Override
    public MotivationEntity insertMotivation(MotivationEntity motivationEntity) {
        if (motivationRepository.searchMotivationbyAnswerAndDescription(motivationEntity.getAnswerType(), motivationEntity.getDescription()) != null) {
            LOGGER.debug("Error in Motivations, duplicate name with same description not admitted");
            throw new BadRequestException("Error in Motivations, duplicate name with same description not admitted", MessageCode.CLAIMS_1009);
        }
        motivationRepository.save(motivationEntity);
        return motivationEntity;
    }

    @Override
    public MotivationEntity updateMotivation(MotivationEntity motivationEntity) {
        if (motivationRepository.searchMotivationrWithDifferentId(motivationEntity.getId(), motivationEntity.getAnswerType(), motivationEntity.getDescription()) != null) {
            LOGGER.debug("Error in Motivations, duplicate name with same description not admitted");
            throw new BadRequestException("Error in Motivations, duplicate name with same description not admitted", MessageCode.CLAIMS_1009);
        }
        motivationRepository.save(motivationEntity);
        return motivationEntity;
    }

    @Override
    public MotivationEntity selectMotivation(UUID id) {
        Optional<MotivationEntity> motivationEntity = motivationRepository.findById(id);
        if (!motivationEntity.isPresent()) {
            LOGGER.debug("Motivation with id " + id + " not found");
            throw new NotFoundException("Motivation with id " + id + " not found", MessageCode.CLAIMS_1010);
        }

        return motivationEntity.get();
    }

    @Override
    public List<MotivationEntity> selectAllMotivation(ClaimsRepairEnum claimsRepairEnum) {
        List<MotivationEntity> resultListMotivation = motivationRepository.searchManagerByTypeComplaint(claimsRepairEnum);

        return resultListMotivation;
    }

    @Override
    public List<MotivationEntity> selectAllMotivationFilteredByIsActive(ClaimsRepairEnum claimsRepairEnum, Boolean isActive) {
        List<MotivationEntity> resultListMotivation =
                isActive == null ?
                selectAllMotivation(claimsRepairEnum) :
                motivationRepository.searchManagerByTypeComplaintAndIsActiveFilter(claimsRepairEnum, isActive);

        return resultListMotivation;
    }

    @Override
    public MotivationResponseV1 deleteMotivation(UUID id) {
        MotivationEntity motivationEntity = selectMotivation(id);
        motivationRepository.delete(motivationEntity);
        return null;
    }

    @Override
    public MotivationEntity updateStatus(UUID uuid) {
        MotivationEntity motivationEntity = selectMotivation(uuid);
        if (motivationEntity.getActive()) {
            motivationEntity.setActive(false);
        } else {
            motivationEntity.setActive(true);
        }

        motivationRepository.save(motivationEntity);
        return motivationEntity;
    }

    @Override
    public List<MotivationEntity> findMotivationsFiltered(Integer page, Integer pageSize, ClaimsRepairEnum claimsRepairEnum, String answerType, String description, Boolean isActive, Boolean asc, String orderBy) {
        return motivationRepositoryV1.findMotivationsFiltered(page, pageSize, claimsRepairEnum, answerType, description, isActive, asc, orderBy);
    }

    @Override
    public Long countMotivationFiltered(ClaimsRepairEnum claimsRepairEnum, String answerType, String description, Boolean isActive) {
        return motivationRepositoryV1.countMotivationFiltered(claimsRepairEnum, answerType, description, isActive);
    }
}