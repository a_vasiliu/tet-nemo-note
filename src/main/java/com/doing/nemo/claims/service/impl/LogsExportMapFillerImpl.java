package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.adapter.EnumAdapter;
import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.jsonb.Historical;
import com.doing.nemo.claims.entity.jsonb.damaged.Damaged;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.Customer;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Map;

@Service("LogsExportMapFillerImpl")
public class LogsExportMapFillerImpl extends ExportMapFiller {

    public Map<String, Object> log(
        Map<String, Object> innerMap,
        ClaimsEntity claimsEntity,
        Historical historical
    ) {
        SimpleDateFormat dateFormat = this.getDateFormat();
        // dd-MM-yyyy HH:mm
        SimpleDateFormat timestampFormat = this.getTimestampFormat();

        //AGGIORNATA IL
        String updatedAt = historical.getUpdateAt().toInstant().atZone( ZoneId.of("Europe/Rome") ).format( DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm") );

        //ID PRATICA
        innerMap.put("practice_id", claimsEntity.getPracticeId().toString());
        //TIPOLOGIA
        innerMap.put("type", claimsEntity.getDamaged().getContract().getContractType());
        //TIPOLOGIA FLUSSO
        innerMap.put("flow", claimsEntity.getType().getValue());
        //DATA SINISTRO
        Date dateAccident = claimsEntity.getComplaint().getDataAccident().getDateAccident();
        innerMap.put( "data_sx", dateFormat.format(dateAccident) );
        //DATI DA DAMAGED
        if ( claimsEntity.getDamaged() != null && claimsEntity.getDamaged().getVehicle() != null ) {
            innerMap.put("targa_danneggiato", claimsEntity.getDamaged().getVehicle().getLicensePlate());
        }

        // STORICO
        if(historical.getEventType() != null) {
            innerMap.put("event_type", EnumAdapter.adptFromEventTypeToItalian(historical.getEventType()));
        }
        if(historical.getStatusEntityOld() != null) {
            innerMap.put("old_status", EnumAdapter.translateClaimsStatus(historical.getStatusEntityOld()));
        }
        if(historical.getStatusEntityNew() != null) {
            innerMap.put("new_status", EnumAdapter.translateClaimsStatus(historical.getStatusEntityNew()));
        }
        if(historical.getOldType() != null) {
            innerMap.put("old_type", historical.getOldType().getValue());
        }
        if(historical.getNewType() != null) {
            innerMap.put("new_type", historical.getNewType().getValue());
        }
        innerMap.put("old_flow", historical.getOldFlow());
        innerMap.put("new_flow", historical.getNewFlow());
        innerMap.put("update_at", updatedAt);
        innerMap.put("user_name", historical.getUserName());
        if(historical.getComunicationDescription() != null && historical.getComunicationDescription().length() > 32765) {
            innerMap.put("communication_description", historical.getComunicationDescription().substring(0,32765));
        }
        else {
            innerMap.put("communication_description", historical.getComunicationDescription());
        }

        //DATA INSERIMENTO
        innerMap.put("inserita_il", timestampFormat.format( DateUtil.convertIS08601StringToUTCDate( DateUtil.convertUTCInstantToIS08601String(claimsEntity.getCreatedAt()) )) );

        //CLIENTE NOME
        if(claimsEntity.getDamaged() != null) {
            Damaged damaged = claimsEntity.getDamaged();
            if (damaged.getCustomer() != null) {
                Customer customer = damaged.getCustomer();
                innerMap.put("cliente_nome", customer.getLegalName());
            }
        }
        return innerMap;
    }

}
