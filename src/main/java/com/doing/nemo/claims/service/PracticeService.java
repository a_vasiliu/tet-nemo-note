package com.doing.nemo.claims.service;

import com.doing.nemo.claims.controller.payload.request.PlatesStatusRequestV1;
import com.doing.nemo.claims.controller.payload.response.practice.PracticeResponseV1;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.enumerated.AuthorityEnum.AuthorityPracticeTypeEnum;
import com.doing.nemo.claims.entity.enumerated.PracticeStatusEnum;
import com.doing.nemo.claims.entity.enumerated.PracticeTypeEnum;
import com.doing.nemo.claims.entity.jsonb.Theft;
import com.doing.nemo.claims.entity.jsonb.practice.Finding;
import com.doing.nemo.claims.entity.jsonb.practice.TheftPractice;
import com.doing.nemo.claims.entity.settings.PracticeEntity;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public interface PracticeService {

    PracticeEntity insertPractice(PracticeEntity practiceEntity) throws IOException;

    PracticeEntity updatePractice(PracticeEntity practiceEntity) throws IOException;

    PracticeEntity selectPractice(UUID id);

    List<PracticeEntity> selectAllPractice();

    PracticeResponseV1 deletePractice(UUID id);

    List<PracticeEntity> findPracticesFiltered (Integer page, Integer pageSize, Long practiceId, String practiceType, String vehiclePlate, String idContract, String statusList, String fromDate, String toDate , String orderBy, Boolean asc, String fleetVehicleId);
    Long countPracticeFiltered(Long practiceId, String practiceType, String vehiclePlate, String idContract, String statusList, String fromDate, String toDate, String fleetVehicleId);
    PracticeEntity patchTypePractice (UUID id, PracticeTypeEnum type, String userId) throws IOException;
    PracticeEntity patchClaimPractice (UUID id, UUID claimId) throws IOException;
    PracticeEntity patchStatusPractice (UUID id, PracticeStatusEnum newStatus);
    List<PracticeEntity> findPracticesPlateAndClaims (String plate, UUID claimsId);
    List<PracticeEntity> findPracticesPlateAndClaimsOr (String plate, UUID claimsId);
    List<PracticeEntity> findPracticesPlateClaimsTypeDate (String plate, UUID claimsId, PracticeTypeEnum practiceTypeEnum, Date dateClaims, PracticeStatusEnum practiceStatusEnum, Boolean last);
    PracticeEntity patchTypePractice(UUID id, PracticeTypeEnum type, String userName, TheftPractice theft) throws IOException;
    PracticeEntity patchTypePractice(UUID id, PracticeTypeEnum type, String userName, Finding finding, TheftPractice theft) throws IOException;
    Pagination<PracticeEntity> getPracticePaginationAuthority(String plate, String chassis, int page, int pageSize, List<AuthorityPracticeTypeEnum> type);
    PracticeEntity insertPracticeByClaims(PracticeEntity practiceEntity) throws IOException;
    void lossPossessionPractice () throws IOException;
    void closeLossPossessionPracticeMarsnova(PlatesStatusRequestV1 requestV1);
    ClaimsEntity patchToTheftAndFinding(String claimsId, String userId, String userName, Theft findingRequest);
    }
