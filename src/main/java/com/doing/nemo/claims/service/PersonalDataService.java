package com.doing.nemo.claims.service;

import com.doing.nemo.claims.controller.payload.response.PersonalDataResponseV1;
import com.doing.nemo.claims.entity.settings.ContractEntity;
import com.doing.nemo.claims.entity.settings.CustomTypeRiskEntity;
import com.doing.nemo.claims.entity.settings.InsurancePolicyPersonalDataEntity;
import com.doing.nemo.claims.entity.settings.PersonalDataEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public interface PersonalDataService {
    PersonalDataEntity insertPersonalData(PersonalDataEntity personalDataEntity);

    PersonalDataEntity updatePersonalData(PersonalDataEntity personalDataEntity, UUID uuid);

    PersonalDataEntity selectPersonalData(UUID id);

    List<PersonalDataEntity> selectAllPersonalData();

    PersonalDataResponseV1 deletePersonalData(UUID id);

    PersonalDataEntity insertPersonalDataContract(List<ContractEntity> contractEntityList, UUID uuid);

    PersonalDataEntity insertPersonalDataCustomTypeRisk(List<CustomTypeRiskEntity> customTypeRiskEntityList, UUID uuid);

    PersonalDataEntity insertPersonalDataInsurancePolicyPersonalData(List<InsurancePolicyPersonalDataEntity> insurancePolicyPersonalDataEntityList, UUID uuid);

    PersonalDataEntity updateStatus(UUID uuid);

    //void saveEsbPersonalData(PersonalDataEntity personalDataEntity);

    PersonalDataEntity attachFatherToPersonalDataInsert (PersonalDataEntity personalDataEntity);

    PersonalDataEntity attachFatherToPersonalDataUpdate (PersonalDataEntity personalDataEntity, String precPersonalDataFather);

    List<PersonalDataEntity> findPersonalDatasFilteredAscName (Integer page, Integer pageSize, String orderBy, Boolean asc, Long personalCode, String personalGroup, String name, String city, String province, String phoneNumber, String fax, String vatNumber, Boolean isActive, String customerId);

    Long countPersonalDataFiltered(Long personalCode, String personalGroup, String name, String city, String province, String phoneNumber, String fax, String vatNumber, Boolean isActive, String customerId);

    PersonalDataEntity getPersonalDataAndUpdateFleetManagerList (UUID id);

}
