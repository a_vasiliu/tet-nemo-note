package com.doing.nemo.claims.service;


import com.doing.nemo.claims.controller.payload.response.SupplierResponseV1;
import com.doing.nemo.claims.entity.SupplierEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public interface SupplierService {
    SupplierEntity insertSupplier(SupplierEntity supplierEntity);

    SupplierEntity updateSupplier(SupplierEntity supplierEntity);

    SupplierEntity selectSupplier(UUID id);

    List<SupplierEntity> selectAllSupplier();

    SupplierResponseV1 deleteSupplier(UUID id);

    SupplierEntity updateStatus(UUID uuid);

    List<SupplierEntity> findSuppliersFiltered (Integer page, Integer pageSize, String orderBy, Boolean asc, String name, String email, Boolean isActive,String codSupplier);

    Long countSupplierFiltered(String name, String email,Boolean isActive,String codSupplier);

    }
