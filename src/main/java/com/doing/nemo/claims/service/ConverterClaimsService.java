package com.doing.nemo.claims.service;


import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.ClaimsNewEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public interface ConverterClaimsService {
    ClaimsNewEntity convertOldEntityToNewClaimsEntity(ClaimsEntity claimsEntity);
    ClaimsEntity wrapFromClaimsNewEntityToClaimsEntity(ClaimsNewEntity claimsNewEntity);
    @Async
    void convertClaims(Integer countTotalClaims);
    List<ClaimsEntity> wrapFromClaimsNewEntityListToClaimsEntityList(List<ClaimsNewEntity> claimsNewEntityList);

    ClaimsEntity wrapFromClaimsComplaintOperatorNewEntityToClaimsEntity(ClaimsNewEntity claimsNewEntity);

    List<ClaimsEntity> wrapFromClaimsComplaintOperatorNewEntityListToClaimsEntityList(List<ClaimsNewEntity> claimsNewEntityList);
}
