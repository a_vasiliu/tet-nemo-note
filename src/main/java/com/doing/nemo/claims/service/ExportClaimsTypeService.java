package com.doing.nemo.claims.service;

import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.settings.ExportClaimsTypeEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public interface ExportClaimsTypeService {
    ExportClaimsTypeEntity saveExportClaimsType(ExportClaimsTypeEntity exportClaimsTypeEntity);
    ExportClaimsTypeEntity updateExportClaimsType(ExportClaimsTypeEntity exportClaimsTypeEntity);
    void deleteExportClaimsType(UUID uuid);
    ExportClaimsTypeEntity getExportClaimsType(UUID uuid);
    List<ExportClaimsTypeEntity> getAllExportClaimsType();
    ExportClaimsTypeEntity patchActive(UUID uuid);
    Pagination<ExportClaimsTypeEntity> findExportClaimsType(Integer page, Integer pageSize, String orderBy, Boolean asc, DataAccidentTypeAccidentEnum claimsType, String code, String description, Boolean isActive);
}
