package com.doing.nemo.claims.service;

import com.doing.nemo.claims.entity.ClaimsEntity;

public interface EligibilityCriterionChecker {

    public Boolean isClaimsEligible(ClaimsEntity claims);

}
