package com.doing.nemo.claims.service;

import java.util.LinkedList;
import java.util.Map;

public interface ClaimsExportSheetsBuilder {

    Map<String, Object> build();

    LinkedList<Map<String, Object>> get(Map<String, Object> map, String sheetName);

}
