package com.doing.nemo.claims.service;

import com.doing.nemo.claims.controller.payload.request.ProfileRequestV1;
import com.doing.nemo.claims.controller.payload.response.IdentifierResponseV1;
import com.doing.nemo.claims.controller.payload.response.PermissionsResponseV1;
import com.doing.nemo.claims.controller.payload.response.ProfileModulePermissionsDetailResponseV1;
import com.doing.nemo.claims.controller.payload.response.ProfilesResponseV1;

import java.io.IOException;

public interface PermissionService {
    ProfilesResponseV1 getProfiles() throws IOException;
    ProfileModulePermissionsDetailResponseV1 getPermissionByProfile(String idProfile) throws IOException;
    PermissionsResponseV1 getPermissionByModule() throws IOException;
    void updateProfile(String idProfile, ProfileRequestV1 request) throws IOException;
    IdentifierResponseV1 createCustomProfile(ProfileRequestV1 request) throws IOException;
    void deleteProfile(String idProfile) throws IOException;
}
