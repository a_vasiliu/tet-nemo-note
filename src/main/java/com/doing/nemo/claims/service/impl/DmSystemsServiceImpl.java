package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.entity.settings.DmSystemsEntity;
import com.doing.nemo.claims.repository.DmSystemsRepository;
import com.doing.nemo.claims.service.DmSystemsService;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class DmSystemsServiceImpl implements DmSystemsService {
    private static Logger LOGGER = LoggerFactory.getLogger(DmSystemsServiceImpl.class);
    @Autowired
    private DmSystemsRepository dmSystemsRepository;

    @Override
    public DmSystemsEntity saveDmSystems(DmSystemsEntity dmSystemsEntity) {
        if (dmSystemsEntity != null)
            dmSystemsRepository.save(dmSystemsEntity);
        return dmSystemsEntity;
    }

    @Override
    public DmSystemsEntity updateDmSystems(DmSystemsEntity dmSystemsEntity) {
        if (dmSystemsEntity != null)
            dmSystemsRepository.save(dmSystemsEntity);
        return dmSystemsEntity;
    }

    @Override
    public void deleteDmSystems(UUID uuid) {
        Optional<DmSystemsEntity> dmSystemsEntityOptional = dmSystemsRepository.findById(uuid);
        if (!dmSystemsEntityOptional.isPresent()) {
            LOGGER.debug("DM System with id " + uuid + " not found");
            throw new NotFoundException("DM System with id " + uuid + " not found", MessageCode.CLAIMS_1010);
        }
        dmSystemsRepository.delete(dmSystemsEntityOptional.get());
    }

    @Override
    public DmSystemsEntity getDmSystems(UUID uuid) {
        Optional<DmSystemsEntity> dmSystemsEntityOptional = dmSystemsRepository.findById(uuid);
        if (!dmSystemsEntityOptional.isPresent()) {
            LOGGER.debug("DM System with id " + uuid + " not found");
            throw new NotFoundException("DM System with id " + uuid + " not found", MessageCode.CLAIMS_1010);
        }
        return dmSystemsEntityOptional.get();
    }

    @Override
    public List<DmSystemsEntity> getAllDmSystems() {
        List<DmSystemsEntity> dmSystemsEntityList = dmSystemsRepository.findAll();
        return dmSystemsEntityList;
    }
}
