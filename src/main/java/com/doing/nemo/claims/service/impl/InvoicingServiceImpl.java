package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.controller.payload.response.OlsaInvoicingResponseV1;
import com.doing.nemo.claims.service.InvoicingService;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.AbstractException;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.exception.InternalException;
import com.doing.nemo.commons.util.HttpUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.Response;
import org.apache.commons.lang3.StringUtils;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

@Service
public class InvoicingServiceImpl implements InvoicingService {

    private static Logger LOGGER = LoggerFactory.getLogger(InvoicingServiceImpl.class);

    @Value("${invoicing.api.endpoint.schema}")
    private String schema;

    @Value("${invoicing.api.endpoint.path}")
    private String invoicingEndpointPath;

    @Value("${invoicing.olsa.po}")
    private String olsaPo;

    @Value("${invoicing.olsa.po.key}")
    private String poKey;

    @Autowired
    private HttpUtil httpUtil;


    private <T, P> T call(String url, HttpMethod method, P value, HashMap<String, String> headersparams, Class<T> outclass) {

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        for (String k : headersparams.keySet()) {
            headers.add(k, headersparams.get(k));
        }

        HttpEntity entity = new HttpEntity(value, headers);

        return restTemplate.exchange(url, method, entity, outclass).getBody();

    }


    private <T, P> T callWithoutCert(String url, HttpMethod method, P value, HashMap<String, String> headersparams, Class<T> outclass) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = null;
        Response responseToken = null;
        try {
            if (value != null) {
                jsonInString = mapper.writeValueAsString(value);
            }

            responseToken = httpUtil.callURL(headersparams, url, jsonInString, method);

            if (responseToken.code() != HttpStatus.OK.value() && responseToken.code() != HttpStatus.CREATED.value()) {
                LOGGER.debug(responseToken.message() + " with code " + responseToken.code());
                throw new BadRequestException(responseToken.message() + " with code " + responseToken.code(), MessageCode.CLAIMS_2000);
            }
            String response = responseToken.body().string();
            LOGGER.debug("Response Invoicing Body: {}", response);
            return mapper.readValue(response, outclass);
        } catch (AbstractException e) {
            LOGGER.error(e.getMessage(), e);
            throw e;
        }  catch (Exception e) {
            String message = String.format(MessageCode.CLAIMS_2012.value(), e.getClass().getSimpleName(), e.getMessage());
            LOGGER.error(message, e);
            throw new InternalException(message, MessageCode.CLAIMS_2012);
        } finally {
            if (responseToken != null && responseToken.body() != null) {
                responseToken.body().close();
            }
        }
    }



    @Override
    public OlsaInvoicingResponseV1 getAttachmentInvoicing(List<String> poList) throws IOException {

        String poCommaList = StringUtils.join(poList, ',');

        MultiValueMap<String, String> parameters = new LinkedMultiValueMap<>();
        parameters.add(poKey, poCommaList);

        UriComponents uriComponents = UriComponentsBuilder.newInstance()
                .scheme(schema)
                .host(invoicingEndpointPath)
                .path(olsaPo)
                .queryParams(parameters)
                .build();

        return callWithoutCert(uriComponents.toUriString(), HttpMethod.GET, null, null, OlsaInvoicingResponseV1.class);
    }
}
