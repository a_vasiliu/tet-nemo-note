package com.doing.nemo.claims.service;

import com.doing.nemo.claims.controller.payload.response.OlsaInvoicingResponseV1;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public interface InvoicingService {
    OlsaInvoicingResponseV1 getAttachmentInvoicing(List<String> poList) throws IOException;
}
