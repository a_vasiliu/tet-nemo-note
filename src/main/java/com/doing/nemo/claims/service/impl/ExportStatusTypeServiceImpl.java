package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.controller.payload.response.PaginationStats;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsFlowEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.doing.nemo.claims.entity.settings.ExportStatusTypeEntity;
import com.doing.nemo.claims.repository.ExportStatusTypeRepository;
import com.doing.nemo.claims.repository.ExportStatusTypeRepositoryV1;
import com.doing.nemo.claims.service.ExportStatusTypeService;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ExportStatusTypeServiceImpl implements ExportStatusTypeService {
    private static Logger LOGGER = LoggerFactory.getLogger(ExportStatusTypeServiceImpl.class);
    @Autowired
    private ExportStatusTypeRepository exportStatusTypeRepository;
    @Autowired
    private ExportStatusTypeRepositoryV1 exportStatusTypeRepositoryV1;

    @Override
    public ExportStatusTypeEntity saveExportStatusType(ExportStatusTypeEntity exportStatusTypeEntity) {
        if (exportStatusTypeEntity != null){
            List<ExportStatusTypeEntity> exportStatusTypeEntities = exportStatusTypeRepository.checkDuplicateExportStatusType(exportStatusTypeEntity.getStatusType(), exportStatusTypeEntity.getFlowType());
            if(exportStatusTypeEntities != null && !exportStatusTypeEntities.isEmpty()){
                LOGGER.debug(MessageCode.CLAIMS_1126.value());
                throw new BadRequestException(MessageCode.CLAIMS_1126);
            }
            exportStatusTypeRepository.save(exportStatusTypeEntity);
        }
        return exportStatusTypeEntity;
    }

    @Override
    public ExportStatusTypeEntity updateExportStatusType(ExportStatusTypeEntity exportStatusTypeEntity) {
        if (exportStatusTypeEntity != null)
            exportStatusTypeRepository.save(exportStatusTypeEntity);
        return exportStatusTypeEntity;
    }

    @Override
    public void deleteExportStatusType(UUID uuid) {
        Optional<ExportStatusTypeEntity> exportStatusTypeEntityOptional = exportStatusTypeRepository.findById(uuid);
        if (!exportStatusTypeEntityOptional.isPresent()) {
            LOGGER.debug("Export Status TypeEnum with id " + uuid + " not found");
            throw new NotFoundException("Export Status TypeEnum with id " + uuid + " not found", MessageCode.CLAIMS_1010);
        }
        exportStatusTypeRepository.delete(exportStatusTypeEntityOptional.get());
    }

    @Override
    public ExportStatusTypeEntity getExportStatusType(UUID uuid) {
        Optional<ExportStatusTypeEntity> exportStatusTypeEntityOptional = exportStatusTypeRepository.findById(uuid);
        if (!exportStatusTypeEntityOptional.isPresent()) {
            LOGGER.debug("Export Status TypeEnum with id " + uuid + " not found");
            throw new NotFoundException("Export Status TypeEnum with id " + uuid + " not found", MessageCode.CLAIMS_1010);
        }
        return exportStatusTypeEntityOptional.get();
    }

    @Override
    public List<ExportStatusTypeEntity> getAllExportStatusType() {
        List<ExportStatusTypeEntity> exportStatusTypeEntityList = exportStatusTypeRepository.findAll();
        return exportStatusTypeEntityList;
    }

    @Override
    public ExportStatusTypeEntity patchActive(UUID uuid) {
        ExportStatusTypeEntity exportStatusTypeEntity = getExportStatusType(uuid);
        if (exportStatusTypeEntity.getActive()) {
            exportStatusTypeEntity.setActive(false);
        } else exportStatusTypeEntity.setActive(true);

        exportStatusTypeRepository.save(exportStatusTypeEntity);
        return exportStatusTypeEntity;
    }

    @Override
    public Pagination<ExportStatusTypeEntity> findExportStatusType(Integer page, Integer pageSize, String orderBy, Boolean asc, String statusType, String flowType, String code, String description, Boolean isActive) {

        Pagination<ExportStatusTypeEntity> exportStatusTypeEntityPagination = new Pagination<>();

        ClaimsStatusEnum claimsStatusEnum = null;
        if (statusType != null)
            claimsStatusEnum = ClaimsStatusEnum.create(statusType);

        ClaimsFlowEnum claimsFlowEnum = null;
        if (flowType != null)
            claimsFlowEnum = ClaimsFlowEnum.create(flowType);

        List<ExportStatusTypeEntity> exportStatusTypeEntityList = exportStatusTypeRepositoryV1.findExportStatusType(page, pageSize, orderBy, asc, claimsStatusEnum, claimsFlowEnum, code, description, isActive);
        Long itemCount = exportStatusTypeRepositoryV1.countPaginationExportStatusType(claimsStatusEnum, claimsFlowEnum, code, description, isActive);
        exportStatusTypeEntityPagination.setItems(exportStatusTypeEntityList);
        exportStatusTypeEntityPagination.setStats(new PaginationStats(itemCount.intValue(), page, pageSize));

        return exportStatusTypeEntityPagination;
    }
}
