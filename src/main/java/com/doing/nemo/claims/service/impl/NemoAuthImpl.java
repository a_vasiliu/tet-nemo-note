package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.controller.payload.request.NemoAuthExternalMetadataRequestV1;
import com.doing.nemo.claims.controller.payload.request.NemoAuthExternalRequestV1;
import com.doing.nemo.claims.controller.payload.request.NemoAuthRequestV1;
import com.doing.nemo.claims.controller.payload.response.NemoAuthCompleteResponseV1;
import com.doing.nemo.claims.controller.payload.response.NemoAuthResponseV1;
import com.doing.nemo.claims.controller.payload.response.NemoAuthTokenResponseV1;
import com.doing.nemo.claims.service.NemoAuthService;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.AbstractException;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.exception.InternalException;
import com.doing.nemo.commons.util.HttpUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.Response;
import org.apache.commons.collections4.MapUtils;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Service
public class NemoAuthImpl implements NemoAuthService {

    private static Logger LOGGER = LoggerFactory.getLogger(NemoAuthImpl.class);
    @Value("${auth.api.endpoint.schema}")
    private String schema;
    @Value("${auth.api.endpoint.path}")
    private String path;
    @Value("${auth.api.oauth}")
    private String authCall;
    @Value("${auth.api.grant}")
    private String grantCall;
    @Value("${auth.client.id.key}")
    private String clientIdKey;
    @Value("${auth.client.id.value}")
    private String clientIdValue;
    @Value("${auth.grant.type.key}")
    private String grantTypeKey;
    @Value("${auth.grant.type.value}")
    private String grantTypeValue;
    @Value("${auth.client.secret.key}")
    private String clientSecretKey;
    @Value("${auth.client.secret.value}")
    private String clientSecretValue;
    @Value("${auth.username.key}")
    private String usernameKey;
    @Value("${auth.username.value}")
    private String usernameValue;
    @Value("${auth.password.key}")
    private String passwordKey;
    @Value("${auth.password.value}")
    private String passwordValue;
    @Autowired
    private HttpUtil httpUtil;
    @Value("${auth.external.api.endpoint.schema}")
    private String extSchema;
    @Value("${auth.external.api.endpoint.path}")
    private String extPath;
    @Value("${auth.external.api.grant}")
    private String extGrant;

    @Value("${auth.external.redirect.url}")
    private String externalRedirectUrl;
    @Value("${auth.external.redirect.url.intervention.ten}")
    private String externalRedirectUrlIncomplete;
    @Value("${auth.external.user.id}")
    private String externalUserId;
    @Value("${auth.external.email}")
    private String externalEmail;
    @Value("${auth.external.incomplete.email}")
    private String externalIncompleteEmail;
    @Value("${claims.base.url}")
    private String claimsBaseUrl;
    @Value("${claims.external.token}")
    private String externalToken;

    private <T, P> T callWithoutCert(String url, HttpMethod method, P value, HashMap<String, String> headersparams, Class<T> outclass) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = null;
        Response responseToken = null;
        try {
            if (value != null) {
                jsonInString = mapper.writeValueAsString(value);
            }
            responseToken = httpUtil.callURL(headersparams, url, jsonInString, method);


            if (responseToken.code() != HttpStatus.ACCEPTED.value() && responseToken.code() != HttpStatus.OK.value() && responseToken.code() != HttpStatus.CREATED.value()) {
                LOGGER.debug(responseToken.message() + " with code " + responseToken.code());
                throw new BadRequestException(responseToken.message() + " with code " + responseToken.code(), MessageCode.CLAIMS_2000);
            }
            String response = responseToken.body().string();
            LOGGER.debug("Response NemoAuth Body: {}", response);
            return mapper.readValue(response, outclass);
        } catch (AbstractException e) {
            LOGGER.error(e.getMessage(), e);
            throw e;
        }  catch (Exception e) {
            String message = String.format(MessageCode.CLAIMS_2012.value(), e.getClass().getSimpleName(), e.getMessage());
            LOGGER.error(message, e);
            throw new InternalException(message, MessageCode.CLAIMS_2012);
        } finally {
            if (responseToken != null && responseToken.body() != null) {
                responseToken.close();
            }
        }
    }


    public NemoAuthResponseV1 getToken() throws IOException {

        MultiValueMap<String, String> parameters = new LinkedMultiValueMap<>();
        parameters.add(clientIdKey, clientIdValue);
        parameters.add(grantTypeKey, grantTypeValue);
        parameters.add(clientSecretKey, clientSecretValue);
        parameters.add(usernameKey, usernameValue);
        parameters.add(passwordKey, passwordValue);
        UriComponents uriComponents = UriComponentsBuilder.newInstance()
                .scheme(schema)
                .host(path)
                .path(authCall)
                .queryParams(parameters)
                .build();

        return callWithoutCert(uriComponents.toUriString(), HttpMethod.GET, null, null, NemoAuthResponseV1.class);
    }

    @Override
    public NemoAuthCompleteResponseV1 postGrant(Map<String, String> metadata) throws IOException {
        UriComponents uriComponents = UriComponentsBuilder.newInstance()
                .scheme(schema)
                .host(path)
                .path(grantCall)
                .build();

        NemoAuthResponseV1 nemoAuthResponseV1 = getToken();
        NemoAuthRequestV1 nemoAuthRequestV1 = new NemoAuthRequestV1();
        nemoAuthRequestV1.setMetadata(metadata);
        nemoAuthRequestV1.setRefreshToken(nemoAuthResponseV1.getRefreshToken());
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Authorization", "Bearer " + nemoAuthResponseV1.getAccessToken());
        NemoAuthCompleteResponseV1 nemoAuthCompleteResponseV1 = callWithoutCert(uriComponents.toUriString(), HttpMethod.POST, nemoAuthRequestV1, headers, NemoAuthCompleteResponseV1.class);
        nemoAuthCompleteResponseV1.setMetadata(metadata);
        nemoAuthCompleteResponseV1.setAccessToken(nemoAuthResponseV1.getAccessToken());
        nemoAuthCompleteResponseV1.setRefreshToken(nemoAuthResponseV1.getRefreshToken());

        return nemoAuthCompleteResponseV1;
    }

    @Override
    public String getExternalToken(NemoAuthExternalMetadataRequestV1 nemoAuthExternalMetadataRequestV1, String claimsId, String practiceId, Boolean isIncomplete) throws IOException {

        UriComponents uriComponents = UriComponentsBuilder.newInstance()
                .scheme(extSchema)
                .host(extPath)
                .path(extGrant)
                .build();
        NemoAuthExternalRequestV1 nemoAuthExternal = new NemoAuthExternalRequestV1();
        String externalRedirectUrlString = null;
        if(isIncomplete){
            externalRedirectUrlString = String.format(externalRedirectUrlIncomplete,practiceId, claimsId);
            nemoAuthExternal.setUserId(null);
            nemoAuthExternal.setEmail(externalIncompleteEmail);
        } else{
            externalRedirectUrlString = String.format(externalRedirectUrl,practiceId, claimsId);
            nemoAuthExternal.setUserId(externalUserId);
            nemoAuthExternal.setEmail(externalEmail);
        }


        nemoAuthExternalMetadataRequestV1.setRedirectUrl(externalRedirectUrlString);
        nemoAuthExternal.setMetadata(nemoAuthExternalMetadataRequestV1);

        NemoAuthTokenResponseV1 nemoAuthTokenResponse = callWithoutCert(uriComponents.toUriString(), HttpMethod.POST, nemoAuthExternal, null, NemoAuthTokenResponseV1.class);

        String token = nemoAuthTokenResponse != null ? nemoAuthTokenResponse.getToken() : "";

        String completeUrl = claimsBaseUrl + String.format(externalToken, token);


        return completeUrl;
    }

    private String buildUrl(String baseUrl, String uri, Map<String, String> pathParams, MultiValueMap<String, String> queryParams) {

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(baseUrl.concat(uri));

        if (MapUtils.isNotEmpty(queryParams)) {
            builder.queryParams(queryParams);
        }

        return builder.buildAndExpand(pathParams).toString();
    }
}
