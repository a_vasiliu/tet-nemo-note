package com.doing.nemo.claims.service;

import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.enumerated.ClaimsRepairEnum;
import com.doing.nemo.claims.entity.settings.EmailTemplateEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public interface EmailTemplateService {

    EmailTemplateEntity insertEmailTemplate(EmailTemplateEntity emailTemplateEntity);

    EmailTemplateEntity updateEmailTemplate(EmailTemplateEntity emailTemplateEntity, UUID uuid);

    EmailTemplateEntity getEmailTemplate(UUID uuid);

    List<EmailTemplateEntity> getAllEmailTemplate(ClaimsRepairEnum claimsRepairEnum);

    EmailTemplateEntity deleteEmailTemplate(UUID uuid);

    EmailTemplateEntity patchStatusEmailTemplate(UUID uuid);

    Pagination<EmailTemplateEntity> paginationEmailTemplate(int page, int pageSize, String flows, String claimsRepairEnum, String orderBy, Boolean asc, String applicationEventTypeId, String type, String description, String object, String heading, String body, String foot, Boolean attachFile, Boolean splittingRecipientsEmail, String clientCode, Boolean isActive);

}
