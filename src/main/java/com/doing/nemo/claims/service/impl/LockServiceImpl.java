package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.entity.settings.LockEntity;
import com.doing.nemo.claims.exception.UnsupportedOperationException;
import com.doing.nemo.claims.repository.LockRepository;
import com.doing.nemo.claims.service.LockService;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;

@Service
public class LockServiceImpl implements LockService {

    private static Logger LOGGER = LoggerFactory.getLogger(LockServiceImpl.class);
    @Autowired
    private LockRepository lockRepository;
    @Value("${lock.ttl}")
    private Long ttl;

    @Override
    public LockEntity lockPractice(String userId, String claimId) {
        LockEntity lockEntity = lockRepository.searchByClaimId(claimId);
        if (lockEntity != null) {
            Timestamp now = new Timestamp(System.currentTimeMillis());
            if (now.compareTo(lockEntity.getNow()) > 0) {
                lockEntity.setNow(new Timestamp(System.currentTimeMillis() + ttl));
                lockEntity.setUserId(userId);
                lockRepository.save(lockEntity);
            } else {
                if (lockEntity.getUserId().equalsIgnoreCase(userId)) {
                    lockEntity.setNow(new Timestamp(System.currentTimeMillis() + ttl));
                    lockRepository.save(lockEntity);
                } else {
                    LOGGER.debug("Practice with id " + claimId + " already locked by another user");
                    throw new UnsupportedOperationException("Practice with id " + claimId + " already locked by another user", MessageCode.CLAIMS_1020);
                }
            }
        } else {
            lockEntity = new LockEntity();
            lockEntity.setUserId(userId);
            lockEntity.setClaimId(claimId);
            lockEntity.setNow(new Timestamp(System.currentTimeMillis() + ttl));
            lockRepository.save(lockEntity);
        }
        return lockEntity;
    }

    @Override
    public void unlockPractice(String userId, String claimId) {
        LockEntity lockEntity = lockRepository.searchByUserIdAndClaimId(userId, claimId);
        if (lockEntity != null) {
            lockRepository.delete(lockEntity);
        } else {
            LOGGER.debug("Lock with practice_id " + claimId + " and user_id " + userId + " not found");
            throw new BadRequestException("Lock with practice_id " + claimId + " and user_id " + userId + " not found", MessageCode.CLAIMS_1010);
        }
    }

    @Override
    public LockEntity alivePractice(String userId, String claimId) {
        LockEntity lockEntity = lockRepository.searchByUserIdAndClaimId(userId, claimId);
        if (lockEntity != null) {
            Timestamp now = new Timestamp(System.currentTimeMillis());
            if (now.compareTo(lockEntity.getNow()) <= 0) {
                lockEntity.setNow(new Timestamp(System.currentTimeMillis() + ttl));
                lockRepository.save(lockEntity);
            } else {
                LOGGER.debug("Lock with practice_id " + claimId + " and user_id " + userId + " already expired");
                throw new UnsupportedOperationException("Lock with practice_id " + claimId + " and user_id " + userId + " already expired", MessageCode.CLAIMS_1020);
            }
        } else {
            LOGGER.debug("Lock with practice_id " + claimId + " and user_id " + userId + " already expired");
            throw new BadRequestException("Lock with practice_id " + claimId + " and user_id " + userId + " not found", MessageCode.CLAIMS_1010);
        }
        return lockEntity;
    }

    @Override
    public void checkLock(String userId, String claimId) {
        LockEntity lockEntity = lockRepository.searchByClaimId(claimId);
        if (lockEntity != null) {
            Timestamp now = new Timestamp(System.currentTimeMillis());
            if (!lockEntity.getUserId().equalsIgnoreCase(userId) && now.compareTo(lockEntity.getNow()) <= 0) {
                LOGGER.debug("Practice with id " + claimId + " locked by another user");
                throw new UnsupportedOperationException("Practice with id " + claimId + " locked by another user", MessageCode.CLAIMS_1020);
            } else if (now.compareTo(lockEntity.getNow()) > 0) {
                LOGGER.debug("Practice with id " + claimId + " must be locked before update");
                throw new UnsupportedOperationException("Practice with id " + claimId + " must be locked before update", MessageCode.CLAIMS_1020);
            }
        } else {
            LOGGER.debug("Practice with id " + claimId + " must be locked before update");
            throw new UnsupportedOperationException("Practice with id " + claimId + " must be locked before update", MessageCode.CLAIMS_1020);
        }
    }


    //chorne per cancellare i lock scaduti nella tabella
    //da definire il tempo rispetto agli altri lock
    //@Scheduled(cron = "0 0 22 * * ?")
    public void lockJob() {
        lockRepository.serchUnlockableClaims();
    }
}

