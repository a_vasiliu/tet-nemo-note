package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.dto.ClaimsExportFilter;
import com.doing.nemo.claims.repository.ClaimsNewRepository;
import com.doing.nemo.claims.service.ExportHelper;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.BadRequestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("ClaimsExportManager")
public class ClaimsExportHelper extends ExportHelper<ClaimsExportFilter> {

    @Autowired
    private ClaimsNewRepository claimsNewRepository;

    @Override
    public Integer getExportCount(ClaimsExportFilter filter) {
        int exportCount = claimsNewRepository.countExportClaims(filter).intValue();
        if(exportCount <= 0) {
            throw new BadRequestException(MessageCode.CLAIMS_1162);
        }
        return exportCount;
    }

}
