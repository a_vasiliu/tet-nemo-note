package com.doing.nemo.claims.service;

import com.doing.nemo.claims.entity.CounterpartyEntity;
import com.doing.nemo.claims.entity.jsonb.Authority;
import com.doing.nemo.claims.entity.jsonb.repair.LastContact;

public interface ExportFieldTranslator {

    String getRepairState(Authority authority);

    String getRepairStatus(CounterpartyEntity currentCounterParty);

    String getContactResult(LastContact lastContact);

    String getProcedure(CounterpartyEntity counterParty);

    String getType(CounterpartyEntity counterParty);

    String getManagementType(CounterpartyEntity counterParty);
}
