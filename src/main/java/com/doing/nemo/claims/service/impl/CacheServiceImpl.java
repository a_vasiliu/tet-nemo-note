package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.cache.CacheHandler;
import com.doing.nemo.claims.controller.payload.response.ClaimsResponseStatsEvidenceV2;
import com.doing.nemo.claims.entity.Stats;
import com.doing.nemo.claims.entity.enumerated.DTO.ClaimsTheftStats;
import com.doing.nemo.claims.entity.enumerated.DTO.CounterpartyStats;
import com.doing.nemo.claims.service.CacheService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class CacheServiceImpl implements CacheService {

    private static Logger LOGGER = LoggerFactory.getLogger(CacheServiceImpl.class);

    @Autowired
    private CacheHandler cacheHandler;

    @Override
    public Stats getCachedClaimsStats(String cacheName, String key) {

        try {
            ObjectMapper objectMapper = new ObjectMapper();
            Map<String, Object> payload = (Map<String, Object>) cacheHandler.getValue(cacheName.concat(key));
            if(payload == null) {
                return null;
            }
            return objectMapper.convertValue(payload, Stats.class);
        }
        catch (Exception e) {
            LOGGER.warn(e.getMessage(), e);
            return null;
        }
    }

    @Override
    public ClaimsResponseStatsEvidenceV2 getCachedClaimsStatsV2(String cacheName, String key) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            Map<String, Object> payload = (Map<String, Object>) cacheHandler.getValue(cacheName.concat(key));
            if(payload == null) {
                return null;
            }
            return objectMapper.convertValue(payload, ClaimsResponseStatsEvidenceV2.class);
        }
        catch (Exception e) {
            LOGGER.warn(e.getMessage(), e);
            return null;
        }
    }

    @Override
    public ClaimsTheftStats getCachedClaimsStatsTheft(String cacheName, String key) {

        try {
            ObjectMapper objectMapper = new ObjectMapper();
            Map<String, Object> payload = (Map<String, Object>) cacheHandler.getValue(cacheName.concat(key));
            if(payload == null) {
                return null;
            }
            return objectMapper.convertValue(payload, ClaimsTheftStats.class);
        }
        catch (Exception e) {
            LOGGER.warn(e.getMessage(), e);
            return null;
        }
    }

    @Override
    public CounterpartyStats getCachedCounterpartyStats(String cacheName, String key) {

        try {
            ObjectMapper objectMapper = new ObjectMapper();
            Map<String, Object> payload = (Map<String, Object>) cacheHandler.getValue(cacheName.concat(key));
            if(payload == null) {
                return null;
            }
            return objectMapper.convertValue(payload, CounterpartyStats.class);
        }
        catch (Exception e) {
            LOGGER.warn(e.getMessage(), e);
            return null;
        }
    }

    @Override
    public void cacheClaimsStats( Map<String, Object> payload, String cacheName, String key) {

        try {
            if(key == null) {
                cacheHandler.setValue(cacheName, payload);
            }
            else {
                cacheHandler.setValue(cacheName.concat(key), payload);
            }

        }
        catch (Exception e) {
            LOGGER.warn(e.getMessage(), e);
        }
    }

    @Override
    public void deleteClaimsStats(String cacheName, String key) {

        try {
            this.deleteClaimsStats( cacheName.concat(key) );
        }
        catch (Exception e) {
            LOGGER.warn(e.getMessage(), e);
        }
    }

    @Override
    public void deleteClaimsStats(String fullKey) {
        cacheHandler.deleteKey(fullKey);
    }
    /* - */



}
