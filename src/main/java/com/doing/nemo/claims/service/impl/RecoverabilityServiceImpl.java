package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.controller.payload.response.PaginationStats;
import com.doing.nemo.claims.controller.payload.response.RecoverabilityResponseV1;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.settings.RecoverabilityEntity;
import com.doing.nemo.claims.repository.RecoverabilityRepository;
import com.doing.nemo.claims.repository.RecoverabilityRepositoryV1;
import com.doing.nemo.claims.service.RecoverabilityService;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class RecoverabilityServiceImpl implements RecoverabilityService {

    private static Logger LOGGER = LoggerFactory.getLogger(RecoverabilityServiceImpl.class);
    @Autowired
    private RecoverabilityRepository recoverabilityRepository;
    @Autowired
    private RecoverabilityRepositoryV1 recoverabilityRepositoryV1;

    @Override
    public RecoverabilityEntity insertRecoverability(RecoverabilityEntity recoverabilityEntity) {
        if (recoverabilityEntity.getPercentRecoverability() < 0 || recoverabilityEntity.getPercentRecoverability() > 100) {
            LOGGER.debug("PercentRecoverability must be greater than 0 and less than 100");
            throw new BadRequestException("PercentRecoverability must be greater than 0 and less than 100", MessageCode.CLAIMS_1021);
        }
        recoverabilityRepository.save(recoverabilityEntity);

        return recoverabilityEntity;
    }

    @Override
    public RecoverabilityEntity updateRecoverability(RecoverabilityEntity recoverabilityEntity) {
        if (recoverabilityEntity.getPercentRecoverability() < 0 || recoverabilityEntity.getPercentRecoverability() > 100) {
            LOGGER.debug("PercentRecoverability must be greater than 0 and less than 100");
            throw new BadRequestException("PercentRecoverability must be greater than 0 and less than 100", MessageCode.CLAIMS_1021);
        }
        recoverabilityRepository.save(recoverabilityEntity);
        return recoverabilityEntity;
    }

    @Override
    public RecoverabilityEntity selectRecoverability(UUID id) {
        Optional<RecoverabilityEntity> recoverabilityEntity = recoverabilityRepository.findById(id);
        if (!recoverabilityEntity.isPresent()) {
            LOGGER.debug("Recoverability with id " + id + " not found");
            throw new NotFoundException("Recoverability with id " + id + " not found", MessageCode.CLAIMS_1010);
        }

        return recoverabilityEntity.get();
    }

    @Override
    public List<RecoverabilityEntity> selectAllRecoverability() {
        List<RecoverabilityEntity> resultListRecoverability = recoverabilityRepository.findAll();

        return resultListRecoverability;
    }

    @Override
    public RecoverabilityResponseV1 deleteRecoverability(UUID id) {
        RecoverabilityEntity recoverabilityEntity = selectRecoverability(id);
        recoverabilityRepository.delete(recoverabilityEntity);
        return null;
    }

    @Override
    public RecoverabilityEntity updateStatus(UUID uuid) {
        RecoverabilityEntity recoverabilityEntity = selectRecoverability(uuid);
        if (recoverabilityEntity.getActive()) {
            recoverabilityEntity.setActive(false);
        } else {
            recoverabilityEntity.setActive(true);
        }

        recoverabilityRepository.save(recoverabilityEntity);
        return recoverabilityEntity;
    }

    @Override
    public RecoverabilityEntity getRecoverabilityByFlowAndTypeClaim(String flow, DataAccidentTypeAccidentEnum typeClaim) {
        RecoverabilityEntity recoverabilityEntity = recoverabilityRepository.searchRecoverabilyByFlowAndClaimType(flow, typeClaim);

        return recoverabilityEntity;
    }

    @Override
    public Pagination<RecoverabilityEntity> paginationRecoverability(int page, int pageSize, String management, String claimsType, Boolean counterpart, Boolean recoverability, Double percentRecoverability, Boolean isActive, String orderBy, Boolean asc) {
        Pagination<RecoverabilityEntity> recoverabilityPagination = new Pagination<>();

        DataAccidentTypeAccidentEnum dataAccidentTypeAccident = null;
        if (claimsType != null)
            dataAccidentTypeAccident = DataAccidentTypeAccidentEnum.create(claimsType);

        List<RecoverabilityEntity> recoverabilityList = recoverabilityRepositoryV1.findPaginationRecoverability(page, pageSize, management, dataAccidentTypeAccident, counterpart, recoverability, percentRecoverability, isActive, orderBy, asc);
        Long itemCount = recoverabilityRepositoryV1.countPaginationRecoverability(management, dataAccidentTypeAccident, counterpart, recoverability, percentRecoverability, isActive);
        recoverabilityPagination.setItems(recoverabilityList);
        recoverabilityPagination.setStats(new PaginationStats(itemCount.intValue(), page, pageSize));

        return recoverabilityPagination;
    }

}