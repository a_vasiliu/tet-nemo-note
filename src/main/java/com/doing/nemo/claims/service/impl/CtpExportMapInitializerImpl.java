package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.service.ExportMapInitializer;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.Map;

@Service("CtpExportMapInitializerImpl")
public class CtpExportMapInitializerImpl implements ExportMapInitializer {

    public Map<String, Object> ctpRepair() {
        Map<String,Object> innerMap = new HashMap<>();
        innerMap.put("counterparty_id", "");
        innerMap.put("status", "");
        innerMap.put("procedure", "");
        innerMap.put("user_create", "");
        innerMap.put("user_created_at", "");
        innerMap.put("user_update", "");
        innerMap.put("plate_ald","");
        innerMap.put("update_at", "");
        innerMap.put("claim_id", "");
        innerMap.put("claim_created_at", "");
        innerMap.put("claim_date", "");
        innerMap.put("claim_hour", "");
        innerMap.put("claim_location", "");
        innerMap.put("claim_region", "");
        innerMap.put("claim_province", "");
        innerMap.put("type", "");
        innerMap.put("vehicle_plate", "");
        innerMap.put("vehicle_model", "");
        innerMap.put("insured_last_name", "");
        innerMap.put("insured_first_name", "");
        innerMap.put("insured_telephone", "");
        innerMap.put("insured_email", "");
        innerMap.put("insured_company_name", "");
        innerMap.put("insured_company_id", "");
        innerMap.put("policy_number", "");
        innerMap.put("manager_name", "");
        innerMap.put("sx_number_ext", "");
        innerMap.put("manager_last_download", "");
        innerMap.put("management_type", "");
        innerMap.put("asked_for_damages", "");
        innerMap.put("legal_or_consultant", "");
        innerMap.put("date_request_damages", "");
        innerMap.put("expiration_date", "");
        innerMap.put("documentation_complete", "");
        innerMap.put("legal_name", "");
        innerMap.put("legal_email", "");
        innerMap.put("replacement", "");
        innerMap.put("replacement_plate", "");
        innerMap.put("replacement_assigned_at", "");
        innerMap.put("replacement_assigned_to", "");
        innerMap.put("driver_surname", "");
        innerMap.put("driver_name", "");
        innerMap.put("driver_address", "");
        innerMap.put("driver_cap", "");
        innerMap.put("driver_locality", "");
        innerMap.put("driver_province", "");
        innerMap.put("driver_telephone", "");
        innerMap.put("driver_email", "");
        innerMap.put("driver_fiscal_code", "");
        innerMap.put("driver_birth_date", "");
        innerMap.put("driver_license", "");
        innerMap.put("driver_license_category", "");
        innerMap.put("driver_cai_part", "");
        innerMap.put("driver_cai_signed", "");
        innerMap.put("driver_cai_sign_type", "");
        innerMap.put("contact_last_call_date", "");
        innerMap.put("contact_result", "");
        innerMap.put("contact_recall_date", "");
        innerMap.put("motivation", "");
        innerMap.put("contact_last_call_note", "");
        innerMap.put("repairer_id", "");
        innerMap.put("repairer_name", "");
        innerMap.put("repairer_email", "");
        innerMap.put("repairer_sent_to", "");
        innerMap.put("last_receive_attachments", "");
        innerMap.put("quotation_sent_date", "");
        innerMap.put("quotation_amount", "");
        innerMap.put("repair_state", "");
        innerMap.put("date_result_authorization", "");
        innerMap.put("practice_id", "");
        innerMap.put("download", "");
        innerMap.put("communicated", "");
        innerMap.put("communicated_type", "");
        innerMap.put("communicated_date", "");
        innerMap.put("communicated_legal_or_consultant", "");
        innerMap.put("communicated_legal_or_consultant_date", "");
        return innerMap;
    }

    public Map<String, Object> calls() {
        Map<String,Object> innerMap = new HashMap<>();
        innerMap.put("counterparty_id", "");
        innerMap.put("status", "");
        innerMap.put("contact_result", "");
        innerMap.put("contact_date", "");
        innerMap.put("motivation", "");
        innerMap.put("contact_last_call_note", "");
        return innerMap;
    }

    public Map<String, Object> attachmentRepair() {
        Map<String,Object> innerMap = new HashMap<>();
        innerMap.put("counterparty_id","");
        innerMap.put("status","");
        innerMap.put("tipologia_denuncia","");
        innerMap.put("nomefile_denuncia","");
        innerMap.put("tipologia_manutenzione","");
        innerMap.put("nomefile_manutenzione","");
        return innerMap;
    }

}