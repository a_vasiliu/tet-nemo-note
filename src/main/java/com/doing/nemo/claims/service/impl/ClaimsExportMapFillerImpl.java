package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.adapter.AuthorityAdapter;
import com.doing.nemo.claims.adapter.EnumAdapter;
import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.dto.ClaimsExportBaseData;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.ClaimsNewEntity;
import com.doing.nemo.claims.entity.CounterpartyEntity;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.EntrustedEnum.EntrustedEnum;
import com.doing.nemo.claims.entity.jsonb.*;
import com.doing.nemo.claims.entity.jsonb.complaint.Complaint;
import com.doing.nemo.claims.entity.jsonb.complaint.jsonbComplaint.Entrusted;
import com.doing.nemo.claims.entity.jsonb.complaint.jsonbComplaint.FromCompany;
import com.doing.nemo.claims.entity.jsonb.complaint.jsonbComplaint.jsonbDataAccidents.DataAccident;
import com.doing.nemo.claims.entity.jsonb.counterparty.InsuranceCompanyCounterparty;
import com.doing.nemo.claims.entity.jsonb.damaged.Damaged;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.*;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbAntiTheftService.AntiTheftRequest;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbAntiTheftService.AntiTheftService;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbAntiTheftService.Registry;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbDriver.Driver;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbDriver.DrivingLicense;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbInsuranceCompany.LegalCost;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbInsuranceCompany.Pai;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbInsuranceCompany.Tpl;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbVehicle.Vehicle;
import com.doing.nemo.claims.entity.jsonb.forms.jsonbForms.Attachment;
import com.doing.nemo.claims.entity.jsonb.refund.Refund;
import com.doing.nemo.claims.entity.jsonb.refund.Split;
import com.doing.nemo.claims.entity.settings.*;
import com.doing.nemo.claims.repository.*;
import com.doing.nemo.claims.service.ConverterClaimsService;
import com.doing.nemo.claims.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.*;

@Service("ClaimsExportMapFillerImpl")
public class ClaimsExportMapFillerImpl extends ExportMapFiller {

    @Autowired
    private ClaimsNewRepository claimsNewRepository;

    @Autowired
    private ConverterClaimsService converterClaimsService;

    @Autowired
    private ConfigureEvidenceRepository configureEvidenceRepository;

    @Autowired
    private LegalRepository legalRepository;

    @Autowired
    private InspectorateRepository inspectorateRepository;

    @Autowired
    private InsuranceManagerRepository insuranceManagerRepository;

    @Autowired
    private InsuranceCompanyRepository insuranceCompanyRepository;

    private Map<String, Object> baseFill(
        Map<String, Object> map,
        ClaimsExportBaseData baseData
    ) {
        //ID PRATICA
        map.put("idcomplaint", baseData.getIdPractice());
        // TARGA DANNEGGIATO
        map.put("targa_danneggiato", baseData.getDamagedPlate());
        //DATA SINISTRO
        map.put("data_sx", baseData.getDateAccident());

        return map;
    }

    public Map<String, Object> claims(
        Map<String, Object> map,
        ClaimsEntity claimsEntity,
        ClaimsExportBaseData baseData
    ) {
        // dd-MM-yyyy
        SimpleDateFormat dateFormat = this.getDateFormat();
        // HH:mm
        SimpleDateFormat hourFormat = this.getHourFormat();
        // dd-MM-yyyy HH:mm
        SimpleDateFormat timestampFormat = this.getTimestampFormat();

        map = this.baseFill(map, baseData);

        //DATA INSERIMENTO
        map.put("inserita_il", timestampFormat.format( DateUtil.convertIS08601StringToUTCDate( DateUtil.convertUTCInstantToIS08601String(claimsEntity.getCreatedAt()) )) );
        //UTENTE
        map.put("utente", this.getUser(claimsEntity));
        //TIPOLOGIA FLUSSO
        map.put("tipo_flusso", claimsEntity.getType() );
        //STATO PRATICA
        map.put("status", this.translateClaimsStatus( claimsEntity.getStatus().getValue() ) );
        //MOTIVAZIONE
        map.put("motivincomp_des", claimsEntity.getMotivation());
        //DATI DA CAI
        if(baseData.getDriverSide().equals("") ) {
            map.put("cai_firmato_a", "NO");
            map.put("cai_firmato_b", "NO");
        }
        //EVIDENZA
        ConfigureEvidenceEntity configureEvidenceEntity = configureEvidenceRepository.searchEvidenceByPracticalState(claimsEntity.getStatus().getValue());
        if(configureEvidenceEntity != null) {
            Calendar calendar = Calendar.getInstance();
            Instant updatedAt = claimsEntity.getUpdateAt();
            calendar.setTime(DateUtil.convertIS08601StringToUTCDate(DateUtil.convertUTCInstantToIS08601String(updatedAt)));
            calendar.add(Calendar.DAY_OF_MONTH, configureEvidenceEntity.getDaysOfStayInTheState().intValue());
            map.put("evidenza", timestampFormat.format(calendar.getTime()));
        }
        //RICEZIONE DENUNCIA
        map.put("ricev_denuncia", timestampFormat.format(DateUtil.convertIS08601StringToUTCDate(DateUtil.convertUTCInstantToIS08601String(claimsEntity.getCreatedAt()))));
        //CITAZIONE - capire da dove recuperarlo
        map.put("citazione", adptBooleanToString(claimsEntity.getComplaint().getQuote()));
        //COMUNICAZIONE PAI
        map.put("comunicazione_pai", adptBooleanToString(claimsEntity.getPaiComunication()));
        //COMUNICAZIONE LEGALE
        map.put("comunicazione_legal", adptBooleanToString(claimsEntity.getLegalComunication()));
        //SEGUITI DA CONFERMARE
        map.put("seguiti_confermare", adptBooleanToString(claimsEntity.getWithContinuation()));
        //DATI DA COMPLAINT
        if(claimsEntity.getComplaint() != null) {
            Complaint complaint = claimsEntity.getComplaint();
            //TIPOLOGIA MODULO
            map.put("tipo_modulo", complaint.getMod());
            //NOTIFICA DES
            map.put("notifica_des", complaint.getNotification());
            //DATA ACCIDENT
            if(complaint.getDataAccident() != null){
                DataAccident dataAccident = complaint.getDataAccident();
                //TIPOLOGIA SINISTRO
                if(dataAccident.getTypeAccident() != null ) {
                    map.put("tiposx_des", dataAccident.getTypeAccident().getValue().replaceAll("_", " "));
                }
                //ORA SINISTRO
                map.put("ora_sx", hourFormat.format(dataAccident.getDateAccident()));
                //INDIRIZZO SINISTRO
                Address address = dataAccident.getAddress();
                if(address != null) {
                    //
                    if (address.getStreet() != null) {
                        map.put("indirizzo_sx", address.getStreet() != null ? address.getStreet() + " " + (address.getStreetNr() != null ? address.getStreetNr() : "") : "");
                    } else {
                        map.put("indirizzo_sx", address.getFormattedAddress());
                    }
                    map.put("localita_sx", address.getLocality());
                    map.put("regione_sx", address.getRegion());
                    map.put("prov_sx", address.getProvince());
                }

                //DATI RECOVERABILITY
                map.put("recuperabilita", adptBooleanToString(dataAccident.getRecoverability()));

               if( dataAccident.getRecoverabilityPercent() != null) {
                    map.put("recuperabilita_perc", dataAccident.getRecoverabilityPercent().toString());
                }

                //PRESENZA CONTROPARTI
                map.put("controparti", adptBooleanToString(dataAccident.getWithCounterparty()));
                //AUTORITA PS
                map.put("autorita_ps", adptBooleanToString(dataAccident.getPolice()));
                //AUTORITA CC
                map.put("autorita_cc", adptBooleanToString(dataAccident.getCc()));
                //AUTORITA VVUU
                map.put("autorita_vvuu", adptBooleanToString(dataAccident.getVvuu()));
                //AUTORITA DESC
                map.put("autorita_desc", dataAccident.getAuthorityData());
            }

            //DATI FROM COMPANY
            if(complaint.getFromCompany() != null){
                FromCompany fromCompany = complaint.getFromCompany();
                //TIPOLOGIA SINISTRO
                if(Util.isNotEmpty(fromCompany.getTypeSx())) {
                    map.put("ext_tiposxdes", fromCompany.getTypeSx());
                }
                //NUMERO SINISTRO
                map.put("ext_numsx", fromCompany.getNumberSx());
                //ISPETTORATO
                map.put("ext_ispettorato", fromCompany.getInspectorate());
                //PERITO
                map.put("ext_perito", fromCompany.getExpert());
                //NOTE
                map.put("ext_note", fromCompany.getNote());
                //RISERVA GLOBALE
                if(fromCompany.getGlobalReserve() != null){
                    map.put("riserva_globale", fromCompany.getGlobalReserve().toString());
                }
                //TOTALE PAGATO
                if(fromCompany.getTotalPaid() != null){
                    map.put("totale_pagato", fromCompany.getTotalPaid().toString());
                }
                //STATUS COMPAGNIA
                map.put("status_compagnia", fromCompany.getStatus());
                //DATA DOWNLOAD MSA
                if(fromCompany.getDwlMan() != null){
                    map.put("data_dwl_sx", timestampFormat.format(fromCompany.getDwlMan()));
                }
                //DATA ULTIMO AGGIORNAMENTO
                if(fromCompany.getLastUpdate() != null){
                    map.put("data_update_sx", timestampFormat.format(fromCompany.getLastUpdate()));
                }
            }

            //DATI ENTRUSTED
            if(
                complaint.getEntrusted() != null &&
                complaint.getEntrusted().getEntrustedTo() != null &&
                complaint.getEntrusted().getEntrustedDay() != null &&
                complaint.getEntrusted().getEntrustedType() != null
            ) {
                Entrusted entrusted = complaint.getEntrusted();
                map.put("affidata_il", timestampFormat.format(entrusted.getEntrustedDay()));
                EntrustedEnum entrustedEnum = entrusted.getEntrustedType();
                switch (entrustedEnum){
                    case LEGAL:
                        if(entrusted.getIdEntrustedTo() != null) {
                            Optional<LegalEntity> legalEntityOptional = legalRepository.findById(entrusted.getIdEntrustedTo());
                            if (legalEntityOptional.isPresent()) {
                                map.put("affidata_a", "Legale");
                                map.put("legale_des", entrusted.getEntrustedTo());
                            }
                        }
                        break;
                    case INSPECTORATE:
                        if(entrusted.getIdEntrustedTo() != null) {
                            Optional<InspectorateEntity> inspectorateEntityOptional = inspectorateRepository.findById(entrusted.getIdEntrustedTo());
                            if (inspectorateEntityOptional.isPresent()) {
                                map.put("affidata_a", "Ispettorato");
                                map.put("ispettorato_des", entrusted.getEntrustedTo());
                            }
                        }
                        break;
                    case MANAGER:
                        if(entrusted.getIdEntrustedTo() != null) {
                            Optional<InsuranceManagerEntity> insuranceManagerEntityOptional = insuranceManagerRepository.findById(entrusted.getIdEntrustedTo());
                            if (insuranceManagerEntityOptional.isPresent()) {
                                InsuranceManagerEntity insuranceManagerEntity = insuranceManagerEntityOptional.get();
                                map.put("affidata_a", "Manager");
                                map.put("manager_des", entrusted.getEntrustedTo());
                            }
                        }
                        break;
                }
                map.put("number_sx_ctp", entrusted.getNumberSxCounterparty());
                if(entrusted.getDwlMan() != null){
                    map.put("data_dwl_affido", timestampFormat.format(entrusted.getDwlMan()));
                }
                //RICHIESTA DANNI STATUS
                map.put("status_richiestadanni", entrusted.getStatus());
            }
        }
        //DATI DA THEFT
        if(claimsEntity.getTheft() != null) {
            Theft theft = claimsEntity.getTheft();
            //FURTO TELEFONO AUTORITA
            map.put("furto_telefono_autorita", theft.getAuthorityTelephone());
            //AVVISO CENTRALE OPERATIVA
            map.put("avviso_centrale_operativa", adptBooleanToString(theft.getOperationsCenterNotified()));
            //FURTO CENTRALE ALD
            map.put("furto_centrale_ald", adptBooleanToString(theft.getTheftOccurredOnCenterAld()));
            //CODICE FORNITORE
            map.put("codice_fornitore", theft.getSupplierCode());

            //POSIZIONE RITROVAMENTO
            if(theft.getAddress() != null){
                Address address = theft.getAddress();
                map.put("ritrov_citta", address.getLocality());
                map.put("ritrov_regione", address.getRegion());
                map.put("ritrov_prov", address.getProvince());
                map.put("ritrov_indirizzo", address.getStreet() != null ? address.getStreet()+ " "+ (address.getStreetNr() != null ? address.getStreetNr() : "") : "");
            }

            //SOTTOSEQUESTRO
            map.put("sottosequestro", adptBooleanToString(theft.getUnderSeizure()));
            //RITROVATO ALL'ESTERO
            map.put("ritrov_estero", adptBooleanToString(theft.getFoundAbroad()));
            //RITROV AUTORITA PS
            map.put("ritrov_autorita_ps", adptBooleanToString(theft.getFindAuthorityPolice()));
            //RITROV AUTORITA CC
            map.put("ritrov_autorita_cc", adptBooleanToString(theft.getFindAuthorityCc()));
            //RITROV AUTORITA VVUU
            map.put("ritrov_autorita_vvuu", adptBooleanToString(theft.getFindAuthorityVvuu()));
            //RITROV AUTORITA DESC
            map.put("ritrov_autorita_desc", theft.getFindAuthorityData());
            //RITROV AUTORITA TEL
            map.put("ritrov_autorita_tel", theft.getFindAuthorityTelephone());
            //VEICOLO CO
            map.put("ritrov_veicolo", theft.getVehicleCo());
            //RICEZIONE CHIAVE 1
            map.put("rx_chiave_1", theft.getFirstKeyReception() != null ? timestampFormat.format(theft.getFirstKeyReception()) : "");
            //RICEZIONE CHIAVE 2
            map.put("rx_chiave_2", theft.getSecondKeyReception() != null ? timestampFormat.format(theft.getSecondKeyReception()) : "");
            //RICEZIONE DENUNCIA ORIGINALE
            map.put("rx_ori_denuncia", theft.getOriginalComplaintReception() != null ? timestampFormat.format(theft.getOriginalComplaintReception()) : "");
            //RICEZIONE DENUNCIA COPIA
            map.put("rx_cpy_denuncia", theft.getCopyComplaintReception() != null ? timestampFormat.format(theft.getCopyComplaintReception()) : "");
            //RAPINA
            map.put("rapina", adptBooleanToString(theft.getRobbery()));
            //RICEZIONE VERBARE RITROVAMENTO ORIGINALE
            map.put("rx_ori_verbritr", theft.getOriginalReportReception() != null ? timestampFormat.format(theft.getOriginalReportReception()) : "");
            //RICEZIONE VERBALE RITROVAMENTO COPIA
            map.put("rx_cpy_verbritr", theft.getCopyReportReception() != null ? timestampFormat.format(theft.getCopyReportReception()) : "");
        }
        //DATI DA EXEMPTION
        if(claimsEntity.getExemption() != null) {
            Exemption exemption = claimsEntity.getExemption();
            //FRANCHIGIA 1
            map.put("data1", exemption.getDeductImported1() != null ? timestampFormat.format(exemption.getDeductImported1()) : "");
            map.put("chiusura1", exemption.getDeductEnd1() != null ? timestampFormat.format(exemption.getDeductEnd1()) : "");
            //FRANCHIGIA 2
            map.put("data2", exemption.getDeductImported2() != null ? timestampFormat.format(exemption.getDeductImported2()) : "");

            //IMPORTO1
            if(exemption.getDeductPaid1() != null) {
                map.put("importo1", exemption.getDeductPaid1().toString());
            }

            if(exemption.getDeductPaid2() != null) {
                map.put("importo2", exemption.getDeductPaid2().toString());
            }
            map.put("chiusura2", exemption.getDeductEnd2() != null ? timestampFormat.format(exemption.getDeductEnd2()) : "");

            //TOTALE PAGATO
            if(exemption.getDeductTotalPaid() != null) {
                map.put("totale_franchigia_pagato", exemption.getDeductTotalPaid().toString());
            }
            //NUMERO POLIZZA
            map.put("numero_polizza", exemption.getPolicyNumber());
            //RIFERIMENTO COMPAGNIA
            map.put("riferimento_compagnia", exemption.getCompanyReference());
        }
        //DATI DA DAMAGED
        if(claimsEntity.getDamaged() != null) {
            Damaged damaged = claimsEntity.getDamaged();
            //FIRMA CAI
            if(baseData.getDriverSide().equalsIgnoreCase("A")){
                map.put("cai_firmato_a", adptBooleanToString(damaged.getCaiSigned()));
            }
            else if(baseData.getDriverSide().equalsIgnoreCase("B")) {
                map.put("cai_firmato_b", adptBooleanToString(damaged.getCaiSigned()));
            }

            //DATI IMPACT POINT
            if(damaged.getImpactPoint() != null){
                ImpactPoint impactPoint = damaged.getImpactPoint();
                map.put("veicolo_danni", impactPoint.getDamageToVehicle());
                map.put("veicolo_osservazioni", impactPoint.getIncidentDescription());
            }
            //DATI DRIVER
            if(damaged.getDriver() != null){
                Driver driver = damaged.getDriver();
                //COGNOME
                map.put("driver_cognome", driver.getLastname());
                //NOME
                map.put("driver_nome", driver.getFirstname());
                //TELEFONO
                map.put("driver_tel", driver.getPhone());
                //EMAIL
                map.put("driver_email", driver.getEmail());
                //DRIVER FERITO
                if(claimsEntity.getWoundedList() == null || claimsEntity.getWoundedList().isEmpty() || !isDriverInjury(claimsEntity.getWoundedList())) {
                    map.put("driver_lesioni", "NO");
                }
                else {
                    map.put("driver_lesioni", "SI");
                }
            }
            //DATI VEICOLO
            if(damaged.getVehicle() != null) {
                Vehicle vehicle = damaged.getVehicle();
                map.put("veicolo_desc", (vehicle.getMake() != null ? vehicle.getMake() : "") + " "+(vehicle.getModel() != null ? vehicle.getModel() : ""));
            }
            //DATI CONTRATTO
            if(damaged.getContract() != null) {
                Contract contract = damaged.getContract();

                //ID CONTRATTO
                if(contract.getContractId() != null) {
                    map.put("contratto_id", contract.getContractId().toString());
                }
                //TIPO CONTRATTO
                map.put("contratto_type", contract.getContractType());
                //DATA INIZIO CONTRATTO
                map.put("contratto_begin", dateFormat.format(contract.getStartDate()));
                //DATA FINE CONTRATTO
                map.put("contratto_end", dateFormat.format(contract.getEndDate()));
                //DATE CHE *in teoria* TORNANO DA MILES
                if( contract.getTakeinDate() != null) {
                    map.put("contratto_return", dateFormat.format(contract.getTakeinDate()));
                }
            }
            //DATI CLIENTE
            if(damaged.getCustomer() != null) {
                Customer customer = damaged.getCustomer();
                //CLIENTE ID
                map.put("cliente_id", customer.getCustomerId());
                //CLIENTE NOME
                map.put("cliente_nome", customer.getLegalName());
                //STATUS
                map.put("status_cliente", customer.getStatus());
            }
            //DATI COMPAGNIA ASSICURATIVA
            if(damaged.getInsuranceCompany() != null) {
                InsuranceCompany insuranceCompany = damaged.getInsuranceCompany();

                //DATI TPL
                if(insuranceCompany.getTpl() != null && insuranceCompany.getTpl().getInsuranceCompanyId() != null ){
                    Tpl tpl = insuranceCompany.getTpl();
                    map.put("polizza_num", tpl.getPolicyNumber());
                    Optional<InsuranceCompanyEntity> insuranceCompanyEntity = insuranceCompanyRepository.findById(tpl.getInsuranceCompanyId());
                    if(insuranceCompanyEntity.isPresent()){
                        map.put("compass_nome", insuranceCompanyEntity.get().getName());
                    }

                    //PENALE RCA
                    map.put("penale_rca", tpl.getDeductible());
                }

                //DATI PAI
                if(insuranceCompany.getPai() != null && insuranceCompany.getPai().getInsuranceCompanyId() != null){
                    Pai pai = insuranceCompany.getPai();
                    map.put("polizza_pai", pai.getPolicyNumber());
                    Optional<InsuranceCompanyEntity> insuranceCompanyEntity = insuranceCompanyRepository.findById(pai.getInsuranceCompanyId());
                    if(insuranceCompanyEntity.isPresent()){
                        map.put("compagnia_pai", insuranceCompanyEntity.get().getName());
                    }
                }

                //DATI LEGALE
                if(insuranceCompany.getLegalCost() != null && insuranceCompany.getLegalCost().getInsuranceCompanyId() != null){
                    LegalCost legalCost = insuranceCompany.getLegalCost();
                    map.put("polizza_legal", legalCost.getPolicyNumber());
                    Optional<InsuranceCompanyEntity> insuranceCompanyEntity = insuranceCompanyRepository.findById(legalCost.getInsuranceCompanyId());
                    if(insuranceCompanyEntity.isPresent()){
                        map.put("compagnia_legal", insuranceCompanyEntity.get().getName());
                    }
                }

                //DATI MATERIAL DAMAGE
                if(insuranceCompany.getKasko() != null){
                    map.put("penale_danni", insuranceCompany.getKasko().getDeductibleValue());
                }
                //DATI THEFT
                if(insuranceCompany.getTheft() != null){
                    map.put("penale_furto", insuranceCompany.getTheft().getDeductible());
                }
            }
            //DATI ANTIFURTO
            if(damaged.getAntiTheftService() != null) {
                AntiTheftService antiTheftService = damaged.getAntiTheftService();
                map.put("anttheft_active", adptBooleanToString(antiTheftService.getActive()));

                //DATI DA LISTA RICHIESTE ANTIFURTO
                if(antiTheftService.getAntiTheftList() != null && !antiTheftService.getAntiTheftList().isEmpty()){
                    List<AntiTheftRequest> antiTheftRequests = antiTheftService.getAntiTheftList();
                    AntiTheftRequest antiTheftRequest = antiTheftRequests.get(0);
                    for(AntiTheftRequest antiTheftRequest1 : antiTheftRequests){
                        if(antiTheftRequest1.getRequest().compareTo(antiTheftRequest.getRequest()) > 0) {
                            antiTheftRequest = antiTheftRequest1;
                        }
                    }

                    //DATA ULTIMO CHECK
                    map.put("atf_lastcheck", timestampFormat.format(antiTheftRequest.getRequest()));
                    //CODICE RISPOSTA
                    map.put("atf_codice_risposta", antiTheftRequest.getResponseType());
                    //MESSAGGIO RISPOSTA
                    map.put("atf_messaggio_risposta", antiTheftRequest.getResponseMessage());
                    map.put("esito", antiTheftRequest.getOutcome());
                    //NUMERO REPORT
                    if(antiTheftRequest.getSubReport() != null) {
                        map.put("atf_numrep", antiTheftRequest.getSubReport().toString());
                    }
                    //FORZA G
                    if(antiTheftRequest.getgPower() != null) {
                        map.put("atf_forzag", antiTheftRequest.getgPower().toString());
                    }
                    //NUMERO CRASH
                    if(antiTheftRequest.getCrashNumber() != null) {
                        map.put("atf_numcrash", antiTheftRequest.getCrashNumber().toString());
                    }
                    //ANOMALIA
                    map.put("atf_anomalia", adptBooleanToString(antiTheftRequest.getAnomaly()));
                }

                //DATI DA LISTA REGISTRY
                if(antiTheftService.getRegistryList() != null && !antiTheftService.getRegistryList().isEmpty()){
                    List<Registry> registries = antiTheftService.getRegistryList();
                    Registry registry = registries.get(0);
                    if(registry.getVoucherId() != null) {
                        map.put("atf_voucherid", registry.getVoucherId().toString());
                    }
                    map.put("atf_provider", registry.getProvider());
                    //NOME APPARATO
                    map.put("apparato_telematico", (registry.getProvider() != null ? registry.getProvider() : "") + " " + (registry.getCodPack() != null ? registry.getCodPack() : ""));
                }
            }
        }
        // CONTRO PARTI
        if(claimsEntity.getCounterparts() != null && !claimsEntity.getCounterparts().isEmpty()) {
            if (claimsEntity.getCounterparts().size() == 1) {
                CounterpartyEntity counterpartyEntity = claimsEntity.getCounterparts().get(0);
                if (baseData.getDriverSide().equalsIgnoreCase("A")) {
                    map.put("cai_firmato_b", adptBooleanToString(counterpartyEntity.getCaiSigned()));
                }
                else if (baseData.getDriverSide().equalsIgnoreCase("B")) {
                    map.put("cai_firmato_a", adptBooleanToString(counterpartyEntity.getCaiSigned()));
                }
                map.put("eleggibilita", adptBooleanToString(counterpartyEntity.getEligibility()));
            }
            else {
                map.put("eleggibilita", "NO");
            }
        }

        if(claimsEntity!=null && claimsEntity.getTheft()!=null ) {
                map.put("pos_amm_definita_furti", adptBooleanToString(claimsEntity.getTheft().getAdministrativePosition()));
                map.put("gestione_contabile_furti", adptBooleanToString(claimsEntity.getTheft().getAccountManagement()));
                map.put("gestione_chiavi", adptBooleanToString(claimsEntity.getTheft().getKeyManagement()));
        }

        return map;
    }

    public Map<String, Object> ctp(
        Map<String, Object> map,
        CounterpartyEntity counterpartyEntity,
        ClaimsExportBaseData baseData
    ) {
        // dd-MM-yyyy
        SimpleDateFormat simpleDateFormat = this.getDateFormat();

        map = this.baseFill(map, baseData);

        //DATI DA VEICOLO
        if(counterpartyEntity.getVehicle() != null) {
            Vehicle vehicle = counterpartyEntity.getVehicle();
            //TARGA danneggiato
            map.put("targa_danneggiato", baseData.getDamagedPlate());
            //TIPOLOGIA VEICOLO
            map.put("ctp_tip_veicolo", EnumAdapter.adptFromVehicleNatureToString(vehicle.getNature()));
        }
        //DATI DA ASSICURATO
        if(counterpartyEntity.getInsured() != null) {
            Insured insured = counterpartyEntity.getInsured();
            //COGNOME
            map.put("ctp_cognome", insured.getLastname());
            //NOME
            map.put("ctp_nome", insured.getFirstname());
        }
        //DATI DA DRIVER
        if(counterpartyEntity.getDriver() != null) {
            Driver driver = counterpartyEntity.getDriver();
            //COGNOME
            map.put("ctp_driver_cognome", driver.getLastname());
            //NOME
            map.put("ctp_driver_nome", driver.getFirstname());
            //INDIRIZZO
            if(driver.getMainAddress() != null){
                Address address = driver.getMainAddress();
                map.put("ctp_driver_indirizzo", address.getStreet() != null ? address.getStreet()+ " "+ (address.getStreetNr() != null ? address.getStreetNr() : "") : "");
            }
            //DATA DI NASCITA
            map.put("ctp_driver_nascita", driver.getDateOfBirth() != null ? simpleDateFormat.format(driver.getDateOfBirth()) : "");
            //TELEFONO
            map.put("ctp_driver_tel", driver.getPhone());
            //EMAIL
            map.put("ctp_driver_email", driver.getEmail());
            //PATENTE
            if(driver.getDrivingLicense() != null) {
                DrivingLicense drivingLicense = driver.getDrivingLicense();
                //NUMERO PATENTE
                map.put("ctp_driver_patente", drivingLicense.getNumber());
                //CATEGORIA
                map.put("ctp_categoria", drivingLicense.getCategory() != null ? drivingLicense.getCategory().getValue() : "");
                //VALIDITA
                StringBuilder validiStringBuilder = new StringBuilder();
                if(drivingLicense.getValidFrom()!=null){
                    validiStringBuilder.append("DAL " + simpleDateFormat.format(drivingLicense.getValidFrom()) + " ");
                }
                if(drivingLicense.getValidTo()!=null){
                    validiStringBuilder.append("AL "+simpleDateFormat.format(drivingLicense.getValidTo()));
                }
                map.put("ctp_validita_driver", validiStringBuilder.toString());
            }
            //CODICE FISCALE
            map.put("ctp_cf", driver.getFiscalCode());
        }
        //SE E' UN VEICOLO
        map.put("ctp_veicolo", EnumAdapter.adptFromVehicleTypeToString(counterpartyEntity.getType()));
        //RESPONSABILE
        map.put("ctp_responsabile", adptBooleanToString(counterpartyEntity.getResponsible()));

        //ASSICURAZIONE
        if(counterpartyEntity.getInsuranceCompany() != null) {
            InsuranceCompanyCounterparty insuranceCompanyCounterparty = counterpartyEntity.getInsuranceCompany();
            InsuranceCompanyEntity insuranceCompanyEntity = insuranceCompanyCounterparty.getEntity();
            map.put("ctp_assicurazione", insuranceCompanyEntity.getName());
        }
        return map;
    }

    public Map<String, Object> authority(
        Map<String, Object> map,
        ClaimsEntity claimsEntity,
        Authority authority,
        PODetails poDetails,
        ClaimsExportBaseData baseData
    ) {
        // dd-MM-yyyy
        SimpleDateFormat dateFormat = this.getDateFormat();
        // dd-MM-yyyy HH:mm
        SimpleDateFormat timestampFormat = this.getTimestampFormat();

        map = this.baseFill(map, baseData);

        // DATA SINISTRO
        if( claimsEntity.getComplaint() != null && claimsEntity.getComplaint().getDataAccident() != null ) {
            map.put("data_sx", dateFormat.format( claimsEntity.getComplaint().getDataAccident().getDateAccident()) );
        }
        //NOME RIPARATORE
        if(authority.getCommodityDetails() != null){
            CommodityDetails commodityDetails = authority.getCommodityDetails();
            map.put("riparat_nome", commodityDetails.getName());
        }
        //STATO RIPARAZIONE
        map.put("repstatusdes", AuthorityAdapter.adptWorkingStatusFromEnglishToItalian(authority.getWorkingStatus()));
        //RIPARAZIONE BLOCCATA
        map.put("replock", adptBooleanToString( claimsEntity.getAuthorityLinkable()) );
        //VALORE VEICOLO
        map.put("valveicolo", authority.getWreckValue());
        //NUMERO PRATICA
        map.put("practice_number", authority.getWorkingNumber());
        //PREVENTIVO - TOTAL
        map.put("preventivo", authority.getTotal() != null ? authority.getTotal().toString() : "");
        //PO AUTORIZZATO
        map.put("po_autorizzato", authority.getAcceptingDate() != null ? timestampFormat.format(authority.getAcceptingDate()) : "");
        //PO RIFIUTATO
        map.put("po_no_autorizzato", authority.getRejectionDate() != null ? timestampFormat.format(authority.getRejectionDate()) : "");
        //WRECK
        map.put("po_totalloss", adptBooleanToString(authority.getWreck()));
        //PENALI
        map.put("penali_practice", authority.getNumberFranchise() != null ? authority.getNumberFranchise().toString() : "");
        //MOTIVAZIONE RIFIUTATO
        map.put("reject_motivation", authority.getNoteRejection());
        //PENALI MILES
        map.put("penali_miles", claimsEntity.getTotalPenalty() != null ? claimsEntity.getTotalPenalty().toString() : "");
        //FATTURATA
        map.put("fatturata", adptBooleanToString(authority.getInvoiced()));
        //OLDEST
        map.put("oldest", adptBooleanToString(authority.getOldest()));
        // TIPO PRATICA
        map.put("tipo_pratica", translateAuthorityType( authority.getType().getValue() ));
        //SINISTRI ASSOCIATI
        List<ClaimsNewEntity> claimsNewEntitiesAssociated = claimsNewRepository.getClaimsToCheckDuplicateAuthority(authority.getAuthorityDossierId(), authority.getAuthorityWorkingId());
        List<ClaimsEntity> claimsEntitiesAssociated = converterClaimsService.wrapFromClaimsNewEntityListToClaimsEntityList(claimsNewEntitiesAssociated);
        if( claimsEntitiesAssociated != null ) {
            String sxAssociated = "";
            for(ClaimsEntity claimsEntity1 : claimsEntitiesAssociated){
                sxAssociated += claimsEntity1.getPracticeId() + ",";
            }
            if (!sxAssociated.equals("")) {
                sxAssociated = sxAssociated.substring(0, sxAssociated.length() - 1);
            }
            map.put("sx_associati", sxAssociated);
        }
        if( poDetails != null ) {
            //PO NUMBER
            map.put("po_number", poDetails.getPoNumber());
            //PO TIPO
            map.put("po_tipo", poDetails.getPoTarget());
            //PO AMOUNT
            map.put("po_amount", poDetails.getPoAmount() != null ? poDetails.getPoAmount().toString() : "");
        }

        return map;
    }

    public Map<String, Object> refund(
        Map<String, Object> map,
        ClaimsEntity claimsEntity,
        Split split,
        ClaimsExportBaseData baseData
    ) {
        // dd-MM-yyyy HH:mm
        SimpleDateFormat timestampFormat = this.getTimestampFormat();

        Refund refund = claimsEntity.getRefund();

        map = this.baseFill(map, baseData);

        if( split != null ) {
            //DATA PAGAMENTO SINGOLO
            map.put("liq1_dataval", split.getIssueDate() != null ? timestampFormat.format(split.getIssueDate()) : "");
            //TIPOLOGIA
            map.put("liq1_tipo", EnumAdapter.adptFromSplitTypeEnumToString(split.getType()));
            //IMPORTO MATERIALE
            map.put("liq1_materiale", split.getMaterialAmount() != null ? split.getMaterialAmount().toString() : "");
            //SPESE LEGALI
            map.put("liq1_legali", split.getLegalFees() != null ? split.getLegalFees().toString() : "");
            //FERMO TECNICO
            map.put("liq1_fermtec", split.getTechnicalStop() != null ? split.getTechnicalStop().toString() : "");
            //ASSEGNO - BONIFICO
            map.put("liq1_assbon", EnumAdapter.adptFromRefundTypeToString(split.getRefundType()));
            //IMPORTO PAGATO
            map.put("liq1_pagato", split.getTotalPaid() != null ? split.getTotalPaid().toString() : "");
            //NOTE PAGAMENTO
            map.put("liq_notepag", split.getNote());
            //SOMMA RICEVUTA
            map.put("somma_ricevuta", split.getReceivedSum() != null ? split.getReceivedSum().toString() : "");
        }
        //DATA DEFINIZIONE
        map.put("liq_datadef", refund.getDefinitionDate() != null ? timestampFormat.format(refund.getDefinitionDate()) : "");
        //TOTALE LIQUIDATO
        map.put("liq_totliq", refund.getTotalLiquidationReceived() != null ? refund.getTotalLiquidationReceived().toString() : "");
        //WRECK VALUE
        map.put("wreckvalue", refund.getWreckValuePre() != null ? refund.getWreckValuePre().toString() : "");
        //SOMMA PO
        map.put("sommapo", refund.getPoSum() != null ? refund.getPoSum().toString() : "");
        //WRECK
        map.put("wreck", adptBooleanToString(refund.getWreck()));
        //IMPORTO FRANCHIGIA FCM
        map.put("impfranfcm", refund.getFranchiseAmountFcm() != null ? refund.getFranchiseAmountFcm().toString() : "");
        //NBV
        map.put("nbv", refund.getNBV());
        //VALORE RELITTO
        map.put("valorerelitto", refund.getWreckValuePost() != null ? refund.getWreckValuePost().toString() : "");
        //IMPORTO DA ADDEBITARE
        map.put("impdaaddebitare", refund.getAmountToBeDebited() != null ? refund.getAmountToBeDebited().toString() : "");
        //EUROTAX BLU
        map.put("eurotax", refund.getBluEurotax());
        //DATA ADDEBITO DELTA
        map.put("dataaddebitodelta", refund.getDeltaDebitDate() != null ? timestampFormat.format(refund.getDeltaDebitDate()) : "");
        //TOTALE RIMBORSO ATTESO
        if(
            refund.isWreck() &&
            claimsEntity.getDamaged() != null &&
            claimsEntity.getDamaged().getVehicle() != null &&
            claimsEntity.getDamaged().getVehicle().getNetBookValue() != null
        ) {
            map.put("totalerimborsoatteso", claimsEntity.getDamaged().getVehicle().getNetBookValue() != null ? claimsEntity.getDamaged().getVehicle().getNetBookValue().toString() : "");
        }
        else if (
            !refund.isWreck() &&
            claimsEntity.isInvoiced()
        ) {
            map.put("totalerimborsoatteso", refund.getPoSum() != null ? refund.getPoSum().toString() : "");
        }
        else {
            map.put("totalerimborsoatteso", 0);
        }

        return map;
    }

    public Map<String, Object> wounded(
        Map<String, Object> map,
        Wounded wounded,
        ClaimsExportBaseData baseData
    ) {

        map = this.baseFill(map, baseData);

        //TIPOLOGIA FERITO
        map.put("tipferito", EnumAdapter.adptFromWoundedTypeToString(wounded.getType()));
        //COGNOME
        map.put("cognome", wounded.getLastname());
        //NOME
        map.put("nome", wounded.getFirstname());
        //INDIRIZZO
        if(wounded.getAddress() != null){
            Address address = wounded.getAddress();
            map.put("indirizzo", address.getStreet() != null ? address.getStreet()+ " "+ (address.getStreetNr() != null ? address.getStreetNr() : "") : "");
        }
        //PRONTO SOCCORSO
        map.put("prontosoccorso", adptBooleanToString(wounded.getEmergencyRoom()));
        //GRAVITA FERITE
        map.put("ferite", EnumAdapter.adptFromWoundedWoundToString(wounded.getWound()));
        //PRIVACY
        map.put("privacy", wounded.getPrivacyAccepted());

        return map;
    }

    public Map<String, Object> deponent(
        Map<String, Object> map,
        Deponent deponent,
        ClaimsExportBaseData baseData
    ) {
        map = this.baseFill(map, baseData);
        //COGNOME
        map.put("cognome", deponent.getLastname());
        //NOME
        map.put("nome", deponent.getFirstname());
        //INDIRIZZO
        if(deponent.getAddress() != null) {
            Address address = deponent.getAddress();
            map.put("indirizzo", address.getStreet() != null ? address.getStreet()+ " "+ (address.getStreetNr() != null ? address.getStreetNr() : "") : "");
        }
        //CODICE FISCALE
        map.put("cf", deponent.getFiscalCode());
        //TELEFONO
        map.put("telefono", deponent.getPhone());
        //EMAIL
        map.put("email", deponent.getEmail());
        return map;
    }

    public Map<String, Object> attachment(
        Map<String, Object> map,
        Attachment attachment,
        ClaimsExportBaseData baseData
    ) {
        map = this.baseFill(map, baseData);
        //TIPOLOGIA FILE
        if(attachment.getFileManagerName() == null || !attachment.getFileManagerName().startsWith("AUTH_")){
            map.put("tipologia", this.translateAttachmentType( attachment.getDescription() ));
        }
        //NOME FILE
        map.put("nomefile", attachment.getFileManagerName());
        //VISIBILITA
        map.put("visibilita", adptBooleanHiddenToString(attachment.getHidden()));
        return map;
    }

    public Map<String, Object> additionalCosts(
        Map<String, Object> map,
        AdditionalCosts additionalCost,
        ClaimsExportBaseData baseData
    ) {
        map = this.baseFill(map, baseData);

        //COST TYPOLOGY
        if (additionalCost.getTypology() != null){
            map.put("additional_costs_typology", additionalCost.getTypology() );
        }

        //COSTS AMOUNT
        if (additionalCost.getAmount() != null){
            map.put("additional_costs_amount", additionalCost.getAmount());
        }

        //COSTS DATE
        if (additionalCost.getDate() != null){
            map.put("additional_costs_amount", additionalCost.getDate());
        }
        return map;
    }

    public String translateClaimsStatus(String status) {
        if( status == null || status.trim().equals("") ) {
            return "";
        }
        else if( status.equalsIgnoreCase("WAITING_FOR_VALIDATION") ) {
            return "Da Validare";
        }
        else if( status.equalsIgnoreCase("WAITING_FOR_AUTHORITY") ) {
            return "In Attesa";
        }
        else if( status.equalsIgnoreCase("TO_ENTRUST") ) {
            return "Da Affidare";
        }
        else if( status.equalsIgnoreCase("WAITING_FOR_REFUND") ) {
            return "In attesa rimborso";
        }
        else if( status.equalsIgnoreCase("CLOSED_TOTAL_REFUND") ) {
            return "Rimborsato Chiuso";
        }
        else if( status.equalsIgnoreCase("CLOSED") ) {
            return "Chiuso senza seguito";
        }
        else if( status.equalsIgnoreCase("DELETED") ) {
            return "Cancellata";
        }
        else if( status.equalsIgnoreCase("REJECTED") ) {
            return "Rifiutato";
        }
        else if( status.equalsIgnoreCase("INCOMPLETE") ) {
            return "Incompleto";
        }
        else if( status.equalsIgnoreCase("CLOSED_PARTIAL_REFUND") ) {
            return "Rimborso Parziale";
        }
        else if( status.equalsIgnoreCase("MANAGED") ) {
            return "Autogestita";
        }
        else {
            return status;
        }
    }

    public String translateAttachmentType(String type) {
        if( type == null || type.trim().equalsIgnoreCase("") ) {
            return "";
        }
        else if( type.equalsIgnoreCase("DOGE") ) {
            return "Riepilogo";
        }
        else if( type.equalsIgnoreCase("REFERENCE_ER") ) {
            return "Referto ferito";
        }
        else if( type.equalsIgnoreCase("OTHER") ) {
            return "Altro";
        }
        else if( type.equalsIgnoreCase("Olsa Invoicing") ) {
            return "Fattura";
        }
        else if( type.equalsIgnoreCase("PHOTO") ) {
            return "Foto";
        }
        else if( type.equalsIgnoreCase("ACCIDENTAL_DECLARATION") ) {
            return "Dichiarazione sinistro";
        }
        else if( type.equalsIgnoreCase("COMPLAINT") ) {
            return "Denucia";
        }
        else if( type.equalsIgnoreCase("WITNESS_DECLARATION") ) {
            return "Dichiarazione testimone";
        }
        else if( type.equalsIgnoreCase("CAI") ) {
            return "CAI";
        }
        else if( type.equalsIgnoreCase("PAI") ) {
            return "PAI";
        }
        else {
            return type;
        }
    }

    public String translateAuthorityType(String type) {
        if( type == null || type.trim().equalsIgnoreCase("") ) {
            return "";
        }
        else if( type.equalsIgnoreCase("CARBODY") ) {
            return "Carrozzeria";
        }
        else if( type.equalsIgnoreCase("MECHANIC") ) {
            return "Meccanica";
        }
        else if( type.equalsIgnoreCase("GLASS") ) {
            return "Cristalli";
        }
        else if( type.equalsIgnoreCase("OTHER") ) {
            return "Amministrativa";
        }
        else {
            return type;
        }
    }

}
