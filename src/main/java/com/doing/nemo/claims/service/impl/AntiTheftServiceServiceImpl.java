package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.controller.payload.response.AntiTheftServiceResponseV1;
import com.doing.nemo.claims.entity.enumerated.AntitheftEnum.AntitheftTypeEnum;
import com.doing.nemo.claims.entity.settings.AntiTheftServiceEntity;
import com.doing.nemo.claims.repository.AntiTheftServiceRepository;
import com.doing.nemo.claims.repository.AntiTheftServiceRepositoryV1;
import com.doing.nemo.claims.service.AntiTheftServiceService;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class AntiTheftServiceServiceImpl implements AntiTheftServiceService {

    private static Logger LOGGER = LoggerFactory.getLogger(AntiTheftServiceServiceImpl.class);
    @Autowired
    private AntiTheftServiceRepository antiTheftServiceRepository;
    @Autowired
    private AntiTheftServiceRepositoryV1 antiTheftServiceRepositoryV1;

    @Override
    public AntiTheftServiceEntity insertAntiTheftService(AntiTheftServiceEntity antiTheftServiceEntity) {
        if (antiTheftServiceRepository.searchAntiTheftServicebyName(antiTheftServiceEntity.getName()) != null) {
            LOGGER.debug("Error in AntiTheftService, duplicate name not admitted");
            throw new BadRequestException("Error in AntiTheftService, duplicate name not admitted", MessageCode.CLAIMS_1009);
        }
        antiTheftServiceRepository.save(antiTheftServiceEntity);
        return antiTheftServiceEntity;
    }

    @Override
    public AntiTheftServiceEntity updateAntiTheftService(AntiTheftServiceEntity antiTheftServiceEntity) {
        if (antiTheftServiceRepository.searchAntiTheftServiceWithDifferentId(antiTheftServiceEntity.getId(), antiTheftServiceEntity.getName()) != null) {
            LOGGER.debug("Error in AntiTheftService, duplicate name not admitted");
            throw new BadRequestException("Error in AntiTheftService, duplicate name not admitted", MessageCode.CLAIMS_1009);
        }
        antiTheftServiceRepository.save(antiTheftServiceEntity);
        return antiTheftServiceEntity;
    }

    @Override
    public AntiTheftServiceEntity selectAntiTheftService(UUID id) {
        Optional<AntiTheftServiceEntity> antiTheftServiceEntity = antiTheftServiceRepository.findById(id);
        if (!antiTheftServiceEntity.isPresent()) {
            LOGGER.debug("AntiTheftService with id " + id + " not found");
            throw new NotFoundException("AntiTheftService with id " + id + " not found", MessageCode.CLAIMS_1010);
        }

        return antiTheftServiceEntity.get();
    }

    @Override
    public List<AntiTheftServiceEntity> selectAllAntiTheftService() {
        List<AntiTheftServiceEntity> resultListAntiTheftService = antiTheftServiceRepository.findAll();
        return resultListAntiTheftService;
    }

    @Override
    public AntiTheftServiceResponseV1 deleteAntiTheftService(UUID id) {
        AntiTheftServiceEntity antiTheftServiceEntity = selectAntiTheftService(id);
        //if(antiTheftServiceRepository.searchContractForeignKey(id) != null)
        // throw new NotFoundException("Foreign key in Contract. AntiTheftService impossible to delete");
        antiTheftServiceRepository.delete(antiTheftServiceEntity);
        return null;
    }

    @Override
    public AntiTheftServiceEntity updateStatus(UUID uuid) {
        AntiTheftServiceEntity antiTheftServiceEntity = selectAntiTheftService(uuid);
        if (antiTheftServiceEntity.getIsActive()) {
            antiTheftServiceEntity.setIsActive(false);
        } else {
            antiTheftServiceEntity.setIsActive(true);
        }

        antiTheftServiceRepository.save(antiTheftServiceEntity);
        return antiTheftServiceEntity;
    }


    @Override
    public AntiTheftServiceEntity selectAntiTheftServiceByProvider(String provider) {
        AntiTheftServiceEntity antiTheftServiceEntity = antiTheftServiceRepositoryV1.searchAntiTheftServicebyProvider(provider);
        if (antiTheftServiceEntity == null) {
            LOGGER.debug("AntiTheftService with provider " + provider + " not found");
            throw new NotFoundException("AntiTheftService with provider " + provider + " not found", MessageCode.CLAIMS_1010);
        }

        return antiTheftServiceEntity;
    }

    @Override
    public List<AntiTheftServiceEntity> findAntiTheftServicesFiltered(Integer page, Integer pageSize, String orderBy, Boolean asc, String codeAntiTheftService, String name, String businessName, Integer supplierCode, String email, String phoneNumber, String providerType, Integer timeOutInSeconds, String endPointUrl, String username, String password, String companyCode, Boolean active, Boolean isActive, AntitheftTypeEnum type) {
        return antiTheftServiceRepositoryV1.findAntiTheftServicesFiltered(page, pageSize, orderBy, asc, codeAntiTheftService, name, businessName, supplierCode, email, phoneNumber, providerType, timeOutInSeconds, endPointUrl, username, password, companyCode, active, isActive, type);
    }

    @Override
    public Long countAntiTheftServiceFiltered(String codeAntiTheftService, String name, String businessName, Integer supplierCode, String email, String phoneNumber, String providerType, Integer timeOutInSeconds, String endPointUrl, String username, String password, String companyCode, Boolean active, Boolean isActive, AntitheftTypeEnum type) {
        return antiTheftServiceRepositoryV1.countAntiTheftServiceFiltered(codeAntiTheftService, name, businessName, supplierCode, email, phoneNumber, providerType, timeOutInSeconds, endPointUrl, username, password, companyCode, active, isActive, type);
    }

}