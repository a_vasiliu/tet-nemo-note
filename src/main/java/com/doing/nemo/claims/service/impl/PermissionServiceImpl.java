package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.controller.payload.request.ProfileRequestV1;
import com.doing.nemo.claims.controller.payload.response.IdentifierResponseV1;
import com.doing.nemo.claims.controller.payload.response.PermissionsResponseV1;
import com.doing.nemo.claims.controller.payload.response.ProfileModulePermissionsDetailResponseV1;
import com.doing.nemo.claims.controller.payload.response.ProfilesResponseV1;
import com.doing.nemo.claims.service.PermissionService;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.AbstractException;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.exception.InternalException;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.commons.util.HttpUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.Response;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashMap;

@Service
public class PermissionServiceImpl implements PermissionService {

    private static Logger LOGGER = LoggerFactory.getLogger(PermissionServiceImpl.class);
    @Value("${permission.api.endopoint}")
    private String endpointUrl;
    @Value("${permission.profile.claims.id}")
    private String profileClaimsId;
    @Autowired
    private HttpUtil httpUtil;

    private <T, P> T callWithoutCert(String url, HttpMethod method, P value, HashMap<String, String> headersparams, Class<T> outclass) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = null;
        Response responseToken = null;
        try {
            if (value != null) {
                jsonInString = mapper.writeValueAsString(value);
            }
            responseToken = httpUtil.callURL(headersparams, url, jsonInString, method);

            if (responseToken.code() == HttpStatus.NOT_FOUND.value()) {
                LOGGER.debug(responseToken.message() + " with code " + responseToken.code());
                throw new NotFoundException(responseToken.message() + " with code " + responseToken.code(), MessageCode.CLAIMS_1010);
            }

            if (responseToken.code() != HttpStatus.OK.value() && responseToken.code() != HttpStatus.NO_CONTENT.value() && responseToken.code() != HttpStatus.CREATED.value()) {
                LOGGER.debug(responseToken.message() + " with code " + responseToken.code());
                throw new BadRequestException(responseToken.message() + " with code " + responseToken.code(), MessageCode.CLAIMS_2000);
            }
            String response = responseToken.body().string();
            LOGGER.debug("Response Permission Body: {}", response);
            if (outclass == null) {
                return null;
            }
            return mapper.readValue(response, outclass);
        } catch (AbstractException e) {
            LOGGER.error(e.getMessage(), e);
            throw e;
        }  catch (Exception e) {
            String message = String.format(MessageCode.CLAIMS_2012.value(), e.getClass().getSimpleName(), e.getMessage());
            LOGGER.error(message, e);
            throw new InternalException(message, MessageCode.CLAIMS_2012);
        } finally {
            if (responseToken != null && responseToken.body() != null) {
                responseToken.close();
            }
        }
    }


    @Override
    public ProfilesResponseV1 getProfiles() throws IOException {
        String url = endpointUrl + "/api/v1/modules/" + profileClaimsId + "/profiles";

        ProfilesResponseV1 profilesResponseV1 = callWithoutCert(url, HttpMethod.GET, null, null, ProfilesResponseV1.class);
        return profilesResponseV1;

    }

    @Override
    public ProfileModulePermissionsDetailResponseV1 getPermissionByProfile(String idProfile) throws IOException {
        String url = endpointUrl + "/api/v1/profiles/" + idProfile;

        ProfileModulePermissionsDetailResponseV1 profileModulePermissionsDetailResponseV1 = callWithoutCert(url, HttpMethod.GET, null, null, ProfileModulePermissionsDetailResponseV1.class);
        return profileModulePermissionsDetailResponseV1;

    }


    @Override
    public PermissionsResponseV1 getPermissionByModule() throws IOException {
        String url = endpointUrl + "/api/v1/modules/" + profileClaimsId + "/permissions";

        PermissionsResponseV1 permissionsResponseV1 = callWithoutCert(url, HttpMethod.GET, null, null, PermissionsResponseV1.class);
        return permissionsResponseV1;

    }

    @Override
    public void updateProfile(String idProfile, ProfileRequestV1 request) throws IOException {
        String url = endpointUrl + "/api/v1/profiles/" + idProfile;

        callWithoutCert(url, HttpMethod.PUT, request, null, null);

    }

    //Create Custom Profile
    @Override
    public IdentifierResponseV1 createCustomProfile(ProfileRequestV1 request) throws IOException {
        String url = endpointUrl + "/api/v1/profiles";

        IdentifierResponseV1 IdentifierResponseV1 = callWithoutCert(url, HttpMethod.POST, request, null, IdentifierResponseV1.class);
        return IdentifierResponseV1;

    }

    //Delete Profile
    @Override
    public void deleteProfile(String idProfile) throws IOException {
        String url = endpointUrl + "/api/v1/profiles/" + idProfile;

        IdentifierResponseV1 IdentifierResponseV1 = callWithoutCert(url, HttpMethod.DELETE, null, null, null);

    }


}
