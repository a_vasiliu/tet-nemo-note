package com.doing.nemo.claims.service;

import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.settings.RepairerEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public interface RepairerService {
    RepairerEntity insertRepairer(RepairerEntity repairerEntity);
    RepairerEntity updateRepairer(RepairerEntity repairerEntity);
    void deleteRepairer(UUID uuid);
    RepairerEntity selectRepairer(UUID uuid);
    List<RepairerEntity> selectAllRepairer();
    RepairerEntity patchActive(UUID uuid);
    Pagination<RepairerEntity> paginationRepairer(int page, int pageSize);
}
