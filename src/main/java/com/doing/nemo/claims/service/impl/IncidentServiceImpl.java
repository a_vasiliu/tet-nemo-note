package com.doing.nemo.claims.service.impl;


import com.doing.nemo.claims.adapter.IncidentAdapter;
import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.ClaimRequest;
import com.doing.nemo.claims.controller.payload.request.IncidentRequestV1;
import com.doing.nemo.claims.controller.payload.response.IncidentResponseV1;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.ClaimsNewEntity;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsHistoricalUserEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.doing.nemo.claims.entity.enumerated.EventTypeEnum;
import com.doing.nemo.claims.entity.jsonb.Historical;
import com.doing.nemo.claims.exception.ExternalCommunicationException;
import com.doing.nemo.claims.repository.ClaimsNewRepository;
import com.doing.nemo.claims.repository.ClaimsRepository;
import com.doing.nemo.claims.service.ConverterClaimsService;
import com.doing.nemo.claims.service.IncidentService;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.AbstractException;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.util.HttpUtil;
import com.doing.nemo.middleware.client.MiddlewareClient;
import com.doing.nemo.middleware.client.payload.request.MiddlewareUpdateIncidentsRequest;
import com.doing.nemo.middleware.client.payload.response.MiddlewareGenericResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.hibernate.exception.LockAcquisitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.orm.jpa.JpaSystemException;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Optional;


@Service
public class IncidentServiceImpl implements IncidentService {

    @Autowired
    private MiddlewareClient middlewareClient;

    @Autowired
    private ClaimsRepository claimsRepository;

    @Autowired
    private IncidentAdapter incidentAdapter;

    @Autowired
    private HttpUtil httpUtil;

    @Value("${enjoy.retry.number}")
    private Integer retryNumber;

    @Autowired
    private ClaimsNewRepository claimsNewRepository;

    @Autowired
    private ConverterClaimsService converterClaimsService;

    private static Logger LOGGER = LoggerFactory.getLogger(IncidentServiceImpl.class);


    @Override
    @Transactional(isolation = Isolation.SERIALIZABLE, noRollbackFor ={LockAcquisitionException.class, JpaSystemException.class} )
    @Retryable(include = {LockAcquisitionException.class, JpaSystemException.class}, maxAttempts = 12,  backoff = @Backoff(delay = 2000))
    public IncidentResponseV1 insertIncident(String claimsId, ClaimsStatusEnum oldStatus) throws IOException,ExternalCommunicationException {
        LOGGER.info("Start call to incident service. ");
        Optional<ClaimsNewEntity> opt = claimsNewRepository.findByIdForUpdate(claimsId);
        if(!opt.isPresent()) {
            throw new BadRequestException(MessageCode.CLAIMS_1010);
        }
        LOGGER.info("Select claims for update ok");
        ClaimsNewEntity claimsNewEntity = opt.get();
        ClaimsEntity claimsEntity = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntity);

        IncidentRequestV1 incidentRequestV1 = incidentAdapter.adptFromClaimsEntityToIncidentRequest(claimsEntity, oldStatus, false,false);
        MiddlewareUpdateIncidentsRequest middlewareUpdateIncidentsRequest = IncidentAdapter.adptIncidentRequestToMiddlewareUpdateIncidentsRequest(incidentRequestV1);
        ObjectMapper map = new ObjectMapper();
        LOGGER.debug("[INCIDENT INFO] Request:"+ map.writeValueAsString(middlewareUpdateIncidentsRequest));

        if(claimsEntity.getDamaged()==null || claimsEntity.getDamaged().getContract()==null || claimsEntity.getDamaged().getContract().getContractId()==null){
            LOGGER.debug("[INCIDENT ERROR] "+MessageCode.CLAIMS_1111.value());
            throw new BadRequestException(MessageCode.CLAIMS_1105);
        }
        IncidentResponseV1 incidentResponseV1 = new IncidentResponseV1();
        MiddlewareGenericResponse response = new MiddlewareGenericResponse();
        String descriptionError = null;
            try{
                LOGGER.info("Start post to Miles");
                response = middlewareClient.postUpdateContractIncidents(claimsEntity.getDamaged().getContract().getContractId().toString(), middlewareUpdateIncidentsRequest);
                LOGGER.info("End post to Miles OK");
                incidentResponseV1.setError(false);
                incidentResponseV1.setDescription(null);
            } catch (AbstractException e){
                LOGGER.error("[INCIDENT ERROR] " + e.getDescription());
                incidentResponseV1.setError(true);
                incidentResponseV1.setDescription("Code:"+e.getCode()+" "+"Message:" +e.getDescription());
                descriptionError = e.getDescription();
                throw new ExternalCommunicationException(e.getDescription(), e, middlewareUpdateIncidentsRequest, incidentResponseV1);
            }finally {
                LOGGER.debug("[INCIDENT INFO] Response: " + incidentResponseV1.toString());
                LOGGER.debug("-- SYNC Mode -- End call to incident service. ");
                if (incidentResponseV1.getError().equals(true))
                    claimsEntity.addHistorical(this.addIncidentHistoricalInsert(incidentRequestV1, claimsEntity, true, descriptionError));
                else
                    claimsEntity.addHistorical(this.addIncidentHistoricalInsert(incidentRequestV1, claimsEntity, false, null));

                //setto solo il nuovo storico
                claimsNewEntity.setHistorical(claimsEntity.getHistorical());
                LOGGER.info("Save Miles Hystorical");
                claimsNewRepository.save(claimsNewEntity);
            }
        return incidentResponseV1;
    }

    /*
    @Override
    public IncidentResponseV1 callMiddlewareAndGetResponse (ClaimsEntity claimsEntity, MiddlewareUpdateIncidentsRequest middlewareUpdateIncidentsRequest){
        IncidentResponseV1 incidentResponseV1 = new IncidentResponseV1();
        MiddlewareGenericResponse response = new MiddlewareGenericResponse();
        String descriptionError = null;
        try{
            response = middlewareClient.postUpdateContractIncidents(claimsEntity.getDamaged().getContract().getContractId().toString(), middlewareUpdateIncidentsRequest);
            incidentResponseV1.setError(false);
            incidentResponseV1.setDescription(null);
        } catch (AbstractException e){
            LOGGER.error("[INCIDENT ERROR] " + e.getDescription());
            incidentResponseV1.setError(true);
            incidentResponseV1.setDescription("Code:"+e.getCode()+" "+"Message:" +e.getDescription());
            descriptionError = e.getDescription();
            throw new ExternalCommunicationException("Errore nell'inserimento su miles", e, middlewareUpdateIncidentsRequest);
        }
        return incidentResponseV1;
    }

     */

    @Override
    @Transactional(isolation = Isolation.SERIALIZABLE, noRollbackFor ={LockAcquisitionException.class, JpaSystemException.class} )
    @Retryable(include = {LockAcquisitionException.class, JpaSystemException.class}, maxAttempts = 12, backoff = @Backoff(delay = 2000))
    public ClaimsEntity insertIncidentEnjoy(ClaimsEntity claimsEntity, ClaimsStatusEnum oldStatus) throws IOException,ExternalCommunicationException {
        Boolean incidentIsOk = false;
        Integer incidentRetryNumber = 0;
        LOGGER.debug("-- SYNC Mode -- Start call to incident service. ");

        IncidentRequestV1 incidentRequestV1 = incidentAdapter.adptFromClaimsEntityToIncidentRequest(claimsEntity, oldStatus, false,false);
        MiddlewareUpdateIncidentsRequest middlewareUpdateIncidentsRequest = IncidentAdapter.adptIncidentRequestToMiddlewareUpdateIncidentsRequest(incidentRequestV1);
        ObjectMapper map = new ObjectMapper();
        LOGGER.debug("[INCIDENT INFO] Request:"+ map.writeValueAsString(middlewareUpdateIncidentsRequest));

        if(claimsEntity.getDamaged()==null || claimsEntity.getDamaged().getContract()==null || claimsEntity.getDamaged().getContract().getContractId()==null){
            LOGGER.debug("[INCIDENT ERROR] "+MessageCode.CLAIMS_1111.value());
            throw new BadRequestException(MessageCode.CLAIMS_1105);
        }
        IncidentResponseV1 incidentResponseV1 = new IncidentResponseV1();
        MiddlewareGenericResponse response = new MiddlewareGenericResponse();
        String descriptionError = null;
        while(incidentRetryNumber < retryNumber && !incidentIsOk) {

            try {
                response = middlewareClient.postUpdateContractIncidents(claimsEntity.getDamaged().getContract().getContractId().toString(), middlewareUpdateIncidentsRequest);
                incidentResponseV1.setError(false);
                incidentResponseV1.setDescription(null);
                incidentIsOk = true;
            } catch (AbstractException e) {
                incidentRetryNumber = incidentRetryNumber+1;
                LOGGER.debug("[ENJOY] Call to incident NOT OK at attempt number "+(incidentRetryNumber+1));
                LOGGER.error("[INCIDENT ERROR] " + e.getDescription());
                incidentResponseV1.setError(true);
                incidentResponseV1.setDescription("Code:" + e.getCode() + " " + "Message:" + e.getDescription());
                descriptionError = e.getDescription();
            }
        }
        LOGGER.debug("[INCIDENT INFO] Response: "+incidentResponseV1.toString());
        LOGGER.debug("-- SYNC Mode -- End call to incident service. ");
        if(incidentRetryNumber.equals(retryNumber)){
            LOGGER.debug("ENJOY] Call to incident failed on claim's id " + claimsEntity.getId());
            LOGGER.criticalError("NEMO_CLAIMS","ENJOY] Call to incident failed on claim's id " + claimsEntity.getId(), null );
        }
        if(incidentResponseV1.getError().equals(true)){
            throw new ExternalCommunicationException(descriptionError,middlewareUpdateIncidentsRequest, incidentResponseV1);
        }else{
            claimsEntity.addHistorical(this.addIncidentHistoricalInsert(incidentRequestV1,claimsEntity, false,null));
        }

        return claimsEntity;
    }

    public Historical addIncidentHistoricalInsert (IncidentRequestV1 requestV1, ClaimsEntity claimsEntity, Boolean error, String messageError)  {
        Historical historical = new Historical();
        historical.setEventType(EventTypeEnum.INCIDENT_SERVICE_INSERT);
        historical.setStatusEntityNew(claimsEntity.getStatus());
        historical.setStatusEntityOld(claimsEntity.getStatus());
        historical.setNewType(claimsEntity.getComplaint().getDataAccident().getTypeAccident());
        historical.setOldType(claimsEntity.getComplaint().getDataAccident().getTypeAccident());
        historical.setUpdateAt(DateUtil.getNowDate());
        historical.setUserName(ClaimsHistoricalUserEnum.SYSTEM_MILES.getValue());
        historical.setOldFlow(claimsEntity.getType().toValue());
        historical.setNewFlow(claimsEntity.getType().toValue());
        ObjectMapper omapper = new ObjectMapper();
        String result = null;
        /*try {
            result = omapper.writeValueAsString(requestV1);
        } catch (JsonProcessingException e) {
           LOGGER.debug("errore nella conversione oggetto incident in json");
        }*/

        if(error)
            result = "Invio al servizio incident non riuscito. ("+messageError+")";
        else {
            ClaimRequest claimRequest = null;
            if(requestV1.getClaims() != null && !requestV1.getClaims().isEmpty()){
                claimRequest = requestV1.getClaims().get(0);
            }
            result = "Invio al servizio incident riuscito. <br> ";
            result += " Campo: Sistema. " + "Valore precedente: Nessuno." + " Valore attuale: "+requestV1.getSystem()+". <br>";
            if(requestV1.getIncidentStatusEnumId()!=null)
                result += " Campo: Stato sinistro. " + "Valore precedente: Nessuno." + " Valore attuale: "+IncidentAdapter.adptIncidentStatusFromMilesToClaims(requestV1.getIncidentStatusEnumId())+". <br>";
            if(requestV1.getIncidentTypeEnumId()!=null)
                result += " Campo: Tipo sinistro. " + "Valore precedente: Nessuno." + " Valore attuale: "+IncidentAdapter.adptIncidentTypeFromMilesToClaims(requestV1.getIncidentTypeEnumId())+". <br>";
            if(requestV1.getIncidentDateTime()!=null)
                result += " Campo: Data sinistro. " + "Valore precedente: Nessuno." + " Valore attuale: "+requestV1.getIncidentDateTime()+". <br>";
            if(requestV1.getReference()!=null)
                result += " Campo: Riferimento sinistro. " + "Valore precedente: Nessuno." + " Valore attuale: "+requestV1.getReference()+". <br>";
            if(requestV1.getIncidentLocation()!=null)
                result += " Campo: Luogo incidente. " + "Valore precedente: Nessuno." + " Valore attuale: "+requestV1.getIncidentLocation()+". <br>";
            if(requestV1.getRegistrationDate()!=null)
                result += " Campo: Dichiarazione sinistro. " + "Valore precedente: Nessuno." + " Valore attuale: "+requestV1.getRegistrationDate()+". <br>";
            if(requestV1.getDescriptionCodeEnumId()!=null)
                result += " Campo: Tipologia sinistro. " + "Valore precedente: Nessuno." + " Valore attuale: "+IncidentAdapter.getMilesCodeFromTypeAccidentFromMilesToClaims(requestV1.getDescriptionCodeEnumId())+". <br>";
            if(requestV1.getRecoverable()!=null)
                result += " Campo: Riparazione recuperabile. " + "Valore precedente: Nessuno." + " Valore attuale: "+requestV1.getRecoverable()+". <br>";
            if(requestV1.getRetrievalDate()!=null)
                result += " Campo: Data recupero veicolo rubato. " + "Valore precedente: Nessuno." + " Valore attuale: "+requestV1.getRetrievalDate()+". <br>";
            if(requestV1.getRetrievAllocation()!=null)
                result += " Campo: Luogo ritrovamento veicolo. " + "Valore precedente: Nessuno." + " Valore attuale: "+requestV1.getRetrievAllocation()+". <br>";
            if(requestV1.getVehicleValue()!=null)
                result += " Campo: Valore veicolo alla data sinistro. " + "Valore precedente: Nessuno." + " Valore attuale: "+requestV1.getVehicleValue()+". <br>";
            if(requestV1.getExpectedRepairCost()!=null)
                result += " Campo: Stima riparazione. " + "Valore precedente: Nessuno." + " Valore attuale: "+requestV1.getExpectedRepairCost()+". <br>";
            if(requestV1.getInternalInsured()!=null)
                result += " Campo: Assicurato ALD. " + "Valore precedente: Nessuno." + " Valore attuale: "+requestV1.getInternalInsured()+". <br>";
            if(requestV1.getAccidentDriver()!=null)
                result += " Campo: Driver incidente. " + "Valore precedente: Nessuno." + " Valore attuale: "+requestV1.getAccidentDriver()+". <br>";
            if(requestV1.getExpectedWreckValue()!=null)
                result += " Campo: Valore veicolo wreck. " + "Valore precedente: Nessuno." + " Valore attuale: "+requestV1.getExpectedWreckValue()+". <br>";
            if(claimRequest != null && claimRequest.getPaymentComplete()!=null)
                result += " Campo: Pagamento completato. " + "Valore precedente: Nessuno." + " Valore attuale: "+claimRequest.getPaymentComplete()+". <br>";
            if(requestV1.getPctRecoverable()!=null)
                result += " Campo: % Recuperabilità. " + "Valore precedente: Nessuno." + " Valore attuale: "+requestV1.getPctRecoverable()+". <br>";
            if(requestV1.getCustomerResponsible()!=null)
                result += " Campo: Responsabilità driver. " + "Valore precedente: Nessuno." + " Valore attuale: "+requestV1.getCustomerResponsible()+". <br>";
            if(requestV1.getThirdpartyInvolved()!=null)
                result += " Campo: Terze parti coinvolte. " + "Valore precedente: Nessuno." + " Valore attuale: "+requestV1.getThirdpartyInvolved()+". <br>";
            if(requestV1.getTotalLoss()!=null)
                result += " Campo: Veicolo dichiarato wreck. " + "Valore precedente: Nessuno." + " Valore attuale: "+requestV1.getTotalLoss()+". <br>";
            if(requestV1.getTotalLossDate()!=null)
                result += " Campo: Data veicolo dichiarato wreck. " + "Valore precedente: Nessuno." + " Valore attuale: "+requestV1.getTotalLossDate()+". <br>";
            if(requestV1.getRebillType()!=null)
                result += " Campo: Tipologia addebito. " + "Valore precedente: Nessuno." + " Valore attuale: "+IncidentAdapter.adptRebillTypeFromMilesToClaim(requestV1.getRebillType())+". <br>";
            if(claimRequest != null && claimRequest.getPolicyNumber()!=null)
                result += " Campo: Numero polizza. " + "Valore precedente: Nessuno." + " Valore attuale: "+claimRequest.getPolicyNumber()+". <br>";
            if(claimRequest != null && claimRequest.getFileNumber()!=null)
                result += " Campo: Numero sinistro. " + "Valore precedente: Nessuno." + " Valore attuale: "+claimRequest.getFileNumber()+". <br>";
            if(claimRequest != null && claimRequest.getReceivedamount()!=null)
                result += " Campo: Importo ricevuto. " + "Valore precedente: Nessuno." + " Valore attuale: "+claimRequest.getReceivedamount()+". <br>";
            if(claimRequest != null && claimRequest.getPaymentDate()!=null)
                result += " Campo: Data pagamento. " + "Valore precedente: Nessuno." + " Valore attuale: "+claimRequest.getPaymentDate()+". <br>";
            if(claimRequest != null && claimRequest.getPaymentMethodEnumid()!=null)
                result += " Campo: Metodo di pagamento. " + "Valore precedente: Nessuno." + " Valore attuale: "+IncidentAdapter.adptPaymentMethodFromMilesToClaims(claimRequest.getPaymentMethodEnumid())+". <br>";
            if(claimRequest != null && claimRequest.getPaymentCommunication()!=null)
                result += " Campo: Note pagamento. " + "Valore precedente: Nessuno." + " Valore attuale: "+claimRequest.getPaymentCommunication()+". <br>";
            if(claimRequest != null && claimRequest.getRequestDate()!=null)
                result += " Campo: Data affido. " + "Valore precedente: Nessuno." + " Valore attuale: "+claimRequest.getRequestDate()+". <br>";
            if(claimRequest != null && claimRequest.getConfirmed()!=null)
                result += " Campo: Confermato. " + "Valore precedente: Nessuno." + " Valore attuale: "+claimRequest.getConfirmed()+". <br>";
            if(claimRequest != null && claimRequest.getClaimCategoryEnumId()!=null)
                result += " Campo: Categoria sinistro. " + "Valore precedente: Nessuno." + " Valore attuale: "+ IncidentAdapter.getMilesCodeFromCategoryEnumIdToClaims(claimRequest.getClaimCategoryEnumId())+". <br>";

        }
        historical.setComunicationDescription(result.replaceAll("null", "Nessuno").replaceAll("false","No").replaceAll("true", "Sì"));
        return historical;
    }


    @Override
    public IncidentResponseV1 modifyIncident(String claimsId, ClaimsStatusEnum oldStatus,Boolean franchise, IncidentRequestV1 oldValue, Boolean lastPayment, ClaimsEntity oldClaims) throws IOException,ExternalCommunicationException {
        LOGGER.debug("-- SYNC Mode -- Start call to incident service. ");
        Optional<ClaimsNewEntity> opt = claimsNewRepository.findById(claimsId);

        if(!opt.isPresent()) {
            throw new BadRequestException(MessageCode.CLAIMS_1010);
        }
        ClaimsNewEntity claimsNewEntity =  opt.get();

        ClaimsEntity claimsEntity = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntity);

        IncidentRequestV1 incidentRequestV1 = incidentAdapter.adptFromClaimsEntityToIncidentRequest(claimsEntity, oldStatus, franchise, lastPayment);
        if(incidentRequestV1.equals(oldValue))
        {
            LOGGER.debug("[INCIDENT INFO] No update is needed. No calls will be made to incident.");
            return null;
        }

        MiddlewareUpdateIncidentsRequest middlewareUpdateIncidentsRequest = IncidentAdapter.adptIncidentRequestToMiddlewareUpdateIncidentsRequest(incidentRequestV1);
        ObjectMapper map = new ObjectMapper();
        LOGGER.debug("[INCIDENT INFO] Request:"+ map.writeValueAsString(middlewareUpdateIncidentsRequest));


        if(claimsEntity.getDamaged()==null || claimsEntity.getDamaged().getContract()==null || claimsEntity.getDamaged().getContract().getContractId()==null){
            LOGGER.debug("[INCIDENT ERROR] "+ MessageCode.CLAIMS_1111.value());
            throw new BadRequestException(MessageCode.CLAIMS_1105);
        }
        IncidentResponseV1 incidentResponseV1 = new IncidentResponseV1();
        MiddlewareGenericResponse response = new MiddlewareGenericResponse();
        String descriptionError = null;
        try{
            response = middlewareClient.postUpdateContractIncidents(claimsEntity.getDamaged().getContract().getContractId().toString(),middlewareUpdateIncidentsRequest);
            incidentResponseV1.setError(false);
            incidentResponseV1.setDescription(null);
        } catch (AbstractException e){
            LOGGER.error("[INCIDENT ERROR] " + e.getDescription());
            incidentResponseV1.setError(true);
            incidentResponseV1.setDescription("Code:"+e.getCode()+" "+"Message:" +e.getDescription());
            descriptionError = e.getDescription();
            throw new ExternalCommunicationException(e.getDescription(), e, middlewareUpdateIncidentsRequest, incidentResponseV1);
        }finally {
            LOGGER.debug("[INCIDENT INFO] Response: "+incidentResponseV1.toString());
            LOGGER.debug("-- SYNC Mode -- End call to incident service. ");
            if(incidentResponseV1.getError().equals(true))
                claimsEntity.addHistorical(this.addIncidentHistoricalModify(incidentRequestV1,claimsEntity, true,oldValue, oldClaims, descriptionError));
            else {
                claimsEntity.addHistorical(this.addIncidentHistoricalModify(incidentRequestV1, claimsEntity, false, oldValue, oldClaims,null));
            }

            claimsNewEntity = converterClaimsService.convertOldEntityToNewClaimsEntity(claimsEntity);
            claimsNewRepository.save(claimsNewEntity);
        }
        return incidentResponseV1;
    }

    private Historical addIncidentHistoricalModify (IncidentRequestV1 requestV1, ClaimsEntity claimsEntity, Boolean error, IncidentRequestV1 oldValue, ClaimsEntity oldClaim, String errorMessage)  {
        LOGGER.debug("OLd value " + oldValue.toString());
        Historical historical = new Historical();
        historical.setEventType(EventTypeEnum.INCIDENT_SERVICE_UPDATE);
        historical.setStatusEntityNew(claimsEntity.getStatus());
        historical.setStatusEntityOld(oldClaim.getStatus());
        historical.setNewType(claimsEntity.getComplaint().getDataAccident().getTypeAccident());
        historical.setOldType(oldClaim.getComplaint().getDataAccident().getTypeAccident());
        historical.setUpdateAt(DateUtil.getNowDate());
        historical.setUserName(ClaimsHistoricalUserEnum.SYSTEM_MILES.getValue());
        historical.setUpdateAt(DateUtil.getNowDate());
        historical.setOldFlow(oldClaim.getType().toValue());
        historical.setNewFlow(claimsEntity.getType().toValue());
        ObjectMapper objectMapper = new ObjectMapper();
        String result= null;
        try {
            result = objectMapper.writeValueAsString(requestV1);
        } catch (JsonProcessingException e) {
            LOGGER.debug("errore nella conversione oggetto incident in json");
        }
        if(error)
            result = "Invio al servizio incident non riuscito. ("+errorMessage+")";
        else {
            result = "Invio al servizio incident riuscito. <br> ";
            if(!requestV1.getIncidentStatusEnumId().equals(oldValue.getIncidentStatusEnumId()))
                result += " Campo: Stato sinistro. " + "Valore precedente: " +IncidentAdapter.adptIncidentStatusFromMilesToClaims(oldValue.getIncidentStatusEnumId())+ ". Valore attuale: "+IncidentAdapter.adptIncidentStatusFromMilesToClaims(requestV1.getIncidentStatusEnumId())+". <br>";
            if(!requestV1.getIncidentTypeEnumId().equals(oldValue.getIncidentTypeEnumId()))
                result += " Campo: Tipo sinistro. " + "Valore precedente: " +IncidentAdapter.adptIncidentTypeFromMilesToClaims(oldValue.getIncidentTypeEnumId())+ ". Valore attuale: "+IncidentAdapter.adptIncidentTypeFromMilesToClaims(requestV1.getIncidentTypeEnumId())+". <br>";
            if(!requestV1.getIncidentDateTime().equals(oldValue.getIncidentDateTime()))
                result += " Campo: Data sinistro. " + "Valore precedente: " +oldValue.getIncidentDateTime()+ ". Valore attuale: "+requestV1.getIncidentDateTime()+". <br>";
            if(requestV1.getIncidentLocation()!=null && !requestV1.getIncidentLocation().equals(oldValue.getIncidentLocation()))
                result += " Campo: Luogo incidente. " + "Valore precedente: " +oldValue.getIncidentLocation()+ ". Valore attuale: "+requestV1.getIncidentLocation()+". <br>";
            if(requestV1.getRegistrationDate()!=null && !requestV1.getRegistrationDate().equals(oldValue.getRegistrationDate()))
                result += " Campo: Dichiarazione sinistro. " + "Valore precedente: " +oldValue.getRegistrationDate()+ ". Valore attuale: "+requestV1.getRegistrationDate()+". <br>";
            if(requestV1.getDescriptionCodeEnumId()!=null && !requestV1.getDescriptionCodeEnumId().equals(oldValue.getDescriptionCodeEnumId()))
                result += " Campo: Tipologia sinistro. " + "Valore precedente: " +IncidentAdapter.getMilesCodeFromTypeAccidentFromMilesToClaims(oldValue.getDescriptionCodeEnumId())+ ". Valore attuale: "+IncidentAdapter.getMilesCodeFromTypeAccidentFromMilesToClaims(requestV1.getDescriptionCodeEnumId())+". <br>";
            if(requestV1.getRecoverable()!=null && !requestV1.getRecoverable().equals(oldValue.getRecoverable()))
                result += " Campo: Riparazione recuperabile. " + "Valore precedente: " +oldValue.getRecoverable()+ ". Valore attuale: "+requestV1.getRecoverable()+". <br>";
            if(requestV1.getRetrievalDate()!=null && !requestV1.getRetrievalDate().equals(oldValue.getRetrievalDate()))
                result += " Campo: Data recupero veicolo rubato. " + "Valore precedente: " +oldValue.getRetrievalDate()+ ". Valore attuale: "+requestV1.getRetrievalDate()+". <br>";
            if(requestV1.getRetrievAllocation()!=null && !requestV1.getRetrievAllocation().equals(oldValue.getRetrievAllocation()))
                result += " Campo: Luogo ritrovamento veicolo. " + "Valore precedente: " +oldValue.getRetrievAllocation()+ ". Valore attuale: "+requestV1.getRetrievAllocation()+". <br>";
            if(requestV1.getVehicleValue()!=null )
            {
                if(oldValue.getVehicleValue() != null){
                    BigDecimal vehicleValueNew = BigDecimal.valueOf(requestV1.getVehicleValue());
                    BigDecimal vehicleValueOld = BigDecimal.valueOf(oldValue.getVehicleValue());
                    if(!vehicleValueNew.equals(vehicleValueOld)){
                        result += " Campo: Valore veicolo alla data sinistro. " + "Valore precedente: " +oldValue.getVehicleValue()+ ". Valore attuale: "+requestV1.getVehicleValue()+". <br>";
                    }
                } else
                {
                    result += " Campo: Valore veicolo alla data sinistro. " + "Valore precedente: Nessuno. Valore attuale: "+requestV1.getVehicleValue()+". <br>";
                }
            }
            if(requestV1.getExpectedRepairCost()!=null)
            {
                if(oldValue.getExpectedRepairCost() != null)
                {
                    BigDecimal expectedRepairCostNew = BigDecimal.valueOf(requestV1.getExpectedRepairCost());
                    BigDecimal expectedRepairCostOld = BigDecimal.valueOf(oldValue.getExpectedRepairCost());
                    if(!expectedRepairCostNew.equals(expectedRepairCostOld))
                    {
                        result += " Campo: Stima riparazione. " + "Valore precedente: " +oldValue.getExpectedRepairCost()+ ". Valore attuale: "+requestV1.getExpectedRepairCost()+". <br>";
                    }
                } else {
                    result += " Campo: Stima riparazione. " + "Valore precedente: Nessuno. Valore attuale: "+requestV1.getExpectedRepairCost()+". <br>";
                }
            }
            if(requestV1.getInternalInsured()!=null && !requestV1.getInternalInsured().equals(oldValue.getInternalInsured()))
                result += " Campo: Assicurato ALD. " + "Valore precedente: " +oldValue.getInternalInsured()+ ". Valore attuale: "+requestV1.getInternalInsured()+". <br>";
            if(requestV1.getAccidentDriver()!=null && !requestV1.getAccidentDriver().equals(oldValue.getAccidentDriver()))
                result += " Campo: Driver incidente. " + "Valore precedente: " + oldValue.getAccidentDriver()   + ". Valore attuale: "+ requestV1.getAccidentDriver() + ". <br>";
            if(requestV1.getExpectedWreckValue()!=null )
            {
                if(oldValue.getExpectedWreckValue() != null)
                {
                    BigDecimal expectedWreckValueNew = BigDecimal.valueOf(requestV1.getExpectedWreckValue());
                    BigDecimal expectedWreckValueOld = BigDecimal.valueOf(oldValue.getExpectedWreckValue());
                    if(!expectedWreckValueNew.equals(expectedWreckValueOld))
                    {
                        result += " Campo: Valore veicolo wreck. " + "Valore precedente: " +oldValue.getExpectedWreckValue()+ ". Valore attuale: "+requestV1.getExpectedWreckValue()+". <br>";
                    }
                } else {
                    result += " Campo: Valore veicolo wreck. " + "Valore precedente: Nessuno. Valore attuale: "+requestV1.getExpectedWreckValue()+". <br>";
                }
            }
            if(requestV1.getPctRecoverable()!=null)
            {
                if(oldValue.getPctRecoverable() != null)
                {
                    BigDecimal pctRecoverableNew = BigDecimal.valueOf(requestV1.getPctRecoverable());
                    BigDecimal pctRecoverableOld = BigDecimal.valueOf(oldValue.getPctRecoverable());
                    if(!pctRecoverableNew.equals(pctRecoverableOld))
                    {
                        result += " Campo: % Recuperabilità. " + "Valore precedente: "+ oldValue.getPctRecoverable()+ ". Valore attuale: "+requestV1.getPctRecoverable()+". <br>";
                    }
                } else {
                    result += " Campo: % Recuperabilità. " + "Valore precedente: Nessuno. Valore attuale: "+requestV1.getPctRecoverable()+". <br>";
                }
            }
            if(requestV1.getCustomerResponsible()!=null && !requestV1.getCustomerResponsible().equals(oldValue.getCustomerResponsible()))
                result += " Campo: Responsabilità driver. " + "Valore precedente: "+ oldValue.getCustomerResponsible() + ". Valore attuale: "+requestV1.getCustomerResponsible()+". <br>";
            if(requestV1.getThirdpartyInvolved()!=null && !requestV1.getThirdpartyInvolved().equals(oldValue.getThirdpartyInvolved()))
                result += " Campo: Terze parti coinvolte. " + "Valore precedente: "+ oldValue.getThirdpartyInvolved() + ". Valore attuale: "+requestV1.getThirdpartyInvolved()+". <br>";
            if(requestV1.getRebillType()!=null && !requestV1.getRebillType().equals(oldValue.getRebillType()))
                result += " Campo: Tipologia addebito. " + "Valore precedente: "+ IncidentAdapter.adptRebillTypeFromMilesToClaim(oldValue.getRebillType()) + ". Valore attuale: "+IncidentAdapter.adptRebillTypeFromMilesToClaim(requestV1.getRebillType())+". <br>";
            if(requestV1.getnRebills()!=null && !requestV1.getnRebills().equals(oldValue.getnRebills()))
                result += " Campo: Numero addebito. " + "Valore precedente: "+ IncidentAdapter.adptIncidentTotalRebillFromMilesToClaims(oldValue.getnRebills())  + ". Valore attuale: "+IncidentAdapter.adptIncidentTotalRebillFromMilesToClaims(requestV1.getnRebills())+". <br>";
            if(requestV1.getTotalLoss()!=null && !requestV1.getTotalLoss().equals(oldValue.getTotalLoss()))
                result += " Campo: Veicolo dichiarato wreck. " + "Valore precedente: "+ oldValue.getTotalLoss()  + ". Valore attuale: "+requestV1.getTotalLoss()+". <br>";
            if(requestV1.getTotalLossDate()!=null && !requestV1.getTotalLossDate().equals(oldValue.getTotalLossDate()))
                result += " Campo: Data veicolo dichiarato wreck. " + "Valore precedente: "+ oldValue.getTotalLossDate()  + ". Valore attuale: "+requestV1.getTotalLossDate()+". <br>";

            if(!requestV1.getClaims().isEmpty() && !oldValue.getClaims().isEmpty()){
                ClaimRequest claimRequestNew = requestV1.getClaims().get(0);
                ClaimRequest claimRequestOld = oldValue.getClaims().get(0);

                if(claimRequestNew.getPolicyNumber()!=null && !claimRequestNew.getPolicyNumber().equals(claimRequestOld.getPolicyNumber()))
                    result += " Campo: Numero polizza. " + "Valore precedente: "+ claimRequestOld.getPolicyNumber() + ". Valore attuale: "+claimRequestNew.getPolicyNumber()+". <br>";
                if(claimRequestNew.getFileNumber()!=null && !claimRequestNew.getFileNumber().equals(claimRequestOld.getFileNumber()))
                    result += " Campo: Numero sinistro. " + "Valore precedente: "+ claimRequestOld.getFileNumber() + ". Valore attuale: "+claimRequestNew.getFileNumber()+". <br>";
                if((claimRequestNew.getPaymentDate()!=null && !claimRequestNew.getPaymentDate().equals(claimRequestOld.getPaymentDate()))  ||
                        (claimRequestNew.getPaymentMethodEnumid()!=null && !claimRequestNew.getPaymentMethodEnumid().equals(claimRequestOld.getPaymentMethodEnumid())) ||
                        (claimRequestNew.getPaymentCommunication()!=null && !claimRequestNew.getPaymentCommunication().equals(claimRequestOld.getPaymentCommunication())) ||
                        (claimRequestNew.getReceivedamount()!=null)
                ){

                    if(claimRequestOld.getReceivedamount() != null){
                        BigDecimal receivedAmountNew = BigDecimal.valueOf(claimRequestNew.getReceivedamount());
                        BigDecimal receivedAmountOld = BigDecimal.valueOf(claimRequestOld.getReceivedamount());
                        if(!receivedAmountNew.equals(receivedAmountOld)){
                            result += " Campo: Metodo di pagamento. " + "Valore precedente: "+ IncidentAdapter.adptPaymentMethodFromMilesToClaims(claimRequestOld.getPaymentMethodEnumid()) + ". Valore attuale: "+IncidentAdapter.adptPaymentMethodFromMilesToClaims(claimRequestNew.getPaymentMethodEnumid())+". <br>";
                            result += " Campo: Note pagamento. " + "Valore precedente: "+ claimRequestOld.getPaymentCommunication() + ". Valore attuale: "+claimRequestNew.getPaymentCommunication()+". <br>";
                            result += " Campo: Data pagamento. " + "Valore precedente: "+ claimRequestOld.getPaymentDate() + ". Valore attuale: "+claimRequestNew.getPaymentDate()+". <br>";
                            result += " Campo: Importo ricevuto. " + "Valore precedente: "+ claimRequestOld.getReceivedamount() + ". Valore attuale: "+claimRequestNew.getReceivedamount()+". <br>";
                        }
                    } else {
                        result += " Campo: Metodo di pagamento. " + "Valore precedente: "+ IncidentAdapter.adptPaymentMethodFromMilesToClaims(claimRequestOld.getPaymentMethodEnumid()) + ". Valore attuale: "+IncidentAdapter.adptPaymentMethodFromMilesToClaims(claimRequestNew.getPaymentMethodEnumid())+". <br>";
                        result += " Campo: Note pagamento. " + "Valore precedente: "+ claimRequestOld.getPaymentCommunication() + ". Valore attuale: "+claimRequestNew.getPaymentCommunication()+". <br>";
                        result += " Campo: Data pagamento. " + "Valore precedente: "+ claimRequestOld.getPaymentDate() + ". Valore attuale: "+claimRequestNew.getPaymentDate()+". <br>";
                        result += " Campo: Importo ricevuto. " + "Valore precedente: Nessuno. Valore attuale: " + claimRequestNew.getReceivedamount() + ". <br>";
                    }

                }
                if(claimRequestNew.getRequestDate()!=null && !claimRequestNew.getRequestDate().equals(claimRequestOld.getRequestDate()))
                    result += " Campo: Data affido. " + "Valore precedente: "+ claimRequestOld.getRequestDate() + ". Valore attuale: "+claimRequestNew.getRequestDate()+". <br>";
                if(claimRequestNew.getConfirmed()!=null && !claimRequestNew.getConfirmed().equals(claimRequestOld.getConfirmed()))
                    result += " Campo: Confermato. " + "Valore precedente: "+ claimRequestOld.getConfirmed() + ". Valore attuale: "+claimRequestNew.getConfirmed()+". <br>";
                if(claimRequestNew.getClaimCategoryEnumId()!=null && !claimRequestNew.getClaimCategoryEnumId().equals(claimRequestOld.getClaimCategoryEnumId()))
                    result += " Campo: Categoria sinistro. " + "Valore precedente: "+ IncidentAdapter.getMilesCodeFromCategoryEnumIdToClaims(claimRequestOld.getClaimCategoryEnumId()) + ". Valore attuale: "+ IncidentAdapter.getMilesCodeFromCategoryEnumIdToClaims(claimRequestNew.getClaimCategoryEnumId())+". <br>";
                if(claimRequestNew.getPaymentComplete()!=null && !claimRequestNew.getPaymentComplete().equals(claimRequestOld.getPaymentComplete()))
                    result += " Campo: Pagamento completato. " + "Valore precedente: " +claimRequestOld.getPaymentComplete()+ ". Valore attuale: "+claimRequestNew.getPaymentComplete()+". <br>";
                if(claimRequestNew.getAmount()!=null)
                {
                    if(claimRequestOld.getAmount() != null)
                    {
                        BigDecimal amountOld = BigDecimal.valueOf(claimRequestOld.getAmount());
                        BigDecimal amountNew = BigDecimal.valueOf(claimRequestNew.getAmount());
                        if(!amountNew.equals(amountOld))
                        {
                            result += " Campo: Totale rimborso atteso. " + "Valore precedente: " +claimRequestOld.getAmount()+ ". Valore attuale: "+claimRequestNew.getAmount()+". <br>";
                        }
                    }
                } else {
                    result += " Campo: Totale rimborso atteso. " + "Valore precedente: Nessuno. Valore attuale: "+claimRequestNew.getAmount()+". <br>";
                }
            }


            else if(!oldValue.getClaims().isEmpty()){
                ClaimRequest claimRequestOld = oldValue.getClaims().get(0);
                if(claimRequestOld.getPolicyNumber()!=null)
                    result += " Campo: Numero polizza. " + "Valore precedente: "+ claimRequestOld.getPolicyNumber() + ". Valore attuale: Nessuno. <br>";
                if(claimRequestOld.getFileNumber()!=null)
                    result += " Campo: Numero sinistro. " + "Valore precedente: "+ claimRequestOld.getFileNumber() + ". Valore attuale: Nessuno. <br>";
                if((claimRequestOld.getPaymentDate()!=null)  ||
                        (claimRequestOld.getPaymentMethodEnumid()!=null ) ||
                        (claimRequestOld.getPaymentCommunication()!=null ) ||
                        (claimRequestOld.getReceivedamount()!=null)
                ){
                    result += " Campo: Data pagamento. " + "Valore precedente: "+ claimRequestOld.getPaymentDate() + ". Valore attuale: Nessuno. <br>";
                    result += " Campo: Importo ricevuto. " + "Valore precedente: "+ claimRequestOld.getReceivedamount() + ". Valore attuale: Nessuno. <br>";
                    result += " Campo: Metodo di pagamento. " + "Valore precedente: "+ IncidentAdapter.adptPaymentMethodFromMilesToClaims(claimRequestOld.getPaymentMethodEnumid()) + ". Valore attuale: Nessuno. <br>";
                    result += " Campo: Note pagamento. " + "Valore precedente: "+ claimRequestOld.getPaymentCommunication() + ". Valore attuale: Nessuno. <br>";
                }
                if(claimRequestOld.getRequestDate()!=null)
                    result += " Campo: Data affido. " + "Valore precedente: "+ claimRequestOld.getRequestDate() + ". Valore attuale: Nessuno. <br>";
                if(claimRequestOld.getConfirmed()!=null)
                    result += " Campo: Confermato. " + "Valore precedente: "+ claimRequestOld.getConfirmed() + ". Valore attuale: Nessuno. <br>";
                if(claimRequestOld.getClaimCategoryEnumId()!=null)
                    result += " Campo: Categoria sinistro. " + "Valore precedente: "+ IncidentAdapter.getMilesCodeFromCategoryEnumIdToClaims(claimRequestOld.getClaimCategoryEnumId()) + ". Valore attuale: Nessuno. <br>";
                if(claimRequestOld.getPaymentComplete()!=null)
                    result += " Campo: Pagamento completato. " + "Valore precedente: " +claimRequestOld.getPaymentComplete()+ ". Valore attuale: Nessuno. <br>";
                if(claimRequestOld.getAmount()!=null)
                    result += " Campo: Totale rimborso atteso. " + "Valore precedente: " +claimRequestOld.getAmount()+ ". Valore attuale: Nessuno. <br>";
            }


            else if(!requestV1.getClaims().isEmpty()){
                ClaimRequest claimRequestNew = requestV1.getClaims().get(0);
                if(claimRequestNew.getPolicyNumber()!=null)
                    result += " Campo: Numero polizza. " + "Valore precedente: Nessuno. Valore attuale: "+claimRequestNew.getPolicyNumber()+". <br>";
                if(claimRequestNew.getFileNumber()!=null)
                    result += " Campo: Numero sinistro. " + "Valore precedente: Nessuno. Valore attuale: "+claimRequestNew.getFileNumber()+". <br>";
                if((claimRequestNew.getPaymentDate()!=null)  ||
                        (claimRequestNew.getPaymentMethodEnumid()!=null ) ||
                        (claimRequestNew.getPaymentCommunication()!=null ) ||
                        (claimRequestNew.getReceivedamount()!=null)
                ){
                    result += " Campo: Data pagamento. " + "Valore precedente: Nessuno. Valore attuale: "+claimRequestNew.getPaymentDate()+". <br>";
                    result += " Campo: Importo ricevuto. " + "Valore precedente: Nessuno. Valore attuale: "+claimRequestNew.getReceivedamount()+". <br>";
                    result += " Campo: Metodo di pagamento. " + "Valore precedente: Nessuno. Valore attuale: "+IncidentAdapter.adptPaymentMethodFromMilesToClaims(claimRequestNew.getPaymentMethodEnumid())+". <br>";
                    result += " Campo: Note pagamento. " + "Valore precedente: Nessuno. Valore attuale: "+claimRequestNew.getPaymentCommunication()+". <br>";
                }
                if(claimRequestNew.getRequestDate()!=null)
                    result += " Campo: Data affido. " + "Valore precedente: Nessuno. Valore attuale: "+claimRequestNew.getRequestDate()+". <br>";
                if(claimRequestNew.getConfirmed()!=null)
                    result += " Campo: Confermato. " + "Valore precedente: Nessuno. Valore attuale: "+claimRequestNew.getConfirmed()+". <br>";
                if(claimRequestNew.getClaimCategoryEnumId()!=null)
                    result += " Campo: Categoria sinistro. " + "Valore precedente: Nessuno. Valore attuale: "+ IncidentAdapter.getMilesCodeFromCategoryEnumIdToClaims(claimRequestNew.getClaimCategoryEnumId())+". <br>";
                if(claimRequestNew.getPaymentComplete()!=null)
                    result += " Campo: Pagamento completato. " + "Valore precedente: Nessuno. Valore attuale: "+claimRequestNew.getPaymentComplete()+". <br>";
                if(claimRequestNew.getAmount()!=null)
                    result += " Campo: Totale rimborso atteso. " + "Valore precedente: Nessuno. Valore attuale: "+claimRequestNew.getAmount()+". <br>";
            }




        }
        historical.setComunicationDescription(result.replaceAll("null", "Nessuno").replaceAll("false","No").replaceAll("true", "Sì"));
        return historical;
    }


}
