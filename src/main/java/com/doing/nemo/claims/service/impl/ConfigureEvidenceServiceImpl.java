package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.adapter.ConfigureEvidenceAdapter;
import com.doing.nemo.claims.controller.payload.request.ConfigureEvidenceRequestV1;
import com.doing.nemo.claims.controller.payload.response.ConfigureEvidenceResponseV1;
import com.doing.nemo.claims.entity.settings.ConfigureEvidenceEntity;
import com.doing.nemo.claims.repository.ConfigureEvidenceRepository;
import com.doing.nemo.claims.repository.ConfigureEvidenceRepositoryV1;
import com.doing.nemo.claims.service.ConfigureEvidenceService;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ConfigureEvidenceServiceImpl implements ConfigureEvidenceService {

    private static Logger LOGGER = LoggerFactory.getLogger(ConfigureEvidenceServiceImpl.class);
    @PersistenceContext
    private EntityManager entityManager;
    @Autowired
    private ConfigureEvidenceRepository configureEvidenceRepository;
    @Autowired
    private ConfigureEvidenceRepositoryV1 configureEvidenceRepositoryV1;

    @Override
    @Transactional
    public UUID insertConfigureEvidence(ConfigureEvidenceRequestV1 configureEvidenceRequestV1) {

        ConfigureEvidenceEntity configureEvidenceEntity = new ConfigureEvidenceEntity();
        List<ConfigureEvidenceEntity> configureEvidenceEntity1 = configureEvidenceRepository.checkDuplicateEvidence(configureEvidenceRequestV1.getPracticalStateEn());
        if(configureEvidenceEntity1 != null && !configureEvidenceEntity1.isEmpty()){
            LOGGER.debug(MessageCode.CLAIMS_1125.value());
            throw new BadRequestException(MessageCode.CLAIMS_1125);
        }
        configureEvidenceEntity.setPracticalStateIt(configureEvidenceRequestV1.getPracticalStateIt());
        configureEvidenceEntity.setPracticalStateEn(configureEvidenceRequestV1.getPracticalStateEn());
        configureEvidenceEntity.setDaysOfStayInTheState(configureEvidenceRequestV1.getDaysOfStayInTheState());
        configureEvidenceEntity.setActive(configureEvidenceRequestV1.getActive());

        entityManager.persist(configureEvidenceEntity);
        return configureEvidenceEntity.getId();
    }

    @Override
    public ConfigureEvidenceResponseV1 getConfigureEvidence(UUID configurationId) {

        Optional<ConfigureEvidenceEntity> configureEvidenceOpt = configureEvidenceRepository.findById(configurationId);
        if (!configureEvidenceOpt.isPresent()) {
            LOGGER.debug("Configure Evidence with id " + configurationId + " not found");
            throw new NotFoundException("Configure Evidence with id " + configurationId + " not found", MessageCode.CLAIMS_1010);
        }

        return ConfigureEvidenceAdapter.adptConfigureEvidenceToConfigureEvidenceResponse(configureEvidenceOpt.get());
    }

    @Override
    @Transactional
    public ConfigureEvidenceResponseV1 updateConfigureEvidence(UUID configurationId, ConfigureEvidenceRequestV1 configureEvidenceRequest) {

        Optional<ConfigureEvidenceEntity> configureEvidenceOpt = configureEvidenceRepository.findById(configurationId);
        if (!configureEvidenceOpt.isPresent()) {
            LOGGER.debug("Configure Evidence with id " + configurationId + " not found");
            throw new NotFoundException("Configure Evidence with id " + configurationId + " not found", MessageCode.CLAIMS_1010);
        }

        ConfigureEvidenceEntity configureEvidence = configureEvidenceOpt.get();

        ConfigureEvidenceEntity configureEvidenceUpdate = ConfigureEvidenceAdapter.adptConfigureEvidenceRequestToConfigureEvidence(configureEvidenceRequest);
        configureEvidence.setDaysOfStayInTheState(configureEvidenceUpdate.getDaysOfStayInTheState());
        configureEvidence.setPracticalStateEn(configureEvidenceUpdate.getPracticalStateEn());
        configureEvidence.setPracticalStateIt(configureEvidenceUpdate.getPracticalStateIt());
        configureEvidence.setActive(configureEvidenceUpdate.getActive());

        configureEvidenceRepository.save(configureEvidence);

        return ConfigureEvidenceAdapter.adptConfigureEvidenceToConfigureEvidenceResponse(configureEvidence);
    }

    @Override
    public void deleteConfigureEvidence(UUID configurationId) {
        entityManager.remove(configurationId);
    }

    @Override
    @Transactional
    public ConfigureEvidenceResponseV1 patchStatusConfigureEvidence(UUID configurationId) {

        Optional<ConfigureEvidenceEntity> configureEvidenceOpt = configureEvidenceRepository.findById(configurationId);
        if (!configureEvidenceOpt.isPresent()) {
            LOGGER.debug("Configure Evidence with id " + configurationId + " not found");
            throw new NotFoundException("Configure Evidence with id " + configurationId + " not found", MessageCode.CLAIMS_1010);
        }

        ConfigureEvidenceEntity configureEvidence = configureEvidenceOpt.get();

        if (configureEvidence.getActive()) {
            configureEvidence.setActive(false);
        } else {
            configureEvidence.setActive(true);
        }

        configureEvidenceRepository.save(configureEvidence);

        return ConfigureEvidenceAdapter.adptConfigureEvidenceToConfigureEvidenceResponse(configureEvidence);
    }

    public List<ConfigureEvidenceResponseV1> getAllConfigureEvidence() {
        return ConfigureEvidenceAdapter.adptConfigureEvidenceToConfigureEvidenceResponse(configureEvidenceRepository.findAll());
    }


    @Override
    public List<ConfigureEvidenceEntity> findConfigureEvidencesFiltered(Integer page, Integer pageSize, String practicalStateIt, String practicalStateEn, Long daysOfStayInTheState, Boolean isActive, String orderBy, Boolean asc) {
        return configureEvidenceRepositoryV1.findConfigureEvidencesFiltered(page, pageSize, practicalStateIt, practicalStateEn, daysOfStayInTheState, isActive, orderBy, asc);
    }

    @Override
    public Long countConfigureEvidenceFiltered(String practicalStateIt, String practicalStateEn, Long daysOfStayInTheState, Boolean isActive) {
        return configureEvidenceRepositoryV1.countConfigureEvidenceFiltered(practicalStateIt, practicalStateEn, daysOfStayInTheState, isActive);
    }
}
