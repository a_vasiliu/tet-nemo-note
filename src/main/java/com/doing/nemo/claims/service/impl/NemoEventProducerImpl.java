package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.dto.KafkaEventDTO;
import com.doing.nemo.claims.dto.KafkaEventExportClaimsDTO;
import com.doing.nemo.claims.entity.enumerated.KafkaEventTypeEnum;
import com.doing.nemo.claims.service.NemoEventProducerService;
import com.doing.nemo.events.client.impl.EventsClientImpl;
import com.doing.nemo.events.client.payload.message.EventsMessage;
import com.doing.nemo.events.client.producer.EventsProducer;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.producer.KafkaProducer;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Map;

@Service
@ConditionalOnExpression(value = "${event.producer.startupEnabled} and not ${migration.enable}")
public class NemoEventProducerImpl extends EventsClientImpl implements NemoEventProducerService {
    private static final Logger LOGGER =
            LoggerFactory.getLogger(NemoEventProducerImpl.class);
    @Autowired
    private EventsProducer eventsProducer;

    KafkaProducer<String, String> producer;

    @PostConstruct
    public void init(){
        producer = eventsProducer.newProducer();
    }

    @Override
    public void validateMessageData(EventsMessage message) {
        try {
            Map<String,Object> data = message.getData();
            LOGGER.debug("Request: "+data);
        } catch(Exception e){
            LOGGER.debug(e.toString());
        }
    }

    @Async
    public void produceEvent(final EventsMessage message, final String topic) {
        newMessage(producer, topic, message);
    }

    public void produceEvent(KafkaEventDTO request, String topic, KafkaEventTypeEnum type){
        EventsMessage message = new EventsMessage();
        message.setClient("nemo-claims");
        message.setTimestamp(DateUtil.getNowInstant());
        ObjectMapper objectMapper = new ObjectMapper();
        Map<String,Object> map = objectMapper.convertValue(request,Map.class);
        message.setData(map);
        message.setType(type.getValue());

        this.produceEvent(message,topic);
    }

    public void produceEvent(KafkaEventExportClaimsDTO request, String topic, KafkaEventTypeEnum type){
        EventsMessage message = new EventsMessage();
        message.setClient("nemo-claims");
        message.setTimestamp(DateUtil.getNowInstant());
        ObjectMapper objectMapper = new ObjectMapper();
        Map<String,Object> map = objectMapper.convertValue(request,Map.class);
        message.setData(map);
        message.setType(type.getValue());

        this.produceEvent(message,topic);
    }
}
