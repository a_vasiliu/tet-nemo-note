package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.entity.CounterpartyEntity;
import com.doing.nemo.claims.entity.jsonb.Authority;
import com.doing.nemo.claims.entity.jsonb.repair.LastContact;
import com.doing.nemo.claims.service.ExportFieldTranslator;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class ExportFieldTranslatorImpl implements ExportFieldTranslator {

    protected static Logger LOGGER = LoggerFactory.getLogger(ExportFieldTranslatorImpl.class);

    @Override
    public String getRepairState(Authority authority) {
        switch (authority.getStatus()) {
            case WAITING_FOR_AUTHORIZATION: return  "Attesa autorizzazione";
            case UNAUTHORIZED: return "Non autorizzato";
            case AUTHORIZED: return "Autorizzato";
            default: LOGGER.error("exportCounterpartyPassingThroughFileManager authority status non riconosciuto");
        }
        return "";
    }

    @Override
    public String getRepairStatus(CounterpartyEntity currentCounterParty) {
        if(currentCounterParty.getRepairStatus() != null) {
            switch (currentCounterParty.getRepairStatus()) {
                case TO_SORT: return "Da smistare";
                case TO_CONTACT: return "Da contattare";
                case TO_RECONTACT: return "Da ricontattare";
                case WAITING_FOR_QUOTATION: return "In attesa preventivo";
                case QUOTATION_APPROVED: return "Preventivo approvato";
                case UNDER_REPAIR: return "In riparazione";
                case UNDER_LIQUIDATION: return "In liquidazione";
                case REPAIRED: return "Riparato";
                case CLOSED_LIQUIDATED: return "Chiuso liquidato";
                case CLOSED_REPAIRED: return "Chiuso riparato";
                case CLOSED_DENIAL: return "Chiuso diniego";
                // NON PIU' UTILIZZATI
                case REPAIRING: return "In riparazione";
                case TO_CONTACT_AGAIN: return "Da ricontattare";
                case WAITING_DOCUMENTS: return "In attesa dei documenti";
                case MANAGEABLE: return "Gestibile";
                case MSA_MANAGEMENT: return "Gestione MSA";
                case WAITING_FOR_NETWORK_TO_REPAIR: return "In attesa di Network per riparare";
                case WAITING_FOR_NETWORK_TO_LIQUIDATE: return "In attesa di Network per liquidare";
                case WAITING_FOR_INVOICE: return "In attesa di fattura";
                case CONFIRMED_LIQUIDATE: return "Liquidazione confermata";
                case CLOSED: return "Chiuso";
                case CANCELLED: return "Cancellato";
                case WRECK: return "Non riparabile";
                default: LOGGER.error("exportCounterpartyPassingThroughFileManager RepairStatus non riconosciuto");
            }
        }
        return "";
    }

    @Override
    public String getContactResult(LastContact lastContact) {
        switch (lastContact.getResult()) {
            case PENDING: return "In sospeso";
            case ACCEPTANCE: return "Accettazione";
            case DENIAL: return "Negato";
            case TO_RECONTACT: return "Da ricontattare";
            case REPAIR_ACCEPTED: return "Riparazione accettata";
            case LIQUIDATION_ACCEPTED: return "Liquidazione accettata";
            case REPAIR_FLOW_ACCEPTED: return "Flusso di riparazione accettato";
            default: LOGGER.error("exportCounterpartyPassingThroughFileManager lastContact result non riconosciuto");
        }
        return "";
    }

    @Override
    public String getProcedure(CounterpartyEntity counterParty) {
        switch (counterParty.getRepairProcedure()) {
            case ALD_REPAIRING: return "ALD";
            case STANDARD: return "MSA";
            default: LOGGER.error("exportCounterpartyPassingThroughFileManager procedure non riconosciuto");
        }
        return "";
    }

    @Override
    public String getType(CounterpartyEntity counterParty) {
        switch (counterParty.getType()) {
            case VEHICLE: return  "Veicolo";
            case OTHER: return "Altro";
            default: LOGGER.error("exportCounterpartyPassingThroughFileManager type non riconosciuto");
        }
        return "";
    }

    @Override
    public String getManagementType(CounterpartyEntity counterParty) {
        switch (counterParty.getManagementType()) {
            case PREVENTIVE: return  "Preventivo";
            case REPAIR: return "Riparazione";
            default: LOGGER.error("exportCounterpartyPassingThroughFileManager managementType non riconosciuto");
        }
        return "";
    }

}
