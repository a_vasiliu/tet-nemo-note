package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.controller.payload.response.InspectorateResponseV1;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.enumerated.DamagedEnum.VehicleEnum.VehicleTypeEnum;
import com.doing.nemo.claims.entity.settings.AutomaticAffiliationRuleInspectorateEntity;
import com.doing.nemo.claims.entity.settings.InspectorateEntity;
import com.doing.nemo.claims.repository.AutomaticAffiliationRuleInspectorateRepository;
import com.doing.nemo.claims.repository.InspectorateRepository;
import com.doing.nemo.claims.repository.InspectorateRepositoryV1;
import com.doing.nemo.claims.service.AutomaticAffiliationRuleInspectorateService;
import com.doing.nemo.claims.service.InspectorateService;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class InspectorateServiceImpl implements InspectorateService {

    private static Logger LOGGER = LoggerFactory.getLogger(InspectorateServiceImpl.class);
    @Autowired
    private InspectorateRepository inspectorateRepository;
    @Autowired
    private InspectorateRepositoryV1 inspectorateRepositoryV1;
    @Autowired
    private AutomaticAffiliationRuleInspectorateService automaticAffiliationRuleInspectorateService;
    @Autowired
    private AutomaticAffiliationRuleInspectorateRepository automaticAffiliationRuleInspectorateRepository;

    @Override
    public InspectorateEntity insertInspectorate(InspectorateEntity inspectorateEntity) {
        if (inspectorateRepository.searchInspectoratebyName(inspectorateEntity.getName()) != null) {
            LOGGER.debug("Error in Inspectorate, duplicate name not admitted");
            throw new BadRequestException("Error in Inspectorate, duplicate name not admitted", MessageCode.CLAIMS_1009);
        }
        if (inspectorateEntity.getAutomaticAffiliationRuleEntityList() != null) {
            List<AutomaticAffiliationRuleInspectorateEntity> automaticAffiliationRuleInspectorateEntityList = automaticAffiliationRuleInspectorateService.updateRuleInspectorateList(inspectorateEntity.getAutomaticAffiliationRuleEntityList());
        }
        inspectorateEntity.setActive(true);
        inspectorateRepository.save(inspectorateEntity);
        return inspectorateEntity;
    }

    @Override
    public InspectorateEntity updateInspectorateWithoutRule(InspectorateEntity inspectorateEntity) {
        inspectorateRepository.save(inspectorateEntity);
        return inspectorateEntity;
    }

    @Override
    public InspectorateResponseV1 deleteInspectorate(UUID uuid) {
        Optional<InspectorateEntity> inspectorateEntity2 = inspectorateRepository.findById(uuid);
        if (!inspectorateEntity2.isPresent()) {
            LOGGER.debug("Inspectorate with id " + uuid + " not found");
            throw new NotFoundException("Inspectorate with id " + uuid + " not found", MessageCode.CLAIMS_1010);
        }
        InspectorateEntity inspectorateEntity = inspectorateEntity2.get();
        //automaticAffiliationRuleInspectorateRepository.deleteAllRules(inspectorateEntity.getId());
        inspectorateRepository.delete(inspectorateEntity);
        return null;
    }

    @Override
    public InspectorateEntity selectInspectorate(UUID uuid) {
        Optional<InspectorateEntity> inspectorateEntity2 = inspectorateRepository.findById(uuid);
        if (!inspectorateEntity2.isPresent()) {
            LOGGER.debug("Inspectorate with id " + uuid + " not found");
            throw new NotFoundException("Inspectorate with id " + uuid + " not found", MessageCode.CLAIMS_1010);
        }
        return inspectorateEntity2.get();
    }

    @Override
    public List<InspectorateEntity> selectAllInspectorate() {
        List<InspectorateEntity> inspectorateEntityList = inspectorateRepository.findAll();

        return inspectorateEntityList;
    }

    public InspectorateEntity selectInspectorateWithMaxVolume(Boolean isNotCompanyCounterParty, Boolean isAbroad, Boolean isWounded, Boolean isRecoverability, UUID counterparty, DataAccidentTypeAccidentEnum typeClaim, VehicleTypeEnum counterpartType, UUID damaged, Double amount, Long practiceId) {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/yyyy");
        sdf.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));
        String dataStr = sdf.format(new Date());
        AutomaticAffiliationRuleInspectorateEntity automaticAffiliationRuleInspectorateEntity = null;
        InspectorateEntity inspectorateEntity = null;
        List<InspectorateEntity> inspectorateEntityList;
        int i;
        LOGGER.debug("[ENTRUST - " + practiceId +" - ISPECTORATE] isNotCompanyCounterParty: " + isNotCompanyCounterParty);
        if (isNotCompanyCounterParty != null && isNotCompanyCounterParty) {
            inspectorateEntityList = inspectorateRepository.getMaxVolumeInspectorate(isNotCompanyCounterParty, isAbroad, isWounded, isRecoverability, typeClaim, dataStr, damaged, amount, counterpartType);
            i = 1;
            if (inspectorateEntityList == null || inspectorateEntityList.isEmpty()) {
                inspectorateEntityList = inspectorateRepository.getMaxVolumeInspectorateAndMonthlyVolumeZero(isNotCompanyCounterParty, isAbroad, isWounded, isRecoverability, typeClaim, dataStr, damaged, amount, counterpartType);
                i = 2;
            }
        } else {
            inspectorateEntityList = inspectorateRepository.getMaxVolumeInspectorateWITHCounterparty(isNotCompanyCounterParty, isAbroad, isWounded, isRecoverability, typeClaim, dataStr, damaged, amount, counterparty, counterpartType);
            i = 3;
            if (inspectorateEntityList == null || inspectorateEntityList.isEmpty()) {
                inspectorateEntityList = inspectorateRepository.getMaxVolumeInspectorateWITHCounterpartyAndMonthlyVolumeZero(isNotCompanyCounterParty, isAbroad, isWounded, isRecoverability, typeClaim, dataStr, damaged, amount, counterparty, counterpartType);
                i = 4;
            }
        }


        LOGGER.debug("[ENTRUST - " + practiceId +" - ISPECTORATE] try case: " + i);
        LOGGER.debug("[ENTRUST - " + practiceId +" - ISPECTORATE] legalEntityList: " + inspectorateEntityList);
        if (!inspectorateEntityList.isEmpty()) {
            inspectorateEntity = inspectorateEntityList.get(0);
            List<AutomaticAffiliationRuleInspectorateEntity> automaticAffiliationRuleLegalEntities = null;
            switch (i) {
                case 1:
                    automaticAffiliationRuleLegalEntities = automaticAffiliationRuleInspectorateRepository.getMaxVolumeRule(isNotCompanyCounterParty, isAbroad, isWounded, isRecoverability, typeClaim, dataStr, damaged, amount, counterpartType, inspectorateEntity.getId());
                    break;
                case 2:
                    automaticAffiliationRuleLegalEntities = automaticAffiliationRuleInspectorateRepository.getMaxVolumeRuleAndMonthlyVolumeZero(isNotCompanyCounterParty, isAbroad, isWounded, isRecoverability, typeClaim, dataStr, damaged, amount, counterpartType, inspectorateEntity.getId());
                    break;
                case 3:
                    automaticAffiliationRuleLegalEntities = automaticAffiliationRuleInspectorateRepository.getMaxVolumeRuleWITHCounterparty(isNotCompanyCounterParty, isAbroad, isWounded, isRecoverability, typeClaim, dataStr, damaged, amount, counterparty, counterpartType, inspectorateEntity.getId());
                    break;
                case 4:
                    automaticAffiliationRuleLegalEntities = automaticAffiliationRuleInspectorateRepository.getMaxVolumeRuleWITHCounterpartyAndMonthlyVolumeZero(isNotCompanyCounterParty, isAbroad, isWounded, isRecoverability, typeClaim, dataStr, damaged, amount, counterparty, counterpartType, inspectorateEntity.getId());
                    break;
                default:
                    break;
            }

            LOGGER.debug("[ENTRUST - " + practiceId +" - ISPECTORATE] automaticAffiliationRuleLegalEntities: " + automaticAffiliationRuleLegalEntities);
            if (automaticAffiliationRuleLegalEntities == null || automaticAffiliationRuleLegalEntities.isEmpty()){
                LOGGER.debug("[ENTRUST - " + practiceId +" - ISPECTORATE] rule not found");
                return null;
            }

            automaticAffiliationRuleInspectorateEntity = automaticAffiliationRuleLegalEntities.get(0);
            automaticAffiliationRuleInspectorateRepository.updateCurrentVolumeRuleById(automaticAffiliationRuleInspectorateEntity.getId());
            LOGGER.debug("[ENTRUST - " + practiceId +" - ISPECTORATE] updateRule");
        }

        return inspectorateEntity;

    }

    @Override
    public InspectorateEntity insertInspectorateRule(List<AutomaticAffiliationRuleInspectorateEntity> automaticAffiliationRuleInspectorateEntityList, UUID uuid) {
        InspectorateEntity inspectorateEntity = selectInspectorate(uuid);
        for (AutomaticAffiliationRuleInspectorateEntity automaticAffiliationRuleInspectorateEntity : automaticAffiliationRuleInspectorateEntityList) {
            if (automaticAffiliationRuleInspectorateRepository.searchRuleForInspectoratebyName(automaticAffiliationRuleInspectorateEntity.getSelectionName(), uuid) != null) {
                LOGGER.debug("Error in Inspectorate Rule, duplicate name not admitted");
                throw new BadRequestException("Error in Inspectorate Rule, duplicate name not admitted", MessageCode.CLAIMS_1009);
            }
        }
        automaticAffiliationRuleInspectorateEntityList = automaticAffiliationRuleInspectorateService.updateRuleInspectorateList(automaticAffiliationRuleInspectorateEntityList);

        inspectorateEntity.getAutomaticAffiliationRuleEntityList().addAll(automaticAffiliationRuleInspectorateEntityList);
        inspectorateEntity = updateInspectorateWithoutRule(inspectorateEntity);
        return inspectorateEntity;
    }

    @Override
    public InspectorateEntity updateInspectorateRule(List<AutomaticAffiliationRuleInspectorateEntity> automaticAffiliationRuleInspectorateEntityList, InspectorateEntity inspectorateEntity) {

        automaticAffiliationRuleInspectorateEntityList = automaticAffiliationRuleInspectorateService.updateRuleInspectorateList(automaticAffiliationRuleInspectorateEntityList);
        inspectorateEntity.setAutomaticAffiliationRuleEntityList(automaticAffiliationRuleInspectorateEntityList);
        inspectorateEntity = updateInspectorateWithoutRule(inspectorateEntity);
        return inspectorateEntity;
    }

    @Override
    public InspectorateEntity updateInspectorate(InspectorateEntity inspectorateEntity, UUID uuid) {
        Optional<InspectorateEntity> inspectorateEntityOptional = inspectorateRepository.findById(uuid);
        if (!inspectorateEntityOptional.isPresent())
            throw new NotFoundException("Inspectorate not found", MessageCode.CLAIMS_1010);

        if (inspectorateRepository.searchInspectorateWithDifferentId(uuid, inspectorateEntity.getName()) != null) {
            LOGGER.debug("Error in Inspectorate Rule, duplicate name not admitted");
            throw new BadRequestException("Error in Inspectorate, duplicate name not admitted", MessageCode.CLAIMS_1009);
        }

        if(inspectorateEntity.getActive()==null){
            LOGGER.debug("Error in Inspectorate, isActive cannot be null.");
            throw new BadRequestException("Error in Inspectorate, isActive cannot be null.", MessageCode.CLAIMS_1161);
        }

        InspectorateEntity inspectorateEntity1 = inspectorateEntityOptional.get();

        for (AutomaticAffiliationRuleInspectorateEntity automaticAffiliationRuleInspectorateEntity : inspectorateEntity1.getAutomaticAffiliationRuleEntityList()) {
            automaticAffiliationRuleInspectorateRepository.delete(automaticAffiliationRuleInspectorateEntity);
        }
        if (inspectorateEntity.getAutomaticAffiliationRuleEntityList() != null) {
            inspectorateEntity = updateInspectorateRule(inspectorateEntity.getAutomaticAffiliationRuleEntityList(), inspectorateEntity);
        } else {
            inspectorateEntity = updateInspectorateWithoutRule(inspectorateEntity);
        }
        return inspectorateEntity;
    }

    @Override
    public InspectorateEntity updateStatus(UUID uuid) {
        InspectorateEntity inspectorateEntity = selectInspectorate(uuid);
        if (inspectorateEntity.getActive()!=null && inspectorateEntity.getActive()) {
            inspectorateEntity.setActive(false);
        } else {
            inspectorateEntity.setActive(true);
        }

        inspectorateRepository.save(inspectorateEntity);
        return inspectorateEntity;
    }

    @Override
    public List<InspectorateEntity> findInspectoratesFiltered(Integer page, Integer pageSize, String orderBy, Boolean asc, String name, String email, String province, String telephone, String fax, Boolean isActive, String address, String zipCode, String city, String state, Long code) {
        return inspectorateRepositoryV1.findInspectoratesFiltered(page, pageSize, orderBy, asc, name, email, province, telephone, fax, isActive, address, zipCode, city, state, code);
    }

    @Override
    public Long countInspectorateFiltered(String name, String email, String province, String telephone, String fax, Boolean isActive, String address, String zipCode, String city, String state, Long code) {
        return inspectorateRepositoryV1.countInspectorateFiltered(name, email, province, telephone, fax, isActive, address, zipCode, city, state, code);
    }

    @Transactional
    public void resetRuleJob() {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/yyyy");
        sdf.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));
        String dataStr = sdf.format(new Date());
        List<AutomaticAffiliationRuleInspectorateEntity> automaticAffiliationRuleInspectorateEntities = automaticAffiliationRuleInspectorateRepository.findAll();
        if (automaticAffiliationRuleInspectorateEntities != null) {
            for (AutomaticAffiliationRuleInspectorateEntity automaticAffiliationRuleInspectorateEntity : automaticAffiliationRuleInspectorateEntities) {
                if (!automaticAffiliationRuleInspectorateEntity.getCurrentMonth().equals(dataStr)) {
                    automaticAffiliationRuleInspectorateEntity.setCurrentMonth(dataStr);
                    automaticAffiliationRuleInspectorateEntity.setCurrentVolume(0);
                    automaticAffiliationRuleInspectorateRepository.save(automaticAffiliationRuleInspectorateEntity);
                }
            }
        }
    }

}