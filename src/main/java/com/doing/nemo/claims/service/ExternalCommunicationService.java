package com.doing.nemo.claims.service;

import com.doing.nemo.claims.controller.payload.request.IncidentRequestV1;
import com.doing.nemo.claims.controller.payload.response.IncidentResponseV1;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.doing.nemo.claims.entity.jsonb.UploadFile;
import com.doing.nemo.claims.websin.websinws.UploadFileResponseType;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.Future;

@Service
public interface ExternalCommunicationService {
    @Async
    Future<IncidentResponseV1> insertIncidentAsync(String claimsId, ClaimsStatusEnum oldStatus) throws IOException;
    @Async
    Future<ClaimsEntity> insertIncidentEnjoyAsync(ClaimsEntity claimsEntity, ClaimsStatusEnum oldStatus) throws IOException;
    @Async
    Future<IncidentResponseV1> modifyIncidentAsync(String claimsId, ClaimsStatusEnum oldStatus, Boolean franchise, IncidentRequestV1 oldValue, Boolean lastPayment, ClaimsEntity oldClaims) throws IOException;

    void modifyIncidentSync(String claimsId, ClaimsStatusEnum oldStatus, Boolean franchise, IncidentRequestV1 oldValue, Boolean lastPayment, ClaimsEntity oldClaims) throws IOException;

    @Async
    Future<UploadFileResponseType> uploadFile(List<UploadFile> uploadFileList, String claimsId);
    @Async
    void retryCallMilesWebSin();
}
