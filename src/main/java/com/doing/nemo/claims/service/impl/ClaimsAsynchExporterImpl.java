package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.dto.ClaimsExportBaseData;
import com.doing.nemo.claims.dto.ClaimsExportFilter;
import com.doing.nemo.claims.entity.AntiTheftRequestEntity;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.ClaimsNewEntity;
import com.doing.nemo.claims.entity.CounterpartyEntity;
import com.doing.nemo.claims.entity.jsonb.Authority;
import com.doing.nemo.claims.entity.jsonb.Deponent;
import com.doing.nemo.claims.entity.jsonb.PODetails;
import com.doing.nemo.claims.entity.jsonb.Wounded;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.AdditionalCosts;
import com.doing.nemo.claims.entity.jsonb.forms.jsonbForms.Attachment;
import com.doing.nemo.claims.entity.jsonb.refund.Split;
import com.doing.nemo.claims.repository.ClaimsNewRepository;
import com.doing.nemo.claims.service.AsynchExporter;
import com.doing.nemo.claims.service.AuthorityAttachmentService;
import com.doing.nemo.claims.service.ClaimsExportBaseDataBuilder;
import com.doing.nemo.claims.service.ConverterClaimsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service("ClaimsAsynchExporterImpl")
public class ClaimsAsynchExporterImpl extends AsynchExporter<ClaimsExportFilter> {

    @Value("${export.claims.blocksize}")
    private Integer exportClaimsBlocksize;

    @Value("${kafka.topic.export.claims}")
    private String kafkaTopicExportClaims;

    @Autowired
    private ClaimsNewRepository claimsNewRepository;

    @Autowired
    private ConverterClaimsService converterClaimsService;

    @Autowired
    private ClaimsExportBaseDataBuilder claimsExportBaseDataBuilder;

    @Autowired
    private ClaimsExportMapInitializerImpl claimsExportMapInitializer;

    @Autowired
    private AuthorityAttachmentService authorityAttachmentService;

    @Autowired
    private ClaimsExportMapFillerImpl claimsExportMapFiller;

    @Async
    @Override
    public void export(ClaimsExportFilter filter, int pagesNumber, int exportCount, String userEmail, String nemoUserId) throws IOException {
        int pageSize = exportClaimsBlocksize;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));
        String now = simpleDateFormat.format(DateUtil.getNowDate());
        String processId = nemoUserId + "-" + "ExportClaims" + "-" + now;

        // GO IN WITH GENERATION
        for (int page = 1; page <= pagesNumber; page++) {
            List<ClaimsNewEntity> claimsNewEntityList = claimsNewRepository.exportClaimsByPages(filter, page, pageSize);
            if(claimsNewEntityList != null) {
                List<ClaimsEntity> claimsEntityList = converterClaimsService.wrapFromClaimsNewEntityListToClaimsEntityList(claimsNewEntityList);
                int index = 0;
                // BUILDING JSON TEMPLATE MAP
                for (ClaimsEntity claimsEntity : claimsEntityList) {
                    // inizializza le strutture che dovranno essere inviate a DoGe
                    Map<String, Object> requestMap = sheetsBuilder.build();
                    LinkedList<Map<String, Object>> claimsList = sheetsBuilder.get(requestMap,"claims"); // sinistri
                    LinkedList<Map<String, Object>> ctpList = sheetsBuilder.get(requestMap,"ctp"); // ctp
                    LinkedList<Map<String, Object>> authorityList = sheetsBuilder.get(requestMap,"authority"); // authority
                    LinkedList<Map<String, Object>> refundList = sheetsBuilder.get(requestMap,"refund"); // rimborso
                    LinkedList<Map<String, Object>> woundedList = sheetsBuilder.get(requestMap,"wounded"); // feriti
                    LinkedList<Map<String, Object>> deponentList = sheetsBuilder.get(requestMap,"deponent"); // testimoni
                    LinkedList<Map<String, Object>> attachmentList = sheetsBuilder.get(requestMap,"attachment"); // allegato
                    LinkedList<Map<String, Object>> additionalCostsList = sheetsBuilder.get(requestMap,"additional_costs"); // spese accessorie

                    ClaimsExportBaseData claimsExportBaseData = claimsExportBaseDataBuilder.build(claimsEntity);

                    // SHEET SINISTRI
                    Map<String,Object> claimsMap = claimsExportMapInitializer.claims();
                    claimsEntity = authorityAttachmentService.addAuthorityAttachment(claimsEntity);
                    // aggiunge gli allegati provenienti dai crash report
                    List<Attachment> crashReportattachments = new LinkedList<>();
                    if( claimsEntity.getAntiTheftRequestEntities() != null && claimsEntity.getAntiTheftRequestEntities().size() != 0 ) {
                        for(AntiTheftRequestEntity current : claimsEntity.getAntiTheftRequestEntities()) {
                            if(current.getCrashReport() != null) {
                                crashReportattachments.add( current.getCrashReport() );
                            }
                        }
                        if(
                            claimsEntity.getForms() != null &&
                            claimsEntity.getForms().getAttachment() != null &&
                            !crashReportattachments.isEmpty()
                        ){
                            List<Attachment> claimsAttachments = claimsEntity.getForms().getAttachment();
                            claimsAttachments.addAll( crashReportattachments );
                            claimsEntity.getForms().setAttachment( claimsAttachments );
                        }
                    }

                    claimsMap = claimsExportMapFiller.claims(claimsMap, claimsEntity, claimsExportBaseData);
                    claimsList.add(claimsMap);

                    //SHEET CONTROPARTI
                    if(claimsEntity.getCounterparts() != null && !claimsEntity.getCounterparts().isEmpty()) {
                        for(CounterpartyEntity counterpartyEntity : claimsEntity.getCounterparts()){
                            Map<String,Object> ctpMap = claimsExportMapInitializer.ctp();
                            ctpMap = claimsExportMapFiller.ctp(ctpMap, counterpartyEntity, claimsExportBaseData);
                            ctpList.add(ctpMap);
                        }
                    }

                    //SHEET AUTHORITY
                    if(claimsEntity.getAuthorities() != null && !claimsEntity.getAuthorities().isEmpty()) {
                        for(Authority authority : claimsEntity.getAuthorities()) {
                            List<PODetails> poDetailsList = authority.getPoDetails();
                            Map<String,Object> authorityMap;
                            if(poDetailsList != null && !poDetailsList.isEmpty()){
                                for(PODetails poDetails : poDetailsList ){
                                    authorityMap = claimsExportMapInitializer.authority();
                                    authorityMap = claimsExportMapFiller.authority(authorityMap, claimsEntity, authority, poDetails, claimsExportBaseData);
                                    authorityList.add(authorityMap);
                                }
                            }
                            else {
                                authorityMap = claimsExportMapInitializer.authority();
                                authorityMap = claimsExportMapFiller.authority(authorityMap, claimsEntity, authority, null, claimsExportBaseData);
                                authorityList.add(authorityMap);
                            }
                        }
                    }

                    //SHEET REFUND
                    if(claimsEntity.getRefund() != null) {
                        Map<String,Object> refundMap;
                        List<Split> splits = claimsEntity.getRefund().getSplitList();
                        if(splits != null && !splits.isEmpty()) {
                            for(Split split : splits) {
                                refundMap = claimsExportMapInitializer.refund();
                                refundMap = claimsExportMapFiller.refund(refundMap, claimsEntity, split, claimsExportBaseData);
                                refundList.add(refundMap);
                            }
                        }
                        else {
                            refundMap = claimsExportMapInitializer.refund();
                            refundMap = claimsExportMapFiller.refund(refundMap, claimsEntity, null, claimsExportBaseData);
                            refundList.add(refundMap);
                        }
                    }

                    //SHEET WOUNDED
                    if(claimsEntity.getWoundedList() != null && !claimsEntity.getWoundedList().isEmpty()) {
                        for(Wounded wounded : claimsEntity.getWoundedList()){
                            Map<String,Object> woundedMap = claimsExportMapInitializer.wounded();
                            woundedMap = claimsExportMapFiller.wounded(woundedMap, wounded, claimsExportBaseData);
                            woundedList.add(woundedMap);
                        }
                    }

                    //SHEET DEPONENT
                    if(claimsEntity.getDeponentList() != null && !claimsEntity.getDeponentList().isEmpty()) {
                        for(Deponent deponent : claimsEntity.getDeponentList()) {
                            Map<String,Object> deponentMap = claimsExportMapInitializer.deponent();
                            deponentMap = claimsExportMapFiller.deponent(deponentMap, deponent, claimsExportBaseData);
                            deponentList.add(deponentMap);
                        }
                    }

                    //SHEET ATTACHMENT
                    if(claimsEntity.getForms() != null && claimsEntity.getForms().getAttachment() != null && !claimsEntity.getForms().getAttachment().isEmpty()) {
                        for(Attachment attachment : claimsEntity.getForms().getAttachment()) {
                            Map<String,Object> attachmentMap = claimsExportMapInitializer.attachment();
                            attachmentMap = claimsExportMapFiller.attachment(attachmentMap, attachment, claimsExportBaseData);
                            attachmentList.add(attachmentMap);
                        }
                    }

                    //SHEET ADDITIONAL COST
                    if (claimsEntity.getDamaged().getAdditionalCosts() != null && !claimsEntity.getDamaged().getAdditionalCosts().isEmpty() ) {
                        for (AdditionalCosts additionalCost: claimsEntity.getDamaged().getAdditionalCosts()){
                            Map<String,Object> additionalCostsMap = claimsExportMapInitializer.additionalCosts();
                            additionalCostsMap = claimsExportMapFiller.additionalCosts(additionalCostsMap, additionalCost, claimsExportBaseData);
                            additionalCostsList.add(additionalCostsMap);
                        }
                    }

                    uploadOnKafka(requestMap, (index + 1) + ((page - 1) * pageSize), exportCount, kafkaTopicExportClaims, userEmail, null, "DettaglioSinistri", processId);
                    index++;
                }
            }
        }
    }

}
