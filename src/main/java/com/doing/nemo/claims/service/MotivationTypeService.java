package com.doing.nemo.claims.service;

import com.doing.nemo.claims.entity.settings.MotivationTypeEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public interface MotivationTypeService {
    MotivationTypeEntity insertMotivationType(MotivationTypeEntity motivationTypeEntity);

    MotivationTypeEntity updateMotivationType(MotivationTypeEntity motivationTypeEntity);

    MotivationTypeEntity selectMotivationType(UUID uuid);

    List<MotivationTypeEntity> selectAllMotivationType();

    MotivationTypeEntity deleteMotivationType(UUID uuid);

    MotivationTypeEntity updateStatusMotivationType(UUID uuid);

    List<MotivationTypeEntity> findMotivationTypesFiltered (Integer page, Integer pageSize, String orderBy, Boolean asc, Long motivationTypeId, String description, Integer orderId, Boolean isActive);

    Long countMotivationTypeFiltered(Long motivationTypeId, String description, Integer orderId, Boolean isActive);
}
