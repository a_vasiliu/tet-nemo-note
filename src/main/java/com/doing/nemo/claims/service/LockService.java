package com.doing.nemo.claims.service;

import com.doing.nemo.claims.entity.settings.LockEntity;
import org.springframework.stereotype.Service;

@Service
public interface LockService {
    LockEntity lockPractice(String userId, String claimId);

    void unlockPractice(String userId, String claimId);

    LockEntity alivePractice(String userId, String claimId);

    void checkLock(String userId, String claimId);

    void lockJob();
}
