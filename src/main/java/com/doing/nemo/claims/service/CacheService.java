package com.doing.nemo.claims.service;

import com.doing.nemo.claims.controller.payload.response.ClaimsResponseStatsEvidenceV2;
import com.doing.nemo.claims.entity.Stats;
import com.doing.nemo.claims.entity.enumerated.DTO.ClaimsTheftStats;
import com.doing.nemo.claims.entity.enumerated.DTO.CounterpartyStats;

import java.util.Map;

public interface CacheService {
    Stats getCachedClaimsStats(String cacheName, String key);
    ClaimsResponseStatsEvidenceV2 getCachedClaimsStatsV2(String cacheName, String key);
    ClaimsTheftStats getCachedClaimsStatsTheft(String cacheName, String key);
    CounterpartyStats getCachedCounterpartyStats(String cacheName, String key);
    void cacheClaimsStats( Map<String, Object> payload, String cacheName, String key);
    void deleteClaimsStats(String cacheName, String key);
    void deleteClaimsStats(String fullKey);
}
