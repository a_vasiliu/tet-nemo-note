package com.doing.nemo.claims.service;

import com.doing.nemo.claims.controller.payload.request.UploadFileRequestV1;
import com.doing.nemo.claims.controller.payload.response.IdentifierResponseV1;
import com.doing.nemo.claims.controller.payload.response.UploadFileResponseV1;
import com.doing.nemo.claims.entity.enumerated.ClaimsRepairEnum;
import com.doing.nemo.claims.entity.settings.GenericAttachmentEntity;

import java.util.List;
import java.util.UUID;

public interface GenericAttachmentService {

    IdentifierResponseV1 uploadGenericAttachment(UploadFileRequestV1 uploadFileRequest, Boolean isActive, ClaimsRepairEnum typeComplaint);
    UploadFileResponseV1 uploadGenericAttachmentUpdate(UploadFileRequestV1 uploadFileRequest, UUID idGenericAttachment, Boolean isActive);
    void deleteGenericAttachment( GenericAttachmentEntity genericAttachmentEntity);
    GenericAttachmentEntity updateStatusGenericAttachment(UUID uuid);
    List<GenericAttachmentEntity> getAllGenericAttachment(ClaimsRepairEnum claimsRepairEnum);


}
