package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.adapter.CounterpartyAdapter;
import com.doing.nemo.claims.adapter.RefundAdapter;
import com.doing.nemo.claims.controller.payload.request.CounterpartyMigrationRequestV1;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.ClaimsNewEntity;
import com.doing.nemo.claims.entity.CounterpartyEntity;
import com.doing.nemo.claims.entity.CounterpartyNewEntity;
import com.doing.nemo.claims.entity.claims.ClaimsRefundEntity;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.doing.nemo.claims.entity.enumerated.DamagedEnum.ImpactPointEnum.ImpactPointDetectionImpactPointEnum;
import com.doing.nemo.claims.entity.jsonb.*;
import com.doing.nemo.claims.entity.jsonb.cai.Cai;
import com.doing.nemo.claims.entity.jsonb.cai.CaiDetails;
import com.doing.nemo.claims.entity.jsonb.complaint.Complaint;
import com.doing.nemo.claims.entity.jsonb.complaint.jsonbComplaint.Entrusted;
import com.doing.nemo.claims.entity.jsonb.complaint.jsonbComplaint.FromCompany;
import com.doing.nemo.claims.entity.jsonb.complaint.jsonbComplaint.jsonbDataAccidents.DataAccident;
import com.doing.nemo.claims.entity.jsonb.counterparty.InsuranceCompanyCounterparty;
import com.doing.nemo.claims.entity.jsonb.damaged.Damaged;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.*;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbAntiTheftService.AntiTheftService;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbAntiTheftService.Registry;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbDriver.Driver;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbDriver.DrivingLicense;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbInsuranceCompany.Kasko;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbInsuranceCompany.Pai;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbInsuranceCompany.Theft;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbInsuranceCompany.Tpl;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbVehicle.Vehicle;
import com.doing.nemo.claims.entity.jsonb.refund.Refund;
import com.doing.nemo.claims.entity.settings.InsuranceCompanyEntity;
import com.doing.nemo.claims.entity.settings.ManagerEntity;
import com.doing.nemo.claims.repository.*;
import com.doing.nemo.claims.service.CacheService;
import com.doing.nemo.claims.service.ClaimsMigrationService;
import com.doing.nemo.claims.service.ConverterClaimsService;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class ClaimsMigrationServiceImpl implements ClaimsMigrationService {

    private static Logger LOGGER = LoggerFactory.getLogger(ClaimsMigrationServiceImpl.class);

    @Autowired
    private ClaimsNewRepository claimsNewRepository;
    @Autowired
    private ManagerRepository managerRepository;
    @Autowired
    private CounterpartyNewRepository counterpartyNewRepository;
    @Autowired
    private ConverterClaimsService converterClaimsService;

    @Autowired
    private CacheService cacheService;

    private static final String CACHENAME = "claims.";
    private static final String STATSkEY = "claimsStats::";
    private static final String STATSEVIDENCE = "evidenceStats::";
    private static final String STATSTHEFT = "theftStats::";
    private static final String STATSSTATUS = "statusStats::";
    private static final String COUNTERPARTYKEY = "counterpartyStats::";
    private static final String STATSkEY2 = "claimsV2Stats::";
    private static final String STATSEVIDENCECONTINUATION = "evidenceContinuationStats::";

    //REFACTOR
    @Override
    public void importClaimsFromMigration(List<ClaimsEntity> claimsEntityList) {

        if (claimsEntityList != null) {
            for (ClaimsEntity currentClaim : claimsEntityList) {
                ClaimsNewEntity claimsNewEntity = buildInsertOrUpdateClaimsNewEntity(currentClaim);
//                ClaimsNewEntity claimsNewEntity = converterClaimsService.convertOldEntityToNewClaimsEntity(currentClaim);
                claimsNewRepository.save(claimsNewEntity);

            }
        }
        cacheService.deleteClaimsStats(CACHENAME, STATSkEY);
        cacheService.deleteClaimsStats(CACHENAME, STATSEVIDENCE);
        cacheService.deleteClaimsStats(CACHENAME, STATSSTATUS);
        cacheService.deleteClaimsStats(CACHENAME, STATSTHEFT);

        cacheService.deleteClaimsStats(CACHENAME, STATSEVIDENCECONTINUATION);
        cacheService.deleteClaimsStats(CACHENAME, STATSkEY2);
    }

    private ClaimsNewEntity buildInsertOrUpdateClaimsNewEntity(ClaimsEntity claimsEntity) {
        ClaimsNewEntity claimToUpdate = claimsNewRepository.getClaimsByIdPratica(claimsEntity.getPracticeId());
        if (claimToUpdate == null) {
            return converterClaimsService.convertOldEntityToNewClaimsEntity(claimsEntity);
        } else {
            ClaimsEntity oldClaim = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimToUpdate);
            updateObjectFieldsWithMigrationObjectInput(oldClaim, claimsEntity);
            return converterClaimsService.convertOldEntityToNewClaimsEntity(oldClaim);
        }
    }


    //REFACTOR
    @Override
    public void   importCounterpartyFromMigration(List<CounterpartyMigrationRequestV1> counterpartyMigrationRequestV1List) {

        if (counterpartyMigrationRequestV1List != null) {
            for (CounterpartyMigrationRequestV1 current : counterpartyMigrationRequestV1List) {
                ClaimsNewEntity claimsNewEntity = claimsNewRepository.getClaimsByIdPratica(current.getPracticeIdClaims());
                if (claimsNewEntity == null){
                    return;
                    //Gestisci eccezione
                } else{
                    CounterpartyEntity counterpartyEntity = CounterpartyAdapter.adptCounterpartyRequestMigrationToCounterparty(current);

                    //conversione nella nuova entità
                    //CounterpartyNewEntity counterpartyNewEntity = CounterpartyAdapter.adptCounterpartyOldToCounterpartyNew(counterpartyEntity);
                    CounterpartyNewEntity counterpartyNewEntity = buildInsertOrUpdateCounterPartyNewEntity(counterpartyEntity);
                    Optional<ClaimsNewEntity> claimsOpt = claimsNewRepository.findById(current.getClaims());


                    Optional<ManagerEntity> managerOpt = null;
                    if (current.getManager() != null) {
                        managerOpt = managerRepository.findById(UUID.fromString(current.getManager()));
                        managerOpt.ifPresent(counterpartyEntity::setManager);
                    }

                    claimsOpt.ifPresent(counterpartyNewEntity::setClaims);
                    counterpartyNewEntity.setClaims(claimsNewEntity);

                    counterpartyNewRepository.save(counterpartyNewEntity);
                }


            }
        }
    }

    private CounterpartyNewEntity buildInsertOrUpdateCounterPartyNewEntity(CounterpartyEntity counterpartyEntity){
        CounterpartyNewEntity counterpartyToUpdate = counterpartyNewRepository.findByPracticeIdCounterparty(counterpartyEntity.getPracticeIdCounterparty());
        if(counterpartyToUpdate==null){
            return CounterpartyAdapter.adptCounterpartyOldToCounterpartyNew(counterpartyEntity);
        }else{
            CounterpartyEntity oldCounterpartyEntity = CounterpartyAdapter.adptCounterpartyNewToCounterpartyOld(counterpartyToUpdate);
            updateObjectFieldsWithMigrationObjectInput(oldCounterpartyEntity, counterpartyEntity);
            return CounterpartyAdapter.adptCounterpartyOldToCounterpartyNew(oldCounterpartyEntity);
        }
    }

    private void updateObjectFieldsWithMigrationObjectInput(CounterpartyEntity counterpartyEntity, CounterpartyEntity etlCounterpartyEntity) {
        //counterpartyEntity.setCounterpartyId(etlCounterpartyEntity.getCounterpartyId());
        counterpartyEntity.setPracticeIdCounterparty(etlCounterpartyEntity.getPracticeIdCounterparty());
        counterpartyEntity.setType(etlCounterpartyEntity.getType());
        counterpartyEntity.setEligibility(etlCounterpartyEntity.getEligibility());
        counterpartyEntity.setResponsible(etlCounterpartyEntity.getResponsible());
        counterpartyEntity.setPolicyNumber(etlCounterpartyEntity.getPolicyNumber());
        counterpartyEntity.setDescription(etlCounterpartyEntity.getDescription());

        if (counterpartyEntity.getDriver() != null) {
            updateObjectFieldsWithMigrationObjectInput(counterpartyEntity.getDriver(), etlCounterpartyEntity.getDriver());
        } else {
            counterpartyEntity.setDriver(etlCounterpartyEntity.getDriver());
        }

        if (counterpartyEntity.getVehicle() != null) {
            updateObjectFieldsWithMigrationObjectInput(counterpartyEntity.getVehicle(), etlCounterpartyEntity.getVehicle());
        } else {
            counterpartyEntity.setVehicle(etlCounterpartyEntity.getVehicle());
        }

        if (counterpartyEntity.getInsuranceCompany() != null) {
            updateObjectFieldsWithMigrationObjectInput(counterpartyEntity.getInsuranceCompany(), etlCounterpartyEntity.getInsuranceCompany());
        } else {
            counterpartyEntity.setInsuranceCompany(etlCounterpartyEntity.getInsuranceCompany());
        }

        if (counterpartyEntity.getInsured() != null) {
            updateObjectFieldsWithMigrationObjectInput(counterpartyEntity.getInsured(), etlCounterpartyEntity.getInsured());
        } else {
            counterpartyEntity.setInsured(etlCounterpartyEntity.getInsured());
        }


        if (counterpartyEntity.getImpactPoint() != null) {
            updateObjectFieldsWithMigrationObjectInput(counterpartyEntity.getImpactPoint(), etlCounterpartyEntity.getImpactPoint());
        } else {
            counterpartyEntity.setImpactPoint(etlCounterpartyEntity.getImpactPoint());
        }

    }

    /**
     * Esegue l'update dei campi settati in ETL per la classe {@link Insured}
     * <br/>
     * Campi non settati in ETL e che non verranno modificati dalla funzione<br/>
     *
     * drivingLicense<br/>
     * customerId<br/>
     * phone<br/>
     *
     * @param insured
     * @param etlInsured
     */
    private void updateObjectFieldsWithMigrationObjectInput(Insured insured, Insured etlInsured) {
        insured.setFirstname(etlInsured.getFirstname());
        insured.setLastname(etlInsured.getLastname());
        insured.setFiscalCode(etlInsured.getFiscalCode());
        insured.setEmail(etlInsured.getEmail());
        insured.setAddress(etlInsured.getAddress());
        insured.setDrivingLicense(etlInsured.getDrivingLicense());
        insured.setCustomerId(etlInsured.getCustomerId());
        insured.setPhone(etlInsured.getPhone());
    }

    /**
     * Esegue l'update dei campi settati in ETL per la classe {@link InsuranceCompanyCounterparty}
     * <br/>
     * Campi non settati in ETL e che non verranno modificati dalla funzione<br/>
     *
     *  name<br/>
     *
     * @param insuranceCompanyCounterparty
     * @param etlInsuranceCompanyCounterparty
     */
    private void updateObjectFieldsWithMigrationObjectInput(InsuranceCompanyCounterparty insuranceCompanyCounterparty, InsuranceCompanyCounterparty etlInsuranceCompanyCounterparty) {
        if(insuranceCompanyCounterparty.getEntity() != null){
            updateObjectFieldsWithMigrationObjectInput(insuranceCompanyCounterparty.getEntity(), etlInsuranceCompanyCounterparty.getEntity());
        }else{
            insuranceCompanyCounterparty.setEntity(etlInsuranceCompanyCounterparty.getEntity());
        }
    }

    /**
     * Esegue l'update dei campi settati in ETL per la classe {@link InsuranceCompanyEntity}
     * <br/>
     * Campi non settati in ETL e che non verranno modificati dalla funzione<br/>
     *
     * code<br/>
     *
     * @param insuranceCompanyEntity
     * @param etlInsuranceCompanyEntity
     */
    private void updateObjectFieldsWithMigrationObjectInput(InsuranceCompanyEntity insuranceCompanyEntity, InsuranceCompanyEntity etlInsuranceCompanyEntity) {
        insuranceCompanyEntity.setName(etlInsuranceCompanyEntity.getName());
        insuranceCompanyEntity.setAddress(etlInsuranceCompanyEntity.getAddress());
        insuranceCompanyEntity.setZipCode(etlInsuranceCompanyEntity.getZipCode());
        insuranceCompanyEntity.setCity(etlInsuranceCompanyEntity.getCity());
        insuranceCompanyEntity.setProvince(etlInsuranceCompanyEntity.getProvince());
        insuranceCompanyEntity.setState(etlInsuranceCompanyEntity.getState());
        insuranceCompanyEntity.setTelephone(etlInsuranceCompanyEntity.getTelephone());
        insuranceCompanyEntity.setFax(etlInsuranceCompanyEntity.getFax());
        insuranceCompanyEntity.setEmail(etlInsuranceCompanyEntity.getEmail());
        insuranceCompanyEntity.setWebSite(etlInsuranceCompanyEntity.getWebSite());
        insuranceCompanyEntity.setContact(etlInsuranceCompanyEntity.getContact());
        insuranceCompanyEntity.setAniaCode(etlInsuranceCompanyEntity.getAniaCode());
        insuranceCompanyEntity.setJoinCard(etlInsuranceCompanyEntity.getJoinCard());
        insuranceCompanyEntity.setContactPai(etlInsuranceCompanyEntity.getContactPai());
        insuranceCompanyEntity.setEmailPai(etlInsuranceCompanyEntity.getEmailPai());
        insuranceCompanyEntity.setActive(etlInsuranceCompanyEntity.getActive());
    }

    /**
     * Esegue l'update dei campi settati in ETL per la classe {@link ClaimsEntity}
     * <br/>
     * Campi non settati in ETL e che non verranno modificati dalla funzione<br/>
     * paiComunication<br/>
     * forced<br/>
     * inEvidence<br/>
     * withContinuation<br/>
     * poVariation<br/>
     * legalComunication<br/>
     * isMigrated<br/>
     * converted<br/>
     * practiceManager<br/>
     * userId<br/>
     * isCompleteDocumentation<br/>
     * idSaleforce<br/>
     * isReadAcclaims<br/>
     * totalPenalty<br/>
     * forms<br/>
     * counterparts<br/>
     * notes<br/>
     * foundModel<br/>
     * exemption<br/>
     * metadata<br/>
     * antiTheftRequestEntities<br/>
     * @param claim
     * @param etlClaim
     */
    private void updateObjectFieldsWithMigrationObjectInput(ClaimsEntity claim, ClaimsEntity etlClaim) {
        //claim.setId(etlClaim.getId());
        claim.setPracticeId(etlClaim.getPracticeId());
        claim.setMotivation(etlClaim.getMotivation());
        claim.setRead(etlClaim.getRead());
        claim.setWithCounterparty(etlClaim.getWithCounterparty());
        claim.setAuthorityLinkable(etlClaim.getAuthorityLinkable());
        claim.setCreatedAt(etlClaim.getCreatedAt());
        claim.setUpdateAt(etlClaim.getUpdateAt());

        if (etlClaim.getStatus()!=null&&etlClaim.getStatus().getValue().equalsIgnoreCase("send_to_client")){
            claim.setStatus(ClaimsStatusEnum.TO_SEND_TO_CUSTOMER);
        }else{
            claim.setStatus(etlClaim.getStatus());
        }



        claim.setType(etlClaim.getType());

        if (claim.getComplaint() != null) {
            updateObjectFieldsWithMigrationObjectInput(claim.getComplaint(), etlClaim.getComplaint());
        } else {
            claim.setComplaint(etlClaim.getComplaint());
        }

        if (claim.getDamaged() != null) {
            updateObjectFieldsWithMigrationObjectInput(claim.getDamaged(),etlClaim.getDamaged());
        } else {
            claim.setDamaged(etlClaim.getDamaged());
        }

        if (claim.getCaiDetails() != null) {
            updateObjectFieldsWithMigrationObjectInput(claim.getCaiDetails(),etlClaim.getCaiDetails());
        } else {
            claim.setCaiDetails(etlClaim.getCaiDetails());
        }

        if (claim.getRefund() != null) {
             updateObjectFieldsWithMigrationObjectInput(claim.getRefund(),RefundAdapter.adptFromRefundJsonbToClaimsRefund(etlClaim.getRefund()));

        } else {
           claim.setRefund(RefundAdapter.adptFromRefundJsonbToClaimsRefund(etlClaim.getRefund()));
        }

        if (claim.getTheft() != null) {
            updateObjectFieldsWithMigrationObjectInput(claim.getTheft(), etlClaim.getTheft());
        } else {
            claim.setTheft(etlClaim.getTheft());
        }

        // Deponent non ha una chiave univoca con cui cercare il deponent nella lista già presente a db
        // e aggiornare i valori, per questo motivo lo ricopriamo interamente
        claim.setDeponentList(etlClaim.getDeponentList());

        // Wounded non ha una chiave che consenta di cercare quale wounded è stato modificato o aggiunto da ETL
        // Procediamo ricopiando interamente la lista da ETL.
        claim.setWoundedList(etlClaim.getWoundedList());

        /****************** old ***************************************************************
        // Historical non ha una chiave che consenta di cercare quale Historical è stato modificato o aggiunto da ETL
        // Procediamo ricopiando interamente la lista da ETL.
                claim.setHistorical(etlClaim.getHistorical());
         *****************/

        /************** init   NE-1307   il log va in append ********************/
        if (!claim.getHistorical().isEmpty()) {
            if (etlClaim.getHistorical()!=null&&etlClaim.getHistorical().size()> 0){
                List<Historical> listHistorical = new ArrayList<>();
                listHistorical = claim.getHistorical();
                for (Historical hItem:etlClaim.getHistorical()) {
                    Historical newItem;
                    newItem = hItem;
                    listHistorical.add(newItem);
                }
                claim.setHistorical(listHistorical);
            }
        }else{
            claim.setHistorical(etlClaim.getHistorical());
        }
        /************** end   NE-1307   il log va in append ********************/

        if(claim.getAuthorities()!=null && !claim.getAuthorities().isEmpty()){
            if(etlClaim.getAuthorities()!=null && !etlClaim.getAuthorities().isEmpty()){
                if(claim.getAuthorities()==null){
                    claim.setAuthorities(new LinkedList<>());
                }

                for(Authority newAuthority : etlClaim.getAuthorities()){
                    boolean found = false;
                    for(Authority oldAuthority : claim.getAuthorities()){
                        if(Objects.equals(oldAuthority.getWorkingNumber(), newAuthority.getWorkingNumber())){
                            found=true;
                            updateObjectFieldsWithMigrationObjectInput(oldAuthority, newAuthority);
                        }
                    }
                    if(!found){
                        claim.getAuthorities().add(newAuthority);
                    }
                }
            }
        }else{
            claim.setAuthorities(etlClaim.getAuthorities());
        }
        
    }

    /**
     * Esegue l'update dei campi settati in ETL per la classe {@link Authority}
     * <br/>
     * Campi non settati in ETL e che non verranno modificati dalla funzione<br/>
     *
     * isInvoiced
     * authorityDossierId;
     * authorityWorkingId;
     * acceptingDate;
     * rejection;
     * noteRejection;
     * eventDate;
     * eventType;
     * status;
     * oldest;
     * workingStatus;
     * nbv;
     * wreckValue;
     * claimsLinkedSize;
     * historical;
     * isMsaDownloaded;
     * 
     * @param authority
     * @param etlAuthority
     */
    private void updateObjectFieldsWithMigrationObjectInput(Authority authority, Authority etlAuthority) {
        if(etlAuthority==null){
            return;
        }

        authority.setWorkingNumber(etlAuthority.getWorkingNumber());
        authority.setAuthorityDossierNumber(etlAuthority.getAuthorityDossierNumber());
        authority.setInvoiced(etlAuthority.getInvoiced());
        authority.setTotal(etlAuthority.getTotal());
        authority.setAuthorizationDate(etlAuthority.getAuthorizationDate());
        authority.setRejectionDate(etlAuthority.getRejectionDate());
        authority.setRejection(etlAuthority.getRejection());
        authority.setWorkingCreatedAt(etlAuthority.getWorkingCreatedAt());
        authority.setNumberFranchise(etlAuthority.getNumberFranchise());
        authority.setWreck(etlAuthority.getWreck());
        authority.setWreckCasual(etlAuthority.getWreckCasual());
        authority.setType(etlAuthority.getType());
        authority.setNotDuplicate(etlAuthority.getNotDuplicate());
        authority.setStatus(etlAuthority.getStatus());
        if(authority.getCommodityDetails()!=null){
            updateObjectFieldsWithMigrationObjectInput(authority.getCommodityDetails(),etlAuthority.getCommodityDetails());
        }else{
            authority.setCommodityDetails(etlAuthority.getCommodityDetails());
        }

        if(etlAuthority.getPoDetails() != null && !etlAuthority.getPoDetails().isEmpty()) {
            for (PODetails newPoDetails : etlAuthority.getPoDetails()) {
                boolean found = false;
                if (authority.getPoDetails()!=null) {
                    for (PODetails oldPoDetails : authority.getPoDetails()) {
                        if (Objects.equals(oldPoDetails.getPoNumber(), newPoDetails.getPoNumber())) {
                            found = true;
                            updateObjectFieldsWithMigrationObjectInput(oldPoDetails, newPoDetails);
                        }
                    }
                    if (!found) {
                        authority.getPoDetails().add(newPoDetails);
                    }
                }else{
                    List<PODetails> newlistPODetail = new ArrayList<>();
                    newlistPODetail.add(newPoDetails);
                    authority.setPoDetails(newlistPODetail);
                }

            }
        }


        if(authority.getUserDetails()!=null){
            updateObjectFieldsWithMigrationObjectInput(authority.getUserDetails(),etlAuthority.getUserDetails());
        }else{
            authority.setUserDetails(etlAuthority.getUserDetails());
        }
    }

    /**
     * Esegue l'update dei campi settati in ETL per la classe {@link PODetails}
     * <br/>
     * Campi non settati in ETL e che non verranno modificati dalla funzione<br/>
     *
     * poTarget<br/>
     * idFilemanager<br/>
     *
     * @param poDetails
     * @param etlPoDetails
     */
    private void updateObjectFieldsWithMigrationObjectInput(PODetails poDetails, PODetails etlPoDetails) {
        if(etlPoDetails==null){
            return;
        }
        poDetails.setCreatedAt(etlPoDetails.getCreatedAt());
        poDetails.setPoNumber(etlPoDetails.getPoNumber());
        poDetails.setPoAmount(etlPoDetails.getPoAmount());
    }

    /**
     * Esegue l'update dei campi settati in ETL per la classe {@link CommodityDetails}
     * <br/>
     * Campi non settati in ETL e che non verranno modificati dalla funzione<br/>
     *
     * private String userId;
     * private String lastname;
     *
     * @param userDetails
     * @param etlUserDetails
     */
    private void updateObjectFieldsWithMigrationObjectInput(UserDetails userDetails, UserDetails etlUserDetails) {
        if (etlUserDetails==null){
            return;
        }

        userDetails.setFirstname(etlUserDetails.getFirstname());
    }

    /**
     * Esegue l'update dei campi settati in ETL per la classe {@link CommodityDetails}
     * <br/>
     * Campi non settati in ETL e che non verranno modificati dalla funzione<br/>
     *
     * commodityId<br/>
     * addressNumber<br/>
     *
     * @param commodityDetails
     * @param etlCommodityDetails
     */
    private void updateObjectFieldsWithMigrationObjectInput(CommodityDetails commodityDetails, CommodityDetails etlCommodityDetails) {
        if(etlCommodityDetails==null){
            return;
        }
        //commodityId;
        commodityDetails.setName(etlCommodityDetails.getName());
        commodityDetails.setAddress(etlCommodityDetails.getAddress());
        commodityDetails.setLocation(etlCommodityDetails.getLocation());
        commodityDetails.setZipCode(etlCommodityDetails.getZipCode());
        commodityDetails.setProvinceCode(etlCommodityDetails.getProvinceCode());
        commodityDetails.setPhone(etlCommodityDetails.getPhone());
        commodityDetails.setFax(etlCommodityDetails.getFax());
        commodityDetails.setEmail(etlCommodityDetails.getEmail());
    }

    /**
     * Esegue l'update dei campi settati in ETL per la classe {@link Deponent}
     * <br/>
     * Campi non settati in ETL e che non verranno modificati dalla funzione<br/>
     *
     * attachments
     *
     * @param deponent
     * @param etlDeponent
     */
    private void updateObjectFieldsWithMigrationObjectInput(Deponent deponent, Deponent etlDeponent) {
        if (etlDeponent==null){
            return;
        }
        deponent.setFirstname(etlDeponent.getFirstname());
        deponent.setLastname(etlDeponent.getLastname());
        deponent.setPhone(etlDeponent.getPhone());
        deponent.setEmail(etlDeponent.getPhone());
        deponent.setFiscalCode(etlDeponent.getFiscalCode());
        updateObjectFieldsWithMigrationObjectInput(deponent.getAddress(),etlDeponent.getAddress());
    }

    /**
     * Esegue l'update dei campi settati in ETL per la classe {@link com.doing.nemo.claims.entity.jsonb.Theft}
     * <br/>
     * Campi non settati in ETL e che non verranno modificati dalla funzione<br/>
     *
     * String certificateDuplicationSalesforceCase<br/>
     * Boolean complaintAuthorityPolice<br/>
     * Boolean complaintAuthorityCc<br/>
     * Boolean complaintAuthorityVvuu<br/>
     * String authorityData<br/>
     * String authorityTelephone<br/>
     * Boolean sequestered<br/>
     * String theftNotes<br/>
     * String findNotes<br/>
     * Long practiceId<br/>
     * Boolean reRegistration<br/>
     * Date reRegistrationRequest<br/>
     * Date reRegistrationAt<br/>
     * String reRegistrationSaleforcesCase<br/>
     * Boolean certificateDuplication<br/>
     * Date certificateDuplicationRequest<br/>
     * Date certificateDuplicationAt<br/>
     * Boolean stamp<br/>
     * Date stampRequest<br/>
     * Date stampAt<br/>
     * String stampSalesforceCase<br/>
     * Boolean demolition<br/>
     * Date demolitionRequest<br/>
     * Date demolitionAt<br/>
     * String demolitionSalesforceCase<br/>
     * Boolean isWithReceptions<br/>
     * Boolean isUnderSeizure<br/>
     * @param theft
     * @param etlTheft
     */
    private void updateObjectFieldsWithMigrationObjectInput(com.doing.nemo.claims.entity.jsonb.Theft theft, com.doing.nemo.claims.entity.jsonb.Theft etlTheft) {
        if(etlTheft==null){
            return;
        }
        theft.setFound(etlTheft.isFound());
        theft.setOperationsCenterNotified(etlTheft.getOperationsCenterNotified());
        theft.setTheftOccurredOnCenterAld(etlTheft.getTheftOccurredOnCenterAld());
        theft.setSupplierCode(etlTheft.getSupplierCode());
        theft.setFindDate(etlTheft.getFindDate());
        theft.setHour(etlTheft.getHour());
        theft.setFoundAbroad(etlTheft.getFoundAbroad());
        theft.setAddress(etlTheft.getAddress());
        theft.setFindAuthorityPolice(etlTheft.getFindAuthorityPolice());
        theft.setFindAuthorityCc(etlTheft.getFindAuthorityCc());
        theft.setFindAuthorityVvuu(etlTheft.getFindAuthorityVvuu());
        theft.setFindAuthorityData(etlTheft.getFindAuthorityData());
        theft.setFindAuthorityTelephone(etlTheft.getFindAuthorityTelephone());
        theft.setRobbery(etlTheft.getRobbery());
        theft.setKeyManagement(etlTheft.getKeyManagement());
        theft.setAccountManagement(etlTheft.getAccountManagement());
        theft.setAdministrativePosition(etlTheft.getAdministrativePosition());
        theft.setVehicleCo(etlTheft.getVehicleCo());
        theft.setFirstKeyReception(etlTheft.getFirstKeyReception());
        theft.setSecondKeyReception(etlTheft.getSecondKeyReception());
        theft.setOriginalComplaintReception(etlTheft.getOriginalComplaintReception());
        theft.setCopyComplaintReception(etlTheft.getCopyComplaintReception());
        theft.setOriginalReportReception(etlTheft.getOriginalReportReception());
        theft.setLossPossessionRequest(etlTheft.getLossPossessionRequest());
        theft.setCopyReportReception(etlTheft.getCopyReportReception());
        theft.setLossPossessionReception(etlTheft.getLossPossessionReception());
        theft.setReturnPossessionRequest(etlTheft.getReturnPossessionRequest());
        theft.setReturnPossessionReception(etlTheft.getReturnPossessionReception());
    }

    /**
     * Esegue l'update dei campi settati in ETL per la classe {@link Refund}
     * <br/>
     * Campi non settati in ETL e che non verranno modificati dalla funzione<br/>
     *
     * wreckValuePre<br/>
     * NBV<br/>
     * franchiseAmountFcm<br/>
     * deltaDebitDate<br/>
     * amountToBeDebited<br/>
     *getPo
     * @param refund
     * @param etlRefound
     */
    private void updateObjectFieldsWithMigrationObjectInput(Refund refund, Refund etlRefound) {
        if (etlRefound==null){
            return;
        }
        refund.setPoSum(etlRefound.getPoSum());
        refund.setWreck(etlRefound.getWreck());
        refund.setWreckValuePost(etlRefound.getWreckValuePost());
        refund.setTotalRefundExpected(etlRefound.getTotalRefundExpected());
        refund.setTotalLiquidationReceived(etlRefound.getTotalLiquidationReceived());
        refund.setDefinitionDate(etlRefound.getDefinitionDate());
        refund.setBluEurotax(etlRefound.getBluEurotax());

        refund.setSplitList(etlRefound.getSplitList());

    }

    /**
     * Esegue l'update dei campi settati in ETL per la classe {@link Cai}
     * <br/>
     *
     * Campi non settati in ETL e che non verranno modificati dalla funzione<br/>
     * note<br/>
     *
     * @param caiDetails
     * @param etlCaiDetails
     */
    private void updateObjectFieldsWithMigrationObjectInput(Cai caiDetails, Cai etlCaiDetails) {
        if(etlCaiDetails==null){
            return;
        }
        caiDetails.setDriverSide(etlCaiDetails.getDriverSide());
        if(caiDetails.getVehicleA()!=null){
            updateObjectFieldsWithMigrationObjectInput(caiDetails.getVehicleA(), etlCaiDetails.getVehicleA());
        }else{
            caiDetails.setVehicleA(etlCaiDetails.getVehicleA());
        }

        if(caiDetails.getVehicleB()!=null){
            updateObjectFieldsWithMigrationObjectInput(caiDetails.getVehicleB(), etlCaiDetails.getVehicleB());
        }else{
            caiDetails.setVehicleB(etlCaiDetails.getVehicleB());
        }
    }

    /**
     * Esegue l'update dei campi settati in ETL per la classe {@link CaiDetails}
     * @param caiDetails
     * @param etlCaiDetails
     */
    private void updateObjectFieldsWithMigrationObjectInput(CaiDetails caiDetails, CaiDetails etlCaiDetails) {
        if(etlCaiDetails==null){
            return;
        }
        caiDetails.setCondition1(etlCaiDetails.getCondition1());
        caiDetails.setCondition2(etlCaiDetails.getCondition2());
        caiDetails.setCondition3(etlCaiDetails.getCondition3());
        caiDetails.setCondition4(etlCaiDetails.getCondition4());
        caiDetails.setCondition5(etlCaiDetails.getCondition5());
        caiDetails.setCondition6(etlCaiDetails.getCondition6());
        caiDetails.setCondition7(etlCaiDetails.getCondition7());
        caiDetails.setCondition8(etlCaiDetails.getCondition8());
        caiDetails.setCondition9(etlCaiDetails.getCondition9());
        caiDetails.setCondition10(etlCaiDetails.getCondition10());
        caiDetails.setCondition11(etlCaiDetails.getCondition11());
        caiDetails.setCondition12(etlCaiDetails.getCondition12());
        caiDetails.setCondition13(etlCaiDetails.getCondition13());
        caiDetails.setCondition14(etlCaiDetails.getCondition14());
        caiDetails.setCondition15(etlCaiDetails.getCondition15());
        caiDetails.setCondition16(etlCaiDetails.getCondition16());
        caiDetails.setCondition17(etlCaiDetails.getCondition17());
    }

    /**
     * Esegue l'update dei campi settati in ETL per la classe {@link Complaint}
     * <br/>
     * Campi non settati in ETL e che non verranno modificati dalla funzione<br/>
     * locator;<br/>
     * activation;<br/>
     * quote;<br/>
     * repair;<br/>
     * @param complaint
     * @param etlComplaint
     */
    private void updateObjectFieldsWithMigrationObjectInput(Complaint complaint, Complaint etlComplaint) {
        if(etlComplaint==null){
            return;
        }
        complaint.setClientId(etlComplaint.getClientId());
        complaint.setPlate(etlComplaint.getPlate());
        complaint.setNotification(etlComplaint.getNotification());
        complaint.setProperty(etlComplaint.getProperty());
        complaint.setMod(etlComplaint.getMod());
        if(complaint.getDataAccident()!=null){
            updateObjectFieldsWithMigrationObjectInput(complaint.getDataAccident(), etlComplaint.getDataAccident());
        }else{
            complaint.setDataAccident(etlComplaint.getDataAccident());
        }

        if(complaint.getFromCompany()!=null){
            updateObjectFieldsWithMigrationObjectInput(complaint.getFromCompany(), etlComplaint.getFromCompany());
        }else {
            complaint.setFromCompany(etlComplaint.getFromCompany());
        }

        if(complaint.getEntrusted()!=null){
            updateObjectFieldsWithMigrationObjectInput(complaint.getEntrusted(),etlComplaint.getEntrusted());
        }else{
            complaint.setEntrusted(etlComplaint.getEntrusted());
        }
    }

    /**
     * Esegue l'update dei campi settati in ETL per la classe {@link DataAccident}
     * <br/>
     * Campi non settati in ETL e che non verranno modificati dalla funzione<br/>
     * oldMotorcyclePlates;<br/>
     * incompleteMotivation;<br/>
     * isRobbery;<br/>
     * centerNotified;<br/>
     * happenedOnCenter<br/>
     * providerCode;<br/>
     * theftDescription;<br/>
     * itemToReceive;<br/>
     *
     * @param dataAccident parametro di destinazione delle modifiche
     * @param etlDataAccident sorgente delle modifiche fornito da ETL
     *
     */
    private void updateObjectFieldsWithMigrationObjectInput(DataAccident dataAccident, DataAccident etlDataAccident) {
        if(etlDataAccident==null){
            return;
        }
        dataAccident.setTypeAccident(etlDataAccident.getTypeAccident());
        dataAccident.setDateAccident(etlDataAccident.getDateAccident());
        dataAccident.setHappenedAbroad(etlDataAccident.getHappenedAbroad());
        dataAccident.setDamageToObjects(etlDataAccident.getDamageToObjects());
        dataAccident.setDamageToVehicles(etlDataAccident.getDamageToVehicles());
        dataAccident.setInterventionAuthority(etlDataAccident.getInterventionAuthority());
        dataAccident.setPolice(etlDataAccident.getPolice());
        dataAccident.setCc(etlDataAccident.getCc());
        dataAccident.setVvuu(etlDataAccident.getVvuu());
        dataAccident.setAuthorityData(etlDataAccident.getAuthorityData());
        dataAccident.setWitnessDescription(etlDataAccident.getWitnessDescription());
        dataAccident.setRecoverability(etlDataAccident.getRecoverability());
        dataAccident.setRecoverabilityPercent(etlDataAccident.getRecoverabilityPercent());
        dataAccident.setResponsible(etlDataAccident.getResponsible());
        dataAccident.setWithCounterparty(etlDataAccident.getWithCounterparty());
        if(etlDataAccident.getAddress()!=null){
            updateObjectFieldsWithMigrationObjectInput(dataAccident.getAddress(), etlDataAccident.getAddress());
        }else{
            dataAccident.setAddress(etlDataAccident.getAddress());
        }
    }

    /**
     * Esegue l'update dei campi settati in ETL per la classe {@link Address}
     * <br/>
     * Campi non settati in ETL e che non verranno modificati dalla funzione<br/>
     *
     * streetNr<br/>
     * region<br/>
     *
     * @param address
     * @param etlAddress
     */
    private void updateObjectFieldsWithMigrationObjectInput(Address address, Address etlAddress){
        if(etlAddress==null){
            return;
        }
        address.setStreet(etlAddress.getStreet());
        address.setZip(etlAddress.getZip());
        address.setLocality(etlAddress.getLocality());
        address.setProvince(etlAddress.getProvince());
        address.setState(etlAddress.getState());
        address.setFormattedAddress(etlAddress.getFormattedAddress());
    }

    /**
     * Esegue l'update dei campi settati in ETL per la classe {@link FromCompany}
     * <br/>
     * Campi non settati in ETL e che non verranno modificati dalla funzione<br/>
     *
     * status<br/>
     * globalReserve<br/>
     * totalPaid<br/>
     *
     * @param fromCompany
     * @param etlFromCompany
     */
    private void updateObjectFieldsWithMigrationObjectInput(FromCompany fromCompany, FromCompany etlFromCompany){
        if(etlFromCompany==null){
            return;
        }
        fromCompany.setCompany(etlFromCompany.getCompany());
        fromCompany.setNumberSx(etlFromCompany.getNumberSx());
        fromCompany.setTypeSx(etlFromCompany.getTypeSx());
        fromCompany.setInspectorate(etlFromCompany.getInspectorate());
        fromCompany.setExpert(etlFromCompany.getExpert());
        fromCompany.setNote(etlFromCompany.getNote());
        fromCompany.setDwlMan(etlFromCompany.getDwlMan());
        fromCompany.setLastUpdate(etlFromCompany.getLastUpdate());
    }

    /**
     * Esegue l'update dei campi settati in ETL per la classe {@link Entrusted}
     * <br/>
     * Campi non settati in ETL e che non verranno modificati dalla funzione<br/>
     *
     * autoEntrust<br/>
     * description<br/>
     * perito<br/>
     * numberSxCounterparty<br/>
     * dwlMan<br/>
     * status<br/>
     *
     * @param entrusted
     * @param etlEntrusted
     */
    private void updateObjectFieldsWithMigrationObjectInput(Entrusted entrusted, Entrusted etlEntrusted){
        if(etlEntrusted==null){
            return;
        }
        entrusted.setIdEntrustedTo(etlEntrusted.getIdEntrustedTo());
        entrusted.setEntrustedTo(etlEntrusted.getEntrustedTo());
        entrusted.setEntrustedDay(etlEntrusted.getEntrustedDay());
        entrusted.setEntrustedType(etlEntrusted.getEntrustedType());
        entrusted.setEntrustedEmail(etlEntrusted.getEntrustedEmail());
    }

    /**
     * Esegue l'update dei campi settati in ETL per la classe {@link Damaged}
     * <br/>
     * Campi non settati in ETL e che non verranno modificati dalla funzione<br/>
     *
     * additionalCosts<br/>
     * fleetManagerList<br/>
     *
     * @param damaged
     * @param etlDamaged
     */
    private void updateObjectFieldsWithMigrationObjectInput(Damaged damaged, Damaged etlDamaged) {
        if(etlDamaged==null){
            return;
        }
        if(damaged.getContract()!=null){
            updateObjectFieldsWithMigrationObjectInput(damaged.getContract(), etlDamaged.getContract());
        }else{
            damaged.setContract(etlDamaged.getContract());
        }

        if(damaged.getCustomer()!=null){
            updateObjectFieldsWithMigrationObjectInput(damaged.getCustomer(),etlDamaged.getCustomer());
        }else{
            damaged.setCustomer(etlDamaged.getCustomer());
        }

        if(damaged.getInsuranceCompany()!=null){
            updateObjectFieldsWithMigrationObjectInput(damaged.getInsuranceCompany(),etlDamaged.getInsuranceCompany());
        }else{
            damaged.setInsuranceCompany(etlDamaged.getInsuranceCompany());
        }

        if(damaged.getDriver()!=null){
           updateObjectFieldsWithMigrationObjectInput(damaged.getDriver(),etlDamaged.getDriver());
        }else{
            damaged.setDriver(etlDamaged.getDriver());
        }
        if(damaged.getAntiTheftService()!=null){
            updateObjectFieldsWithMigrationObjectInput(damaged.getAntiTheftService(), etlDamaged.getAntiTheftService());
        }else{
            damaged.setAntiTheftService(etlDamaged.getAntiTheftService());
        }
        if(damaged.getImpactPoint()!=null){
            updateObjectFieldsWithMigrationObjectInput(damaged.getImpactPoint(), etlDamaged.getImpactPoint());
        }else{
            damaged.setImpactPoint(etlDamaged.getImpactPoint());
        }
        if(damaged.getVehicle()!=null){
            updateObjectFieldsWithMigrationObjectInput(damaged.getVehicle(),etlDamaged.getVehicle());
        }else{
            damaged.setVehicle(etlDamaged.getVehicle());
        }
    }

    /**
     * Esegue l'update dei campi settati in ETL per la classe {@link Contract}
     * <br/>
     * Campi non settati in ETL e che non verranno modificati dalla funzione<br/>
     * status<br/>
     * leasingCompany<br/>
     * contractVersionId<br/>
     * takeinDate<br/>
     * leasingCompanyId<br/>
     * succeedingContractId<br/>
     * fleetVehicleId<br/>
     *
     * @param contract
     * @param etlContract
     */
    private void updateObjectFieldsWithMigrationObjectInput(Contract contract, Contract etlContract) {
        if(etlContract==null){
            return;
        }
        contract.setContractId(etlContract.getContractId());
        contract.setMileage(etlContract.getMileage());
        contract.setDuration(etlContract.getDuration());
        contract.setContractType(etlContract.getContractType());
        contract.setStartDate(etlContract.getStartDate());
        contract.setEndDate(etlContract.getEndDate());
        contract.setLicensePlate(etlContract.getLicensePlate());
        contract.setTakeinDate(etlContract.getTakeinDate());
    }

    /**
     * Esegue l'update dei campi settati in ETL per la classe {@link Customer}
     * <br/>
     * Campi non settati in ETL e che non verranno modificati dalla funzione<br/>
     *
     * status<br/>
     * officialRegistration<br/>
     * pec<br/>
     *
     * @param customer
     * @param etlCustomer
     */
    private void updateObjectFieldsWithMigrationObjectInput(Customer customer, Customer etlCustomer) {
        if(etlCustomer==null){
            return;
        }
        customer.setCustomerId(etlCustomer.getCustomerId());
        customer.setTradingName(etlCustomer.getTradingName());
        customer.setLegalName(etlCustomer.getLegalName());
        customer.setVatNumber(etlCustomer.getVatNumber());
        customer.setEmail(etlCustomer.getEmail());
        customer.setPhonenr(etlCustomer.getPhonenr());
        if(customer.getMainAddress()!=null){
            updateObjectFieldsWithMigrationObjectInput(customer.getMainAddress(), etlCustomer.getMainAddress());
        }else{
            customer.setMainAddress(etlCustomer.getMainAddress());
        }
    }

    /**
     * Esegue l'update dei campi settati in ETL per la classe {@link InsuranceCompany}
     * <br/>
     * Campi non settati in ETL e che non verranno modificati dalla funzione<br/>
     * materialDamage<br/>
     * legalCost<br/>
     * isCard<br/>
     * id<br/>
     * @param insuranceCompany
     * @param etlInsuranceCompany
     */
    private void updateObjectFieldsWithMigrationObjectInput(InsuranceCompany insuranceCompany, InsuranceCompany etlInsuranceCompany){
        if(etlInsuranceCompany==null){
            return;
        }
        if(insuranceCompany.getTpl()!=null){
            updateObjectFieldsWithMigrationObjectInput(insuranceCompany.getTpl(),etlInsuranceCompany.getTpl());
        }else{
            insuranceCompany.setTpl(etlInsuranceCompany.getTpl());
        }
        if(insuranceCompany.getTheft()!=null){
            updateObjectFieldsWithMigrationObjectInput(insuranceCompany.getTheft(),etlInsuranceCompany.getTheft());
        }else{
            insuranceCompany.setTheft(etlInsuranceCompany.getTheft());
        }
        if(insuranceCompany.getPai()!=null){
            updateObjectFieldsWithMigrationObjectInput(insuranceCompany.getPai(), etlInsuranceCompany.getPai());
        }else{
            insuranceCompany.setPai(etlInsuranceCompany.getPai());
        }
        if(insuranceCompany.getKasko()!=null){
            updateObjectFieldsWithMigrationObjectInput(insuranceCompany.getKasko(), etlInsuranceCompany.getKasko());
        }else{
            insuranceCompany.setKasko(etlInsuranceCompany.getKasko());
        }
    }

    /**
     * Esegue l'update dei campi settati in ETL per la classe {@link Tpl}
     * <br/>
     * Campi non settati in ETL e che non verranno modificati dalla funzione<br/>
     *
     * maxCoverage<br/>
     * service<br/>
     * serviceId<br/>
     * companyId<br/>
     * tariffCode<br/>
     * companyDescription<br/>
     *
     * @param tpl
     * @param etlTpl
     */
    private void updateObjectFieldsWithMigrationObjectInput(Tpl tpl, Tpl etlTpl) {
        if(etlTpl==null){
            return;
        }
        tpl.setDeductible(etlTpl.getDeductible());
        tpl.setCompany(etlTpl.getCompany());
        tpl.setTariffId(etlTpl.getTariffId());
        tpl.setTariff(etlTpl.getTariff());
        tpl.setStartDate(etlTpl.getStartDate());
        tpl.setEndDate(etlTpl.getEndDate());
        tpl.setPolicyNumber(etlTpl.getPolicyNumber());
        tpl.setInsuranceCompanyId(etlTpl.getInsuranceCompanyId());
        tpl.setInsurancePolicyId(etlTpl.getInsurancePolicyId());
    }

    /**
     * Esegue l'update dei campi settati in ETL per la classe {@link Theft}
     * <br/>
     * Campi non settati in ETL e che non verranno modificati dalla funzione<br/>
     *
     * serviceId<br/>
     * deductibleId<br/>
     * service<br/>
     *
     * @param theft
     * @param etlTheft
     */
    private void updateObjectFieldsWithMigrationObjectInput(Theft theft, Theft etlTheft) {
        if(etlTheft==null){
            return;
        }
        theft.setDeductible(etlTheft.getDeductible());
    }

    /**
     * Esegue l'update dei campi settati in ETL per la classe {@link Pai}
     * <br/>
     * Campi non settati in ETL e che non verranno modificati dalla funzione<br/>
     *
     * serviceId<br/>
     * service<br/>
     * deductibleId<br/>
     * companyId<br/>
     * tariffCode<br/>
     * percentage<br/>
     * medicalCosts<br/>
     *
     * @param pai
     * @param etlPai
     */
    private void updateObjectFieldsWithMigrationObjectInput(Pai pai, Pai etlPai){
        if(etlPai==null){
            return;
        }
        pai.setMaxCoverage(etlPai.getMaxCoverage());
        pai.setDeductible(etlPai.getDeductible());
        pai.setCompany(etlPai.getCompany());
        pai.setTariffId(etlPai.getTariffId());
        pai.setTariff(etlPai.getTariff());
        pai.setStartDate(etlPai.getStartDate());
        pai.setEndDate(etlPai.getEndDate());
        pai.setPolicyNumber(etlPai.getPolicyNumber());
        pai.setInsuranceCompanyId(etlPai.getInsuranceCompanyId());
        pai.setInsurancePolicyId(etlPai.getInsurancePolicyId());
        pai.setTariffCode(etlPai.getTariffCode());
        pai.setPercentage(etlPai.getPercentage());
    }
    /**
     * Esegue l'update dei campi settati in ETL per la classe {@link Kasko}
     * <br/>
     * Campi non settati in ETL e che non verranno modificati dalla funzione<br/>
     *
     * deductibleId<br/>
     *
     * @param kasko
     * @param etlKasko
     */
    private void updateObjectFieldsWithMigrationObjectInput(Kasko kasko, Kasko etlKasko){
        kasko.setDeductibleValue(etlKasko.getDeductibleValue());
    }

    /**
     * Esegue l'update dei campi settati in ETL per la classe {@link Driver}
     * <br/>
     * Campi non settati in ETL e che non verranno modificati dalla funzione<br/>
     *
     * id<br/>
     * identification<br/>
     * officialRegistration<br/>
     * tradingName<br/>
     * pec<br/>
     * sex<br/>
     * customerId<br/>
     * driverInjury<br/>
     *
     * @param driver
     * @param etlDriver
     */
    private void updateObjectFieldsWithMigrationObjectInput(Driver driver, Driver etlDriver){
        if (etlDriver==null){
            return;
        }

        driver.setFirstname(etlDriver.getFirstname());
        driver.setLastname(etlDriver.getLastname());
        driver.setFiscalCode(etlDriver.getFiscalCode());
        driver.setMainAddress(etlDriver.getMainAddress());
        driver.setDateOfBirth(etlDriver.getDateOfBirth());
        driver.setPhone(etlDriver.getPhone());
        driver.setEmail(etlDriver.getEmail());

        if(driver.getDrivingLicense()!=null){
            updateObjectFieldsWithMigrationObjectInput(driver.getDrivingLicense(), etlDriver.getDrivingLicense());
        }else{
            driver.setDrivingLicense(etlDriver.getDrivingLicense());
        }
    }

    /**
     * Esegue l'update dei campi settati in ETL per la classe {@link DrivingLicense}
     * <br/>
     * Campi non settati in ETL e che non verranno modificati dalla funzione<br/>
     *
     * validFrom<br/>
     * issuingCountry<br/>
     *
     * @param drivingLicense
     * @param etlDrivingLicense
     */
    private void updateObjectFieldsWithMigrationObjectInput(DrivingLicense drivingLicense, DrivingLicense etlDrivingLicense) {
        if(etlDrivingLicense==null){
            return;
        }
        drivingLicense.setNumber(etlDrivingLicense.getNumber());
        drivingLicense.setCategory(etlDrivingLicense.getCategory());
        drivingLicense.setValidTo(etlDrivingLicense.getValidTo());
    }

    /**
     * Esegue l'update dei campi settati in ETL per la classe {@link AntiTheftService}
     * <br/>
     * Campi non settati in ETL e che non verranno modificati dalla funzione<br/>
     *
     *  active - sempre settato a true in ETL<br/>
     *  IsActive - sempre settato a true in ETL<br/>
     *  id
     *  antiTheftList
     *
     * @param antiTheftService
     * @param etlAntiTheftService
     */
    private void updateObjectFieldsWithMigrationObjectInput(AntiTheftService antiTheftService, AntiTheftService etlAntiTheftService) {
        if(etlAntiTheftService==null){
            return;
        }
        antiTheftService.setCodeAntiTheftService(etlAntiTheftService.getCodeAntiTheftService());
        antiTheftService.setName(etlAntiTheftService.getName());
        antiTheftService.setBusinessName(etlAntiTheftService.getBusinessName());
        antiTheftService.setSupplierCode(etlAntiTheftService.getSupplierCode());
        antiTheftService.setEmail(etlAntiTheftService.getEmail());
        antiTheftService.setPhoneNumber(etlAntiTheftService.getPhoneNumber());
        antiTheftService.setProviderType(etlAntiTheftService.getProviderType());
        antiTheftService.setTimeOutInSeconds(etlAntiTheftService.getTimeOutInSeconds());
        antiTheftService.setEndPointUrl(etlAntiTheftService.getEndPointUrl());
        antiTheftService.setUsername(etlAntiTheftService.getUsername());
        antiTheftService.setPassword(etlAntiTheftService.getPassword());
        antiTheftService.setCompanyCode(etlAntiTheftService.getCompanyCode());
        antiTheftService.setType(etlAntiTheftService.getType());

        if(antiTheftService.getRegistryList()!=null && !antiTheftService.getRegistryList().isEmpty()){
            if(etlAntiTheftService.getRegistryList()!=null && !etlAntiTheftService.getRegistryList().isEmpty()){
                if(antiTheftService.getRegistryList()==null){
                    antiTheftService.setRegistryList(new LinkedList<>());
                }

                for(Registry newRegistry : etlAntiTheftService.getRegistryList()){
                    boolean found = false;
                    for(Registry oldRegistry : antiTheftService.getRegistryList()){
                        if(Objects.equals(oldRegistry.getVoucherId(), newRegistry.getVoucherId())){
                            found=true;
                            updateObjectFieldsWithMigrationObjectInput(oldRegistry, newRegistry);
                        }
                    }
                    if(!found){
                        antiTheftService.getRegistryList().add(newRegistry);
                    }
                }
            }
        }else{
            antiTheftService.setRegistryList(etlAntiTheftService.getRegistryList());
        }

    }

    /**
     * Esegue l'update dei campi settati in ETL per la classe {@link Registry}
     * <br/>
     * Campi non settati in ETL e che non verranno modificati dalla funzione<br/>
     *
     * idTransaction<br/>
     * codImei<br/>
     * codProvider<br/>
     * provider<br/>
     * dateActivation<br/>
     * dateEnd<br/>
     * codSerialnumber<br/>
     * typeEntity<br/>
     *
     * @param registry
     * @param etlRegistry
     */
    private void updateObjectFieldsWithMigrationObjectInput(Registry registry, Registry etlRegistry) {
        if(etlRegistry==null){
            return;
        }
        registry.setVoucherId(etlRegistry.getVoucherId());
        registry.setCodPack(etlRegistry.getCodPack());
    }

    /**
     * Esegue l'update dei campi settati in ETL per la classe {@link ImpactPoint}
     *
     * @param impactPoint
     * @param etlImpactPoint
     */
    private void updateObjectFieldsWithMigrationObjectInput(ImpactPoint impactPoint, ImpactPoint etlImpactPoint) {
        if (etlImpactPoint == null) {
            return;
        }
        impactPoint.setDamageToVehicle(etlImpactPoint.getDamageToVehicle());
        impactPoint.setIncidentDescription(etlImpactPoint.getIncidentDescription());
        //List<ImpactPointDetectionImpactPointEnum> detectionsImpactPoint;
        if(impactPoint.getDetectionsImpactPoint()!=null && !impactPoint.getDetectionsImpactPoint().isEmpty()){
            if(etlImpactPoint.getDetectionsImpactPoint()!=null && !etlImpactPoint.getDetectionsImpactPoint().isEmpty()){
                if(impactPoint.getDetectionsImpactPoint()==null){
                    impactPoint.setDetectionsImpactPoint(new LinkedList<>());
                }
                for(ImpactPointDetectionImpactPointEnum newImpactPointDetectionImpactPointEnum : etlImpactPoint.getDetectionsImpactPoint()){
                    if(!impactPoint.getDetectionsImpactPoint().contains(newImpactPointDetectionImpactPointEnum)){
                        impactPoint.getDetectionsImpactPoint().add(newImpactPointDetectionImpactPointEnum);
                    }
                }
            }
        }else{
            impactPoint.setDetectionsImpactPoint(etlImpactPoint.getDetectionsImpactPoint());
        }

    }

    /**
     * Esegue l'update dei campi settati in ETL per la classe {@link Vehicle}
     * <br/>
     * Campi non settati in ETL e che non verranno modificati dalla funzione<br/>
     *
     * nature<br/>
     * fleetVehicleId<br/>
     * modelYear<br/>
     * cc3<br/>
     * dinHp<br/>
     * seats<br/>
     * netWeight<br/>
     * maxWeight<br/>
     * doors<br/>
     * bodyStyle<br/>
     * fuelType<br/>
     * co2emission<br/>
     * consumption<br/>
     * lastKnownMileage<br/>
     * externalColor<br/>
     * internalColor<br/>
     * netBookValue<br/>
     * wreckDate<br/>
     *
     * @param vehicle
     * @param etlVehicle
     */
    private void updateObjectFieldsWithMigrationObjectInput(Vehicle vehicle, Vehicle etlVehicle) {
        if(etlVehicle==null){
            return;
        }
        vehicle.setMake(etlVehicle.getMake());
        vehicle.setModel(etlVehicle.getModel());
        vehicle.setKw(etlVehicle.getKw());
        vehicle.setHp(etlVehicle.getHp());
        vehicle.setLicensePlate(etlVehicle.getLicensePlate());
        vehicle.setChassisNumber(etlVehicle.getChassisNumber());
        vehicle.setRegistrationDate(etlVehicle.getRegistrationDate());
        vehicle.setRegistrationCountry(etlVehicle.getRegistrationCountry());
        vehicle.setWreck(etlVehicle.getWreck());
        vehicle.setLicensePlateTrailer(etlVehicle.getLicensePlateTrailer());
        vehicle.setChassisNumberTrailer(etlVehicle.getChassisNumberTrailer());
        vehicle.setRegistrationCountryTrailer(etlVehicle.getRegistrationCountryTrailer());
        vehicle.setOwnership(etlVehicle.getOwnership());

    }
}