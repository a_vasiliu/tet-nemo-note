package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.controller.payload.response.PaginationStats;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.settings.ExportSupplierCodeEntity;
import com.doing.nemo.claims.repository.ExportSupplierCodeRepository;
import com.doing.nemo.claims.repository.ExportSupplierCodeRepositoryV1;
import com.doing.nemo.claims.service.ExportSupplierCodeService;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ExportSupplierCodeServiceImpl implements ExportSupplierCodeService {
    private static Logger LOGGER = LoggerFactory.getLogger(ExportSupplierCodeServiceImpl.class);
    @Autowired
    private ExportSupplierCodeRepository exportSupplierCodeRepository;
    @Autowired
    private ExportSupplierCodeRepositoryV1 exportSupplierCodeRepositoryV1;

    @Override
    public ExportSupplierCodeEntity saveExportSupplierCode(ExportSupplierCodeEntity exportSupplierCodeEntity) {
        if (exportSupplierCodeEntity != null) {
            List<ExportSupplierCodeEntity> exportSupplierCodeEntities = exportSupplierCodeRepository.checkDuplicateExportSupplierCode(exportSupplierCodeEntity.getVatNumber());
            if (exportSupplierCodeEntities == null || exportSupplierCodeEntities.isEmpty())
                exportSupplierCodeRepository.save(exportSupplierCodeEntity);
            else {
                LOGGER.debug(MessageCode.CLAIMS_1127.value());
                throw new BadRequestException(MessageCode.CLAIMS_1127);
            }
        }
        return exportSupplierCodeEntity;
    }

    @Override
    public ExportSupplierCodeEntity updateExportSupplierCode(ExportSupplierCodeEntity exportSupplierCodeEntity) {
        if (exportSupplierCodeEntity != null) {
            if (exportSupplierCodeRepository.searchExportSupplierCodeByVatNumberWithDifferentId(exportSupplierCodeEntity.getVatNumber(), exportSupplierCodeEntity.getId()) == null) {
                exportSupplierCodeRepository.save(exportSupplierCodeEntity);
            } else {
                LOGGER.debug("Export supplier code with vat number " + exportSupplierCodeEntity.getVatNumber() + " already exists");
                throw new BadRequestException("Export supplier code with vat number " + exportSupplierCodeEntity.getVatNumber() + " already exists", MessageCode.CLAIMS_1009);
            }
        }
        return exportSupplierCodeEntity;
    }

    @Override
    public void deleteExportSupplierCode(UUID uuid) {
        Optional<ExportSupplierCodeEntity> exportSupplierCodeEntityOptional = exportSupplierCodeRepository.findById(uuid);
        if (!exportSupplierCodeEntityOptional.isPresent()) {
            LOGGER.debug("Export Status TypeEnum with id " + uuid + " not found");
            throw new NotFoundException("Export Status TypeEnum with id " + uuid + " not found", MessageCode.CLAIMS_1010);
        }
        exportSupplierCodeRepository.delete(exportSupplierCodeEntityOptional.get());
    }

    @Override
    public ExportSupplierCodeEntity getExportSupplierCode(UUID uuid) {
        Optional<ExportSupplierCodeEntity> exportSupplierCodeEntityOptional = exportSupplierCodeRepository.findById(uuid);
        if (!exportSupplierCodeEntityOptional.isPresent()) {
            LOGGER.debug("Export Status TypeEnum with id " + uuid + " not found");
            throw new NotFoundException("Export Status TypeEnum with id " + uuid + " not found", MessageCode.CLAIMS_1010);
        }
        return exportSupplierCodeEntityOptional.get();
    }

    @Override
    public List<ExportSupplierCodeEntity> getAllExportSupplierCode() {
        List<ExportSupplierCodeEntity> exportSupplierCodeEntityList = exportSupplierCodeRepository.findAll();
        return exportSupplierCodeEntityList;
    }

    @Override
    public ExportSupplierCodeEntity patchActive(UUID uuid) {
        ExportSupplierCodeEntity exportSupplierCodeEntity = getExportSupplierCode(uuid);
        if (exportSupplierCodeEntity.getActive()) {
            exportSupplierCodeEntity.setActive(false);
        } else exportSupplierCodeEntity.setActive(true);

        exportSupplierCodeRepository.save(exportSupplierCodeEntity);
        return exportSupplierCodeEntity;
    }

    @Override
    public Pagination<ExportSupplierCodeEntity> findExportSupplierCode(Integer page, Integer pageSize, String orderBy, Boolean asc, String vatNumber, String denomination, String codeToExport, Boolean isActive) {
        Pagination<ExportSupplierCodeEntity> exportSupplierCodeEntityPagination = new Pagination<>();
        List<ExportSupplierCodeEntity> exportSupplierCodeEntityList = exportSupplierCodeRepositoryV1.findExportSupplierCode(page, pageSize, orderBy, asc, vatNumber, denomination, codeToExport, isActive);
        Long itemCount = exportSupplierCodeRepositoryV1.countPaginationExportSupplierCode(vatNumber, denomination, codeToExport, isActive);
        exportSupplierCodeEntityPagination.setItems(exportSupplierCodeEntityList);
        exportSupplierCodeEntityPagination.setStats(new PaginationStats(itemCount.intValue(), page, pageSize));

        return exportSupplierCodeEntityPagination;
    }

}
