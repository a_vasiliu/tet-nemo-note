package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.adapter.*;
import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.EmailTemplateMessagingRequestV1;
import com.doing.nemo.claims.controller.payload.request.IncidentRequestV1;
import com.doing.nemo.claims.controller.payload.request.PlatesStatusRequestV1;
import com.doing.nemo.claims.controller.payload.response.IncidentResponseV1;
import com.doing.nemo.claims.controller.payload.response.PaginationStats;
import com.doing.nemo.claims.controller.payload.response.practice.PracticeResponseV1;
import com.doing.nemo.claims.dto.KafkaEventDTO;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.ClaimsNewEntity;
import com.doing.nemo.claims.entity.MarsnovaLossPossessionLogEntity;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.enumerated.*;
import com.doing.nemo.claims.entity.enumerated.AuthorityEnum.AuthorityPracticeTypeEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsFlowEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.jsonb.Historical;
import com.doing.nemo.claims.entity.jsonb.Processing;
import com.doing.nemo.claims.entity.jsonb.Theft;
import com.doing.nemo.claims.entity.jsonb.practice.Finding;
import com.doing.nemo.claims.entity.jsonb.practice.TheftPractice;
import com.doing.nemo.claims.entity.settings.PracticeEntity;
import com.doing.nemo.claims.repository.*;
import com.doing.nemo.claims.service.*;
import com.doing.nemo.claims.service.GoLiveStrategyService;
import com.doing.nemo.claims.service.IncidentService;
import com.doing.nemo.claims.service.NemoEventProducerService;
import com.doing.nemo.claims.service.PracticeService;
import com.doing.nemo.claims.util.Util;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.BadParametersException;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;

import javax.transaction.Transactional;
import java.io.IOException;
import java.math.BigInteger;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@Service
public class PracticeServiceImpl implements PracticeService {

    private static Logger LOGGER = LoggerFactory.getLogger(PracticeServiceImpl.class);
    @Autowired
    private PracticeRepository practiceRepository;
    @Autowired
    private PracticeRepositoryV1 practiceRepositoryV1;
    @Autowired
    private ClaimsNewRepository claimsNewRepository;
    @Autowired
    private ConverterClaimsService converterClaimsService;
    @Autowired(required = false)
    private NemoEventProducerService nemoEventProducerService;
    @Autowired
    private IncidentAdapter incidentAdapter;
    @Autowired
    private IncidentService incidentService;
    @Autowired
    private MarsnovaLossPossessionLogRepository marsnovaLossPossessionLogRepository;
    @Autowired
    private GoLiveStrategyService goLiveStrategyService;


    @Autowired
    private MessagingService messagingService;

    @Autowired
    private ExternalCommunicationService externalCommunicationService;

    @Value("${kafka.topic}")
    private String kafkaTopic;

    //REFACTOR
    @Override
    @Transactional
    public PracticeEntity insertPractice(PracticeEntity practiceEntity) throws IOException {
        practiceEntity.setCreatedAt(DateUtil.getNowInstant());
        practiceEntity.setUpdatedAt(DateUtil.getNowInstant());
        practiceEntity.setStatus(PracticeStatusEnum.OPEN);
        ClaimsEntity claims = null;
        if (practiceEntity.getClaimsId() != null) {
            Optional<ClaimsNewEntity> claimsNewEntityOptional = claimsNewRepository.findById(practiceEntity.getClaimsId().toString());
            if (!claimsNewEntityOptional.isPresent()) {
                LOGGER.debug("Practice with id " + practiceEntity.getClaimsId() + " not found");
                throw new NotFoundException("Practice with id " + practiceEntity.getClaimsId() + " not found", MessageCode.CLAIMS_1010);
            }

            claims = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntityOptional.get());
            if (claims.getDamaged() != null && claims.getDamaged().getVehicle() != null && claims.getDamaged().getVehicle().getFleetVehicleId() != null
                    && practiceEntity.getVehicle() != null && practiceEntity.getVehicle().getFleetVehicleId() != null && !practiceEntity.getVehicle().getFleetVehicleId().equals(claims.getDamaged().getVehicle().getFleetVehicleId())) {
                LOGGER.debug(MessageCode.CLAIMS_1092.value());
                throw new BadRequestException(MessageCode.CLAIMS_1092);
            }
            practiceEntity.setTheft(TheftPracticeAdapter.adtpFromTheftClaimsToTheftPractice(claims.getTheft(), claims.getComplaint().getDataAccident().getDateAccident(), claims.getComplaint().getDataAccident().getAddress(),claims.getPracticeId()));
            practiceEntity.setFinding(FindingAdapter.adptFromFindingClaimsToFinding(claims.getTheft(),claims.getPracticeId()));
        }

        practiceRepository.save(practiceEntity);
        return practiceEntity;
    }

    //REFACTOR
    @Override
    public PracticeEntity insertPracticeByClaims(PracticeEntity practiceEntity) throws IOException {
        practiceEntity.setCreatedAt(DateUtil.getNowInstant());
        practiceEntity.setUpdatedAt(DateUtil.getNowInstant());
        practiceEntity.setStatus(PracticeStatusEnum.OPEN);

        if (practiceEntity.getClaimsId() != null) {
            Optional<ClaimsNewEntity> claimsNewEntityOptional = claimsNewRepository.findById(practiceEntity.getClaimsId().toString());
            if (!claimsNewEntityOptional.isPresent()) {
                LOGGER.debug("Practice with id " + practiceEntity.getClaimsId() + " not found");
                throw new NotFoundException("Practice with id " + practiceEntity.getClaimsId() + " not found", MessageCode.CLAIMS_1010);
            }

            ClaimsEntity claims = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntityOptional.get());
            if (claims.getDamaged() != null && claims.getDamaged().getVehicle() != null && claims.getDamaged().getVehicle().getFleetVehicleId() != null
                    && practiceEntity.getVehicle() != null && practiceEntity.getVehicle().getFleetVehicleId() != null && !practiceEntity.getVehicle().getFleetVehicleId().equals(claims.getDamaged().getVehicle().getFleetVehicleId())) {
                LOGGER.debug(MessageCode.CLAIMS_1092.value());
                throw new BadRequestException(MessageCode.CLAIMS_1092);
            }
            //practiceEntity.setTheft(TheftPracticeAdapter.adtpFromTheftClaimsToTheftPractice(claims.getTheft(), claims.getComplaint().getDataAccident().getDateAccident()));
            //practiceEntity.setFinding(FindingAdapter.adptFromFindingClaimsToFinding(claims.getTheft()));
        }


        practiceRepository.save(practiceEntity);
        return practiceEntity;
    }

    @Override
    public PracticeEntity updatePractice(PracticeEntity practiceEntity) throws IOException {
        Optional<PracticeEntity> practiceEntityOld = practiceRepository.findById(practiceEntity.getId());
        if (!practiceEntityOld.isPresent()) {
            LOGGER.debug("Practice with id " + practiceEntity.getId() + " not found");
            throw new NotFoundException("Practice with id " + practiceEntity.getId() + " not found", MessageCode.CLAIMS_1010);
        }
        practiceEntity.setUpdatedAt(DateUtil.getNowInstant());
        practiceEntity.setCreatedAt(practiceEntityOld.get().getCreatedAt());
        practiceEntity.setCreatedBy(practiceEntityOld.get().getCreatedBy());
        practiceEntity.setPracticeId(practiceEntityOld.get().getPracticeId());
        practiceEntity.setPracticeType(practiceEntityOld.get().getPracticeType());
        practiceEntity.setParentId(practiceEntityOld.get().getParentId());
        practiceEntity.setStatus(practiceEntityOld.get().getStatus());
        practiceEntity.setAttachmentList(practiceEntityOld.get().getAttachmentList());

        practiceRepository.save(practiceEntity);
        return practiceEntity;
    }

    @Override
    public PracticeEntity selectPractice(UUID id) {
        Optional<PracticeEntity> practiceEntity = practiceRepository.findById(id);
        if (!practiceEntity.isPresent()) {
            LOGGER.debug("Practice with id " + id + " not found");
            throw new NotFoundException("Practice with id " + id + " not found", MessageCode.CLAIMS_1010);
        }

        return practiceEntity.get();
    }

    @Override
    public List<PracticeEntity> selectAllPractice() {
        return practiceRepository.searchPractice();
    }

    @Override
    public PracticeResponseV1 deletePractice(UUID id) {
        PracticeEntity practiceEntity = selectPractice(id);
        practiceRepository.delete(practiceEntity);
        return null;
    }

    @Override
    public List<PracticeEntity> findPracticesFiltered(Integer page, Integer pageSize, Long practiceId, String practiceType, String vehiclePlate, String idContract, String statusList, String fromDate, String toDate, String orderBy, Boolean asc, String fleetVehicleId) {
        PracticeTypeEnum practiceTypeEnum = null;
        if (practiceType != null)
            practiceTypeEnum = PracticeTypeEnum.create(practiceType);
        return practiceRepositoryV1.findPracticesFiltered(page, pageSize, practiceId, practiceTypeEnum, vehiclePlate, idContract, statusList, fromDate, toDate, orderBy, asc, fleetVehicleId);
    }

    @Override
    public Long countPracticeFiltered(Long practiceId, String practiceType, String vehiclePlate, String idContract, String statusList, String fromDate, String toDate, String fleetVehicleId) {
        PracticeTypeEnum practiceTypeEnum = null;
        if (practiceType != null)
            practiceTypeEnum = PracticeTypeEnum.create(practiceType);
        return practiceRepositoryV1.countPracticeFiltered(practiceId, practiceTypeEnum, vehiclePlate, idContract, statusList, fromDate, toDate, fleetVehicleId);
    }

    @Override
    @Transactional
    public PracticeEntity patchTypePractice(UUID id, PracticeTypeEnum type, String userId) throws IOException {

        if (type == null) {
            LOGGER.debug(MessageCode.CLAIMS_1090.value());
            throw new BadRequestException(MessageCode.CLAIMS_1090);
        }

        PracticeEntity practiceEntity = this.selectPractice(id);

        if (practiceEntity.getProcessingList() != null) {
            for (Processing processing : practiceEntity.getProcessingList()) {
                if (processing.getProcessingDate() == null) {
                    LOGGER.debug(MessageCode.CLAIMS_1088.value());
                    throw new BadRequestException(MessageCode.CLAIMS_1088);
                }
            }
        }
        UUID parentId = null;
        if (practiceEntity.getParentId() == null)
            parentId = practiceEntity.getId();
        else
            parentId = practiceEntity.getParentId();

        practiceEntity.setStatus(PracticeStatusEnum.CLOSED);
        practiceEntity.setUpdatedAt(DateUtil.getNowInstant());
        practiceRepository.save(practiceEntity);



        PracticeEntity practiceEntityChild = new PracticeEntity(parentId, PracticeStatusEnum.OPEN, type, practiceEntity.getPracticeId(),
                practiceEntity.getCreatedAt(), DateUtil.getNowInstant(), userId, practiceEntity.getClaimsId(), null,
                null, practiceEntity.getVehicle(), practiceEntity.getCustomer(), practiceEntity.getContract(), practiceEntity.getFleetManagers(),practiceEntity.getFinding(), practiceEntity.getTheft(),practiceEntity.getMisappropriation(),practiceEntity.getSeizure(),practiceEntity.getReleaseFromSeizure() );


        return this.insertPractice(practiceEntityChild);
    }

    @Override
    public PracticeEntity patchTypePractice(UUID id, PracticeTypeEnum type, String userName, TheftPractice theft) throws IOException {

        if (type == null) {
            LOGGER.debug(MessageCode.CLAIMS_1090.value());
            throw new BadRequestException(MessageCode.CLAIMS_1090);
        }

        PracticeEntity practiceEntity = this.selectPractice(id);

        UUID parentId = null;
        if (practiceEntity.getParentId() == null)
            parentId = practiceEntity.getId();
        else
            parentId = practiceEntity.getParentId();

        practiceEntity.setStatus(PracticeStatusEnum.CLOSED);
        practiceEntity.setUpdatedAt(DateUtil.getNowInstant());
        practiceRepository.save(practiceEntity);

        PracticeEntity practiceEntityChild = new PracticeEntity(parentId, PracticeStatusEnum.OPEN, type, practiceEntity.getPracticeId(),
                practiceEntity.getCreatedAt(), DateUtil.getNowInstant(), userName, practiceEntity.getClaimsId(), null,
                null, practiceEntity.getVehicle(), practiceEntity.getCustomer(), practiceEntity.getContract(), practiceEntity.getFleetManagers(),practiceEntity.getFinding(), theft,practiceEntity.getMisappropriation(),practiceEntity.getSeizure(),practiceEntity.getReleaseFromSeizure() );


        return this.insertPracticeByClaims(practiceEntityChild);
    }


    @Override
    public PracticeEntity patchTypePractice(UUID id, PracticeTypeEnum type, String userName, Finding finding, TheftPractice theftPractice) throws IOException {

        if (type == null) {
            LOGGER.debug(MessageCode.CLAIMS_1090.value());
            throw new BadRequestException(MessageCode.CLAIMS_1090);
        }

        PracticeEntity practiceEntity = this.selectPractice(id);

        UUID parentId = null;
        if (practiceEntity.getParentId() == null)
            parentId = practiceEntity.getId();
        else
            parentId = practiceEntity.getParentId();

        practiceEntity.setStatus(PracticeStatusEnum.CLOSED);
        practiceEntity.setUpdatedAt(DateUtil.getNowInstant());
        practiceRepository.save(practiceEntity);

        PracticeEntity practiceEntityChild = new PracticeEntity(parentId, PracticeStatusEnum.OPEN, type, practiceEntity.getPracticeId(),
                practiceEntity.getCreatedAt(), DateUtil.getNowInstant(), userName, practiceEntity.getClaimsId(), null,
                null, practiceEntity.getVehicle(), practiceEntity.getCustomer(), practiceEntity.getContract(), practiceEntity.getFleetManagers(),finding, theftPractice ,practiceEntity.getMisappropriation(),practiceEntity.getSeizure(),practiceEntity.getReleaseFromSeizure() );


        return this.insertPracticeByClaims(practiceEntityChild);
    }

    //REFACTOR
    @Override
    public PracticeEntity patchClaimPractice(UUID id, UUID claimId) throws IOException {
        PracticeEntity practiceEntity = this.selectPractice(id);

        if (claimId == null) {
            LOGGER.debug(MessageCode.CLAIMS_1089.value());
            throw new BadRequestException(MessageCode.CLAIMS_1089);
        }

        Optional<ClaimsNewEntity> claimsNewEntityOptional = claimsNewRepository.findById(claimId.toString());
        if (!claimsNewEntityOptional.isPresent()) {
            LOGGER.debug("Practice with id " + id + " not found");
            throw new NotFoundException("Practice with id " + id + " not found", MessageCode.CLAIMS_1010);
        }

        ClaimsEntity claims = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntityOptional.get());
        if (claims.getDamaged() != null && claims.getDamaged().getVehicle() != null && claims.getDamaged().getVehicle().getFleetVehicleId() != null
                && practiceEntity.getVehicle() != null && practiceEntity.getVehicle().getFleetVehicleId() != null && !practiceEntity.getVehicle().getFleetVehicleId().equals(claims.getDamaged().getVehicle().getFleetVehicleId())) {
            LOGGER.debug(MessageCode.CLAIMS_1092.value());
            throw new BadRequestException(MessageCode.CLAIMS_1092);
        }


        practiceEntity.setClaimsId(claimId);
        practiceEntity.setTheft(TheftPracticeAdapter.adtpFromTheftClaimsToTheftPractice(claims.getTheft(), claims.getComplaint().getDataAccident().getDateAccident(), claims.getComplaint().getDataAccident().getAddress(),claims.getPracticeId()));
        practiceEntity.setFinding(FindingAdapter.adptFromFindingClaimsToFinding(claims.getTheft(),claims.getPracticeId()));
        practiceEntity = this.updatePractice(practiceEntity);
        if(PracticeTypeEnum.LOSS_POSSESSION == practiceEntity.getPracticeType() &&
                practiceEntity.getContract() != null &&  practiceEntity.getContract().getContractType() != null && practiceEntity.getContract().getContractType().equalsIgnoreCase("pol")
        ){
            //CHIAMATA A KAFKA
            KafkaEventDTO kafkaEventDTO = KafkaAdapter.adptFromClaimsToKafkaEvent(claims);
            nemoEventProducerService.produceEvent(kafkaEventDTO, kafkaTopic, KafkaEventTypeEnum.POSSESSION_LOSS);
        }
        return practiceEntity;

    }

    @Override
    public PracticeEntity patchStatusPractice(UUID id, PracticeStatusEnum newStatus) {

        if (newStatus == null) {
            LOGGER.debug(MessageCode.CLAIMS_1091.value());
            throw new BadRequestException(MessageCode.CLAIMS_1091);
        }

        PracticeEntity practiceEntity = this.selectPractice(id);

        if (newStatus.equals(PracticeStatusEnum.CLOSED)) {
            if (practiceEntity.getProcessingList() != null) {
                for (Processing processing : practiceEntity.getProcessingList()) {
                    if (processing.getProcessingDate() == null) {
                        LOGGER.debug(MessageCode.CLAIMS_1088.value());
                        throw new BadRequestException(MessageCode.CLAIMS_1088);
                    }
                }
            }

        }
        practiceEntity.setStatus(newStatus);
        practiceEntity.setUpdatedAt(DateUtil.getNowInstant());
        practiceRepository.save(practiceEntity);
        return practiceEntity;
    }

    @Override
    public List<PracticeEntity> findPracticesPlateAndClaims (String plate, UUID claimsId){
        return practiceRepositoryV1.findPracticesPlateAndClaims(plate,claimsId);
    }
    @Override
    public List<PracticeEntity> findPracticesPlateAndClaimsOr (String plate, UUID claimsId){
        return practiceRepositoryV1.findPracticesPlateAndClaimsOr(plate,claimsId);
    }

    @Override
    public List<PracticeEntity> findPracticesPlateClaimsTypeDate (String plate, UUID claimsId, PracticeTypeEnum practiceTypeEnum, Date dateClaims, PracticeStatusEnum practiceStatusEnum, Boolean last){
        String dateClaimsString = DateUtil.convertUTCDateToIS08601String(dateClaims);
        return practiceRepositoryV1.findPracticesPlateClaimsTypeDate(plate,claimsId, practiceTypeEnum, dateClaimsString, practiceStatusEnum, last);
    }

    public Pagination<PracticeEntity> getPracticePaginationAuthority(String plate, String chassis, int page, int pageSize, List<AuthorityPracticeTypeEnum> type) {
        try {

            Pagination<PracticeEntity> practiceEntityPagination = new Pagination<>();
            List<String> typeList = new ArrayList<>();
            for(AuthorityPracticeTypeEnum authorityPracticeTypeEnum : type){
                typeList.add(authorityPracticeTypeEnum.getValue());
            }
            List<PracticeEntity> practiceEntities = practiceRepositoryV1.findPracticesAuthorityPagination(page, pageSize,plate, chassis, typeList);
            BigInteger itemCount = practiceRepositoryV1.countPracticesAuthorityPagination(plate, chassis, typeList);


            //Mi serve necessariemante per calcolare la paina di start
            practiceEntityPagination.setStats(new PaginationStats(itemCount.intValue(), page, pageSize));

            /*int start = claimsPagination.getStats().getStartPage();
            int count = 0;*/
            //List<ClaimsEntity> claimsForPagination = new LinkedList<>();

            /*while (count < claimsPagination.getStats().getPageSize() && ((start - 1) + count) < claimsList.size()) {
                claimsForPagination.add((claimsList.get((start - 1) + count)));
                count++;
            }*/

            practiceEntityPagination.setItems(practiceEntities);

            return practiceEntityPagination;

        } catch (BadParametersException ex) {
            LOGGER.debug(ex.getMessage());
            throw ex;
        }
    }


    //REFACTOR
    @Override
    public void lossPossessionPractice () throws IOException {
        String goLiveStepZeroActivationDate = goLiveStrategyService.getGoLiveStepZeroDate();
        //recupero nuova entità
        List<ClaimsNewEntity> claimsNewEntityList =  claimsNewRepository.findTheftForLossPossessionNotMigrated(goLiveStepZeroActivationDate);
        if(claimsNewEntityList==null){
            claimsNewEntityList = new LinkedList<>();
        }
        if(goLiveStepZeroActivationDate!=null && !goLiveStepZeroActivationDate.isEmpty()){
            claimsNewEntityList.addAll(claimsNewRepository.findTheftForLossPossessionMigrated(goLiveStepZeroActivationDate));
        }
        //conversione nella vecchia entità
        List<ClaimsEntity> claimsEntityList = converterClaimsService.wrapFromClaimsNewEntityListToClaimsEntityList(claimsNewEntityList);
        for(ClaimsEntity current: claimsEntityList){

            TheftPractice theftPractice = null;
            if(current.getTheft()!=null){
                theftPractice = TheftPracticeAdapter.adtpFromTheftClaimsToTheftPractice(current.getTheft(), current.getComplaint().getDataAccident().getDateAccident(), current.getComplaint().getDataAccident().getAddress(),current.getPracticeId());
            } else {
                theftPractice = new TheftPractice();
                theftPractice.setTheftDate(current.getComplaint().getDataAccident().getDateAccident());
                if(current.getComplaint().getDataAccident().getAddress()!=null){
                    theftPractice.setProvince(current.getComplaint().getDataAccident().getAddress().getProvince());
                    theftPractice.setState(current.getComplaint().getDataAccident().getAddress().getState());
                    theftPractice.setTheftLocation(Util.replaceNull(current.getComplaint().getDataAccident().getAddress().getStreet()) +" "+ Util.replaceNull(current.getComplaint().getDataAccident().getAddress().getStreetNr())+" , "+Util.replaceNull(current.getComplaint().getDataAccident().getAddress().getZip())+" "+Util.replaceNull(current.getComplaint().getDataAccident().getAddress().getLocality())+" "+Util.replaceNull(current.getComplaint().getDataAccident().getAddress().getProvince())+" "+Util.replaceNull(current.getComplaint().getDataAccident().getAddress().getRegion())+" "+Util.replaceNull(current.getComplaint().getDataAccident().getAddress().getState()));
                    theftPractice.setTheftId(current.getPracticeId().toString());
                }
            }
            List<PracticeEntity> practiceEntitiesWithSamePlateOrClaims = this.findPracticesPlateAndClaimsOr(current.getComplaint().getPlate(),UUID.fromString(current.getId()));

            if(practiceEntitiesWithSamePlateOrClaims==null || practiceEntitiesWithSamePlateOrClaims.size()==0){
                current.getDamaged().getContract().setLicensePlate(current.getDamaged().getVehicle().getLicensePlate());
                PracticeEntity practiceEntity = new PracticeEntity( null, null, PracticeTypeEnum.LOSS_POSSESSION, null, null, null, "Cron automatico", UUID.fromString(current.getId()), null, null, current.getDamaged().getVehicle(), current.getDamaged().getCustomer(), current.getDamaged().getContract(), current.getDamaged().getFleetManagerList(), null, theftPractice, null, null, null);
                this.insertPracticeByClaims(practiceEntity);
                //CHIAMATA A KAFKA
                if( practiceEntity.getContract() != null &&  practiceEntity.getContract().getContractType() != null && practiceEntity.getContract().getContractType().equalsIgnoreCase("pol")) {
                    KafkaEventDTO kafkaEventDTO = KafkaAdapter.adptFromClaimsToKafkaEvent(current);
                    nemoEventProducerService.produceEvent(kafkaEventDTO, kafkaTopic, KafkaEventTypeEnum.POSSESSION_LOSS);
                }
            } else {
                List<PracticeEntity> practiceEntityLossPossessionSamePlateOrClaimsOpened = this.findPracticesPlateClaimsTypeDate(current.getComplaint().getPlate(),null,PracticeTypeEnum.LOSS_POSSESSION,null, PracticeStatusEnum.OPEN,null);
                if(practiceEntityLossPossessionSamePlateOrClaimsOpened == null || practiceEntityLossPossessionSamePlateOrClaimsOpened.size()==0){
                    List<PracticeEntity> practiceEntityLossPossessionSamePlateOrClaimsClosed = this.findPracticesPlateClaimsTypeDate(current.getComplaint().getPlate(),UUID.fromString(current.getId()),PracticeTypeEnum.LOSS_POSSESSION,current.getComplaint().getDataAccident().getDateAccident(),PracticeStatusEnum.CLOSED,null);
                    if(practiceEntityLossPossessionSamePlateOrClaimsClosed==null || practiceEntityLossPossessionSamePlateOrClaimsClosed.size()==0){
                        List<PracticeEntity> listPractice = this.findPracticesPlateClaimsTypeDate(current.getComplaint().getPlate(),UUID.fromString(current.getId()),null,null, null,true);
                        if(listPractice != null && !listPractice.isEmpty()) {
                            PracticeEntity oldPractice = listPractice.get(0);
                            this.patchTypePractice(oldPractice.getId(), PracticeTypeEnum.LOSS_POSSESSION, "Cron automatico", theftPractice);
                        }
                    }
                }
            }
        }
    }


    //REFACTOR
    //PATCH RITROVAMENTO
    @Override
    @org.springframework.transaction.annotation.Transactional(isolation = Isolation.SERIALIZABLE)
    public ClaimsEntity patchToTheftAndFinding(String claimsId, String userId,String userName, Theft findingRequest){
        //Recupero della nuova entità
        Optional<ClaimsNewEntity> claimsNewEntityOptional = claimsNewRepository.findById(claimsId);
        if (!claimsNewEntityOptional.isPresent()) {
            LOGGER.debug("Claims with id " + claimsId + " not found");
            throw new NotFoundException("Claims with id " + claimsId + " not found", MessageCode.CLAIMS_1010);
        }
        //conversione nella vecchia entità
        ClaimsEntity claimsEntity = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntityOptional.get());
        ClaimsEntity oldClaims = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntityOptional.get());
        IncidentRequestV1 oldIncident = incidentAdapter.adptFromClaimsEntityToIncidentRequest(claimsEntity,null,false,false);
        ClaimsStatusEnum oldStatus = claimsEntity.getStatus();
        DataAccidentTypeAccidentEnum oldType = claimsEntity.getComplaint().getDataAccident().getTypeAccident();
        ClaimsFlowEnum oldFlow = claimsEntity.getType();
        claimsEntity.getComplaint().getDataAccident().setTypeAccident(DataAccidentTypeAccidentEnum.FURTO_E_RITROVAMENTO);
        Finding finding = null;
        TheftPractice theft = null;
        Theft claimsTheft = null;
        if(claimsEntity.getTheft()!=null){
            theft = TheftPracticeAdapter.adtpFromTheftClaimsToTheftPractice(claimsEntity.getTheft(), claimsEntity.getComplaint().getDataAccident().getDateAccident(), claimsEntity.getComplaint().getDataAccident().getAddress(),claimsEntity.getPracticeId());

        }
        if(findingRequest!=null){
            finding = FindingAdapter.adptFromFindingClaimsToFinding(findingRequest,claimsEntity.getPracticeId());
            if(claimsEntity.getTheft()!=null){
                claimsTheft = claimsEntity.getTheft();
                claimsTheft = TheftClaimsAdapter.adptTheftRequestV1ToTheftFinding(claimsTheft,findingRequest);
            } else {
                claimsTheft = TheftClaimsAdapter.adptTheftRequestV1ToTheftFinding(new Theft(),findingRequest);
            }
        }
        List<PracticeEntity> practiceEntitiesWithSamePlateOrClaims = this.findPracticesPlateAndClaims(claimsEntity.getComplaint().getPlate(), UUID.fromString(claimsEntity.getId()));
        try {
            if (practiceEntitiesWithSamePlateOrClaims == null || practiceEntitiesWithSamePlateOrClaims.size() == 0) {
                claimsEntity.getDamaged().getContract().setLicensePlate(claimsEntity.getDamaged().getVehicle().getLicensePlate());
                PracticeEntity practiceEntity = new PracticeEntity(null, null, PracticeTypeEnum.RECOVERY, null, null, null, userName, UUID.fromString(claimsEntity.getId()), null, null, claimsEntity.getDamaged().getVehicle(), null, claimsEntity.getDamaged().getContract(), claimsEntity.getDamaged().getFleetManagerList(), finding, theft, null, null, null);
                this.insertPracticeByClaims(practiceEntity);
            } else {
                List<PracticeEntity> listPractice = this.findPracticesPlateClaimsTypeDate(claimsEntity.getComplaint().getPlate(), UUID.fromString(claimsEntity.getId()), null, null, null, true);
                if(listPractice != null && !listPractice.isEmpty()) {
                    PracticeEntity oldPractice = listPractice.get(0);
                    this.patchTypePractice(oldPractice.getId(), PracticeTypeEnum.RECOVERY, userId, finding, theft);
                }
            }
        }catch (IOException e){
            LOGGER.debug(MessageCode.CLAIMS_1088.value());
            throw new BadRequestException(MessageCode.CLAIMS_1088, e);
        }
        claimsEntity.setTheft(claimsTheft);
        claimsEntity.setUpdateAt(DateUtil.getNowInstant());
        Historical historical = new Historical(EventTypeEnum.FINDING, oldStatus, claimsEntity.getStatus(), oldType, claimsEntity.getComplaint().getDataAccident().getTypeAccident(), DateUtil.getNowDate(), claimsEntity.getUserId(), userName, oldFlow.getValue(), claimsEntity.getType().getValue(), "Creazione automatica pratica auto di tipo ritrovamento.");
        claimsEntity.addHistorical(historical);


        /*******************************************
         *     invio mail e salvataggio su log
         ********************************************/
        String description= StringUtils.EMPTY;
        if (findingRequest.getTemplateList()!=null){

            List<EmailTemplateMessagingRequestV1> emailTemplateMessaging = messagingService.splitEmailIfContainsUrl(findingRequest.getTemplateList(), claimsId);

            if(emailTemplateMessaging != null && !emailTemplateMessaging.isEmpty()) {
                description = messagingService.sendMailAndCreateLogs(emailTemplateMessaging, findingRequest.getMotivation(), claimsEntity.getCreatedAt());
            } else {
                description = messagingService.sendMailAndCreateLogs(findingRequest.getTemplateList(), findingRequest.getMotivation(), claimsEntity.getCreatedAt());
            }

        }else {
            List<EmailTemplateMessagingRequestV1> emailTemplateList = MessagingAdapter.adptEmailTemplateMessagingRequestToEmailTemplateMessagingResponseList(messagingService.getMailTemplateByTypeEvent(EventTypeEnum.FINDING, claimsEntity));
            List<EmailTemplateMessagingRequestV1> emailTemplateMessaging = messagingService.splitEmailIfContainsUrl(emailTemplateList, claimsId);

            if(emailTemplateMessaging != null && !emailTemplateMessaging.isEmpty()) {
                description = messagingService.sendMailAndCreateLogs(emailTemplateMessaging, findingRequest.getMotivation(), claimsEntity.getCreatedAt());
            } else {
                description = messagingService.sendMailAndCreateLogs(emailTemplateList, null, claimsEntity.getCreatedAt());
            }



        }

        historical = new Historical(EventTypeEnum.FINDING, oldStatus, claimsEntity.getStatus(), claimsEntity.getComplaint().getDataAccident().getTypeAccident(), claimsEntity.getComplaint().getDataAccident().getTypeAccident(), DateUtil.getNowDate(), claimsEntity.getUserId(), userName, claimsEntity.getType().getValue(), claimsEntity.getType().getValue(), description);
        claimsEntity.addHistorical(historical);

        /*******************************************
         *     invio mail e salvataggio su log
         ********************************************/


        //conversione nella nuova entità
        ClaimsNewEntity claimsNewEntity = converterClaimsService.convertOldEntityToNewClaimsEntity(claimsEntity);
        claimsNewRepository.save(claimsNewEntity);

        //Intervento 6 - C
        try {
            externalCommunicationService.modifyIncidentSync(claimsId,oldStatus, false,oldIncident,false,oldClaims);
        } catch (IOException e) {
            throw new BadRequestException(MessageCode.CLAIMS_1112);
        }

        if( claimsEntity.getDamaged() != null && claimsEntity.getDamaged().getContract() != null && claimsEntity.getDamaged().getContract().getContractType().equalsIgnoreCase("pol")) {            //CHIAMATA A KAFKA
            KafkaEventDTO kafkaEventDTO = KafkaAdapter.adptFromClaimsToKafkaEvent(claimsEntity);
            nemoEventProducerService.produceEvent(kafkaEventDTO, kafkaTopic, KafkaEventTypeEnum.FINDING);
        }


        return claimsEntity;
    }

    private Set<Integer> marsnovaEnum = new HashSet<Integer>() {{
        add(100451);
        add(100591);
        add(100590);
        add(402461);
        add(402462);
    }};

    @Override
    public void closeLossPossessionPracticeMarsnova(PlatesStatusRequestV1 requestV1){
        MarsnovaLossPossessionLogEntity logEntity = new MarsnovaLossPossessionLogEntity();
        logEntity.setCreatedAt(DateUtil.getNowInstant());
        logEntity.setDescription(requestV1.getVehicleStatusDescription());
        logEntity.setPlate(requestV1.getVehiclePlate());
        logEntity.setEnumId(requestV1.getVehicleStatusCode());
        LOGGER.debug("[MARSNOVA LOSS POSSESSION] Request body: "+requestV1);

        if(requestV1.getVehicleStatusCode()==null || !marsnovaEnum.contains(requestV1.getVehicleStatusCode())){
            logEntity.setResult("Bad Request enum value "+requestV1.getVehicleStatusCode()+" not accepted.");
            marsnovaLossPossessionLogRepository.save(logEntity);
            LOGGER.debug("[MARSNOVA LOSS POSSESSION] Bad Request enum value "+requestV1.getVehicleStatusCode()+" not accepted.");
            throw new BadRequestException("[MARSNOVA LOSS POSSESSION] Bad Request enum value "+requestV1.getVehicleStatusCode()+" not accepted.",MessageCode.CLAIMS_1159);
        }

        if(requestV1.getVehiclePlate()==null){
            logEntity.setResult("Plate cannot be null");
            marsnovaLossPossessionLogRepository.save(logEntity);
            LOGGER.debug("[MARSNOVA LOSS POSSESSION] Plate cannot be null");
            throw new NotFoundException("[MARSNOVA LOSS POSSESSION] Plate cannot be null",MessageCode.CLAIMS_1160);
        }
        String foundPractices = "";
        List<PracticeEntity> practiceEntities = practiceRepositoryV1.findPracticesPlateClaimsTypeDate(requestV1.getVehiclePlate(),null,PracticeTypeEnum.LOSS_POSSESSION,null,null,null);
        if(practiceEntities!=null){
            if(practiceEntities.size()>0){
                LOGGER.debug("[MARSNOVA LOSS POSSESSION] Loss possession practices found into NEMO for plate "+requestV1.getVehiclePlate());
                for(PracticeEntity current: practiceEntities){
                    foundPractices = foundPractices+current.getPracticeId()+", ";
                    if(current.getStatus().equals(PracticeStatusEnum.OPEN)){
                        current.setStatus(PracticeStatusEnum.CLOSED);
                        current.setUpdatedAt(DateUtil.getNowInstant());
                        practiceRepository.save(current);
                        LOGGER.debug("[MARSNOVA LOSS POSSESSION] Loss possession practice with id "+current.getId()+" closed." );
                    } else {
                        LOGGER.debug("[MARSNOVA LOSS POSSESSION] Loss possession practice with id "+current.getId()+" already closed." );
                    }
                }
            } else {
                logEntity.setResult("Plate "+requestV1.getVehiclePlate()+" not found into NEMO.");
                marsnovaLossPossessionLogRepository.save(logEntity);
                LOGGER.debug("[MARSNOVA LOSS POSSESSION] Plate "+requestV1.getVehiclePlate()+" not found into NEMO.");
                throw new NotFoundException("[MARSNOVA LOSS POSSESSION] Plate "+requestV1.getVehiclePlate()+" not found into NEMO.",MessageCode.CLAIMS_1160);
            }
        }
        if(!foundPractices.equals("")){
            String finalResult = "Loss possession practices closed (practice_id): "+foundPractices;
            logEntity.setResult(finalResult.substring(0, finalResult.lastIndexOf(",")));
        }
        marsnovaLossPossessionLogRepository.save(logEntity);
    }

}
