package com.doing.nemo.claims.service;

import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.settings.LawyerPaymentsEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public interface LawyerPaymentsService {

    Pagination<LawyerPaymentsEntity> getLawyerPaymentPagination(String plate, Long practiceId, String dateAccidentFrom, String dateAccidentTo,
                                                                      String paymentType, String bank, String lawyerCode, String accSale, String status, int page, int pageSize );

    //REFACTOR
    void postAssignedLawyerPayments(List<UUID> lawyerPaymentsList);
}
