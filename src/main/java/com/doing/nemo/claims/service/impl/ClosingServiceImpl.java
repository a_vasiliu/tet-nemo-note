package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.controller.payload.response.ClosingResponseV1;
import com.doing.nemo.claims.repository.ClaimsNewRepository;
import com.doing.nemo.claims.service.ClosingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClosingServiceImpl implements ClosingService {

    @Autowired
    private ClaimsNewRepository claimsNewRepository;

    @Override
    public List<ClosingResponseV1> findClosing(String idContract) {
        return claimsNewRepository.findClosing(idContract);
    }
}

