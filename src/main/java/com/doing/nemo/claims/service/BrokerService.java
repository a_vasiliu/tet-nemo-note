package com.doing.nemo.claims.service;


import com.doing.nemo.claims.controller.payload.response.BrokerResponseV1;
import com.doing.nemo.claims.entity.settings.BrokerEntity;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.List;
import java.util.UUID;

@Service
public interface BrokerService {
    BrokerEntity insertBroker(BrokerEntity brokerEntity);

    BrokerEntity updateBroker(BrokerEntity brokerEntity);

    BrokerEntity selectBroker(UUID id);

    List<BrokerEntity> selectAllBroker();

    BrokerResponseV1<BrokerEntity> deleteBroker(UUID id);

    BrokerEntity updateStatus(UUID uuid);

    List<BrokerEntity> findBrokersFiltered (Integer page, Integer pageSize);

    BigInteger countBrokerFiltered();

}
