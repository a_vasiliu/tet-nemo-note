package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.controller.payload.response.PaginationStats;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.settings.ExportClaimsTypeEntity;
import com.doing.nemo.claims.repository.ExportClaimsTypeRepository;
import com.doing.nemo.claims.repository.ExportClaimsTypeRepositoryV1;
import com.doing.nemo.claims.service.ExportClaimsTypeService;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ExportClaimsTypeServiceImpl implements ExportClaimsTypeService {
    private static Logger LOGGER = LoggerFactory.getLogger(ExportClaimsTypeServiceImpl.class);
    @Autowired
    private ExportClaimsTypeRepository exportClaimsTypeRepository;
    @Autowired
    private ExportClaimsTypeRepositoryV1 exportClaimsTypeRepositoryV1;

    @Override
    public ExportClaimsTypeEntity saveExportClaimsType(ExportClaimsTypeEntity exportClaimsTypeEntity) {
        if (exportClaimsTypeEntity != null) {
            List<ExportClaimsTypeEntity> exportClaimsTypeEntities = exportClaimsTypeRepository.checkDuplicateExportClaimsType(exportClaimsTypeEntity.getClaimsType());
            if (exportClaimsTypeEntities != null && !exportClaimsTypeEntities.isEmpty()) {
                LOGGER.debug(MessageCode.CLAIMS_1070.value());
                throw new BadRequestException(MessageCode.CLAIMS_1070);
            }

            exportClaimsTypeRepository.save(exportClaimsTypeEntity);
        }
        return exportClaimsTypeEntity;
    }

    @Override
    public ExportClaimsTypeEntity updateExportClaimsType(ExportClaimsTypeEntity exportClaimsTypeEntity) {
        if (exportClaimsTypeEntity != null)
            exportClaimsTypeRepository.save(exportClaimsTypeEntity);
        return exportClaimsTypeEntity;
    }

    @Override
    public void deleteExportClaimsType(UUID uuid) {
        Optional<ExportClaimsTypeEntity> exportClaimsTypeEntityOptional = exportClaimsTypeRepository.findById(uuid);
        if (!exportClaimsTypeEntityOptional.isPresent()) {
            LOGGER.debug("Export Claims TypeEnum with id " + uuid + " not found");
            throw new NotFoundException("Export Claims TypeEnum with id " + uuid + " not found", MessageCode.CLAIMS_1010);
        }
        exportClaimsTypeRepository.delete(exportClaimsTypeEntityOptional.get());
    }

    @Override
    public ExportClaimsTypeEntity getExportClaimsType(UUID uuid) {
        Optional<ExportClaimsTypeEntity> exportClaimsTypeEntityOptional = exportClaimsTypeRepository.findById(uuid);
        if (!exportClaimsTypeEntityOptional.isPresent()) {
            LOGGER.debug("Export Claims TypeEnum with id " + uuid + " not found");
            throw new NotFoundException("Export Claims TypeEnum with id " + uuid + " not found", MessageCode.CLAIMS_1010);
        }
        return exportClaimsTypeEntityOptional.get();
    }

    @Override
    public List<ExportClaimsTypeEntity> getAllExportClaimsType() {
        List<ExportClaimsTypeEntity> exportClaimsTypeEntityList = exportClaimsTypeRepository.findAll();
        return exportClaimsTypeEntityList;
    }

    @Override
    public ExportClaimsTypeEntity patchActive(UUID uuid) {
        ExportClaimsTypeEntity exportClaimsTypeEntity = getExportClaimsType(uuid);
        if (exportClaimsTypeEntity.getActive()) {
            exportClaimsTypeEntity.setActive(false);
        } else exportClaimsTypeEntity.setActive(true);

        exportClaimsTypeRepository.save(exportClaimsTypeEntity);
        return exportClaimsTypeEntity;
    }

    @Override
    public Pagination<ExportClaimsTypeEntity> findExportClaimsType(Integer page, Integer pageSize, String orderBy, Boolean asc, DataAccidentTypeAccidentEnum claimsType, String code, String description, Boolean isActive) {
        Pagination<ExportClaimsTypeEntity> exportClaimsTypeEntityPagination = new Pagination<>();
        List<ExportClaimsTypeEntity> exportClaimsTypeEntityList = exportClaimsTypeRepositoryV1.findExportClaimsType(page, pageSize, orderBy, asc, claimsType, code, description, isActive);
        Long itemCount = exportClaimsTypeRepositoryV1.countPaginationExportClaimsType(claimsType, code, description, isActive);
        exportClaimsTypeEntityPagination.setItems(exportClaimsTypeEntityList);
        exportClaimsTypeEntityPagination.setStats(new PaginationStats(itemCount.intValue(), page, pageSize));

        return exportClaimsTypeEntityPagination;
    }

}
