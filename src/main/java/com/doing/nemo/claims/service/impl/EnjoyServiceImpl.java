package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.adapter.*;
import com.doing.nemo.claims.command.ClaimsInsertEnjoyCommand;
import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.EmailTemplateMessagingRequestV1;
import com.doing.nemo.claims.controller.payload.response.ContractInfoResponseV1;
import com.doing.nemo.claims.controller.payload.response.DogeResponseV1;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.ClaimsNewEntity;
import com.doing.nemo.claims.entity.CounterpartyEntity;
import com.doing.nemo.claims.entity.claims.ClaimsDamagedRegistryEntity;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.enumerated.EventTypeEnum;
import com.doing.nemo.claims.entity.esb.CustomerESB.CustomerEsb;
import com.doing.nemo.claims.entity.esb.DriverESB.DriverEsb;
import com.doing.nemo.claims.entity.esb.FleetManagerESB.FleetManagerEsb;
import com.doing.nemo.claims.entity.esb.InsuranceCompanyESB.InsuranceCompanyEsb;
import com.doing.nemo.claims.entity.esb.RegistryESB;
import com.doing.nemo.claims.entity.esb.VehicleESB.VehicleEsb;
import com.doing.nemo.claims.entity.jsonb.Historical;
import com.doing.nemo.claims.entity.jsonb.damaged.Damaged;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.Customer;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbAntiTheftService.AntiTheftRequest;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbAntiTheftService.AntiTheftService;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbAntiTheftService.Registry;
import com.doing.nemo.claims.entity.jsonb.forms.Forms;
import com.doing.nemo.claims.entity.settings.AntiTheftServiceEntity;
import com.doing.nemo.claims.entity.settings.PersonalDataEntity;
import com.doing.nemo.claims.entity.settings.RecoverabilityEntity;
import com.doing.nemo.claims.repository.AntiTheftServiceRepository;
import com.doing.nemo.claims.repository.ClaimsNewRepository;
import com.doing.nemo.claims.repository.ClaimsRepository;
import com.doing.nemo.claims.repository.PersonalDataRepository;
import com.doing.nemo.claims.service.*;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.AbstractException;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.exception.InternalException;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import com.doing.nemo.commons.util.HttpUtil;
import com.doing.nemo.middleware.client.MiddlewareClient;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.Response;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.*;

@Service
public class EnjoyServiceImpl implements EnjoyService {

    private static Logger LOGGER = LoggerFactory.getLogger(EnjoyServiceImpl.class);

    @Value("${middleware.endpoint}")
    private String middlewareEndpoint;

    @Value("${middleware.api.drivers.id}")
    private String middlewareDriverByIdUri;

    @Value("${middleware.api.vehicleExchanges.plate}")
    private String middlewareVehicleExchangeByPlateUri;

    @Value("${middleware.api.customers.id.fleetmanagers}")
    private String middlewareCustomersFleetManagers;

    @Value("${middleware.api.customers.id.fleetmanagers.id}")
    private String middlewareCustomersFleetManagerById;

    @Value("${middleware.api.contracts.id.telematics}")
    private String middlewareCustomersTelematicsByCustomerId;

    @Value("${middleware.conn.timeout}")
    private Integer middlewareConnTimeout;

    @Value("${middleware.read.timeout}")
    private Integer middlewareReadTimeout;

    @Value("${middleware.write.timeout}")
    private Integer middlewareWriteTimeout;

    @Autowired
    private HttpUtil httpUtil;

    @Autowired
    private ClaimsNewRepository claimsRepository;

    @Autowired
    private ConverterClaimsService converterClaimsService;

    @Autowired
    private PersonalDataRepository personalDataRepository;

    @Autowired
    private ClaimsService claimsService;

    @Autowired
    private MessagingService messagingService;

    @Autowired
    private DwhClaimsService dwhClaimsService;

    @Autowired
    private RecoverabilityService recoverabilityService;

    @Autowired
    private DogeEnquService dogeEnquService;

    @Value("${dwh.call}")
    private Boolean dwhCall;

    @Autowired
    private IncidentService incidentService;

    @Autowired
    private AntiTheftServiceRepository antiTheftServiceRepository;

    @Autowired
    private FileManagerService fileManagerService;

    @Autowired
    private ExternalCommunicationService externalCommunicationService;

    @Autowired
    private AntiTheftRequestService antiTheftRequestService;

    @Value("${enjoy.retry.number}")
    private Integer retryNumber;




    private <T, P> T callWithoutCert(String url, HttpMethod method, P value, HashMap<String, String> headersparams, Class<T> outclass) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = null;
        Response responseToken = null;
        try {
            if (value != null) {
                jsonInString = mapper.writeValueAsString(value);
            }

           responseToken = httpUtil.callURL(headersparams, url, jsonInString, method,
                    middlewareConnTimeout, middlewareReadTimeout, middlewareWriteTimeout);

            if (responseToken.code() == HttpStatus.NOT_FOUND.value()) {
                LOGGER.debug(responseToken.message() + " with code " + responseToken.code());
                throw new NotFoundException(responseToken.message() + " with code " + responseToken.code(), MessageCode.CLAIMS_1010);
            }

            if (responseToken.code() != HttpStatus.OK.value()) {
                LOGGER.debug(responseToken.message() + " with code " + responseToken.code());
                throw new BadRequestException(responseToken.message() + " with code " + responseToken.code(), MessageCode.CLAIMS_2000);
            }
            String response = responseToken.body().string();
            LOGGER.debug("Response Enjoy Body: {}", response);
            return mapper.readValue(response, outclass);
        } catch (AbstractException e) {
            LOGGER.error(e.getMessage(), e);
            throw e;
        }  catch (Exception e) {
            String message = String.format(MessageCode.CLAIMS_2012.value(), e.getClass().getSimpleName(), e.getMessage());
            LOGGER.error(message, e);
            throw new InternalException(message, MessageCode.CLAIMS_2012);
        } finally {
            if (responseToken != null && responseToken.body() != null) {
                responseToken.body().close();
            }
        }

    }

    @Override
    public ContractInfoResponseV1 getDriverRegistryAndFleetFromESB(String idContract, String idCustomer, String idDriver, String date)  {

        String queryDate = date;
        Date date1 = new Date();
        if (queryDate != null && !queryDate.isEmpty()) {
            date1 = DateUtil.convertIS08601StringToUTCDate(queryDate);
        }
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMMdd"); //note: time zone not in format!
        sdf2.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));
        queryDate = sdf2.format(date1);
        LOGGER.debug("Data passata con fuso orario di Roma ESB: "+queryDate);

        ContractInfoResponseV1 result = null;

        result = this.getDriverRegistryFlettManagerInfo(idDriver,idCustomer,idContract, queryDate);
        String formattedDate = queryDate.substring(0, 4) + "-" + queryDate.substring(4, 6) + "-" + queryDate.substring(6, 8);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
        formattedDate += "T00:00:00Z";
        //Date date1 = format.parse(formattedDate);
        Instant dateInstant = DateUtil.convertIS08601StringToUTCInstant(formattedDate);

        return result;
    }

    @Override
    public BlockingQueue asynchronousEsbcalls(String idCustomer, String idDriver, String idContract, String date, BlockingQueue responseList) {

        BlockingQueue<Integer> fleetManagersId = new LinkedBlockingQueue<>();
        BlockingQueue<FleetManagerEsb> fleetManagersInfoList = new LinkedBlockingQueue<>();
        BlockingQueue<RegistryESB> registryESBBlockingQueue = new ArrayBlockingQueue<>(10000);
        ESBCallThread callDriverESB = null;
        if(idDriver != null) {
            String driverUrl = getDriverByIdUrl(idDriver);

            callDriverESB = new ESBCallThread(driverUrl,
                    "driver", HttpMethod.GET, null, fleetManagersId, fleetManagersInfoList, registryESBBlockingQueue, responseList);
        }

        String fleetmanagerListUrl = getFleetmanagersListByIdUrl(idCustomer);
        ESBCallThread callFleetManagersIdESB = new ESBCallThread(fleetmanagerListUrl,
                "fleetmanagersId", HttpMethod.GET, null, fleetManagersId, fleetManagersInfoList, registryESBBlockingQueue, responseList);

        String telematicsUrl = getTelematicsByIdUrl(idContract, date);
        ESBCallThread callTelematics = new ESBCallThread(telematicsUrl,
                "telematics", HttpMethod.GET, null, fleetManagersId, fleetManagersInfoList, registryESBBlockingQueue, responseList);


        try {
            if(callDriverESB != null)
                callDriverESB.startThread();
            callFleetManagersIdESB.startThread();
            callTelematics.startThread();

            if(callDriverESB != null)
                callDriverESB.join();
                callFleetManagersIdESB.join();


                List<Thread> fleetThreads = new LinkedList<>();
                for (Integer currentId : fleetManagersId) {
                    String fleetmanagerUrl = getFleetmanagersByIdUrl(idCustomer, currentId.toString());
                    ESBCallThread fleetThread = new ESBCallThread(fleetmanagerUrl,
                            "fleetmanagers", HttpMethod.GET, null, fleetManagersId, fleetManagersInfoList, registryESBBlockingQueue, responseList);
                    fleetThreads.add(fleetThread);
                    fleetThread.startThread();
                }


                for (Thread currentFM : fleetThreads) {
                    currentFM.join();
                }
                callTelematics.join();
                responseList.add(fleetManagersInfoList);
                responseList.add(registryESBBlockingQueue);



        } catch (InterruptedException e) {
            LOGGER.debug(e.getMessage());
        }

        return responseList;
    }

    private String getFleetmanagersByIdUrl(String idCustomer, String idFleetmanager) {

        return buildUrl(
                middlewareEndpoint,
                middlewareCustomersFleetManagerById,
                new HashMap<String, String>() {{
                    put("customerId", idCustomer);
                    put("fleetManagerId", idFleetmanager);
                }},
                null
        );
    }

    @Override
    public ContractInfoResponseV1 getDriverRegistryFlettManagerInfo(String idDriver,String idCustomer, String idContract, String date)  {

        BlockingQueue<Object> responseList = new ArrayBlockingQueue<>(7);

        ContractInfoResponseV1 contractInfoResponseV1 = new ContractInfoResponseV1();


        String formattedDate = date.substring(0, 4) + "-" + date.substring(4, 6) + "-" + date.substring(6, 8);

        String regex = "\\d+";
        boolean isPlate = true;
            responseList = asynchronousEsbcalls(idCustomer, idDriver, idContract, date, responseList);
            while (!responseList.isEmpty()) {
                Object obj = responseList.poll();
                if (obj instanceof DriverEsb) {
                    DriverEsb driverEsb = (DriverEsb) obj;
                    if (driverEsb.getMainAddress() != null)
                        driverEsb.getMainAddress().setFormattedAddress();
                    contractInfoResponseV1.setDriverResponse(DriverAdapter.adptDriverEsbToDriverResponse(driverEsb));
                }  else if (obj instanceof ArrayBlockingQueue) {
                    ArrayBlockingQueue<RegistryESB> registryESBS = (ArrayBlockingQueue<RegistryESB>) obj;
                    contractInfoResponseV1.setRegistryResponseList(RegistryAdapter.adptFromRegistryEsbToRegistryResponseV1List(registryESBS));
                } else if (obj instanceof BlockingQueue) {
                    BlockingQueue<FleetManagerEsb> fleetManagerEsbs = (BlockingQueue<FleetManagerEsb>) obj;
                    contractInfoResponseV1.setFleetManagerResponseList(FleetManagerAdpter.adtFromBlockingQueueFMESBToListFMResponse(fleetManagerEsbs));
                }
            }


        return contractInfoResponseV1;
    }

    @Override
    public FleetManagerEsb checkFleetManager(String customerId, String fleetManagerId) throws IOException {

        String url = getFleetmanagersByIdUrl(customerId, fleetManagerId);
        return callWithoutCert(url, HttpMethod.GET, null, null, FleetManagerEsb.class);
    }

    @Override
    public List<RegistryESB> getTelematics(String idContract) throws IOException {

        List<RegistryESB> registryESBList = new ArrayList<>();
        Response responseToken = null;
        try {
            String telematicsUrl = getTelematicsByIdUrl(idContract, null);
            responseToken = httpUtil.callURL(null, telematicsUrl, null, HttpMethod.GET,
                    middlewareConnTimeout, middlewareReadTimeout, middlewareWriteTimeout);

            String response = responseToken.body().string();
            LOGGER.debug("Response Enjoy getTelematics Body: {}", response);
            ObjectMapper mapper = new ObjectMapper();
            Map<String, List<Map<String, Object>>> FMMap = mapper.readValue(response, Map.class);
            List<Map<String, Object>> listTelematics = FMMap.get("services");
            for (Map<String, Object> currentTelematic : listTelematics) {
                String jsonTelematics = mapper.writeValueAsString(currentTelematic);
                RegistryESB registryESB = mapper.readValue(jsonTelematics, RegistryESB.class);
                registryESBList.add(registryESB);
            }
            return registryESBList;
        } catch (AbstractException e) {
            LOGGER.error(e.getMessage(), e);
            throw e;
        }  catch (Exception e) {
            String message = String.format(MessageCode.CLAIMS_2012.value(), e.getClass().getSimpleName(), e.getMessage());
            LOGGER.error(message, e);
            throw new InternalException(message, MessageCode.CLAIMS_2012);
        } finally {
            if (responseToken != null && responseToken.body() != null) {
                responseToken.body().close();
            }
        }
    }

    @Override
    public List<FleetManagerEsb> getFleetManagers(String idCustomer) throws IOException {

        List<Integer> fleetManagerIdList = new ArrayList<>();
        List<FleetManagerEsb> fleetManagerList = new ArrayList<>();
        Response responseToken = null;
        Response responseTokenFleet = null;
        try {
            String fleetmanagerListUrl = getFleetmanagersListByIdUrl(idCustomer);
            responseToken = httpUtil.callURL(null, fleetmanagerListUrl, null, HttpMethod.GET,
                    middlewareConnTimeout, middlewareReadTimeout, middlewareWriteTimeout);

            String response = responseToken.body().string();
            LOGGER.debug("Response Enjoy getFleetManagersList Body: {}", response);
            response = response.replaceAll("city", "locality");
            ObjectMapper mapper = new ObjectMapper();
            Map<String, List<Map<String, Integer>>> FMMap = mapper.readValue(response, Map.class);
            List<Map<String, Integer>> listFleet = FMMap.get("contacts");
            for (Map<String, Integer> currentFM : listFleet) {
                fleetManagerIdList.add(currentFM.get("id"));
            }

            for (Integer currentIds : fleetManagerIdList) {
                String fleetmanagerUrl = getFleetmanagersByIdUrl(idCustomer, currentIds.toString());
                responseTokenFleet = httpUtil.callURL(null, fleetmanagerUrl, null, HttpMethod.GET,
                        middlewareConnTimeout, middlewareReadTimeout, middlewareWriteTimeout);
                String responseFleet = responseTokenFleet.body().string();
                LOGGER.debug("Response Enjoy getFleetManagersById Body: {}", responseFleet);
                responseFleet = responseFleet.replaceAll("city", "locality");
                FleetManagerEsb fleetManagerEsb = mapper.readValue(responseFleet, FleetManagerEsb.class);
                fleetManagerList.add(fleetManagerEsb);
            }

        } catch (AbstractException e) {
            LOGGER.error(e.getMessage(), e);
            throw e;
        }  catch (Exception e) {
            String message = String.format(MessageCode.CLAIMS_2012.value(), e.getClass().getSimpleName(), e.getMessage());
            LOGGER.error(message, e);
            throw new InternalException(message, MessageCode.CLAIMS_2012);
        } finally {
            if (responseToken != null && responseToken.body() != null) {
                responseToken.body().close();
            }
            if (responseTokenFleet != null && responseTokenFleet.body() != null) {
                responseTokenFleet.body().close();
            }
        }


        return fleetManagerList;
    }

    private String getDriverByIdUrl(String idDriver) {

        return buildUrl(
                middlewareEndpoint,
                middlewareDriverByIdUri,
                new HashMap<String, String>() {{
                    put("id", idDriver);
                }},
                null
        );
    }

    private String getFleetmanagersListByIdUrl(String idCustomer) {

        return buildUrl(
                middlewareEndpoint,
                middlewareCustomersFleetManagers,
                new HashMap<String, String>() {{
                    put("id", idCustomer);
                }},
                null
        );
    }

    private String getTelematicsByIdUrl(String idContract, String date) {

        return buildUrl(
                middlewareEndpoint,
                middlewareCustomersTelematicsByCustomerId,
                new HashMap<String, String>() {{
                    put("id", idContract);
                }},
                StringUtils.isBlank(date)
                        ? null
                        : new LinkedMultiValueMap<String, String>() {{
                    add("date", date);
                }}
        );
    }

    private static String buildUrl(String baseUrl, String uri, Map<String, String> pathParams, MultiValueMap<String, String> queryParams) {

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(baseUrl.concat(uri));

        if (MapUtils.isNotEmpty(queryParams)) {
            builder.queryParams(queryParams);
        }

        return builder.buildAndExpand(pathParams).toString();
    }

    private class ESBCallThread extends Thread {
        private String path;
        private String tipo;
        private HttpMethod method;
        private Map<String, String> header;

        private BlockingQueue<Integer> fleetManagersId;

        private BlockingQueue<FleetManagerEsb> fleetManagersInfoList;

        private BlockingQueue<RegistryESB> registryESBBlockingQueue;
        private BlockingQueue<Object> responseList;

        public ESBCallThread(String path, String tipo, HttpMethod method, Map<String, String> header, BlockingQueue<Integer> fleetManagersId, BlockingQueue<FleetManagerEsb> fleetManagersInfoList, BlockingQueue<RegistryESB> registryESBBlockingQueue, BlockingQueue<Object> responseList) {
            this.path = path;
            this.tipo = tipo;
            this.method = method;
            this.header = header;
            this.fleetManagersId = fleetManagersId;
            this.fleetManagersInfoList = fleetManagersInfoList;
            this.registryESBBlockingQueue = registryESBBlockingQueue;
            this.responseList = responseList;
        }

        @Override
        public void run() {
            Boolean esbIsOk = false;
            Integer esbRetryNumber = 0;
            Exception esbException = null;
            Response responseToken = null;

            try {
                while (esbRetryNumber < retryNumber && !esbIsOk) {
                    try {

                        responseToken = httpUtil.callURL(this.header, this.path, null, this.method,
                                middlewareConnTimeout, middlewareReadTimeout, middlewareWriteTimeout);

                        String response = responseToken.body().string();


                        if( responseToken == null ||  responseToken.code() != 200){
                            throw new Exception("Esb call " + this.tipo + " failed with error " + responseToken.code() );
                        }

                        response = response.replaceAll("city", "locality");
                        ObjectMapper mapper = new ObjectMapper();
                        esbIsOk = true;
                        Object obj = null;
                        if (this.tipo.equalsIgnoreCase("vehicle")) {
                            obj = mapper.readValue(response, VehicleEsb.class);
                        } else if (this.tipo.equalsIgnoreCase("driver")) {
                            obj = mapper.readValue(response, DriverEsb.class);
                        } else if (this.tipo.equalsIgnoreCase("customer")) {
                            obj = mapper.readValue(response, CustomerEsb.class);
                        } else if (this.tipo.equalsIgnoreCase("fleetmanagersId")) {
                            Map<String, List<Map<String, Integer>>> FMMap = mapper.readValue(response, Map.class);
                            List<Map<String, Integer>> listFleet = FMMap.get("contacts");
                            for (Map<String, Integer> currentFM : listFleet) {
                                fleetManagersId.add(currentFM.get("id"));
                            }
                        } else if (this.tipo.equalsIgnoreCase("fleetmanagers")) {
                            FleetManagerEsb fleetManagerEsb = mapper.readValue(response, FleetManagerEsb.class);
                            fleetManagersInfoList.add(fleetManagerEsb);
                        } else if (this.tipo.equalsIgnoreCase("insurancecompany")) {
                            obj = mapper.readValue(response, InsuranceCompanyEsb.class);
                        } else if (this.tipo.equalsIgnoreCase("telematics")) {
                            Map<String, List<Map<String, Object>>> FMMap = mapper.readValue(response, Map.class);
                            List<Map<String, Object>> listTelematics = FMMap.get("services");

                            if (listTelematics != null) {

                                for (Map<String, Object> currentTelematic : listTelematics) {
                                    String jsonTelematics = mapper.writeValueAsString(currentTelematic);
                                    RegistryESB registryESB = mapper.readValue(jsonTelematics, RegistryESB.class);
                                    registryESBBlockingQueue.add(registryESB);
                                }
                            }
                        }

                        if (obj != null) {
                            responseList.add(obj);
                        }

                        LOGGER.debug("[ENJOY] " + this.tipo + " OK at attempt number "+(esbRetryNumber+1));

                    } catch(Exception e){
                        //LOGGER.debug(MessageCode.CLAIMS_1047.value());
                        // throw new InternalException(MessageCode.CLAIMS_1047, e);
                            LOGGER.debug("[ENJOY] " + this.tipo + " NOT OK at attempt number "+(esbRetryNumber+1));
                            esbRetryNumber = esbRetryNumber+1;
                        esbException = e;

                        if (responseToken != null && responseToken.body() != null) {
                            responseToken.body().close();
                            responseToken = null;
                        }
                    }
                }

                if(!esbIsOk){
                    LOGGER.debug("[ENJOY] " + this.tipo);
                    LOGGER.criticalError("NEMO_CLAIMS","[ENJOY] " + this.tipo + " failed", esbException );
                }

            } catch (AbstractException e) {
                LOGGER.error(e.getMessage(), e);
                throw e;
            }  catch (Exception e) {
                String message = String.format(MessageCode.CLAIMS_2012.value(), e.getClass().getSimpleName(), e.getMessage());
                LOGGER.error(message, e);
                throw new InternalException(message, MessageCode.CLAIMS_2012);
            } finally {
                if (responseToken != null && responseToken.body() != null) {
                    responseToken.body().close();
                }
            }

        }


        public BlockingQueue<Object> startThread() {
            BlockingQueue<Object> result = new ArrayBlockingQueue<>(7);

            this.start();
            result.add(fleetManagersId);
            result.add(fleetManagersInfoList);
            result.add(registryESBBlockingQueue);
            result.add(responseList);
            return result;
        }

    }

    public void enjoyFlow (ClaimsInsertEnjoyCommand command, String uuid, String driverId, String customerId, String contractId, String date) throws IOException {

        Exception exceptionDwh = null;

        LOGGER.info("[ENJOY] Calling esb routes on claim's id " + uuid);
        //INTERROGAZIONE ESB2
        ContractInfoResponseV1 contractInfoResponseV1 = this.getDriverRegistryAndFleetFromESB(contractId,customerId,driverId,date);



        Optional<ClaimsNewEntity> optionalClaimsEntity = claimsRepository.findById(uuid);
        if(!optionalClaimsEntity.isPresent()){
            LOGGER.debug(MessageCode.CLAIMS_1010.value());
            throw new BadRequestException(MessageCode.CLAIMS_1010);
        }
        ClaimsNewEntity claimsNewEntity = optionalClaimsEntity.get();
        ClaimsEntity claimsEntity = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntity);
            if(contractInfoResponseV1 != null){

                claimsEntity.getDamaged().setDriver(DriverAdapter.adptDriverResponseToDriver(contractInfoResponseV1.getDriverResponse()));
                claimsEntity.getDamaged().setFleetManagerList(FleetManagerAdpter.adptFromFMResponseToFM(contractInfoResponseV1.getFleetManagerResponseList()));
                claimsEntity.getDamaged().setAntiTheftService(new AntiTheftService());
                claimsEntity.getDamaged().getAntiTheftService().setRegistryList(RegistryAdapter.adptFromRegistryResponseToRegistryList(contractInfoResponseV1.getRegistryResponseList()));
            }
        if(command.getDamaged() != null && command.getDamaged().getDriver() != null){
            claimsEntity.getDamaged().setDriver(DriverAdapter.adptDriverRequestToDriver(command.getDamaged().getDriver()));
        }
        if(claimsEntity.getDamaged() != null){
            Customer customer = claimsEntity.getDamaged().getCustomer();
            if(customer != null){
                if(customerId != null){
                    PersonalDataEntity customerDb = personalDataRepository.findByCustomerId(customerId);

                    claimsEntity.getDamaged().setFleetManagerList(FleetManagerAdpter.adptFromFleetManagerPersonalDataToFleetManager
                            (customerDb.getFleetManagerPersonalData(), claimsEntity.getDamaged().getFleetManagerList()));

                    customerDb = PersonalDataAdapter.adptFromCustomerToPersonalData(customerDb, customer, claimsEntity.getDamaged().getFleetManagerList());
                    //Inserisco nella lista dei damaged i fleetmanager che non sono presenti nei fleet manager di customerdb
                    //claimsEntity.getDamaged().setFleetManagerList(PersonalDataAdapter.buildFinalFleetManagerListofDamaged(customerDb.getFleetManagerPersonalData(),claimsEntity.getDamaged().getFleetManagerList()));
                    personalDataRepository.save(customerDb);
                }
            }
        }
        DataAccidentTypeAccidentEnum typeAccident = null;
        if(claimsEntity.getComplaint() != null &&
           claimsEntity.getComplaint().getDataAccident() != null &&
           claimsEntity.getComplaint().getDataAccident().getTypeAccident() != null){
            typeAccident = claimsEntity.getComplaint().getDataAccident().getTypeAccident();
        }

        //DOGE E FILE MANAGER
        Forms forms =this.dogeWithRetry(claimsEntity.getId(),command.getUserId(), command.getUserName(),typeAccident);
        claimsEntity.setForms(forms);

        List<EmailTemplateMessagingRequestV1> emailTemplateList = MessagingAdapter.adptEmailTemplateMessagingRequestToEmailTemplateMessagingResponseList(messagingService.getMailTemplateByTypeEvent(EventTypeEnum.COMPLAINT_INSERTION, claimsEntity));
        String descriptionInsertion = messagingService.sendMailAndCreateLogsEnjoy(emailTemplateList, null, claimsEntity.getId());
        Historical historical = new Historical(EventTypeEnum.COMPLAINT_INSERTION,  ClaimsStatusEnum.WAITING_FOR_VALIDATION, ClaimsStatusEnum.WAITING_FOR_VALIDATION, claimsEntity.getComplaint().getDataAccident().getTypeAccident(), claimsEntity.getComplaint().getDataAccident().getTypeAccident(), DateUtil.getNowDate(), claimsEntity.getUserId(), command.getUserName(), claimsEntity.getType().getValue(), claimsEntity.getType().getValue(), descriptionInsertion);
        claimsEntity.addHistorical(historical);
        claimsNewEntity = converterClaimsService.convertOldEntityToNewClaimsEntity(claimsEntity);
        claimsRepository.save(claimsNewEntity);
        //claimsEntity = claimsRepository.getOne(claimsEntity.getId());

/*      try {
          LOGGER.info("[ENJOY] PAI ENJOY" + uuid);
          //IMPLEMANTAZIONE LOGICA RETRY SU PAI NJOY
            List<EmailTemplateMessagingRequestV1> emailTemplatePaiList = null;
            if (claimsService.isPai(claimsEntity)) {
                   emailTemplatePaiList = MessagingAdapter.adptEmailTemplateMessagingRequestToEmailTemplateMessagingResponseList(messagingService.getMailTemplateByTypeEvent(EventTypeEnum.ENTRUST_PAI, claimsEntity));
                if (emailTemplatePaiList != null) {
                    //RETRY INTERNO
                     messagingService.sendEmailEntrustPaiEnjoy(emailTemplatePaiList, claimsEntity.getId(), null, null);
                    claimsEntity.setPaiComunication(true);

                }

            }
        }catch(Exception ex) {
          LOGGER.info(MessageCode.CLAIMS_1151.value(), ex);
        }
        LOGGER.info("[ENJOY] PAI ENJOY FINE" + uuid);
      */
        claimsNewEntity = claimsRepository.getOne(claimsEntity.getId());

        if (claimsNewEntity.getType() != null && claimsNewEntity.getTypeAccident() != null) {

            LOGGER.info("[ENJOY] recoverabilityService" + uuid);
            RecoverabilityEntity recoverabilityEntity = recoverabilityService.getRecoverabilityByFlowAndTypeClaim(claimsNewEntity.getType().getValue().toUpperCase(), claimsNewEntity.getTypeAccident());
            if(recoverabilityEntity != null) {
                claimsNewEntity.setRecoverability(recoverabilityEntity.getRecoverability());
                claimsNewEntity.setRecoverabilityPercent(recoverabilityEntity.getPercentRecoverability());
            }
        }

        claimsNewEntity.setMigrated(false);
        claimsEntity = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntity);
        claimsEntity = claimsService.checkStatusByFlowExternalAndDraft(claimsEntity);

        //INCIDENT
        //Intervento 6 - E
        LOGGER.info("[ENJOY] CHIAMATA SERVIZI ESTERNI" + uuid);
        Future<ClaimsEntity> asyncClaimsEntity = externalCommunicationService.insertIncidentEnjoyAsync(claimsEntity,null);
        try {
            claimsEntity = asyncClaimsEntity.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        Instant nowInstant = DateUtil.getNowInstant();
        claimsEntity.setUpdateAt(nowInstant);
        claimsNewEntity = converterClaimsService.convertOldEntityToNewClaimsEntity(claimsEntity);
        claimsRepository.save(claimsNewEntity);

        claimsNewEntity = claimsRepository.getOne(uuid);


        //OCTO
        String codAntiTheftService = "";
            //recupero localizzatore da tabella setting AntiTheftServiceEntity
            //da capire se avremo mai un localizzatore per il furto e se ci viene passato un tipo
            if (claimsNewEntity.getClaimsDamagedRegistryEntityList() != null && !claimsNewEntity.getClaimsDamagedRegistryEntityList().isEmpty()) {

                ClaimsDamagedRegistryEntity currentRegistry = claimsNewEntity.getClaimsDamagedRegistryEntityList().get(0);
                codAntiTheftService = AntiTheftServiceAdapter.adptProviderToAntiTheftServiceEntity(currentRegistry.getCodPack());
                AntiTheftServiceEntity antiTheftServiceEntity = antiTheftServiceRepository.findAntiTheftByCodAntiTheftService(codAntiTheftService);
                if (antiTheftServiceEntity != null) {

                    // creazione della riga di storico di octo/texa
                    String idAntiTheftRequest = UUID.randomUUID().toString();

                    // TO REFACTOR
                    antiTheftRequestService.insertAntiTheftRequest(idAntiTheftRequest, claimsNewEntity, antiTheftServiceEntity.getProviderType(), "1");
                    claimsNewEntity.setAntiTheftServiceEntity(antiTheftServiceEntity);
                    claimsRepository.save(claimsNewEntity);
                    claimsService.callToOctoAsync(claimsNewEntity, claimsEntity.getStatus(), new AntiTheftRequest(), command.getUserId(), command.getUserId(), idAntiTheftRequest);


                //}
            }
        }

        //DWH

        //RETRY DWH
        if(dwhCall){

            claimsEntity = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntity);

            Boolean dwhIsOk = false;
            Integer dwhRetryNumber = 0;
            while(dwhRetryNumber < retryNumber && !dwhIsOk) {
                try {
                    dwhClaimsService.sendMessage(claimsEntity, com.doing.nemo.dwh.client.payload.request.EventTypeEnum.CREATED);
                    if (claimsEntity.getCounterparts() != null) {
                        for (CounterpartyEntity counterparty : claimsEntity.getCounterparts()) {
                            dwhClaimsService.sendMessage(counterparty, com.doing.nemo.dwh.client.payload.request.EventTypeEnum.CREATED);
                        }
                    }
                    LOGGER.debug("[ENJOY] DWH OK at attempt number "+(dwhRetryNumber+1));
                    dwhIsOk = true;
                }catch (Exception ex){
                    LOGGER.debug("[ENJOY] DWH NOT OK at attempt number "+(dwhRetryNumber+1));
                    dwhRetryNumber = dwhRetryNumber+1;
                    exceptionDwh = ex;
                }
            }

            if(dwhRetryNumber.equals(retryNumber)){
                LOGGER.debug("[ENJOY] DWH failed on claim's id " + uuid);
                LOGGER.criticalError("NEMO_CLAIMS","[ENJOY] DWH failed on claim's id " + uuid, exceptionDwh );
            }

        }



    }



    private Forms dogeWithRetry (String uuid, String userId, String userName, DataAccidentTypeAccidentEnum accidentType){
        Boolean dogeIsOk = false;
        Integer dogeRetryNumber = 0;
        DogeResponseV1 dogeResponseV1 = null;
        Forms forms= null;
        Exception exceptionDoge = null;

        while(dogeRetryNumber < retryNumber && !dogeIsOk){
            try {
                if(accidentType != null && (accidentType.equals(DataAccidentTypeAccidentEnum.FURTO_TOTALE) || accidentType.equals(DataAccidentTypeAccidentEnum.FURTO_E_RITROVAMENTO))){
                    dogeResponseV1 = dogeEnquService.enqueueFileTheftSummary(uuid,userName);
                }else{
                    dogeResponseV1 = dogeEnquService.enqueueClaimsWithoutCounterpart(uuid);
                }

                forms =fileManagerService.attachPdfFileClaimEnjoy(uuid, dogeResponseV1.getDocumentId(), userId, userName, false, false);
                LOGGER.debug("[ENJOY] DocumentGenerator/FileManager OK at attempt number "+(dogeRetryNumber+1));
                dogeIsOk = true;
            } catch (IOException e) {
                LOGGER.debug("[ENJOY] DocumentGenerator/FileManager NOT OK at attempt number "+(dogeRetryNumber+1));
                dogeRetryNumber = dogeRetryNumber+1;
                exceptionDoge = e;
            }
        }

        if(dogeRetryNumber.equals(retryNumber)){
            LOGGER.debug("[ENJOY] DocumentGenerator/FileManager failed on claim's id " + uuid);
            LOGGER.criticalError("NEMO_CLAIMS","[ENJOY] DocumentGenerator/FileManager failed on claim's id " + uuid, exceptionDoge );
        }
        return forms;
    }

    /*@Override
    public ContractInfoResponseV1 getContractInfoEnjoy (String plate, String date){
        String queryDate = date;
        Date date1 = new Date();
        if (queryDate != null && !queryDate.isEmpty()) {
            date1 = DateUtil.convertIS08601StringToUTCDate(queryDate);
        }
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMMdd"); //note: time zone not in format!
        sdf2.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));
        queryDate = sdf2.format(date1);
        MiddlewareClaimInfoResponse middlewareClaimInfoResponse = middlewareClient.getClaimInfoByPlateAndDate(plate, queryDate);

        ContractInfoResponseV1 result = EnjoyAdapter.adptFromMiddlewareResponseToContractInfoResponse(middlewareClaimInfoResponse);


        return result;
    }*/
}
