package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.controller.payload.response.LegalResponseV1;
import com.doing.nemo.claims.controller.payload.response.PaginationStats;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.enumerated.DamagedEnum.VehicleEnum.VehicleTypeEnum;
import com.doing.nemo.claims.entity.settings.AutomaticAffiliationRuleLegalEntity;
import com.doing.nemo.claims.entity.settings.LegalEntity;
import com.doing.nemo.claims.repository.AutomaticAffiliationRuleLegalRepository;
import com.doing.nemo.claims.repository.LegalRepository;
import com.doing.nemo.claims.repository.LegalRepositoryV1;
import com.doing.nemo.claims.service.AutomaticAffiliationRuleLegalService;
import com.doing.nemo.claims.service.LegalService;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class LegalServiceImpl implements LegalService {

    private static Logger LOGGER = LoggerFactory.getLogger(LegalServiceImpl.class);
    @Autowired
    private LegalRepository legalRepository;
    @Autowired
    private LegalRepositoryV1 legalRepositoryV1;
    @Autowired
    private AutomaticAffiliationRuleLegalService automaticAffiliationRuleLegalService;
    @Autowired
    private AutomaticAffiliationRuleLegalRepository automaticAffiliationRuleLegalRepository;

    @Override
    public LegalEntity insertLegal(LegalEntity legalEntity) {
        if (legalRepository.searchLegalbyName(legalEntity.getName()) != null) {
            LOGGER.debug("Error in Legal, duplicate name not admitted");
            throw new BadRequestException("Error in Legal, duplicate name not admitted", MessageCode.CLAIMS_1009);
        }
        if (legalEntity.getAutomaticAffiliationRuleEntityList() != null) {
            List<AutomaticAffiliationRuleLegalEntity> automaticAffiliationRuleLegalEntityList = automaticAffiliationRuleLegalService.updateRuleLegalList(legalEntity.getAutomaticAffiliationRuleEntityList());
        }
        legalEntity.setActive(true);
        legalRepository.save(legalEntity);
        return legalEntity;
    }

    @Override
    public LegalEntity updateLegalWithoutRule(LegalEntity legalEntity) {

        legalRepository.save(legalEntity);
        return legalEntity;
    }

    @Override
    public LegalResponseV1 deleteLegal(UUID uuid) {
        Optional<LegalEntity> legalEntity2 = legalRepository.findById(uuid);
        if (!legalEntity2.isPresent()) {
            LOGGER.debug("Legal with id " + uuid + " not found");
            throw new NotFoundException("Legal with id " + uuid + " not found", MessageCode.CLAIMS_1010);
        }
        LegalEntity legalEntity = legalEntity2.get();
        legalRepository.delete(legalEntity);
        return null;
    }

    @Override
    public LegalEntity selectLegal(UUID uuid) {
        Optional<LegalEntity> legalEntity2 = legalRepository.findById(uuid);
        if (!legalEntity2.isPresent()) {
            LOGGER.debug("Legal with id " + uuid + " not found");
            throw new NotFoundException("Legal with id " + uuid + " not found", MessageCode.CLAIMS_1010);
        }
        return legalEntity2.get();
    }

    @Override
    public List<LegalEntity> selectAllLegal() {
        List<LegalEntity> legalEntityList = legalRepository.findAll();

        return legalEntityList;
    }

    @Override
    public LegalEntity selectLegalWithMaxVolume(Boolean isNotCompanyCounterParty, Boolean isAbroad, Boolean isWounded, Boolean isRecoverability, UUID counterparty, DataAccidentTypeAccidentEnum typeClaim, VehicleTypeEnum counterpartType, UUID damaged, Double amount, Long practiceId) {

        SimpleDateFormat sdf = new SimpleDateFormat("MM/yyyy");
        sdf.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));
        String dataStr = sdf.format(new Date());
        AutomaticAffiliationRuleLegalEntity automaticAffiliationRuleLegalEntity = null;
        LegalEntity legalEntity = null;
        List<LegalEntity> legalEntityList = null;
        int i;
        LOGGER.debug("[ENTRUST - " + practiceId+ " - LEGAL] isNotCompanyCounterParty: " + isNotCompanyCounterParty);
        if (isNotCompanyCounterParty != null && isNotCompanyCounterParty) {
            legalEntityList = legalRepository.getMaxVolumeLegal(isNotCompanyCounterParty, isAbroad, isWounded, isRecoverability, typeClaim, dataStr, damaged, amount, counterpartType);
            i = 1;

            if (legalEntityList == null || legalEntityList.isEmpty()) {
                legalEntityList = legalRepository.getMaxVolumeLegalAndMonthlyVolumeZero(isNotCompanyCounterParty, isAbroad, isWounded, isRecoverability, typeClaim, dataStr, damaged, amount, counterpartType);
                i = 2;
            }
        } else {
            legalEntityList = legalRepository.getMaxVolumeLegalWITHCounterparty(isNotCompanyCounterParty, isAbroad, isWounded, isRecoverability, typeClaim, dataStr, damaged, amount, counterparty, counterpartType);
            i = 3;
            if (legalEntityList == null || legalEntityList.isEmpty()) {
                legalEntityList = legalRepository.getMaxVolumeLegalWITHCounterpartyAndMonthlyVolumeZero(isNotCompanyCounterParty, isAbroad, isWounded, isRecoverability, typeClaim, dataStr, damaged, amount, counterparty, counterpartType);
                i = 4;
            }
        }

      ;
        LOGGER.debug("[ENTRUST - " + practiceId+ " - LEGAL] try case: " + i);
        LOGGER.debug("[ENTRUST - " + practiceId+ " - LEGAL] legalEntityList: " + legalEntityList);


        if (!legalEntityList.isEmpty()) {
            legalEntity = legalEntityList.get(0);
            List<AutomaticAffiliationRuleLegalEntity> automaticAffiliationRuleLegalEntities = null;
            switch (i) {
                case 1:
                    automaticAffiliationRuleLegalEntities = automaticAffiliationRuleLegalRepository.getMaxVolumeRule(isNotCompanyCounterParty, isAbroad, isWounded, isRecoverability, typeClaim, dataStr, damaged, amount, counterpartType, legalEntity.getId());
                    break;
                case 2:
                    automaticAffiliationRuleLegalEntities = automaticAffiliationRuleLegalRepository.getMaxVolumeRuleAndMonthlyVolumeZero(isNotCompanyCounterParty, isAbroad, isWounded, isRecoverability, typeClaim, dataStr, damaged, amount, counterpartType, legalEntity.getId());
                    break;
                case 3:
                    automaticAffiliationRuleLegalEntities = automaticAffiliationRuleLegalRepository.getMaxVolumeRuleWITHCounterparty(isNotCompanyCounterParty, isAbroad, isWounded, isRecoverability, typeClaim, dataStr, damaged, amount, counterparty, counterpartType, legalEntity.getId());
                    break;
                case 4:
                    automaticAffiliationRuleLegalEntities = automaticAffiliationRuleLegalRepository.getMaxVolumeRuleWITHCounterpartyAndMonthlyVolumeZero(isNotCompanyCounterParty, isAbroad, isWounded, isRecoverability, typeClaim, dataStr, damaged, amount, counterparty, counterpartType, legalEntity.getId());
                    break;
                default:
                    break;
            }




            LOGGER.debug("[ENTRUST - " + practiceId+ " - LEGAL] automaticAffiliationRuleLegalEntities: " + automaticAffiliationRuleLegalEntities);


            if (automaticAffiliationRuleLegalEntities == null || automaticAffiliationRuleLegalEntities.isEmpty()) {
                LOGGER.debug("[ENTRUST - " + practiceId+ " - LEGAL] rule not found");
                return null;
            }
            automaticAffiliationRuleLegalEntity = automaticAffiliationRuleLegalEntities.get(0);
            automaticAffiliationRuleLegalRepository.updateRule(automaticAffiliationRuleLegalEntity.getId());

            LOGGER.debug("[ENTRUST - " + practiceId+ " - LEGAL] updateRule");
        }

        return legalEntity;

    }

    @Override
    public LegalEntity insertLegalRule(List<AutomaticAffiliationRuleLegalEntity> automaticAffiliationRuleLegalEntityList, UUID uuid) {
        LegalEntity legalEntity = selectLegal(uuid);
        for (AutomaticAffiliationRuleLegalEntity automaticAffiliationRuleLegalEntity : automaticAffiliationRuleLegalEntityList) {
            if (automaticAffiliationRuleLegalRepository.searchRuleForLegalbyName(automaticAffiliationRuleLegalEntity.getSelectionName(), uuid) != null) {
                LOGGER.debug("Error in Inspectorate Rule, duplicate name not admitted");
                throw new BadRequestException("Error in Inspectorate Rule, duplicate name not admitted", MessageCode.CLAIMS_1009);
            }
        }
        automaticAffiliationRuleLegalEntityList = automaticAffiliationRuleLegalService.updateRuleLegalList(automaticAffiliationRuleLegalEntityList);

        legalEntity.getAutomaticAffiliationRuleEntityList().addAll(automaticAffiliationRuleLegalEntityList);
        legalEntity = updateLegalWithoutRule(legalEntity);
        return legalEntity;
    }

    @Override
    public LegalEntity updateLegalRule(List<AutomaticAffiliationRuleLegalEntity> automaticAffiliationRuleLegalEntityList, LegalEntity legalEntity) {
        automaticAffiliationRuleLegalEntityList = automaticAffiliationRuleLegalService.updateRuleLegalList(automaticAffiliationRuleLegalEntityList);

        legalEntity.setAutomaticAffiliationRuleEntityList(automaticAffiliationRuleLegalEntityList);
        legalEntity = updateLegalWithoutRule(legalEntity);
        return legalEntity;
    }

    @Override
    public LegalEntity updateLegal(LegalEntity legalEntity, UUID uuid) {
        Optional<LegalEntity> legalEntityOptional = legalRepository.findById(uuid);
        if (!legalEntityOptional.isPresent()) throw new NotFoundException("Legal not found", MessageCode.CLAIMS_1010);

        if (legalRepository.searchLegalWithDifferentId(uuid, legalEntity.getName()) != null) {
            LOGGER.debug("Error in Legal, duplicate name not admitted");
            throw new BadRequestException("Error in Legal, duplicate name not admitted", MessageCode.CLAIMS_1009);
        }

        if(legalEntity.getActive()==null){
            LOGGER.debug("Error in Legal, isActive cannot be null.");
            throw new BadRequestException("Error in Legal, isActive cannot be null.", MessageCode.CLAIMS_1161);
        }

        LegalEntity legalEntity1 = legalEntityOptional.get();

        if (legalEntity1.getAutomaticAffiliationRuleEntityList() != null) {
            for (AutomaticAffiliationRuleLegalEntity automaticAffiliationRuleLegalEntity : legalEntity1.getAutomaticAffiliationRuleEntityList()) {
                automaticAffiliationRuleLegalRepository.delete(automaticAffiliationRuleLegalEntity);
            }
        }
        if (legalEntity.getAutomaticAffiliationRuleEntityList() != null && !legalEntity.getAutomaticAffiliationRuleEntityList().isEmpty()) {
            legalEntity = updateLegalRule(legalEntity.getAutomaticAffiliationRuleEntityList(), legalEntity);
        } else {
            legalEntity = updateLegalWithoutRule(legalEntity);
        }
        return legalEntity;
    }

    @Override
    public LegalEntity updateStatusLegal(UUID uuid) {
        LegalEntity legalEntity = selectLegal(uuid);
        if (legalEntity.getActive()!=null && legalEntity.getActive()) {
            legalEntity.setActive(false);
        } else {
            legalEntity.setActive(true);
        }

        legalRepository.save(legalEntity);
        return legalEntity;
    }

    @Override
    public Pagination<LegalEntity> paginationLegal(int page, int pageSize, String orderBy, Boolean asc, String name, String email, String province, String telephone, String fax, Boolean isActive, String address, String zipCode, String city, String state, Long code) {
        Pagination<LegalEntity> legalPagination = new Pagination<>();
        List<LegalEntity> legalList = legalRepositoryV1.findPaginationLegal(page, pageSize, orderBy, asc, name, email, province, telephone, fax, isActive, address, zipCode, city, state, code);
        Long itemCount = legalRepositoryV1.countPaginationLegal(name, email, province, telephone, fax, isActive, address, zipCode, city, state, code);
        legalPagination.setItems(legalList);
        legalPagination.setStats(new PaginationStats(itemCount.intValue(), page, pageSize));

        return legalPagination;
    }

    @Transactional
    public void resetRuleJob() {

        SimpleDateFormat sdf = new SimpleDateFormat("MM/yyyy");
        sdf.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));
        String dataStr = sdf.format(new Date());
        List<AutomaticAffiliationRuleLegalEntity> automaticAffiliationRuleLegalEntities = automaticAffiliationRuleLegalRepository.findAll();
        if (automaticAffiliationRuleLegalEntities != null) {
            for (AutomaticAffiliationRuleLegalEntity automaticAffiliationRuleLegalEntity : automaticAffiliationRuleLegalEntities) {
                if (!automaticAffiliationRuleLegalEntity.getCurrentMonth().equals(dataStr)) {
                    automaticAffiliationRuleLegalEntity.setCurrentMonth(dataStr);
                    automaticAffiliationRuleLegalEntity.setCurrentVolume(0);
                    automaticAffiliationRuleLegalRepository.save(automaticAffiliationRuleLegalEntity);
                }
            }
        }
    }

}
