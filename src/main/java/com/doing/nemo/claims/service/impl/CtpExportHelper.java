package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.dto.CtpExportFilter;
import com.doing.nemo.claims.service.CounterpartyService;
import com.doing.nemo.claims.service.ExportHelper;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.BadRequestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("CtpExportHelper")
public class CtpExportHelper extends ExportHelper<CtpExportFilter> {

    @Autowired
    private CounterpartyService counterpartyService;

    @Override
    public Integer getExportCount(CtpExportFilter filter) {
        int exportCount = counterpartyService.countCounterpartyForExport(filter).intValue();
        if(exportCount <= 0){
            throw new BadRequestException(MessageCode.CLAIMS_1162);
        }
        return exportCount;
    }

}
