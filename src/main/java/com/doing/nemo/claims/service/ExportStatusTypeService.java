package com.doing.nemo.claims.service;

import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.settings.ExportStatusTypeEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public interface ExportStatusTypeService {
    ExportStatusTypeEntity saveExportStatusType(ExportStatusTypeEntity exportStatusTypeEntity);
    ExportStatusTypeEntity updateExportStatusType(ExportStatusTypeEntity exportStatusTypeEntity);
    void deleteExportStatusType(UUID uuid);
    ExportStatusTypeEntity getExportStatusType(UUID uuid);
    List<ExportStatusTypeEntity> getAllExportStatusType();
    ExportStatusTypeEntity patchActive(UUID uuid);
    Pagination<ExportStatusTypeEntity> findExportStatusType(Integer page, Integer pageSize, String orderBy, Boolean asc, String statusType, String flowType, String code, String description, Boolean isActive);

}
