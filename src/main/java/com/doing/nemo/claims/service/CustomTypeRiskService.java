package com.doing.nemo.claims.service;

import com.doing.nemo.claims.entity.settings.CustomTypeRiskEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CustomTypeRiskService {
    List<CustomTypeRiskEntity> updateCustomTypeRiskList(List<CustomTypeRiskEntity> customTypeRiskEntityList);

    void deleteCustomTypeRiskList(List<CustomTypeRiskEntity> customTypeRiskEntityList);
}
