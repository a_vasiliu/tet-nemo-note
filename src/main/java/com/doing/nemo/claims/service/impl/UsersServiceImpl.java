package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.controller.payload.response.UsersResponseV1;
import com.doing.nemo.claims.service.UsersService;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import com.doing.nemo.commons.util.HttpUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.Response;
import org.apache.commons.collections4.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Service
public class UsersServiceImpl implements UsersService {

    private static Logger LOGGER = LoggerFactory.getLogger(UsersServiceImpl.class);

    @Value("${users.api.endpoint}")
    private String usersApiEndpoint;

    @Value("${users.api.path.users}")
    private String userApiPathUsers;

    @Value("${users.api.path.users.id}")
    private String userApiPathUsersId;

    @Autowired
    private HttpUtil httpUtil;

    private <T, P> T callWithoutCert(String url, HttpMethod method, P value, HashMap<String, String> headersparams, Class<T> outclass) throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = null;

        if (value != null) {
            jsonInString = mapper.writeValueAsString(value);
        }

        Response responseToken = httpUtil.callURL(headersparams, url, jsonInString, method);

        if (responseToken.code() == HttpStatus.NOT_FOUND.value()) {
            LOGGER.info(responseToken.message() + " with code " + responseToken.code());
            throw new NotFoundException(responseToken.message() + " with code " + responseToken.code());
        }
        else if (responseToken.code() != HttpStatus.OK.value() && responseToken.code() != HttpStatus.CREATED.value()) {
            LOGGER.info(responseToken.message() + " with code " + responseToken.code());
            throw new BadRequestException(responseToken.message() + " with code " + responseToken.code());
        }

        String response = responseToken.body().string();

        return mapper.readValue(response, outclass);
    }

    @Override
    public String getUserEmailByUserId(String userId, String authorization) throws IOException {
        HashMap<String, String> header = new HashMap<>();
        header.put("Authorization", authorization);

        String path = userApiPathUsers + userApiPathUsersId;
        String url = buildUrl(
                usersApiEndpoint,
                path,
                new HashMap<String, String>() {{
                    put("id", userId);
                }},
                null
        );

        //LOGGER.info("url " + url);

        UsersResponseV1 usersResponseV1 = null;
        try {
            usersResponseV1 = callWithoutCert(url, HttpMethod.GET, null, header, UsersResponseV1.class);
        } catch(NotFoundException e) {
            usersResponseV1 = null;
        } catch(Exception e) {
            LOGGER.error("[getUserEmailByUserId] ", e);
            throw e;
        }

        if(usersResponseV1 == null) { return null; }
        //LOGGER.info(usersResponseV1.toString());

        return usersResponseV1.getUsername();
    }

    private static String buildUrl(String baseUrl, String uri, Map<String, String> pathParams, MultiValueMap<String, String> queryParams) {

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(baseUrl.concat(uri));

        if (MapUtils.isNotEmpty(queryParams)) {
            builder.queryParams(queryParams);
        }

        return builder.buildAndExpand(pathParams).toString();
    }
}
