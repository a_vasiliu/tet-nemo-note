package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.dto.ClaimsExportFilter;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.ClaimsNewEntity;
import com.doing.nemo.claims.entity.jsonb.Historical;
import com.doing.nemo.claims.repository.ClaimsNewRepository;
import com.doing.nemo.claims.service.AsynchExporter;
import com.doing.nemo.claims.service.ConverterClaimsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

@Service("LogsAsynchExporterImpl")
public class LogsAsynchExporterImpl extends AsynchExporter<ClaimsExportFilter>  {

    @Value("${export.claims.blocksize}")
    private Integer exportClaimsBlocksize;

    @Value("${kafka.topic.export.claims}")
    private String kafkaTopicExportClaims;

    @Autowired
    private ClaimsNewRepository claimsNewRepository;

    @Autowired
    private ConverterClaimsService converterClaimsService;

    @Autowired
    private LogsExportMapInitializerImpl logsMapInitializer;

    @Autowired
    private LogsExportMapFillerImpl logsMapFiller;

    @Async
    @Override
    public void export(ClaimsExportFilter filter, int pagesNumber, int exportCount, String userEmail,String nemoUserId) throws IOException {
        int pageSize = exportClaimsBlocksize;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));
        String now = simpleDateFormat.format(DateUtil.getNowDate());
        String processId = nemoUserId + "-" + "ExportClaims" + "-" + now;

        // GO IN WITH GENERATION
        for (int page = 1; page <= pagesNumber; page++) {
            List<ClaimsNewEntity> claimsNewEntityList = claimsNewRepository.exportClaimsByPages(filter, page, pageSize);
            if(claimsNewEntityList != null) {
                List<ClaimsEntity> claimsEntityList = converterClaimsService.wrapFromClaimsNewEntityListToClaimsEntityList(claimsNewEntityList);
                int index = 0;
                // BUILDING JSON TEMPLATE MAP
                for (ClaimsEntity claimsEntity : claimsEntityList) {
                    // inizializza le strutture che dovranno essere inviate a DoGe
                    Map<String, Object> requestMap = sheetsBuilder.build();
                    LinkedList<Map<String, Object>> logList = sheetsBuilder.get(requestMap,"log"); // sinistri

                    List<Historical> historicals = claimsEntity.getHistorical();
                    if(historicals != null && !historicals.isEmpty()) {
                        for (Historical historical : claimsEntity.getHistorical()) {
                            Map<String, Object> innerMap = logsMapInitializer.log();
                            innerMap = logsMapFiller.log(innerMap, claimsEntity, historical);
                            logList.add(innerMap);
                        }
                    }
                    uploadOnKafka(requestMap, (index + 1) + ((page - 1) * pageSize), exportCount, kafkaTopicExportClaims, userEmail, null, "DettaglioSinistri", processId);
                    index++;
                }
            }
        }

    }

}
