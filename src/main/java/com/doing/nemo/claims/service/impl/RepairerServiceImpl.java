package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.controller.payload.response.PaginationStats;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.settings.RepairerEntity;
import com.doing.nemo.claims.repository.RepairerRepository;
import com.doing.nemo.claims.repository.RepairerRepositoryV1;
import com.doing.nemo.claims.service.RepairerService;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class RepairerServiceImpl implements RepairerService {
    private static Logger LOGGER = LoggerFactory.getLogger(RepairerServiceImpl.class);
    @Autowired
    private RepairerRepository repairerRepository;
    @Autowired
    private RepairerRepositoryV1 repairerRepositoryV1;

    public RepairerEntity insertRepairer(RepairerEntity repairerEntity) {
        if (repairerEntity != null)
            repairerRepository.save(repairerEntity);
        return repairerEntity;
    }

    public RepairerEntity updateRepairer(RepairerEntity repairerEntity) {
        if (repairerEntity != null)
            repairerRepository.save(repairerEntity);
        return repairerEntity;
    }

    public RepairerEntity selectRepairer(UUID uuid) {
        Optional<RepairerEntity> repairerEntity1 = repairerRepository.findById(uuid);
        if (!repairerEntity1.isPresent()) {
            LOGGER.debug("Repairer with id " + uuid + " not found");
            throw new NotFoundException("Repairer with id " + uuid + " not found", MessageCode.CLAIMS_1010);
        }
        return repairerEntity1.get();
    }

    public void deleteRepairer(UUID uuid) {
        Optional<RepairerEntity> repairerEntity1 = repairerRepository.findById(uuid);
        if (!repairerEntity1.isPresent()) {
            LOGGER.debug("Repairer with id " + uuid + " not found");
            throw new NotFoundException("Repairer with id " + uuid + " not found", MessageCode.CLAIMS_1010);
        }
        repairerRepository.delete(repairerEntity1.get());
    }

    public List<RepairerEntity> selectAllRepairer() {
        List<RepairerEntity> repairerEntities = repairerRepository.findAll();
        return repairerEntities;
    }

    @Override
    public RepairerEntity patchActive(UUID uuid) {
        RepairerEntity repairerEntity = selectRepairer(uuid);
        if (repairerEntity.getActive()) {
            repairerEntity.setActive(false);
        } else repairerEntity.setActive(true);

        repairerRepository.save(repairerEntity);
        return repairerEntity;
    }

    @Override
    public Pagination<RepairerEntity> paginationRepairer(int page, int pageSize) {
        Pagination<RepairerEntity> repairerPagination = new Pagination<>();
        List<RepairerEntity> repairerList = repairerRepositoryV1.findPaginationRepairer(page, pageSize);
        Long itemCount = repairerRepositoryV1.countPaginationRepairer();
        repairerPagination.setItems(repairerList);
        repairerPagination.setStats(new PaginationStats(itemCount.intValue(), page, pageSize));

        return repairerPagination;
    }

}
