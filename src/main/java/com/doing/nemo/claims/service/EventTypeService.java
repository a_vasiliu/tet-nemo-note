package com.doing.nemo.claims.service;

import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.enumerated.ClaimsRepairEnum;
import com.doing.nemo.claims.entity.settings.EventTypeEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public interface EventTypeService {

    EventTypeEntity insertEventType(EventTypeEntity eventTypeEntity);

    EventTypeEntity updateEventType(EventTypeEntity eventTypeEntity, UUID uuid);

    EventTypeEntity getEventType(UUID uuid);

    List<EventTypeEntity> getAllEventType(ClaimsRepairEnum claimsRepairEnum);

    EventTypeEntity deleteEventType(UUID uuid);

    EventTypeEntity patchStatusEventType(UUID uuid);

    Pagination<EventTypeEntity> paginationEventType(int page, int pageSize, String claimsRepairEnum, String orderBy, String description, String eventType, Boolean createAttachments, String catalog, String attachmentsType, Boolean onlyLast, Boolean isActive, Boolean asc);

}
