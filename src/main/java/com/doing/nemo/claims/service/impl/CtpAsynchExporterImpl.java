package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.dto.CtpExportFilter;
import com.doing.nemo.claims.entity.CounterpartyEntity;
import com.doing.nemo.claims.entity.jsonb.forms.jsonbForms.Attachment;
import com.doing.nemo.claims.entity.jsonb.repair.LastContact;
import com.doing.nemo.claims.service.AsynchExporter;
import com.doing.nemo.claims.service.CounterpartyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service("CtpAsynchExporterImpl")
public class CtpAsynchExporterImpl extends AsynchExporter<CtpExportFilter> {

    @Value("${export.repair.blocksize}")
    private Integer exportRepairBlocksize;

    @Autowired
    private CounterpartyService counterpartyService;

    @Autowired
    private CtpExportMapInitializerImpl ctpMapInitializer;

    @Autowired
    private CtpExportMapFillerImpl ctpMapFiller;

    @Value("${kafka.topic.export.counterparty}")
    private String kafkaTopicExportCounterparty;

    @Async
    @Override
    public void export(CtpExportFilter filter, int pagesNumber, int exportCount, String userEmail,String nemoUserId) throws IOException {
        int pageSize = exportRepairBlocksize;
        int index = 0;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));
        String now = simpleDateFormat.format(DateUtil.getNowDate());
        String processId = nemoUserId + "-" + "ExportClaims" + "-" + now;

        // GO IN WITH GENERATION
        for (int page = 1; page <= pagesNumber; page++) {
            List<CounterpartyEntity> counterpartyObj = counterpartyService.getCounterpartyForExportByPages(filter, page, pageSize);

            if(counterpartyObj != null) {
                for (CounterpartyEntity currentCounterParty : counterpartyObj) {
                    // inizializza la mappa per kafka
                    Map<String, Object> requestMap = sheetsBuilder.build();

                    // SHEET CALLS
                    LinkedList<Map<String, Object>> lastContactList = sheetsBuilder.get(requestMap, "calls");
                    List<LastContact> lastContacts = currentCounterParty.getLastContact();
                    LastContact lastContactRealLastByDate = null;
                    if(lastContacts != null && !lastContacts.isEmpty()) {
                        for (LastContact lastContact : lastContacts) {
                            // CHECKING REAL LAST CONTACT BY DATE
                            if(lastContactRealLastByDate == null || lastContactRealLastByDate.getLastCall().before(lastContact.getLastCall())){
                                lastContactRealLastByDate = lastContact;
                            }
                            Map<String,Object> lastContactMap = ctpMapInitializer.calls();
                            lastContactMap = ctpMapFiller.calls(lastContactMap, currentCounterParty, lastContact);
                            lastContactList.add(lastContactMap);
                        }
                    }

                    // SHEET ATTACHMENT
                    LinkedList<Map<String, Object>> attachmentList = sheetsBuilder.get(requestMap, "attachment_repair");
                    List<Attachment> attachments = currentCounterParty.getAttachments();
                    Attachment lastAttachmentRealLastByDate = null;
                    if(attachments != null && !attachments.isEmpty()) {
                        for(Attachment attachment : attachments){
                            // CHECKING REAL LAST ATTACHMENT BY DATE
                            if(lastAttachmentRealLastByDate == null || lastAttachmentRealLastByDate.getCreatedAt().before(attachment.getCreatedAt())){
                                lastAttachmentRealLastByDate = attachment;
                            }
                            Map<String,Object> attachmentMap = ctpMapInitializer.attachmentRepair();
                            attachmentMap = ctpMapFiller.attachmentRepair(attachmentMap, currentCounterParty, attachment);
                            attachmentList.add(attachmentMap);
                        }
                    }

                    // SHEET CTP
                    LinkedList<Map<String, Object>> ctpList = sheetsBuilder.get(requestMap, "ctp_repair");
                    Map<String,Object> ctpMap = ctpMapInitializer.ctpRepair();
                    ctpMap = ctpMapFiller.ctpRepair(ctpMap, currentCounterParty, lastContactRealLastByDate, lastAttachmentRealLastByDate,filter);
                    ctpList = ctpMapFiller.ctpListRepair(currentCounterParty, ctpList, ctpMap);

                    requestMap.put("ctp_repair", ctpList);
                    requestMap.put("calls", lastContactList);
                    requestMap.put("attachment_repair", attachmentList);

                    uploadOnKafka(requestMap, (index + 1) + ((page - 1) * pageSize), exportCount, kafkaTopicExportCounterparty, userEmail, null, "ExportCounterparty", processId);
                    index++;
                }
            }
        }
    }
}
