package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.entity.enumerated.ClaimsRepairEnum;
import com.doing.nemo.claims.entity.settings.ManagerEntity;
import com.doing.nemo.claims.repository.ManagerRepository;
import com.doing.nemo.claims.repository.ManagerRepositoryV1;
import com.doing.nemo.claims.service.ManagerService;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ManagerServiceImpl implements ManagerService {

    private static Logger LOGGER = LoggerFactory.getLogger(ManagerServiceImpl.class);
    @Autowired
    private ManagerRepository managerRepository;
    @Autowired
    private ManagerRepositoryV1 managerRepositoryV1;

    @Override
    public ManagerEntity insertManager(ManagerEntity managerEntity) {

        managerRepository.save(managerEntity);
        return managerEntity;
    }

    @Override
    public ManagerEntity updateManager(ManagerEntity managerEntity, UUID uuid) {
        Optional<ManagerEntity> managerEntityOld = managerRepository.findById(uuid);
        if (!managerEntityOld.isPresent()) {
            LOGGER.debug("Manager with id " + uuid + " not found");
            throw new NotFoundException("Manager with id " + uuid + " not found", MessageCode.CLAIMS_1010);
        }

        managerEntity.setId(managerEntityOld.get().getId());
        managerEntity.setTypeComplaint(managerEntityOld.get().getTypeComplaint());
        managerRepository.save(managerEntity);
        return managerEntity;
    }

    @Override
    public ManagerEntity getManager(UUID uuid) {

        if (uuid == null)
            return null;


        Optional<ManagerEntity> managerEntity = managerRepository.findById(uuid);
        if (!managerEntity.isPresent()) {
            LOGGER.debug("Manager with id " + uuid + " not found");
            throw new NotFoundException("Manager with id " + uuid + " not found", MessageCode.CLAIMS_1010);
        }

        return managerEntity.get();
    }

    @Override
    public List<ManagerEntity> getAllManager(ClaimsRepairEnum claimsRepairEnum) {
        List<ManagerEntity> managerEntities = managerRepository.searchManagerByTypeComplaint(claimsRepairEnum);

        return managerEntities;
    }


    @Override
    public ManagerEntity deleteManager(UUID uuid) {
        Optional<ManagerEntity> managerEntity = managerRepository.findById(uuid);
        if (!managerEntity.isPresent()) {
            LOGGER.debug("Manager with id " + uuid + " not found");
            throw new NotFoundException("Manager with id " + uuid + " not found", MessageCode.CLAIMS_1010);
        }

        managerRepository.delete(managerEntity.get());

        return null;
    }

    @Override
    public ManagerEntity patchActive(UUID uuid) {
        ManagerEntity managerEntity = getManager(uuid);
        if (managerEntity.getActive()) {
            managerEntity.setActive(false);
        } else managerEntity.setActive(true);

        managerRepository.save(managerEntity);
        return managerEntity;
    }

    @Override
    public List<ManagerEntity> findManagersFiltered(Integer page, Integer pageSize, ClaimsRepairEnum claimsRepairEnum, String name, String prov, String phone, String fax, String email, String externalExportId, Boolean isActive, String orderBy, Boolean asc, String address, String zipCode, String locality, String country, Long code) {
        return managerRepositoryV1.findManagersFiltered(page, pageSize, claimsRepairEnum, name, prov, phone, fax, email, externalExportId, isActive, orderBy, asc, address, zipCode, locality, country, code);
    }

    @Override
    public Long countManagerFiltered(ClaimsRepairEnum claimsRepairEnum, String name, String prov, String phone, String fax, String email, String externalExportId, Boolean isActive, String address, String zipCode, String locality, String country, Long code) {
        return managerRepositoryV1.countManagerFiltered(claimsRepairEnum, name, prov, phone, fax, email, externalExportId, isActive, address, zipCode, locality, country, code);
    }

}
