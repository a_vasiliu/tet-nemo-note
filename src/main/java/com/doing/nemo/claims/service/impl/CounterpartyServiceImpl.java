package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.adapter.AssigneeRepairAdapter;
import com.doing.nemo.claims.adapter.CounterpartyAdapter;
import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.AssignationRequestV1;
import com.doing.nemo.claims.controller.payload.request.CounterpartyProcedureRequestV1;
import com.doing.nemo.claims.controller.payload.request.IdRequestV1;
import com.doing.nemo.claims.controller.payload.response.CounterpartyResponseV1;
import com.doing.nemo.claims.controller.payload.response.PaginationStats;
import com.doing.nemo.claims.controller.payload.response.external.CounterpartyExternalResponse;
import com.doing.nemo.claims.dto.CtpExportFilter;
import com.doing.nemo.claims.entity.*;
import com.doing.nemo.claims.entity.enumerated.DTO.CounterpartyStats;
import com.doing.nemo.claims.entity.enumerated.RepairEnum.RepairProcedureEnum;
import com.doing.nemo.claims.entity.enumerated.RepairEnum.RepairStatusEnum;
import com.doing.nemo.claims.entity.jsonb.Historical;
import com.doing.nemo.claims.entity.jsonb.forms.jsonbForms.Attachment;
import com.doing.nemo.claims.entity.jsonb.repair.HistoricalCounterparty;
import com.doing.nemo.claims.repository.CounterpartyNewRepository;
import com.doing.nemo.claims.service.*;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.BadParametersException;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import com.doing.nemo.dwh.client.payload.request.EventTypeEnum;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.*;

@Service
public class CounterpartyServiceImpl implements CounterpartyService {

    @Autowired
    private CounterpartyNewRepository counterpartyNewRepository;

    @Autowired
    private CounterpartyAdapter counterpartyAdapter;

    @Autowired
    private DwhClaimsService dwhClaimsService;

    @Autowired
    private ConverterClaimsService converterClaimsService;

    @Autowired
    private AuthorityAttachmentService authorityAttachmentService;

    @Autowired
    private EligibilityCriterionChecker eligibilityCriterionChecker;

    private static Logger LOGGER = LoggerFactory.getLogger(CounterpartyServiceImpl.class);

    @Value("${dwh.call}")
    private Boolean dwhCall;

    @Autowired
    private CacheService cacheService;

    private static final String CACHENAME = "claims.";
    private static final String STATSkEY = "claimsStats::";
    private static final String STATSEVIDENCE = "evidenceStats::";
    private static final String STATSTHEFT = "theftStats::";
    private static final String STATSSTATUS = "statusStats::";
    private static final String COUNTERPARTYKEY = "counterpartyStats::";

    @Override
    public CounterpartyStats getClaimsCounterpartyStats(String nemoUserId){
        /* Retrieve Cached Dashboard */
        CounterpartyStats cached = cacheService.getCachedCounterpartyStats(CACHENAME,COUNTERPARTYKEY);

        if(cached != null){
            return cached;
        }
        CounterpartyStats counterpartyStats = counterpartyNewRepository.findCounterpartyStats(nemoUserId);
        ObjectMapper oMapper = new ObjectMapper();
        Map<String, Object> toCache = oMapper.convertValue(counterpartyStats, Map.class);

        /* Save Cache Dashboard */
        cacheService.cacheClaimsStats(toCache,CACHENAME,COUNTERPARTYKEY);
        return counterpartyStats;
    }

    public List<CounterpartyEntity> getCounterpartyPaginationFromDB (String denomination, String plate, Long practiceIdCounterparty, String statusRepair, String dateCreatedFrom,
                                                                     String dateCreatedTo, Boolean asc, String orderBy, int page, int pageSize, String addressSx, String fromCompany, String dateSxFrom, String dateSxTo, String region, String province, String lastContact, String assignedTo, String locality, String recall, String centerEmail, Boolean msaPagination){
        return CounterpartyAdapter.adptCounterpartyNewToCounterpartyOldWithClaimsEntityFullList(counterpartyNewRepository.findCounterpartyForPagination(page, pageSize, denomination, plate, practiceIdCounterparty, statusRepair, dateCreatedFrom, dateCreatedTo, asc, orderBy, addressSx, fromCompany, dateSxFrom, dateSxTo, region, province, lastContact, assignedTo, locality, recall, centerEmail, msaPagination));
    }

    //REFACTOR
    public List<CounterpartyResponseV1> getCounterpartyMsaPaginationFromDB (String denomination, String plate, Long practiceIdCounterparty, String statusRepair, String dateCreatedFrom,
                                                                            String dateCreatedTo, Boolean asc, String orderBy, int page, int pageSize, String addressSx, String fromCompany, String dateSxFrom, String dateSxTo, String region, String province, String lastContact, String assignedTo, String locality, String recall, String centerEmail, Boolean msaPagination){
        //return counterpartyRepository.findAll();
        List<CounterpartyNewEntity> counterpartyNewEntityList = counterpartyNewRepository.findCounterpartyForPagination(page, pageSize, denomination, plate, practiceIdCounterparty, statusRepair, dateCreatedFrom, dateCreatedTo, asc, orderBy, addressSx, fromCompany, dateSxFrom, dateSxTo, region, province, lastContact, assignedTo, locality, recall, centerEmail, msaPagination);
        List<CounterpartyResponseV1> counterpartyResponseV1List = new LinkedList<>();
        for(CounterpartyNewEntity currentCount : counterpartyNewEntityList){
            CounterpartyEntity counterpartyEntity = CounterpartyAdapter.adptCounterpartyNewToCounterpartyOld(currentCount);
            counterpartyEntity = authorityAttachmentService.addAuthorityAttachmentRepair(counterpartyEntity);
            CounterpartyResponseV1 counterpartyResponseV1 = counterpartyAdapter.adptCounterpartsNewToCounterpartyResponseV1ListExternal(counterpartyEntity, currentCount.getClaims() );
            counterpartyResponseV1List.add(counterpartyResponseV1);
        }


        return counterpartyResponseV1List;

    }


    public BigInteger countCounterpartyPagination(String denomination, String plate, Long practiceIdCounterparty, String statusRepair, String dateCreatedFrom,
                                                  String dateCreatedTo, Boolean asc, String orderBy, int page, int pageSize, String addressSx, String fromCompany, String dateSxFrom, String dateSxTo, String region, String province, String lastContact, String assignedTo, String locality, String recall, String centerEmail, Boolean msaPagination){
        return counterpartyNewRepository.countCounterpartyForPagination(page, pageSize, denomination, plate, practiceIdCounterparty, statusRepair, dateCreatedFrom, dateCreatedTo, asc, orderBy, addressSx, fromCompany, dateSxFrom, dateSxTo, region, province, lastContact, assignedTo, locality, recall, centerEmail, msaPagination);
    }

    public List<CounterpartyEntity> getCounterpartyPaginationWithoutToSort (String denomination, String plate, Long practiceIdCounterparty, String statusRepair, String dateCreatedFrom,
                                                                            String dateCreatedTo, Boolean asc, String orderBy, int page, int pageSize, String addressSx, String fromCompany, String dateSxFrom, String dateSxTo, String region, String province, String lastContact, String assignedTo, String locality, String recall){
        return CounterpartyAdapter.adptCounterpartyNewToCounterpartyOldList(counterpartyNewRepository.findCounterpartyForPaginationWithoutToSort(page, pageSize, denomination, plate, practiceIdCounterparty, statusRepair, dateCreatedFrom, dateCreatedTo, asc, orderBy, addressSx, fromCompany, dateSxFrom, dateSxTo, region, province, lastContact, assignedTo, locality, recall));
    }

    public BigInteger countCounterpartyPaginationWithoutToSort(String denomination, String plate, Long practiceIdCounterparty, String statusRepair, String dateCreatedFrom,
                                                  String dateCreatedTo, Boolean asc, String orderBy, int page, int pageSize, String addressSx, String fromCompany, String dateSxFrom, String dateSxTo, String region, String province, String lastContact, String assignedTo, String locality, String recall){
        return counterpartyNewRepository.countCounterpartyForPaginationWithoutToSort(page, pageSize, denomination, plate, practiceIdCounterparty, statusRepair, dateCreatedFrom, dateCreatedTo, asc, orderBy, addressSx, fromCompany, dateSxFrom, dateSxTo, region, province, lastContact, assignedTo, locality, recall);
    }

    public Pagination<CounterpartyResponseV1> generateCounterpartyPagination(List<CounterpartyResponseV1> counterpartyResponseV1s, BigInteger count, int page, int pageSize) {

        Pagination<CounterpartyResponseV1> counterpartyPagination = new Pagination<>();

        counterpartyPagination.setStats(new PaginationStats(count.intValue(), page, pageSize));

        counterpartyPagination.setItems(counterpartyResponseV1s);

        return counterpartyPagination;
    }

    //REFACTOR
    @Override
    public void EligibilityCriterion(ClaimsEntity claims, String userId, String userName) {
        LOGGER.info("[Counterparty] Eligibility criterion");
        if( eligibilityCriterionChecker.isClaimsEligible(claims) ) {
            LOGGER.info("[Counterparty] into if eligibility");
            CounterpartyEntity counterparty = claims.getCounterparts().get(0);
            counterparty
                    .setEligibility(true)
                    .setRepairStatus(RepairStatusEnum.TO_SORT)
                    .setRepairCreatedAt(DateUtil.getNowInstant())
            ;
            String smallDescription= "Creazione Pratica di Repair";
            String description = "Creazione Pratica di Repair";
            HistoricalCounterparty historicalCounterparty = new HistoricalCounterparty(null, RepairStatusEnum.TO_SORT, smallDescription, DateUtil.getNowDate(), userId,userName, description, com.doing.nemo.claims.entity.enumerated.EventTypeEnum.COMPLAINT_INSERTION_REPAIR);
            if(counterparty.getHistoricals() == null){
                counterparty.setHistoricals(new ArrayList<>());
            }
			counterparty.addHistorical(historicalCounterparty);

            counterpartyNewRepository.save(CounterpartyAdapter.adptCounterpartyOldToCounterpartyNew(counterparty));
            dwhClaimsService.sendMessage(counterparty, EventTypeEnum.CREATED);

        }
    }

    @Override
    @Transactional
    public void assignsCounterparty(List<AssignationRequestV1> assignationRequestV1s,String userid, String username){
        for(AssignationRequestV1 currentAssignation : assignationRequestV1s){
            if(currentAssignation.getCounterpartyId() != null) {
                Optional<CounterpartyNewEntity> counterpartsOptional = counterpartyNewRepository.findById(currentAssignation.getCounterpartyId());
                if(counterpartsOptional.isPresent()) {
                    CounterpartyNewEntity counterparty = counterpartsOptional.get();
                    String oldAssignedUserName = "";
                    if (counterparty.getAssignedTo()!=null&&counterparty.getAssignedTo().getUsername()!=null)
                        oldAssignedUserName = counterparty.getAssignedTo().getUsername();

                    counterparty.setAssignedTo(AssigneeRepairAdapter.adptFromAssignationRepairRequestToAssignationRepair(currentAssignation.getAssignee()));

                    if(counterparty.getAssignedTo() != null) {
                        counterparty.setAssignedAt(DateUtil.getNowInstant());
                        StringBuilder description = new StringBuilder();
                        description.append("Vecchio Assegnatario : ").append(oldAssignedUserName).append("<br>");
                        description.append("Nuovo Assegnatario : ").append(counterparty.getAssignedTo().getUsername()).append("<br>");
                        description.append("Data di Assegnazione : ").append(counterparty.getAssignedAt());

                        String smallDescription = "Assegnazione";
                        HistoricalCounterparty historicalCounterparty = new HistoricalCounterparty(counterparty.getRepairStatus(), RepairStatusEnum.TO_CONTACT, smallDescription, DateUtil.getNowDate(), userid, username, description.toString(), com.doing.nemo.claims.entity.enumerated.EventTypeEnum.OPERATOR_ASSIGNMENT_REPAIR);
                        if(counterparty.getHistoricals() == null){
                             counterparty.setHistoricals(new ArrayList<>());
                        }
						counterparty.addHistorical(historicalCounterparty);
                        counterparty.setRepairStatus(RepairStatusEnum.TO_CONTACT);
                        if(counterparty.getIsReadMsa() != null && counterparty.getIsReadMsa()){
                            counterparty.setIsReadMsa(false);
                        }
                        counterpartyNewRepository.save(counterparty);
                        if(dwhCall){
                            dwhClaimsService.sendMessage(CounterpartyAdapter.adptCounterpartyNewToCounterpartyOld(counterparty), EventTypeEnum.UPDATED);
                        }
                    }

                }
            }
        }
    }

    public CounterpartyEntity findById(String countepartyId){
        Optional<CounterpartyNewEntity> counterparty = counterpartyNewRepository.findById(countepartyId);
        if(!counterparty.isPresent()){
            LOGGER.debug("Counterparty with UUID " + countepartyId + " not found ");
            throw new NotFoundException("Counterparty with UUID " + countepartyId + " not found ", MessageCode.CLAIMS_1010);
        }
        return CounterpartyAdapter.adptCounterpartyNewToCounterpartyOldWithClaimsEntityFull(counterparty.get());
    }

    public CounterpartyNewEntity updateCounterparts(CounterpartyNewEntity counterparty){
        if(counterparty == null) return null;
        if(dwhCall)
        {
            dwhClaimsService.sendMessage(CounterpartyAdapter.adptCounterpartyNewToCounterpartyOld(counterparty), EventTypeEnum.UPDATED);
        }
        return counterpartyNewRepository.save(counterparty);
    }

    //REFACTOR
    public Pagination<CounterpartyEntity> getCounterpartyPaginationAuthority(String plate, String chassis, int page, int pageSize) {
        try {

            Pagination<CounterpartyEntity> counterpartyEntityPagination = new Pagination<>();
            //recupero della nuova entità
            List<CounterpartyNewEntity> counterpartyNewEntities = counterpartyNewRepository.findCounterpartyForPaginationAuthority(plate, chassis, page, pageSize);
            //conversione nella vecchia entità
            List<CounterpartyEntity> counterpartyEntities = CounterpartyAdapter.adptCounterpartyNewToCounterpartyOldList(counterpartyNewEntities);
            BigInteger itemCount = counterpartyNewRepository.countCounterpartyForPaginationAuthority(plate, chassis);

            //Mi serve necessariemante per calcolare la paina di start
            counterpartyEntityPagination.setStats(new PaginationStats(itemCount.intValue(), page, pageSize));
            counterpartyEntityPagination.setItems(counterpartyEntities);

            return counterpartyEntityPagination;

        } catch (BadParametersException ex) {
            LOGGER.debug(ex.getMessage());
            throw ex;
        } catch (BadRequestException ex) {
            LOGGER.debug("There are problems with the incoming Date_accident, the correct format is DD-MM-YYYY");
            throw new BadRequestException(MessageCode.CLAIMS_1011, ex);
        }
    }

    public CounterpartyEntity patchProcedureRepair(RepairProcedureEnum procedure, String counterpartyId, CounterpartyProcedureRequestV1 motivationRequest){
        Optional<CounterpartyNewEntity> counterpartyEntityOptional = counterpartyNewRepository.findById(counterpartyId);
        if(!counterpartyEntityOptional.isPresent()){
            throw new BadRequestException(MessageCode.CLAIMS_1010);
        }
        CounterpartyNewEntity counterpartyEntity = counterpartyEntityOptional.get();
        counterpartyEntity.setMotivation(motivationRequest.getMotivation());
        counterpartyEntity.setRepairProcedure(procedure);

        counterpartyNewRepository.save(counterpartyEntity);
        CounterpartyEntity oldCounterparty = CounterpartyAdapter.adptCounterpartyNewToCounterpartyOld(counterpartyEntity);
        if(dwhCall){
            dwhClaimsService.sendMessage(oldCounterparty, EventTypeEnum.UPDATED);
        }

        return oldCounterparty;

    }

    @Override
    public List<CounterpartyExternalResponse> getCounterpartySetMsaReadResponse(List<IdRequestV1> counterpartyIdList) {
        List<CounterpartyExternalResponse> counterpartyExternalResponses = new ArrayList<>();
        for (IdRequestV1 repairIdRequestV1 : counterpartyIdList) {
            Optional<CounterpartyNewEntity> counterpartyEntityOptional = counterpartyNewRepository.findById(repairIdRequestV1.getId());
            if (!counterpartyEntityOptional.isPresent()) {
                LOGGER.debug("Repair with UUID " + repairIdRequestV1.getId() + " not found ");
                throw new NotFoundException("Repair with UUID " + repairIdRequestV1.getId() + " not found ");
            }
            CounterpartyEntity counterpartyEntity = CounterpartyAdapter.adptCounterpartyNewToCounterpartyOld(counterpartyEntityOptional.get());
            counterpartyExternalResponses.add(CounterpartyAdapter.adptCounterpartyToCounterpartyExternalResponse(counterpartyEntity));
        }

        return counterpartyExternalResponses;
    }

    public ClaimsEntity patchStatusHiddenAttachmentCounterpart (String idCounterpart, String idAttachments){
        Optional<CounterpartyNewEntity>  counterpartyOpt = counterpartyNewRepository.findById(idCounterpart);
        if(!counterpartyOpt.isPresent()){
            LOGGER.debug("Repair with UUID " + idCounterpart + " not found ");
            throw new NotFoundException("Repair with UUID " + idCounterpart + " not found ");
        }
        CounterpartyNewEntity counterparty = counterpartyOpt.get();
        ClaimsEntity claimsEntity = null;
        List<String> attachmentList = new ArrayList<>();
        String[] attachments = idAttachments.split(",");
        for (String idAttachment : attachments) {
            attachmentList.add(idAttachment);
        }
        if (counterparty.getAttachments() != null) {
            for (Attachment attachment : counterparty.getAttachments()) {
                if (attachmentList.contains(attachment.getFileManagerId())) {
                    attachment.setHidden(!attachment.getHidden());
                }
            }
        }
        if(counterparty.getIsReadMsa() != null && counterparty.getIsReadMsa()){
            counterparty.setIsReadMsa(false);
        }
        counterparty = this.updateCounterparts(counterparty);
        if(counterparty.getClaims() != null){
            claimsEntity = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(counterparty.getClaims());
        }
        return claimsEntity;
    }

    /*
    * Verifico che una controparte la cui lavorazione è su Websin Repair non venga modificata su NEMO.
    * Semplice controllo d'integrità ma è una casistica che non capiterà mai.*/
    public void isWorkable (CounterpartyNewEntity counterpartyEntity){
        if(counterpartyEntity!=null && counterpartyEntity.isWebsinRepair()){
            LOGGER.info(MessageCode.CLAIMS_2012.value());
            throw new BadRequestException(MessageCode.CLAIMS_2012);
        }
    }

    public BigInteger countCounterpartyForExport (CtpExportFilter filter){
        return counterpartyNewRepository.countCounterpartyForExport(filter);
    }

    public List<CounterpartyEntity> getCounterpartyForExportByPages (CtpExportFilter filter, int page, int pageSize){
        List<CounterpartyNewEntity> counterpartyNewEntities = counterpartyNewRepository.findCounterpartyForExportByPages(filter, page, pageSize);
        return CounterpartyAdapter.adptCounterpartyNewToCounterpartyOldWithClaimsEntityFullList(counterpartyNewEntities);
    }


    //REFACTOR
    @Override
    public List<HistoricalCounterparty> getCounterpartyHistorical(String counterpartyId) {
        //recupero nella nuova entità
        Optional<CounterpartyNewEntity> counterpartyNewEntityOptional = counterpartyNewRepository.findById(counterpartyId);
        if (!counterpartyNewEntityOptional.isPresent()) {
            LOGGER.debug("Counterparty not found");
            throw new NotFoundException("Counterparty not found", MessageCode.CLAIMS_1010);
        }

        CounterpartyNewEntity counterpartyNewEntity = counterpartyNewEntityOptional.get();
        System.out.println(counterpartyNewEntity.getPracticeIdCounterparty());
        //non c'è necessità di convertire nel vecchio historical resta un jsonb
        return counterpartyNewEntity.getHistoricals();
    }

}
