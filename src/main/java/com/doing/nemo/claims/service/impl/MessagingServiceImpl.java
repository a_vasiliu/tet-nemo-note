package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.adapter.AntiTheftServiceAdapter;
import com.doing.nemo.claims.adapter.CounterpartyAdapter;
import com.doing.nemo.claims.adapter.MessagingAdapter;
import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.*;
import com.doing.nemo.claims.controller.payload.request.messaging.Identity;
import com.doing.nemo.claims.controller.payload.response.DogeResponseV1;
import com.doing.nemo.claims.controller.payload.response.EmailTemplateInternalMessagingResponseV1;
import com.doing.nemo.claims.controller.payload.response.EmailTemplateMessagingResponseV1;
import com.doing.nemo.claims.controller.payload.response.NemoAuthExternalResposeV1;
import com.doing.nemo.claims.entity.*;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsFlowEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.EntrustedEnum.EntrustedEnum;
import com.doing.nemo.claims.entity.enumerated.EmailTemplateEnum;
import com.doing.nemo.claims.entity.enumerated.EventTypeEnum;
import com.doing.nemo.claims.entity.enumerated.RecipientEnum;
import com.doing.nemo.claims.entity.jsonb.Address;
import com.doing.nemo.claims.entity.jsonb.Historical;
import com.doing.nemo.claims.entity.jsonb.complaint.Complaint;
import com.doing.nemo.claims.entity.jsonb.complaint.jsonbComplaint.Entrusted;
import com.doing.nemo.claims.entity.jsonb.complaint.jsonbComplaint.FromCompany;
import com.doing.nemo.claims.entity.jsonb.complaint.jsonbComplaint.jsonbDataAccidents.DataAccident;
import com.doing.nemo.claims.entity.jsonb.damaged.Damaged;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.FleetManager;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.ImpactPoint;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.InsuranceCompany;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.Insured;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbAntiTheftService.AntiTheftService;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbDriver.Driver;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbInsuranceCompany.LegalCost;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbInsuranceCompany.Pai;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbInsuranceCompany.Tpl;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbVehicle.Vehicle;
import com.doing.nemo.claims.entity.jsonb.personalData.FleetManagerPersonalData;
import com.doing.nemo.claims.entity.jsonb.repair.Canalization;
import com.doing.nemo.claims.entity.jsonb.repair.HistoricalCounterparty;
import com.doing.nemo.claims.entity.settings.*;
import com.doing.nemo.claims.repository.*;
import com.doing.nemo.claims.service.*;
import com.doing.nemo.claims.util.Util;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.AbstractException;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.exception.InternalException;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.commons.util.HttpUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.Response;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.io.IOException;
import java.time.Instant;
import java.util.*;

@Service
public class MessagingServiceImpl implements MessagingService {

    @Autowired
    private HttpUtil httpUtil;
    @Autowired
    private ClaimsRepository claimsRepository;

    @Autowired
    private ClaimsNewRepository claimsNewRepository;

    @Autowired
    private ConverterClaimsService converterClaimsService;

    @Autowired
    private CounterpartyRepository counterpartyRepository;

    @Autowired
    private CounterpartyNewRepository counterpartyNewRepository;

    @Autowired
    private EmailTemplateRepositoryV1 emailTemplateRepositoryV1;
    @Autowired
    private LegalRepository legalRepository;
    @Autowired
    private InspectorateRepository inspectorateRepository;
    @Autowired
    private BrokerRepository brokerRepository;
    @Autowired
    private ManagerRepository managerRepository;
    @Autowired
    private InsuranceCompanyRepository insuranceCompanyRepository;
    @Autowired
    private AntiTheftServiceRepository antiTheftServiceRepository;
    @Autowired
    private DogeEnquService dogeEnquService;
    @Autowired
    private FileManagerService fileManagerService;
    @Autowired
    private PersonalDataRepository personalDataRepository;
    @Autowired
    private InsurancePolicyRepository insurancePolicyRepository;

    @Value("${messaging.api.endpoint}")
    private String endpointUrl;
    @Value("${messaging.api.mail}")
    private String mail;
    @Value("${messaging.api.template}")
    private String template;

    @Value("${policy.code.base}")
    private String policyCodeBase;

    @Value("${policy.code.plus}")
    private String policyCodePlus;

    @Value("${pdf.base.id}")
    private String pdfBaseId;

    @Value("${pdf.plus.id}")
    private String pdfPlusId;

    @Value("${legal.msa.email}")
    private String legalMsaEmail;

    @Value("${legal.msa.name}")
    private String managerMsa;

    @Autowired
    private NemoAuthService nemoAuthService;

    @Autowired
    private MyAldService myAldService;

    @Value("${acclaims.id}")
    private String acclaimsId;

    @Value("${acclaims.sr.id}")
    private String acclaimsSrId;

    @Value("${enjoy.retry.number}")
    private Integer retryNumber;

    private static Logger LOGGER = LoggerFactory.getLogger(MessagingServiceImpl.class);

    private <T, P> T callWithoutCert(String url, HttpMethod method, P value, HashMap<String, String> headersparams, Class<T> outclass) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = null;
        Response responseToken = null;
        try {
            if (value != null) {
                jsonInString = mapper.writeValueAsString(value);
            }
            responseToken = httpUtil.callURL(headersparams, url, jsonInString, method);


            if (!responseToken.isSuccessful()) {
                LOGGER.debug(responseToken.message() + " with code " + responseToken.code());
                throw new BadRequestException(responseToken.message() + " with code " + responseToken.code(), MessageCode.CLAIMS_2000);
            }

            return outclass.cast(HttpStatus.valueOf(responseToken.code()));
        } catch (AbstractException e) {
            LOGGER.error(e.getMessage(), e);
            throw e;
        }  catch (Exception e) {
            String message = String.format(MessageCode.CLAIMS_2012.value(), e.getClass().getSimpleName(), e.getMessage());
            LOGGER.error(message, e);
            throw new InternalException(message, MessageCode.CLAIMS_2012);
        } finally {
            if (responseToken != null && responseToken.body() != null) {
                responseToken.body().close();
            }
        }
    }

    @Override
    public HttpStatus sendMail(SendMailRequestV1 sendMailRequest) throws IOException {
        return callWithoutCert(endpointUrl + mail, HttpMethod.POST, sendMailRequest, null, HttpStatus.class);
    }

    @Override
    public HttpStatus sendMailWithTemplate(SendMailTemplateRequestV1 sendMailTemplateRequestV1) throws IOException {
        return callWithoutCert(endpointUrl + mail, HttpMethod.POST, sendMailTemplateRequestV1, null, HttpStatus.class);
    }


    @Override
    public HttpStatus sendEmailEnjoy(List<EmailTemplateMessagingRequestV1> emailTemplateMessagingRequestList, String motivation, String claimsId) {
        Integer incidentRetryNumber = 0;
        Boolean emailIsOk = false;

        while(incidentRetryNumber < retryNumber && !emailIsOk) {
            try {

                for (EmailTemplateMessagingRequestV1 att : emailTemplateMessagingRequestList) {

                    SendMailRequestV1 sendMailRequest = MessagingAdapter.adptEmailTemplateMessagingRequestToSendMailRequest(att);

                    if (motivation != null) {
                        sendMailRequest.setBodyContent(sendMailRequest.getBodyContent().replace("<%REATYPDESTY%>", motivation));
                        if(sendMailRequest.getBodyContent().contains("<%REATYPDESTYP%>") && sendMailRequest.getBodyContent().contains("<%REASDESCCOMP%>")) {
                            sendMailRequest.setBodyContent(sendMailRequest.getBodyContent().replace("<%REATYPDESTYP%>", motivation));
                            sendMailRequest.setBodyContent(sendMailRequest.getBodyContent().replace("<%REASDESCCOMP%>", ""));
                        }else{
                            sendMailRequest.setBodyContent(sendMailRequest.getBodyContent().replace("<%REATYPDESTYP%>", motivation));
                            sendMailRequest.setBodyContent(sendMailRequest.getBodyContent().replace("<%REASDESCCOMP%>", motivation));
                        }
                    }

                    if (att.getSplittingRecipientsEmail() != null && att.getSplittingRecipientsEmail()) {
                        for (Identity identity : att.getTos()) {

                            sendMailRequest.setTos(new ArrayList<Identity>() {{
                                add(identity);
                            }});
                            this.sendMail(sendMailRequest);
                            emailIsOk = true;
                        }
                    } else {
                        callWithoutCert(endpointUrl + mail, HttpMethod.POST, sendMailRequest, null, HttpStatus.class);
                        this.sendMail(sendMailRequest);
                        emailIsOk = true;

                    }
                }

            } catch (Exception e) {
                LOGGER.debug(" Retry number mail " + incidentRetryNumber, e);
                incidentRetryNumber = incidentRetryNumber+1;
            }
        }


        if(incidentRetryNumber.equals(retryNumber)){
            LOGGER.debug("[ENJOY] Send mail failed on claim's id " + claimsId);
            LOGGER.criticalError("NEMO_CLAIMS","[ENJOY] Send mail failed on claim's id " +claimsId, null );
            throw new InternalException("Enjoy send Email Failed on claim's id " + claimsId);
        }
        return HttpStatus.ACCEPTED;
    }


    @Override
    public HttpStatus sendEmail(List<EmailTemplateMessagingRequestV1> emailTemplateMessagingRequestList, String motivation , Instant createdAt) {

        try {

            for (EmailTemplateMessagingRequestV1 att : emailTemplateMessagingRequestList) {

                SendMailRequestV1 sendMailRequest = MessagingAdapter.adptEmailTemplateMessagingRequestToSendMailRequest(att);

                if (motivation != null) {
                    sendMailRequest.setBodyContent(sendMailRequest.getBodyContent().replace("<%REATYPDESTY%>", motivation));
                    if(sendMailRequest.getBodyContent().contains("<%REATYPDESTYP%>") && sendMailRequest.getBodyContent().contains("<%REASDESCCOMP%>")) {
                        sendMailRequest.setBodyContent(sendMailRequest.getBodyContent().replace("<%REATYPDESTYP%>", motivation));
                        sendMailRequest.setBodyContent(sendMailRequest.getBodyContent().replace("<%REASDESCCOMP%>", ""));
                    }else{
                        sendMailRequest.setBodyContent(sendMailRequest.getBodyContent().replace("<%REATYPDESTYP%>", motivation));
                        sendMailRequest.setBodyContent(sendMailRequest.getBodyContent().replace("<%REASDESCCOMP%>", motivation));
                    }

                } else {
                    sendMailRequest.setBodyContent(sendMailRequest.getBodyContent().replace("<%REATYPDESTY%>", ""));
                    sendMailRequest.setBodyContent(sendMailRequest.getBodyContent().replace("<%REATYPDESTYP%>", ""));
                    sendMailRequest.setBodyContent(sendMailRequest.getBodyContent().replace("<%REASDESCCOMP%>", ""));
                }

                if(createdAt!=null)
                    sendMailRequest.setBodyContent(sendMailRequest.getBodyContent().replace("<%REICDATECOMP%>", DateUtil.getDateStringWithSeparationCharacters(DateUtil.convertIS08601StringToUTCDate(DateUtil.convertUTCInstantToIS08601String(createdAt))) ));

                if (att.getSplittingRecipientsEmail() != null && att.getSplittingRecipientsEmail()) {
                    for (Identity identity : att.getTos()) {

                        sendMailRequest.setTos(new ArrayList<Identity>() {{
                            add(identity);
                        }});
                        this.sendMail(sendMailRequest);
                    }
                } else {
                    this.sendMail(sendMailRequest);

                }
            }

        } catch (IOException e) {
            LOGGER.debug(e.getMessage());
        }
        return HttpStatus.ACCEPTED;
    }


    //REFACTOR
    @Transactional
    @Override
    public HttpStatus sendAndLogEmail(MessagingSendMailRequestV1 messagingSendMailRequestV1, String motivation, String idClaim, String idCounterparty, String user, String username) {
        List<EmailTemplateMessagingRequestV1> emailTemplateMessagingRequestList = messagingSendMailRequestV1.getTemplateList();
        //recupero della nuova entità
        Optional<ClaimsNewEntity> claimsNewEntityOptional = claimsNewRepository.findById(idClaim);

        if (!claimsNewEntityOptional.isPresent()) {
            LOGGER.debug("Claims not found");
            throw new NotFoundException("Claims not found", MessageCode.CLAIMS_1010);
        }
        //conversione nella vecchia entità
        ClaimsEntity claimsEntity = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntityOptional.get());

        String description = this.sendMailAndCreateLogs(emailTemplateMessagingRequestList, motivation, claimsEntity.getCreatedAt());
        //if(!claimsEntity.getWithCounterparty() && idCounterparty != null)  throw new NotFoundException("Counterparty not found", MessageCode.CLAIMS_1010);
        if(idCounterparty != null) {
            //recupero della vecchia entità
            Optional<CounterpartyNewEntity> counterpartyNewEntityOptional = counterpartyNewRepository.findById(idCounterparty);
            if(!counterpartyNewEntityOptional.isPresent()){
                throw new NotFoundException("Counterparty not found", MessageCode.CLAIMS_1010);
            }

            //conversione nella nuova entità
            CounterpartyEntity counterparty = CounterpartyAdapter.adptCounterpartyNewToCounterpartyOld(counterpartyNewEntityOptional.get());

            HistoricalCounterparty historicalCounterparty = new HistoricalCounterparty(counterparty.getRepairStatus(), counterparty.getRepairStatus(), "Invio email", DateUtil.getNowDate(), user, username, description, messagingSendMailRequestV1.getEventType());
            if(counterparty.getHistoricals() == null){
                counterparty.setHistoricals(new ArrayList<>());
            }
            List<HistoricalCounterparty> historicalCounterparties = counterparty.getHistoricals();
            historicalCounterparties.add(historicalCounterparty);
            counterparty.setHistoricals(historicalCounterparties);
            //convrsione nella nuova entità
            CounterpartyNewEntity counterpartyNewEntity = CounterpartyAdapter.adptCounterpartyOldToCounterpartyNew(counterparty);
            //salvataggio della nuova entità
            counterpartyNewRepository.save(counterpartyNewEntity);
        } else {
            Historical historical = new Historical(null, claimsEntity.getStatus(), claimsEntity.getStatus(), claimsEntity.getComplaint().getDataAccident().getTypeAccident(), claimsEntity.getComplaint().getDataAccident().getTypeAccident(), DateUtil.getNowDate(), user, username, claimsEntity.getType().getValue(), claimsEntity.getType().getValue(), description);
            if (claimsEntity.getHistorical() == null) {
                claimsEntity.setHistorical(new ArrayList<>());
            }
            List<Historical> historicalList = claimsEntity.getHistorical();
            historicalList.add(historical);
            claimsEntity.setHistorical(historicalList);
            //conversione nella nuova entità
            ClaimsNewEntity claimsNewEntity = converterClaimsService.convertOldEntityToNewClaimsEntity(claimsEntity);
            //salvataggio della nuova entità
            claimsNewRepository.save(claimsNewEntity);
        }
        return HttpStatus.ACCEPTED;
    }

    public HttpStatus sendEmail(List<EmailTemplateMessagingRequestV1> emailTemplateMessagingRequestList, String motivation, List<String> attachment, Instant createdAt) {

        try {

            for (EmailTemplateMessagingRequestV1 att : emailTemplateMessagingRequestList) {

                SendMailRequestV1 sendMailRequest = MessagingAdapter.adptEmailTemplateMessagingRequestToSendMailRequest(att);
                sendMailRequest.setFilemanagerIdAttachmentList(attachment);
                sendMailRequest.setBodyContent(sendMailRequest.getBodyContent().replace("<%REICDATECOMP%>", DateUtil.getDateStringWithSeparationCharacters(DateUtil.convertIS08601StringToUTCDate(DateUtil.convertUTCInstantToIS08601String(createdAt))) ));
                if (motivation != null) {
                    sendMailRequest.setBodyContent(sendMailRequest.getBodyContent().replace("<%REATYPDESTY%>", motivation));
                    if(sendMailRequest.getBodyContent().contains("<%REATYPDESTYP%>") && sendMailRequest.getBodyContent().contains("<%REASDESCCOMP%>")) {
                        sendMailRequest.setBodyContent(sendMailRequest.getBodyContent().replace("<%REATYPDESTYP%>", motivation));
                        sendMailRequest.setBodyContent(sendMailRequest.getBodyContent().replace("<%REASDESCCOMP%>", ""));
                    }else{
                        sendMailRequest.setBodyContent(sendMailRequest.getBodyContent().replace("<%REATYPDESTYP%>", motivation));
                        sendMailRequest.setBodyContent(sendMailRequest.getBodyContent().replace("<%REASDESCCOMP%>", motivation));
                    }
                } else {
                    sendMailRequest.setBodyContent(sendMailRequest.getBodyContent().replace("<%REATYPDESTY%>", ""));
                    sendMailRequest.setBodyContent(sendMailRequest.getBodyContent().replace("<%REATYPDESTYP%>", ""));
                    sendMailRequest.setBodyContent(sendMailRequest.getBodyContent().replace("<%REASDESCCOMP%>", ""));
                }

                if (att.getSplittingRecipientsEmail() != null && att.getSplittingRecipientsEmail()) {
                    for (Identity identity : att.getTos()) {

                        sendMailRequest.setTos(new ArrayList<Identity>() {{
                            add(identity);
                        }});
                        this.sendMail(sendMailRequest);
                    }
                } else {
                    this.sendMail(sendMailRequest);
                }
            }

        } catch (IOException e) {
            LOGGER.debug(e.getMessage());
        }
        return HttpStatus.ACCEPTED;
    }

    //REFACTOR
    @Override
    public HttpStatus sendEmailEntrustPai(List<EmailTemplateMessagingRequestV1> emailTemplateMessagingRequestList, String claimsId, String userId, String userName) throws IOException {
        DogeResponseV1 dogeResponseV1 = null;
        try {
            //funzione doge refactorizzata
            dogeResponseV1 = dogeEnquService.enqueueClaimsPAI(claimsId);
        } catch (IOException e) {
            LOGGER.debug(MessageCode.CLAIMS_1055.value());
            throw new BadRequestException(MessageCode.CLAIMS_1055);
        }
        if (dogeResponseV1 == null) {
            LOGGER.debug(MessageCode.CLAIMS_1056.value());
            throw new BadRequestException(MessageCode.CLAIMS_1056);
        }

        //funzione refactorizzata
        fileManagerService.attachPdfFileClaim(claimsId, dogeResponseV1.getDocumentId(), userId, userName, true, false);


        List<String> attachment = new ArrayList<>();
        attachment.add(dogeResponseV1.getDocumentId());

        HttpStatus responseStatus = HttpStatus.BAD_REQUEST;

        if(emailTemplateMessagingRequestList != null && !emailTemplateMessagingRequestList.isEmpty()) {
            List<EmailTemplateMessagingRequestV1> listTos = new LinkedList<>();
            List<EmailTemplateMessagingRequestV1> listNotTos = new LinkedList<>();
            for (EmailTemplateMessagingRequestV1 currentTemplate : emailTemplateMessagingRequestList) {
                if (currentTemplate.getTos() != null && !currentTemplate.getTos().isEmpty() && currentTemplate.getObject()!= null && !currentTemplate.getObject().isEmpty()) {
                    listTos.add(currentTemplate);
                } else {
                    listNotTos.add(currentTemplate);
                }
            }

            //funzione già convertità
            List<EmailTemplateMessagingRequestV1> emailTemplateMessaging = splitEmailIfContainsUrl(listTos, claimsId);
            responseStatus = this.sendEmail(emailTemplateMessaging, null, attachment, null);
            emailTemplateMessaging.addAll(listNotTos);


            if (responseStatus.equals(HttpStatus.ACCEPTED)) {

                //Recupero della nuova entità
                Optional<ClaimsNewEntity> claimsNewEntityOptional = claimsNewRepository.findById(claimsId);
                if (claimsNewEntityOptional.isPresent()) {
                    //conversione nella vecchia entità
                    ClaimsEntity claims = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntityOptional.get());
                    String description = "";

                    description = this.createLogs(emailTemplateMessaging, null);

                    Historical historical = new Historical(EventTypeEnum.ENTRUST_PAI, claims.getStatus(), claims.getStatus(), claims.getComplaint().getDataAccident().getTypeAccident(), claims.getComplaint().getDataAccident().getTypeAccident(), new Date(), userId, userName, claims.getType().getValue(), claims.getType().getValue(), description);
                    claims.addHistorical(historical);
                    claims.setPaiComunication(true);

                    //converto nella nuova entità


                    ClaimsNewEntity claimsNewEntity = converterClaimsService.convertOldEntityToNewClaimsEntity(claims);
                    claimsNewRepository.save(claimsNewEntity);
                }
            }

        }
        return responseStatus;
    }


    @Override
    public HttpStatus sendEmailEntrustPaiEnjoy(List<EmailTemplateMessagingRequestV1> emailTemplateMessagingRequestList, String claimsId, String userId, String userName) throws IOException {
        DogeResponseV1 dogeResponseV1 = null;
        Boolean dogeIsOk = false;
        Integer dogeRetryNumber = 0;
        Exception exceptionDoge = null;

        Boolean fileManagerIsOk = false;
        Integer fileManagerRetryNumber = 0;
        Exception exceptionFileManager = null;

        Boolean messaggingIsOk = false;
        Integer messaggingRetryNumber = 0;
        Exception exceptionMessaging = null;

        while(dogeRetryNumber < retryNumber && !dogeIsOk) {
            try {
                dogeResponseV1 = dogeEnquService.enqueueClaimsPAI(claimsId);
                LOGGER.debug("[ENJOY] DocumentGenerator PAI OK at attempt number "+(dogeRetryNumber+1));
                dogeIsOk = true;
            } catch (Exception e) {
                LOGGER.debug(MessageCode.CLAIMS_1055.value());
                LOGGER.debug("[ENJOY] DocumentGenerator PAI  NOT OK at attempt number "+(dogeRetryNumber+1));
                dogeRetryNumber = dogeRetryNumber+1;
                exceptionDoge = e;
            }
        }

        if (dogeResponseV1 == null || !dogeIsOk) {

            LOGGER.debug("[ENJOY] DocumentGenerator PAI failed on claim's id " + claimsId + " - " + MessageCode.CLAIMS_1056.value());
            LOGGER.criticalError("NEMO_CLAIMS","[ENJOY] DocumentGenerator PAI failed on failed on claim's id " + claimsId +  " - " + MessageCode.CLAIMS_1056.value(), exceptionDoge );
            //throw new BadRequestException(MessageCode.CLAIMS_1056);
            return null;
        }



        while(fileManagerRetryNumber < retryNumber && !fileManagerIsOk) {
            try {

                fileManagerService.attachPdfFileClaim(claimsId, dogeResponseV1.getDocumentId(), userId, userName, true, false);
                LOGGER.debug("[ENJOY] Filemanager PAI OK at attempt number "+(fileManagerRetryNumber+1));
                fileManagerIsOk = true;
            }catch (Exception e) {
                LOGGER.debug(MessageCode.CLAIMS_1055.value());
                LOGGER.debug("[ENJOY] Filemanager PAI  NOT OK at attempt number "+(fileManagerRetryNumber+1));
                fileManagerRetryNumber = fileManagerRetryNumber+1;
                exceptionFileManager = e;
            }
        }

        if(!fileManagerIsOk){
            LOGGER.debug("[ENJOY] Filemanager PAI failed on claim's id " + claimsId);
            LOGGER.criticalError("NEMO_CLAIMS","[ENJOY] Filemanager PAI failed on claim's id " + claimsId, exceptionFileManager );
            return null;
        }

        List<String> attachment = new ArrayList<>();
        attachment.add(dogeResponseV1.getDocumentId());

        HttpStatus responseStatus = null;

        if(emailTemplateMessagingRequestList != null && !emailTemplateMessagingRequestList.isEmpty()) {
            List<EmailTemplateMessagingRequestV1> listTos = new LinkedList<>();
            List<EmailTemplateMessagingRequestV1> listNotTos = new LinkedList<>();
            for (EmailTemplateMessagingRequestV1 currentTemplate : emailTemplateMessagingRequestList) {
                if (currentTemplate.getTos() != null && !currentTemplate.getTos().isEmpty()) {
                    listTos.add(currentTemplate);
                } else {
                    listNotTos.add(currentTemplate);
                }
            }


            List<EmailTemplateMessagingRequestV1> emailTemplateMessaging = splitEmailIfContainsUrl(listTos, claimsId);
            emailTemplateMessaging.addAll(listNotTos);

            while(messaggingRetryNumber < retryNumber && !messaggingIsOk){
                try {
                    responseStatus = this.sendEmail(listTos, null, attachment, null);
                    LOGGER.debug("[ENJOY] Messaging PAI OK at attempt number "+(messaggingRetryNumber+1));
                    responseStatus = this.sendEmail(emailTemplateMessaging, null, attachment, null);
                    LOGGER.info("[ENJOY] Messaging PAI OK at attempt number "+(messaggingRetryNumber+1));
                    messaggingIsOk= true;
                }catch (Exception e) {
                    LOGGER.debug("[ENJOY]  Messaging PAI NOT OK at attempt number "+(messaggingRetryNumber+1));
                    messaggingRetryNumber = messaggingRetryNumber+1;
                    exceptionMessaging = e;
                }
            }


            if (responseStatus.equals(HttpStatus.ACCEPTED) && messaggingIsOk) {

                Optional<ClaimsNewEntity> optClaims = claimsNewRepository.findById(claimsId);
                if (optClaims.isPresent()) {

                    ClaimsNewEntity claims = optClaims.get();
                    String description = "";

                    description = this.createLogs(emailTemplateMessaging, null);

                    Historical historical = new Historical(EventTypeEnum.ENTRUST_PAI, claims.getStatus(), claims.getStatus(), claims.getTypeAccident(), claims.getTypeAccident(), new Date(), userId, userName, claims.getType().getValue(), claims.getType().getValue(), description);
                    claims.addHistorical(historical);
                    claims.setPaiComunication(true);
                    claimsNewRepository.save(claims);
                }
            }else{

                LOGGER.debug("[ENJOY] Messaging PAI  failed on claim's id " + claimsId);
                LOGGER.criticalError("NEMO_CLAIMS","[ENJOY] Messaging PAI  failed on claim's id " + claimsId, exceptionMessaging );
            }

        }
        return responseStatus;
    }


    //REFACTOR
    @Override
    public HttpStatus sendEmailLegalComunication(List<EmailTemplateMessagingRequestV1> emailTemplateMessagingRequestList, String claimsId, String userId, String userName) throws IOException {
        //recupero nuova entità
        Optional<ClaimsNewEntity> claimsNewEntityOptional = claimsNewRepository.findById(claimsId);
        ClaimsEntity claims = null;
        if (!claimsNewEntityOptional.isPresent()) {
            LOGGER.debug(MessageCode.CLAIMS_1010.value());
            throw new BadRequestException(MessageCode.CLAIMS_1010);
        }

        //converto nella vecchia entità
        claims = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntityOptional.get());

        List<String> attachment = new ArrayList<>();
        LegalCost legalCost = claims.getDamaged().getInsuranceCompany().getLegalCost();
        if(legalCost == null) {
            String message = String.format(MessageCode.CLAIMS_1104.value(), claims.getId());
            LOGGER.debug(MessageCode.CLAIMS_1104.value());
            throw new BadRequestException(message, MessageCode.CLAIMS_1104);
        }
        if(legalCost.getTariffId().equals(policyCodeBase)) {
            attachment.add(pdfBaseId);
            fileManagerService.attachPdfFileClaim(claimsId, pdfBaseId, userId, userName, false, true);
        }
        else if(legalCost.getTariffId().equals(policyCodePlus)) {
            attachment.add(pdfPlusId);
            fileManagerService.attachPdfFileClaim(claimsId, pdfPlusId, userId, userName, false, true);
        }


        HttpStatus responseStatus = HttpStatus.BAD_REQUEST;

        if(emailTemplateMessagingRequestList != null && !emailTemplateMessagingRequestList.isEmpty()) {
            List<EmailTemplateMessagingRequestV1> listTos = new LinkedList<>();
            List<EmailTemplateMessagingRequestV1> listNotTos = new LinkedList<>();
            for (EmailTemplateMessagingRequestV1 currentTemplate : emailTemplateMessagingRequestList) {
                if (currentTemplate.getTos() != null && !currentTemplate.getTos().isEmpty() && currentTemplate.getObject()!= null && !currentTemplate.getObject().isEmpty()) {
                    listTos.add(currentTemplate);
                } else {
                    listNotTos.add(currentTemplate);
                }
            }


            List<EmailTemplateMessagingRequestV1> emailTemplateMessaging = splitEmailIfContainsUrl(listTos, claimsId);
            responseStatus = this.sendEmail(emailTemplateMessaging, null, attachment, claims.getCreatedAt());
            emailTemplateMessaging.addAll(listNotTos);


            if (responseStatus.equals(HttpStatus.ACCEPTED)) {
                String description = "";

                description = this.createLogs(emailTemplateMessaging, null);
                //deve essere recuperato perchè neu metodi attachPdfFileClaim viene effettuata una save
                //recupero nella nuova entità
                claimsNewEntityOptional = claimsNewRepository.findById(claimsId);
                if (claimsNewEntityOptional.isPresent()) {
                    //converto nella vecchia entità
                    claims = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntityOptional.get());
                    Historical historical = new Historical(EventTypeEnum.LEGAL_COMUNICATION, claims.getStatus(), claims.getStatus(), claims.getComplaint().getDataAccident().getTypeAccident(), claims.getComplaint().getDataAccident().getTypeAccident(), new Date(), userId, userName, claims.getType().getValue(), claims.getType().getValue(), description);
                    claims.addHistorical(historical);
                    claims.setLegalComunication(true);
                    //converto nella nuova entità
                    ClaimsNewEntity claimsNewEntity = converterClaimsService.convertOldEntityToNewClaimsEntity(claims);
                    claimsNewRepository.save(claimsNewEntity);
                }

            }
        }

        return responseStatus;
    }


    //REFACTOR
    public void sendEmailConvertionURLAndLog (MessagingSendMailRequestV1 messagingSendMailRequestV1, String idCounterparty, String idClaim, String userId, String userName ) {
        String description = "";
        List<EmailTemplateMessagingRequestV1> emailTemplateMessagingRequestList = messagingSendMailRequestV1.getTemplateList();
        if(idClaim!=null && idCounterparty==null){
            if(emailTemplateMessagingRequestList != null && !emailTemplateMessagingRequestList.isEmpty()){
                List<EmailTemplateMessagingRequestV1> listTos = new LinkedList<>();
                List<EmailTemplateMessagingRequestV1> listNotTos = new LinkedList<>();
                for(EmailTemplateMessagingRequestV1 currentTemplate: emailTemplateMessagingRequestList){
                    if(currentTemplate.getTos() != null && !currentTemplate.getTos().isEmpty()){
                        listTos.add(currentTemplate);
                    }else{
                        listNotTos.add(currentTemplate);
                    }
                }

                //funzione refactorizzata
                List<EmailTemplateMessagingRequestV1> emailTemplateMessaging = this.splitEmailIfContainsUrl(listTos, idClaim);
                emailTemplateMessaging.addAll(listNotTos);
                description = this.sendMailAndCreateLogs(emailTemplateMessaging, null, null);
                //recupero della nuova entità
                Optional<ClaimsNewEntity> optionalClaimsNewEntity = claimsNewRepository.findById(idClaim);
                if(optionalClaimsNewEntity.isPresent()){
                    //conversione nella vecchia entità
                    ClaimsEntity claimsEntity = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(optionalClaimsNewEntity.get());

                    Historical historical = new Historical(messagingSendMailRequestV1.getEventType(), claimsEntity.getStatus(), claimsEntity.getStatus(), claimsEntity.getComplaint().getDataAccident().getTypeAccident(), claimsEntity.getComplaint().getDataAccident().getTypeAccident(), DateUtil.getNowDate(), userId, userName, claimsEntity.getType().getValue(), claimsEntity.getType().getValue(), description);
                    claimsEntity.addHistorical(historical);

                    //converto nella nuova entità
                    ClaimsNewEntity claimsNewEntity = converterClaimsService.convertOldEntityToNewClaimsEntity(claimsEntity);
                    claimsNewRepository.save(claimsNewEntity);
                }

            }
        } else if(idCounterparty!=null){
            this.sendAndLogEmail(messagingSendMailRequestV1, null, idClaim, idCounterparty, userId, userName);
        }

    }

    @Override
    public ClaimsEntity sendEmailEntrustPaiInternal(List<EmailTemplateMessagingRequestV1> emailTemplateMessagingRequestList, ClaimsEntity claimsEntity, String userName, ClaimsStatusEnum statusEnum) throws IOException {
        DogeResponseV1 dogeResponseV1 = null;
        try {
            dogeResponseV1 = dogeEnquService.enqueueClaimsPAI(claimsEntity.getId());
        } catch (IOException e) {
            LOGGER.debug(MessageCode.CLAIMS_1055.value());
            throw new BadRequestException(MessageCode.CLAIMS_1055);
        }
        if (dogeResponseV1 == null) {
            LOGGER.debug(MessageCode.CLAIMS_1056.value());
            throw new BadRequestException(MessageCode.CLAIMS_1056);
        }

        fileManagerService.attachPdfFileClaim(claimsEntity.getId(), dogeResponseV1.getDocumentId(), claimsEntity.getUserId(), userName, true, false);

        /* NE-1254  NEMOCLAIMS-1184 ( la Pai inviava mail ma non salvava il forms) */
        ClaimsNewEntity claimsNewEntityUpdate = claimsNewRepository.getOne(claimsEntity.getId());
        claimsEntity = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntityUpdate);
        /* NE-1254  NEMOCLAIMS-1184 ( la Pai inviava mail ma non salvava il forms) */

        List<String> attachment = new ArrayList<>();
        attachment.add(dogeResponseV1.getDocumentId());

        HttpStatus responseStatus = this.sendEmail(removeTemplateWithoutTos(emailTemplateMessagingRequestList), null, attachment, claimsEntity.getCreatedAt());

        if (responseStatus.equals(HttpStatus.ACCEPTED)) {

            String description = "";
            description = this.createLogs(emailTemplateMessagingRequestList, null);
            Historical historical = new Historical(EventTypeEnum.ENTRUST_PAI, statusEnum, statusEnum, claimsEntity.getComplaint().getDataAccident().getTypeAccident(), claimsEntity.getComplaint().getDataAccident().getTypeAccident(), new Date(), claimsEntity.getUserId(), userName, claimsEntity.getType().getValue(), claimsEntity.getType().getValue(), description);
            claimsEntity.addHistorical(historical);
            claimsEntity.setPaiComunication(true);

        }

        return claimsEntity;
    }


    private String checkContentAndReplace(String contentMail, ClaimsEntity claims, CounterpartyEntity counterparty, String externalUrl) {

        //GESTIONE CONTROPARTE
        if (counterparty != null) {
            //Assicurato
            Insured insured= counterparty.getInsured();
            if(insured != null && insured.getEmail() != null){
                contentMail = contentMail.replaceAll("<%ASSI_EMAIL%>", insured.getEmail());
            }
            
            //TRAGA VEICOLO CONTROPARTE
            Vehicle vehicle = counterparty.getVehicle();
            if (vehicle != null) {
                String counterpartyPlate = vehicle.getLicensePlate();
                if (counterpartyPlate != null) {
                    contentMail = contentMail.replaceAll("<%VEIC_TARGA%>", counterpartyPlate);
                }
            }

            Driver driver = counterparty.getDriver();
            if (driver != null) {
                String name = driver.getFirstname();
                String surname = driver.getLastname();
                String phone = driver.getPhone();
                String email = driver.getEmail();

                if (name != null) {
                    contentMail = contentMail.replaceAll("<%COND_NOME%>", name);
                }

                if (surname != null) {
                    contentMail = contentMail.replaceAll("<%COND_COGNOME%>", surname);
                }

                if (phone != null) {
                    contentMail = contentMail.replaceAll("<%COND_TELEFONO%>", phone);
                }else{
                    contentMail = contentMail.replaceAll("<%COND_TELEFONO%>", "");
                }

                if (email != null) {
                    contentMail = contentMail.replaceAll("<%COND_EMAIL%>", email);
                }else{
                    contentMail = contentMail.replaceAll("<%COND_EMAIL%>", "");
                }
            }

            ImpactPoint impactPoint = counterparty.getImpactPoint();
            if (impactPoint != null) {
                String desc = impactPoint.getDamageToVehicle();

                if (desc != null) {
                    contentMail = contentMail.replaceAll("<%DESC_NOTE%>", desc);
                }else{
                    contentMail = contentMail.replaceAll("<%DESC_NOTE%>", "");
                }
            }

            Canalization canalization = counterparty.getCanalization();
            if (canalization != null) {
                //RepairerEntity repairerEntity = canalization.getRepairer();
                CenterEntity centerEntity = canalization.getCenterEntity();
                if (centerEntity != null) {
                    String codRepairer = centerEntity.getAldSupplieCode();
                    String name = centerEntity.getName();

                    if (codRepairer != null) {
                        contentMail = contentMail.replaceAll("<%RIPA_CODICE%>", codRepairer);
                    }

                    if (name != null) {
                        contentMail = contentMail.replaceAll("<%RIPA_NOME%>", name);
                    }
                }
            }

            ManagerEntity manager = counterparty.getManager();
            if (manager != null) {
                String name = manager.getName();

                if (name != null) {
                    contentMail = contentMail.replaceAll("<%GEST_NOME%>", name);
                }else{
                    contentMail = contentMail.replaceAll("<%GEST_NOME%>", "");
                }
            }
        }

        //CREAZIONE SINISTRO
        if(claims.getCreatedAt()!=null){
            String createdAtDate = DateUtil.getDateStringWithSeparationCharacters(DateUtil.convertIS08601StringToUTCDate(DateUtil.convertUTCInstantToIS08601String(claims.getCreatedAt())));
            contentMail = contentMail.replaceAll("<%REICDATECOMP%>", createdAtDate);

        }

        //MOTIVATION
        /*String motivation = claims.getMotivation();

        if (motivation != null) {
            contentMail = contentMail.replaceAll("<%REATYPDESTY%>", motivation);
            contentMail = contentMail.replaceAll("<%REATYPDESTYP%>", motivation);
            contentMail = contentMail.replaceAll("<%REASDESCCOMP%>", motivation);
        } else {
            contentMail = contentMail.replaceAll("<%REATYPDESTY%>", "");
            contentMail = contentMail.replaceAll("<%REATYPDESTYP%>", "");
            contentMail = contentMail.replaceAll("<%REASDESCCOMP%>", "");
        }*/
        //DATA SINISTRO
        if (claims.getComplaint() != null && claims.getComplaint().getDataAccident() != null && claims.getComplaint().getDataAccident().getDateAccident() != null) {

            String dateAccident = DateUtil.getDateStringWithSeparationCharacters(claims.getComplaint().getDataAccident().getDateAccident());

            if (dateAccident != null) {
                contentMail = contentMail.replaceAll("<%ACCIDATECOMP%>", dateAccident);
                contentMail = contentMail.replaceAll("<%SIN_DATA%>", dateAccident);
            }
        }

        //TARGA VEICOLO DANNEGGIATO
        if (claims.getDamaged() != null && claims.getDamaged().getVehicle() != null) {
            String vehiclePlate = claims.getDamaged().getVehicle().getLicensePlate();
            if (vehiclePlate != null) {
                contentMail = contentMail.replaceAll("<%VEHIPLATDAMA%>", vehiclePlate);
                contentMail = contentMail.replaceAll("<%VEICPP_TARGA%>", vehiclePlate);
            } else if (claims.getComplaint() != null && claims.getComplaint().getPlate() != null) {
                contentMail = contentMail.replaceAll("<%VEHIPLATDAMA%>", claims.getComplaint().getPlate());
                contentMail = contentMail.replaceAll("<%VEICPP_TARGA%>", claims.getComplaint().getPlate());
            }
        }

        //ID PRATICA
        if (claims.getPracticeId() != null) {
            contentMail = contentMail.replaceAll("<%IDCOMPLAINT%>", claims.getPracticeId().toString());

        }

        Damaged damaged = claims.getDamaged();
        if (damaged != null) {
            if (damaged.getCustomer() != null) {

                //ID CLIENTE
                if (damaged.getCustomer().getCustomerId() != null) {
                    contentMail = contentMail.replaceAll("<%CUSTOMIDCUST%>", damaged.getCustomer().getCustomerId());
                }else{
                    contentMail = contentMail.replaceAll("<%CUSTOMIDCUST%>", "");
                }

                //RAGIONE SOCIALE
                if (damaged.getCustomer().getLegalName() != null) {
                    contentMail = contentMail.replaceAll("<%CUSTOMRAGSOC%>", damaged.getCustomer().getLegalName());
                }else{
                    contentMail = contentMail.replaceAll("<%CUSTOMRAGSOC%>", "");
                }

            }
            if (damaged.getContract() != null) {

                //ID CONTRATTO
                Long contractId = claims.getDamaged().getContract().getContractId();
                if (contractId != null) {
                    contentMail = contentMail.replaceAll("<%CUSTOIDCONTR%>", contractId.toString());
                }else{
                    contentMail = contentMail.replaceAll("<%CUSTOIDCONTR%>", "");
                }
            }

            //NOME DRIVER
            Driver driver = damaged.getDriver();
            if (driver != null) {
                String driverName = driver.getFirstname();

                if (driverName != null) {
                    contentMail = contentMail.replaceAll("<%DRIVNAMEDAMA%>", driverName);
                }else{
                    contentMail = contentMail.replaceAll("<%DRIVNAMEDAMA%>", "");
                }

                //COGNOME DRIVER
                String driverSurname = driver.getLastname();
                if (driverSurname != null) {
                    contentMail = contentMail.replaceAll("<%DRIVSURNDAMA%>", driverSurname);
                }else{
                    contentMail = contentMail.replaceAll("<%DRIVSURNDAMA%>", "");
                }

                String driverEmail = driver.getEmail();
                if (driverEmail != null) {
                    contentMail = contentMail.replaceAll("<%DRIVEMAIDAMA%>", driverEmail);
                }else{
                    contentMail = contentMail.replaceAll("<%DRIVEMAIDAMA%>", "");
                }
            }

            if (damaged.getImpactPoint() != null) {
                //DESCRIZIONE DEL SINISTRO
                String accidentDescription = claims.getDamaged().getImpactPoint().getIncidentDescription();
                if (accidentDescription != null) {
                    contentMail = contentMail.replaceAll("<%DAMACOMMDAMA%>", accidentDescription);
                }else {
                    contentMail = contentMail.replaceAll("<%DAMACOMMDAMA%>", "descrizione non presente");
                }

                //DANNI AL VEICOLO
                String damageToVehicle = claims.getDamaged().getImpactPoint().getDamageToVehicle();
                if (damageToVehicle != null) {
                    contentMail = contentMail.replaceAll("<%DAMADESCDAMA%>", damageToVehicle);
                }else{
                    contentMail = contentMail.replaceAll("<%DAMADESCDAMA%>", "");
                }
            }else{
                contentMail = contentMail.replaceAll("<%DAMACOMMDAMA%>", "descrizione non presente");
                contentMail = contentMail.replaceAll("<%DAMADESCDAMA%>", "");
            }
            //NOME ANTIFURTO
            AntiTheftService antiTheftService = claims.getDamaged().getAntiTheftService();
            if (antiTheftService != null && antiTheftService.getName() != null) {
                contentMail = contentMail.replaceAll("<%ANTITH_NAME%>", antiTheftService.getName());
            }else {
                contentMail = contentMail.replaceAll("<%ANTITH_NAME%>", "anti-theft non presente");
            }

            //COMPAGNIA ASSICURATRICE DEL DANNEGGIATO
            InsuranceCompany insuranceCompany = claims.getDamaged().getInsuranceCompany();
            if (insuranceCompany != null && (insuranceCompany.getTpl() != null && (insuranceCompany.getTpl().getCompany() != null && !insuranceCompany.getTpl().getCompany().equalsIgnoreCase("")))) {
                contentMail = contentMail.replaceAll("<%INSCOMNAMDAM%>", insuranceCompany.getTpl().getCompany());
            }else {
                contentMail = contentMail.replaceAll("<%INSCOMNAMDAM%>", "Non risulta nessuna Compagnia assicuratrice");
            }
        }

        Complaint complaint = claims.getComplaint();
        if (complaint != null) {
            //CITTA' DELL'INCIDENTE
            DataAccident dataAccident = complaint.getDataAccident();
            if (dataAccident != null) {
                Address address = dataAccident.getAddress();
                if (address != null) {

                    String accidentCity = address.getLocality();
                    if (accidentCity != null) {
                        contentMail = contentMail.replaceAll("<%ACCICITYCOMP%>", accidentCity);
                    }else {
                        contentMail = contentMail.replaceAll("<%ACCICITYCOMP%>", "informazione non presente");
                    }

                    //PROVINCIA DELL'INCIDENTE
                    String accidentProvince = address.getProvince();
                    if (accidentProvince != null) {
                        contentMail = contentMail.replaceAll("<%ACCIPROVCOMP%>", accidentProvince);
                    }else {
                        contentMail = contentMail.replaceAll("<%ACCIPROVCOMP%>", "");
                    }
                }
                //TIPO DI SINISTRO
                DataAccidentTypeAccidentEnum dataAccidentTypeAccidentEnum = dataAccident.getTypeAccident();
                if (dataAccidentTypeAccidentEnum != null) {
                    contentMail = contentMail.replaceAll("<%ACCITYPECDES%>", dataAccidentTypeAccidentEnum.getValue());
                }
            }

            FromCompany fromCompany = complaint.getFromCompany();
            if(fromCompany != null && fromCompany.getNumberSx() != null){
                contentMail = contentMail.replaceAll("<%SIN_NUMEXT%>", fromCompany.getNumberSx());
            } else {
                contentMail = contentMail.replaceAll("<%SIN_NUMEXT%>", "");
            }
        }

        //TIPO DI FLUSSO
        if (claims.getType() != null) {
            contentMail = contentMail.replaceAll("<%flowtypecomp%>", claims.getType().getValue());
        }

        //DATA RITROVAMENTO VEICOLO
        if (claims.getFoundModel() != null && claims.getFoundModel().getDate() != null) {
            String dateFound = DateUtil.getDateStringWithSeparationCharacters(claims.getFoundModel().getDate());
            contentMail = contentMail.replaceAll("<%FINDDATECOMP%>", dateFound);

            if (claims.getFoundModel().getAddress() != null && claims.getFoundModel().getAddress().getLocality() != null) {
                contentMail = contentMail.replaceAll("<%FINDCITYCOMP%>", claims.getFoundModel().getAddress().getLocality());
            }else {
                contentMail = contentMail.replaceAll("<%FINDCITYCOMP%>", "informazione non presente");
            }
        } else {
            contentMail = contentMail.replaceAll("<%FINDDATECOMP%>", "informazione non presente");
            contentMail = contentMail.replaceAll("<%FINDCITYCOMP%>", "informazione non presente");
        }
        //N SX COMPAGNIA
        if (claims.getComplaint() != null && claims.getComplaint().getFromCompany() != null &&
                claims.getComplaint().getFromCompany().getNumberSx() != null
        ) {
            String numberSx = claims.getComplaint().getFromCompany().getNumberSx();
            contentMail = contentMail.replaceAll("<%EXTACCNUMCOM%>", numberSx);
        } else {
            contentMail = contentMail.replaceAll("<%EXTACCNUMCOM%>", "informazione non presente");
        }

        contentMail = contentMail.replaceAll("<%NOTTYPDENOTY%>", "NOTIFICA");

        return contentMail;
    }

    public void parsContentOfMail(EmailTemplateMessagingResponseV1 sendMailRequest, ClaimsEntity claims, CounterpartyEntity counterparty, String externalUrl) {

        if (sendMailRequest.getDescription() != null)
            sendMailRequest.setDescription(this.checkContentAndReplace(sendMailRequest.getDescription(), claims, counterparty, externalUrl));
        if (sendMailRequest.getObject() != null)
            sendMailRequest.setObject(this.checkContentAndReplace(sendMailRequest.getObject(), claims, counterparty, externalUrl));
        if (sendMailRequest.getHeading() != null)
            sendMailRequest.setHeading(this.checkContentAndReplace(sendMailRequest.getHeading(), claims, counterparty, externalUrl));
        if (sendMailRequest.getBody() != null)
            sendMailRequest.setBody(this.checkContentAndReplace(sendMailRequest.getBody(), claims, counterparty, externalUrl));
        if (sendMailRequest.getFoot() != null)
            sendMailRequest.setFoot(this.checkContentAndReplace(sendMailRequest.getFoot(), claims, counterparty, externalUrl));

    }

    @Override
    public HttpStatus sendEmailConfirmSequelNotify(SequelWithNotifyMessagingRequestV1 requestSequelMessaging, String claimsId, String userId, String userName, ClaimsEntity claims) {

        String description = "";
        HttpStatus statusMail = HttpStatus.BAD_REQUEST;
        if (requestSequelMessaging.getEmailTemplateMessagingList() != null && !requestSequelMessaging.getEmailTemplateMessagingList().isEmpty() ) {

            List<EmailTemplateMessagingRequestV1> listTos = new LinkedList<>();
            List<EmailTemplateMessagingRequestV1> listNotTos = new LinkedList<>();

            for (EmailTemplateMessagingRequestV1 currentTemplate : requestSequelMessaging.getEmailTemplateMessagingList()) {
                if (currentTemplate.getTos() != null && !currentTemplate.getTos().isEmpty() && currentTemplate.getObject()!= null && !currentTemplate.getObject().isEmpty()) {
                    listTos.add(currentTemplate);
                } else {
                    listNotTos.add(currentTemplate);
                }
            }

            List<EmailTemplateMessagingRequestV1> emailTemplateMessaging = this.splitEmailIfContainsUrl(listTos, claimsId);
            emailTemplateMessaging.addAll(listNotTos);

            description = this.createLogs(emailTemplateMessaging, null, claims.getCreatedAt());


            Historical historical = new Historical(EventTypeEnum.SEND_WITH_SEQUEL, claims.getStatus(), claims.getStatus(), claims.getComplaint().getDataAccident().getTypeAccident(), claims.getComplaint().getDataAccident().getTypeAccident(), new Date(), userId, userName, claims.getType().getValue(), claims.getType().getValue(), description);

            statusMail = this.sendEmail(emailTemplateMessaging, null, claims.getCreatedAt());

            List<Historical> claimsHistorical = claims.getHistorical();
            if (claimsHistorical == null)
                claimsHistorical = new ArrayList<>();
            claimsHistorical.add(historical);
            claims.setHistorical(claimsHistorical);
            claimsRepository.save(claims);
        }
        /*
        for (EmailTemplateMessagingRequestV1 att : requestSequelMessaging.getEmailTemplateMessagingList()){
            this.parsContentOfMail(att, claims);
        }*/
        return statusMail;
    }


    //REFACTOR
    @Override
    public List<EmailTemplateMessagingResponseV1> getMailTemplateByTypeEvent(EventTypeEnum typeEvent, String claimsId) {
        //recupero della nuova entità
        Optional<ClaimsNewEntity> claimsNewEntityOptional = claimsNewRepository.findById(claimsId);

        if (!claimsNewEntityOptional.isPresent()) {
            LOGGER.debug("Claims with id " + claimsId + " not found");
            throw new NotFoundException("Claims with id " + claimsId + " not found", MessageCode.CLAIMS_1010);
        }

        ClaimsNewEntity claimsNewEntity = claimsNewEntityOptional.get();
        //conversione nella vecchia entità
        ClaimsEntity claims = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntity);
        ClaimsFlowEnum claimsFlow = claims.getType();
        DataAccidentTypeAccidentEnum claimsType = null;
        if (claims.getComplaint() !=null && claims.getComplaint().getDataAccident()!=null && claims.getComplaint().getDataAccident().getTypeAccident() != null) {
            claimsType = claims.getComplaint().getDataAccident().getTypeAccident();
        }

        String customerId = null;
        List<EmailTemplateEntity> emailTemplateListWithFlow;


        //CERCO PRIMA TEMPLATE CON TIPO SINISTRO
        if (claims.getDamaged() != null && claims.getDamaged().getCustomer() != null && Util.isNotEmpty(claims.getDamaged().getCustomer().getCustomerId())) {

            customerId = claims.getDamaged().getCustomer().getCustomerId();
            emailTemplateListWithFlow = emailTemplateRepositoryV1.getTemplateEntityByEventDescription(typeEvent.getValue(), claimsFlow.getValue(), customerId, null, claimsType.getValue());

            if (emailTemplateListWithFlow == null || emailTemplateListWithFlow.isEmpty())
                emailTemplateListWithFlow = emailTemplateRepositoryV1.getTemplateEntityByEventDescription(typeEvent.getValue(), claimsFlow.getValue(), null, null, claimsType.getValue());

        } else {
            emailTemplateListWithFlow = emailTemplateRepositoryV1.getTemplateEntityByEventDescription(typeEvent.getValue(), claimsFlow.getValue(), null, null, claimsType.getValue());
        }

        //SE NON HO TROVATO TEMPLATE CON TIPO SINISTRO CERCO TRA QUELLI CON TIPO SINISTRO NULL
        if(emailTemplateListWithFlow==null || emailTemplateListWithFlow.isEmpty()) {
            if (claims.getDamaged() != null && claims.getDamaged().getCustomer() != null && Util.isNotEmpty(claims.getDamaged().getCustomer().getCustomerId())) {

                customerId = claims.getDamaged().getCustomer().getCustomerId();
                emailTemplateListWithFlow = emailTemplateRepositoryV1.getTemplateEntityByEventDescription(typeEvent.getValue(), claimsFlow.getValue(), customerId, null, null);

                if (emailTemplateListWithFlow == null || emailTemplateListWithFlow.isEmpty())
                    emailTemplateListWithFlow = emailTemplateRepositoryV1.getTemplateEntityByEventDescription(typeEvent.getValue(), claimsFlow.getValue(), null, null, null);

            } else {
                emailTemplateListWithFlow = emailTemplateRepositoryV1.getTemplateEntityByEventDescription(typeEvent.getValue(), claimsFlow.getValue(), null, null, null);
            }
        }


        List<EmailTemplateMessagingResponseV1> emailTemplateMessagingResponseList = new LinkedList<>();

       /* List<EmailTemplateEntity> emailTemplateList = new LinkedList<>();
        for(EmailTemplateEntity emailTemplateEntity: emailTemplateListWithFlow){
            if(emailTemplateEntity.getFlows()!=null && emailTemplateEntity.getFlows().contains(claimsFlow))
                emailTemplateList.add(emailTemplateEntity);
        }*/

        if (emailTemplateListWithFlow != null && !emailTemplateListWithFlow.isEmpty()) {

            for (EmailTemplateEntity emailTemplate : emailTemplateListWithFlow) {

                if (emailTemplate.getFlows() != null && emailTemplate.getFlows().contains(claims.getType())) {

                    List<RecipientTypeEntity> recipientTypeEntityList = emailTemplate.getCustomers();
                    List<Identity> tos = new LinkedList<>();

                    CounterpartyEntity counterparty = this.getCounterpartyResponsable(claims.getCounterparts());
                    if (counterparty != null && counterparty.getAld() != null && counterparty.getAld()) {
                        recipientTypeEntityList.removeIf(recipientTypeEntity -> recipientTypeEntity.getRecipientType().equals(RecipientEnum.COMPANY));
                    }

                    for (RecipientTypeEntity recipientType : recipientTypeEntityList) {

                        Identity customer = new Identity();
                        if(recipientType.getActive() != null && recipientType.getActive()) {
                            if (!recipientType.getFixed()) {

                                //CHIAMATA A METODO findIdentityEmail
                                tos.addAll(this.findIdentityEmail(recipientType.getRecipientType(), claims, null));
                            } else {

                                tos.add(new Identity(recipientType.getEmail(), "", recipientType.getRecipientType().getValue()));
                            }
                        }
                    }

                    /*List<NemoAuthExternalResposeV1> nemoAuthExternalResposeList = this.getExternalTokenString(claims, tos);
                    String url = null;
                    if(nemoAuthExternalResposeList.size() > 0){
                        url = nemoAuthExternalResposeList.get(0).getUrl();
                    }*/

                    EmailTemplateMessagingResponseV1 emailTemplateMessaging = new EmailTemplateMessagingResponseV1();
                    emailTemplateMessaging.setTos(tos);
                    emailTemplateMessaging.setHeading(emailTemplate.getHeading());
                    emailTemplateMessaging.setBody(emailTemplate.getBody());
                    emailTemplateMessaging.setFoot(emailTemplate.getFoot());
                    emailTemplateMessaging.setObject(emailTemplate.getObject());
                    emailTemplateMessaging.setAttachFile(emailTemplate.getAttachFile());
                    emailTemplateMessaging.setSplittingRecipientsEmail(emailTemplate.getSplittingRecipientsEmail());

                    emailTemplateMessaging.setDescription(emailTemplate.getDescription());

                    this.parsContentOfMail(emailTemplateMessaging, claims, null, null);

                    emailTemplateMessagingResponseList.add(emailTemplateMessaging);
                }
            }
        }
        return emailTemplateMessagingResponseList;

    }

    //REFACTOR
    public List<EmailTemplateMessagingResponseV1> getRepairMailTemplateByTypeEvent(EventTypeEnum typeEvent, String claimsId, String counterpartyId) {
        //recupero nuova entità
        Optional<ClaimsNewEntity> claimsNewEntityOpt = claimsNewRepository.findById(claimsId);
        if (!claimsNewEntityOpt.isPresent()) {
            LOGGER.debug("Claims with id " + claimsId + " not found");
            throw new NotFoundException("Claims with id " + claimsId + " not found", MessageCode.CLAIMS_1010);
        }
        //conversione nella nuova entità
        ClaimsEntity claims = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntityOpt.get());

        ClaimsFlowEnum claimsFlow = claims.getType();
        String customerId = null;
        if (claims.getDamaged() != null && claims.getDamaged().getCustomer() != null && claims.getDamaged().getCustomer().getCustomerId() != null)
            customerId = claims.getDamaged().getCustomer().getCustomerId();

        List<EmailTemplateMessagingResponseV1> emailTemplateMessagingResponseList = new LinkedList<>();
        List<EmailTemplateEntity> emailTemplateListWithFlow;
        if (claims.getDamaged() != null && claims.getDamaged().getCustomer() != null && Util.isNotEmpty(claims.getDamaged().getCustomer().getCustomerId())) {
            customerId = claims.getDamaged().getCustomer().getCustomerId();
            emailTemplateListWithFlow = emailTemplateRepositoryV1.getTemplateEntityByEventDescription(typeEvent.getValue(), claimsFlow.getValue(), customerId, null,null);
            if (emailTemplateListWithFlow == null || emailTemplateListWithFlow.isEmpty())
                emailTemplateListWithFlow = emailTemplateRepositoryV1.getTemplateEntityByEventDescription(typeEvent.getValue(), claimsFlow.getValue(), null, null, null);
        } else {
            emailTemplateListWithFlow = emailTemplateRepositoryV1.getTemplateEntityByEventDescription(typeEvent.getValue(), claimsFlow.getValue(), null, null, null);
        }

        if (emailTemplateListWithFlow != null && !emailTemplateListWithFlow.isEmpty()) {

            for (EmailTemplateEntity emailTemplate : emailTemplateListWithFlow) {

                if (emailTemplate.getFlows() != null && emailTemplate.getFlows().contains(claims.getType())) {


                    List<RecipientTypeEntity> recipientTypeEntityList = emailTemplate.getCustomers();
                    List<Identity> tos = new LinkedList<>();

                    /*List<Counterparts> listCounterParty = claims.getCounterparts();
                    if (listCounterParty == null || listCounterParty.isEmpty()) {
                        LOGGER.debug(MessageCode.CLAIMS_1029.value());
                        throw new BadRequestException(MessageCode.CLAIMS_1029);
                    }
                    Counterparts counterparts = null;
                    for (Counterparts currentCounterparts : listCounterParty) {
                        if (currentCounterparts.getCounterpartyId().equalsIgnoreCase(counterpartyId)) {
                            counterparts = currentCounterparts;
                        }
                    }
                    if (counterparts == null) {

                        LOGGER.debug(MessageCode.CLAIMS_1030.value());
                        throw new BadRequestException(MessageCode.CLAIMS_1030);
                    }*/
                    if(counterpartyId == null) {
                        throw new BadRequestException(MessageCode.CLAIMS_1030);
                    }
                    //recupero nuova entità
                    Optional<CounterpartyNewEntity> counterpartyNewEntityOptional = counterpartyNewRepository.findById(counterpartyId);

                    if(!counterpartyNewEntityOptional.isPresent()){
                        throw new BadRequestException(MessageCode.CLAIMS_1030);
                    }
                    //conversione nella vecchia entità
                    CounterpartyEntity counterparty = CounterpartyAdapter.adptCounterpartyNewToCounterpartyOld(counterpartyNewEntityOptional.get());

                    for (RecipientTypeEntity recipientType : recipientTypeEntityList) {

                        Identity customer = new Identity();
                        if(recipientType.getActive() != null && recipientType.getActive()) {
                            if (!recipientType.getFixed()) {

                                //CHIAMATA A METODO findIdentityEmail
                                tos.addAll(this.findIdentityEmail(recipientType.getRecipientType(), claims, counterparty));
                            } else {

                                tos.add(new Identity(recipientType.getEmail(), "", recipientType.getRecipientType().getValue()));
                            }
                        }
                    }

                    EmailTemplateMessagingResponseV1 emailTemplateMessaging = new EmailTemplateMessagingResponseV1();
                    emailTemplateMessaging.setTos(tos);
                    emailTemplateMessaging.setHeading(emailTemplate.getHeading());
                    emailTemplateMessaging.setBody(emailTemplate.getBody());
                    emailTemplateMessaging.setFoot(emailTemplate.getFoot());
                    emailTemplateMessaging.setObject(emailTemplate.getObject());
                    emailTemplateMessaging.setAttachFile(emailTemplate.getAttachFile());
                    emailTemplateMessaging.setSplittingRecipientsEmail(emailTemplate.getSplittingRecipientsEmail());

                    emailTemplateMessaging.setDescription(emailTemplate.getDescription());


                    this.parsContentOfMail(emailTemplateMessaging, claims, counterparty, null);

                    emailTemplateMessagingResponseList.add(emailTemplateMessaging);

                }
            }
        }
        return emailTemplateMessagingResponseList;

    }

    private List<Identity> findIdentityEmail(RecipientEnum recipientType, ClaimsEntity claimsEntity, CounterpartyEntity counterparty) {

        List<Identity> identityList = new LinkedList<>();
        String recipient = "";
        String email = "";
        String name = "";

        if (counterparty != null) {
            switch (recipientType) {
                case DRIVER:
                    recipient= "DRIVER";
                    if(claimsEntity!=null && claimsEntity.getDamaged()!=null && claimsEntity.getDamaged().getDriver()!=null) {
                        Driver driver = claimsEntity.getDamaged().getDriver();
                        if (driver.getEmail()!=null) {
                            email = driver.getEmail();
                            name = (driver.getFirstname()!=null? driver.getFirstname():"") + " " +(driver.getLastname()!=null? driver.getLastname():"");
                        }
                    }
                    break;
                case DRIVER_COUNTERPARTY:
                    recipient = "DRIVER_COUNTERPARTY";
                    Driver driver = counterparty.getDriver();
                    if (driver != null) {
                        if (driver.getEmail() != null && !(driver.getEmail().isEmpty())) {

                            email = driver.getEmail();
                            name = driver.getFirstname();
                        }
                    }

                    break;
                case PRACTICAL_REPAIRER:
                    recipient = "PRACTICAL_REPAIRER";
                    Canalization canalization = counterparty.getCanalization();
                    if (canalization != null) {
                        //RepairerEntity repairerEntity = canalization.getRepairer();
                        CenterEntity centerEntity = canalization.getCenterEntity();
                        if (centerEntity != null) {
                            if (centerEntity.getEmail() != null && !(centerEntity.getEmail().isEmpty())) {
                                email = centerEntity.getEmail();
                                name = centerEntity.getName();
                            }
                        }
                    }
                    break;
                case CLAIMS_MANAGER_REPAIR:
                    recipient = "CLAIMS_MANAGER_REPAIR";
                    ManagerEntity managerEntity = counterparty.getManager();
                    if (managerEntity != null) {
                        if (managerEntity.getEmail() != null && !(managerEntity.getEmail().isEmpty())) {
                            name = managerEntity.getName();
                            email = managerEntity.getEmail();
                        }
                    }
                    break;
                case INSURED_COUNTERPARTY:
                    recipient = "INSURED_COUNTERPARTY";
                    Insured insured = counterparty.getInsured();
                    if (insured != null) {
                        if (insured.getEmail() != null && !(insured.getEmail().isEmpty())) {
                            name = insured.getFirstname();
                            email = insured.getEmail();
                        }
                    }
                    break;
                case LEGAL_OR_CONSULTANT:
                    recipient = "LEGAL_OR_CONSULTANT";
                    ManagerEntity managerEntity1 = counterparty.getManager();
                    if(managerEntity1 != null){
                        name = managerEntity1.getName();
                        email = managerEntity1.getEmail();
                    }
                    break;
                default:
                    LOGGER.debug(MessageCode.CLAIMS_1115.value());
            }

        } else {
            switch (recipientType) {
                case DRIVER:
                    recipient= "DRIVER";
                    if(claimsEntity!=null && claimsEntity.getDamaged()!=null && claimsEntity.getDamaged().getDriver()!=null) {
                        Driver driver = claimsEntity.getDamaged().getDriver();
                        if (driver.getEmail()!=null){
                            email= driver.getEmail();
                            name = (driver.getFirstname()!=null? driver.getFirstname():"") + " " +(driver.getLastname()!=null? driver.getLastname():"");
                        }
                    }
                    break;
                case INSERT_CLAIM_USER:

                    recipient = "INSERT_CLAIM_USER";

                    if (claimsEntity.getMetadata() != null && claimsEntity.getMetadata().getMetadata() != null
                            && claimsEntity.getMetadata().getMetadata().containsKey("email") &&
                            claimsEntity.getMetadata().getMetadata().get("email") != null &&
                            !((String) claimsEntity.getMetadata().getMetadata().get("email")).isEmpty()
                    ) {
                        email = (String) claimsEntity.getMetadata().getMetadata().get("email");
                        name = (String) claimsEntity.getMetadata().getMetadata().get("name");
                    }

                    break;

                case LEGAL:

                    recipient = "LEGAL";

                    if (claimsEntity.getComplaint().getEntrusted() != null) {//SE IL CLAIMS E' STATO AFFIDATO

                        Entrusted entrusted = claimsEntity.getComplaint().getEntrusted();
                        if (entrusted.getEntrustedType() != null && entrusted.getEntrustedType().equals(EntrustedEnum.LEGAL)) {

                            email = entrusted.getEntrustedEmail();
                            name = entrusted.getEntrustedTo();
                        }
                    }
                    //identityList.add(new SendMailResponseV1.IdentityResponse(email, "LEGAL", name));
                    break;
                case CLAIMS_MANAGER_ENTRUSTED:

                    recipient = "CLAIMS_MANAGER";

                    if (claimsEntity.getComplaint().getEntrusted() != null) {//SE IL CLAIMS E' STATO AFFIDATO

                        Entrusted entrusted = claimsEntity.getComplaint().getEntrusted();
                        if (entrusted.getEntrustedType() != null && entrusted.getEntrustedType().equals(EntrustedEnum.MANAGER)) {

                            email = entrusted.getEntrustedEmail();
                            name = entrusted.getEntrustedTo();
                        }
                    }
                    //identityList.add(new SendMailResponseV1.IdentityResponse(email, "LEGAL", name));
                    break;

                case INSPECTORATE:

                    recipient = "INSPECTORATE";
                    if (claimsEntity.getComplaint().getEntrusted() != null) {//SE IL CLAIMS E' STATO AFFIDATO

                        Entrusted entrusted = claimsEntity.getComplaint().getEntrusted();
                        if (entrusted.getEntrustedType() != null && entrusted.getEntrustedType().equals(EntrustedEnum.INSPECTORATE)) {

                            email = entrusted.getEntrustedEmail();
                            name = entrusted.getEntrustedTo();

                        }
                    }
                    //identityList.add(new SendMailResponseV1.IdentityResponse(email, "INSPECTORATE", name));
                    break;

                case COMPANY:

                    recipient = "COMPANY";

                    if (claimsEntity.getDamaged().getInsuranceCompany() != null) {

                        if (claimsEntity.getDamaged().getInsuranceCompany().getTpl() != null) {
                            //RECUPERO L'EMAIL DELLA COMPAGNIA DALL'ENTITY IN DAMAGED->TPL
                            if (claimsEntity.getDamaged().getInsuranceCompany().getTpl().getInsuranceCompanyId() != null) {
                                Optional<InsuranceCompanyEntity> insuranceCompanyEntityOptional = insuranceCompanyRepository.findById(claimsEntity.getDamaged().getInsuranceCompany().getTpl().getInsuranceCompanyId());
                                InsuranceCompanyEntity insuranceCompany;
                                if (insuranceCompanyEntityOptional.isPresent()) {
                                    insuranceCompany = insuranceCompanyEntityOptional.get();
                                    if (insuranceCompany.getEmail() != null && !(insuranceCompany.getEmail().isEmpty())) {
                                        email = insuranceCompany.getEmail();
                                        name = insuranceCompany.getName();
                                    }
                                }
                            }
                        }
                    }
                    break;

                case CLAIMS_MANAGER:

                    recipient = "CLAIMS_MANAGER";
                    //identityList.add(new SendMailResponseV1.IdentityResponse(email, "ESTORI SINISTRI", name));
                    if (claimsEntity.getDamaged().getInsuranceCompany().getTpl() != null) {
                        if (claimsEntity.getDamaged().getInsuranceCompany().getTpl().getInsurancePolicyId() != null) {

                            Optional<InsurancePolicyEntity> insurancePolicyEntityOptional = insurancePolicyRepository.findById(claimsEntity.getDamaged().getInsuranceCompany().getTpl().getInsurancePolicyId());
                            if(insurancePolicyEntityOptional.isPresent()){
                                InsurancePolicyEntity insurancePolicyEntity = insurancePolicyEntityOptional.get();
                                if (insurancePolicyEntity.getInsuranceManagerEntity() != null) {
                                    email = insurancePolicyEntity.getInsuranceManagerEntity().getEmail();
                                    name = insurancePolicyEntity.getInsuranceManagerEntity().getName();
                                }
                            }
                        }
                    }
                    break;

                case INSURED:

                    recipient = "INSURED";

                    identityList = this.addFleetManagerEmail(identityList, claimsEntity, recipient);

                    /*if ((claimsEntity.getDamaged().getFleetManagerList() != null) && (claimsEntity.getDamaged().getFleetManagerList().size() > 0)) {

                        //CONSIDERO IL PRIMO FLEET MANAGER IN QUANTO IL CLAIMS DOVRA' CONTENERNE SOLO UNO E NON PIU' UNA LISTA
                        List<FleetManager> fleetManagerList = claimsEntity.getDamaged().getFleetManagerList();
                        FleetManager fleetManager = fleetManagerList.get(0);
                        if (fleetManager.getEmail() != null) {
                            email = fleetManager.getEmail();
                            name = fleetManager.getFirstName();
                        }
                    }*/
                    break;

                case SEQUEL:

                    recipient = "SEQUEL";

                    //presenza di info rca
                    if (claimsEntity.getDamaged().getInsuranceCompany() != null &&
                            claimsEntity.getDamaged().getInsuranceCompany().getTpl() != null
                    ) {
                        Tpl tpl = claimsEntity.getDamaged().getInsuranceCompany().getTpl();

                        //recupero compagnia assicuratrice
                        if (tpl.getInsuranceCompanyId() != null) {
                            Optional<InsuranceCompanyEntity> insuranceCompanyEntityOptional = insuranceCompanyRepository.findById(tpl.getInsuranceCompanyId());

                            if (insuranceCompanyEntityOptional.isPresent() ) {
                                InsuranceCompanyEntity insuranceCompany = insuranceCompanyEntityOptional.get();
                                if(insuranceCompany.getEmail() != null && !insuranceCompany.getEmail().equalsIgnoreCase("")) {
                                    email = insuranceCompany.getEmail();
                                    name = insuranceCompany.getName();
                                    //recipient= "COMPANY_TPL";
                                    identityList.add(new Identity(email, name, recipient));
                                }
                            }
                        }

                        //recupero insurance manager polizza rca
                        if(tpl.getInsurancePolicyId() != null) {
                            Optional<InsurancePolicyEntity> insurancePolicyEntityOptional = insurancePolicyRepository.findById(tpl.getInsurancePolicyId());

                            if(insurancePolicyEntityOptional.isPresent()){
                                InsuranceManagerEntity insuranceManagerEntity = insurancePolicyEntityOptional.get().getInsuranceManagerEntity();
                                if(insuranceManagerEntity != null && insuranceManagerEntity.getEmail() != null && !insuranceManagerEntity.getEmail().equalsIgnoreCase("")){
                                    email = insuranceManagerEntity.getEmail();
                                    name = insuranceManagerEntity.getName();
                                    //recipient= "MANAGER_TPL";
                                    identityList.add(new Identity(email, name, recipient));
                                }
                            }
                        }

                    }

                    //presenza di info pai solo se è stata già mandata la comunicazione pai
                    if ( claimsEntity.getPaiComunication() != null &&  claimsEntity.getPaiComunication() && claimsEntity.getDamaged().getInsuranceCompany() != null &&
                            claimsEntity.getDamaged().getInsuranceCompany().getPai() != null
                    ) {
                        Pai pai = claimsEntity.getDamaged().getInsuranceCompany().getPai();

                        //recupero compagnia assicuratrice
                        if (pai.getInsuranceCompanyId() != null) {
                            Optional<InsuranceCompanyEntity> insuranceCompanyEntityOptional = insuranceCompanyRepository.findById(pai.getInsuranceCompanyId());

                            if (insuranceCompanyEntityOptional.isPresent() ) {
                                InsuranceCompanyEntity insuranceCompany = insuranceCompanyEntityOptional.get();
                                if(insuranceCompany.getEmail() != null && !insuranceCompany.getEmail().equalsIgnoreCase("")) {
                                    email = insuranceCompany.getEmail();
                                    name = insuranceCompany.getName();
                                    //recipient= "COMPANY_PAI";
                                    identityList.add(new Identity(email, name, recipient));
                                }
                            }
                        }

                        //recupero insurance manager polizza rca
                        if(pai.getInsurancePolicyId() != null) {
                            Optional<InsurancePolicyEntity> insurancePolicyEntityOptional = insurancePolicyRepository.findById(pai.getInsurancePolicyId());

                            if(insurancePolicyEntityOptional.isPresent()){
                                InsuranceManagerEntity insuranceManagerEntity = insurancePolicyEntityOptional.get().getInsuranceManagerEntity();
                                if(insuranceManagerEntity != null && insuranceManagerEntity.getEmail() != null && !insuranceManagerEntity.getEmail().equalsIgnoreCase("")){
                                    email = insuranceManagerEntity.getEmail();
                                    name = insuranceManagerEntity.getName();
                                    //recipient= "MANAGER_PAI";
                                    identityList.add(new Identity(email, name, recipient));
                                }
                            }
                        }

                    }

                    //recupero informazioni affidatario
                    if (claimsEntity.getComplaint().getEntrusted() != null) {
                        Entrusted entrusted = claimsEntity.getComplaint().getEntrusted();
                        if (entrusted.getEntrustedType() != null) {

                            switch (entrusted.getEntrustedType()) {

                                case LEGAL:

                                    if (entrusted.getEntrustedEmail() != null && !entrusted.getEntrustedEmail().equalsIgnoreCase("")) {

                                        email = entrusted.getEntrustedEmail();
                                        name = entrusted.getEntrustedTo();

                                    } else {
                                        if (entrusted.getIdEntrustedTo() != null) {
                                            Optional<LegalEntity> legalEntityOptional = legalRepository.findById(entrusted.getIdEntrustedTo());

                                            if (legalEntityOptional.isPresent()) {

                                                LegalEntity legal = legalEntityOptional.get();
                                                if (legal.getEmail() != null && !legal.getEmail().equalsIgnoreCase("")) {

                                                    email = legal.getEmail();
                                                    name = legal.getName();
                                                }
                                            }
                                        }
                                    }
                                    identityList.add(new Identity(email, name, recipient));
                                    break;

                                case INSPECTORATE:

                                    if (entrusted.getEntrustedEmail() != null && !entrusted.getEntrustedEmail().equalsIgnoreCase("")) {

                                        email = entrusted.getEntrustedEmail();
                                        name = entrusted.getEntrustedTo();
                                    } else {
                                        if (entrusted.getIdEntrustedTo() != null) {
                                            Optional<InspectorateEntity> inspectorateEntityOptional = inspectorateRepository.findById(entrusted.getIdEntrustedTo());
                                            if (inspectorateEntityOptional.isPresent()) {
                                                InspectorateEntity inspectorate = inspectorateEntityOptional.get();
                                                if (inspectorate.getEmail() != null && !inspectorate.getEmail().equalsIgnoreCase("")) {
                                                    email = inspectorate.getEmail();
                                                    name = inspectorate.getName();

                                                }
                                            }
                                        }
                                    }
                                    identityList.add(new Identity(email, name, recipient));
                                    break;

                                case MANAGER:

                                    if (entrusted.getEntrustedEmail() != null && !entrusted.getEntrustedEmail().equalsIgnoreCase("")) {

                                        email = entrusted.getEntrustedEmail();
                                        name = entrusted.getEntrustedTo();

                                    } else {
                                        if (entrusted.getIdEntrustedTo() != null) {
                                            Optional<ManagerEntity> managerEntityOptional = managerRepository.findById(entrusted.getIdEntrustedTo());
                                            if (managerEntityOptional.isPresent()) {
                                                ManagerEntity manager = managerEntityOptional.get();
                                                if (manager.getEmail() != null && !manager.getEmail().equalsIgnoreCase("")) {
                                                    email = manager.getEmail();
                                                    name = manager.getName();
                                                }
                                            }
                                        }
                                    }
                                    identityList.add(new Identity(email, name, recipient));
                                    break;
                            }
                        }
                    }

                    break;

                case ASAT:

                    recipient = "ASAT";

                    if(claimsEntity.getDamaged() != null && claimsEntity.getDamaged().getAntiTheftService() != null && claimsEntity.getDamaged().getAntiTheftService().getCodeAntiTheftService() != null
                            &&  claimsEntity.getDamaged().getAntiTheftService().getEmail() != null &&  !claimsEntity.getDamaged().getAntiTheftService().getEmail().isEmpty()){

                        email = claimsEntity.getDamaged().getAntiTheftService().getEmail();
                        name = claimsEntity.getDamaged().getAntiTheftService().getName();


                    }else if(claimsEntity.getDamaged() != null && claimsEntity.getDamaged().getAntiTheftService() != null && claimsEntity.getDamaged().getAntiTheftService().getRegistryList() != null &&
                            !claimsEntity.getDamaged().getAntiTheftService().getRegistryList().isEmpty() && claimsEntity.getDamaged().getAntiTheftService().getRegistryList().get(0).getCodPack()!= null){

                        String codAntiTheftService = AntiTheftServiceAdapter.adptProviderToAntiTheftServiceEntity(claimsEntity.getDamaged().getAntiTheftService().getRegistryList().get(0).getCodPack());

                        if(codAntiTheftService != null && !codAntiTheftService.equalsIgnoreCase("")) {
                            AntiTheftServiceEntity antiTheftService = antiTheftServiceRepository.findAntiTheftByCodAntiTheftService(codAntiTheftService);

                            if (antiTheftService != null && antiTheftService.getEmail() != null && !(antiTheftService.getEmail().isEmpty())) {

                                email = antiTheftService.getEmail();
                                name = antiTheftService.getName();
                            }
                        }

                    }



                    break;

                case WITHDRAWAL_SERVICE_POINT:

                    recipient = "WITHDRAWAL_SERVICE_POINT";
                    //identityList.add(new SendMailResponseV1.IdentityResponse(email, "SERVICE POINT INCARICA RITIRO",name));
                    break;

                case PAI_COMPANY:

                    recipient = "PAI_COMPANY";

                    if (claimsEntity.getDamaged().getInsuranceCompany() != null) {

                        if (claimsEntity.getDamaged().getInsuranceCompany().getPai() != null) {

                            if (claimsEntity.getDamaged().getInsuranceCompany().getPai().getInsuranceCompanyId() != null) {
                                Optional<InsuranceCompanyEntity> insuranceCompanyEntityOptional = insuranceCompanyRepository.findById(claimsEntity.getDamaged().getInsuranceCompany().getPai().getInsuranceCompanyId());
                                InsuranceCompanyEntity insuranceCompany = null;
                                if (insuranceCompanyEntityOptional.isPresent())
                                    insuranceCompany = insuranceCompanyEntityOptional.get();

                                if (insuranceCompany != null && insuranceCompany.getEmailPai() != null && !(insuranceCompany.getEmailPai().isEmpty())) {

                                    email = insuranceCompany.getEmailPai();
                                    name = insuranceCompany.getName();
                                }
                            }
                        }
                    }
                    break;

                case PAI_MANAGER:

                    recipient = "PAI_MANAGER";
                    if (claimsEntity.getDamaged().getInsuranceCompany().getPai() != null) {
                        if (claimsEntity.getDamaged().getInsuranceCompany().getPai().getInsurancePolicyId() != null) {
                            InsurancePolicyEntity insurancePolicyEntity = insurancePolicyRepository.getOne(claimsEntity.getDamaged().getInsuranceCompany().getPai().getInsurancePolicyId());
                            if (insurancePolicyEntity != null) {
                                if (insurancePolicyEntity.getInsuranceManagerEntity() != null) {
                                    email = insurancePolicyEntity.getInsuranceManagerEntity().getEmail();
                                    name = insurancePolicyEntity.getInsuranceManagerEntity().getName();
                                }
                            }
                        }
                    }

                    break;

                case LEGAL_MANAGER:

                    recipient = "LEGAL_MANAGER";
                    if (claimsEntity.getDamaged().getInsuranceCompany().getLegalCost() != null){
                        if(claimsEntity.getDamaged().getInsuranceCompany().getLegalCost().getInsurancePolicyId() != null) {
                            InsurancePolicyEntity insurancePolicyEntity = insurancePolicyRepository.getOne(claimsEntity.getDamaged().getInsuranceCompany().getLegalCost().getInsurancePolicyId());
                            if (insurancePolicyEntity != null) {
                                if (insurancePolicyEntity.getInsuranceManagerEntity() != null) {
                                    email = insurancePolicyEntity.getInsuranceManagerEntity().getEmail();
                                    name = insurancePolicyEntity.getInsuranceManagerEntity().getName();
                                }
                            }
                        }
                    }

                    break;

                case LEGAL_COMPANY:

                    recipient = "LEGAL_COMPANY";

                    if (claimsEntity.getDamaged().getInsuranceCompany() != null) {

                        if (claimsEntity.getDamaged().getInsuranceCompany().getLegalCost() != null) {

                            if(claimsEntity.getDamaged().getInsuranceCompany().getLegalCost().getInsuranceCompanyId() != null){
                                Optional<InsuranceCompanyEntity> insuranceCompanyEntityOptional = insuranceCompanyRepository.findById(claimsEntity.getDamaged().getInsuranceCompany().getLegalCost().getInsuranceCompanyId());
                                InsuranceCompanyEntity insuranceCompany = null;
                                if(insuranceCompanyEntityOptional.isPresent())
                                    insuranceCompany = insuranceCompanyEntityOptional.get();

                                if (insuranceCompany != null && insuranceCompany.getEmail() != null && !(insuranceCompany.getEmail().isEmpty())) {

                                    email = insuranceCompany.getEmail();
                                    name = insuranceCompany.getName();
                                }
                            }
                        }
                    }
                    break;

                case INSERT_USER_AND_FLEET:

                    recipient = "INSERT_USER_AND_FLEET";
                    if (claimsEntity.getDamaged().getDriver() != null) {

                        Driver driver = claimsEntity.getDamaged().getDriver();
                        if (claimsEntity.getMetadata() != null && claimsEntity.getMetadata().getMetadata() != null
                                && claimsEntity.getMetadata().getMetadata().containsKey("email") &&
                                claimsEntity.getMetadata().getMetadata().get("email") != null &&
                                !((String) claimsEntity.getMetadata().getMetadata().get("email")).isEmpty()
                        ) {
                            email = (String) claimsEntity.getMetadata().getMetadata().get("email");
                            name = driver.getFirstname();

                        }
                    }

                    //identityList.add(new Identity(email, recipient, name));

                    //recipient = "INSERT_USER_AND_FLEET";

                    identityList = this.addFleetManagerEmail(identityList, claimsEntity, recipient);

                    /*String nameFleet = "", emailFleet = "";
                    if ((claimsEntity.getDamaged().getFleetManagerList() != null) && (claimsEntity.getDamaged().getFleetManagerList().size() > 0)) {
                        if(claimsEntity.getDamaged().getCustomer() != null && claimsEntity.getDamaged().getCustomer().getCustomerId() != null){
                            String customerId = claimsEntity.getDamaged().getCustomer().getCustomerId();
                            PersonalDataEntity personalDataEntity = personalDataRepository.findByCustomerId(customerId);
                            if(personalDataEntity != null){
                                List<FleetManagerPersonalData> fleetManagerPersonalData = personalDataEntity.getFleetManagerPersonalData();
                                if(fleetManagerPersonalData != null && !fleetManagerPersonalData.isEmpty()){
                                    for(FleetManager fleetManager : claimsEntity.getDamaged().getFleetManagerList()){
                                        if (fleetManager.getEmail() != null) {
                                            emailFleet = fleetManager.getEmail();
                                            nameFleet = fleetManager.getFirstName()+" "+fleetManager.getLastName();
                                        }
                                        FleetManagerPersonalData fleetManagerPersonalData2 = new FleetManagerPersonalData(fleetManager.getId());
                                        if(fleetManagerPersonalData.contains(fleetManagerPersonalData2)){
                                            for (FleetManagerPersonalData fleetManagerPersonalData1 : fleetManagerPersonalData){
                                                if(fleetManagerPersonalData1.equals(fleetManagerPersonalData2)){
                                                    if(fleetManagerPersonalData1.getFleetManagerSecondaryEmail() != null){
                                                        emailFleet = fleetManagerPersonalData1.getFleetManagerSecondaryEmail();
                                                        nameFleet = fleetManagerPersonalData1.getFleetManagerName();
                                                    }
                                                }
                                            }
                                        }
                                        identityList.add(new Identity(emailFleet,nameFleet,recipient));
                                    }
                                } else {
                                    for(FleetManager fleetManager : claimsEntity.getDamaged().getFleetManagerList()){
                                        if (fleetManager.getEmail() != null) {
                                            emailFleet = fleetManager.getEmail();
                                            nameFleet = fleetManager.getFirstName()+" "+fleetManager.getLastName();
                                        }
                                        identityList.add(new Identity(emailFleet,nameFleet,recipient));
                                    }
                                }
                            } else {
                                for(FleetManager fleetManager : claimsEntity.getDamaged().getFleetManagerList()){
                                    if (fleetManager.getEmail() != null) {
                                        emailFleet = fleetManager.getEmail();
                                        nameFleet = fleetManager.getFirstName()+" "+fleetManager.getLastName();
                                    }
                                    identityList.add(new Identity(emailFleet,nameFleet,recipient));
                                }
                            }
                        } else {
                            for(FleetManager fleetManager : claimsEntity.getDamaged().getFleetManagerList()){
                                if (fleetManager.getEmail() != null) {
                                    emailFleet = fleetManager.getEmail();
                                    nameFleet = fleetManager.getFirstName()+" "+fleetManager.getLastName();
                                }
                                identityList.add(new Identity(emailFleet,nameFleet,recipient));
                            }
                        }
                    }*/
                    break;
                default:
                    LOGGER.debug(MessageCode.CLAIMS_1115.value());
            }
        }
        if (email != null && !email.isEmpty()) {
            if (name == null)
                name = "";
            if (!recipient.equals("INSURED") && !recipient.equalsIgnoreCase("SEQUEL"))
                identityList.add(new Identity(email, name, recipient));
        }

        return identityList;
    }

    private List<Identity> addFleetManagerEmail(List<Identity> identities, ClaimsEntity claimsEntity, String recipient) {
        String nameFleet = "", emailFleet = "";
        if ((claimsEntity.getDamaged().getFleetManagerList() != null) && (claimsEntity.getDamaged().getFleetManagerList().size() > 0)) {
            if (claimsEntity.getDamaged().getCustomer() != null && claimsEntity.getDamaged().getCustomer().getCustomerId() != null) {
                String customerId = claimsEntity.getDamaged().getCustomer().getCustomerId();
                PersonalDataEntity personalDataEntity = personalDataRepository.findByCustomerId(customerId);
                if (personalDataEntity != null) {
                    List<FleetManagerPersonalData> fleetManagerPersonalData = personalDataEntity.getFleetManagerPersonalData();
                    if (fleetManagerPersonalData != null && !fleetManagerPersonalData.isEmpty()) {
                        for (FleetManager fleetManager : claimsEntity.getDamaged().getFleetManagerList()) {
                            if(!fleetManager.isDisableNotification()) {
                                if (fleetManager.getEmail() != null) {
                                    emailFleet = fleetManager.getEmail();
                                    nameFleet = fleetManager.getFirstName() + " " + fleetManager.getLastName();
                                }
                                FleetManagerPersonalData fleetManagerPersonalData2 = new FleetManagerPersonalData(fleetManager.getId());
                                for (FleetManagerPersonalData fleetManagerPersonalData1 : fleetManagerPersonalData) {
                                    if (fleetManagerPersonalData1.equalsFleetManager(fleetManagerPersonalData2)) {
                                        if (fleetManagerPersonalData1.getFleetManagerSecondaryEmail() != null &&
                                                !fleetManagerPersonalData1.getFleetManagerSecondaryEmail().isEmpty()) {
                                            emailFleet = fleetManagerPersonalData1.getFleetManagerSecondaryEmail();
                                            nameFleet = fleetManagerPersonalData1.getFleetManagerName();
                                        } else if (fleetManagerPersonalData1.getFleetManagerPrimaryEmail() != null &&
                                                !fleetManagerPersonalData1.getFleetManagerPrimaryEmail().isEmpty()) {
                                            emailFleet = fleetManagerPersonalData1.getFleetManagerPrimaryEmail();
                                            nameFleet = fleetManagerPersonalData1.getFleetManagerName();
                                        }
                                    }
                                }
                                identities.add(new Identity(emailFleet, nameFleet, recipient));
                            }
                        }
                    } else {
                        for (FleetManager fleetManager : claimsEntity.getDamaged().getFleetManagerList()) {
                            if(!fleetManager.isDisableNotification()) {
                                if (fleetManager.getEmail() != null) {
                                    emailFleet = fleetManager.getEmail();
                                    nameFleet = fleetManager.getFirstName() + " " + fleetManager.getLastName();
                                }
                                identities.add(new Identity(emailFleet, nameFleet, recipient));
                            }
                        }
                    }
                } else {
                    for (FleetManager fleetManager : claimsEntity.getDamaged().getFleetManagerList()) {
                        if(!fleetManager.isDisableNotification()) {
                            if (fleetManager.getEmail() != null) {
                                emailFleet = fleetManager.getEmail();
                                nameFleet = fleetManager.getFirstName() + " " + fleetManager.getLastName();
                            }
                            identities.add(new Identity(emailFleet, nameFleet, recipient));
                        }
                    }
                }
            } else {
                for (FleetManager fleetManager : claimsEntity.getDamaged().getFleetManagerList()) {
                    if(!fleetManager.isDisableNotification()) {
                        if (fleetManager.getEmail() != null) {
                            emailFleet = fleetManager.getEmail();
                            nameFleet = fleetManager.getFirstName() + " " + fleetManager.getLastName();
                        }
                        identities.add(new Identity(emailFleet, nameFleet, recipient));
                    }
                }
            }
        }

        return identities;
    }


    public List<EmailTemplateMessagingRequestV1> setEmailVariable(List<EmailTemplateMessagingRequestV1> emailTemplateMessagingRequestV1s, Map<String, String> emailVariable) {
        if (emailTemplateMessagingRequestV1s == null || emailTemplateMessagingRequestV1s.isEmpty() || emailVariable == null || emailVariable.isEmpty())
            return null;
        for (EmailTemplateMessagingRequestV1 emailTemplateEntity : emailTemplateMessagingRequestV1s) {
            for (String key : emailVariable.keySet()) {
                emailTemplateEntity.setBody(emailTemplateEntity.getBody().replaceAll(key, emailVariable.get(key)));
            }
        }
        return emailTemplateMessagingRequestV1s;
    }

    public List<EmailTemplateMessagingResponseV1> getMailTemplateByTypeEvent(EventTypeEnum typeEvent, ClaimsEntity claims) {
        ClaimsFlowEnum claimsFlow = claims.getType();
        DataAccidentTypeAccidentEnum claimsType = null;
        if (claims.getComplaint() !=null && claims.getComplaint().getDataAccident()!=null && claims.getComplaint().getDataAccident().getTypeAccident() != null) {
            claimsType = claims.getComplaint().getDataAccident().getTypeAccident();
        }

        String customerId = null;
        List<EmailTemplateEntity> emailTemplateListWithFlow;


        //CERCO PRIMA TEMPLATE CON TIPO SINISTRO
        if (claims.getDamaged() != null && claims.getDamaged().getCustomer() != null && Util.isNotEmpty(claims.getDamaged().getCustomer().getCustomerId())) {

            customerId = claims.getDamaged().getCustomer().getCustomerId();
            emailTemplateListWithFlow = emailTemplateRepositoryV1.getTemplateEntityByEventDescription(typeEvent.getValue(), claimsFlow.getValue(), customerId, null, claimsType.getValue());

            if (emailTemplateListWithFlow == null || emailTemplateListWithFlow.isEmpty())
                emailTemplateListWithFlow = emailTemplateRepositoryV1.getTemplateEntityByEventDescription(typeEvent.getValue(), claimsFlow.getValue(), null, null, claimsType.getValue());

        } else {
            emailTemplateListWithFlow = emailTemplateRepositoryV1.getTemplateEntityByEventDescription(typeEvent.getValue(), claimsFlow.getValue(), null, null, claimsType.getValue());
        }

        //SE NON HO TROVATO TEMPLATE CON TIPO SINISTRO CERCO TRA QUELLI CON TIPO SINISTRO NULL
        if(emailTemplateListWithFlow==null || emailTemplateListWithFlow.isEmpty()) {
            if (claims.getDamaged() != null && claims.getDamaged().getCustomer() != null && Util.isNotEmpty(claims.getDamaged().getCustomer().getCustomerId())) {

                customerId = claims.getDamaged().getCustomer().getCustomerId();
                emailTemplateListWithFlow = emailTemplateRepositoryV1.getTemplateEntityByEventDescription(typeEvent.getValue(), claimsFlow.getValue(), customerId, null, null);

                if (emailTemplateListWithFlow == null || emailTemplateListWithFlow.isEmpty())
                    emailTemplateListWithFlow = emailTemplateRepositoryV1.getTemplateEntityByEventDescription(typeEvent.getValue(), claimsFlow.getValue(), null, null, null);

            } else {
                emailTemplateListWithFlow = emailTemplateRepositoryV1.getTemplateEntityByEventDescription(typeEvent.getValue(), claimsFlow.getValue(), null, null, null);
            }
        }

        List<EmailTemplateMessagingResponseV1> emailTemplateMessagingResponseList = new LinkedList<>();

       /* List<EmailTemplateEntity> emailTemplateList = new LinkedList<>();
        for(EmailTemplateEntity emailTemplateEntity: emailTemplateListWithFlow){
            if(emailTemplateEntity.getFlows()!=null && emailTemplateEntity.getFlows().contains(claimsFlow))
                emailTemplateList.add(emailTemplateEntity);
        }*/

        if (emailTemplateListWithFlow != null && !emailTemplateListWithFlow.isEmpty()) {

            for (EmailTemplateEntity emailTemplate : emailTemplateListWithFlow) {


                if (emailTemplate.getFlows() != null && emailTemplate.getFlows().contains(claims.getType())) {

                    List<RecipientTypeEntity> recipientTypeEntityList = emailTemplate.getCustomers();
                    List<Identity> tos = new LinkedList<>();


                    for (RecipientTypeEntity recipientType : recipientTypeEntityList) {

                        Identity customer = new Identity();
                        if(recipientType.getActive() != null && recipientType.getActive()) {
                            if (!recipientType.getFixed()) {

                                //CHIAMATA A METODO findIdentityEmail
                                tos.addAll(this.findIdentityEmail(recipientType.getRecipientType(), claims, null));
                            } else {

                                tos.add(new Identity(recipientType.getEmail(), "", recipientType.getRecipientType().getValue()));
                            }
                        }
                    }

                    EmailTemplateMessagingResponseV1 emailTemplateMessaging = new EmailTemplateMessagingResponseV1();
                    emailTemplateMessaging.setTos(tos);
                    emailTemplateMessaging.setHeading(emailTemplate.getHeading());
                    emailTemplateMessaging.setBody(emailTemplate.getBody());
                    emailTemplateMessaging.setFoot(emailTemplate.getFoot());
                    emailTemplateMessaging.setObject(emailTemplate.getObject());
                    emailTemplateMessaging.setAttachFile(emailTemplate.getAttachFile());
                    emailTemplateMessaging.setSplittingRecipientsEmail(emailTemplate.getSplittingRecipientsEmail());

                    emailTemplateMessaging.setDescription(emailTemplate.getDescription());

                    this.parsContentOfMail(emailTemplateMessaging, claims, null, null);

                    emailTemplateMessagingResponseList.add(emailTemplateMessaging);
                }
            }
        }
        return emailTemplateMessagingResponseList;
    }


    @Override
    public String sendMailAndCreateLogsEnjoy(List<EmailTemplateMessagingRequestV1> listEmailTemplateMessagingRequestV1, String motivation, String claimsId) {

        String description = "";

        for (EmailTemplateMessagingRequestV1 emailTemplateMessagingRequestV1 : listEmailTemplateMessagingRequestV1) {

            if(emailTemplateMessagingRequestV1.getSplittingRecipientsEmail() == null || !emailTemplateMessagingRequestV1.getSplittingRecipientsEmail()){

                if (emailTemplateMessagingRequestV1.getObject() != null && emailTemplateMessagingRequestV1.getTos() != null && !emailTemplateMessagingRequestV1.getTos().isEmpty()) {

                    if (emailTemplateMessagingRequestV1.getDescription() != null)
                        description += emailTemplateMessagingRequestV1.getDescription() + "<br/>INVIATO IL: " + DateUtil.getNowDate().toString() + "<br/>FROM: no-reply@ald.automotivedn.com/nTO: ";
                    else
                        description += "INVIATO IL: " + DateUtil.getNowDate().toString() + "<br/>FROM: no-reply@ald.automotivedn.com/nTO: ";

                    for (Identity identity : emailTemplateMessagingRequestV1.getTos()) {
                        description += identity.getEmail() + ", ";
                    }
                    description = description.substring(0, description.length() - 1);

                } else if (emailTemplateMessagingRequestV1.getObject() == null) {
                    description += "DESTINATARI: ";
                    for (Identity identity : emailTemplateMessagingRequestV1.getTos()) {
                        description += identity.getEmail() + ", ";
                    }
                    description = description.substring(0, description.length() - 1);
                }

                description += "<br/>CC:";
                if (emailTemplateMessagingRequestV1.getCcs() != null && !emailTemplateMessagingRequestV1.getCcs().isEmpty()) {
                    for (Identity identity : emailTemplateMessagingRequestV1.getCcs()) {
                        description += identity.getEmail() + ", ";
                    }
                    description = description.substring(0, description.length() - 1);
                }

                description += "<br/>CCN:";
                if (emailTemplateMessagingRequestV1.getBccs() != null && !emailTemplateMessagingRequestV1.getBccs().isEmpty()) {
                    for (Identity identity : emailTemplateMessagingRequestV1.getBccs()) {
                        description += identity.getEmail() + ", ";
                    }
                    description = description.substring(0, description.length() - 1);
                }


                description += "<br/>OGGETTO: ";
                if (emailTemplateMessagingRequestV1.getObject() != null) {

                    description += emailTemplateMessagingRequestV1.getObject();
                } else {
                    description += "Nessuno. ";
                }

                if (emailTemplateMessagingRequestV1.getHeading() != null) {
                    description += "<br/>" + emailTemplateMessagingRequestV1.getHeading();
                }

                if (emailTemplateMessagingRequestV1.getBody() != null) {
                    description += "<br/>" + emailTemplateMessagingRequestV1.getBody();

                    if (motivation != null) {
                        description = description.replace("<%REATYPDESTY%>", motivation);
                        if(description.contains("<%REATYPDESTYP%>") && description.contains("<%REASDESCCOMP%>")) {
                            description= description.replace("<%REATYPDESTYP%>", motivation);
                            description= description.replace("<%REASDESCCOMP%>", "");
                        }else{
                            description= description.replace("<%REATYPDESTYP%>", motivation);
                            description= description.replace("<%REASDESCCOMP%>", motivation);
                        }
                    }
                } else {
                    description += "<br/>Nessun body.";
                }

                if (emailTemplateMessagingRequestV1.getFoot() != null) {
                    description += "<br/>" + emailTemplateMessagingRequestV1.getFoot() + "<br/>";
                }

                if (emailTemplateMessagingRequestV1.getTos() == null || emailTemplateMessagingRequestV1.getTos().isEmpty()) {
                    description += "<br/>EMAIL NON INVIATA POICHE' NON E' PRESENTE IL DESTINATARIO<br/>";
                }

                if (emailTemplateMessagingRequestV1.getObject() == null) {
                    description += "<br/>EMAIL NON INVIATA PER MANCANZA DI OGGETTO.<br/>";
                }

                description += "<br/><br/>";
            }else{

                if(emailTemplateMessagingRequestV1.getTos() != null ) {
                    for(Identity currentIdentity: emailTemplateMessagingRequestV1.getTos()) {

                        if (emailTemplateMessagingRequestV1.getObject() != null && emailTemplateMessagingRequestV1.getTos() != null && !emailTemplateMessagingRequestV1.getTos().isEmpty()) {


                            if (emailTemplateMessagingRequestV1.getDescription() != null) {
                                description += emailTemplateMessagingRequestV1.getDescription() + "<br/>INVIATO IL: " + DateUtil.getNowDate().toString() + "<br/>FROM: no-reply@ald.automotivedn.com/nTO: ";
                            }else {
                                description += "INVIATO IL: " + DateUtil.getNowDate().toString() + "<br/>FROM: no-reply@ald.automotivedn.com/nTO: ";
                            }


                            description += currentIdentity.getEmail() + ", ";

                        } else if (emailTemplateMessagingRequestV1.getObject() == null) {
                            description += "DESTINATARI: ";

                            description += currentIdentity.getEmail() + ", ";


                        }

                        description += "<br/>CC:";
                        if (emailTemplateMessagingRequestV1.getCcs() != null && !emailTemplateMessagingRequestV1.getCcs().isEmpty()) {
                            for (Identity identity : emailTemplateMessagingRequestV1.getCcs()) {
                                description += identity.getEmail() + ", ";
                            }
                            description = description.substring(0, description.length() - 1);
                        }

                        description += "<br/>CCN:";
                        if (emailTemplateMessagingRequestV1.getBccs() != null && !emailTemplateMessagingRequestV1.getBccs().isEmpty()) {
                            for (Identity identity : emailTemplateMessagingRequestV1.getBccs()) {
                                description += identity.getEmail() + ", ";
                            }
                            description = description.substring(0, description.length() - 1);
                        }


                        description += "<br/>OGGETTO: ";
                        if (emailTemplateMessagingRequestV1.getObject() != null) {

                            description += emailTemplateMessagingRequestV1.getObject();
                        } else {
                            description += "Nessuno. ";
                        }

                        if (emailTemplateMessagingRequestV1.getHeading() != null) {
                            description += "<br/>" + emailTemplateMessagingRequestV1.getHeading();
                        }

                        if (emailTemplateMessagingRequestV1.getBody() != null) {
                            description += "<br/>" + emailTemplateMessagingRequestV1.getBody();

                            if (motivation != null) {
                                description = description.replace("<%REATYPDESTY%>", motivation);
                                if(description.contains("<%REATYPDESTYP%>") && description.contains("<%REASDESCCOMP%>")) {
                                    description= description.replace("<%REATYPDESTYP%>", motivation);
                                    description= description.replace("<%REASDESCCOMP%>", "");
                                }else{
                                    description= description.replace("<%REATYPDESTYP%>", motivation);
                                    description= description.replace("<%REASDESCCOMP%>", motivation);
                                }
                            }
                        } else {
                            description += "<br/>Nessun body.";
                        }

                        if (emailTemplateMessagingRequestV1.getFoot() != null) {
                            description += "<br/>" + emailTemplateMessagingRequestV1.getFoot() + "<br/>";
                        }

                        if (emailTemplateMessagingRequestV1.getTos() == null || emailTemplateMessagingRequestV1.getTos().isEmpty()) {
                            description += "<br/>EMAIL NON INVIATA POICHE' NON E' PRESENTE IL DESTINATARIO<br/>";
                        }

                        if (emailTemplateMessagingRequestV1.getObject() == null) {
                            description += "<br/>EMAIL NON INVIATA PER MANCANZA DI OGGETTO.<br/>";
                        }

                        description += "<br/><br/>";
                    }
                }
            }


        }

        try {
            this.sendEmailEnjoy(removeTemplateWithoutTos(listEmailTemplateMessagingRequestV1), motivation, claimsId);
        }catch(InternalException ex){
            description += "<br/>EMAIL NON INVIATA.<br/><br/>";
        }

        return description;
    }

    @Override
    public String sendMailAndCreateLogs(List<EmailTemplateMessagingRequestV1> listEmailTemplateMessagingRequestV1, String motivation, Instant createdAt) {

        String description = this.createLogs(listEmailTemplateMessagingRequestV1,motivation,createdAt);
        this.sendEmail(removeTemplateWithoutTos(listEmailTemplateMessagingRequestV1), motivation, createdAt );

        return description;
    }


    private String createLogs(List<EmailTemplateMessagingRequestV1> listEmailTemplateMessagingRequestV1, String motivation, Instant createdAt){
        String description = "";

        for (EmailTemplateMessagingRequestV1 emailTemplateMessagingRequestV1 : listEmailTemplateMessagingRequestV1) {

            if(emailTemplateMessagingRequestV1.getSplittingRecipientsEmail() == null || !emailTemplateMessagingRequestV1.getSplittingRecipientsEmail()){

                if (emailTemplateMessagingRequestV1.getObject() != null && emailTemplateMessagingRequestV1.getTos() != null && !emailTemplateMessagingRequestV1.getTos().isEmpty()) {

                    if (emailTemplateMessagingRequestV1.getDescription() != null)
                        description += emailTemplateMessagingRequestV1.getDescription() + "<br/>INVIATO IL: " + DateUtil.getNowDate().toString() + "<br/>FROM: no-reply@ald.automotivedn.com/nTO: ";
                    else
                        description += "INVIATO IL: " + DateUtil.getNowDate().toString() + "<br/>FROM: no-reply@ald.automotivedn.com/nTO: ";

                    for (Identity identity : emailTemplateMessagingRequestV1.getTos()) {
                        description += identity.getEmail() + ", ";
                    }
                    description = description.substring(0, description.length() - 1);

                } else if (emailTemplateMessagingRequestV1.getObject() == null) {
                    description += "DESTINATARI: ";
                    for (Identity identity : emailTemplateMessagingRequestV1.getTos()) {
                        description += identity.getEmail() + ", ";
                    }
                    description = description.substring(0, description.length() - 1);
                }

                description += "<br/>CC:";
                if (emailTemplateMessagingRequestV1.getCcs() != null && !emailTemplateMessagingRequestV1.getCcs().isEmpty()) {
                    for (Identity identity : emailTemplateMessagingRequestV1.getCcs()) {
                        description += identity.getEmail() + ", ";
                    }
                    description = description.substring(0, description.length() - 1);
                }

                description += "<br/>CCN:";
                if (emailTemplateMessagingRequestV1.getBccs() != null && !emailTemplateMessagingRequestV1.getBccs().isEmpty()) {
                    for (Identity identity : emailTemplateMessagingRequestV1.getBccs()) {
                        description += identity.getEmail() + ", ";
                    }
                    description = description.substring(0, description.length() - 1);
                }


                description += "<br/>OGGETTO: ";
                if (emailTemplateMessagingRequestV1.getObject() != null) {

                    description += emailTemplateMessagingRequestV1.getObject();
                } else {
                    description += "Nessuno. ";
                }

                if (emailTemplateMessagingRequestV1.getHeading() != null) {
                    description += "<br/>" + emailTemplateMessagingRequestV1.getHeading();
                }

                if (emailTemplateMessagingRequestV1.getBody() != null) {
                    description += "<br/>" + emailTemplateMessagingRequestV1.getBody();
                    if (motivation != null) {
                        description = description.replace("<%REATYPDESTY%>", motivation);
                        if(description.contains("<%REATYPDESTYP%>") && description.contains("<%REASDESCCOMP%>")) {
                            description= description.replace("<%REATYPDESTYP%>", motivation);
                            description= description.replace("<%REASDESCCOMP%>", "");
                        }else{
                            description= description.replace("<%REATYPDESTYP%>", motivation);
                            description= description.replace("<%REASDESCCOMP%>", motivation);
                        }
                    } else {
                        description = description.replace("<%REATYPDESTY%>", "");
                        description = description.replace("<%REATYPDESTYP%>", "");
                        description = description.replace("<%REASDESCCOMP%>", "");
                    }
                    if(createdAt!=null){
                        description = description.replace("<%REASDESCCOMP%>", DateUtil.getDateStringWithSeparationCharacters(DateUtil.convertIS08601StringToUTCDate(DateUtil.convertUTCInstantToIS08601String(createdAt))));
                        description = description.replace("<%REICDATECOMP%>", DateUtil.getDateStringWithSeparationCharacters(DateUtil.convertIS08601StringToUTCDate(DateUtil.convertUTCInstantToIS08601String(createdAt))));
                    }
                } else {
                    description += "<br/>Nessun body.";
                }

                if (emailTemplateMessagingRequestV1.getFoot() != null) {
                    description += "<br/>" + emailTemplateMessagingRequestV1.getFoot() + "<br/>";
                }

                if (emailTemplateMessagingRequestV1.getTos() == null || emailTemplateMessagingRequestV1.getTos().isEmpty()) {
                    description += "<br/>EMAIL NON INVIATA POICHE' NON E' PRESENTE IL DESTINATARIO<br/>";
                }

                if (emailTemplateMessagingRequestV1.getObject() == null) {
                    description += "<br/>EMAIL NON INVIATA PER MANCANZA DI OGGETTO.<br/>";
                }

                description += "<br/><br/>";
            }else{

                if(emailTemplateMessagingRequestV1.getTos() != null ) {
                    for(Identity currentIdentity: emailTemplateMessagingRequestV1.getTos()) {

                        if (emailTemplateMessagingRequestV1.getObject() != null && emailTemplateMessagingRequestV1.getTos() != null && !emailTemplateMessagingRequestV1.getTos().isEmpty()) {


                            if (emailTemplateMessagingRequestV1.getDescription() != null) {
                                description += emailTemplateMessagingRequestV1.getDescription() + "<br/>INVIATO IL: " + DateUtil.getNowDate().toString() + "<br/>FROM: no-reply@ald.automotivedn.com/nTO: ";
                            }else {
                                description += "INVIATO IL: " + DateUtil.getNowDate().toString() + "<br/>FROM: no-reply@ald.automotivedn.com/nTO: ";
                            }


                            description += currentIdentity.getEmail() + ", ";

                        } else if (emailTemplateMessagingRequestV1.getObject() == null) {
                            description += "DESTINATARI: ";

                            description += currentIdentity.getEmail() + ", ";


                        }

                        description += "<br/>CC:";
                        if (emailTemplateMessagingRequestV1.getCcs() != null && !emailTemplateMessagingRequestV1.getCcs().isEmpty()) {
                            for (Identity identity : emailTemplateMessagingRequestV1.getCcs()) {
                                description += identity.getEmail() + ", ";
                            }
                            description = description.substring(0, description.length() - 1);
                        }

                        description += "<br/>CCN:";
                        if (emailTemplateMessagingRequestV1.getBccs() != null && !emailTemplateMessagingRequestV1.getBccs().isEmpty()) {
                            for (Identity identity : emailTemplateMessagingRequestV1.getBccs()) {
                                description += identity.getEmail() + ", ";
                            }
                            description = description.substring(0, description.length() - 1);
                        }


                        description += "<br/>OGGETTO: ";
                        if (emailTemplateMessagingRequestV1.getObject() != null) {

                            description += emailTemplateMessagingRequestV1.getObject();
                        } else {
                            description += "Nessuno. ";
                        }

                        if (emailTemplateMessagingRequestV1.getHeading() != null) {
                            description += "<br/>" + emailTemplateMessagingRequestV1.getHeading();
                        }

                        if (emailTemplateMessagingRequestV1.getBody() != null) {
                            description += "<br/>" + emailTemplateMessagingRequestV1.getBody();

                            if (motivation != null) {
                                description = description.replace("<%REATYPDESTY%>", motivation);
                                if(description.contains("<%REATYPDESTYP%>") && description.contains("<%REASDESCCOMP%>")) {
                                    description= description.replace("<%REATYPDESTYP%>", motivation);
                                    description= description.replace("<%REASDESCCOMP%>", "");
                                }else{
                                    description= description.replace("<%REATYPDESTYP%>", motivation);
                                    description= description.replace("<%REASDESCCOMP%>", motivation);
                                }
                            } else {
                                description = description.replace("<%REATYPDESTY%>", "");
                                description = description.replace("<%REATYPDESTYP%>", "");
                                description = description.replace("<%REASDESCCOMP%>", "");
                            }
                            if(createdAt!=null){
                                description = description.replace("<%REASDESCCOMP%>", DateUtil.getDateStringWithSeparationCharacters(DateUtil.convertIS08601StringToUTCDate(DateUtil.convertUTCInstantToIS08601String(createdAt))));
                            }
                        } else {
                            description += "<br/>Nessun body.";
                        }

                        if (emailTemplateMessagingRequestV1.getFoot() != null) {
                            description += "<br/>" + emailTemplateMessagingRequestV1.getFoot() + "<br/>";
                        }

                        if (emailTemplateMessagingRequestV1.getTos() == null || emailTemplateMessagingRequestV1.getTos().isEmpty()) {
                            description += "<br/>EMAIL NON INVIATA POICHE' NON E' PRESENTE IL DESTINATARIO<br/>";
                        }

                        if (emailTemplateMessagingRequestV1.getObject() == null) {
                            description += "<br/>EMAIL NON INVIATA PER MANCANZA DI OGGETTO.<br/>";
                        }

                        description += "<br/><br/>";
                    }
                }
            }


        }
        return description;
    }

    @Override
    public String createLogs(List<EmailTemplateMessagingRequestV1> listEmailTemplateMessagingRequestV1, String motivation) {

        String description = "";

        for (EmailTemplateMessagingRequestV1 emailTemplateMessagingRequestV1 : listEmailTemplateMessagingRequestV1) {

            if (emailTemplateMessagingRequestV1.getObject() != null && emailTemplateMessagingRequestV1.getTos() != null && !emailTemplateMessagingRequestV1.getTos().isEmpty()) {

                if (emailTemplateMessagingRequestV1.getDescription() != null)
                    description += emailTemplateMessagingRequestV1.getDescription() + "<br/>INVIATO IL: " + DateUtil.getNowDate().toString() + "<br/>FROM: no-reply@ald.automotivedn.com/nTO: ";
                else
                    description += "INVIATO IL: " + DateUtil.getNowDate().toString() + "<br/>FROM: no-reply@ald.automotivedn.com/nTO: ";

                for (Identity identity : emailTemplateMessagingRequestV1.getTos()) {
                    description += identity.getEmail() + ", ";
                }
                description = description.substring(0, description.length() - 1);

            } else if (emailTemplateMessagingRequestV1.getObject() == null){
                description += "DESTINATARI: ";
                for (Identity identity : emailTemplateMessagingRequestV1.getTos()) {
                    description += identity.getEmail() + ", ";
                }
                description = description.substring(0, description.length() - 1);
            }

            description += "<br/>CC:";
            if (emailTemplateMessagingRequestV1.getCcs() != null && !emailTemplateMessagingRequestV1.getCcs().isEmpty()) {
                for (Identity identity : emailTemplateMessagingRequestV1.getTos()) {
                    description += identity.getEmail() + ", ";
                }
                description = description.substring(0, description.length() - 1);
            }

            description += "<br/>CCN:";
            if (emailTemplateMessagingRequestV1.getBccs() != null && !emailTemplateMessagingRequestV1.getBccs().isEmpty()) {
                for (Identity identity : emailTemplateMessagingRequestV1.getTos()) {
                    description += identity.getEmail() + ", ";
                }
                description = description.substring(0, description.length() - 1);
            }


            description += "<br/>OGGETTO: ";
            if (emailTemplateMessagingRequestV1.getObject() != null) {

                description += emailTemplateMessagingRequestV1.getObject();
            } else {
                description += "Nessuno. ";
            }

            if (emailTemplateMessagingRequestV1.getHeading() != null) {
                description += "<br/>" + emailTemplateMessagingRequestV1.getHeading();
            }

            if (emailTemplateMessagingRequestV1.getBody() != null) {
                description += "<br/>" + emailTemplateMessagingRequestV1.getBody();

                if (motivation != null) {
                    description = description.replace("<%REATYPDESTY%>", motivation);
                    if(description.contains("<%REATYPDESTYP%>") && description.contains("<%REASDESCCOMP%>")) {
                        description= description.replace("<%REATYPDESTYP%>", motivation);
                        description= description.replace("<%REASDESCCOMP%>", "");
                    }else{
                        description= description.replace("<%REATYPDESTYP%>", motivation);
                        description= description.replace("<%REASDESCCOMP%>", motivation);
                    }
                }
            } else {
                description += "<br/>Nessun body.";
            }

            if (emailTemplateMessagingRequestV1.getFoot() != null) {
                description += "<br/>" + emailTemplateMessagingRequestV1.getFoot() + "<br/>";
            }

            if (emailTemplateMessagingRequestV1.getTos() == null || emailTemplateMessagingRequestV1.getTos().isEmpty()) {
                description += "<br/>EMAIL NON INVIATA POICHE' NON E' PRESENTE IL DESTINATARIO<br/>";
            }

            if(emailTemplateMessagingRequestV1.getObject() == null){
                description += "<br/>EMAIL NON INVIATA PER MANCANZA DI OGGETTO.<br/>";
            }

            description += "<br/><br/>";
        }

        return description;
    }


    private static List<EmailTemplateMessagingRequestV1> removeTemplateWithoutTos(List<EmailTemplateMessagingRequestV1> emailTemplateMessagingRequestV1s) {
        //Elimina i template senza destinatari e i destinatari senza e-mail
        List<EmailTemplateMessagingRequestV1> emailTemplateMessagingFinal = new LinkedList<>();
        for (EmailTemplateMessagingRequestV1 emailTemplateMessagingRequestV1 : emailTemplateMessagingRequestV1s) {

            if (emailTemplateMessagingRequestV1.getTos() != null && !emailTemplateMessagingRequestV1.getTos().isEmpty()) {

                List<Identity> identityList = new LinkedList<>();

                for (Identity identity : emailTemplateMessagingRequestV1.getTos()) {
                    if (identity.getEmail() != null && !identity.getEmail().isEmpty()) {
                        identityList.add(identity);
                    }
                }

                if (!identityList.isEmpty() && emailTemplateMessagingRequestV1.getObject() != null) {
                    emailTemplateMessagingRequestV1.setTos(identityList);
                    emailTemplateMessagingFinal.add(emailTemplateMessagingRequestV1);
                }
            }
        }
        return emailTemplateMessagingFinal;
    }

    @Override
    public List<EmailTemplateInternalMessagingResponseV1> getMailTemplateByTypeEventListAndClaimsInternal(List<EventTypeEnum> typeEventList, ClaimsEntity claims) {
        List<EmailTemplateInternalMessagingResponseV1> emailTemplateMessagingResponseV1List = new ArrayList<>();
        for (EventTypeEnum eventTypeEnum : typeEventList) {
            EmailTemplateInternalMessagingResponseV1 emailTemplateInternalMessagingResponseV1 = new EmailTemplateInternalMessagingResponseV1();
            emailTemplateInternalMessagingResponseV1.setEventType(eventTypeEnum);
            emailTemplateInternalMessagingResponseV1.setEmailTemplate(this.getMailTemplateByTypeEventAndClaimsInternal(eventTypeEnum, claims));
            emailTemplateMessagingResponseV1List.add(emailTemplateInternalMessagingResponseV1);
        }
        return emailTemplateMessagingResponseV1List;
    }

    @Override
    public List<EmailTemplateMessagingResponseV1> getMailTemplateByTypeEventAndClaimsInternal(EventTypeEnum typeEvent, ClaimsEntity claims) {


        if (typeEvent == null)
            typeEvent = EventTypeEnum.COMPLAINT_INSERTION;

        ClaimsFlowEnum claimsFlow = claims.getType();
        DataAccidentTypeAccidentEnum claimsType = null;
        if (claims.getComplaint() !=null && claims.getComplaint().getDataAccident()!=null && claims.getComplaint().getDataAccident().getTypeAccident() != null) {
            claimsType = claims.getComplaint().getDataAccident().getTypeAccident();
        }

        String customerId = null;
        List<EmailTemplateEntity> emailTemplateListWithFlow;

        //CERCO PRIMA TEMPLATE CON TIPO SINISTRO
        if (claims.getDamaged() != null && claims.getDamaged().getCustomer() != null && Util.isNotEmpty(claims.getDamaged().getCustomer().getCustomerId())) {

            customerId = claims.getDamaged().getCustomer().getCustomerId();
            emailTemplateListWithFlow = emailTemplateRepositoryV1.getTemplateEntityByEventDescription(typeEvent.getValue(), claimsFlow.getValue(), customerId, null, claimsType.getValue());

            if (emailTemplateListWithFlow == null || emailTemplateListWithFlow.isEmpty())
                emailTemplateListWithFlow = emailTemplateRepositoryV1.getTemplateEntityByEventDescription(typeEvent.getValue(), claimsFlow.getValue(), null, null, claimsType.getValue());

        } else {
            emailTemplateListWithFlow = emailTemplateRepositoryV1.getTemplateEntityByEventDescription(typeEvent.getValue(), claimsFlow.getValue(), null, null, claimsType.getValue());
        }

        //SE NON HO TROVATO TEMPLATE CON TIPO SINISTRO CERCO TRA QUELLI CON TIPO SINISTRO NULL
        if(emailTemplateListWithFlow==null || emailTemplateListWithFlow.isEmpty()) {
            if (claims.getDamaged() != null && claims.getDamaged().getCustomer() != null && Util.isNotEmpty(claims.getDamaged().getCustomer().getCustomerId())) {

                customerId = claims.getDamaged().getCustomer().getCustomerId();
                emailTemplateListWithFlow = emailTemplateRepositoryV1.getTemplateEntityByEventDescription(typeEvent.getValue(), claimsFlow.getValue(), customerId, null, null);

                if (emailTemplateListWithFlow == null || emailTemplateListWithFlow.isEmpty())
                    emailTemplateListWithFlow = emailTemplateRepositoryV1.getTemplateEntityByEventDescription(typeEvent.getValue(), claimsFlow.getValue(), null, null, null);

            } else {
                emailTemplateListWithFlow = emailTemplateRepositoryV1.getTemplateEntityByEventDescription(typeEvent.getValue(), claimsFlow.getValue(), null, null, null);
            }
        }

        List<EmailTemplateMessagingResponseV1> emailTemplateMessagingResponseList = new LinkedList<>();

       /* List<EmailTemplateEntity> emailTemplateList = new LinkedList<>();
        for(EmailTemplateEntity emailTemplateEntity: emailTemplateListWithFlow){
            if(emailTemplateEntity.getFlows()!=null && emailTemplateEntity.getFlows().contains(claimsFlow))
                emailTemplateList.add(emailTemplateEntity);
        }*/

        if (emailTemplateListWithFlow != null && !emailTemplateListWithFlow.isEmpty()) {

            for (EmailTemplateEntity emailTemplate : emailTemplateListWithFlow) {

                if (emailTemplate.getFlows() != null && emailTemplate.getFlows().contains(claims.getType())) {

                    List<RecipientTypeEntity> recipientTypeEntityList = emailTemplate.getCustomers();

                    CounterpartyEntity counterparty = this.getCounterpartyResponsable(claims.getCounterparts());
                    if(counterparty != null && counterparty.getAld() != null && counterparty.getAld()) {
                        recipientTypeEntityList.removeIf(recipientTypeEntity -> recipientTypeEntity.getRecipientType().equals(RecipientEnum.COMPANY));
                    }


                    List<Identity> tos = new LinkedList<>();


                    for (RecipientTypeEntity recipientType : recipientTypeEntityList) {

                        Identity customer = new Identity();
                        if(recipientType.getActive() != null && recipientType.getActive()) {
                            if (!recipientType.getFixed()) {

                                //CHIAMATA A METODO findIdentityEmail
                                tos.addAll(this.findIdentityEmail(recipientType.getRecipientType(), claims, null));
                            } else {

                                tos.add(new Identity(recipientType.getEmail(), "", recipientType.getRecipientType().getValue()));
                            }
                        }
                    }

                    EmailTemplateMessagingResponseV1 emailTemplateMessaging = new EmailTemplateMessagingResponseV1();
                    emailTemplateMessaging.setTos(tos);
                    emailTemplateMessaging.setHeading(emailTemplate.getHeading());
                    emailTemplateMessaging.setBody(emailTemplate.getBody());
                    emailTemplateMessaging.setFoot(emailTemplate.getFoot());
                    emailTemplateMessaging.setObject(emailTemplate.getObject());
                    emailTemplateMessaging.setAttachFile(emailTemplate.getAttachFile());
                    emailTemplateMessaging.setSplittingRecipientsEmail(emailTemplate.getSplittingRecipientsEmail());

                    emailTemplateMessaging.setDescription(emailTemplate.getDescription());


                    this.parsContentOfMail(emailTemplateMessaging, claims, null,null);

                    emailTemplateMessagingResponseList.add(emailTemplateMessaging);
                }
            }
        }
        return emailTemplateMessagingResponseList;

    }

    //REFACTOR
    @Override
    public HttpStatus sendEmailPoVariation(List<EmailTemplateMessagingRequestV1> emailTemplateMessagingRequestList, String claimsId, String userId, String userName) throws IOException {

        HttpStatus responseStatus = HttpStatus.BAD_REQUEST;

        if(emailTemplateMessagingRequestList != null && !emailTemplateMessagingRequestList.isEmpty()) {
            List<EmailTemplateMessagingRequestV1> listTos = new LinkedList<>();
            List<EmailTemplateMessagingRequestV1> listNotTos = new LinkedList<>();
            for (EmailTemplateMessagingRequestV1 currentTemplate : emailTemplateMessagingRequestList) {
                if (currentTemplate.getTos() != null && !currentTemplate.getTos().isEmpty() && currentTemplate.getObject()!= null && !currentTemplate.getObject().isEmpty()) {
                    listTos.add(currentTemplate);
                } else {
                    listNotTos.add(currentTemplate);
                }
            }

            //funzione refactorizzata
            List<EmailTemplateMessagingRequestV1> emailTemplateMessaging = splitEmailIfContainsUrl(listTos, claimsId);
            responseStatus = this.sendEmail(emailTemplateMessaging, null, null);
            emailTemplateMessaging.addAll(listNotTos);

            if (responseStatus.equals(HttpStatus.ACCEPTED)) {
                //recupero nuova entità
                Optional<ClaimsNewEntity> optionalClaimsNewEntity = claimsNewRepository.findById(claimsId);
                if (optionalClaimsNewEntity.isPresent()) {
                    //conversione nella vecchia entità
                    ClaimsEntity claims = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(optionalClaimsNewEntity.get());
                    String description = "";

                    description = this.createLogs(emailTemplateMessaging, null);

                    Historical historical = new Historical(EventTypeEnum.NOTIFYING_VARIATION_PO, claims.getStatus(), claims.getStatus(), claims.getComplaint().getDataAccident().getTypeAccident(), claims.getComplaint().getDataAccident().getTypeAccident(), new Date(), userId, userName, claims.getType().getValue(), claims.getType().getValue(), description);
                    claims.addHistorical(historical);

                    //conversione nella nuova entità
                    ClaimsNewEntity claimsNewEntity = converterClaimsService.convertOldEntityToNewClaimsEntity(claims);
                    claimsNewEntity.setPoVariation(false);
                    claimsNewRepository.save(claimsNewEntity);
                }
            }

        }
        return responseStatus;
    }

    private CounterpartyEntity getCounterpartyResponsable(List<CounterpartyEntity> counterpartyList) {
        if (counterpartyList == null || counterpartyList.isEmpty())
            return null;
        if (counterpartyList.size() == 1)
            return counterpartyList.get(0);
        Iterator<CounterpartyEntity> iterator = counterpartyList.iterator();
        boolean isResponsible = false;
        while (iterator.hasNext() && !isResponsible) {
            CounterpartyEntity counterparty = iterator.next();
            if (counterparty.getResponsible() != null && counterparty.getResponsible())
                return counterparty;
        }
        return null;
    }

    private  NemoAuthExternalResposeV1 getExternalTokenString(ClaimsEntity claimsEntity, Identity toEmailList, Boolean isIncomplete) throws IOException {


        String entrusterType = claimsEntity.getComplaint() != null && claimsEntity.getComplaint().getEntrusted() != null && claimsEntity.getComplaint().getEntrusted().getEntrustedType() != null ? claimsEntity.getComplaint().getEntrusted().getEntrustedType().getValue().toUpperCase() : null;
        String entrusterId = claimsEntity.getComplaint() != null && claimsEntity.getComplaint().getEntrusted() != null && claimsEntity.getComplaint().getEntrusted().getIdEntrustedTo() != null ? claimsEntity.getComplaint().getEntrusted().getIdEntrustedTo().toString() : null;

        NemoAuthExternalMetadataRequestV1 nemoAuthExternalMetadataRequest = new NemoAuthExternalMetadataRequestV1();
        nemoAuthExternalMetadataRequest.setClaimId(claimsEntity.getId());
        nemoAuthExternalMetadataRequest.setClaimPracticeId(claimsEntity.getPracticeId().toString());
        nemoAuthExternalMetadataRequest.setEmail(toEmailList.getEmail());
        if(!isIncomplete){
            nemoAuthExternalMetadataRequest.setEntrusterType(entrusterType);
            nemoAuthExternalMetadataRequest.setEntrusterId(entrusterId);
        }

        NemoAuthExternalResposeV1 nemoAuthExternalRespose = new NemoAuthExternalResposeV1();
        nemoAuthExternalRespose.setEmail(toEmailList.getEmail());
        nemoAuthExternalRespose.setUrl(nemoAuthService.getExternalToken(nemoAuthExternalMetadataRequest,claimsEntity.getId(),claimsEntity.getPracticeId().toString(), isIncomplete));

        return nemoAuthExternalRespose;
    }

    @Override
    public List<EmailTemplateMessagingRequestV1> splitEmailIfContainsUrl(List<EmailTemplateMessagingRequestV1> emailTemplateRequestList, String idClaims) {
        return splitEmailIfContainsUrl(emailTemplateRequestList,idClaims,false);
    }
    //REFACTOR
    @Override
    public List<EmailTemplateMessagingRequestV1> splitEmailIfContainsUrl(List<EmailTemplateMessagingRequestV1> emailTemplateRequestList, String idClaims, boolean isIncomplete){
        //recupero nuova entità
        Optional<ClaimsNewEntity> optionalClaimsNewEntity = claimsNewRepository.findById(idClaims);

        if(optionalClaimsNewEntity.isPresent()){
            //converto nella vecchia entità
            ClaimsNewEntity claimsNewEntity = optionalClaimsNewEntity.get();
            ClaimsEntity claims = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntity);
            List<EmailTemplateMessagingRequestV1> listFinal = new ArrayList<>();
            for( EmailTemplateMessagingRequestV1 currentEmail : emailTemplateRequestList){
                    String contentBody = "";

                if(currentEmail.getBody() != null && (currentEmail.getBody().contains("<%REPOSPUBLINK_EXTERNAL%>"))){
                                        //Assegnazione del destinatario

                    for(Identity currentTo : currentEmail.getTos()){
                        String linkType = null;
                        EmailTemplateMessagingRequestV1 email = new EmailTemplateMessagingRequestV1();
                        String url = null;
                        try {

                            email.setHeading(currentEmail.getHeading());
                            email.setObject(currentEmail.getObject());
                            email.setDescription(currentEmail.getDescription());
                            email.setCcs(currentEmail.getCcs());
                            email.setBccs(currentEmail.getBccs());
                            email.setFoot(currentEmail.getFoot());
                            email.setSplittingRecipientsEmail(currentEmail.getSplittingRecipientsEmail());

                            LinkedList<Identity> tos = new LinkedList<>();
                            tos.add(currentTo);
                            email.setTos(tos);



                            if(currentEmail.getBody().contains("<%REPOSPUBLINK_EXTERNAL%>")) {
                                //Generazione del token e creazione dell'url unico per ogni singolo destinatario
                                NemoAuthExternalResposeV1 nemoAuthExternalRespose = this.getExternalTokenString(claims, currentTo,false);
                                url= nemoAuthExternalRespose.getUrl();
                                linkType = "<%REPOSPUBLINK_EXTERNAL%>";
                            }

                            //GENERAZIONE URL UTENTI ESTERNI
                            if( url != null &&  !url.isEmpty() && !linkType.isEmpty()){
                                contentBody = currentEmail.getBody().replaceAll(linkType, url);
                            }else{
                                contentBody = currentEmail.getBody();
                            }

                            email.setBody(contentBody);

                            System.out.println(email);

                            listFinal.add(email);



                        } catch (Exception e) {
                            LOGGER.debug("Token error external link - MessageCode.CLAIMS_2004 - " + e.getMessage());
                            //throw new BadRequestException(MessageCode.CLAIMS_2004,e);
                        }

                    }
                }
                if(currentEmail.getBody() != null && currentEmail.getBody().contains("<%REPOSPUBLINK_MYALD%>")){
                    String linkType = null;
                    try {

                        for(Identity currentTo : currentEmail.getTos()) {

                            EmailTemplateMessagingRequestV1 email = new EmailTemplateMessagingRequestV1();

                            email.setHeading(currentEmail.getHeading());
                            email.setObject(currentEmail.getObject());
                            email.setDescription(currentEmail.getDescription());
                            email.setCcs(currentEmail.getCcs());
                            email.setBccs(currentEmail.getBccs());
                            email.setFoot(currentEmail.getFoot());
                            email.setSplittingRecipientsEmail(currentEmail.getSplittingRecipientsEmail());

                            LinkedList<Identity> tos = new LinkedList<>();
                            tos.add(currentTo);
                            email.setTos(tos);

                            String url = null;

                            if (currentEmail.getBody().contains("<%REPOSPUBLINK_MYALD%>")) {
                                linkType = "<%REPOSPUBLINK_MYALD%>";
                                if((claims.isIncomplete() || isIncomplete) && claims.isInsertedFromMyAld()){
                                    List<String> myAldUrls = myAldService.getMyaldLink(claims);
                                    if(!CollectionUtils.isEmpty(myAldUrls)){
                                        url = myAldUrls.get(0);
                                    } else {
                                        throw new BadRequestException(MessageCode.CLAIMS_2005);
                                    }
                                } else {
                                    NemoAuthExternalResposeV1 nemoAuthExternalRespose = this.getExternalTokenString(claims, currentTo,true);
                                    url= nemoAuthExternalRespose.getUrl();
                                }
                            }


                            //GENERAZIONE URL UTENTI ESTERNI
                            if (url != null && !url.isEmpty() && !linkType.isEmpty()) {
                                contentBody = currentEmail.getBody().replaceAll(linkType, url);
                            }else{
                                contentBody = currentEmail.getBody();
                            }

                            email.setBody(contentBody);
                            listFinal.add(email);
                        }

                    } catch (Exception e) {
                        LOGGER.error(e.getMessage());
                        //throw new BadRequestException(MessageCode.CLAIMS_2005,e);
                    }
                }

                if(currentEmail.getBody() != null &&  contentBody.equalsIgnoreCase("") ){
                    listFinal.add(currentEmail);
                }
            }

            return listFinal;

        }else{
            LOGGER.error(MessageCode.CLAIMS_1010.value());
            throw new NotFoundException(MessageCode.CLAIMS_1010);
        }
    }


    //REFACTOR
    @Override
    public  List<EmailTemplateMessagingResponseV1> getMailTemplateByTypeEventManualEntrust(Entrusted entrusted, EventTypeEnum typeEvent, String claimsId) {

        EmailTemplateEnum type = null;
        //recupero della nuova entità
        Optional<ClaimsNewEntity> claimsNewEntityOpt = claimsNewRepository.findById(claimsId);

        if (!claimsNewEntityOpt.isPresent()) {
            LOGGER.debug("Claims with id " + claimsId + " not found");
            throw new NotFoundException("Claims with id " + claimsId + " not found", MessageCode.CLAIMS_1010);
        }

        //conversione nella vecchia entità
        ClaimsEntity claims = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntityOpt.get());
        ClaimsFlowEnum claimsFlow = claims.getType();

        //setto l'entrusted in modo tale che venga sostituito nel parse dei destinatari
        if(claims.getComplaint() != null) {
            Complaint complaint = claims.getComplaint();
            complaint.setEntrusted(entrusted);
            claims.setComplaint(complaint);
        }

        String customerId = null;
        List<EmailTemplateEntity> emailTemplateListWithFlow;
        DataAccidentTypeAccidentEnum claimsType = null;
        if (claims.getComplaint() !=null && claims.getComplaint().getDataAccident()!=null && claims.getComplaint().getDataAccident().getTypeAccident() != null) {
            claimsType = claims.getComplaint().getDataAccident().getTypeAccident();
        }
        /*if (claims.getComplaint().getDataAccident().getTypeAccident() != null && claims.getComplaint().getDataAccident().getTypeAccident().equals(DataAccidentTypeAccidentEnum.FURTO_TOTALE))
            type = EmailTemplateEnum.THEFT;
        else type = EmailTemplateEnum.GENERIC;



        if (claims.getDamaged() != null && claims.getDamaged().getCustomer() != null && Util.isNotEmpty(claims.getDamaged().getCustomer().getCustomerId())) {

            customerId = claims.getDamaged().getCustomer().getCustomerId();
            emailTemplateListWithFlow = emailTemplateRepositoryV1.getTemplateEntityByEventDescription(typeEvent.getValue(), claimsFlow.getValue(), customerId, type.getValue());

            if (emailTemplateListWithFlow == null || emailTemplateListWithFlow.isEmpty())
                emailTemplateListWithFlow = emailTemplateRepositoryV1.getTemplateEntityByEventDescription(typeEvent.getValue(), claimsFlow.getValue(), null, type.getValue());

        } else {
            emailTemplateListWithFlow = emailTemplateRepositoryV1.getTemplateEntityByEventDescription(typeEvent.getValue(), claimsFlow.getValue(), null, type.getValue());
        }*/

        //CERCO PRIMA TEMPLATE CON TIPO SINISTRO
        if (claims.getDamaged() != null && claims.getDamaged().getCustomer() != null && Util.isNotEmpty(claims.getDamaged().getCustomer().getCustomerId())) {

            customerId = claims.getDamaged().getCustomer().getCustomerId();
            emailTemplateListWithFlow = emailTemplateRepositoryV1.getTemplateEntityByEventDescription(typeEvent.getValue(), claimsFlow.getValue(), customerId, null, claimsType.getValue());

            if (emailTemplateListWithFlow == null || emailTemplateListWithFlow.isEmpty())
                emailTemplateListWithFlow = emailTemplateRepositoryV1.getTemplateEntityByEventDescription(typeEvent.getValue(), claimsFlow.getValue(), null, null, claimsType.getValue());

        } else {
            emailTemplateListWithFlow = emailTemplateRepositoryV1.getTemplateEntityByEventDescription(typeEvent.getValue(), claimsFlow.getValue(), null, null, claimsType.getValue());
        }

        //SE NON HO TROVATO TEMPLATE CON TIPO SINISTRO CERCO TRA QUELLI CON TIPO SINISTRO NULL
        if(emailTemplateListWithFlow==null || emailTemplateListWithFlow.isEmpty()) {
            if (claims.getDamaged() != null && claims.getDamaged().getCustomer() != null && Util.isNotEmpty(claims.getDamaged().getCustomer().getCustomerId())) {

                customerId = claims.getDamaged().getCustomer().getCustomerId();
                emailTemplateListWithFlow = emailTemplateRepositoryV1.getTemplateEntityByEventDescription(typeEvent.getValue(), claimsFlow.getValue(), customerId, null, null);

                if (emailTemplateListWithFlow == null || emailTemplateListWithFlow.isEmpty())
                    emailTemplateListWithFlow = emailTemplateRepositoryV1.getTemplateEntityByEventDescription(typeEvent.getValue(), claimsFlow.getValue(), null, null, null);

            } else {
                emailTemplateListWithFlow = emailTemplateRepositoryV1.getTemplateEntityByEventDescription(typeEvent.getValue(), claimsFlow.getValue(), null, null, null);
            }
        }




        List<EmailTemplateMessagingResponseV1> emailTemplateMessagingResponseList = new LinkedList<>();


        if (emailTemplateListWithFlow != null && !emailTemplateListWithFlow.isEmpty()) {

            for (EmailTemplateEntity emailTemplate : emailTemplateListWithFlow) {

                if (emailTemplate.getFlows() != null && emailTemplate.getFlows().contains(claims.getType())) {

                    List<RecipientTypeEntity> recipientTypeEntityList = emailTemplate.getCustomers();
                    List<Identity> tos = new LinkedList<>();

                    CounterpartyEntity counterparty = this.getCounterpartyResponsable(claims.getCounterparts());
                    if (counterparty != null && counterparty.getAld() != null && counterparty.getAld()) {
                        recipientTypeEntityList.removeIf(recipientTypeEntity -> recipientTypeEntity.getRecipientType().equals(RecipientEnum.COMPANY));
                    }

                    for (RecipientTypeEntity recipientType : recipientTypeEntityList) {

                        if(recipientType.getRecipientType() != null && recipientType.getRecipientType().equals(RecipientEnum.CLAIMS_MANAGER)){
                            recipientType.setRecipientType(RecipientEnum.CLAIMS_MANAGER_ENTRUSTED);
                        }

                        Identity customer = new Identity();
                        if(recipientType.getActive() != null && recipientType.getActive()) {
                            if (!recipientType.getFixed()) {

                                //CHIAMATA A METODO findIdentityEmail
                                tos.addAll(this.findIdentityEmail(recipientType.getRecipientType(), claims, null));
                            } else {
                                if(recipientType.getRecipientType() != null && recipientType.getRecipientType().equals(RecipientEnum.CLAIMS_MANAGER_ENTRUSTED)){
                                    recipientType.setRecipientType(RecipientEnum.CLAIMS_MANAGER);
                                }

                                tos.add(new Identity(recipientType.getEmail(), "", recipientType.getRecipientType().getValue()));
                            }
                        }else{
                            if(recipientType.getRecipientType() != null && recipientType.getRecipientType().equals(RecipientEnum.CLAIMS_MANAGER_ENTRUSTED)){
                                recipientType.setRecipientType(RecipientEnum.CLAIMS_MANAGER);
                            }
                        }

                    }


                    EmailTemplateMessagingResponseV1 emailTemplateMessaging = new EmailTemplateMessagingResponseV1();
                    emailTemplateMessaging.setTos(tos);
                    emailTemplateMessaging.setHeading(emailTemplate.getHeading());
                    emailTemplateMessaging.setBody(emailTemplate.getBody());
                    emailTemplateMessaging.setFoot(emailTemplate.getFoot());
                    emailTemplateMessaging.setObject(emailTemplate.getObject());
                    emailTemplateMessaging.setAttachFile(emailTemplate.getAttachFile());
                    emailTemplateMessaging.setSplittingRecipientsEmail(emailTemplate.getSplittingRecipientsEmail());

                    emailTemplateMessaging.setDescription(emailTemplate.getDescription());

                    this.parsContentOfMail(emailTemplateMessaging, claims, null, null);

                    emailTemplateMessagingResponseList.add(emailTemplateMessaging);
                }
            }
        }
        return emailTemplateMessagingResponseList;

    }
}
