package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.controller.payload.response.PaginationStats;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.enumerated.ClaimsRepairEnum;
import com.doing.nemo.claims.entity.enumerated.EventTypeEnum;
import com.doing.nemo.claims.entity.settings.EventTypeEntity;
import com.doing.nemo.claims.repository.EventTypeRepository;
import com.doing.nemo.claims.repository.EventTypeRepositoryV1;
import com.doing.nemo.claims.service.EventTypeService;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class EventTypeServiceImpl implements EventTypeService {

    private static Logger LOGGER = LoggerFactory.getLogger(EventTypeServiceImpl.class);
    @Autowired
    private EventTypeRepository eventTypeRepository;
    @Autowired
    private EventTypeRepositoryV1 eventTypeRepositoryV1;

    @Override
    public EventTypeEntity insertEventType(EventTypeEntity eventTypeEntity) {

        eventTypeRepository.save(eventTypeEntity);
        return eventTypeEntity;
    }

    @Override
    public EventTypeEntity updateEventType(EventTypeEntity eventTypeEntity, UUID uuid) {
        Optional<EventTypeEntity> eventTypeEntityOld = eventTypeRepository.findById(uuid);
        if (!eventTypeEntityOld.isPresent()) {
            LOGGER.debug("EventType with id " + uuid + " not found");
            throw new NotFoundException("EventType with id " + uuid + " not found", MessageCode.CLAIMS_1010);
        }

        eventTypeEntity.setId(eventTypeEntityOld.get().getId());
        eventTypeEntity.setTypeComplaint(eventTypeEntityOld.get().getTypeComplaint());
        eventTypeRepository.save(eventTypeEntity);
        return eventTypeEntity;
    }

    @Override
    public EventTypeEntity getEventType(UUID uuid) {
        Optional<EventTypeEntity> eventTypeEntity = eventTypeRepository.findById(uuid);
        if (!eventTypeEntity.isPresent()) {
            LOGGER.debug("EventType with id " + uuid + " not found");
            throw new NotFoundException("EventType with id " + uuid + " not found", MessageCode.CLAIMS_1010);
        }

        return eventTypeEntity.get();
    }

    @Override
    public List<EventTypeEntity> getAllEventType(ClaimsRepairEnum claimsRepairEnum) {
        List<EventTypeEntity> eventTypeEntities = eventTypeRepository.searchEventTypeByTypeComplaint(claimsRepairEnum);

        return eventTypeEntities;
    }


    @Override
    public EventTypeEntity deleteEventType(UUID uuid) {
        Optional<EventTypeEntity> eventTypeEntity = eventTypeRepository.findById(uuid);
        if (!eventTypeEntity.isPresent()) {
            LOGGER.debug("EventType with id " + uuid + " not found");
            throw new NotFoundException("EventType with id " + uuid + " not found", MessageCode.CLAIMS_1010);
        }

        eventTypeRepository.delete(eventTypeEntity.get());

        return null;
    }

    @Override
    public EventTypeEntity patchStatusEventType(UUID uuid) {
        EventTypeEntity eventTypeEntity = getEventType(uuid);
        if (eventTypeEntity.getActive()) {
            eventTypeEntity.setActive(false);
        } else {
            eventTypeEntity.setActive(true);
        }

        eventTypeRepository.save(eventTypeEntity);
        return eventTypeEntity;
    }

    @Override
    public Pagination<EventTypeEntity> paginationEventType(int page, int pageSize, String claimsRepairEnum, String orderBy, String description, String eventTypeEnum, Boolean createAttachments, String catalog, String attachmentsType, Boolean onlyLast, Boolean isActive, Boolean asc) {
        Pagination<EventTypeEntity> eventTypePagination = new Pagination<>();

        ClaimsRepairEnum claimsRepair = null;
        if (claimsRepairEnum != null)
            claimsRepair = ClaimsRepairEnum.create(claimsRepairEnum);

        EventTypeEnum eventType = null;
        if (eventTypeEnum != null)
            eventType = EventTypeEnum.create(eventTypeEnum);


        List<EventTypeEntity> eventTypeList = eventTypeRepositoryV1.findPaginationEventType(page, pageSize, claimsRepair, orderBy, description, eventType, createAttachments, catalog, attachmentsType, onlyLast, isActive, asc);
        Long itemCount = eventTypeRepositoryV1.countPaginationEventType(claimsRepair, description, eventType, createAttachments, catalog, attachmentsType, onlyLast, isActive);
        eventTypePagination.setItems(eventTypeList);
        eventTypePagination.setStats(new PaginationStats(itemCount.intValue(), page, pageSize));

        return eventTypePagination;
    }


}
