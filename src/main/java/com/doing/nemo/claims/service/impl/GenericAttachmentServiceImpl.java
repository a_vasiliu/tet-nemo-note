package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.UploadFileRequestV1;
import com.doing.nemo.claims.controller.payload.response.IdentifierResponseV1;
import com.doing.nemo.claims.controller.payload.response.UploadFileResponseV1;
import com.doing.nemo.claims.entity.enumerated.ClaimsRepairEnum;
import com.doing.nemo.claims.entity.settings.GenericAttachmentEntity;
import com.doing.nemo.claims.repository.GenericAttachmentRepository;
import com.doing.nemo.claims.service.FileManagerService;
import com.doing.nemo.claims.service.GenericAttachmentService;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.InternalException;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.rmi.ConnectException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class GenericAttachmentServiceImpl implements GenericAttachmentService {

    private static Logger LOGGER = LoggerFactory.getLogger(GenericAttachmentServiceImpl.class);
    @Autowired
    private FileManagerService fileManagerService;
    
    @Autowired
    private GenericAttachmentRepository genericAttachmentRepository;


    public GenericAttachmentServiceImpl() {
    }

    @Override
    public IdentifierResponseV1 uploadGenericAttachment(UploadFileRequestV1 uploadFileRequest, Boolean isActive, ClaimsRepairEnum typeComplaint) {

        try {

            GenericAttachmentEntity genericAttachment = new GenericAttachmentEntity();
            genericAttachment.setId(UUID.randomUUID());
            genericAttachment.setTypeComplaint(typeComplaint);

            uploadFileRequest.setResourceId(genericAttachment.getId().toString());
            uploadFileRequest.setResourceType("genericattachment");
            uploadFileRequest.setAttachmentType(uploadFileRequest.getAttachmentType());

            UploadFileResponseV1 uploadFileResponse = fileManagerService.uploadSingleFile(uploadFileRequest);

            genericAttachment.setDescription(uploadFileResponse.getDescription());
            genericAttachment.setOriginalName(uploadFileResponse.getFileName());
            genericAttachment.setAttachmentId(uploadFileResponse.getUuid());
            genericAttachment.setCreatedAt(DateUtil.getNowInstant());
            genericAttachment.setActive(isActive);

            genericAttachmentRepository.save(genericAttachment);

            IdentifierResponseV1 genericAttachmentIdV1 = new IdentifierResponseV1();
            genericAttachmentIdV1.setId(genericAttachment.getId().toString());

            return genericAttachmentIdV1;

        } catch (ConnectException e) {
            LOGGER.debug("Connection Exception with FileManager");
            throw new InternalException(MessageCode.CLAIMS_1003, e);
        }
    }

    @Override
    public UploadFileResponseV1 uploadGenericAttachmentUpdate(UploadFileRequestV1 uploadFileRequest, UUID idGenericAttachment, Boolean isActive) {

        try {

            GenericAttachmentEntity genericAttachment = genericAttachmentRepository.getOne(idGenericAttachment);
            genericAttachment.setId(UUID.randomUUID());

            uploadFileRequest.setResourceId(genericAttachment.getId().toString());
            uploadFileRequest.setResourceType("genericattachment");

            UploadFileResponseV1 uploadFileResponse = fileManagerService.uploadSingleFile(uploadFileRequest);

            genericAttachment.setDescription(uploadFileResponse.getDescription());
            genericAttachment.setOriginalName(uploadFileResponse.getFileName());
            genericAttachment.setAttachmentId(uploadFileResponse.getUuid());
            genericAttachment.setCreatedAt(DateUtil.getNowInstant());
            genericAttachment.setActive(isActive);

            genericAttachmentRepository.save(genericAttachment);

            return uploadFileResponse;

        } catch (ConnectException e) {
            LOGGER.debug("Connection Exception with FileManager");
            throw new InternalException(MessageCode.CLAIMS_1003, e);
        }
    }

    @Override
    public void deleteGenericAttachment(GenericAttachmentEntity genericAttachmentEntity) {
        genericAttachmentRepository.delete(genericAttachmentEntity);

    }

    @Override
    public GenericAttachmentEntity updateStatusGenericAttachment(UUID uuid) {
        Optional<GenericAttachmentEntity> genericAttachmentEntityOptional = genericAttachmentRepository.findById(uuid);
        if (!genericAttachmentEntityOptional.isPresent()) {
            LOGGER.debug("GenericAttachment with id " + uuid + " not found");
            throw new NotFoundException("GenericAttachment with id " + uuid + " not found", MessageCode.CLAIMS_1010);
        }
        GenericAttachmentEntity genericAttachmentEntity = genericAttachmentEntityOptional.get();
        if (genericAttachmentEntity.getActive()) {
            genericAttachmentEntity.setActive(false);
        } else {
            genericAttachmentEntity.setActive(true);
        }

        genericAttachmentRepository.save(genericAttachmentEntity);
        return genericAttachmentEntity;
    }

    @Override
    public List<GenericAttachmentEntity> getAllGenericAttachment(ClaimsRepairEnum claimsRepairEnum) {
        List<GenericAttachmentEntity> genericAttachmentEntities = genericAttachmentRepository.searchGenericAttachmentByTypeComplaint(claimsRepairEnum);


        return genericAttachmentEntities;
    }
}
