package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.entity.settings.ContractEntity;
import com.doing.nemo.claims.repository.ContractRepository;
import com.doing.nemo.claims.service.ContractPersonalDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContractPersonalDataServiceImpl implements ContractPersonalDataService {

    @Autowired
    private ContractRepository contractRepository;

    @Override
    public List<ContractEntity> updateContractList(List<ContractEntity> contractEntityList) {
        if (contractEntityList != null)
            for (ContractEntity contractEntity : contractEntityList) {
                contractRepository.save(contractEntity);
            }
        return contractEntityList;
    }

    @Override
    public void deleteContractList(List<ContractEntity> contractEntityList) {
        if (contractEntityList != null) {
            for (ContractEntity contractEntity : contractEntityList) {
                contractRepository.delete(contractEntity);
            }
        }
    }

}
