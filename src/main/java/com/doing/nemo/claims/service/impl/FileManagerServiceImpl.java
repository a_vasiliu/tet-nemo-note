package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.adapter.AttachmentAdapter;
import com.doing.nemo.claims.adapter.AttachmentTypeAdapter;
import com.doing.nemo.claims.adapter.CounterpartyAdapter;
import com.doing.nemo.claims.adapter.UploadFileAdapter;
import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.UploadFileClaimsMigrationRequestV1;
import com.doing.nemo.claims.controller.payload.UploadFileClaimsRequestV1;
import com.doing.nemo.claims.controller.payload.request.UploadFileRequestV1;
import com.doing.nemo.claims.controller.payload.response.AddTokenResponseV1;
import com.doing.nemo.claims.controller.payload.response.DownloadFileBase64ResponseV1;
import com.doing.nemo.claims.controller.payload.response.ResponseV2;
import com.doing.nemo.claims.controller.payload.response.UploadFileResponseV1;
import com.doing.nemo.claims.controller.payload.response.filemanager.BlobExistsResponseV1;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.ClaimsNewEntity;
import com.doing.nemo.claims.entity.CounterpartyEntity;
import com.doing.nemo.claims.entity.CounterpartyNewEntity;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.doing.nemo.claims.entity.enumerated.DTO.Status;
import com.doing.nemo.claims.entity.jsonb.UploadFile;
import com.doing.nemo.claims.entity.jsonb.forms.Forms;
import com.doing.nemo.claims.entity.jsonb.forms.jsonbForms.Attachment;
import com.doing.nemo.claims.entity.settings.InsurancePolicyEntity;
import com.doing.nemo.claims.entity.settings.PracticeEntity;
import com.doing.nemo.claims.exception.ForbiddenException;
import com.doing.nemo.claims.exception.MigrationException;
import com.doing.nemo.claims.repository.*;
import com.doing.nemo.claims.service.*;
import com.doing.nemo.claims.util.Util;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.commons.util.HttpUtil;
import com.doing.nemo.dwh.client.payload.request.EventTypeEnum;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.Response;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.apache.commons.collections4.MapUtils;
import org.hibernate.exception.LockAcquisitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.orm.jpa.JpaSystemException;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.lang.reflect.Type;
import java.rmi.ConnectException;
import java.text.ParseException;
import java.util.*;

@Service
public class FileManagerServiceImpl implements FileManagerService {

    private static Logger LOGGER = LoggerFactory.getLogger(FileManagerServiceImpl.class);
    @Autowired
    private HttpUtil httpUtil;
    @Value("${filemanager.api.endpoint}")
    private String endpointUrl;
    @Value("${filemanager.api.upload}")
    private String uploadApi;
    @Value("${filemanager.api.addToken}")
    private String addTokenApi;
    @Value("${filemanager.api.downloadBase64.prefix}")
    private String prefixDownloadBase64Api;
    @Value("${filemanager.api.downloadBase64.suffix}")
    private String suffixDownloadBase64Api;
    @Value("${filemanager.api.downloadFile.suffix}")
    private String suffixDownloadFileApi;
    @Value("${filemanager.api.blobexists.prefix}")
    private String prefixBlobExists;
    @Value("#{${filemanager.header}}")
    private HashMap<String, String> header;


    @Autowired
    private ClaimsNewRepository claimsNewRepository;

    @Autowired
    private ConverterClaimsService converterClaimsService;

    @Autowired
    private CounterpartyNewRepository counterpartyNewRepository;
    @Autowired
    private InsurancePolicyRepository insurancePolicyRepository;

    @Autowired
    private AttachmentTypeRepository attachmentTypeRepository;

    @Autowired
    private PracticeRepository practiceRepository;

    @Autowired
    private DwhClaimsService dwhClaimsService;

    @Autowired
    private ExternalCommunicationService externalCommunicationService;

    @Value("${dwh.call}")
    private Boolean dwhCall;

    @Autowired
    private CacheService cacheService;

    @Value("${filemanager.api.migration.upload}")
    private String uploadFileMigrationApi;

    private static final String CACHENAME = "claims.";
    private static final String STATSkEY = "claimsStats::";
    private static final String STATSEVIDENCE = "evidenceStats::";
    private static final String STATSTHEFT = "theftStats::";
    private static final String STATSSTATUS = "statusStats::";
    private static final String COUNTERPARTYKEY = "counterpartyStats::";
    private static final String STATSkEY2 = "claimsV2Stats::";
    private static final String STATSEVIDENCECONTINUATION = "evidenceContinuationStats::";

    private <T, P> T call(String url, HttpMethod method, P value, HashMap<String, String> headersparams, ParameterizedTypeReference<T> typeReference) {
        try {

            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            for (String k : headersparams.keySet()) {
                headers.add(k, headersparams.get(k));
            }

            HttpEntity<P> entity = new HttpEntity<>(value, headers);

            return restTemplate.exchange(url, method, entity, typeReference).getBody();
        } catch (Exception e) {
            LOGGER.debug(e.getMessage());
            throw e;
        }
    }

    private <T, P> T callWithoutCert(String url, HttpMethod method, P value, HashMap<String, String> headersparams, ParameterizedTypeReference<T> typeReference) throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = null;

        if (value != null) {
            jsonInString = mapper.writeValueAsString(value);
        }

        Response responseToken = httpUtil.callURL(headersparams, url, jsonInString, method);

        if (responseToken.code() != HttpStatus.OK.value() && responseToken.code() != HttpStatus.CREATED.value() && responseToken.code() != HttpStatus.ACCEPTED.value()) {
            LOGGER.debug(responseToken.message() + " with code " + responseToken.code());
            throw new BadRequestException(responseToken.message() + " with code " + responseToken.code(), MessageCode.CLAIMS_2000);
        }

        if (typeReference == null)
            return null;

        String response = responseToken.body().string();
        TypeReference tr = new CustomTypeReference(typeReference);
        return mapper.readValue(response, tr);
    }

    private ResponseEntity downloadResourceFile(String idFileManager) throws IOException {

        AddTokenResponseV1 tokenResponse = callWithoutCert(endpointUrl + addTokenApi + idFileManager, HttpMethod.GET, null, header, new ParameterizedTypeReference<AddTokenResponseV1>() {
        });


        return callWithoutCert(endpointUrl + prefixDownloadBase64Api + tokenResponse.getAccessToken() + "/download",
                HttpMethod.GET, null, header, new ParameterizedTypeReference<ResponseEntity>() {
                });


    }

    private BlobExistsResponseV1 blobExists(String blobId) throws Exception {
        return callWithoutCert(endpointUrl + prefixBlobExists + blobId,
                HttpMethod.GET, null, header, new ParameterizedTypeReference<BlobExistsResponseV1>() {
                });
    }

    @Override
    public UploadFileResponseV1 uploadSingleFile(UploadFileRequestV1 uploadFileRequest) throws ConnectException {

        UploadFileResponseV1 response = null;
        try {
            response = callWithoutCert(endpointUrl + uploadApi + "?thumbnail=true", HttpMethod.PUT, uploadFileRequest, header, new ParameterizedTypeReference<UploadFileResponseV1>() {
            });

        } catch (Exception ex) {
            LOGGER.debug(ex.getMessage());
            throw new ConnectException(ex.getMessage());
        }

        return response;
    }

    @Override
    public UploadFileResponseV1 octoFileManager(UploadFileRequestV1 uploadFile, String idClaims) throws IOException {

        List<UploadFileResponseV1> responseList = new LinkedList<>();


        uploadFile.setResourceId(idClaims);
        uploadFile.setResourceType("claims");

        UploadFileResponseV1 response = callWithoutCert(endpointUrl + uploadApi + "?thumbnail=true", HttpMethod.PUT, uploadFile, header, new ParameterizedTypeReference<UploadFileResponseV1>() {
        });

        return response;
    }

    @Override
    public List<UploadFileResponseV1> uploadFileByIdPractice(List<UploadFileClaimsRequestV1> uploadFileRequest, String idPractice, String idCounterparty, String user, String userName, Boolean isValidate) throws IOException {

        List<UploadFileResponseV1> responseList = new LinkedList<>();
        ClaimsNewEntity claimsEntity = null;
        List<Attachment> attachmentList = null;
        CounterpartyNewEntity counterparty = null;

        if (idCounterparty == null) {
            claimsEntity = claimsNewRepository.getClaimsByIdPratica(Long.parseLong(idPractice));

            if(claimsEntity == null){
                LOGGER.debug("Claim with pracitce_id " + idPractice + "not found");
                throw new NotFoundException("Claim with pracitce_id " + idPractice + " not found", MessageCode.CLAIMS_1010);
            }

            if(claimsEntity.getStatus().equals(ClaimsStatusEnum.DRAFT)){
                LOGGER.debug("Claim with pracitce_id " + idPractice + "is in DRAFT status");
                throw new ForbiddenException("Claim with pracitce_id " + idPractice + " is in DRAFT status", MessageCode.CLAIMS_2008);
            }
            if (claimsEntity.getForms() == null)
                claimsEntity.setForms(new Forms());
            else
                attachmentList = claimsEntity.getForms().getAttachment();

        } else {
            Optional<CounterpartyNewEntity> counterpartyNewEntityOptional = counterpartyNewRepository.findById(idCounterparty);
            if (!counterpartyNewEntityOptional.isPresent()) {
                LOGGER.debug("Counterparty with UUID: " + idCounterparty + " not found in this Claim");
                throw new NotFoundException("Counterparty with UUID: " + idCounterparty + " not found in this Claim", MessageCode.CLAIMS_1010);
            }
            counterparty = counterpartyNewEntityOptional.get();
            attachmentList = counterparty.getAttachments();
        }

        List<UploadFile> webSinUploadFileList = new ArrayList<>();
        for (UploadFileClaimsRequestV1 uploadFile : uploadFileRequest) {

            try {
                if (idCounterparty != null)
                    uploadFile.setResourceId(idCounterparty);
                else
                    uploadFile.setResourceId(claimsEntity.getId());

                uploadFile.setResourceType("claims");
                UploadFileRequestV1 uploadFileRequestV1 = UploadFileAdapter.adptFromUploadFileClaimsRequestV1ToUploadFileRequestV1(uploadFile);

                UploadFileResponseV1 response = callWithoutCert(endpointUrl + uploadApi + "?thumbnail=true", HttpMethod.PUT, uploadFileRequestV1, header, new ParameterizedTypeReference<UploadFileResponseV1>() {
                });

                //UploadFileResponseV1 response = callWithoutCert( "https://nemo-dev.doing.com/filemanager/system/ping", HttpMethod.GET, uploadFileRequest, header, UploadFileResponseV1.class );
                //UploadFileResponseV1 response = callWithoutCert( "https://nemo-filemanager-service/system/ping", HttpMethod.GET, null, header, null );
                response.setExternalId(uploadFile.getExternalId());
                Attachment attachment = new Attachment(
                        response.getUuid(),
                        response.getFileName(),
                        response.getBlobName(),
                        response.getFileSize(),
                        response.getMimeType(),
                        response.getDescription(),
                        Util.nowDateISO8601(),
                        user,
                        userName,
                        response.getThumbnailId(),
                        uploadFile.isHidden() == null ? false : uploadFile.isHidden(),
                        isValidate == null ? false : isValidate,
                        uploadFile.getExternalId(),
                        false
                );

                if (attachmentList == null) {
                    attachmentList = new LinkedList<>();
                }
                attachmentList.add(attachment);
                responseList.add(response);
                // Invio verso WebSin
                if(Status.UPLOADED.equals(response.getStatus())
                    || Status.PROCESSED.equals(response.getStatus())
                ){
                    UploadFile uploadFile1 = UploadFileAdapter.dptFromUploadFileClaimsRequestV1ToUploadFile(uploadFile);
                    webSinUploadFileList.add(uploadFile1);
                }

            } catch (Exception ex) {
                LOGGER.debug(ex.getMessage());
                throw new ConnectException(ex.getMessage());
            }
        }
        if (counterparty == null) {
            claimsEntity.getForms().setAttachment(attachmentList);
            if (claimsEntity.getWithContinuation() == null || !claimsEntity.getWithContinuation()) {
                if (isValidate == null || !isValidate)
                    claimsEntity.setWithContinuation(true);
            }
            claimsNewRepository.save(claimsEntity);

            if(dwhCall){
                ClaimsEntity oldClaims = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsEntity);
                dwhClaimsService.sendMessage(oldClaims, EventTypeEnum.UPDATED);
            }
            cacheService.deleteClaimsStats(CACHENAME, STATSkEY);
            cacheService.deleteClaimsStats(CACHENAME, STATSEVIDENCE);
            cacheService.deleteClaimsStats(CACHENAME, STATSSTATUS);
            cacheService.deleteClaimsStats(CACHENAME, STATSTHEFT);

            cacheService.deleteClaimsStats(CACHENAME, STATSEVIDENCECONTINUATION);
            cacheService.deleteClaimsStats(CACHENAME, STATSkEY2);
        } else {

            counterparty.setAttachments(attachmentList);
            if(counterparty.getIsReadMsa() != null && counterparty.getIsReadMsa()){
                counterparty.setIsReadMsa(false);
            }
            counterpartyNewRepository.save(counterparty);
        }

        if(webSinUploadFileList!=null && webSinUploadFileList.size()>0) {
            externalCommunicationService.uploadFile(webSinUploadFileList, claimsEntity.getId());
        }

        return responseList;
    }

    @Override
    @Transactional(isolation = Isolation.SERIALIZABLE, noRollbackFor ={LockAcquisitionException.class, JpaSystemException.class} )
    @Retryable(include = LockAcquisitionException.class, maxAttempts = 15, backoff = @Backoff(delay = 1000))
    public List<UploadFileResponseV1> uploadFile(List<UploadFileClaimsRequestV1> uploadFileRequest, String idClaims, String idCounterparty, String user, String userName, Boolean isValidate) throws IOException {

        List<UploadFileResponseV1> responseList = new LinkedList<>();
        ClaimsNewEntity claimsEntity = null;
        List<Attachment> attachmentList = null;
        CounterpartyNewEntity counterparty = null;

        if (idCounterparty == null) {
            Optional<ClaimsNewEntity> claimsEntityOptional = claimsNewRepository.findByIdForUpdate(idClaims);
            if(!claimsEntityOptional.isPresent()){
                LOGGER.error("Claim with UUID: " + idClaims + " not found");
                throw new NotFoundException("Claim with UUID: " + idClaims + " not found", MessageCode.CLAIMS_1010);
            }
            claimsEntity = claimsEntityOptional.get();
            if (claimsEntity.getForms() == null)
                claimsEntity.setForms(new Forms());
            else
                attachmentList = claimsEntity.getForms().getAttachment();

        } else {
            Optional<CounterpartyNewEntity> counterpartyNewEntityOptional = counterpartyNewRepository.findById(idCounterparty);
            if (!counterpartyNewEntityOptional.isPresent()) {
                LOGGER.error("Counterparty with UUID: " + idCounterparty + " not found in this Claim");
                throw new NotFoundException("Counterparty with UUID: " + idCounterparty + " not found in this Claim", MessageCode.CLAIMS_1010);
            }
            counterparty = counterpartyNewEntityOptional.get();
            attachmentList = counterparty.getAttachments();
        }

        for (UploadFileClaimsRequestV1 uploadFile : uploadFileRequest) {

            try {
                if (idCounterparty != null)
                    uploadFile.setResourceId(idCounterparty);
                else
                    uploadFile.setResourceId(idClaims);

                uploadFile.setResourceType("claims");
                UploadFileRequestV1 uploadFileRequestV1 = UploadFileAdapter.adptFromUploadFileClaimsRequestV1ToUploadFileRequestV1(uploadFile);

                UploadFileResponseV1 response = callWithoutCert(endpointUrl + uploadApi + "?thumbnail=true" , HttpMethod.PUT, uploadFileRequestV1, header, new ParameterizedTypeReference<UploadFileResponseV1>() {
                });

                //UploadFileResponseV1 response = callWithoutCert( "https://nemo-dev.doing.com/filemanager/system/ping", HttpMethod.GET, uploadFileRequest, header, UploadFileResponseV1.class );
                //UploadFileResponseV1 response = callWithoutCert( "https://nemo-filemanager-service/system/ping", HttpMethod.GET, null, header, null );
                response.setExternalId(uploadFile.getExternalId());
                Attachment attachment = new Attachment(
                        response.getUuid(),
                        response.getFileName(),
                        response.getBlobName(),
                        response.getFileSize(),
                        response.getMimeType(),
                        response.getDescription(),
                        Util.nowDateISO8601(),
                        user,
                        userName,
                        response.getThumbnailId(),
                        uploadFile.isHidden() == null ? false : uploadFile.isHidden(),
                        isValidate == null ? false : isValidate,
                        uploadFile.getExternalId(),
                        false
                );

                if (attachmentList == null) {
                    attachmentList = new LinkedList<>();
                }
                attachmentList.add(attachment);
                responseList.add(response);

            } catch (Exception ex) {
                LOGGER.debug(ex.getMessage());
                throw new ConnectException(ex.getMessage());
            }
        }

        if (counterparty == null) {
            claimsEntity.getForms().setAttachment(attachmentList);
            if (claimsEntity.getWithContinuation() == null || !claimsEntity.getWithContinuation()) {
                if (isValidate == null || !isValidate)
                    claimsEntity.setWithContinuation(true);
            }

            claimsNewRepository.save(claimsEntity);

            if(dwhCall){
                ClaimsEntity oldClaims = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsEntity);
                dwhClaimsService.sendMessage(oldClaims, EventTypeEnum.UPDATED);
            }
            cacheService.deleteClaimsStats(CACHENAME, STATSkEY);
            cacheService.deleteClaimsStats(CACHENAME, STATSEVIDENCE);
            cacheService.deleteClaimsStats(CACHENAME, STATSSTATUS);
            cacheService.deleteClaimsStats(CACHENAME, STATSTHEFT);

            cacheService.deleteClaimsStats(CACHENAME, STATSEVIDENCECONTINUATION);
            cacheService.deleteClaimsStats(CACHENAME, STATSkEY2);
        } else {

            counterparty.setAttachments(attachmentList);
            if(counterparty.getIsReadMsa() != null && counterparty.getIsReadMsa()){
                counterparty.setIsReadMsa(false);
            }
            counterpartyNewRepository.save(counterparty);
        }


        return responseList;
    }

    @Override
    public DownloadFileBase64ResponseV1 downloadFile(String idFileManager) throws IOException {
        //calling for token
        //AddTokenResponseV1 tokenResponse = callWithoutCert(endpointUrl + "filemanager/api/v1/token/" + idFileManager, HttpMethod.GET, null, header, AddTokenResponseV1.class );

        AddTokenResponseV1 tokenResponse = callWithoutCert(endpointUrl + addTokenApi + idFileManager, HttpMethod.GET, null, header, new ParameterizedTypeReference<AddTokenResponseV1>() {
        });

        DownloadFileBase64ResponseV1 response = callWithoutCert(endpointUrl + prefixDownloadBase64Api + tokenResponse.getAccessToken() + suffixDownloadBase64Api,
                HttpMethod.GET, null, header, new ParameterizedTypeReference<DownloadFileBase64ResponseV1>() {
                });

        return response;

    }

    @Override
    public List<Attachment> deleteFile(String idAttachment, String idClaims, String idCounterparty) {

        Optional<CounterpartyNewEntity> counterpartyOptional = counterpartyNewRepository.findById(idCounterparty);
        if (!counterpartyOptional.isPresent()) {
            LOGGER.debug("Counterparty with UUID: " + idCounterparty + " not found in this Claim");
            throw new NotFoundException("Counterparty with UUID: " + idCounterparty + " not found in this Claim", MessageCode.CLAIMS_1010);
        }
        CounterpartyNewEntity counterparty = counterpartyOptional.get();
        List<Attachment> attachmentList = null;

        attachmentList = counterparty.getAttachments();
        Iterator<Attachment> itAttachment = attachmentList.iterator();
        boolean foundAtt = false;

        while (itAttachment.hasNext() && !foundAtt) {

            Attachment att = itAttachment.next();

            if (idAttachment.equals(att.getFileManagerId())) {
                attachmentList.remove(att);
                foundAtt = true;
            }
        }
        if(counterparty.getIsReadMsa() != null && counterparty.getIsReadMsa()){
            counterparty.setIsReadMsa(false);
        }
        counterpartyNewRepository.save(counterparty);
        return attachmentList;
    }

    @Override
    public List<Attachment> deleteFile(String idAttachment, String idClaims) throws ConnectException {

        Optional<ClaimsNewEntity> claimsEntityOptional = claimsNewRepository.findById(idClaims);
        if (!claimsEntityOptional.isPresent()) {
            LOGGER.debug("Claim with id " + idClaims + " not found");
            throw new NotFoundException("Claim with id " + idClaims + " not found", MessageCode.CLAIMS_1010);
        }
        ClaimsNewEntity claimsEntity = claimsEntityOptional.get();
        if(claimsEntity.getForms() != null && claimsEntity.getForms().getAttachment() != null) {
            List<Attachment> attachmentList = claimsEntity.getForms().getAttachment();
            Iterator<Attachment> itAttachment = attachmentList.iterator();
            boolean foundAtt = false;
            while (itAttachment.hasNext() && !foundAtt) {
                Attachment att = itAttachment.next();
                if (idAttachment.equals(att.getFileManagerId())) {
                    attachmentList.remove(att);
                    foundAtt = true;
                    try {

                        callWithoutCert(endpointUrl + prefixDownloadBase64Api + idAttachment + "/delete", HttpMethod.DELETE, null, header, null);

                    } catch (IOException e) {
                        LOGGER.debug(e.getMessage());
                        throw new ConnectException(e.getMessage());
                    }
                }
            }
            if (!foundAtt) {
                LOGGER.debug("Attachment with UUID: " + idAttachment + " not found");
                throw new NotFoundException("Attachment with UUID: " + idAttachment + " not found", MessageCode.CLAIMS_1010);
            }
            claimsNewRepository.save(claimsEntity);
            return attachmentList;
        }
        LOGGER.debug("Attachment with UUID: " + idAttachment + " not found");
        throw new NotFoundException("Attachment with UUID: " + idAttachment + " not found", MessageCode.CLAIMS_1010);
    }

    @Override
    public List<Attachment> deleteFileList(List<String> attachmentsList, String idClaims) throws ConnectException {

        Optional<ClaimsNewEntity> claimsEntityOpt = claimsNewRepository.findById(idClaims);

        if (!claimsEntityOpt.isPresent()) {
            LOGGER.debug("Claim with id " + idClaims + " not found");
            throw new NotFoundException("Claim with id " + idClaims + " not found", MessageCode.CLAIMS_1010);
        }

        ClaimsNewEntity claimsEntity = claimsEntityOpt.get();

        for (String id : attachmentsList) {
            List<Attachment> attachments = claimsEntity.getForms().getAttachment();
            attachments.removeIf(attachment1 -> attachment1.getFileManagerId().equalsIgnoreCase(id));
            claimsEntity.getForms().setAttachment(attachments);
            try {

                callWithoutCert(endpointUrl + prefixDownloadBase64Api + id + "/delete", HttpMethod.DELETE, null, header, null);

            } catch (IOException e) {
                LOGGER.debug(e.getMessage());
                throw new ConnectException(e.getMessage());
            }
        }

        claimsNewRepository.save(claimsEntity);
        return claimsEntity.getForms().getAttachment();
    }

    @Override
    public List<Attachment> deleteFileList(List<String> attachmentsList, String idClaims, String idCounterparty) throws ConnectException {

        Optional<CounterpartyNewEntity> counterpartyEntityOptional = counterpartyNewRepository.findById(idCounterparty);

        if (!counterpartyEntityOptional.isPresent()) {
            LOGGER.debug("Counterparty with id " + idCounterparty + " not found");
            throw new NotFoundException("Counterparty with id " + idCounterparty + " not found", MessageCode.CLAIMS_1010);
        }
        CounterpartyNewEntity counterparty = counterpartyEntityOptional.get();
        if(counterparty.getAttachments() != null) {
            for (String id : attachmentsList) {

                List<Attachment> attachments = counterparty.getAttachments();
                attachments.removeIf(attachment1 -> attachment1.getFileManagerId().equalsIgnoreCase(id));
                counterparty.setAttachments(attachments);

                try {

                    callWithoutCert(endpointUrl + prefixDownloadBase64Api + id + "/delete", HttpMethod.DELETE, null, header, null);

                } catch (IOException e) {
                    LOGGER.debug(e.getMessage());
                    throw new ConnectException(e.getMessage());
                }
            }
            if (counterparty.getIsReadMsa() != null && counterparty.getIsReadMsa()) {
                counterparty.setIsReadMsa(false);
            }

            counterpartyNewRepository.save(counterparty);
        }
        return counterparty.getAttachments();
    }

    @Override
    public List<DownloadFileBase64ResponseV1> downloadAllFileAttachment(String idClaims) throws IOException {
        //recupero della nuova entità
        Optional<ClaimsNewEntity> claimsEntityOptional = claimsNewRepository.findById(idClaims);
        if (!claimsEntityOptional.isPresent()) {
            LOGGER.debug("Claims with id " +idClaims+" not found");
            throw new NotFoundException("Claims with id " +idClaims+" not found", MessageCode.CLAIMS_1010);
        }
        //conversione nella vecchia entità
        ClaimsNewEntity claimsEntity = claimsEntityOptional.get();
        if(claimsEntity.getForms() != null && claimsEntity.getForms().getAttachment() != null) {
            List<Attachment> attachmentList = claimsEntity.getForms().getAttachment();
            List<DownloadFileBase64ResponseV1> downloadList = new LinkedList<>();
            String uuidList = "";

            for (Attachment attachment : attachmentList) {
                uuidList = uuidList + attachment.getFileManagerId() + ",";
            }

            uuidList = uuidList.substring(0, uuidList.length() - 1);
            List<AddTokenResponseV1> tokenResponseList = callWithoutCert(endpointUrl + addTokenApi + uuidList, HttpMethod.GET, null, header, new ParameterizedTypeReference<List<AddTokenResponseV1>>() {
            });

            for (AddTokenResponseV1 tokenResponse : tokenResponseList) {
                downloadList.add(callWithoutCert(endpointUrl + prefixDownloadBase64Api + tokenResponse.getAccessToken() + suffixDownloadBase64Api,
                        HttpMethod.GET, null, header, new ParameterizedTypeReference<DownloadFileBase64ResponseV1>() {
                        }));
            }

            return downloadList;
        }

        return null;

    }

    @Override
    public List<AddTokenResponseV1> getTokenList(String uuidFileManager) throws IOException {
        return callWithoutCert(endpointUrl + addTokenApi + uuidFileManager, HttpMethod.GET, null, header, new ParameterizedTypeReference<List<AddTokenResponseV1>>() {
        });
    }

    private void sendFileToWebSin(DownloadFileBase64ResponseV1 downloadFileBase64ResponseV1, String idClaims){
        UploadFile uploadFile = UploadFileAdapter.adptFromDownloadFileBase64ResponseV1ToListUploadFile(downloadFileBase64ResponseV1);
        List<UploadFile> uploadFileList = new ArrayList<>();
        uploadFileList.add(uploadFile);
        externalCommunicationService.uploadFile(uploadFileList, idClaims);
    }

    //REFACTOR
    @Override
    @Transactional(isolation = Isolation.SERIALIZABLE, noRollbackFor ={LockAcquisitionException.class, JpaSystemException.class} )
    @Retryable(include = LockAcquisitionException.class, maxAttempts = 15, backoff = @Backoff(delay = 1000))
    public void attachPdfFileClaim(String idClaims, String idFileManager, String userId, String userName, Boolean isPai, Boolean isLegal) throws IOException {
        LOGGER.info("Start attachPdfFileClaim");
        //recupero della nuova entità
        Optional<ClaimsNewEntity> claimsNewEntityOptional = claimsNewRepository.findByIdForUpdate(idClaims);
        LOGGER.info("Select Claims for Update");

        if (!claimsNewEntityOptional.isPresent()) {
            LOGGER.debug("Claim with id " + idClaims + " not found");
            throw new NotFoundException("Claim with id " + idClaims + " not found", MessageCode.CLAIMS_1010);
        }

        DownloadFileBase64ResponseV1 downloadFileBase64ResponseV1 = downloadFile(idFileManager);
        ClaimsNewEntity claimsNewEntity =claimsNewEntityOptional.get();
        Forms forms = claimsNewEntity.getForms();
        if (forms == null) {
            forms = new Forms();
        }
        List<Attachment> attachmentList = forms.getAttachment();
        if (attachmentList == null) {
            attachmentList = new LinkedList<>();
        }
        if (isPai)
            attachmentList.add(AttachmentAdapter.adptFromDownloadFileBase64ResponseV1toAttachmentPAIAndLegal(downloadFileBase64ResponseV1, idFileManager, userId, userName, "PAI"));
        else if(isLegal)
            attachmentList.add(AttachmentAdapter.adptFromDownloadFileBase64ResponseV1toAttachmentPAIAndLegal(downloadFileBase64ResponseV1, idFileManager, userId, userName, "LEGAL"));
        else
            attachmentList.add(AttachmentAdapter.adptFromDownloadFileBase64ResponseV1toAttachment(downloadFileBase64ResponseV1, idFileManager, userId, userName));



        forms.setAttachment(attachmentList);
        claimsNewEntity.setForms(forms);
        LOGGER.info("Saved Claims after send file");
        claimsNewRepository.save(claimsNewEntity);

        sendFileToWebSin(downloadFileBase64ResponseV1, idClaims);
        ClaimsEntity claimsEntity = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntity);
        if(dwhCall){
            dwhClaimsService.sendMessage(claimsEntity, EventTypeEnum.UPDATED);
        }

    }


    @Override
    public Forms attachPdfFileClaimEnjoy(String idClaims, String idFileManager, String userId, String userName, Boolean isPai, Boolean isLegal) throws IOException {

        DownloadFileBase64ResponseV1 downloadFileBase64ResponseV1 = downloadFile(idFileManager);
        LOGGER.info("[ENJOY] Invio File a Websin: " + downloadFileBase64ResponseV1.getFileName());
        sendFileToWebSin(downloadFileBase64ResponseV1, idClaims);
        Optional<ClaimsNewEntity> claimsEntityOptional = claimsNewRepository.findById(idClaims);

        if (!claimsEntityOptional.isPresent()) {
            LOGGER.debug("Claim with id " + idClaims + " not found");
            throw new NotFoundException("Claim with id " + idClaims + " not found", MessageCode.CLAIMS_1010);
        }
        ClaimsNewEntity claimsEntity = claimsEntityOptional.get();
        Forms forms = claimsEntity.getForms();
        if (forms == null) {
            forms = new Forms();
        }
        List<Attachment> attachmentList = forms.getAttachment();
        if (attachmentList == null) {
            attachmentList = new LinkedList<>();
        }
        if (isPai)
            attachmentList.add(AttachmentAdapter.adptFromDownloadFileBase64ResponseV1toAttachmentPAIAndLegal(downloadFileBase64ResponseV1, idFileManager, userId, userName, "PAI"));
        else if(isLegal)
            attachmentList.add(AttachmentAdapter.adptFromDownloadFileBase64ResponseV1toAttachmentPAIAndLegal(downloadFileBase64ResponseV1, idFileManager, userId, userName, "LEGAL"));
        else
            attachmentList.add(AttachmentAdapter.adptFromDownloadFileBase64ResponseV1toAttachment(downloadFileBase64ResponseV1, idFileManager, userId, userName));

        forms.setAttachment(attachmentList);

       return forms;


    }






    @Override
    public List<UploadFileResponseV1> uploadFileInsurancePolicy(List<UploadFileRequestV1> uploadFileRequest, String id, String user, String userName) throws IOException {

        List<UploadFileResponseV1> responseList = new LinkedList<>();

        Optional<InsurancePolicyEntity> optInsurancePolicyEntity = insurancePolicyRepository.findById(UUID.fromString(id));

        if (optInsurancePolicyEntity.isPresent()) {

            InsurancePolicyEntity insurancePolicyEntity = optInsurancePolicyEntity.get();

            List<Attachment> attachmentList = null;


            if (insurancePolicyEntity.getAttachmentList() == null)
                insurancePolicyEntity.setAttachmentList(new LinkedList<>());

            attachmentList = insurancePolicyEntity.getAttachmentList();

            for (UploadFileRequestV1 uploadFile : uploadFileRequest) {

                try {

                    uploadFile.setResourceId(id);
                    uploadFile.setResourceType("policy");

                    UploadFileResponseV1 response = callWithoutCert(endpointUrl + uploadApi + "?thumbnail=true", HttpMethod.PUT, uploadFile, header, new ParameterizedTypeReference<UploadFileResponseV1>() {
                    });
                    response.setExternalId(uploadFile.getExternalId());
                    Attachment attachment = new Attachment();

                    attachment.setAttachmentType(attachmentTypeRepository.getOne(uploadFile.getAttachmentType().getId()));
                    attachment.setBlobName(response.getBlobName());
                    attachment.setFileManagerName(response.getFileName());
                    attachment.setCreatedAt(DateUtil.getNowDate());
                    attachment.setFileManagerId(response.getUuid());
                    attachment.setMimeType(response.getMimeType());
                    attachment.setUserId(user);
                    attachment.setUserName(userName);
                    attachment.setThumbnail(response.getThumbnailId());
                    attachment.setDescription(response.getDescription());
                    attachment.setExternalId(response.getExternalId());

                    if (attachmentList == null) {
                        attachmentList = new LinkedList<>();
                    }
                    attachmentList.add(attachment);

                    response.setAttachmentTypeResponseList(AttachmentTypeAdapter.getAttachmentTypeAdapter(attachment.getAttachmentType()));
                    responseList.add(response);

                } catch (Exception ex) {
                    LOGGER.debug(ex.getMessage());
                    throw new ConnectException(ex.getMessage());
                }
            }

            insurancePolicyEntity.setAttachmentList(attachmentList);

            insurancePolicyRepository.save(insurancePolicyEntity);

            return responseList;
        }
        return null;
    }

    @Override
    public List<Attachment> deleteFileInsurancePolicy(List<String> idAttachmentList, UUID idPolicy) throws ConnectException {

        if (idAttachmentList != null && idPolicy != null) {

            Optional<InsurancePolicyEntity> optInsurancePolicy = insurancePolicyRepository.findById(idPolicy);

            if (optInsurancePolicy.isPresent()) {

                InsurancePolicyEntity insurancePolicy = optInsurancePolicy.get();

                List<Attachment> insurancePolicyAttachment = insurancePolicy.getAttachmentList();

                for (String attId : idAttachmentList) {

                    insurancePolicyAttachment.removeIf(attAttachProva -> attAttachProva.getFileManagerId().equalsIgnoreCase(attId));

                    try {

                        callWithoutCert(endpointUrl + prefixDownloadBase64Api + attId + "/delete", HttpMethod.DELETE, null, header, null);

                    } catch (IOException e) {
                        LOGGER.debug(e.getMessage());
                        throw new ConnectException(e.getMessage());
                    }

                }

                insurancePolicy.setAttachmentList(insurancePolicyAttachment);
                insurancePolicyRepository.save(insurancePolicy);

                return insurancePolicyAttachment;

            } else {
                LOGGER.debug("Insurance Policy with id " + idPolicy + " not found");
                throw new NotFoundException("Insurance Policy with id " + idPolicy + " not found", MessageCode.CLAIMS_1010);
            }

        } else {
            LOGGER.debug("Insurance Policy with id " + idPolicy + " not found");
            throw new NotFoundException("Insurance Policy with id " + idPolicy + " not found", MessageCode.CLAIMS_1010);
        }
    }

    @Override
    public List<Attachment> deleteFileListForPractice(List<String> attachmentsList, String idPractice) throws ConnectException {

        Optional<PracticeEntity> practiceEntityOpt = practiceRepository.findById(UUID.fromString(idPractice));

        if (!practiceEntityOpt.isPresent()) {
            LOGGER.debug("Practice with id " + idPractice + " not found");
            throw new NotFoundException("Practice with id " + idPractice + " not found", MessageCode.CLAIMS_1010);
        }

        PracticeEntity practiceEntity = practiceEntityOpt.get();

        for (String id : attachmentsList) {

            practiceEntity.getAttachmentList().removeIf(attachment1 -> attachment1.getFileManagerId().equalsIgnoreCase(id));

            try {

                callWithoutCert(endpointUrl + prefixDownloadBase64Api + id + "/delete", HttpMethod.DELETE, null, header, null);

            } catch (IOException e) {
                LOGGER.debug(e.getMessage());
                throw new ConnectException(e.getMessage());
            }
        }

        practiceRepository.save(practiceEntity);
        return practiceEntity.getAttachmentList();
    }

    @Override
    public List<UploadFileResponseV1> uploadFilePractice(List<UploadFileClaimsRequestV1> uploadFileRequest, String id, String user, String userName) throws IOException {

        List<UploadFileResponseV1> responseList = new LinkedList<>();
        PracticeEntity practiceEntity = practiceRepository.getOne(UUID.fromString(id));
        List<Attachment> attachmentList = null;

        attachmentList = practiceEntity.getAttachmentList();


        for (UploadFileClaimsRequestV1 uploadFile : uploadFileRequest) {

            try {

                uploadFile.setResourceId(id);

                uploadFile.setResourceType("practice");
                UploadFileResponseV1 response = callWithoutCert(endpointUrl + uploadApi + "?thumbnail=true", HttpMethod.PUT, uploadFile, header, new ParameterizedTypeReference<UploadFileResponseV1>() {
                });
                response.setExternalId(uploadFile.getExternalId());
                //UploadFileResponseV1 response = callWithoutCert( "https://nemo-dev.doing.com/filemanager/system/ping", HttpMethod.GET, uploadFileRequest, header, UploadFileResponseV1.class );
                //UploadFileResponseV1 response = callWithoutCert( "https://nemo-filemanager-service/system/ping", HttpMethod.GET, null, header, null );

                Attachment attachment = new Attachment(
                        response.getUuid(),
                        response.getFileName(),
                        response.getBlobName(),
                        response.getFileSize(),
                        response.getMimeType(),
                        response.getDescription(),
                        Util.nowDateISO8601(),
                        user,
                        userName,
                        response.getThumbnailId(),
                        uploadFile.isHidden() == null ? false : uploadFile.isHidden(),
                        uploadFile.getExternalId(),
                        false
                );

                if (attachmentList == null) {
                    attachmentList = new LinkedList<>();
                }
                attachmentList.add(attachment);
                responseList.add(response);

            } catch (Exception ex) {
                LOGGER.debug(ex.getMessage());
                throw new ConnectException(ex.getMessage());
            }
        }
        practiceEntity.setAttachmentList(attachmentList);
        practiceRepository.save(practiceEntity);

        return responseList;
    }

    public static class CustomTypeReference extends TypeReference<Object> {
        private final Type type;

        public CustomTypeReference(ParameterizedTypeReference pt) {
            this.type = pt.getType();
        }

        @Override
        public Type getType() {
            return type;
        }
    }

    public List<UploadFileResponseV1> uploadFilePractice(List<UploadFileClaimsRequestV1> uploadFileRequest, Long idClaims, String user, String userName, Boolean isValidate) throws MigrationException, IOException {

        ClaimsNewEntity claimsEntity = claimsNewRepository.getClaimsByIdPratica(idClaims);
        if(claimsEntity == null)
            throw new MigrationException("Claims with id: "+idClaims+" not found");
        return uploadFile(uploadFileRequest, claimsEntity.getId(), null, user, userName, isValidate);
    }

    public List<UploadFileResponseV1> uploadFilePracticeNew(List<UploadFileClaimsMigrationRequestV1> uploadFileRequest, Long idPractice, String user, String userName, Boolean isValidate) throws MigrationException, IOException, ParseException {

        ClaimsNewEntity claimsEntity = claimsNewRepository.getClaimsByIdPratica(idPractice);
        if(claimsEntity == null)
            throw new MigrationException("Claims with id: "+idPractice+" not found");

        List<UploadFileClaimsRequestV1> uploadFileClaimsRequestV1List = new LinkedList<>();

        UploadFileClaimsRequestV1 uploadFileClaimsRequestV1 = new UploadFileClaimsRequestV1();
        for(UploadFileClaimsMigrationRequestV1 current : uploadFileRequest) {
            uploadFileClaimsRequestV1.setDescription(current.getDescription());
            uploadFileClaimsRequestV1.setFileName(current.getFileName());
            uploadFileClaimsRequestV1.setBlobType("ATT");
            uploadFileClaimsRequestV1.setResourceType("claims");

            uploadFileClaimsRequestV1List.add(uploadFileClaimsRequestV1);
        }
        return uploadFileNew(uploadFileClaimsRequestV1List, claimsEntity.getId(), null, user, userName, isValidate);
    }

    private <T, P> T callWithoutCertNew(String url, HttpMethod method, P value, HashMap<String, String> headersparams, ParameterizedTypeReference<T> typeReference) throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = null;

        if (value != null) {
            jsonInString = mapper.writeValueAsString(value);
        }

        Response responseToken = httpUtil.callURL(headersparams, url, jsonInString, method);


        if (!responseToken.isSuccessful() ) {
            String messageError = mapper.readValue(responseToken.body().string(), ResponseV2.class).getError().getMessage();
            LOGGER.debug(messageError);
            throw new MigrationException(messageError);


        }

        if (typeReference == null)
            return null;

        String response = responseToken.body().string();
        TypeReference tr = new CustomTypeReference(typeReference);
        return mapper.readValue(response, tr);
    }

    //REFACTOR
    private List<UploadFileResponseV1> uploadFileNew(List<UploadFileClaimsRequestV1> uploadFileRequest, String idClaims, String idCounterparty, String user, String userName, Boolean isValidate) throws IOException, ParseException {

        List<UploadFileResponseV1> responseList = new LinkedList<>();
        ClaimsEntity claimsEntity = null;
        List<Attachment> attachmentList = null;
        CounterpartyEntity counterparty = null;
        ClaimsNewEntity claimsNewEntity = null;
        CounterpartyNewEntity counterpartyNewEntity = null;

        if (idCounterparty == null) {
            claimsNewEntity = claimsNewRepository.getOne(idClaims);
            claimsEntity = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntity);
            if (claimsEntity.getForms() == null)
                claimsEntity.setForms(new Forms());
            else
                attachmentList = claimsEntity.getForms().getAttachment();

        } else {
            counterpartyNewEntity = counterpartyNewRepository.getOne(idCounterparty);
            counterparty = CounterpartyAdapter.adptCounterpartyNewToCounterpartyOld(counterpartyNewEntity);
            if (counterparty == null) {
                throw new NotFoundException("Counterparty with UUID: " + idCounterparty + " not found in this Claim", MessageCode.CLAIMS_1010);
            }
            attachmentList = counterparty.getAttachments();
        }

        for (UploadFileClaimsRequestV1 uploadFile : uploadFileRequest) {

           // try {
                if (idCounterparty != null)
                    uploadFile.setResourceId(idCounterparty);
                else
                    uploadFile.setResourceId(idClaims);

                uploadFile.setResourceType("claims");
                UploadFileRequestV1 uploadFileRequestV1 = UploadFileAdapter.adptFromUploadFileClaimsRequestV1ToUploadFileRequestV1(uploadFile);
                String practiceId=claimsEntity.getPracticeId().toString();
                String  url = buildUrl(endpointUrl,uploadFileMigrationApi,new HashMap<String, String>() {{
                            put("practice_id",practiceId);
                        }},
                        null
                );
                UploadFileResponseV1 response = callWithoutCertNew(url, HttpMethod.POST, uploadFileRequestV1, header, new ParameterizedTypeReference<UploadFileResponseV1>() {
                });

                //UploadFileResponseV1 response = callWithoutCert( "https://nemo-dev.doing.com/filemanager/system/ping", HttpMethod.GET, uploadFileRequest, header, UploadFileResponseV1.class );
                //UploadFileResponseV1 response = callWithoutCert( "https://nemo-filemanager-service/system/ping", HttpMethod.GET, null, header, null );
                response.setExternalId(uploadFile.getExternalId());
                Attachment attachment = new Attachment(
                        response.getUuid(),
                        response.getFileName(),
                        response.getBlobName(),
                        response.getFileSize(),
                        response.getMimeType(),
                        response.getDescription(),
                        Util.nowDateISO8601(),
                        user,
                        userName,
                        response.getThumbnailId(),
                        uploadFile.isHidden() == null ? false : uploadFile.isHidden(),
                        isValidate == null ? false : isValidate,
                        uploadFile.getExternalId(),
                        false
                );

                if (attachmentList == null) {
                    attachmentList = new LinkedList<>();
                }
                attachmentList.add(attachment);
                responseList.add(response);

            /*} catch (Exception ex) {
                LOGGER.debug(ex.getMessage());
                throw new ConnectException(ex.getMessage());
            }*/
        }

        if (counterparty == null) {
            claimsEntity.getForms().setAttachment(attachmentList);
            if (claimsEntity.getWithContinuation() == null || !claimsEntity.getWithContinuation()) {
                if (isValidate == null || !isValidate)
                    claimsEntity.setWithContinuation(true);
            }

            claimsNewEntity = converterClaimsService.convertOldEntityToNewClaimsEntity(claimsEntity);
            claimsNewRepository.save(claimsNewEntity);
      /*      if(dwhCall){
                dwhClaimsService.sendMessage(claimsEntity, EventTypeEnum.UPDATED);
            }*/
            cacheService.deleteClaimsStats(CACHENAME, STATSkEY);
            cacheService.deleteClaimsStats(CACHENAME, STATSEVIDENCE);
            cacheService.deleteClaimsStats(CACHENAME, STATSSTATUS);
            cacheService.deleteClaimsStats(CACHENAME, STATSTHEFT);

            cacheService.deleteClaimsStats(CACHENAME, STATSEVIDENCECONTINUATION);
            cacheService.deleteClaimsStats(CACHENAME, STATSkEY2);
        } else {

            counterparty.setAttachments(attachmentList);
            if(counterparty.getIsReadMsa() != null && counterparty.getIsReadMsa()){
                counterparty.setIsReadMsa(false);
            }
            counterpartyNewEntity = CounterpartyAdapter.adptCounterpartyOldToCounterpartyNew(counterparty);
            counterpartyNewRepository.save(counterpartyNewEntity);
        }


        return responseList;
    }

    private String buildUrl(String baseUrl, String uri, Map<String, String> pathParams, MultiValueMap<String, String> queryParams) {

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(baseUrl.concat(uri));

        if (MapUtils.isNotEmpty(queryParams)) {
            builder.queryParams(queryParams);
        }

        return builder.buildAndExpand(pathParams).toString();
    }
}
