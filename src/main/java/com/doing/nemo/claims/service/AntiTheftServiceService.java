package com.doing.nemo.claims.service;

import com.doing.nemo.claims.controller.payload.response.AntiTheftServiceResponseV1;
import com.doing.nemo.claims.entity.enumerated.AntitheftEnum.AntitheftTypeEnum;
import com.doing.nemo.claims.entity.settings.AntiTheftServiceEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public interface AntiTheftServiceService {
    AntiTheftServiceEntity insertAntiTheftService(AntiTheftServiceEntity antiTheftServiceEntity);

    AntiTheftServiceEntity updateAntiTheftService(AntiTheftServiceEntity antiTheftServiceEntity);

    AntiTheftServiceEntity selectAntiTheftService(UUID id);

    List<AntiTheftServiceEntity> selectAllAntiTheftService();

    AntiTheftServiceResponseV1 deleteAntiTheftService(UUID id);

    AntiTheftServiceEntity updateStatus(UUID uuid);
    AntiTheftServiceEntity selectAntiTheftServiceByProvider(String provider);

    List<AntiTheftServiceEntity> findAntiTheftServicesFiltered (Integer page, Integer pageSize, String orderBy, Boolean asc, String codeAntiTheftService, String name, String businessName, Integer supplierCode, String email, String phoneNumber, String providerType, Integer timeOutInSeconds, String endPointUrl, String username, String password, String companyCode, Boolean active, Boolean isActive, AntitheftTypeEnum type);

    Long countAntiTheftServiceFiltered(String codeAntiTheftService, String name, String businessName, Integer supplierCode, String email, String phoneNumber, String providerType, Integer timeOutInSeconds, String endPointUrl, String username, String password, String companyCode, Boolean active, Boolean isActive, AntitheftTypeEnum type);



    }
