package com.doing.nemo.claims.service;

import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.Pagination;
import org.springframework.stereotype.Service;

@Service
public interface ExternalManagerService {
    //REFACTOR
    Pagination<ClaimsEntity> getAcclaimsClaimsEntity(Long practiceId, String plate, String dateAccident, Boolean asc, String orderBy, int page, int pageSize);
    //REFACTOR
    Pagination<ClaimsEntity> getMsaClaimsEntity(Long practiceId, String plate, String dateAccident,  Boolean asc, String orderBy, int page, int pageSize);

}
