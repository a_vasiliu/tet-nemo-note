package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.controller.payload.response.PaginationStats;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.settings.NotificationTypeEntity;
import com.doing.nemo.claims.repository.NotificationTypeRepository;
import com.doing.nemo.claims.repository.NotificationTypeRepositoryV1;
import com.doing.nemo.claims.service.NotificationTypeService;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class NotificationTypeServiceImpl implements NotificationTypeService {

    private static Logger LOGGER = LoggerFactory.getLogger(NotificationTypeServiceImpl.class);
    @Autowired
    private NotificationTypeRepository notificationTypeRepository;
    @Autowired
    private NotificationTypeRepositoryV1 notificationTypeRepositoryV1;

    @Override
    public NotificationTypeEntity insertNotificationType(NotificationTypeEntity notificationTypeEntity) {

        notificationTypeRepository.save(notificationTypeEntity);
        return notificationTypeEntity;
    }

    @Override
    public NotificationTypeEntity updateNotificationType(NotificationTypeEntity notificationTypeEntity, UUID uuid) {
        Optional<NotificationTypeEntity> notificationTypeEntityOld = notificationTypeRepository.findById(uuid);
        if (!notificationTypeEntityOld.isPresent()) {
            LOGGER.debug("NotificationType with id " + uuid + " not found");
            throw new NotFoundException("NotificationType with id " + uuid + " not found", MessageCode.CLAIMS_1010);
        }

        notificationTypeEntity.setId(notificationTypeEntityOld.get().getId());
        notificationTypeRepository.save(notificationTypeEntity);
        return notificationTypeEntity;
    }

    @Override
    public NotificationTypeEntity getNotificationType(UUID uuid) {
        Optional<NotificationTypeEntity> notificationTypeEntity = notificationTypeRepository.findById(uuid);
        if (!notificationTypeEntity.isPresent()) {
            LOGGER.debug("NotificationType with id " + uuid + " not found");
            throw new NotFoundException("NotificationType with id " + uuid + " not found", MessageCode.CLAIMS_1010);
        }

        return notificationTypeEntity.get();
    }

    @Override
    public List<NotificationTypeEntity> getAllNotificationType() {
        List<NotificationTypeEntity> notificationTypeEntities = notificationTypeRepository.findAllWithOrder();

        return notificationTypeEntities;
    }


    @Override
    public NotificationTypeEntity deleteNotificationType(UUID uuid) {
        Optional<NotificationTypeEntity> notificationTypeEntity = notificationTypeRepository.findById(uuid);
        if (!notificationTypeEntity.isPresent()) {
            LOGGER.debug("NotificationType with id " + uuid + " not found");
            throw new NotFoundException("NotificationType with id " + uuid + " not found", MessageCode.CLAIMS_1010);
        }

        notificationTypeRepository.delete(notificationTypeEntity.get());

        return null;
    }

    @Override
    public NotificationTypeEntity updateStatus(UUID uuid) {
        NotificationTypeEntity notificationTypeEntity = getNotificationType(uuid);
        if (notificationTypeEntity.getActive()) {
            notificationTypeEntity.setActive(false);
        } else {
            notificationTypeEntity.setActive(true);
        }

        notificationTypeRepository.save(notificationTypeEntity);
        return notificationTypeEntity;
    }

    @Override
    public Pagination<NotificationTypeEntity> paginationNotificationType(int page, int pageSize, String description, Boolean isActive, Integer orderId, String orderBy, Boolean asc) {
        Pagination<NotificationTypeEntity> notificationTypePagination = new Pagination<>();
        List<NotificationTypeEntity> notificationTypeList = notificationTypeRepositoryV1.findPaginationNotificationType(page, pageSize, description, isActive, orderId, orderBy, asc);
        Long itemCount = notificationTypeRepositoryV1.countPaginationNotificationType(description, isActive, orderId);
        notificationTypePagination.setItems(notificationTypeList);
        notificationTypePagination.setStats(new PaginationStats(itemCount.intValue(), page, pageSize));

        return notificationTypePagination;
    }


}
