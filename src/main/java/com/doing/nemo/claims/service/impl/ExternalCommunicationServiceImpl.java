package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.adapter.IncidentAdapter;
import com.doing.nemo.claims.commandHandler.ClaimsInsertExternalCommandHandler;
import com.doing.nemo.claims.controller.payload.UploadFileClaimsRequestV1;
import com.doing.nemo.claims.controller.payload.request.IncidentRequestV1;
import com.doing.nemo.claims.controller.payload.response.IncidentResponseV1;
import com.doing.nemo.claims.entity.*;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.doing.nemo.claims.entity.enumerated.ExternalCommunicationEnum;
import com.doing.nemo.claims.entity.jsonb.UploadFile;
import com.doing.nemo.claims.exception.ExternalCommunicationException;
import com.doing.nemo.claims.exception.UnsupportedOperationException;
import com.doing.nemo.claims.exception.WebSinClaimCommunicationException;
import com.doing.nemo.claims.exception.WebSinFileCommunicationException;
import com.doing.nemo.claims.repository.*;
import com.doing.nemo.claims.service.ExternalCommunicationService;
import com.doing.nemo.claims.service.IncidentService;
import com.doing.nemo.claims.service.LockService;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.claims.websin.WebSinServiceManager;
import com.doing.nemo.claims.websin.websinws.PutComplaintRequest;
import com.doing.nemo.claims.websin.websinws.PutComplaintResponseType;
import com.doing.nemo.claims.websin.websinws.UploadFileRequest;
import com.doing.nemo.claims.websin.websinws.UploadFileResponseType;
import com.doing.nemo.commons.exception.AbstractException;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import com.doing.nemo.middleware.client.MiddlewareClient;
import com.doing.nemo.middleware.client.payload.request.MiddlewareUpdateIncidentsRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Future;

@Service
public class ExternalCommunicationServiceImpl implements ExternalCommunicationService {

    private static Logger LOGGER = LoggerFactory.getLogger(ClaimsInsertExternalCommandHandler.class);

    private static final String ERROR_IN_MILES = "ERROR_IN_MILES";

    @Autowired
    private IncidentService incidentService;

    @Autowired
    private MilesExternalRepository milesExternalRepository;

    @Autowired
    private WebSinExternalRepository webSinExternalRepository;

    @Autowired
    private WebSinFileExternalRepository webSinFileExternalRepository;

    @Autowired
    private ExternalCommunicationRepository externalCommunicationRepository;

    @Autowired
    private MiddlewareClient middlewareClient;

    @Autowired
    private ClaimsNewRepository claimsNewRepository;

    @Autowired
    private WebSinServiceManager webSinServiceManager;

    @Autowired
    private IncidentAdapter incidentAdapter;

    @Autowired
    private LockService lockService;

    @Value("${userId.batch}")
    private String userIdBatch;

    @Override
    @Async
    public Future<IncidentResponseV1> insertIncidentAsync(String claimsId, ClaimsStatusEnum oldStatus) throws IOException {
        IncidentResponseV1 incidentResponseV1 = null;
        MiddlewareUpdateIncidentsRequest milesErrorRequest = null;
        String milesErrorMessage = null;
        PutComplaintRequest websinRequest = null;
        PutComplaintResponseType putComplaintResponseType = null;
        updateOldMilesRowToOutDated(claimsId);
        updateOldWebsinRowToOutDated(claimsId);
        try {
            incidentResponseV1 = incidentService.insertIncident(claimsId, oldStatus);
            websinRequest = webSinServiceManager.buildPutComplaintRequest(claimsId);
            try {
                putComplaintResponseType = webSinServiceManager.putComplaint(websinRequest);
                insertWebSinDatabase(websinRequest, null, claimsId, true);
            }catch (WebSinClaimCommunicationException e){
                insertWebSinDatabase(websinRequest,e.getMessage(), claimsId);
            }

        } catch (ExternalCommunicationException e) {
            LOGGER.info(MessageCode.CLAIMS_1112.value());
            incidentResponseV1 = e.getIncidentResponseV1();
            milesErrorRequest = e.getRequest();
            milesErrorMessage = e.getMessage();
        }

        // Nel caso l'oggetto milesErrorRequest é diverso da Null significa che c'è stato un errore nella chiamata
        // a miles di conseguenza devo annullare tutte le eventuali righe presenti nella tabella
        // <i>external_communication</i> che fanno riferimento al claims in esame, sia per il servizio Miles che Websin.
        if (milesErrorRequest != null) {

            updateOldMilesRowToOutDated(claimsId);
            updateOldWebsinRowToOutDated(claimsId);

            //Inserimento nuova riga miles in tabella LOG
            insertMilesDatabase(milesErrorRequest,milesErrorMessage ,claimsId);

            websinRequest = webSinServiceManager.buildPutComplaintRequest(claimsId);
            insertWebSinDatabase(websinRequest, ERROR_IN_MILES, claimsId);


        }
        return new AsyncResult<>(incidentResponseV1);
    }

    /**
     * Aggiorna nella tabella external_communication le righe di Miles che fanno riferimento al claimsId passato in
     * input e che hanno stato TO_BE_RECOVERED passandole allo stato OUTDATED
     *
     * @param claimsId id del claims a cui fanno riferimento le righe da aggiornare
     */
    private void updateOldMilesRowToOutDated(String claimsId){
        List<MilesExternalEntity> milesExternalEntityOutdatedList = milesExternalRepository.searchByStatus(ExternalCommunicationEnum.TO_BE_RECOVERED, claimsId);
        if (milesExternalEntityOutdatedList != null && milesExternalEntityOutdatedList.size() > 0) {
            for (MilesExternalEntity milesExternalEntityOutdated : milesExternalEntityOutdatedList) {
                milesExternalEntityOutdated.setStatus(ExternalCommunicationEnum.OUTDATED);
                milesExternalRepository.save(milesExternalEntityOutdated);
            }
        }
    }

    /**
     * Aggiorna nella tabella external_communication le righe di WebSin che fanno riferimento al claimsId passato in
     * input e che hanno stato TO_BE_RECOVERED passandole allo stato OUTDATED
     *
     * @param claimsId id del claims a cui fanno riferimento le righe da aggiornare
     */
    private void updateOldWebsinRowToOutDated(String claimsId){
        List<WebSinExternalEntity> webSinExternalEntityOutdatedList = webSinExternalRepository.searchByStatus(ExternalCommunicationEnum.TO_BE_RECOVERED, claimsId);
        if (webSinExternalEntityOutdatedList != null && webSinExternalEntityOutdatedList.size() > 0) {
            for (WebSinExternalEntity webSinExternalEntityOutdated:webSinExternalEntityOutdatedList){
                webSinExternalEntityOutdated.setStatus(ExternalCommunicationEnum.OUTDATED);
                webSinExternalRepository.save(webSinExternalEntityOutdated);
            }
        }
    }
    /**
     * Costruisce una nuova MilesExternalEntity inserendo la richiesta andata in errore e settando come stato TO_BE_RECOVERED
     * inserisce poi nel DB nella tabella external_communication la nuova riga
     *
     * @param milesErrorRequest richiesta inviata a miles andata in errore
     * @param claimsId id del sinistro per cui la chiamata a miles è andata in errore
     */
    private void insertMilesDatabase(MiddlewareUpdateIncidentsRequest milesErrorRequest, String erroMessage, String claimsId){
        MilesExternalEntity milesExternalEntity = new MilesExternalEntity();
        milesExternalEntity.setRequest(milesErrorRequest);
        milesExternalEntity.setStatus(ExternalCommunicationEnum.TO_BE_RECOVERED);
        milesExternalEntity.setClaimsId(claimsId);
        erroMessage = StringUtils.abbreviate(erroMessage, 255);
        milesExternalEntity.setErrorMessage(erroMessage);
        milesExternalRepository.save(milesExternalEntity);
    }



    private void insertWebSinDatabase(PutComplaintRequest websinErrorRequest, String errorMessage,  String claimsId){
        insertWebSinDatabase(websinErrorRequest, errorMessage, claimsId, false);
    }

    /**
     * Costruisce una nuova WebSinExternalEntity inserendo la richiesta andata in errore e settando come stato TO_BE_RECOVERED
     * inserisce poi nel DB nella tabella external_communication la nuova riga
     *
     * @param websinErrorRequest richiesta inviata a miles andata in errore
     * @param claimsId id del sinistro per cui la chiamata a miles è andata in errore
     */
    private void insertWebSinDatabase(PutComplaintRequest websinErrorRequest, String errorMessage,  String claimsId, boolean isOk){
        WebSinExternalEntity webSinExternalEntity = new WebSinExternalEntity();
        if(isOk) {
            webSinExternalEntity.setStatus(ExternalCommunicationEnum.SEND_OK);
        }else{
            webSinExternalEntity.setStatus(ExternalCommunicationEnum.TO_BE_RECOVERED);
        }
        errorMessage = StringUtils.abbreviate(errorMessage, 255);
        webSinExternalEntity.setErrorMessage(errorMessage);
        webSinExternalEntity.setRequest(websinErrorRequest);
        webSinExternalEntity.setClaimsId(claimsId);
        webSinExternalRepository.save(webSinExternalEntity);
    }

    @Override
    @Async
    @Transactional(isolation = Isolation.SERIALIZABLE)
    public Future<ClaimsEntity> insertIncidentEnjoyAsync(ClaimsEntity claimsEntity, ClaimsStatusEnum oldStatus) throws IOException{
        LOGGER.info("[ENJOY] insertIncidentEnjoyAsync");
        ClaimsEntity claimsEntity1 = claimsEntity;
        MiddlewareUpdateIncidentsRequest milesErrorRequest = null;
        String milesErrorMessage = null;
        PutComplaintRequest websinRequest = null;
        PutComplaintResponseType putComplaintResponseType = null;
        String descriptionError = null;
        updateOldMilesRowToOutDated(claimsEntity1.getId());
        updateOldWebsinRowToOutDated(claimsEntity1.getId());
        try {
            LOGGER.info("[ENJOY] insertIncidentEnjoyAsync");
            claimsEntity1 = incidentService.insertIncidentEnjoy(claimsEntity, oldStatus);
            //Chiamata a WebSin
            LOGGER.info("[ENJOY] insertIncidentEnjoyAsync FineOK");
            LOGGER.info("[ENJOY] BuildWebSin");
            websinRequest = webSinServiceManager.buildPutComplaintRequest(claimsEntity1.getId());
            LOGGER.info("[ENJOY] InvioWebSinOK");
            try {
                putComplaintResponseType = webSinServiceManager.putComplaint(websinRequest);
                insertWebSinDatabase(websinRequest, null,claimsEntity1.getId(),true);
            }catch (WebSinClaimCommunicationException e){
                insertWebSinDatabase(websinRequest, e.getMessage(),claimsEntity1.getId());
            }

        } catch (ExternalCommunicationException e){
            LOGGER.info(MessageCode.CLAIMS_1112.value());
            milesErrorRequest = e.getRequest();
            descriptionError = e.getMessage();
            milesErrorMessage = e.getMessage();
            LOGGER.info(MessageCode.CLAIMS_1112.value());
        }
        if(milesErrorRequest != null){
            updateOldMilesRowToOutDated(claimsEntity1.getId());
            updateOldWebsinRowToOutDated(claimsEntity1.getId());

            //Inserimento nuova riga miles in tabella LOG
            insertMilesDatabase(milesErrorRequest,milesErrorMessage ,claimsEntity1.getId());

            websinRequest = webSinServiceManager.buildPutComplaintRequest(claimsEntity1.getId());
            insertWebSinDatabase(websinRequest, ERROR_IN_MILES,claimsEntity1.getId());

            IncidentRequestV1 incidentRequestV1 = incidentAdapter.adptFromClaimsEntityToIncidentRequest(claimsEntity, oldStatus, false,false);
            claimsEntity1.addHistorical(incidentService.addIncidentHistoricalInsert(incidentRequestV1,claimsEntity, true, descriptionError));
        }
        return new AsyncResult<>(claimsEntity1);
    }

    @Override
    @Transactional(isolation = Isolation.SERIALIZABLE)
    public void modifyIncidentSync(String claimsId, ClaimsStatusEnum oldStatus, Boolean franchise, IncidentRequestV1 oldValue, Boolean lastPayment, ClaimsEntity oldClaims) throws IOException{
        this.modifyIncidentAsync(claimsId, oldStatus,franchise,oldValue,lastPayment,oldClaims);
    }

    @Override
    @Async
    @Transactional(isolation = Isolation.SERIALIZABLE)
    public Future<IncidentResponseV1>modifyIncidentAsync(String claimsId, ClaimsStatusEnum oldStatus, Boolean franchise, IncidentRequestV1 oldValue, Boolean lastPayment, ClaimsEntity oldClaims) throws IOException{
        IncidentResponseV1 incidentResponseV1 = null;
        MiddlewareUpdateIncidentsRequest milesErrorRequest = null;
        String milesErrorMessage = null;
        PutComplaintRequest websinRequest = null;
        PutComplaintResponseType putComplaintResponseType = null;
        updateOldMilesRowToOutDated(claimsId);
        updateOldWebsinRowToOutDated(claimsId);
        try {
            incidentResponseV1 = incidentService.modifyIncident(claimsId, oldStatus,franchise,oldValue,lastPayment,oldClaims);
            websinRequest = webSinServiceManager.buildPutComplaintRequest(claimsId);
            try {
                putComplaintResponseType = webSinServiceManager.putComplaint(websinRequest);
                insertWebSinDatabase(websinRequest, null, claimsId,true);
            } catch (WebSinClaimCommunicationException e) {
                insertWebSinDatabase(websinRequest, e.getMessage(), claimsId);
            }
        } catch (ExternalCommunicationException e) {
            LOGGER.info(MessageCode.CLAIMS_1112.value());
            incidentResponseV1 = e.getIncidentResponseV1();
            milesErrorRequest = e.getRequest();
            milesErrorMessage = e.getMessage();
        }

        // Nel caso l'oggetto milesErrorRequest é diverso da Null significa che c'è stato un errore nella chiamata
        // a miles di conseguenza devo annullare tutte le eventuali righe presenti nella tabella
        // <i>external_communication</i> che fanno riferimento al claims in esame, sia per il servizio Miles che Websin.

        if (milesErrorRequest != null) {
            updateOldMilesRowToOutDated(claimsId);
            updateOldWebsinRowToOutDated(claimsId);

            //Inserimento nuova riga miles in tabella LOG
            insertMilesDatabase(milesErrorRequest,milesErrorMessage ,claimsId);

            websinRequest = webSinServiceManager.buildPutComplaintRequest(claimsId);
            insertWebSinDatabase(websinRequest, ERROR_IN_MILES,claimsId);

        }

        return new AsyncResult<>(incidentResponseV1);
    }

    @Override
    public Future<UploadFileResponseType> uploadFile(List<UploadFile> uploadFileList, String claimsId) {
        UploadFileResponseType response=null;

        List<MilesExternalEntity> milesExternalEntityList = milesExternalRepository.searchByStatus(ExternalCommunicationEnum.TO_BE_RECOVERED, claimsId);
        if(milesExternalEntityList.isEmpty()) {
            for (UploadFile uploadFile : uploadFileList) {
                UploadFileRequest uploadFileRequest = webSinServiceManager.buildUploadFileRequest(uploadFile, claimsId);
                try {
                    response = webSinServiceManager.uploadFile(uploadFileRequest);
                    insertWebSinFileDatabase(uploadFileRequest, null, claimsId, ExternalCommunicationEnum.SEND_OK);
                } catch (WebSinFileCommunicationException e) {
                    insertWebSinFileDatabase(e.getRequest(), e.getMessage(),claimsId);
                }
            }
        }else{
            for (UploadFile uploadFile : uploadFileList) {
                UploadFileRequest uploadFileRequest = webSinServiceManager.buildUploadFileRequest(uploadFile, claimsId);
                insertWebSinFileDatabase(uploadFileRequest, ERROR_IN_MILES, claimsId);
            }
        }
        return new AsyncResult<>(response);
    }

    private void insertWebSinFileDatabase(UploadFileRequest request,String errorMessage, String claimsId){
        insertWebSinFileDatabase(request,errorMessage,claimsId,ExternalCommunicationEnum.TO_BE_RECOVERED);
    }

    private void insertWebSinFileDatabase(UploadFileRequest request,String errorMessage, String claimsId,ExternalCommunicationEnum externalCommunicationEnum){
        WebSinFileExternalEntity webSinFileExternalEntity = new WebSinFileExternalEntity();
        if (externalCommunicationEnum.getValue().equalsIgnoreCase(ExternalCommunicationEnum.TO_BE_RECOVERED.getValue())){
            webSinFileExternalEntity.setRequest(request);
        }
        webSinFileExternalEntity.setClaimsId(claimsId);
        webSinFileExternalEntity.setStatus(externalCommunicationEnum);
        errorMessage = StringUtils.abbreviate(errorMessage, 255);
        webSinFileExternalEntity.setErrorMessage(errorMessage);
        webSinFileExternalRepository.save(webSinFileExternalEntity);
    }

    @Override
    @Async
    public void retryCallMilesWebSin(){
        List<String> externalCallIdClaims = externalCommunicationRepository.getAllIdClaimsByStatus(ExternalCommunicationEnum.TO_BE_RECOVERED);
        Boolean resultMilesCall = true;
        Boolean resultWebSinCall = true;
        for(String id: externalCallIdClaims){
            try {
                lockService.lockPractice(userIdBatch, id);

                ClaimsNewEntity claimsNewEntity = claimsNewRepository.getOne(id);
                List<MilesExternalEntity> milesExternalEntityList = milesExternalRepository.searchByStatus(ExternalCommunicationEnum.TO_BE_RECOVERED, id);
                List<WebSinExternalEntity> webSinExternalEntityList = webSinExternalRepository.searchByStatus(ExternalCommunicationEnum.TO_BE_RECOVERED,id);
                List<WebSinFileExternalEntity> webSinFileExternalEntityList = webSinFileExternalRepository.searchByStatus(ExternalCommunicationEnum.TO_BE_RECOVERED,id);
                //Rieseguo per prime le chiamate a Miles

                if(claimsNewEntity!= null && milesExternalEntityList.size()>0){
                    MilesExternalEntity milesExternalEntity = milesExternalEntityList.get(0);
                    resultMilesCall = retryCallMiles(milesExternalEntity, Long.toString(claimsNewEntity.getClaimsDamagedContractEntity().getContractId()));

                    if(resultMilesCall){
                        milesExternalEntity.setStatus(ExternalCommunicationEnum.RECOVERED);
                        milesExternalEntity.setSentAt(new Date());
                        milesExternalRepository.save(milesExternalEntity);
                    }else{
                        milesExternalEntity.setLastRetryDate(new Date());
                        milesExternalRepository.save(milesExternalEntity);
                    }
                }

                //Nel caso in cui Miles è andato a buon fine o c'è solo la chiamata a WebSin

                if(claimsNewEntity!= null && webSinExternalEntityList.size()>0
                        && (resultMilesCall || milesExternalEntityList.size()==0)){
                    WebSinExternalEntity webSinExternalEntity = webSinExternalEntityList.get(0);
                    resultWebSinCall = retryCallWebSin(webSinExternalEntity);
                    if(resultWebSinCall){
                        webSinExternalEntity.setStatus(ExternalCommunicationEnum.RECOVERED);
                        webSinExternalEntity.setSentAt(new Date());
                        webSinExternalRepository.save(webSinExternalEntity);
                    }else{
                        webSinExternalEntity.setLastRetryDate(new Date());
                        webSinExternalRepository.save(webSinExternalEntity);
                    }
                }

                //Nel caso in cui Miles è andato a buon fine e ci sono file da reinviare a websin
                if(claimsNewEntity!= null && webSinFileExternalEntityList.size()>0
                        && (resultMilesCall || milesExternalEntityList.size()==0)){
                    for(WebSinFileExternalEntity webSinFileExternalEntity : webSinFileExternalEntityList){
                        boolean resultSendFile = retryCallWebSinFiles(webSinFileExternalEntity.getRequest());
                        if(resultSendFile){
                            webSinFileExternalEntity.setStatus(ExternalCommunicationEnum.RECOVERED);
                            webSinFileExternalEntity.setSentAt(new Date());
                            UploadFileRequest file = new UploadFileRequest();
                            webSinFileExternalEntity.setRequest(file);
                            webSinFileExternalRepository.save(webSinFileExternalEntity);
                        }else{
                            webSinFileExternalEntity.setLastRetryDate(new Date());
                            webSinFileExternalRepository.save(webSinFileExternalEntity);
                        }
                    }
                }


                lockService.unlockPractice(userIdBatch,id);

            }catch (UnsupportedOperationException e){
                try {
                    lockService.unlockPractice(userIdBatch,id);
                }catch (BadRequestException ex){
                }
                LOGGER.error(e.getMessage());
            }
        }
    }

    public Boolean retryCallMiles(MilesExternalEntity milesExternalEntity, String contractId){
        try{
            middlewareClient.postUpdateContractIncidents(contractId, milesExternalEntity.getRequest());
            return true;
        }catch (AbstractException e){
            LOGGER.error("[MILES ERROR] " + e.getDescription());
            milesExternalEntity.setErrorMessage(StringUtils.substring(e.getMessage(),0, 250));
            return false;
        }
    }

    public Boolean retryCallWebSin(WebSinExternalEntity webSinExternalEntity){
        try {
            webSinServiceManager.putComplaint(webSinExternalEntity.getRequest());
            return true;
        }catch (WebSinClaimCommunicationException e){
            LOGGER.error("[WEBSIN ERROR] "+ e.getMessage());
            webSinExternalEntity.setErrorMessage(e.getMessage());
            return false;
        }
    }

    public Boolean retryCallWebSinFiles(UploadFileRequest uploadFileRequest){
        try {
            webSinServiceManager.uploadFile(uploadFileRequest);
            return true;
        }catch (WebSinFileCommunicationException e){
            LOGGER.error("[WEBSIN ERROR] "+ e.getMessage());
            return false;
        }
    }
}
