package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.controller.payload.response.PaginationStats;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.enumerated.ClaimsRepairEnum;
import com.doing.nemo.claims.entity.enumerated.RecipientEnum;
import com.doing.nemo.claims.entity.settings.RecipientTypeEntity;
import com.doing.nemo.claims.repository.RecipientTypeRepository;
import com.doing.nemo.claims.repository.RecipientTypeRepositoryV1;
import com.doing.nemo.claims.service.RecipientTypeService;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class RecipientTypeServiceImpl implements RecipientTypeService {

    private static Logger LOGGER = LoggerFactory.getLogger(RecipientTypeServiceImpl.class);
    @Autowired
    private RecipientTypeRepository recipientTypeRepository;
    @Autowired
    private RecipientTypeRepositoryV1 recipientTypeRepositoryV1;

    @Override
    public RecipientTypeEntity insertRecipientType(RecipientTypeEntity recipientTypeEntity) {

        recipientTypeRepository.save(recipientTypeEntity);
        return recipientTypeEntity;
    }

    @Override
    public RecipientTypeEntity updateRecipientType(RecipientTypeEntity recipientTypeEntity, UUID uuid) {
        Optional<RecipientTypeEntity> recipientTypeEntityOld = recipientTypeRepository.findById(uuid);
        if (!recipientTypeEntityOld.isPresent()) {
            LOGGER.debug("RecipientType with id " + uuid + " not found");
            throw new NotFoundException("RecipientType with id " + uuid + " not found", MessageCode.CLAIMS_1010);
        }

        recipientTypeEntity.setId(recipientTypeEntityOld.get().getId());
        recipientTypeEntity.setTypeComplaint(recipientTypeEntityOld.get().getTypeComplaint());
        recipientTypeRepository.save(recipientTypeEntity);
        return recipientTypeEntity;
    }

    @Override
    public RecipientTypeEntity getRecipientType(UUID uuid) {
        Optional<RecipientTypeEntity> recipientTypeEntity = recipientTypeRepository.findById(uuid);
        if (!recipientTypeEntity.isPresent()) {
            LOGGER.debug("RecipientType with id " + uuid + " not found");
            throw new NotFoundException("RecipientType with id " + uuid + " not found", MessageCode.CLAIMS_1010);
        }

        return recipientTypeEntity.get();
    }

    @Override
    public List<RecipientTypeEntity> getAllRecipientType(ClaimsRepairEnum claimsRepairEnum) {
        List<RecipientTypeEntity> recipientTypeEntities = recipientTypeRepository.searchRecipientTypeByTypeComplaint(claimsRepairEnum);


        return recipientTypeEntities;
    }

    @Override
    public RecipientTypeEntity deleteRecipientType(UUID uuid) {
        Optional<RecipientTypeEntity> recipientTypeEntity = recipientTypeRepository.findById(uuid);
        if (!recipientTypeEntity.isPresent()) {
            LOGGER.debug("RecipientType with id " + uuid + " not found");
            throw new NotFoundException("RecipientType with id " + uuid + " not found", MessageCode.CLAIMS_1010);
        }

        recipientTypeRepository.delete(recipientTypeEntity.get());

        return null;
    }

    @Override
    public RecipientTypeEntity patchStatusRecipientType(UUID uuid) {
        RecipientTypeEntity recipientType = getRecipientType(uuid);
        if (recipientType.getActive()) {
            recipientType.setActive(false);
        } else {
            recipientType.setActive(true);
        }

        recipientTypeRepository.save(recipientType);
        return recipientType;
    }

    @Override
    public Pagination<RecipientTypeEntity> paginationRecipientType(int page, int pageSize, String claimsRepairEnum, String description, Boolean fixed, String email, String dbFieldEmail, Boolean isActive, String recipientType, String orderBy, Boolean asc) {
        Pagination<RecipientTypeEntity> recipientTypePagination = new Pagination<>();

        RecipientEnum recipientEnum = null;
        if (recipientType != null)
            recipientEnum = RecipientEnum.create(recipientType);

        ClaimsRepairEnum claimsRepairEnum1 = null;
        if (claimsRepairEnum != null)
            claimsRepairEnum1 = ClaimsRepairEnum.create(claimsRepairEnum);

        List<RecipientTypeEntity> recipientTypeList = recipientTypeRepositoryV1.findPaginationRecipientType(page, pageSize, claimsRepairEnum1, description, fixed, email, dbFieldEmail, isActive, recipientEnum, orderBy, asc);
        Long itemCount = recipientTypeRepositoryV1.countPaginationRecipientType(claimsRepairEnum1, description, fixed, email, dbFieldEmail, isActive, recipientEnum);
        recipientTypePagination.setItems(recipientTypeList);
        recipientTypePagination.setStats(new PaginationStats(itemCount.intValue(), page, pageSize));

        return recipientTypePagination;
    }

}
