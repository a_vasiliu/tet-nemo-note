package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.SendMailRequestV1;
import com.doing.nemo.claims.controller.payload.request.UploadFileRequestV1;
import com.doing.nemo.claims.controller.payload.request.messaging.Identity;
import com.doing.nemo.claims.controller.payload.response.PaginationStats;
import com.doing.nemo.claims.controller.payload.response.UploadFileResponseV1;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.enumerated.BodyType;
import com.doing.nemo.claims.entity.enumerated.PolicyEnum.PolicyTypeEnum;
import com.doing.nemo.claims.entity.jsonb.personalData.FleetManagerPersonalData;
import com.doing.nemo.claims.entity.settings.*;
import com.doing.nemo.claims.repository.InsurancePolicyRepository;
import com.doing.nemo.claims.repository.InsurancePolicyRepositoryV1;
import com.doing.nemo.claims.repository.PersonalDataRepository;
import com.doing.nemo.claims.service.*;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.exception.InternalException;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class InsurancePolicyServiceImpl implements InsurancePolicyService {


    @Autowired
    private InsurancePolicyRepository insurancePolicyRepository;
    @Autowired
    private RiskService riskService;
    @Autowired
    private EventService eventService;

    @Autowired
    private EventTypeService eventTypeService;

    @Autowired
    private InsurancePolicyRepositoryV1 insurancePolicyRepositoryV1;
    @Autowired
    private MessagingService messagingService;
    @Autowired
    private PersonalDataRepository personalDataRepository;
    
    @Autowired
    private FileManagerService fileManagerService;

    private static Logger LOGGER = LoggerFactory.getLogger(InsurancePolicyServiceImpl.class);

    @Override
    public InsurancePolicyEntity insertInsurancePolicy(InsurancePolicyEntity insurancePolicyEntity) {
        insurancePolicyRepository.save(insurancePolicyEntity);
        return insurancePolicyEntity;
    }

    @Override
    public InsurancePolicyEntity selectInsurancePolicy(UUID uuid) {
        Optional<InsurancePolicyEntity> insurancePolicyEntity = insurancePolicyRepository.findById(uuid);
        if (!insurancePolicyEntity.isPresent()) {
            LOGGER.debug("Insurance policy with id " + uuid + " not found");
            throw new NotFoundException("Insurance policy with id " + uuid + " not found", MessageCode.CLAIMS_1010);
        }
        return insurancePolicyEntity.get();
    }

    @Override
    public List<InsurancePolicyEntity> selectAllInsurancePolicies() {
        List<InsurancePolicyEntity> insurancePolicyEntityList = insurancePolicyRepository.findAll();
        return insurancePolicyEntityList;
    }

    @Override
    public InsurancePolicyEntity deleteInsurancePolicy(UUID uuid) {
        Optional<InsurancePolicyEntity> insurancePolicyEntity = insurancePolicyRepository.findById(uuid);
        if (!insurancePolicyEntity.isPresent()) {
            LOGGER.debug("Insurance policy with id " + uuid + " not found");
            throw new NotFoundException("Insurance policy with id " + uuid + " not found", MessageCode.CLAIMS_1010);
        }
        insurancePolicyRepository.delete(insurancePolicyEntity.get());
        return null;
    }

    @Override
    public InsurancePolicyEntity updateInsurancePolicy(InsurancePolicyEntity insurancePolicyUpdate, UUID uuid, String userName) {

        Optional<InsurancePolicyEntity> insurancePolicyEntityOptional = insurancePolicyRepository.findById(uuid);

        if (!insurancePolicyEntityOptional.isPresent()) {
            LOGGER.debug("Insurance policy with id " + uuid + " not found");
            throw new NotFoundException("Insurance policy with id " + uuid + " not found", MessageCode.CLAIMS_1010);
        }

        InsurancePolicyEntity insurancePolicyEntity = insurancePolicyEntityOptional.get();

        //eventService.deleteEventEntityList(insurancePolicyEntity.getEventEntityList());
        riskService.deleteRiskEntityList(insurancePolicyEntity.getRiskEntityList());

        //List<EventEntity> eventEntityList = eventService.updateEventEntityList(insurancePolicyUpdate.getEventEntityList());
        List<RiskEntity> riskEntityList = riskService.updateRiskEntityList(insurancePolicyUpdate.getRiskEntityList());

        insurancePolicyEntity = addHistorical(insurancePolicyEntity,insurancePolicyUpdate, userName);

        insurancePolicyEntity.setRiskEntityList(riskEntityList);
        insurancePolicyEntity.setInsuranceCompanyEntity(insurancePolicyUpdate.getInsuranceCompanyEntity());
        insurancePolicyEntity.setInsuranceManagerEntity(insurancePolicyUpdate.getInsuranceManagerEntity());
        insurancePolicyEntity.setActive(insurancePolicyUpdate.getActive());
        insurancePolicyEntity.setType(insurancePolicyUpdate.getType());
        insurancePolicyEntity.setDescription(insurancePolicyUpdate.getDescription());
        insurancePolicyEntity.setClient(insurancePolicyUpdate.getClient());
        insurancePolicyEntity.setClientCode(insurancePolicyUpdate.getClientCode());
        insurancePolicyEntity.setAnnotations(insurancePolicyUpdate.getAnnotations());
        insurancePolicyEntity.setBeginningValidity(insurancePolicyUpdate.getBeginningValidity());
        insurancePolicyEntity.setEndValidity(insurancePolicyUpdate.getEndValidity());
        insurancePolicyEntity.setBookRegister(insurancePolicyUpdate.getBookRegister());
        insurancePolicyEntity.setBookRegisterCode(insurancePolicyUpdate.getBookRegisterCode());
        insurancePolicyEntity.setLastRenewalNotification(insurancePolicyUpdate.getLastRenewalNotification());
        insurancePolicyEntity.setRenewalNotification(insurancePolicyUpdate.getRenewalNotification());
        insurancePolicyEntity.setFranchise(insurancePolicyUpdate.getFranchise());
        insurancePolicyEntity.setNumberPolicy(insurancePolicyUpdate.getNumberPolicy());

        insurancePolicyRepository.save(insurancePolicyEntity);

        return insurancePolicyEntity;

    }

    @Override
    public InsurancePolicyEntity insertRiskInsurancePolicy(List<RiskEntity> riskEntities, UUID uuid) {
        Optional<InsurancePolicyEntity> insurancePolicyEntityOptional = insurancePolicyRepository.findById(uuid);
        if (!insurancePolicyEntityOptional.isPresent()) {
            LOGGER.debug("Insurance policy with id " + uuid + " not found");
            throw new NotFoundException("Insurance policy with id " + uuid + " not found", MessageCode.CLAIMS_1010);
        }
        InsurancePolicyEntity insurancePolicyEntity = insurancePolicyEntityOptional.get();
        List<RiskEntity> riskEntityList = riskService.updateRiskEntityList(riskEntities);
        insurancePolicyEntity.getRiskEntityList().addAll(riskEntityList);

        return insurancePolicyEntity;
    }

    @Override
    public InsurancePolicyEntity insertEventInsurancePolicy(List<EventEntity> eventEntities, UUID uuid) {
        Optional<InsurancePolicyEntity> insurancePolicyEntityOptional = insurancePolicyRepository.findById(uuid);
        if (!insurancePolicyEntityOptional.isPresent()) {
            LOGGER.debug("Insurance policy with id " + uuid + " not found");
            throw new NotFoundException("Insurance policy with id " + uuid + " not found", MessageCode.CLAIMS_1010);
        }
        InsurancePolicyEntity insurancePolicyEntity = insurancePolicyEntityOptional.get();
        List<EventEntity> eventEntityList = eventService.updateEventEntityList(eventEntities);
        insurancePolicyEntity.getEventEntityList().addAll(eventEntityList);

        return insurancePolicyEntity;
    }

    @Override
    public List<UploadFileResponseV1> insertAttachmentInsurancePolicy(List<UploadFileRequestV1> attachmentEntities, UUID uuid, String userId, String userName) {


        try {

            Optional<InsurancePolicyEntity> insurancePolicyEntityOptional = insurancePolicyRepository.findById(uuid);
            if (!insurancePolicyEntityOptional.isPresent()) {
                LOGGER.debug("Insurance policy with id " + uuid + " not found");
                throw new NotFoundException("Insurance policy with id " + uuid + " not found", MessageCode.CLAIMS_1010);
            }

            List<UploadFileResponseV1> responseList = fileManagerService.uploadFileInsurancePolicy(attachmentEntities, uuid.toString(), userId, userName);

            return responseList;

        } catch (IOException e) {
            throw new InternalException(MessageCode.CLAIMS_1003, e);
        }

    }

    @Override
    public InsurancePolicyEntity updateStatusInsurancePolicy(UUID uuid) {
        InsurancePolicyEntity insurancePolicyEntity = selectInsurancePolicy(uuid);
        if (insurancePolicyEntity.getActive()) {
            insurancePolicyEntity.setActive(false);
        } else {
            insurancePolicyEntity.setActive(true);
        }

        insurancePolicyRepository.save(insurancePolicyEntity);
        return insurancePolicyEntity;
    }

    @Override
    public Pagination<InsurancePolicyEntity> paginationInsurancePolicy(int page, int pageSize, String orderBy, Boolean asc, PolicyTypeEnum type, Long clientCode, String client, String numberPolicy, String beginningValidity, String endValidity, String bookRegisterCode, Boolean isActive) {
        Pagination<InsurancePolicyEntity> insurancePolicyPagination = new Pagination<>();
        List<InsurancePolicyEntity> insurancePolicyEntityList = insurancePolicyRepositoryV1.findPaginationInsurancePolicy(page, pageSize, orderBy, asc, type, clientCode, client, numberPolicy, beginningValidity, endValidity, bookRegisterCode, isActive);
        Long itemCount = insurancePolicyRepositoryV1.countPaginationInsurancePolicy(type, clientCode, client, numberPolicy, beginningValidity, endValidity, bookRegisterCode, isActive);
        insurancePolicyPagination.setItems(insurancePolicyEntityList);
        insurancePolicyPagination.setStats(new PaginationStats(itemCount.intValue(), page, pageSize));

        return insurancePolicyPagination;
    }

    @Override
    public List<InsurancePolicyEntity> paginationInsurancePolicyGetAll(String dateValidity, Boolean isActive) {
        List<InsurancePolicyEntity> insurancePolicyEntityList = insurancePolicyRepositoryV1.findInsurancePolicy(dateValidity, isActive);
        return insurancePolicyEntityList;
    }

    @Override
    public void renewalNotificationEmail(String userId) {
        List<InsurancePolicyEntity> insurancePolicyEntities = insurancePolicyRepository.searchInsurancePolicyByRenewalNotification(DateUtil.getNowInstant());
        if (insurancePolicyEntities != null && !insurancePolicyEntities.isEmpty()) {
            for (InsurancePolicyEntity insurancePolicyEntity : insurancePolicyEntities) {
                if (insurancePolicyEntity.getClientCode() != null) {
                    PersonalDataEntity personalDataEntity = personalDataRepository.findByCustomerId(insurancePolicyEntity.getClientCode().toString());
                    if (personalDataEntity != null) {
                        SendMailRequestV1 sendMailRequestV1 = new SendMailRequestV1();
                        sendMailRequestV1.setTos(new LinkedList<>());
                        for (FleetManagerPersonalData fleetManagerPersonalData : personalDataEntity.getFleetManagerPersonalData()) {
                            Identity identity = new Identity();
                            identity.setName(fleetManagerPersonalData.getFleetManagerName());
                            identity.setRecipientType("Fleet Manager");
                            if (fleetManagerPersonalData.getFleetManagerSecondaryEmail() != null && !fleetManagerPersonalData.getFleetManagerSecondaryEmail().isEmpty()) {
                                identity.setEmail(fleetManagerPersonalData.getFleetManagerSecondaryEmail());
                                List<Identity> identities = sendMailRequestV1.getTos();
                                identities.add(identity);
                                sendMailRequestV1.setTos(identities);
                            } else if (fleetManagerPersonalData.getFleetManagerPrimaryEmail() != null && !fleetManagerPersonalData.getFleetManagerPrimaryEmail().isEmpty()) {
                                identity.setEmail(fleetManagerPersonalData.getFleetManagerPrimaryEmail());
                                List<Identity> identities = sendMailRequestV1.getTos();
                                identities.add(identity);
                                sendMailRequestV1.setTos(identities);
                            }
                        }
                        if (personalDataEntity.getFleetManagerPersonalData() != null && !personalDataEntity.getFleetManagerPersonalData().isEmpty()) {
                            try {
                                sendMailRequestV1.setSubject("Rinnovo contratto");
                                sendMailRequestV1.setBodyType(BodyType.HTML);
                                sendMailRequestV1.setBodyContent("Client code Nr." + insurancePolicyEntity.getClientCode());
                                HttpStatus httpStatus = messagingService.sendMail(sendMailRequestV1);
                                if (httpStatus == HttpStatus.ACCEPTED) {
                                    insurancePolicyEntity.setRenewalNotification(false);
                                    if (insurancePolicyEntity.getEventEntityList() == null)
                                        insurancePolicyEntity.setEventEntityList(new LinkedList<>());
                                    EventEntity eventEntity = new EventEntity();
                                    eventEntity.setCreatedAt(DateUtil.getNowInstant());
                                    String description = new String();
                                    description += "<br/>INVIATO IL: " + DateUtil.getNowDate().toString() + "<br/>FROM: no-reply@ald.automotivedn.com/nTO: ";
                                    for (Identity recipients : sendMailRequestV1.getTos()) {
                                        description += recipients.getEmail() + ", ";
                                    }
                                    description += "<br/>CC:<br/>CCN:<br/>OGGETTO: ";
                                    if (sendMailRequestV1.getSubject() != null) {
                                        description += sendMailRequestV1.getSubject();
                                    }
                                    if (sendMailRequestV1.getBodyContent() != null) {
                                        description += "<br/>" + sendMailRequestV1.getBodyContent();
                                    }
                                    eventEntity.setDescription(description);
                                    eventEntity.setUser(userId);
                                    insurancePolicyEntity.getEventEntityList().add(eventEntity);
                                    insurancePolicyEntity.setLastRenewalNotification(DateUtil.getNowInstant());
                                    insurancePolicyEntity.setRenewalNotification(false);
                                    insurancePolicyRepository.save(insurancePolicyEntity);
                                }
                            } catch (IOException e) {
                                LOGGER.debug("Impossible to send e-mail");
                                throw new BadRequestException(MessageCode.CLAIMS_1044, e);
                            }
                        }
                    }
                }
            }

        }

    }

    private InsurancePolicyEntity addHistorical(InsurancePolicyEntity insurancePolicyEntityOld, InsurancePolicyEntity insurancePolicyEntityNew, String userName) {
        String description = new String();

        if(insurancePolicyEntityOld.getType() !=null && !insurancePolicyEntityOld.getType().equals(insurancePolicyEntityNew.getType())){
            if(insurancePolicyEntityNew.getType() != null)
                description += "Campo: Tipo. " + "Valore precedente: "+insurancePolicyEntityOld.getType() + "." + " Valore attuale: "+insurancePolicyEntityNew.getType()+". <br>";
            else
                description += "Campo: Tipo. " + "Valore precedente: "+insurancePolicyEntityOld.getType() + "." + " Valore attuale: Nessuno. <br>";
        } else if (insurancePolicyEntityOld.getType() == null && insurancePolicyEntityNew.getType()!=null) {
            description += "Campo: Tipo. " + "Valore precedente: Nessuno." + " Valore attuale: "+insurancePolicyEntityNew.getType()+". <br>";
        }


        if(insurancePolicyEntityOld.getDescription() !=null && !insurancePolicyEntityOld.getDescription().equals(insurancePolicyEntityNew.getDescription())){
            if(insurancePolicyEntityNew.getDescription() != null)
                description += "Campo: Descrizione. " + "Valore precedente: "+insurancePolicyEntityOld.getDescription() + "." + " Valore attuale: "+insurancePolicyEntityNew.getDescription()+". <br>";
            else
                description += "Campo: Descrizione. " + "Valore precedente: "+insurancePolicyEntityOld.getDescription() + "." + " Valore attuale: Nessuno. <br>";
        } else if (insurancePolicyEntityOld.getDescription() == null && insurancePolicyEntityNew.getDescription()!=null) {
            description += "Campo: Descrizione. " + "Valore precedente: Nessuno." + " Valore attuale: "+insurancePolicyEntityNew.getDescription()+". <br>";
        }

        if(insurancePolicyEntityOld.getNumberPolicy() !=null && !insurancePolicyEntityOld.getNumberPolicy().equals(insurancePolicyEntityNew.getNumberPolicy())){
            if(insurancePolicyEntityNew.getNumberPolicy() != null)
                description += "Campo: Numero Polizza. " + "Valore precedente: "+insurancePolicyEntityOld.getNumberPolicy() + "." + " Valore attuale: "+insurancePolicyEntityNew.getNumberPolicy()+". <br>";
            else
                description += "Campo: Numero Polizza. " + "Valore precedente: "+insurancePolicyEntityOld.getNumberPolicy() + "." + " Valore attuale: Nessuno. <br>";
        } else if (insurancePolicyEntityOld.getNumberPolicy() == null && insurancePolicyEntityNew.getNumberPolicy()!=null) {
            description += "Campo: Numero Polizza. " + "Valore precedente: Nessuno." + " Valore attuale: "+insurancePolicyEntityNew.getNumberPolicy()+". <br>";
        }

        if(insurancePolicyEntityOld.getInsuranceCompanyEntity() !=null && !insurancePolicyEntityOld.getInsuranceCompanyEntity().equals(insurancePolicyEntityNew.getInsuranceCompanyEntity())){
            if(insurancePolicyEntityNew.getInsuranceCompanyEntity() != null)
                description += "Campo: Compangnia assicuratrice. " + "Valore precedente: "+insurancePolicyEntityOld.getInsuranceCompanyEntity().getName() + "." + " Valore attuale: "+insurancePolicyEntityNew.getInsuranceCompanyEntity().getName()+". <br>";
            else
                description += "Campo: Compangnia assicuratrice. " + "Valore precedente: "+insurancePolicyEntityOld.getInsuranceCompanyEntity().getName() + "." + " Valore attuale: Nessuno. <br>";
        } else if (insurancePolicyEntityOld.getInsuranceCompanyEntity() == null && insurancePolicyEntityNew.getInsuranceCompanyEntity()!=null) {
            description += "Campo: Compangnia assicuratrice. " + "Valore precedente: Nessuno." + " Valore attuale: "+insurancePolicyEntityNew.getInsuranceCompanyEntity().getName()+". <br>";
        }

        if(insurancePolicyEntityOld.getInsuranceManagerEntity() !=null && !insurancePolicyEntityOld.getInsuranceManagerEntity().equals(insurancePolicyEntityNew.getInsuranceManagerEntity())){
            if(insurancePolicyEntityNew.getInsuranceManagerEntity() != null)
                description += "Campo: Manager. " + "Valore precedente: "+insurancePolicyEntityOld.getInsuranceManagerEntity().getName() + "." + " Valore attuale: "+insurancePolicyEntityNew.getInsuranceManagerEntity().getName()+". <br>";
            else
                description += "Campo: Manager. " + "Valore precedente: "+insurancePolicyEntityOld.getInsuranceManagerEntity().getName() + "." + " Valore attuale: Nessuno. <br>";
        } else if (insurancePolicyEntityOld.getInsuranceManagerEntity() == null && insurancePolicyEntityNew.getInsuranceManagerEntity()!=null) {
            description += "Campo: Manager. " + "Valore precedente: Nessuno." + " Valore attuale: "+insurancePolicyEntityNew.getInsuranceManagerEntity().getName()+". <br>";
        }

        if(insurancePolicyEntityOld.getClientCode() !=null && !insurancePolicyEntityOld.getClientCode().equals(insurancePolicyEntityNew.getClientCode())){
            if(insurancePolicyEntityNew.getClientCode() != null)
                description += "Campo: Codice cliente. " + "Valore precedente: "+insurancePolicyEntityOld.getClientCode() + "." + " Valore attuale: "+insurancePolicyEntityNew.getClientCode()+". <br>";
            else
                description += "Campo: Codice cliente. " + "Valore precedente: "+insurancePolicyEntityOld.getClientCode() + "." + " Valore attuale: Nessuno. <br>";
        } else if (insurancePolicyEntityOld.getClientCode() == null && insurancePolicyEntityNew.getClientCode()!=null) {
            description += "Campo: Codice cliente. " + "Valore precedente: Nessuno." + " Valore attuale: "+insurancePolicyEntityNew.getClientCode()+". <br>";
        }

        if(insurancePolicyEntityOld.getAnnotations() !=null && !insurancePolicyEntityOld.getAnnotations().equals(insurancePolicyEntityNew.getAnnotations())){
            if(insurancePolicyEntityNew.getAnnotations() != null)
                description += "Campo: Annotazioni. " + "Valore precedente: "+insurancePolicyEntityOld.getAnnotations() + "." + " Valore attuale: "+insurancePolicyEntityNew.getAnnotations()+". <br>";
            else
                description += "Campo: Annotazioni. " + "Valore precedente: "+insurancePolicyEntityOld.getAnnotations() + "." + " Valore attuale: Nessuno. <br>";
        } else if (insurancePolicyEntityOld.getAnnotations() == null && insurancePolicyEntityNew.getAnnotations()!=null) {
            description += "Campo: Annotazioni. " + "Valore precedente: Nessuno." + " Valore attuale: "+insurancePolicyEntityNew.getAnnotations()+". <br>";
        }

        if(insurancePolicyEntityOld.getBeginningValidity() !=null && !insurancePolicyEntityOld.getBeginningValidity().equals(insurancePolicyEntityNew.getBeginningValidity())){
            if(insurancePolicyEntityNew.getBeginningValidity() != null)
                description += "Campo: Inizio validità. " + "Valore precedente: "+insurancePolicyEntityOld.getBeginningValidity() + "." + " Valore attuale: "+insurancePolicyEntityNew.getBeginningValidity()+". <br>";
            else
                description += "Campo: Inizio validità. " + "Valore precedente: "+insurancePolicyEntityOld.getBeginningValidity() + "." + " Valore attuale: Nessuno. <br>";
        } else if (insurancePolicyEntityOld.getBeginningValidity() == null && insurancePolicyEntityNew.getBeginningValidity()!=null) {
            description += "Campo: Inizio validità. " + "Valore precedente: Nessuno." + " Valore attuale: "+insurancePolicyEntityNew.getBeginningValidity()+". <br>";
        }

        if(insurancePolicyEntityOld.getEndValidity() !=null && !insurancePolicyEntityOld.getEndValidity().equals(insurancePolicyEntityNew.getEndValidity())){
            if(insurancePolicyEntityNew.getEndValidity() != null)
                description += "Campo: Fine validità. " + "Valore precedente: "+insurancePolicyEntityOld.getEndValidity() + "." + " Valore attuale: "+insurancePolicyEntityNew.getEndValidity()+". <br>";
            else
                description += "Campo: Fine validità. " + "Valore precedente: "+insurancePolicyEntityOld.getEndValidity() + "." + " Valore attuale: Nessuno. <br>";
        } else if (insurancePolicyEntityOld.getEndValidity() == null && insurancePolicyEntityNew.getEndValidity()!=null) {
            description += "Campo: Fine validità. " + "Valore precedente: Nessuno." + " Valore attuale: "+insurancePolicyEntityNew.getEndValidity()+". <br>";
        }

        if(insurancePolicyEntityOld.getBookRegister() !=null && !insurancePolicyEntityOld.getBookRegister().equals(insurancePolicyEntityNew.getBookRegister())){
            if(insurancePolicyEntityNew.getBookRegister() != null)
                description += "Campo: Book register. " + "Valore precedente: "+insurancePolicyEntityOld.getBookRegister() + "." + " Valore attuale: "+insurancePolicyEntityNew.getBookRegister()+". <br>";
            else
                description += "Campo: Book register. " + "Valore precedente: "+insurancePolicyEntityOld.getBookRegister() + "." + " Valore attuale: Nessuno. <br>";
        } else if (insurancePolicyEntityOld.getBookRegister() == null && insurancePolicyEntityNew.getBookRegister()!=null) {
            description += "Campo: Book register. " + "Valore precedente: Nessuno." + " Valore attuale: "+insurancePolicyEntityNew.getBookRegister()+". <br>";
        }

        if(insurancePolicyEntityOld.getBookRegisterCode() !=null && !insurancePolicyEntityOld.getBookRegisterCode().equals(insurancePolicyEntityNew.getBookRegisterCode())){
            if(insurancePolicyEntityNew.getBookRegisterCode() != null)
                description += "Campo: Book register code. " + "Valore precedente: "+insurancePolicyEntityOld.getBookRegisterCode() + "." + " Valore attuale: "+insurancePolicyEntityNew.getBookRegisterCode()+". <br>";
            else
                description += "Campo: Book register code. " + "Valore precedente: "+insurancePolicyEntityOld.getBookRegisterCode() + "." + " Valore attuale: Nessuno. <br>";
        } else if (insurancePolicyEntityOld.getBookRegisterCode() == null && insurancePolicyEntityNew.getBookRegisterCode()!=null) {
            description += "Campo: Book register code. " + "Valore precedente: Nessuno." + " Valore attuale: "+insurancePolicyEntityNew.getBookRegisterCode()+". <br>";
        }

        if(insurancePolicyEntityOld.getLastRenewalNotification() !=null && !insurancePolicyEntityOld.getLastRenewalNotification().equals(insurancePolicyEntityNew.getLastRenewalNotification())){
            if(insurancePolicyEntityNew.getLastRenewalNotification() != null)
                description += "Campo: Ultima notifica rinnovo. " + "Valore precedente: "+insurancePolicyEntityOld.getLastRenewalNotification() + "." + " Valore attuale: "+insurancePolicyEntityNew.getLastRenewalNotification()+". <br>";
            else
                description += "Campo: Ultima notifica rinnovo. " + "Valore precedente: "+insurancePolicyEntityOld.getLastRenewalNotification() + "." + " Valore attuale: Nessuno. <br>";
        } else if (insurancePolicyEntityOld.getLastRenewalNotification() == null && insurancePolicyEntityNew.getLastRenewalNotification()!=null) {
            description += "Campo: Ultima notifica rinnovo. " + "Valore precedente: Nessuno." + " Valore attuale: "+insurancePolicyEntityNew.getLastRenewalNotification()+". <br>";
        }

        if(insurancePolicyEntityOld.getRenewalNotification() !=null && !insurancePolicyEntityOld.getRenewalNotification().equals(insurancePolicyEntityNew.getRenewalNotification())){
            if(insurancePolicyEntityNew.getRenewalNotification() != null)
                description += "Campo: Notifica rinnovo. " + "Valore precedente: "+insurancePolicyEntityOld.getRenewalNotification() + "." + " Valore attuale: "+insurancePolicyEntityNew.getRenewalNotification()+". <br>";
            else
                description += "Campo: Notifica rinnovo. " + "Valore precedente: "+insurancePolicyEntityOld.getRenewalNotification() + "." + " Valore attuale: Nessuno. <br>";
        } else if (insurancePolicyEntityOld.getRenewalNotification() == null && insurancePolicyEntityNew.getRenewalNotification()!=null) {
            description += "Campo: Notifica rinnovo. " + "Valore precedente: Nessuno." + " Valore attuale: "+insurancePolicyEntityNew.getRenewalNotification()+". <br>";
        }

        if(insurancePolicyEntityOld.getActive() !=null && !insurancePolicyEntityOld.getActive().equals(insurancePolicyEntityNew.getActive())){
            if(insurancePolicyEntityNew.getActive() != null)
                description += "Campo: Attivo. " + "Valore precedente: "+insurancePolicyEntityOld.getActive() + "." + " Valore attuale: "+insurancePolicyEntityNew.getActive()+". <br>";
            else
                description += "Campo: Attivo. " + "Valore precedente: "+insurancePolicyEntityOld.getActive() + "." + " Valore attuale: Nessuno. <br>";
        } else if (insurancePolicyEntityOld.getActive() == null && insurancePolicyEntityNew.getActive()!=null) {
            description += "Campo: Attivo. " + "Valore precedente: Nessuno." + " Valore attuale: "+insurancePolicyEntityNew.getActive()+". <br>";
        }

        if(insurancePolicyEntityOld.getFranchise() !=null){
            if(insurancePolicyEntityNew.getFranchise() != null)
            {
                BigDecimal franchiseOld = BigDecimal.valueOf(insurancePolicyEntityOld.getFranchise());
                BigDecimal franchiseNew = BigDecimal.valueOf(insurancePolicyEntityNew.getFranchise());
                if(!franchiseOld.equals(franchiseNew))
                {
                    description += "Campo: Franchigia. " + "Valore precedente: "+insurancePolicyEntityOld.getFranchise() + "." + " Valore attuale: "+insurancePolicyEntityNew.getFranchise()+". <br>";
                }
            }
            else
                description += "Campo: Franchigia. " + "Valore precedente: "+insurancePolicyEntityOld.getFranchise() + "." + " Valore attuale: Nessuno. <br>";
        } else if (insurancePolicyEntityNew.getFranchise()!=null) {
            description += "Campo: Franchigia. " + "Valore precedente: Nessuno." + " Valore attuale: "+insurancePolicyEntityNew.getFranchise()+". <br>";
        }

        if(!description.isEmpty()){
            if (insurancePolicyEntityOld.getEventEntityList() == null)
                insurancePolicyEntityOld.setEventEntityList(new LinkedList<>());
            EventEntity eventEntity = new EventEntity();
            eventEntity.setUser(userName);
            eventEntity.setDescription(description);
            eventEntity.setCreatedAt(DateUtil.getNowInstant());
            EventTypeEntity eventTypeEntity = eventTypeService.getEventType(UUID.fromString("5e4fd707-6763-432f-9ad8-af65cbafbd26"));
            eventEntity.setEventType(eventTypeEntity);
            insurancePolicyEntityOld.getEventEntityList().add(eventEntity);
        }

        return insurancePolicyEntityOld;
    }


}
