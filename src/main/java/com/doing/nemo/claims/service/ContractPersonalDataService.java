package com.doing.nemo.claims.service;

import com.doing.nemo.claims.entity.settings.ContractEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ContractPersonalDataService {
    List<ContractEntity> updateContractList(List<ContractEntity> contractEntityList);

    void deleteContractList(List<ContractEntity> contractEntityList);
}
