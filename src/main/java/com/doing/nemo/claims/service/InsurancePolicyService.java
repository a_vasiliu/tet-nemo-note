package com.doing.nemo.claims.service;

import com.doing.nemo.claims.controller.payload.request.UploadFileRequestV1;
import com.doing.nemo.claims.controller.payload.response.UploadFileResponseV1;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.enumerated.PolicyEnum.PolicyTypeEnum;
import com.doing.nemo.claims.entity.settings.EventEntity;
import com.doing.nemo.claims.entity.settings.InsurancePolicyEntity;
import com.doing.nemo.claims.entity.settings.RiskEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public interface InsurancePolicyService {
    InsurancePolicyEntity insertInsurancePolicy(InsurancePolicyEntity insurancePolicyEntity);

    InsurancePolicyEntity selectInsurancePolicy(UUID uuid);

    List<InsurancePolicyEntity> selectAllInsurancePolicies();

    InsurancePolicyEntity deleteInsurancePolicy(UUID uuid);

    InsurancePolicyEntity updateInsurancePolicy(InsurancePolicyEntity insurancePolicyEntity, UUID uuid, String userName);

    InsurancePolicyEntity insertRiskInsurancePolicy(List<RiskEntity> riskEntities, UUID uuid);

    InsurancePolicyEntity insertEventInsurancePolicy(List<EventEntity> eventEntities, UUID uuid);

    List<UploadFileResponseV1> insertAttachmentInsurancePolicy(List<UploadFileRequestV1> attachmentEntities, UUID uuid, String userId, String userName);

    InsurancePolicyEntity updateStatusInsurancePolicy(UUID uuid);

    Pagination<InsurancePolicyEntity> paginationInsurancePolicy(int page, int pageSize, String orderBy, Boolean asc, PolicyTypeEnum type, Long clientCode, String client, String numberPolicy, String beginningValidity, String endValidity, String bookRegisterCode, Boolean isActive);
    List<InsurancePolicyEntity> paginationInsurancePolicyGetAll(String dateValidity, Boolean isActive);

    void renewalNotificationEmail (String userId);

}
