package com.doing.nemo.claims.service;

import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.settings.ExportNotificationTypeEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public interface ExportNotificationTypeService {
    ExportNotificationTypeEntity saveExportNotificationType(ExportNotificationTypeEntity exportNotificationTypeEntity);
    ExportNotificationTypeEntity updateExportNotificationType(ExportNotificationTypeEntity exportNotificationTypeEntity);
    void deleteExportNotificationType(UUID uuid);
    ExportNotificationTypeEntity getExportNotificationType(UUID uuid);
    List<ExportNotificationTypeEntity> getAllExportNotificationType();
    ExportNotificationTypeEntity patchActive(UUID uuid);
    Pagination<ExportNotificationTypeEntity> findExportNotificationType(Integer page, Integer pageSize, String orderBy, Boolean asc, String descriptionNotification, String code, String description, Boolean isActive);

}
