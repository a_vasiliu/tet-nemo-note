package com.doing.nemo.claims.service.impl;

import com.doing.nemo.claims.controller.payload.response.BrokerResponseV1;
import com.doing.nemo.claims.entity.settings.BrokerEntity;
import com.doing.nemo.claims.repository.BrokerRepository;
import com.doing.nemo.claims.repository.BrokerRepositoryV1;
import com.doing.nemo.claims.service.BrokerService;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class BrokerServiceImpl implements BrokerService {

    private static Logger LOGGER = LoggerFactory.getLogger(BrokerServiceImpl.class);
    @Autowired
    private BrokerRepository brokerRepository;
    @Autowired
    private BrokerRepositoryV1 brokerRepositoryV1;

    @Override
    public BrokerEntity insertBroker(BrokerEntity brokerEntity) {

        if (brokerRepository.searchBrokerbyName(brokerEntity.getBusinessName()) != null) {
            LOGGER.debug("Error in Brokers, duplicate name not admitted");
            throw new BadRequestException("Error in Brokers, duplicate name not admitted", MessageCode.CLAIMS_1009);
        }
        brokerRepository.save(brokerEntity);

        return brokerEntity;
    }

    @Override
    public BrokerEntity updateBroker(BrokerEntity brokerEntity) {
        if (brokerRepository.searchBrokerWithDifferentId(brokerEntity.getId(), brokerEntity.getBusinessName()) != null) {
            LOGGER.debug("Error in Brokers, duplicate name not admitted");
            throw new BadRequestException("Error in Brokers, duplicate name not admitted", MessageCode.CLAIMS_1009);
        }
        brokerRepository.save(brokerEntity);
        return brokerEntity;
    }

    @Override
    public BrokerEntity selectBroker(UUID id) {
        Optional<BrokerEntity> brokerEntity = brokerRepository.findById(id);
        if (!brokerEntity.isPresent()) {
            LOGGER.debug("Broker with id " + id + " not found");
            throw new NotFoundException("Broker with id " + id + " not found", MessageCode.CLAIMS_1010);
        }

        return brokerEntity.get();
    }

    @Override
    public List<BrokerEntity> selectAllBroker() {
        List<BrokerEntity> resultListBroker = brokerRepository.findAll();

        return resultListBroker;
    }

    @Override
    public BrokerResponseV1<BrokerEntity> deleteBroker(UUID id) {
        BrokerEntity brokerEntity = selectBroker(id);
        //if(brokerRepository.searchInsurancePolicyForeignKey(id) != null)
        //  throw new NotFoundException("Foreign key in InsurancePolicy. Broker impossible to delete");
        brokerRepository.delete(brokerEntity);
        return null;
    }

    @Override
    public BrokerEntity updateStatus(UUID uuid) {
        BrokerEntity brokerEntity = selectBroker(uuid);
        if (brokerEntity.getActive()) {
            brokerEntity.setActive(false);
        } else {
            brokerEntity.setActive(true);
        }

        brokerRepository.save(brokerEntity);
        return brokerEntity;
    }

    @Override
    public List<BrokerEntity> findBrokersFiltered(Integer page, Integer pageSize) {
        return brokerRepositoryV1.findBrokersFiltered(page, pageSize);
    }

    @Override
    public BigInteger countBrokerFiltered() {
        return brokerRepositoryV1.countBrokerFiltered();
    }

}