package com.doing.nemo.claims.controller.payload.request;

import com.doing.nemo.claims.controller.payload.request.complaint.FromCompanyRequest;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class ClaimsPatchExternalMSARequest implements Serializable {
    @JsonProperty("from_company")
    private FromCompanyRequest fromCompany;

    public ClaimsPatchExternalMSARequest(FromCompanyRequest fromCompany) {
        this.fromCompany = fromCompany;
    }

    public ClaimsPatchExternalMSARequest() {
    }

    public FromCompanyRequest getFromCompany() {
        return fromCompany;
    }

    public void setFromCompany(FromCompanyRequest fromCompany) {
        this.fromCompany = fromCompany;
    }
}
