package com.doing.nemo.claims.controller.payload.request;

import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsFlowEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.doing.nemo.claims.entity.settings.DmSystemsEntity;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;

import java.io.Serializable;

public class ExportStatusTypeRequestV1 implements Serializable {

    @JsonProperty("status_type")
    private ClaimsStatusEnum statusType;

    @JsonProperty("flow_type")
    private ClaimsFlowEnum flowType;

    @JsonProperty("dm_system_id")
    private DmSystemsEntity dmSystem;

    @JsonProperty("code")
    private String code;

    @JsonProperty("description")
    private String description;

    @JsonProperty("is_active")
    private Boolean isActive = true;

    public ExportStatusTypeRequestV1(ClaimsStatusEnum statusType, ClaimsFlowEnum flowType, DmSystemsEntity dmSystem, String code, String description, Boolean isActive) {
        this.statusType = statusType;
        this.flowType = flowType;
        this.dmSystem = dmSystem;
        this.code = code;
        this.description = description;
        this.isActive = isActive;
    }

    public ExportStatusTypeRequestV1() {
    }

    public ClaimsStatusEnum getStatusType() {
        return statusType;
    }

    public void setStatusType(ClaimsStatusEnum statusType) {
        this.statusType = statusType;
    }

    public ClaimsFlowEnum getFlowType() {
        return flowType;
    }

    public void setFlowType(ClaimsFlowEnum flowType) {
        this.flowType = flowType;
    }

    public DmSystemsEntity getDmSystem() {
        return dmSystem;
    }

    public void setDmSystem(DmSystemsEntity dmSystem) {
        this.dmSystem = dmSystem;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getActive() {
        return isActive;
    }

    @JsonSetter
    public void setActive(Boolean active) {
        if(active != null)
            isActive = active;
    }

    @Override
    public String toString() {
        return "ExportStatusTypeRequestV1{" +
                "statusType=" + statusType +
                ", flowType=" + flowType +
                ", dmSystem=" + dmSystem +
                ", code='" + code + '\'' +
                ", description='" + description + '\'' +
                ", isActive=" + isActive +
                '}';
    }
}
