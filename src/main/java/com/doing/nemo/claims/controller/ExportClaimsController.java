package com.doing.nemo.claims.controller;

import com.doing.nemo.claims.adapter.ClaimsAdapter;
import com.doing.nemo.claims.repository.ClaimsRepositoryV1;
import com.doing.nemo.claims.service.ExportClaimsService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/v1")
@Api("Export Claims and Log")
public class ExportClaimsController {

    @Autowired
    private ExportClaimsService exportClaimsService;

    @Autowired
    private ClaimsAdapter claimsAdapter;

    @Autowired
    private ClaimsRepositoryV1 claimsRepositoryV1;

    /*@GetMapping("/export/log")
    @Transactional
    @ApiOperation(value = "Export logs ", notes = "")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity exportLog(
            @ApiParam(value = "Filter the search by the date the Claim was created from", example = "2018-01-01", required = false)
            @RequestParam(value = "date_accident_from", required = false) String dateAccidentFrom,
            @ApiParam(value = "Filter the search by the date the Claim was created to", example = "2018-01-01", required = false)
            @RequestParam(value = "date_accident_to", required = false) String dateAccidentTo,
            @ApiParam(value = "Filter the search by the type of accident", required = false)
            @RequestParam(value = "type_accident", required = false) List<String> typeAccident,
            @ApiParam(value = "Filter the search for the flow to which the Claim belongs", required = false)
            @RequestParam(value = "flow", required = false) List<String> flow

    ){

        List<ClaimsFlowEnum> flows = null;
        if(flow != null)
            flows = ClaimsFlowEnum.getFlowListFromString(flow);

        List<DataAccidentTypeAccidentEnum> typeAccidentList = null;
        if(typeAccident != null)
            typeAccidentList = DataAccidentTypeAccidentEnum.getTypeAccidentListFromString(typeAccident);


        List<ClaimsEntity> claimsEntityList = exportClaimsService.exportLogs(dateAccidentFrom, dateAccidentTo, typeAccidentList, flows);
        return new ResponseEntity<List<ExportLogResponse>>(ClaimsAdapter.adptClaimsToExportLogResponse(claimsEntityList),HttpStatus.CREATED);
    }*/

    /*
    aggiungere:

       - data creazione da - a
       - data affidamento da - a
       - denuncia id da - a
       - Lista codici cliente
       - affidata a tipologia( ispettorato legale insurance manager) + uuid della tipologia
       - recuperabilità booleano
       - controparti booleano



    */

    public ClaimsAdapter getClaimsAdapter() {
        return claimsAdapter;
    }








}
