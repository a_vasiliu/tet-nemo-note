package com.doing.nemo.claims.controller.payload.request;

import com.doing.nemo.claims.entity.enumerated.DamagedEnum.VehicleEnum.VehicleTypeEnum;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AutomaticAffiliationRuleLegalRequestV1 implements Serializable {

    @JsonProperty("selection_name")
    private String selectionName;

    @JsonProperty("monthly_volume")
    private Integer monthlyVolume;

    @JsonProperty("current_volume")
    private Integer currentVolume;

    @JsonProperty("detail")
    private Integer detail;

    @JsonProperty("annotations")
    private String annotations = "";

    @JsonProperty("complaints_without_ctp")
    private Boolean complaintsWithoutCtp;

    @JsonProperty("foreign_country_claim")
    private Boolean foreignCountryClaim;

    @JsonProperty("counterpart_type")
    private VehicleTypeEnum counterpartType;

    @JsonProperty("claim_with_injured")
    private Boolean claimWithInjured;

    @JsonProperty("damage_can_not_be_repaired")
    private Boolean damageCanNotBeRepaired;

    @JsonProperty("min_import")
    private Double minImport;

    @JsonProperty("max_import")
    private Double maxImport;

    @JsonProperty("claims_type")
    private List<ClaimsTypeForRuleRequestV1> claimsType;

    @JsonProperty("damaged_insurance_company_list")
    private List<InsuranceCompanyRequestV1> damagedInsuranceCompanyList;

    @JsonProperty("counterpart_insurance_company_list")
    private List<InsuranceCompanyRequestV1> counterpartInsuranceCompanyList;

    public String getSelectionName() {
        return selectionName;
    }

    public void setSelectionName(String selectionName) {
        this.selectionName = selectionName;
    }

    public Integer getMonthlyVolume() {
        return monthlyVolume;
    }

    public void setMonthlyVolume(Integer monthlyVolume) {
        this.monthlyVolume = monthlyVolume;
    }

    public Integer getCurrentVolume() {
        return currentVolume;
    }

    public void setCurrentVolume(Integer currentVolume) {
        this.currentVolume = currentVolume;
    }

    public Integer getDetail() {
        return detail;
    }

    public void setDetail(Integer detail) {
        this.detail = detail;
    }

    public String getAnnotations() {
        return annotations;
    }

    public void setAnnotations(String annotations) {
        this.annotations = annotations;
    }

    public Boolean getComplaintsWithoutCtp() {
        return complaintsWithoutCtp;
    }


    public void setComplaintsWithoutCtp(Boolean complaintsWithoutCtp) {
        this.complaintsWithoutCtp = complaintsWithoutCtp;
    }

    public Boolean getForeignCountryClaim() {
        return foreignCountryClaim;
    }


    public void setForeignCountryClaim(Boolean foreignCountryClaim) {
        this.foreignCountryClaim = foreignCountryClaim;
    }

    public VehicleTypeEnum getCounterpartType() {
        return counterpartType;
    }

    public void setCounterpartType(VehicleTypeEnum counterpartType) {
        this.counterpartType = counterpartType;
    }

    public Boolean getClaimWithInjured() {
        return claimWithInjured;
    }

    public void setClaimWithInjured(Boolean claimWithInjured) {
        this.claimWithInjured = claimWithInjured;
    }


    public Boolean getDamageCanNotBeRepaired() {
        return damageCanNotBeRepaired;
    }

    public void setDamageCanNotBeRepaired(Boolean damageCanNotBeRepaired) {
        this.damageCanNotBeRepaired = damageCanNotBeRepaired;
    }

    public List<ClaimsTypeForRuleRequestV1> getClaimsType() {
        if(claimsType == null){
            return null;
        }
        return new ArrayList<>(claimsType);
    }

    public void setClaimsType(List<ClaimsTypeForRuleRequestV1> claimsType) {
        if(claimsType != null)
        {
            this.claimsType = new ArrayList<>(claimsType);
        }else {
            this.claimsType = null;
        }
    }

    public List<InsuranceCompanyRequestV1> getDamagedInsuranceCompanyList() {
        if(damagedInsuranceCompanyList == null){
            return null;
        }
        return new ArrayList<>(damagedInsuranceCompanyList);
    }

    public void setDamagedInsuranceCompanyList(List<InsuranceCompanyRequestV1> damagedInsuranceCompanyList) {
        if(damagedInsuranceCompanyList != null)
        {
            this.damagedInsuranceCompanyList = new ArrayList<>(damagedInsuranceCompanyList);
        }else {
            this.damagedInsuranceCompanyList = null;
        }
    }

    public List<InsuranceCompanyRequestV1> getCounterpartInsuranceCompanyList() {
        if(counterpartInsuranceCompanyList == null){
            return null;
        }
        return new ArrayList<>(counterpartInsuranceCompanyList);
    }

    public void setCounterpartInsuranceCompanyList(List<InsuranceCompanyRequestV1> counterpartInsuranceCompanyList) {
        if(counterpartInsuranceCompanyList != null)
        {
            this.counterpartInsuranceCompanyList = new ArrayList<>(counterpartInsuranceCompanyList);
        }else {
            this.counterpartInsuranceCompanyList = null;
        }
    }

    public Double getMinImport() {
        return minImport;
    }

    public void setMinImport(Double minImport) {
        this.minImport = minImport;
    }

    public Double getMaxImport() {
        return maxImport;
    }

    public void setMaxImport(Double maxImport) {
        this.maxImport = maxImport;
    }

    @Override
    public String toString() {
        return "AutomaticAffiliationRuleLegalRequestV1{" +
                "selectionName='" + selectionName + '\'' +
                ", monthlyVolume=" + monthlyVolume +
                ", currentVolume=" + currentVolume +
                ", detail=" + detail +
                ", annotations='" + annotations + '\'' +
                ", complaintsWithoutCtp=" + complaintsWithoutCtp +
                ", foreignCountryClaim=" + foreignCountryClaim +
                ", counterpartType='" + counterpartType + '\'' +
                ", claimWithInjured=" + claimWithInjured +
                ", damageCanNotBeRepaired=" + damageCanNotBeRepaired +
                ", minImport=" + minImport +
                ", maxImport=" + maxImport +
                ", claimsType=" + claimsType +
                ", damagedInsuranceCompanyList=" + damagedInsuranceCompanyList +
                ", counterpartInsuranceCompanyList=" + counterpartInsuranceCompanyList +
                '}';
    }
}
