package com.doing.nemo.claims.controller.payload.request.counterparty;

import com.doing.nemo.claims.controller.payload.request.driver.DrivingLicenseRequest;
import com.doing.nemo.claims.controller.payload.request.driver.LinkRequest;
import com.doing.nemo.claims.entity.jsonb.Address;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DriverRequest implements Serializable {

    @JsonProperty("id")
    private Integer id;

    @JsonProperty("identification")
    private String identification;

    @JsonProperty("official_registration")
    private String officialRegistration;

    @JsonProperty("trading_name")
    private String tradingName;

    @JsonProperty("firstname")
    private String firstname;

    @JsonProperty("lastname")
    private String lastname;

    @JsonProperty("fiscal_code")
    private String fiscalCode;

    @JsonProperty("main_address")
    private Address mainAddress;

    @JsonProperty("date_of_birth")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date dateOfBirth;

    @JsonProperty("phone")
    private String phone;

    @JsonProperty("email")
    private String email;

    @JsonProperty("pec")
    private String pec;

    @JsonProperty("sex")
    private String sex;

    @JsonProperty("customer_id")
    private String customerId;

    @JsonProperty("driver_injury")
    private Boolean driverInjury = false;

    @JsonProperty("driving_license")
    private DrivingLicenseRequest drivingLicense;

    @JsonProperty("links")
    private List<LinkRequest> linksList;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public String getOfficialRegistration() {
        return officialRegistration;
    }

    public void setOfficialRegistration(String officialRegistration) {
        this.officialRegistration = officialRegistration;
    }

    public String getTradingName() {
        return tradingName;
    }

    public void setTradingName(String tradingName) {
        this.tradingName = tradingName;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFiscalCode() {
        return fiscalCode;
    }

    public void setFiscalCode(String fiscalCode) {
        this.fiscalCode = fiscalCode;
    }

    public Address getMainAddress() {
        return mainAddress;
    }

    public void setMainAddress(Address mainAddress) {
        this.mainAddress = mainAddress;
    }

    public Date getDateOfBirth() {
        if(dateOfBirth == null){
            return null;
        }
        return (Date)dateOfBirth.clone();
    }

    public void setDateOfBirth(Date dateOfBirth) {
        if(dateOfBirth != null)
        {
            this.dateOfBirth = (Date)dateOfBirth.clone();
        }
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<LinkRequest> getLinksList() {
        if(linksList == null){
            return null;
        }
        return new ArrayList<>(linksList);
    }

    public void setLinksList(List<LinkRequest> linksList) {
        if(linksList != null)
        {
            this.linksList =new ArrayList<>(linksList);
        } else {
            this.linksList = null;
        }
    }

    public String getPec() {
        return pec;
    }

    public void setPec(String pec) {
        this.pec = pec;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public DrivingLicenseRequest getDrivingLicense() {
        return drivingLicense;
    }

    public void setDrivingLicense(DrivingLicenseRequest drivingLicense) {
        this.drivingLicense = drivingLicense;
    }

    public Boolean getDriverInjury() {
        return driverInjury;
    }

    public void setDriverInjury(Boolean driverInjury) {
        this.driverInjury = driverInjury;
    }

    @Override
    public String toString() {
        return "Driver{" +
                "id=" + id +
                ", identification='" + identification + '\'' +
                ", officialRegistration='" + officialRegistration + '\'' +
                ", tradingName='" + tradingName + '\'' +
                ", firstName='" + firstname + '\'' +
                ", lastName='" + lastname + '\'' +
                ", fiscalCode='" + fiscalCode + '\'' +
                ", mainAddress=" + mainAddress +
                ", dateOfBirth=" + dateOfBirth +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", pec='" + pec + '\'' +
                ", sex='" + sex + '\'' +
                ", customerId='" + customerId + '\'' +
                ", driverInjury=" + driverInjury +
                ", drivingLicense=" + drivingLicense +
                ", linksList=" + linksList +
                '}';
    }




}
