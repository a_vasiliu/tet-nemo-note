package com.doing.nemo.claims.controller.payload.response;

import com.doing.nemo.claims.entity.enumerated.ClaimsRepairEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.UUID;

public class  GenericAttachmentResponseV1 implements Serializable {

    @JsonProperty("id")
    private UUID id;

    @JsonProperty("original_name")
    private String originalName;

    @JsonProperty("description")
    private String description;

    @JsonProperty("attachment_id")
    private String attachmentId;

    @JsonProperty("created_at")
    private String createdAt;

    @JsonProperty("is_active")
    private Boolean isActive;

    @JsonProperty("type_complaint")
    private ClaimsRepairEnum typeComplaint;

    public GenericAttachmentResponseV1() {
    }

    public GenericAttachmentResponseV1(UUID id, String originalName, String description, String attachmentId, String createdAt, Boolean isActive, ClaimsRepairEnum typeComplaint) {
        this.id = id;
        this.originalName = originalName;
        this.description = description;
        this.attachmentId = attachmentId;
        this.createdAt = createdAt;
        this.isActive = isActive;
        this.typeComplaint = typeComplaint;
    }

    public ClaimsRepairEnum getTypeComplaint() {
        return typeComplaint;
    }

    public void setTypeComplaint(ClaimsRepairEnum typeComplaint) {
        this.typeComplaint = typeComplaint;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getOriginalName() {
        return originalName;
    }

    public void setOriginalName(String originalName) {
        this.originalName = originalName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAttachmentId() {
        return attachmentId;
    }

    public void setAttachmentId(String attachmentId) {
        this.attachmentId = attachmentId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    @JsonIgnore
    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    @Override
    public String toString() {
        return "GenericAttachmentResponseV1{" +
                "id=" + id +
                ", originalName='" + originalName + '\'' +
                ", description='" + description + '\'' +
                ", attachmentId='" + attachmentId + '\'' +
                ", createdAt=" + createdAt +
                '}';
    }
}
