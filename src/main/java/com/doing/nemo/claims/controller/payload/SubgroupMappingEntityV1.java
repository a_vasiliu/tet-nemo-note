package com.doing.nemo.claims.controller.payload;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

public class SubgroupMappingEntityV1 {

    private UUID id;

    private String name;

    @JsonProperty("subgroup_id")
    private Long subgroupId;

    public SubgroupMappingEntityV1() {
    }

    public SubgroupMappingEntityV1(UUID id, String name, Long subgroupId) {
        this.id = id;
        this.name = name;
        this.subgroupId = subgroupId;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getSubgroupId() {
        return subgroupId;
    }

    public void setSubgroupId(Long subgroupId) {
        this.subgroupId = subgroupId;
    }
}
