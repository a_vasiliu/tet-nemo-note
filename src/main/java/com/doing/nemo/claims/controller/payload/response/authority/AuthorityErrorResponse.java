package com.doing.nemo.claims.controller.payload.response.authority;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AuthorityErrorResponse {

    /*
	"error":"Bad Request",
	"message":"Authority Error, Claims with id '9becaa9a-2bb1-4ec5-a280-52754a08a9ee' is not associable because it's read only",
	"code":"1100"
     */

    @JsonProperty("message")
    private String message;

    @JsonProperty("code")
    private String code;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
