package com.doing.nemo.claims.controller.payload.request.claims;

import com.doing.nemo.claims.controller.payload.request.forms.AttachmentRequest;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class FormRequest implements Serializable {

    @JsonProperty("attachment")
    private List<AttachmentRequest> attachment;

    public List<AttachmentRequest> getAttachment() {
        if(attachment == null){
            return null;
        }
        return new ArrayList<>(attachment);
    }

    public void setAttachment(List<AttachmentRequest> attachment) {
        if(attachment != null)
        {
            this.attachment = new ArrayList<>(attachment);
        } else {
            this.attachment = null;
        }
    }

    @Override
    public String toString() {
        return "FormRequestV1{" +
                "attachment=" + attachment +
                '}';
    }


}
