package com.doing.nemo.claims.controller.payload.response;

import com.doing.nemo.claims.controller.payload.response.forms.AttachmentResponse;
import com.doing.nemo.claims.entity.enumerated.PolicyEnum.PolicyTypeEnum;
import com.doing.nemo.claims.entity.settings.EventEntity;
import com.doing.nemo.claims.entity.settings.InsuranceCompanyEntity;
import com.doing.nemo.claims.entity.settings.InsuranceManagerEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class InsurancePolicyResponseV1 implements Serializable {

    @JsonProperty("id")
    private UUID id;

    @JsonProperty("type")
    private PolicyTypeEnum type;

    @JsonProperty("description")
    private String description;

    @JsonProperty("number_policy")
    private String numberPolicy;

    //@TypeEnum(type = "JsonDataUserType")
    @JsonProperty("insurance_company")
    private InsuranceCompanyEntity insuranceCompanyEntity;

    //@TypeEnum(type = "JsonDataUserType")
    @JsonProperty("insurance_manager")
    private InsuranceManagerEntity insuranceManagerEntity;

    @JsonProperty("client_code")
    private Long clientCode;

    @JsonProperty("client")
    private String client;

    @JsonProperty("annotations")
    private String annotations;

    @JsonProperty("beginning_validity")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date beginningValidity;

    @JsonProperty("end_validity")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date endValidity;

    @JsonProperty("book_register")
    private Boolean bookRegister;

    @JsonProperty("book_register_code")
    private String bookRegisterCode;

    @JsonProperty("last_renewal_notification")
    private String lastRenewalNotification;

    @JsonProperty("renewal_notification")
    private Boolean renewalNotification;

    //@TypeEnum(type = "JsonDataUserType")
    @JsonProperty("risk_entity_list")
    private List<RiskResponseV1> riskEntityList;

    //@TypeEnum(type = "JsonDataUserType")
    @JsonProperty("attachment")
    private List<AttachmentResponse> attachmentEntityList;

    //@TypeEnum(type = "JsonDataUserType")
    @JsonProperty("event")
    private List<EventEntity> eventEntityList;

    @JsonProperty("is_active")
    private Boolean isActive;

    @JsonProperty("franchise")
    private Double franchise;

    public InsurancePolicyResponseV1() {
    }

    public Double getFranchise() {
        return franchise;
    }

    public void setFranchise(Double franchise) {
        this.franchise = franchise;
    }

    public Date getBeginningValidity() {
        if(beginningValidity == null){
            return null;
        }
        return (Date)beginningValidity.clone();
    }

    public Date getEndValidity() {
        if(endValidity == null){
            return null;
        }
        return (Date)endValidity.clone();
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public PolicyTypeEnum getType() {
        return type;
    }

    public void setType(PolicyTypeEnum type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNumberPolicy() {
        return numberPolicy;
    }

    public void setNumberPolicy(String numberPolicy) {
        this.numberPolicy = numberPolicy;
    }

    public InsuranceCompanyEntity getInsuranceCompanyEntity() {
        return insuranceCompanyEntity;
    }

    public void setInsuranceCompanyEntity(InsuranceCompanyEntity insuranceCompanyEntity) {
        this.insuranceCompanyEntity = insuranceCompanyEntity;
    }


    public InsuranceManagerEntity getInsuranceManagerEntity() {
        return insuranceManagerEntity;
    }

    public void setInsuranceManagerEntity(InsuranceManagerEntity insuranceManagerEntity) {
        this.insuranceManagerEntity = insuranceManagerEntity;
    }

    public Long getClientCode() {
        return clientCode;
    }

    public void setClientCode(Long clientCode) {
        this.clientCode = clientCode;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getAnnotations() {
        return annotations;
    }

    public void setAnnotations(String annotations) {
        this.annotations = annotations;
    }

    public Boolean getBookRegister() {
        return bookRegister;
    }

    public void setBookRegister(Boolean bookRegister) {
        this.bookRegister = bookRegister;
    }

    public String getBookRegisterCode() {
        return bookRegisterCode;
    }

    public void setBookRegisterCode(String bookRegisterCode) {
        this.bookRegisterCode = bookRegisterCode;
    }

    public void setBeginningValidity(Date beginningValidity) {
        if(beginningValidity != null)
        {
            this.beginningValidity = (Date)beginningValidity.clone();
        } else {
            this.beginningValidity = null;
        }
    }

    public void setEndValidity(Date endValidity) {
        if(endValidity != null)
        {
            this.endValidity = (Date)endValidity.clone();
        } else {
            this.endValidity = null;
        }
    }

    public String getLastRenewalNotification() {
        return lastRenewalNotification;
    }

    public void setLastRenewalNotification(String lastRenewalNotification) {
        this.lastRenewalNotification = lastRenewalNotification;
    }

    public Boolean getRenewalNotification() {
        return renewalNotification;
    }

    public void setRenewalNotification(Boolean renewalNotification) {
        this.renewalNotification = renewalNotification;
    }

    public List<RiskResponseV1> getRiskEntityList() {
        if(riskEntityList == null){
            return null;
        }
        return new ArrayList<>(riskEntityList);
    }

    public void setRiskEntityList(List<RiskResponseV1> riskEntityList) {
        if(riskEntityList != null)
        {
            this.riskEntityList = new ArrayList<>(riskEntityList);
        } else {
            this.riskEntityList = null;
        }
    }

    public List<AttachmentResponse> getAttachmentEntityList() {
        if(attachmentEntityList == null){
            return null;
        }
        return new ArrayList<>(attachmentEntityList);
    }

    public void setAttachmentEntityList(List<AttachmentResponse> attachmentEntityList) {
        if(attachmentEntityList != null)
        {
            this.attachmentEntityList = new ArrayList<>(attachmentEntityList);
        } else {
            this.attachmentEntityList = null;
        }
    }

    public List<EventEntity> getEventEntityList() {
        if(eventEntityList == null){
            return null;
        }
        return new ArrayList<>(eventEntityList);
    }

    public void setEventEntityList(List<EventEntity> eventEntityList) {
        if(eventEntityList != null)
        {
            this.eventEntityList = new ArrayList<>(eventEntityList);
        } else {
            this.eventEntityList = null;
        }
    }

    @JsonIgnore
    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    @Override
    public String toString() {
        return "InsurancePolicyResponseV1{" +
                "id=" + id +
                ", type=" + type +
                ", description='" + description + '\'' +
                ", numberPolicy='" + numberPolicy + '\'' +
                ", insuranceCompanyEntity=" + insuranceCompanyEntity +
                ", insuranceManagerEntity=" + insuranceManagerEntity +
                ", clientCode=" + clientCode +
                ", client='" + client + '\'' +
                ", annotations='" + annotations + '\'' +
                ", beginningValidity=" + beginningValidity +
                ", endValidity=" + endValidity +
                ", bookRegister=" + bookRegister +
                ", bookRegisterCode='" + bookRegisterCode + '\'' +
                ", lastRenewalNotification='" + lastRenewalNotification + '\'' +
                ", renewalNotification=" + renewalNotification +
                ", riskEntityList=" + riskEntityList +
                ", attachmentEntityList=" + attachmentEntityList +
                ", eventEntityList=" + eventEntityList +
                ", isActive=" + isActive +
                '}';
    }

}
