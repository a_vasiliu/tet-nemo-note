package com.doing.nemo.claims.controller.payload.response;

import com.doing.nemo.claims.controller.payload.response.claims.HistoricalResponse;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsFlowEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


public class ExportLogResponse  implements Serializable {

    @JsonProperty("claims_id")
    private UUID id;

    @JsonProperty("claims_practice_id")
    private Long practiceId;

    @JsonProperty("claims_type")
    private DataAccidentTypeAccidentEnum claimsType;

    @JsonProperty("claims_flow")
    private ClaimsFlowEnum flow;

    @JsonProperty("logs")
    private List<HistoricalResponse> historicalList;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Long getPracticeId() {
        return practiceId;
    }

    public void setPracticeId(Long practiceId) {
        this.practiceId = practiceId;
    }

    public DataAccidentTypeAccidentEnum getClaimsType() {
        return claimsType;
    }

    public void setClaimsType(DataAccidentTypeAccidentEnum claimsType) {
        this.claimsType = claimsType;
    }

    public ClaimsFlowEnum getFlow() {
        return flow;
    }

    public void setFlow(ClaimsFlowEnum flow) {
        this.flow = flow;
    }

    public List<HistoricalResponse> getHistoricalList() {
        if(historicalList == null){
            return null;
        }
        return new ArrayList<>(historicalList);
    }

    public void setHistoricalList(List<HistoricalResponse> historicalList) {
        if(historicalList != null)
        {
            this.historicalList = new ArrayList<>(historicalList);
        } else {
            this.historicalList = null;
        }
    }

    @Override
    public String toString() {
        return "ExportLogResponse{" +
                "id=" + id +
                ", claimsType=" + claimsType +
                ", flow=" + flow +
                ", historicalList=" + historicalList +
                '}';
    }
}
