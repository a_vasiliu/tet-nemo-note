package com.doing.nemo.claims.controller;

import com.doing.nemo.claims.adapter.InsuranceManagerAdapter;
import com.doing.nemo.claims.common.validation.MessageCode;
import com.doing.nemo.claims.common.validation.RequestValidator;
import com.doing.nemo.claims.controller.payload.request.InsuranceManagerRequestV1;
import com.doing.nemo.claims.controller.payload.request.ListRequestV1;
import com.doing.nemo.claims.controller.payload.response.ClaimsResponsePaginationV1;
import com.doing.nemo.claims.controller.payload.response.IdResponseV1;
import com.doing.nemo.claims.controller.payload.response.InsuranceManagerResponseV1;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.settings.InsuranceManagerEntity;
import com.doing.nemo.claims.service.InsuranceManagerService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/v1")
@Api("Insurance Manager")
public class InsuranceManagerController {

    @Autowired
    private InsuranceManagerService insuranceManagerService;

    @Autowired
    private InsuranceManagerAdapter insuranceManagerAdapter;

    @Autowired
    private RequestValidator requestValidator;

    @PostMapping("/insurancemanager")
    @Transactional
    @ApiOperation(value = "Insert Insurance Manager", notes = "Insert insurance manager using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = IdResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<IdResponseV1> insertInsuranceManager(
            @ApiParam(value = "Body of the Insurance Manager to be created", required = true)
            @RequestBody InsuranceManagerRequestV1 insuranceManagerRequest) {

        requestValidator.validateRequest(insuranceManagerRequest, MessageCode.E00X_1000);
        InsuranceManagerEntity insuranceManagerEntity = insuranceManagerService.insertManager(InsuranceManagerAdapter.adptFromIManaRequToIManagEntity(insuranceManagerRequest));
        IdResponseV1 responseV1 = new IdResponseV1(insuranceManagerEntity.getId());
        return new ResponseEntity<>(responseV1, HttpStatus.CREATED);

    }

    @PostMapping("/insurancemanagers")
    @Transactional
    @ApiOperation(value = "Insert a list of Insurance Managers", notes = "Insert a list of insurance managers using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = IdResponseV1.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<InsuranceManagerResponseV1>> insertInsuranceManagers(
            @ApiParam(value = "List of the bodies of the Insurance Companies to be created", required = true)
            @RequestBody ListRequestV1<InsuranceManagerRequestV1> insuranceManagersRequest) {

        List<InsuranceManagerResponseV1> insuranceManagerResponseV1List = new ArrayList<>();
        for (InsuranceManagerRequestV1 insuranceManagerRequestV1 : insuranceManagersRequest.getData()) {
            requestValidator.validateRequest(insuranceManagerRequestV1, MessageCode.E00X_1000);
            InsuranceManagerEntity insuranceManagerEntity = InsuranceManagerAdapter.adptFromIManaRequToIManagEntity(insuranceManagerRequestV1);
            insuranceManagerEntity = insuranceManagerService.insertManager(insuranceManagerEntity);
            InsuranceManagerResponseV1<InsuranceManagerEntity> responseV1 = InsuranceManagerAdapter.adptFromIManaEntityToIManagRespo(insuranceManagerEntity);
            insuranceManagerResponseV1List.add(responseV1);
        }
        return new ResponseEntity<>(insuranceManagerResponseV1List, HttpStatus.CREATED);

    }

    @GetMapping("/insurancemanager/{UUID}")
    @Transactional
    @ApiOperation(value = "Recover Insurance Manager", notes = "Returns a Insurance Manager using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = InsuranceManagerResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<InsuranceManagerResponseV1> getInsuranceManagers(
            @ApiParam(value = "UUID of the Insurance Manager to be found", required = true)
            @PathVariable UUID UUID) {

        InsuranceManagerEntity insuranceManagerEntity = insuranceManagerService.selectManager(UUID);
        InsuranceManagerResponseV1<InsuranceManagerEntity> responseV1 = insuranceManagerAdapter.adptFromIManaEntityToIManagRespo(insuranceManagerEntity);
        return new ResponseEntity<>(responseV1, HttpStatus.OK);
    }

    @GetMapping("/insurancemanager")
    @Transactional
    @ApiOperation(value = "Recover All Insurance Managers", notes = "Returns all Insurance Managers")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = InsuranceManagerResponseV1.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<InsuranceManagerResponseV1>> getAllInsuranceManagers() {

        List<InsuranceManagerEntity> insuranceManagerEntityList = insuranceManagerService.selectAllManager();

        List<InsuranceManagerResponseV1> responseV1 = insuranceManagerAdapter.adptFromIManaEntityToIManagRespoList(insuranceManagerEntityList);
        return new ResponseEntity<>(responseV1, HttpStatus.OK);
    }

    @PutMapping("/insurancemanager/{UUID}")
    @Transactional
    @ApiOperation(value = "Upload Insurance Manager", notes = "Upload Insurance Manager using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = InsuranceManagerResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<InsuranceManagerResponseV1> putInsuranceManagers(
            @ApiParam(value = "UUID that identifies the Insurance Manager to be modified", required = true)
            @PathVariable UUID UUID,
            @ApiParam(value = "Updated body of the Insurance Manager", required = true)
            @RequestBody InsuranceManagerRequestV1 insuranceManagersRequest) {


        requestValidator.validateRequest(insuranceManagersRequest, MessageCode.E00X_1000);
        InsuranceManagerEntity insuranceManagerEntity = insuranceManagerAdapter.adptFromIManaRequToIManaEntityWithID(insuranceManagersRequest, UUID);

        insuranceManagerEntity = insuranceManagerService.updateManager(insuranceManagerEntity);
        InsuranceManagerResponseV1<InsuranceManagerEntity> responseV1 = insuranceManagerAdapter.adptFromIManaEntityToIManagRespo(insuranceManagerEntity);
        return new ResponseEntity<>(responseV1, HttpStatus.OK);

    }

    @DeleteMapping("/insurancemanager/{UUID}")
    @Transactional
    @ApiOperation(value = "Delete Insurance Manager", notes = "Delete Insurance Manager using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = InsuranceManagerResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<InsuranceManagerResponseV1> deleteInsuranceManagers(
            @ApiParam(value = "UUID that identifies the Insurance Manager to be deleted", required = true)
            @PathVariable UUID UUID) {
        InsuranceManagerResponseV1 responseV1 = insuranceManagerService.deleteManager(UUID);
        return new ResponseEntity<>(responseV1, HttpStatus.OK);
    }

    @PatchMapping("/insurancemanager/{UUID}/active")
    @Transactional
    @ApiOperation(value = "Patch status active Insurance Manager", notes = "Patch status active Insurance Manager using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = InsuranceManagerResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<InsuranceManagerResponseV1> patchStatusInsuranceManager(
            @PathVariable UUID UUID
    ) {
        InsuranceManagerEntity insuranceManagerEntity = insuranceManagerService.updateStatusManager(UUID);
        InsuranceManagerResponseV1 responseV1 = insuranceManagerAdapter.adptFromIManaEntityToIManagRespo(insuranceManagerEntity);
        return new ResponseEntity<>(responseV1, HttpStatus.OK);
    }

    @ApiOperation(value="Insurance Manager Pagination", notes = "It retrieves Insurance Manager pagination")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = ClaimsResponsePaginationV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @GetMapping(value="/insurancemanager/pagination", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<ClaimsResponsePaginationV1> managerPagination(
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @RequestParam(value = "page_size", defaultValue = "20") Integer pageSize,
            @RequestParam(value = "order_by", required = false) String orderBy,
            @RequestParam(value = "asc", required = false, defaultValue = "true") Boolean asc,
            @RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "province", required = false) String province,
            @RequestParam(value = "telephone", required = false) String telephone,
            @RequestParam(value = "fax", required = false) String fax,
            @RequestParam(value = "email", required = false) String email,
            @RequestParam(value = "is_active", required = false) Boolean isActive,
            @RequestParam(value = "address", required = false) String address,
            @RequestParam(value = "zip_code", required = false) String zipCode,
            @RequestParam(value = "city", required = false) String city,
            @RequestParam(value = "state", required = false) String state,
            @RequestParam(value = "rif_code", required = false) Long rifCode

    ){
        Pagination<InsuranceManagerEntity> insuranceManagerEntityPagination = insuranceManagerService.paginationInsuranceManager(page, pageSize, orderBy, asc, name, province, telephone, fax, email, isActive, address, zipCode, city, state, rifCode);

        return new ResponseEntity<>(insuranceManagerAdapter.adptInsuranceManagerPaginationToClaimsResponsePagination(insuranceManagerEntityPagination), HttpStatus.OK);
    }
}
