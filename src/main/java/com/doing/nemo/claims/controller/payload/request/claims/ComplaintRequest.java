package com.doing.nemo.claims.controller.payload.request.claims;

import com.doing.nemo.claims.controller.payload.request.complaint.DataAccidentRequest;
import com.doing.nemo.claims.controller.payload.request.complaint.EntrustedRequest;
import com.doing.nemo.claims.controller.payload.request.complaint.FromCompanyRequest;
import com.doing.nemo.claims.controller.payload.request.complaint.RepairRequest;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.ComplaintModEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.ComplaintPropertyEnum;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class ComplaintRequest implements Serializable {

    @JsonProperty("client_id")
    private String clientId;

    @JsonProperty("plate")
    private String plate;

    @JsonProperty("locator")
    private String locator;

    @JsonProperty("activation")
    private String activation;

    @JsonProperty("property")
    private ComplaintPropertyEnum property;

    @JsonProperty("mod")
    private ComplaintModEnum mod;

    @JsonProperty("notification")
    private String notification;

    @JsonProperty("quote")
    private Boolean quote = false;

    @JsonProperty("data_accident")
    private DataAccidentRequest dataAccident;

    @JsonProperty("from_company")
    private FromCompanyRequest fromCompany;

    @JsonProperty("repair")
    private RepairRequest repair;

    @JsonProperty("entrusted")
    private EntrustedRequest entrusted;

    public ComplaintRequest() {
    }

    public ComplaintRequest(String clientId, String plate, String locator, String activation, ComplaintPropertyEnum property,
                            ComplaintModEnum mod, String notification, Boolean quote, DataAccidentRequest dataAccident,
                            FromCompanyRequest fromCompany, RepairRequest repair, EntrustedRequest entrusted) {
        this.clientId = clientId;
        this.plate = plate;
        this.locator = locator;
        this.activation = activation;
        this.property = property;
        this.mod = mod;
        this.notification = notification;
        this.quote = quote;
        this.dataAccident = dataAccident;
        this.fromCompany = fromCompany;
        this.repair = repair;
        this.entrusted = entrusted;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public String getLocator() {
        return locator;
    }

    public void setLocator(String locator) {
        this.locator = locator;
    }

    public String getActivation() {
        return activation;
    }

    public void setActivation(String activation) {
        this.activation = activation;
    }

    public ComplaintPropertyEnum getProperty() {
        return property;
    }

    public void setProperty(ComplaintPropertyEnum property) {
        this.property = property;
    }

    public ComplaintModEnum getMod() {
        return mod;
    }

    public void setMod(ComplaintModEnum mod) {
        this.mod = mod;
    }

    public String getNotification() {
        return notification;
    }

    public void setNotification(String notification) {
        this.notification = notification;
    }

    public Boolean getQuote() {
        return quote;
    }

    public void setQuote(Boolean quote) {
        this.quote = quote;
    }

    public DataAccidentRequest getDataAccident() {
        return dataAccident;
    }

    public void setDataAccident(DataAccidentRequest dataAccident) {
        this.dataAccident = dataAccident;
    }

    public FromCompanyRequest getFromCompany() {
        return fromCompany;
    }

    public void setFromCompany(FromCompanyRequest fromCompany) {
        this.fromCompany = fromCompany;
    }

    public RepairRequest getRepair() {
        return repair;
    }

    public void setRepair(RepairRequest repair) {
        this.repair = repair;
    }

    public EntrustedRequest getEntrusted() {
        return entrusted;
    }

    public void setEntrusted(EntrustedRequest entrusted) {
        this.entrusted = entrusted;
    }

    @Override
    public String toString() {
        return "ComplaintRequestV1{" +
                "clientId=" + clientId +
                ", plate='" + plate + '\'' +
                ", locator='" + locator + '\'' +
                ", activation='" + activation + '\'' +
                ", property=" + property +
                ", mod=" + mod +
                ", notification=" + notification +
                ", quote=" + quote +
                ", dataAccident=" + dataAccident +
                ", fromCompany=" + fromCompany +
                ", repair=" + repair +
                ", entrusted=" + entrusted +
                '}';
    }








}
