package com.doing.nemo.claims.controller.payload.response;

import com.doing.nemo.claims.entity.enumerated.DTO.PageStats;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PaginationResponseV1<T>  implements Serializable {

    private PageStats stats;
    private List<T> items;

    public PaginationResponseV1() {
    }

    public PaginationResponseV1(PageStats stats, List<T> items) {
        this.stats = stats;
        if(items != null)
        {
            this.items = new ArrayList<>(items);
        }
    }

    public PageStats getStats() {
        return stats;
    }

    public void setStats(PageStats stats) {
        this.stats = stats;
    }

    public List<T> getItems() {
        if(items == null){
            return null;
        }
        return new ArrayList<>(items);
    }

    public void setItems(List<T> items) {
        if(items != null)
        {
            this.items = new ArrayList<>(items);
        } else {
            this.items = null;
        }
    }
}
