package com.doing.nemo.claims.controller.payload.request;

import com.doing.nemo.claims.entity.settings.ClaimsTypeEntity;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;

import javax.validation.constraints.NotNull;
import java.io.Serializable;


public class RecoverabilityRequestV1 implements Serializable {

    @JsonProperty("management")
    @NotNull
    private String management;

    @JsonProperty("claims_type")
    private ClaimsTypeEntity claimsType;

    @JsonProperty("counterpart")
    @NotNull
    private Boolean counterpart;

    @JsonProperty("recoverability")
    @NotNull
    private Boolean recoverability;

    @JsonProperty("percent_recoverability")
    @NotNull
    private Double percentRecoverability;

    @JsonProperty("is_active")
    private Boolean isActive = true;

    public RecoverabilityRequestV1(@NotNull String management, ClaimsTypeEntity claimsType, @NotNull Boolean counterpart, @NotNull Boolean recoverability, @NotNull Double percentRecoverability, Boolean isActive) {
        this.management = management;
        this.claimsType = claimsType;
        this.counterpart = counterpart;
        this.recoverability = recoverability;
        this.percentRecoverability = percentRecoverability;
        this.isActive = isActive;
    }

    public String getManagement() {
        return management;
    }

    public void setManagement(String management) {
        this.management = management;
    }

    public ClaimsTypeEntity getClaimsType() {
        return claimsType;
    }

    public void setClaimsType(ClaimsTypeEntity claimsType) {
        this.claimsType = claimsType;
    }

    public Boolean getCounterpart() {
        return counterpart;
    }

    public void setCounterpart(Boolean counterpart) {
        this.counterpart = counterpart;
    }

    public Boolean getRecoverability() {
        return recoverability;
    }

    public void setRecoverability(Boolean recoverability) {
        this.recoverability = recoverability;
    }

    public Double getPercentRecoverability() {
        return percentRecoverability;
    }

    public void setPercentRecoverability(Double percentRecoverability) {
        this.percentRecoverability = percentRecoverability;
    }

    public Boolean getActive() {
        return isActive;
    }

    @JsonSetter
    public void setActive(Boolean active) {
        if (active != null)
            isActive = active;
    }

    @Override
    public String toString() {
        return "RecoverabilityRequestV1{" +
                "management='" + management + '\'' +
                ", claimsType=" + claimsType +
                ", counterpart=" + counterpart +
                ", recoverability=" + recoverability +
                ", percentRecoverability=" + percentRecoverability +
                ", isActive=" + isActive +
                '}';
    }
}