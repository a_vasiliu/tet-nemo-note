package com.doing.nemo.claims.controller.payload.response.authority.claims;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class AuthorityErrorResponseV1 implements Serializable {

    @JsonProperty("claims_id")
    private String claimsId;

    @JsonProperty("reason")
    private String reason;

    public AuthorityErrorResponseV1(String claimsId, String reason) {
        this.claimsId = claimsId;
        this.reason = reason;
    }

    public AuthorityErrorResponseV1() {
    }

    public String getClaimsId() {
        return claimsId;
    }

    public void setClaimsId(String claimsId) {
        this.claimsId = claimsId;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
