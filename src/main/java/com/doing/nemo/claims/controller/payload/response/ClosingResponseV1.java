package com.doing.nemo.claims.controller.payload.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Date;
@JsonIgnoreProperties(ignoreUnknown = true)
public class ClosingResponseV1 implements Serializable {

    @JsonProperty("id_complaint")
    private String idComplaint;

    @JsonProperty("claims_insert_date")
    private Date claimsInsertDate;

    @JsonProperty("exemption")
    private Double exemption;

    @JsonProperty("deduct_pay_1")
    private Double deductPay1;

    @JsonProperty("deduct_pay_2")
    private Double deductPay2;

    @JsonProperty("status")
    private String status;

    @JsonProperty("damage_list")
    private String damageList;


    public Double getDeductPay1() {
        return deductPay1;
    }

    public void setDeductPay1(Double deductPay1) {
        this.deductPay1 = deductPay1;
    }

    public Double getDeductPay2() {
        return deductPay2;
    }

    public void setDeductPay2(Double deductPay2) {
        this.deductPay2 = deductPay2;
    }

    public String getIdComplaint() {
        return idComplaint;
    }

    public void setIdComplaint(String idComplaint) {
        this.idComplaint = idComplaint;
    }

    public Date getClaimsInsertDate() {
        if(claimsInsertDate == null){
            return null;
        }
        return (Date)claimsInsertDate.clone();
    }

    public void setClaimsInsertDate(Date claimsInsertDate) {
        if(claimsInsertDate != null)
        {
            this.claimsInsertDate = (Date)claimsInsertDate.clone();
        } else {
            this.claimsInsertDate = null;
        }
    }

    public Double getExemption() {
        return exemption;
    }

    public void setExemption(Double exemption) {
        this.exemption = exemption;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDamageList() {
        return damageList;
    }

    public void setDamageList(String damageList) {
        this.damageList = damageList;
    }

    @Override
    public String toString() {
        return "ClosingResponseV1{" +
                "idComplaint='" + idComplaint + '\'' +
                ", claimsInsertDate=" + claimsInsertDate +
                ", exemption=" + exemption +
                ", deductPay1=" + deductPay1 +
                ", deductPay2=" + deductPay2 +
                ", status='" + status + '\'' +
                ", damageList='" + damageList + '\'' +
                '}';
    }
}
