package com.doing.nemo.claims.controller;


import com.doing.nemo.claims.adapter.CustomerAdapter;
import com.doing.nemo.claims.adapter.FleetManagerAdpter;
import com.doing.nemo.claims.adapter.PersonalDataAdapter;
import com.doing.nemo.claims.controller.payload.request.GatewayProfilesMap;
import com.doing.nemo.claims.controller.payload.response.*;
import com.doing.nemo.claims.controller.payload.response.authority.claims.VehicleStatusAuthorityResponseV1;
import com.doing.nemo.claims.controller.payload.response.damaged.CustomerResponse;
import com.doing.nemo.claims.entity.enumerated.AuthorityEnum.AuthorityVehicleStatusEnum;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.FleetManager;
import com.doing.nemo.claims.entity.jsonb.personalData.FleetManagerPersonalData;
import com.doing.nemo.claims.entity.settings.PersonalDataEntity;
import com.doing.nemo.claims.exception.ForbiddenException;
import com.doing.nemo.claims.service.*;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.exception.InternalException;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/v1")
public class EsbContractController {

    private static Logger LOGGER = LoggerFactory.getLogger(EsbContractController.class);
    @Autowired
    private ESBService esbService;
    @Autowired
    private ClaimsService claimsService;
    @Autowired
    private PersonalDataService personalDataService;
    @Autowired
    private AuthorityService authorityService;
    @Autowired
    private ClaimsPendingService claimsPendingService;

    @Autowired
    ObjectMapper objectMapper;

    @GetMapping(value = "/contract/{contractOrPlate}")
    public ResponseEntity<ContractInfoResponseV1> getContractInfo(
            @PathVariable String contractOrPlate,
            @RequestHeader(value = "nemo-contract-date", required = false, defaultValue = "") String date,
            @RequestHeader(value="nemo-profiles-tree") String nemoProfileTree
            ) {
        ContractInfoResponseV1 contractInfoResponseV1;
        try {

            GatewayProfilesMap gatewayProfilesMap = objectMapper.readValue(nemoProfileTree, GatewayProfilesMap.class);


            // Se non esiste un claims di tipo furno non chiuso
            if(!claimsService.isNotClosedTheft(contractOrPlate, date)){
               throw new BadRequestException(MessageCode.CLAIMS_1124);
            }

            contractInfoResponseV1 = esbService.getContractByContractIdOrPlate(contractOrPlate, date, gatewayProfilesMap.getProfiles().get("claims"));

        } catch (NotFoundException | ForbiddenException e) {
            LOGGER.debug(e.getMessage());
            throw e;
        } catch (IOException e) {
            LOGGER.debug(MessageCode.CLAIMS_1018.value());
            throw new InternalException(MessageCode.CLAIMS_1018, e);
        }

   /*     if(contractInfoResponseV1 != null){

            VehicleStatusAuthorityResponseV1 vehicleStatusAuthorityResponseV1 = null;
            String chassis = contractInfoResponseV1.getVehicleResponse().getChassisNumber();
            String plate = contractInfoResponseV1.getVehicleResponse().getLicensePlate();
            try {
                vehicleStatusAuthorityResponseV1 = authorityService.getVehicleStatus(chassis, date);

            } catch (NotFoundException e){
                LOGGER.debug(MessageCode.CLAIMS_1101.value());
            } catch (InternalException e){
                if(e.getCode() != MessageCode.CLAIMS_1144.name()){
                    throw e;
                }
                String message = String.format(MessageCode.CLAIMS_1144.value(), chassis);
                LOGGER.debug(message);
            } catch (Exception e) {
                LOGGER.debug(MessageCode.CLAIMS_1103.value());
                throw new InternalException(MessageCode.CLAIMS_1103, e);
            }

            //Se il veicolo è presente nella tabella dei sinistri pending oppure è sotto lavorazione da authority, si blocca l'inserimento.
            if(vehicleStatusAuthorityResponseV1 != null && AuthorityVehicleStatusEnum.UNDER_MAINTENANCE.equals(vehicleStatusAuthorityResponseV1.getStatus())){
                LOGGER.debug("Vehicle with chassis "+vehicleStatusAuthorityResponseV1.getChassis()+" is under maintenance");
                String message = String.format(MessageCode.CLAIMS_1102.value(), vehicleStatusAuthorityResponseV1.getChassis());
                throw new BadRequestException(message, MessageCode.CLAIMS_1102);
            }*//* else if (claimsPendingService.checkIfExistsPendingWorking(plate,date)) {
                LOGGER.info("Vehicle with plate "+plate+" is under maintenance");
                String message = String.format(MessageCode.CLAIMS_1102.value(), plate);
                throw new BadRequestException(message, MessageCode.CLAIMS_1102);
            }*//*
        }*/
        return new ResponseEntity<>(contractInfoResponseV1, HttpStatus.OK);
    }

    @GetMapping(value = "/contract/counterparty/{contractOrPlate}")
    public ResponseEntity<ContractInfoResponseAldVSAldV1> getContractInfoAldVsAld(
            @PathVariable String contractOrPlate,
            @RequestHeader(value = "nemo-contract-date", required = false, defaultValue = "") String date
    ) throws IOException {
        ContractInfoResponseAldVSAldV1 contractInfoResponseV1;

        try {
            contractInfoResponseV1 = esbService.getContractByContractIdOrPlateAldVSAld(contractOrPlate, date);
        } catch (NotFoundException | ForbiddenException e) {
            LOGGER.debug(e.getMessage());
            throw e;
        } catch (Exception e) {
            LOGGER.debug(MessageCode.CLAIMS_1018.value());
            throw new InternalException(MessageCode.CLAIMS_1018);
        }
        return new ResponseEntity<>(contractInfoResponseV1, HttpStatus.OK);
    }


    @PostMapping(value = "/customer/{CUSTOMER_ID}")
    public ResponseEntity<PersonalDataResponseV1> getCustomer(@PathVariable(name = "CUSTOMER_ID") String customerId) throws IOException {

        CustomerResponse customerResponse = null;
        PersonalDataResponseV1 personalDataResponseV1 = null;
        List<FleetManager> fleetManagerList = null;
        try {
            customerResponse = CustomerAdapter.adptMiddlewareCustomerResponseToCustomerResposne(esbService.getCustomer(customerId));

            //List<FleetManagerEsb>
            fleetManagerList = FleetManagerAdpter.adtFromFMESBToListFM(esbService.getFleetManagers(customerId));
        } catch (IOException e) {
            LOGGER.debug(MessageCode.CLAIMS_1018.value());
            throw new InternalException(MessageCode.CLAIMS_1018, e);
        }
        if (customerResponse != null) {
            PersonalDataEntity personalDataEntity;
            personalDataEntity = CustomerAdapter.adptCustomerResponseToPersonalDataEntity(customerResponse);
            List<FleetManagerPersonalData> fleetManagerPersonalData = FleetManagerAdpter.adptFromFleetManagerESBToFleetManagerPersonalData(esbService.getFleetManagers(customerId));
            personalDataEntity.setFleetManagerPersonalData(fleetManagerPersonalData);
            PersonalDataEntity personalData = personalDataService.insertPersonalData(personalDataEntity);
            personalDataResponseV1 = PersonalDataAdapter.adptFromPersonalDataEntityToPersonalDataResponseV1(personalData);
            //aggiungere fleet PRIMARY
        }

        return new ResponseEntity<>(personalDataResponseV1, HttpStatus.OK);
    }

    @GetMapping(value = "/customer/{CUSTOMER_ID}/fleetmanager/{FLEETMANAGER_ID}")
    public ResponseEntity<FleetManagerResponseV1> checkFleetmanager(
            @PathVariable("CUSTOMER_ID") String customerId, @PathVariable("FLEETMANAGER_ID") String fleetmanagerId
    ) throws IOException {

        FleetManagerResponseV1 result = null;

        result = FleetManagerAdpter.adtpFromFMEsbToFMRespnse(esbService.checkFleetManager(customerId, fleetmanagerId));


        return new ResponseEntity<>(result, HttpStatus.OK);
    }


    @GetMapping(value = "/contract/practice/{contractOrPlate}")
    public ResponseEntity<ContractInfoPracticeResponseV1> getContractInfoPractice(
            @PathVariable String contractOrPlate,
            @RequestHeader(value = "nemo-contract-date", required = false, defaultValue = "") String date
    ) throws IOException {

        return new ResponseEntity<>(esbService.getContractByContractIdOrPlatePractice(contractOrPlate, date), HttpStatus.OK);
    }

}

