package com.doing.nemo.claims.controller.payload.request;

import com.doing.nemo.claims.controller.payload.request.counterparty.*;
import com.doing.nemo.claims.controller.payload.request.forms.AttachmentRequest;
import com.doing.nemo.claims.entity.enumerated.DamagedEnum.VehicleEnum.VehicleTypeEnum;
import com.doing.nemo.claims.entity.enumerated.RepairEnum.ManagementTypeEnum;
import com.doing.nemo.claims.entity.enumerated.RepairEnum.RepairProcedureEnum;
import com.doing.nemo.claims.entity.enumerated.RepairEnum.RepairStatusEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class CounterpartyRequest {

    @JsonProperty("counterparty_id")
    private String counterpartyId;

    @JsonProperty("practice_id_counterparty")
    private Long practiceIdCounterparty;

    @JsonProperty("type")
    private VehicleTypeEnum type;

    @JsonProperty("repair_status")
    private RepairStatusEnum repairStatus;

    @JsonProperty("procedure_repair")
    private RepairProcedureEnum repairProcedure;

    @JsonProperty("motivation")
    private String motivation;

    @JsonProperty("user_create")
    private String userCreate;

    @JsonProperty("created_at")
    private String createdAt;

    @JsonProperty("repair_created_at")
    private String repairCreatedAt;

    @JsonProperty("user_update")
    private String userUpdate;

    @JsonProperty("update_at")
    private String updateAt;

    @JsonProperty("assigned_to")
    private AssigneeRepairRequestV1 assignedTo;

    @JsonProperty("assigned_at")
    private String assignedAt;

    @JsonProperty("insured")
    private InsuredRequest insured;

    @JsonProperty("insurance_company")
    private InsuranceCompanyCounterpartyRequest insuranceCompany;

    @JsonProperty("driver")
    private DriverRequest driver;

    @JsonProperty("vehicle")
    private VehicleRequest vehicle;

    @JsonProperty("is_cai_signed")
    private Boolean isCaiSigned;

    @JsonProperty("impact_point")
    private ImpactPointRequest impactPoint;

    @JsonProperty("responsible")
    private Boolean responsible;

    @JsonProperty("attachments")
    private List<AttachmentRequest> attachments;

    @JsonProperty("historicals")
    private List<HistoricalCounterpartyRequest> historicals;

    @JsonProperty("description")
    private String description;

    @JsonProperty("manager")
    private ManagerRequestV1 manager;

    @JsonProperty("canalization")
    private CanalizationRequest canalization;

    @JsonProperty("last_contact")
    private List<LastContactRequest> lastContact;

    @JsonProperty("eligibility")
    private Boolean eligibility;

    @JsonProperty("replacement_car")
    private Boolean replacementCar;

    @JsonProperty("replacement_plate")
    private String replacementPlate;

    @JsonProperty("policy_number")
    private String policyNumber;

    @JsonProperty("policy_beginning_validity")
    private String policyBeginningValidity;

    @JsonProperty("policy_end_validity")
    private String policyEndValidity;

    @JsonProperty("management_type")
    private ManagementTypeEnum managementType;

    @JsonProperty("asked_for_damages")
    private Boolean askedForDamages;

    @JsonProperty("legal_or_consultant")
    private Boolean legalOrConsultant;

    @JsonProperty("date_request_damages")
    private String dateRequestDamages;

    @JsonProperty("expiration_date")
    private String expirationDate;

    @JsonProperty("legal")
    private String legal;

    @JsonProperty("email_legal")
    private String emailLegal;

    @JsonProperty("is_ald")
    private Boolean isAld;

    @JsonProperty("is_read_msa")
    private Boolean isRead = false;


    public CounterpartyRequest() {
    }

    public CounterpartyRequest(String counterpartyId, VehicleTypeEnum type, RepairStatusEnum repairStatus, RepairProcedureEnum repairProcedure, String userCreate, String createdAt, String userUpdate, String updateAt, AssigneeRepairRequestV1 assignedTo, String assignedAt, InsuredRequest insured, InsuranceCompanyCounterpartyRequest insuranceCompany, DriverRequest driver, VehicleRequest vehicle, Boolean isCaiSigned, ImpactPointRequest impactPoint, Boolean responsible, List<AttachmentRequest> attachments, List<HistoricalCounterpartyRequest> historicals, String description, ManagerRequestV1 manager, CanalizationRequest canalization, List<LastContactRequest>lastContact, Boolean eligibility, String policyNumber, String policyBeginningValidity, String policyEndValidity, ManagementTypeEnum managementType, Boolean askedForDamages, Boolean legalOrConsultant, String dateRequestDamages, String expirationDate, String legal, String emailLegal) {
        this.counterpartyId = counterpartyId;
        this.type = type;
        this.repairStatus = repairStatus;
        this.repairProcedure = repairProcedure;
        this.userCreate = userCreate;
        this.createdAt = createdAt;
        this.userUpdate = userUpdate;
        this.updateAt = updateAt;
        this.assignedTo = assignedTo;
        this.assignedAt = assignedAt;
        this.insured = insured;
        this.insuranceCompany = insuranceCompany;
        this.driver = driver;
        this.vehicle = vehicle;
        this.isCaiSigned = isCaiSigned;
        this.impactPoint = impactPoint;
        this.responsible = responsible;
        if(attachments != null)
        {
            this.attachments = new ArrayList<>(attachments);
        }
        if(historicals != null)
        {
            this.historicals = new ArrayList<>(historicals);
        }
        this.description = description;
        this.manager = manager;
        this.canalization = canalization;
        if(lastContact != null)
        {
            this.lastContact = new ArrayList<>(lastContact);
        }
        this.eligibility = eligibility;
        this.policyNumber = policyNumber;
        this.policyBeginningValidity = policyBeginningValidity;
        this.policyEndValidity = policyEndValidity;
        this.managementType = managementType;
        this.askedForDamages = askedForDamages;
        this.legalOrConsultant = legalOrConsultant;
        this.dateRequestDamages = dateRequestDamages;
        this.expirationDate = expirationDate;
        this.legal = legal;
        this.emailLegal = emailLegal;
    }

    public String getReplacementPlate() {
        return replacementPlate;
    }

    public void setReplacementPlate(String replacementPlate) {
        this.replacementPlate = replacementPlate;
    }

    public String getMotivation() {
        return motivation;
    }

    public void setMotivation(String motivation) {
        this.motivation = motivation;
    }

    public String getRepairCreatedAt() {
        return repairCreatedAt;
    }

    public void setRepairCreatedAt(String repairCreatedAt) {
        this.repairCreatedAt = repairCreatedAt;
    }

    public Boolean getReplacementCar() {
        return replacementCar;
    }

    public void setReplacementCar(Boolean replacementCar) {
        this.replacementCar = replacementCar;
    }

    public Long getPracticeIdCounterparty() {
            return practiceIdCounterparty;
        }

    public void setPracticeIdCounterparty(Long practiceIdCounterparty) {
        this.practiceIdCounterparty = practiceIdCounterparty;
    }

    public Boolean getAld() {
        return isAld;
    }

    public void setAld(Boolean ald) {
        isAld = ald;
    }

    public ManagementTypeEnum getManagementType() {
        return managementType;
    }

    public void setManagementType(ManagementTypeEnum managementType) {
        this.managementType = managementType;
    }

    public Boolean getAskedForDamages() {
        return askedForDamages;
    }

    public void setAskedForDamages(Boolean askedForDamages) {
        this.askedForDamages = askedForDamages;
    }

    public Boolean getLegalOrConsultant() {
        return legalOrConsultant;
    }

    public void setLegalOrConsultant(Boolean legalOrConsultant) {
        this.legalOrConsultant = legalOrConsultant;
    }

    public String getDateRequestDamages() {
        return dateRequestDamages;
    }

    public void setDateRequestDamages(String dateRequestDamages) {
        this.dateRequestDamages = dateRequestDamages;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getLegal() {
        return legal;
    }

    public void setLegal(String legal) {
        this.legal = legal;
    }

    public String getEmailLegal() {
        return emailLegal;
    }

    public void setEmailLegal(String emailLegal) {
        this.emailLegal = emailLegal;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public String getPolicyBeginningValidity() {
        return policyBeginningValidity;
    }

    public void setPolicyBeginningValidity(String policyBeginningValidity) {
        this.policyBeginningValidity = policyBeginningValidity;
    }

    public String getPolicyEndValidity() {
        return policyEndValidity;
    }

    public void setPolicyEndValidity(String policyEndValidity) {
        this.policyEndValidity = policyEndValidity;
    }

    public String getCounterpartyId() {
        return counterpartyId;
    }

    public void setCounterpartyId(String counterpartyId) {
        this.counterpartyId = counterpartyId;
    }

    public VehicleTypeEnum getType() {
        return type;
    }

    public void setType(VehicleTypeEnum type) {
        this.type = type;
    }

    public RepairStatusEnum getRepairStatus() {
        return repairStatus;
    }

    public void setRepairStatus(RepairStatusEnum repairStatus) {
        this.repairStatus = repairStatus;
    }

    public InsuredRequest getInsured() {
        return insured;
    }

    public void setInsured(InsuredRequest insured) {
        this.insured = insured;
    }

    public InsuranceCompanyCounterpartyRequest getInsuranceCompany() {
        return insuranceCompany;
    }

    public void setInsuranceCompany(InsuranceCompanyCounterpartyRequest insuranceCompany) {
        this.insuranceCompany = insuranceCompany;
    }

    public DriverRequest getDriver() {
        return driver;
    }

    public void setDriver(DriverRequest driver) {
        this.driver = driver;
    }

    public VehicleRequest getVehicle() {
        return vehicle;
    }

    public void setVehicle(VehicleRequest vehicle) {
        this.vehicle = vehicle;
    }

    @JsonIgnore
    public Boolean getCaiSigned() {
        return isCaiSigned;
    }

    public void setCaiSigned(Boolean caiSigned) {
        if(caiSigned == null)
            caiSigned = false;
        isCaiSigned = caiSigned;
    }

    public ImpactPointRequest getImpactPoint() {
        return impactPoint;
    }

    public void setImpactPoint(ImpactPointRequest impactPoint) {
        this.impactPoint = impactPoint;
    }

    public Boolean getResponsible() {
        return responsible;
    }

    public void setResponsible(Boolean responsible) {
        if(responsible == null)
            responsible = false;
        this.responsible = responsible;
    }

    public Boolean getRead() {
        return isRead;
    }

    @JsonSetter
    public void setRead(Boolean read) {
        if(read != null)
            isRead = read;
    }

    public List<AttachmentRequest> getAttachments() {
        if(attachments == null){
            return null;
        }
        return new ArrayList<>(attachments);
    }

    public void setAttachments(List<AttachmentRequest> attachments) {
        if(attachments != null)
        {
            this.attachments = new ArrayList<>(attachments);
        } else {
            this.attachments = null;
        }
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public RepairProcedureEnum getRepairProcedure() {
        return repairProcedure;
    }

    public void setRepairProcedure(RepairProcedureEnum repairProcedure) {
        this.repairProcedure = repairProcedure;
    }

    public String getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(String userUpdate) {
        this.userUpdate = userUpdate;
    }

    public String getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(String updateAt) {
        this.updateAt = updateAt;
    }

    public AssigneeRepairRequestV1 getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(AssigneeRepairRequestV1 assignedTo) {
        this.assignedTo = assignedTo;
    }

    public String getAssignedAt() {
        return assignedAt;
    }

    public void setAssignedAt(String assignedAt) {
        this.assignedAt = assignedAt;
    }

    public List<HistoricalCounterpartyRequest> getHistoricals() {
        if(historicals == null){
            return null;
        }
        return new ArrayList<>(historicals);
    }

    public void setHistoricals(List<HistoricalCounterpartyRequest> historicals) {
        if(historicals != null)
        {
            this.historicals = new ArrayList<>(historicals);
        } else {
            this.historicals = null;
        }
    }

    public ManagerRequestV1 getManager() {
        return manager;
    }

    public void setManager(ManagerRequestV1 manager) {
        this.manager = manager;
    }

    public CanalizationRequest getCanalization() {
        return canalization;
    }

    public void setCanalization(CanalizationRequest canalization) {
        this.canalization = canalization;
    }

    public List<LastContactRequest> getLastContact() {
        if(lastContact == null){
            return null;
        }
        return new ArrayList<>(lastContact);
    }

    public void setLastContact(List<LastContactRequest> lastContact) {
        if(lastContact != null)
        {
            this.lastContact = new ArrayList<>(lastContact);
        } else {
            this.lastContact = null;
        }
    }

    public Boolean getEligibility() {
        return eligibility;
    }

    public void setEligibility(Boolean eligibility) {
        this.eligibility = eligibility;
    }

    @Override
    public String toString() {
        return "CounterpartyRequest{" +
                "counterpartyId='" + counterpartyId + '\'' +
                ", type=" + type +
                ", repairStatus=" + repairStatus +
                ", repairProcedure=" + repairProcedure +
                ", userCreate='" + userCreate + '\'' +
                ", createdAt=" + createdAt +
                ", userUpdate='" + userUpdate + '\'' +
                ", updateAt=" + updateAt +
                ", assignedTo='" + assignedTo + '\'' +
                ", assignedAt=" + assignedAt +
                ", insured=" + insured +
                ", insuranceCompany=" + insuranceCompany +
                ", driver=" + driver +
                ", vehicle=" + vehicle +
                ", isCaiSigned=" + isCaiSigned +
                ", impactPoint=" + impactPoint +
                ", responsible=" + responsible +
                ", attachments=" + attachments +
                ", historicals=" + historicals +
                ", description='" + description + '\'' +
                ", manager=" + manager +
                ", canalization=" + canalization +
                ", lastContact=" + lastContact +
                ", eligibility=" + eligibility +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CounterpartyRequest that = (CounterpartyRequest) o;
        return Objects.equals(getCounterpartyId(), that.getCounterpartyId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCounterpartyId());
    }
}

