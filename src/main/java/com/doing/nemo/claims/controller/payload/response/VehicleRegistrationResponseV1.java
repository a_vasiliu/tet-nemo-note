package com.doing.nemo.claims.controller.payload.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class VehicleRegistrationResponseV1 implements Serializable {
    private static final long serialVersionUID = 3593759439029258464L;
    @JsonProperty("historical")
    private VehicleRegistrationResponse historicalResponseList;

    public VehicleRegistrationResponseV1(VehicleRegistrationResponse historicalResponseList) {
        this.historicalResponseList = historicalResponseList;
    }

    public VehicleRegistrationResponse getHistoricalResponseList() {
        return historicalResponseList;
    }

    public void setHistoricalResponseList(VehicleRegistrationResponse historicalResponseList) {
        this.historicalResponseList = historicalResponseList;
    }
}
