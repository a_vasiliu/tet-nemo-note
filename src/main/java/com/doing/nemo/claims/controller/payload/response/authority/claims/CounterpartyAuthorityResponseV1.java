package com.doing.nemo.claims.controller.payload.response.authority.claims;

import com.doing.nemo.claims.entity.enumerated.DamagedEnum.VehicleEnum.VehicleAuthorityNatureEnum;
import com.doing.nemo.claims.entity.enumerated.DamagedEnum.VehicleEnum.VehicleNatureEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class CounterpartyAuthorityResponseV1 implements Serializable {

    /*@JsonProperty("id")
    private String id;*/

    @JsonProperty("counterpart_name")
    private String counterpartName;

    @JsonProperty("phone")
    private String phone;

    @JsonProperty("email")
    private String email;

    @JsonProperty("insurance_company")
    private String insuranceCompany;

    @JsonProperty("plate")
    private String plate;

    @JsonProperty("model")
    private String model;

    @JsonProperty("brand")
    private String brand;

    @JsonProperty("nature")
    private VehicleAuthorityNatureEnum nature;

    public CounterpartyAuthorityResponseV1() {
    }

    public CounterpartyAuthorityResponseV1(String counterpartName, String phone, String email, String insuranceCompany, String plate, String model, String brand, VehicleAuthorityNatureEnum nature) {
        this.counterpartName = counterpartName;
        this.phone = phone;
        this.email = email;
        this.insuranceCompany = insuranceCompany;
        this.plate = plate;
        this.model = model;
        this.brand = brand;
        this.nature = nature;
    }

    /*public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }*/

    public String getCounterpartName() {
        return counterpartName;
    }

    public void setCounterpartName(String counterpartName) {
        this.counterpartName = counterpartName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getInsuranceCompany() {
        return insuranceCompany;
    }

    public void setInsuranceCompany(String insuranceCompany) {
        this.insuranceCompany = insuranceCompany;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public VehicleAuthorityNatureEnum getNature() {
        return nature;
    }

    public void setNature(VehicleAuthorityNatureEnum nature) {
        this.nature = nature;
    }

    @Override
    public String toString() {
        return "AuthorityRepairResponseV1{" +
                "counterpartName='" + counterpartName + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", insuranceCompany='" + insuranceCompany + '\'' +
                ", plate='" + plate + '\'' +
                ", model='" + model + '\'' +
                ", brand='" + brand + '\'' +
                ", nature=" + nature +
                '}';
    }
}
