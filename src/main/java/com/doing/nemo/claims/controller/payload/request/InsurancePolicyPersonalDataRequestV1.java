package com.doing.nemo.claims.controller.payload.request;

import com.doing.nemo.claims.entity.settings.BrokerEntity;
import com.doing.nemo.claims.entity.settings.InsuranceManagerEntity;
import com.doing.nemo.claims.entity.settings.InsurancePolicyPersonalDataEntity;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class InsurancePolicyPersonalDataRequestV1 implements Serializable {

    @JsonProperty("type")
    private String type = "";

    @JsonProperty("description")
    private String description = "";

    @JsonProperty("insurance_company")
    private InsurancePolicyPersonalDataEntity insuranceCompany;

    @JsonProperty("number_policy")
    private Long numberPolicy;

    @JsonProperty("beginning_validity")
    private String beginningValidity;

    @JsonProperty("end_validity")
    private String endValidity;

    @JsonProperty("broker_id")
    private BrokerEntity broker;

    @JsonProperty("insurance_manager")
    private InsuranceManagerEntity insuranceManager;

    @JsonProperty("annotations")
    private String annotations = "";

    @JsonProperty("book_register")
    private Boolean bookRegister;

    @JsonProperty("book_register_code")
    private String bookRegisterCode = "";

    @JsonProperty("book_register_pai_code")
    private String bookRegisterPaiCode = "";

    @JsonProperty("crystals_fr")
    private String crystalsFr = "";

    @JsonProperty("furinc")
    private String furinc = "";

    @JsonProperty("massimal")
    private String massimal = "";

    @JsonProperty("furinc_uncover")
    private String furincUncover = "";

    @JsonProperty("kasko_fr")
    private String kaskoFr = "";

    @JsonProperty("crystals_max")
    private String crystalsMax = "";

    public InsurancePolicyPersonalDataRequestV1() {
    }

    public InsurancePolicyPersonalDataRequestV1(String type, String description, InsurancePolicyPersonalDataEntity insuranceCompany, Long numberPolicy, String beginningValidity, String endValidity, BrokerEntity broker, InsuranceManagerEntity insuranceManager, String annotations, Boolean bookRegister, String bookRegisterCode, String bookRegisterPaiCode, String crystalsFr, String furinc, String massimal, String furincUncover, String kaskoFr, String crystalsMax) {
        this.type = type;
        this.description = description;
        this.insuranceCompany = insuranceCompany;
        this.numberPolicy = numberPolicy;
        this.beginningValidity = beginningValidity;
        this.endValidity = endValidity;
        this.broker = broker;
        this.insuranceManager = insuranceManager;
        this.annotations = annotations;
        this.bookRegister = bookRegister;
        this.bookRegisterCode = bookRegisterCode;
        this.bookRegisterPaiCode = bookRegisterPaiCode;
        this.crystalsFr = crystalsFr;
        this.furinc = furinc;
        this.massimal = massimal;
        this.furincUncover = furincUncover;
        this.kaskoFr = kaskoFr;
        this.crystalsMax = crystalsMax;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public InsurancePolicyPersonalDataEntity getInsuranceCompany() {
        return insuranceCompany;
    }

    public void setInsuranceCompany(InsurancePolicyPersonalDataEntity insuranceCompany) {
        this.insuranceCompany = insuranceCompany;
    }

    public Long getNumberPolicy() {
        return numberPolicy;
    }

    public void setNumberPolicy(Long numberPolicy) {
        this.numberPolicy = numberPolicy;
    }

    public String getBeginningValidity() {
        return beginningValidity;
    }

    public void setBeginningValidity(String beginningValidity) {
        this.beginningValidity = beginningValidity;
    }

    public String getEndValidity() {
        return endValidity;
    }

    public void setEndValidity(String endValidity) {
        this.endValidity = endValidity;
    }

    public BrokerEntity getBroker() {
        return broker;
    }

    public void setBroker(BrokerEntity broker) {
        this.broker = broker;
    }

    public InsuranceManagerEntity getInsuranceManager() {
        return insuranceManager;
    }

    public void setInsuranceManager(InsuranceManagerEntity insuranceManager) {
        this.insuranceManager = insuranceManager;
    }

    public String getAnnotations() {
        return annotations;
    }

    public void setAnnotations(String annotations) {
        this.annotations = annotations;
    }

    public Boolean getBookRegister() {
        return bookRegister;
    }

    public void setBookRegister(Boolean bookRegister) {
        this.bookRegister = bookRegister;
    }

    public String getBookRegisterCode() {
        return bookRegisterCode;
    }

    public void setBookRegisterCode(String bookRegisterCode) {
        this.bookRegisterCode = bookRegisterCode;
    }

    public String getBookRegisterPaiCode() {
        return bookRegisterPaiCode;
    }

    public void setBookRegisterPaiCode(String bookRegisterPaiCode) {
        this.bookRegisterPaiCode = bookRegisterPaiCode;
    }

    public String getCrystalsFr() {
        return crystalsFr;
    }

    public void setCrystalsFr(String crystalsFr) {
        this.crystalsFr = crystalsFr;
    }

    public String getFurinc() {
        return furinc;
    }

    public void setFurinc(String furinc) {
        this.furinc = furinc;
    }

    public String getMassimal() {
        return massimal;
    }

    public void setMassimal(String massimal) {
        this.massimal = massimal;
    }

    public String getFurincUncover() {
        return furincUncover;
    }

    public void setFurincUncover(String furincUncover) {
        this.furincUncover = furincUncover;
    }

    public String getKaskoFr() {
        return kaskoFr;
    }

    public void setKaskoFr(String kaskoFr) {
        this.kaskoFr = kaskoFr;
    }

    public String getCrystalsMax() {
        return crystalsMax;
    }

    public void setCrystalsMax(String crystalsMax) {
        this.crystalsMax = crystalsMax;
    }
}
