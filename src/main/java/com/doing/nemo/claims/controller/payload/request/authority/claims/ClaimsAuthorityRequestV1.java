package com.doing.nemo.claims.controller.payload.request.authority.claims;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class ClaimsAuthorityRequestV1 implements Serializable {
    @JsonProperty("id")
    private String id;

    @JsonProperty("franchise_number")
    private Long franchiseNumber;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getFranchiseNumber() {
        return franchiseNumber;
    }

    public void setFranchiseNumber(Long franchiseNumber) {
        this.franchiseNumber = franchiseNumber;
    }

    @Override
    public String toString() {
        return "ClaimsAuthorityRequestV1{" +
                "id='" + id + '\'' +
                ", franchiseNumber=" + franchiseNumber +
                '}';
    }
}
