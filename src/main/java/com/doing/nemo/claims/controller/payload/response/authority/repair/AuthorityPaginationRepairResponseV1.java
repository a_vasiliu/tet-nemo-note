package com.doing.nemo.claims.controller.payload.response.authority.repair;

import com.doing.nemo.claims.controller.payload.response.damaged.ImpactPointResponse;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsFlowEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.doing.nemo.claims.entity.enumerated.RepairEnum.RepairStatusEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AuthorityPaginationRepairResponseV1 implements Serializable {

    @JsonProperty("id")
    private String id;

    @JsonProperty("practice_id")
    private Long practiceId;

    @JsonProperty("created_at")
    private String createdAt;

    @JsonProperty("claim_practice_id")
    private Long claimPracticeId;

    @JsonProperty("plate")
    private String plate;

    @JsonProperty("impact_point")
    private ImpactPointResponse impactPoint;

    @JsonProperty("status")
    private RepairStatusEnum status;

    @JsonProperty("authority_practice_linked")
    private List<String> authorityPracticeLinked;

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getPracticeId() {
        return practiceId;
    }

    public void setPracticeId(Long practiceId) {
        this.practiceId = practiceId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Long getClaimPracticeId() {
        return claimPracticeId;
    }

    public void setClaimPracticeId(Long claimPracticeId) {
        this.claimPracticeId = claimPracticeId;
    }

    public ImpactPointResponse getImpactPoint() {
        return impactPoint;
    }

    public void setImpactPoint(ImpactPointResponse impactPoint) {
        this.impactPoint = impactPoint;
    }

    public RepairStatusEnum getStatus() {
        return status;
    }

    public void setStatus(RepairStatusEnum status) {
        this.status = status;
    }

    public List<String> getAuthorityPracticeLinked() {
        if(authorityPracticeLinked == null){
            return null;
        }
        return new ArrayList<>(authorityPracticeLinked);
    }

    public void setAuthorityPracticeLinked(List<String> authorityPracticeLinked) {
        if(authorityPracticeLinked != null)
        {
            this.authorityPracticeLinked = new ArrayList<>(authorityPracticeLinked);
        } else {
            this.authorityPracticeLinked = null;
        }
    }
}
