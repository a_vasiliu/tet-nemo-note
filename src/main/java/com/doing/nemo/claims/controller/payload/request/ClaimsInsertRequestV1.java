package com.doing.nemo.claims.controller.payload.request;

import com.doing.nemo.claims.controller.payload.request.claims.*;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ClaimsInsertRequestV1 implements Serializable {

    @JsonProperty("complaint")
    private ComplaintRequest complaint;

    @JsonProperty("is_with_counterparty")
    private Boolean isWithCounterparty = false;

    @JsonProperty("counterparty")
    private List<CounterpartyRequest> counterparty;

    @JsonProperty("deponent")
    private List<DeponentRequest> deponentList;

    @JsonProperty("wounded")
    private List<WoundedRequest> woundedList;

    @JsonProperty("damaged")
    private DamagedRequest damaged;

    @JsonProperty("found_model")
    private FoundModelRequest foundModel;

    @JsonProperty("pai_comunication")
    private Boolean paiComunication = false;

    @JsonProperty("legal_comunication")
    private Boolean legalComunication = false;

    @JsonProperty("forced")
    private Boolean forced = false;

    @JsonProperty("in_evidence")
    private Boolean inEvidence = false;

    @JsonProperty("id_saleforce")
    private Long idSaleforce;

    @JsonProperty("cai_details")
    private CaiRequest caiRequest;

    @JsonProperty("with_continuation")
    private Boolean withContinuation = false;

    @JsonProperty("refund")
    private RefundRequest refund;

    @JsonProperty("metadata")
    private Map<String,Object> metadata;

    @JsonProperty("theft")
    private TheftRequestV1 theft;

    @JsonProperty("is_read_msa")
    private Boolean isRead = false;

    @JsonProperty("is_read_acclaims")
    private Boolean isReadAcclaims = false;


    @JsonProperty("is_authority_linkable")
    private Boolean isAuthorityLinkable;

    @JsonProperty("email_template_list")
    private List<EmailTemplateMessagingValidatedRequestV1> emailTemplateList;

    @JsonProperty("notes")
    private List<NoteRequest> notes;

    @JsonProperty("po_variation")
    private Boolean poVariation = false;

    @JsonProperty("is_complete_documentation")
    private Boolean isCompleteDocumentation ;

    @JsonProperty("motivation")
    private String motivation;

    public Boolean getCompleteDocumentation() {
        return isCompleteDocumentation;
    }

    public void setCompleteDocumentation(Boolean completeDocumentation) {
        isCompleteDocumentation = completeDocumentation;
    }

    public Boolean getReadAcclaims() {
        return isReadAcclaims;
    }
    @JsonSetter
    public void setReadAcclaims(Boolean readAcclaims) {
        if(readAcclaims != null)
        isReadAcclaims = readAcclaims;
    }

    public Boolean getLegalComunication() {
        return legalComunication;
    }

    public void setLegalComunication(Boolean legalComunication) {
        this.legalComunication = legalComunication;
    }

    public Boolean getPoVariation() {
        return poVariation;
    }

    public void setPoVariation(Boolean poVariation) {
        this.poVariation = poVariation;
    }

    public List<EmailTemplateMessagingValidatedRequestV1> getEmailTemplateList()
    {
        if(emailTemplateList == null){
            return null;
        }
        return new ArrayList<>(emailTemplateList) ;
    }

    public void setEmailTemplateList(List<EmailTemplateMessagingValidatedRequestV1> emailTemplateList) {
        if(emailTemplateList != null)
        {
            this.emailTemplateList = new ArrayList<>(emailTemplateList) ;
        }else {
            this.emailTemplateList = null;
        }
    }

    public Boolean getAuthorityLinkable() {
        return isAuthorityLinkable;
    }

    public void setAuthorityLinkable(Boolean authorityLinkable) {
        isAuthorityLinkable = authorityLinkable;
    }

    public TheftRequestV1 getTheft() {
        return theft;
    }

    public void setTheft(TheftRequestV1 theft) {
        this.theft = theft;
    }

    public ComplaintRequest getComplaint() {
        return complaint;
    }

    public void setComplaint(ComplaintRequest complaint) {
        this.complaint = complaint;
    }

    @JsonIgnore
    public Boolean getWithCounterparty() {
        return isWithCounterparty;
    }

    public void setWithCounterparty(Boolean withCounterparty) {
        isWithCounterparty = withCounterparty;
    }

    public List<CounterpartyRequest> getCounterparty() {
        if(counterparty == null){
            return null;
        }
        return new ArrayList<>(counterparty) ;
    }

    public void setCounterparty(List<CounterpartyRequest> counterparty) {
        if(counterparty != null)
        {
            this.counterparty = new ArrayList<>(counterparty) ;
        }else {
            this.counterparty = null;
        }
    }

    public List<DeponentRequest> getDeponentList() {
        if(deponentList == null){
            return null;
        }
        return new ArrayList<>(deponentList);
    }

    public void setDeponentList(List<DeponentRequest> deponentList) {
        if(deponentList != null)
        {
            this.deponentList = new ArrayList<>(deponentList);
        }else {
            this.deponentList = null;
        }
    }

    public List<WoundedRequest> getWoundedList() {
        if(woundedList == null){
            return null;
        }
        return new ArrayList<>(woundedList);
    }

    public void setWoundedList(List<WoundedRequest> woundedList) {
        if(woundedList != null)
        {
            this.woundedList = new ArrayList<>(woundedList);
        }else {
            this.woundedList = null;
        }
    }

    public DamagedRequest getDamaged() {
        return damaged;
    }

    public void setDamaged(DamagedRequest damaged) {
        this.damaged = damaged;
    }

    public FoundModelRequest getFoundModel() {
        return foundModel;
    }

    public void setFoundModel(FoundModelRequest foundModel) {
        this.foundModel = foundModel;
    }

    public Boolean getPaiComunication() {
        return paiComunication;
    }

    public void setPaiComunication(Boolean paiComunication) {
        this.paiComunication = paiComunication;
    }

    public Boolean getForced() {
        return forced;
    }

    public void setForced(Boolean forced) {
        this.forced = forced;
    }

    public Boolean getInEvidence() {
        return inEvidence;
    }

    public void setInEvidence(Boolean inEvidence) {
        this.inEvidence = inEvidence;
    }

    public Long getIdSaleforce() {
        return idSaleforce;
    }

    public void setIdSaleforce(Long idSaleforce) {
        this.idSaleforce = idSaleforce;
    }

    public CaiRequest getCaiRequest() {
        return caiRequest;
    }

    public void setCaiRequest(CaiRequest caiRequest) {
        this.caiRequest = caiRequest;
    }

    public Boolean getWithContinuation() {
        return withContinuation;
    }

    public void setWithContinuation(Boolean withContinuation) {
        this.withContinuation = withContinuation;
    }

    public RefundRequest getRefund() {
        return refund;
    }

    public void setRefund(RefundRequest refund) {
        this.refund = refund;
    }

    public Map<String,Object> getMetadata() {
        return metadata;
    }

    public void setMetadata(Map<String,Object> metadata) {
        this.metadata = metadata;
    }

    public List<NoteRequest> getNotes() {
        if(notes == null){
            return null;
        }
        return new ArrayList<>(notes);
    }

    public void setNotes(List<NoteRequest> notes) {
        if(notes != null){
            this.notes = new ArrayList<>(notes);
        }else {
            this.notes = null;
        }
    }

    public Boolean getRead() {
        return isRead;
    }

    @JsonSetter
    public void setRead(Boolean read) {
        if(read != null)
        isRead = read;
    }

    public String getMotivation() {
        return motivation;
    }

    public void setMotivation(String motivation) {
        this.motivation = motivation;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ClaimsInsertRequestV1{");
        sb.append("complaint=").append(complaint);
        sb.append(", isWithCounterparty=").append(isWithCounterparty);
        sb.append(", counterparty=").append(counterparty);
        sb.append(", deponentList=").append(deponentList);
        sb.append(", woundedList=").append(woundedList);
        sb.append(", damaged=").append(damaged);
        sb.append(", foundModel=").append(foundModel);
        sb.append(", paiComunication=").append(paiComunication);
        sb.append(", legalComunication=").append(legalComunication);
        sb.append(", forced=").append(forced);
        sb.append(", inEvidence=").append(inEvidence);
        sb.append(", idSaleforce=").append(idSaleforce);
        sb.append(", caiRequest=").append(caiRequest);
        sb.append(", withContinuation=").append(withContinuation);
        sb.append(", refund=").append(refund);
        sb.append(", metadata=").append(metadata);
        sb.append(", theft=").append(theft);
        sb.append(", isRead=").append(isRead);
        sb.append(", isReadAcclaims=").append(isReadAcclaims);
        sb.append(", isAuthorityLinkable=").append(isAuthorityLinkable);
        sb.append(", emailTemplateList=").append(emailTemplateList);
        sb.append(", notes=").append(notes);
        sb.append(", poVariation=").append(poVariation);
        sb.append(", isCompleteDocumentation=").append(isCompleteDocumentation);
        sb.append('}');
        return sb.toString();
    }

}
