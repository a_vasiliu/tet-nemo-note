package com.doing.nemo.claims.controller.payload.response;

import com.doing.nemo.claims.entity.enumerated.LawyerPaymentsStatusEnum;
import com.doing.nemo.claims.entity.enumerated.RefundEnum.RefundTypeEnum;
import com.doing.nemo.claims.entity.enumerated.RefundEnum.TypeEnum;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.time.Instant;
import java.util.UUID;

public class LawyerPaymentsResponseV1 implements Serializable {

    @JsonProperty( "id")
    private UUID id;

    //Targa
    @JsonProperty( "plate")
    private String plate;

    //Numero sinistro
    @JsonProperty( "practice_id")
    private Long practiceId;

    //Data sinistro
    @JsonProperty( "date_accident")
    private Instant dateAccident;

    //Danno
    @JsonProperty( "damage")
    private Double damage;

    //Fermo tecnico
    @JsonProperty(  "technical_shutdown")
    private Double technicalShutdown;

    //Onorari
    @JsonProperty( "fee")
    private Double fee;

    //Spese causa
    @JsonProperty( "expenses_suit")
    private Double expensesSuit;

    //Accredito/Saldo
    @JsonProperty( "acc_sale")
    private TypeEnum accSale;

    //Tipologia pagamento
    @JsonProperty( "paymentType")
    private RefundTypeEnum paymentType;

    //Importo
    @JsonProperty( "amount")
    private Double amount;

    //Numero Transazione
    @JsonProperty( "transaction_number")
    private String transactionNumber;

    //Banca
    @JsonProperty( "bank")
    private String bank;

    //Note
    @JsonProperty( "note")
    private String note;

    //Codice Avvocato
    @JsonProperty( "lawyer_code")
    private Long lawyerCode;

    @JsonProperty("status")
    private LawyerPaymentsStatusEnum status;

    public LawyerPaymentsResponseV1() {
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public Long getPracticeId() {
        return practiceId;
    }

    public void setPracticeId(Long practiceId) {
        this.practiceId = practiceId;
    }

    public Instant getDateAccident() {
        return dateAccident;
    }

    public void setDateAccident(Instant dateAccident) {
        this.dateAccident = dateAccident;
    }

    public Double getDamage() {
        return damage;
    }

    public void setDamage(Double damage) {
        this.damage = damage;
    }

    public Double getTechnicalShutdown() {
        return technicalShutdown;
    }

    public void setTechnicalShutdown(Double technicalShutdown) {
        this.technicalShutdown = technicalShutdown;
    }

    public Double getFee() {
        return fee;
    }

    public void setFee(Double fee) {
        this.fee = fee;
    }

    public Double getExpensesSuit() {
        return expensesSuit;
    }

    public void setExpensesSuit(Double expensesSuit) {
        this.expensesSuit = expensesSuit;
    }

    public TypeEnum getAccSale() {
        return accSale;
    }

    public void setAccSale(TypeEnum accSale) {
        this.accSale = accSale;
    }

    public RefundTypeEnum getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(RefundTypeEnum paymentType) {
        this.paymentType = paymentType;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getTransactionNumber() {
        return transactionNumber;
    }

    public void setTransactionNumber(String transactionNumber) {
        this.transactionNumber = transactionNumber;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Long getLawyerCode() {
        return lawyerCode;
    }

    public void setLawyerCode(Long lawyerCode) {
        this.lawyerCode = lawyerCode;
    }

    public LawyerPaymentsStatusEnum getStatus() {
        return status;
    }

    public void setStatus(LawyerPaymentsStatusEnum status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "LawyerPaymentsResponseV1{" +
                "id=" + id +
                ", plate='" + plate + '\'' +
                ", practiceId=" + practiceId +
                ", dateAccident=" + dateAccident +
                ", damage=" + damage +
                ", technicalShutdown=" + technicalShutdown +
                ", fee=" + fee +
                ", expensesSuit=" + expensesSuit +
                ", accSale='" + accSale + '\'' +
                ", paymentType=" + paymentType +
                ", amount=" + amount +
                ", transactionNumber='" + transactionNumber + '\'' +
                ", bank='" + bank + '\'' +
                ", note='" + note + '\'' +
                ", lawyerCode=" + lawyerCode +
                ", status=" + status +
                '}';
    }
}
