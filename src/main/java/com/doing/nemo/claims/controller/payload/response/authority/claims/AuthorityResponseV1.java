package com.doing.nemo.claims.controller.payload.response.authority.claims;

import com.doing.nemo.claims.controller.payload.response.HistoricalAuthorityResponseV1;
import com.doing.nemo.claims.entity.enumerated.AuthorityEnum.AuthorityEventTypeEnum;
import com.doing.nemo.claims.entity.enumerated.AuthorityEnum.AuthorityTypeEnum;
import com.doing.nemo.claims.entity.enumerated.WorkingStatusEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class AuthorityResponseV1 implements Serializable {

    @JsonProperty("is_invoiced")
    private Boolean isInvoiced;

    @JsonProperty("authority_dossier_id")
    private String authorityDossierId;

    @JsonProperty("authority_working_id")
    private String authorityWorkingId;

    @JsonProperty("working_number")
    private String workingNumber;

    @JsonProperty("authority_dossier_number")
    private String authorityDossierNumber;

    @JsonProperty("total")
    private Double total;

    @JsonProperty("accepting_date")
    private String acceptingDate;

    @JsonProperty("authorization_date")
    private String authorizationDate;

    @JsonProperty("rejection_date")
    private String rejectionDate;

    @JsonProperty("rejection")
    private Boolean rejection;

    @JsonProperty("note_rejection")
    private String noteRejection;

    @JsonProperty("working_created_at")
    private String workingCreatedAt;

    @JsonProperty("franchise_number")
    private Long numberFranchise;

    @JsonProperty("is_wreck")
    private Boolean isWreck;

    @JsonProperty("wreck_causal")
    private String wreckCasual;

    @JsonProperty("event_date")
    private String eventDate;

    @JsonProperty("event_type")
    private AuthorityEventTypeEnum eventType;

    @JsonProperty("po_details")
    private List<PODetailsResponseV1> poDetails;

    /*@JsonProperty("status")
    private AuthorityStatusEnum status;*/

    @JsonProperty("type")
    private AuthorityTypeEnum type;

    @JsonProperty("is_not_duplicate")
    private Boolean isNotDuplicate;

    @JsonProperty("oldest")
    private Boolean oldest;

    @JsonProperty("commodity_details")
    private CommodityDetailsResponseV1 commodityDetails;

    @JsonProperty("status")
    private WorkingStatusEnum workingStatus;

    @JsonProperty("user_details")
    private UserDetailsResponseV1 userDetails;

    @JsonProperty("NBV")
    private BigDecimal nbv;

    @JsonProperty("wreck_value")
    private String wreckValue;

    @JsonProperty("claims_linked_size")
    private Long claimsLinkedSize;

    @JsonProperty("historical")
    private List<HistoricalAuthorityResponseV1> historical;

    public String getNoteRejection() {
        return noteRejection;
    }

    public void setNoteRejection(String noteRejection) {
        this.noteRejection = noteRejection;
    }

    @JsonIgnore
    public Boolean getInvoiced() {
        return isInvoiced;
    }

    public void setInvoiced(Boolean invoiced) {
        isInvoiced = invoiced;
    }

    public Long getClaimsLinkedSize() {
        return claimsLinkedSize;
    }

    public void setClaimsLinkedSize(Long claimsLinkedSize) {
        this.claimsLinkedSize = claimsLinkedSize;
    }

    public BigDecimal getNbv() {
        return nbv;
    }

    public void setNbv(BigDecimal nbv) {
        this.nbv = nbv;
    }

    public String getWreckValue() {
        return wreckValue;
    }

    public void setWreckValue(String wreckValue) {
        this.wreckValue = wreckValue;
    }

    public UserDetailsResponseV1 getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(UserDetailsResponseV1 userDetails) {
        this.userDetails = userDetails;
    }

    public WorkingStatusEnum getWorkingStatus() {
        return workingStatus;
    }

    public void setWorkingStatus(WorkingStatusEnum workingStatus) {
        this.workingStatus = workingStatus;
    }

    public String getAuthorityDossierNumber() {
        return authorityDossierNumber;
    }

    public void setAuthorityDossierNumber(String authorityDossierNumber) {
        this.authorityDossierNumber = authorityDossierNumber;
    }

    public CommodityDetailsResponseV1 getCommodityDetails() {
        return commodityDetails;
    }

    public void setCommodityDetails(CommodityDetailsResponseV1 commodityDetails) {
        this.commodityDetails = commodityDetails;
    }

    @JsonIgnore
    public Boolean getNotDuplicate() {
        return isNotDuplicate;
    }

    public void setNotDuplicate(Boolean notDuplicate) {
        isNotDuplicate = notDuplicate;
    }

    public AuthorityTypeEnum getType() {
        return type;
    }

    public void setType(AuthorityTypeEnum type) {
        this.type = type;
    }

    public String getAuthorityDossierId() {
        return authorityDossierId;
    }

    public void setAuthorityDossierId(String authorityDossierId) {
        this.authorityDossierId = authorityDossierId;
    }

    public String getAuthorityWorkingId() {
        return authorityWorkingId;
    }

    public void setAuthorityWorkingId(String authorityWorkingId) {
        this.authorityWorkingId = authorityWorkingId;
    }

    public String getWorkingNumber() {
        return workingNumber;
    }

    public void setWorkingNumber(String workingNumber) {
        this.workingNumber = workingNumber;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public String getAcceptingDate() {
        return acceptingDate;
    }

    public void setAcceptingDate(String acceptingDate) {
        this.acceptingDate = acceptingDate;
    }

    public String getAuthorizationDate() {
        return authorizationDate;
    }

    public void setAuthorizationDate(String authorizationDate) {
        this.authorizationDate = authorizationDate;
    }

    public String getRejectionDate() {
        return rejectionDate;
    }

    public void setRejectionDate(String rejectionDate) {
        this.rejectionDate = rejectionDate;
    }

    public Boolean getRejection() {
        return rejection;
    }

    public void setRejection(Boolean rejection) {
        this.rejection = rejection;
    }

    public String getWorkingCreatedAt() {
        return workingCreatedAt;
    }

    public void setWorkingCreatedAt(String workingCreatedAt) {
        this.workingCreatedAt = workingCreatedAt;
    }

    public Long getNumberFranchise() {
        return numberFranchise;
    }

    public void setNumberFranchise(Long numberFranchise) {
        this.numberFranchise = numberFranchise;
    }

    @JsonIgnore
    public Boolean getWreck() {
        return isWreck;
    }

    public void setWreck(Boolean wreck) {
        isWreck = wreck;
    }

    public String getWreckCasual() {
        return wreckCasual;
    }

    public void setWreckCasual(String wreckCasual) {
        this.wreckCasual = wreckCasual;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public AuthorityEventTypeEnum getEventType() {
        return eventType;
    }

    public void setEventType(AuthorityEventTypeEnum eventType) {
        this.eventType = eventType;
    }

    public List<PODetailsResponseV1> getPoDetails() {
        if(poDetails == null){
            return null;
        }
        return new ArrayList<>(poDetails);
    }

    public void setPoDetails(List<PODetailsResponseV1> poDetails) {
        if(poDetails != null)
        {
            this.poDetails = new ArrayList<>(poDetails);
        } else {
            this.poDetails = null;
        }
    }

    public Boolean getOldest() {
        return oldest;
    }

    public void setOldest(Boolean oldest) {
        this.oldest = oldest;
    }

    public List<HistoricalAuthorityResponseV1> getHistorical() {
        if(historical == null){
            return null;
        }
        return new ArrayList<>(historical);
    }

    public void setHistorical(List<HistoricalAuthorityResponseV1> historical) {
        if(historical != null)
        {
            this.historical = new ArrayList<>(historical);
        } else {
            this.historical = null;
        }
    }

    @Override
    public String toString() {
        return "AuthorityResponseV1{" +
                "isInvoiced=" + isInvoiced +
                ", authorityDossierId='" + authorityDossierId + '\'' +
                ", authorityWorkingId='" + authorityWorkingId + '\'' +
                ", workingNumber='" + workingNumber + '\'' +
                ", authorityDossierNumber='" + authorityDossierNumber + '\'' +
                ", total=" + total +
                ", acceptingDate='" + acceptingDate + '\'' +
                ", authorizationDate='" + authorizationDate + '\'' +
                ", rejectionDate='" + rejectionDate + '\'' +
                ", rejection=" + rejection +
                ", workingCreatedAt='" + workingCreatedAt + '\'' +
                ", numberFranchise=" + numberFranchise +
                ", isWreck=" + isWreck +
                ", wreckCasual='" + wreckCasual + '\'' +
                ", eventDate='" + eventDate + '\'' +
                ", eventType='" + eventType + '\'' +
                ", poDetails=" + poDetails +
                ", type=" + type +
                ", isNotDuplicate=" + isNotDuplicate +
                ", oldest=" + oldest +
                ", commodityDetails=" + commodityDetails +
                ", workingStatus=" + workingStatus +
                ", userDetails=" + userDetails +
                ", nbv=" + nbv +
                ", wreckValue='" + wreckValue + '\'' +
                ", claimsLinkedSize=" + claimsLinkedSize +
                '}';
    }
}
