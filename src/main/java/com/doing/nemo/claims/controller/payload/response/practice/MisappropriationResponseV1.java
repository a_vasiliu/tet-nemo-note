package com.doing.nemo.claims.controller.payload.response.practice;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Date;

public class MisappropriationResponseV1 implements Serializable {

    private static final long serialVersionUID = 809173064960583663L;

    @JsonProperty("misappropriation_date")
    private Date misappropriationDate;

    @JsonProperty("misappropriation_id")
    private String misappropriationId;

    @JsonProperty("provider")
    private String provider;

    @JsonProperty("provider_code")
    private String providerCode;

    @JsonProperty("complaint_police")
    private Boolean complaintPolice;

    @JsonProperty("complaint_cc")
    private Boolean complaintCc;

    @JsonProperty("complaint_vvuu")
    private Boolean complaintVvuu;

    @JsonProperty("complaint_gdf")
    private Boolean complaintGdf;

    @JsonProperty("authority_data")
    private String authorityData;

    @JsonProperty("note")
    private String note;

    @JsonProperty("happened_abroad")
    private Boolean happenedAbroad;

    @JsonProperty("sub_rental")
    private String subRental;

    @JsonProperty("plate")
    private String plate;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Date getMisappropriationDate() {
        if(misappropriationDate == null){
            return null;
        }
        return (Date)misappropriationDate.clone();
    }

    public void setMisappropriationDate(Date misappropriationDate) {
        if(misappropriationDate != null){
            this.misappropriationDate = (Date)misappropriationDate.clone();
        } else {
            this.misappropriationDate = null;
        }
    }

    public String getMisappropriationId() {
        return misappropriationId;
    }

    public void setMisappropriationId(String misappropriationId) {
        this.misappropriationId = misappropriationId;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getProviderCode() {
        return providerCode;
    }

    public void setProviderCode(String providerCode) {
        this.providerCode = providerCode;
    }

    public Boolean getComplaintPolice() {
        return complaintPolice;
    }

    public void setComplaintPolice(Boolean complaintPolice) {
        this.complaintPolice = complaintPolice;
    }

    public Boolean getComplaintCc() {
        return complaintCc;
    }

    public void setComplaintCc(Boolean complaintCc) {
        this.complaintCc = complaintCc;
    }

    public Boolean getComplaintVvuu() {
        return complaintVvuu;
    }

    public void setComplaintVvuu(Boolean complaintVvuu) {
        this.complaintVvuu = complaintVvuu;
    }

    public Boolean getComplaintGdf() {
        return complaintGdf;
    }

    public void setComplaintGdf(Boolean complaintGdf) {
        this.complaintGdf = complaintGdf;
    }

    public String getAuthorityData() {
        return authorityData;
    }

    public void setAuthorityData(String authorityData) {
        this.authorityData = authorityData;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Boolean getHappenedAbroad() {
        return happenedAbroad;
    }

    public void setHappenedAbroad(Boolean happenedAbroad) {
        this.happenedAbroad = happenedAbroad;
    }

    public String getSubRental() {
        return subRental;
    }

    public void setSubRental(String subRental) {
        this.subRental = subRental;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }


    @Override
    public String toString() {
        return "Misappropriation{" +
                "misappropriationDate=" + misappropriationDate +
                ", misappropriationId='" + misappropriationId + '\'' +
                ", provider='" + provider + '\'' +
                ", providerCode='" + providerCode + '\'' +
                ", complaintPolice=" + complaintPolice +
                ", complaintCc=" + complaintCc +
                ", complaintVvuu=" + complaintVvuu +
                ", complaintGdf=" + complaintGdf +
                ", authorityData='" + authorityData + '\'' +
                ", note='" + note + '\'' +
                ", happenedAbroad=" + happenedAbroad +
                ", subRental='" + subRental + '\'' +
                ", plate='" + plate + '\'' +
                '}';
    }


}
