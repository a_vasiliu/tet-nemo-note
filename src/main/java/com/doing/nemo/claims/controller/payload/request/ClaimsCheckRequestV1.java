package com.doing.nemo.claims.controller.payload.request;

import com.doing.nemo.claims.controller.payload.request.claims.ComplaintRequest;
import com.doing.nemo.claims.controller.payload.request.claims.DamagedRequest;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ClaimsCheckRequestV1 implements Serializable {

    @JsonProperty("status")
    private ClaimsStatusEnum status;

    @JsonProperty("complaint")
    private ComplaintRequest complaint;

    @JsonProperty("counterparty")
    private List<CounterpartyRequest> counterparty;

    @JsonProperty("damaged")
    private DamagedRequest damaged;


    public ClaimsStatusEnum getStatus() {
        return status;
    }

    public void setStatus(ClaimsStatusEnum status) {
        this.status = status;
    }

    public ComplaintRequest getComplaint() {
        return complaint;
    }

    public void setComplaint(ComplaintRequest complaint) {
        this.complaint = complaint;
    }

    public List<CounterpartyRequest> getCounterparty() {
        if(counterparty == null)
        {
            return null;
        }
        return new ArrayList<>(counterparty);
    }

    public void setCounterparty(List<CounterpartyRequest> counterparty) {
        if(counterparty != null)
        {
            this.counterparty = new ArrayList<>(counterparty);
        }else {
            this.counterparty = null;
        }
    }

    public DamagedRequest getDamaged() {
        return damaged;
    }

    public void setDamaged(DamagedRequest damaged) {
        this.damaged = damaged;
    }
}
