package com.doing.nemo.claims.controller.payload.response;

import com.doing.nemo.claims.entity.settings.DmSystemsEntity;
import com.doing.nemo.claims.entity.settings.NotificationTypeEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.UUID;

public class ExportNotificationTypeResponseV1 implements Serializable {

    @JsonProperty("id")
    private UUID id;

    @JsonProperty("notification_type")
    private NotificationTypeEntity notificationType;

    @JsonProperty("dm_system_id")
    private DmSystemsEntity dmSystem;

    @JsonProperty("code")
    private String code;

    @JsonProperty("description")
    private String description;

    @JsonProperty("is_active")
    private Boolean isActive;

    public ExportNotificationTypeResponseV1() {
    }

    public ExportNotificationTypeResponseV1(UUID id, NotificationTypeEntity notificationType, DmSystemsEntity dmSystem, String code, String description, Boolean isActive) {
        this.id = id;
        this.notificationType = notificationType;
        this.dmSystem = dmSystem;
        this.code = code;
        this.description = description;
        this.isActive = isActive;
    }

    public NotificationTypeEntity getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(NotificationTypeEntity notificationType) {
        this.notificationType = notificationType;
    }

    public DmSystemsEntity getDmSystem() {
        return dmSystem;
    }

    public void setDmSystem(DmSystemsEntity dmSystem) {
        this.dmSystem = dmSystem;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    @JsonIgnore
    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    @Override
    public String toString() {
        return "ExportNotificationTypeResponseV1{" +
                "id=" + id +
                ", notificationType=" + notificationType +
                ", dmSystem=" + dmSystem +
                ", code='" + code + '\'' +
                ", description='" + description + '\'' +
                ", isActive=" + isActive +
                '}';
    }
}
