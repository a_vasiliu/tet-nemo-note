package com.doing.nemo.claims.controller.payload.request.myald;


import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class MyAldLinkObjectRequestV1 implements Serializable {
    @JsonProperty("claim_id")
    private String claimId;
    @JsonProperty("claim_status")
    private String claimStatus;
    @JsonProperty("user_created")
    private Integer userCreated;
    @JsonProperty("ID_customer")
    private Long idCustomer;

    public String getClaimId() {
        return claimId;
    }

    public void setClaimId(String claimId) {
        this.claimId = claimId;
    }

    public String getClaimStatus() {
        return claimStatus;
    }

    public void setClaimStatus(String claimStatus) {
        this.claimStatus = claimStatus;
    }

    public Integer getUserCreated() {
        return userCreated;
    }

    public void setUserCreated(Integer userCreated) {
        this.userCreated = userCreated;
    }

    public Long getIdCustomer() {
        return idCustomer;
    }

    public void setIdCustomer(Long idCustomer) {
        this.idCustomer = idCustomer;
    }

    @Override
    public String toString() {
        return "MyAldLinkObjectRequestV1{" +
                "claimId='" + claimId + '\'' +
                ", claimStatus='" + claimStatus + '\'' +
                ", userCreated=" + userCreated +
                ", idCustomer=" + idCustomer +
                '}';
    }
}
