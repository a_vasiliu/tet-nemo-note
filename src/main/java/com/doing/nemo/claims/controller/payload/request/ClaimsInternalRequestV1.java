package com.doing.nemo.claims.controller.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ClaimsInternalRequestV1 implements Serializable {

    @JsonProperty("claim")
    private ClaimsInsertRequestV1 claims;

    @JsonProperty("email_template_list")
    private List<EmailTemplateMessagingValidatedRequestV1> emailTemplateList;


    public ClaimsInsertRequestV1 getClaims() {
        return claims;
    }

    public void setClaims(ClaimsInsertRequestV1 claims) {
        this.claims = claims;
    }

    public List<EmailTemplateMessagingValidatedRequestV1> getEmailTemplateList() {
        if(emailTemplateList == null){
            return null;
        }
        return new ArrayList<>(emailTemplateList) ;
    }

    public void setEmailTemplateList(List<EmailTemplateMessagingValidatedRequestV1> emailTemplateList) {
        if(emailTemplateList != null)
        {
            this.emailTemplateList =  new ArrayList<>(emailTemplateList) ;
        } else {
            this.emailTemplateList = null;
        }
    }
}
