package com.doing.nemo.claims.controller.payload.request;

import com.doing.nemo.claims.entity.enumerated.ClaimsRepairEnum;
import com.doing.nemo.claims.entity.enumerated.EventTypeEnum;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.UUID;

public class EventTypeRequestV1 implements Serializable {
    @JsonProperty("id")
    private UUID id;

    @NotNull
    @JsonProperty("description")
    private String description;

    @NotNull
    @JsonProperty("event_type")
    private EventTypeEnum eventType;

    @NotNull
    @JsonProperty("create_attachments")
    private Boolean createAttachments;

    @JsonProperty("catalog")
    private String catalog;

    @JsonProperty("attachments_type")
    private String attachmentsType;

    @JsonProperty("only_last")
    private Boolean onlyLast;

    @JsonProperty("is_active")
    private Boolean isActive = true;

    @JsonProperty("type_complaint")
    private ClaimsRepairEnum typeComplaint;

    public EventTypeRequestV1(UUID id, @NotNull String description, @NotNull EventTypeEnum eventType, @NotNull Boolean createAttachments, String catalog, String attachmentsType, Boolean onlyLast, Boolean isActive, ClaimsRepairEnum typeComplaint) {
        this.id = id;
        this.description = description;
        this.eventType = eventType;
        this.createAttachments = createAttachments;
        this.catalog = catalog;
        this.attachmentsType = attachmentsType;
        this.onlyLast = onlyLast;
        this.isActive = isActive;
        this.typeComplaint = typeComplaint;
    }

    public ClaimsRepairEnum getTypeComplaint() {
        return typeComplaint;
    }

    public void setTypeComplaint(ClaimsRepairEnum typeComplaint) {
        this.typeComplaint = typeComplaint;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getCreateAttachments() {
        return createAttachments;
    }

    public void setCreateAttachments(Boolean createAttachments) {
        this.createAttachments = createAttachments;
    }

    public String getCatalog() {
        return catalog;
    }

    public void setCatalog(String catalog) {
        this.catalog = catalog;
    }

    public String getAttachmentsType() {
        return attachmentsType;
    }

    public void setAttachmentsType(String attachmentsType) {
        this.attachmentsType = attachmentsType;
    }

    public Boolean getOnlyLast() {
        return onlyLast;
    }

    public void setOnlyLast(Boolean onlyLast) {
        this.onlyLast = onlyLast;
    }

    public Boolean getActive() {
        return isActive;
    }

    @JsonSetter
    public void setActive(Boolean active) {
        if (active != null)
            isActive = active;
    }

    public EventTypeEnum getEventType() {
        return eventType;
    }

    public void setEventType(EventTypeEnum eventType) {
        this.eventType = eventType;
    }

    @Override
    public String toString() {
        return "EventTypeRequestV1{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", eventType=" + eventType +
                ", createAttachments=" + createAttachments +
                ", catalog='" + catalog + '\'' +
                ", attachmentsType='" + attachmentsType + '\'' +
                ", onlyLast=" + onlyLast +
                ", isActive=" + isActive +
                '}';
    }
}
