package com.doing.nemo.claims.controller.payload.response;

import com.doing.nemo.claims.entity.enumerated.GoLiveSystemEnum;
import com.doing.nemo.claims.entity.enumerated.RiskEnum;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.UUID;

public class GoLiveMSAResponseV1 implements Serializable {

    @JsonProperty("system")
    private GoLiveSystemEnum system;

    public GoLiveMSAResponseV1(GoLiveSystemEnum system) {
        this.system = system;
    }

    public GoLiveSystemEnum getSystem() {
        return system;
    }

    public void setSystem(GoLiveSystemEnum system) {
        this.system = system;
    }

    @Override
    public String toString() {
        return "GoLiveMSAResponseV1{" +
                "system=" + system +
                '}';
    }
}
