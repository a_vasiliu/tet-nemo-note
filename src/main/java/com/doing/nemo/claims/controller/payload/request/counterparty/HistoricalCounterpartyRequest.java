package com.doing.nemo.claims.controller.payload.request.counterparty;

import com.doing.nemo.claims.entity.enumerated.EventTypeEnum;
import com.doing.nemo.claims.entity.enumerated.RepairEnum.RepairStatusEnum;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class HistoricalCounterpartyRequest implements Serializable {
    @JsonProperty("old_status")
    private RepairStatusEnum statusEntityOld;

    @JsonProperty("new_status")
    private RepairStatusEnum statusEntityNew;

    @JsonProperty("description")
    private String description;

    @JsonProperty("update_at")
    private String updateAt;

    @JsonProperty("user_id")
    private String userId;

    @JsonProperty("user_name")
    private String userName;

    @JsonProperty("comunication_description")
    private String comunicationDescription;

    @JsonProperty("event_type")
    private EventTypeEnum eventType;

    public HistoricalCounterpartyRequest() {
    }

    public HistoricalCounterpartyRequest(RepairStatusEnum statusEntityOld, RepairStatusEnum statusEntityNew, String description, String updateAt, String userId, String comunicationDescription, EventTypeEnum eventType) {
        this.statusEntityOld = statusEntityOld;
        this.statusEntityNew = statusEntityNew;
        this.description = description;
        this.updateAt = updateAt;
        this.userId = userId;
        this.comunicationDescription = comunicationDescription;
        this.eventType = eventType;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public RepairStatusEnum getStatusEntityOld() {
        return statusEntityOld;
    }

    public void setStatusEntityOld(RepairStatusEnum statusEntityOld) {
        this.statusEntityOld = statusEntityOld;
    }

    public RepairStatusEnum getStatusEntityNew() {
        return statusEntityNew;
    }

    public void setStatusEntityNew(RepairStatusEnum statusEntityNew) {
        this.statusEntityNew = statusEntityNew;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(String updateAt) {
        this.updateAt = updateAt;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getComunicationDescription() {
        return comunicationDescription;
    }

    public void setComunicationDescription(String comunicationDescription) {
        this.comunicationDescription = comunicationDescription;
    }

    public EventTypeEnum getEventType() {
        return eventType;
    }

    public void setEventType(EventTypeEnum eventType) {
        this.eventType = eventType;
    }

    @Override
    public String toString() {
        return "HistoricalCounterpartyRequest{" +
                "statusEntityOld=" + statusEntityOld +
                ", statusEntityNew=" + statusEntityNew +
                ", description='" + description + '\'' +
                ", updateAt='" + updateAt + '\'' +
                ", userId='" + userId + '\'' +
                ", userName='" + userName + '\'' +
                ", comunicationDescription='" + comunicationDescription + '\'' +
                ", eventType=" + eventType +
                '}';
    }
}
