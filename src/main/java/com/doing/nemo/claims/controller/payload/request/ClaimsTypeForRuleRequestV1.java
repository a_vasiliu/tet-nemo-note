package com.doing.nemo.claims.controller.payload.request;

import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.UUID;

public class ClaimsTypeForRuleRequestV1 implements Serializable {

    @JsonProperty("id")
    @NotNull
    private UUID id;

    @JsonProperty("type")
    @NotNull
    private String type;

    @JsonProperty("claims_accident_type")
    @NotNull
    private DataAccidentTypeAccidentEnum claimsAccidentType;


    public ClaimsTypeForRuleRequestV1(@NotNull UUID id,@NotNull String type,@NotNull DataAccidentTypeAccidentEnum claimsAccidentType) {
        this.id = id;
        this.type = type;
        this.claimsAccidentType = claimsAccidentType;
    }


    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public DataAccidentTypeAccidentEnum getClaimsAccidentType() {
        return claimsAccidentType;
    }

    public void setClaimsAccidentType(DataAccidentTypeAccidentEnum claimsAccidentType) {
        this.claimsAccidentType = claimsAccidentType;
    }

    @Override
    public String toString() {
        return "ClaimsTypeForRuleRequestV1{" +
                "id=" + id +
                ", type='" + type + '\'' +
                ", claimsAccidentType=" + claimsAccidentType +
                '}';
    }
}
