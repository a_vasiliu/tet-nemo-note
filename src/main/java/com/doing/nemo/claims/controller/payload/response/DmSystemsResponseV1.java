package com.doing.nemo.claims.controller.payload.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.UUID;

public class DmSystemsResponseV1 implements Serializable {

    @JsonProperty("id")
    private UUID id;

    @JsonProperty("system_name")
    private String systemName;

    public DmSystemsResponseV1() {
    }

    public DmSystemsResponseV1(UUID id, String systemName) {
        this.id = id;
        this.systemName = systemName;
    }

    public String getSystemName() {
        return systemName;
    }

    public void setSystemName(String systemName) {
        this.systemName = systemName;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "DmSystemsResponseV1{" +
                "systemName='" + systemName + '\'' +
                '}';
    }
}
