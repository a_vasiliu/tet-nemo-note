package com.doing.nemo.claims.controller.payload.response;

import com.doing.nemo.claims.entity.enumerated.DTO.PageStats;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class DataManagementImportResponsePaginationV1 implements Serializable {

    private PageStats stats;
    private List<DataManagementImportResponseV1> items;

    public DataManagementImportResponsePaginationV1() {
    }

    public DataManagementImportResponsePaginationV1(PageStats stats, List<DataManagementImportResponseV1> items) {
        this.stats = stats;
        if(items != null)
        {
            this.items = new ArrayList<>(items);
        }
    }

    public PageStats getStats() {
        return stats;
    }

    public void setStats(PageStats stats) {
        this.stats = stats;
    }

    public List<DataManagementImportResponseV1> getItems() {
        if(items == null){
            return null;
        }
        return new ArrayList<>(items);
    }

    public void setItems(List<DataManagementImportResponseV1> items) {
        if(items != null)
        {
            this.items = new ArrayList<>(items);
        } else {
            this.items = null;
        }
    }

}
