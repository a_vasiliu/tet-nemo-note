package com.doing.nemo.claims.controller.payload.response;


import com.doing.nemo.claims.entity.enumerated.DTO.PageStats;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GenericAttachmentResponsePaginationV1 implements Serializable {

    @JsonProperty("stats")
    private PageStats stats;

    @JsonProperty("items")
    private List<GenericAttachmentResponseV1> items;

    public GenericAttachmentResponsePaginationV1() {
    }

    public GenericAttachmentResponsePaginationV1(PageStats stats, List<GenericAttachmentResponseV1> items) {
        this.stats = stats;
        if(items != null)
        {
            this.items = new ArrayList<>(items);
        }
    }

    public PageStats getStats() {
        return stats;
    }

    public void setStats(PageStats stats) {
        this.stats = stats;
    }

    public List<GenericAttachmentResponseV1> getItems() {
        if(items == null){
            return null;
        }
        return new ArrayList<>(items);
    }

    public void setItems(List<GenericAttachmentResponseV1> items) {
        if(items != null)
        {
            this.items = new ArrayList<>(items);
        } else {
            this.items = null;
        }
    }

    @Override
    public String toString() {
        return "GenericAttachmentResponsePaginationV1{" +
                "stats=" + stats +
                ", items=" + items +
                '}';
    }
}
