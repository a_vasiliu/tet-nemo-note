package com.doing.nemo.claims.controller;

import com.doing.nemo.claims.adapter.DataManagementExportAdapter;
import com.doing.nemo.claims.controller.payload.response.DataManagementExportResponsePaginationV1;
import com.doing.nemo.claims.controller.payload.response.DataManagementExportResponseV1;
import com.doing.nemo.claims.entity.settings.DataManagementExportEntity;
import com.doing.nemo.claims.repository.DataManagementExportRepository;
import com.doing.nemo.claims.repository.DataManagementExportRepositoryV1;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/v1")
public class ExportController {

    @Autowired
    private DataManagementExportRepository dataManagementExportRepository;

    @Autowired
    private DataManagementExportRepositoryV1 dataManagementExportRepositoryV1;

    @Autowired
    private DataManagementExportAdapter dataManagementExportAdapter;


    @ApiOperation(value="Data managment export pagination", notes = "")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = DataManagementExportResponsePaginationV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @GetMapping(value="/datamanagmentexport", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<DataManagementExportResponsePaginationV1> searchDataManagmentImport(
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @RequestParam(value = "page_size", defaultValue = "20") Integer pageSize,
            @RequestParam(value = "type_export", defaultValue = "FILE EXPORT") String typeExport
    ){

        List<DataManagementExportEntity> listDataImport = dataManagementExportRepositoryV1.findPaginationDataManagementExport(page, pageSize, typeExport);
        Long count = dataManagementExportRepositoryV1.countPaginationDataManagementExport(typeExport);

        DataManagementExportResponsePaginationV1 dataManagementImportPaginationResponseV1 = DataManagementExportAdapter.adptPagination(listDataImport, page, pageSize, count);

        return new ResponseEntity<>(dataManagementImportPaginationResponseV1, HttpStatus.OK);
    }

    @ApiOperation(value="Data managment export by UUID", notes = "")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = DataManagementExportResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @GetMapping(value="/datamanagmentexport/{UUID_DATAEXPORT}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<DataManagementExportResponseV1> getDataManagmentExportById(
            @ApiParam(value = "UUID that identifies the data management export", required = true)
            @PathVariable(name = "UUID_DATAEXPORT") String dataImportId
    ){

        DataManagementExportResponseV1 searchDataExportResponseV1 = DataManagementExportAdapter.adptDataManagementExportToDataManagementExportResponse(dataManagementExportRepository.getOne(UUID.fromString(dataImportId)));

        return new ResponseEntity<>(searchDataExportResponseV1, HttpStatus.OK);
    }



}
