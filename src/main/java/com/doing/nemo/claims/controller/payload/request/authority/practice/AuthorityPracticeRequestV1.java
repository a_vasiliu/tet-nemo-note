package com.doing.nemo.claims.controller.payload.request.authority.practice;

import com.doing.nemo.claims.controller.payload.request.IdRequestV1;
import com.doing.nemo.claims.controller.payload.request.authority.claims.CommodityDetailsRequestV1;
import com.doing.nemo.claims.entity.enumerated.AuthorityEnum.AdministrativePracticeTypeEnum;
import com.doing.nemo.claims.entity.enumerated.AuthorityEnum.AuthorityPracticeTypeEnum;
import com.doing.nemo.claims.entity.enumerated.AuthorityEnum.AuthorityWorkingTypeEnum;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AuthorityPracticeRequestV1 implements Serializable {


    @JsonProperty("dossier_id")
    private String authorityDossierId;

    @JsonProperty("dossier_number")
    private String authorityDossierNumber;

    @JsonProperty("working_id")
    private String authorityWorkingId;

    @JsonProperty("working_number")
    private String workingNumber;

    @JsonProperty("practice_list")
    private List<IdRequestV1> practiceList;

    @JsonProperty("plate")
    private String plate;

    @JsonProperty("type")
    private List<AuthorityPracticeTypeEnum> type;

    @JsonProperty("commodity_details")
    private CommodityDetailsRequestV1 commodityDetails;

    @JsonProperty("working_type")
    private AuthorityWorkingTypeEnum workingType;

    public AuthorityWorkingTypeEnum getWorkingType() {
        return workingType;
    }

    public void setWorkingType(AuthorityWorkingTypeEnum workingType) {
        this.workingType = workingType;
    }

    public String getAuthorityDossierId() {
        return authorityDossierId;
    }

    public void setAuthorityDossierId(String authorityDossierId) {
        this.authorityDossierId = authorityDossierId;
    }

    public String getAuthorityDossierNumber() {
        return authorityDossierNumber;
    }

    public void setAuthorityDossierNumber(String authorityDossierNumber) {
        this.authorityDossierNumber = authorityDossierNumber;
    }

    public String getAuthorityWorkingId() {
        return authorityWorkingId;
    }

    public void setAuthorityWorkingId(String authorityWorkingId) {
        this.authorityWorkingId = authorityWorkingId;
    }

    public String getWorkingNumber() {
        return workingNumber;
    }

    public void setWorkingNumber(String workingNumber) {
        this.workingNumber = workingNumber;
    }

    public List<IdRequestV1> getPracticeList() {
        if(practiceList == null){
            return null;
        }
        return new ArrayList<>(practiceList);
    }

    public void setPracticeList(List<IdRequestV1> practiceList) {
        if(practiceList != null)
        {
            this.practiceList = new ArrayList<>(practiceList);
        } else {
            this.practiceList = null;
        }
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public List<AuthorityPracticeTypeEnum> getType() {
        if(type == null){
            return null;
        }
        return new ArrayList<>(type);
    }

    public void setType(List<AuthorityPracticeTypeEnum> type) {
        if(type != null)
        {
            this.type = new ArrayList<>(type);
        } else {
            this.type = null;
        }
    }

    public CommodityDetailsRequestV1 getCommodityDetails() {
        return commodityDetails;
    }

    public void setCommodityDetails(CommodityDetailsRequestV1 commodityDetails) {
        this.commodityDetails = commodityDetails;
    }


    @Override
    public String toString() {
        return "AuthorityPracticeRequestV1{" +
                "authorityDossierId='" + authorityDossierId + '\'' +
                ", authorityDossierNumber='" + authorityDossierNumber + '\'' +
                ", authorityWorkingId='" + authorityWorkingId + '\'' +
                ", workingNumber='" + workingNumber + '\'' +
                ", practiceList=" + practiceList +
                ", plate='" + plate + '\'' +
                ", type=" + type +
                ", commodityDetails=" + commodityDetails +
                '}';
    }
}
