package com.doing.nemo.claims.controller.payload.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Map;

public class ClaimsResponseStatsEvidenceV2 implements Serializable {

    @JsonProperty("stats")
    private StatsDashboardV1 stats = new StatsDashboardV1();

    @JsonProperty("status")
    private Map<String, StatusDashboardElementV1> status;

    public StatsDashboardV1 getStats() {
        return stats;
    }

    public void setStats(StatsDashboardV1 stats) {
        this.stats = stats;
    }

    public Map<String, StatusDashboardElementV1> getStatus() {
        return status;
    }

    public void setStatus(Map<String, StatusDashboardElementV1> status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "ClaimsResponseStatsEvidenceV2{" +
                "stats=" + stats +
                ", status=" + status +
                '}';
    }
}
