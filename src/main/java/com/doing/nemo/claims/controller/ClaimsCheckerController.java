package com.doing.nemo.claims.controller;


import com.doing.nemo.claims.adapter.ClaimsAdapter;
import com.doing.nemo.claims.controller.payload.request.ClaimsCheckRequestV1;
import com.doing.nemo.claims.controller.payload.response.*;
import com.doing.nemo.claims.controller.payload.response.authority.claims.VehicleStatusAuthorityResponseV1;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.enumerated.AuthorityEnum.AuthorityVehicleStatusEnum;
import com.doing.nemo.claims.service.AuthorityService;
import com.doing.nemo.claims.service.ClaimsCheckerService;
import com.doing.nemo.claims.service.ClaimsPendingService;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.exception.InternalException;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/v1")
@Api("ClaimsChecker")
public class ClaimsCheckerController {

    private static Logger LOGGER = LoggerFactory.getLogger(ClaimsCheckerController.class);

    @Autowired
    private ClaimsCheckerService claimsCheckerService;

    @Autowired
    private AuthorityService authorityService;
    @Autowired
    private ClaimsPendingService claimsPendingService;

    //REFACTOR
    //prova checkClaimsDuplicate
    /*INSERIMENTO CLAIMS*/
    @GetMapping(value = "/duplicate/{plate}/{data_accident}",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Recover Claim", notes = "Returns a Claim using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ClaimsResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ClaimsCheckerResponseV1> checkClaimsDuplicate(
            @PathVariable("plate") String plate,
            @PathVariable("data_accident") String dateAccident,
            @RequestParam(value = "type_accident", required = false) String typeAccident
    ){

        return new ResponseEntity<>(claimsCheckerService.checkClaimsDuplicate(plate,dateAccident, typeAccident, null), HttpStatus.OK);
    }



    //REFACTOR
    //prova checkClaimsDuplicate
    /*VALIDAZIONE CLAIMS*/
    @GetMapping(value = "/duplicate/validation/{ID_CLAIMS}",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Recover Claim", notes = "Returns a Claim using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ClaimsResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ClaimsCheckerResponseV1> checkClaimsDuplicateInValidation(
            @PathVariable("ID_CLAIMS") String idClaims
    ){
        return new ResponseEntity<>(claimsCheckerService.checkClaimsInValidation(idClaims), HttpStatus.OK);
        //claimsCheckerService.checkClaimsInValidation(idClaims);
    }



    //REFACTOR
    @GetMapping(value = "/check/counterparty",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Recover Claim", notes = "Returns a Claim using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ClaimsResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ClaimsCheckerResponseV1> checkCountpartyWarning(
            @RequestParam(value = "driver_plate", required = false)String driverPlate,
            @RequestParam(value = "date_accident", required = false)String dateAccident,
            @RequestParam(value = "counterparty_plate", required = false)String counterpartyPlate,
            @RequestParam(value = "counterparty_name", required = false)String counterpartyName,
            @RequestParam(value = "counterparty_surname", required = false)String counterpartySurnameName,
            @RequestParam(value = "id_claim", required = false) String idClaims
    ){

        return new ResponseEntity<>(claimsCheckerService.checkIfExistsAnotherCounterpartyWithEqualsPlateName(driverPlate, dateAccident, counterpartyName, counterpartySurnameName, counterpartyPlate, idClaims), HttpStatus.OK);
        //claimsCheckerService.checkIfExistsAnotherCounterpartyWithEqualsPlateName(driverPlate, dateAccident, counterpartyName, counterpartySurnameName, counterpartyPlate, idClaims);
    }


    @GetMapping(value = "/check/flowtype",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Recover Claim", notes = "Returns a Claim using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ClaimsResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<FlowTypeResponseV1> checkflowtype(
            @RequestParam(value = "contractType", required = false)String contractType,
            @RequestParam(value = "customerId", required = false) String customerId

    ){
        return new ResponseEntity<>(claimsCheckerService.checkFlowtype(contractType, customerId) , HttpStatus.OK);


    }


    @GetMapping(value = "/check/authority",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Recover Claim", notes = "Returns a Claim using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ClaimsResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ClaimsCheckAuthorityResponseV1> checkMaintenance(
            @RequestParam(value = "chassis", required = true) String chassis,
            @RequestParam(value = "contractDate", required = true) String contractDate

    ){

           ClaimsCheckAuthorityResponseV1 checkAuthorityResponse = new ClaimsCheckAuthorityResponseV1();
           VehicleStatusAuthorityResponseV1 vehicleStatusAuthorityResponseV1 = null;
           ClaimsCheckAuthority checkAuthority = new ClaimsCheckAuthority();
            try {
                vehicleStatusAuthorityResponseV1 = authorityService.getVehicleStatus(chassis, contractDate);

            } catch (NotFoundException e){
                LOGGER.debug(MessageCode.CLAIMS_1101.value());
            } catch (InternalException e){
                if(e.getCode() != MessageCode.CLAIMS_1144.name()){
                    throw e;
                }
                String message = String.format(MessageCode.CLAIMS_1144.value(), chassis);
                LOGGER.debug(message);
            } catch (Exception e) {
                LOGGER.debug(MessageCode.CLAIMS_1103.value());
                throw new InternalException(MessageCode.CLAIMS_1103, e);
            }

            //Se il veicolo è presente nella tabella dei sinistri pending oppure è sotto lavorazione da authority, viene inviato un messaggio di Warning al FE
            if(vehicleStatusAuthorityResponseV1 != null && AuthorityVehicleStatusEnum.UNDER_MAINTENANCE.equals(vehicleStatusAuthorityResponseV1.getStatus())){
                LOGGER.debug("Vehicle with chassis "+vehicleStatusAuthorityResponseV1.getChassis()+" is under maintenance");
                String message = String.format(MessageCode.CLAIMS_1102.value(), vehicleStatusAuthorityResponseV1.getChassis());

                checkAuthority.setCode(MessageCode.CLAIMS_1102.name());
                checkAuthority.setMessage(message);
            }/* else if (claimsPendingService.checkIfExistsPendingWorking(plate,date)) {
                LOGGER.info("Vehicle with plate "+plate+" is under maintenance");
                String message = String.format(MessageCode.CLAIMS_1102.value(), plate);
                throw new BadRequestException(message, MessageCode.CLAIMS_1102);
            }*/

          checkAuthorityResponse.setClaimsCheckAuthority(checkAuthority);
          return new ResponseEntity<>(checkAuthorityResponse, HttpStatus.OK);

        //return new ResponseEntity<>(claimsCheckerService.checkFlowtype(contractType, customerId) , HttpStatus.OK);


    }

}
