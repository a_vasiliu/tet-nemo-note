package com.doing.nemo.claims.controller.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class SupplierRequestV1 implements Serializable {

    @JsonProperty("cod_supplier")
    private String codSupplier;

    @JsonProperty("name")
    private String name;

    @JsonProperty("email")
    private String email;

    @JsonProperty("is_active")
    private Boolean isActive = true;

    public SupplierRequestV1() {
    }

    public SupplierRequestV1(String codSupplier, String name, String email, Boolean isActive) {
        this.codSupplier = codSupplier;
        this.name = name;
        this.email = email;
        this.isActive = isActive;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public String getCodSupplier() {
        return codSupplier;
    }

    public void setCodSupplier(String codSupplier) {
        this.codSupplier = codSupplier;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "SupplierRequestV1{" +
                "codSupplier='" + codSupplier + '\'' +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", isActive=" + isActive +
                '}';
    }
}

