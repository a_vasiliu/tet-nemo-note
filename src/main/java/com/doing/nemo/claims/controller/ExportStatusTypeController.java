package com.doing.nemo.claims.controller;

import com.doing.nemo.claims.adapter.ExportStatusTypeAdapter;
import com.doing.nemo.claims.controller.payload.request.ExportStatusTypeRequestV1;
import com.doing.nemo.claims.controller.payload.request.ListRequestV1;
import com.doing.nemo.claims.controller.payload.response.ClaimsResponsePaginationV1;
import com.doing.nemo.claims.controller.payload.response.IdResponseV1;
import com.doing.nemo.claims.controller.payload.response.ExportStatusTypeResponseV1;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.settings.ExportStatusTypeEntity;
import com.doing.nemo.claims.service.ExportStatusTypeService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/v1")
@Api("Export Status TypeEnum")
public class ExportStatusTypeController {
    @Autowired
    private ExportStatusTypeService exportStatusTypeService;

    @Autowired
    private ExportStatusTypeAdapter exportStatusTypeAdapter;

    @PostMapping("/export/statustype")
    @Transactional
    @ApiOperation(value = "Insert Export Status TypeEnum", notes = "Insert Export Status TypeEnum using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = ExportStatusTypeResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<IdResponseV1> saveExportStatusType(
            @RequestBody ExportStatusTypeRequestV1 exportStatusTypeRequestV1
            )
    {
        ExportStatusTypeEntity exportStatusTypeEntity = ExportStatusTypeAdapter.adptFromExportStatusTypeRequestToExportStatusTypeEntity(exportStatusTypeRequestV1);
        exportStatusTypeEntity = exportStatusTypeService.saveExportStatusType(exportStatusTypeEntity);
        return new ResponseEntity<>(new IdResponseV1(exportStatusTypeEntity.getId()), HttpStatus.CREATED);
    }

    @PostMapping("/export/statustype/all")
    @Transactional
    @ApiOperation(value = "Insert a list of Export Status TypeEnum", notes = "Insert a list of insurance companies using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = ExportStatusTypeResponseV1.class, responseContainer="List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<ExportStatusTypeResponseV1>> insertExportStatusTypeAll(
            @ApiParam(value = "List of the bodies of the Export Status TypeEnum to be created", required = true)
            @RequestBody ListRequestV1<ExportStatusTypeRequestV1> insuranceCompaniesRequest){

        List<ExportStatusTypeResponseV1> exportStatusTypeResponseV1List = new ArrayList<>();
        for (ExportStatusTypeRequestV1 exportStatusTypeRequestV1 : insuranceCompaniesRequest.getData())
        {

            ExportStatusTypeEntity exportStatusTypeEntity = ExportStatusTypeAdapter.adptFromExportStatusTypeRequestToExportStatusTypeEntity(exportStatusTypeRequestV1);
            exportStatusTypeEntity = exportStatusTypeService.saveExportStatusType(exportStatusTypeEntity);
            ExportStatusTypeResponseV1 responseV1 = ExportStatusTypeAdapter.adptFromExportStatusTypeEntityToExportStatusTypeResponse(exportStatusTypeEntity);
            exportStatusTypeResponseV1List.add(responseV1);
        }
        return new ResponseEntity<>(exportStatusTypeResponseV1List, HttpStatus.CREATED);

    }


    @GetMapping("/export/statustype/{UUID}")
    @Transactional
    @ApiOperation(value = "Recover Export Status TypeEnum", notes = "Returns a Export Status TypeEnum using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ExportStatusTypeResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ExportStatusTypeResponseV1> getExportStatusType(
            @PathVariable("UUID") UUID uuid
    ){
        ExportStatusTypeEntity exportStatusTypeEntity = exportStatusTypeService.getExportStatusType(uuid);
        return new ResponseEntity<>(exportStatusTypeAdapter.adptFromExportStatusTypeEntityToExportStatusTypeResponse(exportStatusTypeEntity), HttpStatus.OK);
    }

    @GetMapping("/export/statustype")
    @Transactional
    @ApiOperation(value = "Recover Export Status TypeEnum", notes = "Returns a Export Status TypeEnum using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ExportStatusTypeResponseV1.class, responseContainer="List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<ExportStatusTypeResponseV1>> getExportStatusType(
    ){
        List<ExportStatusTypeEntity> exportStatusTypeEntityList = exportStatusTypeService.getAllExportStatusType();
        return new ResponseEntity<>(exportStatusTypeAdapter.adptFromExportStatusTypeEntityToExportStatusTypeResponseList(exportStatusTypeEntityList), HttpStatus.OK);
    }

    @PutMapping("/export/statustype/{UUID}")
    @Transactional
    @ApiOperation(value = "Insert Export Status TypeEnum", notes = "Insert Export Status TypeEnum using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = ExportStatusTypeResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ExportStatusTypeResponseV1> updateExportStatusType(
            @RequestBody ExportStatusTypeRequestV1 exportStatusTypeRequestV1,
            @PathVariable("UUID") UUID uuid
    )
    {
        ExportStatusTypeEntity exportStatusTypeEntity = exportStatusTypeAdapter.adptFromExportStatusTypeRequestToExportStatusTypeEntityWithID(exportStatusTypeRequestV1, uuid);
        exportStatusTypeEntity = exportStatusTypeService.updateExportStatusType(exportStatusTypeEntity);
        return new ResponseEntity<>(ExportStatusTypeAdapter.adptFromExportStatusTypeEntityToExportStatusTypeResponse(exportStatusTypeEntity), HttpStatus.OK);
    }

    @DeleteMapping("/export/statustype/{UUID}")
    @Transactional
    @ApiOperation(value = "Delete  Export Status TypeEnum", notes = "Delete Export Status TypeEnum using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity deleteExportStatusType(
            @PathVariable("UUID") UUID uuid
    ){
        exportStatusTypeService.deleteExportStatusType(uuid);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PatchMapping("/export/statustype/{UUID}/active")
    @Transactional
    @ApiOperation(value = "Patch status active Export Status TypeEnum", notes = "Patch status active Export Status TypeEnum using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ExportStatusTypeResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ExportStatusTypeResponseV1> patchStatusExportStatusType(
            @PathVariable UUID UUID
    ) {
        ExportStatusTypeEntity exportStatusTypeEntity = exportStatusTypeService.patchActive(UUID);
        ExportStatusTypeResponseV1 responseV1 = exportStatusTypeAdapter.adptFromExportStatusTypeEntityToExportStatusTypeResponse(exportStatusTypeEntity);
        return new ResponseEntity<>(responseV1, HttpStatus.OK);
    }

    @ApiOperation(value="Export Status TypeEnum Pagination", notes = "It retrieves Export Status TypeEnum pagination")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = ClaimsResponsePaginationV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @GetMapping(value="export/statustype/pagination", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<ClaimsResponsePaginationV1> statusTypePagination(
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @RequestParam(value = "page_size", defaultValue = "20") Integer pageSize,
            @RequestParam(value = "order_by", required = false) String orderBy,
            @RequestParam(value = "asc", required = false, defaultValue = "true") Boolean asc,
            @RequestParam(value = "status_type", required = false) String statusType,
            @RequestParam(value = "flow_type", required = false) String flowType,
            @RequestParam(value = "code", required = false) String code,
            @RequestParam(value = "description", required = false) String description,
            @RequestParam(value = "is_active", required = false) Boolean isActive
    ){

        Pagination<ExportStatusTypeEntity> exportStatusTypeEntityPagination = exportStatusTypeService.findExportStatusType(page, pageSize, orderBy, asc, statusType, flowType, code, description, isActive);

        return new ResponseEntity<>(exportStatusTypeAdapter.adptFromExportStatusTypePaginationToNotificationResponsePagination(exportStatusTypeEntityPagination), HttpStatus.OK);
    }

}
