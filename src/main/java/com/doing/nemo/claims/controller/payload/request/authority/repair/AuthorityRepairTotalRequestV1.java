package com.doing.nemo.claims.controller.payload.request.authority.repair;

import com.doing.nemo.claims.controller.payload.request.IdRequestV1;
import com.doing.nemo.claims.controller.payload.request.authority.claims.UserDetailsRequestV1;
import com.doing.nemo.claims.entity.enumerated.AuthorityEnum.AuthorityEventTypeEnum;
import com.doing.nemo.claims.entity.enumerated.WorkingStatusEnum;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AuthorityRepairTotalRequestV1 implements Serializable {

    @JsonProperty("repair_list")
    private List<IdRequestV1> repairList;

    @JsonProperty("authority_dossier_id")
    private String authorityDossierId;

    @JsonProperty("authority_working_id")
    private String authorityWorkingId;

    @JsonProperty("authority_dossier_number")
    private String authorityDossierNumber;

    @JsonProperty("working_number")
    private String workingNumber;

    @JsonProperty("total")
    private Double total;

    @JsonProperty("accepting_date")
    private String acceptingDate;

    @JsonProperty("approving_date")
    private String authorizationDate;

    @JsonProperty("rejection_date")
    private String rejectionDate;

    @JsonProperty("rejection")
    private Boolean rejection;

    @JsonProperty("note_rejection")
    private String noteRejection;

    @JsonProperty("working_created_at")
    private String workingCreatedAt;

    @JsonProperty("event_date")
    private String eventDate;

    @JsonProperty("event_type")
    private AuthorityEventTypeEnum eventType;

    @JsonProperty("working_status")
    private WorkingStatusEnum workingStatus;

    @JsonProperty("user_details")
    private UserDetailsRequestV1 userDetails;

    public List<IdRequestV1> getRepairList() {
        if(repairList == null){
            return null;
        }
        return new ArrayList<>(repairList);
    }

    public void setRepairList(List<IdRequestV1> repairList) {
        if(repairList != null)
        {
            this.repairList = new ArrayList<>(repairList);
        } else {
            this.repairList = null;
        }
    }

    public String getAuthorityDossierId() {
        return authorityDossierId;
    }

    public void setAuthorityDossierId(String authorityDossierId) {
        this.authorityDossierId = authorityDossierId;
    }

    public String getAuthorityWorkingId() {
        return authorityWorkingId;
    }

    public void setAuthorityWorkingId(String authorityWorkingId) {
        this.authorityWorkingId = authorityWorkingId;
    }

    public String getAuthorityDossierNumber() {
        return authorityDossierNumber;
    }

    public void setAuthorityDossierNumber(String authorityDossierNumber) {
        this.authorityDossierNumber = authorityDossierNumber;
    }

    public String getWorkingNumber() {
        return workingNumber;
    }

    public void setWorkingNumber(String workingNumber) {
        this.workingNumber = workingNumber;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public String getAcceptingDate() {
        return acceptingDate;
    }

    public void setAcceptingDate(String acceptingDate) {
        this.acceptingDate = acceptingDate;
    }

    public String getAuthorizationDate() {
        return authorizationDate;
    }

    public void setAuthorizationDate(String authorizationDate) {
        this.authorizationDate = authorizationDate;
    }

    public String getRejectionDate() {
        return rejectionDate;
    }

    public void setRejectionDate(String rejectionDate) {
        this.rejectionDate = rejectionDate;
    }

    public Boolean getRejection() {
        return rejection;
    }

    public void setRejection(Boolean rejection) {
        this.rejection = rejection;
    }

    public String getNoteRejection() {
        return noteRejection;
    }

    public void setNoteRejection(String noteRejection) {
        this.noteRejection = noteRejection;
    }

    public String getWorkingCreatedAt() {
        return workingCreatedAt;
    }

    public void setWorkingCreatedAt(String workingCreatedAt) {
        this.workingCreatedAt = workingCreatedAt;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public AuthorityEventTypeEnum getEventType() {
        return eventType;
    }

    public void setEventType(AuthorityEventTypeEnum eventType) {
        this.eventType = eventType;
    }

    public WorkingStatusEnum getWorkingStatus() {
        return workingStatus;
    }

    public void setWorkingStatus(WorkingStatusEnum workingStatus) {
        this.workingStatus = workingStatus;
    }

    public UserDetailsRequestV1 getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(UserDetailsRequestV1 userDetails) {
        this.userDetails = userDetails;
    }
}
