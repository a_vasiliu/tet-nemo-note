package com.doing.nemo.claims.controller.payload.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.UUID;

public class SupplierResponseV1 implements Serializable {

    @JsonProperty("id")
    private UUID id;

    @JsonProperty("cod_supplier")
    private String codSupplier;

    @JsonProperty("name")
    private String name;

    @JsonProperty("email")
    private String email;

    @JsonProperty("is_active")
    private Boolean isActive;

    public SupplierResponseV1() {
    }

    public SupplierResponseV1(UUID id, String codSupplier, String name, String email, Boolean isActive) {
        this.id = id;
        this.codSupplier = codSupplier;
        this.name = name;
        this.email = email;
        this.isActive = isActive;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getCodSupplier() {
        return codSupplier;
    }

    public void setCodSupplier(String codSupplier) {
        this.codSupplier = codSupplier;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @JsonIgnore
    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

}

