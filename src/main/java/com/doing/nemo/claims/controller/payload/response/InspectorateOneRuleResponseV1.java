package com.doing.nemo.claims.controller.payload.response;

import com.doing.nemo.claims.entity.settings.AutomaticAffiliationRuleInspectorateEntity;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.UUID;

public class InspectorateOneRuleResponseV1 implements Serializable {

    @JsonProperty("id")
    private UUID id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("email")
    private String email;

    @JsonProperty("address")
    private String address;

    @JsonProperty("zip_code")
    private Integer zipCode;

    @JsonProperty("city")
    private String city;

    @JsonProperty("province")
    private String province;

    @JsonProperty("state")
    private String state;

    @JsonProperty("vat_number")
    private String vatNumber;

    @JsonProperty("fiscal_code")
    private String fiscalCode;

    @JsonProperty("telephone")
    private String telephone;

    @JsonProperty("fax")
    private String fax;

    @JsonProperty("web_site")
    private String webSite;

    @JsonProperty("reference_person")
    private String referencePerson;

    @JsonProperty("attorney")
    private String attorney;

    @JsonProperty("external_code")
    private String externalCode;

    @JsonProperty("automatic_affiliation_rule")
    private AutomaticAffiliationRuleInspectorateEntity automaticAffiliationRuleEntity;

    public InspectorateOneRuleResponseV1() {
    }

    public InspectorateOneRuleResponseV1(UUID id, String name, String email, String address, Integer zipCode, String city, String province, String state, String vatNumber, String fiscalCode, String telephone, String fax, String webSite, String referencePerson, String attorney, String externalCode, AutomaticAffiliationRuleInspectorateEntity automaticAffiliationRuleEntity) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.address = address;
        this.zipCode = zipCode;
        this.city = city;
        this.province = province;
        this.state = state;
        this.vatNumber = vatNumber;
        this.fiscalCode = fiscalCode;
        this.telephone = telephone;
        this.fax = fax;
        this.webSite = webSite;
        this.referencePerson = referencePerson;
        this.attorney = attorney;
        this.externalCode = externalCode;
        this.automaticAffiliationRuleEntity = automaticAffiliationRuleEntity;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getZipCode() {
        return zipCode;
    }

    public void setZipCode(Integer zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getVatNumber() {
        return vatNumber;
    }

    public void setVatNumber(String vatNumber) {
        this.vatNumber = vatNumber;
    }

    public String getFiscalCode() {
        return fiscalCode;
    }

    public void setFiscalCode(String fiscalCode) {
        this.fiscalCode = fiscalCode;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getWebSite() {
        return webSite;
    }

    public void setWebSite(String webSite) {
        this.webSite = webSite;
    }

    public String getReferencePerson() {
        return referencePerson;
    }

    public void setReferencePerson(String referencePerson) {
        this.referencePerson = referencePerson;
    }

    public String getAttorney() {
        return attorney;
    }

    public void setAttorney(String attorney) {
        this.attorney = attorney;
    }

    public String getExternalCode() {
        return externalCode;
    }

    public void setExternalCode(String externalCode) {
        this.externalCode = externalCode;
    }

    public AutomaticAffiliationRuleInspectorateEntity getAutomaticAffiliationRuleEntity() {
        return automaticAffiliationRuleEntity;
    }

    public void setAutomaticAffiliationRuleEntity(AutomaticAffiliationRuleInspectorateEntity automaticAffiliationRuleEntity) {
        this.automaticAffiliationRuleEntity = automaticAffiliationRuleEntity;
    }

    @Override
    public String toString() {
        return "InspectorateOneRuleResponseV1{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", address='" + address + '\'' +
                ", zipCode=" + zipCode +
                ", city='" + city + '\'' +
                ", province='" + province + '\'' +
                ", state='" + state + '\'' +
                ", vatNumber='" + vatNumber + '\'' +
                ", fiscalCode='" + fiscalCode + '\'' +
                ", telephone='" + telephone + '\'' +
                ", fax='" + fax + '\'' +
                ", webSite='" + webSite + '\'' +
                ", referencePerson='" + referencePerson + '\'' +
                ", attorney='" + attorney + '\'' +
                ", externalCode='" + externalCode + '\'' +
                ", automaticAffiliationRuleEntity=" + automaticAffiliationRuleEntity +
                '}';
    }
}
