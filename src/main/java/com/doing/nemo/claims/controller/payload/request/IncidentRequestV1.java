package com.doing.nemo.claims.controller.payload.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class IncidentRequestV1 implements Serializable {

    @JsonProperty("system")
    private String system;

    @JsonProperty("incidentstatus_enumid")
    private Integer incidentStatusEnumId;

    @JsonProperty("immobilitystatus_enumid")
    private Integer immobilityStatusEnumId;

    @JsonProperty("incidenttype_enumid")
    private Integer incidentTypeEnumId;

    @JsonProperty("description")
    private String description;

    @JsonProperty("customerresponsible")
    private Boolean customerResponsible;

    @JsonProperty("incidentdatetime")
    private Instant incidentDateTime;

    @JsonProperty("distance")
    private Double distance;

    @JsonProperty("incidentlocation")
    private String incidentLocation;

    @JsonProperty("reference")
    private String reference;

    @JsonProperty("reportnumber")
    private String reportNumber;

    @JsonProperty("pctliability")
    private Double pctliability;

    @JsonProperty("safreceived")
    private Boolean safReceived;

    @JsonProperty("datesafreceived")
    private Instant dateSafReceived;

    @JsonProperty("totalloss")
    private Boolean totalLoss;

    @JsonProperty("totallossdate")
    private Instant totalLossDate;

    @JsonProperty("totallosstype_enumid")
    private Integer totalLossTypeEnumId;

    @JsonProperty("specificattention_enumid")
    private Integer specificAttentionEnumId;

    @JsonProperty("registrationdate")
    private Instant registrationDate;

    @JsonProperty("descriptioncode_enumid")
    private Integer descriptionCodeEnumId;

    @JsonProperty("recoverable")
    private Boolean recoverable;

    @JsonProperty("weathercondition_enumid")
    private Integer weatherConditionEnumId;

    @JsonProperty("policereportdate")
    private String policeReportDate;

    @JsonProperty("thief")
    private String thief;

    @JsonProperty("thefttype_enumid")
    private Integer theftTypeEnumId;

    @JsonProperty("retrievaldate")
    private Instant retrievalDate;

    @JsonProperty("retrievallocation")
    private String retrievAllocation;

    @JsonProperty("vehiclevalue")
    private Double vehicleValue;

    @JsonProperty("expectedrepaircost")
    private Double expectedRepairCost;

    @JsonProperty("relationshiptype_enumid")
    private Integer relationshipTypeEnumId;

    @JsonProperty("locationtype_enumid")
    private Integer locationTypeEnumId;

    @JsonProperty("speedlimit_enumid")
    private Integer speedLimitEnumId;

    @JsonProperty("incidentspeed")
    private Double incidentSpeed;

    @JsonProperty("seatbelt")
    private Boolean seatBelt;

    @JsonProperty("internalinsured")
    private Boolean internalInsured;

    @JsonProperty("thirdpartyinvolved")
    private Boolean thirdpartyInvolved;

    @JsonProperty("n_rebills")
    private Integer nRebills;

    @JsonProperty("rebill_type")
    private Integer rebillType;

    @JsonProperty("accident_driver")
    private String accidentDriver;

    @JsonProperty("pctrecoverable")
    private Double pctRecoverable;

    @JsonProperty("expectedwreckvalue")
    private Double expectedWreckValue;

    @JsonProperty("claims")
    private List<ClaimRequest> claims;

    public IncidentRequestV1() {
    }

    public IncidentRequestV1(String system, Integer incidentStatusEnumId, Integer immobilityStatusEnumId, Integer incidentTypeEnumId, String description, Boolean customerResponsible, Instant incidentDateTime, Double distance, String incidentLocation, String reference, String reportNumber, Double pctliability, Boolean safReceived, Instant dateSafReceived, Boolean totalLoss, Instant totalLossDate, Integer totalLossTypeEnumId, Integer specificAttentionEnumId, Instant registrationDate, Integer descriptionCodeEnumId, Boolean recoverable, Integer weatherConditionEnumId, String policeReportDate, String thief, Integer theftTypeEnumId, Instant retrievalDate, String retrievAllocation, Double vehicleValue, Double expectedRepairCost, Integer relationshipTypeEnumId, Integer locationTypeEnumId, Integer speedLimitEnumId, Double incidentSpeed, Boolean seatBelt, Boolean internalInsured, Boolean thirdpartyInvolved, Integer nRebills, Integer rebillType, String accidentDriver, Double pctRecoverable, Double expectedWreckValue, List<ClaimRequest> claims) {
        this.system = system;
        this.incidentStatusEnumId = incidentStatusEnumId;
        this.immobilityStatusEnumId = immobilityStatusEnumId;
        this.incidentTypeEnumId = incidentTypeEnumId;
        this.description = description;
        this.customerResponsible = customerResponsible;
        this.incidentDateTime = incidentDateTime;
        this.distance = distance;
        this.incidentLocation = incidentLocation;
        this.reference = reference;
        this.reportNumber = reportNumber;
        this.pctliability = pctliability;
        this.safReceived = safReceived;
        this.dateSafReceived = dateSafReceived;
        this.totalLoss = totalLoss;
        this.totalLossDate = totalLossDate;
        this.totalLossTypeEnumId = totalLossTypeEnumId;
        this.specificAttentionEnumId = specificAttentionEnumId;
        this.registrationDate = registrationDate;
        this.descriptionCodeEnumId = descriptionCodeEnumId;
        this.recoverable = recoverable;
        this.weatherConditionEnumId = weatherConditionEnumId;
        this.policeReportDate = policeReportDate;
        this.thief = thief;
        this.theftTypeEnumId = theftTypeEnumId;
        this.retrievalDate = retrievalDate;
        this.retrievAllocation = retrievAllocation;
        this.vehicleValue = vehicleValue;
        this.expectedRepairCost = expectedRepairCost;
        this.relationshipTypeEnumId = relationshipTypeEnumId;
        this.locationTypeEnumId = locationTypeEnumId;
        this.speedLimitEnumId = speedLimitEnumId;
        this.incidentSpeed = incidentSpeed;
        this.seatBelt = seatBelt;
        this.internalInsured = internalInsured;
        this.thirdpartyInvolved = thirdpartyInvolved;
        this.nRebills = nRebills;
        this.rebillType = rebillType;
        this.accidentDriver = accidentDriver;
        this.pctRecoverable = pctRecoverable;
        this.expectedWreckValue = expectedWreckValue;
        if(claims != null)
        {
            this.claims = new ArrayList<>(claims);
        }
    }

    public String getSystem() {
        return system;
    }

    public void setSystem(String system) {
        this.system = system;
    }

    public Integer getIncidentStatusEnumId() {
        return incidentStatusEnumId;
    }

    public void setIncidentStatusEnumId(Integer incidentStatusEnumId) {
        this.incidentStatusEnumId = incidentStatusEnumId;
    }

    public Integer getImmobilityStatusEnumId() {
        return immobilityStatusEnumId;
    }

    public void setImmobilityStatusEnumId(Integer immobilityStatusEnumId) {
        this.immobilityStatusEnumId = immobilityStatusEnumId;
    }

    public Integer getIncidentTypeEnumId() {
        return incidentTypeEnumId;
    }

    public void setIncidentTypeEnumId(Integer incidentTypeEnumId) {
        this.incidentTypeEnumId = incidentTypeEnumId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getCustomerResponsible() {
        return customerResponsible;
    }

    public void setCustomerResponsible(Boolean customerResponsible) {
        this.customerResponsible = customerResponsible;
    }

    public Instant getIncidentDateTime() {
        return incidentDateTime;
    }

    public void setIncidentDateTime(Instant incidentDateTime) {
        this.incidentDateTime = incidentDateTime;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public String getIncidentLocation() {
        return incidentLocation;
    }

    public void setIncidentLocation(String incidentLocation) {
        this.incidentLocation = incidentLocation;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getReportNumber() {
        return reportNumber;
    }

    public void setReportNumber(String reportNumber) {
        this.reportNumber = reportNumber;
    }

    public Double getPctliability() {
        return pctliability;
    }

    public void setPctliability(Double pctliability) {
        this.pctliability = pctliability;
    }

    public Boolean getSafReceived() {
        return safReceived;
    }

    public void setSafReceived(Boolean safReceived) {
        this.safReceived = safReceived;
    }

    public Instant getDateSafReceived() {
        return dateSafReceived;
    }

    public void setDateSafReceived(Instant dateSafReceived) {
        this.dateSafReceived = dateSafReceived;
    }

    public Boolean getTotalLoss() {
        return totalLoss;
    }

    public void setTotalLoss(Boolean totalLoss) {
        this.totalLoss = totalLoss;
    }

    public Instant getTotalLossDate() {
        return totalLossDate;
    }

    public void setTotalLossDate(Instant totalLossDate) {
        this.totalLossDate = totalLossDate;
    }

    public Integer getTotalLossTypeEnumId() {
        return totalLossTypeEnumId;
    }

    public void setTotalLossTypeEnumId(Integer totalLossTypeEnumId) {
        this.totalLossTypeEnumId = totalLossTypeEnumId;
    }

    public Integer getSpecificAttentionEnumId() {
        return specificAttentionEnumId;
    }

    public void setSpecificAttentionEnumId(Integer specificAttentionEnumId) {
        this.specificAttentionEnumId = specificAttentionEnumId;
    }

    public Instant getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Instant registrationDate) {
        this.registrationDate = registrationDate;
    }

    public Integer getDescriptionCodeEnumId() {
        return descriptionCodeEnumId;
    }

    public void setDescriptionCodeEnumId(Integer descriptionCodeEnumId) {
        this.descriptionCodeEnumId = descriptionCodeEnumId;
    }

    public Boolean getRecoverable() {
        return recoverable;
    }

    public void setRecoverable(Boolean recoverable) {
        this.recoverable = recoverable;
    }

    public Integer getWeatherConditionEnumId() {
        return weatherConditionEnumId;
    }

    public void setWeatherConditionEnumId(Integer weatherConditionEnumId) {
        this.weatherConditionEnumId = weatherConditionEnumId;
    }

    public String getPoliceReportDate() {
        return policeReportDate;
    }

    public void setPoliceReportDate(String policeReportDate) {
        this.policeReportDate = policeReportDate;
    }

    public String getThief() {
        return thief;
    }

    public void setThief(String thief) {
        this.thief = thief;
    }

    public Integer getTheftTypeEnumId() {
        return theftTypeEnumId;
    }

    public void setTheftTypeEnumId(Integer theftTypeEnumId) {
        this.theftTypeEnumId = theftTypeEnumId;
    }

    public Instant getRetrievalDate() {
        return retrievalDate;
    }

    public void setRetrievalDate(Instant retrievalDate) {
        this.retrievalDate = retrievalDate;
    }

    public String getRetrievAllocation() {
        return retrievAllocation;
    }

    public void setRetrievAllocation(String retrievAllocation) {
        this.retrievAllocation = retrievAllocation;
    }

    public Double getVehicleValue() {
        return vehicleValue;
    }

    public void setVehicleValue(Double vehicleValue) {
        this.vehicleValue = vehicleValue;
    }

    public Double getExpectedRepairCost() {
        return expectedRepairCost;
    }

    public void setExpectedRepairCost(Double expectedRepairCost) {
        this.expectedRepairCost = expectedRepairCost;
    }

    public Integer getRelationshipTypeEnumId() {
        return relationshipTypeEnumId;
    }

    public void setRelationshipTypeEnumId(Integer relationshipTypeEnumId) {
        this.relationshipTypeEnumId = relationshipTypeEnumId;
    }

    public Integer getLocationTypeEnumId() {
        return locationTypeEnumId;
    }

    public void setLocationTypeEnumId(Integer locationTypeEnumId) {
        this.locationTypeEnumId = locationTypeEnumId;
    }

    public Integer getSpeedLimitEnumId() {
        return speedLimitEnumId;
    }

    public void setSpeedLimitEnumId(Integer speedLimitEnumId) {
        this.speedLimitEnumId = speedLimitEnumId;
    }

    public Double getIncidentSpeed() {
        return incidentSpeed;
    }

    public void setIncidentSpeed(Double incidentSpeed) {
        this.incidentSpeed = incidentSpeed;
    }

    public Boolean getSeatBelt() {
        return seatBelt;
    }

    public void setSeatBelt(Boolean seatBelt) {
        this.seatBelt = seatBelt;
    }

    public Boolean getInternalInsured() {
        return internalInsured;
    }

    public void setInternalInsured(Boolean internalInsured) {
        this.internalInsured = internalInsured;
    }

    public Boolean getThirdpartyInvolved() {
        return thirdpartyInvolved;
    }

    public void setThirdpartyInvolved(Boolean thirdpartyInvolved) {
        this.thirdpartyInvolved = thirdpartyInvolved;
    }

    public Integer getnRebills() {
        return nRebills;
    }

    public void setnRebills(Integer nRebills) {
        this.nRebills = nRebills;
    }

    public Integer getRebillType() {
        return rebillType;
    }

    public void setRebillType(Integer rebillType) {
        this.rebillType = rebillType;
    }

    public String getAccidentDriver() {
        return accidentDriver;
    }

    public void setAccidentDriver(String accidentDriver) {
        this.accidentDriver = accidentDriver;
    }

    public Double getPctRecoverable() {
        return pctRecoverable;
    }

    public void setPctRecoverable(Double pctRecoverable) {
        this.pctRecoverable = pctRecoverable;
    }

    public Double getExpectedWreckValue() {
        return expectedWreckValue;
    }

    public void setExpectedWreckValue(Double expectedWreckValue) {
        this.expectedWreckValue = expectedWreckValue;
    }

    public List<ClaimRequest> getClaims() {
        if(claims == null){
            return null;
        }
        return new ArrayList<>(claims);
    }

    public void setClaims(List<ClaimRequest> claims) {
        if(claims != null)
        {
            this.claims =  new ArrayList<>(claims);
        } else {
            this.claims = null;
        }
    }

    @Override
    public String toString() {
        return "IncidentRequestV1{" +
                "system='" + system + '\'' +
                ", incidentStatusEnumId=" + incidentStatusEnumId +
                ", immobilityStatusEnumId=" + immobilityStatusEnumId +
                ", incidentTypeEnumId=" + incidentTypeEnumId +
                ", description='" + description + '\'' +
                ", customerResponsible=" + customerResponsible +
                ", incidentDateTime='" + incidentDateTime + '\'' +
                ", distance=" + distance +
                ", incidentLocation='" + incidentLocation + '\'' +
                ", reference='" + reference + '\'' +
                ", reportNumber='" + reportNumber + '\'' +
                ", pctliability=" + pctliability +
                ", safReceived=" + safReceived +
                ", dateSafReceived='" + dateSafReceived + '\'' +
                ", totalLoss=" + totalLoss +
                ", totalLossDate='" + totalLossDate + '\'' +
                ", totalLossTypeEnumId=" + totalLossTypeEnumId +
                ", specificAttentionEnumId=" + specificAttentionEnumId +
                ", registrationDate='" + registrationDate + '\'' +
                ", descriptionCodeEnumId=" + descriptionCodeEnumId +
                ", recoverable=" + recoverable +
                ", weatherConditionEnumId=" + weatherConditionEnumId +
                ", policeReportDate='" + policeReportDate + '\'' +
                ", thief='" + thief + '\'' +
                ", theftTypeEnumId=" + theftTypeEnumId +
                ", retrievalDate='" + retrievalDate + '\'' +
                ", retrievAllocation='" + retrievAllocation + '\'' +
                ", vehicleValue=" + vehicleValue +
                ", expectedRepairCost=" + expectedRepairCost +
                ", relationshipTypeEnumId=" + relationshipTypeEnumId +
                ", locationTypeEnumId=" + locationTypeEnumId +
                ", speedLimitEnumId=" + speedLimitEnumId +
                ", incidentSpeed=" + incidentSpeed +
                ", seatBelt=" + seatBelt +
                ", internalInsured=" + internalInsured +
                ", thirdpartyInvolved=" + thirdpartyInvolved +
                ", nRebills=" + nRebills +
                ", rebillType=" + rebillType +
                ", accidentDriver='" + accidentDriver + '\'' +
                ", pctRecoverable=" + pctRecoverable +
                ", expectedWreckValue=" + expectedWreckValue +
                ", claims=" + claims +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IncidentRequestV1 that = (IncidentRequestV1) o;
        return Objects.equals(getSystem(), that.getSystem()) &&
                Objects.equals(getIncidentStatusEnumId(), that.getIncidentStatusEnumId()) &&
                Objects.equals(getImmobilityStatusEnumId(), that.getImmobilityStatusEnumId()) &&
                Objects.equals(getIncidentTypeEnumId(), that.getIncidentTypeEnumId()) &&
                Objects.equals(getDescription(), that.getDescription()) &&
                Objects.equals(getCustomerResponsible(), that.getCustomerResponsible()) &&
                Objects.equals(getIncidentDateTime(), that.getIncidentDateTime()) &&
                Objects.equals(getDistance(), that.getDistance()) &&
                Objects.equals(getIncidentLocation(), that.getIncidentLocation()) &&
                Objects.equals(getReference(), that.getReference()) &&
                Objects.equals(getReportNumber(), that.getReportNumber()) &&
                Objects.equals(getPctliability(), that.getPctliability()) &&
                Objects.equals(getSafReceived(), that.getSafReceived()) &&
                Objects.equals(getDateSafReceived(), that.getDateSafReceived()) &&
                Objects.equals(getTotalLoss(), that.getTotalLoss()) &&
                Objects.equals(getTotalLossDate(), that.getTotalLossDate()) &&
                Objects.equals(getTotalLossTypeEnumId(), that.getTotalLossTypeEnumId()) &&
                Objects.equals(getSpecificAttentionEnumId(), that.getSpecificAttentionEnumId()) &&
                Objects.equals(getRegistrationDate(), that.getRegistrationDate()) &&
                Objects.equals(getDescriptionCodeEnumId(), that.getDescriptionCodeEnumId()) &&
                Objects.equals(getRecoverable(), that.getRecoverable()) &&
                Objects.equals(getWeatherConditionEnumId(), that.getWeatherConditionEnumId()) &&
                Objects.equals(getPoliceReportDate(), that.getPoliceReportDate()) &&
                Objects.equals(getThief(), that.getThief()) &&
                Objects.equals(getTheftTypeEnumId(), that.getTheftTypeEnumId()) &&
                Objects.equals(getRetrievalDate(), that.getRetrievalDate()) &&
                Objects.equals(getRetrievAllocation(), that.getRetrievAllocation()) &&
                Objects.equals(getVehicleValue(), that.getVehicleValue()) &&
                Objects.equals(getExpectedRepairCost(), that.getExpectedRepairCost()) &&
                Objects.equals(getRelationshipTypeEnumId(), that.getRelationshipTypeEnumId()) &&
                Objects.equals(getLocationTypeEnumId(), that.getLocationTypeEnumId()) &&
                Objects.equals(getSpeedLimitEnumId(), that.getSpeedLimitEnumId()) &&
                Objects.equals(getIncidentSpeed(), that.getIncidentSpeed()) &&
                Objects.equals(getSeatBelt(), that.getSeatBelt()) &&
                Objects.equals(getInternalInsured(), that.getInternalInsured()) &&
                Objects.equals(getThirdpartyInvolved(), that.getThirdpartyInvolved()) &&
                Objects.equals(getnRebills(), that.getnRebills()) &&
                Objects.equals(getRebillType(), that.getRebillType()) &&
                Objects.equals(getAccidentDriver(), that.getAccidentDriver()) &&
                Objects.equals(getPctRecoverable(), that.getPctRecoverable()) &&
                Objects.equals(getExpectedWreckValue(), that.getExpectedWreckValue()) &&
                Objects.equals(getClaims(), that.getClaims());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getSystem(), getIncidentStatusEnumId(), getImmobilityStatusEnumId(), getIncidentTypeEnumId(), getDescription(), getCustomerResponsible(), getIncidentDateTime(), getDistance(), getIncidentLocation(), getReference(), getReportNumber(), getPctliability(), getSafReceived(), getDateSafReceived(), getTotalLoss(), getTotalLossDate(), getTotalLossTypeEnumId(), getSpecificAttentionEnumId(), getRegistrationDate(), getDescriptionCodeEnumId(), getRecoverable(), getWeatherConditionEnumId(), getPoliceReportDate(), getThief(), getTheftTypeEnumId(), getRetrievalDate(), getRetrievAllocation(), getVehicleValue(), getExpectedRepairCost(), getRelationshipTypeEnumId(), getLocationTypeEnumId(), getSpeedLimitEnumId(), getIncidentSpeed(), getSeatBelt(), getInternalInsured(), getThirdpartyInvolved(), getnRebills(), getRebillType(), getAccidentDriver(), getPctRecoverable(), getExpectedWreckValue(), getClaims());
    }
}
