package com.doing.nemo.claims.controller.payload.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class InsuranceCompanyCounterpartyResponse implements Serializable {
    @JsonProperty("entity")
    private InsuranceCompanyResponseV1 entity;

    @JsonProperty("name")
    private String name;

    public InsuranceCompanyCounterpartyResponse() {
    }

    public InsuranceCompanyCounterpartyResponse(InsuranceCompanyResponseV1 entity, String name) {
        this.entity = entity;
        this.name = name;
    }

    public InsuranceCompanyResponseV1 getEntity() {
        return entity;
    }

    public void setEntity(InsuranceCompanyResponseV1 entity) {
        this.entity = entity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "InsuranceCompanyCounterpartyResponse{" +
                "entity=" + entity +
                ", name='" + name + '\'' +
                '}';
    }
}
