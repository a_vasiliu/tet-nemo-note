package com.doing.nemo.claims.controller.payload.response;

import com.doing.nemo.claims.entity.enumerated.EventTypeEnum;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class EmailTemplateInternalMessagingResponseV1 implements Serializable {

    @JsonProperty("event_type")
    private EventTypeEnum eventType;

    @JsonProperty("email_template")
    private List<EmailTemplateMessagingResponseV1> emailTemplate;


    public EventTypeEnum getEventType() {
        return eventType;
    }

    public void setEventType(EventTypeEnum eventType) {
        this.eventType = eventType;
    }

    public List<EmailTemplateMessagingResponseV1> getEmailTemplate() {
        if(emailTemplate == null){
            return null;
        }
        return new ArrayList<>(emailTemplate);
    }

    public void setEmailTemplate(List<EmailTemplateMessagingResponseV1> emailTemplate) {
        if(emailTemplate != null)
        {
            this.emailTemplate = new ArrayList<>(emailTemplate);
        } else {
            this.emailTemplate = null;
        }
    }
}
