package com.doing.nemo.claims.controller;

import com.doing.nemo.claims.adapter.AutomaticAffiliationRuleLegalAdapter;
import com.doing.nemo.claims.adapter.LegalAdapter;
import com.doing.nemo.claims.common.validation.MessageCode;
import com.doing.nemo.claims.common.validation.RequestValidator;
import com.doing.nemo.claims.controller.payload.request.*;
import com.doing.nemo.claims.controller.payload.response.ClaimsResponsePaginationV1;
import com.doing.nemo.claims.controller.payload.response.IdResponseV1;
import com.doing.nemo.claims.controller.payload.response.LegalResponseV1;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.settings.AutomaticAffiliationRuleLegalEntity;
import com.doing.nemo.claims.entity.settings.LegalEntity;
import com.doing.nemo.claims.service.AutomaticAffiliationRuleLegalService;
import com.doing.nemo.claims.service.LegalService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/v1")
@Api("Legal")
public class LegalController {

    @Autowired
    private LegalService legalService;

    @Autowired
    private LegalAdapter legalAdapter;

    @Autowired
    private AutomaticAffiliationRuleLegalAdapter automaticAffiliationRuleLegalAdapter;

    @Autowired
    private AutomaticAffiliationRuleLegalService automaticAffiliationRuleLegalService;

    @Autowired
    private RequestValidator requestValidator;

    @PostMapping("/legal")
    @Transactional
    @ApiOperation(value = "Insert Legal", notes = "Insert legal using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = IdResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<IdResponseV1> legalCompany(
            @ApiParam(value = "Body of the Legal to be created", required = true)
            @RequestBody LegalRequestV1 legalRequest) {
        requestValidator.validateRequest(legalRequest, MessageCode.E00X_1000);
        LegalEntity legalEntity = legalService.insertLegal(legalAdapter.adptFromLegalRequToLegalEntity(legalRequest, HttpMethod.POST));
        IdResponseV1 responseV1 = new IdResponseV1(legalEntity.getId());
        return new ResponseEntity<>(responseV1, HttpStatus.CREATED);

    }

    @PostMapping("/legals")
    @Transactional
    @ApiOperation(value = "Insert a list of Legal", notes = "Insert a list of legal using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = LegalResponseV1.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<LegalResponseV1>> insertMoreLegal(
            @ApiParam(value = "List of the bodies of the Legal to be created", required = true)
            @RequestBody ListRequestV1<LegalRequestV1> legalListRequest) {

        List<LegalResponseV1> legalResponseV1List = new ArrayList<>();
        for (LegalRequestV1 legalRequestV1 : legalListRequest.getData()) {
            requestValidator.validateRequest(legalRequestV1, MessageCode.E00X_1000);
            LegalEntity legalEntity = legalAdapter.adptFromLegalRequToLegalEntity(legalRequestV1, HttpMethod.POST);
            legalEntity = legalService.insertLegal(legalEntity);
            LegalResponseV1<LegalEntity> responseV1 = LegalAdapter.adptFromLegalEntityToLegalRespo(legalEntity);
            legalResponseV1List.add(responseV1);
        }
        return new ResponseEntity<>(legalResponseV1List, HttpStatus.CREATED);

    }

    @PostMapping("/legal/{UUID}")
    @Transactional
    @ApiOperation(value = "Insert Legal's Volume Rules", notes = "Insert rules for the legal identified by the UUID")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = LegalResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<LegalResponseV1> legalCompanyRule(
            @ApiParam(value = "Body of the rules to be created", required = true)
            @RequestBody ListRequestV1<AutomaticAffiliationRuleLegalRequestV1> automaticAffiliationRuleLegalListRequestV1,
            @ApiParam(value = "UUID that identifies the Legal to be modified", required = true)
            @PathVariable UUID UUID) {

        requestValidator.validateRequest(automaticAffiliationRuleLegalListRequestV1, MessageCode.E00X_1000);
        List<AutomaticAffiliationRuleLegalEntity> automaticAffiliationRuleLegalEntity = automaticAffiliationRuleLegalAdapter.adptFromAARuleLegalRequToAARuleLegalEntityList(automaticAffiliationRuleLegalListRequestV1.getData(), HttpMethod.POST);
        LegalEntity legalEntity = legalService.insertLegalRule(automaticAffiliationRuleLegalEntity, UUID);

        LegalResponseV1<LegalEntity> responseV1 = LegalAdapter.adptFromLegalEntityToLegalRespo(legalEntity);
        return new ResponseEntity<>(responseV1, HttpStatus.CREATED);

    }

    @GetMapping("/legal/{UUID}")
    @Transactional
    @ApiOperation(value = "Recover Legal", notes = "Returns a Legal using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = LegalResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<LegalResponseV1> getLegal(
            @ApiParam(value = "UUID of the Legal to be found", required = true)
            @PathVariable UUID UUID) {

        LegalEntity legalEntity = legalService.selectLegal(UUID);
        LegalResponseV1<LegalEntity> responseV1 = LegalAdapter.adptFromLegalEntityToLegalRespo(legalEntity);
        return new ResponseEntity<>(responseV1, HttpStatus.OK);
    }

    @GetMapping("/legal")
    @Transactional
    @ApiOperation(value = "Recover All Legal", notes = "Returns all Legal")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = LegalResponseV1.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<LegalResponseV1>> getAllLegal() {

        List<LegalEntity> legalEntityList = legalService.selectAllLegal();

        List<LegalResponseV1> responseV1 = LegalAdapter.adptFromLegalEntityToLegalRespoList(legalEntityList);
        return new ResponseEntity<>(responseV1, HttpStatus.OK);
    }

    @PutMapping("/legal/{UUID}")
    @Transactional
    @ApiOperation(value = "Upload Legal", notes = "Upload Legal using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = LegalResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<LegalResponseV1> putLegal(
            @ApiParam(value = "UUID that identifies the Legal to be modified", required = true)
            @PathVariable UUID UUID,
            @ApiParam(value = "Updated body of the Legal", required = true)
            @RequestBody LegalRequestV1 legalRequest) {

        requestValidator.validateRequest(legalRequest, MessageCode.E00X_1000);
        LegalEntity legalEntity = legalAdapter.adptFromLegalRequToLegalEntityWithID(legalRequest, UUID, HttpMethod.PUT);
        legalService.updateLegal(legalEntity, UUID);

        LegalResponseV1<LegalEntity> responseV1 = LegalAdapter.adptFromLegalEntityToLegalRespo(legalEntity);
        return new ResponseEntity<>(responseV1, HttpStatus.OK);

    }

    @DeleteMapping("/legal/{UUID}")
    @Transactional
    @ApiOperation(value = "Delete Legal", notes = "Delete Legal using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = LegalResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<LegalResponseV1> deleteLegal(
            @ApiParam(value = "UUID that identifies the Legal to be deleted", required = true)
            @PathVariable UUID UUID) {
        LegalResponseV1 responseV1 = legalService.deleteLegal(UUID);
        return new ResponseEntity<>(responseV1, HttpStatus.OK);
    }

    @PatchMapping("/legal/{UUID}/active")
    @Transactional
    @ApiOperation(value = "Patch status active Legal", notes = "Patch status active Legal using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = LegalResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<LegalResponseV1> patchStatusLegal(
            @PathVariable UUID UUID
    ) {
        LegalEntity legalEntity = legalService.updateStatusLegal(UUID);
        LegalResponseV1 responseV1 = LegalAdapter.adptFromLegalEntityToLegalRespo(legalEntity);
        return new ResponseEntity<>(responseV1, HttpStatus.OK);
    }

    @ApiOperation(value="Legal Pagination", notes = "It retrieves Legal pagination")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = ClaimsResponsePaginationV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @GetMapping(value="/legal/pagination", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<ClaimsResponsePaginationV1> legalPagination(
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @RequestParam(value = "page_size", defaultValue = "20") Integer pageSize,
            @RequestParam(value = "order_by", required = false) String orderBy,
            @RequestParam(value = "asc", required = false, defaultValue = "true") Boolean asc,
            @RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "email", required = false) String email,
            @RequestParam(value = "province", required = false) String province,
            @RequestParam(value = "telephone", required = false) String telephone,
            @RequestParam(value = "fax", required = false) String fax,
            @RequestParam(value = "is_active", required = false) Boolean isActive,
            @RequestParam(value = "address", required = false) String address,
            @RequestParam(value = "zip_code", required = false) String zipCode,
            @RequestParam(value = "city", required = false) String city,
            @RequestParam(value = "state", required = false) String state,
            @RequestParam(value = "code", required = false) Long code

    ){

        Pagination<LegalEntity> legalEntityPagination = legalService.paginationLegal(page, pageSize, orderBy, asc, name, email, province, telephone, fax, isActive, address, zipCode, city, state, code);

        return new ResponseEntity<>(LegalAdapter.adptLegalPaginationToClaimsResponsePagination(legalEntityPagination), HttpStatus.OK);
    }


    @ApiOperation(value="Reset rule", notes = "Job for reset legal rule")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/legal/rule/reset")
    public void resetLegalRule(){
        legalService.resetRuleJob();
    }
}
