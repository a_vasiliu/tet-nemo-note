package com.doing.nemo.claims.controller.payload.request.myald;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class MyAldLinkRequestV1 implements Serializable {
    @JsonProperty("claim_detail")
    private MyAldLinkObjectRequestV1 claimDetail;

    public MyAldLinkObjectRequestV1 getClaimDetail() {
        return claimDetail;
    }

    public void setClaimDetail(MyAldLinkObjectRequestV1 claimDetail) {
        this.claimDetail = claimDetail;
    }

    @Override
    public String toString() {
        return "MyAldLinkRequestV1{" +
                "claimDetail=" + claimDetail +
                '}';
    }
}
