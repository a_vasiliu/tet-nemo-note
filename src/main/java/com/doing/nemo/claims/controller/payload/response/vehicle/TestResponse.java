package com.doing.nemo.claims.controller.payload.response.vehicle;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class TestResponse implements Serializable {

    @JsonProperty("test")
    private String test;

    public TestResponse() {
    }

    public TestResponse(String test) {
        this.test = test;
    }

    public String getTest() {
        return test;
    }

    public void setTest(String test) {
        this.test = test;
    }

    @Override
    public String toString() {
        return "TestResponseV1{" +
                "test='" + test + '\'' +
                '}';
    }
}
