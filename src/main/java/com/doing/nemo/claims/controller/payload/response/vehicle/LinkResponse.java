package com.doing.nemo.claims.controller.payload.response.vehicle;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class LinkResponse implements Serializable {

    @JsonProperty("rel")
    private String rel;

    @JsonProperty("href")
    private String href;

    @JsonProperty("test")
    private List<TestResponse> testList;

    public String getRel() {
        return rel;
    }

    public void setRel(String rel) {
        this.rel = rel;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public List<TestResponse> getTestList()
    {
        if(testList == null){
            return null;
        }
        return new ArrayList<>(testList);
    }

    public void setTestList(List<TestResponse> testList) {
        if(testList != null)
        {
            this.testList = new ArrayList<>(testList);
        } else {
            this.testList = null;
        }
    }

    @Override
    public String toString() {
        return "Link{" +
                "rel='" + rel + '\'' +
                ", href='" + href + '\'' +
                ", testList=" + testList +
                '}';
    }


}