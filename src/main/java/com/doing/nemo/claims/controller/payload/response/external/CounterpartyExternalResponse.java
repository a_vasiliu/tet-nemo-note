package com.doing.nemo.claims.controller.payload.response.external;

import com.doing.nemo.claims.controller.payload.response.InsuranceCompanyCounterpartyResponse;
import com.doing.nemo.claims.controller.payload.response.counterparty.InsuredResponse;
import com.doing.nemo.claims.controller.payload.response.damaged.DriverResponse;
import com.doing.nemo.claims.controller.payload.response.damaged.ImpactPointResponse;
import com.doing.nemo.claims.controller.payload.response.damaged.VehicleResponse;
import com.doing.nemo.claims.controller.payload.response.forms.AttachmentResponse;
import com.doing.nemo.claims.entity.enumerated.DamagedEnum.VehicleEnum.VehicleTypeEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CounterpartyExternalResponse implements Serializable {

    @JsonProperty("id")
    private String id;

    @JsonProperty("practice_id_counterparty")
    private Long practiceIdCounterparty;

    @JsonProperty("type")
    private VehicleTypeEnum type;

    @JsonProperty("insured")
    private InsuredResponse insured;

    @JsonProperty("insurance_company")
    private InsuranceCompanyCounterpartyResponse insuranceCompany;

    @JsonProperty("driver")
    private DriverResponse driver;

    @JsonProperty("vehicle")
    private VehicleResponse vehicle;

    @JsonProperty("is_cai_signed")
    private Boolean isCaiSigned;

    @JsonProperty("impact_point")
    private ImpactPointResponse impactPoint;

    @JsonProperty("responsible")
    private Boolean responsible;

    @JsonProperty("description")
    private String description;

    @JsonProperty("policy_number")
    private String policyNumber;

    @JsonProperty("policy_beginning_validity")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date policyBeginningValidity;

    @JsonProperty("policy_end_validity")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date policyEndValidity;

    @JsonProperty("attachments")
    private List<AttachmentResponse> attachments;

    public List<AttachmentResponse> getAttachments() {
        if(attachments == null){
            return null;
        }
        return new ArrayList<>(attachments);
    }

    public void setAttachments(List<AttachmentResponse> attachments) {
        if(attachments != null)
        {
            this.attachments = new ArrayList<>(attachments);
        } else {
            this.attachments = null;
        }
    }

    public Long getPracticeIdCounterparty() {
        return practiceIdCounterparty;
    }

    public void setPracticeIdCounterparty(Long practiceIdCounterparty) {
        this.practiceIdCounterparty = practiceIdCounterparty;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public Date getPolicyBeginningValidity() {
        if(policyBeginningValidity == null){
            return null;
        }
        return (Date)policyBeginningValidity.clone();
    }

    public void setPolicyBeginningValidity(Date policyBeginningValidity) {
        if(policyBeginningValidity != null)
        {
            this.policyBeginningValidity = (Date)policyBeginningValidity.clone();
        } else {
            this.policyBeginningValidity = null;
        }
    }

    public Date getPolicyEndValidity() {
        if(policyEndValidity == null){
            return null;
        }
        return (Date)policyEndValidity.clone();
    }

    public void setPolicyEndValidity(Date policyEndValidity) {
        if(policyEndValidity != null)
        {
            this.policyEndValidity = (Date)policyEndValidity.clone();
        } else {
            this.policyEndValidity = null;
        }
    }

    public VehicleTypeEnum getType() {
        return type;
    }

    public void setType(VehicleTypeEnum type) {
        this.type = type;
    }

    public InsuredResponse getInsured() {
        return insured;
    }

    public void setInsured(InsuredResponse insured) {
        this.insured = insured;
    }

    public InsuranceCompanyCounterpartyResponse getInsuranceCompany() {
        return insuranceCompany;
    }

    public void setInsuranceCompany(InsuranceCompanyCounterpartyResponse insuranceCompany) {
        this.insuranceCompany = insuranceCompany;
    }

    public DriverResponse getDriver() {
        return driver;
    }

    public void setDriver(DriverResponse driver) {
        this.driver = driver;
    }

    public VehicleResponse getVehicle() {
        return vehicle;
    }

    public void setVehicle(VehicleResponse vehicle) {
        this.vehicle = vehicle;
    }

    @JsonIgnore
    public Boolean getCaiSigned() {
        return isCaiSigned;
    }

    public void setCaiSigned(Boolean caiSigned) {
        isCaiSigned = caiSigned;
    }

    public ImpactPointResponse getImpactPoint() {
        return impactPoint;
    }

    public void setImpactPoint(ImpactPointResponse impactPoint) {
        this.impactPoint = impactPoint;
    }

    public Boolean getResponsible() {
        return responsible;
    }

    public void setResponsible(Boolean responsible) {
        this.responsible = responsible;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "CounterpartyExternalResponse{" +
                "type=" + type +
                ", insured=" + insured +
                ", insuranceCompany=" + insuranceCompany +
                ", driver=" + driver +
                ", vehicle=" + vehicle +
                ", isCaiSigned=" + isCaiSigned +
                ", impactPoint=" + impactPoint +
                ", responsible=" + responsible +
                ", description='" + description + '\'' +
                ", policyNumber='" + policyNumber + '\'' +
                ", policyBeginningValidity=" + policyBeginningValidity +
                ", policyEndValidity=" + policyEndValidity +
                '}';
    }
}
