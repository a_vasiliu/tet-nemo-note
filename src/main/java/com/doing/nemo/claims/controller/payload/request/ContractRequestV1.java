package com.doing.nemo.claims.controller.payload.request;

import com.doing.nemo.claims.entity.settings.AntiTheftServiceEntity;
import com.doing.nemo.claims.entity.settings.ContractTypeEntity;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class ContractRequestV1 implements Serializable {

    @JsonProperty("contract_type")
    private ContractTypeEntity contractType;

    @JsonProperty("beginning_of_validity")
    private String beginningOfValidity;

    @JsonProperty("end_of_validity")
    private String endOfValidity;

    @JsonProperty("plate_number")
    private String plateNumber = "";

    @JsonProperty("delivery_center")
    private String delivertyCenter = "";

    @JsonProperty("ex_plate_number")
    private String exPlateNumber = "";

    @JsonProperty("brand")
    private String brand = "";

    @JsonProperty("model")
    private String model = "";

    @JsonProperty("hp")
    private Integer hp;

    @JsonProperty("kw")
    private Integer kw;

    @JsonProperty("weight")
    private Integer weight;

    @JsonProperty("matriculation")
    private String matriculation;

    @JsonProperty("color")
    private String color = "";

    @JsonProperty("frame")
    private String frame = "";

    @JsonProperty("jato")
    private String jato = "";

    @JsonProperty("eurotax")
    private String eurotax = "";

    @JsonProperty("km_last_check")
    private Integer kmLastCheck;

    @JsonProperty("book_register")
    private String bookRegister = "";

    @JsonProperty("book_register_pai")
    private String bookRegisterPai = "";

    @JsonProperty("insurance_theft_lc")
    private Double insuranceTheftLc;

    @JsonProperty("insurance_kasko_md")
    private Double insuranceKaskoMd;

    @JsonProperty("insurance_search_tpl")
    private Double insuranceSearchTpl;

    @JsonProperty("insurance_ex_thf")
    private Double insuranceExThf;

    @JsonProperty("insurance_pai_max")
    private Integer insurancePaiMax;

    @JsonProperty("insurance_deduct_md")
    private Double insuranceDeductMd;

    @JsonProperty("insurance_pai_pass")
    private Double insurancePaiPass;

    @JsonProperty("insurance_ex_tpl")
    private Double insuranceExTpl;

    @JsonProperty("insurance_max_pass")
    private Double insuranceMaxPass;

    @JsonProperty("insurance_pai_ex")
    private Integer insurancePaiEx;

    @JsonProperty("anti_theft")
    private AntiTheftServiceEntity antiTheft;

    @JsonProperty("voucher_id")
    private Long voucherId;

    @JsonProperty("activation")
    private String activation;

    @JsonProperty("contract_area_business")
    private String contractAreaBusiness = "";

    @JsonProperty("km_contract")
    private Integer kmContract;

    @JsonProperty("months_duration")
    private Integer monthsDuration;

    @JsonProperty("begin_contract")
    private String beginContract;

    @JsonProperty("end_contract")
    private String endContract;

    @JsonProperty("suspension")
    private String suspension;

    @JsonProperty("reactivation")
    private String reactivation;

    @JsonProperty("lengthened")
    private String lengthened;

    @JsonProperty("cancellation")
    private String cancellation;

    @JsonProperty("returned")
    private String returned;

    @JsonProperty("lock_start")
    private String lockStart;

    @JsonProperty("lock_end")
    private String lockEnd;

    public ContractRequestV1() {
    }

    public ContractRequestV1(ContractTypeEntity contractType, String beginningOfValidity, String endOfValidity, String plateNumber, String delivertyCenter, String exPlateNumber, String brand, String model, Integer hp, Integer kw, Integer weight, String matriculation, String color, String frame, String jato, String eurotax, Integer kmLastCheck, String bookRegister, String bookRegisterPai, Double insuranceTheftLc, Double insuranceKaskoMd, Double insuranceSearchTpl, Double insuranceExThf, Integer insurancePaiMax, Double insuranceDeductMd, Double insurancePaiPass, Double insuranceExTpl, Double insuranceMaxPass, Integer insurancePaiEx, AntiTheftServiceEntity antiTheft, Long voucherId, String activation, String contractAreaBusiness, Integer kmContract, Integer monthsDuration, String beginContract, String endContract, String suspension, String reactivation, String lengthened, String cancellation, String returned, String lockStart, String lockEnd) {
        this.contractType = contractType;
        this.beginningOfValidity = beginningOfValidity;
        this.endOfValidity = endOfValidity;
        this.plateNumber = plateNumber;
        this.delivertyCenter = delivertyCenter;
        this.exPlateNumber = exPlateNumber;
        this.brand = brand;
        this.model = model;
        this.hp = hp;
        this.kw = kw;
        this.weight = weight;
        this.matriculation = matriculation;
        this.color = color;
        this.frame = frame;
        this.jato = jato;
        this.eurotax = eurotax;
        this.kmLastCheck = kmLastCheck;
        this.bookRegister = bookRegister;
        this.bookRegisterPai = bookRegisterPai;
        this.insuranceTheftLc = insuranceTheftLc;
        this.insuranceKaskoMd = insuranceKaskoMd;
        this.insuranceSearchTpl = insuranceSearchTpl;
        this.insuranceExThf = insuranceExThf;
        this.insurancePaiMax = insurancePaiMax;
        this.insuranceDeductMd = insuranceDeductMd;
        this.insurancePaiPass = insurancePaiPass;
        this.insuranceExTpl = insuranceExTpl;
        this.insuranceMaxPass = insuranceMaxPass;
        this.insurancePaiEx = insurancePaiEx;
        this.antiTheft = antiTheft;
        this.voucherId = voucherId;
        this.activation = activation;
        this.contractAreaBusiness = contractAreaBusiness;
        this.kmContract = kmContract;
        this.monthsDuration = monthsDuration;
        this.beginContract = beginContract;
        this.endContract = endContract;
        this.suspension = suspension;
        this.reactivation = reactivation;
        this.lengthened = lengthened;
        this.cancellation = cancellation;
        this.returned = returned;
        this.lockStart = lockStart;
        this.lockEnd = lockEnd;
    }

    public ContractTypeEntity getContractType() {
        return contractType;
    }

    public void setContractType(ContractTypeEntity contractType) {
        this.contractType = contractType;
    }

    public String getBeginningOfValidity() {
        return beginningOfValidity;
    }

    public void setBeginningOfValidity(String beginningOfValidity) {
        this.beginningOfValidity = beginningOfValidity;
    }

    public String getEndOfValidity() {
        return endOfValidity;
    }

    public void setEndOfValidity(String endOfValidity) {
        this.endOfValidity = endOfValidity;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public String getDelivertyCenter() {
        return delivertyCenter;
    }

    public void setDelivertyCenter(String delivertyCenter) {
        this.delivertyCenter = delivertyCenter;
    }

    public String getExPlateNumber() {
        return exPlateNumber;
    }

    public void setExPlateNumber(String exPlateNumber) {
        this.exPlateNumber = exPlateNumber;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Integer getHp() {
        return hp;
    }

    public void setHp(Integer hp) {
        this.hp = hp;
    }

    public Integer getKw() {
        return kw;
    }

    public void setKw(Integer kw) {
        this.kw = kw;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public String getMatriculation() {
        return matriculation;
    }

    public void setMatriculation(String matriculation) {
        this.matriculation = matriculation;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getFrame() {
        return frame;
    }

    public void setFrame(String frame) {
        this.frame = frame;
    }

    public String getJato() {
        return jato;
    }

    public void setJato(String jato) {
        this.jato = jato;
    }

    public String getEurotax() {
        return eurotax;
    }

    public void setEurotax(String eurotax) {
        this.eurotax = eurotax;
    }

    public Integer getKmLastCheck() {
        return kmLastCheck;
    }

    public void setKmLastCheck(Integer kmLastCheck) {
        this.kmLastCheck = kmLastCheck;
    }

    public String getBookRegister() {
        return bookRegister;
    }

    public void setBookRegister(String bookRegister) {
        this.bookRegister = bookRegister;
    }

    public String getBookRegisterPai() {
        return bookRegisterPai;
    }

    public void setBookRegisterPai(String bookRegisterPai) {
        this.bookRegisterPai = bookRegisterPai;
    }

    public Double getInsuranceTheftLc() {
        return insuranceTheftLc;
    }

    public void setInsuranceTheftLc(Double insuranceTheftLc) {
        this.insuranceTheftLc = insuranceTheftLc;
    }

    public Double getInsuranceKaskoMd() {
        return insuranceKaskoMd;
    }

    public void setInsuranceKaskoMd(Double insuranceKaskoMd) {
        this.insuranceKaskoMd = insuranceKaskoMd;
    }

    public Double getInsuranceSearchTpl() {
        return insuranceSearchTpl;
    }

    public void setInsuranceSearchTpl(Double insuranceSearchTpl) {
        this.insuranceSearchTpl = insuranceSearchTpl;
    }

    public Double getInsuranceExThf() {
        return insuranceExThf;
    }

    public void setInsuranceExThf(Double insuranceExThf) {
        this.insuranceExThf = insuranceExThf;
    }

    public Integer getInsurancePaiMax() {
        return insurancePaiMax;
    }

    public void setInsurancePaiMax(Integer insurancePaiMax) {
        this.insurancePaiMax = insurancePaiMax;
    }

    public Double getInsuranceDeductMd() {
        return insuranceDeductMd;
    }

    public void setInsuranceDeductMd(Double insuranceDeductMd) {
        this.insuranceDeductMd = insuranceDeductMd;
    }

    public Double getInsurancePaiPass() {
        return insurancePaiPass;
    }

    public void setInsurancePaiPass(Double insurancePaiPass) {
        this.insurancePaiPass = insurancePaiPass;
    }

    public Double getInsuranceExTpl() {
        return insuranceExTpl;
    }

    public void setInsuranceExTpl(Double insuranceExTpl) {
        this.insuranceExTpl = insuranceExTpl;
    }

    public Double getInsuranceMaxPass() {
        return insuranceMaxPass;
    }

    public void setInsuranceMaxPass(Double insuranceMaxPass) {
        this.insuranceMaxPass = insuranceMaxPass;
    }

    public Integer getInsurancePaiEx() {
        return insurancePaiEx;
    }

    public void setInsurancePaiEx(Integer insurancePaiEx) {
        this.insurancePaiEx = insurancePaiEx;
    }

    public AntiTheftServiceEntity getAntiTheft() {
        return antiTheft;
    }

    public void setAntiTheft(AntiTheftServiceEntity antiTheft) {
        this.antiTheft = antiTheft;
    }

    public Long getVoucherId() {
        return voucherId;
    }

    public void setVoucherId(Long voucherId) {
        this.voucherId = voucherId;
    }

    public String getActivation() {
        return activation;
    }

    public void setActivation(String activation) {
        this.activation = activation;
    }

    public String getContractAreaBusiness() {
        return contractAreaBusiness;
    }

    public void setContractAreaBusiness(String contractAreaBusiness) {
        this.contractAreaBusiness = contractAreaBusiness;
    }

    public Integer getKmContract() {
        return kmContract;
    }

    public void setKmContract(Integer kmContract) {
        this.kmContract = kmContract;
    }

    public Integer getMonthsDuration() {
        return monthsDuration;
    }

    public void setMonthsDuration(Integer monthsDuration) {
        this.monthsDuration = monthsDuration;
    }

    public String getBeginContract() {
        return beginContract;
    }

    public void setBeginContract(String beginContract) {
        this.beginContract = beginContract;
    }

    public String getEndContract() {
        return endContract;
    }

    public void setEndContract(String endContract) {
        this.endContract = endContract;
    }

    public String getSuspension() {
        return suspension;
    }

    public void setSuspension(String suspension) {
        this.suspension = suspension;
    }

    public String getReactivation() {
        return reactivation;
    }

    public void setReactivation(String reactivation) {
        this.reactivation = reactivation;
    }

    public String getLengthened() {
        return lengthened;
    }

    public void setLengthened(String lengthened) {
        this.lengthened = lengthened;
    }

    public String getCancellation() {
        return cancellation;
    }

    public void setCancellation(String cancellation) {
        this.cancellation = cancellation;
    }

    public String getReturned() {
        return returned;
    }

    public void setReturned(String returned) {
        this.returned = returned;
    }

    public String getLockStart() {
        return lockStart;
    }

    public void setLockStart(String lockStart) {
        this.lockStart = lockStart;
    }

    public String getLockEnd() {
        return lockEnd;
    }

    public void setLockEnd(String lockEnd) {
        this.lockEnd = lockEnd;
    }
}
