package com.doing.nemo.claims.controller.payload.request;

import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsFlowEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.enumerated.EmailTemplateEnum;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class EmailTemplateRequestV1 implements Serializable {

    private UUID id;

    @NotNull
    @JsonProperty("application_event")
    private EventTypeRequestV1 applicationEvent;

    @JsonProperty("type")
    private EmailTemplateEnum type;

    @NotNull
    @JsonProperty("description")
    private String description;

    @JsonProperty("object")
    private String object;

    @JsonProperty("heading")
    private String heading;

    @JsonProperty("body")
    private String body;

    @JsonProperty("foot")
    private String foot;

    @NotNull
    @JsonProperty("attach_file")
    private Boolean attachFile;

    @JsonProperty("splitting_recipients_email")
    private Boolean splittingRecipientsEmail;

    @JsonProperty("client_code")
    private String clientCode;

    @JsonProperty("customers")
    private List<RecipientTypeRequestV1> customers;

    @JsonProperty("is_active")
    private Boolean isActive = true;

    @JsonProperty("flows")
    private List<ClaimsFlowEnum> flows;

    @JsonProperty("accident_type_list")
    private List<DataAccidentTypeAccidentEnum> accidentTypeList;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public EventTypeRequestV1 getApplicationEvent() {
        return applicationEvent;
    }

    public void setApplicationEvent(EventTypeRequestV1 applicationEvent) {
        this.applicationEvent = applicationEvent;
    }

    public EmailTemplateEnum getType() {
        return type;
    }

    public void setType(EmailTemplateEnum type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getFoot() {
        return foot;
    }

    public void setFoot(String foot) {
        this.foot = foot;
    }

    public Boolean getAttachFile() {
        return attachFile;
    }

    public void setAttachFile(Boolean attachFile) {
        this.attachFile = attachFile;
    }

    public Boolean getSplittingRecipientsEmail() {
        return splittingRecipientsEmail;
    }

    public void setSplittingRecipientsEmail(Boolean splittingRecipientsEmail) {
        this.splittingRecipientsEmail = splittingRecipientsEmail;
    }

    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    public List<RecipientTypeRequestV1> getCustomers() {
        if(customers == null){
            return null;
        }
        return new ArrayList<>(customers);
    }

    public void setCustomers(List<RecipientTypeRequestV1> customers) {
        if(customers != null)
        {
            this.customers = new ArrayList<>(customers);
        } else {
            this.customers = null;
        }
    }

    public Boolean getActive() {
        return isActive;
    }

    @JsonSetter
    public void setActive(Boolean active) {
        if (active != null)
            isActive = active;
    }

    public List<ClaimsFlowEnum> getFlows() {
        if(flows == null){
            return null;
        }
        return new ArrayList<>(flows);
    }

    public void setFlows(List<ClaimsFlowEnum> flows) {
        if(flows != null)
        {
            this.flows =  new ArrayList<>(flows);
        } else {
            this.flows = null;
        }
    }

    public List<DataAccidentTypeAccidentEnum> getAccidentTypeList() {
        if(accidentTypeList == null){
            return null;
        }
        return new ArrayList<>(accidentTypeList);    }

    public void setAccidentTypeList(List<DataAccidentTypeAccidentEnum> accidentTypeList) {
        if(accidentTypeList != null)
        {
            this.accidentTypeList = new ArrayList<>(accidentTypeList);
        } else {
            this.accidentTypeList = null;
        }
    }

    @Override
    public String toString() {
        return "EmailTemplateRequestV1{" +
                "id=" + id +
                ", applicationEvent=" + applicationEvent +
                ", type=" + type +
                ", description='" + description + '\'' +
                ", object='" + object + '\'' +
                ", heading='" + heading + '\'' +
                ", body='" + body + '\'' +
                ", foot='" + foot + '\'' +
                ", attachFile=" + attachFile +
                ", splittingRecipientsEmail=" + splittingRecipientsEmail +
                ", clientCode='" + clientCode + '\'' +
                ", customers=" + customers +
                ", isActive=" + isActive +
                ", flows=" + flows +
                '}';
    }
}
