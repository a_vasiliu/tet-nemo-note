package com.doing.nemo.claims.controller.payload.response;

import com.doing.nemo.claims.entity.enumerated.ClaimsRepairEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.UUID;

public class ManagerResponseV1 implements Serializable {

    private UUID id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("address")
    private String address;

    @JsonProperty("zip_code")
    private String zipCode;

    @JsonProperty("locality")
    private String locality;

    @JsonProperty("prov")
    private String prov;

    @JsonProperty("country")
    private String country;

    @JsonProperty("phone")
    private String phone;

    @JsonProperty("fax")
    private String fax;

    @JsonProperty("email")
    private String email;

    @JsonProperty("website")
    private String website;

    @JsonProperty("contact")
    private String contact;

    @JsonProperty("external_export_id")
    private String externalExportId;

    @JsonProperty("is_active")
    private Boolean isActive;

    @JsonProperty("type_complaint")
    private ClaimsRepairEnum typeComplaint;

    public ManagerResponseV1() {
    }

    public ManagerResponseV1(UUID id, String name, String address, String zipCode, String locality, String prov, String country, String phone, String fax, String email, String website, String contact, String externalExportId, Boolean isActive, ClaimsRepairEnum typeComplaint) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.zipCode = zipCode;
        this.locality = locality;
        this.prov = prov;
        this.country = country;
        this.phone = phone;
        this.fax = fax;
        this.email = email;
        this.website = website;
        this.contact = contact;
        this.externalExportId = externalExportId;
        this.isActive = isActive;
        this.typeComplaint = typeComplaint;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getProv() {
        return prov;
    }

    public void setProv(String prov) {
        this.prov = prov;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getExternalExportId() {
        return externalExportId;
    }

    public void setExternalExportId(String externalExportId) {
        this.externalExportId = externalExportId;
    }

    @JsonIgnore
    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public ClaimsRepairEnum getTypeComplaint() {
        return typeComplaint;
    }

    public void setTypeComplaint(ClaimsRepairEnum typeComplaint) {
        this.typeComplaint = typeComplaint;
    }

    @Override
    public String toString() {
        return "ManagerResponseV1{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", zipCode=" + zipCode +
                ", locality='" + locality + '\'' +
                ", prov='" + prov + '\'' +
                ", country='" + country + '\'' +
                ", phone='" + phone + '\'' +
                ", fax='" + fax + '\'' +
                ", email='" + email + '\'' +
                ", website='" + website + '\'' +
                ", contact='" + contact + '\'' +
                ", externalExportId='" + externalExportId + '\'' +
                ", isActive=" + isActive +
                '}';
    }
}
