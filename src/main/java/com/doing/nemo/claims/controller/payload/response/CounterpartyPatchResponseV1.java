package com.doing.nemo.claims.controller.payload.response;

import com.doing.nemo.claims.controller.payload.response.authority.repair.AuthorityRepairResponseV1;
import com.doing.nemo.claims.controller.payload.response.counterparty.CanalizationResponse;
import com.doing.nemo.claims.controller.payload.response.counterparty.InsuredResponse;
import com.doing.nemo.claims.controller.payload.response.counterparty.LastContactResponse;
import com.doing.nemo.claims.controller.payload.response.damaged.DriverResponse;
import com.doing.nemo.claims.controller.payload.response.damaged.ImpactPointResponse;
import com.doing.nemo.claims.controller.payload.response.damaged.VehicleResponse;
import com.doing.nemo.claims.entity.enumerated.DamagedEnum.VehicleEnum.VehicleTypeEnum;
import com.doing.nemo.claims.entity.enumerated.RepairEnum.RepairProcedureEnum;
import com.doing.nemo.claims.entity.enumerated.RepairEnum.RepairStatusEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CounterpartyPatchResponseV1 implements Serializable {
    @JsonProperty("claims_id")
    private String claimsId;
    @JsonProperty("counterparty_id")
    private String counterpartyId;
    @JsonProperty("practice_id_counterparty")
    private Long practiceIdCounterparty;
    @JsonProperty("user_create")
    private String userCreate;
    @JsonProperty("motivation")
    private String motivation;
    @JsonProperty("created_at")
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    private String createdAt;
    @JsonProperty("repair_created_at")
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    private String repairCreatedAt;
    @JsonProperty("update_at")
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    private String updateAt;
    @JsonProperty("type")
    private VehicleTypeEnum type;
    @JsonProperty("repair_status")
    private RepairStatusEnum repairStatus;
    @JsonProperty("procedure_repair")
    private RepairProcedureEnum repairProcedure;
    @JsonProperty("user_update")
    private String userUpdate;
    @JsonProperty("assigned_to")
    private String assignedTo;
    @JsonProperty("assigned_at")
    private String assignedAt;
    @JsonProperty("insured")
    private InsuredResponse insured;
    @JsonProperty("insurance_company")
    private InsuranceCompanyCounterpartyResponse insuranceCompany;
    @JsonProperty("driver")
    private DriverResponse driver;
    @JsonProperty("vehicle")
    private VehicleResponse vehicle;
    @JsonProperty("is_cai_signed")
    private Boolean isCaiSigned;
    @JsonProperty("impact_point")
    private ImpactPointResponse impactPoint;
    @JsonProperty("responsible")
    private Boolean responsible;
    @JsonProperty("eligibility")
    private Boolean eligibility;
    @JsonProperty("replacement_car")
    private Boolean replacementCar;
    @JsonProperty("replacement_plate")
    private String replacementPlate;
    @JsonProperty("description")
    private String description;
    @JsonProperty("last_contact")
    private List<LastContactResponse> lastContact;
    @JsonProperty("manager")
    private ManagerResponseV1 manager;
    @JsonProperty("canalization")
    private CanalizationResponse canalization;
    @JsonProperty("is_ald")
    private Boolean isAld;
    @JsonProperty("authorities")
    private List<AuthorityRepairResponseV1> authorities;
    @JsonProperty("is_read_msa")
    private Boolean isRead;

    public String getReplacementPlate() {
        return replacementPlate;
    }

    public void setReplacementPlate(String replacementPlate) {
        this.replacementPlate = replacementPlate;
    }

    public String getMotivation() {
        return motivation;
    }

    public void setMotivation(String motivation) {
        this.motivation = motivation;
    }

    public String getRepairCreatedAt() {
        return repairCreatedAt;
    }

    public void setRepairCreatedAt(String repairCreatedAt) {
        this.repairCreatedAt = repairCreatedAt;
    }

    public Boolean getReplacementCar() {
        return replacementCar;
    }

    public void setReplacementCar(Boolean replacementCar) {
        this.replacementCar = replacementCar;
    }

    public List<AuthorityRepairResponseV1> getAuthorities() {
        if(authorities == null){
            return null;
        }
        return new ArrayList<>(authorities);
    }

    public void setAuthorities(List<AuthorityRepairResponseV1> authorities) {
        if(authorities != null)
        {
            this.authorities =new ArrayList<>(authorities);
        } else {
            this.authorities = null;
        }
    }

    public Long getPracticeIdCounterparty() {
        return practiceIdCounterparty;
    }

    public void setPracticeIdCounterparty(Long practiceIdCounterparty) {
        this.practiceIdCounterparty = practiceIdCounterparty;
    }

    public Boolean getRead() {
        return isRead;
    }

    public void setRead(Boolean read) {
        isRead = read;
    }

    public Boolean getAld() {
        return isAld;
    }

    public void setAld(Boolean ald) {
        isAld = ald;
    }

    public String getClaimsId() {
        return claimsId;
    }

    public void setClaimsId(String claimsId) {
        this.claimsId = claimsId;
    }

    public String getCounterpartyId() {
        return counterpartyId;
    }

    public void setCounterpartyId(String counterpartyId) {
        this.counterpartyId = counterpartyId;
    }

    public String getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(String updateAt) {
        this.updateAt = updateAt;
    }

    public VehicleTypeEnum getType() {
        return type;
    }

    public void setType(VehicleTypeEnum type) {
        this.type = type;
    }

    public RepairStatusEnum getRepairStatus() {
        return repairStatus;
    }

    public void setRepairStatus(RepairStatusEnum repairStatus) {
        this.repairStatus = repairStatus;
    }

    public RepairProcedureEnum getRepairProcedure() {
        return repairProcedure;
    }

    public void setRepairProcedure(RepairProcedureEnum repairProcedure) {
        this.repairProcedure = repairProcedure;
    }

    public String getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(String userUpdate) {
        this.userUpdate = userUpdate;
    }

    public String getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(String assignedTo) {
        this.assignedTo = assignedTo;
    }

    public String getAssignedAt() {
        return assignedAt;
    }

    public void setAssignedAt(String assignedAt) {
        this.assignedAt = assignedAt;
    }

    public InsuredResponse getInsured() {
        return insured;
    }

    public void setInsured(InsuredResponse insured) {
        this.insured = insured;
    }

    public InsuranceCompanyCounterpartyResponse getInsuranceCompany() {
        return insuranceCompany;
    }

    public void setInsuranceCompany(InsuranceCompanyCounterpartyResponse insuranceCompany) {
        this.insuranceCompany = insuranceCompany;
    }

    public DriverResponse getDriver() {
        return driver;
    }

    public void setDriver(DriverResponse driver) {
        this.driver = driver;
    }

    public VehicleResponse getVehicle() {
        return vehicle;
    }

    public void setVehicle(VehicleResponse vehicle) {
        this.vehicle = vehicle;
    }

    @JsonIgnore
    public Boolean getCaiSigned() {
        return isCaiSigned;
    }

    public void setCaiSigned(Boolean caiSigned) {
        isCaiSigned = caiSigned;
    }

    public ImpactPointResponse getImpactPoint() {
        return impactPoint;
    }

    public void setImpactPoint(ImpactPointResponse impactPoint) {
        this.impactPoint = impactPoint;
    }

    public Boolean getResponsible() {
        return responsible;
    }

    public void setResponsible(Boolean responsible) {
        this.responsible = responsible;
    }

    public Boolean getEligibility() {
        return eligibility;
    }

    public void setEligibility(Boolean eligibility) {
        this.eligibility = eligibility;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<LastContactResponse> getLastContact() {
        if(lastContact == null){
            return null;
        }
        return new ArrayList<>(lastContact);
    }

    public void setLastContact(List<LastContactResponse> lastContact) {
        if(lastContact != null)
        {
            this.lastContact = new ArrayList<>(lastContact);
        } else {
            this.lastContact = null;
        }
    }

    public ManagerResponseV1 getManager() {
        return manager;
    }

    public void setManager(ManagerResponseV1 manager) {
        this.manager = manager;
    }

    public CanalizationResponse getCanalization() {
        return canalization;
    }

    public void setCanalization(CanalizationResponse canalization) {
        this.canalization = canalization;
    }
}
