package com.doing.nemo.claims.controller.payload.request;

import com.doing.nemo.claims.entity.enumerated.AntitheftEnum.AntitheftTypeEnum;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class RegistryRequestV1 implements Serializable {

    @JsonProperty("type")
    private AntitheftTypeEnum typeEntity;

    @JsonProperty("id_transaction")
    private Long idTransaction;

    @JsonProperty("cod_imei")
    private String codImei;

    @JsonProperty("cod_pack")
    private String codPack;

    @JsonProperty("cod_provider")
    private Integer codProvider;

    @JsonProperty("provider")
    private String provider;

    @JsonProperty("date_activation")
    private String dateActivation;

    @JsonProperty("date_end")
    private String dateEnd;

    @JsonProperty("cod_serialnumber")
    private String codSerialnumber;

    @JsonProperty("voucher_id")
    private Integer voucherId;

    public RegistryRequestV1(String codeAntiTheftService, AntitheftTypeEnum typeEntity, Long idTransaction, String codImei, String codPack, Integer codProvider, String provider, String dateActivation, String dateEnd, String codSerialnumber, Integer voucherId) {
        this.typeEntity = typeEntity;
        this.idTransaction = idTransaction;
        this.codImei = codImei;
        this.codPack = codPack;
        this.codProvider = codProvider;
        this.provider = provider;
        this.dateActivation = dateActivation;
        this.dateEnd = dateEnd;
        this.codSerialnumber = codSerialnumber;
        this.voucherId = voucherId;
    }

    public AntitheftTypeEnum getTypeEntity() {
        return typeEntity;
    }



    public Long getIdTransaction() {
        return idTransaction;
    }

    public void setIdTransaction(Long idTransaction) {
        this.idTransaction = idTransaction;
    }

    public String getCodImei() {
        return codImei;
    }

    public void setCodImei(String codImei) {
        this.codImei = codImei;
    }

    public String getCodPack() {
        return codPack;
    }

    public void setCodPack(String codPack) {
        this.codPack = codPack;
    }

    public Integer getCodProvider() {
        return codProvider;
    }

    public void setCodProvider(Integer codProvider) {
        this.codProvider = codProvider;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getDateActivation() {
        return dateActivation;
    }

    public void setDateActivation(String dateActivation) {
        this.dateActivation = dateActivation;
    }

    public String getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    public String getCodSerialnumber() {
        return codSerialnumber;
    }

    public void setCodSerialnumber(String codSerialnumber) {
        this.codSerialnumber = codSerialnumber;
    }

    public Integer getVoucherId() {
        return voucherId;
    }

    public void setVoucherId(Integer voucherId) {
        this.voucherId = voucherId;
    }

    @Override
    public String toString() {
        return "RegistryRequestV1{" +
                ", idTransaction=" + idTransaction +
                ", codImei='" + codImei + '\'' +
                ", codPack='" + codPack + '\'' +
                ", codProvider=" + codProvider +
                ", provider='" + provider + '\'' +
                ", dateActivation=" + dateActivation +
                ", dateEnd=" + dateEnd +
                ", codSerialnumber='" + codSerialnumber + '\'' +
                ", voucherId=" + voucherId +
                '}';
    }
}
