package com.doing.nemo.claims.controller.payload;

import com.doing.nemo.claims.entity.settings.SubgroupMappingEntity;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

public class SubgroupIdsEntityV1 {

    private UUID id;

    @JsonProperty("user_id")
    private Long userId;

    @JsonProperty("subgroup_mapping")
    private SubgroupMappingEntity subgroupMappingEntity;

    public SubgroupIdsEntityV1() {
    }

    public SubgroupIdsEntityV1(UUID id, Long userId, SubgroupMappingEntity subgroupMappingEntity) {
        this.id = id;
        this.userId = userId;
        this.subgroupMappingEntity = subgroupMappingEntity;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public SubgroupMappingEntity getSubgroupMappingEntity() {
        return subgroupMappingEntity;
    }

    public void setSubgroupMappingEntity(SubgroupMappingEntity subgroupMappingEntity) {
        this.subgroupMappingEntity = subgroupMappingEntity;
    }
}
