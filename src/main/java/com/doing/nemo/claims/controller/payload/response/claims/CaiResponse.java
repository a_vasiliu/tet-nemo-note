package com.doing.nemo.claims.controller.payload.response.claims;

import com.doing.nemo.claims.controller.payload.response.cai.CaiDetailsResponse;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class CaiResponse  implements Serializable {

    @JsonProperty("A")
    private CaiDetailsResponse vehicleA;

    @JsonProperty("B")
    private CaiDetailsResponse vehicleB;

    @JsonProperty("driver_side")
    private String driverSide;

    @JsonProperty("note")
    private String note;

    public CaiResponse() {
    }

    public CaiResponse(CaiDetailsResponse vehicleA, CaiDetailsResponse vehicleB, String driverSide, String note) {
        this.vehicleA = vehicleA;
        this.vehicleB = vehicleB;
        this.driverSide = driverSide;
        this.note = note;
    }

    public CaiDetailsResponse getVehicleA() {
        return vehicleA;
    }

    public void setVehicleA(CaiDetailsResponse vehicleA) {
        this.vehicleA = vehicleA;
    }

    public CaiDetailsResponse getVehicleB() {
        return vehicleB;
    }

    public void setVehicleB(CaiDetailsResponse vehicleB) {
        this.vehicleB = vehicleB;
    }

    public String getDriverSide() {
        return driverSide;
    }

    public void setDriverSide(String driverSide) {
        this.driverSide = driverSide;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public String toString() {
        return "CaiResponse{" +
                "vehicleA=" + vehicleA +
                ", vehicleB=" + vehicleB +
                ", driverSide='" + driverSide + '\'' +
                ", note='" + note + '\'' +
                '}';
    }
}
