package com.doing.nemo.claims.controller.payload.request.damaged;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AdditionalCostsRequest  implements Serializable {

    @JsonProperty("typology")
    private String typology;

    @JsonProperty("amount")
    private Double amount;

    @JsonProperty("date")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date date;

    @JsonProperty("attachment_list")
    private List<String> attachments;


    public List<String> getAttachments() {
        if(attachments == null){
            return null;
        }
        return new ArrayList<>(attachments);
    }

    public void setAttachments(List<String> attachments) {
        if(attachments != null)
        {
            this.attachments = new ArrayList<>(attachments);
        } else {
            this.attachments = null;
        }
    }

    public String getTypology() {
        return typology;
    }

    public void setTypology(String typology) {
        this.typology = typology;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Date getDate() {
        if(date == null){
            return null;
        }
        return (Date)date.clone();
    }

    public void setDate(Date date) {
        if(date != null)
        {
            this.date = (Date) date.clone();
        } else {
            this.date = null;
        }
    }

    @Override
    public String toString() {
        return "AdditionalCostsRequest{" +
                "typology='" + typology + '\'' +
                ", amount=" + amount +
                ", date=" + date +
                '}';
    }
}
