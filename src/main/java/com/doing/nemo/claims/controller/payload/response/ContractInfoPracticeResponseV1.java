package com.doing.nemo.claims.controller.payload.response;


import com.doing.nemo.claims.controller.payload.response.damaged.ContractResponse;
import com.doing.nemo.claims.controller.payload.response.damaged.CustomerResponse;
import com.doing.nemo.claims.controller.payload.response.damaged.VehicleResponse;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ContractInfoPracticeResponseV1 implements Serializable {

    @JsonProperty("contract")
    private ContractResponse contractResponse;

    @JsonProperty("vehicle")
    private VehicleResponse vehicleResponse;

    @JsonProperty("customer")
    private CustomerResponse customerResponse;

    @JsonProperty("fleet_managers")
    private List<FleetManagerResponseV1> fleetManagerResponseList;

    public ContractResponse getContractResponse() {
        return contractResponse;
    }

    public void setContractResponse(ContractResponse contractResponse) {
        this.contractResponse = contractResponse;
    }

    public VehicleResponse getVehicleResponse() {
        return vehicleResponse;
    }

    public void setVehicleResponse(VehicleResponse vehicleResponse) {
        this.vehicleResponse = vehicleResponse;
    }

    public CustomerResponse getCustomerResponse() {
        return customerResponse;
    }

    public void setCustomerResponse(CustomerResponse customerResponse) {
        this.customerResponse = customerResponse;
    }

    public List<FleetManagerResponseV1> getFleetManagerResponseList() {
        if(fleetManagerResponseList == null){
            return null;
        }
        return new ArrayList<>(fleetManagerResponseList);
    }

    public void setFleetManagerResponseList(List<FleetManagerResponseV1> fleetManagerResponseList) {
        if(fleetManagerResponseList != null)
        {
            this.fleetManagerResponseList = new ArrayList<>(fleetManagerResponseList);
        } else {
            this.fleetManagerResponseList = null;
        }
    }
}