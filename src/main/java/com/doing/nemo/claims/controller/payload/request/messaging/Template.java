package com.doing.nemo.claims.controller.payload.request.messaging;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Map;

public class Template implements Serializable {

    @NotNull
    private String name;
    private Map<String, String> subjectPlaceholders;
    private Map<String, String> contentPlaceholders;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, String> getSubjectPlaceholders() {
        return subjectPlaceholders;
    }

    public void setSubjectPlaceholders(Map<String, String> subjectPlaceholders) {
        this.subjectPlaceholders = subjectPlaceholders;
    }

    public Map<String, String> getContentPlaceholders() {
        return contentPlaceholders;
    }

    public void setContentPlaceholders(Map<String, String> contentPlaceholders) {
        this.contentPlaceholders = contentPlaceholders;
    }
}
