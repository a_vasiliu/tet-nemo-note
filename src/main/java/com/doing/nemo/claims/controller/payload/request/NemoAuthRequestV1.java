package com.doing.nemo.claims.controller.payload.request;

/*example

{
	"refresh_token": "Y2ZjNTA1NmNmNmY1YThlZjRlNmU0ZTRlMTNkMDE3Yzc0NmVlMDZmNjA4YjE4ZjU2YTAwMWY1ZDE1MDEzMzBmNg",
	"metadata" : {
		"email": "marco.chillemi@doing.com"
	}
}

 */

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Map;

public class NemoAuthRequestV1 implements Serializable {

    @JsonProperty("refresh_token")
    private String refreshToken;

    @JsonProperty("metadata")
    private Map<String,String> metadata;

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public Map<String, String> getMetadata() {
        return metadata;
    }

    public void setMetadata(Map<String, String> metadata) {
        this.metadata = metadata;
    }

    @Override
    public String toString() {
        return "NemoAuthRequestV1{" +
                "refreshToken='" + refreshToken + '\'' +
                ", metadata=" + metadata +
                '}';
    }
}
