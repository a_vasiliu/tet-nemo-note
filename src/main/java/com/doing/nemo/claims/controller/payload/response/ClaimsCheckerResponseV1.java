package com.doing.nemo.claims.controller.payload.response;


import com.doing.nemo.claims.controller.payload.response.claims.Claims;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class ClaimsCheckerResponseV1 implements Serializable {

    @JsonProperty("code")
    private String code;

    @JsonProperty("message")
    private String message;

    @JsonProperty("claims")
    private List<Claims> claims;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Claims> getClaims() {
        if(claims == null){
            return null;
        }
        return new ArrayList<>(claims);
    }

    public void setClaims(List<Claims> claims) {
        if(claims != null)
        {
            this.claims =  new ArrayList<>(claims);
        } else {
            this.claims = null;
        }
    }

    @Override
    public String toString() {
        return "ClaimsCheckerResponseV1{" +
                "code=" + code +
                ", message='" + message + '\'' +
                ", claims=" + claims +
                '}';
    }





}
