package com.doing.nemo.claims.controller;

import com.doing.nemo.claims.adapter.AutomaticAffiliationRuleInspectorateAdapter;
import com.doing.nemo.claims.adapter.InspectorateAdapter;
import com.doing.nemo.claims.common.validation.MessageCode;
import com.doing.nemo.claims.common.validation.RequestValidator;
import com.doing.nemo.claims.controller.payload.request.*;
import com.doing.nemo.claims.controller.payload.response.IdResponseV1;
import com.doing.nemo.claims.controller.payload.response.InspectorateResponseV1;
import com.doing.nemo.claims.controller.payload.response.PaginationResponseV1;
import com.doing.nemo.claims.entity.settings.AutomaticAffiliationRuleInspectorateEntity;
import com.doing.nemo.claims.entity.settings.InspectorateEntity;
import com.doing.nemo.claims.service.InspectorateService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/v1")
@Api("Inspectorate")
public class InspectorateController {

    @Autowired
    private InspectorateService inspectorateService;

    @Autowired
    private InspectorateAdapter inspectorateAdapter;

    @Autowired
    private AutomaticAffiliationRuleInspectorateAdapter automaticAffiliationRuleInspectorateAdapter;

    @Autowired
    private RequestValidator requestValidator;

    @PostMapping("/inspectorate")
    @Transactional
    @ApiOperation(value = "Insert Inspectorate", notes = "Insert Inspectorate using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = IdResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<IdResponseV1> postInspectorate(@ApiParam(value = "Body of the Inspectorate to be created", required = true) @RequestBody InspectorateRequestV1 inspectorateRequestV1) {
        requestValidator.validateRequest(inspectorateRequestV1, MessageCode.E00X_1000);
        InspectorateEntity inspectorateEntity = inspectorateAdapter.adptFromInspectorateRequestToInspectorateEntity(inspectorateRequestV1, HttpMethod.POST);
        inspectorateEntity = inspectorateService.insertInspectorate(inspectorateEntity);
        IdResponseV1 inspectorateResponseIdV1 = new IdResponseV1(inspectorateEntity.getId());
        return new ResponseEntity(inspectorateResponseIdV1, HttpStatus.CREATED);
    }

    @PostMapping("/inspectorate/{UUID}")
    @Transactional
    @ApiOperation(value = "Insert Rules for Inspectorate", notes = "Insert rules for inspectorate using body in the request")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = InspectorateResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<InspectorateResponseV1> postRulesForInspectorate(@PathVariable(name = "UUID") UUID id,
                                                                           @ApiParam(value = "Body of the Rule to be created", required = true) @RequestBody ListRequestV1<AutomaticAffiliationRuleInspectorateRequestV1> automaticAffiliationRuleInspectorateRequestV1List) {
        List<AutomaticAffiliationRuleInspectorateEntity> automaticAffiliationRuleInspectorateEntity = automaticAffiliationRuleInspectorateAdapter.adptFromAutomaticAffiliationRuleInspectorateRequestToAutomaticAffiliationRuleInspectorateEntityList(automaticAffiliationRuleInspectorateRequestV1List.getData(), HttpMethod.POST);
        InspectorateEntity inspectorateEntity = inspectorateService.insertInspectorateRule(automaticAffiliationRuleInspectorateEntity, id);

        InspectorateResponseV1<InspectorateEntity> responseV1 = InspectorateAdapter.adptFromInspectorateEntityToInspectorateResponseV1(inspectorateEntity);
        return new ResponseEntity<>(responseV1, HttpStatus.CREATED);
    }

    @PostMapping("/inspectorates")
    @Transactional
    @ApiOperation(value = "Insert More Inspectorates", notes = "Insert Inspectorates using info passed in the bodies")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = InspectorateResponseV1.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<InspectorateResponseV1>> postInspectorates(@ApiParam(value = "Bodies of the Inspectorates to be created", required = true) @RequestBody ListRequestV1<InspectorateRequestV1> inspectorateListRequestV1) {

        List<InspectorateResponseV1> inspectorateResponseV1List = new ArrayList<>();
        for (InspectorateRequestV1 inspectorateRequestV1 : inspectorateListRequestV1.getData()) {
            requestValidator.validateRequest(inspectorateRequestV1, MessageCode.E00X_1000);
            InspectorateEntity inspectorateEntity = inspectorateAdapter.adptFromInspectorateRequestToInspectorateEntity(inspectorateRequestV1, HttpMethod.POST);
            inspectorateEntity = inspectorateService.insertInspectorate(inspectorateEntity);
            InspectorateResponseV1 inspectorateResponseV1 = InspectorateAdapter.adptFromInspectorateEntityToInspectorateResponseV1(inspectorateEntity);
            inspectorateResponseV1List.add(inspectorateResponseV1);
        }
        return new ResponseEntity(inspectorateResponseV1List, HttpStatus.CREATED);
    }

    @PutMapping("/inspectorate/{UUID}")
    @Transactional
    @ApiOperation(value = "Upload Inspectorate", notes = "Upload Inspectorate using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = InspectorateResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<InspectorateResponseV1> putInspectorate(@ApiParam(value = "UUID that identifies the Inspectorate to be modified", required = true) @PathVariable(name = "UUID") UUID id, @ApiParam(value = "Updated body of the Inspectorate", required = true)
    @RequestBody InspectorateRequestV1 inspectorateRequestV1) {
        requestValidator.validateRequest(inspectorateRequestV1, MessageCode.E00X_1000);
        InspectorateEntity inspectorateEntity = inspectorateService.selectInspectorate(id);
        InspectorateEntity inspectorateEntityNew = inspectorateAdapter.adptFromInspectorateRequestToInspectorateEntity(inspectorateRequestV1, HttpMethod.PUT);
        inspectorateEntityNew.setId(inspectorateEntity.getId());
        inspectorateEntityNew = inspectorateService.updateInspectorate(inspectorateEntityNew, id);
        InspectorateResponseV1<InspectorateEntity> inspectorateResponseV1 = InspectorateAdapter.adptFromInspectorateEntityToInspectorateResponseV1(inspectorateEntityNew);
        return new ResponseEntity<>(inspectorateResponseV1, HttpStatus.OK);
    }

    @GetMapping("/inspectorate/{UUID}")
    @Transactional
    @ApiOperation(value = "Recover Inspectorate", notes = "Return a Inspectorate using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = InspectorateResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<InspectorateResponseV1> getInspectorate(@ApiParam(value = "UUID of the Inspectorate to be found", required = true) @PathVariable(name = "UUID") UUID id) {
        InspectorateEntity inspectorateEntity = inspectorateService.selectInspectorate(id);
        InspectorateResponseV1 inspectorateResponseV1 = InspectorateAdapter.adptFromInspectorateEntityToInspectorateResponseV1(inspectorateEntity);
        return new ResponseEntity<>(inspectorateResponseV1, HttpStatus.OK);
    }

    @GetMapping("/inspectorate")
    @Transactional
    @ApiOperation(value = "Recover All Inspectorates", notes = "Returns all Inspectorates")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = InspectorateResponseV1.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<InspectorateResponseV1>> getAllInspectorate() {
        List<InspectorateEntity> inspectorateEntityList = inspectorateService.selectAllInspectorate();
        List<InspectorateResponseV1<InspectorateEntity>> inspectorateResponseV1List = InspectorateAdapter.adptFromInspectorateEntityToInspectorateResponseV1List(inspectorateEntityList);
        return new ResponseEntity(inspectorateResponseV1List, HttpStatus.OK);
    }

    @DeleteMapping("/inspectorate/{UUID}")
    @Transactional
    @ApiOperation(value = "Delete Inspectorate", notes = "Delete a Inspectorate using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = InspectorateResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<InspectorateResponseV1> deleteInspectorate(@ApiParam(value = "UUID of the Inspectorate to be deleted", required = true) @PathVariable(name = "UUID") UUID id) {
        InspectorateResponseV1<InspectorateEntity> inspectorateResponseV1 = inspectorateService.deleteInspectorate(id);
        return new ResponseEntity<>(inspectorateResponseV1, HttpStatus.OK);
    }

    @PatchMapping("/inspectorate/{UUID}/active")
    @Transactional
    @ApiOperation(value = "Patch active status Inspectorate", notes = "Patch active status of Inspectorate using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = InspectorateResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<InspectorateResponseV1> patchStatusInspectorate(
            @PathVariable UUID UUID
    ) {
        InspectorateEntity inspectorateEntity = inspectorateService.updateStatus(UUID);
        InspectorateResponseV1<InspectorateEntity> responseV1 = InspectorateAdapter.adptFromInspectorateEntityToInspectorateResponseV1(inspectorateEntity);
        return new ResponseEntity<>(responseV1, HttpStatus.OK);
    }


    @ApiOperation(value="Search Inspectorate", notes = "It retrieves inspectorate data through one of more parameters")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = PaginationResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @GetMapping(value="/inspectorate/pagination", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<PaginationResponseV1<InspectorateResponseV1>> searchInspectorate(
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @RequestParam(value = "page_size", defaultValue = "20") Integer pageSize,
            @RequestParam(value = "order_by", required = false) String orderBy,
            @RequestParam(value = "asc", required = false, defaultValue = "true") Boolean asc,
            @RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "email", required = false) String email,
            @RequestParam(value = "province", required = false) String province,
            @RequestParam(value = "telephone", required = false) String telephone,
            @RequestParam(value = "fax", required = false) String fax,
            @RequestParam(value = "is_active", required = false) Boolean isActive,
            @RequestParam(value = "address", required = false) String address,
            @RequestParam(value = "zip_code", required = false) String zipCode,
            @RequestParam(value = "city", required = false) String city,
            @RequestParam(value = "state", required = false) String state,
            @RequestParam(value = "code", required = false) Long code
    ){

        List<InspectorateEntity> listInspectorate = inspectorateService.findInspectoratesFiltered(page,pageSize, orderBy, asc, name, email, province, telephone, fax, isActive, address, zipCode, city, state, code);
        Long itemCount = inspectorateService.countInspectorateFiltered(name, email, province, telephone, fax, isActive, address, zipCode, city, state, code);

        PaginationResponseV1<InspectorateResponseV1> paginationResponseV1 = InspectorateAdapter.adptPagination(listInspectorate, page, pageSize, itemCount);

        return new ResponseEntity<>(paginationResponseV1, HttpStatus.OK);
    }

    @ApiOperation(value="Reset rule", notes = "Job for reset inspectorate rule")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/inspectorate/rule/reset")
    public void resetInspectorateRule(){
        inspectorateService.resetRuleJob();
    }

}
