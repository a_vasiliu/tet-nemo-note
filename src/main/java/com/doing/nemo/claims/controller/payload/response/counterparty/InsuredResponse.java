package com.doing.nemo.claims.controller.payload.response.counterparty;

import com.doing.nemo.claims.controller.payload.response.driver.DrivingLicenseResponse;
import com.doing.nemo.claims.entity.jsonb.Address;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class InsuredResponse  implements Serializable {

    @JsonProperty("customer_id")
    private String customerId;

    @JsonProperty("firstname")
    private String firstname;

    @JsonProperty("lastname")
    private String lastname;

    @JsonProperty("fiscal_code")
    private String fiscalCode;

    @JsonProperty("address")
    private Address address;

    @JsonProperty("driving_license")
    private DrivingLicenseResponse drivingLicense;

    @JsonProperty("phone")
    private String phone;

    @JsonProperty("email")
    private String email;

    public InsuredResponse() {
    }

    public InsuredResponse(String customerId, String firstname, String lastname, String fiscalCode, Address address, DrivingLicenseResponse drivingLicense, String phone, String email) {
        this.customerId = customerId;
        this.firstname = firstname;
        this.lastname = lastname;
        this.fiscalCode = fiscalCode;
        this.address = address;
        this.drivingLicense = drivingLicense;
        this.phone = phone;
        this.email = email;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFiscalCode() {
        return fiscalCode;
    }

    public void setFiscalCode(String fiscalCode) {
        this.fiscalCode = fiscalCode;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public DrivingLicenseResponse getDrivingLicense() {
        return drivingLicense;
    }

    public void setDrivingLicense(DrivingLicenseResponse drivingLicense) {
        this.drivingLicense = drivingLicense;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Insured{" +
                "firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", fiscalCode='" + fiscalCode + '\'' +
                ", address=" + address +
                ", drivingLicense=" + drivingLicense +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                '}';
    }

}