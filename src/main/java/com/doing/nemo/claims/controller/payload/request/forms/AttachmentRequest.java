package com.doing.nemo.claims.controller.payload.request.forms;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;

import java.io.Serializable;

public class AttachmentRequest  implements Serializable {

    @JsonProperty("file_manager_id")
    private String fileManagerId;

    @JsonProperty("file_manager_name")
    private String fileManagerName;

    @JsonProperty("blob_name")
    private String blobName;

    @JsonProperty("blob_size")
    private Integer blobSize;

    @JsonProperty("mime_type")
    private String mimeType;

    @JsonProperty("description")
    private String description;

    @JsonProperty("created_at")
    private String createdAt;

    @JsonProperty("hidden")
    private Boolean hidden = false;

    @JsonProperty("validate")
    private Boolean validate = false;

    public AttachmentRequest() {
    }

    public AttachmentRequest(String fileManagerId, String fileManagerName, String blobName, Integer blobSize, String mimeType, String description, String createdAt, Boolean hidden, Boolean validate) {
        this.fileManagerId = fileManagerId;
        this.fileManagerName = fileManagerName;
        this.blobName = blobName;
        this.blobSize = blobSize;
        this.mimeType = mimeType;
        this.description = description;
        this.createdAt = createdAt;
        this.hidden = hidden;
        this.validate = validate;
    }

    public String getFileManagerId() {
        return fileManagerId;
    }

    public void setFileManagerId(String fileManagerId) {
        this.fileManagerId = fileManagerId;
    }

    public String getFileManagerName() {
        return fileManagerName;
    }

    public void setFileManagerName(String fileManagerName) {
        this.fileManagerName = fileManagerName;
    }

    public String getBlobName() {
        return blobName;
    }

    public void setBlobName(String blobName) {
        this.blobName = blobName;
    }

    public Integer getBlobSize() {
        return blobSize;
    }

    public void setBlobSize(Integer blobSize) {
        this.blobSize = blobSize;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Boolean getHidden() {
        return hidden;
    }

    @JsonSetter
    public void setHidden(Boolean hidden) {
        if (hidden != null)
            this.hidden = hidden;
    }

    public Boolean getValidate() {
        return validate;
    }

    @JsonSetter
    public void setValidate(Boolean validate) {
        if(validate != null)
            this.validate = validate;
    }

    @Override
    public String toString() {
        return "AttachmentRequest{" +
                "fileManagerId='" + fileManagerId + '\'' +
                ", fileManagerName='" + fileManagerName + '\'' +
                ", blobName='" + blobName + '\'' +
                ", blobSize=" + blobSize +
                ", mimeType='" + mimeType + '\'' +
                ", description='" + description + '\'' +
                ", createdAt=" + createdAt +
                ", hidden=" + hidden +
                '}';
    }
}
