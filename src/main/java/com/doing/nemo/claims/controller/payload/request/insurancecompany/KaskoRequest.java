package com.doing.nemo.claims.controller.payload.request.insurancecompany;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class KaskoRequest  implements Serializable {
    @JsonProperty("deductible_id")
    private Integer deductibleId;
    @JsonProperty("deductible_value")
    private String deductibleValue;

    public KaskoRequest() {
    }

    public KaskoRequest(Integer deductibleId, String deductibleValue) {
        this.deductibleId = deductibleId;
        this.deductibleValue = deductibleValue;
    }

    public Integer getDeductibleId() {
        return deductibleId;
    }

    public void setDeductibleId(Integer deductibleId) {
        this.deductibleId = deductibleId;
    }

    public String getDeductibleValue() {
        return deductibleValue;
    }

    public void setDeductibleValue(String deductibleValue) {
        this.deductibleValue = deductibleValue;
    }
}
