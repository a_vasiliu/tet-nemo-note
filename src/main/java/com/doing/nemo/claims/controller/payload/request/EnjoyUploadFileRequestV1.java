package com.doing.nemo.claims.controller.payload.request;

import com.doing.nemo.claims.controller.payload.UploadFileClaimsRequestV1;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class EnjoyUploadFileRequestV1 implements Serializable {
    @JsonProperty("data")
    private List<UploadFileClaimsRequestV1> data;

    public EnjoyUploadFileRequestV1() {
    }

    public EnjoyUploadFileRequestV1(List<UploadFileClaimsRequestV1> data) {
        if(data != null)
        {
            this.data = new ArrayList<>(data);
        } else {
            this.data = null;
        }
    }

    public List<UploadFileClaimsRequestV1> getData() {
        if(data == null){
            return null;
        }
        return new ArrayList<>(data);
    }

    public void setData(List<UploadFileClaimsRequestV1> data) {
        if(data != null)
        {
            this.data = new ArrayList<>(data);
        } else {
            this.data = null;
        }
    }

    @Override
    public String toString() {
        return "EnjoyUploadFileRequestV1{" +
                "data=" + data +
                '}';
    }
}
