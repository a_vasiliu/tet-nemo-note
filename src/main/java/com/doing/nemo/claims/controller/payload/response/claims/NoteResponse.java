package com.doing.nemo.claims.controller.payload.response.claims;

import com.doing.nemo.claims.entity.enumerated.NotesEnum.NotesTypeEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class NoteResponse implements Serializable {

    @JsonProperty("title")
    private String noteTitle;

    @JsonProperty("note_type")
    private NotesTypeEnum noteType;

    @JsonProperty("note_id")
    private String noteId;

    @JsonProperty("is_important")
    private Boolean isImportant;

    @JsonProperty("description")
    private String description;

    @JsonProperty("created_at")
    private String createdAt;

    @JsonProperty("user_id")
    private String userId;

    @JsonProperty("hidden")
    private Boolean hidden;

    public NoteResponse() {
    }

    public NoteResponse(String noteTitle, NotesTypeEnum noteType, String noteId, Boolean isImportant, String description, String createdAt, String userId, Boolean hidden) {
        this.noteTitle = noteTitle;
        this.noteType = noteType;
        this.noteId = noteId;
        this.isImportant = isImportant;
        this.description = description;
        this.createdAt = createdAt;
        this.userId = userId;
        this.hidden = hidden;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @JsonIgnore
    public Boolean getImportant() {
        return isImportant;
    }

    public void setImportant(Boolean important) {
        isImportant = important;
    }

    public String getNoteTitle() {
        return noteTitle;
    }

    public void setNoteTitle(String noteTitle) {
        this.noteTitle = noteTitle;
    }

    public NotesTypeEnum getNoteType() {
        return noteType;
    }

    public void setNoteType(NotesTypeEnum noteType) {
        this.noteType = noteType;
    }

    public String getNoteId() {
        return noteId;
    }

    public void setNoteId(String noteId) {
        this.noteId = noteId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Boolean getHidden() {
        return hidden;
    }

    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }

    @Override
    public String toString() {
        return "NoteResponse{" +
                "noteTitle='" + noteTitle + '\'' +
                ", noteType=" + noteType +
                ", noteId='" + noteId + '\'' +
                ", isImportant=" + isImportant +
                ", description='" + description + '\'' +
                ", createdAt=" + createdAt +
                ", userId='" + userId + '\'' +
                ", hidden=" + hidden +
                '}';
    }
}
