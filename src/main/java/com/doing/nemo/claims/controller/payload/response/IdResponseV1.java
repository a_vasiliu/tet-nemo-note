package com.doing.nemo.claims.controller.payload.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.UUID;

public class IdResponseV1 implements Serializable {
    @JsonProperty("id")
    private UUID id;

    public IdResponseV1(UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "IdResponseV1{" +
                "id=" + id +
                '}';
    }
}
