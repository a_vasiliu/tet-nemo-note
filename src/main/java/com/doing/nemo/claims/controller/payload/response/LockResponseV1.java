package com.doing.nemo.claims.controller.payload.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.sql.Timestamp;

public class LockResponseV1 implements Serializable {

    @JsonProperty("user_id")
    private String userId;

    @JsonProperty("claim_id")
    private String claimId;

    @JsonProperty("now")
    private Timestamp now;

    public LockResponseV1() {
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getClaimId() {
        return claimId;
    }

    public void setClaimId(String claimId) {
        this.claimId = claimId;
    }

    public Timestamp getNow()
    {
        if(now == null){
            return null;
        }
        return (Timestamp) now.clone();
    }

    public void setNow(Timestamp now) {
        if(now != null)
        {
            this.now = (Timestamp) now.clone();
        } else {
            this.now = null;
        }
    }

    @Override
    public String toString() {
        return "LockResponseV1{" +
                "userId='" + userId + '\'' +
                ", claimId='" + claimId + '\'' +
                ", now=" + now +
                '}';
    }
}
