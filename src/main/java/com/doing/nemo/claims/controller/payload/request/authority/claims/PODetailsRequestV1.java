package com.doing.nemo.claims.controller.payload.request.authority.claims;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.math.BigDecimal;

public class PODetailsRequestV1 implements Serializable {

    @JsonProperty("po_number")
    private String poNumber;

    @JsonProperty("po_amount")
    private BigDecimal poAmount;

    @JsonProperty("po_target")
    private String poTarget;


    public String getPoNumber() {
        return poNumber;
    }

    public void setPoNumber(String poNumber) {
        this.poNumber = poNumber;
    }

    public BigDecimal getPoAmount() {
        return poAmount;
    }

    public void setPoAmount(BigDecimal poAmount) {
        this.poAmount = poAmount;
    }

    public String getPoTarget() {
        return poTarget;
    }

    public void setPoTarget(String poTarget) {
        this.poTarget = poTarget;
    }
}
