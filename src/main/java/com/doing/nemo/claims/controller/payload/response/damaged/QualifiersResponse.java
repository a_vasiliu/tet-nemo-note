package com.doing.nemo.claims.controller.payload.response.damaged;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class QualifiersResponse  implements Serializable {
    @JsonProperty("attribute")
    private Long attribute;

    @JsonProperty("attribute_description")
    private String attributeDescription;

    @JsonProperty("value")
    private String value;

    @JsonProperty("value_description")
    private String valueDescription;

    public QualifiersResponse() {
    }

    public Long getAttribute() {
        return attribute;
    }

    public void setAttribute(Long attribute) {
        this.attribute = attribute;
    }

    public String getAttributeDescription() {
        return attributeDescription;
    }

    public void setAttributeDescription(String attributeDescription) {
        this.attributeDescription = attributeDescription;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getValueDescription() {
        return valueDescription;
    }

    public void setValueDescription(String valueDescription) {
        this.valueDescription = valueDescription;
    }

    @Override
    public String toString() {
        return "QualifiersResponse{" +
                "attribute=" + attribute +
                ", attributeDescription='" + attributeDescription + '\'' +
                ", value='" + value + '\'' +
                ", valueDescription='" + valueDescription + '\'' +
                '}';
    }
}
