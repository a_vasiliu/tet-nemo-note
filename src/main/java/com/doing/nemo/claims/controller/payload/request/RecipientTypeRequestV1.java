package com.doing.nemo.claims.controller.payload.request;

import com.doing.nemo.claims.entity.enumerated.ClaimsRepairEnum;
import com.doing.nemo.claims.entity.enumerated.RecipientEnum;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.UUID;

public class RecipientTypeRequestV1 implements Serializable {

    private UUID id;

    @NotNull
    @JsonProperty("description")
    private String description;

    @NotNull
    @JsonProperty("fixed")
    private Boolean fixed;

    @NotNull
    @JsonProperty("email")
    private String email;

    @JsonProperty("db_field_email")
    private String dbFieldEmail;

    @NotNull
    @JsonProperty("recipient_type")
    private RecipientEnum recipientType;

    @JsonProperty("is_active")
    private Boolean isActive = true;

    @JsonProperty("type_complaint")
    private ClaimsRepairEnum typeComplaint;

    public RecipientTypeRequestV1(UUID id, @NotNull String description, @NotNull Boolean fixed, @NotNull String email, String dbFieldEmail, @NotNull RecipientEnum recipientType, Boolean isActive, ClaimsRepairEnum typeComplaint) {
        this.id = id;
        this.description = description;
        this.fixed = fixed;
        this.email = email;
        this.dbFieldEmail = dbFieldEmail;
        this.recipientType = recipientType;
        this.isActive = isActive;
        this.typeComplaint = typeComplaint;
    }

    public ClaimsRepairEnum getTypeComplaint() {
        return typeComplaint;
    }

    public void setTypeComplaint(ClaimsRepairEnum typeComplaint) {
        this.typeComplaint = typeComplaint;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public RecipientEnum getRecipientType() {
        return recipientType;
    }

    public void setRecipientType(RecipientEnum recipientType) {
        this.recipientType = recipientType;
    }

    public Boolean getFixed() {
        return fixed;
    }

    public void setFixed(Boolean fixed) {
        this.fixed = fixed;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDbFieldEmail() {
        return dbFieldEmail;
    }

    public void setDbFieldEmail(String dbFieldEmail) {
        this.dbFieldEmail = dbFieldEmail;
    }

    public Boolean getActive() {
        return isActive;
    }

    @JsonSetter
    public void setActive(Boolean active) {
        if (active != null)
            isActive = active;
    }

    @Override
    public String toString() {
        return "RecipientTypeRequestV1{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", fixed=" + fixed +
                ", email='" + email + '\'' +
                ", dbFieldEmail='" + dbFieldEmail + '\'' +
                ", isActive=" + isActive +
                ", recipientType=" + recipientType +
                '}';
    }
}
