package com.doing.nemo.claims.controller.payload.request.practice;

import com.doing.nemo.claims.controller.payload.request.authority.practice.ProcessingRequestV1;
import com.doing.nemo.claims.controller.payload.request.counterparty.VehicleRequest;
import com.doing.nemo.claims.controller.payload.request.damaged.ContractRequest;
import com.doing.nemo.claims.controller.payload.request.damaged.CustomerRequest;
import com.doing.nemo.claims.controller.payload.request.damaged.FleetManagerRequest;
import com.doing.nemo.claims.entity.enumerated.PracticeStatusEnum;
import com.doing.nemo.claims.entity.enumerated.PracticeTypeEnum;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class PracticeRequestV1 implements Serializable {

    /*@JsonProperty("parent_id")
    private UUID parentId;*/

    @JsonProperty("status")
    private PracticeStatusEnum status;

    @JsonProperty("practice_type")
    private PracticeTypeEnum practiceType;

    @JsonProperty("claim_id")
    private UUID claimsId;

    @JsonProperty("processing_list")
    private List<ProcessingRequestV1> processingList;

    /*@JsonProperty("attachment_list")
    private List<Attachment> attachmentList;*/

    @JsonProperty("vehicle")
    private VehicleRequest vehicle;

    @JsonProperty("customer")
    private CustomerRequest customer;

    @JsonProperty("contract")
    private ContractRequest contract;

    @JsonProperty("fleet_managers")
    private List<FleetManagerRequest> fleetManagers;

    @JsonProperty("seizure")
    private SeizureRequestV1 seizure;

    @JsonProperty("release_from_seizure")
    private ReleaseFromSeizureRequestV1 releaseFromSeizure;

    @JsonProperty("misappropriation")
    private MisappropriationRequestV1 misappropriation;

    @JsonProperty("finding")
    private FindingRequestV1 finding;

    @JsonProperty("theft")
    private TheftPracticeRequestV1 theft;

    public List<FleetManagerRequest> getFleetManagers() {
        if(fleetManagers == null){
            return null;
        }
        return new ArrayList<>(fleetManagers);
    }

    public SeizureRequestV1 getSeizure() {
        return seizure;
    }

    public void setSeizure(SeizureRequestV1 seizure) {
        this.seizure = seizure;
    }

    public ReleaseFromSeizureRequestV1 getReleaseFromSeizure() {
        return releaseFromSeizure;
    }

    public void setReleaseFromSeizure(ReleaseFromSeizureRequestV1 releaseFromSeizure) {
        this.releaseFromSeizure = releaseFromSeizure;
    }

    public MisappropriationRequestV1 getMisappropriation() {
        return misappropriation;
    }

    public void setMisappropriation(MisappropriationRequestV1 misappropriation) {
        this.misappropriation = misappropriation;
    }

    public FindingRequestV1 getFinding() {
        return finding;
    }

    public void setFinding(FindingRequestV1 finding) {
        this.finding = finding;
    }

    public void setFleetManagers(List<FleetManagerRequest> fleetManagers) {
        if(fleetManagers != null){
            this.fleetManagers = new ArrayList<>(fleetManagers);
        } else {
            this.fleetManagers = null;
        }
    }

    public PracticeStatusEnum getStatus() {
        return status;
    }

    public void setStatus(PracticeStatusEnum status) {
        this.status = status;
    }

    public PracticeTypeEnum getPracticeType() {
        return practiceType;
    }

    public void setPracticeType(PracticeTypeEnum practiceType) {
        this.practiceType = practiceType;
    }

    public UUID getClaimsId() {
        return claimsId;
    }

    public void setClaimsId(UUID claimsId) {
        this.claimsId = claimsId;
    }

    public List<ProcessingRequestV1> getProcessingRequestV1List() {
        if(processingList == null){
            return null;
        }
        return new ArrayList<>(processingList);
    }

    public void setProcessingRequestV1List(List<ProcessingRequestV1> processingList) {
        if(processingList != null)
        {
            this.processingList = new ArrayList<>(processingList);
        } else {
            this.processingList = null;
        }
    }

    public VehicleRequest getVehicle() {
        return vehicle;
    }

    public void setVehicle(VehicleRequest vehicle) {
        this.vehicle = vehicle;
    }

    public CustomerRequest getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerRequest customer) {
        this.customer = customer;
    }

    public ContractRequest getContract() {
        return contract;
    }

    public void setContract(ContractRequest contract) {
        this.contract = contract;
    }

    public TheftPracticeRequestV1 getTheft() {
        return theft;
    }

    public void setTheft(TheftPracticeRequestV1 theft) {
        this.theft = theft;
    }


    @Override
    public String toString() {
        return "PracticeRequestV1{" +
                "status=" + status +
                ", practiceType=" + practiceType +
                ", claimsId=" + claimsId +
                ", processingList=" + processingList +
                ", vehicle=" + vehicle +
                ", customer=" + customer +
                ", contract=" + contract +
                ", fleetManagers=" + fleetManagers +
                ", seizure=" + seizure +
                ", releaseFromSeizure=" + releaseFromSeizure +
                ", misappropriation=" + misappropriation +
                ", finding=" + finding +
                ", theft=" + theft +
                '}';
    }


}
