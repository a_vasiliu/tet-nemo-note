package com.doing.nemo.claims.controller.payload.request;

import com.doing.nemo.claims.entity.enumerated.AttachmentEnum.GroupsEnum;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;

import java.io.Serializable;
import java.util.UUID;

public class AttachmentTypeRequestV1 implements Serializable {

    private UUID id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("groups")
    private GroupsEnum groups;

    @JsonProperty("is_active")
    private Boolean isActive = true;

    public AttachmentTypeRequestV1() {
    }

    public AttachmentTypeRequestV1(UUID id, String name, GroupsEnum groups, Boolean isActive) {
        this.id = id;
        this.name = name;
        this.groups = groups;
        this.isActive = isActive;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public GroupsEnum getGroups() {
        return groups;
    }

    public void setGroups(GroupsEnum groups) {
        this.groups = groups;
    }

    public Boolean getActive() {
        return isActive;
    }

    @JsonSetter
    public void setActive(Boolean active) {
        if (active != null)
            isActive = active;
    }

    @Override
    public String toString() {
        return "AttachmentTypeRequestV1{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", groups=" + groups +
                ", isActive=" + isActive +
                '}';
    }
}
