package com.doing.nemo.claims.controller.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class CounterpartyProcedureRequestV1 implements Serializable {
    @JsonProperty("motivation")
    private String motivation;

    public CounterpartyProcedureRequestV1(String motivation) {
        this.motivation = motivation;
    }

    public CounterpartyProcedureRequestV1() {
    }

    public String getMotivation() {
        return motivation;
    }

    public void setMotivation(String motivation) {
        this.motivation = motivation;
    }

    @Override
    public String toString() {
        return "CounterpartyProcedureRequestV1{" +
                "motivation='" + motivation + '\'' +
                '}';
    }
}
