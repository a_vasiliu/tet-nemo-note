package com.doing.nemo.claims.controller;

import com.doing.nemo.claims.adapter.EventAdapter;
import com.doing.nemo.claims.adapter.InsurancePolicyAdapter;
import com.doing.nemo.claims.adapter.RiskAdapter;
import com.doing.nemo.claims.common.validation.MessageCode;
import com.doing.nemo.claims.common.validation.RequestValidator;
import com.doing.nemo.claims.controller.payload.request.*;
import com.doing.nemo.claims.controller.payload.response.ClaimsResponsePaginationV1;
import com.doing.nemo.claims.controller.payload.response.IdResponseV1;
import com.doing.nemo.claims.controller.payload.response.InsurancePolicyResponseV1;
import com.doing.nemo.claims.controller.payload.response.UploadFileResponseV1;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.enumerated.PolicyEnum.PolicyTypeEnum;
import com.doing.nemo.claims.entity.esb.CustomerESB.CustomerEsb;
import com.doing.nemo.claims.entity.settings.EventEntity;
import com.doing.nemo.claims.entity.settings.InsurancePolicyEntity;
import com.doing.nemo.claims.entity.settings.RiskEntity;
import com.doing.nemo.claims.service.ESBService;
import com.doing.nemo.claims.service.InsurancePolicyService;
import com.doing.nemo.commons.exception.NotFoundException;
import io.swagger.annotations.*;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/v1")
@Api("Insurance Policy")
public class InsurancePolicyController {

    @Autowired
    private InsurancePolicyService insurancePolicyService;

    @Autowired
    private InsurancePolicyAdapter insurancePolicyAdapter;

    @Autowired
    private EventAdapter eventAdapter;

    @Autowired
    private RequestValidator requestValidator;

    @Autowired
    private ESBService esbService;

    private static Logger LOGGER = LoggerFactory.getLogger(InsurancePolicyController.class);


    @PostMapping("/policy")
    @ApiOperation(value = "Insert Insurance Policy", notes = "Insert insurance policy using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = IdResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<IdResponseV1> insertInsurancePolicy(
            @ApiParam(value = "Body of the Insurance Policy to be created", required = true)
            @RequestBody InsurancePolicyRequestV1 insurancePolicyRequest) throws IOException {
        requestValidator.validateRequest(insurancePolicyRequest, MessageCode.E00X_1000);

        CustomerEsb customerEsb = null;
        InsurancePolicyEntity insurancePolicyEntity =insurancePolicyAdapter.adptInsurancePolicyRequestToInsurancePolicy(insurancePolicyRequest);

        if (insurancePolicyRequest.getClientCode()!=null) {
            //recupero il nome del customer
            Long customerId = insurancePolicyRequest.getClientCode();

            customerEsb = esbService.getCustomer(customerId.toString());
            if (customerEsb == null) {
                LOGGER.debug("Customer with code " + customerId + " not found ");
                throw new NotFoundException("Customer with code " + customerId + " not found ", com.doing.nemo.claims.validation.MessageCode.CLAIMS_1010);
            }else{
                insurancePolicyEntity.setClient(customerEsb.getTradingName());
            }

        }

        insurancePolicyEntity = insurancePolicyService.insertInsurancePolicy(insurancePolicyEntity);


        IdResponseV1 insurancePolicyResponseIdV1 = new IdResponseV1(insurancePolicyEntity.getId());
        return new ResponseEntity<>(insurancePolicyResponseIdV1, HttpStatus.CREATED);



    }

    @PostMapping("/policies")
    @ApiOperation(value = "Insert List of Insurance Policies", notes = "Insert list of insurance policies using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = InsurancePolicyResponseV1.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<InsurancePolicyResponseV1>> insertInsurancePolicies(
            @ApiParam(value = "Body of the list of Insurance Policies to be created", required = true)
            @RequestBody ListRequestV1<InsurancePolicyRequestV1> insurancePolicyListRequestV1) {
        List<InsurancePolicyResponseV1> insurancePolicyResponseV1List = new ArrayList<>();
        for (InsurancePolicyRequestV1 insurancePolicyRequestV1 : insurancePolicyListRequestV1.getData()) {
            requestValidator.validateRequest(insurancePolicyRequestV1, MessageCode.E00X_1000);
            InsurancePolicyEntity insurancePolicyEntity = insurancePolicyService.insertInsurancePolicy(insurancePolicyAdapter.adptInsurancePolicyRequestToInsurancePolicy(insurancePolicyRequestV1));
            InsurancePolicyResponseV1 insurancePolicyResponseV1 = InsurancePolicyAdapter.adptFromIPolicyEntityToIPolicyRespo(insurancePolicyEntity);
            insurancePolicyResponseV1List.add(insurancePolicyResponseV1);
        }
        return new ResponseEntity<>(insurancePolicyResponseV1List, HttpStatus.CREATED);
    }

    @GetMapping("/policy/{UUID}")
    @ApiOperation(value = "Recover Insurance Policy", notes = "Returns a Insurance Policy using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = InsurancePolicyResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<InsurancePolicyResponseV1> getInsurancePolicy(
            @ApiParam(value = "UUID of the Insurance Policy to be found", required = true)
            @PathVariable UUID UUID) {
        InsurancePolicyEntity insurancePolicyEntity = insurancePolicyService.selectInsurancePolicy(UUID);

        InsurancePolicyResponseV1 insurancePolicyResponseV1 = InsurancePolicyAdapter.adptFromIPolicyEntityToIPolicyRespo(insurancePolicyEntity);
        return new ResponseEntity<>(insurancePolicyResponseV1, HttpStatus.OK);
    }

    @GetMapping("/policy")
    @ApiOperation(value = "Recover All Insurance Policies", notes = "Returns all Insurance Policies")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = InsurancePolicyResponseV1.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<InsurancePolicyResponseV1>> getAllInsurancePolicies(
            @RequestParam(value = "date_validity", required = false) String dateValidity,
            @RequestParam(value = "is_active", required = false) Boolean isActive
    ) {
        System.out.println("Data validità: "+dateValidity);
        List<InsurancePolicyEntity> insurancePolicyEntityList = insurancePolicyService.paginationInsurancePolicyGetAll(dateValidity,isActive);
        List<InsurancePolicyResponseV1> insurancePolicyResponseV1List = InsurancePolicyAdapter.adptFromIPolicyEntityToIPolicyRespoList(insurancePolicyEntityList);
        return new ResponseEntity<>(insurancePolicyResponseV1List, HttpStatus.OK);
    }

    @DeleteMapping("/policy/{UUID}")
    @ApiOperation(value = "Delete Insurance Policy", notes = "Delete Insurance Policy using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = InsurancePolicyResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<InsurancePolicyResponseV1> deleteInsurancePolicy(
            @ApiParam(value = "UUID that identifies the Insurance Policy to be deleted", required = true)
            @PathVariable UUID UUID) {
        InsurancePolicyEntity insurancePolicyEntity = insurancePolicyService.deleteInsurancePolicy(UUID);
        InsurancePolicyResponseV1 insurancePolicyResponseV1 = InsurancePolicyAdapter.adptFromIPolicyEntityToIPolicyRespo(insurancePolicyEntity);
        return new ResponseEntity<>(insurancePolicyResponseV1, HttpStatus.OK);

    }

    @PutMapping("/policy/{UUID}")
    @ApiOperation(value = "Upload Insurance Policy", notes = "Upload Insurance Policy using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = InsurancePolicyResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<InsurancePolicyResponseV1> putInsurancePolicy(
            @ApiParam(value = "UUID that identifies the Insurance Policy to be modified", required = true)
            @PathVariable UUID UUID,
            @ApiParam(value = "Updated body of the Insurance Policy", required = true)
            @RequestBody InsurancePolicyRequestV1 insurancePolicyRequestV1,
            @RequestHeader(name = "nemo-user-id") String userId,
            @RequestHeader(name = "nemo-user-first-name") String userName,
            @RequestHeader(name = "nemo-user-last-name") String lastName) throws IOException{

        requestValidator.validateRequest(insurancePolicyRequestV1, MessageCode.E00X_1000);
        Long customerId = insurancePolicyRequestV1.getClientCode();

        CustomerEsb customerEsb = null;
        InsurancePolicyEntity current = insurancePolicyService.selectInsurancePolicy(UUID);
        if((current.getClientCode()==null && insurancePolicyRequestV1.getClientCode()!=null) || (current.getClientCode()!=null && insurancePolicyRequestV1.getClientCode()!=null && !current.getClientCode().equals(insurancePolicyRequestV1.getClientCode()))){
            try {
                customerEsb = esbService.getCustomer(customerId.toString());
            } catch (Exception e) {
                if(e instanceof NotFoundException)
                {
                    LOGGER.debug("Customer with code " + customerId  + " not found ");
                    throw new NotFoundException("Customer with code " + customerId  + " not found ", com.doing.nemo.claims.validation.MessageCode.CLAIMS_1010);
                }
                else
                {
                    LOGGER.debug(e.getMessage());
                    throw e;
                }
            }
        }

        InsurancePolicyEntity insurancePolicyEntity = insurancePolicyAdapter.adptFromIPolicyRequToIPolicyEntityWithID(insurancePolicyRequestV1, UUID);
        if(customerEsb!=null)
            insurancePolicyEntity.setClient(customerEsb.getTradingName());
        insurancePolicyEntity = insurancePolicyService.updateInsurancePolicy(insurancePolicyEntity, UUID, userName + " "+lastName);
        InsurancePolicyResponseV1 insurancePolicyResponseV1 = InsurancePolicyAdapter.adptFromIPolicyEntityToIPolicyRespo(insurancePolicyEntity);
        return new ResponseEntity<>(insurancePolicyResponseV1, HttpStatus.OK);
    }

    @PostMapping("/policy/risk/{UUID}")
    @ApiOperation(value = "Insert Insurance Policy's risks", notes = "Insert risks for the insurance policy identified by the UUID")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = InsurancePolicyResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<InsurancePolicyResponseV1> postRiskInsurancePolicy(
            @ApiParam(value = "Body of the risks to be created", required = true)
            @RequestBody ListRequestV1<RiskRequestV1> riskListRequestV1,
            @ApiParam(value = "UUID that identifies the Insurance Policy to be modified", required = true)
            @PathVariable UUID UUID) {
        requestValidator.validateRequest(riskListRequestV1, MessageCode.E00X_1000);
        List<RiskEntity> riskEntityList = RiskAdapter.adptFromRiskRequestToRiskEntityList(riskListRequestV1.getData());
        InsurancePolicyEntity insurancePolicyEntity = insurancePolicyService.insertRiskInsurancePolicy(riskEntityList, UUID);
        InsurancePolicyResponseV1 insurancePolicyResponseV1 = InsurancePolicyAdapter.adptFromIPolicyEntityToIPolicyRespo(insurancePolicyEntity);
        return new ResponseEntity<>(insurancePolicyResponseV1, HttpStatus.CREATED);
    }

    @PostMapping("/policy/event/{UUID}")
    @ApiOperation(value = "Insert Insurance Policy's events", notes = "Insert events for the insurance policy identified by the UUID")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = InsurancePolicyResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<InsurancePolicyResponseV1> postEventInsurancePolicy(
            @ApiParam(value = "Body of the events to be created", required = true)
            @RequestBody ListRequestV1<EventRequestV1> eventListRequestV1,
            @ApiParam(value = "UUID that identifies the Insurance Policy to be modified", required = true)
            @PathVariable UUID UUID) {
        requestValidator.validateRequest(eventListRequestV1, MessageCode.E00X_1000);
        List<EventEntity> eventEntityList = eventAdapter.adptFromEventRequestToEventEntityList(eventListRequestV1.getData());
        InsurancePolicyEntity insurancePolicyEntity = insurancePolicyService.insertEventInsurancePolicy(eventEntityList, UUID);
        InsurancePolicyResponseV1 insurancePolicyResponseV1 = InsurancePolicyAdapter.adptFromIPolicyEntityToIPolicyRespo(insurancePolicyEntity);
        return new ResponseEntity<>(insurancePolicyResponseV1, HttpStatus.CREATED);
    }

    @PostMapping("/policy/attachment/{UUID}")
    @ApiOperation(value = "Insert Insurance Policy's attachments", notes = "Insert attachments for the insurance policy identified by the UUID")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = InsurancePolicyResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<UploadFileResponseV1>> postAttachmentInsurancePolicy(
            @ApiParam(value = "Body of the attachments to be created", required = true)
            @RequestBody List<UploadFileRequestV1> attachmentListRequestV1,
            @RequestHeader(name = "nemo-user-id") String userId,
            @RequestHeader(name = "nemo-user-first-name") String userName,
            @ApiParam(value = "UUID that identifies the Insurance Policy to be modified", required = true)
            @PathVariable UUID UUID) {

        requestValidator.validateRequest(attachmentListRequestV1, MessageCode.E00X_1000);

        List<UploadFileResponseV1> response = insurancePolicyService.insertAttachmentInsurancePolicy(attachmentListRequestV1, UUID, userId, userName);

        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }


    @PatchMapping("/policy/{UUID}/active")
    @ApiOperation(value = "Patch status active Insurance Policy", notes = "Patch status active Insurance Manager using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = InsurancePolicyResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<InsurancePolicyResponseV1> patchStatusInsuranceManager(
            @PathVariable UUID UUID
    ) {
        InsurancePolicyEntity insurancePolicyEntity = insurancePolicyService.updateStatusInsurancePolicy(UUID);
        InsurancePolicyResponseV1 responseV1 = InsurancePolicyAdapter.adptFromIPolicyEntityToIPolicyRespo(insurancePolicyEntity);
        return new ResponseEntity<>(responseV1, HttpStatus.OK);
    }

    @ApiOperation(value="Insurance Policy Pagination", notes = "It retrieves Insurance Policy pagination")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = ClaimsResponsePaginationV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @GetMapping(value="/policy/pagination", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<ClaimsResponsePaginationV1> policyPagination(
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @RequestParam(value = "page_size", defaultValue = "20") Integer pageSize,
            @RequestParam(value = "order_by", required = false) String orderBy,
            @RequestParam(value = "asc", required = false, defaultValue = "true") Boolean asc,
            @RequestParam(value = "type", required = false) PolicyTypeEnum type,
            @RequestParam(value = "client_code", required = false) Long clientCode,
            @RequestParam(value = "client", required = false) String client,
            @RequestParam(value = "number_policy", required = false) String numberPolicy,
            @RequestParam(value = "beginning_validity", required = false) String beginningValidity,
            @RequestParam(value = "end_validity", required = false) String endValidity,
            @RequestParam(value = "book_register_code", required = false) String bookRegisterCode,
            @RequestParam(value = "is_active", required = false) Boolean isActive

    ){

        Pagination<InsurancePolicyEntity> insurancePolicyEntityPagination = insurancePolicyService.paginationInsurancePolicy(page, pageSize, orderBy, asc, type, clientCode, client, numberPolicy, beginningValidity, endValidity, bookRegisterCode, isActive);

        return new ResponseEntity<>(InsurancePolicyAdapter.adptInsurancePolicyPaginationToClaimsResponsePagination(insurancePolicyEntityPagination), HttpStatus.OK);
    }


    @PostMapping("/policy/cron/comunication")
    @ApiOperation(value = "Comunication policy", notes = "Comunication policy with cron")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })

    public void sendEmailPolicyRenewalContractNotification(@ApiParam(value = "TypeEnum of User who calls the service", required = true)
                                                               @RequestHeader(name = "nemo-user-id") String user){
        insurancePolicyService.renewalNotificationEmail(user);
    }


}
