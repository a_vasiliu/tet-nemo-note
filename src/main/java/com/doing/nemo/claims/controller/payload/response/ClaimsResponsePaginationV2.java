package com.doing.nemo.claims.controller.payload.response;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
@JsonIgnoreProperties(ignoreUnknown = true)
public class ClaimsResponsePaginationV2 implements Serializable {

    @JsonProperty(value = "stats")
    private StatsResponseV2 stats;

    @JsonProperty(value = "items")
    private List<ClaimsResponseV2> claims;

    public ClaimsResponsePaginationV2() {
    }

    public ClaimsResponsePaginationV2(StatsResponseV2 stats, List<ClaimsResponseV2> claims) {
        this.stats = stats;
        this.claims = claims;
    }




}