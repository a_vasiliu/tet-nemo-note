package com.doing.nemo.claims.controller;

import com.doing.nemo.claims.adapter.DmSystemsAdapter;
import com.doing.nemo.claims.controller.payload.request.DmSystemsRequestV1;
import com.doing.nemo.claims.controller.payload.request.ListRequestV1;
import com.doing.nemo.claims.controller.payload.response.IdResponseV1;
import com.doing.nemo.claims.controller.payload.response.DmSystemsResponseV1;
import com.doing.nemo.claims.entity.settings.DmSystemsEntity;
import com.doing.nemo.claims.service.DmSystemsService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/v1")
@Api("DM Systems")
public class DmSystemsController {
    @Autowired
    private DmSystemsService dmSystemsService;

    @Autowired
    private DmSystemsAdapter dmSystemsAdapter;

    @PostMapping("/dmsystems")
    @Transactional
    @ApiOperation(value = "Insert DM System", notes = "Insert DM System using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = DmSystemsResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<IdResponseV1> insertDmSystems(
            @RequestBody DmSystemsRequestV1 dmSystemsRequestV1
            )
    {
        DmSystemsEntity dmSystemsEntity = DmSystemsAdapter.adptFromDmSystemsRequestToDmSystemsEntity(dmSystemsRequestV1);
        dmSystemsEntity = dmSystemsService.saveDmSystems(dmSystemsEntity);
        return new ResponseEntity<>(new IdResponseV1(dmSystemsEntity.getId()), HttpStatus.CREATED);
    }

    @PostMapping("/dmsystems/all")
    @Transactional
    @ApiOperation(value = "Insert a list of DM Systems", notes = "Insert a list of insurance companies using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = DmSystemsResponseV1.class, responseContainer="List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<DmSystemsResponseV1>> insertDmSystemsAll(
            @ApiParam(value = "List of the bodies of the DM Systems to be created", required = true)
            @RequestBody ListRequestV1<DmSystemsRequestV1> insuranceCompaniesRequest){

        List<DmSystemsResponseV1> dmSystemsResponseV1List = new ArrayList<>();
        for (DmSystemsRequestV1 dmSystemsRequestV1 : insuranceCompaniesRequest.getData())
        {
            
            DmSystemsEntity dmSystemsEntity = DmSystemsAdapter.adptFromDmSystemsRequestToDmSystemsEntity(dmSystemsRequestV1);
            dmSystemsEntity = dmSystemsService.saveDmSystems(dmSystemsEntity);
            DmSystemsResponseV1 responseV1 = DmSystemsAdapter.adptFromDmSystemEntityToDmSystemResponse(dmSystemsEntity);
            dmSystemsResponseV1List.add(responseV1);
        }
        return new ResponseEntity<>(dmSystemsResponseV1List, HttpStatus.CREATED);

    }

    @PutMapping("/dmsystems/{UUID}")
    @Transactional
    @ApiOperation(value = "Insert DM System", notes = "Insert DM System using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = DmSystemsResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<DmSystemsResponseV1> updateDmSystems(
            @RequestBody DmSystemsRequestV1 dmSystemsRequestV1,
            @PathVariable("UUID") UUID uuid
            )
    {
        DmSystemsEntity dmSystemsEntity = dmSystemsAdapter.adptFromDmSystemsRequestToDmSystemsEntityWithID(dmSystemsRequestV1, uuid);
        dmSystemsEntity = dmSystemsService.updateDmSystems(dmSystemsEntity);
        return new ResponseEntity<>(DmSystemsAdapter.adptFromDmSystemEntityToDmSystemResponse(dmSystemsEntity), HttpStatus.OK);
    }

    @GetMapping("/dmsystems/{UUID}")
    @Transactional
    @ApiOperation(value = "Recover DM System", notes = "Returns a DM System using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = DmSystemsResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<DmSystemsResponseV1> getDmSystems(
            @PathVariable("UUID") UUID uuid
    ){
        DmSystemsEntity dmSystemsEntity = dmSystemsService.getDmSystems(uuid);
        return new ResponseEntity<>(dmSystemsAdapter.adptFromDmSystemEntityToDmSystemResponse(dmSystemsEntity), HttpStatus.OK);
    }

    @GetMapping("/dmsystems")
    @Transactional
    @ApiOperation(value = "Recover DM System", notes = "Returns a DM System using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = DmSystemsResponseV1.class, responseContainer="List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<DmSystemsResponseV1>> getDmSystems(
    ){
        List<DmSystemsEntity> dmSystemsEntity = dmSystemsService.getAllDmSystems();
        return new ResponseEntity<>(dmSystemsAdapter.adptFromDmSystemEntityToDmSystemResponseList(dmSystemsEntity), HttpStatus.OK);
    }


    @DeleteMapping("/dmsystems/{UUID}")
    @Transactional
    @ApiOperation(value = "Delete Insurance Manager", notes = "Delete Insurance Manager using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity deleteDmSystems(
            @PathVariable("UUID") UUID uuid
    ){
        dmSystemsService.deleteDmSystems(uuid);
        return new ResponseEntity(HttpStatus.OK);
    }
}
