package com.doing.nemo.claims.controller;

import com.doing.nemo.claims.adapter.SupplierAdapter;
import com.doing.nemo.claims.common.validation.MessageCode;
import com.doing.nemo.claims.common.validation.RequestValidator;
import com.doing.nemo.claims.controller.payload.request.ListRequestV1;
import com.doing.nemo.claims.controller.payload.request.SupplierRequestV1;
import com.doing.nemo.claims.controller.payload.response.PaginationResponseV1;
import com.doing.nemo.claims.controller.payload.response.IdResponseV1;
import com.doing.nemo.claims.controller.payload.response.SupplierResponseV1;
import com.doing.nemo.claims.entity.SupplierEntity;
import com.doing.nemo.claims.service.SupplierService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/v1")
@Api("Supplier")
public class SupplierController {

    @Autowired
    private SupplierService supplierService;

    @Autowired
    private RequestValidator requestValidator;


    @PostMapping("/supplier")
    @Transactional
    @ApiOperation(value = "Insert Supplier", notes = "Insert Supplier using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = IdResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<IdResponseV1> postSupplier(@ApiParam(value = "Body of the Supplier to be created", required = true) @RequestBody SupplierRequestV1 supplierRequestV1) {
        requestValidator.validateRequest(supplierRequestV1, MessageCode.E00X_1000);
        SupplierEntity supplierEntity = SupplierAdapter.adptFromSupplierRequestToSupplierEntity(supplierRequestV1);
        supplierEntity = supplierService.insertSupplier(supplierEntity);
        IdResponseV1 supplierResponseIdV1 = new IdResponseV1(supplierEntity.getId());
        return new ResponseEntity(supplierResponseIdV1, HttpStatus.CREATED);
    }

    @PostMapping("/suppliers")
    @Transactional
    @ApiOperation(value = "Insert More Suppliers", notes = "Insert Suppliers using info passed in the bodies")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = SupplierResponseV1.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<SupplierResponseV1>> postSuppliers(@ApiParam(value = "Bodies of the Suppliers to be created", required = true) @RequestBody ListRequestV1<SupplierRequestV1> supplierListRequestV1) {

        List<SupplierResponseV1> supplierResponseV1List = new ArrayList<>();
        for (SupplierRequestV1 supplierRequestV1 : supplierListRequestV1.getData()) {
            requestValidator.validateRequest(supplierRequestV1, MessageCode.E00X_1000);
            SupplierEntity supplierEntity = SupplierAdapter.adptFromSupplierRequestToSupplierEntity(supplierRequestV1);
            supplierEntity = supplierService.insertSupplier(supplierEntity);
            SupplierResponseV1 supplierResponseV1 = SupplierAdapter.adptFromSupplierEntityToSupplierResponseV1(supplierEntity);
            supplierResponseV1List.add(supplierResponseV1);
        }
        return new ResponseEntity(supplierResponseV1List, HttpStatus.CREATED);
    }

    @PutMapping("/supplier/{UUID}")
    @Transactional
    @ApiOperation(value = "Upload Supplier", notes = "Upload Supplier using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = SupplierResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<SupplierResponseV1> putSupplier(@ApiParam(value = "UUID that identifies the Supplier to be modified", required = true) @PathVariable(name = "UUID") UUID id, @ApiParam(value = "Updated body of the Supplier", required = true)
    @RequestBody SupplierRequestV1 supplierRequestV1) {
        requestValidator.validateRequest(supplierRequestV1, MessageCode.E00X_1000);
        SupplierEntity supplierEntity = supplierService.selectSupplier(id);
        SupplierEntity supplierEntityNew = SupplierAdapter.adptFromSupplierRequestToSupplierEntity(supplierRequestV1);
        supplierEntityNew.setId(supplierEntity.getId());
        supplierEntityNew = supplierService.updateSupplier(supplierEntityNew);
        SupplierResponseV1 supplierResponseV1 = SupplierAdapter.adptFromSupplierEntityToSupplierResponseV1(supplierEntityNew);
        return new ResponseEntity<>(supplierResponseV1, HttpStatus.OK);
    }

    @GetMapping("/supplier/{UUID}")
    @Transactional
    @ApiOperation(value = "Recover Supplier", notes = "Return a Supplier using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = SupplierResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<SupplierResponseV1> getSupplier(@ApiParam(value = "UUID of the Supplier to be found", required = true) @PathVariable(name = "UUID") UUID id) {
        SupplierEntity supplierEntity = supplierService.selectSupplier(id);
        SupplierResponseV1 supplierResponseV1 = SupplierAdapter.adptFromSupplierEntityToSupplierResponseV1(supplierEntity);
        return new ResponseEntity<>(supplierResponseV1, HttpStatus.OK);
    }

    @GetMapping("/supplier")
    @Transactional
    @ApiOperation(value = "Recover All Suppliers", notes = "Returns all Suppliers")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = SupplierResponseV1.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<SupplierResponseV1>> getAllSupplier() {
        List<SupplierEntity> supplierEntityList = supplierService.selectAllSupplier();
        List<SupplierResponseV1> supplierResponseV1List = SupplierAdapter.adptFromSupplierEntityToSupplierResponseV1List(supplierEntityList);
        return new ResponseEntity(supplierResponseV1List, HttpStatus.OK);
    }

    @DeleteMapping("/supplier/{UUID}")
    @Transactional
    @ApiOperation(value = "Delete Supplier", notes = "Delete a Supplier using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = SupplierResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<SupplierResponseV1> deleteSupplier(@ApiParam(value = "UUID of the Supplier to be deleted", required = true) @PathVariable(name = "UUID") UUID id) {
        SupplierResponseV1 supplierResponseV1 = supplierService.deleteSupplier(id);
        return new ResponseEntity<>(supplierResponseV1, HttpStatus.OK);
    }

    @PatchMapping("/supplier/{UUID}/active")
    @Transactional
    @ApiOperation(value = "Patch active status Supplier", notes = "Patch active status of Supplier using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = SupplierResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<SupplierResponseV1> patchStatusSupplier(
            @PathVariable UUID UUID
    ) {
        SupplierEntity supplierEntity = supplierService.updateStatus(UUID);
        SupplierResponseV1 responseV1 = SupplierAdapter.adptFromSupplierEntityToSupplierResponseV1(supplierEntity);
        return new ResponseEntity<>(responseV1, HttpStatus.OK);
    }


    @GetMapping(value="/supplier/pagination", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<PaginationResponseV1<SupplierResponseV1>> searchSupplier(
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @RequestParam(value = "page_size", defaultValue = "20") Integer pageSize,
            @RequestParam(value = "order_by", required = false) String orderBy,
            @RequestParam(value = "asc", required = false, defaultValue = "true") Boolean asc,
            @RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "email", required = false) String email,
            @RequestParam(value = "is_active", required = false) Boolean isActive,
            @RequestParam(value = "cod_supplier", required = false) String codSupplier

    ){

        List<SupplierEntity> listSupplier = supplierService.findSuppliersFiltered(page,pageSize, orderBy, asc, name, email, isActive, codSupplier);
        Long itemCount = supplierService.countSupplierFiltered(name, email, isActive, codSupplier);

        PaginationResponseV1<SupplierResponseV1> paginationResponseV1 = SupplierAdapter.adptPagination(listSupplier, page, pageSize, itemCount);

        return new ResponseEntity<>(paginationResponseV1, HttpStatus.OK);
    }

}
