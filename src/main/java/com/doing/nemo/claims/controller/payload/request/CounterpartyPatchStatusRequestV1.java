package com.doing.nemo.claims.controller.payload.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class CounterpartyPatchStatusRequestV1 {
    @JsonIgnore
    @JsonProperty("description")
    private String description;

    @JsonProperty("template_list")
    private List<EmailTemplateMessagingRequestV1> templateList;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<EmailTemplateMessagingRequestV1> getTemplateList() {
        if(templateList == null){
            return null;
        }
        return new ArrayList<>(templateList);
    }

    public void setTemplateList(List<EmailTemplateMessagingRequestV1> templateList) {
        if(templateList != null)
        {
            this.templateList = new ArrayList<>(templateList);
        } else {
            this.templateList = null;
        }
    }

    @Override
    public String toString() {
        return "CounterpartyPatchStatusRequestV1{" +
                "description='" + description + '\'' +
                ", templateList=" + templateList +
                '}';
    }
}
