package com.doing.nemo.claims.controller.payload.response;

import com.doing.nemo.claims.entity.enumerated.ClaimsRepairEnum;
import com.doing.nemo.claims.entity.enumerated.RecipientEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.UUID;

public class RecipientTypeResponseV1 implements Serializable {

    private UUID id;

    @JsonProperty("description")
    private String description;

    @JsonProperty("fixed")
    private Boolean fixed;

    @JsonProperty("email")
    private String email;

    @JsonProperty("db_field_email")
    private String dbFieldEmail;

    @JsonProperty("is_active")
    private Boolean isActive;

    @JsonProperty("recipient_type")
    private RecipientEnum recipientType;

    @JsonProperty("type_complaint")
    private ClaimsRepairEnum typeComplaint;

    public RecipientTypeResponseV1() {
    }

    public RecipientTypeResponseV1(UUID id, String description, Boolean fixed, String email, String dbFieldEmail, Boolean isActive, RecipientEnum recipientType, ClaimsRepairEnum typeComplaint) {
        this.id = id;
        this.description = description;
        this.fixed = fixed;
        this.email = email;
        this.dbFieldEmail = dbFieldEmail;
        this.isActive = isActive;
        this.recipientType = recipientType;
        this.typeComplaint = typeComplaint;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public RecipientEnum getRecipientType() {
        return recipientType;
    }

    public void setRecipientType(RecipientEnum recipientType) {
        this.recipientType = recipientType;
    }

    public Boolean getFixed() {
        return fixed;
    }

    public void setFixed(Boolean fixed) {
        this.fixed = fixed;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDbFieldEmail() {
        return dbFieldEmail;
    }

    public void setDbFieldEmail(String dbFieldEmail) {
        this.dbFieldEmail = dbFieldEmail;
    }

    @JsonIgnore
    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public ClaimsRepairEnum getTypeComplaint() {
        return typeComplaint;
    }

    public void setTypeComplaint(ClaimsRepairEnum typeComplaint) {
        this.typeComplaint = typeComplaint;
    }

    @Override
    public String toString() {
        return "RecipientTypeResponseV1{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", fixed=" + fixed +
                ", email='" + email + '\'' +
                ", dbFieldEmail='" + dbFieldEmail + '\'' +
                ", isActive=" + isActive +
                ", recipientType=" + recipientType +
                '}';
    }
}
