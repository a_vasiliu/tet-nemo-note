package com.doing.nemo.claims.controller.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class PlatesStatusRequestV1 implements Serializable {

    @JsonProperty("vehicle_plate")
    private String vehiclePlate;

    @JsonProperty("vehicle_status_code")
    private Integer vehicleStatusCode;

    @JsonProperty("vehicle_status_description")
    private String vehicleStatusDescription;

    public String getVehiclePlate() {
        return vehiclePlate;
    }

    public void setVehiclePlate(String vehiclePlate) {
        this.vehiclePlate = vehiclePlate;
    }

    public Integer getVehicleStatusCode() {
        return vehicleStatusCode;
    }

    public void setVehicleStatusCode(Integer vehicleStatusCode) {
        this.vehicleStatusCode = vehicleStatusCode;
    }

    public String getVehicleStatusDescription() {
        return vehicleStatusDescription;
    }

    public void setVehicleStatusDescription(String vehicleStatusDescription) {
        this.vehicleStatusDescription = vehicleStatusDescription;
    }

    @Override
    public String toString() {
        return "PlatesStatusRequestV1{" +
                "plate='" + vehiclePlate + '\'' +
                ", status=" + vehicleStatusCode +
                ", description='" + vehicleStatusDescription + '\'' +
                '}';
    }
}
