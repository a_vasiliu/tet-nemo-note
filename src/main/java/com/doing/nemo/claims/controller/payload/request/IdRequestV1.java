package com.doing.nemo.claims.controller.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class IdRequestV1 implements Serializable {
    @JsonProperty("id")
    private String id;

    public IdRequestV1() {
    }

    public IdRequestV1(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "IdRequestV1{" +
                "id='" + id + '\'' +
                '}';
    }
}
