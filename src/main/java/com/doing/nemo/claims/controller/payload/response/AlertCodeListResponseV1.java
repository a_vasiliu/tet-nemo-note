package com.doing.nemo.claims.controller.payload.response;

import com.doing.nemo.claims.validation.MessageCode;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AlertCodeListResponseV1 implements Serializable {
    @JsonProperty("alert_code_list")
    private List<MessageCode> alertCodeList;

    public List<MessageCode> getAlertCodeList() {
        if(alertCodeList == null){
            return null;
        }
        return new ArrayList<>(alertCodeList);
    }

    public void setAlertCodeList(List<MessageCode> alertCodeList) {
        if(alertCodeList != null)
        {
            this.alertCodeList = new ArrayList<>(alertCodeList);
        } else {
            this.alertCodeList = null;
        }
    }

    @Override
    public String toString() {
        return "AlertCodeListResponseV1{" +
                "alertCodeList=" + alertCodeList +
                '}';
    }
}
