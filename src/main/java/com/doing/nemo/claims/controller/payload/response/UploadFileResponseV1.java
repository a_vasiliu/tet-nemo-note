package com.doing.nemo.claims.controller.payload.response;

import com.doing.nemo.claims.entity.enumerated.DTO.Status;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UploadFileResponseV1 implements Serializable {

    private static final long serialVersionUID = -4736987678746191454L;

    @ApiModelProperty(example = "897g34s3-0ca7-4226-b10b-c5db10003257")
    private String uuid;

    @ApiModelProperty(example = "897g34s3-0ca7-4226-b10b-c5db10003257")
    private String id;

    @JsonProperty("blob_name")
    @ApiModelProperty(example = "897g34s3-0ca7-4226-b10b-c5db10003257.png")
    private String blobName;

    @JsonProperty("resource_type")
    @ApiModelProperty(example = "orders")
    private String resourceType;

    @JsonProperty("resource_id")
    @ApiModelProperty(example = "129347")
    private String resourceId;

    @JsonProperty("file_name")
    @ApiModelProperty(example = "supermario.png")
    private String fileName;

    @JsonProperty("file_size")
    @ApiModelProperty(example = "28579")
    private Integer fileSize;

    @JsonProperty("mime_type")
    @ApiModelProperty(example = "image/png")
    private String mimeType;

    @ApiModelProperty(example = "Whooa there's a description field, too!")
    @JsonProperty("description")
    private String description;

    @JsonProperty("blob_type")
    @ApiModelProperty(example = "adc")
    private String blobType;

    @JsonProperty("attachment_type")
    private AttachmentTypeResponseV1 attachmentTypeResponseList;

    @ApiModelProperty(example = "PROCESSED")
    @JsonProperty("status")
    private Status status;

    @JsonProperty("thumbnail_id")
    @ApiModelProperty(example = "897g34s3-0ca7-4226-b10b-c5db10003257")
    private String thumbnailId;

    @JsonProperty("external_id")
    private String externalId;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getBlobName() {
        return blobName;
    }

    public void setBlobName(String blobName) {
        this.blobName = blobName;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public String getResourceId() {
        return resourceId;
    }

    public void setResourceId(String resourceId) {
        this.resourceId = resourceId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Integer getFileSize() {
        return fileSize;
    }

    public void setFileSize(Integer fileSize) {
        this.fileSize = fileSize;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBlobType() {
        return blobType;
    }

    public void setBlobType(String blobType) {
        this.blobType = blobType;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getThumbnailId() {
        return thumbnailId;
    }

    public void setThumbnailId(String thumbnailId) {
        this.thumbnailId = thumbnailId;
    }

    public AttachmentTypeResponseV1 getAttachmentTypeResponseList() {
        return attachmentTypeResponseList;
    }

    public void setAttachmentTypeResponseList(AttachmentTypeResponseV1 attachmentTypeResponseList) {
        this.attachmentTypeResponseList = attachmentTypeResponseList;
    }



    @Override
    public String toString() {
        return "UploadFileResponseV1{" +
                "uuid='" + uuid + '\'' +
                "id='" + id + '\'' +
                ", blobName='" + blobName + '\'' +
                ", resourceType='" + resourceType + '\'' +
                ", resourceId='" + resourceId + '\'' +
                ", fileName='" + fileName + '\'' +
                ", fileSize=" + fileSize +
                ", mimeType='" + mimeType + '\'' +
                ", description='" + description + '\'' +
                ", blobType='" + blobType + '\'' +
                ", attachmentTypeResponseList=" + attachmentTypeResponseList +
                ", status=" + status +
                ", thumbnailId='" + thumbnailId + '\'' +
                '}';
    }
}


