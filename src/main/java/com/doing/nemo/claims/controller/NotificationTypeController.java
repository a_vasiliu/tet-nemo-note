package com.doing.nemo.claims.controller;

import com.doing.nemo.claims.adapter.NotificationTypeAdapter;
import com.doing.nemo.claims.common.validation.MessageCode;
import com.doing.nemo.claims.common.validation.RequestValidator;
import com.doing.nemo.claims.controller.payload.request.NotificationTypeRequestV1;
import com.doing.nemo.claims.controller.payload.response.ClaimsResponsePaginationV1;
import com.doing.nemo.claims.controller.payload.response.IdResponseV1;
import com.doing.nemo.claims.controller.payload.response.NotificationTypeResponseV1;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.settings.NotificationTypeEntity;
import com.doing.nemo.claims.service.NotificationTypeService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/v1")
public class NotificationTypeController {

    @Autowired
    private NotificationTypeService notificationTypeService;

    @Autowired
    private NotificationTypeAdapter notificationTypeAdapter;

    @Autowired
    private RequestValidator requestValidator;

    @PostMapping(value = "/notificationtype",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Insert Notification TypeEnum", notes = "Insert notification type using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = IdResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @Transactional
    public ResponseEntity<IdResponseV1> postInsertNotificationType(
            @ApiParam(value = "Body of the Notification TypeEnum to be created", required = true)
            @RequestBody NotificationTypeRequestV1 notificationTypeRequest) {

        requestValidator.validateRequest(notificationTypeRequest, MessageCode.E00X_1000);
        NotificationTypeEntity notificationTypeEntity = NotificationTypeAdapter.adptNotificationTypeRequestToNotificationTypeEntity(notificationTypeRequest);
        notificationTypeEntity = notificationTypeService.insertNotificationType(notificationTypeEntity);
        IdResponseV1 response = new IdResponseV1(notificationTypeEntity.getId());
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @PutMapping(value = "/notificationtype/{UUID}",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Upload Notification TypeEnum", notes = "Upload notification type using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = NotificationTypeResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @Transactional
    public ResponseEntity<NotificationTypeResponseV1> putNotificationType(
            @ApiParam(value = "UUID that identifies the Notification TypeEnum to be modified", required = true)
            @PathVariable(name = "UUID") UUID uuid,
            @ApiParam(value = "Updated body of the Notification TypeEnum", required = true)
            @RequestBody NotificationTypeRequestV1 notificationTypeRequest) {

        requestValidator.validateRequest(notificationTypeRequest, MessageCode.E00X_1000);
        NotificationTypeEntity notificationTypeEntity = NotificationTypeAdapter.adptNotificationTypeRequestToNotificationTypeEntity(notificationTypeRequest);
        notificationTypeEntity = notificationTypeService.updateNotificationType(notificationTypeEntity, uuid);
        NotificationTypeResponseV1 response = NotificationTypeAdapter.adptNotificationTypeEntityToNotificationTypeResponse(notificationTypeEntity);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping(value = "/notificationtype/{UUID}",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Recover Notification TypeEnum", notes = "Returns a Notification TypeEnum using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = NotificationTypeResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<NotificationTypeResponseV1> getNotificationType(
            @ApiParam(value = "UUID of the Notification TypeEnum to be found", required = true)
            @PathVariable(name = "UUID") UUID uuid) {

        NotificationTypeEntity notificationTypeEntity = notificationTypeService.getNotificationType(uuid);
        NotificationTypeResponseV1 response = NotificationTypeAdapter.adptNotificationTypeEntityToNotificationTypeResponse(notificationTypeEntity);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping(value = "/notificationtype",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Recover Notification Types", notes = "Returns all the Notification Types")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = NotificationTypeResponseV1.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<NotificationTypeResponseV1>> getAllNotificationType() {

        List<NotificationTypeEntity> notificationTypeEntities = notificationTypeService.getAllNotificationType();
        List<NotificationTypeResponseV1> response = NotificationTypeAdapter.adptNotificationTypeEntityListToNotificationTypeResponseList(notificationTypeEntities);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @DeleteMapping(value = "/notificationtype/{UUID}",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Delete Notification TypeEnum", notes = "Delete a Notification TypeEnum using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = NotificationTypeResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @Transactional
    public ResponseEntity deleteNotificationType(@PathVariable(name = "UUID") UUID uuid) {

        NotificationTypeEntity notificationTypeEntity = notificationTypeService.deleteNotificationType(uuid);
        if (notificationTypeEntity == null)
            return new ResponseEntity<>(notificationTypeEntity, HttpStatus.OK);
        NotificationTypeResponseV1 response = NotificationTypeAdapter.adptNotificationTypeEntityToNotificationTypeResponse(notificationTypeEntity);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PatchMapping("/notificationtype/{UUID}/active")
    @Transactional
    @ApiOperation(value = "Patch active status NotificationType", notes = "Patch active status of NotificationType using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = NotificationTypeResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<NotificationTypeResponseV1> patchStatusNotificationType(
            @PathVariable UUID UUID
    ) {
        NotificationTypeEntity notificationTypeEntity = notificationTypeService.updateStatus(UUID);
        NotificationTypeResponseV1 responseV1 = NotificationTypeAdapter.adptNotificationTypeEntityToNotificationTypeResponse(notificationTypeEntity);
        return new ResponseEntity<>(responseV1, HttpStatus.OK);
    }

    @ApiOperation(value="NotificationType Pagination", notes = "It retrieves NotificationType pagination")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = ClaimsResponsePaginationV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @GetMapping(value="/notificationtype/pagination", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<ClaimsResponsePaginationV1> notificationTypePagination(
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @RequestParam(value = "page_size", defaultValue = "20") Integer pageSize,
            @RequestParam(value = "description", required = false) String description,
            @RequestParam(value = "is_active", required = false) Boolean isActive,
            @RequestParam(value = "order_id", required = false) Integer orderId,
            @RequestParam(value = "order_by", required = false) String orderBy,
            @RequestParam(value = "asc", required = false, defaultValue = "true") Boolean asc
    ){

        Pagination<NotificationTypeEntity> notificationTypeEntityPagination = notificationTypeService.paginationNotificationType(page, pageSize, description, isActive, orderId, orderBy, asc);

        return new ResponseEntity<>(NotificationTypeAdapter.adptNotificationTypePaginationToClaimsResponsePagination(notificationTypeEntityPagination), HttpStatus.OK);
    }
}
