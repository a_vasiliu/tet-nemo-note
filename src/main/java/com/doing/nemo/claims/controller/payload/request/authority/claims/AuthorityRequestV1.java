package com.doing.nemo.claims.controller.payload.request.authority.claims;

import com.doing.nemo.claims.entity.enumerated.AuthorityEnum.AuthorityTypeEnum;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AuthorityRequestV1 implements Serializable {

    @JsonProperty("is_invoiced")
    private String isInvoiced;

    @JsonProperty("authority_dossier_id")
    private String authorityDossierId;

    @JsonProperty("authority_working_id")
    private String authorityWorkingId;

    /*
    @JsonProperty("claims_id_list")
    private List<String> claimsIdList;
    */

    @JsonProperty("claims_list")
    private List<ClaimsAuthorityRequestV1> claimsList;

    @JsonProperty("plate")
    private String palte;

    @JsonProperty("type")
    private AuthorityTypeEnum type;

    @JsonProperty("working_number")
    private String workingNumber;

    @JsonProperty("authority_dossier_number")
    private String authorityDossierNumber;

    @JsonProperty("commodity_details")
    private CommodityDetailsRequestV1 commodityDetails;

    public String getIsInvoiced() {
        return isInvoiced;
    }

    public void setIsInvoiced(String isInvoiced) {
        this.isInvoiced = isInvoiced;
    }

    public String getAuthorityDossierId() {
        return authorityDossierId;
    }

    public void setAuthorityDossierId(String authorityDossierId) {
        this.authorityDossierId = authorityDossierId;
    }

    public String getAuthorityWorkingId() {
        return authorityWorkingId;
    }

    public void setAuthorityWorkingId(String authorityWorkingId) {
        this.authorityWorkingId = authorityWorkingId;
    }

    public String getAuthorityDossierNumber() {
        return authorityDossierNumber;
    }

    public void setAuthorityDossierNumber(String authorityDossierNumber) {
        this.authorityDossierNumber = authorityDossierNumber;
    }

    /*
    public List<String> getClaimsIdList() {
        return claimsIdList;
    }

    public void setClaimsIdList(List<String> claimsIdList) {
        this.claimsIdList = claimsIdList;
    }*/

    public String getPalte() {
        return palte;
    }

    public void setPalte(String palte) {
        this.palte = palte;
    }

    public AuthorityTypeEnum getType() {
        return type;
    }

    public void setType(AuthorityTypeEnum type) {
        this.type = type;
    }

    public String getWorkingNumber() {
        return workingNumber;
    }

    public void setWorkingNumber(String workingNumber) {
        this.workingNumber = workingNumber;
    }

    public CommodityDetailsRequestV1 getCommodityDetails() {
        return commodityDetails;
    }

    public void setCommodityDetails(CommodityDetailsRequestV1 commodityDetails) {
        this.commodityDetails = commodityDetails;
    }

    public List<ClaimsAuthorityRequestV1> getClaimsList() {
        if(claimsList == null){
            return null;
        }
        return new ArrayList<>(claimsList);
    }

    public void setClaimsList(List<ClaimsAuthorityRequestV1> claimsList) {
        if(claimsList != null){
            this.claimsList = new ArrayList<>(claimsList);
        } else {
            this.claimsList = null;
        }
    }

    @Override
    public String toString() {
        return "AuthorityRequestV1{" +
                "isInvoiced='" + isInvoiced + '\'' +
                ", authorityDossierId='" + authorityDossierId + '\'' +
                ", authorityWorkingId='" + authorityWorkingId + '\'' +
                ", claimsList=" + claimsList +
                ", palte='" + palte + '\'' +
                ", type=" + type +
                ", workingNumber='" + workingNumber + '\'' +
                ", authorityDossierNumber='" + authorityDossierNumber + '\'' +
                ", commodityDetails=" + commodityDetails +
                '}';
    }
}
