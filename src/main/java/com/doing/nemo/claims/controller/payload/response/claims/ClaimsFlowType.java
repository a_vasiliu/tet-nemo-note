package com.doing.nemo.claims.controller.payload.response.claims;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;

public class ClaimsFlowType implements Serializable {

    private static final long serialVersionUID = -9019284806638032451L;

    @JsonProperty("type")
    private String type;
    @JsonProperty("claimsAccidentType")
    private String claimsAccidentType;
    @JsonProperty("flow")
    private String flow;
    @JsonProperty("typeRisk")
    private String typeRisk;

    public ClaimsFlowType() {
    }

    public ClaimsFlowType(String type, String claimsAccidentType, String flow, String typeRisk) {
        this.type = type;
        this.claimsAccidentType = claimsAccidentType;
        this.flow = flow;
        this.typeRisk = typeRisk;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getClaimsAccidentType() {
        return claimsAccidentType;
    }

    public void setClaimsAccidentType(String claimsAccidentType) {
        this.claimsAccidentType = claimsAccidentType;
    }

    public String getFlow() {
        return flow;
    }

    public void setFlow(String flow) {
        this.flow = flow;
    }

    public String getTypeRisk() {
        return typeRisk;
    }

    public void setTypeRisk(String typeRisk) {
        this.typeRisk = typeRisk;
    }

    @Override
    public String toString() {
        return "ClaimsFlowType{" +
                "type='" + type + '\'' +
                ", claimsAccidentType='" + claimsAccidentType + '\'' +
                ", flow='" + flow + '\'' +
                ", typeRisk='" + typeRisk + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        ClaimsFlowType that = (ClaimsFlowType) o;

        return new EqualsBuilder().append(type, that.type).append(claimsAccidentType, that.claimsAccidentType)
                .append(flow, that.flow).append(typeRisk, that.typeRisk).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(type).append(claimsAccidentType)
                .append(flow).append(typeRisk).toHashCode();
    }
}
