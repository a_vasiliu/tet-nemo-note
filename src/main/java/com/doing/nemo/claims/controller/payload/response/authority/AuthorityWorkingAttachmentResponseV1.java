package com.doing.nemo.claims.controller.payload.response.authority;

import com.doing.nemo.claims.controller.payload.response.UploadFileResponseV1;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AuthorityWorkingAttachmentResponseV1 implements Serializable {
    @JsonProperty("attachments")
    private List<UploadFileResponseV1> attachments;

    @JsonProperty("working_id")
    private String workingId;

    @JsonProperty("working_number")
    private String workingNumber;

    public List<UploadFileResponseV1> getAttachments() {
        if(attachments == null){
            return null;
        }
        return new ArrayList<>(attachments);
    }

    public void setAttachments(List<UploadFileResponseV1> attachments) {
        if(attachments != null)
        {
            this.attachments =  new ArrayList<>(attachments);
        } else {
            this.attachments = null;
        }
    }

    public String getWorkingId() {
        return workingId;
    }

    public void setWorkingId(String workingId) {
        this.workingId = workingId;
    }

    public String getWorkingNumber() {
        return workingNumber;
    }

    public void setWorkingNumber(String workingNumber) {
        this.workingNumber = workingNumber;
    }

    @Override
    public String toString() {
        return "AuthorityWorkingAttachmentResponseV1{" +
                "attachments=" + attachments +
                ", workingId='" + workingId + '\'' +
                ", workingNumber='" + workingNumber + '\'' +
                '}';
    }
}
