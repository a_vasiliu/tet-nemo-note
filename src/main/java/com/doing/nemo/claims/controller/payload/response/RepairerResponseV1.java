package com.doing.nemo.claims.controller.payload.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.UUID;

public class RepairerResponseV1 implements Serializable {

    @JsonProperty("id")
    private UUID id;

    @JsonProperty("supplier_code")
    private String supplierCode;

    @JsonProperty("business_name")
    private String businessName;

    @JsonProperty("address")
    private String address;

    @JsonProperty("zip_code")
    private String zipCode;

    @JsonProperty( "locality")
    private String locality;

    @JsonProperty("prov")
    private String prov;

    @JsonProperty("reference_person")
    private String referencePerson;

    @JsonProperty("phone")
    private String phone;

    @JsonProperty("fax")
    private String fax;

    @JsonProperty("email")
    private String email;

    @JsonProperty("web_site")
    private String webSite;

    @JsonProperty("vat_number")
    private String vatNumber;

    @JsonProperty("fiscal_code")
    private String fiscalCode;

    @JsonProperty("legal_representative")
    private String legalRepresentative;

    @JsonProperty("is_active")
    private Boolean isActive;

    public RepairerResponseV1() {
    }

    public RepairerResponseV1(UUID id, String supplierCode, String businessName, String address, String zipCode, String locality, String prov, String referencePerson, String phone, String fax, String email, String webSite, String vatNumber, String fiscalCode, String legalRepresentative, Boolean isActive) {
        this.id = id;
        this.supplierCode = supplierCode;
        this.businessName = businessName;
        this.address = address;
        this.zipCode = zipCode;
        this.locality = locality;
        this.prov = prov;
        this.referencePerson = referencePerson;
        this.phone = phone;
        this.fax = fax;
        this.email = email;
        this.webSite = webSite;
        this.vatNumber = vatNumber;
        this.fiscalCode = fiscalCode;
        this.legalRepresentative = legalRepresentative;
        this.isActive = isActive;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getSupplierCode() {
        return supplierCode;
    }

    public void setSupplierCode(String supplierCode) {
        this.supplierCode = supplierCode;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getProv() {
        return prov;
    }

    public void setProv(String prov) {
        this.prov = prov;
    }

    public String getReferencePerson() {
        return referencePerson;
    }

    public void setReferencePerson(String referencePerson) {
        this.referencePerson = referencePerson;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebSite() {
        return webSite;
    }

    public void setWebSite(String webSite) {
        this.webSite = webSite;
    }

    public String getVatNumber() {
        return vatNumber;
    }

    public void setVatNumber(String vatNumber) {
        this.vatNumber = vatNumber;
    }

    public String getFiscalCode() {
        return fiscalCode;
    }

    public void setFiscalCode(String fiscalCode) {
        this.fiscalCode = fiscalCode;
    }

    public String getLegalRepresentative() {
        return legalRepresentative;
    }

    public void setLegalRepresentative(String legalRepresentative) {
        this.legalRepresentative = legalRepresentative;
    }

    @JsonIgnore
    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    @Override
    public String toString() {
        return "RepairerResponseV1{" +
                "id=" + id +
                ", supplierCode='" + supplierCode + '\'' +
                ", businessName='" + businessName + '\'' +
                ", address='" + address + '\'' +
                ", zipCode=" + zipCode +
                ", locality='" + locality + '\'' +
                ", prov='" + prov + '\'' +
                ", referencePerson='" + referencePerson + '\'' +
                ", phone='" + phone + '\'' +
                ", fax='" + fax + '\'' +
                ", email='" + email + '\'' +
                ", webSite='" + webSite + '\'' +
                ", vatNumber='" + vatNumber + '\'' +
                ", fiscalCode='" + fiscalCode + '\'' +
                ", legalRepresentative='" + legalRepresentative + '\'' +
                ", isActive=" + isActive +
                '}';
    }
}
