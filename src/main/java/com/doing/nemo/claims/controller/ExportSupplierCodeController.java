package com.doing.nemo.claims.controller;

import com.doing.nemo.claims.adapter.ExportSupplierCodeAdapter;
import com.doing.nemo.claims.common.validation.MessageCode;
import com.doing.nemo.claims.common.validation.RequestValidator;
import com.doing.nemo.claims.controller.payload.request.ExportSupplierCodeRequestV1;
import com.doing.nemo.claims.controller.payload.request.ListRequestV1;
import com.doing.nemo.claims.controller.payload.response.ClaimsResponsePaginationV1;
import com.doing.nemo.claims.controller.payload.response.IdResponseV1;
import com.doing.nemo.claims.controller.payload.response.ExportSupplierCodeResponseV1;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.settings.ExportSupplierCodeEntity;
import com.doing.nemo.claims.service.ExportSupplierCodeService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/v1")
@Api("Export Supplier Code")
public class ExportSupplierCodeController {
    @Autowired
    private ExportSupplierCodeService exportSupplierCodeService;

    @Autowired
    private ExportSupplierCodeAdapter exportSupplierCodeAdapter;

    @Autowired
    private RequestValidator requestValidator;

    @PostMapping("/export/suppliercode")
    @Transactional
    @ApiOperation(value = "Insert Export Supplier Code", notes = "Insert Export Supplier Code using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = ExportSupplierCodeResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<IdResponseV1> saveExportSupplierCode(
            @RequestBody ExportSupplierCodeRequestV1 exportSupplierCodeRequestV1
            )
    {
        requestValidator.validateRequest(exportSupplierCodeRequestV1, MessageCode.E00X_1000);
        ExportSupplierCodeEntity exportSupplierCodeEntity = ExportSupplierCodeAdapter.adptFromExportSupplierCodeRequestToExportSupplierCodeEntity(exportSupplierCodeRequestV1);
        exportSupplierCodeEntity = exportSupplierCodeService.saveExportSupplierCode(exportSupplierCodeEntity);
        return new ResponseEntity<>(new IdResponseV1(exportSupplierCodeEntity.getId()), HttpStatus.CREATED);
    }

    @PostMapping("/export/suppliercode/all")
    @Transactional
    @ApiOperation(value = "Insert a list of Export Supplier Code", notes = "Insert a list of insurance companies using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = ExportSupplierCodeResponseV1.class, responseContainer="List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<ExportSupplierCodeResponseV1>> insertExportSupplierCodeAll(
            @ApiParam(value = "List of the bodies of the Export Supplier Code to be created", required = true)
            @RequestBody ListRequestV1<ExportSupplierCodeRequestV1> insuranceCompaniesRequest){

        List<ExportSupplierCodeResponseV1> exportSupplierCodeResponseV1List = new ArrayList<>();
        for (ExportSupplierCodeRequestV1 exportSupplierCodeRequestV1 : insuranceCompaniesRequest.getData())
        {

            ExportSupplierCodeEntity exportSupplierCodeEntity = ExportSupplierCodeAdapter.adptFromExportSupplierCodeRequestToExportSupplierCodeEntity(exportSupplierCodeRequestV1);
            exportSupplierCodeEntity = exportSupplierCodeService.saveExportSupplierCode(exportSupplierCodeEntity);
            ExportSupplierCodeResponseV1 responseV1 = ExportSupplierCodeAdapter.adptFromExportSupplierCodeEntityToExportSupplierCodeResponse(exportSupplierCodeEntity);
            exportSupplierCodeResponseV1List.add(responseV1);
        }
        return new ResponseEntity<>(exportSupplierCodeResponseV1List, HttpStatus.CREATED);

    }

    @GetMapping("/export/suppliercode/{UUID}")
    @Transactional
    @ApiOperation(value = "Recover Export Supplier Code", notes = "Returns a Export Supplier Code using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ExportSupplierCodeResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ExportSupplierCodeResponseV1> getExportSupplierCode(
            @PathVariable("UUID") UUID uuid
    ){
        ExportSupplierCodeEntity exportSupplierCodeEntity = exportSupplierCodeService.getExportSupplierCode(uuid);
        return new ResponseEntity<>(exportSupplierCodeAdapter.adptFromExportSupplierCodeEntityToExportSupplierCodeResponse(exportSupplierCodeEntity), HttpStatus.OK);
    }

    @GetMapping("/export/suppliercode")
    @Transactional
    @ApiOperation(value = "Recover Export Supplier Code", notes = "Returns a Export Supplier Code using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ExportSupplierCodeResponseV1.class, responseContainer="List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<ExportSupplierCodeResponseV1>> getExportSupplierCode(
    ){
        List<ExportSupplierCodeEntity> exportSupplierCodeEntityList = exportSupplierCodeService.getAllExportSupplierCode();
        return new ResponseEntity<>(exportSupplierCodeAdapter.adptFromExportSupplierCodeEntityToExportSupplierCodeResponseList(exportSupplierCodeEntityList), HttpStatus.OK);
    }

    @PutMapping("/export/suppliercode/{UUID}")
    @Transactional
    @ApiOperation(value = "Insert Export Supplier Code", notes = "Insert Export Supplier Code using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = ExportSupplierCodeResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ExportSupplierCodeResponseV1> updateExportSupplierCode(
            @RequestBody ExportSupplierCodeRequestV1 exportSupplierCodeRequestV1,
            @PathVariable("UUID") UUID uuid
    )
    {
        requestValidator.validateRequest(exportSupplierCodeRequestV1, MessageCode.E00X_1000);
        ExportSupplierCodeEntity exportSupplierCodeEntity = exportSupplierCodeAdapter.adptFromExportSupplierCodeRequestToExportSupplierCodeEntityWithID(exportSupplierCodeRequestV1, uuid);
        exportSupplierCodeEntity = exportSupplierCodeService.updateExportSupplierCode(exportSupplierCodeEntity);
        return new ResponseEntity<>(ExportSupplierCodeAdapter.adptFromExportSupplierCodeEntityToExportSupplierCodeResponse(exportSupplierCodeEntity), HttpStatus.OK);
    }

    @DeleteMapping("/export/suppliercode/{UUID}")
    @Transactional
    @ApiOperation(value = "Delete  Export Supplier Code", notes = "Delete Export Supplier Code using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity deleteExportSupplierCode(
            @PathVariable("UUID") UUID uuid
    ){
        exportSupplierCodeService.deleteExportSupplierCode(uuid);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PatchMapping("/export/suppliercode/{UUID}/active")
    @Transactional
    @ApiOperation(value = "Patch status active Supplier Code", notes = "Patch status active Supplier Code using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ExportSupplierCodeResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ExportSupplierCodeResponseV1> patchStatusExportSupplierCode(
            @PathVariable UUID UUID
    ) {
        ExportSupplierCodeEntity exportSupplierCodeEntity = exportSupplierCodeService.patchActive(UUID);
        ExportSupplierCodeResponseV1 responseV1 = exportSupplierCodeAdapter.adptFromExportSupplierCodeEntityToExportSupplierCodeResponse(exportSupplierCodeEntity);
        return new ResponseEntity<>(responseV1, HttpStatus.OK);
    }

    @ApiOperation(value="Export Supplier Code Pagination", notes = "It retrieves Export Supplier Code pagination")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = ClaimsResponsePaginationV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @GetMapping(value="export/suppliercode/pagination", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<ClaimsResponsePaginationV1> supplierCodePagination(
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @RequestParam(value = "page_size", defaultValue = "20") Integer pageSize,
            @RequestParam(value = "order_by", required = false) String orderBy,
            @RequestParam(value = "asc", required = false, defaultValue = "true") Boolean asc,
            @RequestParam(value = "vat_number", required = false) String vatNumber,
            @RequestParam(value = "denomination", required = false) String denomination,
            @RequestParam(value = "code_to_export", required = false) String code,
            @RequestParam(value = "is_active", required = false) Boolean isActive
    ){

        Pagination<ExportSupplierCodeEntity> exportSupplierCodeEntityPagination = exportSupplierCodeService.findExportSupplierCode(page, pageSize, orderBy, asc, vatNumber, denomination, code, isActive);

        return new ResponseEntity<>(exportSupplierCodeAdapter.adptFromExportSupplierCodePaginationToNotificationResponsePagination(exportSupplierCodeEntityPagination), HttpStatus.OK);
    }

}
