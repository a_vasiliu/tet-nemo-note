package com.doing.nemo.claims.controller.payload.response;

import com.doing.nemo.claims.entity.settings.ClaimsTypeEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.UUID;

public class RecoverabilityResponseV1 implements Serializable {

    @JsonProperty("id")
    private UUID id;

    @JsonProperty("management")
    private String management;

    @JsonProperty("claims_type")
    private ClaimsTypeEntity claimsType;

    @JsonProperty("counterpart")
    private Boolean counterpart;

    @JsonProperty("recoverability")
    private Boolean recoverability;

    @JsonProperty("percent_recoverability")
    private Double percentRecoverability;

    @JsonProperty("is_active")
    private Boolean isActive;

    public RecoverabilityResponseV1() {
    }

    public RecoverabilityResponseV1(UUID id, String management, ClaimsTypeEntity claimsType, Boolean counterpart, Boolean recoverability, Double percentRecoverability, Boolean isActive) {
        this.id = id;
        this.management = management;
        this.claimsType = claimsType;
        this.counterpart = counterpart;
        this.recoverability = recoverability;
        this.percentRecoverability = percentRecoverability;
        this.isActive = isActive;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getManagement() {
        return management;
    }

    public void setManagement(String management) {
        this.management = management;
    }

    public ClaimsTypeEntity getClaimsType() {
        return claimsType;
    }

    public void setClaimsType(ClaimsTypeEntity claimsType) {
        this.claimsType = claimsType;
    }

    public Boolean getCounterpart() {
        return counterpart;
    }

    public void setCounterpart(Boolean counterpart) {
        this.counterpart = counterpart;
    }

    public Boolean getRecoverability() {
        return recoverability;
    }

    public void setRecoverability(Boolean recoverability) {
        this.recoverability = recoverability;
    }

    public Double getPercentRecoverability() {
        return percentRecoverability;
    }

    public void setPercentRecoverability(Double percentRecoverability) {
        this.percentRecoverability = percentRecoverability;
    }

    @JsonIgnore
    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    @Override
    public String toString() {
        return "RecoverabilityResponseV1{" +
                "id=" + id +
                ", management='" + management + '\'' +
                ", claimsType=" + claimsType +
                ", counterpart=" + counterpart +
                ", recoverability=" + recoverability +
                ", percentRecoverability=" + percentRecoverability +
                ", isActive=" + isActive +
                '}';
    }
}
