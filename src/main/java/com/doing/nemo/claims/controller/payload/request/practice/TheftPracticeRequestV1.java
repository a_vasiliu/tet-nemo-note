package com.doing.nemo.claims.controller.payload.request.practice;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Date;

public class TheftPracticeRequestV1 implements Serializable {

    private static final long serialVersionUID = 809173064960583663L;

    @JsonProperty("theft_date")
    private Date theftDate;

    @JsonProperty("theft_id")
    private String theftId;

    @JsonProperty("complaint_police")
    private Boolean complaintPolice;

    @JsonProperty("complaint_cc")
    private Boolean complaintCc;

    @JsonProperty("complaint_vvuu")
    private Boolean complaintVvuu;

    @JsonProperty("complaint_gdf")
    private Boolean complaintGdf;

    @JsonProperty("authority_data")
    private String authorityData;

    @JsonProperty("note")
    private String note;

    @JsonProperty("happened_abroad")
    private Boolean happenedAbroad;

    @JsonProperty("theft_location")
    private String theftLocation;

    @JsonProperty("province")
    private String province;

    @JsonProperty("state")
    private String state;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Date getTheftDate() {
        if(theftDate == null){
            return null;
        }
        return (Date)theftDate.clone();
    }

    public void setTheftDate(Date theftDate) {
        if(theftDate != null)
        {
            this.theftDate = (Date)theftDate.clone();
        } else {
            this.theftDate = null;
        }
    }

    public String getTheftId() {
        return theftId;
    }

    public void setTheftId(String theftId) {
        this.theftId = theftId;
    }

    public Boolean getComplaintPolice() {
        return complaintPolice;
    }

    public void setComplaintPolice(Boolean complaintPolice) {
        this.complaintPolice = complaintPolice;
    }

    public Boolean getComplaintCc() {
        return complaintCc;
    }

    public void setComplaintCc(Boolean complaintCc) {
        this.complaintCc = complaintCc;
    }

    public Boolean getComplaintVvuu() {
        return complaintVvuu;
    }

    public void setComplaintVvuu(Boolean complaintVvuu) {
        this.complaintVvuu = complaintVvuu;
    }

    public Boolean getComplaintGdf() {
        return complaintGdf;
    }

    public void setComplaintGdf(Boolean complaintGdf) {
        this.complaintGdf = complaintGdf;
    }

    public String getAuthorityData() {
        return authorityData;
    }

    public void setAuthorityData(String authorityData) {
        this.authorityData = authorityData;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Boolean getHappenedAbroad() {
        return happenedAbroad;
    }

    public void setHappenedAbroad(Boolean happenedAbroad) {
        this.happenedAbroad = happenedAbroad;
    }

    public String getTheftLocation() {
        return theftLocation;
    }

    public void setTheftLocation(String theftLocation) {
        this.theftLocation = theftLocation;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    @Override
    public String toString() {
        return "TheftPracticeRequestV1{" +
                "theftDate=" + theftDate +
                ", theftId='" + theftId + '\'' +
                ", complaintPolice=" + complaintPolice +
                ", complaintCc=" + complaintCc +
                ", complaintVvuu=" + complaintVvuu +
                ", complaintGdf=" + complaintGdf +
                ", authorityData='" + authorityData + '\'' +
                ", note='" + note + '\'' +
                ", happenedAbroad=" + happenedAbroad +
                ", theftLocation='" + theftLocation + '\'' +
                ", province='" + province + '\'' +
                ", state='" + state + '\'' +
                '}';
    }
}
