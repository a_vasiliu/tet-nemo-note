package com.doing.nemo.claims.controller.payload.response;

import com.doing.nemo.claims.controller.payload.response.authority.claims.PoNumberErrorResponseV1;
import com.doing.nemo.claims.controller.payload.response.authority.claims.PoNumberResponseV1;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@JsonIgnoreProperties(ignoreUnknown = true)
public class OlsaInvoicingResponseV1 implements Serializable {

    @JsonProperty("found_po_list")
    private List<PoNumberResponseV1> foundPoList;

    @JsonProperty("not_found_po_list")
    private List<PoNumberErrorResponseV1> notFoundPoList;

    public List<PoNumberResponseV1> getFoundPoList() {
        if(foundPoList == null){
            return null;
        }
        return new ArrayList<>(foundPoList);
    }

    public void setFoundPoList(List<PoNumberResponseV1> foundPoList) {
        if(foundPoList != null)
        {
            this.foundPoList = new ArrayList<>(foundPoList);
        } else {
            this.foundPoList = null;
        }
    }

    public List<PoNumberErrorResponseV1> getNotFoundPoList() {
        if(notFoundPoList == null){
            return null;
        }
        return new ArrayList<>(notFoundPoList);
    }

    public void setNotFoundPoList(List<PoNumberErrorResponseV1> notFoundPoList) {
        if(notFoundPoList != null)
        {
            this.notFoundPoList = new ArrayList<>(notFoundPoList);
        } else {
            this.notFoundPoList = null;
        }
    }

    @Override
    public String toString() {
        return "OlsaInvoicingResponseV1{" +
                "foundPoList=" + foundPoList +
                ", notFoundPoList=" + notFoundPoList +
                '}';
    }
}
