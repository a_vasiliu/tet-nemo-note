package com.doing.nemo.claims.controller.payload.response.authority.claims;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class UserDetailsResponseV1 implements Serializable {

    @JsonProperty("user_id")
    private String userId;

    @JsonProperty("first_name")
    private String firstname;

    @JsonProperty("last_name")
    private String lastname;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
}
