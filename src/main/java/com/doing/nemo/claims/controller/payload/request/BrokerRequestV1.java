package com.doing.nemo.claims.controller.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.UUID;

public class BrokerRequestV1 implements Serializable {

    private UUID id;

    @JsonProperty("business_name")
    @NotNull
    private String businessName;

    @JsonProperty("address")
    private String address = "";

    @JsonProperty("zip_code")
    private String zipCode;

    @JsonProperty("city")
    private String city = "";

    @JsonProperty("province")
    private String province = "";

    @JsonProperty("state")
    @NotNull
    private String state;

    @JsonProperty("phone_number")
    private String phoneNumber = "";

    @JsonProperty("fax")
    private String fax = "";

    @JsonProperty("email")
    private String email = "";

    @JsonProperty("web_site")
    private String webSite = "";

    @JsonProperty("contact")
    private String contact = "";

    @JsonProperty("is_active")
    private Boolean isActive = true;


    public BrokerRequestV1(UUID id,@NotNull String businessName, String address, String zipCode, String city, String province,@NotNull String state, String phoneNumber, String fax, String email, String webSite, String contact) {
        this.id = id;
        this.businessName = businessName;
        this.address = address;
        this.zipCode = zipCode;
        this.city = city;
        this.province = province;
        this.state = state;
        this.phoneNumber = phoneNumber;
        this.fax = fax;
        this.email = email;
        this.webSite = webSite;
        this.contact = contact;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebSite() {
        return webSite;
    }

    public void setWebSite(String webSite) {
        this.webSite = webSite;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public Boolean getActive() {
        return isActive;
    }

    @JsonSetter
    public void setActive(Boolean active) {
        if (isActive != null)
            isActive = active;
    }

    @Override
    public String toString() {
        return "BrokerRequestV1{" +
                "id=" + id +
                ", businessName='" + businessName + '\'' +
                ", address='" + address + '\'' +
                ", zipCode=" + zipCode +
                ", city='" + city + '\'' +
                ", province='" + province + '\'' +
                ", state='" + state + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", fax='" + fax + '\'' +
                ", email='" + email + '\'' +
                ", webSite='" + webSite + '\'' +
                ", contact='" + contact + '\'' +
                '}';
    }
}
