package com.doing.nemo.claims.controller.payload.request;

import com.doing.nemo.claims.entity.settings.AttachmentTypeEntity;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class AttachmentRequestV1 implements Serializable {

    @JsonProperty("blob_name")
    private String blobName = "";

    @JsonProperty("resource_type")
    private String resourceType = "";

    @JsonProperty("resource_id")
    private String resourceId = "";

    @JsonProperty("file_name")
    private String fileName = "";

    @JsonProperty("file_size")
    private Integer fileSize;

    @JsonProperty("mime_type")
    private String mimeType = "";

    @JsonProperty("description")
    private String description = "";

    @JsonProperty("blob_type")
    private String blobType = "";

    @JsonProperty("id_filemanager")
    private String idFilemanager = "";

    @JsonProperty("user_id")
    private String userId;

    @JsonProperty("attachment_type")
    private AttachmentTypeEntity attachmentType;

    /*@JsonProperty("created_at")
    private Date createdAt;*/

    public AttachmentRequestV1(String blobName, String resourceType, String resourceId, String fileName, Integer fileSize, String mimeType, String description, String blobType, String idFilemanager, String userId, AttachmentTypeEntity attachmentType) {
        this.blobName = blobName;
        this.resourceType = resourceType;
        this.resourceId = resourceId;
        this.fileName = fileName;
        this.fileSize = fileSize;
        this.mimeType = mimeType;
        this.description = description;
        this.blobType = blobType;
        this.idFilemanager = idFilemanager;
        this.userId = userId;
        this.attachmentType = attachmentType;
    }

    public AttachmentRequestV1() {
    }

    public String getBlobName() {
        return blobName;
    }

    public void setBlobName(String blobName) {
        this.blobName = blobName;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public String getResourceId() {
        return resourceId;
    }

    public void setResourceId(String resourceId) {
        this.resourceId = resourceId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Integer getFileSize() {
        return fileSize;
    }

    public void setFileSize(Integer fileSize) {
        this.fileSize = fileSize;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBlobType() {
        return blobType;
    }

    public void setBlobType(String blobType) {
        this.blobType = blobType;
    }

    public String getIdFilemanager() {
        return idFilemanager;
    }

    public void setIdFilemanager(String idFilemanager) {
        this.idFilemanager = idFilemanager;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public AttachmentTypeEntity getAttachmentType() {
        return attachmentType;
    }

    public void setAttachmentType(AttachmentTypeEntity attachmentType) {
        this.attachmentType = attachmentType;
    }

    @Override
    public String toString() {
        return "AttachmentRequestV1{" +
                "blobName='" + blobName + '\'' +
                ", resourceType='" + resourceType + '\'' +
                ", resourceId='" + resourceId + '\'' +
                ", fileName='" + fileName + '\'' +
                ", fileSize=" + fileSize +
                ", mimeType='" + mimeType + '\'' +
                ", description='" + description + '\'' +
                ", blobType='" + blobType + '\'' +
                ", idFilemanager='" + idFilemanager + '\'' +
                ", userId=" + userId +
                ", attachmentType=" + attachmentType +
                '}';
    }
}
