package com.doing.nemo.claims.controller;

import com.doing.nemo.claims.controller.payload.response.LawyerPaymentsResponseV1;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.service.LawyerPaymentsService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/v1")
@Api("LawyerPayment")
public class LawyerPaymentsController {

    @Autowired
    private LawyerPaymentsService lawyerPaymentsService;


    @GetMapping("/lawyer/payments/pagination")
    @Transactional
    @ApiOperation(value = "Recover All Lawyer Payment", notes = "Returns all Lawyer Payments")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = LawyerPaymentsResponseV1.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<Pagination> getLawyerPaymentsPagination(
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @RequestParam(value = "page_size", defaultValue = "20") Integer pageSize,
            @ApiParam(value = "Filter the search by practice_id", required = false)
            @RequestParam(value = "practice_id", required = false) Long practiceId,
            @ApiParam(value = "Filter the search for plate", required = false)
            @RequestParam(value = "plate", required = false) String plate,
            @ApiParam(value = "Filter the search by acc/salw", required = false)
            @RequestParam(value = "acc_sale", required = false) String accSale,
            @ApiParam(value = "Filter the search by payment_type", required = false)
            @RequestParam(value = "payment_type", required = false) String paymentType,
            @ApiParam(value = "Filter the search by lawyer_code", required = false)
            @RequestParam(value = "lawyer_code", required = false) String lawyerCode,
            @ApiParam(value = "Filter the search by bank", required = false)
            @RequestParam(value = "bank", required = false) String bank,
            @ApiParam(value = "Filter the search by status", required = false)
            @RequestParam(value = "status", required = false) String status,
            @ApiParam(value = "Filter the search by date_accident_from", required = false)
            @RequestParam(value = "date_accident_from", required = false) String dateAccidentFrom,
            @ApiParam(value = "Filter the search by date_accident_from", required = false)
            @RequestParam(value = "date_accident_to", required = false) String dateAccidentTo
    ) {
        return new ResponseEntity<>(lawyerPaymentsService.getLawyerPaymentPagination(plate, practiceId, dateAccidentFrom, dateAccidentTo,
                paymentType, bank, lawyerCode, accSale, status, page, pageSize), HttpStatus.OK);
    }


    //REFACTOR
    @PostMapping("/lawyer/payments")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })public ResponseEntity postAssignedLawyerPaymentsToClaims( @RequestParam(name = "lawyer_payments_list") List<UUID> lawyerPaymentsList){


        lawyerPaymentsService.postAssignedLawyerPayments(lawyerPaymentsList);

        return new ResponseEntity(HttpStatus.OK);
    }

}
