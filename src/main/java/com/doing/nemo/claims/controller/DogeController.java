package com.doing.nemo.claims.controller;

import com.doing.nemo.claims.controller.payload.response.DogeResponseV1;
import com.doing.nemo.claims.controller.payload.response.IdentifierResponseV1;
import com.doing.nemo.claims.dto.ClaimsExportFilter;
import com.doing.nemo.claims.dto.CtpExportFilter;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsFlowEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.EntrustedEnum.EntrustedEnum;
import com.doing.nemo.claims.service.DogeEnquService;
import com.doing.nemo.claims.service.FileManagerService;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.exception.InternalException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/v1")
@Api("Doge")
public class DogeController {

    private static Logger LOGGER = LoggerFactory.getLogger(DogeController.class);

    @Autowired
    private DogeEnquService dogeEnquService;

    @Autowired
    private FileManagerService fileManagerService;

    //REFACTOR
    @ApiOperation(value = "Download complaint with counterpart", notes = "")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = IdentifierResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @GetMapping(value = "/pdf/cai/{ID_CLAIM}")
    public ResponseEntity<IdentifierResponseV1> getComplaintWithCounterpart(
            @PathVariable("ID_CLAIM") String idClaim,
            @RequestHeader(name = "nemo-user-id") String userId,
            @RequestHeader(name = "nemo-user-first-name") String firstName,
            @RequestHeader(name = "nemo-user-last-name") String lastName
    ) throws IOException {

        DogeResponseV1 dogeResponseV1 = dogeEnquService.enqueueClaimsWithCounterpart(idClaim);
        fileManagerService.attachPdfFileClaim(idClaim, dogeResponseV1.getDocumentId(), userId, firstName+ " "+lastName, false, false);
        return new ResponseEntity<>(new IdentifierResponseV1(dogeResponseV1.getDocumentId()), HttpStatus.OK);
    }

    //REFACTOR
    @ApiOperation(value = "Download complaint without CAI", notes = "")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = IdentifierResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @GetMapping(value = "/pdf/complaint/{ID_CLAIM}")
    public ResponseEntity<IdentifierResponseV1> getComplaintWithoutCai(
            @PathVariable("ID_CLAIM") String idClaim,
            @RequestHeader(name = "nemo-user-id") String userId,
            @RequestHeader(name = "nemo-user-first-name") String firstName,
            @RequestHeader(name = "nemo-user-last-name") String lastName

    ) throws IOException {
        DogeResponseV1 dogeResponseV1 = dogeEnquService.enqueueClaimsWithoutCai(idClaim);
        fileManagerService.attachPdfFileClaim(idClaim, dogeResponseV1.getDocumentId(), userId, firstName+ " "+lastName, false, false);
        return new ResponseEntity<>(new IdentifierResponseV1(dogeResponseV1.getDocumentId()), HttpStatus.OK);
    }

    //REFACTOR
    @ApiOperation(value = "Download PDF Complaints without counterpart", notes = "It retrieves claims data without counterpart to download")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK", response = IdentifierResponseV1.class),
        @ApiResponse(code = 400, message = "Bad Request"),
        @ApiResponse(code = 403, message = "Forbidden"),
        @ApiResponse(code = 404, message = "Not Found"),
        @ApiResponse(code = 406, message = "Not Acceptable"),
        @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @GetMapping(value = "/pdf/summary/{UUID_CLAIMS}")
    public ResponseEntity<IdentifierResponseV1> getFileComplaintWithoutCounterpart(
       @ApiParam(value = "UUID of the Claim", required = true)
       @PathVariable("UUID_CLAIMS") String idClaim,
       @RequestHeader(name = "nemo-user-id") String userId,
       @RequestHeader(name = "nemo-user-first-name") String firstName,
       @RequestHeader(name = "nemo-user-last-name") String lastName
    ) throws IOException {

        DogeResponseV1 dogeResponseV1 = dogeEnquService.enqueueClaimsWithoutCounterpart(idClaim);
        fileManagerService.attachPdfFileClaim(idClaim, dogeResponseV1.getDocumentId(), userId, firstName+ " "+lastName, false, false);
        return new ResponseEntity<>(new IdentifierResponseV1(dogeResponseV1.getDocumentId()), HttpStatus.OK);
    }

    //REFACTOR
    @ApiOperation(value = "Download PDF Complaints without counterpart", notes = "It retrieves claims data without counterpart to download")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = IdentifierResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @GetMapping(value = "/pdf/pai/{UUID_CLAIMS}")
    public ResponseEntity<IdentifierResponseV1> getFileClaimsPAI(
            @ApiParam(value = "UUID of the Claim", required = true)
            @PathVariable("UUID_CLAIMS") String idClaim,
            @RequestHeader(name = "nemo-user-id") String userId,
            @RequestHeader(name = "nemo-user-first-name") String userName,
            @RequestHeader(name = "nemo-user-last-name") String lastName
    ) throws IOException {

        DogeResponseV1 dogeResponseV1 = dogeEnquService.enqueueClaimsPAI(idClaim);
        fileManagerService.attachPdfFileClaim(idClaim, dogeResponseV1.getDocumentId(), userId, userName + " " + lastName, true, false);
        return new ResponseEntity<>(new IdentifierResponseV1(dogeResponseV1.getDocumentId()), HttpStatus.OK);
    }

    //REFACTOR
    @ApiOperation(value = "Download PDF Frontispiece", notes = "It retrieves claims data for Olsa to download")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = IdentifierResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @GetMapping(value = "/pdf/frontispiece/{UUID_CLAIMS}")
    public ResponseEntity<IdentifierResponseV1> getFileClaimsFrontespizio(
            @ApiParam(value = "UUID of the Claim", required = true)
            @PathVariable("UUID_CLAIMS") String idClaim,
            @RequestHeader(name = "nemo-user-id") String userId,
            @RequestHeader(name = "nemo-user-first-name") String userName,
            @RequestHeader(name = "nemo-user-last-name") String lastName
    ) throws IOException {

        DogeResponseV1 dogeResponseV1 = dogeEnquService.enqueueFrontespizio(idClaim, userName+" "+lastName);
        fileManagerService.attachPdfFileClaim(idClaim, dogeResponseV1.getDocumentId(), userId, userName + " " + lastName, true, false);
        return new ResponseEntity<>(new IdentifierResponseV1(dogeResponseV1.getDocumentId()), HttpStatus.OK);
    }


    @GetMapping(value = "/claims/export/complaint", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = IdentifierResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<IdentifierResponseV1> getClaimsEntityExport(
        @RequestParam(value = "flow", required = false) List<ClaimsFlowEnum> flow,
        @RequestParam(value = "entrusted_type", required = false) EntrustedEnum entrustedType,
        @RequestParam(value = "type_accident", required = false) List<DataAccidentTypeAccidentEnum> typeAccident,
        @RequestParam(value = "status", required = false) List<ClaimsStatusEnum> status,
        @RequestParam(required = false) Map<String, Object> exportFilters,
        @RequestHeader(name = "nemo-user-id", required = true) String nemoUserId,
        @RequestHeader(name = "Authorization", required = true) String authorization
    ) {
        try {
            ClaimsExportFilter filter = new ObjectMapper().convertValue(exportFilters, ClaimsExportFilter.class);
            filter
                .setFlow(flow)
                .setEntrustedType(entrustedType)
                .setTypeAccident(typeAccident)
                .setStatus(status)
            ;
            dogeEnquService.exportClaims(filter, nemoUserId, authorization);
        } catch(BadRequestException | InternalException e){
            LOGGER.error(MessageCode.CLAIMS_1192.value(), e);
            throw e;
        } catch(Exception e){
            LOGGER.error(MessageCode.CLAIMS_1192.value(), e);
            throw new InternalException(MessageCode.CLAIMS_1192, e);
        }
        return new ResponseEntity<>(null, HttpStatus.ACCEPTED);
    }

    @GetMapping(value = "/claims/export/counterparty", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Ok", response = IdentifierResponseV1.class),
        @ApiResponse(code = 400, message = "Bad Request"),
        @ApiResponse(code = 403, message = "Forbidden"),
        @ApiResponse(code = 404, message = "Not Found"),
        @ApiResponse(code = 406, message = "Not Acceptable"),
        @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity getCounterpartyExport(
        @RequestParam(required = false) Map<String, Object> exportFilters,
        @RequestHeader(name = "nemo-user-id", required = true) String nemoUserId,
        @RequestHeader(name = "Authorization") String authorization
    ) {
        try {
            CtpExportFilter filter = new ObjectMapper().convertValue(exportFilters, CtpExportFilter.class);
            dogeEnquService.exportCtp(filter, nemoUserId, authorization);
        } catch(BadRequestException | InternalException e){
            LOGGER.error(MessageCode.CLAIMS_1192.value(), e);
            throw e;
        } catch(Exception e){
            LOGGER.error(MessageCode.CLAIMS_1192.value(), e);
            throw new InternalException(MessageCode.CLAIMS_1192, e);
        }
        return new ResponseEntity<>(null, HttpStatus.ACCEPTED);
    }

    @GetMapping(value = "/claims/export/log", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Ok", response = IdentifierResponseV1.class),
        @ApiResponse(code = 400, message = "Bad Request"),
        @ApiResponse(code = 403, message = "Forbidden"),
        @ApiResponse(code = 404, message = "Not Found"),
        @ApiResponse(code = 406, message = "Not Acceptable"),
        @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity getClaimsEntityExportLog(
        @RequestParam(value = "flow", required = false) List<ClaimsFlowEnum> flow,
        @RequestParam(value = "entrusted_type", required = false) EntrustedEnum entrustedType,
        @RequestParam(value = "type_accident", required = false) List<DataAccidentTypeAccidentEnum> typeAccident,
        @RequestParam(value = "status", required = false) List<ClaimsStatusEnum> status,
        @RequestParam(required = false) Map<String, Object> exportFilters,
        @RequestHeader(name = "nemo-user-id", required = true) String nemoUserId,
        @RequestHeader(name = "Authorization", required = true) String authorization
    ) {
        try {
            ClaimsExportFilter filter = new ObjectMapper().convertValue(exportFilters, ClaimsExportFilter.class);
            filter
                .setFlow(flow)
                .setEntrustedType(entrustedType)
                .setTypeAccident(typeAccident)
                .setStatus(status)
            ;

            dogeEnquService.exportLogs(filter, nemoUserId, authorization);
        } catch(BadRequestException | InternalException e){
            LOGGER.error(MessageCode.CLAIMS_1192.value(), e);
            throw e;
        } catch(Exception e){
            LOGGER.error(MessageCode.CLAIMS_1192.value(), e);
            throw new InternalException(MessageCode.CLAIMS_1192, e);
        }
        return new ResponseEntity<>(null, HttpStatus.ACCEPTED);
    }

    @ApiOperation(value="Export Practice", notes = "It generates excel with loss possession practice")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = IdentifierResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @GetMapping(value="/practice/export", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<IdentifierResponseV1> searchPractice(
            @RequestParam(value = "practice_id_from", required = false) Long practiceIdFrom,
            @RequestParam(value = "practice_id_to", required = false) Long practiceIdTo,
            @RequestParam(value = "license_plate", required = false) String licensePlate,
            @RequestParam(value = "contract_id", required = false) String contractId,
            @RequestParam(value = "date_created_from", required = false) String dateCreatedFrom,
            @RequestParam(value = "date_created_to", required = false) String dateCreatedTo,
            @RequestParam(value = "status", required = false) String status,
            @RequestParam(value = "order_by", required = false) String orderBy,
            @RequestParam(value = "asc", required = false, defaultValue = "true") Boolean asc,
            @RequestParam(value = "fleet_vehicle_id", required = false) String fleetVehicleId
    ) throws IOException {

        DogeResponseV1 dogeResponseV1 = dogeEnquService.enqueueExportPracticeCar(practiceIdFrom, practiceIdTo,licensePlate, contractId,  dateCreatedFrom, dateCreatedTo , status, orderBy, asc, fleetVehicleId);
        return new ResponseEntity<>(new IdentifierResponseV1(dogeResponseV1.getDocumentId()), HttpStatus.OK);
    }

    //REFACTOR
    /*NE-384*/
    @ApiOperation(value = "Download PDF Theft", notes = "It retrieves theft data.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = IdentifierResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @GetMapping(value = "/pdf/theft/summary/{UUID_CLAIMS}")
    public ResponseEntity<IdentifierResponseV1> getFileTheftSummary(@ApiParam(value = "UUID of the Claim", required = true)
                                                                                   @PathVariable("UUID_CLAIMS") String idClaim,
                                                                                   @RequestHeader(name = "nemo-user-id") String userId,
                                                                                   @RequestHeader(name = "nemo-user-first-name") String firstName,
                                                                                   @RequestHeader(name = "nemo-user-last-name") String lastName) throws IOException {

        DogeResponseV1 dogeResponseV1 = dogeEnquService.enqueueFileTheftSummary(idClaim, firstName +" "+lastName);
        fileManagerService.attachPdfFileClaim(idClaim, dogeResponseV1.getDocumentId(), userId, firstName+ " "+lastName, false, false);
        return new ResponseEntity<>(new IdentifierResponseV1(dogeResponseV1.getDocumentId()), HttpStatus.OK);
    }

}
