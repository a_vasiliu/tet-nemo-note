package com.doing.nemo.claims.controller.payload.response;

import com.doing.nemo.claims.controller.payload.response.damaged.CustomerResponse;
import com.doing.nemo.claims.controller.payload.response.damaged.DriverResponse;
import com.doing.nemo.claims.controller.payload.response.damaged.VehicleResponse;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class ContractInfoResponseAldVSAldV1 implements Serializable {

    @JsonProperty("driver")
    private DriverResponse driverResponse;

    /*@JsonProperty("contract")
    private ContractResponse contractResponse;*/

    @JsonProperty("vehicle")
    private VehicleResponse vehicleResponse;

    @JsonProperty("customer")
    private CustomerResponse customerResponse;

    @JsonProperty("insurance_company")
    private InsuranceCompanyResponseV1 insuranceCompany;

    @JsonProperty("policy_number")
    private String policyNumber;

    @JsonProperty("policy_beginning_validity")
    private String policyBeginningValidity;

    @JsonProperty("policy_end_validity")

    private String policyEndValidity;

    public DriverResponse getDriverResponse() {
        return driverResponse;
    }

    public void setDriverResponse(DriverResponse driverResponse) {
        this.driverResponse = driverResponse;
    }

    public VehicleResponse getVehicleResponse() {
        return vehicleResponse;
    }

    public void setVehicleResponse(VehicleResponse vehicleResponse) {
        this.vehicleResponse = vehicleResponse;
    }

    public CustomerResponse getCustomerResponse() {
        return customerResponse;
    }

    public void setCustomerResponse(CustomerResponse customerResponse) {
        this.customerResponse = customerResponse;
    }

    public InsuranceCompanyResponseV1 getInsuranceCompany() {
        return insuranceCompany;
    }

    public void setInsuranceCompany(InsuranceCompanyResponseV1 insuranceCompany) {
        this.insuranceCompany = insuranceCompany;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public String getPolicyBeginningValidity() {
        return policyBeginningValidity;
    }

    public void setPolicyBeginningValidity(String policyBeginningValidity) {
        this.policyBeginningValidity = policyBeginningValidity;
    }

    public String getPolicyEndValidity() {
        return policyEndValidity;
    }

    public void setPolicyEndValidity(String policyEndValidity) {
        this.policyEndValidity = policyEndValidity;
    }

    @Override
    public String toString() {
        return "ContractInfoResponseAldVSAldV1{" +
                "driverResponse=" + driverResponse +
                ", vehicleResponse=" + vehicleResponse +
                ", customerResponse=" + customerResponse +
                ", insuranceCompany=" + insuranceCompany +
                ", policyNumber='" + policyNumber + '\'' +
                ", policyBeginningValidity=" + policyBeginningValidity +
                ", policyEndValidity=" + policyEndValidity +
                '}';
    }
}
