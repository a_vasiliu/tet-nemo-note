package com.doing.nemo.claims.controller.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Map;

public class ClaimsPublicRequestV1 implements Serializable {

    @JsonProperty("metadata")
    private Map<String,String> metadata;

    public Map<String, String> getMetadata() {
        return metadata;
    }

    public void setMetadata(Map<String, String> metadata) {
        this.metadata = metadata;
    }

    @Override
    public String toString() {
        return "ClaimsPublicRequestV1{" +
                "metadata=" + metadata +
                '}';
    }
}
