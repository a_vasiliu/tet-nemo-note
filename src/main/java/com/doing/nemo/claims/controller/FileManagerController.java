package com.doing.nemo.claims.controller;

import com.doing.nemo.claims.adapter.UploadFileAdapter;
import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.UploadFileClaimsMigrationRequestV1;
import com.doing.nemo.claims.controller.payload.UploadFileClaimsRequestV1;
import com.doing.nemo.claims.controller.payload.request.EnjoyUploadFileRequestV1;
import com.doing.nemo.claims.controller.payload.response.DownloadFileBase64ResponseV1;
import com.doing.nemo.claims.controller.payload.response.InsurancePolicyResponseV1;
import com.doing.nemo.claims.controller.payload.response.UploadFileResponseV1;
import com.doing.nemo.claims.controller.payload.response.*;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.ClaimsNewEntity;
import com.doing.nemo.claims.entity.CounterpartyNewEntity;
import com.doing.nemo.claims.entity.claims.ClaimsFromCompanyEntity;
import com.doing.nemo.claims.entity.enumerated.EventTypeEnum;
import com.doing.nemo.claims.entity.jsonb.Historical;
import com.doing.nemo.claims.entity.jsonb.forms.jsonbForms.Attachment;
import com.doing.nemo.claims.exception.MigrationException;
import com.doing.nemo.claims.repository.ClaimsNewRepository;
import com.doing.nemo.claims.repository.ClaimsRepository;
import com.doing.nemo.claims.repository.CounterpartyNewRepository;
import com.doing.nemo.claims.repository.CounterpartyRepository;
import com.doing.nemo.claims.service.*;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.exception.InternalException;
import com.doing.nemo.commons.exception.NotFoundException;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.commons.io.IOUtils;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.*;
import org.springframework.util.InvalidMimeTypeException;
import org.springframework.web.bind.annotation.*;

import java.io.*;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.rmi.ConnectException;
import java.text.ParseException;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@RestController
@CrossOrigin
@ControllerAdvice
@RequestMapping(value = "/api/v1")
public class FileManagerController {

    private static Logger logger = LoggerFactory.getLogger(FileManagerController.class);
    @Autowired
    private FileManagerService fileManagerService;
    @Autowired
    private LockService lockService;
    @Autowired
    private AuthorityAttachmentService authorityAttachmentService;

    /* ROTTE ALLEGATI PER OPERATORE CLAIMS */
    @Autowired
    private ClaimsRepository claimsRepository;
    @Autowired
    private ClaimsNewRepository claimsNewRepository;

    @Autowired
    private CounterpartyRepository counterpartyRepository;
    @Autowired
    private CounterpartyNewRepository counterpartyNewRepository;

    @Autowired
    private ConverterClaimsService converterClaimsService;

    @Autowired ExternalCommunicationService externalCommunicationService;

    //REFACTOR
    @PostMapping("/claims/attachment/{UUID_CLAIMS}")
    public ResponseEntity<List<UploadFileResponseV1>> insertDocument(
            @RequestHeader(name = "nemo-user-id") String user,
            @RequestHeader(name = "nemo-user-first-name") String userFirstName,
            @RequestHeader(name = "nemo-user-last-name") String userLastName,
            @RequestBody List<UploadFileClaimsRequestV1> uploadFileRequest,
            @PathVariable(value = "UUID_CLAIMS") String idClaims,
            @RequestParam(name = "is_validated", required = false) Boolean isValidate
    ) {
        List<UploadFileResponseV1> response;
        try {
            response = fileManagerService.uploadFile(uploadFileRequest, idClaims, null, user, userFirstName + " " + userLastName, isValidate);
            externalCommunicationService.uploadFile(UploadFileAdapter.adptFromListUploadFileClaimsRequestV1ToListUploadFile(uploadFileRequest),idClaims);
        } catch (IOException e) {
            logger.info("Error Upload File: " + e.getMessage());
            throw new InternalException(MessageCode.CLAIMS_1003, e);
        }
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    //REFACTOR
    @PostMapping("/claims/enjoy/attachment/{PRACTICE_ID}")
    public ResponseEntity<EnjoyUploadFileResponseV1> insertDocumentEnjoy(
            @RequestHeader(name = "nemo-user-id") String user,
            @RequestHeader(name = "nemo-user-first-name") String userFirstName,
            @RequestHeader(name = "nemo-user-last-name") String userLastName,
            @RequestBody EnjoyUploadFileRequestV1 uploadFileRequest,
            @PathVariable(value = "PRACTICE_ID") String idPractice,
            @RequestParam(name = "is_validated", required = false) Boolean isValidate
    ) {
        //lockService.checkLock(user, idPractice);

        EnjoyUploadFileResponseV1 response = null;
        if(uploadFileRequest!=null && uploadFileRequest.getData()!=null) {


            try {
                response = new EnjoyUploadFileResponseV1();
                response.setData(fileManagerService.uploadFileByIdPractice(uploadFileRequest.getData(), idPractice, null, user, userFirstName + " " + userLastName, isValidate));
            } catch (IOException e) {
                logger.info("Error Upload File: " + e.getMessage());
                throw new InternalException(MessageCode.CLAIMS_1003, e);
            }



        }
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    //REFACTOR
    @PostMapping("/claims/external/attachment/{UUID_CLAIMS}")
    public ResponseEntity<List<UploadFileResponseV1>> insertDocumentExternal(@RequestBody List<UploadFileClaimsRequestV1> uploadFileRequest, @PathVariable(value = "UUID_CLAIMS") String idClaims,
                                                                             @RequestHeader(name = "nemo-user-id") String user,
                                                                             @RequestHeader(name = "nemo-user-first-name") String userFirstName,
                                                                             @RequestHeader(name = "nemo-user-last-name") String userLastName) {

        List<UploadFileResponseV1> response;
        try {
            response = fileManagerService.uploadFile(uploadFileRequest, idClaims, null, user, userFirstName + " " + userLastName, null);
            externalCommunicationService.uploadFile(UploadFileAdapter.adptFromListUploadFileClaimsRequestV1ToListUploadFile(uploadFileRequest),idClaims);
        } catch (IOException e) {
            logger.info("Error Upload File: " + e.getMessage());
            throw new InternalException("Error Upload File: " + e.getMessage(), e);
        }
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    //REFACTOR
    @PostMapping("/claims/counterparty/attachment/{UUID_CLAIMS}/{UUID_COUNTERPARTY}")
    public ResponseEntity<List<UploadFileResponseV1>> insertCounterpartyAttachment(
            @RequestHeader(name = "nemo-user-id") String user,
            @RequestHeader(name = "nemo-user-first-name") String userFirstName,
            @RequestHeader(name = "nemo-user-last-name") String userLastName,
            @RequestBody List<UploadFileClaimsRequestV1> uploadFileRequest,
            @PathVariable(value = "UUID_CLAIMS") String idClaims,
            @PathVariable(value = "UUID_COUNTERPARTY") String idCounterparty
    ) {

        lockService.checkLock(user, idCounterparty);

        List<UploadFileResponseV1> response;
        try {
            response = fileManagerService.uploadFile(uploadFileRequest, idClaims, idCounterparty, user, userFirstName + " " + userLastName, null);
            externalCommunicationService.uploadFile(UploadFileAdapter.adptFromListUploadFileClaimsRequestV1ToListUploadFile(uploadFileRequest),idClaims);
        } catch (IOException e) {
            logger.info("Error Upload File: " + e.getMessage());
            throw new InternalException(MessageCode.CLAIMS_1003, e);
        }
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    //REFACTOR
    @DeleteMapping("/claims/attachment/{UUID_CLAIMS}/{UUID_ATTACHMENT}")
    public ResponseEntity deleteClaimsAttachment(
            @RequestHeader(name = "nemo-user-id") String user,
            @PathVariable(value = "UUID_ATTACHMENT") String idAttachment,
            @PathVariable(value = "UUID_CLAIMS") String idClaims
    ) {

        lockService.checkLock(user, idClaims);

        try {
            fileManagerService.deleteFile(idAttachment, idClaims);
        } catch (ConnectException e) {
            logger.info("Error Upload File: " + e.getMessage());
            throw new InternalException(MessageCode.CLAIMS_1003, e);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    //REFACTOR
    @DeleteMapping("/claims/counterparty/attachment/{UUID_CLAIMS}/{UUID_COUNTERPARTY}/{UUID_ATTACHMENT}")
    public ResponseEntity deleteCounterpartyAttachment(
            @RequestHeader(name = "nemo-user-id") String user,
            @PathVariable(value = "UUID_ATTACHMENT") String idAttachment,
            @PathVariable(value = "UUID_CLAIMS") String idClaims,
            @PathVariable(value = "UUID_COUNTERPARTY") String idCounterparty
    ) {
        lockService.checkLock(user, idCounterparty);


        fileManagerService.deleteFile(idAttachment, idClaims, idCounterparty);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    /* FINE ROTTE ATTACHMENT PER OPERATORE CLAIMS*/

    /* GESTIONE ATTACHMENT SENZA LOCK*/

    //REFACTOR
    @PostMapping("/claims/attachment/unlock/{UUID_CLAIMS}")
    public ResponseEntity<List<UploadFileResponseV1>> insertDocumentWithoutLock(
            @RequestHeader(name = "nemo-user-id") String user,
            @RequestHeader(name = "nemo-user-first-name") String userFirstName,
            @RequestHeader(name = "nemo-user-last-name") String userLastName,
            @RequestBody List<UploadFileClaimsRequestV1> uploadFileRequest,
            @PathVariable(value = "UUID_CLAIMS") String idClaims
    ) {

        List<UploadFileResponseV1> response;
        try {
            response = fileManagerService.uploadFile(uploadFileRequest, idClaims, null, user, userFirstName + " " + userLastName, null);
            externalCommunicationService.uploadFile(UploadFileAdapter.adptFromListUploadFileClaimsRequestV1ToListUploadFile(uploadFileRequest),idClaims);
        } catch (IOException e) {
            logger.info("Error Upload File: " + e.getMessage());
            throw new InternalException(MessageCode.CLAIMS_1003, e);
        }
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    //REFACTOR
    @DeleteMapping("/claims/attachment/unlock/{UUID_CLAIMS}/{UUID_ATTACHMENT}")
    public ResponseEntity deleteClaimsAttachmentWithoutLock(
            @RequestHeader(name = "nemo-user-id") String user,
            @PathVariable(value = "UUID_ATTACHMENT") String idAttachment,
            @PathVariable(value = "UUID_CLAIMS") String idClaims
    ) {

        try {
            fileManagerService.deleteFile(idAttachment, idClaims);
        } catch (ConnectException e) {
            logger.info("Error Upload File: " + e.getMessage());
            throw new InternalException(MessageCode.CLAIMS_1003, e);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /* FINE GESTIONE ATTACHMENT SENZA LOCK */

    //REFACTOR NON NECESSARIO
    @GetMapping("/claims/attachment/{UUID_FILEMANAGER}")
    public ResponseEntity<DownloadFileBase64ResponseV1> downloadFile(@PathVariable(value = "UUID_FILEMANAGER") String idFileManager) {

        DownloadFileBase64ResponseV1 response;
        try {
            response = fileManagerService.downloadFile(idFileManager);
        } catch (IOException e) {
            logger.info("Error Download File: " + e.getMessage());
            throw new InternalException(MessageCode.CLAIMS_1003, e);
        }
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    //REFACTOR
    @GetMapping("/claims/attachment/all/{UUID_CLAIMS}")
    public ResponseEntity<List<DownloadFileBase64ResponseV1>> downloadAllFileAttachment(@PathVariable(value = "UUID_CLAIMS") String idClaims) {

        List<DownloadFileBase64ResponseV1> response;
        try {
            response = fileManagerService.downloadAllFileAttachment(idClaims);
        } catch (IOException e) {
            logger.info("Error Download File: " + e.getMessage());
            throw new InternalException(MessageCode.CLAIMS_1003, e);
        }
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    //GESTORE ESTERNO


    //scarica direttamente il file con l'evento dal browser
    //REFACTOR NON NECESSARIO
    @GetMapping("/claims/external/file/{UUID_FILEMANAGER}")
    public ResponseEntity<Resource> getObjectFile(@PathVariable(value = "UUID_FILEMANAGER") String idFileManager) throws IOException {

        //scarico il file in base 64
        DownloadFileBase64ResponseV1 fileManager = fileManagerService.downloadFile(idFileManager);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileManager.getFileName());
        headers.add(HttpHeaders.LAST_MODIFIED, Calendar.getInstance().getTime().toString());
        headers.add(HttpHeaders.CACHE_CONTROL, CacheControl.noStore().getHeaderValue());

        ByteArrayResource resource = new ByteArrayResource(Base64.getDecoder().decode(fileManager.getFileContent()));

        MediaType mediaType;
        try {
            mediaType = MediaType.parseMediaType(fileManager.getMimeType());
        } catch (InvalidMimeTypeException imte) {
            mediaType = MediaType.APPLICATION_OCTET_STREAM;
        }

        return ResponseEntity.ok().headers(headers).contentLength(fileManager.getFileSize()).contentType(mediaType).body(resource);
    }

    //REFACTOR NON NECESSARIO
    @GetMapping("/claims/external/attachment/{UUID_FILEMANAGER}")
    public ResponseEntity<DownloadFileBase64ResponseV1> downloadFileExternal(@PathVariable(value = "UUID_FILEMANAGER") String idFileManager) {

        DownloadFileBase64ResponseV1 response;
        try {
            response = fileManagerService.downloadFile(idFileManager);
        } catch (IOException e) {
            logger.info("Error Download File: " + e.getMessage());
            throw new InternalException("Error Download File: " + e.getMessage(), e);
        }
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    //REFACTOR
    @GetMapping("/claims/external/zip/{CLAIMS_UUID}")
    public ResponseEntity<InputStreamResource> downloadZipFileExternal(@PathVariable(value = "CLAIMS_UUID") String idClaims) throws IOException {

        //get new entity
        Optional<ClaimsNewEntity> claimsEntityOptional = claimsNewRepository.findById(idClaims);
        if (!claimsEntityOptional.isPresent()) {
            logger.info(MessageCode.CLAIMS_1010.value());
            throw new BadRequestException(MessageCode.CLAIMS_1010);
        }
        ClaimsNewEntity claimsNewEntity = claimsEntityOptional.get();
        //convert from new entity to old entity
        if(claimsNewEntity.getClaimsFromCompanyEntity() == null) {
            claimsNewEntity.setClaimsFromCompanyEntity(new ClaimsFromCompanyEntity());
        }
        claimsNewEntity.getClaimsFromCompanyEntity().setLastUpdate(DateUtil.getNowInstant());
        ClaimsEntity claimsEntity = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntity);
        claimsNewRepository.save(claimsNewEntity);
        String filename = UUID.randomUUID().toString();
        File file = new File(filename);
        try (
                FileOutputStream fos = new FileOutputStream(file);
                ZipOutputStream zipOutputStream = new ZipOutputStream(fos, FilemanagerConstants.DEFAULT_CHARSET);
        ) {
            claimsEntity = authorityAttachmentService.addAuthorityAttachment(claimsEntity);
            List<Attachment> attachments = claimsEntity.getForms().getAttachment();

            for (Attachment currentAttachment : attachments) {
                ResponseEntity<Resource> entity = getObjectFile(currentAttachment.getFileManagerId());
                Resource resource = entity.getBody();

                String entryFilename = URLDecoder.decode(entity.getHeaders().get(HttpHeaders.CONTENT_DISPOSITION).get(0)
                        .replaceFirst("(?i)^.*filename ?= ?\"?([^\"]+)\"?.*$", "$1").replaceAll("/", "-"), FilemanagerConstants.DEFAULT_CHARSET.displayName());

                zipOutputStream.putNextEntry(new ZipEntry(currentAttachment.getFileManagerId()+"_"+entryFilename));
                InputStream inputStream = resource.getInputStream();
                IOUtils.copy(inputStream, zipOutputStream);

                inputStream.close();
                zipOutputStream.closeEntry();
            }


            InputStreamResource response = new InputStreamResource(new FileInputStream(file));
            //String outputFilename = "Allegati" + formatter.format(LocalDateTime.now()) + ".zip";
            String outputFilename = "Allegati_" + claimsEntity.getPracticeId() + ".zip";

            return ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_TYPE, "application/zip")
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + outputFilename + "\"")
                    .body(response);

        } finally {
            Boolean deleted = file.delete();
            logger.info("File deleted: "+deleted);
        }
    }

    //REFACTOR
    @GetMapping("/claims/download/external/zip/{CLAIMS_UUID}")
    public ResponseEntity<InputStreamResource> downloadZipFileExternalWithLog(@PathVariable(value = "CLAIMS_UUID") String idClaims,
                                                                              @RequestHeader(name = "nemo-entruster-id") String idExternalUser,
                                                                              @RequestHeader(name = "nemo-entruster-first-name") String externalUsername,
                                                                              @RequestParam(value= "file_managers") List<String> fileManagers) throws IOException {

        Optional<ClaimsNewEntity> claimsEntityOptional = claimsNewRepository.findById(idClaims);
        if (!claimsEntityOptional.isPresent()) {
            logger.error("Claim with UUID: " + idClaims + " not found");
            throw new NotFoundException("Claim with UUID: " + idClaims + " not found", MessageCode.CLAIMS_1010);
        }
        ClaimsNewEntity claimsEntity = claimsEntityOptional.get();
        String filename = UUID.randomUUID().toString();
        File file = new File(filename);
        try (
                FileOutputStream fos = new FileOutputStream(file);
                ZipOutputStream zipOutputStream = new ZipOutputStream(fos, FilemanagerConstants.DEFAULT_CHARSET);
        ) {

            for (String fileManager : fileManagers) {
                ResponseEntity<Resource> entity = getObjectFile(fileManager);
                Resource resource = entity.getBody();

                String entryFilename = URLDecoder.decode(entity.getHeaders().get(HttpHeaders.CONTENT_DISPOSITION).get(0)
                        .replaceFirst("(?i)^.*filename ?= ?\"?([^\"]+)\"?.*$", "$1").replaceAll("/", "-"), FilemanagerConstants.DEFAULT_CHARSET.displayName());

                zipOutputStream.putNextEntry(new ZipEntry(fileManager+"_"+entryFilename));
                InputStream inputStream = resource.getInputStream();
                IOUtils.copy(inputStream, zipOutputStream);

                inputStream.close();
                zipOutputStream.closeEntry();
            }

            InputStreamResource response = new InputStreamResource(new FileInputStream(file));
            //String outputFilename = "Allegati" + formatter.format(LocalDateTime.now()) + ".zip";
            String outputFilename = "Allegati_" + claimsEntity.getPracticeId() + ".zip";

            Historical historical = new Historical();
            historical.setEventType(EventTypeEnum.ENTRUSTED_DOWNLOAD);
            historical.setOldFlow(claimsEntity.getType().toValue());
            historical.setNewFlow(claimsEntity.getType().toValue());
            historical.setOldType(claimsEntity.getTypeAccident());
            historical.setNewType(claimsEntity.getTypeAccident());
            historical.setStatusEntityOld(claimsEntity.getStatus());
            historical.setStatusEntityNew(claimsEntity.getStatus());
            historical.setUpdateAt(DateUtil.getNowDate());
            historical.setUserId(idExternalUser);
            historical.setUserName(externalUsername);

            if(claimsEntity.getClaimsEntrustedEntity() != null &&
                    claimsEntity.getClaimsEntrustedEntity().getEntrustedType() != null &&
                    claimsEntity.getClaimsEntrustedEntity().getIdEntrustedTo() != null ) {

                historical.setComunicationDescription(claimsEntity.getClaimsEntrustedEntity().getEntrustedType().toValue() + " "+ claimsEntity.getClaimsEntrustedEntity().getIdEntrustedTo());
            }

            if(claimsEntity.getHistorical() == null || claimsEntity.getHistorical().isEmpty()){
                claimsEntity.setHistorical(new LinkedList<>());
            }

            List<Historical> historicalList = claimsEntity.getHistorical();
            historicalList.add(historical);
            claimsEntity.setHistorical(historicalList);
            claimsNewRepository.save(claimsEntity);

            return ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_TYPE, "application/zip")
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + outputFilename + "\"")
                    .body(response);


        } finally {
            boolean deleted = file.delete();
            logger.info("File deleted: "+deleted);
        }
    }

    //REFACTOR
    @GetMapping("/claims/zip/{CLAIMS_UUID}")
    public ResponseEntity<InputStreamResource> downloadZipFile(@PathVariable(value = "CLAIMS_UUID") String idClaims) throws IOException {

        Optional<ClaimsNewEntity> claimsEntityOptional = claimsNewRepository.findById(idClaims);
        if (!claimsEntityOptional.isPresent()) {
            logger.error("Claim with UUID: " + idClaims + " not found");
            throw new NotFoundException("Claim with UUID: " + idClaims + " not found", MessageCode.CLAIMS_1010);
        }
        ClaimsNewEntity claimsEntity = claimsEntityOptional.get();
        String filename = UUID.randomUUID().toString();
        File file = new File(filename);
        try (
                FileOutputStream fos = new FileOutputStream(file);
                ZipOutputStream zipOutputStream = new ZipOutputStream(fos, FilemanagerConstants.DEFAULT_CHARSET);
        ) {


            for (Attachment currentAttachment : claimsEntity.getForms().getAttachment()) {
                ResponseEntity<Resource> entity = getObjectFile(currentAttachment.getFileManagerId());
                Resource resource = entity.getBody();

                String entryFilename = URLDecoder.decode(entity.getHeaders().get(HttpHeaders.CONTENT_DISPOSITION).get(0)
                        .replaceFirst("(?i)^.*filename ?= ?\"?([^\"]+)\"?.*$", "$1").replaceAll("/", "-"), FilemanagerConstants.DEFAULT_CHARSET.displayName());

                zipOutputStream.putNextEntry(new ZipEntry(currentAttachment.getFileManagerId()+"_"+entryFilename));
                InputStream inputStream = resource.getInputStream();
                IOUtils.copy(inputStream, zipOutputStream);

                inputStream.close();
                zipOutputStream.closeEntry();
            }


            InputStreamResource response = new InputStreamResource(new FileInputStream(file));
            //String outputFilename = "Allegati" + formatter.format(LocalDateTime.now()) + ".zip";
            String outputFilename = "Allegati_" + claimsEntity.getPracticeId() + ".zip";

            return ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_TYPE, "application/zip")
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + outputFilename + "\"")
                    .body(response);

        } finally {
            boolean deleted = file.delete();
            logger.info("File deleted: "+deleted);
        }
    }

    //REFACTOR NON NECESSARIO
    // /*DELETE INSURANCE POLICY ATTACHMENT*/
    @DeleteMapping("/policy/{UUID}/attachment")
    @ApiOperation(value = "Delete Insurance Policy Attachment", notes = "Delete Insurance Policy Attachment using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = InsurancePolicyResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<Attachment>> deleteInsurancePolicy(
            @ApiParam(value = "UUID that identifies the Insurance Policy", required = true)
            @PathVariable(name = "UUID") UUID uuid,
            @ApiParam(value = "UUID that identifies the Insurance Policy Attachment to be deleted", required = true)
            @RequestParam(name = "ids") List<String> uuidAttacmentList) {

        List<Attachment> response;

        try {

            response = fileManagerService.deleteFileInsurancePolicy(uuidAttacmentList, uuid);

        } catch (ConnectException e) {
            logger.info("Error Delete File: " + e.getMessage());
            throw new InternalException(MessageCode.CLAIMS_1003, e);
        }

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    //REFACTOR
    // Eliminazione lista di attachemnte in un claims, senza controllo lock
    @DeleteMapping("/claims/attachment/unlock/{UUID_CLAIMS}")
    public ResponseEntity deleteClaimsAttachmentsListWithoutLock(
            @RequestHeader(name = "nemo-user-id") String user,
            @RequestParam(name = "attachments_id") List<String> attachmentsId,
            @PathVariable(value = "UUID_CLAIMS") String idClaims
    ) {

        try {
            fileManagerService.deleteFileList(attachmentsId, idClaims);
        } catch (ConnectException e) {
            logger.info("Error Delete File: " + e.getMessage());
            throw new InternalException(MessageCode.CLAIMS_1003, e);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    //REFACTOR
    // Eliminazione lista di attachemnte in un claims, con controllo lock
    @DeleteMapping("/claims/attachment/{UUID_CLAIMS}")
    public ResponseEntity deleteMoreClaimsAttachmentsList(
            @RequestHeader(name = "nemo-user-id") String user,
            @RequestParam(name = "attachments_id") List<String> attachmentsId,
            @PathVariable(value = "UUID_CLAIMS") String idClaims
    ) {

        lockService.checkLock(user, idClaims);

        try {
            fileManagerService.deleteFileList(attachmentsId, idClaims);
        } catch (ConnectException e) {
            logger.info("Error Delete File: " + e.getMessage());
            throw new InternalException(MessageCode.CLAIMS_1003, e);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    //REFACTOR
    // Eliminazione lista di attachemnte in un repair, senza controllo lock
    @DeleteMapping("/claims/counterparty/attachment/unlock/{UUID_CLAIMS}/{UUID_COUNTERPARTY}")
    public ResponseEntity deleteRepairAttachmentsListWithoutLock(
            @RequestHeader(name = "nemo-user-id") String user,
            @RequestParam(name = "attachments_id") List<String> attachmentsId,
            @PathVariable(value = "UUID_CLAIMS") String idClaims,
            @PathVariable(value = "UUID_COUNTERPARTY") String idCounterparty
    ) {

        try {
            fileManagerService.deleteFileList(attachmentsId, idClaims, idCounterparty);
        } catch (ConnectException e) {
            logger.info("Error Delete File: " + e.getMessage());
            throw new InternalException(MessageCode.CLAIMS_1003, e);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    //REFACTOR
    // Eliminazione lista di attachemnte in un repair, con controllo lock
    @DeleteMapping("/claims/counterparty/attachment/{UUID_CLAIMS}/{UUID_COUNTERPARTY}")
    public ResponseEntity deleteRepairAttachmentsListWithLock(
            @RequestHeader(name = "nemo-user-id") String user,
            @RequestParam(name = "attachments_id") List<String> attachmentsId,
            @PathVariable(value = "UUID_CLAIMS") String idClaims,
            @PathVariable(value = "UUID_COUNTERPARTY") String idCounterparty
    ) {

        lockService.checkLock(user, idCounterparty);

        try {
            fileManagerService.deleteFileList(attachmentsId, idClaims, idCounterparty);
        } catch (ConnectException e) {
            logger.info("Error Delete File: " + e.getMessage());
            throw new InternalException(MessageCode.CLAIMS_1003, e);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    //REFACTOR NON NECESSARIO
    // Eliminazione lista di attachemnte in una pratica, senza controllo lock
    @DeleteMapping("/practice/attachment/unlock/{UUID}")
    public ResponseEntity deletePracticeAttachmentsListWithoutLock(
            @RequestHeader(name = "nemo-user-id") String user,
            @RequestParam(name = "attachments_id") List<String> attachmentsId,
            @PathVariable(value = "UUID") String idPractice
    ) {

        try {
            fileManagerService.deleteFileListForPractice(attachmentsId, idPractice);
        } catch (ConnectException e) {
            logger.info("Error Delete File: " + e.getMessage());
            throw new InternalException(MessageCode.CLAIMS_1003, e);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    //REFACTOR NON NECESSARIO
    // Eliminazione lista di attachment in una pratica, con controllo lock
    @DeleteMapping("/practice/attachment/{UUID}")
    public ResponseEntity deleteMorePracticeAttachmentsList(
            @RequestHeader(name = "nemo-user-id") String user,
            @RequestParam(name = "attachments_id") List<String> attachmentsId,
            @PathVariable(value = "UUID") String idPractice
    ) {

        lockService.checkLock(user, idPractice);

        try {
            fileManagerService.deleteFileListForPractice(attachmentsId, idPractice);
        } catch (ConnectException e) {
            logger.info("Error Delete File: " + e.getMessage());
            throw new InternalException(MessageCode.CLAIMS_1003, e);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    //REFACTOR NON NECESSARIO
    @PostMapping("/practice/attachment/{UUID}")
    public ResponseEntity<List<UploadFileResponseV1>> insertDocumentForPractice(
            @RequestHeader(name = "nemo-user-id") String user,
            @RequestHeader(name = "nemo-user-first-name") String userFirstName,
            @RequestHeader(name = "nemo-user-last-name") String userLastName,
            @RequestBody List<UploadFileClaimsRequestV1> uploadFileRequest,
            @PathVariable(value = "UUID") String idPractice
    ) {
        //lockService.checkLock(user, idPractice);

        List<UploadFileResponseV1> response;
        try {
            response = fileManagerService.uploadFilePractice(uploadFileRequest, idPractice, user, userFirstName + " " + userLastName);
            ClaimsNewEntity claimsNewEntity = claimsNewRepository.getClaimsByIdPratica(Long.valueOf(idPractice));
            externalCommunicationService.uploadFile(UploadFileAdapter.adptFromListUploadFileClaimsRequestV1ToListUploadFile(uploadFileRequest),claimsNewEntity.getId());
        } catch (IOException e) {
            logger.info("Error Upload File: " + e.getMessage());
            throw new InternalException(MessageCode.CLAIMS_1003, e);
        }
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    public static class FilemanagerConstants {

        public final static String APIKEY = "apikey";
        public final static Charset DEFAULT_CHARSET = StandardCharsets.UTF_8;
        public final static String METADATA_RESOURCE_TYPE_KEY = "resource_type";
        public final static String METADATA_RESOURCE_ID_KEY = "resource_id";
        public final static String METADATA_FILE_NAME_KEY = "file_name";
        public final static String METADATA_FILE_SIZE_KEY = "file_size";
        public final static String METADATA_MIME_TYPE_KEY = "mime_type";
        public final static String METADATA_DESCRIPTION_KEY = "description";
        public final static String METADATA_BLOB_TYPE_KEY = "blob_type";

    }

    //REFACTOR
    //Attachment Gestore esterno repair
    @PostMapping("/repair/attachment/{uuid_counterparty}")
    public ResponseEntity<List<UploadFileResponseV1>> insertCounterpartyAttachment(
            @RequestHeader(name = "nemo-user-id") String user,
            @RequestHeader(name = "nemo-user-first-name") String userFirstName,
            @RequestHeader(name = "nemo-user-last-name") String userLastName,
            @RequestBody List<UploadFileClaimsRequestV1> uploadFileRequest,
            @PathVariable(value = "uuid_counterparty") String idCounterparty
    ) {
        List<UploadFileResponseV1> response;
        try {
            response = fileManagerService.uploadFile(uploadFileRequest, null, idCounterparty, user, userFirstName + " " + userLastName, null);
        } catch (IOException e) {
            logger.info("Error Upload File: " + e.getMessage());
            throw new InternalException(MessageCode.CLAIMS_1003, e);
        }
            return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    //REFACTOR NON NECESSARIO
    //scarica direttamente il file con l'evento dal browser
    @GetMapping("/repair/external/file/{UUID_FILEMANAGER}")
    public ResponseEntity<Resource> downloadOjcetRepairResource(@PathVariable(value = "UUID_FILEMANAGER") String idFileManager) throws IOException {

        //scarico il file in base 64
        DownloadFileBase64ResponseV1 fileManager = fileManagerService.downloadFile(idFileManager);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileManager.getFileName());
        headers.add(HttpHeaders.LAST_MODIFIED, Calendar.getInstance().getTime().toString());
        headers.add(HttpHeaders.CACHE_CONTROL, CacheControl.noStore().getHeaderValue());

        ByteArrayResource resource = new ByteArrayResource(Base64.getDecoder().decode(fileManager.getFileContent()));

        MediaType mediaType;
        try {
            mediaType = MediaType.parseMediaType(fileManager.getMimeType());
        } catch (InvalidMimeTypeException imte) {
            mediaType = MediaType.APPLICATION_OCTET_STREAM;
        }

        return ResponseEntity.ok().headers(headers).contentLength(fileManager.getFileSize()).contentType(mediaType).body(resource);
    }

    //REFACTOR NON NECESSARIO
    @GetMapping("/repair/external/attachment/{UUID_FILEMANAGER}")
    public ResponseEntity<DownloadFileBase64ResponseV1> downloadRepairAttachmentExternal(
            @PathVariable(value = "UUID_FILEMANAGER") String idFileManager
    ) {

        DownloadFileBase64ResponseV1 response;
        try {
            response = fileManagerService.downloadFile(idFileManager);
        } catch (IOException e) {
            logger.info("Error Download File: " + e.getMessage());
            throw new InternalException("Error Download File: " + e.getMessage(), e);
        }
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    //REFACTOR
    @GetMapping("/repair/external/zip/{uuid_counterparty}")
    public ResponseEntity<InputStreamResource> downloadZipRepairFileExternal(
            @PathVariable(value = "uuid_counterparty") String idCounterparty
    ) throws IOException {

        Optional<CounterpartyNewEntity> counterpartyEntityOptional = counterpartyNewRepository.findById(idCounterparty);
        if (!counterpartyEntityOptional.isPresent()) {
            //DIVERSIFICARE GLI ERRORI CON LE CONTROPARTE
            logger.info("Counterparty with id "+ idCounterparty+" not found");
            throw new BadRequestException("Counterparty with id "+ idCounterparty+" not found",MessageCode.CLAIMS_1010);
        }
        CounterpartyNewEntity counterparty = counterpartyEntityOptional.get();

        String filename = UUID.randomUUID().toString();
        File file = new File(filename);
        try (
                FileOutputStream fos = new FileOutputStream(file);
                ZipOutputStream zipOutputStream = new ZipOutputStream(fos, FilemanagerConstants.DEFAULT_CHARSET);
        ) {


            for (Attachment currentAttachment : counterparty.getAttachments()) {
                ResponseEntity<Resource> entity = getObjectFile(currentAttachment.getFileManagerId());
                Resource resource = entity.getBody();

                String entryFilename = URLDecoder.decode(entity.getHeaders().get(HttpHeaders.CONTENT_DISPOSITION).get(0)
                        .replaceFirst("(?i)^.*filename ?= ?\"?([^\"]+)\"?.*$", "$1").replaceAll("/", "-"), FilemanagerConstants.DEFAULT_CHARSET.displayName());

                zipOutputStream.putNextEntry(new ZipEntry(currentAttachment.getFileManagerId()+"_"+entryFilename));
                InputStream inputStream = resource.getInputStream();
                IOUtils.copy(inputStream, zipOutputStream);

                inputStream.close();
                zipOutputStream.closeEntry();
            }


            InputStreamResource response = new InputStreamResource(new FileInputStream(file));
            //String outputFilename = "Allegati" + formatter.format(LocalDateTime.now()) + ".zip";
            String outputFilename = "Allegati_.zip";

            return ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_TYPE, "application/zip")
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + outputFilename + "\"")
                    .body(response);

        } finally {
            boolean deleted = file.delete();
            logger.info("File deleted: "+deleted);
        }
    }

    //REFACTOR
    //vecchia rotta per caricamento attachment in migrazione
    @PostMapping("/claims/attachment/practice/{UUID_CLAIMS}")
    public ResponseEntity<List<UploadFileResponseV1>> insertDocumentByPracticeId(
            @RequestHeader(name = "nemo-user-id", required = false) String user,
            @RequestHeader(name = "nemo-user-first-name", required = false) String userFirstName,
            @RequestHeader(name = "nemo-user-last-name", required = false) String userLastName,
            @RequestBody List<UploadFileClaimsRequestV1> uploadFileRequest,
            @PathVariable(value = "UUID_CLAIMS") Long idClaims,
            @RequestParam(name = "is_validated", required = false) Boolean isValidate
    ) throws MigrationException, IOException {
        //lockService.checkLock(user, idPractice);

        List<UploadFileResponseV1> response;
        response = fileManagerService.uploadFilePractice(uploadFileRequest, idClaims, user, userFirstName + " " + userLastName, isValidate);
        externalCommunicationService.uploadFile(UploadFileAdapter.adptFromListUploadFileClaimsRequestV1ToListUploadFile(uploadFileRequest),String.valueOf(idClaims));
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @ExceptionHandler(MigrationException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public String processValidationError(MigrationException ex) {
        String result = ex.getMessage();
        System.out.println("###########"+result);
        return result;
    }

    //REFACTOR
    @PostMapping("file/claims-import/{practice_id}")
    public ResponseEntity<List<UploadFileResponseV1>> uploadDocumentByPracticeId(
            @RequestHeader(name = "nemo-user-id", required = false) String user,
            @RequestHeader(name = "nemo-user-first-name", required = false) String userFirstName,
            @RequestHeader(name = "nemo-user-last-name", required = false) String userLastName,
            @RequestBody List<UploadFileClaimsMigrationRequestV1> uploadFileRequest,
            @PathVariable(value = "practice_id") Long idPractice,
            @RequestParam(name = "is_validated", required = false) Boolean isValidate
    ) throws MigrationException, IOException, ParseException {

        List<UploadFileResponseV1> response;
       // try {


         String username = null;
         if(userFirstName != null ){
             username = userFirstName;
         }



            response = fileManagerService.uploadFilePracticeNew(uploadFileRequest, idPractice, user, username, isValidate);
            return new ResponseEntity<>(HttpStatus.OK);

       /* }catch( MigrationException e){

            logger.info(e.getMessage());
            throw  new MigrationException(e.getMessage());

        }*/
    }

}
