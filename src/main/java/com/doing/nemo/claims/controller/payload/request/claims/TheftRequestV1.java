package com.doing.nemo.claims.controller.payload.request.claims;

import com.doing.nemo.claims.controller.payload.request.EmailTemplateMessagingRequestV1;
import com.doing.nemo.claims.entity.enumerated.EventTypeEnum;
import com.doing.nemo.claims.entity.jsonb.Address;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

public class TheftRequestV1 implements Serializable {
    @JsonProperty("is_found")
    private Boolean isFound;

    @JsonProperty("operations_center_notified")
    private Boolean OperationsCenterNotified;

    @JsonProperty("theft_occurred_on_center_ald")
    private Boolean theftOccurredOnCenterAld;

    @JsonProperty("supplier_code")
    private String supplierCode;

    @JsonProperty("complaint_authority_police")
    private Boolean complaintAuthorityPolice;

    @JsonProperty("complaint_authority_cc")
    private Boolean complaintAuthorityCc;

    @JsonProperty("complaint_authority_vvuu")
    private Boolean complaintAuthorityVvuu;

    @JsonProperty("authority_data")
    private String authorityData;

    @JsonProperty("authority_telephone")
    private String authorityTelephone;

    @JsonProperty("find_date")
    private String findDate;

    @JsonProperty("hour")
    private String hour;

    @JsonProperty("found_abroad")
    private Boolean foundAbroad;

    @JsonProperty("vehicle_co")
    private String vehicleCo;

    @JsonProperty("sequestered")
    private Boolean sequestered;

    @JsonProperty("address")
    private Address address;

    @JsonProperty("find_authority_police")
    private Boolean findAuthorityPolice;

    @JsonProperty("find_authority_cc")
    private Boolean findAuthorityCc;

    @JsonProperty("find_authority_vvuu")
    private Boolean findAuthorityVvuu;

    @JsonProperty("find_authority_data")
    private String findAuthorityData;

    @JsonProperty("find_authority_telephone")
    private String findAuthorityTelephone;

    @JsonProperty("theft_notes")
    private String theftNotes;

    @JsonProperty("find_notes")
    private String findNotes;

    @JsonProperty("first_key_reception")
    private String firstKeyReception;

    @JsonProperty("second_key_reception")
    private String secondKeyReception;

    @JsonProperty("original_complaint_reception")
    private String originalComplaintReception;

    @JsonProperty("copy_complaint_reception")
    private String copyComplaintReception;

    @JsonProperty("original_report_reception")
    private String originalReportReception;

    @JsonProperty("copy_report_reception")
    private String copyReportReception;

    @JsonProperty("loss_possession_request")
    private String lossPossessionRequest;

    @JsonProperty("loss_possession_reception")
    private String lossPossessionReception;

    @JsonProperty("return_possession_request")
    private String returnPossessionRequest;

    @JsonProperty("return_possession_reception")
    private String returnPossessionReception;

    @JsonProperty("robbery")
    private Boolean robbery;

    @JsonProperty("key_management")
    private Boolean keyManagement;

    @JsonProperty("account_management")
    private Boolean accountManagement;

    @JsonProperty("administrative_position")
    private Boolean administrativePosition;

    @JsonProperty("practice_id")
    private Long practiceId;

    @JsonProperty("re_registration")
    private Boolean reRegistration;

    @JsonProperty("re_registration_request")
    private String reRegistrationRequest;

    @JsonProperty("re_registration_at")
    private String reRegistrationAt;

    @JsonProperty("re_registration_salesforce_case")
    private String reRegistrationSaleforcesCase;

    @JsonProperty("certificate_duplication")
    private Boolean certificateDuplication;

    @JsonProperty("certificate_duplication_request")
    private String certificateDuplicationRequest;

    @JsonProperty("certificate_duplication_at")
    private String certificateDuplicationAt;

    @JsonProperty("certificate_duplication_salesforce_case")
    private String certificateDuplicationSalesforceCase;

    @JsonProperty("stamp")
    private Boolean stamp;

    @JsonProperty("stamp_request")
    private String stampRequest;

    @JsonProperty("stamp_at")
    private String stampAt;

    @JsonProperty("stamp_salesforce_case")
    private String stampSalesforceCase;

    @JsonProperty("demolition")
    private Boolean demolition;

    @JsonProperty("demolition_request")
    private String demolitionRequest;

    @JsonProperty("demolition_at")
    private String demolitionAt;

    @JsonProperty("demolition_salesforce_case")
    private String demolitionSalesforceCase;

    @JsonProperty("is_with_receptions")
    private Boolean isWithReceptions;


    @JsonProperty("is_under_seizure")
    private Boolean isUnderSeizure;

    @JsonProperty("event_type")
    private EventTypeEnum eventType;

    @JsonProperty("motivation")
    private String motivation;

    @JsonProperty("template_list")
    private List<EmailTemplateMessagingRequestV1> templateList;



    public TheftRequestV1() {
    }

    public TheftRequestV1(Boolean isFound, Boolean operationsCenterNotified, Boolean theftOccurredOnCenterAld, String supplierCode,
                          Boolean complaintAuthorityPolice, Boolean complaintAuthorityCc, Boolean complaintAuthorityVvuu,
                          String authorityData, String authorityTelephone, String findDate, String hour, Boolean foundAbroad, String vehicleCo,
                          Boolean sequestered, Address address, Boolean findAuthorityPolice, Boolean findAuthorityCc, Boolean findAuthorityVvuu,
                          String findAuthorityData, String findAuthorityTelephone, String theftNotes, String findNotes, String firstKeyReception,
                          String secondKeyReception, String originalComplaintReception, String copyComplaintReception, String originalReportReception,
                          String copyReportReception, String lossPossessionRequest, String lossPossessionReception, String returnPossessionRequest,
                          String returnPossessionReception, Boolean robbery, Boolean keyManagement, Boolean accountManagement,
                          Boolean administrativePosition, Long practiceId, Boolean reRegistration, String reRegistrationRequest,
                          String reRegistrationAt, String reRegistrationSaleforcesCase, Boolean certificateDuplication, String certificateDuplicationRequest,
                          String certificateDuplicationAt, String certificateDuplicationSalesforceCase, Boolean stamp, String stampRequest,
                          String stampAt, String stampSalesforceCase, Boolean demolition, String demolitionRequest, String demolitionAt,
                          String demolitionSalesforceCase, Boolean isWithReceptions, Boolean isUnderSeizure,
                          EventTypeEnum eventType, String motivation, List<EmailTemplateMessagingRequestV1> templateList) {
        this.isFound = isFound;
        OperationsCenterNotified = operationsCenterNotified;
        this.theftOccurredOnCenterAld = theftOccurredOnCenterAld;
        this.supplierCode = supplierCode;
        this.complaintAuthorityPolice = complaintAuthorityPolice;
        this.complaintAuthorityCc = complaintAuthorityCc;
        this.complaintAuthorityVvuu = complaintAuthorityVvuu;
        this.authorityData = authorityData;
        this.authorityTelephone = authorityTelephone;
        this.findDate = findDate;
        this.hour = hour;
        this.foundAbroad = foundAbroad;
        this.vehicleCo = vehicleCo;
        this.sequestered = sequestered;
        this.address = address;
        this.findAuthorityPolice = findAuthorityPolice;
        this.findAuthorityCc = findAuthorityCc;
        this.findAuthorityVvuu = findAuthorityVvuu;
        this.findAuthorityData = findAuthorityData;
        this.findAuthorityTelephone = findAuthorityTelephone;
        this.theftNotes = theftNotes;
        this.findNotes = findNotes;
        this.firstKeyReception = firstKeyReception;
        this.secondKeyReception = secondKeyReception;
        this.originalComplaintReception = originalComplaintReception;
        this.copyComplaintReception = copyComplaintReception;
        this.originalReportReception = originalReportReception;
        this.copyReportReception = copyReportReception;
        this.lossPossessionRequest = lossPossessionRequest;
        this.lossPossessionReception = lossPossessionReception;
        this.returnPossessionRequest = returnPossessionRequest;
        this.returnPossessionReception = returnPossessionReception;
        this.robbery = robbery;
        this.keyManagement = keyManagement;
        this.accountManagement = accountManagement;
        this.administrativePosition = administrativePosition;
        this.practiceId = practiceId;
        this.reRegistration = reRegistration;
        this.reRegistrationRequest = reRegistrationRequest;
        this.reRegistrationAt = reRegistrationAt;
        this.reRegistrationSaleforcesCase = reRegistrationSaleforcesCase;
        this.certificateDuplication = certificateDuplication;
        this.certificateDuplicationRequest = certificateDuplicationRequest;
        this.certificateDuplicationAt = certificateDuplicationAt;
        this.certificateDuplicationSalesforceCase = certificateDuplicationSalesforceCase;
        this.stamp = stamp;
        this.stampRequest = stampRequest;
        this.stampAt = stampAt;
        this.stampSalesforceCase = stampSalesforceCase;
        this.demolition = demolition;
        this.demolitionRequest = demolitionRequest;
        this.demolitionAt = demolitionAt;
        this.demolitionSalesforceCase = demolitionSalesforceCase;
        this.isWithReceptions = isWithReceptions;
        this.isUnderSeizure = isUnderSeizure;
        this.eventType = eventType;
        this.motivation = motivation;
        this.templateList = templateList;
    }

    public Boolean getUnderSeizure() {
        return isUnderSeizure;
    }

    public void setUnderSeizure(Boolean underSeizure) {
        isUnderSeizure = underSeizure;
    }

    public Boolean getFound() {
        return isFound;
    }

    public void setFound(Boolean found) {
        isFound = found;
    }

    public Boolean getOperationsCenterNotified() {
        return OperationsCenterNotified;
    }

    public void setOperationsCenterNotified(Boolean operationsCenterNotified) {
        OperationsCenterNotified = operationsCenterNotified;
    }

    public Boolean getTheftOccurredOnCenterAld() {
        return theftOccurredOnCenterAld;
    }

    public void setTheftOccurredOnCenterAld(Boolean theftOccurredOnCenterAld) {
        this.theftOccurredOnCenterAld = theftOccurredOnCenterAld;
    }

    public String getSupplierCode() {
        return supplierCode;
    }

    public void setSupplierCode(String supplierCode) {
        this.supplierCode = supplierCode;
    }

    public Boolean getComplaintAuthorityPolice() {
        return complaintAuthorityPolice;
    }

    public void setComplaintAuthorityPolice(Boolean complaintAuthorityPolice) {
        this.complaintAuthorityPolice = complaintAuthorityPolice;
    }

    public Boolean getComplaintAuthorityCc() {
        return complaintAuthorityCc;
    }

    public void setComplaintAuthorityCc(Boolean complaintAuthorityCc) {
        this.complaintAuthorityCc = complaintAuthorityCc;
    }

    public Boolean getComplaintAuthorityVvuu() {
        return complaintAuthorityVvuu;
    }

    public void setComplaintAuthorityVvuu(Boolean complaintAuthorityVvuu) {
        this.complaintAuthorityVvuu = complaintAuthorityVvuu;
    }

    public String getAuthorityData() {
        return authorityData;
    }

    public void setAuthorityData(String authorityData) {
        this.authorityData = authorityData;
    }

    public String getAuthorityTelephone() {
        return authorityTelephone;
    }

    public void setAuthorityTelephone(String authorityTelephone) {
        this.authorityTelephone = authorityTelephone;
    }

    public String getFindDate() {
        return findDate;
    }

    public void setFindDate(String findDate) {
        this.findDate = findDate;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public Boolean getFoundAbroad() {
        return foundAbroad;
    }

    public void setFoundAbroad(Boolean foundAbroad) {
        this.foundAbroad = foundAbroad;
    }

    public String getVehicleCo() {
        return vehicleCo;
    }

    public void setVehicleCo(String vehicleCo) {
        this.vehicleCo = vehicleCo;
    }

    public Boolean getSequestered() {
        return sequestered;
    }

    public void setSequestered(Boolean sequestered) {
        this.sequestered = sequestered;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Boolean getFindAuthorityPolice() {
        return findAuthorityPolice;
    }

    public void setFindAuthorityPolice(Boolean findAuthorityPolice) {
        this.findAuthorityPolice = findAuthorityPolice;
    }

    public Boolean getFindAuthorityCc() {
        return findAuthorityCc;
    }

    public void setFindAuthorityCc(Boolean findAuthorityCc) {
        this.findAuthorityCc = findAuthorityCc;
    }

    public Boolean getFindAuthorityVvuu() {
        return findAuthorityVvuu;
    }

    public void setFindAuthorityVvuu(Boolean findAuthorityVvuu) {
        this.findAuthorityVvuu = findAuthorityVvuu;
    }

    public String getFindAuthorityData() {
        return findAuthorityData;
    }

    public void setFindAuthorityData(String findAuthorityData) {
        this.findAuthorityData = findAuthorityData;
    }

    public String getFindAuthorityTelephone() {
        return findAuthorityTelephone;
    }

    public void setFindAuthorityTelephone(String findAuthorityTelephone) {
        this.findAuthorityTelephone = findAuthorityTelephone;
    }

    public String getTheftNotes() {
        return theftNotes;
    }

    public void setTheftNotes(String theftNotes) {
        this.theftNotes = theftNotes;
    }

    public String getFindNotes() {
        return findNotes;
    }

    public void setFindNotes(String findNotes) {
        this.findNotes = findNotes;
    }

    public String getFirstKeyReception() {
        return firstKeyReception;
    }

    public void setFirstKeyReception(String firstKeyReception) {
        this.firstKeyReception = firstKeyReception;
    }

    public String getSecondKeyReception() {
        return secondKeyReception;
    }

    public void setSecondKeyReception(String secondKeyReception) {
        this.secondKeyReception = secondKeyReception;
    }

    public String getOriginalComplaintReception() {
        return originalComplaintReception;
    }

    public void setOriginalComplaintReception(String originalComplaintReception) {
        this.originalComplaintReception = originalComplaintReception;
    }

    public String getCopyComplaintReception() {
        return copyComplaintReception;
    }

    public void setCopyComplaintReception(String copyComplaintReception) {
        this.copyComplaintReception = copyComplaintReception;
    }

    public String getOriginalReportReception() {
        return originalReportReception;
    }

    public void setOriginalReportReception(String originalReportReception) {
        this.originalReportReception = originalReportReception;
    }

    public String getCopyReportReception() {
        return copyReportReception;
    }

    public void setCopyReportReception(String copyReportReception) {
        this.copyReportReception = copyReportReception;
    }

    public String getLossPossessionRequest() {
        return lossPossessionRequest;
    }

    public void setLossPossessionRequest(String lossPossessionRequest) {
        this.lossPossessionRequest = lossPossessionRequest;
    }

    public String getLossPossessionReception() {
        return lossPossessionReception;
    }

    public void setLossPossessionReception(String lossPossessionReception) {
        this.lossPossessionReception = lossPossessionReception;
    }

    public String getReturnPossessionRequest() {
        return returnPossessionRequest;
    }

    public void setReturnPossessionRequest(String returnPossessionRequest) {
        this.returnPossessionRequest = returnPossessionRequest;
    }

    public String getReturnPossessionReception() {
        return returnPossessionReception;
    }

    public void setReturnPossessionReception(String returnPossessionReception) {
        this.returnPossessionReception = returnPossessionReception;
    }

    public Boolean getRobbery() {
        return robbery;
    }

    public void setRobbery(Boolean robbery) {
        this.robbery = robbery;
    }

    public Boolean getKeyManagement() {
        return keyManagement;
    }

    public void setKeyManagement(Boolean keyManagement) {
        this.keyManagement = keyManagement;
    }

    public Boolean getAccountManagement() {
        return accountManagement;
    }

    public void setAccountManagement(Boolean accountManagement) {
        this.accountManagement = accountManagement;
    }

    public Boolean getAdministrativePosition() {
        return administrativePosition;
    }

    public void setAdministrativePosition(Boolean administrativePosition) {
        this.administrativePosition = administrativePosition;
    }

    public Long getPracticeId() {
        return practiceId;
    }

    public void setPracticeId(Long practiceId) {
        this.practiceId = practiceId;
    }

    public Boolean getReRegistration() {
        return reRegistration;
    }

    public void setReRegistration(Boolean reRegistration) {
        this.reRegistration = reRegistration;
    }

    public String getReRegistrationRequest() {
        return reRegistrationRequest;
    }

    public void setReRegistrationRequest(String reRegistrationRequest) {
        this.reRegistrationRequest = reRegistrationRequest;
    }

    public String getReRegistrationAt() {
        return reRegistrationAt;
    }

    public void setReRegistrationAt(String reRegistrationAt) {
        this.reRegistrationAt = reRegistrationAt;
    }

    public String getReRegistrationSaleforcesCase() {
        return reRegistrationSaleforcesCase;
    }

    public void setReRegistrationSaleforcesCase(String reRegistrationSaleforcesCase) {
        this.reRegistrationSaleforcesCase = reRegistrationSaleforcesCase;
    }

    public Boolean getCertificateDuplication() {
        return certificateDuplication;
    }

    public void setCertificateDuplication(Boolean certificateDuplication) {
        this.certificateDuplication = certificateDuplication;
    }

    public String getCertificateDuplicationRequest() {
        return certificateDuplicationRequest;
    }

    public void setCertificateDuplicationRequest(String certificateDuplicationRequest) {
        this.certificateDuplicationRequest = certificateDuplicationRequest;
    }

    public String getCertificateDuplicationAt() {
        return certificateDuplicationAt;
    }

    public void setCertificateDuplicationAt(String certificateDuplicationAt) {
        this.certificateDuplicationAt = certificateDuplicationAt;
    }

    public String getCertificateDuplicationSalesforceCase() {
        return certificateDuplicationSalesforceCase;
    }

    public void setCertificateDuplicationSalesforceCase(String certificateDuplicationSalesforceCase) {
        this.certificateDuplicationSalesforceCase = certificateDuplicationSalesforceCase;
    }

    public Boolean getStamp() {
        return stamp;
    }

    public void setStamp(Boolean stamp) {
        this.stamp = stamp;
    }

    public String getStampRequest() {
        return stampRequest;
    }

    public void setStampRequest(String stampRequest) {
        this.stampRequest = stampRequest;
    }

    public String getStampAt() {
        return stampAt;
    }

    public void setStampAt(String stampAt) {
        this.stampAt = stampAt;
    }

    public String getStampSalesforceCase() {
        return stampSalesforceCase;
    }

    public void setStampSalesforceCase(String stampSalesforceCase) {
        this.stampSalesforceCase = stampSalesforceCase;
    }

    public Boolean getDemolition() {
        return demolition;
    }

    public void setDemolition(Boolean demolition) {
        this.demolition = demolition;
    }

    public String getDemolitionRequest() {
        return demolitionRequest;
    }

    public void setDemolitionRequest(String demolitionRequest) {
        this.demolitionRequest = demolitionRequest;
    }

    public String getDemolitionAt() {
        return demolitionAt;
    }

    public void setDemolitionAt(String demolitionAt) {
        this.demolitionAt = demolitionAt;
    }

    public String getDemolitionSalesforceCase() {
        return demolitionSalesforceCase;
    }

    public void setDemolitionSalesforceCase(String demolitionSalesforceCase) {
        this.demolitionSalesforceCase = demolitionSalesforceCase;
    }

    public Boolean getWithReceptions() {
        return isWithReceptions;
    }

    public void setWithReceptions(Boolean withReceptions) {
        isWithReceptions = withReceptions;
    }

    public EventTypeEnum getEventType() {
        return eventType;
    }

    public void setEventType(EventTypeEnum eventType) {
        this.eventType = eventType;
    }

    public String getMotivation() {
        return motivation;
    }

    public void setMotivation(String motivation) {
        this.motivation = motivation;
    }

    public List<EmailTemplateMessagingRequestV1> getTemplateList() {
        return templateList;
    }

    public void setTemplateList(List<EmailTemplateMessagingRequestV1> templateList) {
        this.templateList = templateList;
    }

    @Override
    public String toString() {
        return "TheftRequestV1{" +
                "isFound=" + isFound +
                ", OperationsCenterNotified=" + OperationsCenterNotified +
                ", theftOccurredOnCenterAld=" + theftOccurredOnCenterAld +
                ", supplierCode='" + supplierCode + '\'' +
                ", complaintAuthorityPolice=" + complaintAuthorityPolice +
                ", complaintAuthorityCc=" + complaintAuthorityCc +
                ", complaintAuthorityVvuu=" + complaintAuthorityVvuu +
                ", authorityData='" + authorityData + '\'' +
                ", authorityTelephone='" + authorityTelephone + '\'' +
                ", findDate='" + findDate + '\'' +
                ", hour='" + hour + '\'' +
                ", foundAbroad=" + foundAbroad +
                ", vehicleCo='" + vehicleCo + '\'' +
                ", sequestered=" + sequestered +
                ", address=" + address +
                ", findAuthorityPolice=" + findAuthorityPolice +
                ", findAuthorityCc=" + findAuthorityCc +
                ", findAuthorityVvuu=" + findAuthorityVvuu +
                ", findAuthorityData='" + findAuthorityData + '\'' +
                ", findAuthorityTelephone='" + findAuthorityTelephone + '\'' +
                ", theftNotes='" + theftNotes + '\'' +
                ", findNotes='" + findNotes + '\'' +
                ", firstKeyReception='" + firstKeyReception + '\'' +
                ", secondKeyReception='" + secondKeyReception + '\'' +
                ", originalComplaintReception='" + originalComplaintReception + '\'' +
                ", copyComplaintReception='" + copyComplaintReception + '\'' +
                ", originalReportReception='" + originalReportReception + '\'' +
                ", copyReportReception='" + copyReportReception + '\'' +
                ", lossPossessionRequest='" + lossPossessionRequest + '\'' +
                ", lossPossessionReception='" + lossPossessionReception + '\'' +
                ", returnPossessionRequest='" + returnPossessionRequest + '\'' +
                ", returnPossessionReception='" + returnPossessionReception + '\'' +
                ", robbery=" + robbery +
                ", keyManagement=" + keyManagement +
                ", accountManagement=" + accountManagement +
                ", administrativePosition=" + administrativePosition +
                ", practiceId=" + practiceId +
                ", reRegistration=" + reRegistration +
                ", reRegistrationRequest='" + reRegistrationRequest + '\'' +
                ", reRegistrationAt='" + reRegistrationAt + '\'' +
                ", reRegistrationSaleforcesCase='" + reRegistrationSaleforcesCase + '\'' +
                ", certificateDuplication=" + certificateDuplication +
                ", certificateDuplicationRequest='" + certificateDuplicationRequest + '\'' +
                ", certificateDuplicationAt='" + certificateDuplicationAt + '\'' +
                ", certificateDuplicationSalesforceCase='" + certificateDuplicationSalesforceCase + '\'' +
                ", stamp=" + stamp +
                ", stampRequest='" + stampRequest + '\'' +
                ", stampAt='" + stampAt + '\'' +
                ", stampSalesforceCase='" + stampSalesforceCase + '\'' +
                ", demolition=" + demolition +
                ", demolitionRequest='" + demolitionRequest + '\'' +
                ", demolitionAt='" + demolitionAt + '\'' +
                ", demolitionSalesforceCase='" + demolitionSalesforceCase + '\'' +
                ", isWithReceptions=" + isWithReceptions +
                ", isUnderSeizure=" + isUnderSeizure +
                ", eventType=" + eventType +
                ", motivation='" + motivation + '\'' +
                ", templateList=" + templateList +
                '}';
    }
}