package com.doing.nemo.claims.controller.payload.request;

import com.doing.nemo.claims.entity.enumerated.EventTypeEnum;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MessagingSendMailRequestV1 implements Serializable {

    @JsonProperty("event_type")
    private EventTypeEnum eventType;

    @JsonProperty("template_list")
    private List<EmailTemplateMessagingRequestV1> templateList;

    public MessagingSendMailRequestV1() {
    }

    public MessagingSendMailRequestV1(EventTypeEnum eventType, List<EmailTemplateMessagingRequestV1> templateList) {
        this.eventType = eventType;
        if(templateList != null)
        {
            this.templateList = new ArrayList<>(templateList) ;
        }
    }

    public EventTypeEnum getEventType() {
        return eventType;
    }

    public void setEventType(EventTypeEnum eventType) {
        this.eventType = eventType;
    }

    public List<EmailTemplateMessagingRequestV1> getTemplateList() {
        if(templateList == null){
            return null;
        }
        return new ArrayList<>(templateList) ;
    }

    public void setTemplateList(List<EmailTemplateMessagingRequestV1> templateList) {
        if(templateList != null)
        {
            this.templateList = new ArrayList<>(templateList) ;
        } else {
            this.templateList = null;
        }
    }

    @Override
    public String toString() {
        return "ClaimsPatchStatusRequestV1{" +
                "eventType=" + eventType +
                ", templateList=" + templateList +
                '}';
    }

}
