package com.doing.nemo.claims.controller.payload.response;

import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsFlowEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.doing.nemo.claims.entity.settings.DmSystemsEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.UUID;

public class ExportStatusTypeResponseV1  implements Serializable {

    @JsonProperty("id")
    private UUID id;

    @JsonProperty("status_type")
    private ClaimsStatusEnum statusType;

    @JsonProperty("flow_type")
    private ClaimsFlowEnum flowType;

    @JsonProperty("dm_system_id")
    private DmSystemsEntity dmSystem;

    @JsonProperty("code")
    private String code;

    @JsonProperty("description")
    private String description;

    @JsonProperty("is_active")
    private Boolean isActive;

    public ExportStatusTypeResponseV1(UUID id, ClaimsStatusEnum statusType, ClaimsFlowEnum flowType, DmSystemsEntity dmSystem, String code, String description, Boolean isActive) {
        this.id = id;
        this.statusType = statusType;
        this.flowType = flowType;
        this.dmSystem = dmSystem;
        this.code = code;
        this.description = description;
        this.isActive = isActive;
    }

    public ExportStatusTypeResponseV1() {
    }

    public ClaimsStatusEnum getStatusType() {
        return statusType;
    }

    public void setStatusType(ClaimsStatusEnum statusType) {
        this.statusType = statusType;
    }

    public ClaimsFlowEnum getFlowType() {
        return flowType;
    }

    public void setFlowType(ClaimsFlowEnum flowType) {
        this.flowType = flowType;
    }

    public DmSystemsEntity getDmSystem() {
        return dmSystem;
    }

    public void setDmSystem(DmSystemsEntity dmSystem) {
        this.dmSystem = dmSystem;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    @JsonIgnore
    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    @Override
    public String toString() {
        return "ExportStatusTypeResponseV1{" +
                "id=" + id +
                ", statusType=" + statusType +
                ", flowType=" + flowType +
                ", dmSystem=" + dmSystem +
                ", code='" + code + '\'' +
                ", description='" + description + '\'' +
                ", isActive=" + isActive +
                '}';
    }
}
