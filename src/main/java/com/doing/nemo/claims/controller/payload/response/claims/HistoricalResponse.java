package com.doing.nemo.claims.controller.payload.response.claims;

import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.enumerated.EventTypeEnum;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class HistoricalResponse implements Serializable {

    @JsonProperty("event_type")
    private EventTypeEnum eventType;

    @JsonProperty("old_status")
    private ClaimsStatusEnum statusEntityOld;

    @JsonProperty("new_status")
    private ClaimsStatusEnum statusEntityNew;

    @JsonProperty("old_type")
    private DataAccidentTypeAccidentEnum oldType;

    @JsonProperty("new_type")
    private DataAccidentTypeAccidentEnum newType;

    @JsonProperty("update_at")
    private String updateAt;

    @JsonProperty("user_id")
    private String userId;

    @JsonProperty("user_name")
    private String userName;

    @JsonProperty("old_flow")
    private String oldFlow;

    @JsonProperty("new_flow")
    private String newFlow;

    @JsonProperty("comunication_description")
    private String comunicationDescription;

    public HistoricalResponse() {
    }

    public HistoricalResponse(EventTypeEnum eventType, ClaimsStatusEnum statusEntityOld, ClaimsStatusEnum statusEntityNew, DataAccidentTypeAccidentEnum oldType, DataAccidentTypeAccidentEnum newType, String updateAt, String userId, String userName, String oldFlow, String newFlow, String comunicationDescription) {
        this.eventType = eventType;
        this.statusEntityOld = statusEntityOld;
        this.statusEntityNew = statusEntityNew;
        this.oldType = oldType;
        this.newType = newType;
        this.updateAt = updateAt;
        this.userId = userId;
        this.userName = userName;
        this.oldFlow = oldFlow;
        this.newFlow = newFlow;
        this.comunicationDescription = comunicationDescription;
    }

    public DataAccidentTypeAccidentEnum getOldType() {
        return oldType;
    }

    public void setOldType(DataAccidentTypeAccidentEnum oldType) {
        this.oldType = oldType;
    }

    public DataAccidentTypeAccidentEnum getNewType() {
        return newType;
    }

    public void setNewType(DataAccidentTypeAccidentEnum newType) {
        this.newType = newType;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public ClaimsStatusEnum getStatusEntityOld() {
        return statusEntityOld;
    }

    public void setStatusEntityOld(ClaimsStatusEnum statusEntityOld) {
        this.statusEntityOld = statusEntityOld;
    }

    public ClaimsStatusEnum getStatusEntityNew() {
        return statusEntityNew;
    }

    public void setStatusEntityNew(ClaimsStatusEnum statusEntityNew) {
        this.statusEntityNew = statusEntityNew;
    }

    public String getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(String updateAt) {
        this.updateAt = updateAt;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public EventTypeEnum getEventType() {
        return eventType;
    }

    public void setEventType(EventTypeEnum eventType) {
        this.eventType = eventType;
    }

    public String getComunicationDescription() {
        return comunicationDescription;
    }

    public void setComunicationDescription(String comunicationDescription) {
        this.comunicationDescription = comunicationDescription;
    }

    public String getOldFlow() {
        return oldFlow;
    }

    public void setOldFlow(String oldFlow) {
        this.oldFlow = oldFlow;
    }

    public String getNewFlow() {
        return newFlow;
    }

    public void setNewFlow(String newFlow) {
        this.newFlow = newFlow;
    }

    @Override
    public String toString() {
        return "HistoricalResponse{" +
                "eventType=" + eventType +
                ", statusEntityOld=" + statusEntityOld +
                ", statusEntityNew=" + statusEntityNew +
                ", oldType=" + oldType +
                ", newType=" + newType +
                ", updateAt='" + updateAt + '\'' +
                ", userId='" + userId + '\'' +
                ", userName='" + userName + '\'' +
                ", oldFlow='" + oldFlow + '\'' +
                ", newFlow='" + newFlow + '\'' +
                ", comunicationDescription='" + comunicationDescription + '\'' +
                '}';
    }
}
