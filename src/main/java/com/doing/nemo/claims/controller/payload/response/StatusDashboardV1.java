package com.doing.nemo.claims.controller.payload.response;

import java.io.Serializable;

public class StatusDashboardV1 implements Serializable {
    private StatusDashboardElementV1 WAITING_FOR_VALIDATION = new StatusDashboardElementV1();
    private StatusDashboardElementV1 INCOMPLETE = new StatusDashboardElementV1();
    private StatusDashboardElementV1 TO_ENTRUST = new StatusDashboardElementV1();
    private StatusDashboardElementV1 WAITING_FOR_REFUND = new StatusDashboardElementV1();
    private StatusDashboardElementV1 CLOSED_PARTIAL_REFUND = new StatusDashboardElementV1();
    private StatusDashboardElementV1 TO_SEND_TO_CUSTOMER = new StatusDashboardElementV1();

    public StatusDashboardV1() {
    }


    public StatusDashboardElementV1 getWAITING_FOR_VALIDATION() {
        return WAITING_FOR_VALIDATION;
    }

    public void setWAITING_FOR_VALIDATION(StatusDashboardElementV1 WAITING_FOR_VALIDATION) {
        this.WAITING_FOR_VALIDATION = WAITING_FOR_VALIDATION;
    }

    public StatusDashboardElementV1 getINCOMPLETE() {
        return INCOMPLETE;
    }

    public void setINCOMPLETE(StatusDashboardElementV1 INCOMPLETE) {
        this.INCOMPLETE = INCOMPLETE;
    }

    public StatusDashboardElementV1 getTO_ENTRUST() {
        return TO_ENTRUST;
    }

    public void setTO_ENTRUST(StatusDashboardElementV1 TO_ENTRUST) {
        this.TO_ENTRUST = TO_ENTRUST;
    }

    public StatusDashboardElementV1 getWAITING_FOR_REFUND() {
        return WAITING_FOR_REFUND;
    }

    public void setWAITING_FOR_REFUND(StatusDashboardElementV1 WAITING_FOR_REFUND) {
        this.WAITING_FOR_REFUND = WAITING_FOR_REFUND;
    }

    public StatusDashboardElementV1 getCLOSED_PARTIAL_REFUND() {
        return CLOSED_PARTIAL_REFUND;
    }

    public void setCLOSED_PARTIAL_REFUND(StatusDashboardElementV1 CLOSED_PARTIAL_REFUND) {
        this.CLOSED_PARTIAL_REFUND = CLOSED_PARTIAL_REFUND;
    }

    public StatusDashboardElementV1 getTO_SEND_TO_CUSTOMER() {
        return TO_SEND_TO_CUSTOMER;
    }

    public void setTO_SEND_TO_CUSTOMER(StatusDashboardElementV1 TO_SEND_TO_CUSTOMER) {
        this.TO_SEND_TO_CUSTOMER = TO_SEND_TO_CUSTOMER;
    }

    @Override
    public String toString() {
        return "StatusDashboardV1{" +
                "WAITING_FOR_VALIDATION=" + WAITING_FOR_VALIDATION +
                ", INCOMPLETE=" + INCOMPLETE +
                ", TO_ENTRUST=" + TO_ENTRUST +
                ", WAITING_FOR_REFUND=" + WAITING_FOR_REFUND +
                ", CLOSED_PARTIAL_REFUND=" + CLOSED_PARTIAL_REFUND +
                ", TO_SEND_TO_CUSTOMER=" + TO_SEND_TO_CUSTOMER +
                '}';
    }
}
