package com.doing.nemo.claims.controller.payload.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@JsonIgnoreProperties(ignoreUnknown = true)
public class PermissionsResponseV1 implements Serializable {


    private static final long serialVersionUID = -4479105426021042227L;


    @JsonProperty("permissions")
    private List<PermissionResponseV1> permissions = new ArrayList<>();

    public List<PermissionResponseV1> getPermissions() {
        if(permissions == null){
            return null;
        }
        return new ArrayList<>(permissions);
    }

    public void setPermissions(List<PermissionResponseV1> permissions) {
        if(permissions != null)
        {
            this.permissions = new ArrayList<>(permissions);
        } else {
            this.permissions = null;
        }
    }
}
