package com.doing.nemo.claims.controller.payload.response.damaged;

import com.doing.nemo.claims.entity.enumerated.DamagedEnum.ImpactPointEnum.ImpactPointDetectionImpactPointEnum;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ImpactPointResponse implements Serializable {

    @JsonProperty("damage_to_vehicle")
    private String damageToVehicle;

    @JsonProperty("incident_description")
    private String incidentDescription;

    @JsonProperty("detections_impact_point")
    private List<ImpactPointDetectionImpactPointEnum> detectionsImpactPoint;

    public String getDamageToVehicle() {
        return damageToVehicle;
    }

    public void setDamageToVehicle(String damageToVehicle) {
        this.damageToVehicle = damageToVehicle;
    }

    public String getIncidentDescription() {
        return incidentDescription;
    }

    public void setIncidentDescription(String incidentDescription) {
        this.incidentDescription = incidentDescription;
    }

    public List<ImpactPointDetectionImpactPointEnum> getDetectionsImpactPoint() {
        if(detectionsImpactPoint == null){
            return null;
        }
        return new ArrayList<>(detectionsImpactPoint);
    }

    public void setDetectionsImpactPoint(List<ImpactPointDetectionImpactPointEnum> detectionsImpactPoint) {
        if(detectionsImpactPoint != null)
        {
            this.detectionsImpactPoint = new ArrayList<>(detectionsImpactPoint);
        } else {
            this.detectionsImpactPoint = null;
        }
    }

    @Override
    public String toString() {
        return "ImpactPoint{" +
                "damageToVehicle='" + damageToVehicle + '\'' +
                ", incidentDescription='" + incidentDescription + '\'' +
                ", detectionsImpactPoint=" + detectionsImpactPoint +
                '}';
    }

}
