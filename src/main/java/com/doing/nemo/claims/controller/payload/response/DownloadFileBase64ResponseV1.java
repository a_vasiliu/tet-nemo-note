package com.doing.nemo.claims.controller.payload.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel
public class DownloadFileBase64ResponseV1 implements Serializable {

    private static final long serialVersionUID = 259548357601649146L;

    @JsonProperty("resource_type")
    @ApiModelProperty(example = "orders")
    private String resourceType;

    @JsonProperty("resource_id")
    @ApiModelProperty(example = "129347")
    private String resourceId;

    @JsonProperty("file_name")
    @ApiModelProperty(example = "supermario.png")
    private String fileName;

    @JsonProperty("file_size")
    @ApiModelProperty(example = "28579")
    private Integer fileSize;

    @JsonProperty("mime_type")
    @ApiModelProperty(example = "image/png")
    private String mimeType;

    @ApiModelProperty(example = "Whooa there's a description field, too!")
    private String description;

    @JsonProperty("blob_type")
    @ApiModelProperty(example = "adc")
    private String blobType;

    @JsonProperty("file_content")
    @ApiModelProperty(example = "<base64 encoded string>")
    private String fileContent;


    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public String getResourceId() {
        return resourceId;
    }

    public void setResourceId(String resourceId) {
        this.resourceId = resourceId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Integer getFileSize() {
        return fileSize;
    }

    public void setFileSize(Integer fileSize) {
        this.fileSize = fileSize;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBlobType() {
        return blobType;
    }

    public void setBlobType(String blobType) {
        this.blobType = blobType;
    }

    public String getFileContent() {
        return fileContent;
    }

    public void setFileContent(String fileContent) {
        this.fileContent = fileContent;
    }

    @Override
    public String toString() {
        return "DownloadFileBase64ResponseV1{" +
                "resourceType='" + resourceType + '\'' +
                ", resourceId='" + resourceId + '\'' +
                ", fileName='" + fileName + '\'' +
                ", fileSize=" + fileSize +
                ", mimeType='" + mimeType + '\'' +
                ", description='" + description + '\'' +
                ", blobType='" + blobType + '\'' +
                ", fileContent='" + fileContent + '\'' +
                '}';
    }
}
