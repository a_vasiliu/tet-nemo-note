package com.doing.nemo.claims.controller.payload.response;

import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsFlowEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ContractTypeEntityResponseV1 implements Serializable {

    @JsonProperty("id")
    private String id;

    @JsonProperty("cod_contract_type")
    private String codContractType;

    @JsonProperty("description")
    private String description;

    @JsonProperty("unlocking_entry")
    private Boolean flagWS;

    @JsonProperty("default_flow")
    private ClaimsFlowEnum defaultFlow;

    @JsonProperty("ctrnote")
    private String ctrnote;

    @JsonProperty("ricaricar")
    private Boolean ricaricar;

    @JsonProperty("franchise")
    private Double franchise;

    @JsonProperty("flow_contract_type")
    private List<FlowContractResponseV1> flowContractResponseV1List = new LinkedList<>();

    @JsonProperty("is_active")
    private Boolean isActive;

    @JsonIgnore
    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCodContractType() {
        return codContractType;
    }

    public void setCodContractType(String codContractType) {
        this.codContractType = codContractType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getFlagWS() {
        return flagWS;
    }

    public void setFlagWS(Boolean flagWS) {
        this.flagWS = flagWS;
    }

    public String getCtrnote() {
        return ctrnote;
    }

    public void setCtrnote(String ctrnote) {
        this.ctrnote = ctrnote;
    }

    public Boolean getRicaricar() {
        return ricaricar;
    }

    public void setRicaricar(Boolean ricaricar) {
        this.ricaricar = ricaricar;
    }

    public Double getFranchise() {
        return franchise;
    }

    public void setFranchise(Double franchise) {
        this.franchise = franchise;
    }

    public List<FlowContractResponseV1> getFlowContractResponseV1List() {
        if(flowContractResponseV1List == null){
            return null;
        }
        return new ArrayList<>(flowContractResponseV1List);
    }

    public void setFlowContractResponseV1List(List<FlowContractResponseV1> flowContractResponseV1List) {
        if(flowContractResponseV1List != null)
        {
            this.flowContractResponseV1List = new ArrayList<>(flowContractResponseV1List);
        } else {
            this.flowContractResponseV1List = null;
        }
    }

    public ClaimsFlowEnum getDefaultFlow() {
        return defaultFlow;
    }

    public void setDefaultFlow(ClaimsFlowEnum defaultFlow) {
        this.defaultFlow = defaultFlow;
    }

    @Override
    public String toString() {
        return "ContractTypeEntityResponseV1{" +
                "id='" + id + '\'' +
                ", codContractType='" + codContractType + '\'' +
                ", description='" + description + '\'' +
                ", flagWS=" + flagWS +
                ", defaultFlow=" + defaultFlow +
                ", ctrnote='" + ctrnote + '\'' +
                ", ricaricar=" + ricaricar +
                ", franchise=" + franchise +
                ", flowContractResponseV1List=" + flowContractResponseV1List +
                '}';
    }
}
