package com.doing.nemo.claims.controller.payload.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.UUID;

public class InsuranceCompanyExternalResponseV1 implements Serializable {

    @JsonProperty("id")
    private UUID id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("ania_code")
    private Integer aniaCode;

    public InsuranceCompanyExternalResponseV1() {
    }

    public InsuranceCompanyExternalResponseV1(UUID id, String name) {
        this.id = id;
        this.name = name;
    }

    public InsuranceCompanyExternalResponseV1(UUID id, String name, Integer aniaCode) {
        this.id = id;
        this.name = name;
        this.aniaCode = aniaCode;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAniaCode() {
        return aniaCode;
    }

    public void setAniaCode(Integer aniaCode) {
        this.aniaCode = aniaCode;
    }
}
