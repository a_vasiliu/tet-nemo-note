package com.doing.nemo.claims.controller.payload.response;

import com.doing.nemo.claims.entity.jsonb.personalData.FleetManagerPersonalData;
import com.doing.nemo.claims.entity.settings.ContractEntity;
import com.doing.nemo.claims.entity.settings.CustomTypeRiskEntity;
import com.doing.nemo.claims.entity.settings.InsurancePolicyPersonalDataEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class PersonalDataResponseV1 implements Serializable {

    @JsonProperty("id")
    private UUID id;

    @JsonProperty("personal_code")
    private Long personalCode;

    @JsonProperty("customer_id")
    private String customerId;

    @JsonProperty("name")
    private String name;

    @JsonProperty("address")
    private String address;

    @JsonProperty("zip_code")
    private String zipCode;

    @JsonProperty("city")
    private String city;

    @JsonProperty("province")
    private String province;

    @JsonProperty("state")
    private String state;

    @JsonProperty("phone_number")
    private String phoneNumber;

    @JsonProperty("fax")
    private String fax;

    @JsonProperty("email")
    private String email;

    @JsonProperty("pec")
    private String pec;

    @JsonProperty("web_site")
    private String webSite;

    @JsonProperty("personal_group")
    private String personalGroup;

    @JsonProperty("contact")
    private String contact;

    @JsonProperty("vat_number")
    private String vatNumber;

    @JsonProperty("fiscal_code")
    private String fiscalCode;

    @JsonProperty("personal_father_id")
    private String personalFatherId;

    @JsonProperty("personal_father_name")
    private String personalFatherName;

    @JsonProperty("fleet_managers")
    private List<FleetManagerPersonalData> fleetManagerPersonalData;

    @JsonProperty("contracts")
    private List<ContractEntity> contractEntityList;

    @JsonProperty("insurance_policies")
    private List<InsurancePolicyPersonalDataEntity> insurancePolicyPersonalDataEntityList;

    @JsonProperty("custom_type_risks")
    private List<CustomTypeRiskEntity> customTypeRiskEntityList;

    @JsonProperty("is_active")
    private Boolean isActive;

    @JsonProperty("created_at")
    private String createdAt;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Long getPersonalCode() {
        return personalCode;
    }

    public void setPersonalCode(Long personalCode) {
        this.personalCode = personalCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebSite() {
        return webSite;
    }

    public void setWebSite(String webSite) {
        this.webSite = webSite;
    }

    public String getPersonalGroup() {
        return personalGroup;
    }

    public void setPersonalGroup(String personalGroup) {
        this.personalGroup = personalGroup;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getVatNumber() {
        return vatNumber;
    }

    public void setVatNumber(String vatNumber) {
        this.vatNumber = vatNumber;
    }

    public String getFiscalCode() {
        return fiscalCode;
    }

    public void setFiscalCode(String fiscalCode) {
        this.fiscalCode = fiscalCode;
    }

    public String getPersonalFatherId() {
        return personalFatherId;
    }

    public void setPersonalFatherId(String personalFatherId) {
        this.personalFatherId = personalFatherId;
    }

    public String getPersonalFatherName() {
        return personalFatherName;
    }

    public void setPersonalFatherName(String personalFatherName) {
        this.personalFatherName = personalFatherName;
    }

    public List<FleetManagerPersonalData> getFleetManagerPersonalData() {
        if(fleetManagerPersonalData == null){
            return null;
        }
        return new ArrayList<>(fleetManagerPersonalData);
    }

    public void setFleetManagerPersonalData(List<FleetManagerPersonalData> fleetManagerPersonalData) {
        if(fleetManagerPersonalData != null)
        {
            this.fleetManagerPersonalData = new ArrayList<>(fleetManagerPersonalData);
        } else {
            this.fleetManagerPersonalData = null;
        }
    }

    public List<ContractEntity> getContractEntityList() {
        if(contractEntityList == null){
            return null;
        }
        return new ArrayList<>(contractEntityList);
    }

    public void setContractEntityList(List<ContractEntity> contractEntityList) {
        if(contractEntityList != null)
        {
            this.contractEntityList = new ArrayList<>(contractEntityList);
        } else {
            this.contractEntityList = null;
        }
    }

    public List<InsurancePolicyPersonalDataEntity> getInsurancePolicyPersonalDataEntityList() {
        if(insurancePolicyPersonalDataEntityList == null){
            return null;
        }
        return new ArrayList<>(insurancePolicyPersonalDataEntityList);
    }

    public void setInsurancePolicyPersonalDataEntityList(List<InsurancePolicyPersonalDataEntity> insurancePolicyPersonalDataEntityList) {
        if(insurancePolicyPersonalDataEntityList != null)
        {
            this.insurancePolicyPersonalDataEntityList = new ArrayList<>(insurancePolicyPersonalDataEntityList);
        } else {
            this.insurancePolicyPersonalDataEntityList = null;
        }
    }

    public List<CustomTypeRiskEntity> getCustomTypeRiskEntityList() {
        if(customTypeRiskEntityList == null){
            return null;
        }
        return new ArrayList<>(customTypeRiskEntityList);
    }

    public void setCustomTypeRiskEntityList(List<CustomTypeRiskEntity> customTypeRiskEntityList) {
        if(customTypeRiskEntityList != null)
        {
            this.customTypeRiskEntityList = new ArrayList<>(customTypeRiskEntityList);
        } else {
            this.customTypeRiskEntityList = null;
        }
    }

    @JsonIgnore
    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public String getPec() {
        return pec;
    }

    public void setPec(String pec) {
        this.pec = pec;
    }


    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public String toString() {
        return "PersonalDataResponseV1{" +
                "id=" + id +
                ", personalCode=" + personalCode +
                ", customerId='" + customerId + '\'' +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", zipCode='" + zipCode + '\'' +
                ", city='" + city + '\'' +
                ", province='" + province + '\'' +
                ", state='" + state + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", fax='" + fax + '\'' +
                ", email='" + email + '\'' +
                ", pec='" + pec + '\'' +
                ", webSite='" + webSite + '\'' +
                ", personalGroup='" + personalGroup + '\'' +
                ", contact='" + contact + '\'' +
                ", vatNumber='" + vatNumber + '\'' +
                ", fiscalCode='" + fiscalCode + '\'' +
                ", personalFatherId='" + personalFatherId + '\'' +
                ", personalFatherName='" + personalFatherName + '\'' +
                ", fleetManagerPersonalData=" + fleetManagerPersonalData +
                ", contractEntityList=" + contractEntityList +
                ", insurancePolicyPersonalDataEntityList=" + insurancePolicyPersonalDataEntityList +
                ", customTypeRiskEntityList=" + customTypeRiskEntityList +
                ", isActive=" + isActive +
                ", createdAt=" + createdAt +
                '}';
    }
}
