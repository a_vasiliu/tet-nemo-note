package com.doing.nemo.claims.controller;

import com.doing.nemo.claims.adapter.DataManagementImportAdapter;
import com.doing.nemo.claims.controller.payload.response.DataManagementImportResponsePaginationV1;
import com.doing.nemo.claims.controller.payload.response.DataManagementImportResponseV1;
import com.doing.nemo.claims.entity.enumerated.FileImportTypeEnum;
import com.doing.nemo.claims.entity.settings.DataManagementImportEntity;
import com.doing.nemo.claims.repository.DataManagementImportRepository;
import com.doing.nemo.claims.repository.DataManagementImportRepositoryV1;
import com.doing.nemo.claims.service.ImportService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.rmi.ConnectException;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/v1")
public class ImportController {



    @Autowired
    private ImportService importService;

    @Autowired
    private DataManagementImportRepositoryV1 dataManagementImportRepositoryV1;

    @Autowired
    private DataManagementImportRepository dataManagementImportRepository;


    //REFACTOR
    @PostMapping(value = "/import/exemption")
    public ResponseEntity postImportExemptionFile(@ApiParam(value = "File to be processed", required = true)
                                @RequestParam("file") MultipartFile file,
                                @ApiParam(value = "File content description", required = true)
                                @RequestParam("description") String fileDescription,
                                @ApiParam(value = "File Import TypeEnum", required = true)
                                @RequestParam("file_type") FileImportTypeEnum fileType) throws ConnectException {

        return new ResponseEntity<>(importService.importFileWithWasteFile(file, fileDescription, fileType), HttpStatus.OK);
    }


    //REFACTOR
    //QUESTA FUNZIONE DOVRA' ESSERE CHIAMATA DAL CRONE
    //@Scheduled(cron = "0 0 2 * * ?")
    @PostMapping("/import/exemptionone/calculation")
    public void importExemptionOneCalculation(){

        importService.startJob(FileImportTypeEnum.FRANCHISE_1);
    }

    //REFACTOR
    //@Scheduled(cron = "0 0 3 * * ?")
    @PostMapping("/import/exemptiontwo/calculation")
    public void importExemptionTwoCalculation(){

        importService.startJob(FileImportTypeEnum.FRANCHISE_2);
    }

    //REFACTOR
    //@Scheduled(cron = "0 0 4 * * ?")
    @PostMapping("/import/claimsnumber")
    public void importClaimsNumer(){
        importService.startJob(FileImportTypeEnum.NUMBER_SX);
    }


    @ApiOperation(value="Data managment import pagination", notes = "")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = DataManagementImportResponsePaginationV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @GetMapping(value="/datamanagmentimport", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<DataManagementImportResponsePaginationV1> searchDataManagmentImport(
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @RequestParam(value = "page_size", defaultValue = "20") Integer pageSize
    ){

        List<DataManagementImportEntity> listDataImport = dataManagementImportRepositoryV1.findPaginationDataManagementImport(page, pageSize );
        Long count = dataManagementImportRepositoryV1.countPaginationDataManagementImport();
        /*if((listDataImport == null || listDataImport.isEmpty()) && page != 1){
            page = 1;
            listDataImport = dataManagementImportRepository.getDataManagementPagination(pageSize, (page-1)*pageSize );
        }*/
        DataManagementImportResponsePaginationV1 dataManagementImportPaginationResponseV1 = DataManagementImportAdapter.adptPagination(listDataImport, page, pageSize, count);
        return new ResponseEntity<>(dataManagementImportPaginationResponseV1, HttpStatus.OK);
    }

    @ApiOperation(value="Data managment import by UUID", notes = "")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = DataManagementImportResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @GetMapping(value="/datamanagmentimport/{UUID_DATAIMPORT}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<DataManagementImportResponseV1> getDataManagmentImportById(
            @ApiParam(value = "UUID that identifies the data management import", required = true)
            @PathVariable(name = "UUID_DATAIMPORT") String dataImportId
    ){

        DataManagementImportResponseV1 searchAppropriationResponseV1 = DataManagementImportAdapter.adptDataManagementImportToDataManagementImportResponse(dataManagementImportRepository.getOne(UUID.fromString(dataImportId)));

        return new ResponseEntity<>(searchAppropriationResponseV1, HttpStatus.OK);
    }


    //NON PIU' UTILIZZATA
    @PostMapping(value = "/import")
    public ResponseEntity postImportFile(@ApiParam(value = "File to be processed", required = true)
                                                  @RequestParam("file") MultipartFile file,
                                                  @ApiParam(value = "File content description", required = true)
                                                  @RequestParam("description") String fileDescription,
                                                  @ApiParam(value = "File Import TypeEnum", required = true)
                                                  @RequestParam("file_type") FileImportTypeEnum fileType,
                                                  @ApiParam(value = "Practice status", required = true)
                                                  @RequestParam("status") String status) {

        importService.importFile(file, fileDescription, fileType, status);


        return new ResponseEntity<>( HttpStatus.OK);
    }


    //REFACTOR
    @PostMapping(value = "/import/massive/entrust")
    public ResponseEntity postImportMassiveEntrustFile(@ApiParam(value = "File to be processed", required = true)
                                         @RequestParam("file") MultipartFile file,
                                         @ApiParam(value = "File content description", required = true)
                                         @RequestParam("description") String fileDescription) throws ConnectException {

        return new ResponseEntity<>( importService.importFileWithWasteFile(file, fileDescription, FileImportTypeEnum.MASSIVE_ENTRUST), HttpStatus.OK);
    }

    //REFACTOR
    @PostMapping("import/massive/entrust/execute")
    public void executeMassiveEntrust(){

        importService.startJob(FileImportTypeEnum.MASSIVE_ENTRUST);
    }


    //REFACTOR
    @PostMapping(value = "/import/lawyer/payments")
    public ResponseEntity postImportLawyerPaymentsFile(@ApiParam(value = "File to be processed", required = true)
                                                       @RequestParam("file") MultipartFile file,
                                                       @ApiParam(value = "File content description", required = true)
                                                       @RequestParam("description") String fileDescription,
                                                       @ApiParam(value = "File Import TypeEnum", required = true)
                                                       @RequestParam("file_type") FileImportTypeEnum fileType) throws ConnectException {

        return new ResponseEntity<>( importService.importFileWithWasteFile(file, fileDescription, fileType), HttpStatus.OK);
    }

    //REFACTOR
    @PostMapping("import/lawyer/payments/execute")
    public void executeImportLegalPaymentsFile(){

        importService.startJob(FileImportTypeEnum.BANK_TRANSFER);
        importService.startJob(FileImportTypeEnum.ALLOWANCE);
    }

}
