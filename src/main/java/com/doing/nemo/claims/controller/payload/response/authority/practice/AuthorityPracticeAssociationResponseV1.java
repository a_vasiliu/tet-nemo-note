package com.doing.nemo.claims.controller.payload.response.authority.practice;

import com.doing.nemo.claims.entity.enumerated.PracticeTypeEnum;
import com.doing.nemo.claims.entity.jsonb.Processing;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AuthorityPracticeAssociationResponseV1 implements Serializable {

    @JsonProperty("id")
    private String id;

    @JsonProperty("practice_id")
    private Long practiceId;

    @JsonProperty("practice_type")
    private PracticeTypeEnum practiceType;

    @JsonProperty("processing_list")
    private List<Processing> processingList;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getPracticeId() {
        return practiceId;
    }

    public void setPracticeId(Long practiceId) {
        this.practiceId = practiceId;
    }

    public PracticeTypeEnum getPracticeType() {
        return practiceType;
    }

    public void setPracticeType(PracticeTypeEnum practiceType) {
        this.practiceType = practiceType;
    }

    public List<Processing> getProcessingList() {
        if(processingList == null){
            return null;
        }
        return new ArrayList<>(processingList);
    }

    public void setProcessingList(List<Processing> processingList) {
        if(processingList != null)
        {
            this.processingList = new ArrayList<>(processingList);
        } else {
            this.processingList = null;
        }
    }

    @Override
    public String toString() {
        return "AuthorityPracticeAssociationResponseV1{" +
                "id='" + id + '\'' +
                ", practiceId=" + practiceId +
                ", practiceType=" + practiceType +
                ", processingList=" + processingList +
                '}';
    }
}
