package com.doing.nemo.claims.controller;


import com.doing.nemo.claims.adapter.MotivationAdapter;
import com.doing.nemo.claims.common.validation.MessageCode;
import com.doing.nemo.claims.common.validation.RequestValidator;
import com.doing.nemo.claims.controller.payload.request.ListRequestV1;
import com.doing.nemo.claims.controller.payload.request.MotivationRequestV1;
import com.doing.nemo.claims.controller.payload.response.IdResponseV1;
import com.doing.nemo.claims.controller.payload.response.MotivationResponseV1;
import com.doing.nemo.claims.controller.payload.response.PaginationResponseV1;
import com.doing.nemo.claims.entity.enumerated.ClaimsRepairEnum;
import com.doing.nemo.claims.entity.settings.MotivationEntity;
import com.doing.nemo.claims.service.MotivationService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/v1")
@Api("Motivation")
public class MotivationController {

    @Autowired
    private MotivationService motivationService;

    @Autowired
    private RequestValidator requestValidator;


    @PostMapping("/motivation")
    @Transactional
    @ApiOperation(value = "Insert Motivation", notes = "Insert Motivation using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = IdResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<IdResponseV1> postMotivation(
            @ApiParam(value = "Body of the Motivation to be created", required = true)
            @RequestBody MotivationRequestV1 motivationRequestV1) {
        requestValidator.validateRequest(motivationRequestV1, MessageCode.E00X_1000);
        MotivationEntity motivationEntity = MotivationAdapter.adptFromMotivationRequestToMotivationEntity(motivationRequestV1);
        motivationEntity.setTypeComplaint(ClaimsRepairEnum.REPAIR);
        motivationEntity = motivationService.insertMotivation(motivationEntity);
        IdResponseV1 motivationResponseIdV1 = new IdResponseV1(motivationEntity.getId());
        return new ResponseEntity(motivationResponseIdV1, HttpStatus.CREATED);
    }

    @PostMapping("/motivations")
    @Transactional
    @ApiOperation(value = "Insert More Motivations", notes = "Insert Motivations using info passed in the bodies")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = MotivationResponseV1.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<MotivationResponseV1>> postMotivations(
            @ApiParam(value = "Bodies of the Motivations to be created", required = true)
            @RequestBody ListRequestV1<MotivationRequestV1> motivationListRequestV1,
            @ApiParam(value = "TypeEnum of complaint", required = true)
            @RequestParam(value = "type_complaint") ClaimsRepairEnum typeComplaint) {

        List<MotivationResponseV1> motivationResponseV1List = new ArrayList<>();
        for (MotivationRequestV1 motivationRequestV1 : motivationListRequestV1.getData()) {
            requestValidator.validateRequest(motivationRequestV1, MessageCode.E00X_1000);
            MotivationEntity motivationEntity = MotivationAdapter.adptFromMotivationRequestToMotivationEntity(motivationRequestV1);
            motivationEntity.setTypeComplaint(typeComplaint);
            motivationEntity = motivationService.insertMotivation(motivationEntity);
            MotivationResponseV1 motivationResponseV1 = MotivationAdapter.adptFromMotivationEntityToMotivationResponseV1(motivationEntity);
            motivationResponseV1List.add(motivationResponseV1);
        }
        return new ResponseEntity(motivationResponseV1List, HttpStatus.CREATED);
    }

    @PutMapping("/motivation/{UUID}")
    @Transactional
    @ApiOperation(value = "Upload Motivation", notes = "Upload Motivation using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = MotivationResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<MotivationResponseV1> putMotivation(@ApiParam(value = "UUID that identifies the Motivation to be modified", required = true) @PathVariable(name = "UUID") UUID id, @ApiParam(value = "Updated body of the Motivation", required = true)
    @RequestBody MotivationRequestV1 motivationRequestV1) {
        requestValidator.validateRequest(motivationRequestV1, MessageCode.E00X_1000);
        MotivationEntity motivationEntity = motivationService.selectMotivation(id);
        MotivationEntity motivationEntityNew = MotivationAdapter.adptFromMotivationRequestToMotivationEntity(motivationRequestV1);
        motivationEntityNew.setId(motivationEntity.getId());
        motivationEntityNew.setTypeComplaint(ClaimsRepairEnum.REPAIR);
        motivationEntityNew = motivationService.updateMotivation(motivationEntityNew);
        MotivationResponseV1 motivationResponseV1 = MotivationAdapter.adptFromMotivationEntityToMotivationResponseV1(motivationEntityNew);
        return new ResponseEntity<>(motivationResponseV1, HttpStatus.OK);
    }

    @GetMapping("/motivation/{UUID}")
    @Transactional
    @ApiOperation(value = "Recover Motivation", notes = "Return a Motivation using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = MotivationResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<MotivationResponseV1> getMotivation(@ApiParam(value = "UUID of the Motivation to be found", required = true) @PathVariable(name = "UUID") UUID id) {
        MotivationEntity motivationEntity = motivationService.selectMotivation(id);
        MotivationResponseV1 motivationResponseV1 = MotivationAdapter.adptFromMotivationEntityToMotivationResponseV1(motivationEntity);
        return new ResponseEntity<>(motivationResponseV1, HttpStatus.OK);
    }

    @GetMapping("/motivation")
    @Transactional
    @ApiOperation(value = "Recover All Motivations", notes = "Returns all Motivations")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = MotivationResponseV1.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<MotivationResponseV1>> getAllMotivation(
            @RequestParam(value = "is_active", required = false) Boolean isActive
    ) {
        ClaimsRepairEnum typeComplaint = ClaimsRepairEnum.REPAIR;
        List<MotivationEntity> motivationEntityList = motivationService.selectAllMotivationFilteredByIsActive(typeComplaint, isActive);
        List<MotivationResponseV1> motivationResponseV1List = MotivationAdapter.adptFromMotivationEntityToMotivationResponseV1List(motivationEntityList);
        return new ResponseEntity(motivationResponseV1List, HttpStatus.OK);
    }

    @DeleteMapping("/motivation/{UUID}")
    @Transactional
    @ApiOperation(value = "Delete Motivation", notes = "Delete a Motivation using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = MotivationResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<MotivationResponseV1> deleteMotivation(@ApiParam(value = "UUID of the Motivation to be deleted", required = true) @PathVariable(name = "UUID") UUID id) {
        MotivationResponseV1 motivationResponseV1 = motivationService.deleteMotivation(id);
        return new ResponseEntity<>(motivationResponseV1, HttpStatus.OK);
    }

    @PatchMapping("/motivation/{UUID}/active")
    @Transactional
    @ApiOperation(value = "Patch active status Motivation", notes = "Patch active status of Motivation using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = MotivationResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<MotivationResponseV1> patchStatusMotivation(
            @PathVariable UUID UUID
    ) {
        MotivationEntity motivationEntity = motivationService.updateStatus(UUID);
        MotivationResponseV1 responseV1 = MotivationAdapter.adptFromMotivationEntityToMotivationResponseV1(motivationEntity);
        return new ResponseEntity<>(responseV1, HttpStatus.OK);
    }

    @ApiOperation(value="Search Motivation", notes = "It retrieves motivations data through one of more parameters")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = PaginationResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @GetMapping(value="/motivation/pagination", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<PaginationResponseV1<MotivationResponseV1>> searchMotivation(
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @RequestParam(value = "page_size", defaultValue = "20") Integer pageSize,
            //@RequestParam(value = "type_complaint", required = false) ClaimsRepairEnum claimsRepairEnum,
            @RequestParam(value = "answer_type", required = false) String answerType,
            @RequestParam(value = "description", required = false) String description,
            @RequestParam(value = "is_active", required = false) Boolean isActive,
            @RequestParam(value = "order_by", required = false, defaultValue="answer_type") String orderBy,
            @RequestParam(value = "asc", required = false, defaultValue = "true") Boolean asc
    ){

        ClaimsRepairEnum claimsRepairEnum = ClaimsRepairEnum.REPAIR;
        List<MotivationEntity> listMotivation = motivationService.findMotivationsFiltered(page,pageSize, claimsRepairEnum, answerType, description,isActive, asc, orderBy);
        Long itemCount = motivationService.countMotivationFiltered(claimsRepairEnum, answerType, description,isActive);

        PaginationResponseV1<MotivationResponseV1> paginationResponseV1 = MotivationAdapter.adptPagination(listMotivation, page, pageSize, itemCount);

        return new ResponseEntity<>(paginationResponseV1, HttpStatus.OK);
    }

}
