package com.doing.nemo.claims.controller.payload.request.authority.practice;

import com.doing.nemo.claims.entity.enumerated.AuthorityEnum.CarPracticeTypeEnum;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CarPracticeRequestV1 implements Serializable {

    @JsonProperty("id")
    private String id;

    @JsonProperty("practice_id")
    private Long practiceId;

    @JsonProperty("practice_type")
    private CarPracticeTypeEnum practiceType;

    @JsonProperty("workings_list")
    private List<AuthorityWorkingListRequestV1> workingsList;

    public List<AuthorityWorkingListRequestV1> getWorkingsList() {
        if(workingsList == null){
            return null;
        }
        return new ArrayList<>(workingsList);
    }

    public void setWorkingsList(List<AuthorityWorkingListRequestV1> workingsList) {
        if(workingsList != null)
        {
            this.workingsList = new ArrayList<>(workingsList);
        } else {
            this.workingsList = null;
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getPracticeId() {
        return practiceId;
    }

    public void setPracticeId(Long practiceId) {
        this.practiceId = practiceId;
    }

    public CarPracticeTypeEnum getPracticeType() {
        return practiceType;
    }

    public void setPracticeType(CarPracticeTypeEnum practiceType) {
        this.practiceType = practiceType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CarPracticeRequestV1 that = (CarPracticeRequestV1) o;
        return Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}