package com.doing.nemo.claims.controller.payload.request.authority.repair;

import com.doing.nemo.claims.controller.payload.request.IdRequestV1;
import com.doing.nemo.claims.controller.payload.request.authority.claims.CommodityDetailsRequestV1;
import com.doing.nemo.claims.entity.enumerated.AuthorityEnum.AuthorityTypeEnum;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AuthorityRepairRequestV1 implements Serializable {


    @JsonProperty("authority_dossier_id")
    private String authorityDossierId;

    @JsonProperty("authority_dossier_number")
    private String authorityDossierNumber;

    @JsonProperty("authority_working_id")
    private String authorityWorkingId;

    @JsonProperty("working_number")
    private String workingNumber;

    /*
    @JsonProperty("claims_id_list")
    private List<String> claimsIdList;
    */

    @JsonProperty("repair_list")
    private List<IdRequestV1> repairList;

    @JsonProperty("plate")
    private String plate;

    @JsonProperty("type")
    private AuthorityTypeEnum type;

    @JsonProperty("commodity_details")
    private CommodityDetailsRequestV1 commodityDetails;

    public String getAuthorityDossierId() {
        return authorityDossierId;
    }

    public void setAuthorityDossierId(String authorityDossierId) {
        this.authorityDossierId = authorityDossierId;
    }

    public String getAuthorityDossierNumber() {
        return authorityDossierNumber;
    }

    public void setAuthorityDossierNumber(String authorityDossierNumber) {
        this.authorityDossierNumber = authorityDossierNumber;
    }

    public String getAuthorityWorkingId() {
        return authorityWorkingId;
    }

    public void setAuthorityWorkingId(String authorityWorkingId) {
        this.authorityWorkingId = authorityWorkingId;
    }

    public String getWorkingNumber() {
        return workingNumber;
    }

    public void setWorkingNumber(String workingNumber) {
        this.workingNumber = workingNumber;
    }

    public List<IdRequestV1> getRepairList() {
        if(repairList == null){
            return null;
        }
        return new ArrayList<>(repairList);
    }

    public void setRepairList(List<IdRequestV1> repairList) {
        if(repairList != null)
        {
            this.repairList = new ArrayList<>(repairList);
        } else {
            this.repairList = null;
        }
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public AuthorityTypeEnum getType() {
        return type;
    }

    public void setType(AuthorityTypeEnum type) {
        this.type = type;
    }

    public CommodityDetailsRequestV1 getCommodityDetails() {
        return commodityDetails;
    }

    public void setCommodityDetails(CommodityDetailsRequestV1 commodityDetails) {
        this.commodityDetails = commodityDetails;
    }

    @Override
    public String toString() {
        return "AuthorityRepairRequestV1{" +
                "authorityDossierId='" + authorityDossierId + '\'' +
                ", authorityDossierNumber='" + authorityDossierNumber + '\'' +
                ", authorityWorkingId='" + authorityWorkingId + '\'' +
                ", workingNumber='" + workingNumber + '\'' +
                ", repairList=" + repairList +
                ", plate='" + plate + '\'' +
                ", type=" + type +
                ", commodityDetails=" + commodityDetails +
                '}';
    }
}
