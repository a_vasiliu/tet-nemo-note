package com.doing.nemo.claims.controller.payload.request;

import com.doing.nemo.claims.controller.payload.request.complaint.FromCompanyRequest;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class ClaimsPatchExternalAcclaimsRequest implements Serializable {
    @JsonProperty("number_sx_counterparty")
    private String numberSxCounterparty;

    @JsonProperty("status")
    private String status;

    public ClaimsPatchExternalAcclaimsRequest() {
    }

    public String getNumberSxCounterparty() {
        return numberSxCounterparty;
    }

    public void setNumberSxCounterparty(String numberSxCounterparty) {
        this.numberSxCounterparty = numberSxCounterparty;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "ClaimsPatchExternalAcclaimsRequest{" +
                "numberSxCounterparty='" + numberSxCounterparty + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
