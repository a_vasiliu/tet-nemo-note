package com.doing.nemo.claims.controller.payload.request;

import com.doing.nemo.claims.controller.payload.request.counterparty.*;
import com.doing.nemo.claims.entity.enumerated.DamagedEnum.VehicleEnum.VehicleTypeEnum;
import com.doing.nemo.claims.entity.enumerated.RepairEnum.ManagementTypeEnum;
import com.doing.nemo.claims.entity.enumerated.RepairEnum.RepairProcedureEnum;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;

import javax.persistence.Column;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CounterpartyPatchRequestV1 implements Serializable {

    @JsonProperty("type")
    private VehicleTypeEnum type;

    @JsonProperty("procedure_repair")
    private RepairProcedureEnum repairProcedure;

    @JsonProperty("user_update")
    private String userUpdate;

    @JsonProperty("assigned_to")
    private AssigneeRepairRequestV1 assignedTo;

    @JsonProperty("assigned_at")
    private String assignedAt;

    @JsonProperty("insured")
    private InsuredRequest insured;

    @JsonProperty("insurance_company")
    private InsuranceCompanyCounterpartyRequest insuranceCompany;

    @JsonProperty("driver")
    private DriverRequest driver;

    @JsonProperty("vehicle")
    private VehicleRequest vehicle;

    @JsonProperty("is_cai_signed")
    private Boolean isCaiSigned;

    @JsonProperty("impact_point")
    private ImpactPointRequest impactPoint;

    @JsonProperty("responsible")
    private Boolean responsible;

    @JsonProperty("is_complete_documentation")
    private Boolean isCompleteDocumentation = false;

    @JsonProperty("replacement_car")
    private Boolean replacementCar;

    @JsonProperty("replacement_plate")
    private String replacementPlate;

    @JsonProperty("eligibility")
    private Boolean eligibility;

    @JsonProperty("description")
    private String description;

    @JsonProperty("last_contact")
    private List<LastContactRequest> lastContact;

    @JsonProperty("manager")
    private ManagerRequestV1 manager;

    @JsonProperty("canalization")
    private CanalizationRequest canalization;

    @JsonProperty("policy_number")
    private String policyNumber;

    @JsonProperty("policy_beginning_validity")
    private String policyBeginningValidity;

    @JsonProperty("policy_end_validity")
    private String policyEndValidity;

    @JsonProperty("management_type")
    private ManagementTypeEnum managementType;

    @JsonProperty("asked_for_damages")
    private Boolean askedForDamages;

    @JsonProperty("legal_or_consultant")
    private Boolean legalOrConsultant;

    @JsonProperty("date_request_damages")
    private String dateRequestDamages;

    @JsonProperty("expiration_date")
    private String expirationDate;

    @JsonProperty("legal")
    private String legal;

    @JsonProperty("email_legal")
    private String emailLegal;

    @JsonProperty("is_ald")
    private Boolean isAld;

    @JsonProperty("is_read_msa")
    private Boolean isRead = false;

    public Boolean getIsCompleteDocumentation() {
        return isCompleteDocumentation;
    }

    public void setIsCompleteDocumentation(Boolean completeDocumentation) {
        isCompleteDocumentation = completeDocumentation;
    }

    public String getReplacementPlate() {
        return replacementPlate;
    }

    public void setReplacementPlate(String replacementPlate) {
        this.replacementPlate = replacementPlate;
    }

    public Boolean getReplacementCar() {
        return replacementCar;
    }

    public void setReplacementCar(Boolean replacementCar) {
        this.replacementCar = replacementCar;
    }

    public ManagementTypeEnum getManagementType() {
        return managementType;
    }

    public void setManagementType(ManagementTypeEnum managementType) {
        this.managementType = managementType;
    }

    public Boolean getAskedForDamages() {
        return askedForDamages;
    }

    public void setAskedForDamages(Boolean askedForDamages) {
        this.askedForDamages = askedForDamages;
    }

    public Boolean getLegalOrConsultant() {
        return legalOrConsultant;
    }

    public void setLegalOrConsultant(Boolean legalOrConsultant) {
        this.legalOrConsultant = legalOrConsultant;
    }

    public String getDateRequestDamages() {
        return dateRequestDamages;
    }

    public void setDateRequestDamages(String dateRequestDamages) {
        this.dateRequestDamages = dateRequestDamages;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getLegal() {
        return legal;
    }

    public void setLegal(String legal) {
        this.legal = legal;
    }

    public String getEmailLegal() {
        return emailLegal;
    }

    public void setEmailLegal(String emailLegal) {
        this.emailLegal = emailLegal;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public String getPolicyBeginningValidity() {
        return policyBeginningValidity;
    }

    public void setPolicyBeginningValidity(String policyBeginningValidity) {
        this.policyBeginningValidity = policyBeginningValidity;
    }

    public String getPolicyEndValidity() {
        return policyEndValidity;
    }

    public void setPolicyEndValidity(String policyEndValidity) {
        this.policyEndValidity = policyEndValidity;
    }

    public VehicleTypeEnum getType() {
        return type;
    }

    public void setType(VehicleTypeEnum type) {
        this.type = type;
    }

    public RepairProcedureEnum getRepairProcedure() {
        return repairProcedure;
    }

    public void setRepairProcedure(RepairProcedureEnum repairProcedure) {
        this.repairProcedure = repairProcedure;
    }

    public String getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(String userUpdate) {
        this.userUpdate = userUpdate;
    }

    public AssigneeRepairRequestV1 getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(AssigneeRepairRequestV1 assignedTo) {
        this.assignedTo = assignedTo;
    }

    public String getAssignedAt() {
        return assignedAt;
    }

    public void setAssignedAt(String assignedAt) {
        this.assignedAt = assignedAt;
    }

    public InsuredRequest getInsured() {
        return insured;
    }

    public void setInsured(InsuredRequest insured) {
        this.insured = insured;
    }

    public InsuranceCompanyCounterpartyRequest getInsuranceCompany() {
        return insuranceCompany;
    }

    public void setInsuranceCompany(InsuranceCompanyCounterpartyRequest insuranceCompany) {
        this.insuranceCompany = insuranceCompany;
    }

    public DriverRequest getDriver() {
        return driver;
    }

    public void setDriver(DriverRequest driver) {
        this.driver = driver;
    }

    public VehicleRequest getVehicle() {
        return vehicle;
    }

    public void setVehicle(VehicleRequest vehicle) {
        this.vehicle = vehicle;
    }

    public Boolean getCaiSigned() {
        return isCaiSigned;
    }

    public void setCaiSigned(Boolean caiSigned) {
        isCaiSigned = caiSigned;
    }

    public ImpactPointRequest getImpactPoint() {
        return impactPoint;
    }

    public void setImpactPoint(ImpactPointRequest impactPoint) {
        this.impactPoint = impactPoint;
    }

    public Boolean getResponsible() {
        return responsible;
    }

    public void setResponsible(Boolean responsible) {
        this.responsible = responsible;
    }

    public Boolean getEligibility() {
        return eligibility;
    }

    public void setEligibility(Boolean eligibility) {
        this.eligibility = eligibility;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<LastContactRequest> getLastContact() {
        if(lastContact == null){
            return null;
        }
        return new ArrayList<>(lastContact);
    }

    public void setLastContact(List<LastContactRequest> lastContact) {
        if(lastContact != null)
        {
            this.lastContact = new ArrayList<>(lastContact);
        } else {
            this.lastContact = null;
        }
    }

    public ManagerRequestV1 getManager() {
        return manager;
    }

    public void setManager(ManagerRequestV1 manager) {
        this.manager = manager;
    }

    public CanalizationRequest getCanalization() {
        return canalization;
    }

    public void setCanalization(CanalizationRequest canalization) {
        this.canalization = canalization;
    }

    public Boolean getAld() {
        return isAld;
    }

    public void setAld(Boolean ald) {
        isAld = ald;
    }

    public Boolean getRead() {
        return isRead;
    }

    @JsonSetter
    public void setRead(Boolean read) {
        if(read != null)
            isRead = read;
    }

    @Override
    public String toString() {
        return "CounterpartyPatchRequestV1{" +
                "type=" + type +
                ", repairProcedure=" + repairProcedure +
                ", userUpdate='" + userUpdate + '\'' +
                ", assignedTo='" + assignedTo + '\'' +
                ", assignedAt=" + assignedAt +
                ", insured=" + insured +
                ", insuranceCompany=" + insuranceCompany +
                ", driver=" + driver +
                ", vehicle=" + vehicle +
                ", isCaiSigned=" + isCaiSigned +
                ", impactPoint=" + impactPoint +
                ", responsible=" + responsible +
                ", eligibility=" + eligibility +
                ", description='" + description + '\'' +
                ", lastContact=" + lastContact +
                ", manager=" + manager +
                ", canalization=" + canalization +
                '}';
    }
}
