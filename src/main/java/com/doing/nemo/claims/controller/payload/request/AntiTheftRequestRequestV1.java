package com.doing.nemo.claims.controller.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class AntiTheftRequestRequestV1 implements Serializable {

    @JsonProperty("request")
    private String request;

    @JsonProperty("id_websin")
    private String idWebSin;

    @JsonProperty("provider_type")
    private String providerType;

    @JsonProperty("response")
    private String response;

    @JsonProperty("response_type")
    private String responseType;

    @JsonProperty("outcome")
    private String outcome;

    @JsonProperty("sub_report")
    private Integer subReport;

    @JsonProperty("g_power")
    private Double gPower;

    @JsonProperty("crash_number")
    private Integer crashNumber;

    @JsonProperty("anomaly")
    private Boolean anomaly;

    @JsonProperty("pdf")
    private String pdf;

    @JsonProperty("response_message")
    private String responseMessage;

    public AntiTheftRequestRequestV1() {
    }

    public AntiTheftRequestRequestV1(String request, String idWebSin, String providerType, String response, String responseType, String outcome, Integer subReport, Double gPower, Integer crashNumber, Boolean anomaly, String pdf, String responseMessage) {
        this.request = request;
        this.idWebSin = idWebSin;
        this.providerType = providerType;
        this.response = response;
        this.responseType = responseType;
        this.outcome = outcome;
        this.subReport = subReport;
        this.gPower = gPower;
        this.crashNumber = crashNumber;
        this.anomaly = anomaly;
        this.pdf = pdf;
        this.responseMessage = responseMessage;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getIdWebSin() {
        return idWebSin;
    }

    public void setIdWebSin(String idWebSin) {
        this.idWebSin = idWebSin;
    }

    public String getProviderType() {
        return providerType;
    }

    public void setProviderType(String providerType) {
        this.providerType = providerType;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getResponseType() {
        return responseType;
    }

    public void setResponseType(String responseType) {
        this.responseType = responseType;
    }

    public String getOutcome() {
        return outcome;
    }

    public void setOutcome(String outcome) {
        this.outcome = outcome;
    }

    public Integer getSubReport() {
        return subReport;
    }

    public void setSubReport(Integer subReport) {
        this.subReport = subReport;
    }

    public Double getgPower() {
        return gPower;
    }

    public void setgPower(Double gPower) {
        this.gPower = gPower;
    }

    public Integer getCrashNumber() {
        return crashNumber;
    }

    public void setCrashNumber(Integer crashNumber) {
        this.crashNumber = crashNumber;
    }

    public Boolean getAnomaly() {
        return anomaly;
    }

    public void setAnomaly(Boolean anomaly) {
        this.anomaly = anomaly;
    }

    public String getPdf() {
        return pdf;
    }

    public void setPdf(String pdf) {
        this.pdf = pdf;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    @Override
    public String toString() {
        return "AntiTheftRequestRequestV1{" +
                ", request=" + request +
                ", idWebSin='" + idWebSin + '\'' +
                ", providerType='" + providerType + '\'' +
                ", response=" + response +
                ", responseType=" + responseType +
                ", outcome='" + outcome + '\'' +
                ", subReport=" + subReport +
                ", gPower=" + gPower +
                ", crashNumber=" + crashNumber +
                ", anomaly=" + anomaly +
                ", pdf='" + pdf + '\'' +
                ", responseMessage='" + responseMessage + '\'' +
                '}';
    }
}
