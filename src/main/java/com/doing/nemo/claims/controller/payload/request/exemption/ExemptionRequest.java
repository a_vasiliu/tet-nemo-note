package com.doing.nemo.claims.controller.payload.request.exemption;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Date;

public class ExemptionRequest  implements Serializable {
    @JsonProperty("deduct_end_1")
    private Date deductEnd1;

    @JsonProperty("deduct_end_2")
    private Date deductEnd2;

    @JsonProperty("deduct_paid_1")
    private Double deductPaid1;

    @JsonProperty("deduct_paid_2")
    private Double deductPaid2;

    @JsonProperty("deduct_imported_1")
    private Date deductImported1;

    @JsonProperty("deduct_imported_2")
    private Date deductImported2;

    @JsonProperty("deduct_total_paid")
    private Double deductTotalPaid;

    @JsonProperty("company_reference")
    private String companyReference;

    @JsonProperty("policy_number")
    private String policyNumber;

    public Date getDeductEnd1() {
        if(deductEnd1 == null){
            return null;
        }
        return (Date)deductEnd1.clone();
    }

    public void setDeductEnd1(Date deductEnd1) {
        if(deductEnd1 != null)
        {
            this.deductEnd1 = (Date)deductEnd1.clone();
        } else {
            this.deductEnd1 = null;
        }
    }

    public Date getDeductEnd2() {
        if(deductEnd2 == null){
            return null;
        }
        return (Date)deductEnd2.clone();
    }

    public void setDeductEnd2(Date deductEnd2) {
        if(deductEnd2 != null)
        {
            this.deductEnd2 =  (Date)deductEnd2.clone();
        } else {
            this.deductEnd2 = null;
        }
    }

    public Double getDeductPaid1() {
        return deductPaid1;
    }

    public void setDeductPaid1(Double deductPaid1) {
        this.deductPaid1 = deductPaid1;
    }

    public Double getDeductPaid2() {
        return deductPaid2;
    }

    public void setDeductPaid2(Double deductPaid2) {
        this.deductPaid2 = deductPaid2;
    }

    public Date getDeductImported1() {
        if(deductImported1 == null){
            return null;
        }
        return (Date)deductImported1.clone();
    }

    public void setDeductImported1(Date deductImported1) {
        if(deductImported1 != null)
        {
            this.deductImported1 =(Date)deductImported1.clone();
        } else {
            this.deductImported1 = null;
        }
    }

    public Date getDeductImported2() {
        if(deductImported2 == null){
            return null;
        }
        return (Date)deductImported2.clone();
    }

    public void setDeductImported2(Date deductImported2) {
        if(deductImported2 != null)
        {
            this.deductImported2 = (Date)deductImported2.clone();
        } else {
            this.deductImported2 = null;
        }
    }

    public Double getDeductTotalPaid() {
        return deductTotalPaid;
    }

    public void setDeductTotalPaid(Double deductTotalPaid) {
        this.deductTotalPaid = deductTotalPaid;
    }

    public String getCompanyReference() {
        return companyReference;
    }

    public void setCompanyReference(String companyReference) {
        this.companyReference = companyReference;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }
}
