package com.doing.nemo.claims.controller;

import com.doing.nemo.claims.adapter.EntrustedAdapter;
import com.doing.nemo.claims.controller.payload.request.ClaimsInsertRequestV1;
import com.doing.nemo.claims.controller.payload.request.EmailTemplateMessagingRequestV1;
import com.doing.nemo.claims.controller.payload.request.MessagingSendMailRequestV1;
import com.doing.nemo.claims.controller.payload.request.SequelWithNotifyMessagingRequestV1;
import com.doing.nemo.claims.controller.payload.request.complaint.EntrustedRequest;
import com.doing.nemo.claims.controller.payload.request.complaint.EntrustedRequestLight;
import com.doing.nemo.claims.controller.payload.response.EmailTemplateInternalMessagingResponseV1;
import com.doing.nemo.claims.controller.payload.response.EmailTemplateMessagingResponseV1;
import com.doing.nemo.claims.controller.payload.response.SendMailResponseV1;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.enumerated.EventTypeEnum;
import com.doing.nemo.claims.entity.jsonb.complaint.jsonbComplaint.Entrusted;
import com.doing.nemo.claims.service.ClaimsService;
import com.doing.nemo.claims.service.MessagingService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/v1")
public class MessagingController {

    @Autowired
    private MessagingService messagingService;

    @Autowired
    private ClaimsService claimsService;

    //REFACTOR
    /*RECUPERO TEMPLATE PER INVIO MAIL IN BASE ALL'EVENTO SCATENATO */
    @GetMapping(value = "/messaging/{EVENT_TYPE}/{UUID_CLAIMS}",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Get template mail", notes = "Recover a specific email template using the event type")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = SendMailResponseV1.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<EmailTemplateMessagingResponseV1>> getEmailTemplate(
            @ApiParam(value = "UUID of the Claim", required = true)
            @PathVariable(name = "UUID_CLAIMS") String claimsId,
            @ApiParam(value = "Defines the type of event", required = true)
            @PathVariable(name = "EVENT_TYPE") EventTypeEnum eventType) throws IOException {

        //return new ResponseEntity<>(messagingService.getMailTemplateByTypeEvent(eventType, claimsRepository.getOne(claimsId)), HttpStatus.OK);
        return new ResponseEntity<>(messagingService.getMailTemplateByTypeEvent(eventType, claimsId), HttpStatus.OK);
    }


    //REFACTOR
    /*RECUPERO TEMPLATE PER INVIO MAIL IN BASE AL CLAIMS*/
    @PostMapping(value = "/messaging/generate",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Generate template mail", notes = "Generate a specific email template using the body of claim")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = SendMailResponseV1.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<EmailTemplateInternalMessagingResponseV1>> generateEmailMessaging(
            @ApiParam(value = "Body of the Claims to be created", required = true)
            @RequestBody ClaimsInsertRequestV1 claimsRequestV1,
            @RequestParam(value = "is_incomplete", defaultValue = "false") boolean isIncomplete) {

        //return new ResponseEntity<>(messagingService.getMailTemplateByTypeEvent(eventType, claimsRepository.getOne(claimsId)), HttpStatus.OK);
        ClaimsEntity claimsEntity = claimsService.adptClaimsInsertRequestToClaimsEntityWithFlow(claimsRequestV1);
        List<EventTypeEnum> eventTypeEnum = claimsService.checkEventTypeByFlowInternal(claimsEntity, isIncomplete);
        System.out.println(eventTypeEnum);
        return new ResponseEntity<>(messagingService.getMailTemplateByTypeEventListAndClaimsInternal(eventTypeEnum,claimsEntity), HttpStatus.OK);
    }


    //REFACTOR
    /*RECUPERO TEMPLATE PER INVIO MAIL IN BASE ALL'EVENTO SCATENATO */
    @GetMapping(value = "/messaging/repair/{EVENT_TYPE}/{UUID_CLAIMS}/{UUID_COUNTERPARTY}",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Get template mail", notes = "Recover a specific repair email template using the event type")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = SendMailResponseV1.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<EmailTemplateMessagingResponseV1>> getEmailTemplateRepairer(
            @ApiParam(value = "UUID of the Claim", required = true)
            @PathVariable(name = "UUID_CLAIMS") String claimsId,
            @ApiParam(value = "UUID of the counterparty", required = true)
            @PathVariable(name = "UUID_COUNTERPARTY") String counterpartyId,
            @ApiParam(value = "Defines the type of event", required = true)
            @PathVariable(name = "EVENT_TYPE") EventTypeEnum eventType) {

        ;
        return new ResponseEntity<>(messagingService.getRepairMailTemplateByTypeEvent(eventType, claimsId, counterpartyId), HttpStatus.OK);
    }


    //REFACTOR
    /*RECUPERO TEMPLATE PER INVIO MAIL IN BASE ALL'EVENTO AFFIDO PAI */
    @GetMapping(value = "/messaging/entrustpai/{UUID_CLAIMS}",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Get template mail", notes = "Recover a specific email template using the event type")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = SendMailResponseV1.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<EmailTemplateMessagingResponseV1>> getEmailTemplateEntrustPai(
            @ApiParam(value = "UUID of the Claim", required = true)
            @PathVariable(name = "UUID_CLAIMS") String claimsId) throws IOException {

        return new ResponseEntity<>(messagingService.getMailTemplateByTypeEvent(EventTypeEnum.ENTRUST_PAI, claimsId), HttpStatus.OK);
    }


    //REFACTOR
    /*RECUPERO TEMPLATE PER INVIO MAIL IN BASE ALL'EVENTO COMUNICAZIONE LEGALE */
    @GetMapping(value = "/messaging/legal/{UUID_CLAIMS}",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Get template mail", notes = "Recover a specific email template using the event type")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = SendMailResponseV1.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<EmailTemplateMessagingResponseV1>> getEmailTemplateLegalComunication(
            @ApiParam(value = "UUID of the Claim", required = true)
            @PathVariable(name = "UUID_CLAIMS") String claimsId) throws IOException {

        return new ResponseEntity<>(messagingService.getMailTemplateByTypeEvent(EventTypeEnum.LEGAL_COMUNICATION, claimsId), HttpStatus.OK);
    }

    //REFACTOR
    /*INVIO EMAIL*/
    @PostMapping(value = "/messaging",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Send mail", notes = "Send email using email template received")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = SendMailResponseV1.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity postSendEmail(
            @RequestHeader(name = "nemo-user-id") String userId,
            @RequestHeader(name = "nemo-user-first-name") String userName,
            @RequestHeader(name = "nemo-user-last-name") String lastName,
            @ApiParam(value = "List of template email to be send", required = true)
            @RequestBody MessagingSendMailRequestV1 messagingSendMailRequestV1,
            @RequestParam(value = "id_claim", required = false) String idClaim,
            @RequestParam(value = "id_counterparty", required = false) String idCounterparty) {

        messagingService.sendEmailConvertionURLAndLog(messagingSendMailRequestV1, idCounterparty, idClaim, userId, userName+" "+lastName);
        return new ResponseEntity<>(HttpStatus.OK);
    }


    //REFACTOR
    /*INVIO EMAIL ENTRUST PAI*/
    @PostMapping(value = "/messaging/entrustpai/{UUID_CLAIMS}",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Send mail", notes = "Send email using email template received")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = SendMailResponseV1.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity postSendEmailEntrustPai(
            @ApiParam(value = "UUID of the Claim", required = true)
            @PathVariable(name = "UUID_CLAIMS") String claimsId,
            @ApiParam(value = "Id of the user who calls the service", required = true)
            @RequestHeader(name = "nemo-user-id") String userId,
            @RequestHeader(name = "nemo-user-first-name") String userName,
            @RequestHeader(name = "nemo-user-last-name") String lastName,
            @ApiParam(value = "List of template email to be send", required = true)
            @RequestBody List<EmailTemplateMessagingRequestV1> emailTemplateMessagingRequestList) throws IOException {

        return new ResponseEntity<>(messagingService.sendEmailEntrustPai(emailTemplateMessagingRequestList, claimsId, userId, userName + " " + lastName));
    }

    //REFACTOR
    /*INVIO EMAIL LEGAL COMUNICATION*/
    @PostMapping(value = "/messaging/legal/{UUID_CLAIMS}",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Send mail", notes = "Send email using email template received")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = SendMailResponseV1.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity postSendEmailLegalComunication(
            @ApiParam(value = "UUID of the Claim", required = true)
            @PathVariable(name = "UUID_CLAIMS") String claimsId,
            @ApiParam(value = "Id of the user who calls the service", required = true)
            @RequestHeader(name = "nemo-user-id") String userId,
            @RequestHeader(name = "nemo-user-first-name") String userName,
            @RequestHeader(name = "nemo-user-last-name") String lastName,
            @ApiParam(value = "List of template email to be send", required = true)
            @RequestBody List<EmailTemplateMessagingRequestV1> emailTemplateMessagingRequestList) throws IOException {

        return new ResponseEntity<>(messagingService.sendEmailLegalComunication(emailTemplateMessagingRequestList, claimsId, userId, userName + " " + lastName));
    }


    //REFACTOR
    /*INVIO EMAIL PO VARIATION*/
    @PostMapping(value = "/messaging/po/{UUID_CLAIMS}",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Send mail", notes = "Send email using email template received")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = SendMailResponseV1.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity postSendEmailPoVariation(
            @ApiParam(value = "UUID of the Claim", required = true)
            @PathVariable(name = "UUID_CLAIMS") String claimsId,
            @ApiParam(value = "Id of the user who calls the service", required = true)
            @RequestHeader(name = "nemo-user-id") String userId,
            @RequestHeader(name = "nemo-user-first-name") String userName,
            @RequestHeader(name = "nemo-user-last-name") String lastName,
            @ApiParam(value = "List of template email to be send", required = true)
            @RequestBody List<EmailTemplateMessagingRequestV1> emailTemplateMessagingRequestList) throws IOException {

        return new ResponseEntity<>(messagingService.sendEmailPoVariation(emailTemplateMessagingRequestList, claimsId, userId, userName + " " + lastName));
    }


    //REFACTOR
    /*INVIO EMAIL CONFERMA CON NOTIFICA SEGUITI*/
    @PostMapping(value = "/messaging/sequelnotify/{UUID_CLAIMS}",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Send mail", notes = "Send email using email template received")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = SendMailResponseV1.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity postSendEmailConfirmSequelNotify(
            @ApiParam(value = "UUID of the Claim", required = true)
            @PathVariable(name = "UUID_CLAIMS") String claimsId,
            @ApiParam(value = "Id of the user who calls the service", required = true)
            @RequestHeader(name = "nemo-user-id") String userId,
            @RequestHeader(name = "nemo-user-first-name") String userName,
            @RequestHeader(name = "nemo-user-last-name") String lastName,
            @ApiParam(value = "List of template email to be send", required = true)
            @RequestBody SequelWithNotifyMessagingRequestV1 requestSequelMessaging) {

        ClaimsEntity claims = claimsService.patchValidateAttachments(claimsId, requestSequelMessaging.getData());

        return new ResponseEntity<>(messagingService.sendEmailConfirmSequelNotify(requestSequelMessaging, claimsId, userId, userName + " " + lastName, claims));
    }

    //REFACTOR
    /* NE-370 */
    //RECUPER EMAIL PER AFFIDO MANUALE
    @PostMapping(value = "/messaging/entrust/{EVENT_TYPE}/{UUID_CLAIMS}",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Get template mail", notes = "Recover a specific email template using the event type")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = SendMailResponseV1.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<EmailTemplateMessagingResponseV1>> getEmailTemplateWithEntrust(
            @ApiParam(value = "UUID of the Claim", required = true)
            @PathVariable(name = "UUID_CLAIMS") String claimsId,
            @ApiParam(value = "Defines the type of event", required = true)
            @PathVariable(name = "EVENT_TYPE") EventTypeEnum eventType,
            @RequestBody EntrustedRequestLight entrustedRequest) throws IOException {

        Entrusted entrusted = EntrustedAdapter.adptEntrustedRequestLightToEntrusted(entrustedRequest);
        return new ResponseEntity<>(messagingService.getMailTemplateByTypeEventManualEntrust(entrusted,eventType, claimsId), HttpStatus.OK);
    }

}
