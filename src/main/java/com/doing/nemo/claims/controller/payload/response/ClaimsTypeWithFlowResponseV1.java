package com.doing.nemo.claims.controller.payload.response;

import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.UUID;

public class ClaimsTypeWithFlowResponseV1 implements Serializable {

    @JsonProperty("id")
    private UUID id;

    @JsonProperty("type")
    private String type;

    @JsonProperty("claims_accident_type")
    private DataAccidentTypeAccidentEnum claimsAccidentType;

    @JsonProperty("flow")
    private String flow;

    @JsonProperty("rebilling")
    private String rebilling;

    public ClaimsTypeWithFlowResponseV1() {
    }

    public ClaimsTypeWithFlowResponseV1(UUID id, String type, DataAccidentTypeAccidentEnum claimsAccidentType, String flow, String rebilling) {
        this.id = id;
        this.type = type;
        this.claimsAccidentType = claimsAccidentType;
        this.flow = flow;
        this.rebilling = rebilling;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public DataAccidentTypeAccidentEnum getClaimsAccidentType() {
        return claimsAccidentType;
    }

    public void setClaimsAccidentType(DataAccidentTypeAccidentEnum claimsAccidentType) {
        this.claimsAccidentType = claimsAccidentType;
    }

    public String getFlow() {
        return flow;
    }

    public void setFlow(String flow) {
        this.flow = flow;
    }

    public String getRebilling() {
        return rebilling;
    }

    public void setRebilling(String rebilling) {
        this.rebilling = rebilling;
    }

    @Override
    public String toString() {
        return "ClaimsTypeWithFlowResponseV1{" +
                "id=" + id +
                ", type='" + type + '\'' +
                ", claimsAccidentType=" + claimsAccidentType +
                ", flow='" + flow + '\'' +
                ", rebilling='" + rebilling + '\'' +
                '}';
    }
}
