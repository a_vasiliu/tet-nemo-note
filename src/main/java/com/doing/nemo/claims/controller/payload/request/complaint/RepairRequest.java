package com.doing.nemo.claims.controller.payload.request.complaint;

import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.RepairEnum.RepairStatusRepairEnum;
import com.doing.nemo.claims.entity.jsonb.Address;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class RepairRequest implements Serializable {

    @JsonProperty("status_repair")
    private RepairStatusRepairEnum statusRepair;

    @JsonProperty("vehicle_value")
    private Double vehicleValue;

    @JsonProperty("blocks_repair")
    private Boolean blocksRepair = false;

    @JsonProperty("unrepairable")
    private Boolean unrepairable = false;

    @JsonProperty("motivation")
    private String motivation;

    @JsonProperty("repairer")
    private String repairer;

    @JsonProperty("phone")
    private String phone;

    @JsonProperty("email")
    private String email;

    @JsonProperty("address")
    private Address address;

    public RepairRequest() {
    }

    public RepairRequest(RepairStatusRepairEnum statusRepair, Double vehicleValue, Boolean blocksRepair, Boolean unrepairable,
                         String motivation, String repairer, String phone, String email, Address address) {

        this.statusRepair = statusRepair;
        this.vehicleValue = vehicleValue;
        this.blocksRepair = blocksRepair;
        this.unrepairable = unrepairable;
        this.motivation = motivation;
        this.repairer = repairer;
        this.phone = phone;
        this.email = email;
        this.address = address;
    }

    public RepairStatusRepairEnum getStatusRepair() {
        return statusRepair;
    }

    public void setStatusRepair(RepairStatusRepairEnum statusRepair) {
        this.statusRepair = statusRepair;
    }

    public Double getVehicleValue() {
        return vehicleValue;
    }

    public void setVehicleValue(Double vehicleValue) {
        this.vehicleValue = vehicleValue;
    }

    public Boolean getBlocksRepair() {
        return blocksRepair;
    }

    public void setBlocksRepair(Boolean blocksRepair) {
        this.blocksRepair = blocksRepair;
    }

    public Boolean getUnrepairable() {
        return unrepairable;
    }

    public void setUnrepairable(Boolean unrepairable) {
        this.unrepairable = unrepairable;
    }

    public String getMotivation() {
        return motivation;
    }

    public void setMotivation(String motivation) {
        this.motivation = motivation;
    }

    public String getRepairer() {
        return repairer;
    }

    public void setRepairer(String repairer) {
        this.repairer = repairer;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Repair{" +
                "statusRepair=" + statusRepair +
                ", vehicleValue=" + vehicleValue +
                ", blocksRepair=" + blocksRepair +
                ", unrepairable=" + unrepairable +
                ", motivation='" + motivation + '\'' +
                ", repairer='" + repairer + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", address=" + address +
                '}';
    }
}
