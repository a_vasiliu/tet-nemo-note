package com.doing.nemo.claims.controller;

import com.doing.nemo.claims.adapter.EmailTemplateAdapter;
import com.doing.nemo.claims.common.validation.MessageCode;
import com.doing.nemo.claims.common.validation.RequestValidator;
import com.doing.nemo.claims.controller.payload.request.EmailTemplateRequestV1;
import com.doing.nemo.claims.controller.payload.request.ListRequestV1;
import com.doing.nemo.claims.controller.payload.response.ClaimsResponsePaginationV1;
import com.doing.nemo.claims.controller.payload.response.IdResponseV1;
import com.doing.nemo.claims.controller.payload.response.EmailTemplateResponseV1;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.enumerated.ClaimsRepairEnum;
import com.doing.nemo.claims.entity.settings.EmailTemplateEntity;
import com.doing.nemo.claims.service.EmailTemplateService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/v1")
public class EmailTemplateController {

    @Autowired
    private EmailTemplateService emailTemplateService;

    @Autowired
    private EmailTemplateAdapter emailTemplateAdapter;

    @Autowired
    private RequestValidator requestValidator;

    @PostMapping(value = "/emailtemplate",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Insert Email Template", notes = "Insert email template using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = IdResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @Transactional
    public ResponseEntity<IdResponseV1> postInsertEmailTemplate(
            @ApiParam(value = "Body of the Email Template to be created", required = true)
            @RequestBody EmailTemplateRequestV1 emailTemplateRequest,
            @ApiParam(value = "TypeEnum of complaint", required = true)
            @RequestParam(value = "type_complaint") ClaimsRepairEnum typeComplaint) {

        requestValidator.validateRequest(emailTemplateRequest, MessageCode.E00X_1000);
        EmailTemplateEntity emailTemplateEntity = emailTemplateAdapter.getEmailTemplateEntity(emailTemplateRequest);
        emailTemplateEntity.setTypeComplaint(typeComplaint);
        emailTemplateEntity = emailTemplateService.insertEmailTemplate(emailTemplateEntity);
        IdResponseV1 response = new IdResponseV1(emailTemplateEntity.getId());
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @PostMapping("/emailtemplates")
    @Transactional
    @ApiOperation(value = "Insert a list of Email Templates", notes = "Insert a list of email templates using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = EmailTemplateResponseV1.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<EmailTemplateResponseV1>> insertEvents(
            @ApiParam(value = "List of the bodies of the Email Templates to be created", required = true)
            @RequestBody ListRequestV1<EmailTemplateRequestV1> emailTemplateListRequestV1,
            @ApiParam(value = "TypeEnum of complaint", required = true)
            @RequestParam(value = "type_complaint") ClaimsRepairEnum typeComplaint) {

        List<EmailTemplateResponseV1> emailTemplateResponseV1List = new ArrayList<>();
        for (EmailTemplateRequestV1 emailTemplateRequestV1 : emailTemplateListRequestV1.getData()) {
            requestValidator.validateRequest(emailTemplateRequestV1, MessageCode.E00X_1000);
            EmailTemplateEntity emailTemplateEntity = emailTemplateAdapter.getEmailTemplateEntity(emailTemplateRequestV1);
            emailTemplateEntity.setTypeComplaint(typeComplaint);
            emailTemplateEntity = emailTemplateService.insertEmailTemplate(emailTemplateEntity);
            emailTemplateResponseV1List.add(EmailTemplateAdapter.getEmailTemplateAdapter(emailTemplateEntity));
        }
        return new ResponseEntity<>(emailTemplateResponseV1List, HttpStatus.CREATED);

    }

    @PutMapping(value = "/emailtemplate/{UUID}",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Upload Email Template", notes = "Upload email template using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = EmailTemplateResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @Transactional
    public ResponseEntity<EmailTemplateResponseV1> putEmailTemplate(
            @ApiParam(value = "UUID that identifies the Email Template to be modified", required = true)
            @PathVariable(name = "UUID") UUID uuid,
            @ApiParam(value = "Updated body of the Email Template", required = true)
            @RequestBody EmailTemplateRequestV1 emailTemplateRequest) {

        requestValidator.validateRequest(emailTemplateRequest, MessageCode.E00X_1000);
        EmailTemplateEntity emailTemplateEntity = emailTemplateAdapter.getEmailTemplateEntity(emailTemplateRequest);
        emailTemplateService.updateEmailTemplate(emailTemplateEntity, uuid);
        EmailTemplateEntity emailTemplateEntity1 = emailTemplateService.getEmailTemplate(uuid);
        EmailTemplateResponseV1 response = EmailTemplateAdapter.getEmailTemplateAdapter(emailTemplateEntity1);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping(value = "/emailtemplate/{UUID}",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Recover Email Template", notes = "Returns an Email Template using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = EmailTemplateResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<EmailTemplateResponseV1> getEmailTemplate(
            @ApiParam(value = "UUID of the Email Template to be found", required = true)
            @PathVariable(name = "UUID") UUID uuid) {

        EmailTemplateEntity emailTemplateEntity = emailTemplateService.getEmailTemplate(uuid);
        EmailTemplateResponseV1 response = EmailTemplateAdapter.getEmailTemplateAdapter(emailTemplateEntity);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping(value = "/emailtemplate",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Recover Email Templates", notes = "Returns all the Email Templates")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = EmailTemplateResponseV1.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<EmailTemplateResponseV1>> getAllEmailTemplate(
            @ApiParam(value = "TypeEnum of complaint", required = true)
            @RequestParam(value = "type_complaint") ClaimsRepairEnum typeComplaint
    ) {

        List<EmailTemplateEntity> emailTemplateEntities = emailTemplateService.getAllEmailTemplate(typeComplaint);
        List<EmailTemplateResponseV1> response = EmailTemplateAdapter.getEmailTemplateListAdapter(emailTemplateEntities);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @DeleteMapping(value = "/emailtemplate/{UUID}",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Delete Email Template", notes = "Delete an Email Template using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = EmailTemplateResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @Transactional
    public ResponseEntity deleteEmailTemplate(@PathVariable(name = "UUID") UUID uuid) {

        EmailTemplateEntity emailTemplateEntity = emailTemplateService.deleteEmailTemplate(uuid);
        if (emailTemplateEntity == null)
            return new ResponseEntity<>(emailTemplateEntity, HttpStatus.OK);
        EmailTemplateResponseV1 response = EmailTemplateAdapter.getEmailTemplateAdapter(emailTemplateEntity);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PatchMapping("/emailtemplate/{UUID}/active")
    @Transactional
    @ApiOperation(value = "Patch status active Email Template", notes = "Patch status active Email Template using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = EmailTemplateResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<EmailTemplateResponseV1> patchStatusEmailTemplate(
            @PathVariable UUID UUID
    ) {
        EmailTemplateEntity emailTemplateEntity = emailTemplateService.patchStatusEmailTemplate(UUID);
        EmailTemplateResponseV1 responseV1 = EmailTemplateAdapter.getEmailTemplateAdapter(emailTemplateEntity);
        return new ResponseEntity<>(responseV1, HttpStatus.OK);
    }

    @ApiOperation(value="Email Template Pagination", notes = "It retrieves Email Template pagination")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = ClaimsResponsePaginationV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @GetMapping(value="/emailtemplate/pagination", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<ClaimsResponsePaginationV1> emailTemplatestypePagination(
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @RequestParam(value = "page_size", defaultValue = "20") Integer pageSize,
            @RequestParam(value = "type_complaint", required = false) String claimsRepairEnum,
            @RequestParam(value = "order_by", required = false) String orderBy,
            @RequestParam(value = "asc", required = false, defaultValue = "true") Boolean asc,
            @RequestParam(value = "event_type", required = false) String applicationEventTypeId,
            @RequestParam(value = "type", required = false) String type,
            @RequestParam(value = "description", required = false) String description,
            @RequestParam(value = "object", required = false) String object,
            @RequestParam(value = "heading", required = false) String heading,
            @RequestParam(value = "body", required = false) String body,
            @RequestParam(value = "foot", required = false) String foot,
            @RequestParam(value = "attach_file", required = false) Boolean attachFile,
            @RequestParam(value = "splitting_recipients_email", required = false) Boolean splittingRecipientsEmail,
            @RequestParam(value = "client_code", required = false) String clientCode,
            @RequestParam(value = "is_active", required = false) Boolean isActive,
            @RequestParam(value = "flows", required = false) String flows
            ){

        Pagination<EmailTemplateEntity> emailTemplateEntityPagination = emailTemplateService.paginationEmailTemplate(page, pageSize, flows, claimsRepairEnum, orderBy, asc, applicationEventTypeId, type, description, object, heading, body, foot, attachFile, splittingRecipientsEmail, clientCode, isActive);

        return new ResponseEntity<>(EmailTemplateAdapter.adptEmailTemplatePaginationToClaimsResponsePagination(emailTemplateEntityPagination), HttpStatus.OK);
    }
}
