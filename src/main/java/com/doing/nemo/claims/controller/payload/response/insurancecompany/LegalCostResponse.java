package com.doing.nemo.claims.controller.payload.response.insurancecompany;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.UUID;

public class LegalCostResponse implements Serializable {

    @JsonProperty("service_id")
    private String serviceId;

    @JsonProperty("service")
    private String service;

    @JsonProperty("company_id")
    private String companyId;

    @JsonProperty("company")
    private String company;

    @JsonProperty("tariff")
    private String tariff;

    @JsonProperty("tariff_id")
    private String tariffId;

    @JsonProperty("tariff_code")
    private String tariffCode;

    @JsonProperty("policy_number")
    private String policyNumber;

    @JsonProperty("start_date")
    private String startDate;

    @JsonProperty("end_date")
    private String endDate;

    @JsonProperty("insurance_company_id")
    private UUID insuranceCompanyId;

    @JsonProperty("insurance_policy_id")
    private UUID insurancePolicyId;

    public UUID getInsurancePolicyId() {
        return insurancePolicyId;
    }

    public void setInsurancePolicyId(UUID insurancePolicyId) {
        this.insurancePolicyId = insurancePolicyId;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }


    public String getTariff() {
        return tariff;
    }

    public String getTariffId() {
        return tariffId;
    }

    public void setTariffId(String tariffId) {
        this.tariffId = tariffId;
    }

    public void setTariff(String tariff) {
        this.tariff = tariff;
    }

    public String getTariffCode() {
        return tariffCode;
    }

    public void setTariffCode(String tariffCode) {
        this.tariffCode = tariffCode;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public UUID getInsuranceCompanyId() {
        return insuranceCompanyId;
    }

    public void setInsuranceCompanyId(UUID insuranceCompanyId) {
        this.insuranceCompanyId = insuranceCompanyId;
    }
}

