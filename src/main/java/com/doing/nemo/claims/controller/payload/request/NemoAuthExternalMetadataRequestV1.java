package com.doing.nemo.claims.controller.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class NemoAuthExternalMetadataRequestV1 {
    @JsonProperty("entruster_type")
    private String entrusterType;
    @JsonProperty("entruster_id")
    private String entrusterId;
    @JsonProperty("redirect_url")
    private String redirectUrl;
    @JsonProperty("email")
    private String email;
    @JsonProperty("claim_practice_id")
    private String claimPracticeId;
    @JsonProperty("claim_id")
    private String claimId;

    public String getEntrusterType() {
        return entrusterType;
    }

    public void setEntrusterType(String entrusterType) {
        this.entrusterType = entrusterType;
    }

    public String getEntrusterId() {
        return entrusterId;
    }

    public void setEntrusterId(String entrusterId) {
        this.entrusterId = entrusterId;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getClaimPracticeId() {
        return claimPracticeId;
    }

    public void setClaimPracticeId(String claimPracticeId) {
        this.claimPracticeId = claimPracticeId;
    }

    public String getClaimId() {
        return claimId;
    }

    public void setClaimId(String claimId) {
        this.claimId = claimId;
    }
}
