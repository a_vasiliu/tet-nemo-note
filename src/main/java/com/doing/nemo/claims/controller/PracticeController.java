package com.doing.nemo.claims.controller;

import com.doing.nemo.claims.adapter.ClaimsAdapter;
import com.doing.nemo.claims.adapter.PracticeAdapter;
import com.doing.nemo.claims.adapter.TheftClaimsAdapter;
import com.doing.nemo.claims.common.validation.MessageCode;
import com.doing.nemo.claims.common.validation.RequestValidator;
import com.doing.nemo.claims.controller.payload.request.ListRequestV1;
import com.doing.nemo.claims.controller.payload.request.PlatesStatusRequestV1;
import com.doing.nemo.claims.controller.payload.request.claims.TheftRequestV1;
import com.doing.nemo.claims.controller.payload.request.practice.PracticeRequestV1;
import com.doing.nemo.claims.controller.payload.response.ClaimsResponseV1;
import com.doing.nemo.claims.controller.payload.response.PaginationResponseV1;
import com.doing.nemo.claims.controller.payload.response.PracticeResponseIdV1;
import com.doing.nemo.claims.controller.payload.response.practice.PracticeResponseV1;
import com.doing.nemo.claims.entity.jsonb.Theft;
import com.doing.nemo.claims.entity.settings.PracticeEntity;
import com.doing.nemo.claims.service.LockService;
import com.doing.nemo.claims.service.PracticeService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/v1")
@Api("Practice")
public class PracticeController {

    @Autowired
    private PracticeService practiceService;

    @Autowired
    private RequestValidator requestValidator;

    @Autowired
    private LockService lockService;

    @PostMapping("/practice")
    @ApiOperation(value = "Insert Practice", notes = "Insert Practice using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = PracticeResponseIdV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<PracticeResponseIdV1> postPractice(
            @ApiParam(value = "Body of the Practice to be created", required = true)
            @RequestBody PracticeRequestV1 practiceRequestV1,
            @RequestHeader(name = "nemo-user-id") String userId
    ) throws IOException {
        requestValidator.validateRequest(practiceRequestV1, MessageCode.E00X_1000);
        PracticeEntity practiceEntity = PracticeAdapter.adptPracticeRequestToPracticeEntity(practiceRequestV1);
        practiceEntity.setCreatedBy(userId);
        practiceEntity = practiceService.insertPractice(practiceEntity);
        practiceEntity = practiceService.selectPractice(practiceEntity.getId());
        PracticeResponseIdV1 practiceResponseIdV1 = new PracticeResponseIdV1(practiceEntity.getId(), practiceEntity.getPracticeId());
        return new ResponseEntity(practiceResponseIdV1, HttpStatus.CREATED);
    }

    @PostMapping("/practices")
    @Transactional
    @ApiOperation(value = "Insert More Practices", notes = "Insert Practices using info passed in the bodies")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = PracticeResponseV1.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<PracticeResponseV1>> postPractices(
            @ApiParam(value = "Bodies of the Practices to be created", required = true)
            @RequestBody ListRequestV1<PracticeRequestV1> practiceListRequestV1
    ) throws IOException {
        List<PracticeResponseV1> practiceResponseV1List = new ArrayList<>();
        for (PracticeRequestV1 practiceRequestV1 : practiceListRequestV1.getData()) {
            requestValidator.validateRequest(practiceRequestV1, MessageCode.E00X_1000);
            PracticeEntity practiceEntity = PracticeAdapter.adptPracticeRequestToPracticeEntity(practiceRequestV1);
            practiceEntity = practiceService.insertPractice(practiceEntity);
            PracticeResponseV1 practiceResponseV1 = PracticeAdapter.adptPracticeEntityToPracticeResponseV1(practiceEntity);
            practiceResponseV1List.add(practiceResponseV1);
        }
        return new ResponseEntity(practiceResponseV1List, HttpStatus.CREATED);
    }

    @PutMapping("/practice/{UUID}")
    @Transactional
    @ApiOperation(value = "Upload Practice", notes = "Upload Practice using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = PracticeResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<PracticeResponseV1> putPractice(
            @ApiParam(value = "UUID that identifies the Practice to be modified", required = true)
            @PathVariable(name = "UUID") String id,
            @ApiParam(value = "Updated body of the Practice", required = true)
            @RequestBody PracticeRequestV1 practiceRequestV1,
            @RequestHeader(name = "nemo-user-id") String userId

    ) throws IOException {
        lockService.checkLock(userId, id);
        requestValidator.validateRequest(practiceRequestV1, MessageCode.E00X_1000);
        PracticeEntity practiceEntity = practiceService.selectPractice(UUID.fromString(id));
        PracticeEntity practiceEntityNew = PracticeAdapter.adptPracticeRequestToPracticeEntity(practiceRequestV1);
        practiceEntityNew.setId(practiceEntity.getId());
        practiceEntityNew = practiceService.updatePractice(practiceEntityNew);
        PracticeResponseV1 practiceResponseV1 = PracticeAdapter.adptPracticeEntityToPracticeResponseV1(practiceEntityNew);
        return new ResponseEntity<>(practiceResponseV1, HttpStatus.OK);
    }

    @GetMapping("/practice/{UUID}")
    @Transactional
    @ApiOperation(value = "Recover Practice", notes = "Return a Practice using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = PracticeResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<PracticeResponseV1> getPractice(
            @ApiParam(value = "UUID of the Practice to be found", required = true)
            @PathVariable(name = "UUID") UUID id
    ) {
        PracticeEntity practiceEntity = practiceService.selectPractice(id);
        PracticeResponseV1 practiceResponseV1 = PracticeAdapter.adptPracticeEntityToPracticeResponseV1(practiceEntity);
        return new ResponseEntity<>(practiceResponseV1, HttpStatus.OK);
    }

    @GetMapping("/practice")
    @Transactional
    @ApiOperation(value = "Recover All Practices", notes = "Returns all Practices")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = PracticeResponseV1.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<PracticeResponseV1>> getAllPractice() {
        List<PracticeEntity> practiceEntityList = practiceService.selectAllPractice();
        List<PracticeResponseV1> practiceResponseV1List = PracticeAdapter.adptPracticeEntityToPracticeResponseV1List(practiceEntityList);
        return new ResponseEntity(practiceResponseV1List, HttpStatus.OK);
    }

    @DeleteMapping("/practice/{UUID}")
    @Transactional
    @ApiOperation(value = "Delete Practice", notes = "Delete a Practice using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = PracticeResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<PracticeResponseV1> deletePractice(
            @ApiParam(value = "UUID of the Practice to be deleted", required = true)
            @PathVariable(name = "UUID") UUID id
    ) {
        PracticeResponseV1 practiceResponseV1 = practiceService.deletePractice(id);
        return new ResponseEntity<>(practiceResponseV1, HttpStatus.OK);
    }

    @ApiOperation(value="Search Practice", notes = "It retrieves practice data through one of more parameters")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = PaginationResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @GetMapping(value="/practice/pagination", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<PaginationResponseV1<PracticeResponseV1>> searchPractice(
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @RequestParam(value = "page_size", defaultValue = "20") Integer pageSize,
            @RequestParam(value = "practice_id", required = false) Long practiceId,
            @RequestParam(value = "practice_type", required = false) String practiceType,
            @RequestParam(value = "license_plate", required = false) String licensePlate,
            @RequestParam(value = "contract_id", required = false) String contractId,
            @RequestParam(value = "date_created_from", required = false) String dateCreatedFrom,
            @RequestParam(value = "date_created_to", required = false) String dateCreatedTo,
            @RequestParam(value = "status", required = false) String status,
            @RequestParam(value = "order_by", required = false) String orderBy,
            @RequestParam(value = "asc", required = false, defaultValue = "true") Boolean asc,
            @RequestParam(value = "fleet_vehicle_id", required = false) String fleetVehicleId
            ){

        List<PracticeEntity> listPractice = practiceService.findPracticesFiltered(page,pageSize, practiceId, practiceType,licensePlate, contractId, status, dateCreatedFrom, dateCreatedTo , orderBy, asc, fleetVehicleId);
        Long itemCount = practiceService.countPracticeFiltered(practiceId, practiceType,licensePlate, contractId, status, dateCreatedFrom, dateCreatedTo, fleetVehicleId );

        PaginationResponseV1<PracticeResponseV1> paginationResponseV1 = PracticeAdapter.adptPagination(listPractice, page, pageSize, itemCount);

        return new ResponseEntity<>(paginationResponseV1, HttpStatus.OK);
    }


    /*CAMBIO TIPO PRATICA*/
    @PatchMapping(value = "/practice/type/{UUID}",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Upload Practice TypeEnum", notes = "Update the practice type using info passed in the request")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = PracticeResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<PracticeResponseIdV1> patchPracticeType(
            @ApiParam(value = "UUID of the practice", required = true)
            @PathVariable(name = "UUID") String practiceId,
            @ApiParam(value = "New type of the practice", required = true)
            @RequestBody PracticeRequestV1 practiceRequestV1,
            @RequestHeader(name = "nemo-user-id") String userId
    ) throws IOException {
        lockService.checkLock(userId, practiceId);
        PracticeEntity practiceEntity = practiceService.patchTypePractice(UUID.fromString(practiceId), practiceRequestV1.getPracticeType(), userId);
        PracticeResponseIdV1 practiceResponseIdV1 = new PracticeResponseIdV1(practiceEntity.getId(), practiceEntity.getPracticeId());
        return new ResponseEntity<>(practiceResponseIdV1, HttpStatus.OK);
    }


    /*ATTACCO PRATICA AL CLAIM*/
    @PatchMapping(value = "/practice/claim/{UUID}",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Upload Practice Claim", notes = "Update the practice claim using info passed in the request")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = PracticeResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<PracticeResponseV1> patchPracticeClaim(
            @ApiParam(value = "UUID of the practice", required = true)
            @PathVariable(name = "UUID") String practiceId,
            @ApiParam(value = "New claim of the practice", required = true)
            @RequestBody PracticeRequestV1 practiceRequestV1,
            @RequestHeader(name = "nemo-user-id") String userId
    ) throws IOException {
        lockService.checkLock(userId, practiceId);
        PracticeResponseV1 practiceResponseV1 = PracticeAdapter.adptPracticeEntityToPracticeResponseV1(practiceService.patchClaimPractice(UUID.fromString(practiceId), practiceRequestV1.getClaimsId()));
        return new ResponseEntity<>(practiceResponseV1, HttpStatus.OK);
    }


    /*CAMBIO STATO PRATICA*/
    @PatchMapping(value = "/practice/status/{UUID}",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Upload Practice Status", notes = "Update the practice status using info passed in the request")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = PracticeResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<PracticeResponseV1> patchPracticeStatus(
            @ApiParam(value = "UUID of the practice", required = true)
            @PathVariable(name = "UUID") String practiceId,
            @ApiParam(value = "New status of the practice", required = true)
            @RequestBody PracticeRequestV1 practiceRequestV1,
            @RequestHeader(name = "nemo-user-id") String userId

    ) {
        PracticeResponseV1 practiceResponseV1 = PracticeAdapter.adptPracticeEntityToPracticeResponseV1(practiceService.patchStatusPractice(UUID.fromString(practiceId),practiceRequestV1.getStatus()));
        return new ResponseEntity<>(practiceResponseV1, HttpStatus.OK);
    }

    /*CANCELLA PRATICHE VECCHIE*/
    @GetMapping(value = "/practice/delete/olddocument",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Upload Practice Status", notes = "Update the practice status using info passed in the request")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = PracticeResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<PracticeResponseV1> oldPractice(

    ) {

        return new ResponseEntity<>(HttpStatus.OK);
    }

    //REFACTOR
    @PostMapping("/claims/theft/cron/practice")
    @ApiOperation(value = "Create loss possession practice for thefts", notes = "Create loss possession practice for thefts")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })

    public void createLossPossessionForTheft() throws IOException {
        practiceService.lossPossessionPractice();
    }

    //REFACTOR
    @PatchMapping("/claims/theft/finding/{CLAIMS_ID}")
    @Transactional
    @ApiOperation(value = "Change type of Thefts", notes = "Change type of Theft into theft and finding using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ClaimsResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ClaimsResponseV1> patchTheftAndFinding(
            @ApiParam(value = "UUID of the Claims to which to patch type", required = true)
            @PathVariable(name = "CLAIMS_ID") String claimsId,
            @RequestHeader(name = "nemo-user-id") String userId,
            @RequestHeader(name = "nemo-user-first-name") String userName,
            @RequestHeader(name = "nemo-user-last-name") String userLastName,
            @RequestBody TheftRequestV1 theftRequestV1
    ) {
        Theft theft = TheftClaimsAdapter.adptTheftRequestV1ToTheft(theftRequestV1);
        ClaimsResponseV1 responseV1 = ClaimsAdapter.adptClaimsToClaimsResponse(practiceService.patchToTheftAndFinding(claimsId,userId, userName+" "+userLastName, theft));
        return new ResponseEntity<>(responseV1, HttpStatus.OK);
    }

    @PostMapping("/practice/out/fleet")
    @ApiOperation(value = "Route to put possession lost car", notes = "Route to put possession lost car out of ald fleet")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<HttpStatus> postPutOfFleet(
            @ApiParam(value = "Request of the plates with stauts to put out of fleet", required = true)
            @RequestBody PlatesStatusRequestV1 outOfFleetReques
    ) {
        practiceService.closeLossPossessionPracticeMarsnova(outOfFleetReques);
        return new ResponseEntity(HttpStatus.OK);
    }


}
