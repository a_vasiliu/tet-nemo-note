package com.doing.nemo.claims.controller.payload.request.driver;

import com.doing.nemo.claims.entity.enumerated.DamagedEnum.DriverEnum.DriverCategoryEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Date;

public class DrivingLicenseRequest implements Serializable {

    @JsonProperty("number")
    private String number;

    @JsonProperty("issuing_country")
    private String issuingCountry;

    @JsonProperty("category")
    private DriverCategoryEnum category;

    @JsonProperty("valid_from")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date validFrom;

    @JsonProperty("valid_to")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date validTo;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getIssuingCountry() {
        return issuingCountry;
    }

    public void setIssuingCountry(String issuingCountry) {
        this.issuingCountry = issuingCountry;
    }

    public Date getValidFrom() {
        if(validFrom == null){
            return null;
        }
        return (Date)validFrom.clone();
    }

    public void setValidFrom(Date validFrom) {
        if(validFrom != null)
        {
            this.validFrom = (Date)validFrom.clone();
        } else {
            this.validFrom = null;
        }
    }

    public Date getValidTo() {
        if(validTo == null){
            return null;
        }
        return (Date)validTo.clone();
    }

    public void setValidTo(Date validTo) {
        if(validTo != null)
        {
            this.validTo = (Date)validTo.clone();
        } else {
            this.validTo = null;
        }
    }

    public DriverCategoryEnum getCategory() {
        return category;
    }

    public void setCategory(DriverCategoryEnum category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "DrivingLicense{" +
                "number='" + number + '\'' +
                ", issuingCountry='" + issuingCountry + '\'' +
                ", category=" + category +
                ", validFrom='" + validFrom + '\'' +
                ", validTo='" + validTo + '\'' +
                '}';
    }
}
