package com.doing.nemo.claims.controller.payload.response.damaged;

import com.doing.nemo.claims.controller.payload.response.insurancecompany.*;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class InsuranceCompanyResponse implements Serializable {

    @JsonProperty("is_card")
    private Boolean isCard = false;

    /////////////////////////////////////////////////////////
    //da eliminare perchè sarebbe company
    //@JsonProperty("denomination")
    //private String denomination;
    /////////////////////////////////////////////////////////

    @JsonProperty("id")
    private String id;

    @JsonProperty("tpl")
    private TplResponse tpl;

    @JsonProperty("theft")
    private TheftResponse theft;

    @JsonProperty("material_damage")
    private MaterialDamageResponse materialDamage;

    @JsonProperty("pai")
    private PaiResponse pai;

    @JsonProperty("legal_cost")
    private LegalCostResponse legalCost;

    @JsonProperty("kasko")
    private KaskoResponse kasko;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public TplResponse getTpl() {
        return tpl;
    }

    public void setTpl(TplResponse tpl) {
        this.tpl = tpl;
    }

    public TheftResponse getTheft() {
        return theft;
    }

    public void setTheft(TheftResponse theft) {
        this.theft = theft;
    }

    public MaterialDamageResponse getMaterialDamage() {
        return materialDamage;
    }

    public void setMaterialDamage(MaterialDamageResponse materialDamage) {
        this.materialDamage = materialDamage;
    }

    public PaiResponse getPai() {
        return pai;
    }

    public void setPai(PaiResponse pai) {
        this.pai = pai;
    }

    public LegalCostResponse getLegalCost() {
        return legalCost;
    }

    public void setLegalCost(LegalCostResponse legalCost) {
        this.legalCost = legalCost;
    }

    @JsonIgnore
    public Boolean getCard() {
        return isCard;
    }

    public void setCard(Boolean card) {
        isCard = card;
    }

    public KaskoResponse getKasko() {
        return kasko;
    }

    public void setKasko(KaskoResponse kasko) {
        this.kasko = kasko;
    }
}
