package com.doing.nemo.claims.controller;

import com.doing.nemo.claims.adapter.ClaimsAdapter;
import com.doing.nemo.claims.adapter.InsuranceCompanyAdapter;
import com.doing.nemo.claims.command.*;
import com.doing.nemo.claims.common.commandBus.Command;
import com.doing.nemo.claims.common.commandBus.CommandBus;
import com.doing.nemo.claims.controller.payload.request.*;
import com.doing.nemo.claims.controller.payload.response.ClaimsExternalResponse;
import com.doing.nemo.claims.controller.payload.response.ClaimsResponseIdV1;
import com.doing.nemo.claims.controller.payload.response.ClaimsResponsePaginationV1;
import com.doing.nemo.claims.controller.payload.response.InsuranceCompanyExternalResponseV1;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.settings.InsuranceCompanyEntity;
import com.doing.nemo.claims.repository.ClaimsRepository;
import com.doing.nemo.claims.service.*;
import com.doing.nemo.claims.service.impl.EnjoyServiceImpl;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/v1")
@Api("Enjoy")
public class EnjoyClaimsManagerController {
    private static Logger LOGGER = LoggerFactory.getLogger(EnjoyClaimsManagerController.class);

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    private CommandBus commandBus;

    @Autowired
    private ClaimsService claimsService;

    @Autowired
    private EnjoyService enjoyService;

    @Autowired
    private ESBService esbService;




    @PostMapping(value = "/claims/enjoy",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Insert a new claims by external manager", notes = "Insert a new claims by external manager")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ClaimsResponseIdV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ClaimsResponseIdV1> postClaimsExternalManager(
            @ApiParam(value = "Body of the Claims to be created", required = true)
            @RequestBody ClaimsInsertEnjoyRequest claimsInsertEnjoyRequest,
            @RequestHeader(name = "nemo-user-first-name") String userName,
            @RequestHeader(name = "nemo-user-last-name") String lastName,
            @RequestHeader(name = "nemo-user-id") String userId) throws IOException {
        LOGGER.info("[ENJOY] INIZIO API INSERIMENTO - postClaimsExternalManager");
        String claimsId = UUID.randomUUID().toString();
        LOGGER.info("[ENJOY] claimsId " + claimsId);
        String json = objectMapper.writeValueAsString(claimsInsertEnjoyRequest);
        LOGGER.error("[ENJOY] JSON = " + json);

       Command claimsInsertCommand = new ClaimsInsertEnjoyCommand(
                claimsId,
                claimsInsertEnjoyRequest.getComplaint(),
                claimsInsertEnjoyRequest.getWithCounterparty(),
                claimsInsertEnjoyRequest.getCounterparty(),
                claimsInsertEnjoyRequest.getDeponent(),
                claimsInsertEnjoyRequest.getWounded(),
                claimsInsertEnjoyRequest.getDamaged(),
                claimsInsertEnjoyRequest.getTheftRequest(),
                claimsInsertEnjoyRequest.getIdSaleforce(),
                claimsInsertEnjoyRequest.getCaiDetails(),
                userName +" " + lastName,
                userId,
               claimsInsertEnjoyRequest.getCompleteDocumentation()

        );

        commandBus.execute(claimsInsertCommand);

       ClaimsEntity claimsEntity = claimsService.findById(claimsId);

        return new ResponseEntity<>(ClaimsAdapter.adptIdToIdResponse(claimsEntity.getId(), claimsEntity.getPracticeId()), HttpStatus.OK);

    }




    @GetMapping(value = "/claims/enjoy/contract/{contractOrPlate}",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Insert a new claims by external manager", notes = "Insert a new claims by external manager")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ClaimsResponseIdV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity postClaimsExternalManager(
            @RequestHeader(name = "nemo-user-first-name") String userName,
            @RequestHeader(name = "nemo-user-last-name") String lastName,
            @RequestHeader(name = "nemo-user-id") String userId,
            @PathVariable String contractOrPlate,
            @RequestHeader(value = "nemo-contract-date", required = false, defaultValue = "") String date
            ) throws IOException, ParseException {



             return new ResponseEntity(  esbService.getContractInfoEnjoy(contractOrPlate, date), HttpStatus.OK);
            }

}
