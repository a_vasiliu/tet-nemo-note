package com.doing.nemo.claims.controller.payload.request.claims;

import com.doing.nemo.claims.controller.payload.request.counterparty.DriverRequest;
import com.doing.nemo.claims.controller.payload.request.counterparty.ImpactPointRequest;
import com.doing.nemo.claims.controller.payload.request.counterparty.VehicleRequest;
import com.doing.nemo.claims.controller.payload.request.damaged.*;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class DamagedRequest implements Serializable {

    @JsonProperty("contract")
    private ContractRequest contract;

    @JsonProperty("customer")
    private CustomerRequest customer;

    @JsonProperty("insurance_company")
    private InsuranceCompanyRequest insuranceCompany;

    @JsonProperty("driver")
    private DriverRequest driver;

    @JsonProperty("vehicle")
    private VehicleRequest vehicle;

    @JsonProperty("fleet_managers")
    private List<FleetManagerRequest> fleetManagerList;

    @JsonProperty("is_cai_signed")
    private Boolean isCaiSigned;

    @JsonProperty("impact_point")
    private ImpactPointRequest impactPoint;

    @JsonProperty("additional_costs")
    private List<AdditionalCostsRequest> additionalCosts;

    @JsonProperty("anti_theft_service")
    private AntiTheftServiceRequest antiTheftServiceRequest;

    public ContractRequest getContract() {
        return contract;
    }

    public void setContract(ContractRequest contract) {
        this.contract = contract;
    }

    public CustomerRequest getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerRequest customer) {
        this.customer = customer;
    }

    public InsuranceCompanyRequest getInsuranceCompany() {
        return insuranceCompany;
    }

    public void setInsuranceCompany(InsuranceCompanyRequest insuranceCompany) {
        this.insuranceCompany = insuranceCompany;
    }

    public DriverRequest getDriver() {
        return driver;
    }

    public void setDriver(DriverRequest driver) {
        this.driver = driver;
    }

    public VehicleRequest getVehicle() {
        return vehicle;
    }

    public void setVehicle(VehicleRequest vehicle) {
        this.vehicle = vehicle;
    }

    public List<FleetManagerRequest> getFleetManagerList() {
        if(fleetManagerList == null){
            return null;
        }
        return new ArrayList<>(fleetManagerList);
    }

    public void setFleetManagerList(List<FleetManagerRequest> fleetManagerList) {
        if(fleetManagerList != null)
        {
            this.fleetManagerList = new ArrayList<>(fleetManagerList);
        } else {
            this.fleetManagerList = null;
        }
    }

    public Boolean getCaiSigned() {
        return isCaiSigned;
    }

    public void setCaiSigned(Boolean caiSigned) {
        this.isCaiSigned = caiSigned;
    }

    public ImpactPointRequest getImpactPoint() {
        return impactPoint;
    }

    public void setImpactPoint(ImpactPointRequest impactPoint) {
        this.impactPoint = impactPoint;
    }

    public List<AdditionalCostsRequest> getAdditionalCosts() {
        if(additionalCosts == null){
            return null;
        }
        return new ArrayList<>(additionalCosts);
    }

    public void setAdditionalCosts(List<AdditionalCostsRequest> additionalCosts) {
        if(additionalCosts != null)
        {
            this.additionalCosts =new ArrayList<>(additionalCosts);
        } else {
            this.additionalCosts = null;
        }
    }

    public AntiTheftServiceRequest getAntiTheftServiceRequest() {
        return antiTheftServiceRequest;
    }

    public void setAntiTheftServiceRequest(AntiTheftServiceRequest antiTheftServiceRequest) {
        this.antiTheftServiceRequest = antiTheftServiceRequest;
    }

    @Override
    public String toString() {
        return "DamagedRequest{" +
                "contract=" + contract +
                ", customer=" + customer +
                ", insuranceCompany=" + insuranceCompany +
                ", driver=" + driver +
                ", vehicle=" + vehicle +
                ", fleetManagerList=" + fleetManagerList +
                ", isCaiSigned=" + isCaiSigned +
                ", impactPoint=" + impactPoint +
                ", additionalCosts=" + additionalCosts +
                '}';
    }

}
