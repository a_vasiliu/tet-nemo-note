package com.doing.nemo.claims.controller.payload.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.UUID;

public class MotivationTypeResponseV1 implements Serializable {

    @JsonProperty("id")
    private UUID id;

    @JsonProperty("motivation_type_id")
    private Long motivationTypeId;

    @JsonProperty("description")
    private String description;

    @JsonProperty("order")
    private Integer order;

    @JsonProperty("is_active")
    private Boolean isActive;

    public MotivationTypeResponseV1(UUID id, Long motivationTypeId, String description, Integer order, Boolean isActive) {
        this.id = id;
        this.motivationTypeId = motivationTypeId;
        this.description = description;
        this.order = order;
        this.isActive = isActive;
    }

    public MotivationTypeResponseV1() {
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Long getMotivationTypeId() {
        return motivationTypeId;
    }

    public void setMotivationTypeId(Long motivationTypeId) {
        this.motivationTypeId = motivationTypeId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    @JsonIgnore
    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }
}
