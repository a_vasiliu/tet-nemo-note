package com.doing.nemo.claims.controller.payload.response.claims;

import com.doing.nemo.claims.controller.payload.response.refund.SplitResponse;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RefundResponse implements Serializable {
    @JsonProperty("po_sum")
    private Double poSum;

    @JsonProperty("wreck")
    private Boolean wreck;

    @JsonProperty("wreck_value_pre")
    private Double wreckValuePre;

    @JsonProperty("wreck_value_post")
    private Double wreckValuePost;

    @JsonProperty("NBV")
    private String NBV;

    @JsonProperty("franchise_amount_fcm")
    private Double franchiseAmountFcm;

    @JsonProperty("total_refund_expected")
    private Double totalRefundExpected;

    @JsonProperty("total_liquidation_received")
    private Double totalLiquidationReceived;

    @JsonProperty("definition_date")
    private String definitionDate;

    @JsonProperty("blu_eurotax")
    private String bluEurotax;

    @JsonProperty("amount_to_be_debited")
    private Double amountToBeDebited;

    @JsonProperty("delta_debit_date")
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    private Date deltaDebitDate;

    @JsonProperty("split_list")
    private List<SplitResponse> splitList;

    public Double getPoSum() {
        return poSum;
    }

    public void setPoSum(Double poSum) {
        this.poSum = poSum;
    }

    public Boolean getWreck() {
        return wreck;
    }

    public void setWreck(Boolean wreck) {
        this.wreck = wreck;
    }

    public Double getWreckValuePre() {
        return wreckValuePre;
    }

    public void setWreckValuePre(Double wreckValuePre) {
        this.wreckValuePre = wreckValuePre;
    }

    public Double getWreckValuePost() {
        return wreckValuePost;
    }

    public void setWreckValuePost(Double wreckValuePost) {
        this.wreckValuePost = wreckValuePost;
    }

    public String getNBV() {
        return NBV;
    }

    public void setNBV(String NBV) {
        this.NBV = NBV;
    }

    public Double getFranchiseAmountFcm() {
        return franchiseAmountFcm;
    }

    public void setFranchiseAmountFcm(Double franchiseAmountFcm) {
        this.franchiseAmountFcm = franchiseAmountFcm;
    }

    public Double getTotalRefundExpected() {
        return totalRefundExpected;
    }

    public void setTotalRefundExpected(Double totalRefundExpected) {
        this.totalRefundExpected = totalRefundExpected;
    }

    public Double getTotalLiquidationReceived() {
        return totalLiquidationReceived;
    }

    public void setTotalLiquidationReceived(Double totalLiquidationReceived) {
        this.totalLiquidationReceived = totalLiquidationReceived;
    }

    public String getDefinitionDate() {
        return definitionDate;
    }

    public void setDefinitionDate(String definitionDate) {
        this.definitionDate = definitionDate;
    }

    public List<SplitResponse> getSplitList() {
        if(splitList == null){
            return null;
        }
        return new ArrayList<>(splitList);
    }

    public void setSplitList(List<SplitResponse> splitList) {
        if(splitList != null)
        {
            this.splitList = new ArrayList<>(splitList);
        } else {
            this.splitList = null;
        }
    }

    public String getBluEurotax() {
        return bluEurotax;
    }

    public void setBluEurotax(String bluEurotax) {
        this.bluEurotax = bluEurotax;
    }

    public Double getAmountToBeDebited() {
        return amountToBeDebited;
    }

    public void setAmountToBeDebited(Double amountToBeDebited) {
        this.amountToBeDebited = amountToBeDebited;
    }

    public Date getDeltaDebitDate() {
        if(deltaDebitDate == null){
            return null;
        }
        return (Date)deltaDebitDate.clone();
    }

    public void setDeltaDebitDate(Date deltaDebitDate) {
        if(deltaDebitDate != null)
        {
            this.deltaDebitDate = (Date)deltaDebitDate.clone();
        } else {
            this.deltaDebitDate = null;
        }
    }
}
