package com.doing.nemo.claims.controller.payload.response;

import com.doing.nemo.claims.entity.enumerated.ClaimsRepairEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.UUID;

public class MotivationResponseV1 implements Serializable {

    @JsonProperty("id")
    private UUID id;

    @JsonProperty("answer_type")
    private String answerType;

    @JsonProperty("description")
    private String description;

    @JsonProperty("is_active")
    private Boolean isActive;

    @JsonIgnore
    @JsonProperty("type_complaint")
    private ClaimsRepairEnum typeComplaint;

    public MotivationResponseV1() {

    }

    public MotivationResponseV1(UUID id, String answerType, String description, Boolean isActive, ClaimsRepairEnum typeComplaint) {
        this.id = id;
        this.answerType = answerType;
        this.description = description;
        this.isActive = isActive;
        this.typeComplaint = typeComplaint;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getAnswerType() {
        return answerType;
    }

    public void setAnswerType(String answerType) {
        this.answerType = answerType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @JsonIgnore
    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }


    @JsonIgnore
    public ClaimsRepairEnum getTypeComplaint() {
        return typeComplaint;
    }

    public void setTypeComplaint(ClaimsRepairEnum typeComplaint) {
        this.typeComplaint = typeComplaint;
    }

    @Override
    public String toString() {
        return "MotivationResponseV1{" +
                "id=" + id +
                ", answerType='" + answerType + '\'' +
                ", description='" + description + '\'' +
                ", isActive=" + isActive +
                '}';
    }
}

