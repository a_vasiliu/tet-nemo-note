package com.doing.nemo.claims.controller.payload.response.counterparty;

import com.doing.nemo.claims.controller.payload.response.MotivationResponseV1;
import com.doing.nemo.claims.entity.enumerated.RepairEnum.RepairContactResultEnum;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class LastContactResponse implements Serializable {

    @JsonProperty("last_call")
    private String lastCall;

    @JsonProperty("result")
    private RepairContactResultEnum result;

    @JsonProperty("recall")
    private String recall;

    @JsonProperty("note")
    private String note;

    @JsonProperty("motivation")
    private MotivationResponseV1 motivation;

    public LastContactResponse() {
    }

    public LastContactResponse(String lastCall, RepairContactResultEnum result, String recall, String note, MotivationResponseV1 motivation) {
        this.lastCall = lastCall;
        this.result = result;
        this.recall = recall;
        this.note = note;
        this.motivation = motivation;
    }

    public String getLastCall() {
        return lastCall;
    }

    public void setLastCall(String lastCall) {
        this.lastCall = lastCall;
    }

    public RepairContactResultEnum getResult() {
        return result;
    }

    public void setResult(RepairContactResultEnum result) {
        this.result = result;
    }

    public String getRecall() {
        return recall;
    }

    public void setRecall(String recall) {
        this.recall = recall;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public MotivationResponseV1 getMotivation() {
        return motivation;
    }

    public void setMotivation(MotivationResponseV1 motivation) {
        this.motivation = motivation;
    }

    @Override
    public String toString() {
        return "LastContactResponse{" +
                "lastCall=" + lastCall +
                ", result=" + result +
                ", recall=" + recall +
                ", note='" + note + '\'' +
                ", motivation=" + motivation +
                '}';
    }
}
