package com.doing.nemo.claims.controller.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class InsuranceCompanyCounterpartyRequest implements Serializable {
    @JsonProperty("entity")
    private InsuranceCompanyRequestV1 entity;

    @JsonProperty("name")
    private String name;

    public InsuranceCompanyCounterpartyRequest() {
    }

    public InsuranceCompanyCounterpartyRequest(InsuranceCompanyRequestV1 entity, String name) {
        this.entity = entity;
        this.name = name;
    }

    public InsuranceCompanyRequestV1 getEntity() {
        return entity;
    }

    public void setEntity(InsuranceCompanyRequestV1 entity) {
        this.entity = entity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "InsuranceCompanyCounterpartyRequest{" +
                "entity=" + entity +
                ", name='" + name + '\'' +
                '}';
    }
}
