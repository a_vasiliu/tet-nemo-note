package com.doing.nemo.claims.controller.payload.request;

import com.doing.nemo.claims.entity.enumerated.DamagedEnum.VehicleEnum.VehicleTypeEnum;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AutomaticAffiliationRuleInspectorateRequestV1 implements Serializable {

    @JsonProperty("selection_name")
    private String selectionName = "";

    @JsonProperty("monthly_volume")
    private Integer monthlyVolume;

    @JsonProperty("current_volume")
    private Integer currentVolume;

    @JsonProperty("detail")
    private Integer detail;

    @JsonProperty("annotations")
    private String annotations = "";

    @JsonProperty("select_only_complaints_without_ctp")
    private Boolean selectOnlyComplaintsWithoutCTP;

    @JsonProperty("foreign_country_claim")
    private Boolean foreignCountryClaim;

    @JsonProperty("counterpart_type")
    private VehicleTypeEnum counterPartType;

    @JsonProperty("claim_with_injured")
    private Boolean claimWithInjured;

    @JsonProperty("damage_can_not_be_repaired")
    private Boolean damageCanNotBeRepaired;

    @JsonProperty("min_import")
    private Double minImport;

    @JsonProperty("max_import")
    private Double maxImport;

    @JsonProperty("claims_type_entity_list")
    private List<ClaimsTypeForRuleRequestV1> claimsTypeEntityList;

    @JsonProperty("damaged_insurance_company_list")
    private List<InsuranceCompanyRequestV1> damagedInsuranceCompanyList;

    @JsonProperty("counter_parter_insurance_company_list")
    private List<InsuranceCompanyRequestV1> counterParterInsuranceCompanyList;

    public String getSelectionName() {
        return selectionName;
    }

    public void setSelectionName(String selectionName) {
        this.selectionName = selectionName;
    }

    public Integer getMonthlyVolume() {
        return monthlyVolume;
    }

    public void setMonthlyVolume(Integer monthlyVolume) {
        this.monthlyVolume = monthlyVolume;
    }

    public Integer getCurrentVolume() {
        return currentVolume;
    }

    public void setCurrentVolume(Integer currentVolume) {
        this.currentVolume = currentVolume;
    }

    public Integer getDetail() {
        return detail;
    }

    public void setDetail(Integer detail) {
        this.detail = detail;
    }

    public String getAnnotations() {
        return annotations;
    }

    public void setAnnotations(String annotations) {
        this.annotations = annotations;
    }

    public Boolean getSelectOnlyComplaintsWithoutCTP() {
        return selectOnlyComplaintsWithoutCTP;
    }

    public void setSelectOnlyComplaintsWithoutCTP(Boolean selectOnlyComplaintsWithoutCTP) {
        this.selectOnlyComplaintsWithoutCTP = selectOnlyComplaintsWithoutCTP;
    }

    public Boolean getForeignCountryClaim() {
        return foreignCountryClaim;
    }

    public void setForeignCountryClaim(Boolean foreignCountryClaim) {
        this.foreignCountryClaim = foreignCountryClaim;
    }

    public VehicleTypeEnum getCounterPartType() {
        return counterPartType;
    }

    public void setCounterPartType(VehicleTypeEnum counterPartType) {
        this.counterPartType = counterPartType;
    }

    public Boolean getClaimWithInjured() {
        return claimWithInjured;
    }

    public void setClaimWithInjured(Boolean claimWithInjured) {
        this.claimWithInjured = claimWithInjured;
    }

    public Boolean getDamageCanNotBeRepaired() {
        return damageCanNotBeRepaired;
    }

    public void setDamageCanNotBeRepaired(Boolean damageCanNotBeRepaired) {
        this.damageCanNotBeRepaired = damageCanNotBeRepaired;
    }

    public List<ClaimsTypeForRuleRequestV1> getClaimsTypeEntityList() {
        if(claimsTypeEntityList == null){
            return null;
        }
        return new ArrayList<>(claimsTypeEntityList);
    }

    public void setClaimsTypeEntityList(List<ClaimsTypeForRuleRequestV1> claimsTypeEntityList) {
        if(claimsTypeEntityList != null)
        {
            this.claimsTypeEntityList = new ArrayList<>(claimsTypeEntityList);
        }else {
            this.claimsTypeEntityList = null;
        }
    }

    public List<InsuranceCompanyRequestV1> getDamagedInsuranceCompanyList() {
        if(damagedInsuranceCompanyList == null){
            return null;
        }
        return new ArrayList<>(damagedInsuranceCompanyList);
    }

    public void setDamagedInsuranceCompanyList(List<InsuranceCompanyRequestV1> damagedInsuranceCompanyList) {
        if(damagedInsuranceCompanyList != null)
        {
            this.damagedInsuranceCompanyList = new ArrayList<>(damagedInsuranceCompanyList);
        }else {
            this.damagedInsuranceCompanyList = null;
        }
    }

    public List<InsuranceCompanyRequestV1> getCounterParterInsuranceCompanyList() {
        if(counterParterInsuranceCompanyList == null){
            return null;
        }
        return new ArrayList<>(counterParterInsuranceCompanyList);
    }

    public void setCounterParterInsuranceCompanyList(List<InsuranceCompanyRequestV1> counterParterInsuranceCompanyList) {
        if(counterParterInsuranceCompanyList != null)
        {
            this.counterParterInsuranceCompanyList =  new ArrayList<>(counterParterInsuranceCompanyList);
        }else {
            this.counterParterInsuranceCompanyList = null;
        }
    }

    public Double getMinImport() {
        return minImport;
    }

    public void setMinImport(Double minImport) {
        this.minImport = minImport;
    }

    public Double getMaxImport() {
        return maxImport;
    }

    public void setMaxImport(Double maxImport) {
        this.maxImport = maxImport;
    }

    @Override
    public String toString() {
        return "AutomaticAffiliationRuleInspectorateRequestV1{" +
                "selectionName='" + selectionName + '\'' +
                ", monthlyVolume=" + monthlyVolume +
                ", currentVolume=" + currentVolume +
                ", detail=" + detail +
                ", annotations='" + annotations + '\'' +
                ", selectOnlyComplaintsWithoutCTP=" + selectOnlyComplaintsWithoutCTP +
                ", foreignCountryClaim=" + foreignCountryClaim +
                ", counterPartType='" + counterPartType + '\'' +
                ", claimWithInjured=" + claimWithInjured +
                ", damageCanNotBeRepaired=" + damageCanNotBeRepaired +
                ", minImport=" + minImport +
                ", maxImport=" + maxImport +
                ", claimsTypeEntityList=" + claimsTypeEntityList +
                ", damagedInsuranceCompanyList=" + damagedInsuranceCompanyList +
                ", counterParterInsuranceCompanyList=" + counterParterInsuranceCompanyList +
                '}';
    }
}
