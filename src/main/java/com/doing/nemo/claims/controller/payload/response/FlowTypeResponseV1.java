package com.doing.nemo.claims.controller.payload.response;

import com.doing.nemo.claims.controller.payload.response.claims.Claims;
import com.doing.nemo.claims.controller.payload.response.claims.ClaimsFlowType;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

public class FlowTypeResponseV1 implements Serializable {
    private static final long serialVersionUID = 7383377301940715295L;

    @JsonProperty("claimsFlowType")
    private List<ClaimsFlowType> claimsFlowType;

    public List<ClaimsFlowType> getClaimsFlowType() {
        return claimsFlowType;
    }

    public void setClaimsFlowType(List<ClaimsFlowType> claimsFlowType) {
        this.claimsFlowType = claimsFlowType;
    }
}
