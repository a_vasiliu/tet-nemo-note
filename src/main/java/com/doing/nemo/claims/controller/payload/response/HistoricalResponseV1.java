package com.doing.nemo.claims.controller.payload.response;

import com.doing.nemo.claims.controller.payload.response.claims.HistoricalResponse;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class HistoricalResponseV1 implements Serializable {

    @JsonProperty("historical")
    private List<HistoricalResponse> historicalResponseList;

    public HistoricalResponseV1() {
    }

    public HistoricalResponseV1(List<HistoricalResponse> historicalResponseList) {
        if(historicalResponseList != null)
        {
            this.historicalResponseList = new ArrayList<>(historicalResponseList);
        }
    }

    public List<HistoricalResponse> getHistoricalResponseList() {
        if(historicalResponseList == null){
            return null;
        }
        return new ArrayList<>(historicalResponseList);
    }

    public void setHistoricalResponseList(List<HistoricalResponse> historicalResponseList) {
        if(historicalResponseList != null)
        {
            this.historicalResponseList = new ArrayList<>(historicalResponseList);
        } else {
            this.historicalResponseList = null;
        }
    }

    @Override
    public String toString() {
        return "HistoricalResponseV1{" +
                "historicalResponseList=" + historicalResponseList +
                '}';
    }
}
