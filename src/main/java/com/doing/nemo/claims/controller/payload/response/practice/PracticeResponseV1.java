package com.doing.nemo.claims.controller.payload.response.practice;

import com.doing.nemo.claims.controller.payload.response.FleetManagerResponseV1;
import com.doing.nemo.claims.controller.payload.response.damaged.ContractResponse;
import com.doing.nemo.claims.controller.payload.response.damaged.CustomerResponse;
import com.doing.nemo.claims.controller.payload.response.damaged.VehicleResponse;
import com.doing.nemo.claims.entity.enumerated.PracticeStatusEnum;
import com.doing.nemo.claims.entity.enumerated.PracticeTypeEnum;
import com.doing.nemo.claims.entity.jsonb.Processing;
import com.doing.nemo.claims.entity.jsonb.forms.jsonbForms.Attachment;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class PracticeResponseV1 implements Serializable {

    @JsonProperty("id")
    private UUID id;

    @JsonProperty("practice_id")
    private Long practiceId;

    /*@JsonProperty("parent_id")
    private UUID parentId;*/

    @JsonProperty("created_at")
    private String createdAt;

    @JsonProperty("updated_at")
    private String updatedAt;

    @JsonProperty("created_by")
    private String createdBy;

    @JsonProperty("status")
    private PracticeStatusEnum status;

    @JsonProperty("practice_type")
    private PracticeTypeEnum practiceType;

    @JsonProperty("claim_id")
    private UUID claimsId;

    @JsonProperty("processing_list")
    private List<ProcessingResponseV1> processingList;

    @JsonProperty("attachment_list")
    private List<Attachment> attachmentList;

    @JsonProperty("vehicle")
    private VehicleResponse vehicle;

    @JsonProperty("customer")
    private CustomerResponse customer;

    @JsonProperty("contract")
    private ContractResponse contract;

    @JsonProperty("fleet_managers")
    private List<FleetManagerResponseV1> fleetManagers;

    @JsonProperty("seizure")
    private SeizureResponseV1 seizure;

    @JsonProperty("release_from_seizure")
    private ReleaseFromSeizureResponseV1 releaseFromSeizure;

    @JsonProperty("misappropriation")
    private MisappropriationResponseV1 misappropriation;

    @JsonProperty("finding")
    private FindingResponseV1 finding;

    @JsonProperty("theft")
    private TheftPracticeResponseV1 theft;

    public SeizureResponseV1 getSeizure() {
        return seizure;
    }

    public void setSeizure(SeizureResponseV1 seizure) {
        this.seizure = seizure;
    }

    public ReleaseFromSeizureResponseV1 getReleaseFromSeizure() {
        return releaseFromSeizure;
    }

    public void setReleaseFromSeizure(ReleaseFromSeizureResponseV1 releaseFromSeizure) {
        this.releaseFromSeizure = releaseFromSeizure;
    }

    public MisappropriationResponseV1 getMisappropriation() {
        return misappropriation;
    }

    public void setMisappropriation(MisappropriationResponseV1 misappropriation) {
        this.misappropriation = misappropriation;
    }

    public FindingResponseV1 getFinding() {
        return finding;
    }

    public void setFinding(FindingResponseV1 finding) {
        this.finding = finding;
    }

    public List<FleetManagerResponseV1> getFleetManagers() {
        if(fleetManagers == null){
            return null;
        }
        return new ArrayList<>(fleetManagers);
    }

    public void setFleetManagers(List<FleetManagerResponseV1> fleetManagers) {
        if(fleetManagers != null)
        {
            this.fleetManagers =  new ArrayList<>(fleetManagers);
        } else {
            this.fleetManagers = null;
        }
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Long getPracticeId() {
        return practiceId;
    }

    public void setPracticeId(Long practiceId) {
        this.practiceId = practiceId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public PracticeStatusEnum getStatus() {
        return status;
    }

    public void setStatus(PracticeStatusEnum status) {
        this.status = status;
    }

    public PracticeTypeEnum getPracticeType() {
        return practiceType;
    }

    public void setPracticeType(PracticeTypeEnum practiceType) {
        this.practiceType = practiceType;
    }

    public UUID getClaimsId() {
        return claimsId;
    }

    public void setClaimsId(UUID claimsId) {
        this.claimsId = claimsId;
    }

    public List<ProcessingResponseV1> getProcessingList() {
        if(processingList == null){
            return null;
        }
        return new ArrayList<>(processingList);
    }

    public void setProcessingList(List<ProcessingResponseV1> processingList) {
        if(processingList != null)
        {
            this.processingList =new ArrayList<>(processingList);
        } else {
            this.processingList = null;
        }
    }

    public List<Attachment> getAttachmentList() {
        if(attachmentList == null){
            return  null;
        }
        return new ArrayList<>(attachmentList);
    }

    public void setAttachmentList(List<Attachment> attachmentList) {
        if(attachmentList != null)
        {
            this.attachmentList = new ArrayList<>(attachmentList);
        } else {
            this.attachmentList = null;
        }
    }

    public VehicleResponse getVehicle() {
        return vehicle;
    }

    public void setVehicle(VehicleResponse vehicle) {
        this.vehicle = vehicle;
    }

    public CustomerResponse getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerResponse customer) {
        this.customer = customer;
    }

    public ContractResponse getContract() {
        return contract;
    }

    public void setContract(ContractResponse contract) {
        this.contract = contract;
    }

    public TheftPracticeResponseV1 getTheft() {
        return theft;
    }

    public void setTheft(TheftPracticeResponseV1 theft) {
        this.theft = theft;
    }

    /*public UUID getParentId() {
        return parentId;
    }

    public void setParentId(UUID parentId) {
        this.parentId = parentId;
    }*/


    @Override
    public String toString() {
        return "PracticeResponseV1{" +
                "id=" + id +
                ", practiceId=" + practiceId +
                ", createdAt='" + createdAt + '\'' +
                ", updatedAt='" + updatedAt + '\'' +
                ", createdBy='" + createdBy + '\'' +
                ", status=" + status +
                ", practiceType=" + practiceType +
                ", claimsId=" + claimsId +
                ", processingList=" + processingList +
                ", attachmentList=" + attachmentList +
                ", vehicle=" + vehicle +
                ", customer=" + customer +
                ", contract=" + contract +
                ", fleetManagers=" + fleetManagers +
                ", seizure=" + seizure +
                ", releaseFromSeizure=" + releaseFromSeizure +
                ", misappropriation=" + misappropriation +
                ", finding=" + finding +
                ", theft=" + theft +
                '}';
    }
}
