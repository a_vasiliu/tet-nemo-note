package com.doing.nemo.claims.controller.payload.response.cai;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class CaiDetailsResponse  implements Serializable {

    @JsonProperty("condition_1")
    private Boolean condition1;

    @JsonProperty("condition_2")
    private Boolean condition2;

    @JsonProperty("condition_3")
    private Boolean condition3;

    @JsonProperty("condition_4")
    private Boolean condition4;

    @JsonProperty("condition_5")
    private Boolean condition5;

    @JsonProperty("condition_6")
    private Boolean condition6;

    @JsonProperty("condition_7")
    private Boolean condition7;

    @JsonProperty("condition_8")
    private Boolean condition8;

    @JsonProperty("condition_9")
    private Boolean condition9;

    @JsonProperty("condition_10")
    private Boolean condition10;

    @JsonProperty("condition_11")
    private Boolean condition11;

    @JsonProperty("condition_12")
    private Boolean condition12;

    @JsonProperty("condition_13")
    private Boolean condition13;

    @JsonProperty("condition_14")
    private Boolean condition14;

    @JsonProperty("condition_15")
    private Boolean condition15;

    @JsonProperty("condition_16")
    private Boolean condition16;

    @JsonProperty("condition_17")
    private Boolean condition17;

    public CaiDetailsResponse() {
    }

    public Boolean getCondition1() {
        return condition1;
    }

    public void setCondition1(Boolean condition1) {
        this.condition1 = condition1;
    }

    public Boolean getCondition2() {
        return condition2;
    }

    public void setCondition2(Boolean condition2) {
        this.condition2 = condition2;
    }

    public Boolean getCondition3() {
        return condition3;
    }

    public void setCondition3(Boolean condition3) {
        this.condition3 = condition3;
    }

    public Boolean getCondition4() {
        return condition4;
    }

    public void setCondition4(Boolean condition4) {
        this.condition4 = condition4;
    }

    public Boolean getCondition5() {
        return condition5;
    }

    public void setCondition5(Boolean condition5) {
        this.condition5 = condition5;
    }

    public Boolean getCondition6() {
        return condition6;
    }

    public void setCondition6(Boolean condition6) {
        this.condition6 = condition6;
    }

    public Boolean getCondition7() {
        return condition7;
    }

    public void setCondition7(Boolean condition7) {
        this.condition7 = condition7;
    }

    public Boolean getCondition8() {
        return condition8;
    }

    public void setCondition8(Boolean condition8) {
        this.condition8 = condition8;
    }

    public Boolean getCondition9() {
        return condition9;
    }

    public void setCondition9(Boolean condition9) {
        this.condition9 = condition9;
    }

    public Boolean getCondition10() {
        return condition10;
    }

    public void setCondition10(Boolean condition10) {
        this.condition10 = condition10;
    }

    public Boolean getCondition11() {
        return condition11;
    }

    public void setCondition11(Boolean condition11) {
        this.condition11 = condition11;
    }

    public Boolean getCondition12() {
        return condition12;
    }

    public void setCondition12(Boolean condition12) {
        this.condition12 = condition12;
    }

    public Boolean getCondition13() {
        return condition13;
    }

    public void setCondition13(Boolean condition13) {
        this.condition13 = condition13;
    }

    public Boolean getCondition14() {
        return condition14;
    }

    public void setCondition14(Boolean condition14) {
        this.condition14 = condition14;
    }

    public Boolean getCondition15() {
        return condition15;
    }

    public void setCondition15(Boolean condition15) {
        this.condition15 = condition15;
    }

    public Boolean getCondition16() {
        return condition16;
    }

    public void setCondition16(Boolean condition16) {
        this.condition16 = condition16;
    }

    public Boolean getCondition17() {
        return condition17;
    }

    public void setCondition17(Boolean condition17) {
        this.condition17 = condition17;
    }

    @Override
    public String toString() {
        return "Cai_details{" +
                "condition1=" + condition1 +
                ", condition2=" + condition2 +
                ", condition3=" + condition3 +
                ", condition4=" + condition4 +
                ", condition5=" + condition5 +
                ", condition6=" + condition6 +
                ", condition7=" + condition7 +
                ", condition8=" + condition8 +
                ", condition9=" + condition9 +
                ", condition10=" + condition10 +
                ", condition11=" + condition11 +
                ", condition12=" + condition12 +
                ", condition13=" + condition13 +
                ", condition14=" + condition14 +
                ", condition15=" + condition15 +
                ", condition16=" + condition16 +
                ", condition17=" + condition17 +
                '}';
    }

}
