package com.doing.nemo.claims.controller;

import com.doing.nemo.claims.adapter.RecipientTypeAdapter;
import com.doing.nemo.claims.common.validation.MessageCode;
import com.doing.nemo.claims.common.validation.RequestValidator;
import com.doing.nemo.claims.controller.payload.request.ListRequestV1;
import com.doing.nemo.claims.controller.payload.request.RecipientTypeRequestV1;
import com.doing.nemo.claims.controller.payload.response.ClaimsResponsePaginationV1;
import com.doing.nemo.claims.controller.payload.response.IdResponseV1;
import com.doing.nemo.claims.controller.payload.response.RecipientTypeResponseV1;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.enumerated.ClaimsRepairEnum;
import com.doing.nemo.claims.entity.settings.RecipientTypeEntity;
import com.doing.nemo.claims.service.RecipientTypeService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/v1")
public class RecipientTypeController {

    @Autowired
    private RecipientTypeService recipientTypeService;

    @Autowired
    private RequestValidator requestValidator;

    @PostMapping(value = "/recipient",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Insert Recipient TypeEnum", notes = "Insert recipient type using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = IdResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @Transactional
    public ResponseEntity<IdResponseV1> postInsertRecipientType(
            @ApiParam(value = "Body of the Recipient TypeEnum to be created", required = true)
            @RequestBody RecipientTypeRequestV1 recipientTypeRequest,
            @ApiParam(value = "TypeEnum of complaint", required = true)
            @RequestParam(value = "type_complaint") ClaimsRepairEnum typeComplaint) {

        requestValidator.validateRequest(recipientTypeRequest, MessageCode.E00X_1000);
        RecipientTypeEntity recipientTypeEntity = RecipientTypeAdapter.getRecipientTypeEntity(recipientTypeRequest);
        recipientTypeEntity.setTypeComplaint(typeComplaint);
        recipientTypeEntity = recipientTypeService.insertRecipientType(recipientTypeEntity);
        IdResponseV1 response = new IdResponseV1(recipientTypeEntity.getId());
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @PostMapping("/recipients")
    @Transactional
    @ApiOperation(value = "Insert a list of Recipients", notes = "Insert a list of recipients using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = RecipientTypeResponseV1.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<RecipientTypeResponseV1>> insertRecipients(
            @ApiParam(value = "List of the bodies of the Recipients to be created", required = true)
            @RequestBody ListRequestV1<RecipientTypeRequestV1> recipientTypeListRequestV1,
            @ApiParam(value = "TypeEnum of complaint", required = true)
            @RequestParam(value = "type_complaint") ClaimsRepairEnum typeComplaint) {

        List<RecipientTypeResponseV1> recipientTypeListRequestV1s = new ArrayList<>();
        for (RecipientTypeRequestV1 recipientTypeRequestV1 : recipientTypeListRequestV1.getData()) {
            requestValidator.validateRequest(recipientTypeRequestV1, MessageCode.E00X_1000);
            RecipientTypeEntity recipientTypeEntity = RecipientTypeAdapter.getRecipientTypeEntity(recipientTypeRequestV1);
            recipientTypeEntity.setTypeComplaint(typeComplaint);
            recipientTypeEntity = recipientTypeService.insertRecipientType(recipientTypeEntity);
            recipientTypeListRequestV1s.add(RecipientTypeAdapter.getRecipientTypeAdapter(recipientTypeEntity));
        }
        return new ResponseEntity<>(recipientTypeListRequestV1s, HttpStatus.CREATED);

    }

    @PutMapping(value = "/recipient/{UUID}",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Upload Recipient TypeEnum", notes = "Upload recipient type using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = RecipientTypeResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @Transactional
    public ResponseEntity<RecipientTypeResponseV1> putRecipientType(
            @ApiParam(value = "UUID that identifies the Recipient TypeEnum to be modified", required = true)
            @PathVariable(name = "UUID") UUID uuid,
            @ApiParam(value = "Updated body of the Recipient TypeEnum", required = true)
            @RequestBody RecipientTypeRequestV1 recipientTypeRequest) {

        requestValidator.validateRequest(recipientTypeRequest, MessageCode.E00X_1000);
        RecipientTypeEntity recipientTypeEntity = RecipientTypeAdapter.getRecipientTypeEntity(recipientTypeRequest);
        recipientTypeEntity = recipientTypeService.updateRecipientType(recipientTypeEntity, uuid);
        RecipientTypeResponseV1 response = RecipientTypeAdapter.getRecipientTypeAdapter(recipientTypeEntity);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping(value = "/recipient/{UUID}",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Recover Recipient TypeEnum", notes = "Returns a Recipient TypeEnum using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = RecipientTypeResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<RecipientTypeResponseV1> getRecipientType(
            @ApiParam(value = "UUID of the Recipient TypeEnum to be found", required = true)
            @PathVariable(name = "UUID") UUID uuid) {

        RecipientTypeEntity recipientTypeEntity = recipientTypeService.getRecipientType(uuid);
        RecipientTypeResponseV1 response = RecipientTypeAdapter.getRecipientTypeAdapter(recipientTypeEntity);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping(value = "/recipient",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Recover Recipient Types", notes = "Returns all the Recipient Types")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = RecipientTypeResponseV1.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<RecipientTypeResponseV1>> getAllRecipientType(
            @ApiParam(value = "TypeEnum of complaint", required = true)
            @RequestParam(value = "type_complaint") ClaimsRepairEnum typeComplaint
    ) {

        List<RecipientTypeEntity> recipientTypeEntities = recipientTypeService.getAllRecipientType(typeComplaint);
        List<RecipientTypeResponseV1> response = RecipientTypeAdapter.getRecipientTypeListAdapter(recipientTypeEntities);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @DeleteMapping(value = "/recipient/{UUID}",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Delete Recipient TypeEnum", notes = "Delete a Recipient TypeEnum using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = RecipientTypeResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @Transactional
    public ResponseEntity deleteRecipientType(@PathVariable(name = "UUID") UUID uuid) {

        RecipientTypeEntity recipientTypeEntity = recipientTypeService.deleteRecipientType(uuid);
        if (recipientTypeEntity == null)
            return new ResponseEntity<>(recipientTypeEntity, HttpStatus.OK);
        RecipientTypeResponseV1 response = RecipientTypeAdapter.getRecipientTypeAdapter(recipientTypeEntity);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PatchMapping("/recipient/{UUID}/active")
    @Transactional
    @ApiOperation(value = "Patch status active Email Template", notes = "Patch status active Email Template using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = RecipientTypeResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<RecipientTypeResponseV1> patchStatusRecipientType(
            @PathVariable UUID UUID
    ) {
        RecipientTypeEntity recipientTypeEntity = recipientTypeService.patchStatusRecipientType(UUID);
        RecipientTypeResponseV1 responseV1 = RecipientTypeAdapter.getRecipientTypeAdapter(recipientTypeEntity);
        return new ResponseEntity<>(responseV1, HttpStatus.OK);
    }

    @ApiOperation(value="RecipientType Pagination", notes = "It retrieves RecipientType pagination")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = ClaimsResponsePaginationV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @GetMapping(value="/recipienttype/pagination", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<ClaimsResponsePaginationV1> recipientTypePagination(
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @RequestParam(value = "page_size", defaultValue = "20") Integer pageSize,
            @RequestParam(value = "type_complaint", required = false) String claimsRepairEnum,
            @RequestParam(value = "description", required = false) String description,
            @RequestParam(value = "fixed", required = false) Boolean fixed,
            @RequestParam(value = "email", required = false) String email,
            @RequestParam(value = "db_field_email", required = false) String dbFieldEmail,
            @RequestParam(value = "is_active", required = false) Boolean isActive,
            @RequestParam(value = "recipient_type", required = false) String recipientType,
            @RequestParam(value = "order_by", required = false) String orderBy,
            @RequestParam(value = "asc", required = false, defaultValue = "true") Boolean asc
    ){

        Pagination<RecipientTypeEntity> recipientTypeEntityPagination = recipientTypeService.paginationRecipientType(page, pageSize, claimsRepairEnum, description, fixed, email, dbFieldEmail, isActive, recipientType, orderBy, asc);

        return new ResponseEntity<>(RecipientTypeAdapter.adptRecipientTypePaginationToClaimsResponsePagination(recipientTypeEntityPagination), HttpStatus.OK);
    }
}
