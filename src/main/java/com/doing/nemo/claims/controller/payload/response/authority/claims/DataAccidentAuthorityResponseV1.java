package com.doing.nemo.claims.controller.payload.response.authority.claims;

import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class DataAccidentAuthorityResponseV1 implements Serializable {
    @JsonProperty("date_accident")
    private String dateAccident;

    @JsonProperty("type_accident")
    private DataAccidentTypeAccidentEnum typeAccident;

    public String getDateAccident() {
        return dateAccident;
    }

    public void setDateAccident(String dateAccident) {
        this.dateAccident = dateAccident;
    }

    public DataAccidentTypeAccidentEnum getTypeAccident() {
        return typeAccident;
    }

    public void setTypeAccident(DataAccidentTypeAccidentEnum typeAccident) {
        this.typeAccident = typeAccident;
    }
}
