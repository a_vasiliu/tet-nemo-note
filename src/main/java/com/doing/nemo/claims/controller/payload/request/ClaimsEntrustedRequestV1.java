package com.doing.nemo.claims.controller.payload.request;

import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.EntrustedEnum.EntrustedEnum;
import com.doing.nemo.claims.entity.enumerated.EventTypeEnum;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ClaimsEntrustedRequestV1 implements Serializable {

    @JsonProperty("entrusted_type")
    private EntrustedEnum entrustedType;

    @JsonProperty("legal")
    private LegalRequestV1 legal;

    @JsonProperty("inspectorate")
    private InspectorateRequestV1 inspectorate;

    @JsonProperty("broker")
    private BrokerRequestV1 broker;

    @JsonProperty("insurance_manager")
    private InsuranceManagerRequestV1 insuranceManager;

    @JsonProperty("event_type")
    private EventTypeEnum eventType;

    @JsonProperty("motivation")
    private String motivation;

    @JsonProperty("template_list")
    private List<EmailTemplateMessagingRequestV1> templateList;

    public ClaimsEntrustedRequestV1(EntrustedEnum entrustedType, LegalRequestV1 legal, InspectorateRequestV1 inspectorate, BrokerRequestV1 broker, InsuranceManagerRequestV1 insuranceManager, EventTypeEnum eventType, String motivation, List<EmailTemplateMessagingRequestV1> templateList) {
        this.entrustedType = entrustedType;
        this.legal = legal;
        this.inspectorate = inspectorate;
        this.broker = broker;
        this.insuranceManager = insuranceManager;
        this.eventType = eventType;
        this.motivation = motivation;
        if(templateList != null)
        {
            this.templateList = new ArrayList<>(templateList);
        }
    }

    public EventTypeEnum getEventType() {
        return eventType;
    }

    public void setEventType(EventTypeEnum eventType) {
        this.eventType = eventType;
    }

    public String getMotivation() {
        return motivation;
    }

    public void setMotivation(String motivation) {
        this.motivation = motivation;
    }

    public List<EmailTemplateMessagingRequestV1> getTemplateList() {
        if(templateList == null){
            return null;
        }
        return new ArrayList<>(templateList);
    }

    public void setTemplateList(List<EmailTemplateMessagingRequestV1> templateList) {
        if(templateList != null)
        {
            this.templateList = new ArrayList<>(templateList);
        }else {
            this.templateList = null;
        }
    }

    public EntrustedEnum getEntrustedType() {
        return entrustedType;
    }

    public void setEntrustedType(EntrustedEnum entrustedType) {
        this.entrustedType = entrustedType;
    }

    public LegalRequestV1 getLegal() {
        return legal;
    }

    public void setLegal(LegalRequestV1 legal) {
        this.legal = legal;
    }

    public InspectorateRequestV1 getInspectorate() {
        return inspectorate;
    }

    public void setInspectorate(InspectorateRequestV1 inspectorate) {
        this.inspectorate = inspectorate;
    }

    public BrokerRequestV1 getBroker() {
        return broker;
    }

    public void setBroker(BrokerRequestV1 broker) {
        this.broker = broker;
    }

    public InsuranceManagerRequestV1 getInsuranceManager() {
        return insuranceManager;
    }

    public void setInsuranceManager(InsuranceManagerRequestV1 insuranceManager) {
        this.insuranceManager = insuranceManager;
    }
}
