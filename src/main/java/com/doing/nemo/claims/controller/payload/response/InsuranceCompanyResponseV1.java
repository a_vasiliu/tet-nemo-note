package com.doing.nemo.claims.controller.payload.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.UUID;

public class InsuranceCompanyResponseV1<T> implements Serializable {

    @JsonProperty("id")
    private UUID id;

    @JsonProperty("code")
    private Long code;

    @JsonProperty("name")
    private String name;

    @JsonProperty("address")
    private String address;

    @JsonProperty("zip_code")
    private String zipCode;

    @JsonProperty("city")
    private String city;

    @JsonProperty("province")
    private String province;

    @JsonProperty("state")
    private String state;

    @JsonProperty("telephone")
    private String telephone;

    @JsonProperty("fax")
    private String fax;

    @JsonProperty("email")
    private String email;

    @JsonProperty("web_site")
    private String webSite;

    @JsonProperty("contact")
    private String contact;

    @JsonProperty("ania_code")
    private Integer aniaCode;

    @JsonProperty("join_card")
    private Boolean joinCard;

    @JsonProperty("contact_pai")
    private String contactPai;

    @JsonProperty("email_pai")
    private String emailPai;

    @JsonProperty("is_active")
    private Boolean isActive;


    public InsuranceCompanyResponseV1() {
    }

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public InsuranceCompanyResponseV1(UUID id) {
        this.id = id;
    }


    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebSite() {
        return webSite;
    }

    public void setWebSite(String webSite) {
        this.webSite = webSite;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public Integer getAniaCode() {
        return aniaCode;
    }

    public void setAniaCode(Integer aniaCode) {
        this.aniaCode = aniaCode;
    }

    public Boolean getJoinCard() {
        return joinCard;
    }

    public void setJoinCard(Boolean joinCard) {
        this.joinCard = joinCard;
    }

    public String getContactPai() {
        return contactPai;
    }

    public void setContactPai(String contactPai) {
        this.contactPai = contactPai;
    }

    public String getEmailPai() {
        return emailPai;
    }

    public void setEmailPai(String emailPai) {
        this.emailPai = emailPai;
    }

    @JsonIgnore
    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    @Override
    public String toString() {
        return "InsuranceCompanyResponseV1{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", zipCode=" + zipCode +
                ", city='" + city + '\'' +
                ", province='" + province + '\'' +
                ", state='" + state + '\'' +
                ", telephone='" + telephone + '\'' +
                ", fax='" + fax + '\'' +
                ", email='" + email + '\'' +
                ", webSite='" + webSite + '\'' +
                ", contact='" + contact + '\'' +
                ", aniaCode=" + aniaCode +
                ", joinCard=" + joinCard +
                ", contactPai='" + contactPai + '\'' +
                ", emailPai='" + emailPai + '\'' +
                ", isActive=" + isActive +
                '}';
    }
}
