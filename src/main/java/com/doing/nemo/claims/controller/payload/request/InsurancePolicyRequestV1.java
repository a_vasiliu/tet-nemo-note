package com.doing.nemo.claims.controller.payload.request;

import com.doing.nemo.claims.entity.enumerated.PolicyEnum.PolicyTypeEnum;
import com.doing.nemo.claims.entity.jsonb.forms.jsonbForms.Attachment;
import com.doing.nemo.claims.entity.settings.InsuranceCompanyEntity;
import com.doing.nemo.claims.entity.settings.InsuranceManagerEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class InsurancePolicyRequestV1 implements Serializable {

    @JsonProperty("type")
    private PolicyTypeEnum type;

    @JsonProperty("description")
    private String description = "";

    @JsonProperty("number_policy")
    private String numberPolicy;

    @JsonProperty("insurance_company")
    private InsuranceCompanyEntity insuranceCompanyEntity;

    @JsonProperty("insurance_manager")
    private InsuranceManagerEntity insuranceManagerEntity;

    @JsonProperty("client_code")
    private Long clientCode;

    @JsonProperty("client")
    private String client = "";

    @JsonProperty("annotations")
    private String annotations = "";

    @JsonProperty("beginning_validity")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date beginningValidity;

    @JsonProperty("end_validity")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date endValidity;

    @JsonProperty("book_register")
    private Boolean bookRegister;

    @JsonProperty("book_register_code")
    private String bookRegisterCode = "";

    @JsonProperty("last_renewal_notification")
    private String lastRenewalNotification;

    @JsonProperty("renewal_notification")
    private Boolean renewalNotification;

    @JsonProperty("risk_entity_list")
    private List<RiskRequestV1> riskEntityList;

    @JsonProperty("attachment")
    private List<Attachment> attachmentEntityList;

    @JsonProperty("event")
    private List<EventRequestV1> eventEntityList;

    @JsonProperty("is_active")
    private Boolean isActive = true;

    @JsonProperty("franchise")
    private Double franchise;

    public Double getFranchise() {
        return franchise;
    }

    public void setFranchise(Double franchise) {
        this.franchise = franchise;
    }

    public PolicyTypeEnum getType() {
        return type;
    }

    public void setType(PolicyTypeEnum type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNumberPolicy() {
        return numberPolicy;
    }

    public void setNumberPolicy(String numberPolicy) {
        this.numberPolicy = numberPolicy;
    }

    public InsuranceCompanyEntity getInsuranceCompanyEntity() {
        return insuranceCompanyEntity;
    }

    public void setInsuranceCompanyEntity(InsuranceCompanyEntity insuranceCompanyEntity) {
        this.insuranceCompanyEntity = insuranceCompanyEntity;
    }

    public InsuranceManagerEntity getInsuranceManagerEntity() {
        return insuranceManagerEntity;
    }

    public void setInsuranceManagerEntity(InsuranceManagerEntity insuranceManagerEntity) {
        this.insuranceManagerEntity = insuranceManagerEntity;
    }

    public Long getClientCode() {
        return clientCode;
    }

    public void setClientCode(Long clientCode) {
        this.clientCode = clientCode;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getAnnotations() {
        return annotations;
    }

    public void setAnnotations(String annotations) {
        this.annotations = annotations;
    }

    public Date getBeginningValidity() {
        if(beginningValidity == null){
            return null;
        }
        return (Date)beginningValidity.clone();
    }

    public void setBeginningValidity(Date beginningValidity) {
        if(beginningValidity != null)
        {
            this.beginningValidity = (Date)beginningValidity.clone();
        } else {
            this.beginningValidity = null;
        }
    }

    public Date getEndValidity() {
        if(endValidity == null){
            return null;
        }
        return (Date)endValidity.clone();
    }

    public void setEndValidity(Date endValidity) {
        if(endValidity != null)
        {
            this.endValidity = (Date)endValidity.clone();
        } else {
            this.endValidity = null;
        }
    }

    public Boolean getBookRegister() {
        return bookRegister;
    }

    public void setBookRegister(Boolean bookRegister) {
        this.bookRegister = bookRegister;
    }

    public String getBookRegisterCode() {
        return bookRegisterCode;
    }

    public void setBookRegisterCode(String bookRegisterCode) {
        this.bookRegisterCode = bookRegisterCode;
    }

    public String getLastRenewalNotification() {
        return lastRenewalNotification;
    }

    public void setLastRenewalNotification(String lastRenewalNotification) {
        this.lastRenewalNotification = lastRenewalNotification;
    }

    public Boolean getRenewalNotification() {
        return renewalNotification;
    }

    public void setRenewalNotification(Boolean renewalNotification) {
        this.renewalNotification = renewalNotification;
    }

    public List<RiskRequestV1> getRiskEntityList() {
        if(riskEntityList == null){
            return null;
        }
        return new ArrayList<>(riskEntityList);
    }

    public void setRiskEntityList(List<RiskRequestV1> riskEntityList) {
        if(riskEntityList != null)
        {
            this.riskEntityList = new ArrayList<>(riskEntityList);
        } else {
            this.riskEntityList = null;
        }
    }

    public List<Attachment> getAttachmentEntityList() {
        if(attachmentEntityList == null){
            return null;
        }
        return new ArrayList<>(attachmentEntityList);
    }

    public void setAttachmentEntityList(List<Attachment> attachmentEntityList) {
        if(attachmentEntityList != null)
        {
            this.attachmentEntityList = new ArrayList<>(attachmentEntityList);
        } else {
            this.attachmentEntityList = null;
        }
    }

    public List<EventRequestV1> getEventEntityList() {
        if(eventEntityList == null){
            return null;
        }
        return new ArrayList<>(eventEntityList);
    }

    public void setEventEntityList(List<EventRequestV1> eventEntityList) {
        if(eventEntityList != null)
        {
            this.eventEntityList = new ArrayList<>(eventEntityList);
        } else {
            this.eventEntityList = null;
        }
    }

    public Boolean getActive() {
        return isActive;
    }

    @JsonSetter
    public void setActive(Boolean active) {
        if (active != null)
            isActive = active;
    }

    @Override
    public String toString() {
        return "InsurancePolicyRequestV1{" +
                "type='" + type + '\'' +
                ", description='" + description + '\'' +
                ", numberPolicy=" + numberPolicy +
                ", insuranceCompanyEntity=" + insuranceCompanyEntity +
                ", insuranceManagerEntity=" + insuranceManagerEntity +
                ", clientCode=" + clientCode +
                ", client='" + client + '\'' +
                ", annotations='" + annotations + '\'' +
                ", beginningValidity=" + beginningValidity +
                ", endValidity=" + endValidity +
                ", bookRegister=" + bookRegister +
                ", bookRegisterCode='" + bookRegisterCode + '\'' +
                ", lastRenewalNotification=" + lastRenewalNotification +
                ", renewalNotification=" + renewalNotification +
                ", riskEntityList=" + riskEntityList +
                ", attachmentEntityList=" + attachmentEntityList +
                ", eventEntityList=" + eventEntityList +
                ", isActive=" + isActive +
                '}';
    }
}
