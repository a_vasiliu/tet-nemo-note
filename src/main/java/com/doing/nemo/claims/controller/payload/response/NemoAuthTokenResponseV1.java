package com.doing.nemo.claims.controller.payload.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class NemoAuthTokenResponseV1 implements Serializable {
    @JsonProperty("token")
    private String token;
    @JsonProperty("expires_in")
    private String expiresIn;
    @JsonProperty("number_of_uses")
    private String numberOfUses;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(String expiresIn) {
        this.expiresIn = expiresIn;
    }

    public String getNumberOfUses() {
        return numberOfUses;
    }

    public void setNumberOfUses(String numberOfUses) {
        this.numberOfUses = numberOfUses;
    }
}
