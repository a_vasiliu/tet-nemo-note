package com.doing.nemo.claims.controller.payload.response.claims;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class Claims  implements Serializable {

    @JsonProperty("id")
    private String id;

    @JsonProperty("practice_id")
    private Long practiceId;

    public Claims() {
    }

    public Claims(String id, Long practiceId) {
        this.id = id;
        this.practiceId = practiceId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getPracticeId() {
        return practiceId;
    }

    public void setPracticeId(Long practiceId) {
        this.practiceId = practiceId;
    }

    @Override
    public String toString() {
        return "Claims{" +
                "id='" + id + '\'' +
                ", practiceId=" + practiceId +
                '}';
    }
}
