package com.doing.nemo.claims.controller.payload.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class ProvaResponse implements Serializable {
    @JsonProperty("resourse")
    private String resource;

    public ProvaResponse() {
    }

    public ProvaResponse(String resource) {
        this.resource = resource;
    }

    public String getResource() {
        return resource;
    }

    public void setResource(String resource) {
        this.resource = resource;
    }
}
