package com.doing.nemo.claims.controller.payload.response;

import com.doing.nemo.claims.entity.enumerated.AuthorityEnum.AuthorityEventTypeEnum;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class HistoricalAuthorityResponseV1 implements Serializable {

    @JsonProperty("id")
    private Long id;

    @JsonProperty("event_type")
    private AuthorityEventTypeEnum eventType;

    @JsonProperty("update_at")
    private String updateAt;

    @JsonProperty("user_id")
    private String userId;

    @JsonProperty("user_name")
    private String userName;

    @JsonProperty("description")
    private String description;

    @JsonProperty("old_total")
    private Double oldTotal;

    @JsonProperty("new_total")
    private Double newTotal;


    public HistoricalAuthorityResponseV1() {
    }

    public HistoricalAuthorityResponseV1(Long id, AuthorityEventTypeEnum eventType, String updateAt, String userId, String userName, String description, Double oldTotal, Double newTotal) {
        this.id = id;
        this.eventType = eventType;
        this.updateAt = updateAt;
        this.userId = userId;
        this.userName = userName;
        this.description = description;
        this.oldTotal = oldTotal;
        this.newTotal = newTotal;
    }

    public Double getOldTotal() {
        return oldTotal;
    }

    public void setOldTotal(Double oldTotal) {
        this.oldTotal = oldTotal;
    }

    public Double getNewTotal() {
        return newTotal;
    }

    public void setNewTotal(Double newTotal) {
        this.newTotal = newTotal;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AuthorityEventTypeEnum getEventType() {
        return eventType;
    }

    public void setEventType(AuthorityEventTypeEnum eventType) {
        this.eventType = eventType;
    }

    public String getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(String updateAt) {
        this.updateAt = updateAt;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "HistoricalAuthorityResponseV1{" +
                "id=" + id +
                ", eventType=" + eventType +
                ", updateAt='" + updateAt + '\'' +
                ", userId='" + userId + '\'' +
                ", userName='" + userName + '\'' +
                ", description='" + description + '\'' +
                ", oldTotal=" + oldTotal +
                ", newTotal=" + newTotal +
                '}';
    }
}
