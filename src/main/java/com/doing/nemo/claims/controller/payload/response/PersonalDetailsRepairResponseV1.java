package com.doing.nemo.claims.controller.payload.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Date;

public class PersonalDetailsRepairResponseV1 implements Serializable {
    @JsonProperty("firstname")
    private String firstname;

    @JsonProperty("lastname")
    private String lastname;

    @JsonProperty("gender")
    private String gender;

    @JsonProperty("birth_date")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date birthDate;

    @JsonProperty("birth_place")
    private String birthPlace;

    @JsonProperty("birth_country")
    private String birthCountry;

    @JsonProperty("birth_province")
    private String birthProvince;

    @JsonProperty("tax_code")
    private String taxCode;

    @JsonProperty("office_role")
    private String officeRole;

    @JsonProperty("phone")
    private String phone;

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Date getBirthDate() {
        if(birthDate == null){
            return null;
        }
        return (Date)birthDate.clone();
    }

    public void setBirthDate(Date birthDate) {
        if(birthDate != null)
        {
            this.birthDate = (Date)birthDate.clone();
        } else {
            this.birthDate = null;
        }
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public String getBirthCountry() {
        return birthCountry;
    }

    public void setBirthCountry(String birthCountry) {
        this.birthCountry = birthCountry;
    }

    public String getBirthProvince() {
        return birthProvince;
    }

    public void setBirthProvince(String birthProvince) {
        this.birthProvince = birthProvince;
    }

    public String getTaxCode() {
        return taxCode;
    }

    public void setTaxCode(String taxCode) {
        this.taxCode = taxCode;
    }

    public String getOfficeRole() {
        return officeRole;
    }

    public void setOfficeRole(String officeRole) {
        this.officeRole = officeRole;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "PersonalDetailsRepair{" +
                "firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", gender='" + gender + '\'' +
                ", birthDate=" + birthDate +
                ", birthPlace='" + birthPlace + '\'' +
                ", birthCountry='" + birthCountry + '\'' +
                ", birthProvince='" + birthProvince + '\'' +
                ", taxCode='" + taxCode + '\'' +
                ", officeRole='" + officeRole + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }
}
