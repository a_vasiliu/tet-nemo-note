package com.doing.nemo.claims.controller.payload.response.vehicle;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RelatedContractResponse implements Serializable {

    @JsonProperty("id")
    private Integer id;

    @JsonProperty("start_date")
    private String startDate;

    @JsonProperty("end_date")
    private String endDate;

    @JsonProperty("is_active")
    private Boolean isActive;

    @JsonProperty("links")
    private List<LinkResponse> linksList;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public List<LinkResponse> getLinksList() {
        if(linksList == null){
            return null;
        }
        return new ArrayList<>(linksList);
    }

    public void setLinksList(List<LinkResponse> linksList) {
        if(linksList != null)
        {
            this.linksList =  new ArrayList<>(linksList);
        } else {
            this.linksList = null;
        }
    }

    @Override
    public String toString() {
        return "RelatedContract{" +
                "id='" + id + '\'' +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", isActive=" + isActive +
                ", linksList=" + linksList +
                '}';
    }
}
