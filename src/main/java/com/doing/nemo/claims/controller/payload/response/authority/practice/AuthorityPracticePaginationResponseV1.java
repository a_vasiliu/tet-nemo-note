package com.doing.nemo.claims.controller.payload.response.authority.practice;

import com.doing.nemo.claims.controller.payload.response.damaged.ImpactPointResponse;
import com.doing.nemo.claims.entity.enumerated.PracticeStatusEnum;
import com.doing.nemo.claims.entity.enumerated.PracticeTypeEnum;
import com.doing.nemo.claims.entity.enumerated.RepairEnum.RepairStatusEnum;
import com.doing.nemo.claims.entity.jsonb.Processing;
import com.doing.nemo.claims.entity.jsonb.ProcessingAuthorityResponse;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AuthorityPracticePaginationResponseV1 implements Serializable {

    @JsonProperty("id")
    private String id;

    @JsonProperty("practice_id")
    private Long practiceId;

    @JsonProperty("practice_type")
    private PracticeTypeEnum practiceType;

    @JsonProperty("created_at")
    private String createdAt;

    @JsonProperty("processing_list")
    private List<ProcessingAuthorityResponse> processingList;

    @JsonProperty("authorities")
    private List<String> authorities;

    @JsonProperty("status")
    private PracticeStatusEnum status;

    public PracticeStatusEnum getStatus() {
        return status;
    }

    public void setStatus(PracticeStatusEnum status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getPracticeId() {
        return practiceId;
    }

    public void setPracticeId(Long practiceId) {
        this.practiceId = practiceId;
    }

    public PracticeTypeEnum getPracticeType() {
        return practiceType;
    }

    public void setPracticeType(PracticeTypeEnum practiceType) {
        this.practiceType = practiceType;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public List<ProcessingAuthorityResponse> getProcessingList() {
        if(processingList == null){
            return null;
        }
        return new ArrayList<>(processingList);
    }

    public void setProcessingList(List<ProcessingAuthorityResponse> processingList) {
        if(processingList != null)
        {
            this.processingList = new ArrayList<>(processingList);
        } else {
            this.processingList = null;
        }
    }

    public List<String> getAuthorities() {
        if(authorities == null){
            return null;
        }
        return new ArrayList<>(authorities);
    }

    public void setAuthorities(List<String> authorities) {
        if(authorities != null)
        {
            this.authorities = new ArrayList<>(authorities);
        } else {
            this.authorities = null;
        }
    }
}
