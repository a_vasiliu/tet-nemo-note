package com.doing.nemo.claims.controller;

import com.doing.nemo.claims.adapter.ClaimsAdapter;
import com.doing.nemo.claims.adapter.GoLiveMSAAdapter;
import com.doing.nemo.claims.adapter.InsuranceCompanyAdapter;
import com.doing.nemo.claims.command.ClaimsInsertExternalCommand;
import com.doing.nemo.claims.command.ClaimsPatchExternalAcclaimsCommand;
import com.doing.nemo.claims.command.ClaimsPatchExternalMSACommand;
import com.doing.nemo.claims.command.ClaimsPatchReadExternalCommand;
import com.doing.nemo.claims.common.commandBus.Command;
import com.doing.nemo.claims.common.commandBus.CommandBus;
import com.doing.nemo.claims.controller.payload.request.*;
import com.doing.nemo.claims.controller.payload.response.ClaimsExternalResponse;
import com.doing.nemo.claims.controller.payload.response.ClaimsResponsePaginationV1;
import com.doing.nemo.claims.controller.payload.response.InsuranceCompanyExternalResponseV1;
import com.doing.nemo.claims.controller.payload.response.*;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.enumerated.GoLiveSystemEnum;
import com.doing.nemo.claims.entity.jsonb.Authority;
import com.doing.nemo.claims.entity.jsonb.forms.jsonbForms.Attachment;
import com.doing.nemo.claims.entity.settings.InsuranceCompanyEntity;
import com.doing.nemo.claims.service.*;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.exception.NotFoundException;
import io.swagger.annotations.*;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/v1")
@Api("External Manager")
public class ExternalClaimsManagerController {


    private static Logger LOGGER = LoggerFactory.getLogger(ExternalClaimsManagerController.class);

    @Autowired
    private CommandBus commandBus;

    @Autowired
    private ExternalManagerService externalManagerService;

    @Autowired
    private InsuranceCompanyService insuranceCompanyService;

    @Autowired
    private AuthorityAttachmentService authorityAttachmentService;

    @Autowired
    private ClaimsCheckerService claimsCheckerService;

    @Autowired
    private ClaimsService claimsService;

    //REFACTOR
    @Autowired
    private ESBService esbService;

    @GetMapping(value = "/claims/external",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ClaimsResponsePaginationV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ClaimsResponsePaginationV1> getClaimsEntityPaginationExternalManager(
            @ApiParam(value = "Defines how many Claims can contain the single page", required = false)
            @RequestParam(value = "page_size", defaultValue = "20") Integer pageSize,
            @ApiParam(value = "Defines the page number to be displayed", required = false)
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @ApiParam(value = "Filter the search by the date the Claim was created from", required = false)
            @RequestParam(value = "date_accident", required = false) String dateAccident,
            @ApiParam(value = "Filter the search by plate", required = false)
            @RequestParam(value = "plate", required = false) String plate,
            @ApiParam(value = "Order", required = false)
            @RequestParam(value = "asc", required = false, defaultValue = "true") Boolean asc,
            @ApiParam(value = "OrderBy", required = false)
            @RequestParam(value = "order_by", defaultValue = "created_at", required = false) String orderBy,
            @RequestHeader(value = "nemo-profiles", required = true) List<String> nemoProfilesList,
            @ApiParam(value = "Filter the search by practice_id")
            @RequestParam(value = "practice_id", required = false) Long practiceId
    ) {
        //claims_insurance_supplier_msa
        //claims_insurance_supplier_acclaims
        Pagination<ClaimsEntity> claimsPagination;
        if (nemoProfilesList != null && !nemoProfilesList.isEmpty() && nemoProfilesList.contains("claims_insurance_supplier_acclaims")) {
            claimsPagination = externalManagerService.getAcclaimsClaimsEntity(practiceId, plate, dateAccident, asc, orderBy, page, pageSize);
            claimsPagination = authorityAttachmentService.addAuthorityAttachment(claimsPagination);
        } else if (nemoProfilesList != null && !nemoProfilesList.isEmpty() && nemoProfilesList.contains("claims_insurance_supplier_msa")) {
            claimsPagination = externalManagerService.getMsaClaimsEntity(practiceId, plate, dateAccident, asc, orderBy, page, pageSize);
            claimsPagination = authorityAttachmentService.addAuthorityAttachment(claimsPagination);
        } else {
            LOGGER.debug("Profile key not valid");
            throw new BadRequestException("Profile key not valid");
        }

        return new ResponseEntity<>(ClaimsAdapter.adptClaimsPaginationToClaimsPaginationExternalResponse(claimsPagination), HttpStatus.OK);

    }


    //sinistri che hanno una polizza associata alla compagnia sogessour
    //sinistri incompleta e validata
    //legale multiserass id 9
    // multiserass = 28445608-4a5f-11e9-8646-d663bd873d93
    //compagnia assicuratrice sogessour = a83be7e2-4a58-11e9-8646-d663bd873d93
    //acclaims = a4f522b2-59fb-11e9-8647-d663bd873d93
    //acclaims sr = a4f526cc-59fb-11e9-8647-d663bd873d93
    //legale acclaims id 8 15 acclaims sr

    //REFACTOR
    @PatchMapping(value = "/claims/external")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ClaimsResponsePaginationV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<ClaimsExternalResponse>> patchClaimsAsRead(
            @ApiParam(value = "List of id claims to set as read", required = true)
            @RequestBody ListRequestV1<IdRequestV1> claimsIdListRequest,
            @RequestHeader(value = "nemo-profiles", required = true) List<String> nemoProfilesList
    ) throws IOException {
        Command claimsPatchReadExternalCommand = new ClaimsPatchReadExternalCommand(claimsIdListRequest, nemoProfilesList);
        commandBus.execute(claimsPatchReadExternalCommand);
        List<ClaimsExternalResponse> claimsExternalResponses = new ArrayList<>();

        for (IdRequestV1 claimsIdRequestV1 : claimsIdListRequest.getData()) {
            //REFACTOR
            ClaimsEntity claimsEntity = claimsService.findById(claimsIdRequestV1.getId());
            claimsEntity = authorityAttachmentService.addAuthorityAttachment(claimsEntity);
            claimsExternalResponses.add(ClaimsAdapter.adptClaimsToClaimsExternalResponse(claimsEntity));
        }

        return new ResponseEntity<>(claimsExternalResponses, HttpStatus.OK);
    }


    //REFACTOR
    @PatchMapping(value = "/claims/external/msa/{UUID_CLAIMS}",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Patch claims from company", notes = "Patch claims from company into complaint")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ClaimsExternalResponse.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ClaimsExternalResponse> patchFromCompanyExternalMSA(
            @ApiParam(value = "UUID of the Claim to be found", required = true)
            @PathVariable(name = "UUID_CLAIMS") String claimsId,
            @ApiParam(value = "From company body")
            @RequestBody ClaimsPatchExternalMSARequest claimsPatchExternalMSARequest
    ) throws IOException {
        Command command = new ClaimsPatchExternalMSACommand(
                claimsId,
                claimsPatchExternalMSARequest.getFromCompany()
        );
        commandBus.execute(command);
        //REFACTOR
        ClaimsEntity claimsEntity = claimsService.findById(claimsId);
        claimsEntity = authorityAttachmentService.addAuthorityAttachment(claimsEntity);
        return new ResponseEntity<>(ClaimsAdapter.adptClaimsToClaimsExternalResponse(claimsEntity), HttpStatus.OK);
    }

    //REFACTOR
    @PatchMapping(value = "/claims/external/acclaims/{UUID_CLAIMS}",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Patch claims from company", notes = "Patch claims from company into complaint")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ClaimsExternalResponse.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ClaimsExternalResponse> patchFromCompanyExternalAcclaims(
            @ApiParam(value = "UUID of the Claim to be found", required = true)
            @PathVariable(name = "UUID_CLAIMS") String claimsId,
            @ApiParam(value = "Patch ACCLAIMS body")
            @RequestBody ClaimsPatchExternalAcclaimsRequest claimsPatchExternalAcclaimsRequest
    ) throws IOException {
        Command command = new ClaimsPatchExternalAcclaimsCommand(
                claimsId,
                claimsPatchExternalAcclaimsRequest.getNumberSxCounterparty(),
                claimsPatchExternalAcclaimsRequest.getStatus()
        );
        commandBus.execute(command);
        //REFACTOR
        ClaimsEntity claimsEntity = claimsService.findById(claimsId);
        claimsEntity = authorityAttachmentService.addAuthorityAttachment(claimsEntity);
        return new ResponseEntity<>(ClaimsAdapter.adptClaimsToClaimsExternalResponse(claimsEntity), HttpStatus.OK);
    }


    //REFACTOR
    @GetMapping(value = "/claims/external/{UUID_CLAIMS}",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Recover Claim", notes = "Returns a Claim using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ClaimsExternalResponse.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ClaimsExternalResponse> getClaimsEntityByIdExternalManager(
            @ApiParam(value = "UUID of the Claim to be found", required = true)
            @PathVariable(name = "UUID_CLAIMS") String claimsId) {

        ClaimsEntity claimsEntity = claimsService.findById(claimsId);
        claimsEntity = authorityAttachmentService.addAuthorityAttachment(claimsEntity);
        return new ResponseEntity<>(ClaimsAdapter.adptClaimsToClaimsExternalResponse(claimsEntity), HttpStatus.OK);
    }


    //REFACTOR
    @GetMapping(value = "/claims/external/practice/{PRACTICE_ID}",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Recover Claim", notes = "Returns a Claim using the Practice id passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ClaimsExternalResponse.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ClaimsExternalResponse> getClaimsEntityByPracticeId(
            @ApiParam(value = "Practice id of the Claim to be found", required = true)
            @PathVariable(name = "PRACTICE_ID") Long practiceId) {

        try {

            ClaimsEntity claimsEntity = claimsService.findByIdPratica(practiceId);
            claimsEntity = authorityAttachmentService.addAuthorityAttachment(claimsEntity);
            return new ResponseEntity<>(ClaimsAdapter.adptClaimsToClaimsExternalResponse(claimsEntity), HttpStatus.OK);
        }catch (Exception e){
            LOGGER.error(MessageCode.CLAIMS_1010.value());
            throw new NotFoundException(MessageCode.CLAIMS_1010);
        }
    }

    //REFACTOR
    @PostMapping(value = "/claims/external",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Insert a new claims by external manager", notes = "Insert a new claims by external manager")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ClaimsExternalResponse.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity postClaimsExternalManager(
            @ApiParam(value = "Body of the Claims to be created", required = true)
            @RequestBody ClaimsInsertExternalRequest claimsInsertExternalRequest,
            @RequestHeader(name = "nemo-user-first-name") String userName,
            @RequestHeader(name = "nemo-user-last-name") String lastName,
            @RequestHeader(name = "nemo-user-id") String userId) throws IOException {


        String driverPlate = claimsInsertExternalRequest.getComplaint().getPlate();
        String dateAccident = claimsInsertExternalRequest.getComplaint().getDataAccident().getDateAccident();
        DataAccidentTypeAccidentEnum typeAccident = claimsInsertExternalRequest.getComplaint().getDataAccident().getTypeAccident();
        String claimsId = UUID.randomUUID().toString();

        LOGGER.debug( "[MSA] damaged driver plate: " + driverPlate );
        LOGGER.debug( "[MSA] claim's date accident: " + dateAccident );
        LOGGER.debug( "[MSA] claim's type accident: " + typeAccident );


        //rotta checkClaimsDuplicate refactorizzata
        ClaimsCheckerResponseV1 claimsCheckerResponseClaim = claimsCheckerService.checkClaimsDuplicate(driverPlate,dateAccident,(typeAccident != null ? typeAccident.getValue() : null), null);
        if(claimsCheckerResponseClaim != null && claimsCheckerResponseClaim.getCode() != null){
            MessageCode messageCode = MessageCode.create(claimsCheckerResponseClaim.getCode());
            LOGGER.debug( "[MSA] claim's duplicate check: " + claimsCheckerResponseClaim );
            return new ResponseEntity<>(claimsCheckerResponseClaim, HttpStatus.OK);
        }

        //controllo per chiascuna controparte
        if(claimsInsertExternalRequest.getWithCounterparty() != null && claimsInsertExternalRequest.getWithCounterparty() && claimsInsertExternalRequest.getCounterparty() != null && !claimsInsertExternalRequest.getCounterparty().isEmpty()) {
            for(ClaimsInsertExternalRequest.CounterpartyExternalRequest currentCounterparty : claimsInsertExternalRequest.getCounterparty()) {

                //CLAIMS-1059 - CLAIMS-1060 - CLAIMS -1061

                String counterpartyInsuredName = currentCounterparty.getInsured() != null ? currentCounterparty.getInsured().getFirstname() : null;
                String counterpartyInsuredSurname = currentCounterparty.getInsured() != null ? currentCounterparty.getInsured().getLastname() : null;
                String counterpartyPlate = currentCounterparty.getVehicle() != null ? currentCounterparty.getVehicle().getLicensePlate() : null;

                LOGGER.debug( "[MSA] counterparty insured name: " + counterpartyInsuredName );
                LOGGER.debug( "[MSA] counterparty insured surname: " + counterpartyInsuredSurname );
                LOGGER.debug( "[MSA] counterparty plate: " + counterpartyPlate );

                ClaimsCheckerResponseV1 claimsCheckerResponseCounterparty1 = claimsCheckerService.checkIfExistsAnotherCounterpartyWithEqualsPlateName(driverPlate, dateAccident,counterpartyInsuredName, counterpartyInsuredSurname, counterpartyPlate, claimsId);
                if(claimsCheckerResponseCounterparty1 != null && claimsCheckerResponseCounterparty1.getCode() != null){
                    MessageCode messageCode = MessageCode.create(claimsCheckerResponseCounterparty1.getCode());
                    LOGGER.debug( "[MSA] counterparty's duplicate check: " +  claimsCheckerResponseCounterparty1);
                    return new ResponseEntity<>(claimsCheckerResponseCounterparty1, HttpStatus.OK);
                }
            }
        }


        Command claimsInsertCommand = new ClaimsInsertExternalCommand(
                claimsId,
                claimsInsertExternalRequest.getWithCounterparty(),
                claimsInsertExternalRequest.getComplaint(),
                claimsInsertExternalRequest.getDamaged(),
                claimsInsertExternalRequest.getCounterparty(),
                claimsInsertExternalRequest.getDeponent(),
                claimsInsertExternalRequest.getWounded(),
                claimsInsertExternalRequest.getCaiDetails(),
                claimsInsertExternalRequest.getIdSaleforce(),
                claimsInsertExternalRequest.getRefund(),
                claimsInsertExternalRequest.getTheftRequest(),
                claimsInsertExternalRequest.getRead(),
                userName + " " + lastName,
                userId,
                claimsInsertExternalRequest.getDriver(),
                claimsInsertExternalRequest.getCompleteDocumentation()
        );

        commandBus.execute(claimsInsertCommand);
        //recupero della nuova entità e conversione nella vecchia entità
        ClaimsEntity claimsEntity = claimsService.findById(claimsId);
        return new ResponseEntity<>(ClaimsAdapter.adptIdToIdResponse(claimsEntity.getId(), claimsEntity.getPracticeId()), HttpStatus.OK);

    }

    @GetMapping("/company/external")
    @Transactional
    @ApiOperation(value = "Recover All Insurance Companies for External Manager", notes = "Returns all Insurance Companies")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = InsuranceCompanyExternalResponseV1.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<InsuranceCompanyExternalResponseV1>> getAllInsuranceCompanies() {

        List<InsuranceCompanyEntity> insuranceCompanyEntityList = insuranceCompanyService.selectAllCompany();

        List<InsuranceCompanyExternalResponseV1> responseV1 = InsuranceCompanyAdapter.adptFromICompEntityToICompRespListExternal(insuranceCompanyEntityList);
        return new ResponseEntity<>(responseV1, HttpStatus.OK);
    }

    @GetMapping(value = "/claims/external/checkcustomer",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = GoLiveMSAResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<GoLiveMSAResponseV1> getExternalCheckCustomer(
            @ApiParam(value = "plate", required = true)
            @RequestParam(value = "plate") String plate,
            @ApiParam(value = "date_accident", required = false)
            @RequestParam(value = "date_accident") String dateAccident
    ) {
        Boolean isNemo = esbService.getContractByContractIdOrPlateGoLiveMSA(plate,dateAccident);
        GoLiveMSAResponseV1 responseV1 = GoLiveMSAAdapter.adptFromGoLiveStrategyBooleanToGoLiveStrategyResponse(isNemo);
        return new ResponseEntity<>(responseV1, HttpStatus.OK);

    }

}
