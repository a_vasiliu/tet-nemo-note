package com.doing.nemo.claims.controller;

import com.doing.nemo.claims.adapter.ExportNotificationTypeAdapter;
import com.doing.nemo.claims.controller.payload.request.ExportNotificationTypeRequestV1;
import com.doing.nemo.claims.controller.payload.request.ListRequestV1;
import com.doing.nemo.claims.controller.payload.response.ClaimsResponsePaginationV1;
import com.doing.nemo.claims.controller.payload.response.IdResponseV1;
import com.doing.nemo.claims.controller.payload.response.ExportNotificationTypeResponseV1;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.settings.ExportNotificationTypeEntity;
import com.doing.nemo.claims.service.ExportNotificationTypeService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/v1")
@Api("Export Notification TypeEnum")
public class ExportNotificationTypeController {
    @Autowired
    private ExportNotificationTypeService exportNotificationTypeService;

    @Autowired
    private ExportNotificationTypeAdapter exportNotificationTypeAdapter;

    @PostMapping("/export/notificationtype")
    @Transactional
    @ApiOperation(value = "Insert Export Notification TypeEnum", notes = "Insert Export Notification TypeEnum using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = ExportNotificationTypeResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<IdResponseV1> saveExportNotificationType(
            @RequestBody ExportNotificationTypeRequestV1 exportNotificationTypeRequestV1
            )
    {
        ExportNotificationTypeEntity exportNotificationTypeEntity = exportNotificationTypeAdapter.adptFromExportNotificationTypeRequestToExportNotificationTypeEntity(exportNotificationTypeRequestV1);
        exportNotificationTypeEntity = exportNotificationTypeService.saveExportNotificationType(exportNotificationTypeEntity);
        return new ResponseEntity<>(new IdResponseV1(exportNotificationTypeEntity.getId()), HttpStatus.CREATED);
    }

    @PostMapping("/export/notificationtype/all")
    @Transactional
    @ApiOperation(value = "Insert a list of Export Notification TypeEnum", notes = "Insert a list of insurance companies using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = ExportNotificationTypeResponseV1.class, responseContainer="List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<ExportNotificationTypeResponseV1>> insertExportNotificationTypeAll(
            @ApiParam(value = "List of the bodies of the Export Notification TypeEnum to be created", required = true)
            @RequestBody ListRequestV1<ExportNotificationTypeRequestV1> insuranceCompaniesRequest){

        List<ExportNotificationTypeResponseV1> exportNotificationTypeResponseV1List = new ArrayList<>();
        for (ExportNotificationTypeRequestV1 exportNotificationTypeRequestV1 : insuranceCompaniesRequest.getData())
        {

            ExportNotificationTypeEntity exportNotificationTypeEntity = exportNotificationTypeAdapter.adptFromExportNotificationTypeRequestToExportNotificationTypeEntity(exportNotificationTypeRequestV1);
            exportNotificationTypeEntity = exportNotificationTypeService.saveExportNotificationType(exportNotificationTypeEntity);
            ExportNotificationTypeResponseV1 responseV1 = ExportNotificationTypeAdapter.adptFromExportNotificationTypeEntityToExportNotificationTypeResponse(exportNotificationTypeEntity);
            exportNotificationTypeResponseV1List.add(responseV1);
        }
        return new ResponseEntity<>(exportNotificationTypeResponseV1List, HttpStatus.CREATED);

    }

    @GetMapping("/export/notificationtype/{UUID}")
    @Transactional
    @ApiOperation(value = "Recover Export Notification TypeEnum", notes = "Returns a Export Notification TypeEnum using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ExportNotificationTypeResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ExportNotificationTypeResponseV1> getExportNotificationType(
            @PathVariable("UUID") UUID uuid
    ){
        ExportNotificationTypeEntity exportNotificationTypeEntity = exportNotificationTypeService.getExportNotificationType(uuid);
        return new ResponseEntity<>(ExportNotificationTypeAdapter.adptFromExportNotificationTypeEntityToExportNotificationTypeResponse(exportNotificationTypeEntity), HttpStatus.OK);
    }

    @GetMapping("/export/notificationtype")
    @Transactional
    @ApiOperation(value = "Recover Export Notification TypeEnum", notes = "Returns a Export Notification TypeEnum using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ExportNotificationTypeResponseV1.class, responseContainer="List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<ExportNotificationTypeResponseV1>> getExportNotificationType(
    ){
        List<ExportNotificationTypeEntity> exportNotificationTypeEntityList = exportNotificationTypeService.getAllExportNotificationType();
        return new ResponseEntity<>(ExportNotificationTypeAdapter.adptFromExportNotificationTypeEntityToExportNotificationTypeResponseList(exportNotificationTypeEntityList), HttpStatus.OK);
    }

    @PutMapping("/export/notificationtype/{UUID}")
    @Transactional
    @ApiOperation(value = "Insert Export Notification TypeEnum", notes = "Insert Export Notification TypeEnum using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = ExportNotificationTypeResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ExportNotificationTypeResponseV1> updateExportNotificationType(
            @RequestBody ExportNotificationTypeRequestV1 exportNotificationTypeRequestV1,
            @PathVariable("UUID") UUID uuid
    )
    {
        ExportNotificationTypeEntity exportNotificationTypeEntity = exportNotificationTypeAdapter.adptFromExportNotificationTypeRequestToExportNotificationTypeEntityWithID(exportNotificationTypeRequestV1, uuid);
        exportNotificationTypeEntity = exportNotificationTypeService.updateExportNotificationType(exportNotificationTypeEntity);
        return new ResponseEntity<>(ExportNotificationTypeAdapter.adptFromExportNotificationTypeEntityToExportNotificationTypeResponse(exportNotificationTypeEntity), HttpStatus.OK);
    }

    @DeleteMapping("/export/notificationtype/{UUID}")
    @Transactional
    @ApiOperation(value = "Delete  Export Notification TypeEnum", notes = "Delete Export Notification TypeEnum using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity deleteExportNotificationType(
            @PathVariable("UUID") UUID uuid
    ){
        exportNotificationTypeService.deleteExportNotificationType(uuid);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PatchMapping("/export/notificationtype/{UUID}/active")
    @Transactional
    @ApiOperation(value = "Patch status active Export Notification TypeEnum", notes = "Patch status active Export Notification TypeEnum using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ExportNotificationTypeResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ExportNotificationTypeResponseV1> patchStatusExportNotificationType(
            @PathVariable UUID UUID
    ) {
        ExportNotificationTypeEntity exportNotificationTypeEntity = exportNotificationTypeService.patchActive(UUID);
        ExportNotificationTypeResponseV1 responseV1 = ExportNotificationTypeAdapter.adptFromExportNotificationTypeEntityToExportNotificationTypeResponse(exportNotificationTypeEntity);
        return new ResponseEntity<>(responseV1, HttpStatus.OK);
    }

    @ApiOperation(value="Export Notification TypeEnum Pagination", notes = "It retrieves Export Notification TypeEnum pagination")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = ClaimsResponsePaginationV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @GetMapping(value="export/notificationtype/pagination", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<ClaimsResponsePaginationV1> notificationTypePagination(
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @RequestParam(value = "page_size", defaultValue = "20") Integer pageSize,
            @RequestParam(value = "order_by", required = false) String orderBy,
            @RequestParam(value = "asc", required = false, defaultValue = "true") Boolean asc,
            @RequestParam(value = "description_notification", required = false) String descriptionNotification,
            @RequestParam(value = "code", required = false) String code,
            @RequestParam(value = "description", required = false) String description,
            @RequestParam(value = "is_active", required = false) Boolean isActive
    ){

        Pagination<ExportNotificationTypeEntity> exportNotificationTypeEntityPagination = exportNotificationTypeService.findExportNotificationType(page, pageSize, orderBy, asc, descriptionNotification, code, description, isActive);

        return new ResponseEntity<>(exportNotificationTypeAdapter.adptFromExportNotificationTypePaginationToNotificationResponsePagination(exportNotificationTypeEntityPagination), HttpStatus.OK);
    }
}
