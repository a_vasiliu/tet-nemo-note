package com.doing.nemo.claims.controller.payload.request.authority.practice;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AdministrativeAssociationRequestV1 implements Serializable {

    @JsonProperty(value = "practices_detail")
    private List<CarPracticeRequestV1> items;

    public List<CarPracticeRequestV1> getItems() {
        if(items == null){
            return null;
        }
        return new ArrayList<>(items);
    }

    public void setItems(List<CarPracticeRequestV1> items) {
        if(items != null)
        {
            this.items = new ArrayList<>(items);
        } else {
            this.items = null;
        }
    }

    @Override
    public String toString() {
        return "CarPracticeAssociationRequestV1{" +
                ", items=" + items +
                '}';
    }
}
