package com.doing.nemo.claims.controller;

import com.doing.nemo.claims.adapter.AttachmentTypeAdapter;
import com.doing.nemo.claims.common.validation.MessageCode;
import com.doing.nemo.claims.common.validation.RequestValidator;
import com.doing.nemo.claims.controller.payload.request.AttachmentTypeRequestV1;
import com.doing.nemo.claims.controller.payload.response.IdResponseV1;
import com.doing.nemo.claims.controller.payload.response.AttachmentTypeResponseV1;
import com.doing.nemo.claims.controller.payload.response.PaginationResponseV1;
import com.doing.nemo.claims.entity.enumerated.AttachmentEnum.GroupsEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsRepairEnum;
import com.doing.nemo.claims.entity.settings.AttachmentTypeEntity;
import com.doing.nemo.claims.service.AttachmentTypeService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/v1")
public class AttachmentTypeController {

    @Autowired
    private AttachmentTypeService attachmentTypeService;
    

    @Autowired
    private RequestValidator requestValidator;

    @PostMapping(value = "/attachment",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Insert Attachment TypeEnum", notes = "Insert attachment type using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = IdResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @Transactional
    public ResponseEntity<IdResponseV1> postInsertAttachmentType(
            @ApiParam(value = "Body of the Attachment TypeEnum to be created", required = true)
            @RequestBody AttachmentTypeRequestV1 attachmentTypeRequest,
            @ApiParam(value = "TypeEnum of complaint", required = true)
            @RequestParam(value = "type_complaint") ClaimsRepairEnum typeComplaint) {

        requestValidator.validateRequest(attachmentTypeRequest, MessageCode.E00X_1000);
        AttachmentTypeEntity attachmentTypeEntity = AttachmentTypeAdapter.getAttachmentTypeEntity(attachmentTypeRequest);
        attachmentTypeEntity.setTypeComplaint(typeComplaint);
        attachmentTypeEntity = attachmentTypeService.insertAttachmentType(attachmentTypeEntity);
        IdResponseV1 response = new IdResponseV1(attachmentTypeEntity.getId());
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @PutMapping(value = "/attachment/{UUID}",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Upload Attachment TypeEnum", notes = "Upload attachment type using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = AttachmentTypeResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @Transactional
    public ResponseEntity<AttachmentTypeResponseV1> putAttachmentType(
            @ApiParam(value = "UUID that identifies the Attachment TypeEnum to be modified", required = true)
            @PathVariable(name = "UUID") UUID uuid,
            @ApiParam(value = "Updated body of the Attachment TypeEnum", required = true)
            @RequestBody AttachmentTypeRequestV1 attachmentTypeRequest) {

        requestValidator.validateRequest(attachmentTypeRequest, MessageCode.E00X_1000);
        AttachmentTypeEntity attachmentTypeEntity = AttachmentTypeAdapter.getAttachmentTypeEntity(attachmentTypeRequest);
        attachmentTypeEntity = attachmentTypeService.updateAttachmentType(attachmentTypeEntity, uuid);
        AttachmentTypeResponseV1 response = AttachmentTypeAdapter.getAttachmentTypeAdapter(attachmentTypeEntity);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping(value = "/attachment/{UUID}",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Recover Attachment TypeEnum", notes = "Returns an Attachment TypeEnum using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = AttachmentTypeResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<AttachmentTypeResponseV1> getAttachmentType(
            @ApiParam(value = "UUID of the Attachment TypeEnum to be found", required = true)
            @PathVariable(name = "UUID") UUID uuid) {

        AttachmentTypeEntity attachmentTypeEntity = attachmentTypeService.getAttachmentType(uuid);
        AttachmentTypeResponseV1 response = AttachmentTypeAdapter.getAttachmentTypeAdapter(attachmentTypeEntity);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping(value = "/attachment",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Recover Attachment Types", notes = "Returns all the Attachment Types")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = AttachmentTypeResponseV1.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<AttachmentTypeResponseV1>> getAllAttachmentType(
            @ApiParam(value = "TypeEnum of complaint", required = true)
            @RequestParam(value = "type_complaint") ClaimsRepairEnum typeComplaint
    ) {

        List<AttachmentTypeEntity> attachmentTypeEntities = attachmentTypeService.getAllAttachmentType(typeComplaint);
        List<AttachmentTypeResponseV1> response = AttachmentTypeAdapter.getAttachmentTypeListAdapter(attachmentTypeEntities);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @DeleteMapping(value = "/attachment/{UUID}",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Delete Attachment TypeEnum", notes = "Delete an Attachment TypeEnum using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = AttachmentTypeResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @Transactional
    public ResponseEntity deleteAttachmentType(@PathVariable(name = "UUID") UUID uuid) {

        AttachmentTypeEntity attachmentTypeEntity = attachmentTypeService.deleteAttachmentType(uuid);
        if (attachmentTypeEntity == null)
            return new ResponseEntity<>(attachmentTypeEntity, HttpStatus.OK);
        AttachmentTypeResponseV1 response = AttachmentTypeAdapter.getAttachmentTypeAdapter(attachmentTypeEntity);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


    @PatchMapping("/attachment/{UUID}/active")
    @Transactional
    @ApiOperation(value = "Patch active status AttachmentType", notes = "Patch active status of AttachmentType using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = AttachmentTypeResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<AttachmentTypeResponseV1> patchStatusAttachmentType(
            @PathVariable UUID UUID
    ) {
        AttachmentTypeEntity attachmentTypeEntity = attachmentTypeService.updateStatus(UUID);
        AttachmentTypeResponseV1 responseV1 = AttachmentTypeAdapter.getAttachmentTypeAdapter(attachmentTypeEntity);
        return new ResponseEntity<>(responseV1, HttpStatus.OK);
    }

    @ApiOperation(value="Search AttachmentType", notes = "It retrieves antitheftservice data through one of more parameters")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = PaginationResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @GetMapping(value="/attachmenttype/pagination", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<PaginationResponseV1<AttachmentTypeResponseV1>> searchAttachmentType(
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @RequestParam(value = "page_size", defaultValue = "20") Integer pageSize,
            @RequestParam(value = "type_complaint", required = false) String claimsRepairEnum,
            @RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "groups", required = false) GroupsEnum groups,
            @RequestParam(value = "is_active", required = false) Boolean isActive,
            @RequestParam(value = "order_by", required = false) String orderBy,
            @RequestParam(value = "asc", required = false, defaultValue = "true") Boolean asc
    ){

        ClaimsRepairEnum typeComplaint = null;
        if(claimsRepairEnum != null)
            typeComplaint = ClaimsRepairEnum.create(claimsRepairEnum);

        List<AttachmentTypeEntity> listAttachmentType = attachmentTypeService.findAttachmentTypesFiltered(page,pageSize, typeComplaint, name, groups, isActive, orderBy, asc);

        Long itemCount = attachmentTypeService.countAttachmentTypeFiltered(typeComplaint, name, groups, isActive);

        PaginationResponseV1<AttachmentTypeResponseV1> paginationResponseV1 = AttachmentTypeAdapter.adptPagination(listAttachmentType, page, pageSize, itemCount);

        return new ResponseEntity<>(paginationResponseV1, HttpStatus.OK);
    }
}
