package com.doing.nemo.claims.controller.payload.response.authority;

import com.doing.nemo.claims.controller.payload.response.authority.AuthorityWorkingAttachmentResponseV1;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AuthorityWorkingsAttachmentResponseV1 implements Serializable {
    @JsonProperty("workings_attachments")
    private List<AuthorityWorkingAttachmentResponseV1> workingsAttachments;

    public List<AuthorityWorkingAttachmentResponseV1> getWorkingsAttachments() {
        if(workingsAttachments == null){
            return null;
        }
        return new ArrayList<>(workingsAttachments);
    }

    public void setWorkingsAttachments(List<AuthorityWorkingAttachmentResponseV1> workingsAttachments) {
        if(workingsAttachments != null)
        {
            this.workingsAttachments = new ArrayList<>(workingsAttachments);
        } else {
            this.workingsAttachments = null;
        }
    }

    @Override
    public String toString() {
        return "AuthorityWorkingsAttachmentResponseV1{" +
                "workingsAttachments=" + workingsAttachments +
                '}';
    }
}
