package com.doing.nemo.claims.controller.payload.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

public class DataManagementExportResponseV1 implements Serializable {

    private UUID id;

    @JsonProperty("user_import")
    private String user;

    @JsonProperty("user_firstname")
    private String userFirstname;

    @JsonProperty("user_lastname")
    private String userLastname;

    @JsonProperty("user_role")
    private String role;

    @JsonProperty("description")
    private String description;

    @JsonProperty("result")
    private String result;

    @JsonProperty("date")
    private String date;

    @JsonProperty("processing_files")
    private List<ExportingFileResponseV1> processingFiles = new LinkedList<>();

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public String getUserFirstname() {
        return userFirstname;
    }

    public void setUserFirstname(String userFirstname) {
        this.userFirstname = userFirstname;
    }

    public String getUserLastname() {
        return userLastname;
    }

    public void setUserLastname(String userLastname) {
        this.userLastname = userLastname;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public void setUser(String user) {
        this.user = user;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<ExportingFileResponseV1> getProcessingFiles() {
        if(processingFiles == null){
            return null;
        }
        return new ArrayList<>(processingFiles);
    }

    public void setProcessingFiles(List<ExportingFileResponseV1> processingFiles) {
        if(processingFiles != null)
        {
            this.processingFiles = new ArrayList<>(processingFiles);
        } else{
            this.processingFiles = null;
        }
    }

    @Override
    public String toString() {
        return "DataManagementExportResponseV1{" +
                "id=" + id +
                ", user='" + user + '\'' +
                ", userFirstname='" + userFirstname + '\'' +
                ", userLastname='" + userLastname + '\'' +
                ", role='" + role + '\'' +
                ", description='" + description + '\'' +
                ", result='" + result + '\'' +
                ", date='" + date + '\'' +
                ", processingFiles=" + processingFiles +
                '}';
    }

    public static class ExportingFileResponseV1 implements Serializable{

        private UUID id;

        @JsonProperty("attachment_id")
        private UUID attachmentId;

        @JsonProperty("file_name")
        private String fileName;

        @JsonProperty("file_size")
        private Integer fileSize;

        public ExportingFileResponseV1() {
        }

        public ExportingFileResponseV1(UUID id, UUID attachmentId, String fileName, Integer fileSize) {
            this.id = id;
            this.attachmentId = attachmentId;
            this.fileName = fileName;
            this.fileSize = fileSize;
        }

        public UUID getId() {
            return id;
        }

        public void setId(UUID id) {
            this.id = id;
        }

        public UUID getAttachmentId() {
            return attachmentId;
        }

        public void setAttachmentId(UUID attachmentId) {
            this.attachmentId = attachmentId;
        }

        public String getFileName() {
            return fileName;
        }

        public void setFileName(String fileName) {
            this.fileName = fileName;
        }

        public Integer getFileSize() {
            return fileSize;
        }

        public void setFileSize(Integer fileSize) {
            this.fileSize = fileSize;
        }

        @Override
        public String toString() {
            return "ExportingFileResponseV1{" +
                    "id=" + id +
                    ", attachmentId=" + attachmentId +
                    ", fileName='" + fileName + '\'' +
                    ", fileSize=" + fileSize +
                    '}';
        }
    }
}
