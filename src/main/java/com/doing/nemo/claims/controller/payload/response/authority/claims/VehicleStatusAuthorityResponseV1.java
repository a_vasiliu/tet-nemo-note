package com.doing.nemo.claims.controller.payload.response.authority.claims;

import com.doing.nemo.claims.entity.enumerated.AuthorityEnum.AuthorityVehicleStatusEnum;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;


@JsonIgnoreProperties(ignoreUnknown = true)
public class VehicleStatusAuthorityResponseV1 implements Serializable {
    @JsonProperty("chassis")
    private String chassis;

    @JsonProperty("status")
    private AuthorityVehicleStatusEnum status;

    public String getChassis() {
        return chassis;
    }

    public void setChassis(String chassis) {
        this.chassis = chassis;
    }

    public AuthorityVehicleStatusEnum getStatus() {
        return status;
    }

    public void setStatus(AuthorityVehicleStatusEnum status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "VehicleStatusAuthorityResponseV1{" +
                "chassis='" + chassis + '\'' +
                ", status=" + status +
                '}';
    }
}
