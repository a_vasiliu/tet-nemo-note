package com.doing.nemo.claims.controller.payload.response;

import com.doing.nemo.claims.entity.enumerated.ClaimsUpdateProcedureEnum;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class CheckUpdateProcedureResponseV1 implements Serializable {
    @JsonProperty("procedure")
    private ClaimsUpdateProcedureEnum procedure;

    public CheckUpdateProcedureResponseV1() {
    }

    public CheckUpdateProcedureResponseV1(ClaimsUpdateProcedureEnum procedure) {
        this.procedure = procedure;
    }

    public ClaimsUpdateProcedureEnum getProcedure() {
        return procedure;
    }

    public void setProcedure(ClaimsUpdateProcedureEnum procedure) {
        this.procedure = procedure;
    }
}
