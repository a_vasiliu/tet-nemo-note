package com.doing.nemo.claims.controller.payload.request.claims;

import com.doing.nemo.claims.controller.payload.request.cai.CaiDetailsRequest;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class CaiRequest implements Serializable {
    @JsonProperty("A")
    private CaiDetailsRequest vehicleA;

    @JsonProperty("B")
    private CaiDetailsRequest vehicleB;

    @JsonProperty("driver_side")
    private String driverSide;

    @JsonProperty("note")
    private String note;



    public CaiRequest() {
    }

    public CaiRequest(CaiDetailsRequest vehicleA, CaiDetailsRequest vehicleB, String driverSide, String note) {
        this.vehicleA = vehicleA;
        this.vehicleB = vehicleB;
        this.driverSide = driverSide;
        this.note = note;
    }

    public CaiDetailsRequest getVehicleA() {
        return vehicleA;
    }

    public void setVehicleA(CaiDetailsRequest vehicleA) {
        this.vehicleA = vehicleA;
    }

    public CaiDetailsRequest getVehicleB() {
        return vehicleB;
    }

    public void setVehicleB(CaiDetailsRequest vehicleB) {
        this.vehicleB = vehicleB;
    }

    public String getDriverSide() {
        return driverSide;
    }

    public void setDriverSide(String driverSide) {
        this.driverSide = driverSide;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public String toString() {
        return "CaiRequest{" +
                "vehicleA=" + vehicleA +
                ", vehicleB=" + vehicleB +
                ", driverSide='" + driverSide + '\'' +
                ", note='" + note + '\'' +
                '}';
    }
}
