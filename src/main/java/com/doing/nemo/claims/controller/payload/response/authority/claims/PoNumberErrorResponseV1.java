package com.doing.nemo.claims.controller.payload.response.authority.claims;


import com.doing.nemo.claims.validation.MessageCode;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class PoNumberErrorResponseV1  implements Serializable {
    @JsonProperty("po_number")
    private String poNumber;

    @JsonProperty("error")
    private MessageCode error;

    public String getPoNumber() {
        return poNumber;
    }

    public void setPoNumber(String poNumber) {
        this.poNumber = poNumber;
    }

    public MessageCode getError() {
        return error;
    }

    public void setError(MessageCode error) {
        this.error = error;
    }
}
