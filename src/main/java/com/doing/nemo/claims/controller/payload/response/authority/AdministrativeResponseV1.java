package com.doing.nemo.claims.controller.payload.response.authority;

import com.doing.nemo.claims.entity.enumerated.PracticeTypeEnum;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AdministrativeResponseV1 implements Serializable {

    @JsonProperty("workings")
    private List<PracticeAdministrativeListResponse> workings;

    public List<PracticeAdministrativeListResponse> getWorkings() {
        if(workings == null){
            return null;
        }
        return new ArrayList<>(workings);
    }

    public void setWorkings(List<PracticeAdministrativeListResponse> workings) {
        if(workings != null)
        {
            this.workings =  new ArrayList<>(workings);
        } else {
            this.workings = null;
        }
    }

    @Override
    public String toString() {
        return "AdministrativeResponseV1{" +
                "workings=" + workings +
                '}';
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class PracticeAdministrativeListResponse implements Serializable {
        @JsonProperty("practice_list")
        private List<PracticeAdministrativeResponse> practiceList;

        @JsonProperty("working_id")
        private String workingId;

        public List<PracticeAdministrativeResponse> getPracticeList() {
            if(practiceList == null){
                return null;
            }
            return new ArrayList<>(practiceList);
        }

        public void setPracticeList(List<PracticeAdministrativeResponse> practiceList) {
            if(practiceList != null)
            {
                this.practiceList =new ArrayList<>(practiceList);
            } else {
                this.practiceList = null;
            }
        }

        public String getWorkingId() {
            return workingId;
        }

        public void setWorkingId(String workingId) {
            this.workingId = workingId;
        }

        @Override
        public String toString() {
            return "PracticeAdministrativeListResponse{" +
                    "practiceList=" + practiceList +
                    ", workingId='" + workingId + '\'' +
                    '}';
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class PracticeAdministrativeResponse implements Serializable {
        @JsonProperty("id")
        private String id;

        @JsonProperty("practice_id")
        private Integer practiceId;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public Integer getPracticeId() {
            return practiceId;
        }

        public void setPracticeId(Integer practiceId) {
            this.practiceId = practiceId;
        }

        @Override
        public String toString() {
            return "PracticeAdministrativeResponse{" +
                    "id='" + id + '\'' +
                    ", practiceId=" + practiceId +
                    '}';
        }
    }

}
