package com.doing.nemo.claims.controller.payload.response.dataaccident;


import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.ReceivedItem.ItemToReceiveTypeEnum;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class ReceivedItemResponse implements Serializable {

    @JsonProperty("is_received")
    private Boolean isReceived;

    @JsonProperty("type")
    private ItemToReceiveTypeEnum type;

    @JsonProperty("date_received")
    private String dateReceived;

    @JsonProperty("report")
    private String report;

    @JsonProperty("note")
    private String note;

    public ReceivedItemResponse() {
    }

    public ReceivedItemResponse(Boolean isReceived, ItemToReceiveTypeEnum type, String dateReceived, String report, String note) {
        this.isReceived = isReceived;
        this.type = type;
        this.dateReceived = dateReceived;
        this.report = report;
        this.note = note;
    }

    public Boolean getReceived() {
        return isReceived;
    }

    public void setReceived(Boolean received) {
        isReceived = received;
    }

    public ItemToReceiveTypeEnum getType() {
        return type;
    }

    public void setType(ItemToReceiveTypeEnum type) {
        this.type = type;
    }

    public String getDateReceived() {
        return dateReceived;
    }

    public void setDateReceived(String dateReceived) {
        this.dateReceived = dateReceived;
    }

    public String getReport() {
        return report;
    }

    public void setReport(String report) {
        this.report = report;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public String toString() {
        return "ReceivedItem{" +
                "isReceived=" + isReceived +
                ", type=" + type +
                ", dateReceived=" + dateReceived +
                ", report='" + report + '\'' +
                ", note='" + note + '\'' +
                '}';
    }
}