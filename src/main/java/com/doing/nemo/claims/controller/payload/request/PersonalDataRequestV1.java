package com.doing.nemo.claims.controller.payload.request;

import com.doing.nemo.claims.entity.jsonb.personalData.FleetManagerPersonalData;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PersonalDataRequestV1 implements Serializable {

    @JsonProperty("customer_id")
    private String customerId = "";

    @JsonProperty("name")
    private String name = "";

    @JsonProperty("address")
    private String address = "";

    @JsonProperty("zip_code")
    private String zipCode;

    @JsonProperty("city")
    private String city = "";

    @JsonProperty("province")
    private String province = "";

    @JsonProperty("state")
    private String state = "";

    @JsonProperty("phone_number")
    private String phoneNumber = "";

    @JsonProperty("fax")
    private String fax = "";

    @JsonProperty("email")
    private String email = "";

    @JsonProperty("pec")
    private String pec = "";

    @JsonProperty("web_site")
    private String webSite = "";

    @JsonProperty("personal_group")
    private String personalGroup = "";

    @JsonProperty("contact")
    private String contact = "";

    @JsonProperty("vat_number")
    private String vatNumber = "";

    @JsonProperty("fiscal_code")
    private String fiscalCode = "";

    @JsonProperty("personal_father_id")
    private String personalFatherId;

    @JsonProperty("fleet_managers")
    private List<FleetManagerPersonalData> fleetManagerPersonalData;

    @JsonProperty("contracts")
    private List<ContractRequestV1> contractList;

    @JsonProperty("insurance_policies")
    private List<InsurancePolicyPersonalDataRequestV1> insurancePolicyPersonalDataList;

    @JsonProperty("custom_type_risks")
    private List<CustomTypeRiskRequestV1> customTypeRiskList;

    @JsonProperty("is_active")
    private Boolean isActive = true;

    public String getPec() {
        return pec;
    }

    public void setPec(String pec) {
        this.pec = pec;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebSite() {
        return webSite;
    }

    public void setWebSite(String webSite) {
        this.webSite = webSite;
    }

    public String getPersonalGroup() {
        return personalGroup;
    }

    public void setPersonalGroup(String personalGroup) {
        this.personalGroup = personalGroup;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getVatNumber() {
        return vatNumber;
    }

    public void setVatNumber(String vatNumber) {
        this.vatNumber = vatNumber;
    }

    public String getFiscalCode() {
        return fiscalCode;
    }

    public void setFiscalCode(String fiscalCode) {
        this.fiscalCode = fiscalCode;
    }

    public String getPersonalFatherId() {
        return personalFatherId;
    }

    public void setPersonalFatherId(String personalFatherId) {
        this.personalFatherId = personalFatherId;
    }

    public List<FleetManagerPersonalData> getFleetManagerPersonalData() {
        if(fleetManagerPersonalData == null){
            return null;
        }
        return new ArrayList<>(fleetManagerPersonalData);
    }

    public void setFleetManagerPersonalData(List<FleetManagerPersonalData> fleetManagerPersonalData) {
        if(fleetManagerPersonalData != null)
        {
            this.fleetManagerPersonalData = new ArrayList<>(fleetManagerPersonalData);
        } else {
            this.fleetManagerPersonalData = null;
        }
    }

    public List<ContractRequestV1> getContractList() {
        if(contractList == null){
            return null;
        }
        return new ArrayList<>(contractList);
    }

    public void setContractList(List<ContractRequestV1> contractList) {
        if(contractList != null)
        {
            this.contractList = new ArrayList<>(contractList);
        } else {
            this.contractList = null;
        }
    }

    public List<InsurancePolicyPersonalDataRequestV1> getInsurancePolicyPersonalDataList() {
        if(insurancePolicyPersonalDataList == null){
            return null;
        }
        return new ArrayList<>(insurancePolicyPersonalDataList);
    }

    public void setInsurancePolicyPersonalDataList(List<InsurancePolicyPersonalDataRequestV1> insurancePolicyPersonalDataList) {
        if(insurancePolicyPersonalDataList != null)
        {
            this.insurancePolicyPersonalDataList = new ArrayList<>(insurancePolicyPersonalDataList);
        } else {
            this.insurancePolicyPersonalDataList = null;
        }
    }

    public List<CustomTypeRiskRequestV1> getCustomTypeRiskList() {
        if(customTypeRiskList == null){
            return null;
        }
        return new ArrayList<>(customTypeRiskList);
    }

    public void setCustomTypeRiskList(List<CustomTypeRiskRequestV1> customTypeRiskList) {
        if(customTypeRiskList != null)
        {
            this.customTypeRiskList = new ArrayList<>(customTypeRiskList);
        } else {
            this.customTypeRiskList = null;
        }
    }

    public Boolean getActive() {
        return isActive;
    }

    @JsonSetter
    public void setActive(Boolean active) {
        if (active != null)
            isActive = active;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }
}
