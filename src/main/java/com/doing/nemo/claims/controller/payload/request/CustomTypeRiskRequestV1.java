package com.doing.nemo.claims.controller.payload.request;

import com.doing.nemo.claims.entity.settings.ClaimsTypeEntity;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;


public class CustomTypeRiskRequestV1 implements Serializable {

    @JsonProperty("claims_type")
    private ClaimsTypeEntity claimsType;

    @JsonProperty("flow")
    private String flow = "";

    @JsonProperty("type_of_chargeback")
    private String typeOfChargeback = "";

    public CustomTypeRiskRequestV1() {
    }

    public CustomTypeRiskRequestV1(ClaimsTypeEntity claimsType, String flow, String typeOfChargeback) {
        this.claimsType = claimsType;
        this.flow = flow;
        this.typeOfChargeback = typeOfChargeback;
    }

    public ClaimsTypeEntity getClaimsType() {
        return claimsType;
    }

    public void setClaimsType(ClaimsTypeEntity claimsType) {
        this.claimsType = claimsType;
    }

    public String getFlow() {
        return flow;
    }

    public void setFlow(String flow) {
        this.flow = flow;
    }

    public String getTypeOfChargeback() {
        return typeOfChargeback;
    }

    public void setTypeOfChargeback(String typeOfChargeback) {
        this.typeOfChargeback = typeOfChargeback;
    }

    @Override
    public String toString() {
        return "CustomTypeRiskRequestV1{" +
                "claimsType=" + claimsType +
                ", flow='" + flow + '\'' +
                ", typeOfChargeback='" + typeOfChargeback + '\'' +
                '}';
    }
}
