package com.doing.nemo.claims.controller.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.UUID;

public class NotificationTypeRequestV1 implements Serializable {

    private UUID id;

    @NotNull
    @JsonProperty("description")
    private String description;

    @JsonProperty("is_active")
    private Boolean isActive = true;

    @JsonProperty("order_id")
    private Integer orderId;

    public NotificationTypeRequestV1(UUID id, @NotNull String description, Boolean isActive, Integer orderId) {
        this.id = id;
        this.description = description;
        this.isActive = isActive;
        this.orderId = orderId;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getActive() {
        return isActive;
    }

    @JsonSetter
    public void setActive(Boolean active) {
        if (active != null)
            isActive = active;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    @Override
    public String toString() {
        return "NotificationTypeRequestV1{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", isActive=" + isActive +
                ", orderId=" + orderId +
                '}';
    }
}
