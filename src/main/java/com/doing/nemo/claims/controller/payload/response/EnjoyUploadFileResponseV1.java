package com.doing.nemo.claims.controller.payload.response;

import com.doing.nemo.claims.controller.payload.UploadFileClaimsRequestV1;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class EnjoyUploadFileResponseV1 implements Serializable {
    @JsonProperty("data")
    private List<UploadFileResponseV1> data;

    public EnjoyUploadFileResponseV1() {
    }

    public EnjoyUploadFileResponseV1(List<UploadFileResponseV1> data) {
        if(data != null)
        {
            this.data = new ArrayList<>(data);
        } else {
            this.data = null;
        }
    }

    public List<UploadFileResponseV1> getData() {
        if(data == null){
            return null;
        }
        return new ArrayList<>(data);
    }

    public void setData(List<UploadFileResponseV1> data) {
        if(data != null)
        {
            this.data = new ArrayList<>(data);
        } else {
            this.data = null;
        }
    }

    @Override
    public String toString() {
        return "EnjoyUploadFileResponseV1{" +
                "data=" + data +
                '}';
    }
}
