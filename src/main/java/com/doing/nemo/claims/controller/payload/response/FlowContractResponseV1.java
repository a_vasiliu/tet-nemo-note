package com.doing.nemo.claims.controller.payload.response;

import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class FlowContractResponseV1 implements Serializable {

    private String id;

    @JsonProperty("claims_type")
    private String claimsTypeEntity;

    @JsonProperty("claims_accident_type")
    private DataAccidentTypeAccidentEnum calimsAccident;

    @JsonProperty("flow")
    private String flow;

    @JsonProperty("rebilling")
    private String rebilling;

    public FlowContractResponseV1() {
    }

    public FlowContractResponseV1(String id, String claimsTypeEntity, DataAccidentTypeAccidentEnum calimsAccident, String flow, String rebilling) {
        this.id = id;
        this.claimsTypeEntity = claimsTypeEntity;
        this.calimsAccident = calimsAccident;
        this.flow = flow;
        this.rebilling = rebilling;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getClaimsTypeEntity() {
        return claimsTypeEntity;
    }

    public void setClaimsTypeEntity(String claimsTypeEntity) {
        this.claimsTypeEntity = claimsTypeEntity;
    }

    public String getFlow() {
        return flow;
    }

    public void setFlow(String flow) {
        this.flow = flow;
    }

    public String getRebilling() {
        return rebilling;
    }

    public void setRebilling(String rebilling) {
        this.rebilling = rebilling;
    }

    public DataAccidentTypeAccidentEnum getCalimsAccident() {
        return calimsAccident;
    }

    public void setCalimsAccident(DataAccidentTypeAccidentEnum calimsAccident) {
        this.calimsAccident = calimsAccident;
    }

    @Override
    public String toString() {
        return "FlowContractResponseV1{" +
                "id='" + id + '\'' +
                ", claimsTypeEntity='" + claimsTypeEntity + '\'' +
                ", calimsAccident=" + calimsAccident +
                ", flow='" + flow + '\'' +
                ", rebilling='" + rebilling + '\'' +
                '}';
    }
}
