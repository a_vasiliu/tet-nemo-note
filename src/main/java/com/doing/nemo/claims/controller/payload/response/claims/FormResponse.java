package com.doing.nemo.claims.controller.payload.response.claims;

import com.doing.nemo.claims.controller.payload.response.forms.AttachmentResponse;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class FormResponse implements Serializable {


    @JsonProperty("attachment")
    private List<AttachmentResponse> attachment;

    public List<AttachmentResponse> getAttachment() {
        if(attachment == null){
            return null;
        }
        return new ArrayList<>(attachment);
    }

    public void setAttachment(List<AttachmentResponse> attachment) {
        if(attachment != null)
        {
            this.attachment =new ArrayList<>(attachment);
        } else {
            this.attachment = null;
        }
    }

    @Override
    public String toString() {
        return "FormResponseV1{" +
                "attachment=" + attachment +
                '}';
    }



}