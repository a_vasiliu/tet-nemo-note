package com.doing.nemo.claims.controller.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class NemoAuthExternalRequestV1 {
    @JsonProperty("user_id")
    private String userId;
    @JsonProperty("email")
    private String email;


    private NemoAuthExternalMetadataRequestV1 metadata;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public NemoAuthExternalMetadataRequestV1 getMetadata() {
        return metadata;
    }

    public void setMetadata(NemoAuthExternalMetadataRequestV1 metadata) {
        this.metadata = metadata;
    }

}
