package com.doing.nemo.claims.controller;

import com.doing.nemo.claims.controller.payload.request.ProfileRequestV1;
import com.doing.nemo.claims.controller.payload.response.IdentifierResponseV1;
import com.doing.nemo.claims.controller.payload.response.PermissionsResponseV1;
import com.doing.nemo.claims.controller.payload.response.ProfileModulePermissionsDetailResponseV1;
import com.doing.nemo.claims.controller.payload.response.ProfilesResponseV1;
import com.doing.nemo.claims.service.PermissionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/v1")
@Api("Claims Permission")
public class PermissionController {

    private static Logger LOGGER = LoggerFactory.getLogger(PermissionController.class);

    @Autowired
    private PermissionService permissionService;



    // Recuper tutti i ruoli legati a claims sottoforma di profili

    @GetMapping(value ="/claims/profiles")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ProfilesResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ProfilesResponseV1> getClaimsProfiles(
    ) throws IOException {


       return new ResponseEntity(permissionService.getProfiles(), HttpStatus.OK);
    }


    //recupera tutti i permessi associati ad un profilo (ruolo)

    @GetMapping(value ="/claims/permissions/profile/{ID_PROFILE}")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ProfileModulePermissionsDetailResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ProfileModulePermissionsDetailResponseV1> getClaimsProfiles(
            @PathVariable("ID_PROFILE") String idProfilo
    ) throws IOException {


        return new ResponseEntity(permissionService.getPermissionByProfile(idProfilo), HttpStatus.OK);
    }


    //rotta che che recupera tutti i permessi associati ad un modulo
    @GetMapping(value ="/claims/permissions/module")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = PermissionsResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<PermissionsResponseV1> getClaimsPermission(
    ) throws IOException {


        return new ResponseEntity(permissionService.getPermissionByModule(), HttpStatus.OK);
    }


    //rotta che che recupera tutti i permessi associati ad un modulo
    @PutMapping(value ="/claims/permissions/profile/{ID_PROFILE}")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "No Content"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity updateProfile(
            @PathVariable("ID_PROFILE") String idProfilo,
            @RequestBody ProfileRequestV1 request
    ) throws IOException {


        permissionService.updateProfile(idProfilo,request);

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @PostMapping(value ="/claims/permission/profile",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "OK"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<IdentifierResponseV1> createProfileCustom(@RequestBody ProfileRequestV1 request) throws IOException {
        return new ResponseEntity<>(permissionService.createCustomProfile(request), HttpStatus.OK);
    }

    @DeleteMapping(value ="/claims/profile/{ID_PROFILE}")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "OK"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity deleteProfile(
            @PathVariable("ID_PROFILE") String idProfilo
    ) throws IOException {
        permissionService.deleteProfile(idProfilo);
        return new ResponseEntity( HttpStatus.OK);
    }


}
