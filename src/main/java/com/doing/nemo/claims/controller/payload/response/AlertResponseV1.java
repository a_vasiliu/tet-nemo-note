package com.doing.nemo.claims.controller.payload.response;

import com.doing.nemo.claims.entity.enumerated.AlertEnum.AlertReadersEnum;
import com.doing.nemo.claims.entity.jsonb.forms.jsonbForms.Attachment;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@ApiModel
public class AlertResponseV1 implements Serializable {

    @JsonProperty("module")
    private String module;                      //Potrebbe essere un enum

    @JsonProperty("readers")
    private List<AlertReadersEnum> readers;   //Obbligatorio

    @JsonProperty("attachments")
    private List<Attachment> attachments;

    @JsonProperty("alert_id")
    private UUID alertId;

    @JsonProperty("created_by")
    private String createdBy;                   //Obbligatorio

    @JsonProperty("user_id")
    private UUID userId;

    @JsonProperty("commodity_id")
    private UUID commodityId;                  //Obbligatorio

    @JsonProperty("ald_commodity_id")
    private Integer aldCommodityId;           //Obbligatorio

    @JsonProperty("ald_order_id")
    private Integer aldOrderId;               //Obbligatorio

    @JsonProperty("subject")
    private String subject;                     //Obbligatorio

    @JsonProperty("text")
    private String text;                        //Obbligatorio

    @JsonProperty("thread_id")
    private UUID threadId;                      //Obbligatorio

    @JsonProperty("is_read")
    private Boolean isRead;                  //Obbligatorio

    @JsonProperty("created_at")
    private String createdAt;                //Obbligatorio

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public List<AlertReadersEnum> getReaders() {
        if(readers == null){
            return null;
        }
        return new ArrayList<>(readers);
    }

    public void setReaders(List<AlertReadersEnum> readers) {
        if(readers != null)
        {
            this.readers = new ArrayList<>(readers);
        } else {
            this.readers = null;
        }
    }

    public List<Attachment> getAttachments() {
        if(attachments == null){
            return null;
        }
        return new ArrayList<>(attachments);
    }

    public void setAttachments(List<Attachment> attachments) {
        if(attachments != null)
        {
            this.attachments = new ArrayList<>(attachments);
        } else {
            this.attachments = null;
        }
    }

    public UUID getAlertId() {
        return alertId;
    }

    public void setAlertId(UUID alertId) {
        this.alertId = alertId;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public UUID getCommodityId() {
        return commodityId;
    }

    public void setCommodityId(UUID commodityId) {
        this.commodityId = commodityId;
    }

    public Integer getAldCommodityId() {
        return aldCommodityId;
    }

    public void setAldCommodityId(Integer aldCommodityId) {
        this.aldCommodityId = aldCommodityId;
    }

    public Integer getAldOrderId() {
        return aldOrderId;
    }

    public void setAldOrderId(Integer aldOrderId) {
        this.aldOrderId = aldOrderId;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public UUID getThreadId() {
        return threadId;
    }

    public void setThreadId(UUID threadId) {
        this.threadId = threadId;
    }

    public Boolean getRead() {
        return isRead;
    }

    public void setRead(Boolean read) {
        isRead = read;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public String toString() {
        return "AlertResponseV1{" +
                "module='" + module + '\'' +
                ", readers=" + readers +
                ", attachments=" + attachments +
                ", alertId=" + alertId +
                ", createdBy='" + createdBy + '\'' +
                ", userId=" + userId +
                ", commodityId=" + commodityId +
                ", aldCommodityId=" + aldCommodityId +
                ", aldOrderId=" + aldOrderId +
                ", subject='" + subject + '\'' +
                ", text='" + text + '\'' +
                ", threadId=" + threadId +
                ", isRead=" + isRead +
                ", createdAt=" + createdAt +
                '}';
    }
}
