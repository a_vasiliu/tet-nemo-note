package com.doing.nemo.claims.controller.payload.response;

/* Example

{
    "access_token": "ZmY4NjQyNWUyNzJkY2UzZTQ1MDAwYmYzYThlMGU4ZGNjYjY4ZDhmY2IxZmQ4OWY0MDlmYjJmMDdlOTdkMzVkMw",
    "expires_in": 3600,
    "token_type": "bearer",
    "scope": "user",
    "refresh_token": "Y2ZjNTA1NmNmNmY1YThlZjRlNmU0ZTRlMTNkMDE3Yzc0NmVlMDZmNjA4YjE4ZjU2YTAwMWY1ZDE1MDEzMzBmNg"
}
 */


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class NemoAuthResponseV1 implements Serializable {

    @JsonProperty("access_token")
    private String accessToken;

    @JsonProperty("expires_in")
    private String expiresIn;

    @JsonProperty("token_type")
    private String tokenType;

    @JsonProperty("scope")
    private String scope;

    @JsonProperty("refresh_token")
    private String refreshToken;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(String expiresIn) {
        this.expiresIn = expiresIn;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    @Override
    public String toString() {
        return "NemoAuthResponseV1{" +
                "accessToken='" + accessToken + '\'' +
                ", expiresIn='" + expiresIn + '\'' +
                ", tokenType='" + tokenType + '\'' +
                ", scope='" + scope + '\'' +
                ", refreshToken='" + refreshToken + '\'' +
                '}';
    }
}
