package com.doing.nemo.claims.controller.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;

import java.io.Serializable;

public class ConfigureEvidenceRequestV1 implements Serializable {

    @JsonProperty("practical_state_it")
    private String practicalStateIt;

    @JsonProperty("practical_state_en")
    private String practicalStateEn;

    @JsonProperty("days_of_stay_in_the_state")
    private Long daysOfStayInTheState;

    @JsonProperty("is_active")
    private Boolean isActive = true;

    public ConfigureEvidenceRequestV1() {
    }

    public ConfigureEvidenceRequestV1(String practicalStateIt, String practicalStateEn, Long daysOfStayInTheState, Boolean isActive) {
        this.practicalStateIt = practicalStateIt;
        this.practicalStateEn = practicalStateEn;
        this.daysOfStayInTheState = daysOfStayInTheState;
        this.isActive = isActive;
    }

    public String getPracticalStateIt() {
        return practicalStateIt;
    }

    public void setPracticalStateIt(String practicalStateIt) {
        this.practicalStateIt = practicalStateIt;
    }

    public String getPracticalStateEn() {
        return practicalStateEn;
    }

    public void setPracticalStateEn(String practicalStateEn) {
        this.practicalStateEn = practicalStateEn;
    }

    public Long getDaysOfStayInTheState() {
        return daysOfStayInTheState;
    }

    public void setDaysOfStayInTheState(Long daysOfStayInTheState) {
        this.daysOfStayInTheState = daysOfStayInTheState;
    }

    public Boolean getActive() {
        return isActive;
    }

    @JsonSetter
    public void setActive(Boolean active) {
        if (active != null)
            isActive = active;
    }

    @Override
    public String toString() {
        return "ConfigureEvidenceRequestV1{" +
                "practicalStateIt='" + practicalStateIt + '\'' +
                ", practicalStateEn='" + practicalStateEn + '\'' +
                ", daysOfStayInTheState=" + daysOfStayInTheState +
                ", isActive=" + isActive +
                '}';
    }
}
