package com.doing.nemo.claims.controller.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class ClaimsPatchReceptionsRequestV1 implements Serializable {

    @JsonProperty("first_key_reception")
    private String firstKeyReception;

    @JsonProperty("second_key_reception")
    private String secondKeyReception;

    @JsonProperty("original_complaint_reception")
    private String originalComplaintReception;

    @JsonProperty("copy_complaint_reception")
    private String copyComplaintReception;

    @JsonProperty("original_report_reception")
    private String originalReportReception;

    @JsonProperty("copy_report_reception")
    private String copyReportReception;

    public ClaimsPatchReceptionsRequestV1(){

    }

    public ClaimsPatchReceptionsRequestV1(String firstKeyReception, String secondKeyReception, String originalComplaintReception, String copyComplaintReception, String originalReportReception, String copyReportReception) {
        this.firstKeyReception = firstKeyReception;
        this.secondKeyReception = secondKeyReception;
        this.originalComplaintReception = originalComplaintReception;
        this.copyComplaintReception = copyComplaintReception;
        this.originalReportReception = originalReportReception;
        this.copyReportReception = copyReportReception;
    }

    public String getFirstKeyReception() {
        return firstKeyReception;
    }

    public void setFirstKeyReception(String firstKeyReception) {
        this.firstKeyReception = firstKeyReception;
    }

    public String getSecondKeyReception() {
        return secondKeyReception;
    }

    public void setSecondKeyReception(String secondKeyReception) {
        this.secondKeyReception = secondKeyReception;
    }

    public String getOriginalComplaintReception() {
        return originalComplaintReception;
    }

    public void setOriginalComplaintReception(String originalComplaintReception) {
        this.originalComplaintReception = originalComplaintReception;
    }

    public String getCopyComplaintReception() {
        return copyComplaintReception;
    }

    public void setCopyComplaintReception(String copyComplaintReception) {
        this.copyComplaintReception = copyComplaintReception;
    }

    public String getOriginalReportReception() {
        return originalReportReception;
    }

    public void setOriginalReportReception(String originalReportReception) {
        this.originalReportReception = originalReportReception;
    }

    public String getCopyReportReception() {
        return copyReportReception;
    }

    public void setCopyReportReception(String copyReportReception) {
        this.copyReportReception = copyReportReception;
    }
}
