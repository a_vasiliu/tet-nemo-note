package com.doing.nemo.claims.controller.payload.response.practice;

import com.doing.nemo.claims.entity.enumerated.AuthorityEnum.AuthorityPracticeStatusEnum;
import com.doing.nemo.claims.entity.enumerated.AuthorityEnum.AuthorityWorkingTypeEnum;
import com.doing.nemo.claims.entity.enumerated.ProcessingTypeEnum;
import com.doing.nemo.claims.entity.jsonb.CommodityDetails;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProcessingResponseV1 implements Serializable {

    private static final long serialVersionUID = 816814485243599654L;

    @JsonProperty("processing_type")
    private ProcessingTypeEnum processingType;

    @JsonProperty("processing_date")
    private String processingDate;

    @JsonProperty("dossier_id")
    private String dossierId;

    @JsonProperty("working_id")
    private String workingId;

    @JsonProperty("working_number")
    private String workingNumber;

    @JsonProperty("dossier_number")
    private String dossierNumber;

    @JsonProperty("processing_authority_date")
    private String processingAuthorityDate;

    @JsonProperty("commodity_details")
    private CommodityDetails commodityDetails;

    @JsonProperty("status")
    private AuthorityPracticeStatusEnum status;

    @JsonProperty("working_type")
    private AuthorityWorkingTypeEnum workingType;

    public AuthorityPracticeStatusEnum getStatus() {
        return status;
    }

    public void setStatus(AuthorityPracticeStatusEnum status) {
        this.status = status;
    }

    public AuthorityWorkingTypeEnum getWorkingType() {
        return workingType;
    }

    public void setWorkingType(AuthorityWorkingTypeEnum workingType) {
        this.workingType = workingType;
    }

    public ProcessingTypeEnum getProcessingType() {
        return processingType;
    }

    public void setProcessingType(ProcessingTypeEnum processingType) {
        this.processingType = processingType;
    }

    public String getProcessingDate() {
        return processingDate;
    }

    public void setProcessingDate(String processingDate) {
        this.processingDate = processingDate;
    }

    public String getProcessingAuthorityDate() {
        return processingAuthorityDate;
    }

    public void setProcessingAuthorityDate(String processingAuthorityDate) {
        this.processingAuthorityDate = processingAuthorityDate;
    }

    public String getDossierId() {
        return dossierId;
    }

    public void setDossierId(String dossierId) {
        this.dossierId = dossierId;
    }

    public String getWorkingId() {
        return workingId;
    }

    public void setWorkingId(String workingId) {
        this.workingId = workingId;
    }

    public String getWorkingNumber() {
        return workingNumber;
    }

    public void setWorkingNumber(String workingNumber) {
        this.workingNumber = workingNumber;
    }

    public String getDossierNumber() {
        return dossierNumber;
    }

    public void setDossierNumber(String dossierNumber) {
        this.dossierNumber = dossierNumber;
    }

    public CommodityDetails getCommodityDetails() {
        return commodityDetails;
    }

    public void setCommodityDetails(CommodityDetails commodityDetails) {
        this.commodityDetails = commodityDetails;
    }

}
