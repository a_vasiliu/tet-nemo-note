package com.doing.nemo.claims.controller.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.UUID;

public class InsuranceManagerRequestV1 implements Serializable {

    @JsonProperty("id")
    private UUID id;

    @JsonProperty("code")
    private String code = "";

    @NotNull
    @JsonProperty("name")
    private String name;

    @JsonProperty("address")
    private String address = "";

    @JsonProperty("zip_code")
    private String zipCode;

    @JsonProperty("city")
    private String city = "";

    @JsonProperty("province")
    private String province = "";

    @JsonProperty("state")
    private String state = "";

    @JsonProperty("reference_person")
    private String referencePerson = "";

    @JsonProperty("telephone")
    private String telephone = "";

    @JsonProperty("fax")
    private String fax = "";

    @JsonProperty("email")
    private String email = "";

    @JsonProperty("web_site")
    private String webSite = "";

    @JsonProperty("is_active")
    private Boolean isActive = true;


    public InsuranceManagerRequestV1(UUID id, String code, @NotNull String name, String address, String zipCode, String city, String province, String state, String referencePerson, String telephone, String fax, String email, String webSite, Boolean isActive) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.address = address;
        this.zipCode = zipCode;
        this.city = city;
        this.province = province;
        this.state = state;
        this.referencePerson = referencePerson;
        this.telephone = telephone;
        this.fax = fax;
        this.email = email;
        this.webSite = webSite;
        this.isActive = isActive;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getReferencePerson() {
        return referencePerson;
    }

    public void setReferencePerson(String referencePerson) {
        this.referencePerson = referencePerson;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebSite() {
        return webSite;
    }

    public void setWebSite(String webSite) {
        this.webSite = webSite;
    }

    public Boolean getActive() {
        return isActive;
    }


    @JsonSetter
    public void setActive(Boolean active) {
        if (active != null)
            isActive = active;
    }
}
