package com.doing.nemo.claims.controller.payload.request.messaging;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

public class Identity implements Serializable {

    @Email
    @JsonProperty("email")
    private String email;

    @JsonProperty("name")
    private String name;

    @JsonProperty("recipient_type")
    private String recipientType;

    public Identity() {
    }

    public Identity(@Email String email, String name, String recipientType) {
        this.email = email;
        this.name = name;
        this.recipientType = recipientType;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRecipientType() {
        return recipientType;
    }

    public void setRecipientType(String recipientType) {
        this.recipientType = recipientType;
    }

    @Override
    public String toString() {
        return "Identity{" +
                "email='" + email + '\'' +
                ", name='" + name + '\'' +
                ", recipientType='" + recipientType + '\'' +
                '}';
    }
}
