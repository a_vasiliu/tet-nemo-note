package com.doing.nemo.claims.controller.payload.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.UUID;

public class ExportSupplierCodeResponseV1 implements Serializable {

    @JsonProperty("id")
    private UUID id;

    @JsonProperty("vat_number")
    private String vatNumber;

    @JsonProperty("denomination")
    private String denomination;

    @JsonProperty("code_to_export")
    private String codeToExport;

    @JsonProperty("is_active")
    private Boolean isActive;

    public ExportSupplierCodeResponseV1() {
    }

    public ExportSupplierCodeResponseV1(UUID id, String vatNumber, String denomination, String codeToExport, Boolean isActive) {
        this.id = id;
        this.vatNumber = vatNumber;
        this.denomination = denomination;
        this.codeToExport = codeToExport;
        this.isActive = isActive;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getVatNumber() {
        return vatNumber;
    }

    public void setVatNumber(String vatNumber) {
        this.vatNumber = vatNumber;
    }

    public String getDenomination() {
        return denomination;
    }

    public void setDenomination(String denomination) {
        this.denomination = denomination;
    }

    public String getCodeToExport() {
        return codeToExport;
    }

    public void setCodeToExport(String codeToExport) {
        this.codeToExport = codeToExport;
    }

    @JsonIgnore
    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    @Override
    public String toString() {
        return "ExportSupplierCodeResponseV1{" +
                "id=" + id +
                ", vatNumber='" + vatNumber + '\'' +
                ", denomination='" + denomination + '\'' +
                ", codeToExport='" + codeToExport + '\'' +
                ", isActive=" + isActive +
                '}';
    }
}
