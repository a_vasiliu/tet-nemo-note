package com.doing.nemo.claims.controller.payload.response.registration;

import com.doing.nemo.middleware.client.config.YearMonthDayInstantDeserializer;
import com.doing.nemo.middleware.client.config.YearMonthDayInstantSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

public class PlateRegistrationHistoryItem implements Serializable {
    @JsonProperty("plate")
    private String plate;
    @JsonProperty("valid_from")
    @JsonSerialize(
            using = YearMonthDayInstantSerializer.class
    )
    @JsonDeserialize(
            using = YearMonthDayInstantDeserializer.class
    )
    private Instant validFrom;
    @JsonProperty("valid_to")
    @JsonSerialize(
            using = YearMonthDayInstantSerializer.class
    )
    @JsonDeserialize(
            using = YearMonthDayInstantDeserializer.class
    )
    private Instant validTo;
    @JsonProperty("registration_date")
    @JsonSerialize(
            using = YearMonthDayInstantSerializer.class
    )
    @JsonDeserialize(
            using = YearMonthDayInstantDeserializer.class
    )
    private Instant registrationDate;

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public Instant getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Instant validFrom) {
        this.validFrom = validFrom;
    }

    public Instant getValidTo() {
        return validTo;
    }

    public void setValidTo(Instant validTo) {
        this.validTo = validTo;
    }

    public Instant getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Instant registrationDate) {
        this.registrationDate = registrationDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PlateRegistrationHistoryItem that = (PlateRegistrationHistoryItem) o;
        return plate.equals(that.plate) && validFrom.equals(that.validFrom) && validTo.equals(that.validTo) && registrationDate.equals(that.registrationDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(plate, validFrom, validTo, registrationDate);
    }
}
