package com.doing.nemo.claims.controller.payload.request.damaged;

import com.doing.nemo.claims.controller.payload.request.insurancecompany.*;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class InsuranceCompanyRequest implements Serializable {

    @JsonProperty("is_card")
    private Boolean isCard = false;

    /////////////////////////////////////////////////////////
    //da eliminare perchè sarebbe company
    //@JsonProperty("denomination")
    // private String denomination;
    /////////////////////////////////////////////////////////

    @JsonProperty("id")
    private String id;
    @JsonProperty("tpl")
    private TplRequest tpl;
    @JsonProperty("theft")
    private TheftRequest theft;
    @JsonProperty("material_damage")
    private MaterialDamageRequest materialDamage;
    @JsonProperty("pai")
    private PaiRequest pai;
    @JsonProperty("legal_cost")
    private LegalCostRequest legalCost;
    @JsonProperty("kasko")
    private KaskoRequest kasko;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public TplRequest getTpl() {
        return tpl;
    }

    public void setTpl(TplRequest tpl) {
        this.tpl = tpl;
    }

    public TheftRequest getTheft() {
        return theft;
    }

    public void setTheft(TheftRequest theft) {
        this.theft = theft;
    }

    public MaterialDamageRequest getMaterialDamage() {
        return materialDamage;
    }

    public void setMaterialDamage(MaterialDamageRequest materialDamage) {
        this.materialDamage = materialDamage;
    }

    public PaiRequest getPai() {
        return pai;
    }

    public void setPai(PaiRequest pai) {
        this.pai = pai;
    }

    public LegalCostRequest getLegalCost() {
        return legalCost;
    }

    public void setLegalCost(LegalCostRequest legalCost) {
        this.legalCost = legalCost;
    }

    public Boolean getCard() {
        return isCard;
    }

    public void setCard(Boolean card) {
        isCard = card;
    }

    public KaskoRequest getKasko() {
        return kasko;
    }

    public void setKasko(KaskoRequest kasko) {
        this.kasko = kasko;
    }
}