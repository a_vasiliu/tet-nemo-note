package com.doing.nemo.claims.controller.payload.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.UUID;

public class PracticeResponseIdV1 implements Serializable {

    @JsonProperty("id")
    private UUID id;

    @JsonProperty("practice_id")
    private Long practiceId;

    public PracticeResponseIdV1() {
    }

    public PracticeResponseIdV1(UUID id, Long practiceId) {
        this.id = id;
        this.practiceId = practiceId;
    }

    public Long getPracticeId() {
        return practiceId;
    }

    public void setPracticeId(Long practiceId) {
        this.practiceId = practiceId;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "PracticeResponseIdV1{" +
                "id='" + id + '\'' +
                '}';
    }
}
