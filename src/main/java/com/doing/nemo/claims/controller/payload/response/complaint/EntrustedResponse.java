package com.doing.nemo.claims.controller.payload.response.complaint;

import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.EntrustedEnum.EntrustedEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Date;

public class EntrustedResponse implements Serializable {

    @JsonProperty("auto_entrust")
    private Boolean autoEntrust;

    @JsonProperty("entrusted_to")
    private String entrustedTo;

    @JsonProperty("entrusted_day")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date entrustedDay;

    @JsonProperty("type")
    private EntrustedEnum entrustedType;

    @JsonProperty("description")
    private String description;

    @JsonProperty("perito")
    private Boolean perito;

    @JsonProperty("entrusted_email")
    private String entrustedEmail;

    @JsonProperty("id_entrusted_to")
    private String idEntrustedTo;

    @JsonProperty("number_sx_counterparty")
    private String numberSxCounterparty;

    @JsonProperty("dwl_man")
    private String dwlMan;

    @JsonProperty("status")
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDwlMan() {
        return dwlMan;
    }

    public void setDwlMan(String dwlMan) {
        this.dwlMan = dwlMan;
    }

    public String getNumberSxCounterparty() {
        return numberSxCounterparty;
    }

    public void setNumberSxCounterparty(String numberSxCounterparty) {
        this.numberSxCounterparty = numberSxCounterparty;
    }

    public Boolean getAutoEntrust() {
        return autoEntrust;
    }

    public void setAutoEntrust(Boolean autoEntrust) {
        this.autoEntrust = autoEntrust;
    }

    public String getEntrustedTo() {
        return entrustedTo;
    }

    public void setEntrustedTo(String entrustedTo) {
        this.entrustedTo = entrustedTo;
    }

    public Date getEntrustedDay() {
        if(entrustedDay == null){
            return null;
        }
        return (Date)entrustedDay.clone();
    }

    public void setEntrustedDay(Date entrustedDay) {
        if(entrustedDay != null)
        {
            this.entrustedDay = (Date)entrustedDay.clone();
        } else {
            this.entrustedDay = null;
        }
    }

    public EntrustedEnum getEntrustedType() {
        return entrustedType;
    }

    public void setEntrustedType(EntrustedEnum entrustedType) {
        this.entrustedType = entrustedType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getPerito() {
        return perito;
    }

    public void setPerito(Boolean perito) {
        this.perito = perito;
    }

    public String getEntrustedEmail() {
        return entrustedEmail;
    }

    public void setEntrustedEmail(String entrustedEmail) {
        this.entrustedEmail = entrustedEmail;
    }

    public String getIdEntrustedTo() {
        return idEntrustedTo;
    }

    public void setIdEntrustedTo(String idEntrustedTo) {
        this.idEntrustedTo = idEntrustedTo;
    }

    @Override
    public String toString() {
        return "EntrustedResponse{" +
                "autoEntrust=" + autoEntrust +
                ", entrustedTo='" + entrustedTo + '\'' +
                ", entrustedDay=" + entrustedDay +
                ", entrustedType=" + entrustedType +
                ", description='" + description + '\'' +
                ", perito=" + perito +
                ", entrustedEmail='" + entrustedEmail + '\'' +
                ", idEntrustedTo='" + idEntrustedTo + '\'' +
                '}';
    }
}
