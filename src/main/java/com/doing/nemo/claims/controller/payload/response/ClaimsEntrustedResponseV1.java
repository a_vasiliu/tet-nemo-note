package com.doing.nemo.claims.controller.payload.response;

import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.EntrustedEnum.EntrustedEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Date;

public class ClaimsEntrustedResponseV1 implements Serializable {

    @JsonProperty("id_claims")
    private String idClaims;

    @JsonProperty("status")
    private ClaimsStatusEnum status;

    @JsonProperty("entrusted_to")
    private String entrustedTo;

    @JsonProperty("auto_entrust")
    private Boolean autoEntrust;

    @JsonProperty("entrusted_day")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date entrustedDay;

    @JsonProperty("type")
    private EntrustedEnum entrustedType;

    @JsonProperty("description")
    private String description;

    @JsonProperty("perito")
    private Boolean perito;

    @JsonProperty("entrusted_email")
    private String entrustedEmail;

    @JsonProperty("id_entrusted_to")
    private String idEntrustedTo;

    public String getIdEntrustedTo() {
        return idEntrustedTo;
    }

    public void setIdEntrustedTo(String idEntrustedTo) {
        this.idEntrustedTo = idEntrustedTo;
    }

    public String getIdClaims() {
        return idClaims;
    }

    public void setIdClaims(String idClaims) {
        this.idClaims = idClaims;
    }

    public ClaimsStatusEnum getStatus() {
        return status;
    }

    public void setStatus(ClaimsStatusEnum status) {
        this.status = status;
    }

    public String getEntrustedTo() {
        return entrustedTo;
    }

    public void setEntrustedTo(String entrustedTo) {
        this.entrustedTo = entrustedTo;
    }

    public Date getEntrustedDay() {
        if(entrustedDay == null){
            return null;
        }
        return (Date)entrustedDay.clone();
    }

    public void setEntrustedDay(Date entrustedDay) {
        if(entrustedDay != null)
        {
            this.entrustedDay = (Date)entrustedDay.clone();
        } else {
            this.entrustedDay = null;
        }
    }

    public EntrustedEnum getEntrustedType() {
        return entrustedType;
    }

    public void setEntrustedType(EntrustedEnum entrustedType) {
        this.entrustedType = entrustedType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getPerito() {
        return perito;
    }

    public void setPerito(Boolean perito) {
        this.perito = perito;
    }

    public Boolean getAutoEntrust() {
        return autoEntrust;
    }

    public void setAutoEntrust(Boolean autoEntrust) {
        this.autoEntrust = autoEntrust;
    }

    public String getEntrustedEmail() {
        return entrustedEmail;
    }

    public void setEntrustedEmail(String entrustedEmail) {
        this.entrustedEmail = entrustedEmail;
    }


    @Override
    public String toString() {
        return "ClaimsEntrustedResponseV1{" +
                "idClaims='" + idClaims + '\'' +
                ", status=" + status +
                ", entrustedTo='" + entrustedTo + '\'' +
                ", autoEntrust=" + autoEntrust +
                ", entrustedDay=" + entrustedDay +
                ", entrustedType=" + entrustedType +
                ", description='" + description + '\'' +
                ", perito=" + perito +
                ", entrustedEmail='" + entrustedEmail + '\'' +
                ", idEntrustedTo='" + idEntrustedTo + '\'' +
                '}';
    }
}
