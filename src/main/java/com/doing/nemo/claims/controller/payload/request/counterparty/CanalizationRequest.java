package com.doing.nemo.claims.controller.payload.request.counterparty;

import com.doing.nemo.claims.controller.payload.request.CenterRequestV1;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class CanalizationRequest implements Serializable {

    @JsonProperty("center")
    private CenterRequestV1 center;

    @JsonProperty("date")
    private String date;

    public CanalizationRequest() {
    }

    public CenterRequestV1 getCenter() {
        return center;
    }

    public void setCenter(CenterRequestV1 center) {
        this.center = center;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "CanalizationRequest{" +
                "center=" + center +
                ", date='" + date + '\'' +
                '}';
    }
}
