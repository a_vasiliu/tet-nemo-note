package com.doing.nemo.claims.controller.payload.request.authority.practice;

import com.doing.nemo.claims.controller.payload.request.authority.claims.CommodityDetailsRequestV1;
import com.doing.nemo.claims.entity.enumerated.AuthorityEnum.AuthorityPracticeStatusEnum;
import com.doing.nemo.claims.entity.enumerated.AuthorityEnum.AuthorityWorkingTypeEnum;
import com.doing.nemo.claims.entity.enumerated.ProcessingTypeEnum;
import com.doing.nemo.claims.entity.jsonb.CommodityDetails;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProcessingRequestV1 implements Serializable {

    @JsonProperty("processing_type")
    private ProcessingTypeEnum processingType;

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    @JsonProperty("processing_date")
    private String processingDate;

    @JsonProperty("dossier_id")
    private String dossierId;

    @JsonProperty("working_id")
    private String workingId;

    @JsonProperty("working_number")
    private String workingNumber;

    @JsonProperty("dossier_number")
    private String dossierNumber;

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    @JsonProperty("processing_authority_date")
    private String processingAuthorityDate;

    @JsonProperty("commodity_details")
    private CommodityDetailsRequestV1 commodityDetails;

    @JsonProperty("status")
    private AuthorityPracticeStatusEnum status;

    @JsonProperty("working_type")
    private AuthorityWorkingTypeEnum workingType;

    public ProcessingTypeEnum getProcessingType() {
        return processingType;
    }

    public void setProcessingType(ProcessingTypeEnum processingType) {
        this.processingType = processingType;
    }

    public String getProcessingDate() {
        return processingDate;
    }

    public void setProcessingDate(String processingDate) {
        this.processingDate = processingDate;
    }

    public String getDossierId() {
        return dossierId;
    }

    public void setDossierId(String dossierId) {
        this.dossierId = dossierId;
    }

    public String getWorkingId() {
        return workingId;
    }

    public void setWorkingId(String workingId) {
        this.workingId = workingId;
    }

    public String getWorkingNumber() {
        return workingNumber;
    }

    public void setWorkingNumber(String workingNumber) {
        this.workingNumber = workingNumber;
    }

    public String getDossierNumber() {
        return dossierNumber;
    }

    public void setDossierNumber(String dossierNumber) {
        this.dossierNumber = dossierNumber;
    }

    public String getProcessingAuthorityDate() {
        return processingAuthorityDate;
    }

    public void setProcessingAuthorityDate(String processingAuthorityDate) {
        this.processingAuthorityDate = processingAuthorityDate;
    }

    public CommodityDetailsRequestV1 getCommodityDetails() {
        return commodityDetails;
    }

    public void setCommodityDetails(CommodityDetailsRequestV1 commodityDetails) {
        this.commodityDetails = commodityDetails;
    }

    public AuthorityPracticeStatusEnum getStatus() {
        return status;
    }

    public void setStatus(AuthorityPracticeStatusEnum status) {
        this.status = status;
    }

    public AuthorityWorkingTypeEnum getWorkingType() {
        return workingType;
    }

    public void setWorkingType(AuthorityWorkingTypeEnum workingType) {
        this.workingType = workingType;
    }

    @Override
    public String toString() {
        return "ProcessingRequestV1{" +
                "processingType=" + processingType +
                ", processingDate=" + processingDate +
                ", dossierId='" + dossierId + '\'' +
                ", workingId='" + workingId + '\'' +
                ", workingNumber='" + workingNumber + '\'' +
                ", dossierNumber='" + dossierNumber + '\'' +
                ", processingAuthorityDate=" + processingAuthorityDate +
                ", commodityDetails=" + commodityDetails +
                ", status=" + status +
                ", workingType=" + workingType +
                '}';
    }
}
