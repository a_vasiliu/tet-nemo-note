package com.doing.nemo.claims.controller.payload.response;

import com.doing.nemo.claims.controller.payload.response.claims.*;
import com.doing.nemo.claims.controller.payload.response.external.*;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsFlowEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ClaimsExternalResponse implements Serializable {
    @JsonProperty("id")
    private String id;

    @JsonProperty("created_at")
    private String createdAt;

    @JsonProperty("update_at")
    private String updateAt;

    @JsonProperty("practice_id")
    private Long practiceId;

    @JsonProperty("status")
    private ClaimsStatusEnum status;

    @JsonProperty("motivation")
    private String motivation;

    @JsonProperty("type")
    private ClaimsFlowEnum type;

    @JsonProperty("complaint")
    private ComplaintExternalResponse complaint;

    @JsonProperty("forms")
    private FormsExternalResponse forms;

    @JsonProperty("is_with_counterparty")
    private Boolean isWithCounterparty;

    @JsonProperty("counterparty")
    private List<CounterpartyExternalResponse> counterparty;

    @JsonProperty("deponent")
    private List<DeponentResponse> deponent;

    @JsonProperty("wounded")
    private List<WoundedResponse> wounded;

    @JsonProperty("damaged")
    private DamagedExternalResponse damaged;

    @JsonProperty("theft")
    private TheftExternalResponse theftResponse;

    @JsonProperty("id_saleforce")
    private Long idSaleforce;

    @JsonProperty("cai_details")
    private CaiResponse caiDetails;

    @JsonProperty("exemption")
    private ExemptionResponse exemption;

    @JsonProperty("refund")
    private RefundResponse refund;

    @JsonProperty("is_read_msa")
    private Boolean isRead;

    @JsonProperty("is_read_acclaims")
    private Boolean isReadAcclaims;

    @JsonProperty("po_variation")
    private Boolean poVariation;

    @JsonProperty("total_penalty")
    private Long totalPenalty;

    @JsonProperty("is_migrated")
    private Boolean isMigrated;

    @JsonProperty("is_complete_documentation")
    private Boolean isCompleteDocumentation;

    @JsonIgnore
    public Boolean getCompleteDocumentation() {
        return isCompleteDocumentation;
    }

    public void setCompleteDocumentation(Boolean completeDocumentation) {
        isCompleteDocumentation = completeDocumentation;
    }

    @JsonIgnore
    public Boolean getMigrated() {
        return isMigrated;
    }

    public void setMigrated(Boolean migrated) {
        isMigrated = migrated;
    }

    @JsonIgnore
    public Boolean getReadAcclaims() {
        return isReadAcclaims;
    }

    public void setReadAcclaims(Boolean readAcclaims) {
        isReadAcclaims = readAcclaims;
    }

    public Long getTotalPenalty() {
        return totalPenalty;
    }

    public void setTotalPenalty(Long totalPenalty) {
        this.totalPenalty = totalPenalty;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(String updateAt) {
        this.updateAt = updateAt;
    }

    public Long getPracticeId() {
        return practiceId;
    }

    public void setPracticeId(Long practiceId) {
        this.practiceId = practiceId;
    }

    public ClaimsStatusEnum getStatus() {
        return status;
    }

    public void setStatus(ClaimsStatusEnum status) {
        this.status = status;
    }

    public String getMotivation() {
        return motivation;
    }

    public void setMotivation(String motivation) {
        this.motivation = motivation;
    }

    public ClaimsFlowEnum getType() {
        return type;
    }

    public void setType(ClaimsFlowEnum type) {
        this.type = type;
    }

    public ComplaintExternalResponse getComplaint() {
        return complaint;
    }

    public void setComplaint(ComplaintExternalResponse complaint) {
        this.complaint = complaint;
    }

    public FormsExternalResponse getForms() {
        return forms;
    }

    public void setForms(FormsExternalResponse forms) {
        this.forms = forms;
    }

    @JsonIgnore
    public Boolean getWithCounterparty() {
        return isWithCounterparty;
    }

    public void setWithCounterparty(Boolean withCounterparty) {
        isWithCounterparty = withCounterparty;
    }

    public List<CounterpartyExternalResponse> getCounterparty() {
        if(counterparty == null){
            return null;
        }
        return new ArrayList<>(counterparty);
    }

    public void setCounterparty(List<CounterpartyExternalResponse> counterparty) {
        if(counterparty != null)
        {
            this.counterparty = new ArrayList<>(counterparty);
        } else {
            this.counterparty = null;
        }
    }

    public List<DeponentResponse> getDeponent() {
        if(deponent == null){
            return null;
        }
        return new ArrayList<>(deponent);
    }

    public void setDeponent(List<DeponentResponse> deponent) {
        if(deponent != null)
        {
            this.deponent = new ArrayList<>(deponent);
        } else {
            this.deponent = null;
        }
    }

    public List<WoundedResponse> getWounded() {
        if(wounded == null){
            return null;
        }
        return new ArrayList<>(wounded);
    }

    public void setWounded(List<WoundedResponse> wounded) {
        if(wounded != null)
        {
            this.wounded = new ArrayList<>(wounded);
        } else {
            this.wounded = null;
        }
    }

    public DamagedExternalResponse getDamaged() {
        return damaged;
    }

    public void setDamaged(DamagedExternalResponse damaged) {
        this.damaged = damaged;
    }

    public TheftExternalResponse getTheftResponse() {
        return theftResponse;
    }

    public void setTheftResponse(TheftExternalResponse theftResponse) {
        this.theftResponse = theftResponse;
    }

    public Long getIdSaleforce() {
        return idSaleforce;
    }

    public void setIdSaleforce(Long idSaleforce) {
        this.idSaleforce = idSaleforce;
    }

    public CaiResponse getCaiDetails() {
        return caiDetails;
    }

    public void setCaiDetails(CaiResponse caiDetails) {
        this.caiDetails = caiDetails;
    }

    public ExemptionResponse getExemption() {
        return exemption;
    }

    public void setExemption(ExemptionResponse exemption) {
        this.exemption = exemption;
    }

    public RefundResponse getRefund() {
        return refund;
    }

    public void setRefund(RefundResponse refund) {
        this.refund = refund;
    }

    @JsonIgnore
    public Boolean getRead() {
        return isRead;
    }

    public void setRead(Boolean read) {
        isRead = read;
    }

    public Boolean getPoVariation() {
        return poVariation;
    }

    public void setPoVariation(Boolean poVariation) {
        this.poVariation = poVariation;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ClaimsExternalResponse{");
        sb.append("id='").append(id).append('\'');
        sb.append(", createdAt='").append(createdAt).append('\'');
        sb.append(", updateAt='").append(updateAt).append('\'');
        sb.append(", practiceId=").append(practiceId);
        sb.append(", status=").append(status);
        sb.append(", motivation='").append(motivation).append('\'');
        sb.append(", type=").append(type);
        sb.append(", complaint=").append(complaint);
        sb.append(", forms=").append(forms);
        sb.append(", isWithCounterparty=").append(isWithCounterparty);
        sb.append(", counterparty=").append(counterparty);
        sb.append(", deponent=").append(deponent);
        sb.append(", wounded=").append(wounded);
        sb.append(", damaged=").append(damaged);
        sb.append(", theftResponse=").append(theftResponse);
        sb.append(", idSaleforce=").append(idSaleforce);
        sb.append(", caiDetails=").append(caiDetails);
        sb.append(", exemption=").append(exemption);
        sb.append(", refund=").append(refund);
        sb.append(", isRead=").append(isRead);
        sb.append(", isReadAcclaims=").append(isReadAcclaims);
        sb.append(", poVariation=").append(poVariation);
        sb.append(", totalPenalty=").append(totalPenalty);
        sb.append(", isMigrated=").append(isMigrated);
        sb.append(", isCompleteDocumentation=").append(isCompleteDocumentation);
        sb.append('}');
        return sb.toString();
    }
}
