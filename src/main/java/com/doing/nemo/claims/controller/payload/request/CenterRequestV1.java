package com.doing.nemo.claims.controller.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class CenterRequestV1 implements Serializable {
    private static final long serialVersionUID = -61589083904741009L;

    @JsonProperty("address")
    private String address;
    @JsonProperty("address_number")
    private String addressNumber;
    @JsonProperty("region")
    private String region;
    @JsonProperty("province_code")
    private String provinceCode;
    @JsonProperty("zip_code")
    private String zipCode;
    @JsonProperty("fiscal_code")
    private String fiscalCode;
    @JsonProperty("vat_number")
    private String vatNumber;
    @JsonProperty("ald_commodity_id")
    private String aldCommodityId;
    @JsonProperty("ald_supplier_code")
    private String aldSupplieCode;
    @JsonProperty("contact_phone")
    private String  contactPhone;
    @JsonProperty("distance")
    private String distance;
    @JsonProperty("has_dedicated_expert")
    private Boolean hasDedicatedExpert;
    @JsonProperty("municipality")
    private String municipality;
    @JsonProperty("name")
    private String name;
    @JsonProperty("place_id")
    private String placeId;
    @JsonProperty("type")
    private String type;
    @JsonProperty("email")
    private String email;

    public CenterRequestV1() {
    }

    public CenterRequestV1(String address, String addressNumber, String region, String provinceCode, String zipCode, String fiscalCode, String vatNumber, String aldCommodityId, String aldSupplieCode, String contactPhone, String distance, Boolean hasDedicatedExpert, String municipality, String name, String placeId, String type, String email) {
        this.address = address;
        this.addressNumber = addressNumber;
        this.region = region;
        this.provinceCode = provinceCode;
        this.zipCode = zipCode;
        this.fiscalCode = fiscalCode;
        this.vatNumber = vatNumber;
        this.aldCommodityId = aldCommodityId;
        this.aldSupplieCode = aldSupplieCode;
        this.contactPhone = contactPhone;
        this.distance = distance;
        this.hasDedicatedExpert = hasDedicatedExpert;
        this.municipality = municipality;
        this.name = name;
        this.placeId = placeId;
        this.type = type;
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddressNumber() {
        return addressNumber;
    }

    public void setAddressNumber(String addressNumber) {
        this.addressNumber = addressNumber;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getProvinceCode() {
        return provinceCode;
    }

    public void setProvinceCode(String provinceCode) {
        this.provinceCode = provinceCode;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zip_code) {
        this.zipCode = zip_code;
    }

    public String getFiscalCode() {
        return fiscalCode;
    }

    public void setFiscalCode(String fiscalCode) {
        this.fiscalCode = fiscalCode;
    }

    public String getVatNumber() {
        return vatNumber;
    }

    public void setVatNumber(String vatNumber) {
        this.vatNumber = vatNumber;
    }

    public String getAldCommodityId() {
        return aldCommodityId;
    }

    public void setAldCommodityId(String aldCommodityId) {
        this.aldCommodityId = aldCommodityId;
    }

    public String getAldSupplieCode() {
        return aldSupplieCode;
    }

    public void setAldSupplieCode(String aldSupplieCode) {
        this.aldSupplieCode = aldSupplieCode;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public Boolean getHasDedicatedExpert() {
        return hasDedicatedExpert;
    }

    public void setHasDedicatedExpert(Boolean hasDedicatedExpert) {
        this.hasDedicatedExpert = hasDedicatedExpert;
    }

    public String getMunicipality() {
        return municipality;
    }

    public void setMunicipality(String municipality) {
        this.municipality = municipality;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "CenterEntity{" +
                "address='" + address + '\'' +
                ", addressNumber='" + addressNumber + '\'' +
                ", region='" + region + '\'' +
                ", provinceCode='" + provinceCode + '\'' +
                ", zipCode='" + zipCode + '\'' +
                ", fiscalCode='" + fiscalCode + '\'' +
                ", vatNumber='" + vatNumber + '\'' +
                ", aldCommodityId=" + aldCommodityId +
                ", aldSupplieCode=" + aldSupplieCode +
                ", contactPhone='" + contactPhone + '\'' +
                ", distance=" + distance +
                ", hasDedicatedExpert=" + hasDedicatedExpert +
                ", municipality='" + municipality + '\'' +
                ", name='" + name + '\'' +
                ", placeId='" + placeId + '\'' +
                ", type='" + type + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
