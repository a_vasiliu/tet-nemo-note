package com.doing.nemo.claims.controller.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class DmSystemsRequestV1  implements Serializable {

    @JsonProperty("system_name")
    private String systemName;

    public DmSystemsRequestV1() {
    }

    public DmSystemsRequestV1(String systemName) {
        this.systemName = systemName;
    }

    public String getSystemName() {
        return systemName;
    }

    public void setSystemName(String systemName) {
        this.systemName = systemName;
    }

    @Override
    public String toString() {
        return "DmSystemsRequestV1{" +
                "systemName='" + systemName + '\'' +
                '}';
    }
}
