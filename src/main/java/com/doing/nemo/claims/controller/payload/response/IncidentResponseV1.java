package com.doing.nemo.claims.controller.payload.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class IncidentResponseV1  implements Serializable {

    @JsonProperty("error")
    private Boolean error;

    @JsonProperty("description")
    private String description;

    public IncidentResponseV1() {
    }

    public IncidentResponseV1(Boolean error, String description) {
        this.error = error;
        this.description = description;
    }


    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "IncidentResponseV1{" +
                "error=" + error +
                ", description='" + description + '\'' +
                '}';
    }
}
