package com.doing.nemo.claims.controller.payload.response.authority.claims;

import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsFlowEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class AuthorityOldestResponseV1 implements Serializable {

    @JsonProperty("id")
    private String id;

    @JsonProperty("oldest")
    private Boolean oldest;

    @JsonProperty("flow_type")
    private ClaimsFlowEnum flowType;

    @JsonProperty("practice_id")
    private Long practiceId;

    @JsonProperty("created_at")
    private String createdAt;

    @JsonProperty("claim_type")
    private DataAccidentTypeAccidentEnum claimType;

    @JsonProperty("date_accident")
    private String dateAccident;

    @JsonProperty("number_franchise")
    private Long numberFranchise;

    @JsonProperty("status")
    private ClaimsStatusEnum status;

    public AuthorityOldestResponseV1() {
    }

    public AuthorityOldestResponseV1(String id, Boolean oldest, ClaimsFlowEnum flowType, Long practiceId, String createdAt, DataAccidentTypeAccidentEnum claimType, String dateAccident, Long numberFranchise, ClaimsStatusEnum status) {
        this.id = id;
        this.oldest = oldest;
        this.flowType = flowType;
        this.practiceId = practiceId;
        this.createdAt = createdAt;
        this.claimType = claimType;
        this.dateAccident = dateAccident;
        this.numberFranchise = numberFranchise;
        this.status = status;
    }

    public ClaimsFlowEnum getFlowType() {
        return flowType;
    }

    public void setFlowType(ClaimsFlowEnum flowType) {
        this.flowType = flowType;
    }

    public Long getPracticeId() {
        return practiceId;
    }

    public void setPracticeId(Long practiceId) {
        this.practiceId = practiceId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public DataAccidentTypeAccidentEnum getClaimType() {
        return claimType;
    }

    public void setClaimType(DataAccidentTypeAccidentEnum claimType) {
        this.claimType = claimType;
    }

    public String getDateAccident() {
        return dateAccident;
    }

    public void setDateAccident(String dateAccident) {
        this.dateAccident = dateAccident;
    }

    public Long getNumberFranchise() {
        return numberFranchise;
    }

    public void setNumberFranchise(Long numberFranchise) {
        this.numberFranchise = numberFranchise;
    }

    public ClaimsStatusEnum getStatus() {
        return status;
    }

    public void setStatus(ClaimsStatusEnum status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Boolean getOldest() {
        return oldest;
    }

    public void setOldest(Boolean oldest) {
        this.oldest = oldest;
    }

    @Override
    public String toString() {
        return "AuthorityOldestResponseV1{" +
                "id='" + id + '\'' +
                ", oldest=" + oldest +
                ", flowType=" + flowType +
                ", practiceId=" + practiceId +
                ", createdAt=" + createdAt +
                ", claimType=" + claimType +
                ", dateAccident=" + dateAccident +
                ", numberFranchise=" + numberFranchise +
                ", status=" + status +
                '}';
    }
}
