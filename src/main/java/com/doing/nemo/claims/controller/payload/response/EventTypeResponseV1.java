package com.doing.nemo.claims.controller.payload.response;

import com.doing.nemo.claims.entity.enumerated.ClaimsRepairEnum;
import com.doing.nemo.claims.entity.enumerated.EventTypeEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.UUID;

public class EventTypeResponseV1 implements Serializable {

    @JsonProperty("id")
    private UUID id;

    @JsonProperty("description")
    private String description;

    @JsonProperty("event_type")
    private EventTypeEnum eventType;

    @JsonProperty("create_attachments")
    private Boolean createAttachments;

    @JsonProperty("catalog")
    private String catalog;

    @JsonProperty("attachments_type")
    private String attachmentsType;

    @JsonProperty("only_last")
    private Boolean onlyLast;

    @JsonProperty("is_active")
    private Boolean isActive;

    @JsonProperty("type_complaint")
    private ClaimsRepairEnum typeComplaint;


    public EventTypeResponseV1() {
    }

    public EventTypeResponseV1(UUID id, String description, EventTypeEnum eventType, Boolean createAttachments, String catalog, String attachmentsType, Boolean onlyLast, Boolean isActive, ClaimsRepairEnum typeComplaint) {
        this.id = id;
        this.description = description;
        this.eventType = eventType;
        this.createAttachments = createAttachments;
        this.catalog = catalog;
        this.attachmentsType = attachmentsType;
        this.onlyLast = onlyLast;
        this.isActive = isActive;
        this.typeComplaint = typeComplaint;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getCreateAttachments() {
        return createAttachments;
    }

    public void setCreateAttachments(Boolean createAttachments) {
        this.createAttachments = createAttachments;
    }

    public String getCatalog() {
        return catalog;
    }

    public void setCatalog(String catalog) {
        this.catalog = catalog;
    }

    public String getAttachmentsType() {
        return attachmentsType;
    }

    public void setAttachmentsType(String attachmentsType) {
        this.attachmentsType = attachmentsType;
    }

    public Boolean getOnlyLast() {
        return onlyLast;
    }

    public void setOnlyLast(Boolean onlyLast) {
        this.onlyLast = onlyLast;
    }

    @JsonIgnore
    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public EventTypeEnum getEventType() {
        return eventType;
    }

    public void setEventType(EventTypeEnum eventType) {
        this.eventType = eventType;
    }

    public ClaimsRepairEnum getTypeComplaint() {
        return typeComplaint;
    }

    public void setTypeComplaint(ClaimsRepairEnum typeComplaint) {
        this.typeComplaint = typeComplaint;
    }

    @Override
    public String toString() {
        return "EventTypeResponseV1{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", eventType=" + eventType +
                ", createAttachments=" + createAttachments +
                ", catalog='" + catalog + '\'' +
                ", attachmentsType='" + attachmentsType + '\'' +
                ", onlyLast=" + onlyLast +
                ", isActive=" + isActive +
                '}';
    }
}
