package com.doing.nemo.claims.controller.payload.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel
public class AddTokenResponseV1 implements Serializable {

    private static final long serialVersionUID = 6072334952030775656L;

    @JsonProperty("access_token")
    @ApiModelProperty(example = "897g34s3-0ca7-4226-b10b-c5db10003257")
    private String accessToken;

    @JsonProperty("expires_at")
    @ApiModelProperty(example = "1531401311")
    private long expiresAt;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public long getExpiresAt() {
        return expiresAt;
    }

    public void setExpiresAt(long expiresAt) {
        this.expiresAt = expiresAt;
    }

    @Override
    public String toString() {
        return "AddTokenResponseV1{" +
                "accessToken='" + accessToken + '\'' +
                ", expiresAt=" + expiresAt +
                '}';
    }
}