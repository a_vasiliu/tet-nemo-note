package com.doing.nemo.claims.controller.payload.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.UUID;

public class NotificationTypeResponseV1 implements Serializable {

    private UUID id;

    @JsonProperty("description")
    private String description;

    @JsonProperty("is_active")
    private Boolean isActive;

    @JsonProperty("order_id")
    private Integer orderId;

    public NotificationTypeResponseV1() {
    }

    public NotificationTypeResponseV1(UUID id, String description, Boolean isActive, Integer orderId) {
        this.id = id;
        this.description = description;
        this.isActive = isActive;
        this.orderId = orderId;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @JsonIgnore
    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    @Override
    public String toString() {
        return "NotificationTypeResponseV1{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", isActive=" + isActive +
                ", orderId=" + orderId +
                '}';
    }
}
