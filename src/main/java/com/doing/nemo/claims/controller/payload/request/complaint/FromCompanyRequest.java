package com.doing.nemo.claims.controller.payload.request.complaint;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class FromCompanyRequest implements Serializable {

    @JsonProperty("company")
    private String company;

    @JsonProperty("number_sx")
    private String numberSx;

    @JsonProperty("type_sx")
    private String typeSx;

    @JsonProperty("inspectorate")
    private String inspectorate;

    @JsonProperty("expert")
    private String expert;

    @JsonProperty("note")
    private String note;

    @JsonProperty("status")
    private String status;

    @JsonProperty("global_reserve")
    private Double globalReserve;

    @JsonProperty("total_paid")
    private Double totalPaid;

    public FromCompanyRequest() {
    }

    public FromCompanyRequest(
        String company,
        String numberSx,
        String typeSx,
        String inspectorate,
        String expert,
        String note
    ) {
        this.company = company;
        this.numberSx = numberSx;
        this.typeSx = typeSx;
        this.inspectorate = inspectorate;
        this.expert = expert;
        this.note = note;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Double getGlobalReserve() {
        return globalReserve;
    }

    public void setGlobalReserve(Double globalReserve) {
        this.globalReserve = globalReserve;
    }

    public Double getTotalPaid() {
        return totalPaid;
    }

    public void setTotalPaid(Double totalPaid) {
        this.totalPaid = totalPaid;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getNumberSx() {
        return numberSx;
    }

    public void setNumberSx(String numberSx) {
        this.numberSx = numberSx;
    }

    public String getTypeSx() {
        return typeSx;
    }

    public void setTypeSx(String typeSx) {
        this.typeSx = typeSx;
    }

    public String getInspectorate() {
        return inspectorate;
    }

    public void setInspectorate(String inspectorate) {
        this.inspectorate = inspectorate;
    }

    public String getExpert() {
        return expert;
    }

    public void setExpert(String expert) {
        this.expert = expert;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public String toString() {
        return "FromCompany{" +
                "company='" + company + '\'' +
                ", numberSx='" + numberSx + '\'' +
                ", typeSx='" + typeSx + '\'' +
                ", inspectorate='" + inspectorate + '\'' +
                ", expert='" + expert + '\'' +
                ", note='" + note + '\'' +
                '}';
    }
}
