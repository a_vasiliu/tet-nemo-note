package com.doing.nemo.claims.controller.payload.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ClaimsResponsePaginationV1<T> implements Serializable {

    @JsonProperty("stats")
    private PaginationStats stats;

    @JsonProperty("items")
    private List<T> items;

    public PaginationStats getStats() {
        return stats;
    }

    public void setStats(PaginationStats stats) {
        this.stats = stats;
    }

    public List<T> getItems() {
        if(items == null){
            return null;
        }
        return new ArrayList<>(items);
    }

    public void setItems(List<T> items) {
        if(items != null)
        {
            this.items = new ArrayList<>(items);
        } else {
            this.items = null;
        }
    }

    @Override
    public String toString() {
        return "ClaimsResponsePaginationV1{" +
                "stats=" + stats +
                ", items=" + items +
                '}';
    }


}
