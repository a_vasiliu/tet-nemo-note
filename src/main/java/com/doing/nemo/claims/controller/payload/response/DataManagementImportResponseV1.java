package com.doing.nemo.claims.controller.payload.response;

import com.doing.nemo.claims.entity.enumerated.FileImportTypeEnum;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

public class DataManagementImportResponseV1 implements Serializable {

    @JsonProperty("id")
    private String id;

    @JsonProperty("user_import")
    private String user;

    @JsonProperty("description")
    private String description;

    @JsonProperty("file_type")
    private FileImportTypeEnum fileType;

    @JsonProperty("result")
    private String result;

    @JsonProperty("date")
    private String date;

    @JsonProperty("processing_files")
    private List<ProcessingFileResponseV1> processingFiles = new LinkedList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public FileImportTypeEnum getFileType() {
        return fileType;
    }

    public void setFileType(FileImportTypeEnum fileType) {
        this.fileType = fileType;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<ProcessingFileResponseV1> getProcessingFiles() {
        if(processingFiles == null){
            return null;
        }
        return new ArrayList<>(processingFiles);
    }

    public void setProcessingFiles(List<ProcessingFileResponseV1> processingFiles) {
        if(processingFiles != null)
        {
            this.processingFiles = new ArrayList<>(processingFiles);
        } else {
            this.processingFiles = null;
        }
    }

    @Override
    public String toString() {
        return "DataManagementImportResponseV1{" +
                "id='" + id + '\'' +
                ", user='" + user + '\'' +
                ", description='" + description + '\'' +
                ", fileType=" + fileType +
                ", result='" + result + '\'' +
                ", date=" + date +
                ", processingFiles=" + processingFiles +
                '}';
    }

    public static class ProcessingFileResponseV1{

        @JsonProperty("id")
        private UUID id;

        @JsonProperty("attachment_id")
        private UUID attachmentId;

        @JsonProperty("file_name")
        private String fileName;

        @JsonProperty("file_size")
        private Integer fileSize;

        public ProcessingFileResponseV1() {
        }

        public ProcessingFileResponseV1(UUID id, UUID attachmentId, String fileName, Integer fileSize) {
            this.id = id;
            this.attachmentId = attachmentId;
            this.fileName = fileName;
            this.fileSize = fileSize;
        }

        public UUID getId() {
            return id;
        }

        public void setId(UUID id) {
            this.id = id;
        }

        public UUID getAttachmentId() {
            return attachmentId;
        }

        public void setAttachmentId(UUID attachmentId) {
            this.attachmentId = attachmentId;
        }

        public String getFileName() {
            return fileName;
        }

        public void setFileName(String fileName) {
            this.fileName = fileName;
        }

        public Integer getFileSize() {
            return fileSize;
        }

        public void setFileSize(Integer fileSize) {
            this.fileSize = fileSize;
        }

        @Override
        public String toString() {
            return "ProcessingFileResponseV1{" +
                    "id=" + id +
                    ", attachmentId=" + attachmentId +
                    ", fileName='" + fileName + '\'' +
                    ", fileSize=" + fileSize +
                    '}';
        }
    }

}
