package com.doing.nemo.claims.controller.payload.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.UUID;

public class ConfigureEvidenceResponseV1 implements Serializable {

    @JsonProperty("id")
    private UUID id;

    @JsonProperty("practical_state_it")
    private String practicalStateIt;

    @JsonProperty("practical_state_en")
    private String practicalStateEn;

    @JsonProperty("days_of_stay_in_the_state")
    private Long daysOfStayInTheState;

    @JsonProperty("is_active")
    private Boolean isActive;

    public ConfigureEvidenceResponseV1() {
    }

    public ConfigureEvidenceResponseV1(UUID id, String practicalStateIt, String practicalStateEn, Long daysOfStayInTheState, Boolean isActive) {
        this.id = id;
        this.practicalStateIt = practicalStateIt;
        this.practicalStateEn = practicalStateEn;
        this.daysOfStayInTheState = daysOfStayInTheState;
        this.isActive = isActive;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getPracticalStateIt() {
        return practicalStateIt;
    }

    public void setPracticalStateIt(String practicalStateIt) {
        this.practicalStateIt = practicalStateIt;
    }

    public String getPracticalStateEn() {
        return practicalStateEn;
    }

    public void setPracticalStateEn(String practicalStateEn) {
        this.practicalStateEn = practicalStateEn;
    }

    public Long getDaysOfStayInTheState() {
        return daysOfStayInTheState;
    }

    public void setDaysOfStayInTheState(Long daysOfStayInTheState) {
        this.daysOfStayInTheState = daysOfStayInTheState;
    }

    @JsonIgnore
    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    @Override
    public String toString() {
        return "ConfigureEvidenceResponseV1{" +
                "id=" + id +
                ", practicalStateIt='" + practicalStateIt + '\'' +
                ", practicalStateEn='" + practicalStateEn + '\'' +
                ", daysOfStayInTheState=" + daysOfStayInTheState +
                ", isActive=" + isActive +
                '}';
    }
}
