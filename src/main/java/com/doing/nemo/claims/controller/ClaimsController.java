package com.doing.nemo.claims.controller;

import com.doing.nemo.claims.adapter.*;
import com.doing.nemo.claims.command.*;
import com.doing.nemo.claims.common.commandBus.Command;
import com.doing.nemo.claims.common.commandBus.CommandBus;
import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.*;
import com.doing.nemo.claims.controller.payload.request.claims.DamagedRequest;
import com.doing.nemo.claims.controller.payload.request.claims.NoteRequest;
import com.doing.nemo.claims.controller.payload.request.refund.SplitRequest;
import com.doing.nemo.claims.controller.payload.response.*;
import com.doing.nemo.claims.controller.payload.response.claims.NoteResponse;
import com.doing.nemo.claims.dto.FilterView;
import com.doing.nemo.claims.dto.ClaimsViewSearchDTO;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.ClaimsNewEntity;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.claims.ClaimsDamagedCustomerEntity;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsUpdateProcedureEnum;
import com.doing.nemo.claims.entity.esb.CustomerESB.CustomerEsb;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbAntiTheftService.AntiTheftRequest;
import com.doing.nemo.claims.repository.ClaimsRepository;
import com.doing.nemo.claims.service.*;
import com.doing.nemo.claims.util.Util;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.claims.websin.WebSinServiceManager;
import com.doing.nemo.commons.exception.AbstractException;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.exception.InternalException;
import com.doing.nemo.commons.exception.NotFoundException;
import io.swagger.annotations.*;
import org.postgresql.util.PSQLException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.transaction.Transactional;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/v1")
@Api("Claims")
public class ClaimsController {

    private static Logger LOGGER = LoggerFactory.getLogger(ClaimsController.class);

    @Autowired
    private ClaimsSearchRequestDTOAdapter claimsSearchRequestDTOAdapter;
    @Autowired
    private StatsDTOResponseV2Adapter statsDTOResponseAdapter;
    @Autowired
    private ClaimsSearchDTOResponseV2Adapter claimsSearchDTOResponseV2Adapter;
    @Autowired
    private ClaimsRepository claimsRepository;

    @Autowired
    private ClaimsService claimsService;

    @Autowired
    private CommandBus commandBus;

    @Autowired
    private NotesAdapter notesAdapter;

    @Autowired
    private EntrustedAdapter entrustedAdapter;

    @Autowired
    private LockService lockService;

    @Autowired
    private NemoAuthService nemoAuthService;

    @Autowired
    private ESBService esbService;

    @Autowired
    private CounterpartyService counterpartyService;

    @Autowired
    private CacheService cacheService;

    @Autowired
    private AntiTheftRequestService antiTheftRequestService;

    @Autowired
    private ConverterClaimsService converterClaimsService;

    private static final String CACHENAME = "claims.";
    private static final String STATSkEY = "claimsStats::";
    private static final String STATSEVIDENCE = "evidenceStats::";
    private static final String STATSTHEFT = "theftStats::";
    private static final String STATSSTATUS = "statusStats::";
    private static final String COUNTERPARTYKEY = "counterpartyStats::";

    @Bean
    public static RestTemplate rest() {
        return new RestTemplate();
    }


    /*-------------------------------------CLAIMS----------------------------------------------*/
    /*INSERIMENTO CLAIMS*/
    //REFACTOR
    @PostMapping(value = "/claims",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Insert Claims", notes = "Insert claim using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = ClaimsResponseIdV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ClaimsResponseIdV1> postInsertClaimsAdmin(
            @ApiParam(value = "Body of the Claims to be created", required = true)
            @RequestBody ClaimsInsertRequestV1 claimsRequestV1,
            @ApiParam(value = "TypeEnum of User who calls the service", required = true)
            @RequestHeader(name = "nemo-user-id") String userId,
            @RequestHeader(name = "nemo-user-first-name") String userName,
            @RequestHeader(name = "nemo-user-last-name") String lastName) throws IOException {

        String claimsId = UUID.randomUUID().toString();
        Command claimsInsertCommand = new ClaimsInsertCommand(
                claimsId,
                userId,
                userName+" "+lastName,
                null,
                claimsRequestV1.getWithCounterparty(),
                claimsRequestV1.getInEvidence(),
                claimsRequestV1.getComplaint(),
                claimsRequestV1.getDamaged(),
                claimsRequestV1.getCounterparty(),
                claimsRequestV1.getDeponentList(),
                claimsRequestV1.getWoundedList(),
                claimsRequestV1.getPaiComunication(),
                claimsRequestV1.getForced(),
                claimsRequestV1.getCaiRequest(),
                claimsRequestV1.getIdSaleforce(),
                claimsRequestV1.getRefund(),
                claimsRequestV1.getMetadata(),
                claimsRequestV1.getTheft(),
                claimsRequestV1.getNotes(),
                claimsRequestV1.getPoVariation(),
                claimsRequestV1.getCompleteDocumentation()
        );

        commandBus.execute(claimsInsertCommand);

        return new ResponseEntity<>(ClaimsAdapter.adptIdToIdResponse(claimsId), HttpStatus.OK);
    }

    /*REFACTOR*/
    @PostMapping(value = "/claims/olsa",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Insert Claims", notes = "Insert claim using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = ClaimsResponseIdV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ClaimsResponseIdV1> postInsertClaimsOlsa(
            @ApiParam(value = "Body of the Claims to be created", required = true)
            @RequestBody ClaimsInsertRequestV1 claimsRequestV1,
            @ApiParam(value = "TypeEnum of User who calls the service", required = true)
            @RequestHeader(name = "nemo-user-id") String userId,
            @RequestHeader(name = "nemo-user-first-name") String userName,
            @RequestHeader(name = "nemo-user-last-name") String lastName
    ) throws IOException {

        String claimsId = UUID.randomUUID().toString();
        Command claimsInsertCommand = new ClaimsInsertOlsaCommand(
                claimsId,
                userId,
                userName + " " + lastName,
                null,
                claimsRequestV1.getWithCounterparty(),
                claimsRequestV1.getInEvidence(),
                claimsRequestV1.getComplaint(),
                claimsRequestV1.getDamaged(),
                claimsRequestV1.getCounterparty(),
                claimsRequestV1.getDeponentList(),
                claimsRequestV1.getWoundedList(),
                claimsRequestV1.getPaiComunication(),
                claimsRequestV1.getForced(),
                claimsRequestV1.getCaiRequest(),
                claimsRequestV1.getIdSaleforce(),
                claimsRequestV1.getRefund(),
                claimsRequestV1.getMetadata(),
                claimsRequestV1.getTheft(),
                claimsRequestV1.getNotes(),
                claimsRequestV1.getCompleteDocumentation()
        );

        commandBus.execute(claimsInsertCommand);

        return new ResponseEntity<>(ClaimsAdapter.adptIdToIdResponse(claimsId), HttpStatus.OK);
    }

    //REFACTOR
    /*INSERIMENTO CLAIMS IN BOZZA*/
    @PostMapping(value = "/claims/draft",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Insert Claims", notes = "Insert claim using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = ClaimsResponseIdV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ClaimsResponseIdV1> postInsertClaimsAdminOnDraft(
            @ApiParam(value = "Body of the Claims to be saved on draft status", required = true)
            @RequestBody ClaimsUpdateRequestV1 claimsRequestV1,
            @ApiParam(value = "TypeEnum of User who calls the service", required = true)
            @RequestHeader(name = "nemo-user-id") String userId,
            @RequestHeader(name = "nemo-user-first-name") String userName,
            @RequestHeader(name = "nemo-user-last-name") String lastName
    ) throws IOException {

        String claimsId = UUID.randomUUID().toString();

        Command claimsInsertDraftCommand = new ClaimsInsertDraftCommand(
                claimsId,
                userId,
                userName + " " + lastName,
                ClaimsStatusEnum.DRAFT,
                claimsRequestV1.getComplaint(),
                claimsRequestV1.getDamaged(),
                claimsRequestV1.getCounterparty(),
                claimsRequestV1.getDeponentList(),
                claimsRequestV1.getWoundedList(),
                claimsRequestV1.getCai(),
                claimsRequestV1.getIdSaleforce(),
                claimsRequestV1.getPaiComunication(),
                claimsRequestV1.getForms(),
                claimsRequestV1.getNotes(),
                claimsRequestV1.getFoundModel(),
                claimsRequestV1.getRefund(),
                claimsRequestV1.getMetadata(),
                claimsRequestV1.getWithCounterparty()
        );

        commandBus.execute(claimsInsertDraftCommand);
        return new ResponseEntity<>(ClaimsAdapter.adptIdToIdResponse(claimsId), HttpStatus.OK);
    }

    // REFACTOR
    /*INSERIMENTO CLAIMS OPERATORE DENUNCIA*/
    /*Status di default: WAITING_FOR_AUTHORITY*/
    @PostMapping(value = "/claims/complaintoperator",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Insert Claims", notes = "Insert claim using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = ClaimsResponseIdV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ClaimsResponseIdV1> postInsertClaimsComplaintOperator(
            @ApiParam(value = "Body of the Claims to be created", required = true)
            @RequestBody ClaimsInternalRequestV1 claimsRequestV1,
            @ApiParam(value = "TypeEnum of User who calls the service", required = true)
            @RequestHeader(name = "nemo-user-id") String userId,
            @RequestHeader(name = "nemo-user-first-name") String userName,
            @RequestHeader(name = "nemo-user-last-name") String lastName,
            @RequestParam(name="is_incomplete", defaultValue = "false") boolean isIncomplete
    ) throws IOException {

        String claimsId = UUID.randomUUID().toString();
        Command claimsInsertValidatedCommand = new ClaimsInsertValidatedCommand(
                claimsId,
                userId,
                userName + " " + lastName,
                null,
                claimsRequestV1.getClaims().getWithCounterparty(),
                claimsRequestV1.getClaims().getInEvidence(),
                claimsRequestV1.getClaims().getComplaint(),
                claimsRequestV1.getClaims().getDamaged(),
                claimsRequestV1.getClaims().getCounterparty(),
                claimsRequestV1.getClaims().getDeponentList(),
                claimsRequestV1.getClaims().getWoundedList(),
                claimsRequestV1.getClaims().getPaiComunication(),
                claimsRequestV1.getClaims().getForced(),
                claimsRequestV1.getClaims().getCaiRequest(),
                claimsRequestV1.getClaims().getIdSaleforce(),
                claimsRequestV1.getClaims().getRefund(),
                claimsRequestV1.getClaims().getMetadata(),
                claimsRequestV1.getClaims().getTheft(),
                claimsRequestV1.getEmailTemplateList(),
                claimsRequestV1.getClaims().getNotes(),
                claimsRequestV1.getClaims().getCompleteDocumentation(),
                isIncomplete,
                claimsRequestV1.getClaims().getMotivation());

        try {
            commandBus.execute(claimsInsertValidatedCommand);
        } catch (RuntimeException | IOException e) {
            Throwable rootCause = com.google.common.base.Throwables.getRootCause(e);
            if (rootCause instanceof PSQLException) {
                LOGGER.debug(MessageCode.CLAIMS_1087.value());
                throw new BadRequestException(MessageCode.CLAIMS_1087, e);
            } else {
                LOGGER.debug(e.getMessage());
                throw e;
            }
        }

        return new ResponseEntity<>(ClaimsAdapter.adptIdToIdResponse(claimsId), HttpStatus.OK);
    }

    //REFACTOR
    /*INSERIMENTO CLAIMS LOJACK*/
    /*Status di default: INCOMPLETE se non c'è controparte, altrimenti exception*/
    @PostMapping(value = "/claims/lojack",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Insert Claims", notes = "Insert claim using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = ClaimsResponseIdV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ClaimsResponseIdV1> postInsertClaimsLojack(
            @ApiParam(value = "Body of the Claims to be created", required = true)
            @RequestBody ClaimsInsertRequestV1 claimsRequestV1,
            @ApiParam(value = "TypeEnum of User who calls the service", required = true)
            @RequestHeader(name = "nemo-user-id") String userId,
            @RequestHeader(name = "nemo-user-first-name") String userName,
            @RequestHeader(name = "nemo-user-last-name") String lastName
    ) throws IOException {

        String claimsId = UUID.randomUUID().toString();
        Command claimsInsertLojackCommand = new ClaimsInsertLojackCommand(
                claimsId,
                userId,
                userName+" "+lastName,
                null,
                claimsRequestV1.getWithCounterparty(),
                claimsRequestV1.getInEvidence(),
                claimsRequestV1.getComplaint(),
                claimsRequestV1.getDamaged(),
                claimsRequestV1.getCounterparty(),
                claimsRequestV1.getDeponentList(),
                claimsRequestV1.getWoundedList(),
                claimsRequestV1.getPaiComunication(),
                claimsRequestV1.getForced(),
                claimsRequestV1.getCaiRequest(),
                claimsRequestV1.getIdSaleforce(),
                claimsRequestV1.getRefund(),
                claimsRequestV1.getMetadata(),
                claimsRequestV1.getTheft(),
                claimsRequestV1.getCompleteDocumentation()
        );

        commandBus.execute(claimsInsertLojackCommand);

        return new ResponseEntity<>(ClaimsAdapter.adptIdToIdResponse(claimsId), HttpStatus.OK);
    }

    //REFACTOR
    /*MODIFICA TOTALE DEL CLAIMS*/
    /* da richiamare prima della put del sinistro*/
    @PostMapping(value = "claims/check/update",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Upload Claims", notes = "Upload claim usin ifno passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = CheckUpdateProcedureResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<CheckUpdateProcedureResponseV1> checkModifyClaims(
            @ApiParam(value = "Updated body of the Claim", required = true)
            @RequestBody ClaimsUpdateRequestV1 claimsRequestV1
    ) throws IOException {

        return new ResponseEntity<>(new CheckUpdateProcedureResponseV1(claimsService.checkModifyClaims(claimsRequestV1)), HttpStatus.OK);
    }


    //REFACTOR
    /*MODIFICA TOTALE DEL CLAIMS*/
    @PutMapping(value = "/claims/{UUID_CLAIMS}",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Upload Claims", notes = "Upload claim usin ifno passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ClaimsResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ClaimsResponseV1> putModifyClaims(
            @ApiParam(value = "UUID that identifies the Calim to be modified", required = true)
            @PathVariable(name = "UUID_CLAIMS") String claimsId,
            @ApiParam(value = "Updated body of the Claim", required = true)
            @RequestBody ClaimsUpdateRequestV1 claimsRequestV1,
            @ApiParam(value = "TypeEnum of User who calls the service", required = true)
            @RequestHeader(name = "nemo-user-id") String userId,
            @RequestHeader(name = "nemo-user-first-name") String userName,
            @RequestHeader(name = "nemo-user-last-name") String lastName,
            @RequestHeader(name = "nemo-procedure", required = false) ClaimsUpdateProcedureEnum claimsUpdateProcedureEnum
    ) throws IOException {

        Command claimsUpdateCommand = null;

        String newClaimsId = UUID.randomUUID().toString();

        claimsUpdateCommand = new ClaimsUpdateCommand(
                claimsId,
                DateUtil.getNowDate(),
                claimsRequestV1.getPaiComunication(),
                claimsRequestV1.getComplaint(),
                claimsRequestV1.getWithCounterparty(),
                claimsRequestV1.getForms(),
                claimsRequestV1.getDamaged(),
                claimsRequestV1.getCai(),
                claimsRequestV1.getExemption(),
                claimsRequestV1.getCounterparty(),
                claimsRequestV1.getDeponentList(),
                claimsRequestV1.getWoundedList(),
                claimsRequestV1.getNotes(),
                claimsRequestV1.getFoundModel(),
                claimsRequestV1.getIdSaleforce(),
                claimsRequestV1.getRefund(),
                claimsRequestV1.getTheft(),
                userId,
                userName + " " + lastName,
                claimsRequestV1.getType(),
                claimsRequestV1.getPoVariation(),
                newClaimsId,
                claimsRequestV1.getLegalComunication(),
                claimsRequestV1.getCompleteDocumentation()
        );

        commandBus.execute(claimsUpdateCommand);

        String outputClaimId = null;

        if (claimsUpdateProcedureEnum == null ||
                claimsUpdateProcedureEnum.equals(ClaimsUpdateProcedureEnum.UPDATE) ||
                claimsUpdateProcedureEnum.equals(ClaimsUpdateProcedureEnum.CHANGE_POLICY)
        ) {
            outputClaimId = claimsId;
        } else {
            outputClaimId = newClaimsId;
        }


        ClaimsEntity claimsEntity = claimsService.findById(outputClaimId);


        return new ResponseEntity<>(ClaimsAdapter.adptClaimsToClaimsResponse(claimsEntity), HttpStatus.OK);
    }

    /* REFACTOR */
    /*RECUPERO CLAIMS TRAMITE ID*/
    @GetMapping(value = "/claims/adminsupervisor/{UUID_CLAIMS}",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Recover Claim", notes = "Returns a Claim using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ClaimsResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ClaimsResponseV1> getClaimsEntityByIdAdminSupervisor(
            @ApiParam(value = "UUID of the Claim to be found", required = true)
            @PathVariable(name = "UUID_CLAIMS") String claimsId) {

        ClaimsEntity claimsEntity = claimsService.findById(claimsId);
        ClaimsResponseV1 claimsResponseV1 = claimsService.isPending(ClaimsAdapter.adptClaimsToClaimsResponse(claimsEntity));
        return new ResponseEntity<>(claimsResponseV1, HttpStatus.OK);
    }


    //REFACTOR
    /*CAMBIO STATO CLAIMS*/
    @PatchMapping(value = "/claims/{UUID_CLAIMS}/{STATUS}",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Upload Claims Status", notes = "Update the claim status using info passed in the request")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ClaimsResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ClaimsResponseV1> patchClaimsStatus(
            @ApiParam(value = "UUID of the Claim", required = true)
            @PathVariable(name = "UUID_CLAIMS") String claimsId,
            @ApiParam(value = "The State in which the Claim must go", required = true)
            @PathVariable(name = "STATUS") String status,
            @ApiParam(value = "Indicates who manages the paratic", required = false)
            @RequestParam(value = "practice_manager", required = false) String practiceManager,
            @ApiParam(value = "Id of the user who calls the service", required = true)
            @RequestHeader(name = "nemo-user-id") String userId,
            @RequestHeader(name = "nemo-user-first-name") String userName,
            @RequestHeader(name = "nemo-user-last-name") String lastName,
            @ApiParam(value = "List of template email to be send", required = true)
            @RequestBody ClaimsPatchStatusRequestV1 claimsPatchStatusRequestV1
    ) throws IOException {


        Command claimsPatchStatusCommand = new ClaimsPatchStatusCommand(
                claimsId,
                ClaimsStatusEnum.create(Util.replaceStringForEnumValue(status)),
                practiceManager,
                userId,
                userName + " " + lastName,
                claimsPatchStatusRequestV1
        );

        //alertService.createAlert(alertRequestV1);
        commandBus.execute(claimsPatchStatusCommand);

        ClaimsEntity claimsEntity = claimsService.findById(claimsId);

        return new ResponseEntity<>(ClaimsAdapter.adptClaimsToClaimsResponse(claimsEntity), HttpStatus.OK);


    }


    //REFACTOR
    /*CAMBIO SEGUITI THEFT*/
    @PatchMapping(value = "/claims/theft/{UUID_CLAIMS}",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Upload Claims Theft Receptions", notes = "Update the claim receptions for theft using info passed in the request")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ClaimsResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ClaimsResponseV1> patchTheftReceptions(
            @ApiParam(value = "UUID of the Claim", required = true)
            @PathVariable(name = "UUID_CLAIMS") String claimsId,
            @ApiParam(value = "Id of the user who calls the service", required = true)
            @RequestHeader(name = "nemo-user-id") String userId,
            @RequestHeader(name = "nemo-user-first-name") String userName,
            @RequestHeader(name = "nemo-user-last-name") String lastName,
            @ApiParam(value = "Request to be send", required = true)
            @RequestBody ClaimsPatchReceptionsRequestV1 claimsPatchReceptionsRequestV1
    ) throws IOException {

        lockService.checkLock(userId, claimsId);
        Command claimsPatchReceptionsCommand = new ClaimsPatchReceptionsCommand(
                claimsId,
                DateUtil.convertIS08601StringToUTCDate(claimsPatchReceptionsRequestV1.getFirstKeyReception()),
                DateUtil.convertIS08601StringToUTCDate(claimsPatchReceptionsRequestV1.getSecondKeyReception()),
                DateUtil.convertIS08601StringToUTCDate(claimsPatchReceptionsRequestV1.getOriginalComplaintReception()),
                DateUtil.convertIS08601StringToUTCDate(claimsPatchReceptionsRequestV1.getCopyComplaintReception()),
                DateUtil.convertIS08601StringToUTCDate(claimsPatchReceptionsRequestV1.getOriginalReportReception()),
                DateUtil.convertIS08601StringToUTCDate(claimsPatchReceptionsRequestV1.getCopyReportReception()));


        //alertService.createAlert(alertRequestV1);
        commandBus.execute(claimsPatchReceptionsCommand);
        return new ResponseEntity<>(ClaimsAdapter.adptClaimsToClaimsResponse(claimsService.findById(claimsId)), HttpStatus.OK);

    }


    //REFACTOR
    /*MODIFICA PARZIALE DI UN CLAIMS*/
    @PatchMapping(value = "/claims/{UUID_CLAIMS}",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Upload a part of the Claim", notes = "Update a part of the Claim using the info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ClaimsResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ClaimsResponseV1> patchClaims(
            @ApiParam(value = "UUID of the Claim", required = true)
            @PathVariable(name = "UUID_CLAIMS") String claimsId,
            @ApiParam(value = "Id of the user who calls the service", required = true)
            @RequestHeader(name = "nemo-user-id") String userId,
            @RequestHeader(name = "nemo-user-first-name") String userName,
            @RequestHeader(name = "nemo-user-last-name") String lastName,
            @ApiParam(value = "Some attributes of the body of the Claim to be modified", required = true)
            @RequestBody ClaimsPatchRequestV1 claimsRequestV1) throws IOException {


        Command claimsPatchCommand = new ClaimsPatchCommand(
                claimsId,
                DateUtil.getNowInstant(),
                claimsRequestV1.getPaiComunication(),
                claimsRequestV1.getWithCounterparty(),
                claimsRequestV1.getComplaint(),
                claimsRequestV1.getForms(),
                claimsRequestV1.getDamaged(),
                claimsRequestV1.getCai(),
                claimsRequestV1.getExemption(),
                claimsRequestV1.getCounterparty(),
                claimsRequestV1.getDeponentList(),
                claimsRequestV1.getWoundedList(),
                claimsRequestV1.getNotes(),
                claimsRequestV1.getFoundModel(),
                claimsRequestV1.getIdSaleforce(),
                claimsRequestV1.getRefund(),
                claimsRequestV1.getTheft(),
                userId,
                userName + " " + lastName,
                claimsRequestV1.getPoVariation(),
                claimsRequestV1.getLegalComunication(),
                claimsRequestV1.getCompleteDocumentation()
         );
        commandBus.execute(claimsPatchCommand);

        return new ResponseEntity<>(ClaimsAdapter.adptClaimsToClaimsResponse(claimsService.findById(claimsId)), HttpStatus.OK);
    }

    //REFACTOR
    /*BLOCCA/SBOLOCCA CLAIMS*/
    @PatchMapping(value = "/claims/lock/{UUID_CLAIMS}",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Lock or Unlock Claim", notes = "")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ClaimsResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ClaimsResponseV1> patchIsLockedClaims(
            @ApiParam(value = "UUID of the Claim", required = true)
            @PathVariable(name = "UUID_CLAIMS") String claimsId) throws IOException {

        Command claimsPatchLockedCommand = new ClaimsPatchLockedCommand(claimsId);
        commandBus.execute(claimsPatchLockedCommand);

        return new ResponseEntity<>(ClaimsAdapter.adptClaimsToClaimsResponse(claimsService.findById(claimsId)), HttpStatus.OK);
    }

    /*REFACTOR*/
    /*STATISTICHE CLAIMS CON IN-EVIDENCE*/
    /*@GetMapping(value = "/claims/evidence/stats",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ClaimsResponseStatsEvidenceV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ClaimsResponseStatsEvidenceV1> getClaimsStatsEvidenceAdminSuperviros() {

        try {
            return new ResponseEntity<>(ClaimsAdapter.adpdStatsEvidenceToStatsEvidenceResponse(claimsService.getClaimsStats(true)), HttpStatus.OK);
        } catch (NotFoundException ex) {
            LOGGER.debug(ex.getMessage());
            throw ex;
        }
    }*/


    @GetMapping(value = "/claims/evidence/stats",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ClaimsResponseStatsEvidenceV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ClaimsResponseStatsEvidenceV2> getClaimsStatsEvidenceAdminSupervirosTotCounter(
            @RequestParam(value = "in_evidence", required = false) Boolean inEvidence,
            @RequestParam(value = "with_continuation", required = false) Boolean withContinuation

    ) {

        try {
            return new ResponseEntity<>(claimsService.getClaimsStatsV2(inEvidence, withContinuation), HttpStatus.OK);
        } catch (NotFoundException ex) {
            LOGGER.info(ex.getMessage());
            throw ex;
        }
    }

    //REFACTOR
    /*PAGINAZIONE CLAIMS*/
    /* recupera tutto */
    @GetMapping(value = "/claims/adminsupervisor",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ClaimsResponsePaginationV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ClaimsResponsePaginationV1> getClaimsEntityPaginationAdminSupervisor(
            @ApiParam(value = "Defines how many Claims can contain the single page", required = false)
            @RequestParam(value = "page_size", defaultValue = "20") Integer pageSize,
            @ApiParam(value = "Defines the page number to be displayed", required = false)
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @ApiParam(value = "Filter the search by practice_id", required = false)
            @RequestParam(value = "practice_id", required = false) Long practiceId,
            @ApiParam(value = "Filter the search for the flow to which the Claim belongs", required = false)
            @RequestParam(value = "flow", required = false) String flow,
            @ApiParam(value = "Filter the search by client_id", required = false)
            @RequestParam(value = "client_id", required = false) List<Long> clientId,
            @ApiParam(value = "Filter the search by client_name", required = false)
            @RequestParam(value = "client_name", required = false) String clientName,
            @ApiParam(value = "Filter the search by the date the Claim was created from", example = "2018-01-01", required = false)
            @RequestParam(value = "date_accident_from", required = false) String dateAccidentFrom,
            @ApiParam(value = "Filter the search by the date the Claim was created to", example = "2018-01-01", required = false)
            @RequestParam(value = "date_accident_to", required = false) String dateAccidentTo,
            @ApiParam(value = "Filter the search by the type of accident", required = false)
            @RequestParam(value = "type_accident", required = false) String typeAccident,
            @ApiParam(value = "Filter the search by plate", required = false)
            @RequestParam(value = "plate", required = false) String plate,
            @ApiParam(value = "Filter the search for the status of the Claim", required = false)
            @RequestParam(value = "status", required = false) String status,
            @ApiParam(value = "Filter the search for the locator", required = false)
            @RequestParam(value = "locator", required = false) String locator,
            @ApiParam(value = "Filter the search for in evidence Claims", required = false)
            @RequestParam(value = "in_evidence", required = false) Boolean inEvidence,
            @ApiParam(value = "Order", required = false)
            @RequestParam(value = "asc", required = false, defaultValue = "true") Boolean asc,
            @ApiParam(value = "OrderBy", required = false)
            @RequestParam(value = "order_by", defaultValue = "created_at", required = false) String orderBy,
            @ApiParam(value = "Filter the search for with continuation Claims", required = false)
            @RequestParam(value = "with_continuation", required = false) Boolean withContinuation,
            @ApiParam(value = "Filter the search for Company name Claims", required = false)
            @RequestParam(value = "company", required = false) String company,
            @ApiParam(value = "Filter the search for with receptions of Theft", required = false)
            @RequestParam(value = "with_receptions", required = false) Boolean withReceptions,
            @ApiParam(value = "Filter the search for po_variation", required = false)
            @RequestParam(value = "po_variation", required = false) Boolean poVariation,
            @ApiParam(value = "Filter the search for fleet_vehicle_id", required = false)
            @RequestParam(value = "fleet_vehicle_id", required = false) Long fleetVehicleId

    ) {

        Pagination<ClaimsEntity> claimsPagination = claimsService.getClaimsPagination(inEvidence, practiceId, flow, clientId, plate, status, locator, dateAccidentFrom, dateAccidentTo, typeAccident, asc, orderBy, page, pageSize, withContinuation, company, withReceptions, clientName, poVariation, fleetVehicleId);
        return new ResponseEntity<>(ClaimsAdapter.adptClaimsPaginationToClaimsPaginationResponse(claimsPagination), HttpStatus.OK);
    }


    @GetMapping(value = "/claims/pagination/export",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ClaimsResponsePaginationV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<IdentifierResponseV1> getClaimsEntityPaginationExport(
            @ApiParam(value = "Defines how many Claims can contain the single page", required = false)
            @RequestParam(value = "page_size", defaultValue = "20") Integer pageSize,
            @ApiParam(value = "Defines the page number to be displayed", required = false)
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @ApiParam(value = "Filter the search by practice_id", required = false)
            @RequestParam(value = "practice_id", required = false) Long practiceId,
            @ApiParam(value = "Filter the search for the flow to which the Claim belongs", required = false)
            @RequestParam(value = "flow", required = false) String flow,
            @ApiParam(value = "Filter the search by client_id", required = false)
            @RequestParam(value = "client_id", required = false) List<Long> clientIdList,
            @ApiParam(value = "Filter the search by client_name", required = false)
            @RequestParam(value = "client_name", required = false) String clientName,
            @ApiParam(value = "Filter the search by the date the Claim was created from", example = "2018-01-01", required = false)
            @RequestParam(value = "date_accident_from", required = false) String dateAccidentFrom,
            @ApiParam(value = "Filter the search by the date the Claim was created to", example = "2018-01-01", required = false)
            @RequestParam(value = "date_accident_to", required = false) String dateAccidentTo,
            @ApiParam(value = "Filter the search by the type of accident", required = false)
            @RequestParam(value = "type_accident", required = false) String typeAccident,
            @ApiParam(value = "Filter the search by plate", required = false)
            @RequestParam(value = "plate", required = false) String plate,
            @ApiParam(value = "Filter the search for the status of the Claim", required = false)
            @RequestParam(value = "status", required = false) String status,
            @ApiParam(value = "Filter the search for the locator", required = false)
            @RequestParam(value = "locator", required = false) String locator,
            @ApiParam(value = "Filter the search for in evidence Claims", required = false)
            @RequestParam(value = "in_evidence", required = false) Boolean inEvidence,
            @ApiParam(value = "Order", required = false)
            @RequestParam(value = "asc", required = false, defaultValue = "true") Boolean asc,
            @ApiParam(value = "OrderBy", required = false)
            @RequestParam(value = "order_by", defaultValue = "created_at", required = false) String orderBy,
            @ApiParam(value = "Filter the search for with continuation Claims", required = false)
            @RequestParam(value = "with_continuation", required = false) Boolean withContinuation,
            @ApiParam(value = "Filter the search for Company name Claims", required = false)
            @RequestParam(value = "company", required = false) String company,
            @ApiParam(value = "Filter the search for po_variation", required = false)
            @RequestParam(value = "po_variation", required = false) Boolean poVariation,
            @ApiParam(value = "Filter the search for waiting_for_number_sx", required = false)
            @RequestParam(value = "waiting_for_number_sx", required = false) Boolean waitingForNumberSx,
            @ApiParam(value = "Filter the search for fleet_vehicle_id", required = false)
            @RequestParam(value = "fleet_vehicle_id", required = false) Long fleetVehicleId,
            @ApiParam(value = "Filter the search for waiting_for_authority", required = false)
            @RequestParam(value = "waiting_for_authority", required = false) Boolean waitingForAuthority
    ) throws IOException {
        UploadFileResponseV1 csvUploadResponse = claimsService.buildCsvPaginationExport(inEvidence, practiceId, flow, clientIdList, plate, status, locator, dateAccidentFrom, dateAccidentTo, typeAccident, asc, orderBy, page, pageSize, withContinuation, company, clientName, poVariation, waitingForNumberSx, fleetVehicleId, waitingForAuthority);
        IdentifierResponseV1 response = new IdentifierResponseV1();
        response.setId(csvUploadResponse.getUuid());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


    /*PAGINAZIONE CLAIMS*/
    /* recupera claims validi per authority */
    /*@GetMapping(value = "/claims/pagination/authority",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ClaimsResponsePaginationV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ClaimsResponsePaginationV1> getClaimsEntityPaginationAuthority(
            @ApiParam(value = "Defines how many Claims can contain the single page", required = false)
            @RequestParam(value = "page_size", defaultValue = "20") Integer pageSize,
            @ApiParam(value = "Defines the page number to be displayed", required = false)
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @ApiParam(value = "Filter the search by plate", required = false)
            @RequestParam(value = "plate", required = false) String plate

    ) {

        Pagination<ClaimsEntity> claimsPagination = claimsService.getClaimsPaginationAuthority(plate, page, pageSize);
        return new ResponseEntity<>(ClaimsAdapter.adptClaimsPaginationToClaimsPaginationResponse(claimsPagination), HttpStatus.OK);
    }*/


    // REFACTOR
    /*PAGINAZIONE CLAIMS OPERATORE DENUNCIA*/
    /* senza bozza e senza theft */
    @GetMapping(value = "/claims/complaintoperator",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ClaimsResponsePaginationV2.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ClaimsResponsePaginationV2> getClaimsViewEntityPaginationComplaintOperator(
            @ApiParam(value = "Defines how many Claims can contain the single page", required = false)
            @RequestParam(value = "page_size", defaultValue = "20") Integer pageSize,
            @ApiParam(value = "Defines the page number to be displayed", required = false)
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @ApiParam(value = "Filter the search by practice_id", required = false)
            @RequestParam(value = "practice_id", required = false) Long practiceId,
            @ApiParam(value = "Filter the search for the flow to which the Claim belongs", required = false)
            @RequestParam(value = "flow", required = false) String flow,
            @ApiParam(value = "Filter the search by client_id", required = false)
            @RequestParam(value = "client_id", required = false) List<Long> clientIdList,
            @ApiParam(value = "Filter the search by client_name", required = false)
            @RequestParam(value = "client_name", required = false) String clientName,
            @ApiParam(value = "Filter the search by the date the Claim was created from", example = "2018-01-01", required = false)
            @RequestParam(value = "date_accident_from", required = false) String dateAccidentFrom,
            @ApiParam(value = "Filter the search by the date the Claim was created to", example = "2018-01-01", required = false)
            @RequestParam(value = "date_accident_to", required = false) String dateAccidentTo,
            @ApiParam(value = "Filter the search by the type of accident", required = false)
            @RequestParam(value = "type_accident", required = false) String typeAccident,
            @ApiParam(value = "Filter the search by plate", required = false)
            @RequestParam(value = "plate", required = false) String plate,
            @ApiParam(value = "Filter the search for the status of the Claim", required = false)
            @RequestParam(value = "status", required = false) String status,
            @ApiParam(value = "Filter the search for the locator", required = false)
            @RequestParam(value = "locator", required = false) String locator,
            @ApiParam(value = "Filter the search for in evidence Claims", required = false)
            @RequestParam(value = "in_evidence", required = false) Boolean inEvidence,
            @ApiParam(value = "Order", required = false)
            @RequestParam(value = "asc", required = false, defaultValue = "true") Boolean asc,
            @ApiParam(value = "OrderBy", required = false)
            @RequestParam(value = "order_by", defaultValue = "created_at", required = false) String orderBy,
            @ApiParam(value = "Filter the search for with continuation Claims", required = false)
            @RequestParam(value = "with_continuation", required = false) Boolean withContinuation,
            @ApiParam(value = "Filter the search for Company name Claims", required = false)
            @RequestParam(value = "company", required = false) String company,
            @ApiParam(value = "Filter the search for po_variation", required = false)
            @RequestParam(value = "po_variation", required = false) Boolean poVariation,
            @ApiParam(value = "Filter the search for waiting_for_number_sx", required = false)
            @RequestParam(value = "waiting_for_number_sx", required = false) Boolean waitingForNumberSx,
            @ApiParam(value = "Filter the search for fleet_vehicle_id", required = false)
            @RequestParam(value = "fleet_vehicle_id", required = false) Long fleetVehicleId,
            @ApiParam(value = "Filter the search for waiting_for_authority", required = false)
            @RequestParam(value = "waiting_for_authority", required = false) Boolean waitingForAuthority


    ) {

        FilterView claimsRequestFilterViewDTO = claimsSearchRequestDTOAdapter.adaptRequestToDTO(inEvidence, practiceId, flow, clientIdList, plate, status, locator, dateAccidentFrom, dateAccidentTo, typeAccident,  withContinuation, company, clientName, poVariation, waitingForNumberSx, fleetVehicleId, waitingForAuthority);
        ClaimsViewSearchDTO searchDTO =  claimsService.getClaimsViewPaginationWithoutDraftAndTheft(claimsRequestFilterViewDTO,asc, orderBy, page, pageSize);

        try{

            ClaimsResponsePaginationV2 response = new ClaimsResponsePaginationV2(statsDTOResponseAdapter.adaptToResponse(searchDTO.getStats()),
                    claimsSearchDTOResponseV2Adapter.adaptToResponseLite(searchDTO.getClaims()));

            return new ResponseEntity<>(response, HttpStatus.OK);

        } catch (AbstractException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new InternalException(MessageCode.CLAIMS_2015, e);
        }
       // return new ResponseEntity<>(ClaimsAdapter.adptClaimsPaginationToClaimsViewPaginationResponseComplaintOperator(claimsPagination), HttpStatus.OK);


    }

   /* @GetMapping(value = "/claims/complaintoperator",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ClaimsResponsePaginationV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ClaimsResponsePaginationV1> getClaimsEntityPaginationComplaintOperator(
            @ApiParam(value = "Defines how many Claims can contain the single page", required = false)
            @RequestParam(value = "page_size", defaultValue = "20") Integer pageSize,
            @ApiParam(value = "Defines the page number to be displayed", required = false)
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @ApiParam(value = "Filter the search by practice_id", required = false)
            @RequestParam(value = "practice_id", required = false) Long practiceId,
            @ApiParam(value = "Filter the search for the flow to which the Claim belongs", required = false)
            @RequestParam(value = "flow", required = false) String flow,
            @ApiParam(value = "Filter the search by client_id", required = false)
            @RequestParam(value = "client_id", required = false) List<Long> clientIdList,
            @ApiParam(value = "Filter the search by client_name", required = false)
            @RequestParam(value = "client_name", required = false) String clientName,
            @ApiParam(value = "Filter the search by the date the Claim was created from", example = "2018-01-01", required = false)
            @RequestParam(value = "date_accident_from", required = false) String dateAccidentFrom,
            @ApiParam(value = "Filter the search by the date the Claim was created to", example = "2018-01-01", required = false)
            @RequestParam(value = "date_accident_to", required = false) String dateAccidentTo,
            @ApiParam(value = "Filter the search by the type of accident", required = false)
            @RequestParam(value = "type_accident", required = false) String typeAccident,
            @ApiParam(value = "Filter the search by plate", required = false)
            @RequestParam(value = "plate", required = false) String plate,
            @ApiParam(value = "Filter the search for the status of the Claim", required = false)
            @RequestParam(value = "status", required = false) String status,
            @ApiParam(value = "Filter the search for the locator", required = false)
            @RequestParam(value = "locator", required = false) String locator,
            @ApiParam(value = "Filter the search for in evidence Claims", required = false)
            @RequestParam(value = "in_evidence", required = false) Boolean inEvidence,
            @ApiParam(value = "Order", required = false)
            @RequestParam(value = "asc", required = false, defaultValue = "true") Boolean asc,
            @ApiParam(value = "OrderBy", required = false)
            @RequestParam(value = "order_by", defaultValue = "created_at", required = false) String orderBy,
            @ApiParam(value = "Filter the search for with continuation Claims", required = false)
            @RequestParam(value = "with_continuation", required = false) Boolean withContinuation,
            @ApiParam(value = "Filter the search for Company name Claims", required = false)
            @RequestParam(value = "company", required = false) String company,
            @ApiParam(value = "Filter the search for po_variation", required = false)
            @RequestParam(value = "po_variation", required = false) Boolean poVariation,
            @ApiParam(value = "Filter the search for waiting_for_number_sx", required = false)
            @RequestParam(value = "waiting_for_number_sx", required = false) Boolean waitingForNumberSx,
            @ApiParam(value = "Filter the search for fleet_vehicle_id", required = false)
            @RequestParam(value = "fleet_vehicle_id", required = false) Long fleetVehicleId,
            @ApiParam(value = "Filter the search for waiting_for_authority", required = false)
            @RequestParam(value = "waiting_for_authority", required = false) Boolean waitingForAuthority


    ) {

        Pagination<ClaimsEntity> claimsPagination = claimsService.getClaimsPaginationWithoutDraftAndTheft(inEvidence, practiceId, flow, clientIdList, plate, status, locator, dateAccidentFrom, dateAccidentTo, typeAccident, asc, orderBy, page, pageSize, withContinuation, company, clientName, poVariation, waitingForNumberSx, fleetVehicleId, waitingForAuthority);
        return new ResponseEntity<>(ClaimsAdapter.adptClaimsPaginationToClaimsPaginationResponseComplaintOperator(claimsPagination), HttpStatus.OK);
    }
*/

    // REFACTOR
    /*PAGINAZIONE CLAIMS OPERATORE FURTI*/
    /* senza bozza e solo theft */
    @GetMapping(value = "/claims/theftoperator",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ClaimsResponsePaginationV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ClaimsResponsePaginationV1> getClaimsEntityPaginationTheftOperator(
            @ApiParam(value = "Defines how many Claims can contain the single page", required = false)
            @RequestParam(value = "page_size", defaultValue = "20") Integer pageSize,
            @ApiParam(value = "Defines the page number to be displayed", required = false)
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @ApiParam(value = "Filter the search by practice_id", required = false)
            @RequestParam(value = "practice_id", required = false) Long practiceId,
            @ApiParam(value = "Filter the search for the flow to which the Claim belongs", required = false)
            @RequestParam(value = "flow", required = false) String flow,
            @ApiParam(value = "Filter the search by client_id", required = false)
            @RequestParam(value = "client_id", required = false) String clientId,
            @ApiParam(value = "Filter the search by client_name", required = false)
            @RequestParam(value = "client_name", required = false) String clientName,
            @ApiParam(value = "Filter the search by the date the Claim was created from", example = "2018-01-01", required = false)
            @RequestParam(value = "date_accident_from", required = false) String dateAccidentFrom,
            @ApiParam(value = "Filter the search by the date the Claim was created to", example = "2018-01-01", required = false)
            @RequestParam(value = "date_accident_to", required = false) String dateAccidentTo,
            @ApiParam(value = "Filter the search by the type of accident", required = false)
            @RequestParam(value = "type_accident", required = false) String typeAccident,
            @ApiParam(value = "Filter the search by plate", required = false)
            @RequestParam(value = "plate", required = false) String plate,
            @ApiParam(value = "Filter the search for the status of the Claim", required = false)
            @RequestParam(value = "status", required = false) String status,
            @ApiParam(value = "Filter the search for the locator", required = false)
            @RequestParam(value = "locator", required = false) String locator,
            @ApiParam(value = "Filter the search for in evidence Claims", required = false)
            @RequestParam(value = "in_evidence", required = false) Boolean inEvidence,
            @ApiParam(value = "Order", required = false)
            @RequestParam(value = "asc", required = false, defaultValue = "true") Boolean asc,
            @ApiParam(value = "OrderBy", required = false)
            @RequestParam(value = "order_by", defaultValue = "created_at", required = false) String orderBy,
            @ApiParam(value = "Filter the search for with continuation Claims", required = false)
            @RequestParam(value = "with_continuation", required = false) Boolean withContinuation,
            @ApiParam(value = "Filter the search for Company name Claims", required = false)
            @RequestParam(value = "company", required = false) String company,
            @ApiParam(value = "Filter the search for po_variation", required = false)
            @RequestParam(value = "po_variation", required = false) Boolean poVariation,
            @ApiParam(value = "Filter the search for waiting_for_number_sx", required = false)
            @RequestParam(value = "waiting_for_number_sx", required = false) Boolean waitingForNumberSx,
            @ApiParam(value = "Filter the search for fleet_vehicle_id", required = false)
            @RequestParam(value = "fleet_vehicle_id", required = false) Long fleetVehicleId,
            @ApiParam(value = "Filter the search for waiting_for_authority", required = false)
            @RequestParam(value = "waiting_for_authority", required = false) Boolean waitingForAuthority

    ) {
        Pagination<ClaimsEntity> claimsPagination = claimsService.getClaimsPaginationTheftOperator(inEvidence, practiceId, flow, clientId, plate, status, locator, dateAccidentFrom, dateAccidentTo, typeAccident, asc, orderBy, page, pageSize, withContinuation, company, clientName, poVariation, waitingForNumberSx, fleetVehicleId, waitingForAuthority);
        return new ResponseEntity<>(ClaimsAdapter.adptClaimsPaginationToClaimsPaginationResponse(claimsPagination), HttpStatus.OK);
    }



    /*-------------------------------------NOTE----------------------------------------------*/

    //REFACTOR
    /*INSERIMENTO NOTE*/
    @PostMapping(value = "/claims/notes/{UUID_CLAIMS}",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ClaimsResponseNoteV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ClaimsResponseNoteV1> postClaimNote(
            @ApiParam(value = "UUID of the Claims to which to add a note", required = true)
            @PathVariable(name = "UUID_CLAIMS") String claimsId,
            @ApiParam(value = "user id", required = true)
            @RequestHeader(name = "nemo-user-id") String userId,
            @RequestHeader(name = "nemo-user-first-name") String userName,
            @RequestHeader(name = "nemo-user-last-name") String lastName,
            @ApiParam(value = "Body of the note containing all the information ", required = true)
            @RequestBody NoteRequest note) throws IOException {

        lockService.checkLock(userId, claimsId);
        String noteId = UUID.randomUUID().toString();
        Command claimsInsertNoteCommand = new ClaimsInsertNoteCommand(claimsId, userId, userName + " " + lastName, noteId, notesAdapter.adptNotesRequestToNotes(note));
        commandBus.execute(claimsInsertNoteCommand);

        return new ResponseEntity<>(ClaimsAdapter.adptNoteToNoteEntityResponse(claimsService.findClaimNoteById(noteId)), HttpStatus.OK);

    }

    //REFACTOR
    /*CAMBIO STATO DI UNA NOTA*/
    @PatchMapping(value = "/claims/notes/{UUID_CLAIMS}/{UUID_NOTE}",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = NoteResponse.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<NoteResponse> patchClaimNoteStatus(
            @ApiParam(value = "UUID of the Claim", required = true)
            @PathVariable(name = "UUID_CLAIMS") String claimsId,
            @ApiParam(value = "user id", required = true)
            @RequestHeader(name = "nemo-user-id") String userId,
            @ApiParam(value = "UUID of the Note within the selected Claim", required = true)
            @PathVariable(name = "UUID_NOTE") String noteId) throws IOException {

        lockService.checkLock(userId, claimsId);
        Command claimsPatchNoteStatusCommand = new ClaimsPatchNoteStatusCommand(claimsId, noteId);
        commandBus.execute(claimsPatchNoteStatusCommand);

        return new ResponseEntity<>(NotesAdapter.adptNotesEntityObjectToNotesResponse(claimsService.findClaimNoteById(noteId)), HttpStatus.OK);
    }

    //REFACTOR
    /*CANCELLAZIONE NOTA*/
    @DeleteMapping(value = "/claims/notes/{UUID_CLAIMS}/{UUID_NOTE}",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity deleteClaimsNote(
            @ApiParam(value = "UUID of the Claim", required = true)
            @PathVariable(name = "UUID_CLAIMS") String claimsId,
            @ApiParam(value = "user id", required = true)
            @RequestHeader(name = "nemo-user-id") String userId,
            @ApiParam(value = "UUID of the Note within the selected  Claim", required = true)
            @PathVariable(name = "UUID_NOTE") String noteId) throws IOException {

        lockService.checkLock(userId, claimsId);
        Command claimsDeleteNoteCommand = new ClaimsDeleteNoteCommand(claimsId, noteId);
        commandBus.execute(claimsDeleteNoteCommand);

        return new ResponseEntity<>(HttpStatus.OK);
    }


    //REFACTOR
    /*--------------------CAMBIO STATO HIDDEN NOTE-------------------------*/
    @PatchMapping("/note/{ID_CLAIMS}/{ID_NOTE}/hidden")
    @Transactional
    @ApiOperation(value = "Patch status hidden for note", notes = "Patch status hidden attachment of claims identified by ID_CLAIMS using the ID_NOTE passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ClaimsResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ClaimsResponseV1> patchStatusHiddenNotes(
            @PathVariable("ID_CLAIMS") String idClaims,
            @PathVariable("ID_NOTE") String idNotes,
            @ApiParam(value = "user id", required = true)
            @RequestHeader(name = "nemo-user-id") String userId
    ) throws IOException {

        lockService.checkLock(userId, idClaims);
        Command claimsPatchNoteHiddenCommand = new ClaimsPatchNoteHiddenCommand(idClaims, idNotes);
        commandBus.execute(claimsPatchNoteHiddenCommand);

        return new ResponseEntity<>(ClaimsAdapter.adptClaimsToClaimsResponse(claimsService.findById(idClaims)), HttpStatus.OK);

    }

    /*-------------------------------------STORICO----------------------------------------------*/
    //REFACTOR
    /*STORICO CAMBI DI STATO CLAIMS */
    @GetMapping(value = "/claims/historical/{UUID_CLAIMS}",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = HistoricalResponseV1.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<HistoricalResponseV1> getClaimsHistorical(
            @ApiParam(value = "UUID of the Claim", required = true)
            @PathVariable("UUID_CLAIMS") String idClaims
    )  {

        return new ResponseEntity<>(new HistoricalResponseV1(ClaimsAdapter.adptHistoricalToHistoricalResponse(claimsService.getClaimsHistorical(idClaims))), HttpStatus.OK);

    }

    /*STORICO TARGHE REIMMATRICOLATE */
    @GetMapping(value = "/claims/vehicleregistration/{PLATE}",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = VehicleRegistrationResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<VehicleRegistrationResponseV1> getClaimsRegistrationHistorical(
            @ApiParam(value = "Filter the search by plate", required = true)
            @PathVariable("PLATE") String plate
    ) throws IOException {

        return new ResponseEntity<>(new VehicleRegistrationResponseV1(esbService.getVehicleRegistrationsByPlate(plate)), HttpStatus.OK);
    }


    /*-----------------------------------ENTRUSTED--------------------------------------------------*/
    //REFACTOR
    //AFFIDO MANUALE
    @PatchMapping(value = "/entrust/{UUID_CLAIMS}",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ClaimsEntrustedResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ClaimsEntrustedResponseV1> entrustClaims(
            @RequestBody ClaimsEntrustedRequestV1 claimsEntrustedRequest,
            @ApiParam(value = "UUID of the Claim", required = true)
            @PathVariable("UUID_CLAIMS") String idClaims,
            @ApiParam(value = "user id", required = true)
            @RequestHeader(name = "nemo-user-id") String userId,
            @RequestHeader(name = "nemo-user-first-name") String userName,
            @RequestHeader(name = "nemo-user-last-name") String lastName
    ) throws IOException {
        //lockService.checkLock(userId, idClaims);
        ClaimsPatchEntrustedCommand claimsPatchEntrustedCommand = new ClaimsPatchEntrustedCommand(
                idClaims,
                claimsEntrustedRequest.getEntrustedType(),
                claimsEntrustedRequest.getLegal(),
                claimsEntrustedRequest.getInspectorate(),
                claimsEntrustedRequest.getBroker(),
                claimsEntrustedRequest.getInsuranceManager(),
                new ClaimsPatchStatusRequestV1(claimsEntrustedRequest.getEventType(), claimsEntrustedRequest.getMotivation(), claimsEntrustedRequest.getTemplateList()),
                userId,
                userName + " " + lastName
        );

        commandBus.execute(claimsPatchEntrustedCommand);

        return new ResponseEntity<>(entrustedAdapter.adptEntrustedToClaimsEntrustedResponse(claimsService.findById(idClaims)), HttpStatus.OK);
    }



    /*-----------------CAMBIO STATO HIDDEN ALLEGATI--------------------*/
    @PatchMapping("/attachment/{ID_CLAIMS}/{ID_ATTACHMENT}/hidden")
    @Transactional
    @ApiOperation(value = "Patch status hidden for attachment", notes = "Patch status hidden attachment of claims identified by ID_CLAIMS using the ID_ATTACHMENT passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ClaimsResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ClaimsResponseV1> patchStatusHiddenAttachment(
            @PathVariable("ID_CLAIMS") String idClaims,
            @PathVariable("ID_ATTACHMENT") String idAttachments,
            @ApiParam(value = "user id", required = true)
            @RequestHeader(name = "nemo-user-id") String userId
    ) {
        lockService.checkLock(userId, idClaims);
        ClaimsEntity claimsEntity =claimsService.toggleHiddenAttachment(idClaims,idAttachments);

        return new ResponseEntity<>(ClaimsAdapter.adptClaimsToClaimsResponse(claimsEntity), HttpStatus.OK);
    }



    @PatchMapping("/claims/attachment/validate/withoutnotify/{UUID_CLAIMS}")
    @ApiOperation(value = "Validate attachment without notify claims", notes = "Validate attachment without notify claims ID_CLAIMS using the Id Attachments passed into the body")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ClaimsResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ClaimsResponseV1> patchValidateAttachmentClaims(
            @PathVariable("UUID_CLAIMS") String idClaims,
            @RequestBody ListRequestV1<IdRequestV1> idAttachments
    ) {
        ClaimsEntity claimsEntity = claimsService.patchValidateAttachments(idClaims, idAttachments.getData());
        return new ResponseEntity<>(ClaimsAdapter.adptClaimsToClaimsResponse(claimsEntity), HttpStatus.OK);
    }


    //REFACTOR
    // GESTIONE FLUSSO PUBBLICO
    @PostMapping(value = "/claims/public",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = NemoAuthCompleteResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<NemoAuthCompleteResponseV1> getPublicFlow(@RequestBody ClaimsPublicRequestV1 claimsPublicRequestV1
    ) throws IOException {
        return new ResponseEntity<>(nemoAuthService.postGrant(claimsPublicRequestV1.getMetadata()), HttpStatus.OK);
    }


    //REFACTOR
    /*CHIAMATA SOAP*/
    @GetMapping("/claims/soap/{CLAIMS_UUID}")
    @ApiOperation(value = "Call to OCTO", notes = "Soap call to OCTO provides")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ClaimsResponseV1> addLog(@ApiParam(value = "CLAIMS_UUID", required = true)
                                                   @PathVariable(name = "CLAIMS_UUID") String claimsId,
                                                   @RequestHeader(name = "nemo-user-id") String userId,
                                                   @RequestHeader(name = "nemo-user-first-name") String userFirstName,
                                                   @RequestHeader(name = "nemo-user-last-name") String userLastName)  {

        lockService.checkLock(userId, claimsId);

        ClaimsNewEntity claimsNewEntity = claimsService.findByIdNewEntity(claimsId);

        if(claimsNewEntity.getAntiTheftServiceEntity() !=null) {
            //insert di storico chiamata
            String idAntiTheftRequest = UUID.randomUUID().toString();
            antiTheftRequestService.insertAntiTheftRequest(idAntiTheftRequest, claimsNewEntity, claimsNewEntity.getAntiTheftServiceEntity().getProviderType(), null);
            claimsService.callToOcto(claimsNewEntity, claimsNewEntity.getStatus(), new AntiTheftRequest(), userId, userFirstName + " " + userLastName, idAntiTheftRequest);

        }
        return new ResponseEntity<>(HttpStatus.OK);
    }


    //REFACTOR
    /*RECUPERO DEI LOG*/
    @GetMapping("/claims/log/{CLAIMS_UUID}")
    @ApiOperation(value = "Recover log", notes = "Recover Log for Claim")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<AntiTheftRequestResponseV1>> getLog(@ApiParam(value = "CLAIMS_UUID", required = true)
                                                                   @PathVariable(name = "CLAIMS_UUID") String claimsId) {

        List<AntiTheftRequestResponseV1> antiTheftRequestResponseV1List = AntiTheftRequestAdapter.adptAntiTheftRequestEntityListToAntiTheftRequestResponseV1List(AntiTheftRequestAdapter.adptAntiTheftRequestNewToAntiTheftRequestOldList(claimsService.getLogForClaim(claimsId)));
        return new ResponseEntity<>(antiTheftRequestResponseV1List, HttpStatus.OK);
    }

    @PostMapping("/claims/theft/cron/comunication")
    @ApiOperation(value = "Comunication theft", notes = "Comunication theft with cron")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })

    public void sendEmailTheftChangeStatus() throws IOException {
        claimsService.sendEmailTheftChangeStatus();
    }


    @PostMapping("/claims/cron/sendtoclient")
    @ApiOperation(value = "Comunication close total refund claims to client", notes = "Comunication claims with cron")
    @ApiResponses(value = {
                        @ApiResponse(code = 200, message = "Ok"),
                        @ApiResponse(code = 400, message = "Bad Request"),
                        @ApiResponse(code = 403, message = "Forbidden"),
                        @ApiResponse(code = 404, message = "Not Found"),
                        @ApiResponse(code = 406, message = "Not Acceptable"),
                        @ApiResponse(code = 500, message = "Internal Server Error")
                })

            public void sendToClient() throws IOException {
               claimsService.sendToClient();
    }



    @PostMapping("/claims/theft/cron/firstcomunication")
    @ApiOperation(value = "Comunication theft", notes = "Comunication theft with cron")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })

    public void sendEmailTheft() {
        claimsService.sendEmailTheft();
    }




    //CHIAMATE CON CRON

    //Eliminazione claims in bozza
    //REFACTOR
    @DeleteMapping("/claims/cron/draft")
    @ApiOperation(value = "Delete claims in draft", notes = "Delete claims in draft with cron")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public void deleteDraftClaims() {
        claimsService.deleteClaimsInBozzaRange(2);
    }

    //REFACTOR
    //Aggiornare claims evidence
    @PatchMapping("/claims/cron/evidence")
    @ApiOperation(value = "Update claims evidence", notes = "Update claims evidence with cron")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public void setClaimsEvidence() {

        claimsService.updateClaimsEvidence();

        cacheService.deleteClaimsStats(CACHENAME, STATSkEY);
        cacheService.deleteClaimsStats(CACHENAME, STATSEVIDENCE);
        cacheService.deleteClaimsStats(CACHENAME, STATSSTATUS);
        cacheService.deleteClaimsStats(CACHENAME, STATSTHEFT);
    }


    //REFACTOR
    //Affido automatico
    @PatchMapping("/claims/cron/entrust")
    @ApiOperation(value = "Generate automatic entrust", notes = "Generate automatic entrust with cron")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public void setAutomaticEntrust() throws IOException {
        claimsService.setAutomaticEntrust();
    }


    //REFACTOR
    //Stato del customer
    @GetMapping("/claims/customer/status/{UUID}")
    @ApiOperation(value = "Get status of claims' customer", notes = "Recover status of claims' customer")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<CustomerStatusResponse> getCustomerStatus(
            @ApiParam(value = "Id claims", required = true)
            @PathVariable(value = "UUID") String claimsId
    ) throws IOException {

        ClaimsNewEntity claimsEntity = claimsService.findByIdNewEntity(claimsId);

        ClaimsDamagedCustomerEntity customer = claimsEntity.getClaimsDamagedCustomerEntity();
        if (customer == null || customer.getCustomerId() == null) {
            LOGGER.debug(MessageCode.CLAIMS_1035.value());
            throw new BadRequestException(MessageCode.CLAIMS_1035);
        }

        CustomerEsb customerEsb = esbService.getCustomer(customer.getCustomerId().toString());
        if (customerEsb == null) {
            LOGGER.debug(MessageCode.CLAIMS_1034.value());
            throw new NotFoundException(MessageCode.CLAIMS_1034);
        }
        customer.setStatus(customerEsb.getStatus());

        claimsEntity.setClaimsDamagedCustomerEntity(customer);
        claimsService.saveClaimsNew(claimsEntity);
        CustomerStatusResponse customerStatusResponse = new CustomerStatusResponse();
        customerStatusResponse.setCustomerId(customer.getCustomerId().toString());
        customerStatusResponse.setStatus(customer.getStatus());
        return new ResponseEntity<>(customerStatusResponse, HttpStatus.OK);
    }


    //REFACTOR
    //toogle per l'associabilità delle pratiche di autorizzazione
    @PatchMapping("claims/{UUID}/linkable")
    @ApiOperation(value = "Patch linkable into claims", notes = "Patch linkable into claims")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ClaimsResponseV1> patchStatusLinkable(
            @PathVariable(value = "UUID") String claimsId
    ) {
        ClaimsEntity claimsEntity = claimsService.updateLinkable(claimsId);
        return new ResponseEntity<>(ClaimsAdapter.adptClaimsToClaimsResponse(claimsEntity), HttpStatus.OK);
    }

    //REFACTOR
    //Rotta per provare i controlli dell'inserimento ALD VS ALD
    @GetMapping("claims/prova/ald/{ID_CLAIM}")
    public ResponseEntity<ClaimsResponseV1> getAldVSAld(
            @PathVariable(value = "ID_CLAIM") String claimsId
    ) {
        ClaimsEntity claimsEntity = claimsService.findById(claimsId);
        claimsEntity = claimsService.checkALDvsALD(claimsEntity);

        return new ResponseEntity<>(ClaimsAdapter.adptClaimsToClaimsResponse(claimsEntity), HttpStatus.OK);
    }

    /**---------------------------------INIZIO GESTIONE SPLIT---------------------------------------*/

    //REFACTOR
    /*INSERIMENTO SPLIT RIMBORSO*/
    @PostMapping(value = "/claims/refund/split/{UUID_CLAIMS}",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Insert split into Claims", notes = "Upload claim usin ifno passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ClaimsResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ClaimsResponseV1> addSplitIntoClaimsRefund(
            @PathVariable(value = "UUID_CLAIMS") String claimsId,
            @ApiParam(value = "Body of the split to be insert", required = true)
            @RequestBody List<SplitRequest> splitList
    ) {

        return new ResponseEntity<>(ClaimsAdapter.adptClaimsToClaimsResponse(claimsService.addSplit(claimsId, splitList)), HttpStatus.OK);
    }


    //REFACTOR
    @PostMapping(value = "claims/{CLAIMS_UUID}/split",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Insert split into Claims", notes = "Upload claim usin ifno passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ClaimsResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ClaimsResponseV1> addSplitIntoClaims(
            @PathVariable(value = "CLAIMS_UUID") String claimsId,
            @ApiParam(value = "Body of the split to be insert", required = true)
            @RequestBody List<SplitRequest> splitList
    ) {

        return new ResponseEntity<>(ClaimsAdapter.adptClaimsToClaimsResponse(claimsService.addSplit(claimsId, splitList)), HttpStatus.OK);
    }


    //REFACTOR
    @DeleteMapping(value = "claims/{CLAIMS_UUID}/split/{SPLIT_UUID}",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Delete split into Claims", notes = "Upload claim usin ifno passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ClaimsResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ClaimsResponseV1> deleteplitIntoClaims(
            @PathVariable(value = "CLAIMS_UUID") String claimsId,
            @PathVariable(value = "SPLIT_UUID") String splitId
    ) {

        return new ResponseEntity<>(ClaimsAdapter.adptClaimsToClaimsResponse(claimsService.deleteSplit(claimsId, splitId)), HttpStatus.OK);
    }

    //REFACTOR
    @PutMapping(value = "claims/{CLAIMS_UUID}/split/{SPLIT_UUID}",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Update a single split in to Claims", notes = "Upload claim using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ClaimsResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ClaimsResponseV1> updateSplitIntoClaims(
            @PathVariable(value = "CLAIMS_UUID") String claimsId,
            @PathVariable(value = "SPLIT_UUID") String splitId,
            @ApiParam(value = "Updated body of the Split", required = true)
            @RequestBody SplitRequest splitUpdate
    ) {

        return new ResponseEntity<>(ClaimsAdapter.adptClaimsToClaimsResponse(claimsService.updateSplit(claimsId, splitId, splitUpdate)), HttpStatus.OK);
    }


    /**---------------------------------FINE GESTIONE SPLIT---------------------------------------*/

    //REFACTOR
    @PostMapping(value = "claims/counterparty/customer",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Check same customer", notes = "Check if claims and counterparty into ALD VS ALD have same customer")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = AlertCodeListResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<AlertCodeListResponseV1> checkSameCustomerAldVSAld(
            @ApiParam(value = "Body of the claim", required = true)
            @RequestBody ClaimsInsertRequestV1 claimsInsertRequestV1
    ) {
        return new ResponseEntity<>(claimsService.checkSameCustomerAldVSAld(claimsInsertRequestV1), HttpStatus.OK);
    }


    //REFACTOR
    /**---------------------------------MODIFICA ATTRIBUTI PRESENTI IN CONTRACT E DELLE POLIZZE---------------------------------------*/
    @PatchMapping(value = "claims/{CLAIMS_UUID}/contract",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Update single or multi-value on contract in to Claims", notes = "Update single or multi-value on contract in to Claims")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ContractPatchResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ContractPatchResponseV1> patchContractInfoInToClaims(
            @PathVariable(value = "CLAIMS_UUID") String claimsId,
            @ApiParam(value = "Updated body of the Contract", required = true)
            @RequestBody DamagedRequest damagedRequest,
            @RequestHeader(name = "nemo-user-id") String userId,
            @RequestHeader(name = "nemo-user-first-name") String userName,
            @RequestHeader(name = "nemo-user-last-name") String lastName
    ) throws IOException {
        Command command = new ClaimsPatchContractInformationCommand(
                claimsId,
                damagedRequest,
                userId,
                userName + " "+ lastName
        );

        commandBus.execute(command);

        ClaimsEntity claimsEntity = claimsService.findById(claimsId);

        return new ResponseEntity(ClaimsAdapter.adptClaimsToClaimsResponse(claimsEntity), HttpStatus.OK);
    }

    /**-------------------------------------------------------------------------------------------------------*/



    //Conversione dei sinistri dal vecchio al nuovo database
    //Refactor
    @GetMapping(value = "/claims/convert")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity convertClaims(@RequestParam(name = "range", defaultValue = "10000") Integer range) {
        LOGGER.debug("convert route");

        converterClaimsService.convertClaims(range);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Autowired
    WebSinServiceManager webSinServiceManager;

    @GetMapping(value = "/websin")
    public ResponseEntity<Object> testWebsin() throws MalformedURLException {
        ClaimsEntity claimsEntity = claimsRepository.getClaimsByIdPratica(new Long(4000000));
//        webSinServiceManager.putComplaint(claimsEntity);
        return new ResponseEntity<>(null,HttpStatus.OK);
    }

    @Autowired
    ExternalCommunicationService externalCommunicationService;

    @GetMapping(value = "/recover")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = HistoricalResponseV1.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<Object> testExternalCall(){
        //Chiama metodo
        externalCommunicationService.retryCallMilesWebSin();
        return new ResponseEntity<>("Batch lanciato con successo", HttpStatus.OK);

    }

}




