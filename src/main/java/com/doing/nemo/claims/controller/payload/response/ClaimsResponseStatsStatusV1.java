package com.doing.nemo.claims.controller.payload.response;

import com.doing.nemo.claims.controller.payload.response.ClaimsResponseStatsV1.StatsElementResponse;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ClaimsResponseStatsStatusV1 implements Serializable {

    @JsonProperty("stats")
    private List<StatsElementResponse> stats;

    public List<StatsElementResponse> getStats() {
        if(stats == null){
            return null;
        }
        return new ArrayList<>(stats);
    }

    public void setStats(List<StatsElementResponse> stats) {
        if(stats != null)
        {
            this.stats = new ArrayList<>(stats);
        } else {
            this.stats = null;
        }
    }

    @Override
    public String toString() {
        return "ClaimsResponseStatsV1{" +
                "stats=" + stats +
                '}';
    }


}
