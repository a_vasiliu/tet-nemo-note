package com.doing.nemo.claims.controller.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SequelWithNotifyMessagingRequestV1 implements Serializable {

    @JsonProperty("data")
    private List<IdRequestV1> data;

    @JsonProperty("email_template_list")
    private List<EmailTemplateMessagingRequestV1> emailTemplateMessagingList;


    public List<IdRequestV1> getData() {
        if(data == null){
            return null;
        }
        return new ArrayList<>(data);
    }

    public void setData(List<IdRequestV1> data) {
        if(data != null)
        {
            this.data = new ArrayList<>(data);
        } else {
            this.data = null;
        }
    }

    public List<EmailTemplateMessagingRequestV1> getEmailTemplateMessagingList() {
        if(emailTemplateMessagingList == null){
            return null;
        }
        return new ArrayList<>(emailTemplateMessagingList);
    }

    public void setEmailTemplateMessagingList(List<EmailTemplateMessagingRequestV1> emailTemplateMessagingList) {
        if(emailTemplateMessagingList != null)
        {
            this.emailTemplateMessagingList = new ArrayList<>(emailTemplateMessagingList);
        } else {
            this.emailTemplateMessagingList = null;
        }
    }

    @Override
    public String toString() {
        return "SequelWithNotifyMessagingRequestV1{" +
                "data=" + data +
                ", emailTemplateMessagingList=" + emailTemplateMessagingList +
                '}';
    }
}
