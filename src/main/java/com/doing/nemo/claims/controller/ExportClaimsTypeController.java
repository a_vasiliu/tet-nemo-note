package com.doing.nemo.claims.controller;

import com.doing.nemo.claims.adapter.ExportClaimsTypeAdapter;
import com.doing.nemo.claims.controller.payload.request.ExportClaimsTypeRequestV1;
import com.doing.nemo.claims.controller.payload.request.ListRequestV1;
import com.doing.nemo.claims.controller.payload.response.ClaimsResponsePaginationV1;
import com.doing.nemo.claims.controller.payload.response.IdResponseV1;
import com.doing.nemo.claims.controller.payload.response.ExportClaimsTypeResponseV1;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.settings.ExportClaimsTypeEntity;
import com.doing.nemo.claims.service.ExportClaimsTypeService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/v1")
@Api("Export Claims TypeEnum")
public class ExportClaimsTypeController {

    @Autowired
    private ExportClaimsTypeService exportClaimsTypeService;

    @Autowired
    private ExportClaimsTypeAdapter exportClaimsTypeAdapter;

    @PostMapping("/export/claimstype")
    @Transactional
    @ApiOperation(value = "Insert Export Claims TypeEnum", notes = "Insert Export Claims TypeEnum using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = ExportClaimsTypeResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<IdResponseV1> saveExportClaimsType(
            @RequestBody ExportClaimsTypeRequestV1 exportClaimsTypeRequestV1
            )
    {
        ExportClaimsTypeEntity exportClaimsTypeEntity = ExportClaimsTypeAdapter.adptFromExportClaimsTypeRequestToExportClaimsTypeEntity(exportClaimsTypeRequestV1);
        exportClaimsTypeEntity = exportClaimsTypeService.saveExportClaimsType(exportClaimsTypeEntity);
        return new ResponseEntity<>(new IdResponseV1(exportClaimsTypeEntity.getId()), HttpStatus.CREATED);
    }

    @PostMapping("/export/claimstype/all")
    @Transactional
    @ApiOperation(value = "Insert a list of Export Claims TypeEnum", notes = "Insert a list of insurance companies using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = ExportClaimsTypeResponseV1.class, responseContainer="List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<ExportClaimsTypeResponseV1>> insertExportClaimsTypeAll(
            @ApiParam(value = "List of the bodies of the Export Claims TypeEnum to be created", required = true)
            @RequestBody ListRequestV1<ExportClaimsTypeRequestV1> insuranceCompaniesRequest){

        List<ExportClaimsTypeResponseV1> exportClaimsTypeResponseV1List = new ArrayList<>();
        for (ExportClaimsTypeRequestV1 exportClaimsTypeRequestV1 : insuranceCompaniesRequest.getData())
        {

            ExportClaimsTypeEntity exportClaimsTypeEntity = ExportClaimsTypeAdapter.adptFromExportClaimsTypeRequestToExportClaimsTypeEntity(exportClaimsTypeRequestV1);
            exportClaimsTypeEntity = exportClaimsTypeService.saveExportClaimsType(exportClaimsTypeEntity);
            ExportClaimsTypeResponseV1 responseV1 = ExportClaimsTypeAdapter.adptFromExportClaimsTypeEntityToExportClaimsTypeResponse(exportClaimsTypeEntity);
            exportClaimsTypeResponseV1List.add(responseV1);
        }
        return new ResponseEntity<>(exportClaimsTypeResponseV1List, HttpStatus.CREATED);

    }

    @GetMapping("/export/claimstype/{UUID}")
    @Transactional
    @ApiOperation(value = "Recover Export Claims TypeEnum", notes = "Returns a Export Claims TypeEnum using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ExportClaimsTypeResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ExportClaimsTypeResponseV1> getExportClaimsType(
            @PathVariable("UUID") UUID uuid
    ){
        ExportClaimsTypeEntity exportClaimsTypeEntity = exportClaimsTypeService.getExportClaimsType(uuid);
        return new ResponseEntity<>(ExportClaimsTypeAdapter.adptFromExportClaimsTypeEntityToExportClaimsTypeResponse(exportClaimsTypeEntity), HttpStatus.OK);
    }

    @GetMapping("/export/claimstype")
    @Transactional
    @ApiOperation(value = "Recover Export Claims TypeEnum", notes = "Returns a Export Claims TypeEnum using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ExportClaimsTypeResponseV1.class, responseContainer="List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<ExportClaimsTypeResponseV1>> getExportClaimsType(
    ){
        List<ExportClaimsTypeEntity> exportClaimsTypeEntityList = exportClaimsTypeService.getAllExportClaimsType();
        return new ResponseEntity<>(ExportClaimsTypeAdapter.adptFromExportClaimsTypeEntityToExportClaimsTypeResponseList(exportClaimsTypeEntityList), HttpStatus.OK);
    }

    @PutMapping("/export/claimstype/{UUID}")
    @Transactional
    @ApiOperation(value = "Insert Export Claims TypeEnum", notes = "Insert Export Claims TypeEnum using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = ExportClaimsTypeResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ExportClaimsTypeResponseV1> updateExportClaimsType(
            @RequestBody ExportClaimsTypeRequestV1 exportClaimsTypeRequestV1,
            @PathVariable("UUID") UUID uuid
    )
    {
        ExportClaimsTypeEntity exportClaimsTypeEntity = exportClaimsTypeAdapter.adptFromExportClaimsTypeRequestToExportClaimsTypeEntityWithID(exportClaimsTypeRequestV1, uuid);
        exportClaimsTypeEntity = exportClaimsTypeService.updateExportClaimsType(exportClaimsTypeEntity);
        return new ResponseEntity<>(ExportClaimsTypeAdapter.adptFromExportClaimsTypeEntityToExportClaimsTypeResponse(exportClaimsTypeEntity), HttpStatus.OK);
    }

    @DeleteMapping("/export/claimstype/{UUID}")
    @Transactional
    @ApiOperation(value = "Delete  Export Claims TypeEnum", notes = "Delete Export Claims TypeEnum using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity deleteExportClaimsType(
            @PathVariable("UUID") UUID uuid
    ){
        exportClaimsTypeService.deleteExportClaimsType(uuid);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PatchMapping("/export/claimstype/{UUID}/active")
    @Transactional
    @ApiOperation(value = "Patch status active Export Claims TypeEnum", notes = "Patch status active Export Claims TypeEnum using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ExportClaimsTypeResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ExportClaimsTypeResponseV1> patchStatusExportClaimsType(
            @PathVariable UUID UUID
    ) {
        ExportClaimsTypeEntity exportClaimsTypeEntity = exportClaimsTypeService.patchActive(UUID);
        ExportClaimsTypeResponseV1 responseV1 = ExportClaimsTypeAdapter.adptFromExportClaimsTypeEntityToExportClaimsTypeResponse(exportClaimsTypeEntity);
        return new ResponseEntity<>(responseV1, HttpStatus.OK);
    }

    @ApiOperation(value="Export Claims TypeEnum Pagination", notes = "It retrieves Export Claims TypeEnum pagination")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = ClaimsResponsePaginationV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @GetMapping(value="/claimstype/pagination", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<ClaimsResponsePaginationV1> claimsTypePagination(
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @RequestParam(value = "page_size", defaultValue = "20") Integer pageSize,
            @RequestParam(value = "order_by", required = false) String orderBy,
            @RequestParam(value = "asc", required = false, defaultValue = "true") Boolean asc,
            @RequestParam(value = "claims_type", required = false) DataAccidentTypeAccidentEnum claimsType,
            @RequestParam(value = "code", required = false) String code,
            @RequestParam(value = "description", required = false) String description,
            @RequestParam(value = "is_active", required = false) Boolean isActive
    ){

        Pagination<ExportClaimsTypeEntity> exportClaimsTypeEntityPagination = exportClaimsTypeService.findExportClaimsType(page, pageSize, orderBy, asc, claimsType, code, description, isActive);

        return new ResponseEntity<>(exportClaimsTypeAdapter.adptFromExportClaimsTypePaginationToClaimsResponsePagination(exportClaimsTypeEntityPagination), HttpStatus.OK);
    }

}
