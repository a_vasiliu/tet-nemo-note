package com.doing.nemo.claims.controller;

import com.doing.nemo.claims.adapter.*;
import com.doing.nemo.claims.command.*;
import com.doing.nemo.claims.common.commandBus.Command;
import com.doing.nemo.claims.common.commandBus.CommandBus;
import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.*;
import com.doing.nemo.claims.controller.payload.request.claims.DamagedRequest;
import com.doing.nemo.claims.controller.payload.request.claims.NoteRequest;
import com.doing.nemo.claims.controller.payload.request.refund.SplitRequest;
import com.doing.nemo.claims.controller.payload.response.*;
import com.doing.nemo.claims.controller.payload.response.claims.NoteResponse;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.CounterpartyEntity;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsUpdateProcedureEnum;
import com.doing.nemo.claims.entity.enumerated.DTO.ClaimsTheftStats;
import com.doing.nemo.claims.entity.esb.CustomerESB.CustomerEsb;
import com.doing.nemo.claims.entity.jsonb.Notes;
import com.doing.nemo.claims.entity.jsonb.damaged.Damaged;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.Customer;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbAntiTheftService.AntiTheftRequest;
import com.doing.nemo.claims.entity.jsonb.forms.jsonbForms.Attachment;
import com.doing.nemo.claims.repository.ClaimsRepository;
import com.doing.nemo.claims.repository.ClaimsRepositoryV1;
import com.doing.nemo.claims.service.*;
import com.doing.nemo.claims.util.Util;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.exception.NotFoundException;
import io.swagger.annotations.*;
import org.postgresql.util.PSQLException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/v1")
@Api("Claims Migration")
public class ClaimsMigrationController {

    private static Logger LOGGER = LoggerFactory.getLogger(ClaimsMigrationController.class);

    @Autowired
    private ClaimsMigrationService claimsMigrationService;

    @Bean
    public static RestTemplate rest() {
        return new RestTemplate();
    }


    /*-------------------------------------CLAIMS----------------------------------------------*/
    //REFACTOR
    /*INSERIMENTO CLAIMS*/
    @PostMapping(value = "/claims/migration",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Insert Claims", notes = "Insert claim using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = ClaimsResponseIdV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ClaimsResponseIdV1> postInsertClaimsAdmin(
            @ApiParam(value = "Body of the Claims to be created", required = true)
            @RequestBody List<ClaimsEntity> listClaimsEntity
    ) throws IOException {



        claimsMigrationService.importClaimsFromMigration(listClaimsEntity);

        return new ResponseEntity<>( HttpStatus.OK);
    }


    /*-------------------------------------COUNTERPARTY----------------------------------------------*/
    //REFACTOR
    /*INSERIMENTO COUNTERPARTY*/
    @PostMapping(value = "/counterparty/migration",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Insert Counterparty", notes = "Insert counterparty using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = ClaimsResponseIdV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ClaimsResponseIdV1> postInsertCounterpartyAdmin(
            @ApiParam(value = "Body of the Counterparty to be created", required = true)
            @RequestBody List<CounterpartyMigrationRequestV1> counterpartyMigrationRequestV1List
    ) throws IOException {


        claimsMigrationService.importCounterpartyFromMigration(counterpartyMigrationRequestV1List);

        return new ResponseEntity<>(HttpStatus.OK);
    }


}
