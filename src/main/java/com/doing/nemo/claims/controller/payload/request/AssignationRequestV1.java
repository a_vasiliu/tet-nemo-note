package com.doing.nemo.claims.controller.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class AssignationRequestV1 implements Serializable {
    @JsonProperty("claims_id")
    private String claimsId;

    @JsonProperty("counterparty_id")
    private String counterpartyId;

    @JsonProperty("assignee")
    private AssigneeRepairRequestV1 assignee;

    public AssignationRequestV1(String claimsId, String counterpartyId, AssigneeRepairRequestV1 assignee) {
        this.claimsId = claimsId;
        this.counterpartyId = counterpartyId;
        this.assignee = assignee;
    }

    public AssignationRequestV1() {
    }

    public String getClaimsId() {
        return claimsId;
    }

    public void setClaimsId(String claimsId) {
        this.claimsId = claimsId;
    }

    public String getCounterpartyId() {
        return counterpartyId;
    }

    public void setCounterpartyId(String counterpartyId) {
        this.counterpartyId = counterpartyId;
    }

    public AssigneeRepairRequestV1 getAssignee() {
        return assignee;
    }

    public void setAssignee(AssigneeRepairRequestV1 assignee) {
        this.assignee = assignee;
    }

    @Override
    public String toString() {
        return "AssignationRequestV1{" +
                "claimsId='" + claimsId + '\'' +
                ", counterpartyId='" + counterpartyId + '\'' +
                ", assignee=" + assignee +
                '}';
    }
}
