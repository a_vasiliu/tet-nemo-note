package com.doing.nemo.claims.controller.payload.request;

import com.doing.nemo.claims.entity.enumerated.RiskEnum;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class RiskRequestV1 implements Serializable {


    @JsonProperty("risk_id")
    private Long riskId;

    @JsonProperty("type")
    private RiskEnum type;

    @JsonProperty("description")
    private String description;

    @JsonProperty("maximal")
    private Double maximal;

    @JsonProperty("exemption")
    private Double exemption;

    @JsonProperty("overdraft")
    private Double overdraft;

    @JsonProperty("active")
    private Boolean active;

    public RiskRequestV1(Long riskId, RiskEnum type, String description, Double maximal, Double exemption, Double overdraft, Boolean active) {
        this.riskId = riskId;
        this.type = type;
        this.description = description;
        this.maximal = maximal;
        this.exemption = exemption;
        this.overdraft = overdraft;
        this.active = active;
    }

    public RiskRequestV1() {
    }

    public RiskEnum getType() {
        return type;
    }

    public void setType(RiskEnum type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getMaximal() {
        return maximal;
    }

    public void setMaximal(Double maximal) {
        this.maximal = maximal;
    }

    public Double getExemption() {
        return exemption;
    }

    public void setExemption(Double exemption) {
        this.exemption = exemption;
    }

    public Double getOverdraft() {
        return overdraft;
    }

    public void setOverdraft(Double overdraft) {
        this.overdraft = overdraft;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }


    public Long getRiskId() {
        return riskId;
    }

    public void setRiskId(Long riskId) {
        this.riskId = riskId;
    }

    @Override
    public String toString() {
        return "RiskRequestV1{" +
                "riskId=" + riskId +
                ", type=" + type +
                ", description='" + description + '\'' +
                ", maximal=" + maximal +
                ", exemption=" + exemption +
                ", overdraft=" + overdraft +
                ", active=" + active +
                '}';
    }
}
