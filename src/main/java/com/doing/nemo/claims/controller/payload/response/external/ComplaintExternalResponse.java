package com.doing.nemo.claims.controller.payload.response.external;

import com.doing.nemo.claims.controller.payload.response.complaint.DataAccidentResponse;
import com.doing.nemo.claims.controller.payload.response.complaint.EntrustedResponse;
import com.doing.nemo.claims.controller.payload.response.complaint.FromCompanyResponse;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.ComplaintModEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.ComplaintPropertyEnum;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class ComplaintExternalResponse implements Serializable {
    @JsonProperty("plate")
    private String plate;

    @JsonProperty("locator")
    private String locator;

    @JsonProperty("activation")
    private String activation;

    @JsonProperty("property")
    private ComplaintPropertyEnum property;

    @JsonProperty("mod")
    private ComplaintModEnum mod;

    @JsonProperty("notification")
    private String notification;

    @JsonProperty("quote")
    private Boolean quote;

    @JsonProperty("formatted_address")
    private String formattedAddress;

    @JsonProperty("data_accident")
    private DataAccidentResponse dataAccident;

    @JsonProperty("from_company")
    private FromCompanyResponse fromCompany;

    @JsonProperty("repair")
    private RepairExternalResponse repair;

    @JsonProperty("entrusted")
    private EntrustedResponse entrusted;

    public ComplaintExternalResponse() {
    }

    public ComplaintExternalResponse(String plate, String locator, String activation, ComplaintPropertyEnum property, ComplaintModEnum mod, String notification, Boolean quote, String formattedAddress, DataAccidentResponse dataAccident, FromCompanyResponse fromCompany, RepairExternalResponse repair, EntrustedResponse entrusted) {
        this.plate = plate;
        this.locator = locator;
        this.activation = activation;
        this.property = property;
        this.mod = mod;
        this.notification = notification;
        this.quote = quote;
        this.formattedAddress = formattedAddress;
        this.dataAccident = dataAccident;
        this.fromCompany = fromCompany;
        this.repair = repair;
        this.entrusted = entrusted;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public String getLocator() {
        return locator;
    }

    public void setLocator(String locator) {
        this.locator = locator;
    }

    public String getActivation() {
        return activation;
    }

    public void setActivation(String activation) {
        this.activation = activation;
    }

    public ComplaintPropertyEnum getProperty() {
        return property;
    }

    public void setProperty(ComplaintPropertyEnum property) {
        this.property = property;
    }

    public ComplaintModEnum getMod() {
        return mod;
    }

    public void setMod(ComplaintModEnum mod) {
        this.mod = mod;
    }

    public String getNotification() {
        return notification;
    }

    public void setNotification(String notification) {
        this.notification = notification;
    }

    public Boolean getQuote() {
        return quote;
    }

    public void setQuote(Boolean quote) {
        this.quote = quote;
    }

    public String getFormattedAddress() {
        return formattedAddress;
    }

    public void setFormattedAddress(String formattedAddress) {
        this.formattedAddress = formattedAddress;
    }

    public DataAccidentResponse getDataAccident() {
        return dataAccident;
    }

    public void setDataAccident(DataAccidentResponse dataAccident) {
        this.dataAccident = dataAccident;
    }

    public FromCompanyResponse getFromCompany() {
        return fromCompany;
    }

    public void setFromCompany(FromCompanyResponse fromCompany) {
        this.fromCompany = fromCompany;
    }

    public RepairExternalResponse getRepair() {
        return repair;
    }

    public void setRepair(RepairExternalResponse repair) {
        this.repair = repair;
    }

    public EntrustedResponse getEntrusted() {
        return entrusted;
    }

    public void setEntrusted(EntrustedResponse entrusted) {
        this.entrusted = entrusted;
    }

    @Override
    public String toString() {
        return "ComplaintExternalResponse{" +
                "plate='" + plate + '\'' +
                ", locator='" + locator + '\'' +
                ", activation='" + activation + '\'' +
                ", property=" + property +
                ", mod=" + mod +
                ", notification=" + notification +
                ", quote=" + quote +
                ", formattedAddress='" + formattedAddress + '\'' +
                ", dataAccident=" + dataAccident +
                ", fromCompany=" + fromCompany +
                ", repair=" + repair +
                ", entrusted=" + entrusted +
                '}';
    }




}
