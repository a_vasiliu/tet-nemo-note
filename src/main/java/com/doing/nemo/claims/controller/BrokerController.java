package com.doing.nemo.claims.controller;

import com.doing.nemo.claims.adapter.BrokerAdapter;
import com.doing.nemo.claims.common.validation.MessageCode;
import com.doing.nemo.claims.common.validation.RequestValidator;
import com.doing.nemo.claims.controller.payload.request.BrokerRequestV1;
import com.doing.nemo.claims.controller.payload.request.ListRequestV1;
import com.doing.nemo.claims.controller.payload.response.IdResponseV1;
import com.doing.nemo.claims.controller.payload.response.BrokerResponseV1;
import com.doing.nemo.claims.controller.payload.response.PaginationResponseV1;
import com.doing.nemo.claims.entity.settings.BrokerEntity;
import com.doing.nemo.claims.service.BrokerService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/v1")
@Api("Broker")
public class BrokerController {

    @Autowired
    private BrokerService brokerService;

    @Autowired
    private RequestValidator requestValidator;


    @PostMapping("/broker")
    @Transactional
    @ApiOperation(value = "Insert Broker", notes = "Insert Broker using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = IdResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<IdResponseV1> postBroker(@ApiParam(value = "Body of the Broker to be created", required = true) @RequestBody BrokerRequestV1 brokerRequestV1) {
        requestValidator.validateRequest(brokerRequestV1, MessageCode.E00X_1000);
        BrokerEntity brokerEntity = BrokerAdapter.adptFromBrokerRequestToBrokerEntity(brokerRequestV1);
        brokerEntity = brokerService.insertBroker(brokerEntity);
        IdResponseV1 brokerResponseIdV1 = new IdResponseV1(brokerEntity.getId());
        return new ResponseEntity(brokerResponseIdV1, HttpStatus.CREATED);
    }

    @PostMapping("/brokers")
    @Transactional
    @ApiOperation(value = "Insert More Brokers", notes = "Insert Brokers using info passed in the bodies")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = BrokerResponseV1.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<BrokerResponseV1>> postBrokers(@ApiParam(value = "Bodies of the Brokers to be created", required = true) @RequestBody ListRequestV1<BrokerRequestV1> brokerListRequestV1) {

        List<BrokerResponseV1> brokerResponseV1List = new ArrayList<>();
        for (BrokerRequestV1 brokerRequestV1 : brokerListRequestV1.getData()) {
            requestValidator.validateRequest(brokerRequestV1, MessageCode.E00X_1000);
            BrokerEntity brokerEntity = BrokerAdapter.adptFromBrokerRequestToBrokerEntity(brokerRequestV1);
            brokerEntity = brokerService.insertBroker(brokerEntity);
            BrokerResponseV1 brokerResponseV1 = BrokerAdapter.adptFromBrokerEntityToBrokerResponseV1(brokerEntity);
            brokerResponseV1List.add(brokerResponseV1);
        }
        return new ResponseEntity(brokerResponseV1List, HttpStatus.CREATED);
    }

    @PutMapping("/broker/{UUID}")
    @Transactional
    @ApiOperation(value = "Upload Broker", notes = "Upload Broker using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = BrokerResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<BrokerResponseV1> putBroker(@ApiParam(value = "UUID that identifies the Broker to be modified", required = true) @PathVariable(name = "UUID") UUID id, @ApiParam(value = "Updated body of the Broker", required = true)
    @RequestBody BrokerRequestV1 brokerRequestV1) {
        requestValidator.validateRequest(brokerRequestV1, MessageCode.E00X_1000);
        BrokerEntity brokerEntity = brokerService.selectBroker(id);
        BrokerEntity brokerEntityNew = BrokerAdapter.adptFromBrokerRequestToBrokerEntity(brokerRequestV1);
        brokerEntityNew.setId(brokerEntity.getId());
        brokerEntityNew = brokerService.updateBroker(brokerEntityNew);
        BrokerResponseV1<BrokerEntity> brokerResponseV1 = BrokerAdapter.adptFromBrokerEntityToBrokerResponseV1(brokerEntityNew);
        return new ResponseEntity<>(brokerResponseV1, HttpStatus.OK);
    }

    @GetMapping("/broker/{UUID}")
    @Transactional
    @ApiOperation(value = "Recover Broker", notes = "Return a Broker using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = BrokerResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<BrokerResponseV1> getBroker(@ApiParam(value = "UUID of the Broker to be found", required = true) @PathVariable(name = "UUID") UUID id) {
        BrokerEntity brokerEntity = brokerService.selectBroker(id);
        BrokerResponseV1 brokerResponseV1 = BrokerAdapter.adptFromBrokerEntityToBrokerResponseV1(brokerEntity);
        return new ResponseEntity<>(brokerResponseV1, HttpStatus.OK);
    }

    @GetMapping("/broker")
    @Transactional
    @ApiOperation(value = "Recover All Brokers", notes = "Returns all Brokers")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = BrokerResponseV1.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<BrokerResponseV1>> getAllBroker() {
        List<BrokerEntity> brokerEntityList = brokerService.selectAllBroker();
        List<BrokerResponseV1<BrokerEntity>> brokerResponseV1List = BrokerAdapter.adptFromBrokerEntityToBrokerResponseV1List(brokerEntityList);
        return new ResponseEntity(brokerResponseV1List, HttpStatus.OK);
    }

    @DeleteMapping("/broker/{UUID}")
    @Transactional
    @ApiOperation(value = "Delete Broker", notes = "Delete a Broker using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = BrokerResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<BrokerResponseV1> deleteBroker(@ApiParam(value = "UUID of the Broker to be deleted", required = true) @PathVariable(name = "UUID") UUID id) {
        BrokerResponseV1<BrokerEntity> brokerResponseV1 = brokerService.deleteBroker(id);
        return new ResponseEntity<>(brokerResponseV1, HttpStatus.OK);
    }

    @PatchMapping("/broker/{UUID}/active")
    @Transactional
    @ApiOperation(value = "Patch active status Broker", notes = "Patch active status of Broker using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = BrokerResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<BrokerResponseV1> patchStatusBroker(
            @PathVariable UUID UUID
    ) {
        BrokerEntity brokerEntity = brokerService.updateStatus(UUID);
        BrokerResponseV1<BrokerEntity> responseV1 = BrokerAdapter.adptFromBrokerEntityToBrokerResponseV1(brokerEntity);
        return new ResponseEntity<>(responseV1, HttpStatus.OK);
    }


    @ApiOperation(value="Search Broker", notes = "It retrieves broker data through one of more parameters")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = PaginationResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @GetMapping(value="/broker/pagination", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<PaginationResponseV1<BrokerResponseV1>> searchBroker(
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @RequestParam(value = "page_size", defaultValue = "20") Integer pageSize
    ){

        List<BrokerEntity> listBroker = brokerService.findBrokersFiltered(page,pageSize);
        BigInteger itemCount = brokerService.countBrokerFiltered();

        PaginationResponseV1<BrokerResponseV1> paginationResponseV1 = BrokerAdapter.adptPagination(listBroker, page, pageSize, itemCount);

        return new ResponseEntity<>(paginationResponseV1, HttpStatus.OK);
    }

}
