package com.doing.nemo.claims.controller.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

public class UploadFileRequestV1 implements Serializable {

    private static final long serialVersionUID = -3037818086294484134L;

    @NotNull
    @JsonProperty("file_content")
    @ApiModelProperty(value = "Base64 encoded file content", example = "QmFzZTY0IGVuY29kZWQgZmlsZSBjb250ZW50")
    private String fileContent;

    @NotNull
    @JsonProperty("file_name")
    @ApiModelProperty(value = "Name of the file to upload", example = "supermario.png")
    private String fileName;

    @JsonProperty("description")
    @ApiModelProperty(value = "Description", example = "Whooa there's a description field, too!")
    private String description;

    @Size(min = 36)
    @JsonProperty("uuid")
    @ApiModelProperty(value = "UUID of the file", example = "897g34s3-0ca7-4226-b10b-c5db10003213")
    private String uuid;

    @JsonProperty("blob_type")
    @ApiModelProperty(value = "Blob type", example = "adc")
    private String blobType;

    @JsonProperty("resource_type")
    @ApiModelProperty(value = "TypeEnum of the resource to upload, tipically 'orders' or 'alerts'", example = "orders")
    private String resourceType;

    @JsonProperty("resource_id")
    @ApiModelProperty(value = "ID of the resource", example = "333")
    private String resourceId;

    @JsonProperty("attachment_type")
    private AttachmentTypeRequestV1 attachmentType;

    @JsonProperty("external_id")
    private String externalId;

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getFileContent() {
        return fileContent;
    }

    public void setFileContent(String fileContent) {
        this.fileContent = fileContent;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getResourceId() {
        return resourceId;
    }

    public void setResourceId(String resourceId) {
        this.resourceId = resourceId;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getBlobType() {
        return blobType;
    }

    public void setBlobType(String blobType) {
        this.blobType = blobType;
    }

    public AttachmentTypeRequestV1 getAttachmentType() {
        return attachmentType;
    }

    public void setAttachmentType(AttachmentTypeRequestV1 attachmentType) {
        this.attachmentType = attachmentType;
    }

    @Override
    public String toString() {
        return "UploadFileRequestV1{" +
                "fileContent='" + fileContent + '\'' +
                ", fileName='" + fileName + '\'' +
                ", description='" + description + '\'' +
                ", uuid='" + uuid + '\'' +
                ", blobType='" + blobType + '\'' +
                ", resourceType='" + resourceType + '\'' +
                ", resourceId='" + resourceId + '\'' +
                ", attachmentType=" + attachmentType +
                '}';
    }
}
