package com.doing.nemo.claims.controller;


import com.doing.nemo.claims.adapter.ClaimsAdapter;
import com.doing.nemo.claims.adapter.CounterpartyAdapter;
import com.doing.nemo.claims.command.ClaimsPatchReadExternalCommand;
import com.doing.nemo.claims.command.ClaimsPatchReadRepairExternalCommand;
import com.doing.nemo.claims.common.commandBus.Command;
import com.doing.nemo.claims.common.commandBus.CommandBus;
import com.doing.nemo.claims.controller.payload.request.IdRequestV1;
import com.doing.nemo.claims.controller.payload.request.ListRequestV1;
import com.doing.nemo.claims.controller.payload.response.*;
import com.doing.nemo.claims.controller.payload.response.external.CounterpartyExternalResponse;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.CounterpartyEntity;
import com.doing.nemo.claims.entity.CounterpartyNewEntity;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.repository.CounterpartyRepository;
import com.doing.nemo.claims.service.AuthorityAttachmentService;
import com.doing.nemo.claims.service.ClaimsService;
import com.doing.nemo.claims.service.CounterpartyService;
import com.doing.nemo.commons.exception.NotFoundException;
import io.swagger.annotations.*;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@RestController
@CrossOrigin
@RequestMapping(value = "/api/v1")
@Api("External Manager")
public class ExternalRepairManagerController {

    private static Logger LOGGER = LoggerFactory.getLogger(ExternalRepairManagerController.class);

    @Autowired
    private CommandBus commandBus;

    @Autowired
    private CounterpartyService counterpartyService;

    @Autowired
    private ClaimsService claimsService;

    @Autowired
    private CounterpartyAdapter counterpartyAdapter;

    @Autowired
    private AuthorityAttachmentService authorityAttachmentService;

    //GESTORE ESTERNO
    /*PAGINAZIONE CONTROPARTI*/
    //recupera le pratiche repair
    //REFACTOR
    @GetMapping(value = "/repair/external",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = CounterpartyResponsePaginationV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<CounterpartyResponsePaginationV1> getRepairPaginationAdminSupervisor(
            @ApiParam(value = "Defines how many Counterparty can contain the single page", required = false)
            @RequestParam(value = "page_size", defaultValue = "20", required = false) Integer pageSize,
            @ApiParam(value = "Defines the page number to be displayed", required = false)
            @RequestParam(value = "page", defaultValue = "1", required = false) Integer page
    ) {
        //VERIFICARE CHE VENGANO RECUPERATE LE PRATICHE REPAIR
        //REFACTOR
        List<CounterpartyResponseV1> counterpartyResponseV1s = counterpartyService.getCounterpartyMsaPaginationFromDB(null, null, null, null, null, null, null, null, page, pageSize, null, null, null, null, null, null, null, null, null, null,null, true);

        //REFACTOR
        BigInteger count = counterpartyService.countCounterpartyPagination(null, null, null, null, null, null, null, null, page, pageSize, null, null, null, null, null, null, null, null, null, null, null,true);

        Pagination<CounterpartyResponseV1> pagination = counterpartyService.generateCounterpartyPagination(counterpartyResponseV1s, count, page, pageSize);
        return new ResponseEntity<>(CounterpartyAdapter.adptCounterpartyPaginationToCounterpartyPaginationResponse(pagination), HttpStatus.OK);


    }

    //REFACTOR
    //Settare la lettura della pratica repair da parte del gestore esterno
    @PatchMapping(value = "/repair/external")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = CounterpartyExternalResponse.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<CounterpartyExternalResponse>> patchRepairAsRead(
            @ApiParam(value = "List of id claims to set as read", required = true)
            @RequestBody ListRequestV1<IdRequestV1> repairIdListRequest,
            @RequestHeader(value = "nemo-profiles", required = true) List<String> nemoProfilesList

    ) throws IOException {
        Command claimsPatchReadRepairExternalCommand = new ClaimsPatchReadRepairExternalCommand(repairIdListRequest, nemoProfilesList);
        commandBus.execute(claimsPatchReadRepairExternalCommand);

        List<CounterpartyExternalResponse> counterpartyExternalResponses = counterpartyService.getCounterpartySetMsaReadResponse(repairIdListRequest.getData());
        return new ResponseEntity<>(counterpartyExternalResponses, HttpStatus.OK);
    }


    //REFACTOR
    //Get by id della singola pratica repair per poter sempre leggere i dettagli
    @GetMapping(value = "/repair/external/{uuid_counterparty}",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = CounterpartyResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<CounterpartyResponseV1> getRepairPaginationAdminSupervisor(
            @PathVariable(name = "uuid_counterparty") String counterpartyId
    ) {
        CounterpartyEntity counterpartyEntity = counterpartyService.findById(counterpartyId);
        counterpartyEntity = authorityAttachmentService.addAuthorityAttachmentRepair(counterpartyEntity);
        CounterpartyResponseV1 counterpartyResponseV1 = counterpartyAdapter.adptCounterpartsNewToCounterpartyResponseV1ListExternal(counterpartyEntity, claimsService.findByIdNewEntity(counterpartyEntity.getClaims().getId()));
        return new ResponseEntity<>(counterpartyResponseV1, HttpStatus.OK);

    }


}
