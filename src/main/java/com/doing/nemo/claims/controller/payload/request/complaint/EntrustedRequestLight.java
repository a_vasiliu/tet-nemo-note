package com.doing.nemo.claims.controller.payload.request.complaint;

import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.EntrustedEnum.EntrustedEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Date;

public class EntrustedRequestLight implements Serializable {

    @JsonProperty("entrusted_to")
    private String entrustedTo;

    @JsonProperty("type")
    private EntrustedEnum entrustedType;

    @JsonProperty("entrusted_email")
    private String entrustedEmail;

    @JsonProperty("id_entrusted_to")
    private String idEntrustedTo;

    public String getEntrustedTo() {
        return entrustedTo;
    }

    public void setEntrustedTo(String entrustedTo) {
        this.entrustedTo = entrustedTo;
    }

    public EntrustedEnum getEntrustedType() {
        return entrustedType;
    }

    public void setEntrustedType(EntrustedEnum entrustedType) {
        this.entrustedType = entrustedType;
    }

    public String getEntrustedEmail() {
        return entrustedEmail;
    }

    public void setEntrustedEmail(String entrustedEmail) {
        this.entrustedEmail = entrustedEmail;
    }

    public String getIdEntrustedTo() {
        return idEntrustedTo;
    }

    public void setIdEntrustedTo(String idEntrustedTo) {
        this.idEntrustedTo = idEntrustedTo;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("EntrustedRequestLight{");
        sb.append("entrustedTo='").append(entrustedTo).append('\'');
        sb.append(", entrustedType=").append(entrustedType);
        sb.append(", entrustedEmail='").append(entrustedEmail).append('\'');
        sb.append(", idEntrustedTo='").append(idEntrustedTo).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
