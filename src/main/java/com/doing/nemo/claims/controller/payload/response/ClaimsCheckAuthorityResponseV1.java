package com.doing.nemo.claims.controller.payload.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class ClaimsCheckAuthorityResponseV1 implements Serializable {

    @JsonProperty("ClaimsCheckAuthority")
    private ClaimsCheckAuthority claimsCheckAuthority;

    public ClaimsCheckAuthority getClaimsCheckAuthority() {
        return claimsCheckAuthority;
    }

    public void setClaimsCheckAuthority(ClaimsCheckAuthority claimsCheckAuthority) {
        this.claimsCheckAuthority = claimsCheckAuthority;
    }

    @Override
    public String toString() {
        return "ClaimsCheckAuthorityResponseV1{" +
                "claimsCheckAuthority=" + claimsCheckAuthority +
                '}';
    }
}
