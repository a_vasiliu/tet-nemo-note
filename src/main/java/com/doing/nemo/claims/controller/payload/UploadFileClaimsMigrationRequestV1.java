package com.doing.nemo.claims.controller.payload;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

public class UploadFileClaimsMigrationRequestV1 implements Serializable {

    private static final long serialVersionUID = -3037818086294484134L;


    @NotNull
    @JsonProperty("file_name")
    @ApiModelProperty(value = "Name of the file to upload", example = "supermario.png", required = true)
    private String fileName;

    @JsonProperty("description")
    @ApiModelProperty(value = "Description", example = "Whooa there's a description field, too!")
    private String description;


    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    @Override
    public String toString() {
        return "UploadFileClaimsMigrationRequestV1{" +
                "fileName='" + fileName + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
