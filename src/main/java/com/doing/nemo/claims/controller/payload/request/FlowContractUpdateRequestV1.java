package com.doing.nemo.claims.controller.payload.request;

import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsFlowEnum;
import com.doing.nemo.claims.entity.settings.FlowContract;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class FlowContractUpdateRequestV1 implements Serializable {

    @JsonProperty("cod_contract_type")
    private String codContractType;

    @JsonProperty("description")
    private String description;

    @JsonProperty("default_flow")
    private ClaimsFlowEnum defaultFlow;

    @JsonProperty("unlocking_entry")
    private Boolean flagWS;

    @JsonProperty("ownership")
    private String ownership;

    @JsonProperty("ctrnote")
    private String ctrnote;

    @JsonProperty("ricaricar")
    private Boolean ricaricar;

    @JsonProperty("franchise")
    private Double franchise;

    @JsonProperty("flow_contract_type")
    private List<FlowContract> flowContractTypeList = new LinkedList<>();

    @JsonProperty("is_active")
    private Boolean isActive = true;

    public List<FlowContract> getFlowContractTypeList() {
        if(flowContractTypeList == null){
            return null;
        }
        return new ArrayList<>(flowContractTypeList);
    }

    public void setFlowContractTypeList(List<FlowContract> flowContractTypeList) {
        if(flowContractTypeList != null)
        {
            this.flowContractTypeList = new ArrayList<>(flowContractTypeList);
        } else {
            this.flowContractTypeList = null;
        }
    }

    public String getCodContractType() {
        return codContractType;
    }

    public void setCodContractType(String codContractType) {
        this.codContractType = codContractType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ClaimsFlowEnum getDefaultFlow() {
        return defaultFlow;
    }

    public void setDefaultFlow(ClaimsFlowEnum defaultFlow) {
        this.defaultFlow = defaultFlow;
    }

    public Boolean getFlagWS() {
        return flagWS;
    }

    public void setFlagWS(Boolean flagWS) {
        this.flagWS = flagWS;
    }

    public String getOwnership() {
        return ownership;
    }

    public void setOwnership(String ownership) {
        this.ownership = ownership;
    }

    public String getCtrnote() {
        return ctrnote;
    }

    public void setCtrnote(String ctrnote) {
        this.ctrnote = ctrnote;
    }

    public Boolean getRicaricar() {
        return ricaricar;
    }

    public void setRicaricar(Boolean ricaricar) {
        this.ricaricar = ricaricar;
    }

    public Double getFranchise() {
        return franchise;
    }

    public void setFranchise(Double franchise) {
        this.franchise = franchise;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    @Override
    public String toString() {
        return "FlowContractUpdateRequestV1{" +
                "flowContractTypeList=" + flowContractTypeList +
                '}';
    }
}
