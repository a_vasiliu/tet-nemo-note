package com.doing.nemo.claims.controller.payload.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProfileRequestV1 implements Serializable {

    private static final long serialVersionUID = 969286529608738044L;

    @NotNull(message = "the property is required")
    @Size(min = 1, max = 255, message = "the property value supports chars number into range 1-255")
    @JsonProperty("name")
    private String name;

    @NotNull(message = "the property is required")
    @Size(min = 1, max = 255, message = "the property value supports chars number into range 1-255")
    @JsonProperty("key")
    private String key;

    @NotNull(message = "the property is required")
    @Size(min = 1, max = 255, message = "the property value supports chars number into range 1-255")
    @JsonProperty("id_module")
    private String idModule;

    @Size(max = 255, message = "the property value supports supports a maximum of 255 chars")
    @JsonProperty("description")
    private String description;

    @JsonProperty("permissions")
    private List<String> permissions = new ArrayList<>();

    public ProfileRequestV1(@NotNull String name,@NotNull String key,@NotNull String idModule, String description, List<String> permissions) {
        this.name = name;
        this.key = key;
        this.idModule = idModule;
        this.description = description;
        if(permissions != null)
        {
            this.permissions = new ArrayList<>(permissions);
        }
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdModule() {
        return idModule;
    }

    public void setIdModule(String idModule) {
        this.idModule = idModule;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getPermissions() {
        if(permissions == null){
            return null;
        }
        return new ArrayList<>(permissions);
    }

    public void setPermissions(List<String> permissions) {
        if(permissions != null)
        {
            this.permissions = new ArrayList<>(permissions);
        } else {
            this.permissions = null;
        }
    }

    @Override
    public String toString() {
        return "ProfileRequestV1{" +
                "name='" + name + '\'' +
                ", key='" + key + '\'' +
                ", idModule='" + idModule + '\'' +
                ", description='" + description + '\'' +
                ", permissions=" + permissions +
                '}';
    }
}
