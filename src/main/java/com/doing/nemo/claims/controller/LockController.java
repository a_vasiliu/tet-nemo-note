package com.doing.nemo.claims.controller;

import com.doing.nemo.claims.adapter.LockAdapter;
import com.doing.nemo.claims.controller.payload.response.LockResponseV1;
import com.doing.nemo.claims.entity.settings.LockEntity;
import com.doing.nemo.claims.service.LockService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/v1")
@Api("Lock")
public class LockController {

    @Autowired
    private LockService lockService;



    @PostMapping("/lock/{claim_id}/{user_id}")
    @Transactional
    public ResponseEntity<LockResponseV1> lockClaims(
            @PathVariable(name = "claim_id") String claimId,
            @PathVariable(name = "user_id") String userId) {
        LockEntity lockEntity = lockService.lockPractice(userId, claimId);
        return new ResponseEntity<>(LockAdapter.adptFromLockEntityToLockResponse(lockEntity), HttpStatus.OK);
    }

    @PostMapping("/unlock/{claim_id}/{user_id}")
    @Transactional
    public void unlockClaims(
            @PathVariable(name = "claim_id") String claimId,
            @PathVariable(name = "user_id") String userId) {
        lockService.unlockPractice(userId, claimId);
    }

    @PostMapping("/lock/alive/{claim_id}/{user_id}")
    @Transactional
    public ResponseEntity<LockResponseV1> aliveClaims(
            @PathVariable(name = "claim_id") String claimId,
            @PathVariable(name = "user_id") String userId) {
        LockEntity lockEntity = lockService.alivePractice(userId, claimId);
        return new ResponseEntity<>(LockAdapter.adptFromLockEntityToLockResponse(lockEntity), HttpStatus.OK);
    }

    @DeleteMapping("/lock/cron/unlockable")
    @Transactional
    @ApiOperation(value = "Unlock claims", notes = "Unlock claims with cron")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public void lockJob (){
        lockService.lockJob();
    }
}