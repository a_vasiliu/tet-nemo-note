package com.doing.nemo.claims.controller.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;

import java.io.Serializable;

public class ExportSupplierCodeRequestV1 implements Serializable {

    @JsonProperty("vat_number")
    private String vatNumber;

    @JsonProperty("denomination")
    private String denomination;

    @JsonProperty("code_to_export")
    private String codeToExport;

    @JsonProperty("is_active")
    private Boolean isActive = true;

    public ExportSupplierCodeRequestV1() {
    }

    public ExportSupplierCodeRequestV1(String vatNumber, String denomination, String codeToExport, Boolean isActive) {
        this.vatNumber = vatNumber;
        this.denomination = denomination;
        this.codeToExport = codeToExport;
        this.isActive = isActive;
    }

    public String getVatNumber() {
        return vatNumber;
    }

    public void setVatNumber(String vatNumber) {
        this.vatNumber = vatNumber;
    }

    public String getDenomination() {
        return denomination;
    }

    public void setDenomination(String denomination) {
        this.denomination = denomination;
    }

    public String getCodeToExport() {
        return codeToExport;
    }

    public void setCodeToExport(String codeToExport) {
        this.codeToExport = codeToExport;
    }

    public Boolean getActive() {
        return isActive;
    }

    @JsonSetter
    public void setActive(Boolean active) {
        if(active != null)
            isActive = active;
    }

    @Override
    public String toString() {
        return "ExportSupplierCodeRequestV1{" +
                "vatNumber='" + vatNumber + '\'' +
                ", denomination='" + denomination + '\'' +
                ", codeToExport='" + codeToExport + '\'' +
                ", isActive=" + isActive +
                '}';
    }
}
