package com.doing.nemo.claims.controller.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.UUID;

public class MotivationRequestV1 implements Serializable {

    @JsonProperty("id")
    private UUID id;

    @JsonProperty("answer_type")
    @NotNull
    private String answerType;

    @JsonProperty("description")
    @NotNull
    private String description;

    @JsonProperty("is_active")
    private Boolean isActive = true;

    public MotivationRequestV1(UUID id, @NotNull String answerType, @NotNull String description, Boolean isActive) {
        this.id = id;
        this.answerType = answerType;
        this.description = description;
        this.isActive = isActive;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getAnswerType() {
        return answerType;
    }

    public void setAnswerType(String answerType) {
        this.answerType = answerType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getActive() {
        return isActive;
    }

    @JsonSetter
    public void setActive(Boolean active) {
        if (active != null)
            isActive = active;
    }

    @Override
    public String toString() {
        return "MotivationRequestV1{" +
                "id=" + id +
                ", answerType='" + answerType + '\'' +
                ", description='" + description + '\'' +
                ", isActive=" + isActive +
                '}';
    }
}
