package com.doing.nemo.claims.controller.payload.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@JsonIgnoreProperties(ignoreUnknown = true)
public class ProfileModulePermissionsDetailResponseV1 implements Serializable {


    private static final long serialVersionUID = -79175279130716308L;

    @JsonProperty("id")
    private String id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("key")
    private String key;

    @JsonProperty("module")
    private ModuleResponseV1 module;

    @JsonProperty("description")
    private String description;

    @JsonProperty("permissions")
    private List<PermissionResponseV1> permissions = new ArrayList<>();

    @JsonProperty("is_custom")
    private boolean custom;

    @JsonProperty("has_commodities")
    private boolean commodities;

    @JsonProperty("created_at")
    private String createdAt;

    @JsonProperty("updated_at")
    private String updatedAt;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ModuleResponseV1 getModule() {
        return module;
    }

    public void setModule(ModuleResponseV1 module) {
        this.module = module;
    }

    public List<PermissionResponseV1> getPermissions() {
        if(permissions == null){
            return null;
        }
        return new ArrayList<>(permissions);
    }

    public void setPermissions(List<PermissionResponseV1> permissions) {
        if(permissions != null)
        {
            this.permissions = new ArrayList<>(permissions);
        } else {
            this.permissions = null;
        }
    }

    public boolean isCustom() {
        return custom;
    }

    public void setCustom(boolean custom) {
        this.custom = custom;
    }

    public boolean isCommodities() {
        return commodities;
    }

    public void setCommodities(boolean commodities) {
        this.commodities = commodities;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
}
