package com.doing.nemo.claims.controller.payload.response.authority.claims;

import com.doing.nemo.claims.controller.payload.response.damaged.ImpactPointResponse;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class DamagedAuthorityResponseV1 implements Serializable {

    @JsonProperty("impact_point")
    private ImpactPointResponse impactPoint;

    public ImpactPointResponse getImpactPoint() {
        if (impactPoint==null) {
            impactPoint = new ImpactPointResponse();
            impactPoint.setDamageToVehicle("Descrizione non presente");
            impactPoint.setDetectionsImpactPoint(null);
            impactPoint.setIncidentDescription("Descrizione non presente");
        }
        return impactPoint;
    }

    public void setImpactPoint(ImpactPointResponse impactPoint) {
        this.impactPoint = impactPoint;
    }
}
