package com.doing.nemo.claims.controller.payload.response;

import com.doing.nemo.claims.entity.settings.AutomaticAffiliationRuleInspectorateEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.Type;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class InspectorateResponseV1<T> implements Serializable {

    @JsonProperty("id")
    private UUID id;

    @JsonProperty("code")
    private Long code;

    @JsonProperty("name")
    private String name;

    @JsonProperty("email")
    private String email;

    @JsonProperty("address")
    private String address;

    @JsonProperty("zip_code")
    private String zipCode;

    @JsonProperty("city")
    private String city;

    @JsonProperty("province")
    private String province;

    @JsonProperty("state")
    private String state;

    @JsonProperty("vat_number")
    private String vatNumber;

    @JsonProperty("fiscal_code")
    private String fiscalCode;

    @JsonProperty("telephone")
    private String telephone;

    @JsonProperty("fax")
    private String fax;

    @JsonProperty("web_site")
    private String webSite;

    @JsonProperty("reference_person")
    private String referencePerson;

    @JsonProperty("attorney")
    private String attorney;

    @JsonProperty("external_code")
    private String externalCode;

    @JsonProperty("automatic_affiliation_rule")
    @Type(type = "JsonDataUserType")
    private List<AutomaticAffiliationRuleInspectorateEntity> automaticAffiliationRuleEntityList;

    @JsonProperty("is_active")
    private Boolean isActive;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getVatNumber() {
        return vatNumber;
    }

    public void setVatNumber(String vatNumber) {
        this.vatNumber = vatNumber;
    }

    public String getFiscalCode() {
        return fiscalCode;
    }

    public void setFiscalCode(String fiscalCode) {
        this.fiscalCode = fiscalCode;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getWebSite() {
        return webSite;
    }

    public void setWebSite(String webSite) {
        this.webSite = webSite;
    }

    public String getReferencePerson() {
        return referencePerson;
    }

    public void setReferencePerson(String referencePerson) {
        this.referencePerson = referencePerson;
    }

    public String getAttorney() {
        return attorney;
    }

    public void setAttorney(String attorney) {
        this.attorney = attorney;
    }

    public String getExternalCode() {
        return externalCode;
    }

    public void setExternalCode(String externalCode) {
        this.externalCode = externalCode;
    }

    public List<AutomaticAffiliationRuleInspectorateEntity> getAutomaticAffiliationRuleEntityList() {
        if(automaticAffiliationRuleEntityList == null){
            return  null;
        }
        return new ArrayList<>(automaticAffiliationRuleEntityList);
    }

    public void setAutomaticAffiliationRuleEntityList(List<AutomaticAffiliationRuleInspectorateEntity> automaticAffiliationRuleEntityList) {
        if(automaticAffiliationRuleEntityList != null)
        {
            this.automaticAffiliationRuleEntityList = new ArrayList<>(automaticAffiliationRuleEntityList);
        } else {
            this.automaticAffiliationRuleEntityList = null;
        }
    }

    @JsonIgnore
    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    @Override
    public String toString() {
        return "InspectorateResponseV1{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", address='" + address + '\'' +
                ", zipCode=" + zipCode +
                ", city='" + city + '\'' +
                ", province='" + province + '\'' +
                ", state='" + state + '\'' +
                ", vatNumber='" + vatNumber + '\'' +
                ", fiscalCode='" + fiscalCode + '\'' +
                ", telephone='" + telephone + '\'' +
                ", fax='" + fax + '\'' +
                ", webSite='" + webSite + '\'' +
                ", referencePerson='" + referencePerson + '\'' +
                ", attorney='" + attorney + '\'' +
                ", externalCode='" + externalCode + '\'' +
                ", automaticAffiliationRuleEntityList=" + automaticAffiliationRuleEntityList +
                ", isActive=" + isActive +
                '}';
    }
}
