package com.doing.nemo.claims.controller.payload.response;

import com.doing.nemo.claims.controller.payload.response.claims.ComplaintResponse;
import com.doing.nemo.claims.controller.payload.response.claims.DamagedResponse;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsFlowEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class ClaimsResponseComplaintOperatorV2 implements Serializable {

    @JsonProperty("id")
    private String id;

    @JsonProperty("created_at")
    private String createdAt;

    @JsonProperty("update_at")
    private String updateAt;

    @JsonProperty("type")
    private String type;

    @JsonProperty("practice_id")
    private Long practiceId;

    @JsonProperty("status")
    private String status;

    @JsonProperty("in_evidence")
    private Boolean inEvidence;

    @JsonProperty("po_variation")
    private Boolean poVariation;

    @JsonProperty("complaint")
    private ComplaintResponse complaint;

    @JsonProperty("damaged")
    private DamagedResponse damaged;

    @JsonProperty("is_pending")
    private Boolean isPending;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(String updateAt) {
        this.updateAt = updateAt;
    }

    public Long getPracticeId() {
        return practiceId;
    }

    public void setPracticeId(Long practiceId) {
        this.practiceId = practiceId;
    }


    public Boolean getInEvidence() {
        return inEvidence;
    }

    public void setInEvidence(Boolean inEvidence) {
        this.inEvidence = inEvidence;
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ComplaintResponse getComplaint() {
        return complaint;
    }

    public void setComplaint(ComplaintResponse complaint) {
        this.complaint = complaint;
    }

    public DamagedResponse getDamaged() {
        return damaged;
    }

    public void setDamaged(DamagedResponse damaged) {
        this.damaged = damaged;
    }

    public Boolean getPoVariation() {
        return poVariation;
    }

    public void setPoVariation(Boolean poVariation) {
        this.poVariation = poVariation;
    }

    @JsonIgnore
    public Boolean getPending() {
        return isPending;
    }

    public void setPending(Boolean pending) {
        isPending = pending;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ClaimsResponseV2{");
        sb.append("id='").append(id).append('\'');
        sb.append(", createdAt='").append(createdAt).append('\'');
        sb.append(", updateAt='").append(updateAt).append('\'');
        sb.append(", practiceId=").append(practiceId);
        sb.append(", inEvidence=").append(inEvidence);
        sb.append(", status=").append(status);
        sb.append(", type=").append(type);
        sb.append(", complaint=").append(complaint);
        sb.append(", damaged=").append(damaged);
        sb.append(", poVariation=").append(poVariation);
        sb.append('}');
        return sb.toString();
    }
}
