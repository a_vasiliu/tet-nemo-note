package com.doing.nemo.claims.controller.payload.request.damaged;

import com.doing.nemo.claims.controller.payload.request.RegistryRequestV1;
import com.doing.nemo.claims.entity.enumerated.AntitheftEnum.AntitheftTypeEnum;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbAntiTheftService.AntiTheftRequest;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class AntiTheftServiceRequest  implements Serializable {
    @JsonProperty("id")
    private UUID id;

    @JsonProperty("code_anti_theft_service")
    private String codeAntiTheftService = "";

    @JsonProperty("name")
    @NotNull
    private String name;

    @JsonProperty("business_name")
    @NotNull
    private String businessName;

    @JsonProperty("supplier_code")
    @NotNull
    private Integer supplierCode;

    @JsonProperty("email")
    private String email = "";

    @JsonProperty("phone_number")
    @NotNull
    private String phoneNumber;

    @JsonProperty("provider_type")
    private String providerType = "";

    @JsonProperty("time_out_in_seconds")
    private Integer timeOutInSeconds;

    @JsonProperty("end_point_url")
    private String endPointUrl = "";

    @JsonProperty("username")
    private String username = "";

    @JsonProperty("password")
    private String password = "";

    @JsonProperty("company_code")
    private String companyCode = "";

    @JsonProperty("active")
    private Boolean active;

    @JsonProperty("type")
    private AntitheftTypeEnum type;

    @JsonProperty("is_active")
    private Boolean IsActive=true;

    @JsonProperty("anti_theft_request_list")
    private List<AntiTheftRequest> antiTheftList;

    @JsonProperty("registry_list")
    private List<RegistryRequestV1> registryRequestV1List;


    public List<RegistryRequestV1> getRegistryRequestV1List() {
        if(registryRequestV1List == null){
            return null;
        }
        return new ArrayList<>(registryRequestV1List);
    }

    public void setRegistryRequestV1List(List<RegistryRequestV1> registryRequestV1List) {
        if(registryRequestV1List != null){
            this.registryRequestV1List = new ArrayList<>(registryRequestV1List);
        } else {
            this.registryRequestV1List = null;
        }
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getCodeAntiTheftService() {
        return codeAntiTheftService;
    }

    public void setCodeAntiTheftService(String codeAntiTheftService) {
        this.codeAntiTheftService = codeAntiTheftService;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public Integer getSupplierCode() {
        return supplierCode;
    }

    public void setSupplierCode(Integer supplierCode) {
        this.supplierCode = supplierCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getProviderType() {
        return providerType;
    }

    public void setProviderType(String providerType) {
        this.providerType = providerType;
    }

    public Integer getTimeOutInSeconds() {
        return timeOutInSeconds;
    }

    public void setTimeOutInSeconds(Integer timeOutInSeconds) {
        this.timeOutInSeconds = timeOutInSeconds;
    }

    public String getEndPointUrl() {
        return endPointUrl;
    }

    public void setEndPointUrl(String endPointUrl) {
        this.endPointUrl = endPointUrl;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public List<AntiTheftRequest> getAntiTheftList() {
        if(antiTheftList == null){
            return null;
        }
        return new ArrayList<>(antiTheftList);
    }

    public void setAntiTheftList(List<AntiTheftRequest> antiTheftList) {
        if(antiTheftList != null){
            this.antiTheftList = new ArrayList<>(antiTheftList);
        } else {
            this.antiTheftList = null;
        }
    }

    public AntitheftTypeEnum getType() {
        return type;
    }

    public void setType(AntitheftTypeEnum type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "AntiTheftServiceRequest{" +
                "registryRequestV1List=" + registryRequestV1List +
                '}';
    }
}
