package com.doing.nemo.claims.controller.payload.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Map;
@JsonIgnoreProperties(ignoreUnknown = true)
public class NemoAuthCompleteResponseV1 implements Serializable {

    @JsonProperty("metadata")
    private Map<String,String> metadata;

    @JsonProperty("access_token")
    private String accessToken;

    @JsonProperty("refresh_token")
    private String refreshToken;

    @JsonProperty("sessionmetadata_token")
    private String sessionmetadataToken;

    public Map<String, String> getMetadata() {
        return metadata;
    }

    public void setMetadata(Map<String, String> metadata) {
        this.metadata = metadata;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public String getSessionmetadataToken() {
        return sessionmetadataToken;
    }

    public void setSessionmetadataToken(String sessionmetadataToken) {
        this.sessionmetadataToken = sessionmetadataToken;
    }

    @Override
    public String toString() {
        return "NemoAuthCompleteResponseV1{" +
                "metadata=" + metadata +
                ", accessToken='" + accessToken + '\'' +
                ", refreshToken='" + refreshToken + '\'' +
                ", sessionmetadataToken='" + sessionmetadataToken + '\'' +
                '}';
    }
}
