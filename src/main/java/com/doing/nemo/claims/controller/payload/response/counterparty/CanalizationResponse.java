package com.doing.nemo.claims.controller.payload.response.counterparty;

import com.doing.nemo.claims.controller.payload.response.CenterResponseV1;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class CanalizationResponse implements Serializable {

    @JsonProperty("center")
    private CenterResponseV1 center;

    @JsonProperty("date")
    private String date;

    public CanalizationResponse() {
    }

    public CanalizationResponse(CenterResponseV1 center, String date) {
        this.center = center;
        this.date = date;
    }

    public CenterResponseV1 getCenter() {
        return center;
    }

    public void setCenter(CenterResponseV1 center) {
        this.center = center;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "CanalizationResponse{" +
                "center=" + center +
                ", date='" + date + '\'' +
                '}';
    }
}
