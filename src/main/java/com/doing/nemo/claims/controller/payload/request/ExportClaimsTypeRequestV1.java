package com.doing.nemo.claims.controller.payload.request;

import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.settings.DmSystemsEntity;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;

import java.io.Serializable;

public class ExportClaimsTypeRequestV1 implements Serializable {

    @JsonProperty("claims_type")
    private DataAccidentTypeAccidentEnum claimsType;

    @JsonProperty("dm_system_id")
    private DmSystemsEntity dmSystem;

    @JsonProperty("code")
    private String code;

    @JsonProperty("description")
    private String description;

    @JsonProperty("is_active")
    private Boolean isActive = true;

    public ExportClaimsTypeRequestV1() {
    }

    public ExportClaimsTypeRequestV1(DataAccidentTypeAccidentEnum claimsType, DmSystemsEntity dmSystem, String code, String description, Boolean isActive) {
        this.claimsType = claimsType;
        this.dmSystem = dmSystem;
        this.code = code;
        this.description = description;
        this.isActive = isActive;
    }

    public DataAccidentTypeAccidentEnum getClaimsType() {
        return claimsType;
    }

    public void setClaimsType(DataAccidentTypeAccidentEnum claimsType) {
        this.claimsType = claimsType;
    }

    public DmSystemsEntity getDmSystem() {
        return dmSystem;
    }

    public void setDmSystem(DmSystemsEntity dmSystem) {
        this.dmSystem = dmSystem;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getActive() {
        return isActive;
    }

    @JsonSetter
    public void setActive(Boolean active) {
        if(active != null)
        isActive = active;
    }

    @Override
    public String toString() {
        return "ExportClaimsTypeRequestV1{" +
                "claimsType=" + claimsType +
                ", dmSystem=" + dmSystem +
                ", code='" + code + '\'' +
                ", description='" + description + '\'' +
                ", isActive=" + isActive +
                '}';
    }
}
