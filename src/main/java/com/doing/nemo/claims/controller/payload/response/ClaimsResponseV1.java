package com.doing.nemo.claims.controller.payload.response;

import com.doing.nemo.claims.controller.payload.response.authority.claims.AuthorityResponseV1;
import com.doing.nemo.claims.controller.payload.response.claims.*;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsFlowEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Column;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ClaimsResponseV1 implements Serializable {

    @JsonProperty("id")
    private String id;

    @JsonProperty("created_at")
    private String createdAt;

    @JsonProperty("update_at")
    private String updateAt;

    @JsonProperty("practice_id")
    private Long practiceId;

    @JsonProperty("pai_comunication")
    private Boolean paiComunication;

    @JsonProperty("legal_comunication")
    private Boolean legalComunication;

    @JsonProperty("forced")
    private Boolean forced;

    @JsonProperty("in_evidence")
    private Boolean inEvidence;

    @JsonProperty("status")
    private ClaimsStatusEnum status;

    @JsonProperty("motivation")
    private String motivation;

    @JsonProperty("type")
    private ClaimsFlowEnum type;

    @JsonProperty("complaint")
    private ComplaintResponse complaint;

    @JsonProperty("forms")
    private FormResponse forms;

    @JsonProperty("damaged")
    private DamagedResponse damaged;

    @JsonProperty("cai_details")
    private CaiResponse caiDetails;

    @JsonProperty("exemption")
    private ExemptionResponse exemption;

    @JsonProperty("is_with_counterparty")
    private Boolean isWithCounterparty;

    @JsonProperty("counterparty")
    private List<CounterpartyResponse> counterparty;

    @JsonProperty("deponent")
    private List<DeponentResponse> deponentList;

    @JsonProperty("wounded")
    private List<WoundedResponse> woundedList;

    @JsonProperty("notes")
    private List<NoteResponse> notes;

    @JsonProperty("historical")
    private List<HistoricalResponse> historical;

    @JsonProperty("found_model")
    private FoundModelResponse foundModel;

    @JsonProperty("id_saleforce")
    private Long idSaleforce;

    @JsonProperty("with_continuation")
    private Boolean withContinuation;

    @JsonProperty("refund")
    private RefundResponse refund;

    @JsonProperty("metadata")
    private Map<String,Object> metadata;

    @JsonProperty("theft")
    private TheftResponseV1 theft;

    @JsonProperty("is_read_msa")
    private Boolean isRead;

    @JsonProperty("is_read_acclaims")
    private Boolean isReadAcclaims;


    @JsonProperty("authorities")
    private List<AuthorityResponseV1> authorityResponseV1List;

    @JsonProperty("is_authority_linkable")
    private Boolean isAuthorityLinkable;

    @JsonProperty("po_variation")
    private Boolean poVariation;

    @JsonProperty("total_miles_franchise")
    private Long totalPenalty;

    @JsonProperty("is_migrated")
    private Boolean isMigrated;

    @JsonProperty("is_complete_documentation")
    private Boolean isCompleteDocumentation;

    @JsonProperty("is_pending")
    private Boolean isPending;

    @JsonIgnore
    public Boolean getMigrated() {
        return isMigrated;
    }

    public void setMigrated(Boolean migrated) {
        isMigrated = migrated;
    }

    @JsonIgnore
    public Boolean getReadAcclaims() {
        return isReadAcclaims;
    }

    public void setReadAcclaims(Boolean readAcclaims) {
        isReadAcclaims = readAcclaims;
    }

    public Long getTotalPenalty() {
        return totalPenalty;
    }

    public void setTotalPenalty(Long totalPenalty) {
        this.totalPenalty = totalPenalty;
    }

    public Boolean getLegalComunication() {
        return legalComunication;
    }

    public void setLegalComunication(Boolean legalComunication) {
        this.legalComunication = legalComunication;
    }

    @JsonIgnore
    public Boolean getAuthorityLinkable() {
        return isAuthorityLinkable;
    }

    public void setAuthorityLinkable(Boolean authorityLinkable) {
        isAuthorityLinkable = authorityLinkable;
    }

    public TheftResponseV1 getTheft() {
        return theft;
    }

    public void setTheft(TheftResponseV1 theft) {
        this.theft = theft;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(String updateAt) {
        this.updateAt = updateAt;
    }

    public Long getPracticeId() {
        return practiceId;
    }

    public void setPracticeId(Long practiceId) {
        this.practiceId = practiceId;
    }

    public Boolean getPaiComunication() {
        return paiComunication;
    }

    public void setPaiComunication(Boolean paiComunication) {
        this.paiComunication = paiComunication;
    }

    public Boolean getForced() {
        return forced;
    }

    public void setForced(Boolean forced) {
        this.forced = forced;
    }

    public Boolean getInEvidence() {
        return inEvidence;
    }

    public void setInEvidence(Boolean inEvidence) {
        this.inEvidence = inEvidence;
    }

    public ClaimsStatusEnum getStatus() {
        return status;
    }

    public void setStatus(ClaimsStatusEnum status) {
        this.status = status;
    }

    public ClaimsFlowEnum getType() {
        return type;
    }

    public void setType(ClaimsFlowEnum type) {
        this.type = type;
    }

    public ComplaintResponse getComplaint() {
        return complaint;
    }

    public void setComplaint(ComplaintResponse complaint) {
        this.complaint = complaint;
    }

    public FormResponse getForms() {
        return forms;
    }

    public void setForms(FormResponse forms) {
        this.forms = forms;
    }

    public DamagedResponse getDamaged() {
        return damaged;
    }

    public void setDamaged(DamagedResponse damaged) {
        this.damaged = damaged;
    }

    public CaiResponse getCaiDetails() {
        return caiDetails;
    }

    public void setCaiDetails(CaiResponse caiDetails) {
        this.caiDetails = caiDetails;
    }

    @JsonIgnore
    public Boolean getWithCounterparty() {
        return isWithCounterparty;
    }

    public void setWithCounterparty(Boolean withCounterparty) {
        isWithCounterparty = withCounterparty;
    }

    public List<CounterpartyResponse> getCounterparty() {
        if(counterparty == null){
            return null;
        }
        return new ArrayList<>(counterparty);
    }

    public void setCounterparty(List<CounterpartyResponse> counterparty) {
        if(counterparty != null)
        {
            this.counterparty = new ArrayList<>(counterparty);
        } else {
            this.counterparty = null;
        }
    }

    public List<DeponentResponse> getDeponentList() {
        if(deponentList == null){
            return null;
        }
        return new ArrayList<>(deponentList);
    }

    public void setDeponentList(List<DeponentResponse> deponentList) {
        if(deponentList != null)
        {
            this.deponentList =new ArrayList<>(deponentList);
        } else {
            this.deponentList = null;
        }
    }

    public List<WoundedResponse> getWoundedList() {
        if(woundedList == null){
            return null;
        }
        return new ArrayList<>(woundedList);
    }

    public void setWoundedList(List<WoundedResponse> woundedList) {
        if(woundedList != null)
        {
            this.woundedList =new ArrayList<>(woundedList);
        } else {
            this.woundedList = null;
        }
    }

    public List<NoteResponse> getNotes() {
        if(notes == null){
            return null;
        }
        return new ArrayList<>(notes);
    }

    public void setNotes(List<NoteResponse> notes) {
        if(notes != null)
        {
            this.notes = new ArrayList<>(notes);
        } else {
            this.notes = null;
        }
    }

    public List<HistoricalResponse> getHistorical() {
        if(historical == null){
            return null;
        }
        return new ArrayList<>(historical);
    }

    public void setHistorical(List<HistoricalResponse> historical) {
        if(historical != null)
        {
            this.historical =new ArrayList<>(historical);
        } else {
            this.historical = null;
        }
    }

    public FoundModelResponse getFoundModel() {
        return foundModel;
    }

    public void setFoundModel(FoundModelResponse foundModel) {
        this.foundModel = foundModel;
    }

    public Long getIdSaleforce() {
        return idSaleforce;
    }

    public void setIdSaleforce(Long idSaleforce) {
        this.idSaleforce = idSaleforce;
    }

    public ExemptionResponse getExemption() {
        return exemption;
    }

    public void setExemption(ExemptionResponse exemption) {
        this.exemption = exemption;
    }

    public Boolean getWithContinuation() {
        return withContinuation;
    }

    public void setWithContinuation(Boolean withContinuation) {
        this.withContinuation = withContinuation;
    }

    public RefundResponse getRefund() {
        return refund;
    }

    public void setRefund(RefundResponse refund) {
        this.refund = refund;
    }

    public Map<String,Object> getMetadata() {
        return metadata;
    }

    public void setMetadata(Map<String,Object> metadata) {
        this.metadata = metadata;
    }

    public String getMotivation() {
        return motivation;
    }

    public void setMotivation(String motivation) {
        this.motivation = motivation;
    }

    @JsonIgnore
    public Boolean getRead() {
        return isRead;
    }

    public void setRead(Boolean read) {
        isRead = read;
    }

    public List<AuthorityResponseV1> getAuthorityResponseV1List() {
        if(authorityResponseV1List == null){
            return null;
        }
        return new ArrayList<>(authorityResponseV1List);
    }

    public void setAuthorityResponseV1List(List<AuthorityResponseV1> authorityResponseV1List) {
        if(authorityResponseV1List != null)
        {
            this.authorityResponseV1List = new ArrayList<>(authorityResponseV1List);
        } else {
            this.authorityResponseV1List = null;
        }
    }

    public Boolean getPoVariation() {
        return poVariation;
    }

    public void setPoVariation(Boolean poVariation) {
        this.poVariation = poVariation;
    }

    @JsonIgnore
    public Boolean getCompleteDocumentation() {
        return isCompleteDocumentation;
    }

    public void setCompleteDocumentation(Boolean completeDocumentation) {
        isCompleteDocumentation = completeDocumentation;
    }

    @JsonIgnore
    public Boolean getPending() {
        return isPending;
    }

    public void setPending(Boolean pending) {
        this.isPending = pending;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ClaimsResponseV1{");
        sb.append("id='").append(id).append('\'');
        sb.append(", createdAt='").append(createdAt).append('\'');
        sb.append(", updateAt='").append(updateAt).append('\'');
        sb.append(", practiceId=").append(practiceId);
        sb.append(", paiComunication=").append(paiComunication);
        sb.append(", legalComunication=").append(legalComunication);
        sb.append(", forced=").append(forced);
        sb.append(", inEvidence=").append(inEvidence);
        sb.append(", status=").append(status);
        sb.append(", motivation='").append(motivation).append('\'');
        sb.append(", type=").append(type);
        sb.append(", complaint=").append(complaint);
        sb.append(", forms=").append(forms);
        sb.append(", damaged=").append(damaged);
        sb.append(", caiDetails=").append(caiDetails);
        sb.append(", exemption=").append(exemption);
        sb.append(", isWithCounterparty=").append(isWithCounterparty);
        sb.append(", counterparty=").append(counterparty);
        sb.append(", deponentList=").append(deponentList);
        sb.append(", woundedList=").append(woundedList);
        sb.append(", notes=").append(notes);
        sb.append(", historical=").append(historical);
        sb.append(", foundModel=").append(foundModel);
        sb.append(", idSaleforce=").append(idSaleforce);
        sb.append(", withContinuation=").append(withContinuation);
        sb.append(", refund=").append(refund);
        sb.append(", metadata=").append(metadata);
        sb.append(", theft=").append(theft);
        sb.append(", isRead=").append(isRead);
        sb.append(", isReadAcclaims=").append(isReadAcclaims);
        sb.append(", authorityResponseV1List=").append(authorityResponseV1List);
        sb.append(", isAuthorityLinkable=").append(isAuthorityLinkable);
        sb.append(", poVariation=").append(poVariation);
        sb.append(", totalPenalty=").append(totalPenalty);
        sb.append(", isMigrated=").append(isMigrated);
        sb.append(", isCompleteDocumentation=").append(isCompleteDocumentation);
        sb.append('}');
        return sb.toString();
    }


}
