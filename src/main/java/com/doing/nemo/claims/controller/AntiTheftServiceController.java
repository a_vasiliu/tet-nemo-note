package com.doing.nemo.claims.controller;

import com.doing.nemo.claims.adapter.AntiTheftServiceAdapter;
import com.doing.nemo.claims.common.validation.MessageCode;
import com.doing.nemo.claims.common.validation.RequestValidator;
import com.doing.nemo.claims.controller.payload.request.AntiTheftServiceRequestV1;
import com.doing.nemo.claims.controller.payload.request.ListRequestV1;
import com.doing.nemo.claims.controller.payload.response.AntiTheftServiceResponseV1;
import com.doing.nemo.claims.controller.payload.response.IdResponseV1;
import com.doing.nemo.claims.controller.payload.response.PaginationResponseV1;
import com.doing.nemo.claims.entity.enumerated.AntitheftEnum.AntitheftTypeEnum;
import com.doing.nemo.claims.entity.settings.AntiTheftServiceEntity;
import com.doing.nemo.claims.service.AntiTheftServiceService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/v1")
@Api("AntiTheftService")
public class AntiTheftServiceController {

    @Autowired
    private AntiTheftServiceService antiTheftServiceService;

    @Autowired
    private RequestValidator requestValidator;

    @PostMapping("/antitheftservice")
    @Transactional
    @ApiOperation(value = "Insert AntiTheftService", notes = "Insert AntiTheftService using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = IdResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<IdResponseV1> postAntiTheftService(@ApiParam(value = "Body of the AntiTheftService to be created", required = true) @RequestBody AntiTheftServiceRequestV1 antiTheftServiceRequestV1) {
        requestValidator.validateRequest(antiTheftServiceRequestV1, MessageCode.E00X_1000);
        AntiTheftServiceEntity antiTheftServiceEntity = AntiTheftServiceAdapter.adptFromAntiTheftServiceRequestToAntiTheftServiceEntity(antiTheftServiceRequestV1);
        antiTheftServiceEntity = antiTheftServiceService.insertAntiTheftService(antiTheftServiceEntity);
        IdResponseV1 antiTheftServiceResponseIdV1 = new IdResponseV1(antiTheftServiceEntity.getId());
        return new ResponseEntity(antiTheftServiceResponseIdV1, HttpStatus.CREATED);
    }

    @PostMapping("/antitheftservices")
    @Transactional
    @ApiOperation(value = "Insert More AntiTheftServices", notes = "Insert AntiTheftServices using info passed in the bodies")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = AntiTheftServiceResponseV1.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<AntiTheftServiceResponseV1>> postAntiTheftServices(@ApiParam(value = "Bodies of the AntiTheftServices to be created", required = true) @RequestBody ListRequestV1<AntiTheftServiceRequestV1> antiTheftServiceListRequestV1) {

        List<AntiTheftServiceResponseV1> antiTheftServiceResponseV1List = new ArrayList<>();
        for (AntiTheftServiceRequestV1 antiTheftServiceRequestV1 : antiTheftServiceListRequestV1.getData()) {
            requestValidator.validateRequest(antiTheftServiceRequestV1, MessageCode.E00X_1000);
            AntiTheftServiceEntity antiTheftServiceEntity = AntiTheftServiceAdapter.adptFromAntiTheftServiceRequestToAntiTheftServiceEntity(antiTheftServiceRequestV1);
            antiTheftServiceEntity = antiTheftServiceService.insertAntiTheftService(antiTheftServiceEntity);
            AntiTheftServiceResponseV1 antiTheftServiceResponseV1 = AntiTheftServiceAdapter.adptFromAntiTheftServiceEntityToAntiTheftServiceResponseV1(antiTheftServiceEntity);
            antiTheftServiceResponseV1List.add(antiTheftServiceResponseV1);
        }
        return new ResponseEntity(antiTheftServiceResponseV1List, HttpStatus.CREATED);
    }

    @PutMapping("/antitheftservice/{UUID}")
    @Transactional
    @ApiOperation(value = "Upload AntiTheftService", notes = "Upload AntiTheftService using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = AntiTheftServiceResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<AntiTheftServiceResponseV1> putAntiTheftService(@ApiParam(value = "UUID that identifies the AntiTheftService to be modified", required = true) @PathVariable(name = "UUID") UUID id, @ApiParam(value = "Updated body of the AntiTheftService", required = true)
    @RequestBody AntiTheftServiceRequestV1 antiTheftServiceRequestV1) {
        requestValidator.validateRequest(antiTheftServiceRequestV1, MessageCode.E00X_1000);
        AntiTheftServiceEntity antiTheftServiceEntity = antiTheftServiceService.selectAntiTheftService(id);
        AntiTheftServiceEntity antiTheftServiceEntityNew = AntiTheftServiceAdapter.adptFromAntiTheftServiceRequestToAntiTheftServiceEntity(antiTheftServiceRequestV1);
        antiTheftServiceEntityNew.setId(antiTheftServiceEntity.getId());
        antiTheftServiceEntityNew = antiTheftServiceService.updateAntiTheftService(antiTheftServiceEntityNew);
        AntiTheftServiceResponseV1 antiTheftServiceResponseV1 = AntiTheftServiceAdapter.adptFromAntiTheftServiceEntityToAntiTheftServiceResponseV1(antiTheftServiceEntityNew);
        return new ResponseEntity<>(antiTheftServiceResponseV1, HttpStatus.OK);
    }

    @GetMapping("/antitheftservice/{UUID}")
    @Transactional
    @ApiOperation(value = "Recover AntiTheftService", notes = "Return a AntiTheftService using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = AntiTheftServiceResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<AntiTheftServiceResponseV1> getAntiTheftService(@ApiParam(value = "UUID of the AntiTheftService to be found", required = true) @PathVariable(name = "UUID") UUID id) {
        AntiTheftServiceEntity antiTheftServiceEntity = antiTheftServiceService.selectAntiTheftService(id);
        AntiTheftServiceResponseV1 antiTheftServiceResponseV1 = AntiTheftServiceAdapter.adptFromAntiTheftServiceEntityToAntiTheftServiceResponseV1(antiTheftServiceEntity);
        return new ResponseEntity<>(antiTheftServiceResponseV1, HttpStatus.OK);
    }

    @GetMapping("/antitheftservice")
    @Transactional
    @ApiOperation(value = "Recover All AntiTheftServices", notes = "Returns all AntiTheftServices")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = AntiTheftServiceResponseV1.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<AntiTheftServiceResponseV1>> getAllAntiTheftService() {
        List<AntiTheftServiceEntity> antiTheftServiceEntityList = antiTheftServiceService.selectAllAntiTheftService();
        List<AntiTheftServiceResponseV1> antiTheftServiceResponseV1List = AntiTheftServiceAdapter.adptFromAntiTheftServiceEntityToAntiTheftServiceResponseV1List(antiTheftServiceEntityList);
        return new ResponseEntity(antiTheftServiceResponseV1List, HttpStatus.OK);
    }

    @DeleteMapping("/antitheftservice/{UUID}")
    @Transactional
    @ApiOperation(value = "Delete AntiTheftService", notes = "Delete a AntiTheftService using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = AntiTheftServiceResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<AntiTheftServiceResponseV1> deleteAntiTheftService(@ApiParam(value = "UUID of the AntiTheftService to be deleted", required = true) @PathVariable(name = "UUID") UUID id) {
        AntiTheftServiceResponseV1 antiTheftServiceResponseV1 = antiTheftServiceService.deleteAntiTheftService(id);
        return new ResponseEntity<>(antiTheftServiceResponseV1, HttpStatus.OK);
    }

    @PatchMapping("/antitheftservice/{UUID}/active")
    @Transactional
    @ApiOperation(value = "Patch active status AntiTheftService", notes = "Patch active status of AntiTheftService using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = AntiTheftServiceResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<AntiTheftServiceResponseV1> patchStatusAntiTheftService(
            @PathVariable UUID UUID
    ) {
        AntiTheftServiceEntity antiTheftServiceEntity = antiTheftServiceService.updateStatus(UUID);
        AntiTheftServiceResponseV1 responseV1 = AntiTheftServiceAdapter.adptFromAntiTheftServiceEntityToAntiTheftServiceResponseV1(antiTheftServiceEntity);
        return new ResponseEntity<>(responseV1, HttpStatus.OK);
    }

    @ApiOperation(value="Search AntiTheftService", notes = "It retrieves antitheftservice data through one of more parameters")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = PaginationResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @GetMapping(value="/antitheftservice/pagination", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<PaginationResponseV1<AntiTheftServiceResponseV1>> searchAntiTheftService(
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @RequestParam(value = "page_size", defaultValue = "20") Integer pageSize,
            @RequestParam(value = "order_by", required = false) String orderBy,
            @RequestParam(value = "asc", required = false, defaultValue = "true") Boolean asc,
            @RequestParam(value = "code_anti_theft_service", required = false) String codeAntiTheftService,
            @RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "business_name", required = false) String businessName,
            @RequestParam(value = "supplier_code", required = false) Integer supplierCode,
            @RequestParam(value = "email", required = false) String email,
            @RequestParam(value = "phone_number", required = false) String phoneNumber,
            @RequestParam(value = "provider_type", required = false) String providerType,
            @RequestParam(value = "time_out_in_seconds", required = false) Integer timeOutInSeconds,
            @RequestParam(value = "end_point_url", required = false) String endPointUrl,
            @RequestParam(value = "username", required = false) String username,
            @RequestParam(value = "password", required = false) String password,
            @RequestParam(value = "company_code", required = false) String companyCode,
            @RequestParam(value = "active", required = false) Boolean active,
            @RequestParam(value = "is_active", required = false) Boolean isActive,
            @RequestParam(value = "type", required = false) AntitheftTypeEnum type
            ){

        List<AntiTheftServiceEntity> listAntiTheftService = antiTheftServiceService.findAntiTheftServicesFiltered(page,pageSize, orderBy, asc, codeAntiTheftService, name, businessName, supplierCode, email, phoneNumber, providerType, timeOutInSeconds, endPointUrl, username, password, companyCode, active, isActive, type);
        Long itemCount = antiTheftServiceService.countAntiTheftServiceFiltered( codeAntiTheftService, name, businessName, supplierCode, email, phoneNumber, providerType, timeOutInSeconds, endPointUrl, username, password, companyCode, active, isActive, type);

        PaginationResponseV1<AntiTheftServiceResponseV1> paginationResponseV1 = AntiTheftServiceAdapter.adptPagination(listAntiTheftService, page, pageSize, itemCount);

        return new ResponseEntity<>(paginationResponseV1, HttpStatus.OK);
    }

}
