package com.doing.nemo.claims.controller.payload.response;

import com.doing.nemo.claims.controller.payload.response.damaged.*;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ContractInfoResponseV1 implements Serializable {

    @JsonProperty("driver")
    private DriverResponse driverResponse;

    @JsonProperty("contract")
    private ContractResponse contractResponse;

    @JsonProperty("vehicle")
    private VehicleResponse vehicleResponse;

    @JsonProperty("customer")
    private CustomerResponse customerResponse;

    @JsonProperty("fleet_managers")
    private List<FleetManagerResponseV1> fleetManagerResponseList;

    @JsonProperty("insurance_company")
    private InsuranceCompanyResponse insuranceCompany;

    @JsonProperty("registry")
    private List<RegistryResponseV1> registryResponseList;

    public DriverResponse getDriverResponse() {
        return driverResponse;
    }

    public void setDriverResponse(DriverResponse driverResponse) {
        this.driverResponse = driverResponse;
    }

    public ContractResponse getContractResponse() {
        return contractResponse;
    }

    public void setContractResponse(ContractResponse contractResponse) {
        this.contractResponse = contractResponse;
    }

    public VehicleResponse getVehicleResponse() {
        return vehicleResponse;
    }

    public void setVehicleResponse(VehicleResponse vehicleResponse) {
        this.vehicleResponse = vehicleResponse;
    }

    public CustomerResponse getCustomerResponse() {
        return customerResponse;
    }

    public void setCustomerResponse(CustomerResponse customerResponse) {
        this.customerResponse = customerResponse;
    }

    public List<FleetManagerResponseV1> getFleetManagerResponseList() {
        if(fleetManagerResponseList == null){
            return null;
        }
        return new ArrayList<>(fleetManagerResponseList);
    }

    public void setFleetManagerResponseList(List<FleetManagerResponseV1> fleetManagerResponseList) {
        if(fleetManagerResponseList != null)
        {
            this.fleetManagerResponseList =  new ArrayList<>(fleetManagerResponseList);
        } else {
            this.fleetManagerResponseList = null;
        }
    }

    public InsuranceCompanyResponse getInsuranceCompany() {
        return insuranceCompany;
    }

    public void setInsuranceCompany(InsuranceCompanyResponse insuranceCompany) {
        this.insuranceCompany = insuranceCompany;
    }

    public List<RegistryResponseV1> getRegistryResponseList() {
        if(registryResponseList == null){
            return null;
        }
        return new ArrayList<>(registryResponseList);
    }

    public void setRegistryResponseList(List<RegistryResponseV1> registryResponseList) {
        if(registryResponseList != null)
        {
            this.registryResponseList = new ArrayList<>(registryResponseList);
        } else {
            this.registryResponseList = null;
        }
    }

    @Override
    public String toString() {
        return "ContractInfoResponseV1{" +
                "driverResponse=" + driverResponse +
                ", contractResponse=" + contractResponse +
                ", vehicleResponse=" + vehicleResponse +
                ", customerResponse=" + customerResponse +
                ", fleetManagerResponseList=" + fleetManagerResponseList +
                ", insuranceCompany=" + insuranceCompany +
                ", registryResponseList=" + registryResponseList +
                '}';
    }
}
