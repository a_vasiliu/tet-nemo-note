package com.doing.nemo.claims.controller.payload.response.practice;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Date;

public class FindingResponseV1 implements Serializable {

    private static final long serialVersionUID = 809173064960583663L;

    @JsonProperty("finding_date")
    private Date findingDate;

    @JsonProperty("finding_id")
    private String findingId;

    @JsonProperty("sequestered")
    private Boolean sequestered;

    @JsonProperty("provider")
    private String provider;

    @JsonProperty("provider_code")
    private String providerCode;

    @JsonProperty("complaint_police")
    private Boolean complaintPolice;

    @JsonProperty("complaint_cc")
    private Boolean complaintCc;

    @JsonProperty("complaint_vvuu")
    private Boolean complaintVvuu;

    @JsonProperty("complaint_gdf")
    private Boolean complaintGdf;

    @JsonProperty("authority_data")
    private String authorityData;

    @JsonProperty("note")
    private String note;

    @JsonProperty("happened_abroad")
    private Boolean happenedAbroad;

    @JsonProperty("found_location")
    private String foundLocation;

    @JsonProperty("province")
    private String province;

    @JsonProperty("state")
    private String state;

    @JsonProperty("double_key")
    private Boolean doubleKey;


    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Date getFindingDate() {
        if(findingDate == null){
            return null;
        }
        return (Date)findingDate.clone();
    }

    public void setFindingDate(Date findingDate) {
        if(findingDate != null)
        {
            this.findingDate = (Date)findingDate.clone();
        } else {
            this.findingDate = null;
        }
    }

    public String getFindingId() {
        return findingId;
    }

    public void setFindingId(String findingId) {
        this.findingId = findingId;
    }

    public Boolean getSequestered() {
        return sequestered;
    }

    public void setSequestered(Boolean sequestered) {
        this.sequestered = sequestered;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getProviderCode() {
        return providerCode;
    }

    public void setProviderCode(String providerCode) {
        this.providerCode = providerCode;
    }

    public Boolean getComplaintPolice() {
        return complaintPolice;
    }

    public void setComplaintPolice(Boolean complaintPolice) {
        this.complaintPolice = complaintPolice;
    }

    public Boolean getComplaintCc() {
        return complaintCc;
    }

    public void setComplaintCc(Boolean complaintCc) {
        this.complaintCc = complaintCc;
    }

    public Boolean getComplaintVvuu() {
        return complaintVvuu;
    }

    public void setComplaintVvuu(Boolean complaintVvuu) {
        this.complaintVvuu = complaintVvuu;
    }

    public Boolean getComplaintGdf() {
        return complaintGdf;
    }

    public void setComplaintGdf(Boolean complaintGdf) {
        this.complaintGdf = complaintGdf;
    }

    public String getAuthorityData() {
        return authorityData;
    }

    public void setAuthorityData(String authorityData) {
        this.authorityData = authorityData;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Boolean getHappenedAbroad() {
        return happenedAbroad;
    }

    public void setHappenedAbroad(Boolean happenedAbroad) {
        this.happenedAbroad = happenedAbroad;
    }

    public String getFoundLocation() {
        return foundLocation;
    }

    public void setFoundLocation(String foundLocation) {
        this.foundLocation = foundLocation;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Boolean getDoubleKey() {
        return doubleKey;
    }

    public void setDoubleKey(Boolean doubleKey) {
        this.doubleKey = doubleKey;
    }

    @Override
    public String toString() {
        return "FindingResponseV1{" +
                "findingDate=" + findingDate +
                ", findingId='" + findingId + '\'' +
                ", sequestered=" + sequestered +
                ", provider='" + provider + '\'' +
                ", providerCode='" + providerCode + '\'' +
                ", complaintPolice=" + complaintPolice +
                ", complaintCc=" + complaintCc +
                ", complaintVvuu=" + complaintVvuu +
                ", complaintGdf=" + complaintGdf +
                ", authorityData='" + authorityData + '\'' +
                ", note='" + note + '\'' +
                ", happenedAbroad=" + happenedAbroad +
                ", foundLocation='" + foundLocation + '\'' +
                ", province='" + province + '\'' +
                ", state='" + state + '\'' +
                ", doubleKey=" + doubleKey +
                '}';
    }


}