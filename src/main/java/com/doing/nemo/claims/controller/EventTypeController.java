package com.doing.nemo.claims.controller;

import com.doing.nemo.claims.adapter.EventTypeAdapter;
import com.doing.nemo.claims.common.validation.MessageCode;
import com.doing.nemo.claims.common.validation.RequestValidator;
import com.doing.nemo.claims.controller.payload.request.EventTypeRequestV1;
import com.doing.nemo.claims.controller.payload.request.ListRequestV1;
import com.doing.nemo.claims.controller.payload.response.ClaimsResponsePaginationV1;
import com.doing.nemo.claims.controller.payload.response.EventTypeResponseIdV1;
import com.doing.nemo.claims.controller.payload.response.EventTypeResponseV1;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.enumerated.ClaimsRepairEnum;
import com.doing.nemo.claims.entity.settings.EventTypeEntity;
import com.doing.nemo.claims.service.EventTypeService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/v1")
public class EventTypeController {

    @Autowired
    private EventTypeService eventTypeService;

    @Autowired
    private RequestValidator requestValidator;

    @PostMapping(value = "/event",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Insert Event TypeEnum", notes = "Insert event type using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = EventTypeResponseIdV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @Transactional
    public ResponseEntity<EventTypeResponseIdV1> postInsertEventType(
            @ApiParam(value = "Body of the Event TypeEnum to be created", required = true)
            @RequestBody EventTypeRequestV1 eventTypeRequest,
            @ApiParam(value = "TypeEnum of complaint", required = true)
            @RequestParam(value = "type_complaint") ClaimsRepairEnum typeComplaint) {

        requestValidator.validateRequest(eventTypeRequest, MessageCode.E00X_1000);
        EventTypeEntity eventTypeEntity = EventTypeAdapter.getEventTypeEntity(eventTypeRequest);
        eventTypeEntity.setTypeComplaint(typeComplaint);
        eventTypeEntity = eventTypeService.insertEventType(eventTypeEntity);
        EventTypeResponseIdV1 response = new EventTypeResponseIdV1(eventTypeEntity.getId());
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @PostMapping("/events")
    @Transactional
    @ApiOperation(value = "Insert a list of Events", notes = "Insert a list of events using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = EventTypeResponseV1.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<EventTypeResponseV1>> insertEvents(
            @ApiParam(value = "List of the bodies of the Events to be created", required = true)
            @RequestBody ListRequestV1<EventTypeRequestV1> eventTypeListRequestV1,
            @ApiParam(value = "TypeEnum of complaint", required = true)
            @RequestParam(value = "type_complaint") ClaimsRepairEnum typeComplaint) {

        List<EventTypeResponseV1> eventTypeResponseV1List = new ArrayList<>();
        for (EventTypeRequestV1 eventTypeRequestV1 : eventTypeListRequestV1.getData()) {
            requestValidator.validateRequest(eventTypeRequestV1, MessageCode.E00X_1000);
            EventTypeEntity eventTypeEntity = EventTypeAdapter.getEventTypeEntity(eventTypeRequestV1);
            eventTypeEntity.setTypeComplaint(typeComplaint);
            eventTypeEntity = eventTypeService.insertEventType(eventTypeEntity);
            eventTypeResponseV1List.add(EventTypeAdapter.getEventTypeAdapter(eventTypeEntity));
        }
        return new ResponseEntity<>(eventTypeResponseV1List, HttpStatus.CREATED);

    }

    @PutMapping(value = "/event/{UUID}",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Upload Event TypeEnum", notes = "Upload event type using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = EventTypeResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @Transactional
    public ResponseEntity<EventTypeResponseV1> putEventType(
            @ApiParam(value = "UUID that identifies the Event TypeEnum to be modified", required = true)
            @PathVariable(name = "UUID") UUID uuid,
            @ApiParam(value = "Updated body of the Event TypeEnum", required = true)
            @RequestBody EventTypeRequestV1 eventTypeRequest) {

        requestValidator.validateRequest(eventTypeRequest, MessageCode.E00X_1000);
        EventTypeEntity eventTypeEntity = EventTypeAdapter.getEventTypeEntity(eventTypeRequest);
        eventTypeEntity = eventTypeService.updateEventType(eventTypeEntity, uuid);
        EventTypeResponseV1 response = EventTypeAdapter.getEventTypeAdapter(eventTypeEntity);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping(value = "/event/{UUID}",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Recover Event TypeEnum", notes = "Returns an Event TypeEnum using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = EventTypeResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<EventTypeResponseV1> getEventType(
            @ApiParam(value = "UUID of the Event TypeEnum to be found", required = true)
            @PathVariable(name = "UUID") UUID uuid) {

        EventTypeEntity eventTypeEntity = eventTypeService.getEventType(uuid);
        EventTypeResponseV1 response = EventTypeAdapter.getEventTypeAdapter(eventTypeEntity);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping(value = "/event",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Recover Event Types", notes = "Returns all the Event Types")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = EventTypeResponseV1.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<EventTypeResponseV1>> getAllEventType(
            @ApiParam(value = "TypeEnum of complaint", required = true)
            @RequestParam(value = "type_complaint") ClaimsRepairEnum typeComplaint
    ) {

        List<EventTypeEntity> eventTypeEntities = eventTypeService.getAllEventType(typeComplaint);
        List<EventTypeResponseV1> response = EventTypeAdapter.getEventTypeListAdapter(eventTypeEntities);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @DeleteMapping(value = "/event/{UUID}",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Delete Event TypeEnum", notes = "Delete an Event TypeEnum using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = EventTypeResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @Transactional
    public ResponseEntity deleteEventType(@PathVariable(name = "UUID") UUID uuid) {

        EventTypeEntity eventTypeEntity = eventTypeService.deleteEventType(uuid);
        if (eventTypeEntity == null)
            return new ResponseEntity<>(eventTypeEntity, HttpStatus.OK);
        EventTypeResponseV1 response = EventTypeAdapter.getEventTypeAdapter(eventTypeEntity);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PatchMapping("/event/{UUID}/active")
    @Transactional
    @ApiOperation(value = "Patch status active Email Template", notes = "Patch status active Email Template using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = EventTypeResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<EventTypeResponseV1> patchStatusEventType(
            @PathVariable UUID UUID
    ) {
        EventTypeEntity eventTypeEntity = eventTypeService.patchStatusEventType(UUID);
        EventTypeResponseV1 responseV1 = EventTypeAdapter.getEventTypeAdapter(eventTypeEntity);
        return new ResponseEntity<>(responseV1, HttpStatus.OK);
    }

    @ApiOperation(value="EventType Pagination", notes = "It retrieves EventType pagination")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = ClaimsResponsePaginationV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @GetMapping(value="/eventtypes/pagination", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<ClaimsResponsePaginationV1> eventTypePagination(
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @RequestParam(value = "page_size", defaultValue = "20") Integer pageSize,
            @RequestParam(value = "type_complaint", required = false) String claimsRepairEnum,
            @RequestParam(value = "order_by", required = false) String orderBy,
            @RequestParam(value = "description", required = false) String description,
            @RequestParam(value = "event_type", required = false) String eventType,
            @RequestParam(value = "create_attachments", required = false) Boolean createAttachments,
            @RequestParam(value = "catalog", required = false) String catalog,
            @RequestParam(value = "attachments_type", required = false) String attachmentsType,
            @RequestParam(value = "only_last", required = false) Boolean onlyLast,
            @RequestParam(value = "is_active", required = false) Boolean isActive,
            @RequestParam(value = "asc", required = false, defaultValue = "true") Boolean asc
            ){

        Pagination<EventTypeEntity> eventTypeEntityPagination = eventTypeService.paginationEventType(page, pageSize, claimsRepairEnum, orderBy, description, eventType, createAttachments, catalog, attachmentsType, onlyLast, isActive, asc);

        return new ResponseEntity<>(EventTypeAdapter.adptEventTypePaginationToClaimsResponsePagination(eventTypeEntityPagination), HttpStatus.OK);
    }
}
