package com.doing.nemo.claims.controller;

import com.doing.nemo.claims.adapter.ClaimsAdapter;
import com.doing.nemo.claims.adapter.CounterpartyAdapter;
import com.doing.nemo.claims.adapter.HistoricalCounterpartyAdapter;
import com.doing.nemo.claims.command.CounterpartyPatchCommand;
import com.doing.nemo.claims.command.CounterpartyPatchStatusRepairCommand;
import com.doing.nemo.claims.command.CounterpartyUpdateCommand;
import com.doing.nemo.claims.common.commandBus.Command;
import com.doing.nemo.claims.common.commandBus.CommandBus;
import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.AssignationRequestV1;
import com.doing.nemo.claims.controller.payload.request.CounterpartyPatchRequestV1;
import com.doing.nemo.claims.controller.payload.request.CounterpartyPatchStatusRequestV1;
import com.doing.nemo.claims.controller.payload.request.CounterpartyProcedureRequestV1;
import com.doing.nemo.claims.controller.payload.response.*;
import com.doing.nemo.claims.entity.*;
import com.doing.nemo.claims.entity.enumerated.DTO.CounterpartyStats;
import com.doing.nemo.claims.entity.enumerated.RepairEnum.RepairProcedureEnum;
import com.doing.nemo.claims.entity.enumerated.RepairEnum.RepairStatusEnum;
import com.doing.nemo.claims.entity.jsonb.forms.jsonbForms.Attachment;
import com.doing.nemo.claims.service.ConverterClaimsService;
import com.doing.nemo.claims.service.CounterpartyService;
import com.doing.nemo.claims.service.DogeEnquService;
import com.doing.nemo.claims.service.LockService;
import com.doing.nemo.claims.util.Util;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.NotFoundException;
import io.swagger.annotations.*;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/v1")
@Api("Counterparty")
public class CounterpartyController {

    private static Logger LOGGER = LoggerFactory.getLogger(CounterpartyController.class);
    @Autowired
    private CounterpartyAdapter counterpartyAdapter;
    @Autowired
    private CounterpartyService counterpartyService;
    @Autowired
    private LockService lockService;
    @Autowired
    private CommandBus commandBus;

    //REFACTOR
    /*RECUPERO CONTROPARTE TRAMITE ID*/
    @GetMapping(value = "/claims/counterparty/{UUID_CLAIMS}/{UUID_COUNTERPARTY}",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = CounterpartyResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<CounterpartyResponseV1> getCounterpartyById(
            @ApiParam(value = "cliams_id", required = true)
            @PathVariable(name = "UUID_CLAIMS") String claimsId,
            @ApiParam(value = "counterparty_id", required = true)
            @PathVariable(name = "UUID_COUNTERPARTY") String counterpartyId
    ){
        CounterpartyResponseV1 counterpartyResponseV1 = CounterpartyAdapter.adptFromCounterpartyToCounterpartyResponseV1(counterpartyService.findById(counterpartyId));
        return new ResponseEntity<>(counterpartyResponseV1, HttpStatus.OK);
    }

    //REFACTOR
    /*PAGINAZIONE CONTROPARTI*/
    @GetMapping(value = "/claims/counterparty/adminsupervisor",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = CounterpartyResponsePaginationV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<CounterpartyResponsePaginationV1> getRepairPaginationAdminSupervisor(
            @ApiParam(value = "Defines how many Counterparty can contain the single page", required = false)
            @RequestParam(value = "page_size", defaultValue = "20", required = false) Integer pageSize,
            @ApiParam(value = "Defines the page number to be displayed", required = false)
            @RequestParam(value = "page", defaultValue = "1", required = false) Integer page,
            @ApiParam(value = "Filter the search for company name", required = false)
            @RequestParam(name = "denomination", required = false) String denomination,
            @ApiParam(value = "Filter the search for plate", required = false)
            @RequestParam(name = "plate", required = false) String plate,
            @ApiParam(value = "Filter the search for repair status ", required = false)
            @RequestParam(name = "status_repair", required = false) String statusRepair,
            @ApiParam(value = "Filter the search by the date the Claim was created from", required = false)
            @RequestParam(value = "date_created_from", required = false) String dateCreatedFrom,
            @ApiParam(value = "Filter the search by the date the Claim was created to", required = false)
            @RequestParam(value = "date_created_to", required = false) String dateCreatedTo,
            @ApiParam(value = "Filter the search by practice_id", required = false)
            @RequestParam(value = "practice_id_counterparty", required = false) Long practiceIdCounterparty,
            @ApiParam(value = "Order", required = false)
            @RequestParam(value = "asc", required = false, defaultValue = "true") Boolean asc,
            @ApiParam(value = "OrderBy", required = false)
            @RequestParam(value = "order_by", defaultValue = "created_at", required = false) String orderBy,
            @ApiParam(value = "Filter the search by address", required = false)
            @RequestParam(value = "address", required = false) String address,
            @ApiParam(value = "Filter the search by region", required = false)
            @RequestParam(value = "region", required = false) String region,
            @ApiParam(value = "Filter the search by province", required = false)
            @RequestParam(value = "province", required = false) String province,
            @ApiParam(value = "Filter the search by number sx", required = false)
            @RequestParam(value = "number_sx", required = false) String numberSx,
            @ApiParam(value = "Filter the search by assignedTo", required = false)
            @RequestParam(value = "assigned_to", required = false) String assignedTo,
            @ApiParam(value = "Filter the search by last call", required = false)
            @RequestParam(value = "last_call", required = false) String lastCall,
            @ApiParam(value = "Filter the search by date accident from", required = false)
            @RequestParam(value = "date_accident_from", required = false) String dateAccidentFrom,
            @ApiParam(value = "Filter the search by date accident to", required = false)
            @RequestParam(value = "date_accident_to", required = false) String dateAccidentTo,
            @ApiParam(value = "Filter the search by city", required = false)
            @RequestParam(value = "locality", required = false) String locality,
            @ApiParam(value = "Filter the search by recall", required = false)
            @RequestParam(value = "recall", required = false) String recall,
            @ApiParam(value = "Filter the search by email center", required = false)
            @RequestParam(value = "center_email", required = false) String centerEmail

    ) {
            List<CounterpartyEntity> counterpartyObj = counterpartyService.getCounterpartyPaginationFromDB(denomination, plate, practiceIdCounterparty, statusRepair, dateCreatedFrom, dateCreatedTo, asc, orderBy, page, pageSize, address, numberSx, dateAccidentFrom, dateAccidentTo, region, province, lastCall, assignedTo, locality, recall,centerEmail, false);
            BigInteger count = counterpartyService.countCounterpartyPagination(denomination, plate, practiceIdCounterparty, statusRepair, dateCreatedFrom, dateCreatedTo, asc, orderBy, page, pageSize, address, numberSx, dateAccidentFrom, dateAccidentTo, region, province, lastCall, assignedTo, locality, recall, centerEmail,false);
            //conver from new to old entity
            List<CounterpartyResponseV1> counterpartyResponseV1s = counterpartyAdapter.adptCounterpartsToCounterpartyResponseV1List(counterpartyObj);
            Pagination<CounterpartyResponseV1> pagination = counterpartyService.generateCounterpartyPagination(counterpartyResponseV1s, count, page, pageSize);
            return new ResponseEntity<>(CounterpartyAdapter.adptCounterpartyPaginationToCounterpartyPaginationResponse(pagination), HttpStatus.OK);

    }

    //REFACTOR
    /*PAGINAZIONE CONTROPARTI SENZA TO_SORT*/
    @GetMapping(value = "/claims/counterparty/operator",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = CounterpartyResponsePaginationV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<CounterpartyResponsePaginationV1> getRepairPaginationAdminSupervisorWithoutToSort(
            @ApiParam(value = "Defines how many Counterparty can contain the single page", required = false)
            @RequestParam(value = "page_size", defaultValue = "20", required = false) Integer pageSize,
            @ApiParam(value = "Defines the page number to be displayed", required = false)
            @RequestParam(value = "page", defaultValue = "1", required = false) Integer page,
            @ApiParam(value = "Filter the search for company name", required = false)
            @RequestParam(name = "denomination", required = false) String denomination,
            @ApiParam(value = "Filter the search for plate", required = false)
            @RequestParam(name = "plate", required = false) String plate,
            @ApiParam(value = "Filter the search for repair status ", required = false)
            @RequestParam(name = "status_repair", required = false) String statusRepair,
            @ApiParam(value = "Filter the search by the date the Claim was created from", required = false)
            @RequestParam(value = "date_created_from", required = false) String dateCreatedFrom,
            @ApiParam(value = "Filter the search by the date the Claim was created to", required = false)
            @RequestParam(value = "date_created_to", required = false) String dateCreatedTo,
            @ApiParam(value = "Filter the search by practice_id", required = false)
            @RequestParam(value = "practice_id_counterparty", required = false) Long practiceIdCounterparty,
            @ApiParam(value = "Order", required = false)
            @RequestParam(value = "asc", required = false, defaultValue = "true") Boolean asc,
            @ApiParam(value = "OrderBy", required = false)
            @RequestParam(value = "order_by", defaultValue = "created_at", required = false) String orderBy,
            @ApiParam(value = "Filter the search by address", required = false)
            @RequestParam(value = "address", required = false) String address,
            @ApiParam(value = "Filter the search by region", required = false)
            @RequestParam(value = "region", required = false) String region,
            @ApiParam(value = "Filter the search by province", required = false)
            @RequestParam(value = "province", required = false) String province,
            @ApiParam(value = "Filter the search by number sx", required = false)
            @RequestParam(value = "number_sx", required = false) String numberSx,
            //@ApiParam(value = "Filter the search by assignedTo", required = false)
            @ApiParam(value = "Filter the search by last call", required = false)
            @RequestParam(value = "last_call", required = false) String lastCall,
            @ApiParam(value = "Filter the search by date accident from", required = false)
            @RequestParam(value = "date_accident_from", required = false) String dateAccidentFrom,
            @ApiParam(value = "Filter the search by date accident to", required = false)
            @RequestParam(value = "date_accident_to", required = false) String dateAccidentTo,
            @ApiParam(value = "Filter the search by city", required = false)
            @RequestParam(value = "locality", required = false) String locality,
            @ApiParam(value = "Filter the search by recall", required = false)
            @RequestParam(value = "recall", required = false) String recall,
            @RequestHeader(value = "nemo-user-id", required = true) String userId
    ) {

            List<CounterpartyEntity> counterpartyObj = counterpartyService.getCounterpartyPaginationWithoutToSort(denomination, plate, practiceIdCounterparty, statusRepair, dateCreatedFrom, dateCreatedTo, asc, orderBy, page, pageSize, address, numberSx, dateAccidentFrom, dateAccidentTo, region, province, lastCall, userId, locality, recall);
            BigInteger count = counterpartyService.countCounterpartyPaginationWithoutToSort(denomination, plate, practiceIdCounterparty, statusRepair, dateCreatedFrom, dateCreatedTo, asc, orderBy, page, pageSize, address, numberSx, dateAccidentFrom, dateAccidentTo, region, province, lastCall, userId, locality, recall);
            //convert from new to old entity
            List<CounterpartyResponseV1> counterpartyResponseV1s = counterpartyAdapter.adptCounterpartsToCounterpartyResponseV1List(counterpartyObj);
            Pagination<CounterpartyResponseV1> pagination = counterpartyService.generateCounterpartyPagination(counterpartyResponseV1s, count, page, pageSize);
            return new ResponseEntity<>(CounterpartyAdapter.adptCounterpartyPaginationToCounterpartyPaginationResponse(pagination), HttpStatus.OK);


    }

    //REFACTOR
    /*CAMBIO STATO DELLA CONTROPARTE*/
    @PatchMapping(value = "/claims/counterparty/{UUID_CLAIMS}/{UUID_COUNTERPARTY}/{REPAIR_STATUS}",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = CounterpartyResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<CounterpartyResponseV1> patchStatusRepair(
            @ApiParam(value = "UUID of the Claim", required = true)
            @PathVariable("UUID_CLAIMS") String claimsId,
            @ApiParam(value = "UUID of the Counterparty within the selected  Claim", required = true)
            @PathVariable("UUID_COUNTERPARTY") String counterpartyId,
            @ApiParam(value = "The Repair State in which the Counterparty must go", required = true)
            @PathVariable("REPAIR_STATUS") String statusRepair,
            @ApiParam(value = "TypeEnum of User who calls the service", required = true)
            @RequestHeader(name = "nemo-user-id") String user,
            @ApiParam(value = "User name who calls the service", required = true)
            @RequestHeader(name = "nemo-user-first-name") String userNameFirst,
            @ApiParam(value = "User name who calls the service", required = true)
            @RequestHeader(name = "nemo-user-last-name") String userLastName,
            @RequestHeader(name = "nemo-profiles-tree") String profiles,
            @ApiParam(value = "Body of the email to send and description of the change status", required = true)
            @RequestBody CounterpartyPatchStatusRequestV1 counterpartyPatchStatusRequestV1
    ) throws IOException {
        String userName = userNameFirst + " " + userLastName;
        Command command = new CounterpartyPatchStatusRepairCommand(
                claimsId,
                counterpartyId,
                user,
                userNameFirst,
                RepairStatusEnum.create(Util.replaceStringForEnumValue(statusRepair)),
                counterpartyPatchStatusRequestV1,
                userLastName,
                profiles
        );

        commandBus.execute(command);
        CounterpartyResponseV1 counterpartyResponseV1 = CounterpartyAdapter.adptFromCounterpartyToCounterpartyResponseV1(counterpartyService.findById(counterpartyId));
        return new ResponseEntity<>(counterpartyResponseV1, HttpStatus.OK);

    }

    //REFACTOR - TO TEST
    /*STATISTICHE COUNTERPARTY REPAIR*/
    @ApiOperation(value = "Counterparty repair Statistics", notes = "Provides stats of counterparty data")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = StatsResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @GetMapping(value = "/claims/counterparty/stats", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<StatsResponseV1<CounterpartyStats>> getCounterpartyStats(
            @RequestParam(value = "filter_assigned",required = false) Boolean filterAssigned,
            @RequestHeader(name = "nemo-user-id") String userId
    ) {

        StatsResponseV1<CounterpartyStats> statsResponseV1 = null;
        if(filterAssigned != null && filterAssigned) {
            statsResponseV1 = new StatsResponseV1<>(counterpartyService.getClaimsCounterpartyStats(userId));
        }else {
            statsResponseV1 = new StatsResponseV1<>(counterpartyService.getClaimsCounterpartyStats(null));
        }
        return new ResponseEntity<>(statsResponseV1, HttpStatus.OK);

    }

    //REFACTOR - TO TEST
    @PatchMapping(value = "/counterparty/{UUID_CLAIMS}/{UUID_COUNTERPARTY}",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Upload a part of the Counterparty", notes = "Update a part of the Counterparty using the info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = CounterpartyResponse.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<CounterpartyResponse> patchCounterparty(
            @ApiParam(value = "UUID of the Claim", required = true)
            @PathVariable(name = "UUID_CLAIMS") String claimsId,
            @ApiParam(value = "UUID of the Counterparty", required = true)
            @PathVariable(name = "UUID_COUNTERPARTY") String counterpartyId,
            @ApiParam(value = "Id of the user who calls the service", required = true)
            @RequestHeader(name = "nemo-user-id") String user,
            @ApiParam(value = "Some attributes of the body of the Counterparty to be modified", required = true)
            @RequestBody CounterpartyPatchRequestV1 counterpartyPatchRequestV1) throws IOException {

        lockService.checkLock(user, counterpartyId);
        Command counterpartyPatchCommand = new CounterpartyPatchCommand(
                claimsId,
                counterpartyId,
                counterpartyPatchRequestV1.getType(),
                counterpartyPatchRequestV1.getRepairProcedure(),
                user,
                counterpartyPatchRequestV1.getAssignedTo(),
                DateUtil.convertIS08601StringToUTCDate(counterpartyPatchRequestV1.getAssignedAt()),
                counterpartyPatchRequestV1.getInsured(),
                counterpartyPatchRequestV1.getInsuranceCompany(),
                counterpartyPatchRequestV1.getDriver(),
                counterpartyPatchRequestV1.getVehicle(),
                counterpartyPatchRequestV1.getCaiSigned(),
                counterpartyPatchRequestV1.getImpactPoint(),
                counterpartyPatchRequestV1.getResponsible(),
                counterpartyPatchRequestV1.getEligibility(),
                counterpartyPatchRequestV1.getDescription(),
                counterpartyPatchRequestV1.getLastContact(),
                counterpartyPatchRequestV1.getManager(),
                counterpartyPatchRequestV1.getCanalization(),
                counterpartyPatchRequestV1.getPolicyNumber(),
                counterpartyPatchRequestV1.getPolicyBeginningValidity(),
                counterpartyPatchRequestV1.getPolicyEndValidity(),
                counterpartyPatchRequestV1.getManagementType(),
                counterpartyPatchRequestV1.getAskedForDamages(),
                counterpartyPatchRequestV1.getLegalOrConsultant(),
                counterpartyPatchRequestV1.getDateRequestDamages(),
                counterpartyPatchRequestV1.getExpirationDate(),
                counterpartyPatchRequestV1.getLegal(),
                counterpartyPatchRequestV1.getEmailLegal(),
                counterpartyPatchRequestV1.getAld(),
                counterpartyPatchRequestV1.getReplacementCar(),
                counterpartyPatchRequestV1.getReplacementPlate(),
                counterpartyPatchRequestV1.getIsCompleteDocumentation()
        );
        commandBus.execute(counterpartyPatchCommand);
        return new ResponseEntity<>(CounterpartyAdapter.adptCounterpartyToCounterpartyResponse(counterpartyService.findById(counterpartyId)), HttpStatus.OK);
    }

    //REFACTOR
    @PutMapping(value = "/counterparty/{UUID_CLAIMS}/{UUID_COUNTERPARTY}",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Upload Counterparty", notes = "Update Counterparty using the info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = CounterpartyResponse.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<CounterpartyResponse> putCounterparty(
            @ApiParam(value = "UUID of the Claim", required = true)
            @PathVariable(name = "UUID_CLAIMS") String claimsId,
            @ApiParam(value = "UUID of the Counterparty", required = true)
            @PathVariable(name = "UUID_COUNTERPARTY") String counterpartyId,
            @ApiParam(value = "Id of the user who calls the service", required = true)
            @RequestHeader(name = "nemo-user-id") String userId,
            @RequestHeader(name = "nemo-user-first-name") String userName,
            @RequestHeader(name = "nemo-user-last-name") String lastName,
            @ApiParam(value = "Some attributes of the body of the Counterparty to be modified", required = true)
            @RequestBody CounterpartyPatchRequestV1 counterpartyPatchRequestV1) throws IOException {

        lockService.checkLock(userId, counterpartyId);
        Command counterpartyUpdateCommand = new CounterpartyUpdateCommand(
                claimsId,
                counterpartyId,
                counterpartyPatchRequestV1.getType(),
                counterpartyPatchRequestV1.getRepairProcedure(),
                userId,
                userName,
                lastName,
                counterpartyPatchRequestV1.getAssignedTo(),
                counterpartyPatchRequestV1.getAssignedAt(),
                counterpartyPatchRequestV1.getInsured(),
                counterpartyPatchRequestV1.getInsuranceCompany(),
                counterpartyPatchRequestV1.getDriver(),
                counterpartyPatchRequestV1.getVehicle(),
                counterpartyPatchRequestV1.getCaiSigned(),
                counterpartyPatchRequestV1.getImpactPoint(),
                counterpartyPatchRequestV1.getResponsible(),
                counterpartyPatchRequestV1.getEligibility(),
                counterpartyPatchRequestV1.getDescription(),
                counterpartyPatchRequestV1.getLastContact(),
                counterpartyPatchRequestV1.getManager(),
                counterpartyPatchRequestV1.getCanalization(),
                counterpartyPatchRequestV1.getPolicyNumber(),
                counterpartyPatchRequestV1.getPolicyBeginningValidity(),
                counterpartyPatchRequestV1.getPolicyEndValidity(),
                counterpartyPatchRequestV1.getManagementType(),
                counterpartyPatchRequestV1.getAskedForDamages(),
                counterpartyPatchRequestV1.getLegalOrConsultant(),
                counterpartyPatchRequestV1.getDateRequestDamages(),
                counterpartyPatchRequestV1.getExpirationDate(),
                counterpartyPatchRequestV1.getLegal(),
                counterpartyPatchRequestV1.getEmailLegal(),
                counterpartyPatchRequestV1.getAld(),
                counterpartyPatchRequestV1.getReplacementCar(),
                counterpartyPatchRequestV1.getReplacementPlate(),
                counterpartyPatchRequestV1.getIsCompleteDocumentation()
        );
        commandBus.execute(counterpartyUpdateCommand);
        return new ResponseEntity<>(CounterpartyAdapter.adptCounterpartyToCounterpartyResponse(counterpartyService.findById(counterpartyId)), HttpStatus.OK);
    }

    //REFACTOR
    @PatchMapping(value = "/counterparty/assignation",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Assignation Counteparty", notes = "Assign counteparty to user passed into the body")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = CounterpartyResponse.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity patchCounterpartyAssignation(
            @ApiParam(value = "Id of the user who calls the service", required = true)
            @RequestHeader(name = "nemo-user-id") String userid,
            @RequestHeader(name = "nemo-user-first-name") String userNameFirst,
            @RequestHeader(name = "nemo-user-last-name") String userLastName,
            @ApiParam(value = "Some attributes of the body of the Counterparty to be modified", required = true)
            @RequestBody List<AssignationRequestV1> assignationRequestV1s) {

        counterpartyService.assignsCounterparty(assignationRequestV1s, userid, userNameFirst + " " + userLastName);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    //REFACTOR
    /*-----------------CAMBIO STATO HIDDEN ALLEGATI CONTROPARTE--------------------*/
    @PatchMapping("/attachment/{ID_CLAIMS}/{ID_COUNTERPART}/{ID_ATTACHMENT}/hidden")
    @Transactional
    @ApiOperation(value = "Patch status hidden for attachment", notes = "Patch status hidden attachment of claims identified by ID_CLAIMS using the ID_ATTACHMENT passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ClaimsResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ClaimsResponseV1> patchStatusHiddenAttachmentCounterpart(
            @PathVariable("ID_CLAIMS") String idClaims,
            @PathVariable("ID_COUNTERPART") String idCounterpart,
            @PathVariable("ID_ATTACHMENT") String idAttachments,
            @ApiParam(value = "user id", required = true)
            @RequestHeader(name = "nemo-user-id") String userId
    ) {
        lockService.checkLock(userId, idCounterpart);
        return new ResponseEntity<>(ClaimsAdapter.adptClaimsToClaimsResponse(counterpartyService.patchStatusHiddenAttachmentCounterpart(idCounterpart,idAttachments)), HttpStatus.OK);
    }

    //REFACTOR
    @PatchMapping(value = "/counterparty/procedure/{COUNTERPARTY_ID}/{PROCEDURE}",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Assignation Counteparty", notes = "Assign counteparty to user passed into the body")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = CounterpartyResponse.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<CounterpartyResponse> patchCounterpartyAssignation(
            @ApiParam(value = "Id of the user who calls the service", required = true)
            @RequestHeader(name = "nemo-user-id") String user,
            @ApiParam(value = "Some attributes of the body of the Counterparty to be modified", required = true)
            @PathVariable(value = "PROCEDURE") RepairProcedureEnum procedureEnum,
            @PathVariable(value = "COUNTERPARTY_ID") String counterpartyId,
            @RequestBody CounterpartyProcedureRequestV1 motivationProcedure) {

        CounterpartyEntity counterpartyEntity = counterpartyService.patchProcedureRepair(procedureEnum, counterpartyId, motivationProcedure);
        return new ResponseEntity<>(CounterpartyAdapter.adptCounterpartyToCounterpartyResponse(counterpartyEntity), HttpStatus.OK);
    }

    /*-------------------------------------STORICO----------------------------------------------*/
    /*STORICO CAMBI DI STATO counterparty */
    @GetMapping(value = "/counterparty/historical/{UUID_COUNTERPARTY}",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = HistoricalResponseV1.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<HistoricalCounterPartyResponseV1> getClaimsHistorical(
            @ApiParam(value = "UUID of the Counterparty", required = true)
            @PathVariable("UUID_COUNTERPARTY") String counterpartyId
    )  {

        return new ResponseEntity<>(new HistoricalCounterPartyResponseV1(HistoricalCounterpartyAdapter.adptHistoricalCounterpartyToHistoricalCounterpartyResponse(counterpartyService.getCounterpartyHistorical(counterpartyId))), HttpStatus.OK);

    }
}
