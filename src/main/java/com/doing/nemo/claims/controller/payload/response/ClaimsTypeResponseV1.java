package com.doing.nemo.claims.controller.payload.response;

import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.UUID;

public class ClaimsTypeResponseV1 implements Serializable {

    @JsonProperty("id")
    private UUID id;

    @JsonProperty("type")
    private String type;

    @JsonProperty("claims_accident_type")
    private DataAccidentTypeAccidentEnum claimsAccidentType;

    public ClaimsTypeResponseV1() {
    }

    public ClaimsTypeResponseV1(UUID id, String type, DataAccidentTypeAccidentEnum claimsAccidentType) {
        this.id = id;
        this.type = type;
        this.claimsAccidentType = claimsAccidentType;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public DataAccidentTypeAccidentEnum getClaimsAccidentType() {
        return claimsAccidentType;
    }

    public void setClaimsAccidentType(DataAccidentTypeAccidentEnum claimsAccidentType) {
        this.claimsAccidentType = claimsAccidentType;
    }

    @Override
    public String toString() {
        return "ClaimsTypeResponseV1{" +
                "id=" + id +
                ", type='" + type + '\'' +
                ", claimsAccidentType=" + claimsAccidentType +
                '}';
    }
}
