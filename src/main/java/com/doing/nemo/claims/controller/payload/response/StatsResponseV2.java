package com.doing.nemo.claims.controller.payload.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonIgnoreProperties(ignoreUnknown = true)
public class StatsResponseV2 {

    @JsonProperty(value = "current_page")
    private Integer currentPage;

    @JsonProperty(value = "item_count")
    private Integer itemCount;

    @JsonProperty(value = "page_size")
    private Integer pageSize;

    @JsonProperty(value = "page_count")
    private Integer pageCount;

    public StatsResponseV2() {
    }

    public StatsResponseV2(Integer currentPage, Integer itemCount, Integer pageSize, Integer pageCount) {
        this.currentPage = currentPage;
        this.itemCount = itemCount;
        this.pageSize = pageSize;
        this.pageCount = pageCount;
    }

    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

    public Integer getItemCount() {
        return itemCount;
    }

    public void setItemCount(Integer itemCount) {
        this.itemCount = itemCount;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getPageCount() {
        return pageCount;
    }

    public void setPageCount(Integer pageCount) {
        this.pageCount = pageCount;
    }
}
