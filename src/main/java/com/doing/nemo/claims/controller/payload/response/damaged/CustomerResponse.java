package com.doing.nemo.claims.controller.payload.response.damaged;

import com.doing.nemo.claims.entity.jsonb.Address;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class CustomerResponse implements Serializable {

    @JsonProperty("customer_id")
    private String customerId;

    @JsonProperty("trading_name")
    private String tradingName;

    @JsonProperty("legal_name")
    private String legalName;

    @JsonProperty("status")
    private String status;

    @JsonProperty("vat_number")
    private String vatNumber;

    @JsonProperty("official_registration")
    private String officialRegistration;

    @JsonProperty("email")
    private String email;

    @JsonProperty("phonenr")
    private String phonenr;

    @JsonProperty("pec")
    private String pec;

    @JsonProperty("main_address")
    private Address mainAddress;

    public CustomerResponse() {
    }

    public CustomerResponse(String customerId, String tradingName, String legalName, String status, String vatNumber, String officialRegistration, String email, String phonenr, String pec, Address mainAddress) {
        this.customerId = customerId;
        this.tradingName = tradingName;
        this.legalName = legalName;
        this.status = status;
        this.vatNumber = vatNumber;
        this.officialRegistration = officialRegistration;
        this.email = email;
        this.phonenr = phonenr;
        this.pec = pec;
        this.mainAddress = mainAddress;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getTradingName() {
        return tradingName;
    }

    public void setTradingName(String tradingName) {
        this.tradingName = tradingName;
    }

    public String getLegalName() {
        return legalName;
    }

    public void setLegalName(String legalName) {
        this.legalName = legalName;
    }

    public String getVatNumber() {
        return vatNumber;
    }

    public void setVatNumber(String vatNumber) {
        this.vatNumber = vatNumber;
    }

    public String getOfficialRegistration() {
        return officialRegistration;
    }

    public void setOfficialRegistration(String officialRegistration) {
        this.officialRegistration = officialRegistration;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhonenr() {
        return phonenr;
    }

    public void setPhonenr(String phonenr) {
        this.phonenr = phonenr;
    }

    public String getPec() {
        return pec;
    }

    public void setPec(String pec) {
        this.pec = pec;
    }

    public Address getMainAddress() {
        return mainAddress;
    }

    public void setMainAddress(Address mainAddress) {
        this.mainAddress = mainAddress;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "customerId='" + customerId + '\'' +
                ", tradingName='" + tradingName + '\'' +
                ", legalName='" + legalName + '\'' +
                ", vatNumber='" + vatNumber + '\'' +
                ", officialRegistration='" + officialRegistration + '\'' +
                ", email='" + email + '\'' +
                ", phonenr='" + phonenr + '\'' +
                ", pec='" + pec + '\'' +
                ", mainAddress=" + mainAddress +
                '}';
    }
}
