package com.doing.nemo.claims.controller.payload.response.claims;

import com.doing.nemo.claims.controller.payload.response.AntiTheftServiceResponseV1;
import com.doing.nemo.claims.controller.payload.response.FleetManagerResponseV1;
import com.doing.nemo.claims.controller.payload.response.damaged.*;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class DamagedResponse implements Serializable {

    @JsonProperty("contract")
    private ContractResponse contract;

    @JsonProperty("customer")
    private CustomerResponse customer;

    @JsonProperty("insurance_company")
    private InsuranceCompanyResponse insuranceCompany;

    @JsonProperty("driver")
    private DriverResponse driver;

    @JsonProperty("vehicle")
    private VehicleResponse vehicle;

    @JsonProperty("fleet_managers")
    private List<FleetManagerResponseV1> fleetManagerList;

    @JsonProperty("is_cai_signed")
    private Boolean isCaiSigned;

    @JsonProperty("impact_point")
    private ImpactPointResponse impactPoint;

    @JsonProperty("additional_costs")
    private List<AdditionalCostsResponse> additionalCosts;

    @JsonProperty("anti_theft_service")
    private AntiTheftServiceResponseV1 antiTheftServiceResponseV1;

    public ContractResponse getContract() {
        return contract;
    }

    public void setContract(ContractResponse contract) {
        this.contract = contract;
    }

    public CustomerResponse getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerResponse customer) {
        this.customer = customer;
    }

    public InsuranceCompanyResponse getInsuranceCompany() {
        return insuranceCompany;
    }

    public void setInsuranceCompany(InsuranceCompanyResponse insuranceCompany) {
        this.insuranceCompany = insuranceCompany;
    }

    public DriverResponse getDriver() {
        return driver;
    }

    public void setDriver(DriverResponse driver) {
        this.driver = driver;
    }

    public VehicleResponse getVehicle() {
        return vehicle;
    }

    public void setVehicle(VehicleResponse vehicle) {
        this.vehicle = vehicle;
    }

    public List<FleetManagerResponseV1> getFleetManagerList() {
        if(fleetManagerList == null){
            return null;
        }
        return new ArrayList<>(fleetManagerList);
    }

    public void setFleetManagerList(List<FleetManagerResponseV1> fleetManagerList) {
        if(fleetManagerList != null)
        {
            this.fleetManagerList = new ArrayList<>(fleetManagerList);
        } else {
            this.fleetManagerList = null;
        }
    }

    @JsonIgnore
    public Boolean getCaiSigned() {
        return isCaiSigned;
    }

    public void setCaiSigned(Boolean caiSigned) {
        isCaiSigned = caiSigned;
    }

    public ImpactPointResponse getImpactPoint() {
        return impactPoint;
    }

    public void setImpactPoint(ImpactPointResponse impactPoint) {
        this.impactPoint = impactPoint;
    }

    public List<AdditionalCostsResponse> getAdditionalCosts() {
        if(additionalCosts == null){
            return null;
        }
        return new ArrayList<>(additionalCosts);
    }

    public void setAdditionalCosts(List<AdditionalCostsResponse> additionalCosts) {
        if(additionalCosts != null)
        {
            this.additionalCosts =new ArrayList<>(additionalCosts);
        } else {
            this.additionalCosts = null;
        }
    }

    public AntiTheftServiceResponseV1 getAntiTheftServiceResponseV1() {
        return antiTheftServiceResponseV1;
    }

    public void setAntiTheftServiceResponseV1(AntiTheftServiceResponseV1 antiTheftServiceResponseV1) {
        this.antiTheftServiceResponseV1 = antiTheftServiceResponseV1;
    }

    @Override
    public String toString() {
        return "DamagedResponse{" +
                "contract=" + contract +
                ", customer=" + customer +
                ", insuranceCompany=" + insuranceCompany +
                ", driver=" + driver +
                ", vehicle=" + vehicle +
                ", fleetManagerList=" + fleetManagerList +
                ", isCaiSigned=" + isCaiSigned +
                ", impactPoint=" + impactPoint +
                ", additionalCosts=" + additionalCosts +
                ", antiTheftServiceResponseV1=" + antiTheftServiceResponseV1 +
                '}';
    }
}
