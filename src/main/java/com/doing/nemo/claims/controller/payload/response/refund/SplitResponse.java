package com.doing.nemo.claims.controller.payload.response.refund;

import com.doing.nemo.claims.entity.enumerated.RefundEnum.RefundTypeEnum;
import com.doing.nemo.claims.entity.enumerated.RefundEnum.TypeEnum;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.UUID;

public class SplitResponse implements Serializable {

    @JsonProperty("id")
    private UUID id;

    @JsonProperty("issue_date")
    private String issueDate;

    @JsonProperty("type")
    private TypeEnum type;

    @JsonProperty("refund_type")
    private RefundTypeEnum refundType;

    @JsonProperty("received_sum")
    private Double receivedSum;

    @JsonProperty("legal_fees")
    private Double legalFees;

    @JsonProperty("technical_stop")
    private Double technicalStop;

    @JsonProperty("material_amount")
    private Double materialAmount;

    @JsonProperty("total_paid")
    private Double totalPaid;

    @JsonProperty("note")
    private String note;

    public SplitResponse() {
    }

    public SplitResponse(UUID id, String issueDate, TypeEnum type, RefundTypeEnum refundType, Double receivedSum, Double legalFees, Double technicalStop, Double materialAmount, Double totalPaid, String note) {
        this.id = id;
        this.issueDate = issueDate;
        this.type = type;
        this.refundType = refundType;
        this.receivedSum = receivedSum;
        this.legalFees = legalFees;
        this.technicalStop = technicalStop;
        this.materialAmount = materialAmount;
        this.totalPaid = totalPaid;
        this.note = note;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public TypeEnum getType() {
        return type;
    }

    public void setType(TypeEnum type) {
        this.type = type;
    }

    public RefundTypeEnum getRefundType() {
        return refundType;
    }

    public void setRefundType(RefundTypeEnum refundType) {
        this.refundType = refundType;
    }

    public Double getReceivedSum() {
        return receivedSum;
    }

    public void setReceivedSum(Double receivedSum) {
        this.receivedSum = receivedSum;
    }

    public Double getTotalPaid() {
        return totalPaid;
    }

    public void setTotalPaid(Double totalPaid) {
        this.totalPaid = totalPaid;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Double getLegalFees() {
        return legalFees;
    }

    public void setLegalFees(Double legalFees) {
        this.legalFees = legalFees;
    }

    public Double getTechnicalStop() {
        return technicalStop;
    }

    public void setTechnicalStop(Double technicalStop) {
        this.technicalStop = technicalStop;
    }

    public Double getMaterialAmount() {
        return materialAmount;
    }

    public void setMaterialAmount(Double materialAmount) {
        this.materialAmount = materialAmount;
    }

    @Override
    public String toString() {
        return "SplitResponse{" +
                "id=" + id +
                ", issueDate='" + issueDate + '\'' +
                ", type=" + type +
                ", refundType=" + refundType +
                ", receivedSum=" + receivedSum +
                ", legalFees=" + legalFees +
                ", technicalStop=" + technicalStop +
                ", materialAmount=" + materialAmount +
                ", totalPaid=" + totalPaid +
                ", note='" + note + '\'' +
                '}';
    }
}