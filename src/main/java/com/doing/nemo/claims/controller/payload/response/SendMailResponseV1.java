package com.doing.nemo.claims.controller.payload.response;

import com.doing.nemo.claims.controller.payload.request.messaging.Identity;
import com.doing.nemo.claims.entity.enumerated.BodyType;
import com.doing.nemo.claims.entity.enumerated.EventTypeEnum;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
@JsonIgnoreProperties(ignoreUnknown = true)
public class SendMailResponseV1 implements Serializable {

    @JsonProperty("tos")
    private List<Identity> tos = new ArrayList<>();

    @JsonProperty("ccs")
    private List<Identity> ccs = new ArrayList<>();

    @JsonProperty("bccs")
    private List<Identity> bccs = new ArrayList<>();

    @JsonProperty("reply_to")
    private Identity replyTo;

    @JsonProperty("subject")
    private String subject;

    @JsonProperty("body_type")
    private BodyType bodyType;

    @JsonProperty("body_content")
    private String bodyContent;

    @JsonProperty("event_type")
    private EventTypeEnum eventType;

    @JsonProperty("filemanager_id_attachment_list")
    private List<String> filemanagerIdAttachmentList = new ArrayList<>();

    @JsonProperty("status")
    private String status;

    public List<Identity> getTos() {
        if(tos == null){
            return null;
        }
        return new ArrayList<>(tos);
    }

    public void setTos(List<Identity> tos) {
        if(tos != null)
        {
            this.tos = new ArrayList<>(tos);
        } else {
            this.tos = null;
        }
    }

    public List<Identity> getCcs() {
        if(ccs == null){
            return null;
        }
        return new ArrayList<>(ccs);
    }

    public void setCcs(List<Identity> ccs) {
        if(ccs != null)
        {
            this.ccs = new ArrayList<>(ccs);
        } else {
            this.ccs = null;
        }
    }

    public List<Identity> getBccs() {
        if(bccs == null){
            return null;
        }
        return new ArrayList<>(bccs);
    }

    public void setBccs(List<Identity> bccs) {
        if(bccs != null){
            this.bccs = new ArrayList<>(bccs);
        } else {
            this.bccs = null;
        }

    }

    public Identity getReplyTo() {
        return replyTo;
    }

    public void setReplyTo(Identity replyTo) {
        this.replyTo = replyTo;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public BodyType getBodyType() {
        return bodyType;
    }

    public void setBodyType(BodyType bodyType) {
        this.bodyType = bodyType;
    }

    public String getBodyContent() {
        return bodyContent;
    }

    public void setBodyContent(String bodyContent) {
        this.bodyContent = bodyContent;
    }

    public EventTypeEnum getEventType() {
        return eventType;
    }

    public void setEventType(EventTypeEnum eventType) {
        this.eventType = eventType;
    }

    public List<String> getFilemanagerIdAttachmentList() {
        if(filemanagerIdAttachmentList == null){
            return null;
        }
        return new ArrayList<>(filemanagerIdAttachmentList);
    }

    public void setFilemanagerIdAttachmentList(List<String> filemanagerIdAttachmentList) {
        if(filemanagerIdAttachmentList != null)
        {
            this.filemanagerIdAttachmentList = new ArrayList<>(filemanagerIdAttachmentList);
        } else {
            this.filemanagerIdAttachmentList = null;
        }
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "SendMailResponseV1{" +
                "tos=" + tos +
                ", ccs=" + ccs +
                ", bccs=" + bccs +
                ", replyTo=" + replyTo +
                ", subject='" + subject + '\'' +
                ", bodyType=" + bodyType +
                ", bodyContent='" + bodyContent + '\'' +
                ", eventType=" + eventType +
                ", filemanagerIdAttachmentList=" + filemanagerIdAttachmentList +
                ", status='" + status + '\'' +
                '}';
    }
}
