package com.doing.nemo.claims.controller;

import com.doing.nemo.claims.adapter.ContractAdapter;
import com.doing.nemo.claims.controller.payload.request.ClaimsTypeRequestV1;
import com.doing.nemo.claims.controller.payload.request.FlowContractUpdateRequestV1;
import com.doing.nemo.claims.controller.payload.request.ListRequestV1;
import com.doing.nemo.claims.controller.payload.response.*;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsFlowEnum;
import com.doing.nemo.claims.entity.settings.ContractTypeEntity;
import com.doing.nemo.claims.entity.settings.CustomTypeRiskEntity;
import com.doing.nemo.claims.entity.settings.PersonalDataEntity;
import com.doing.nemo.claims.entity.settings.SettingsRequest;
import com.doing.nemo.claims.repository.PersonalDataRepository;
import com.doing.nemo.claims.service.ContractService;
import io.swagger.annotations.*;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/v1")
@Api("Claims")
public class ContractSettingController {

    private static Logger LOGGER = LoggerFactory.getLogger(ContractSettingController.class);

    @Autowired
    private ContractService contractService;

    @Autowired
    private PersonalDataRepository personalDataRepository;

    @Autowired
    private ContractAdapter contractAdapter;


    @PostMapping("/claimstype")
    public ResponseEntity<List<ClaimsTypeResponseV1>> inserClaimsTypeData(@RequestBody ClaimsTypeRequestV1 claimsType) {

        contractService.insertClaimType(claimsType.getData());
        return new ResponseEntity<>(contractService.getListClaimsType(), HttpStatus.OK);
    }

    @PostMapping("/contract")
    @Transactional
    public ResponseEntity<ContractResponseIdV1> insertContract(@RequestBody SettingsRequest settingsRequest) {

        return new ResponseEntity<>(new ContractResponseIdV1(contractService.insertContract(settingsRequest)), HttpStatus.OK);
    }

    @PostMapping("/contracts")
    @Transactional
    @ApiOperation(value = "Insert a list of Contracts", notes = "Insert a list of contracts using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = ContractResponseIdV1.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<ContractResponseIdV1>> insertContracts(
            @ApiParam(value = "List of the bodies of the Contracts to be created", required = true)
            @RequestBody ListRequestV1<SettingsRequest> settingsListRequest) {

        List<ContractResponseIdV1> contractTypeEntityResponseV1List = new ArrayList<>();
        for (SettingsRequest settingsRequest : settingsListRequest.getData()) {
            contractTypeEntityResponseV1List.add(new ContractResponseIdV1(contractService.insertContract(settingsRequest)));
        }
        return new ResponseEntity<>(contractTypeEntityResponseV1List, HttpStatus.CREATED);

    }

    @GetMapping("/contract")
    public ResponseEntity<List<ContractTypeEntityResponseV1>> getContractList(
            @ApiParam(value = "defines one or more contract codes to be applied as a search filter", required = false)
            @RequestParam(value = "contract_code", required = false) String contractCode) {

        return new ResponseEntity<>(contractService.getListContractType(contractCode), HttpStatus.OK);
    }

    @GetMapping("/contract/setting/{UUID_CONTRACT}")
    public ResponseEntity<ContractTypeEntityResponseV1> getContractById(
            @ApiParam(value = "UUID of the Contract", required = true)
            @PathVariable("UUID_CONTRACT") String uuid) {

        return new ResponseEntity<>(contractService.getContractTypeById(uuid), HttpStatus.OK);
    }

    @GetMapping("/claimstype")
    public ResponseEntity<List<ClaimsTypeResponseV1>> getClaimsTypeList() {
        return new ResponseEntity<>(contractService.getListClaimsType(), HttpStatus.OK);
    }


    //logica custom risk
    @GetMapping("/contract/codcontract/{COD_CONTRACT}")
    public ResponseEntity<List<ClaimsTypeWithFlowResponseV1>> getContractByCodContract(
            @ApiParam(value = "Contract code", required = true)
            @PathVariable("COD_CONTRACT") String codContract,
            @RequestHeader(value ="customer_id", required = false) String customerId
    ) {
        List<ClaimsTypeWithFlowResponseV1> flowResponseV1List = contractService.getContractTypeByCodContract(codContract);

        if(customerId != null){
            //recupero in custom risk che si trova in personal data
            PersonalDataEntity personalDataEntity =personalDataRepository.findByCustomerId(customerId);
            if(personalDataEntity != null){
                List<CustomTypeRiskEntity> customTypeRiskEntityList = personalDataEntity.getCustomTypeRiskEntityList();
                if(customTypeRiskEntityList != null && !customTypeRiskEntityList.isEmpty()){
                    //manipolazione
                    for(CustomTypeRiskEntity currentCustomRisk :customTypeRiskEntityList ){
                        for(ClaimsTypeWithFlowResponseV1 currentFlow : flowResponseV1List){
                            if(currentCustomRisk.getClaimsTypeEntity().getClaimsAccidentType().equals(currentFlow.getClaimsAccidentType())){
                                //se trovo lo stesso tipo sinistro sovrascrivo il flusso il rebilling
                                currentFlow.setFlow(currentCustomRisk.getFlow());
                                currentFlow.setRebilling(currentCustomRisk.getTypeOfChargeback());
                            }
                        }
                    }
                }

            }
        }
        return new ResponseEntity<>(flowResponseV1List, HttpStatus.OK);
    }

    @PutMapping("/contract/{UUID_CONTRACT}")
    @Transactional
    public ResponseEntity<ContractTypeEntityResponseV1> updateFlowContract(
            @ApiParam(value = "UUID of the Contract", required = true)
            @PathVariable("UUID_CONTRACT") String uuid,
            @ApiParam(value = "Body containing the information to be updated in the contract identified", required = false)
            @RequestBody FlowContractUpdateRequestV1 flowContractUpdateRequestV1) {

        return new ResponseEntity<>(contractService.updateFlowContractList(uuid, flowContractUpdateRequestV1), HttpStatus.OK);

    }

    @PatchMapping("/contract/{UUID}/active")
    @Transactional
    @ApiOperation(value = "Patch status active Contract TypeEnum", notes = "Patch status active Contract TypeEnum using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ContractTypeEntityResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ContractTypeEntityResponseV1> patchStatusManager(
            @PathVariable UUID UUID
    ) {
        ContractTypeEntity contractTypeEntity = contractService.patchActive(UUID);
        ContractTypeEntityResponseV1 responseV1 = ContractAdapter.getContractAdapter(contractTypeEntity);
        return new ResponseEntity<>(responseV1, HttpStatus.OK);
    }

    @ApiOperation(value="Contract TypeEnum Pagination", notes = "It retrieves Contract TypeEnum pagination")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = ClaimsResponsePaginationV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @GetMapping(value="/contract/pagination", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<ClaimsResponsePaginationV1> contractstypePagination(
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @RequestParam(value = "page_size", defaultValue = "20") Integer pageSize,
            @RequestParam(value = "order_by", required = false) String orderBy,
            @RequestParam(value = "asc", required = false, defaultValue = "true") Boolean asc,
            @RequestParam(value = "cod_contract_type", required = false) String codContractType,
            @RequestParam(value = "description", required = false) String description,
            @RequestParam(value = "unlocking_entry", required = false) Boolean flagWS,
            @RequestParam(value = "default_flow", required = false) String defaultFlow,
            @RequestParam(value = "ctrnote", required = false) String ctrnote,
            @RequestParam(value = "ricaricar", required = false) Boolean ricaricar,
            @RequestParam(value = "franchise", required = false) Double franchise,
            @RequestParam(value = "is_active", required = false) Boolean isActive
            ){

        ClaimsFlowEnum claimsFlowEnum = null;
        if(defaultFlow != null)
            claimsFlowEnum = ClaimsFlowEnum.create(defaultFlow);

        Pagination<ContractTypeEntity> contractTypeEntityPagination = contractService.paginationContractType(page, pageSize, orderBy, asc, codContractType, description, flagWS, claimsFlowEnum, ctrnote, ricaricar, franchise, isActive);

        return new ResponseEntity<>(ContractAdapter.adptContractTypePaginationToClaimsResponsePagination(contractTypeEntityPagination), HttpStatus.OK);
    }
}
