package com.doing.nemo.claims.controller.payload.response.damaged;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class LeaseServiceComponentsResponse implements Serializable {

    @JsonProperty("id")
    private Long id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("product_id")
    private Long productId;

    @JsonProperty("qualifiers")
    private List<QualifiersResponse> qualifiersList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public List<QualifiersResponse> getQualifiersList() {
        if(qualifiersList == null){
            return null;
        }
        return new ArrayList<>(qualifiersList);
    }

    public void setQualifiersList(List<QualifiersResponse> qualifiersList) {
        if(qualifiersList != null)
        {
            this.qualifiersList = new ArrayList<>(qualifiersList);
        } else {
            this.qualifiersList = null;
        }
    }

    @Override
    public String toString() {
        return "LeaseServiceComponentsResponse{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", productId=" + productId +
                ", qualifiersList=" + qualifiersList +
                '}';
    }
}
