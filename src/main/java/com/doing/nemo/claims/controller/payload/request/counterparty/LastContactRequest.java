package com.doing.nemo.claims.controller.payload.request.counterparty;

import com.doing.nemo.claims.controller.payload.request.MotivationRequestV1;
import com.doing.nemo.claims.entity.enumerated.RepairEnum.RepairContactResultEnum;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class LastContactRequest implements Serializable {

    @JsonProperty("last_call")
    private String lastCall;

    @JsonProperty("result")
    private RepairContactResultEnum result;

    @JsonProperty("recall")
    private String recall;

    @JsonProperty("note")
    private String note;

    @JsonProperty("motivation")
    private MotivationRequestV1 motivation;

    public LastContactRequest() {
    }

    public LastContactRequest(String lastCall, RepairContactResultEnum result, String recall, String note, MotivationRequestV1 motivation) {
        this.lastCall = lastCall;
        this.result = result;
        this.recall = recall;
        this.note = note;
        this.motivation = motivation;
    }

    public String getLastCall() {
        return lastCall;
    }

    public void setLastCall(String lastCall) {
        this.lastCall = lastCall;
    }

    public RepairContactResultEnum getResult() {
        return result;
    }

    public void setResult(RepairContactResultEnum result) {
        this.result = result;
    }

    public String getRecall() {
        return recall;
    }

    public void setRecall(String recall) {
        this.recall = recall;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public MotivationRequestV1 getMotivation() {
        return motivation;
    }

    public void setMotivation(MotivationRequestV1 motivation) {
        this.motivation = motivation;
    }

    @Override
    public String toString() {
        return "LastContactRequest{" +
                "lastCall=" + lastCall +
                ", result=" + result +
                ", recall=" + recall +
                ", note='" + note + '\'' +
                ", motivation=" + motivation +
                '}';
    }
}
