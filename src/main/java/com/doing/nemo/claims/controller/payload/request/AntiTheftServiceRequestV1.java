package com.doing.nemo.claims.controller.payload.request;

import com.doing.nemo.claims.entity.enumerated.AntitheftEnum.AntitheftTypeEnum;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.UUID;

public class AntiTheftServiceRequestV1 implements Serializable {

    @JsonProperty("id")
    private UUID id;

    @JsonProperty("code_anti_theft_service")
    private String codeAntiTheftService = "";

    @JsonProperty("name")
    @NotNull
    private String name;

    @JsonProperty("business_name")
    @NotNull
    private String businessName;

    @JsonProperty("supplier_code")
    @NotNull
    private Integer supplierCode;

    @JsonProperty("email")
    private String email = "";

    @JsonProperty("phone_number")
    @NotNull
    private String phoneNumber;

    @JsonProperty("provider_type")
    private String providerType;

    @JsonProperty("time_out_in_seconds")
    private Integer timeOutInSeconds;

    @JsonProperty("end_point_url")
    private String endPointUrl = "";

    @JsonProperty("username")
    private String username = "";

    @JsonProperty("password")
    private String password = "";

    @JsonProperty("company_code")
    private String companyCode = "";

    @JsonProperty("active")
    private Boolean active;

    @JsonProperty("type")
    private AntitheftTypeEnum type;

    @JsonProperty("is_active")
    private Boolean IsActive = true;

    public AntiTheftServiceRequestV1(UUID id, String codeAntiTheftService, @NotNull String name, @NotNull String businessName, @NotNull Integer supplierCode, String email, @NotNull String phoneNumber, String providerType, Integer timeOutInSeconds, String endPointUrl, String username, String password, String companyCode, Boolean active, AntitheftTypeEnum type, Boolean isActive) {
        this.id = id;
        this.codeAntiTheftService = codeAntiTheftService;
        this.name = name;
        this.businessName = businessName;
        this.supplierCode = supplierCode;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.providerType = providerType;
        this.timeOutInSeconds = timeOutInSeconds;
        this.endPointUrl = endPointUrl;
        this.username = username;
        this.password = password;
        this.companyCode = companyCode;
        this.active = active;
        this.type = type;
        IsActive = isActive;

    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getCodeAntiTheftService() {
        return codeAntiTheftService;
    }

    public void setCodeAntiTheftService(String codeAntiTheftService) {
        this.codeAntiTheftService = codeAntiTheftService;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public Integer getSupplierCode() {
        return supplierCode;
    }

    public void setSupplierCode(Integer supplierCode) {
        this.supplierCode = supplierCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getProviderType() {
        return providerType;
    }

    public void setProviderType(String providerType) {
        this.providerType = providerType;
    }

    public Integer getTimeOutInSeconds() {
        return timeOutInSeconds;
    }

    public void setTimeOutInSeconds(Integer timeOutInSeconds) {
        this.timeOutInSeconds = timeOutInSeconds;
    }

    public String getEndPointUrl() {
        return endPointUrl;
    }

    public void setEndPointUrl(String endPointUrl) {
        this.endPointUrl = endPointUrl;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public Boolean getIsActive() {
        return IsActive;
    }

    public void setIsActive(Boolean active) {
        this.IsActive = active;
    }

    public Boolean getActive() {
        return active;
    }

    @JsonSetter
    public void setActive(Boolean active) {
        if (active != null)
            this.active = active;
    }

    public AntitheftTypeEnum getType() {
        return type;
    }

    public void setType(AntitheftTypeEnum type) {
        this.type = type;
    }



    @Override
    public String toString() {
        return "AntiTheftServiceRequestV1{" +
                "id=" + id +
                ", codeAntiTheftService='" + codeAntiTheftService + '\'' +
                ", name='" + name + '\'' +
                ", businessName='" + businessName + '\'' +
                ", supplierCode=" + supplierCode +
                ", email='" + email + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", providerType='" + providerType + '\'' +
                ", timeOutInSeconds=" + timeOutInSeconds +
                ", endPointUrl='" + endPointUrl + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", companyCode='" + companyCode + '\'' +
                ", active=" + active +
                ", type=" + type +
                ", IsActive=" + IsActive +
                '}';
    }
}
