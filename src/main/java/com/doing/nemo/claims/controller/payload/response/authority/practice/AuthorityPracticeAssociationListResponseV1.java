package com.doing.nemo.claims.controller.payload.response.authority.practice;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AuthorityPracticeAssociationListResponseV1 implements Serializable {
    @JsonProperty("items")
    private List<AuthorityPracticeAssociationResponseV1> items;

    public List<AuthorityPracticeAssociationResponseV1> getItems() {
        if(items == null){
            return null;
        }
        return new ArrayList<>(items);
    }

    public void setItems(List<AuthorityPracticeAssociationResponseV1> items) {
        if(items != null)
        {
            this.items = new ArrayList<>(items);
        } else {
            this.items = null;
        }
    }

    @Override
    public String toString() {
        return "AuthorityPracticeAssociationListResponseV1{" +
                "items=" + items +
                '}';
    }
}
