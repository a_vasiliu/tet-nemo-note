package com.doing.nemo.claims.controller.payload.request;


import com.doing.nemo.claims.controller.payload.request.claims.CaiRequest;
import com.doing.nemo.claims.controller.payload.request.claims.DeponentRequest;
import com.doing.nemo.claims.controller.payload.request.claims.WoundedRequest;
import com.doing.nemo.claims.controller.payload.request.complaint.DataAccidentRequest;
import com.doing.nemo.claims.controller.payload.request.counterparty.DriverRequest;
import com.doing.nemo.claims.controller.payload.request.counterparty.ImpactPointRequest;
import com.doing.nemo.claims.controller.payload.request.counterparty.InsuredRequest;
import com.doing.nemo.claims.controller.payload.request.counterparty.VehicleRequest;
import com.doing.nemo.claims.controller.payload.request.damaged.AdditionalCostsRequest;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.ComplaintModEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.ComplaintPropertyEnum;
import com.doing.nemo.claims.entity.enumerated.DamagedEnum.VehicleEnum.VehicleTypeEnum;
import com.doing.nemo.claims.entity.jsonb.Address;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ClaimsInsertEnjoyRequest implements Serializable {
    @JsonProperty("complaint")
    private ComplaintEnjoyRequest complaint;

    @JsonProperty("is_with_counterparty")
    private Boolean isWithCounterparty;

    @JsonProperty("counterparty")
    private List<CounterpartyEnjoyRequest> counterparty;

    @JsonProperty("deponent")
    private List<DeponentRequest> deponent;

    @JsonProperty("wounded")
    private List<WoundedRequest> wounded;

    @JsonProperty("damaged")
    private DamagedEnjoyRequest damaged;

    @JsonProperty("theft")
    private TheftEnjoyRequest theftRequest;

    @JsonProperty("id_saleforce")
    private Long idSaleforce;
    
    @JsonProperty("cai_details")
    private CaiRequest caiDetails;

    @JsonProperty("is_complete_documentation")
    private Boolean isCompleteDocumentation ;
    
    
    public ClaimsInsertEnjoyRequest() {}


    public static class ComplaintEnjoyRequest {

        @JsonProperty("plate")
        private String plate;

        @JsonProperty("locator")
        private String locator;

        @JsonProperty("activation")
        private String activation;

        @JsonProperty("property")
        private ComplaintPropertyEnum property;

        @JsonProperty("mod")
        private ComplaintModEnum mod;

        @JsonProperty("notification")
        private String notification;

        @JsonProperty("quote")
        private Boolean quote;

        @JsonProperty("formatted_address")
        private String formattedAddress;

        @JsonProperty("data_accident")
        private DataAccidentRequest dataAccident;

        public ComplaintEnjoyRequest() {}

        public String getPlate() {
            return plate;
        }

        public void setPlate(String plate) {
            this.plate = plate;
        }

        public String getLocator() {
            return locator;
        }

        public void setLocator(String locator) {
            this.locator = locator;
        }

        public String getActivation() {
            return activation;
        }

        public void setActivation(String activation) {
            this.activation = activation;
        }

        public ComplaintPropertyEnum getProperty() {
            return property;
        }

        public void setProperty(ComplaintPropertyEnum property) {
            this.property = property;
        }

        public ComplaintModEnum getMod() {
            return mod;
        }

        public void setMod(ComplaintModEnum mod) {
            this.mod = mod;
        }

        public String getNotification() {
            return notification;
        }

        public void setNotification(String notification) {
            this.notification = notification;
        }

        public Boolean getQuote() {
            return quote;
        }

        public void setQuote(Boolean quote) {
            this.quote = quote;
        }

        public String getFormattedAddress() {
            return formattedAddress;
        }

        public void setFormattedAddress(String formattedAddress) {
            this.formattedAddress = formattedAddress;
        }

        public DataAccidentRequest getDataAccident() {
            return dataAccident;
        }

        public void setDataAccident(DataAccidentRequest dataAccident) {
            this.dataAccident = dataAccident;
        }


        @Override
        public String toString() {
            return "ComplaintCheckRequest{" +
                    "plate='" + plate + '\'' +
                    ", locator='" + locator + '\'' +
                    ", activation='" + activation + '\'' +
                    ", property=" + property +
                    ", mod=" + mod +
                    ", notification=" + notification +
                    ", quote=" + quote +
                    ", formattedAddress='" + formattedAddress + '\'' +
                    ", dataAccident=" + dataAccident +
                    '}';
        }

        public static class RepairExternalRequest {
            @JsonProperty("vehicle_value")
            private Double vehicleValue;

            @JsonProperty("blocks_repair")
            private Boolean blocksRepair = false;

            @JsonProperty("unrepairable")
            private Boolean unrepairable = false;

            @JsonProperty("motivation")
            private String motivation;

            @JsonProperty("repairer")
            private String repairer;

            @JsonProperty("phone")
            private String phone;

            @JsonProperty("email")
            private String email;

            @JsonProperty("address")
            private Address address;

            public RepairExternalRequest() {
            }

            public RepairExternalRequest(Double vehicleValue, Boolean blocksRepair, Boolean unrepairable, String motivation, String repairer, String phone, String email, Address address) {
                this.vehicleValue = vehicleValue;
                this.blocksRepair = blocksRepair;
                this.unrepairable = unrepairable;
                this.motivation = motivation;
                this.repairer = repairer;
                this.phone = phone;
                this.email = email;
                this.address = address;
            }

            public Double getVehicleValue() {
                return vehicleValue;
            }

            public void setVehicleValue(Double vehicleValue) {
                this.vehicleValue = vehicleValue;
            }

            public Boolean getBlocksRepair() {
                return blocksRepair;
            }

            public void setBlocksRepair(Boolean blocksRepair) {
                this.blocksRepair = blocksRepair;
            }

            public Boolean getUnrepairable() {
                return unrepairable;
            }

            public void setUnrepairable(Boolean unrepairable) {
                this.unrepairable = unrepairable;
            }

            public String getMotivation() {
                return motivation;
            }

            public void setMotivation(String motivation) {
                this.motivation = motivation;
            }

            public String getRepairer() {
                return repairer;
            }

            public void setRepairer(String repairer) {
                this.repairer = repairer;
            }

            public String getPhone() {
                return phone;
            }

            public void setPhone(String phone) {
                this.phone = phone;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public Address getAddress() {
                return address;
            }

            public void setAddress(Address address) {
                this.address = address;
            }

            @Override
            public String toString() {
                return "RepairExternalRequest{" +
                        "vehicleValue=" + vehicleValue +
                        ", blocksRepair=" + blocksRepair +
                        ", unrepairable=" + unrepairable +
                        ", motivation='" + motivation + '\'' +
                        ", repairer='" + repairer + '\'' +
                        ", phone='" + phone + '\'' +
                        ", email='" + email + '\'' +
                        ", address=" + address +
                        '}';
            }
        }


    }

    public static class CounterpartyEnjoyRequest {
        @JsonProperty("type")
        private VehicleTypeEnum type;

        @JsonProperty("insured")
        private InsuredRequest insured;

        @JsonProperty("insurance_company")
        private InsuranceCompanyCounterpartyRequest insuranceCompany;

        @JsonProperty("driver")
        private DriverRequest driver;

        @JsonProperty("vehicle")
        private VehicleRequest vehicle;

        @JsonProperty("is_cai_signed")
        private Boolean isCaiSigned;

        @JsonProperty("impact_point")
        private ImpactPointRequest impactPoint;

        @JsonProperty("responsible")
        private Boolean responsible;

        @JsonProperty("description")
        private String description;

        @JsonProperty("policy_number")
        private String policyNumber;

        @JsonProperty("policy_beginning_validity")
        @JsonFormat(pattern = "yyyy-MM-dd")
        private Date policyBeginningValidity;

        @JsonProperty("policy_end_validity")
        @JsonFormat(pattern = "yyyy-MM-dd")
        private Date policyEndValidity;

        public CounterpartyEnjoyRequest() {}

        public VehicleTypeEnum getType() {
            return type;
        }

        public void setType(VehicleTypeEnum type) {
            this.type = type;
        }

        public InsuredRequest getInsured() {
            return insured;
        }

        public void setInsured(InsuredRequest insured) {
            this.insured = insured;
        }

        public InsuranceCompanyCounterpartyRequest getInsuranceCompany() {
            return insuranceCompany;
        }

        public void setInsuranceCompany(InsuranceCompanyCounterpartyRequest insuranceCompany) {
            this.insuranceCompany = insuranceCompany;
        }

        public DriverRequest getDriver() {
            return driver;
        }

        public void setDriver(DriverRequest driver) {
            this.driver = driver;
        }

        public VehicleRequest getVehicle() {
            return vehicle;
        }

        public void setVehicle(VehicleRequest vehicle) {
            this.vehicle = vehicle;
        }

        public Boolean getCaiSigned() {
            return isCaiSigned;
        }

        public void setCaiSigned(Boolean caiSigned) {
            isCaiSigned = caiSigned;
        }

        public ImpactPointRequest getImpactPoint() {
            return impactPoint;
        }

        public void setImpactPoint(ImpactPointRequest impactPoint) {
            this.impactPoint = impactPoint;
        }

        public Boolean getResponsible() {
            return responsible;
        }

        public void setResponsible(Boolean responsible) {
            this.responsible = responsible;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getPolicyNumber() {
            return policyNumber;
        }

        public void setPolicyNumber(String policyNumber) {
            this.policyNumber = policyNumber;
        }

        public Date getPolicyBeginningValidity()
        {
            if(policyBeginningValidity == null){
                return null;
            }
            return (Date)policyBeginningValidity.clone();
        }

        public void setPolicyBeginningValidity(Date policyBeginningValidity) {
            if( policyBeginningValidity != null) {
                this.policyBeginningValidity = (Date)policyBeginningValidity.clone();
            }else{
                this.policyBeginningValidity = null;
            }
        }

        public Date getPolicyEndValidity() {
            if(policyEndValidity == null){
                return null;
            }
            return (Date)policyEndValidity.clone();
        }

        public void setPolicyEndValidity(Date policyEndValidity) {
            if(policyEndValidity != null) {
                this.policyEndValidity = (Date) policyEndValidity.clone();
            }else{
                this.policyEndValidity = null;
            }
        }

        @Override
        public String toString() {
            return "CounterpartyExternalRequest{" +
                    "type=" + type +
                    ", insured=" + insured +
                    ", insuranceCompany=" + insuranceCompany +
                    ", driver=" + driver +
                    ", vehicle=" + vehicle +
                    ", isCaiSigned=" + isCaiSigned +
                    ", impactPoint=" + impactPoint +
                    ", responsible=" + responsible +
                    ", description='" + description + '\'' +
                    ", policyNumber='" + policyNumber + '\'' +
                    ", policyBeginningValidity=" + policyBeginningValidity +
                    ", policyEndValidity=" + policyEndValidity +
                    '}';
        }
    }

    public static class DamagedEnjoyRequest {
        @JsonProperty("driver")
        private DriverRequest driver;

        @JsonProperty("is_cai_signed")
        private Boolean isCaiSigned;

        @JsonProperty("impact_point")
        private ImpactPointRequest impactPoint;

        @JsonProperty("additional_costs")
        private List<AdditionalCostsRequest> additionalCosts;

        public DamagedEnjoyRequest() {}

        public DriverRequest getDriver() {
            return driver;
        }

        public void setDriver(DriverRequest driver) {
            this.driver = driver;
        }

        public Boolean getCaiSigned() {
            return isCaiSigned;
        }

        public void setCaiSigned(Boolean caiSigned) {
            isCaiSigned = caiSigned;
        }

        public ImpactPointRequest getImpactPoint() {
            return impactPoint;
        }

        public void setImpactPoint(ImpactPointRequest impactPoint) {
            this.impactPoint = impactPoint;
        }

        public List<AdditionalCostsRequest> getAdditionalCosts() {
            if (additionalCosts == null){
                return null;
             }
           return new ArrayList<>(additionalCosts);
        }

        public void setAdditionalCosts(List<AdditionalCostsRequest> additionalCosts) {
            if(additionalCosts != null) {
                this.additionalCosts = new ArrayList<>(additionalCosts);
            }else{
                this.additionalCosts = null;
            }
        }

        @Override
        public String toString() {
            return "DamagedCheckRequest{" +
                    "isCaiSigned=" + isCaiSigned +
                    ", impactPoint=" + impactPoint +
                    ", additionalCosts=" + additionalCosts +
                    '}';
        }
    }

    public static class TheftEnjoyRequest {
        @JsonProperty("is_found")
        private Boolean isFound;

        @JsonProperty("operations_center_notified")
        private Boolean OperationsCenterNotified;

        @JsonProperty("theft_occurred_on_center_ald")
        private Boolean theftOccurredOnCenterAld;

        @JsonProperty("complaint_authority_police")
        private Boolean complaintAuthorityPolice;

        @JsonProperty("complaint_authority_cc")
        private Boolean complaintAuthorityCc;

        @JsonProperty("complaint_authority_vvuu")
        private Boolean complaintAuthorityVvuu;

        @JsonProperty("authority_data")
        private String authorityData;

        @JsonProperty("authority_telephone")
        private String authorityTelephone;

        @JsonProperty("find_date")
        private String findDate;

        @JsonProperty("hour")
        private String hour;

        @JsonProperty("found_abroad")
        private Boolean foundAbroad;

        @JsonProperty("vehicle_co")
        private String vehicleCo;

        @JsonProperty("sequestered")
        private Boolean sequestered;

        @JsonProperty("address")
        private Address address;

        @JsonProperty("find_authority_police")
        private Boolean findAuthorityPolice;

        @JsonProperty("find_authority_cc")
        private Boolean findAuthorityCc;

        @JsonProperty("find_authority_vvuu")
        private Boolean findAuthorityVvuu;

        @JsonProperty("find_authority_data")
        private String findAuthorityData;

        @JsonProperty("find_authority_telephone")
        private String findAuthorityTelephone;

        @JsonProperty("theft_notes")
        private String theftNotes;

        @JsonProperty("find_notes")
        private String findNotes;

        public TheftEnjoyRequest() {}

        public Boolean getFound() {
            return isFound;
        }

        public void setFound(Boolean found) {
            isFound = found;
        }

        public Boolean getOperationsCenterNotified() {
            return OperationsCenterNotified;
        }

        public void setOperationsCenterNotified(Boolean operationsCenterNotified) {
            OperationsCenterNotified = operationsCenterNotified;
        }

        public Boolean getTheftOccurredOnCenterAld() {
            return theftOccurredOnCenterAld;
        }

        public void setTheftOccurredOnCenterAld(Boolean theftOccurredOnCenterAld) {
            this.theftOccurredOnCenterAld = theftOccurredOnCenterAld;
        }

        public Boolean getComplaintAuthorityPolice() {
            return complaintAuthorityPolice;
        }

        public void setComplaintAuthorityPolice(Boolean complaintAuthorityPolice) {
            this.complaintAuthorityPolice = complaintAuthorityPolice;
        }

        public Boolean getComplaintAuthorityCc() {
            return complaintAuthorityCc;
        }

        public void setComplaintAuthorityCc(Boolean complaintAuthorityCc) {
            this.complaintAuthorityCc = complaintAuthorityCc;
        }

        public Boolean getComplaintAuthorityVvuu() {
            return complaintAuthorityVvuu;
        }

        public void setComplaintAuthorityVvuu(Boolean complaintAuthorityVvuu) {
            this.complaintAuthorityVvuu = complaintAuthorityVvuu;
        }

        public String getAuthorityData() {
            return authorityData;
        }

        public void setAuthorityData(String authorityData) {
            this.authorityData = authorityData;
        }

        public String getAuthorityTelephone() {
            return authorityTelephone;
        }

        public void setAuthorityTelephone(String authorityTelephone) {
            this.authorityTelephone = authorityTelephone;
        }

        public String getFindDate() {
            return findDate;
        }

        public void setFindDate(String findDate) {
            this.findDate = findDate;
        }

        public String getHour() {
            return hour;
        }

        public void setHour(String hour) {
            this.hour = hour;
        }

        public Boolean getFoundAbroad() {
            return foundAbroad;
        }

        public void setFoundAbroad(Boolean foundAbroad) {
            this.foundAbroad = foundAbroad;
        }

        public String getVehicleCo() {
            return vehicleCo;
        }

        public void setVehicleCo(String vehicleCo) {
            this.vehicleCo = vehicleCo;
        }

        public Boolean getSequestered() {
            return sequestered;
        }

        public void setSequestered(Boolean sequestered) {
            this.sequestered = sequestered;
        }

        public Address getAddress() {
            return address;
        }

        public void setAddress(Address address) {
            this.address = address;
        }

        public Boolean getFindAuthorityPolice() {
            return findAuthorityPolice;
        }

        public void setFindAuthorityPolice(Boolean findAuthorityPolice) {
            this.findAuthorityPolice = findAuthorityPolice;
        }

        public Boolean getFindAuthorityCc() {
            return findAuthorityCc;
        }

        public void setFindAuthorityCc(Boolean findAuthorityCc) {
            this.findAuthorityCc = findAuthorityCc;
        }

        public Boolean getFindAuthorityVvuu() {
            return findAuthorityVvuu;
        }

        public void setFindAuthorityVvuu(Boolean findAuthorityVvuu) {
            this.findAuthorityVvuu = findAuthorityVvuu;
        }

        public String getFindAuthorityData() {
            return findAuthorityData;
        }

        public void setFindAuthorityData(String findAuthorityData) {
            this.findAuthorityData = findAuthorityData;
        }

        public String getFindAuthorityTelephone() {
            return findAuthorityTelephone;
        }

        public void setFindAuthorityTelephone(String findAuthorityTelephone) {
            this.findAuthorityTelephone = findAuthorityTelephone;
        }

        public String getTheftNotes() {
            return theftNotes;
        }

        public void setTheftNotes(String theftNotes) {
            this.theftNotes = theftNotes;
        }

        public String getFindNotes() {
            return findNotes;
        }

        public void setFindNotes(String findNotes) {
            this.findNotes = findNotes;
        }

        @Override
        public String toString() {
            return "TheftExternalRequest{" +
                    "isFound=" + isFound +
                    ", OperationsCenterNotified=" + OperationsCenterNotified +
                    ", theftOccurredOnCenterAld=" + theftOccurredOnCenterAld +
                    ", complaintAuthorityPolice=" + complaintAuthorityPolice +
                    ", complaintAuthorityCc=" + complaintAuthorityCc +
                    ", complaintAuthorityVvuu=" + complaintAuthorityVvuu +
                    ", authorityData='" + authorityData + '\'' +
                    ", authorityTelephone='" + authorityTelephone + '\'' +
                    ", findDate=" + findDate +
                    ", hour='" + hour + '\'' +
                    ", foundAbroad=" + foundAbroad +
                    ", vehicleCo='" + vehicleCo + '\'' +
                    ", sequestered=" + sequestered +
                    ", address=" + address +
                    ", findAuthorityPolice=" + findAuthorityPolice +
                    ", findAuthorityCc=" + findAuthorityCc +
                    ", findAuthorityVvuu=" + findAuthorityVvuu +
                    ", findAuthorityData='" + findAuthorityData + '\'' +
                    ", findAuthorityTelephone='" + findAuthorityTelephone + '\'' +
                    ", theftNotes='" + theftNotes + '\'' +
                    ", findNotes='" + findNotes + '\'' +
                    '}';
        }
    }


    public Boolean getCompleteDocumentation() {
        return isCompleteDocumentation;
    }

    public void setCompleteDocumentation(Boolean completeDocumentation) {
        isCompleteDocumentation = completeDocumentation;
    }

    public ComplaintEnjoyRequest getComplaint() {
        return complaint;
    }

    public void setComplaint(ComplaintEnjoyRequest complaint) {
        this.complaint = complaint;
    }

    public Boolean getWithCounterparty() {
        return isWithCounterparty;
    }

    public void setWithCounterparty(Boolean withCounterparty) {
        isWithCounterparty = withCounterparty;
    }

    public List<CounterpartyEnjoyRequest> getCounterparty() {
        if(counterparty == null){
            return null;
        }
        return new ArrayList<>(counterparty) ;
    }

    public void setCounterparty(List<CounterpartyEnjoyRequest> counterparty) {
        if( counterparty != null) {
            this.counterparty = new ArrayList<>(counterparty);
        }else{
            this.counterparty = null;
        }
    }

    public List<DeponentRequest> getDeponent() {
        if(deponent == null){
            return null;
        }
        return new ArrayList<>(deponent) ;
    }

    public void setDeponent(List<DeponentRequest> deponent) {
        if( deponent != null) {
            this.deponent = new ArrayList<>(deponent);
        }else{
            this.deponent = null;
        }
    }

    public List<WoundedRequest> getWounded() {
        if(wounded == null){
            return null;
        }
        return new ArrayList<>(wounded) ;
    }

    public void setWounded(List<WoundedRequest> wounded) {
        if(wounded != null ) {
            this.wounded = new ArrayList<>(wounded);
        }else{
            wounded = null;
        }
    }

    public DamagedEnjoyRequest getDamaged() {
        return damaged;
    }

    public void setDamaged(DamagedEnjoyRequest damaged) {
        this.damaged = damaged;
    }

    public TheftEnjoyRequest getTheftRequest() {
        return theftRequest;
    }

    public void setTheftRequest(TheftEnjoyRequest theftRequest) {
        this.theftRequest = theftRequest;
    }

    public Long getIdSaleforce() {
        return idSaleforce;
    }

    public void setIdSaleforce(Long idSaleforce) {
        this.idSaleforce = idSaleforce;
    }

    public CaiRequest getCaiDetails() {
        return caiDetails;
    }

    public void setCaiDetails(CaiRequest caiDetails) {
        this.caiDetails = caiDetails;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ClaimsInsertEnjoyRequest{");
        sb.append("complaint=").append(complaint);
        sb.append(", isWithCounterparty=").append(isWithCounterparty);
        sb.append(", counterparty=").append(counterparty);
        sb.append(", deponent=").append(deponent);
        sb.append(", wounded=").append(wounded);
        sb.append(", damaged=").append(damaged);
        sb.append(", theftRequest=").append(theftRequest);
        sb.append(", idSaleforce=").append(idSaleforce);
        sb.append(", caiDetails=").append(caiDetails);
        sb.append(", isCompleteDocumentation=").append(isCompleteDocumentation);
        sb.append('}');
        return sb.toString();
    }
}
