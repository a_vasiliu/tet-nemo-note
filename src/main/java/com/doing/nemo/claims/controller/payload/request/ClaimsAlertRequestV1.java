package com.doing.nemo.claims.controller.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class ClaimsAlertRequestV1 implements Serializable {

    @JsonProperty("claims")
    private ClaimsInsertRequestV1 claimsRequestV1;

    @JsonProperty("alert")
    private AlertRequestV1 alertRequestV1;

    public ClaimsAlertRequestV1() {
    }

    public ClaimsAlertRequestV1(ClaimsInsertRequestV1 claimsRequestV1, AlertRequestV1 alertRequestV1) {
        this.claimsRequestV1 = claimsRequestV1;
        this.alertRequestV1 = alertRequestV1;
    }

    public ClaimsInsertRequestV1 getClaimsRequestV1() {
        return claimsRequestV1;
    }

    public void setClaimsRequestV1(ClaimsInsertRequestV1 claimsRequestV1) {
        this.claimsRequestV1 = claimsRequestV1;
    }

    public AlertRequestV1 getAlertRequestV1() {
        return alertRequestV1;
    }

    public void setAlertRequestV1(AlertRequestV1 alertRequestV1) {
        this.alertRequestV1 = alertRequestV1;
    }

    @Override
    public String toString() {
        return "ClaimsAlertRequestV1{" +
                "claimsRequestV1=" + claimsRequestV1 +
                ", alertRequestV1=" + alertRequestV1 +
                '}';
    }
}
