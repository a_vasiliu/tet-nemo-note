package com.doing.nemo.claims.controller.payload.response.claims;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class ExemptionResponse implements Serializable {

    @JsonProperty("deduct_end_1")
    private String deductEnd1;

    @JsonProperty("deduct_end_2")
    private String deductEnd2;

    @JsonProperty("deduct_paid_1")
    private Double deductPaid1;

    @JsonProperty("deduct_paid_2")
    private Double deductPaid2;

    @JsonProperty("deduct_imported_1")
    private String deductImported1;

    @JsonProperty("deduct_imported_2")
    private String deductImported2;

    @JsonProperty("deduct_total_paid")
    private Double deductTotalPaid;

    @JsonProperty("company_reference")
    private String companyReference;

    @JsonProperty("policy_number")
    private String policyNumber;

    public ExemptionResponse() {
    }

    public ExemptionResponse(String deductEnd1, String deductEnd2, Double deductPaid1, Double deductPaid2, String deductImported1, String deductImported2, Double deductTotalPaid, String companyReference, String policyNumber) {
        this.deductEnd1 = deductEnd1;
        this.deductEnd2 = deductEnd2;
        this.deductPaid1 = deductPaid1;
        this.deductPaid2 = deductPaid2;
        this.deductImported1 = deductImported1;
        this.deductImported2 = deductImported2;
        this.deductTotalPaid = deductTotalPaid;
        this.companyReference = companyReference;
        this.policyNumber = policyNumber;
    }

    public String getDeductEnd1() {
        return deductEnd1;
    }

    public void setDeductEnd1(String deductEnd1) {
        this.deductEnd1 = deductEnd1;
    }

    public String getDeductEnd2() {
        return deductEnd2;
    }

    public void setDeductEnd2(String deductEnd2) {
        this.deductEnd2 = deductEnd2;
    }

    public Double getDeductPaid1() {
        return deductPaid1;
    }

    public void setDeductPaid1(Double deductPaid1) {
        this.deductPaid1 = deductPaid1;
    }

    public Double getDeductPaid2() {
        return deductPaid2;
    }

    public void setDeductPaid2(Double deductPaid2) {
        this.deductPaid2 = deductPaid2;
    }

    public String getDeductImported1() {
        return deductImported1;
    }

    public void setDeductImported1(String deductImported1) {
        this.deductImported1 = deductImported1;
    }

    public String getDeductImported2() {
        return deductImported2;
    }

    public void setDeductImported2(String deductImported2) {
        this.deductImported2 = deductImported2;
    }

    public Double getDeductTotalPaid() {
        return deductTotalPaid;
    }

    public void setDeductTotalPaid(Double deductTotalPaid) {
        this.deductTotalPaid = deductTotalPaid;
    }

    public String getCompanyReference() {
        return companyReference;
    }

    public void setCompanyReference(String companyReference) {
        this.companyReference = companyReference;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    @Override
    public String toString() {
        return "ExemptionResponse{" +
                "deductEnd1=" + deductEnd1 +
                ", deductEnd2=" + deductEnd2 +
                ", deductPaid1=" + deductPaid1 +
                ", deductPaid2=" + deductPaid2 +
                ", deductImported1=" + deductImported1 +
                ", deductImported2=" + deductImported2 +
                ", deductTotalPaid=" + deductTotalPaid +
                ", companyReference='" + companyReference + '\'' +
                ", policyNumber='" + policyNumber + '\'' +
                '}';
    }
}
