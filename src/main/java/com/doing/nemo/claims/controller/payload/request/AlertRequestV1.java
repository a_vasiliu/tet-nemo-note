package com.doing.nemo.claims.controller.payload.request;

import com.doing.nemo.claims.entity.enumerated.AlertEnum.AlertReadersEnum;
import com.doing.nemo.claims.entity.jsonb.forms.jsonbForms.Attachment;
import com.doing.nemo.claims.entity.jsonb.metadata.Metadata;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class AlertRequestV1 implements Serializable {

    @JsonProperty("alert_id")
    private UUID alertId;
    private String module;//Potrebbe essere un enum
    @JsonProperty("created_by")
    private String createdBy;                   //Obbligatorio
    @JsonProperty("user_id")
    private UUID userId;
    private List<AlertReadersEnum> readers;   //Obbligatorio
    @JsonProperty("commodity_id")
    private UUID commodityId;                  //Obbligatorio
    @JsonProperty("ald_commodity_id")
    private Integer aldCommodityId;           //Obbligatorio
    @JsonProperty("ald_order_id")
    private Integer aldOrderId;               //Obbligatorio
    private String subject;                     //Obbligatorio
    private String text;                        //Obbligatorio
    private Metadata metadata;                  //Obbligatorio
    private List<Attachment> attachments;


    public UUID getAlertId() {
        return alertId;
    }

    public void setAlertId(UUID alertId) {
        this.alertId = alertId;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public String getCreatdeBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public List<AlertReadersEnum> getReaders() {
        if(readers == null){
            return null;
        }
        return new ArrayList<>(readers);
    }

    public void setReaders(List<AlertReadersEnum> readers) {
        if(readers != null)
        {
            this.readers = new ArrayList<>(readers);
        }else {
            this.readers = null;
        }
    }

    public UUID getCommodityId() {
        return commodityId;
    }

    public void setCommodityId(UUID commodityId) {
        this.commodityId = commodityId;
    }

    public Integer getAldCommodityId() {
        return aldCommodityId;
    }

    public void setAldCommodityId(Integer aldCommodityId) {
        this.aldCommodityId = aldCommodityId;
    }

    public Integer getAldOrderId() {
        return aldOrderId;
    }

    public void setAldOrderId(Integer aldOrderId) {
        this.aldOrderId = aldOrderId;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Metadata getMetadata() {
        return metadata;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

    public List<Attachment> getAttachments() {
        if(attachments == null)
        {
            return null;
        }
        return new ArrayList<>(attachments);
    }

    public void setAttachments(List<Attachment> attachments) {
        if(attachments != null)
        {
            this.attachments = new ArrayList<>(attachments);
        }else {
            this.attachments = null;
        }
    }

    @Override
    public String toString() {
        return "AlertRequestV1{" +
                "alertId=" + alertId +
                ", module='" + module + '\'' +
                ", createdBy='" + createdBy + '\'' +
                ", userId=" + userId +
                ", readers=" + readers +
                ", commodityId=" + commodityId +
                ", aldCommodityId=" + aldCommodityId +
                ", aldOrderId=" + aldOrderId +
                ", subject='" + subject + '\'' +
                ", text='" + text + '\'' +
                ", metadata=" + metadata +
                ", attachments=" + attachments +
                '}';
    }
}
