package com.doing.nemo.claims.controller.payload.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.Response;

import java.io.IOException;
import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseV2 implements  Serializable{
    @JsonProperty("error")
    private Error error;

    public ResponseV2(){ }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }


    @Override
    public String toString() {
        return "{" +
                "error='" + error + '\'' +
                '}';
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public class Error implements Serializable {
        @JsonProperty("message")
        private String message;

        public Error() {
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        @Override
        public String toString() {
            return "Error{" +
                    "message='" + message + '\'' +
                    '}';
        }
    }
}
