package com.doing.nemo.claims.controller;


import com.doing.nemo.claims.adapter.AuthorityAdapter;
import com.doing.nemo.claims.adapter.PracticeAdapter;
import com.doing.nemo.claims.controller.payload.request.authority.claims.AuthorityRequestV1;
import com.doing.nemo.claims.controller.payload.request.authority.claims.AuthorityTotalRequestV1;
import com.doing.nemo.claims.controller.payload.request.authority.practice.AuthorityPracticeRequestV1;
import com.doing.nemo.claims.controller.payload.request.authority.practice.AuthorityPracticeUpdateRequestV1;
import com.doing.nemo.claims.controller.payload.request.authority.practice.ProcessingRequestV1;
import com.doing.nemo.claims.controller.payload.request.authority.repair.AuthorityRepairRequestV1;
import com.doing.nemo.claims.controller.payload.request.authority.repair.AuthorityRepairTotalRequestV1;
import com.doing.nemo.claims.controller.payload.response.*;
import com.doing.nemo.claims.controller.payload.response.authority.*;
import com.doing.nemo.claims.controller.payload.response.authority.claims.*;
import com.doing.nemo.claims.controller.payload.response.authority.practice.AuthorityPracticeAssociationListResponseV1;
import com.doing.nemo.claims.controller.payload.response.authority.practice.AuthorityPracticePaginationResponseV1;
import com.doing.nemo.claims.controller.payload.response.authority.practice.WorkingAdministrationResponseV1;
import com.doing.nemo.claims.controller.payload.response.authority.repair.AuthorityPaginationRepairResponseV1;
import com.doing.nemo.claims.controller.payload.response.practice.PracticeResponseV1;
import com.doing.nemo.claims.controller.payload.response.registration.PlateRegistrationHistoryItem;
import com.doing.nemo.claims.entity.CounterpartyEntity;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.enumerated.AuthorityEnum.AdministrativePracticeTypeEnum;
import com.doing.nemo.claims.entity.enumerated.AuthorityEnum.AuthorityPracticeTypeEnum;
import com.doing.nemo.claims.exception.NotFoundException;
import com.doing.nemo.claims.service.AuthorityAttachmentService;
import com.doing.nemo.claims.service.AuthorityService;
import com.doing.nemo.claims.service.ESBService;
import com.doing.nemo.claims.validation.MessageCode;
import io.swagger.annotations.*;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/v1")
@Api("Authority")
public class AuthorityController {

    private static Logger LOGGER = LoggerFactory.getLogger(AuthorityController.class);
    @Autowired
    private AuthorityService authorityService;

    @Autowired
    private AuthorityAttachmentService authorityAttachmentService;

    @Autowired
    private ESBService esbService;

    //unica rotta di post put
    //ASSOCIAZIONE CON AUTHORITY
    //REFACTOR
    @PostMapping(value = "/claims/authority",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Link authority working to claims entities",notes = "Link authority working to claims entities")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created" ,responseContainer = "List", response = AuthorityOldestResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity postInsertClaimsAdmin(
            @ApiParam(value = "Body of the authority data", required = true)
            @RequestBody AuthorityRequestV1 authorityRequestV1
    ) {

        authorityService.linkUnlinkAuthority(authorityRequestV1);
        return new ResponseEntity<>(HttpStatus.OK);
    }



    //REFACTOR
    //ROTTA DI APPROVAZIONE
    @PostMapping(value = "/claims/authority/approved",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Link authority working to claims entities", notes = "Link authority working to claims entities")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created" /*, response = ClaimsResponseIdV1.class*/),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity associateClaims(
            @RequestBody AuthorityTotalRequestV1 authorityTotalRequestV1 )
    {
        Map<String,String> claimsNotValid = authorityService.associateClaims(authorityTotalRequestV1);
        return new ResponseEntity(HttpStatus.OK);

    }



    //nuove rotte integrazione authority

    //pagination GO LIVE
    @GetMapping(value = "/claims/workability",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Link authority working to claims entities", notes = "Link authority working to claims entities")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created" , response = ClaimsResponsePaginationV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<Pagination<AuthorityPaginationResponseV1>> getClaimsAuthPaginationWorkability(
            @ApiParam(value = "Defines how many Claims can contain the single page", required = false)
            @RequestParam(value = "page_size", defaultValue = "20") Integer pageSize,
            @ApiParam(value = "Defines the page number to be displayed", required = false)
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @ApiParam(value = "Filter the search by plate", required = false)
            @RequestParam(value = "plate", required = false) String plate,
            @RequestParam(value = "chassis", required = false) String chassis
    ) throws IOException {

        List<String> plates = new ArrayList<>();

        if (!plate.isEmpty()) {
            VehicleRegistrationResponse vehicle = esbService.getVehicleRegistrationsByPlate(plate);

            vehicle.getPlateHistoryItems().stream()
                    .map(PlateRegistrationHistoryItem::getPlate)
                    .forEach(p -> plates.add(p));

            if (plates.contains(plate) == false)
                plates.add(plate);
        }
        return new ResponseEntity(authorityService.paginationAuthorityResponseListFirstCheck(page, pageSize,null, null,plates,chassis), HttpStatus.OK);
    }


    //Pagination
    @GetMapping(value = "/claims/pagination/authority",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Link authority working to claims entities", notes = "Link authority working to claims entities")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created" , response = ClaimsResponsePaginationV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<Pagination<AuthorityPaginationResponseV1>> getClaimsAuthPagination(
            @ApiParam(value = "Defines how many Claims can contain the single page", required = false)
            @RequestParam(value = "page_size", defaultValue = "20") Integer pageSize,
            @ApiParam(value = "Defines the page number to be displayed", required = false)
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @ApiParam(value = "Filter the search by plate", required = false)
            @RequestParam(value = "plate", required = false) String plate,
            @RequestParam(value = "chassis", required = false) String chassis
    ) throws IOException {

        List<String> plates = new ArrayList<>();
        if (!plate.isEmpty()) {

            VehicleRegistrationResponse vehicle = esbService.getVehicleRegistrationsByPlate(plate);

            vehicle.getPlateHistoryItems().stream()
                    .map(PlateRegistrationHistoryItem::getPlate)
                    .forEach(p -> plates.add(p));

            if (plates.contains(plate) == false)
                plates.add(plate);
        }

        return new ResponseEntity(authorityService.paginationAuthorityResponseListSecondCheck(page, pageSize,null, null,plates,chassis), HttpStatus.OK);
    }


    //REFACTOR
    @GetMapping(value = "/claims/authority/oldest/{DOSSIER_ID}/{WORKING_ID}",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Link authority working to claims entities", notes = "Link authority working to claims entities")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created" , response = AuthorityOldestResponseV1.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<AuthorityOldestResponseV1>> getClaimsOldest(
            @PathVariable(value = "DOSSIER_ID") String dossierId,
            @PathVariable(value = "WORKING_ID") String workingId
    ) {

        return new ResponseEntity(authorityService.getOldestClaimsByDossierAndWorking(dossierId,workingId), HttpStatus.OK);
    }

    //REFACTOR
    @GetMapping(value = "/claims/authority/detail/{CLAIMS_ID}/{WORKING_ID}",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Link authority working to claims entities", notes = "Link authority working to claims entities")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created" , response = AuthorityClaimsResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<AuthorityClaimsResponseV1> getClaimsOldestProva(
            @PathVariable(value = "CLAIMS_ID") String claimsId,
            @PathVariable(value = "WORKING_ID") String workingId
    ) throws IOException {

        return new ResponseEntity(authorityService.getAuthorityPracticeByWorkingId(claimsId,workingId), HttpStatus.OK);
    }


    //REFACTOR
    @GetMapping(value = "/repair/authority/{PLATE}",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Link authority working to claims entities", notes = "Link authority working to claims entities")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created" , response = CounterpartyAuthorityResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<CounterpartyAuthorityResponseV1> getRepairByPlate(
            @ApiParam(value = "Filter the search by plate", required = true)
            @PathVariable(name = "PLATE", required = false) String plate
    ) {
        List<CounterpartyEntity> counterpartyEntities = authorityService.getCounterpartyByPlate(plate);
        if(counterpartyEntities == null || counterpartyEntities.isEmpty()){
            String message = String.format(MessageCode.CLAIMS_1123.value(), plate);
            throw new NotFoundException(message);
        }
        return new ResponseEntity(AuthorityAdapter.adptFromCounterpartyToAuthorityRepairResponse(counterpartyEntities.get(0)), HttpStatus.OK);
    }

    //REFACTOR
    @GetMapping(value = "/repair/pagination/authority",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Link authority working to claims entities", notes = "Link authority working to claims entities")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created" , response = ClaimsResponsePaginationV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<Pagination<AuthorityPaginationRepairResponseV1>> getRepairPagination(
            @ApiParam(value = "Defines how many Claims can contain the single page", required = false)
            @RequestParam(value = "page_size", defaultValue = "20") Integer pageSize,
            @ApiParam(value = "Defines the page number to be displayed", required = false)
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @ApiParam(value = "Filter the search by plate", required = false)
            @RequestParam(value = "plate", required = false) String plate,
            @RequestParam(value = "chassis", required = false) String chassis
    ) {

        return new ResponseEntity(authorityService.paginationRepairAuthorityResponseList(page, pageSize, null, null, plate, chassis), HttpStatus.OK);
    }


    //REFACTOR
    @PostMapping(value = "/repair/authority",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Link authority working to repair entities",notes = "Link authority working to repair entities")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity postRepairAuthorityAssociation(
            @ApiParam(value = "Body of the authority data", required = true)
            @RequestBody AuthorityRepairRequestV1 authorityRequestV1
    ) {

        authorityService.linkUnlinkAuthorityRepair(authorityRequestV1);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    //REFACTOR
    @PostMapping(value = "/repair/authority/approved",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Link authority working to repair entities",notes = "Link authority working to repair entities")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity postRepairAuthorityApproved(
            @ApiParam(value = "Body of the authority data", required = true)
            @RequestBody AuthorityRepairTotalRequestV1 authorityRequestV1
    ) {
        authorityService.associateCounterparty(authorityRequestV1);
        //authorityService.linkUnlinkAuthority(authorityRequestV1);
        return new ResponseEntity<>(HttpStatus.OK);
    }


    //REFACTOR
    @GetMapping(value = "/claims/authority/attachments/{claim_uuid}",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Recover authority working attachment", notes = "Recover authority working attachment")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created" , response = AuthorityWorkingAttachmentResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<AuthorityWorkingsAttachmentResponseV1> getAuthorityAttachmentClaims(
            @PathVariable("claim_uuid") String claimId
    ) throws IOException {

        return new ResponseEntity(authorityAttachmentService.getWorkingsAttachmentByClaimId(claimId), HttpStatus.OK);
    }

    //REFACTOR
    @GetMapping(value = "/repair/authority/attachments/{repair_uuid}",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Recover authority working attachment", notes = "Recover authority working attachment")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created" , response = AuthorityWorkingAttachmentResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<AuthorityWorkingsAttachmentResponseV1> getAuthorityAttachmentRepair(
            @PathVariable("repair_uuid") String repairId
    ) throws IOException {

        return new ResponseEntity(authorityAttachmentService.getWorkingsAttachmentByRepairId(repairId), HttpStatus.OK);
    }

    //INTEGRAZIONE PRATICHE AMMINISTRATIVE


    @GetMapping(value = "/practice/pagination/authority",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Get practice entities", notes = "Get practice entities by plate or chassis")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created" , response = ClaimsResponsePaginationV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<Pagination<AuthorityPracticePaginationResponseV1>> getPracticePagination(
            @ApiParam(value = "Defines how many Claims can contain the single page", required = false)
            @RequestParam(value = "page_size", defaultValue = "20") Integer pageSize,
            @ApiParam(value = "Defines the page number to be displayed", required = false)
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @ApiParam(value = "Filter the search by plate", required = false)
            @RequestParam(value = "plate", required = false) String plate,
            @RequestParam(value = "chassis", required = false) String chassis,
            @RequestParam(value = "type") List<AuthorityPracticeTypeEnum> type
    ) {
        //return new ResponseEntity<>(new Pagination<>(), HttpStatus.OK);
        return new ResponseEntity(authorityService.paginationPracticeAuthorityResponseList(page, pageSize, plate, chassis, type), HttpStatus.OK);
    }

    @PostMapping(value = "/practice/authority",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Link authority practice to practice entities",notes = "Link authority practice to practice entities")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<AuthorityPracticeAssociationListResponseV1> postPracticeAuthorityAssociation(
            @ApiParam(value = "Body of the authority data", required = true)
            @RequestBody AuthorityPracticeRequestV1 authorityRequestV1
    ) {

        return new ResponseEntity<>(authorityService.linkUnlinkAuthorityPractice(authorityRequestV1), HttpStatus.OK);
    }

    @PostMapping(value = "/practice/authority/approved",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Update authority practice",notes = "Update authority practice linked to practice entities")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity postPracticeAuthorityApproved(
            @ApiParam(value = "Body of the authority data", required = true)
            @RequestBody AuthorityPracticeUpdateRequestV1 authorityRequestV1
    ) {
        authorityService.associatePractice(authorityRequestV1);
        //authorityService.linkUnlinkAuthority(authorityRequestV1);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(value = "/practice/administrative/authority/{plateOrChassis}",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Get practice entities", notes = "Get practice entities by plate or chassis")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created" , response = ClaimsResponsePaginationV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<PaginationResponseV1<WorkingAdministrationResponseV1>> getPracticeAdministrativePagination(
            @ApiParam(value = "Defines how many Practice Administrative can contain the single page")
            @RequestParam(value = "page_size", defaultValue = "20") Integer pageSize,
            @ApiParam(value = "Defines the page number to be displayed")
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @RequestParam(value = "order_by",  defaultValue = "created_at", required = false) String orderBy,
            @RequestParam(value = "asc", defaultValue = "false", required = false) Boolean asc,
            @ApiParam(value = "Filter the search by plate or chassis", required = true)
            @PathVariable(name = "plateOrChassis") String plateOrChassis,
            @ApiParam(value = "Filter the search by practice administrative type", required = true)
            @RequestParam(value = "type", required = false) AdministrativePracticeTypeEnum type,
            @RequestHeader("nemo-user-id") String userId,
            @RequestHeader("nemo-profiles-tree") String profiles
    ) throws IOException {
        return new ResponseEntity(authorityService.paginationPracticeAdministrative(page, pageSize, orderBy, asc, plateOrChassis, type, userId, profiles), HttpStatus.OK);
    }

    @PostMapping(value = "/practice/administrative/authority/{ID_PRACTICE}",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Update authority practice",notes = "Update authority practice linked to practice entities")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<PracticeResponseV1> postPracticeAdministrative(
            @ApiParam(value = "UUID of the practice", required = true)
            @PathVariable(name = "ID_PRACTICE") UUID practiceId,
            @ApiParam(value = "Body of the authority data", required = true)
            @RequestBody List<ProcessingRequestV1> processingRequestV1s
    ) throws IOException {

        //authorityService.linkUnlinkAuthority(authorityRequestV1);
        return new ResponseEntity<>(PracticeAdapter.adptPracticeEntityToPracticeResponseV1(authorityService.associateProcessingAdministrative(practiceId, processingRequestV1s)),HttpStatus.OK);
    }

    @PutMapping(value = "/practice/administrative/authority/{ID_PRACTICE}",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Update authority practice",notes = "Update authority practice linked to practice entities")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<PracticeResponseV1> putPracticeAdministrative(
            @ApiParam(value = "UUID of the practice", required = true)
            @PathVariable(name = "ID_PRACTICE") UUID practiceId,
            @ApiParam(value = "Body of the authority data", required = true)
            @RequestBody List<ProcessingRequestV1> processingRequestV1s
    ) throws IOException {

        //authorityService.linkUnlinkAuthority(authorityRequestV1);
        return new ResponseEntity<>(PracticeAdapter.adptPracticeEntityToPracticeResponseV1(authorityService.disassociateProcessingAdministrative(practiceId, processingRequestV1s)),HttpStatus.OK);
    }
}
