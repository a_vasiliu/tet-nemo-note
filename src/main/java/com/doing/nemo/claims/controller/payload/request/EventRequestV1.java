package com.doing.nemo.claims.controller.payload.request;

import com.doing.nemo.claims.entity.settings.EventTypeEntity;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class EventRequestV1 implements Serializable {

    @JsonProperty("event_type")
    private EventTypeEntity eventType;

    @JsonProperty("user_id")
    private String userId;

    @JsonProperty("description")
    private String description = "";

    public EventRequestV1(EventTypeEntity eventType, String userId, String description) {
        this.eventType = eventType;
        this.userId = userId;
        this.description = description;
    }

    public EventRequestV1() {
    }

    public EventTypeEntity getEventType() {
        return eventType;
    }

    public void setEventType(EventTypeEntity eventType) {
        this.eventType = eventType;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "EventRequestV1{" +
                "eventType=" + eventType +
                ", userId=" + userId +
                ", description='" + description + '\'' +
                '}';
    }
}
