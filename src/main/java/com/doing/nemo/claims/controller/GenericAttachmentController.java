package com.doing.nemo.claims.controller;

import com.doing.nemo.claims.adapter.GenericAttachmentAdapter;
import com.doing.nemo.claims.controller.payload.request.UploadFileRequestV1;
import com.doing.nemo.claims.controller.payload.response.GenericAttachmentResponsePaginationV1;
import com.doing.nemo.claims.controller.payload.response.GenericAttachmentResponseV1;
import com.doing.nemo.claims.entity.enumerated.ClaimsRepairEnum;
import com.doing.nemo.claims.entity.settings.GenericAttachmentEntity;
import com.doing.nemo.claims.repository.GenericAttachmentRepository;
import com.doing.nemo.claims.repository.GenericAttachmentRepositoryV1;
import com.doing.nemo.claims.service.GenericAttachmentService;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.NotFoundException;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/v1")
public class GenericAttachmentController {

    private static Logger LOGGER = LoggerFactory.getLogger(GenericAttachmentController.class);
    @Autowired
    private GenericAttachmentService genericAttachmentService;
    @Autowired
    private GenericAttachmentRepository genericAttachmentRepository;
    @Autowired
    private GenericAttachmentRepositoryV1 genericAttachmentRepositoryV1;
    @Autowired
    private GenericAttachmentAdapter genericAttachmentAdapter;

    @PostMapping(value = "/genericattachment")
    public ResponseEntity postImportExemptionFile(@ApiParam(value = "File to be upload", required = true)
                                                  @RequestBody UploadFileRequestV1 uploadFileRequest,
                                                  @ApiParam(value = "Defines the status o attachment")
                                                  @RequestParam(value = "is_active", defaultValue = "true") Boolean isActive,
                                                  @ApiParam(value = "TypeEnum of complaint", required = true)
                                                  @RequestParam(value = "type_complaint") ClaimsRepairEnum typeComplaint) {

        return new ResponseEntity<>(genericAttachmentService.uploadGenericAttachment(uploadFileRequest, isActive, typeComplaint), HttpStatus.OK);
    }

    @ApiOperation(value = "Generic Attachment pagination", notes = "")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = GenericAttachmentResponsePaginationV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @GetMapping(value = "/genericattachment/pagination", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<GenericAttachmentResponsePaginationV1> searchGenericAttachment(
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @RequestParam(value = "page_size", defaultValue = "20") Integer pageSize,
            @RequestParam(value = "type_complaint", required = false) String typeComplaint
    ) {

        List<GenericAttachmentEntity> listGenericAttachment = genericAttachmentRepositoryV1.findPaginationGenericAttachment(page, pageSize, typeComplaint);
        Long count = genericAttachmentRepositoryV1.countPaginationGenericAttachment(typeComplaint);

        GenericAttachmentResponsePaginationV1 genericAttachmentPaginationResponseV1 = GenericAttachmentAdapter.adptPagination(listGenericAttachment, page, pageSize, count);

        return new ResponseEntity<>(genericAttachmentPaginationResponseV1, HttpStatus.OK);
    }

    @ApiOperation(value = "Generic Attachment by UUID", notes = "")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = GenericAttachmentResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @GetMapping(value = "/genericattachment/{UUID_GENERICATTACHMENT}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<GenericAttachmentResponseV1> getGenericAttachmentById(
            @ApiParam(value = "UUID that identifies the generic attachment", required = true)
            @PathVariable(name = "UUID_GENERICATTACHMENT") String dataImportId
    ) {

        Optional<GenericAttachmentEntity> optionalGenericAttachment = genericAttachmentRepository.findById(UUID.fromString(dataImportId));
        GenericAttachmentResponseV1 searchGenericAttachmentResponse = null;
        if (optionalGenericAttachment.isPresent()) {
            searchGenericAttachmentResponse = GenericAttachmentAdapter.adptGenericAttachmentToGenericAttachmentResponse(optionalGenericAttachment.get());
            return new ResponseEntity<>(searchGenericAttachmentResponse, HttpStatus.OK);
        }
        LOGGER.debug("Generic Attachment with ID not found");
        throw new NotFoundException("Generic Attachment with ID not found", MessageCode.CLAIMS_1010);
    }

    /*@ApiOperation(value="Generic Attachment by UUID", notes = "")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = GenericAttachmentResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PutMapping(value="/genericattachment/{UUID_GENERICATTACHMENT}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<GenericAttachmentResponseV1> putGenericAttachmentById(
            @ApiParam(value = "UUID that identifies the generic attachment", required = true)
            @PathVariable(name = "UUID_GENERICATTACHMENT")  String dataImportId,
            @ApiParam(value = "File to be upload", required = true)
            @RequestBody UploadFileRequestV1 uploadFileRequest
    ){

        Optional<GenericAttachmentEntity> optionalGenericAttachment = genericAttachmentRepository.findById(UUID.fromString(dataImportId));

        //fare update
        if(optionalGenericAttachment.isPresent()){
            GenericAttachmentEntity genericAttachmentEntity =optionalGenericAttachment.get();

            genericAttachmentService.uploadGenericAttachmentUpdate(uploadFileRequest, genericAttachmentEntity.getId());
            GenericAttachmentResponseV1 searchGenericAttachmentResponse = GenericAttachmentAdapter.adptGenericAttachmentToGenericAttachmentResponse(optionalGenericAttachment.get());
            return new ResponseEntity<>(searchGenericAttachmentResponse, HttpStatus.OK);
        }

        throw new NotFoundException("Generic Attachment with ID not found", MessageCode.CLAIMS_1010);
    }*/


    @ApiOperation(value = "Delete Generic Attachment by UUID", notes = "")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = GenericAttachmentResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @DeleteMapping(value = "/genericattachment/{UUID_GENERICATTACHMENT}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity deleteGenericAttachmentById(
            @ApiParam(value = "UUID that identifies the generic attachment", required = true)
            @PathVariable(name = "UUID_GENERICATTACHMENT") String dataImportId
    ) {

        Optional<GenericAttachmentEntity> optionalGenericAttachment = genericAttachmentRepository.findById(UUID.fromString(dataImportId));

        //fare update
        if (optionalGenericAttachment.isPresent()) {
            GenericAttachmentEntity genericAttachmentEntity = optionalGenericAttachment.get();
            genericAttachmentService.deleteGenericAttachment(genericAttachmentEntity);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        LOGGER.debug("Generic Attachment with ID not found");
        throw new NotFoundException("Generic Attachment with ID not found", MessageCode.CLAIMS_1010);
    }


    @GetMapping(value = "/genericattachment",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Recover Generic Attachments", notes = "Returns all the Generic Attachments")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = GenericAttachmentResponseV1.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<GenericAttachmentResponseV1>> getAllGenericAttachment(
            @ApiParam(value = "TypeEnum of complaint", required = true)
            @RequestParam(value = "type_complaint") ClaimsRepairEnum typeComplaint
    ) {

        List<GenericAttachmentEntity> genericAttachmentEntities = genericAttachmentService.getAllGenericAttachment(typeComplaint);
        List<GenericAttachmentResponseV1> response = GenericAttachmentAdapter.adptGenericAttachmentToGenericAttachmentResponse(genericAttachmentEntities);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


    @PatchMapping("/genericattachment/{UUID}/active")
    @Transactional
    @ApiOperation(value = "Patch status active GenericAttachment", notes = "Patch status active GenericAttachment using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = GenericAttachmentResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<GenericAttachmentResponseV1> patchStatusGenericAttachment(
            @PathVariable UUID UUID
    ) {
        GenericAttachmentEntity genericAttachmentEntity = genericAttachmentService.updateStatusGenericAttachment(UUID);
        GenericAttachmentResponseV1 responseV1 = GenericAttachmentAdapter.adptGenericAttachmentToGenericAttachmentResponse(genericAttachmentEntity);
        return new ResponseEntity<>(responseV1, HttpStatus.OK);
    }

}
