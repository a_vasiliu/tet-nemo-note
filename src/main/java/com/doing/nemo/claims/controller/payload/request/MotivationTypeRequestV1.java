package com.doing.nemo.claims.controller.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

public class MotivationTypeRequestV1 implements Serializable {


    @JsonProperty("description")
    @NotNull
    private String description = "";

    @JsonProperty("order")
    @NotNull
    private Integer order;

    @JsonProperty("is_active")
    private Boolean isActive = true;


    public MotivationTypeRequestV1(@NotNull String description, @NotNull Integer order, Boolean isActive) {
        this.description = description;
        this.order = order;
        this.isActive = isActive;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public Boolean getActive() {
        return isActive;
    }

    @JsonSetter
    public void setActive(Boolean active) {
        if (active != null)
            isActive = active;
    }

    @Override
    public String toString() {
        return "MotivationTypeRequestV1{" +
                "description='" + description + '\'' +
                ", order=" + order +
                '}';
    }
}
