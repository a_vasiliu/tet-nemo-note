package com.doing.nemo.claims.controller.payload.response;

import com.doing.nemo.claims.entity.enumerated.DTO.PageStats;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class DataManagementExportResponsePaginationV1 implements Serializable {

    private PageStats stats;
    private List<DataManagementExportResponseV1> items;

    public DataManagementExportResponsePaginationV1() {
    }

    public DataManagementExportResponsePaginationV1(PageStats stats, List<DataManagementExportResponseV1> items) {
        this.stats = stats;
        if(items != null)
        {
            this.items = new ArrayList<>(items);
        }
    }

    public PageStats getStats() {
        return stats;
    }

    public void setStats(PageStats stats) {
        this.stats = stats;
    }

    public List<DataManagementExportResponseV1> getItems() {
        if(items == null){
            return null;
        }
        return new ArrayList<>(items);
    }

    public void setItems(List<DataManagementExportResponseV1> items) {
        if(items != null)
        {
            this.items = new ArrayList<>(items);
        } else {
            this.items = null;
        }
    }

}
