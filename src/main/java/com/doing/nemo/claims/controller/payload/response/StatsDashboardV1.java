package com.doing.nemo.claims.controller.payload.response;

import java.io.Serializable;

public class StatsDashboardV1 implements Serializable {
    private Integer tot;
    private Integer ful;
    private Integer fcm;
    private Integer fni;

    public StatsDashboardV1() {}

    public Integer getTot() {
        return tot;
    }

    public void setTot(Integer tot) {
        this.tot = tot;
    }

    public Integer getFul() {
        return ful;
    }

    public void setFul(Integer ful) {
        this.ful = ful;
    }

    public Integer getFcm() {
        return fcm;
    }

    public void setFcm(Integer fcm) {
        this.fcm = fcm;
    }

    public Integer getFni() {
        return fni;
    }

    public void setFni(Integer fni) {
        this.fni = fni;
    }

    @Override
    public String toString() {
        return "StatsDashboardV1{" +
                "tot=" + tot +
                ", ful=" + ful +
                ", fcm=" + fcm +
                ", fni=" + fni +
                '}';
    }
}
