package com.doing.nemo.claims.controller.payload.response.external;


import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class FormsExternalResponse  implements Serializable {
    @JsonProperty("attachment")
    private List<AttachmentExternalResponse> attachment;

    public List<AttachmentExternalResponse> getAttachment() {
        if(attachment == null){
            return null;
        }
        return new ArrayList<>(attachment);
    }

    public void setAttachment(List<AttachmentExternalResponse> attachment) {
        if(attachment != null)
        {
            this.attachment = new ArrayList<>(attachment);
        } else {
            this.attachment = null;
        }
    }
}
