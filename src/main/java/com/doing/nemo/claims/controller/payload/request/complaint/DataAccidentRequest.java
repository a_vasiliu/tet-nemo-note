package com.doing.nemo.claims.controller.payload.request.complaint;

import com.doing.nemo.claims.controller.payload.request.complaint.dataaccident.ReceivedItemRequest;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.jsonb.Address;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class DataAccidentRequest implements Serializable {

    @JsonProperty("type_accident")
    private DataAccidentTypeAccidentEnum typeAccident;

    @JsonProperty("responsible")
    private Boolean responsible = false;

    @JsonProperty("date_accident")
    private String dateAccident;

    @JsonProperty("happened_abroad")
    private Boolean happenedAbroad = false;

    @JsonProperty("address")
    private Address address;

    @JsonProperty("damage_to_objects")
    private Boolean damageToObjects = false;

    @JsonProperty("damage_to_vehicles")
    private Boolean damageToVehicles = false;

    //Da eliminare probabilmente
    @JsonProperty("old_motorcycle_plates")
    private String oldMotorcyclePlates;

    @JsonProperty("intervention_authority")
    private Boolean interventionAuthority = false;

    @JsonProperty("police")
    private Boolean police = false;

    @JsonProperty("cc")
    private Boolean cc = false;

    @JsonProperty("vvuu")
    private Boolean vvuu = false;

    @JsonProperty("authority_data")
    private String authorityData;

    @JsonProperty("witness_description")
    private String witnessDescription;

    @JsonProperty("recoverability")
    private Boolean recoverability = false;

    @JsonProperty("recoverability_percent")
    private Double recoverabilityPercent;

    @JsonProperty("incomplete_motivation")
    private String incompleteMotivation;

    @JsonProperty("is_robbery")
    private Boolean robbery = false;

    @JsonProperty("center_notified")
    private Boolean centerNotified = false;

    @JsonProperty("happened_on_center")
    private Boolean happenedOnCenter = false;

    @JsonProperty("provider_code")
    private String providerCode;

    @JsonProperty("theft_description")
    private String theftDescription;

    @JsonProperty("item_to_receive")
    private List<ReceivedItemRequest> itemToReceive;

    //Questo dovrebbe non servire, in quanto viene aggiornato in base all'attributo presente a livello del claims
    @JsonProperty("is_with_counterparty")
    private Boolean isWithCounterparty = false;

    public DataAccidentTypeAccidentEnum getTypeAccident() {
        return typeAccident;
    }

    public void setTypeAccident(DataAccidentTypeAccidentEnum typeAccident) {
        this.typeAccident = typeAccident;
    }

    public Boolean getResponsible() {
        return responsible;
    }

    public void setResponsible(Boolean responsible) {
        this.responsible = responsible;
    }

    public String getDateAccident() {
        return dateAccident;
    }

    public void setDateAccident(String dateAccident) {
        this.dateAccident = dateAccident;
    }

    public Boolean getHappenedAbroad() {
        return happenedAbroad;
    }

    public void setHappenedAbroad(Boolean happenedAbroad) {
        this.happenedAbroad = happenedAbroad;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Boolean getDamageToObjects() {
        return damageToObjects;
    }

    public void setDamageToObjects(Boolean damageToObjects) {
        this.damageToObjects = damageToObjects;
    }

    public Boolean getDamageToVehicles() {
        return damageToVehicles;
    }

    public void setDamageToVehicles(Boolean damageToVehicles) {
        this.damageToVehicles = damageToVehicles;
    }

    public String getOldMotorcyclePlates() {
        return oldMotorcyclePlates;
    }

    public void setOldMotorcyclePlates(String oldMotorcyclePlates) {
        this.oldMotorcyclePlates = oldMotorcyclePlates;
    }

    public Boolean getInterventionAuthority() {
        return interventionAuthority;
    }

    public void setInterventionAuthority(Boolean interventionAuthority) {
        this.interventionAuthority = interventionAuthority;
    }

    public Boolean getPolice() {
        return police;
    }

    public void setPolice(Boolean police) {
        this.police = police;
    }

    public Boolean getCc() {
        return cc;
    }

    public void setCc(Boolean cc) {
        this.cc = cc;
    }

    public Boolean getVvuu() {
        return vvuu;
    }

    public void setVvuu(Boolean vvuu) {
        this.vvuu = vvuu;
    }

    public String getAuthorityData() {
        return authorityData;
    }

    public void setAuthorityData(String authorityData) {
        this.authorityData = authorityData;
    }

    public String getWitnessDescription() {
        return witnessDescription;
    }

    public void setWitnessDescription(String witnessDescription) {
        this.witnessDescription = witnessDescription;
    }

    public Boolean getRecoverability() {
        return recoverability;
    }

    public void setRecoverability(Boolean recoverability) {
        this.recoverability = recoverability;
    }

    public Double getRecoverabilityPercent() {
        return recoverabilityPercent;
    }

    public void setRecoverabilityPercent(Double recoverabilityPercent) {
        this.recoverabilityPercent = recoverabilityPercent;
    }

    public String getIncompleteMotivation() {
        return incompleteMotivation;
    }

    public void setIncompleteMotivation(String incompleteMotivation) {
        this.incompleteMotivation = incompleteMotivation;
    }

    public Boolean getRobbery() {
        return robbery;
    }

    public void setRobbery(Boolean robbery) {
        this.robbery = robbery;
    }

    public Boolean getCenterNotified() {
        return centerNotified;
    }

    public void setCenterNotified(Boolean centerNotified) {
        this.centerNotified = centerNotified;
    }

    public Boolean getHappenedOnCenter() {
        return happenedOnCenter;
    }

    public void setHappenedOnCenter(Boolean happenedOnCenter) {
        this.happenedOnCenter = happenedOnCenter;
    }

    public String getProviderCode() {
        return providerCode;
    }

    public void setProviderCode(String providerCode) {
        this.providerCode = providerCode;
    }

    public String getTheftDescription() {
        return theftDescription;
    }

    public void setTheftDescription(String theftDescription) {
        this.theftDescription = theftDescription;
    }

    public List<ReceivedItemRequest> getItemToReceive() {
        if(itemToReceive == null){
            return null;
        }
        return new ArrayList<>(itemToReceive);
    }

    public void setItemToReceive(List<ReceivedItemRequest> itemToReceive) {
        if(itemToReceive != null)
        {
            this.itemToReceive = new ArrayList<>(itemToReceive);
        } else {
            this.itemToReceive = null;
        }
    }

    @JsonIgnore
    public Boolean getWithCounterparty() {
        return isWithCounterparty;
    }

    public void setWithCounterparty(Boolean withCounterparty) {
        isWithCounterparty = withCounterparty;
    }

    @Override
    public String toString() {
        return "DataAccident{" +
                "typeAccident=" + typeAccident +
                ", responsible=" + responsible +
                ", dateAccident=" + dateAccident +
                ", happenedAbroad=" + happenedAbroad +
                ", address=" + address +
                ", damageToObjects=" + damageToObjects +
                ", damageToVehicles=" + damageToVehicles +
                ", oldMotorcyclePlates='" + oldMotorcyclePlates + '\'' +
                ", interventionAuthority=" + interventionAuthority +
                ", police=" + police +
                ", cc=" + cc +
                ", vvuu=" + vvuu +
                ", authorityData='" + authorityData + '\'' +
                ", witnessDescription='" + witnessDescription + '\'' +
                ", recoverability=" + recoverability +
                ", recoverabilityPercent=" + recoverabilityPercent +
                ", incompleteMotivation='" + incompleteMotivation + '\'' +
                ", isRobbery=" + robbery +
                ", centerNotified=" + centerNotified +
                ", happenedOnCenter=" + happenedOnCenter +
                ", providerCode='" + providerCode + '\'' +
                ", theftDescription='" + theftDescription + '\'' +
                ", itemToReceive=" + itemToReceive +
                ", isWithCounterparty=" + isWithCounterparty +
                '}';
    }


}
