package com.doing.nemo.claims.controller.payload.request.practice;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Date;

public class SeizureRequestV1 implements Serializable {

    private static final long serialVersionUID = 809173064960583663L;

    @JsonProperty("seizure_date")
    private Date misappropriationDate;

    @JsonProperty("return_good_faith")
    private Boolean returnGoodFaith;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Date getMisappropriationDate() {
        if(misappropriationDate == null){
            return null;
        }
        return (Date)misappropriationDate.clone();
    }

    public void setMisappropriationDate(Date misappropriationDate) {
        if(misappropriationDate != null)
        {
            this.misappropriationDate = (Date)misappropriationDate.clone();
        } else {
            this.misappropriationDate = null;
        }
    }

    public Boolean getReturnGoodFaith() {
        return returnGoodFaith;
    }

    public void setReturnGoodFaith(Boolean returnGoodFaith) {
        this.returnGoodFaith = returnGoodFaith;
    }

    @Override
    public String toString() {
        return "Seizure{" +
                "misappropriationDate=" + misappropriationDate +
                ", returnGoodFaith=" + returnGoodFaith +
                '}';
    }
}
