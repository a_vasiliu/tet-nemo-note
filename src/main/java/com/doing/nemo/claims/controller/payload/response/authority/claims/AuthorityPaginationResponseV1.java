package com.doing.nemo.claims.controller.payload.response.authority.claims;

import com.doing.nemo.claims.controller.payload.response.authority.claims.ComplaintAuthorityResponseV1;
import com.doing.nemo.claims.controller.payload.response.authority.claims.DamagedAuthorityResponseV1;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsFlowEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.doing.nemo.claims.entity.enumerated.WorkabilitySystemEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AuthorityPaginationResponseV1  implements Serializable {

    @JsonProperty("id")
    private String id;

    @JsonProperty("practice_id")
    private Long practiceId;

    @JsonProperty("created_at")
    private String createdAt;

    @JsonProperty("damaged")
    private DamagedAuthorityResponseV1 damaged;

    @JsonProperty("complaint")
    private ComplaintAuthorityResponseV1 complaint;

    @JsonProperty("is_associable")
    private Boolean isAssociable;

    @JsonProperty("is_repair")
    private Boolean isRepair;

    @JsonProperty("type")
    private ClaimsFlowEnum type;

    @JsonProperty("event_description")
    private String eventDescription;

    @JsonProperty("claim_description")
    private String claimDescription;

    @JsonProperty("authority_practice_linked")
    private List<String> authorityPracticeLinked;

    @JsonProperty("claim_status")
    private ClaimsStatusEnum claimStatus;

    @JsonProperty("source_type")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private WorkabilitySystemEnum sourceType;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getPracticeId() {
        return practiceId;
    }

    public void setPracticeId(Long practiceId) {
        this.practiceId = practiceId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public DamagedAuthorityResponseV1 getDamaged() {
        return damaged;
    }

    public void setDamaged(DamagedAuthorityResponseV1 damaged) {
        this.damaged = damaged;
    }

    public ComplaintAuthorityResponseV1 getComplaint() {
        return complaint;
    }

    public void setComplaint(ComplaintAuthorityResponseV1 complaint) {
        this.complaint = complaint;
    }

    @JsonIgnore
    public Boolean getAssociable() {
        return isAssociable;
    }

    public void setAssociable(Boolean associable) {
        isAssociable = associable;
    }

    @JsonIgnore
    public Boolean getRepair() {
        return isRepair;
    }

    public void setRepair(Boolean repair) {
        isRepair = repair;
    }

    public ClaimsFlowEnum getType() {
        return type;
    }

    public void setType(ClaimsFlowEnum type) {
        this.type = type;
    }

    public String getEventDescription() {
        return eventDescription;
    }

    public void setEventDescription(String eventDescription) {
        this.eventDescription = eventDescription;
    }

    public String getClaimDescription() {
        return claimDescription;
    }

    public void setClaimDescription(String claimDescription) {
        this.claimDescription = claimDescription;
    }

    public ClaimsStatusEnum getClaimStatus() {
        return claimStatus;
    }

    public void setClaimStatus(ClaimsStatusEnum claimStatus) {
        this.claimStatus = claimStatus;
    }

    public List<String> getAuthorityPracticeLinked() {
        if(authorityPracticeLinked == null){
            return null;
        }
        return new ArrayList<>(authorityPracticeLinked);
    }

    public void setAuthorityPracticeLinked(List<String> authorityPracticeLinked) {
        if(authorityPracticeLinked != null)
        {
            this.authorityPracticeLinked = new ArrayList<>(authorityPracticeLinked);
        } else {
            this.authorityPracticeLinked = null;
        }
    }

    public WorkabilitySystemEnum getSourceType() {
        return sourceType;
    }

    public void setSourceType(WorkabilitySystemEnum sourceType) {
        this.sourceType = sourceType;
    }
}
