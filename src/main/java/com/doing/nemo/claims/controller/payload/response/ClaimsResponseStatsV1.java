package com.doing.nemo.claims.controller.payload.response;

import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ClaimsResponseStatsV1 implements Serializable {

    @JsonProperty("stats")
    private List<StatsElementResponse> stats;

    public ClaimsResponseStatsV1() {
        this.stats = new LinkedList<>();
    }

    public List<StatsElementResponse> getStats() {
        if(stats == null){
            return null;
        }
        return new ArrayList<>(stats);
    }

    public void setStats(List<StatsElementResponse> statsElem) {
        if(statsElem != null)
        {
            this.stats = new ArrayList<>(statsElem);
        }
    }

    public StatsElementResponse getStats(ClaimsStatusEnum claimsStatus) {

        for (StatsElementResponse sta : this.getStats()) {
            if (sta.getClaimsStatus().equals(claimsStatus)) {
                return sta;
            }
        }
        return null;
    }

    public static class StatsElementResponse {

        @JsonProperty("claims_status")
        private ClaimsStatusEnum claimsStatus;

        @JsonProperty("TOT_claims")
        private Integer total;

            /*@JsonInclude(JsonInclude.Include.NON_NULL)
            @JsonProperty("client_list")
            private List<ClientListResponse> clientList;*/

        public StatsElementResponse() {
            this.total = 0;
            // this.clientList = new LinkedList<>();
        }

        public StatsElementResponse(ClaimsStatusEnum claimsStatus, Integer total) {
            this.claimsStatus = claimsStatus;
            this.total = total;
        }

        public ClaimsStatusEnum getClaimsStatus() {
            return claimsStatus;
        }

        public void setClaimsStatus(ClaimsStatusEnum claimsStatus) {
            this.claimsStatus = claimsStatus;
        }

        public Integer getTotal() {
            return total;
        }

        public void setTotal(Integer total) {
            this.total = total;
        }

           /* @JsonIgnore
            public List<ClientListResponse> getClientList() {
                return clientList;
            }

            public void setClientList(List<ClientListResponse> clientList) {
                this.clientList = clientList;
            }*/


        @Override
        public String toString() {
            return "Stats{" +
                    "claimsStatus=" + claimsStatus +
                    ", total=" + total +
                    '}';
        }

        public static class CustomerListResponse {

            @JsonProperty("customer_id")
            private Long customerId;

            @JsonProperty("number_claims_customer")
            private Integer numberClaimsCustomer;

            public CustomerListResponse() {
            }

            public CustomerListResponse(Long customerId, Integer numberClaimsCustomer) {
                this.customerId = customerId;
                this.numberClaimsCustomer = numberClaimsCustomer;
            }

            public Long getCustomerId() {
                return customerId;
            }

            public void setCustomerId(Long customerId) {
                this.customerId = customerId;
            }

            public Integer getNumberClaimsCustomer() {
                return numberClaimsCustomer;
            }

            public void setNumberClaimsCustomer(Integer numberClaimsCustomer) {
                this.numberClaimsCustomer = numberClaimsCustomer;
            }

            @Override
            public String toString() {
                return "CustomerListResponse{" +
                        "customerId=" + customerId +
                        ", numberClaimsCustomer=" + numberClaimsCustomer +
                        '}';
            }
        }
    }

}
