package com.doing.nemo.claims.controller.payload.request;

import com.doing.nemo.claims.controller.payload.request.messaging.Identity;
import com.doing.nemo.claims.controller.payload.request.messaging.Template;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SendMailTemplateRequestV1 implements Serializable {

    @Size(min = 1)
    @JsonProperty("tos")
    private List<Identity> tos = new ArrayList<>();

    @JsonProperty("ccs")
    private List<Identity> ccs = new ArrayList<>();

    @JsonProperty("bccs")
    private List<Identity> bccs = new ArrayList<>();

    @JsonProperty("replay_to")
    private Identity replyTo;

    @NotNull
    @Valid
    @JsonProperty("template")
    private Template template;

    @JsonProperty("filemanager_id_attachment_list")
    private List<String> filemanagerIdAttachmentList = new ArrayList<>();

    public List<Identity> getTos() {
        if(tos == null){
            return null;
        }
        return new ArrayList<>(tos);
    }

    public void setTos(List<Identity> tos) {
        if(tos != null)
        {
            this.tos = new ArrayList<>(tos);
        } else {
            this.tos = null;
        }
    }

    public List<Identity> getCcs() {
        if(ccs == null){
            return null;
        }
        return new ArrayList<>(ccs);
    }

    public void setCcs(List<Identity> ccs) {
        if(ccs != null)
        {
            this.ccs = new ArrayList<>(ccs);
        } else {
            this.ccs = null;
        }
    }

    public List<Identity> getBccs() {
        if(bccs == null){
            return null;
        }
        return new ArrayList<>(bccs);
    }

    public void setBccs(List<Identity> bccs) {
        if(bccs != null)
        {
            this.bccs = new ArrayList<>(bccs);
        } else {
            this.bccs = null;
        }
    }

    public Identity getReplyTo() {
        return replyTo;
    }

    public void setReplyTo(Identity replyTo) {
        this.replyTo = replyTo;
    }

    public Template getTemplate() {
        return template;
    }

    public void setTemplate(Template template) {
        this.template = template;
    }

    public List<String> getFilemanagerIdAttachmentList() {
        if(filemanagerIdAttachmentList == null){
            return null;
        }
        return new ArrayList<>(filemanagerIdAttachmentList);
    }

    public void setFilemanagerIdAttachmentList(List<String> filemanagerIdAttachmentList) {
        if(filemanagerIdAttachmentList != null)
        {
            this.filemanagerIdAttachmentList = new ArrayList<>(filemanagerIdAttachmentList);
        } else {
            this.filemanagerIdAttachmentList = null;
        }
    }
}
