package com.doing.nemo.claims.controller.payload.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class ContractPatchResponseV1 implements Serializable {

    @JsonProperty("start_date")
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    private String startDate;

    @JsonProperty("end_date")
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    private String endDate;

    @JsonProperty("voucher_id")
    private Integer voucherId;

    @JsonProperty("cod_pack")
    private String codPack;

    public ContractPatchResponseV1() {
    }

    public ContractPatchResponseV1(String startDate, String endDate, Integer voucherId, String codPack) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.voucherId = voucherId;
        this.codPack = codPack;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Integer getVoucherId() {
        return voucherId;
    }

    public void setVoucherId(Integer voucherId) {
        this.voucherId = voucherId;
    }

    public String getCodPack() {
        return codPack;
    }

    public void setCodPack(String codPack) {
        this.codPack = codPack;
    }

    @Override
    public String toString() {
        return "ContractPatchResponseV1{" +
                "startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", voucherId=" + voucherId +
                ", codPack='" + codPack + '\'' +
                '}';
    }
}
