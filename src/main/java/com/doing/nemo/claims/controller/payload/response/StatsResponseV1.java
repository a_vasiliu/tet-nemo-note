package com.doing.nemo.claims.controller.payload.response;


import java.io.Serializable;

public class StatsResponseV1<T>  implements Serializable {

    private T stats;

    public StatsResponseV1() {
    }

    public StatsResponseV1(T stats) {
        this.stats = stats;
    }

    public T getStats() {
        return stats;
    }

    public void setStats(T stats) {
        this.stats = stats;
    }
}
