package com.doing.nemo.claims.controller.payload.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ClaimRequest implements Serializable {

    @JsonProperty("confirmed")
    private Boolean confirmed;

    @JsonProperty("confirmationdate")
    private String confirmationDate;

    @JsonProperty("requestdate")
    private Instant requestDate;

    @JsonProperty("amount")
    private Double amount;

    @JsonProperty("claimstatus_enumid")
    private Integer claimStatusEnumId;

    @JsonProperty("description")
    private String description;

    @JsonProperty("reference")
    private String reference;

    @JsonProperty("acceptance_enumid")
    private Integer acceptanceEnumId;

    @JsonProperty("rejectionreason_enumid")
    private Integer rejectionReasonEnumId;

    @JsonProperty("paymentcommunication")
    private String paymentCommunication;

    @JsonProperty("paymentmethod_enumid")
    private Integer paymentMethodEnumid;

    @JsonProperty("claimcategory_enumid")
    private Integer claimCategoryEnumId;

    @JsonProperty("receivedamount")
    private Double receivedamount;

    @JsonProperty("outstandingamount")
    private Double outstandingamount;

    @JsonProperty("vatcode_enumid")
    private Integer vatCodeEnumId;

    @JsonProperty("servicetype_enumid")
    private Integer serviceTypeEnumId;

    @JsonProperty("policynumber")
    private String policyNumber;

    @JsonProperty("filenumber")
    private String fileNumber;

    @JsonProperty("fileopendate")
    private String fileOpenDate;

    @JsonProperty("fileclosedate")
    private String fileCloseDate;

    @JsonProperty("liabilitycode")
    private String liabilityCode;

    @JsonProperty("damagetype")
    private String damageType;

    @JsonProperty("nature_enumid")
    private Integer natureEnumId;

    @JsonProperty("paymentcomplete")
    private Boolean paymentComplete;

    @JsonProperty("paymentdate")
    private Instant paymentDate;

    public ClaimRequest() {
    }

    public ClaimRequest(Boolean confirmed, String confirmationDate, Instant requestDate, Double amount, Integer claimStatusEnumId, String description, String reference, Integer acceptanceEnumId, Integer rejectionReasonEnumId, String paymentCommunication, Integer paymentMethodEnumid, Integer claimCategoryEnumId, Double receivedamount, Double outstandingamount, Integer vatCodeEnumId, Integer serviceTypeEnumId, String policyNumber, String fileNumber, String fileOpenDate, String fileCloseDate, String liabilityCode, String damageType, Integer natureEnumId, Boolean paymentComplete, Instant paymentDate) {
        this.confirmed = confirmed;
        this.confirmationDate = confirmationDate;
        this.requestDate = requestDate;
        this.amount = amount;
        this.claimStatusEnumId = claimStatusEnumId;
        this.description = description;
        this.reference = reference;
        this.acceptanceEnumId = acceptanceEnumId;
        this.rejectionReasonEnumId = rejectionReasonEnumId;
        this.paymentCommunication = paymentCommunication;
        this.paymentMethodEnumid = paymentMethodEnumid;
        this.claimCategoryEnumId = claimCategoryEnumId;
        this.receivedamount = receivedamount;
        this.outstandingamount = outstandingamount;
        this.vatCodeEnumId = vatCodeEnumId;
        this.serviceTypeEnumId = serviceTypeEnumId;
        this.policyNumber = policyNumber;
        this.fileNumber = fileNumber;
        this.fileOpenDate = fileOpenDate;
        this.fileCloseDate = fileCloseDate;
        this.liabilityCode = liabilityCode;
        this.damageType = damageType;
        this.natureEnumId = natureEnumId;
        this.paymentComplete = paymentComplete;
        this.paymentDate = paymentDate;
    }

    public Boolean getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(Boolean confirmed) {
        this.confirmed = confirmed;
    }

    public String getConfirmationDate() {
        return confirmationDate;
    }

    public void setConfirmationDate(String confirmationDate) {
        this.confirmationDate = confirmationDate;
    }

    public Instant getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Instant requestDate) {
        this.requestDate = requestDate;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Integer getClaimStatusEnumId() {
        return claimStatusEnumId;
    }

    public void setClaimStatusEnumId(Integer claimStatusEnumId) {
        this.claimStatusEnumId = claimStatusEnumId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Integer getAcceptanceEnumId() {
        return acceptanceEnumId;
    }

    public void setAcceptanceEnumId(Integer acceptanceEnumId) {
        this.acceptanceEnumId = acceptanceEnumId;
    }

    public Integer getRejectionReasonEnumId() {
        return rejectionReasonEnumId;
    }

    public void setRejectionReasonEnumId(Integer rejectionReasonEnumId) {
        this.rejectionReasonEnumId = rejectionReasonEnumId;
    }

    public String getPaymentCommunication() {
        return paymentCommunication;
    }

    public void setPaymentCommunication(String paymentCommunication) {
        this.paymentCommunication = paymentCommunication;
    }

    public Integer getPaymentMethodEnumid() {
        return paymentMethodEnumid;
    }

    public void setPaymentMethodEnumid(Integer paymentMethodEnumid) {
        this.paymentMethodEnumid = paymentMethodEnumid;
    }

    public Integer getClaimCategoryEnumId() {
        return claimCategoryEnumId;
    }

    public void setClaimCategoryEnumId(Integer claimCategoryEnumId) {
        this.claimCategoryEnumId = claimCategoryEnumId;
    }

    public Double getReceivedamount() {
        return receivedamount;
    }

    public void setReceivedamount(Double receivedamount) {
        this.receivedamount = receivedamount;
    }

    public Double getOutstandingamount() {
        return outstandingamount;
    }

    public void setOutstandingamount(Double outstandingamount) {
        this.outstandingamount = outstandingamount;
    }

    public Integer getVatCodeEnumId() {
        return vatCodeEnumId;
    }

    public void setVatCodeEnumId(Integer vatCodeEnumId) {
        this.vatCodeEnumId = vatCodeEnumId;
    }

    public Integer getServiceTypeEnumId() {
        return serviceTypeEnumId;
    }

    public void setServiceTypeEnumId(Integer serviceTypeEnumId) {
        this.serviceTypeEnumId = serviceTypeEnumId;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public String getFileNumber() {
        return fileNumber;
    }

    public void setFileNumber(String fileNumber) {
        this.fileNumber = fileNumber;
    }

    public String getFileOpenDate() {
        return fileOpenDate;
    }

    public void setFileOpenDate(String fileOpenDate) {
        this.fileOpenDate = fileOpenDate;
    }

    public String getFileCloseDate() {
        return fileCloseDate;
    }

    public void setFileCloseDate(String fileCloseDate) {
        this.fileCloseDate = fileCloseDate;
    }

    public String getLiabilityCode() {
        return liabilityCode;
    }

    public void setLiabilityCode(String liabilityCode) {
        this.liabilityCode = liabilityCode;
    }

    public String getDamageType() {
        return damageType;
    }

    public void setDamageType(String damageType) {
        this.damageType = damageType;
    }

    public Integer getNatureEnumId() {
        return natureEnumId;
    }

    public void setNatureEnumId(Integer natureEnumId) {
        this.natureEnumId = natureEnumId;
    }

    public Boolean getPaymentComplete() {
        return paymentComplete;
    }

    public void setPaymentComplete(Boolean paymentComplete) {
        this.paymentComplete = paymentComplete;
    }

    public Instant getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Instant paymentDate) {
        this.paymentDate = paymentDate;
    }

    @Override
    public String toString() {
        return "ClaimRequest{" +
                "confirmed=" + confirmed +
                ", confirmationDate='" + confirmationDate + '\'' +
                ", requestDate='" + requestDate + '\'' +
                ", amount=" + amount +
                ", claimStatusEnumId=" + claimStatusEnumId +
                ", description='" + description + '\'' +
                ", reference='" + reference + '\'' +
                ", acceptanceEnumId=" + acceptanceEnumId +
                ", rejectionReasonEnumId=" + rejectionReasonEnumId +
                ", paymentCommunication='" + paymentCommunication + '\'' +
                ", paymentMethodEnumid=" + paymentMethodEnumid +
                ", claimCategoryEnumId=" + claimCategoryEnumId +
                ", receivedamount=" + receivedamount +
                ", outstandingamount=" + outstandingamount +
                ", vatCodeEnumId=" + vatCodeEnumId +
                ", serviceTypeEnumId=" + serviceTypeEnumId +
                ", policyNumber='" + policyNumber + '\'' +
                ", fileNumber='" + fileNumber + '\'' +
                ", fileOpenDate='" + fileOpenDate + '\'' +
                ", fileCloseDate='" + fileCloseDate + '\'' +
                ", liabilityCode='" + liabilityCode + '\'' +
                ", damageType='" + damageType + '\'' +
                ", natureEnumId=" + natureEnumId +
                ", paymentComplete=" + paymentComplete +
                ", paymentDate='" + paymentDate + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClaimRequest that = (ClaimRequest) o;
        return Objects.equals(getConfirmed(), that.getConfirmed()) &&
                Objects.equals(getConfirmationDate(), that.getConfirmationDate()) &&
                Objects.equals(getRequestDate(), that.getRequestDate()) &&
                Objects.equals(getAmount(), that.getAmount()) &&
                Objects.equals(getClaimStatusEnumId(), that.getClaimStatusEnumId()) &&
                Objects.equals(getDescription(), that.getDescription()) &&
                Objects.equals(getReference(), that.getReference()) &&
                Objects.equals(getAcceptanceEnumId(), that.getAcceptanceEnumId()) &&
                Objects.equals(getRejectionReasonEnumId(), that.getRejectionReasonEnumId()) &&
                Objects.equals(getPaymentCommunication(), that.getPaymentCommunication()) &&
                Objects.equals(getPaymentMethodEnumid(), that.getPaymentMethodEnumid()) &&
                Objects.equals(getClaimCategoryEnumId(), that.getClaimCategoryEnumId()) &&
                Objects.equals(getReceivedamount(), that.getReceivedamount()) &&
                Objects.equals(getOutstandingamount(), that.getOutstandingamount()) &&
                Objects.equals(getVatCodeEnumId(), that.getVatCodeEnumId()) &&
                Objects.equals(getServiceTypeEnumId(), that.getServiceTypeEnumId()) &&
                Objects.equals(getPolicyNumber(), that.getPolicyNumber()) &&
                Objects.equals(getFileNumber(), that.getFileNumber()) &&
                Objects.equals(getFileOpenDate(), that.getFileOpenDate()) &&
                Objects.equals(getFileCloseDate(), that.getFileCloseDate()) &&
                Objects.equals(getLiabilityCode(), that.getLiabilityCode()) &&
                Objects.equals(getDamageType(), that.getDamageType()) &&
                Objects.equals(getNatureEnumId(), that.getNatureEnumId()) &&
                Objects.equals(getPaymentComplete(), that.getPaymentComplete()) &&
                Objects.equals(getPaymentDate(), that.getPaymentDate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getConfirmed(), getConfirmationDate(), getRequestDate(), getAmount(), getClaimStatusEnumId(), getDescription(), getReference(), getAcceptanceEnumId(), getRejectionReasonEnumId(), getPaymentCommunication(), getPaymentMethodEnumid(), getClaimCategoryEnumId(), getReceivedamount(), getOutstandingamount(), getVatCodeEnumId(), getServiceTypeEnumId(), getPolicyNumber(), getFileNumber(), getFileOpenDate(), getFileCloseDate(), getLiabilityCode(), getDamageType(), getNatureEnumId(), getPaymentComplete(), getPaymentDate());
    }
}