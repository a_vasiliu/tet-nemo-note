package com.doing.nemo.claims.controller.payload.request;

import com.doing.nemo.claims.controller.payload.request.claims.*;
import com.doing.nemo.claims.controller.payload.request.exemption.ExemptionRequest;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ClaimsPatchRequestV1 implements Serializable{

    @JsonProperty("notes")
    private List<NoteRequest> notes;

    @JsonProperty("cai_details")
    private CaiRequest cai;

    @JsonProperty("forms")
    private FormRequest forms;

    @JsonProperty("pai_comunication")
    private Boolean paiComunication;

    @JsonProperty("legal_comunication")
    private Boolean legalComunication;

    @JsonProperty("complaint")
    private ComplaintRequest complaint;

    @JsonProperty("is_with_counterparty")
    private Boolean isWithCounterparty;

    @JsonProperty("exemption")
    private ExemptionRequest exemption;

    @JsonProperty("counterparty")
    private List<CounterpartyRequest> counterparty;

    @JsonProperty("deponent")
    private List<DeponentRequest> deponentList;

    @JsonProperty("wounded")
    private List<WoundedRequest> woundedList;

    @JsonProperty("damaged")
    private DamagedRequest damaged;

    @JsonProperty("found_model")
    private FoundModelRequest foundModel;

    @JsonProperty("id_saleforce")
    private Long idSaleforce;

    @JsonProperty("forced")
    private Boolean forced;

    @JsonProperty("in_evidence")
    private Boolean inEvidence;

    @JsonProperty("refund")
    private RefundRequest refund;

    @JsonProperty("theft")
    private TheftRequestV1 theft;

    @JsonProperty("po_variation")
    private Boolean poVariation;

    @JsonProperty("is_complete_documentation")
    private Boolean isCompleteDocumentation ;

    public Boolean getCompleteDocumentation() {
        return isCompleteDocumentation;
    }

    public void setCompleteDocumentation(Boolean completeDocumentation) {
        isCompleteDocumentation = completeDocumentation;
    }

    public Boolean getLegalComunication() {
        return legalComunication;
    }

    public void setLegalComunication(Boolean legalComunication) {
        this.legalComunication = legalComunication;
    }

    public Boolean getPoVariation() {
        return poVariation;
    }

    public void setPoVariation(Boolean poVariation) {
        this.poVariation = poVariation;
    }

    public Boolean getForced() {
        return forced;
    }

    public ExemptionRequest getExemption() {
        return exemption;
    }

    public void setExemption(ExemptionRequest exemption) {
        this.exemption = exemption;
    }

    public void setForced(Boolean forced) {
        this.forced = forced;
    }

    public Boolean getInEvidence() {
        return inEvidence;
    }

    public void setInEvidence(Boolean inEvidence) {
        this.inEvidence = inEvidence;
    }

    public TheftRequestV1 getTheft() {
        return theft;
    }

    public void setTheft(TheftRequestV1 theft) {
        this.theft = theft;
    }

    public List<NoteRequest> getNotes() {
        if(notes == null){
            return null;
        }
        return new ArrayList<>(notes) ;
    }

    public void setNotes(List<NoteRequest> notes) {
        if(notes != null)
        {
            this.notes = new ArrayList<>(notes) ;
        } else {
            this.notes = null;
        }
    }

    public CaiRequest getCai() {
        return cai;
    }

    public void setCai(CaiRequest cai) {
        this.cai = cai;
    }

    public FormRequest getForms() {
        return forms;
    }

    public void setForms(FormRequest forms) {
        this.forms = forms;
    }

    public Boolean getPaiComunication() {
        return paiComunication;
    }

    public void setPaiComunication(Boolean paiComunication) {
        this.paiComunication = paiComunication;
    }

    public ComplaintRequest getComplaint() {
        return complaint;
    }

    public void setComplaint(ComplaintRequest complaint) {
        this.complaint = complaint;
    }

    public Boolean getWithCounterparty() {
        return isWithCounterparty;
    }

    public void setWithCounterparty(Boolean withCounterparty) {
        isWithCounterparty = withCounterparty;
    }

    public List<CounterpartyRequest> getCounterparty() {
        if(counterparty == null){
            return null;
        }
        return new ArrayList<>(counterparty) ;
    }

    public void setCounterparty(List<CounterpartyRequest> counterparty) {
        if(counterparty != null)
        {
            this.counterparty = new ArrayList<>(counterparty) ;
        }else {
            this.counterparty = null;
        }
    }

    public List<DeponentRequest> getDeponentList() {
        if(deponentList == null){
            return null;
        }
        return new ArrayList<>(deponentList);
    }

    public void setDeponentList(List<DeponentRequest> deponentList) {
        if(deponentList != null)
        {
            this.deponentList = new ArrayList<>(deponentList);
        }else {
            this.deponentList = null;
        }
    }

    public List<WoundedRequest> getWoundedList() {
        if(woundedList == null){
            return null;
        }
        return new ArrayList<>(woundedList);
    }

    public void setWoundedList(List<WoundedRequest> woundedList) {
        if(woundedList != null)
        {
            this.woundedList =  new ArrayList<>(woundedList);
        } else {
            this.woundedList = null;
        }
    }

    public DamagedRequest getDamaged() {
        return damaged;
    }

    public void setDamaged(DamagedRequest damaged) {
        this.damaged = damaged;
    }

    public FoundModelRequest getFoundModel() {
        return foundModel;
    }

    public void setFoundModel(FoundModelRequest foundModel) {
        this.foundModel = foundModel;
    }

    public Long getIdSaleforce() {
        return idSaleforce;
    }

    public void setIdSaleforce(Long idSaleforce) {
        this.idSaleforce = idSaleforce;
    }

    public RefundRequest getRefund() {
        return refund;
    }

    public void setRefund(RefundRequest refund) {
        this.refund = refund;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ClaimsPatchRequestV1{");
        sb.append("notes=").append(notes);
        sb.append(", cai=").append(cai);
        sb.append(", forms=").append(forms);
        sb.append(", paiComunication=").append(paiComunication);
        sb.append(", legalComunication=").append(legalComunication);
        sb.append(", complaint=").append(complaint);
        sb.append(", isWithCounterparty=").append(isWithCounterparty);
        sb.append(", exemption=").append(exemption);
        sb.append(", counterparty=").append(counterparty);
        sb.append(", deponentList=").append(deponentList);
        sb.append(", woundedList=").append(woundedList);
        sb.append(", damaged=").append(damaged);
        sb.append(", foundModel=").append(foundModel);
        sb.append(", idSaleforce=").append(idSaleforce);
        sb.append(", forced=").append(forced);
        sb.append(", inEvidence=").append(inEvidence);
        sb.append(", refund=").append(refund);
        sb.append(", theft=").append(theft);
        sb.append(", poVariation=").append(poVariation);
        sb.append(", isCompleteDocumentation=").append(isCompleteDocumentation);
        sb.append('}');
        return sb.toString();
    }
}


