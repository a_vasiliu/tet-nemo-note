package com.doing.nemo.claims.controller.payload.request;

import com.doing.nemo.claims.entity.settings.DmSystemsEntity;
import com.doing.nemo.claims.entity.settings.NotificationTypeEntity;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;

import java.io.Serializable;


public class ExportNotificationTypeRequestV1 implements Serializable {

    @JsonProperty("notification_type")
    private NotificationTypeEntity notificationType;

    @JsonProperty("dm_system_id")
    private DmSystemsEntity dmSystem;

    @JsonProperty("code")
    private String code;

    @JsonProperty("description")
    private String description;

    @JsonProperty("is_active")
    private Boolean isActive = true;

    public ExportNotificationTypeRequestV1() {
    }

    public ExportNotificationTypeRequestV1(NotificationTypeEntity notificationType, DmSystemsEntity dmSystem, String code, String description, Boolean isActive) {
        this.notificationType = notificationType;
        this.dmSystem = dmSystem;
        this.code = code;
        this.description = description;
        this.isActive = isActive;
    }

    public NotificationTypeEntity getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(NotificationTypeEntity notificationType) {
        this.notificationType = notificationType;
    }

    public DmSystemsEntity getDmSystem() {
        return dmSystem;
    }

    public void setDmSystem(DmSystemsEntity dmSystem) {
        this.dmSystem = dmSystem;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getActive() {
        return isActive;
    }

    @JsonSetter
    public void setActive(Boolean active) {
        if(active != null)
        isActive = active;
    }

    @Override
    public String toString() {
        return "ExportNotificationTypeRequestV1{" +
                "notificationType=" + notificationType +
                ", dmSystem=" + dmSystem +
                ", code='" + code + '\'' +
                ", description='" + description + '\'' +
                ", isActive=" + isActive +
                '}';
    }
}
