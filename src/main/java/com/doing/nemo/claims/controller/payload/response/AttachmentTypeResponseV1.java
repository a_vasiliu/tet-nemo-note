package com.doing.nemo.claims.controller.payload.response;

import com.doing.nemo.claims.entity.enumerated.AttachmentEnum.GroupsEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsRepairEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.UUID;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AttachmentTypeResponseV1 implements Serializable {

    private UUID id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("groups")
    private GroupsEnum groups;

    @JsonProperty("is_active")
    private Boolean isActive;

    @JsonProperty("type_complaint")
    private ClaimsRepairEnum typeComplaint;

    public AttachmentTypeResponseV1() {
    }

    public AttachmentTypeResponseV1(UUID id, String name, GroupsEnum groups, Boolean isActive, ClaimsRepairEnum typeComplaint) {
        this.id = id;
        this.name = name;
        this.groups = groups;
        this.isActive = isActive;
        this.typeComplaint = typeComplaint;
    }

    @JsonIgnore
    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public GroupsEnum getGroups() {
        return groups;
    }

    public void setGroups(GroupsEnum groups) {
        this.groups = groups;
    }

    public ClaimsRepairEnum getTypeComplaint() {
        return typeComplaint;
    }

    public void setTypeComplaint(ClaimsRepairEnum typeComplaint) {
        this.typeComplaint = typeComplaint;
    }

    @Override
    public String toString() {
        return "AttachmentTypeResponseV1{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", groups=" + groups +
                ", isActive=" + isActive +
                '}';
    }
}
