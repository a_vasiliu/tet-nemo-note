package com.doing.nemo.claims.controller.payload.response.practice;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Date;

public class ReleaseFromSeizureResponseV1 implements Serializable {

    private static final long serialVersionUID = 809173064960583663L;

    @JsonProperty("note")
    private String note;

    @JsonProperty("provider")
    private String provider;

    @JsonProperty("provider_code")
    private String providerCode;

    @JsonProperty("seizure_date")
    private Date seizureDate;

    @JsonProperty("release_from_seizure_date")
    private Date releaseFromSeizureDate;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getProviderCode() {
        return providerCode;
    }

    public void setProviderCode(String providerCode) {
        this.providerCode = providerCode;
    }

    public Date getSeizureDate() {
        if(seizureDate == null){
            return null;
        }
        return (Date)seizureDate.clone();
    }

    public void setSeizureDate(Date seizureDate) {
        if(seizureDate != null)
        {
            this.seizureDate = (Date)seizureDate.clone();
        } else {
            this.seizureDate = null;
        }
    }

    public Date getReleaseFromSeizureDate() {
        if(releaseFromSeizureDate == null){
            return null;
        }
        return (Date)releaseFromSeizureDate.clone();
    }

    public void setReleaseFromSeizureDate(Date releaseFromSeizureDate) {
        if(releaseFromSeizureDate != null)
        {
            this.releaseFromSeizureDate = (Date)releaseFromSeizureDate.clone();
        } else {
            this.releaseFromSeizureDate = null;
        }
    }

    @Override
    public String toString() {
        return "ReleaseFromSeizure{" +
                "note='" + note + '\'' +
                ", provider='" + provider + '\'' +
                ", providerCode='" + providerCode + '\'' +
                ", seizureDate=" + seizureDate +
                ", releaseFromSeizureDate=" + releaseFromSeizureDate +
                '}';
    }
}