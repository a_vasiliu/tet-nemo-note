package com.doing.nemo.claims.controller.payload.response;

import com.doing.nemo.claims.entity.enumerated.NotesEnum.NotesTypeEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class ClaimsResponseNoteV1 implements Serializable {

    @JsonProperty("title")
    private String noteTitle;

    @JsonProperty("note_type")
    private NotesTypeEnum noteType;

    @JsonProperty("note_id")
    private String noteId;

    @JsonProperty("is_important")
    private Boolean isImportant;

    @JsonProperty("description")
    private String description;

    @JsonProperty("user_id")
    private String userId;

    @JsonProperty("created_at")
    private String createdAt;

    @JsonProperty("hidden")
    private Boolean hidden;

    public ClaimsResponseNoteV1() {
    }

    public ClaimsResponseNoteV1(String noteTitle, NotesTypeEnum noteType, String noteId, Boolean isImportant, String description, String userId, String createdAt, Boolean hidden) {
        this.noteTitle = noteTitle;
        this.noteType = noteType;
        this.noteId = noteId;
        this.isImportant = isImportant;
        this.description = description;
        this.userId = userId;
        this.createdAt = createdAt;
        this.hidden = hidden;
    }

    public String getNoteTitle() {
        return noteTitle;
    }

    public void setNoteTitle(String noteTitle) {
        this.noteTitle = noteTitle;
    }

    public NotesTypeEnum getNoteType() {
        return noteType;
    }

    public void setNoteType(NotesTypeEnum noteType) {
        this.noteType = noteType;
    }

    public String getNoteId() {
        return noteId;
    }

    public void setNoteId(String noteId) {
        this.noteId = noteId;
    }

    @JsonIgnore
    public Boolean getImportant() {
        return isImportant;
    }

    public void setImportant(Boolean important) {
        isImportant = important;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @JsonIgnore
    public Boolean getHidden() {
        return hidden;
    }

    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }

    @Override
    public String toString() {
        return "ClaimsResponseNoteV1{" +
                "noteTitle='" + noteTitle + '\'' +
                ", noteType=" + noteType +
                ", noteId='" + noteId + '\'' +
                ", isImportant=" + isImportant +
                ", description='" + description + '\'' +
                ", userId='" + userId + '\'' +
                ", createdAt=" + createdAt +
                '}';
    }
}
