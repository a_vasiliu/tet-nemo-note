package com.doing.nemo.claims.controller.payload.response;

import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ClaimsResponseStatsEvidenceV1 implements Serializable {

    @JsonProperty("stats")
    private List<StatsElementEvidenceResponse> stats;

    public ClaimsResponseStatsEvidenceV1() {
        this.stats = new LinkedList<>();
    }

    public List<StatsElementEvidenceResponse> getStats() {
        if(stats == null){
            return null;
        }
        return new ArrayList<>(stats);
    }

    public void setStats(List<StatsElementEvidenceResponse> statsElem) {
        if(statsElem != null)
        {
            this.stats = new ArrayList<>(statsElem);
        } else {
            this.stats = null;
        }
    }

    public StatsElementEvidenceResponse getStats(ClaimsStatusEnum claimsStatus) {

        for (StatsElementEvidenceResponse sta : this.getStats()) {
            if (sta.getClaimsStatus().equals(claimsStatus)) {
                return sta;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return "ClaimsResponseStatsEvidenceV1{" +
                "stats=" + stats +
                '}';
    }

    public static class StatsElementEvidenceResponse {

        @JsonProperty("claims_status")
        private ClaimsStatusEnum claimsStatus;

        @JsonProperty("ful")
        private InternalCounterResponse ful;

        @JsonProperty("fcm")
        private InternalCounterResponse fcm;

        @JsonProperty("fni")
        private InternalCounterResponse fni;

        /*@JsonInclude(JsonInclude.Include.NON_NULL)
        @JsonProperty("client_list")
        private List<ClaimsResponseStatsV1.StatsElementResponse.ClientListResponse> clientList;*/

        public StatsElementEvidenceResponse() {

        }

        public StatsElementEvidenceResponse(ClaimsStatusEnum claimsStatus, InternalCounterResponse ful, InternalCounterResponse fcm, InternalCounterResponse fni) {
            this.claimsStatus = claimsStatus;
            this.ful = ful;
            this.fcm = fcm;
            this.fni = fni;
        }

        public ClaimsStatusEnum getClaimsStatus() {
            return claimsStatus;
        }

        public void setClaimsStatus(ClaimsStatusEnum claimsStatus) {
            this.claimsStatus = claimsStatus;
        }

        public InternalCounterResponse getFul() {
            return ful;
        }

        public void setFul(InternalCounterResponse ful) {
            this.ful = ful;
        }

        public InternalCounterResponse getFcm() {
            return fcm;
        }

        public void setFcm(InternalCounterResponse fcm) {
            this.fcm = fcm;
        }

        public InternalCounterResponse getFni() {
            return fni;
        }

        public void setFni(InternalCounterResponse fni) {
            this.fni = fni;
        }

        @Override
        public String toString() {
            return "StatsElementEvidenceResponse{" +
                    "claimsStatus=" + claimsStatus +
                    ", ful=" + ful +
                    ", fcm=" + fcm +
                    ", fni=" + fni +
                    '}';
        }

        public static class InternalCounterResponse{

            @JsonProperty("tot_claims")
            private Integer total;

            @JsonProperty("in_evidence")
            private Integer inEvidence;

            @JsonProperty("with_continuation")
            private Integer withContinuation;

            @JsonProperty("with_continuation_in_evidence")
            private Integer withContinuationInEvidence;

            public InternalCounterResponse() {
                this.total = 0;
            }

            public Integer getTotal() {
                return total;
            }

            public void setTotal(Integer total) {
                this.total = total;
            }

            public Integer getInEvidence() {
                return inEvidence;
            }

            public void setInEvidence(Integer inEvidence) {
                this.inEvidence = inEvidence;
            }

            public Integer getWithContinuation() {
                return withContinuation;
            }

            public void setWithContinuation(Integer withContinuation) {
                this.withContinuation = withContinuation;
            }

            public Integer getWithContinuationInEvidence() {
                return withContinuationInEvidence;
            }

            public void setWithContinuationInEvidence(Integer withContinuationInEvidence) {
                this.withContinuationInEvidence = withContinuationInEvidence;
            }
        }
    }
}
