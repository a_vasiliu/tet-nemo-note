package com.doing.nemo.claims.controller.payload.request.insurancecompany;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class TheftRequest implements Serializable {


    @JsonProperty("service_id")
    private Long serviceId;
    @JsonProperty("service")
    private String service;
    @JsonProperty("deductible_id")
    private Long deductibleId;
    @JsonProperty("deductible")
    private String deductible;

    public Long getServiceId() {
        return serviceId;
    }

    public void setServiceId(Long serviceId) {
        this.serviceId = serviceId;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public Long getDeductibleId() {
        return deductibleId;
    }

    public void setDeductibleId(Long deductibleId) {
        this.deductibleId = deductibleId;
    }

    public String getDeductible() {
        return deductible;
    }

    public void setDeductible(String deductible) {
        this.deductible = deductible;
    }
}
