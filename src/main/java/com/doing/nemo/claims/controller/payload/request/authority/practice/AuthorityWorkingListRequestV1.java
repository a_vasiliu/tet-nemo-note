package com.doing.nemo.claims.controller.payload.request.authority.practice;

import com.doing.nemo.claims.entity.enumerated.AuthorityEnum.AdministrativePracticeTypeEnum;
import com.doing.nemo.claims.entity.enumerated.AuthorityEnum.AuthorityPracticeTypeEnum;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class AuthorityWorkingListRequestV1 implements Serializable {
    @JsonProperty("type")
    private List<AdministrativePracticeTypeEnum> type;

    @JsonProperty("working_id")
    private String workingId;

    @JsonProperty("working_type")
    private String workingType;

    public List<AdministrativePracticeTypeEnum> getType() {
        if(type == null){
            return null;
        }
        return new ArrayList<>(type);
    }

    public void setType(List<AdministrativePracticeTypeEnum> type) {
        if(type != null)
        {
            this.type = new ArrayList<>(type);
        } else {
            this.type = null;
        }
    }

    public String getWorkingId() {
        return workingId;
    }

    public void setWorkingId(String workingId) {
        this.workingId = workingId;
    }

    public String getWorkingType() {
        return workingType;
    }

    public void setWorkingType(String workingType) {
        this.workingType = workingType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AuthorityWorkingListRequestV1 that = (AuthorityWorkingListRequestV1) o;
        return Objects.equals(getType(), that.getType()) &&
                Objects.equals(getWorkingId(), that.getWorkingId()) &&
                Objects.equals(getWorkingType(), that.getWorkingType());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getType(), getWorkingId(), getWorkingType());
    }
}
