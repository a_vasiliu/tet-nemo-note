package com.doing.nemo.claims.controller.payload.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;


@JsonIgnoreProperties(ignoreUnknown = true)
public class PermissionResponseV1 implements Serializable {


    private static final long serialVersionUID = 4853326388242781237L;


    @JsonProperty(value = "id")
    private String id;

    @JsonProperty(value = "key")
    private String key;

    @JsonProperty(value = "description")
    private String description;

    @JsonProperty(value = "created_at")
    private String createdAt;

    public PermissionResponseV1(String id, String key, String description, String createdAt) {
        this.id = id;
        this.key = key;
        this.description = description;
        this.createdAt = createdAt;
    }

    public PermissionResponseV1() {}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
}
