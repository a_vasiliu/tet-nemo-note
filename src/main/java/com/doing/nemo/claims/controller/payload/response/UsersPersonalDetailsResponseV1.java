package com.doing.nemo.claims.controller.payload.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;

import java.io.Serializable;
import java.util.Date;

@ApiModel
public class UsersPersonalDetailsResponseV1 implements Serializable {

    @JsonProperty("birth_country")
    private String birthCountry;

    @JsonProperty("birth_date")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date birthDate;

    @JsonProperty("birth_place")
    private String birthPlace;

    @JsonProperty("birth_province")
    private String birthProvince;

    @JsonProperty("firstname")
    private String firstname;

    @JsonProperty("gender")
    private String gender;

    @JsonProperty("lastname")
    private String lastname;

    @JsonProperty("office_role")
    private String officeRole;

    @JsonProperty("phone")
    private String phone;

    @JsonProperty("tax_code")
    private String taxCode;

    public UsersPersonalDetailsResponseV1() {

    }

    public String getBirthCountry() {
        return birthCountry;
    }

    public void setBirthCountry(String birthCountry) {
        this.birthCountry = birthCountry;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public String getBirthProvince() {
        return birthProvince;
    }

    public void setBirthProvince(String birthProvince) {
        this.birthProvince = birthProvince;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getOfficeRole() {
        return officeRole;
    }

    public void setOfficeRole(String officeRole) {
        this.officeRole = officeRole;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getTaxCode() {
        return taxCode;
    }

    public void setTaxCode(String taxCode) {
        this.taxCode = taxCode;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("UsersPersonalDetailsResponseV1{");
        sb.append("birthCountry='").append(birthCountry).append('\'');
        sb.append(", birthDate=").append(birthDate);
        sb.append(", birthPlace='").append(birthPlace).append('\'');
        sb.append(", birthProvince='").append(birthProvince).append('\'');
        sb.append(", firstname='").append(firstname).append('\'');
        sb.append(", gender='").append(gender).append('\'');
        sb.append(", lastname='").append(lastname).append('\'');
        sb.append(", officeRole='").append(officeRole).append('\'');
        sb.append(", phone='").append(phone).append('\'');
        sb.append(", taxCode='").append(taxCode).append('\'');
        sb.append('}');
        return sb.toString();
    }

}
