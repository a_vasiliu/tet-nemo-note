package com.doing.nemo.claims.controller.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ListRequestV1<T> implements Serializable {
    @JsonProperty("data")
    private List<T> data;

    public ListRequestV1() {
    }

    public ListRequestV1(List<T> data) {
        if(data != null)
        {
            this.data = new ArrayList<>(data);
        } else {
            this.data = null;
        }
    }

    public List<T> getData() {
        if(data == null){
            return null;
        }
        return new ArrayList<>(data);
    }

    public void setData(List<T> data) {
        if(data != null)
        {
            this.data = new ArrayList<>(data);
        } else {
            this.data = null;
        }
    }

    @Override
    public String toString() {
        return "ListRequestV1{" +
                "data=" + data +
                '}';
    }
}
