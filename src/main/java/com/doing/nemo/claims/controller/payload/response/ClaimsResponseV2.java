package com.doing.nemo.claims.controller.payload.response;

import com.doing.nemo.claims.controller.payload.response.authority.claims.AuthorityResponseV1;
import com.doing.nemo.claims.controller.payload.response.claims.*;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsFlowEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ClaimsResponseV2 implements Serializable {

    @JsonProperty("id")
    private String id;

    @JsonProperty("created_at")
    private String createdAt;

    @JsonProperty("update_at")
    private String updateAt;

    @JsonProperty("practice_id")
    private Long practiceId;

    @JsonProperty("pai_comunication")
    private Boolean paiComunication;

    @JsonProperty("legal_comunication")
    private Boolean legalComunication;

    @JsonProperty("forced")
    private Boolean forced;

    @JsonProperty("in_evidence")
    private Boolean inEvidence;

    @JsonProperty("status")
    private String status;

    @JsonProperty("motivation")
    private String motivation;

    @JsonProperty("type")
    private String type;

    @JsonProperty("complaint")
    private ComplaintResponse complaint;

    @JsonProperty("forms")
    private FormResponse forms;

    @JsonProperty("damaged")
    private DamagedResponse damaged;

    @JsonProperty("cai_details")
    private CaiResponse caiDetails;

    @JsonProperty("exemption")
    private ExemptionResponse exemption;

    @JsonProperty("is_with_counterparty")
    private Boolean isWithCounterparty;

    @JsonProperty("counterparty")
    private List<CounterpartyResponse> counterparty;

    @JsonProperty("deponent")
    private List<DeponentResponse> deponentList;

    @JsonProperty("wounded")
    private List<WoundedResponse> woundedList;

    @JsonProperty("notes")
    private List<NoteResponse> notes;

    @JsonProperty("historical")
    private List<HistoricalResponse> historical;

    @JsonProperty("found_model")
    private FoundModelResponse foundModel;

    @JsonProperty("id_saleforce")
    private Long idSaleforce;

    @JsonProperty("with_continuation")
    private Boolean withContinuation;

    @JsonProperty("refund")
    private RefundResponse refund;

    @JsonProperty("metadata")
    private Map<String,Object> metadata;

    @JsonProperty("theft")
    private TheftResponseV1 theft;

    @JsonProperty("is_read_msa")
    private Boolean isRead;

    @JsonProperty("is_read_acclaims")
    private Boolean isReadAcclaims;


    @JsonProperty("authorities")
    private List<AuthorityResponseV1> authorityResponseV1List;

    @JsonProperty("is_authority_linkable")
    private Boolean isAuthorityLinkable;

    @JsonProperty("po_variation")
    private Boolean poVariation;

    @JsonProperty("total_miles_franchise")
    private Long totalPenalty;

    @JsonProperty("is_migrated")
    private Boolean isMigrated;

    @JsonProperty("is_complete_documentation")
    private Boolean isCompleteDocumentation;

    @JsonProperty("is_pending")
    private Boolean isPending;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(String updateAt) {
        this.updateAt = updateAt;
    }

    public Long getPracticeId() {
        return practiceId;
    }

    public void setPracticeId(Long practiceId) {
        this.practiceId = practiceId;
    }

    public Boolean getPaiComunication() {
        return paiComunication;
    }

    public void setPaiComunication(Boolean paiComunication) {
        this.paiComunication = paiComunication;
    }

    public Boolean getLegalComunication() {
        return legalComunication;
    }

    public void setLegalComunication(Boolean legalComunication) {
        this.legalComunication = legalComunication;
    }

    public Boolean getForced() {
        return forced;
    }

    public void setForced(Boolean forced) {
        this.forced = forced;
    }

    public Boolean getInEvidence() {
        return inEvidence;
    }

    public void setInEvidence(Boolean inEvidence) {
        this.inEvidence = inEvidence;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMotivation() {
        return motivation;
    }

    public void setMotivation(String motivation) {
        this.motivation = motivation;
    }

    public ComplaintResponse getComplaint() {
        return complaint;
    }

    public void setComplaint(ComplaintResponse complaint) {
        this.complaint = complaint;
    }

    public FormResponse getForms() {
        return forms;
    }

    public void setForms(FormResponse forms) {
        this.forms = forms;
    }

    public DamagedResponse getDamaged() {
        return damaged;
    }

    public void setDamaged(DamagedResponse damaged) {
        this.damaged = damaged;
    }

    public CaiResponse getCaiDetails() {
        return caiDetails;
    }

    public void setCaiDetails(CaiResponse caiDetails) {
        this.caiDetails = caiDetails;
    }

    public ExemptionResponse getExemption() {
        return exemption;
    }

    public void setExemption(ExemptionResponse exemption) {
        this.exemption = exemption;
    }

    public Boolean getWithCounterparty() {
        return isWithCounterparty;
    }

    public void setWithCounterparty(Boolean withCounterparty) {
        isWithCounterparty = withCounterparty;
    }

    public List<CounterpartyResponse> getCounterparty() {
        return counterparty;
    }

    public void setCounterparty(List<CounterpartyResponse> counterparty) {
        this.counterparty = counterparty;
    }

    public List<DeponentResponse> getDeponentList() {
        return deponentList;
    }

    public void setDeponentList(List<DeponentResponse> deponentList) {
        this.deponentList = deponentList;
    }

    public List<WoundedResponse> getWoundedList() {
        return woundedList;
    }

    public void setWoundedList(List<WoundedResponse> woundedList) {
        this.woundedList = woundedList;
    }

    public List<NoteResponse> getNotes() {
        return notes;
    }

    public void setNotes(List<NoteResponse> notes) {
        this.notes = notes;
    }

    public List<HistoricalResponse> getHistorical() {
        return historical;
    }

    public void setHistorical(List<HistoricalResponse> historical) {
        this.historical = historical;
    }

    public FoundModelResponse getFoundModel() {
        return foundModel;
    }

    public void setFoundModel(FoundModelResponse foundModel) {
        this.foundModel = foundModel;
    }

    public Long getIdSaleforce() {
        return idSaleforce;
    }

    public void setIdSaleforce(Long idSaleforce) {
        this.idSaleforce = idSaleforce;
    }

    public Boolean getWithContinuation() {
        return withContinuation;
    }

    public void setWithContinuation(Boolean withContinuation) {
        this.withContinuation = withContinuation;
    }

    public RefundResponse getRefund() {
        return refund;
    }

    public void setRefund(RefundResponse refund) {
        this.refund = refund;
    }

    public Map<String, Object> getMetadata() {
        return metadata;
    }

    public void setMetadata(Map<String, Object> metadata) {
        this.metadata = metadata;
    }

    public TheftResponseV1 getTheft() {
        return theft;
    }

    public void setTheft(TheftResponseV1 theft) {
        this.theft = theft;
    }

    public Boolean getRead() {
        return isRead;
    }

    public void setRead(Boolean read) {
        isRead = read;
    }

    public Boolean getReadAcclaims() {
        return isReadAcclaims;
    }

    public void setReadAcclaims(Boolean readAcclaims) {
        isReadAcclaims = readAcclaims;
    }

    public List<AuthorityResponseV1> getAuthorityResponseV1List() {
        return authorityResponseV1List;
    }

    public void setAuthorityResponseV1List(List<AuthorityResponseV1> authorityResponseV1List) {
        this.authorityResponseV1List = authorityResponseV1List;
    }

    public Boolean getAuthorityLinkable() {
        return isAuthorityLinkable;
    }

    public void setAuthorityLinkable(Boolean authorityLinkable) {
        isAuthorityLinkable = authorityLinkable;
    }

    public Boolean getPoVariation() {
        return poVariation;
    }

    public void setPoVariation(Boolean poVariation) {
        this.poVariation = poVariation;
    }

    public Long getTotalPenalty() {
        return totalPenalty;
    }

    public void setTotalPenalty(Long totalPenalty) {
        this.totalPenalty = totalPenalty;
    }

    public Boolean getMigrated() {
        return isMigrated;
    }

    public void setMigrated(Boolean migrated) {
        isMigrated = migrated;
    }

    public Boolean getCompleteDocumentation() {
        return isCompleteDocumentation;
    }

    public void setCompleteDocumentation(Boolean completeDocumentation) {
        isCompleteDocumentation = completeDocumentation;
    }

    public Boolean getPending() {
        return isPending;
    }

    public void setPending(Boolean pending) {
        isPending = pending;
    }
}
