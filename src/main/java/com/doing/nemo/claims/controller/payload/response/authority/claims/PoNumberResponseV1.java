package com.doing.nemo.claims.controller.payload.response.authority.claims;

import com.doing.nemo.claims.controller.payload.response.FileManagerResponseV1;
import com.doing.nemo.claims.controller.payload.response.UploadFileResponseV1;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class PoNumberResponseV1  implements Serializable {
    @JsonProperty("po_number")
    private String poNumber;

    @JsonProperty("file_manager")
    private UploadFileResponseV1 fileManagerResponseV1;

    public String getPoNumber() {
        return poNumber;
    }

    public void setPoNumber(String poNumber) {
        this.poNumber = poNumber;
    }

    public UploadFileResponseV1 getFileManagerResponseV1() {
        return fileManagerResponseV1;
    }

    public void setFileManagerResponseV1(UploadFileResponseV1 fileManagerResponseV1) {
        this.fileManagerResponseV1 = fileManagerResponseV1;
    }

    @Override
    public String toString() {
        return "PoNumberResponseV1{" +
                "poNumber='" + poNumber + '\'' +
                ", fileManagerResponseV1=" + fileManagerResponseV1 +
                '}';
    }
}
