package com.doing.nemo.claims.controller;


import com.doing.nemo.claims.controller.payload.response.ClaimsMyAldResponseV1;
import com.doing.nemo.claims.controller.payload.response.ClaimsResponseMyAldPaginationV1;
import com.doing.nemo.claims.entity.enumerated.myAld.MyAldInsertedByEnum;
import com.doing.nemo.claims.service.MyAldService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@CrossOrigin
@RequestMapping(value = "/api/v1")
@Api("Closing")
public class MyAldController {

    @Autowired
    private MyAldService myAldService;

    //REFACTOR
    @ApiOperation(value="Pagination MyAld Fleet Manager", notes = "It retrieves MyAld Fleet Manager claims' pagination")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK" /*,response = ClaimsResponseMyAldPaginationV1.class*/),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @GetMapping(value="/claims/myald/fm", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<ClaimsResponseMyAldPaginationV1<ClaimsMyAldResponseV1>> searchClaimsMyAldFleetManager(
            @ApiParam(value = "Defines how many Claims can contain the single page")
            @RequestParam(value = "page_size", defaultValue = "20") Integer pageSize,
            @ApiParam(value = "Defines the page number to be displayed")
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @RequestParam(name = "contract_plate_number_sx" , required = false) String contractPlateNumberSx,
            @RequestParam(name = "date_accident_from", required = false) String dateAccidentFrom,
            @RequestParam(name = "date_accident_to", required = false) String dateAccidentTo,
            @RequestParam(name = "client_id", required = false) String clientId,
            @RequestParam(name = "status", required = false) List<String> statusList,
            @RequestParam(name = "date_created_from", required = false) String dateCreatedFrom,
            @RequestParam(name = "date_created_to", required = false) String dateCreatedTo,
            @RequestParam(name = "inserted_by", required = false) List<MyAldInsertedByEnum> insertedByList,
            @ApiParam(value = "Order")
            @RequestParam(value = "asc", required = false, defaultValue = "true") Boolean asc,
            @ApiParam(value = "OrderBy")
            @RequestParam(value = "order_by", defaultValue = "created_at", required = false) String orderBy,
            @RequestHeader(name = "nemo-user-id", required = true) String userId,
            @RequestHeader(name = "nemo-user-email", required = true) String userEmail,
            @RequestHeader(name = "client-ids", required = true) List<String> clientIdList

    ){
        //rotta per i profili -> https://nemo-dev.aldautomotive.it/permission/api/v1/profiles
        //rotta recuperare i dettagli dell'utente contenuto nel nemo-user-id https://nemo-dev.aldautomotive.it/permission/api/v1/users/9efe432f-9ea5-49d4-badc-8b91b7f77e49
        return new ResponseEntity(myAldService.getPaginationFmMyAld(contractPlateNumberSx,clientIdList,dateAccidentFrom,dateAccidentTo,statusList,dateCreatedFrom,dateCreatedTo,insertedByList,asc,orderBy,page,pageSize, userId, clientId), HttpStatus.OK);
    }


    //REFACTOR
    @ApiOperation(value="Pagination MyAld Driver", notes = "It retrieves MyAld Driver claims' pagination")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK" /*,response = ClaimsResponseMyAldPaginationV1.class*/),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })

    @GetMapping(value="/claims/myald/driver", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<ClaimsResponseMyAldPaginationV1<ClaimsMyAldResponseV1>> searchClaimsMyAldDriver(
            @ApiParam(value = "Defines how many Claims can contain the single page")
            @RequestParam(value = "page_size", defaultValue = "20") Integer pageSize,
            @ApiParam(value = "Defines the page number to be displayed")
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @RequestParam(name = "contract_plate_number_sx" , required = false) String contractPlateNumberSx,
            @RequestParam(name = "date_accident_from", required = false) String dateAccidentFrom,
            @RequestParam(name = "date_accident_to", required = false) String dateAccidentTo,
            @RequestParam(name = "status", required = false) List<String> statusList,
            @RequestParam(name = "date_created_from", required = false) String dateCreatedFrom,
            @RequestParam(name = "date_created_to", required = false) String dateCreatedTo,
            @RequestParam(name = "inserted_by", required = false) List<MyAldInsertedByEnum> insertedByList,
            @ApiParam(value = "Order")
            @RequestParam(value = "asc", required = false, defaultValue = "true") Boolean asc,
            @ApiParam(value = "OrderBy")
            @RequestParam(value = "order_by", defaultValue = "created_at", required = false) String orderBy,
            @RequestHeader(name = "myald-nemo-user-id", required = true) String userId,
            @RequestHeader(name = "nemo-user-email", required = true) String userEmail,
            @RequestHeader(name = "plates", required = true) List<String> plates,
            @RequestHeader(name = "is-inclusive", required = true) Boolean isInclusive

    ){

        //rotta per i profili -> https://nemo-dev.aldautomotive.it/permission/api/v1/profiles
        //rotta recuperare i dettagli dell'utente contenuto nel nemo-user-id https://nemo-dev.aldautomotive.it/permission/api/v1/users/9efe432f-9ea5-49d4-badc-8b91b7f77e49

        return new ResponseEntity(myAldService.getPaginationDriverMyAld(plates,contractPlateNumberSx,dateAccidentFrom,dateAccidentTo,statusList,dateCreatedFrom,dateCreatedTo,insertedByList,asc,orderBy,page,pageSize,userId, isInclusive), HttpStatus.OK);
    }
}
