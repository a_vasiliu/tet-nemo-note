package com.doing.nemo.claims.controller;

import com.doing.nemo.claims.adapter.ConfigureEvidenceAdapter;
import com.doing.nemo.claims.controller.payload.request.ConfigureEvidenceRequestV1;
import com.doing.nemo.claims.controller.payload.response.ConfigureEvidenceResponseV1;
import com.doing.nemo.claims.controller.payload.response.PaginationResponseV1;
import com.doing.nemo.claims.entity.settings.ConfigureEvidenceEntity;
import com.doing.nemo.claims.service.ConfigureEvidenceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;


@RestController
@CrossOrigin
@RequestMapping(value = "/api/v1")
@Api("Claims")
public class ConfigureEvidenceController {

    private static Logger LOGGER = LoggerFactory.getLogger(ConfigureEvidenceController.class);

    @Autowired
    private ConfigureEvidenceService configureEvidenceService;

    @Autowired
    private ConfigureEvidenceAdapter configureEvidenceAdapter;

    //scrivo solo evidence ma si intende la possibilità di inserire
    //una nuova configurazione per uno stato pratica e i relativi giorni di permanenza

    @PostMapping("/evidence")
    public ResponseEntity inserConfigureEvidence(@RequestBody ConfigureEvidenceRequestV1 configureEvidenceRequestV1) {

        UUID idConfigureEvidence = configureEvidenceService.insertConfigureEvidence(configureEvidenceRequestV1);
        return new ResponseEntity("insert new configuration evidence " + idConfigureEvidence.toString(), HttpStatus.CREATED);
    }

    @GetMapping("/evidence/{UUID_CONFIGURATION_EVIDENCE}")
    public ResponseEntity getConfigureEvidence(@PathVariable(name = "UUID_CONFIGURATION_EVIDENCE") UUID configurationId) {

        return new ResponseEntity(configureEvidenceService.getConfigureEvidence(configurationId), HttpStatus.OK);
    }

    @GetMapping("/evidence")
    public ResponseEntity<List<ConfigureEvidenceResponseV1>> getAllConfigureEvidence() {

        return new ResponseEntity<>(configureEvidenceService.getAllConfigureEvidence(), HttpStatus.OK);
    }

    @PutMapping("/evidence/{UUID_CONFIGURATION_EVIDENCE}")
    public ResponseEntity updateConfigureEvidence(@PathVariable(name = "UUID_CONFIGURATION_EVIDENCE") UUID configurationId,
                                                  @RequestBody ConfigureEvidenceRequestV1 configureEvidenceRequestV1) {

        return new ResponseEntity(configureEvidenceService.updateConfigureEvidence(configurationId, configureEvidenceRequestV1), HttpStatus.OK);
    }

    @DeleteMapping("/evidence/{UUID_CONFIGURATION_EVIDENCE}")
    public ResponseEntity deleteConfigureEvidence(@PathVariable(name = "UUID_CONFIGURATION_EVIDENCE") UUID configurationId) {

        configureEvidenceService.deleteConfigureEvidence(configurationId);
        return new ResponseEntity("successful deleting", HttpStatus.OK);
    }

    @PatchMapping("/evidence/{UUID}/active")
    public ResponseEntity updateStatusConfigureEvidence(@PathVariable UUID UUID) {
        return new ResponseEntity(configureEvidenceService.patchStatusConfigureEvidence(UUID), HttpStatus.OK);
    }

    @ApiOperation(value="Search ConfigureEvidence", notes = "It retrieves antitheftservice data through one of more parameters")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = PaginationResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @GetMapping(value="/evidence/pagination", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<PaginationResponseV1<ConfigureEvidenceResponseV1>> searchConfigureEvidence(
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @RequestParam(value = "page_size", defaultValue = "20") Integer pageSize,
            @RequestParam(value = "order_by", required = false) String orderBy,
            @RequestParam(value = "asc", required = false, defaultValue = "true") Boolean asc,
            @RequestParam(value = "practical_state_it", required = false) String practicalStateIt,
            @RequestParam(value = "practical_state_en", required = false) String practicalStateEn,
            @RequestParam(value = "days_of_stay_in_the_state", required = false) Long daysOfStayInTheState,
            @RequestParam(value = "is_active", required = false) Boolean isActive
    ){

        List<ConfigureEvidenceEntity> listConfigureEvidence = configureEvidenceService.findConfigureEvidencesFiltered(page,pageSize, practicalStateIt, practicalStateEn, daysOfStayInTheState, isActive, orderBy, asc);
        Long itemCount = configureEvidenceService.countConfigureEvidenceFiltered(practicalStateIt, practicalStateEn, daysOfStayInTheState, isActive);

        PaginationResponseV1<ConfigureEvidenceResponseV1> paginationResponseV1 = configureEvidenceAdapter.adptPagination(listConfigureEvidence, page, pageSize, itemCount);

        return new ResponseEntity<>(paginationResponseV1, HttpStatus.OK);
    }
}
