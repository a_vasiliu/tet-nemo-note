package com.doing.nemo.claims.controller;

import com.doing.nemo.claims.adapter.RecoverabilityAdapter;
import com.doing.nemo.claims.common.validation.MessageCode;
import com.doing.nemo.claims.common.validation.RequestValidator;
import com.doing.nemo.claims.controller.payload.request.ListRequestV1;
import com.doing.nemo.claims.controller.payload.request.RecoverabilityRequestV1;
import com.doing.nemo.claims.controller.payload.response.ClaimsResponsePaginationV1;
import com.doing.nemo.claims.controller.payload.response.IdResponseV1;
import com.doing.nemo.claims.controller.payload.response.RecoverabilityResponseV1;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.settings.RecoverabilityEntity;
import com.doing.nemo.claims.service.RecoverabilityService;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.exception.NotFoundException;
import io.swagger.annotations.*;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/v1")
@Api("Recoverability")
public class RecoverabilityController {

    private static Logger LOGGER = LoggerFactory.getLogger(RecoverabilityController.class);
    @Autowired
    private RecoverabilityService recoverabilityService;
    @Autowired
    private RecoverabilityAdapter recoverabilityAdapter;
    @Autowired
    private RequestValidator requestValidator;

    @PostMapping("/recoverability")
    @Transactional
    @ApiOperation(value = "Insert Recoverability", notes = "Insert Recoverability using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = IdResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<IdResponseV1> postRecoverability(@ApiParam(value = "Body of the Recoverability to be created", required = true) @RequestBody RecoverabilityRequestV1 recoverabilityRequestV1) {

        requestValidator.validateRequest(recoverabilityRequestV1, MessageCode.E00X_1000);
        RecoverabilityEntity recoverabilityEntity = recoverabilityAdapter.adptFromRecoverabilityRequestToRecoverabilityEntity(recoverabilityRequestV1);

        if (recoverabilityService.getRecoverabilityByFlowAndTypeClaim(recoverabilityEntity.getManagement(), recoverabilityEntity.getClaimsType().getClaimsAccidentType()) != null) {
            LOGGER.debug("Already exists recoverability with same claims type and flow");
            throw new BadRequestException("Already exists recoverability with same claims type and flow", com.doing.nemo.claims.validation.MessageCode.CLAIMS_1009);
        }
        recoverabilityEntity = recoverabilityService.insertRecoverability(recoverabilityEntity);
        IdResponseV1 recoverabilityResponseIdV1 = new IdResponseV1(recoverabilityEntity.getId());

        return new ResponseEntity(recoverabilityResponseIdV1, HttpStatus.CREATED);
    }

    @PostMapping("/recoverabilities")
    @Transactional
    @ApiOperation(value = "Insert More Recoverabilities", notes = "Insert Recoverabilities using info passed in the bodies")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = RecoverabilityResponseV1.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<RecoverabilityResponseV1>> postRecoverabilitys(@ApiParam(value = "Bodies of the Recoverabilitys to be created", required = true) @RequestBody ListRequestV1<RecoverabilityRequestV1> recoverabilityListRequestV1) {

        List<RecoverabilityResponseV1> recoverabilityResponseV1List = new ArrayList<>();
        for (RecoverabilityRequestV1 recoverabilityRequestV1 : recoverabilityListRequestV1.getData()) {
            requestValidator.validateRequest(recoverabilityRequestV1, MessageCode.E00X_1000);
            RecoverabilityEntity recoverabilityEntity = recoverabilityAdapter.adptFromRecoverabilityRequestToRecoverabilityEntity(recoverabilityRequestV1);
            if (recoverabilityService.getRecoverabilityByFlowAndTypeClaim(recoverabilityEntity.getManagement(), recoverabilityEntity.getClaimsType().getClaimsAccidentType()) == null) {
                recoverabilityEntity = recoverabilityService.insertRecoverability(recoverabilityEntity);
                RecoverabilityResponseV1 recoverabilityResponseV1 = RecoverabilityAdapter.adptFromRecoverabilityEntityToRecoverabilityResponseV1(recoverabilityEntity);
                recoverabilityResponseV1List.add(recoverabilityResponseV1);
            }

        }
        return new ResponseEntity(recoverabilityResponseV1List, HttpStatus.CREATED);
    }

    @PutMapping("/recoverability/{UUID}")
    @Transactional
    @ApiOperation(value = "Upload Recoverability", notes = "Upload Recoverability using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = RecoverabilityResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<RecoverabilityResponseV1> putRecoverability(@ApiParam(value = "UUID that identifies the Recoverability to be modified", required = true) @PathVariable(name = "UUID") UUID id, @ApiParam(value = "Updated body of the Recoverability", required = true)
    @RequestBody RecoverabilityRequestV1 recoverabilityRequestV1) {
        requestValidator.validateRequest(recoverabilityRequestV1, MessageCode.E00X_1000);
        RecoverabilityEntity recoverabilityEntity = recoverabilityService.selectRecoverability(id);
        RecoverabilityEntity recoverabilityEntityNew = recoverabilityAdapter.adptFromRecoverabilityRequestToRecoverabilityEntity(recoverabilityRequestV1);
        recoverabilityEntityNew.setId(recoverabilityEntity.getId());
        recoverabilityEntityNew = recoverabilityService.updateRecoverability(recoverabilityEntityNew);
        RecoverabilityResponseV1 recoverabilityResponseV1 = RecoverabilityAdapter.adptFromRecoverabilityEntityToRecoverabilityResponseV1(recoverabilityEntityNew);
        return new ResponseEntity<>(recoverabilityResponseV1, HttpStatus.OK);
    }

    @GetMapping("/recoverability/{UUID}")
    @Transactional
    @ApiOperation(value = "Recover Recoverability", notes = "Return a Recoverability using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = RecoverabilityResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<RecoverabilityResponseV1> getRecoverability(@ApiParam(value = "UUID of the Recoverability to be found", required = true) @PathVariable(name = "UUID") UUID id) {
        RecoverabilityEntity recoverabilityEntity = recoverabilityService.selectRecoverability(id);
        RecoverabilityResponseV1 recoverabilityResponseV1 = RecoverabilityAdapter.adptFromRecoverabilityEntityToRecoverabilityResponseV1(recoverabilityEntity);
        return new ResponseEntity<>(recoverabilityResponseV1, HttpStatus.OK);
    }

    @GetMapping("/recoverability")
    @Transactional
    @ApiOperation(value = "Recover All Recoverabilities", notes = "Returns all Recoverabilitys")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = RecoverabilityResponseV1.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<RecoverabilityResponseV1>> getAllRecoverability() {
        List<RecoverabilityEntity> recoverabilityEntityList = recoverabilityService.selectAllRecoverability();
        List<RecoverabilityResponseV1> recoverabilityResponseV1List = RecoverabilityAdapter.adptFromRecoverabilityEntityToRecoverabilityResponseV1List(recoverabilityEntityList);
        return new ResponseEntity(recoverabilityResponseV1List, HttpStatus.OK);
    }

    @DeleteMapping("/recoverability/{UUID}")
    @Transactional
    @ApiOperation(value = "Delete Recoverability", notes = "Delete a Recoverability using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = RecoverabilityResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<RecoverabilityResponseV1> deleteRecoverability(@ApiParam(value = "UUID of the Recoverability to be deleted", required = true) @PathVariable(name = "UUID") UUID id) {
        RecoverabilityResponseV1 recoverabilityResponseV1 = recoverabilityService.deleteRecoverability(id);
        return new ResponseEntity<>(recoverabilityResponseV1, HttpStatus.OK);
    }

    @PatchMapping("/recoverability/{UUID}/active")
    @Transactional
    @ApiOperation(value = "Patch active status Recoverability", notes = "Patch active status of Recoverability using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = RecoverabilityResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<RecoverabilityResponseV1> patchStatusRecoverability(@PathVariable UUID UUID) {
        RecoverabilityEntity recoverabilityEntity = recoverabilityService.updateStatus(UUID);
        RecoverabilityResponseV1 responseV1 = RecoverabilityAdapter.adptFromRecoverabilityEntityToRecoverabilityResponseV1(recoverabilityEntity);
        return new ResponseEntity<>(responseV1, HttpStatus.OK);
    }

    @GetMapping("/recoverability/{TYPE_CLAIMS}/{FLOW}")
    @Transactional
    @ApiOperation(value = "Recover recoverability by type claims and flow", notes = "Recover recoverability by type claims and flow")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = RecoverabilityResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<RecoverabilityResponseV1> getRecoverabilityByFlowAndTypeClaim(
            @PathVariable(name = "TYPE_CLAIMS") String typeClaims,
            @PathVariable(name = "FLOW") String flow) {
        DataAccidentTypeAccidentEnum typeAccidentEntity = DataAccidentTypeAccidentEnum.create(typeClaims);
        RecoverabilityEntity recoverabilityEntity = recoverabilityService.getRecoverabilityByFlowAndTypeClaim(flow.toUpperCase(), typeAccidentEntity);
        if (recoverabilityEntity == null) {
            LOGGER.debug("Recoverability with claim type " + typeClaims + " and flow " + flow + " not found");
            throw new NotFoundException("Recoverability with claim type " + typeClaims + " and flow " + flow + " not found", com.doing.nemo.claims.validation.MessageCode.CLAIMS_1010);
        }
        RecoverabilityResponseV1 recoverabilityResponseV1 = RecoverabilityAdapter.adptFromRecoverabilityEntityToRecoverabilityResponseV1(recoverabilityEntity);
        return new ResponseEntity<>(recoverabilityResponseV1, HttpStatus.OK);
    }

    @ApiOperation(value = "Recoverability Pagination", notes = "It retrieves Recoverability pagination")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = ClaimsResponsePaginationV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @GetMapping(value = "/recoverability/pagination", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<ClaimsResponsePaginationV1> recoverabilityPagination(
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @RequestParam(value = "page_size", defaultValue = "20") Integer pageSize,
            @RequestParam(value = "order_by", required = false) String orderBy,
            @RequestParam(value = "asc", required = false, defaultValue = "true") Boolean asc,
            @RequestParam(value = "management", required = false) String management,
            @RequestParam(value = "claims_accident_type", required = false) String claimsType,
            @RequestParam(value = "counterpart", required = false) Boolean counterpart,
            @RequestParam(value = "recoverability", required = false) Boolean recoverability,
            @RequestParam(value = "percent_recoverability", required = false) Double percentRecoverability,
            @RequestParam(value = "is_active", required = false) Boolean isActive
    ) {

        Pagination<RecoverabilityEntity> recoverabilityPagination = recoverabilityService.paginationRecoverability(page, pageSize, management, claimsType, counterpart, recoverability, percentRecoverability, isActive, orderBy, asc);

        return new ResponseEntity<>(RecoverabilityAdapter.adptRecoverabilityPaginationToClaimsResponsePagination(recoverabilityPagination), HttpStatus.OK);
    }

}
