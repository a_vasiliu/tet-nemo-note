package com.doing.nemo.claims.controller.payload.response.authority.claims;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.math.BigDecimal;

public class PODetailsResponseV1 implements Serializable {

    @JsonProperty("created_at")
    private String createdAt;

    @JsonProperty("po_number")
    private String poNumber;

    @JsonProperty("po_amount")
    private BigDecimal poAmount;

    @JsonProperty("po_target")
    private String poTarget;

    @JsonProperty("id_filemanager")
    private String idFilemanager;

    public String getIdFilemanager() {
        return idFilemanager;
    }

    public void setIdFilemanager(String idFilemanager) {
        this.idFilemanager = idFilemanager;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getPoNumber() {
        return poNumber;
    }

    public void setPoNumber(String poNumber) {
        this.poNumber = poNumber;
    }

    public BigDecimal getPoAmount() {
        return poAmount;
    }

    public void setPoAmount(BigDecimal poAmount) {
        this.poAmount = poAmount;
    }

    public String getPoTarget() {
        return poTarget;
    }

    public void setPoTarget(String poTarget) {
        this.poTarget = poTarget;
    }
}
