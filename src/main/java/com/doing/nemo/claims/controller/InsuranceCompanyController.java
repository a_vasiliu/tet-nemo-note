package com.doing.nemo.claims.controller;

import com.doing.nemo.claims.adapter.InsuranceCompanyAdapter;
import com.doing.nemo.claims.common.validation.MessageCode;
import com.doing.nemo.claims.common.validation.RequestValidator;
import com.doing.nemo.claims.controller.payload.request.InsuranceCompanyRequestV1;
import com.doing.nemo.claims.controller.payload.request.ListRequestV1;
import com.doing.nemo.claims.controller.payload.response.ClaimsResponsePaginationV1;
import com.doing.nemo.claims.controller.payload.response.IdResponseV1;
import com.doing.nemo.claims.controller.payload.response.InsuranceCompanyResponseV1;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.settings.InsuranceCompanyEntity;
import com.doing.nemo.claims.service.InsuranceCompanyService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/v1")
@Api("Insurance Company")
public class InsuranceCompanyController {

    @Autowired
    private InsuranceCompanyService insuranceCompanyService;

    @Autowired
    private InsuranceCompanyAdapter insuranceCompanyAdapter;

    @Autowired
    private RequestValidator requestValidator;

    @PostMapping("/company")
    @Transactional
    @ApiOperation(value = "Insert Insurance Company", notes = "Insert insurance company using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = IdResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<IdResponseV1> insertInsuranceCompany(
            @ApiParam(value = "Body of the Insurance Company to be created", required = true)
            @RequestBody InsuranceCompanyRequestV1 insuranceCompanyRequest) {

        requestValidator.validateRequest(insuranceCompanyRequest, MessageCode.E00X_1000);

        InsuranceCompanyEntity insuranceCompanyEntity = insuranceCompanyService.insertCompany(InsuranceCompanyAdapter.adptFromICompRequToICompEntity(insuranceCompanyRequest));

        IdResponseV1 responseV1 = new IdResponseV1(insuranceCompanyEntity.getId());
        return new ResponseEntity<>(responseV1, HttpStatus.CREATED);
    }

    @PostMapping("/companies")
    @Transactional
    @ApiOperation(value = "Insert a list of Insurance Companies", notes = "Insert a list of insurance companies using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = IdResponseV1.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<InsuranceCompanyResponseV1>> insertInsuranceCompanies(
            @ApiParam(value = "List of the bodies of the Insurance Companies to be created", required = true)
            @RequestBody ListRequestV1<InsuranceCompanyRequestV1> insuranceCompaniesRequest) {

        List<InsuranceCompanyResponseV1> insuranceCompanyResponseV1List = new ArrayList<>();
        for (InsuranceCompanyRequestV1 insuranceCompanyRequestV1 : insuranceCompaniesRequest.getData()) {
            requestValidator.validateRequest(insuranceCompanyRequestV1, MessageCode.E00X_1000);
            InsuranceCompanyEntity insuranceCompanyEntity = InsuranceCompanyAdapter.adptFromICompRequToICompEntity(insuranceCompanyRequestV1);
            insuranceCompanyEntity = insuranceCompanyService.insertCompany(insuranceCompanyEntity);
            InsuranceCompanyResponseV1<InsuranceCompanyEntity> responseV1 = InsuranceCompanyAdapter.adptFromICompEntityToICompResp(insuranceCompanyEntity);
            insuranceCompanyResponseV1List.add(responseV1);
        }
        return new ResponseEntity<>(insuranceCompanyResponseV1List, HttpStatus.CREATED);

    }

    @GetMapping("/company/{UUID}")
    @Transactional
    @ApiOperation(value = "Recover Insurance Company", notes = "Returns a Insurance Company using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = InsuranceCompanyResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<InsuranceCompanyResponseV1> getInsuranceCompany(
            @ApiParam(value = "UUID of the Insurance Company to be found", required = true)
            @PathVariable UUID UUID) {

        InsuranceCompanyEntity insuranceCompanyEntity = insuranceCompanyService.selectCompany(UUID);
        InsuranceCompanyResponseV1<InsuranceCompanyEntity> responseV1 = InsuranceCompanyAdapter.adptFromICompEntityToICompResp(insuranceCompanyEntity);
        return new ResponseEntity<>(responseV1, HttpStatus.OK);
    }

    @GetMapping("/company")
    @Transactional
    @ApiOperation(value = "Recover All Insurance Companies", notes = "Returns all Insurance Companies")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = InsuranceCompanyResponseV1.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<InsuranceCompanyResponseV1>> getAllInsuranceCompanies() {

        List<InsuranceCompanyEntity> insuranceCompanyEntityList = insuranceCompanyService.selectAllCompany();

        List<InsuranceCompanyResponseV1> responseV1 = InsuranceCompanyAdapter.adptFromICompEntityToICompRespList(insuranceCompanyEntityList);
        return new ResponseEntity<>(responseV1, HttpStatus.OK);
    }

    @PutMapping("/company/{UUID}")
    @Transactional
    @ApiOperation(value = "Upload Insurance Company", notes = "Upload Insurance Company using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = InsuranceCompanyResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<InsuranceCompanyResponseV1> putInsuranceCompanies(
            @ApiParam(value = "UUID that identifies the Insurance Company to be modified", required = true)
            @PathVariable UUID UUID,
            @ApiParam(value = "Updated body of the Insurance Company", required = true)
            @RequestBody InsuranceCompanyRequestV1 insuranceCompanyRequest) {


        requestValidator.validateRequest(insuranceCompanyRequest, MessageCode.E00X_1000);
        InsuranceCompanyEntity insuranceCompanyEntity = insuranceCompanyAdapter.adptFromICompRequToICompEntityWithID(insuranceCompanyRequest, UUID);

        insuranceCompanyEntity = insuranceCompanyService.updateCompany(insuranceCompanyEntity);
        InsuranceCompanyResponseV1<InsuranceCompanyEntity> responseV1 = InsuranceCompanyAdapter.adptFromICompEntityToICompResp(insuranceCompanyEntity);
        return new ResponseEntity<>(responseV1, HttpStatus.OK);

    }

    @DeleteMapping("/company/{UUID}")
    @Transactional
    @ApiOperation(value = "Delete Insurance Company", notes = "Delete Insurance Company using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = InsuranceCompanyResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<InsuranceCompanyResponseV1> deleteInsuranceCompanies(
            @ApiParam(value = "UUID that identifies the Insurance Company to be deleted", required = true)
            @PathVariable UUID UUID) {
        InsuranceCompanyResponseV1 responseV1 = insuranceCompanyService.deleteCompany(UUID);
        return new ResponseEntity<>(responseV1, HttpStatus.OK);
    }

    @PatchMapping("/company/{UUID}/active")
    @Transactional
    @ApiOperation(value = "Patch status active Insurance Company", notes = "Patch status active Insurance Company using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = InsuranceCompanyResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<InsuranceCompanyResponseV1> patchStatusInsuranceCompany(
            @PathVariable UUID UUID
    ) {
        InsuranceCompanyEntity insuranceCompanyEntity = insuranceCompanyService.updateStatusCompany(UUID);
        InsuranceCompanyResponseV1<InsuranceCompanyEntity> responseV1 = InsuranceCompanyAdapter.adptFromICompEntityToICompResp(insuranceCompanyEntity);
        return new ResponseEntity<>(responseV1, HttpStatus.OK);
    }

    @ApiOperation(value="Insurance Company Pagination", notes = "It retrieves Insurance Company pagination")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = ClaimsResponsePaginationV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @GetMapping(value="/company/pagination", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<ClaimsResponsePaginationV1> companyPagination(
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @RequestParam(value = "page_size", defaultValue = "20") Integer pageSize,
            @RequestParam(value = "order_by", required = false) String orderBy,
            @RequestParam(value = "asc", required = false, defaultValue = "true") Boolean asc,
            @RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "zip_code", required = false) String zipCode,
            @RequestParam(value = "city", required = false) String city,
            @RequestParam(value = "province", required = false) String province,
            @RequestParam(value = "telephone", required = false) String telephone,
            @RequestParam(value = "is_active", required = false) Boolean isActive,
            @RequestParam(value = "code", required = false) Long code
    ){

        Pagination<InsuranceCompanyEntity> insuranceCompanyEntityPagination = insuranceCompanyService.paginationInsuranceCompany(page, pageSize, orderBy, asc, name, zipCode, city, province, telephone, isActive, code);

        return new ResponseEntity<>(InsuranceCompanyAdapter.adptInsuranceCompanyPaginationToClaimsResponsePagination(insuranceCompanyEntityPagination), HttpStatus.OK);
    }

}
