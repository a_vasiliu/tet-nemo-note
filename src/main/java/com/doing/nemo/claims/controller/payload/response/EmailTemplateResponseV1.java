package com.doing.nemo.claims.controller.payload.response;

import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsFlowEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsRepairEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.enumerated.EmailTemplateEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EmailTemplateResponseV1 implements Serializable {

    @JsonProperty("id")
    private UUID id;

    @JsonProperty("application_event")
    private EventTypeResponseV1 applicationEvent;

    @JsonProperty("type")
    private EmailTemplateEnum type;

    @JsonProperty("description")
    private String description;

    @JsonProperty("object")
    private String object;

    @JsonProperty("heading")
    private String heading;

    @JsonProperty("body")
    private String body;

    @JsonProperty("foot")
    private String foot;

    @JsonProperty("attach_file")
    private Boolean attachFile;

    @JsonProperty("splitting_recipients_email")
    private Boolean splittingRecipientsEmail;

    @JsonProperty("client_code")
    private String clientCode;

    @JsonProperty("customers")
    private List<RecipientTypeResponseV1> customers;

    @JsonProperty("is_active")
    private Boolean isActive;

    @JsonProperty("type_complaint")
    private ClaimsRepairEnum typeComplaint;

    @JsonProperty("flows")
    private List<ClaimsFlowEnum> flows;

    @JsonProperty("accident_type_list")
    private List<DataAccidentTypeAccidentEnum> accidentTypeList;


    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public EventTypeResponseV1 getApplicationEvent() {
        return applicationEvent;
    }

    public void setApplicationEvent(EventTypeResponseV1 applicationEvent) {
        this.applicationEvent = applicationEvent;
    }

    public List<RecipientTypeResponseV1> getCustomers() {
        if(customers == null){
            return null;
        }
        return new ArrayList<>(customers);
    }

    public void setCustomers(List<RecipientTypeResponseV1> customers) {
        if(customers != null)
        {
            this.customers = new ArrayList<>(customers);
        } else {
            this.customers = null;
        }
    }

    public EmailTemplateEnum getType() {
        return type;
    }

    public void setType(EmailTemplateEnum type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getFoot() {
        return foot;
    }

    public void setFoot(String foot) {
        this.foot = foot;
    }

    public Boolean getAttachFile() {
        return attachFile;
    }

    public void setAttachFile(Boolean attachFile) {
        this.attachFile = attachFile;
    }

    public Boolean getSplittingRecipientsEmail() {
        return splittingRecipientsEmail;
    }

    public void setSplittingRecipientsEmail(Boolean splittingRecipientsEmail) {
        this.splittingRecipientsEmail = splittingRecipientsEmail;
    }

    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    @JsonIgnore
    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public ClaimsRepairEnum getTypeComplaint() {
        return typeComplaint;
    }

    public void setTypeComplaint(ClaimsRepairEnum typeComplaint) {
        this.typeComplaint = typeComplaint;
    }

    public List<ClaimsFlowEnum> getFlows() {
        if(flows == null){
            return null;
        }
        return new ArrayList<>(flows);
    }

    public void setFlows(List<ClaimsFlowEnum> flows) {
        if(flows != null)
        {
            this.flows = new ArrayList<>(flows);
        } else {
            this.flows = null;
        }
    }

    public List<DataAccidentTypeAccidentEnum> getAccidentTypeList() {
        if(accidentTypeList == null){
            return null;
        }
        return new ArrayList<>(accidentTypeList);    }

    public void setAccidentTypeList(List<DataAccidentTypeAccidentEnum> accidentTypeList) {
        if(accidentTypeList != null)
        {
            this.accidentTypeList = new ArrayList<>(accidentTypeList);
        } else {
            this.accidentTypeList = null;
        }
    }


    @Override
    public String toString() {
        return "EmailTemplateResponseV1{" +
                "id=" + id +
                ", applicationEvent=" + applicationEvent +
                ", type=" + type +
                ", description='" + description + '\'' +
                ", object='" + object + '\'' +
                ", heading='" + heading + '\'' +
                ", body='" + body + '\'' +
                ", foot='" + foot + '\'' +
                ", attachFile=" + attachFile +
                ", splittingRecipientsEmail=" + splittingRecipientsEmail +
                ", clientCode='" + clientCode + '\'' +
                ", customers=" + customers +
                ", isActive=" + isActive +
                ", typeComplaint=" + typeComplaint +
                ", flows=" + flows +
                '}';
    }
}
