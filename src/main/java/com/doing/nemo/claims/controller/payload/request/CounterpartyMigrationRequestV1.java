package com.doing.nemo.claims.controller.payload.request;

import com.doing.nemo.claims.controller.payload.request.counterparty.*;
import com.doing.nemo.claims.controller.payload.request.forms.AttachmentRequest;
import com.doing.nemo.claims.dto.AssigneeRepair;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.enumerated.DamagedEnum.VehicleEnum.VehicleTypeEnum;
import com.doing.nemo.claims.entity.enumerated.RepairEnum.ManagementTypeEnum;
import com.doing.nemo.claims.entity.enumerated.RepairEnum.RepairProcedureEnum;
import com.doing.nemo.claims.entity.enumerated.RepairEnum.RepairStatusEnum;
import com.doing.nemo.claims.entity.jsonb.Authority;
import com.doing.nemo.claims.entity.jsonb.counterparty.InsuranceCompanyCounterparty;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.ImpactPoint;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.Insured;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbDriver.Driver;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbVehicle.Vehicle;
import com.doing.nemo.claims.entity.jsonb.forms.jsonbForms.Attachment;
import com.doing.nemo.claims.entity.jsonb.repair.Canalization;
import com.doing.nemo.claims.entity.jsonb.repair.HistoricalCounterparty;
import com.doing.nemo.claims.entity.jsonb.repair.LastContact;
import com.doing.nemo.claims.entity.settings.ManagerEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class CounterpartyMigrationRequestV1 {

    @JsonProperty("counterparty_id")
    private String counterpartyId;

    @JsonProperty("practice_id_counterparty")
    private Long practiceIdCounterparty;

    @JsonProperty("practice_id_claims")
    private Long practiceIdClaims;

    @Enumerated(EnumType.STRING)
    @JsonProperty("type")
    private VehicleTypeEnum type;

    @Enumerated(EnumType.STRING)
    @JsonProperty("status_repair")
    private RepairStatusEnum repairStatus;

    @Enumerated(EnumType.STRING)
    @JsonProperty("procedure_repair")
    private RepairProcedureEnum repairProcedure;

    @JsonProperty("user_create")
    private String userCreate;

    @JsonProperty("created_at")
    private Instant createdAt;

    @JsonProperty("user_update")
    private String userUpdate;

    @JsonProperty("updated_at")
    private Instant updateAt;

    @JsonProperty("assigned_to")
    private AssigneeRepair assignedTo;

    @JsonProperty("assigned_at")
    private Instant assignedAt;

    @JsonProperty("insured")
    private Insured insured;

    @JsonProperty("insurance_company")
    private InsuranceCompanyCounterparty insuranceCompany;

    @JsonProperty("driver")
    private Driver driver;

    @JsonProperty("vehicle")
    private Vehicle vehicle;

    @JsonProperty("is_cai_signed")
    private Boolean isCaiSigned;

    @JsonProperty("impact_point")
    private ImpactPoint impactPoint;

    @JsonProperty("responsible")
    private Boolean responsible;

    @JsonProperty("eligibility")
    private Boolean eligibility;

    @JsonProperty("attachments")
    private List<Attachment> attachments;

    @JsonProperty("historicals")
    private List<HistoricalCounterparty> historicals;

    @JsonProperty("description")
    private String description;

    @JsonProperty("last_contact")
    private List<LastContact> lastContact;

    @JsonProperty("manager")
    private String manager;

    @JsonProperty("canalization")
    private Canalization canalization;

    @JsonProperty("policy_number")
    private String policyNumber;

    @JsonProperty("policy_beginning_validity")
    private Instant policyBeginningValidity;

    @JsonProperty("policy_end_validity")
    private Instant policyEndValidity;

    @Enumerated(EnumType.STRING)
    @JsonProperty("management_type")
    private ManagementTypeEnum managementType;

    @JsonProperty("asked_for_damages")
    private Boolean askedForDamages;

    @JsonProperty("legal_or_consultant")
    private Boolean legalOrConsultant;

    @JsonProperty("date_request_damages")
    private Instant dateRequestDamages;

    @JsonProperty("expiration_date")
    private Instant expirationInstant;

    @JsonProperty("legal")
    private String legal;

    @JsonProperty("email_legal")
    private String emailLegal;

    @JsonProperty("claims_id")
    private String claims;

    @JsonProperty("is_ald")
    private Boolean isAld;

    @JsonProperty("authorities")
    private List<Authority> authorities;

    @JsonProperty("is_read_msa")
    private Boolean isReadMsa;


    public String getCounterpartyId() {
        return counterpartyId;
    }

    public void setCounterpartyId(String counterpartyId) {
        this.counterpartyId = counterpartyId;
    }

    public Long getPracticeIdCounterparty() {
        return practiceIdCounterparty;
    }

    public void setPracticeIdCounterparty(Long practiceIdCounterparty) {
        this.practiceIdCounterparty = practiceIdCounterparty;
    }

    public Long getPracticeIdClaims() {
        return practiceIdClaims;
    }

    public void setPracticeIdClaims(Long practiceIdClaims) {
        this.practiceIdClaims = practiceIdClaims;
    }

    public VehicleTypeEnum getType() {
        return type;
    }

    public void setType(VehicleTypeEnum type) {
        this.type = type;
    }

    public RepairStatusEnum getRepairStatus() {
        return repairStatus;
    }

    public void setRepairStatus(RepairStatusEnum repairStatus) {
        this.repairStatus = repairStatus;
    }

    public RepairProcedureEnum getRepairProcedure() {
        return repairProcedure;
    }

    public void setRepairProcedure(RepairProcedureEnum repairProcedure) {
        this.repairProcedure = repairProcedure;
    }

    public String getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public String getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(String userUpdate) {
        this.userUpdate = userUpdate;
    }

    public Instant getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Instant updateAt) {
        this.updateAt = updateAt;
    }

    public AssigneeRepair getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(AssigneeRepair assignedTo) {
        this.assignedTo = assignedTo;
    }

    public Instant getAssignedAt() {
        return assignedAt;
    }

    public void setAssignedAt(Instant assignedAt) {
        this.assignedAt = assignedAt;
    }

    public Insured getInsured() {
        return insured;
    }

    public void setInsured(Insured insured) {
        this.insured = insured;
    }

    public InsuranceCompanyCounterparty getInsuranceCompany() {
        return insuranceCompany;
    }

    public void setInsuranceCompany(InsuranceCompanyCounterparty insuranceCompany) {
        this.insuranceCompany = insuranceCompany;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public Boolean getCaiSigned() {
        return isCaiSigned;
    }

    public void setCaiSigned(Boolean caiSigned) {
        isCaiSigned = caiSigned;
    }

    public ImpactPoint getImpactPoint() {
        return impactPoint;
    }

    public void setImpactPoint(ImpactPoint impactPoint) {
        this.impactPoint = impactPoint;
    }

    public Boolean getResponsible() {
        return responsible;
    }

    public void setResponsible(Boolean responsible) {
        this.responsible = responsible;
    }

    public Boolean getEligibility() {
        return eligibility;
    }

    public void setEligibility(Boolean eligibility) {
        this.eligibility = eligibility;
    }

    public List<Attachment> getAttachments() {
        if(attachments == null){
            return null;
        }
        return new ArrayList<>(attachments);
    }

    public void setAttachments(List<Attachment> attachments) {
        if(attachments != null){
            this.attachments = new ArrayList<>(attachments);
        } else {
            this.attachments = null;
        }
    }

    public List<HistoricalCounterparty> getHistoricals() {
        if(historicals == null){
            return null;
        }
        return new ArrayList<>(historicals) ;
    }

    public void setHistoricals(List<HistoricalCounterparty> historicals) {
        if(historicals != null)
        {
            this.historicals =  new ArrayList<>(historicals) ;
        } else {
            this.historicals = null;
        }
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<LastContact> getLastContact() {
        if(lastContact == null){
            return null;
        }
        return new ArrayList<>(lastContact);
    }

    public void setLastContact(List<LastContact> lastContact) {
        if(lastContact != null)
        {
            this.lastContact = new ArrayList<>(lastContact);
        } else {
            this.lastContact = null;
        }
    }

    public String getManager() {
        return manager;
    }

    public void setManager(String manager) {
        this.manager = manager;
    }

    public Canalization getCanalization() {
        return canalization;
    }

    public void setCanalization(Canalization canalization) {
        this.canalization = canalization;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public Instant getPolicyBeginningValidity() {
        return policyBeginningValidity;
    }

    public void setPolicyBeginningValidity(Instant policyBeginningValidity) {
        this.policyBeginningValidity = policyBeginningValidity;
    }

    public Instant getPolicyEndValidity() {
        return policyEndValidity;
    }

    public void setPolicyEndValidity(Instant policyEndValidity) {
        this.policyEndValidity = policyEndValidity;
    }

    public ManagementTypeEnum getManagementType() {
        return managementType;
    }

    public void setManagementType(ManagementTypeEnum managementType) {
        this.managementType = managementType;
    }

    public Boolean getAskedForDamages() {
        return askedForDamages;
    }

    public void setAskedForDamages(Boolean askedForDamages) {
        this.askedForDamages = askedForDamages;
    }

    public Boolean getLegalOrConsultant() {
        return legalOrConsultant;
    }

    public void setLegalOrConsultant(Boolean legalOrConsultant) {
        this.legalOrConsultant = legalOrConsultant;
    }

    public Instant getDateRequestDamages() {
        return dateRequestDamages;
    }

    public void setDateRequestDamages(Instant dateRequestDamages) {
        this.dateRequestDamages = dateRequestDamages;
    }

    public Instant getExpirationInstant() {
        return expirationInstant;
    }

    public void setExpirationInstant(Instant expirationInstant) {
        this.expirationInstant = expirationInstant;
    }

    public String getLegal() {
        return legal;
    }

    public void setLegal(String legal) {
        this.legal = legal;
    }

    public String getEmailLegal() {
        return emailLegal;
    }

    public void setEmailLegal(String emailLegal) {
        this.emailLegal = emailLegal;
    }

    public String getClaims() {
        return claims;
    }

    public void setClaims(String claims) {
        this.claims = claims;
    }

    public Boolean getAld() {
        return isAld;
    }

    public void setAld(Boolean ald) {
        isAld = ald;
    }

    public List<Authority> getAuthorities() {
        if(authorities == null){
            return null;
        }
        return new ArrayList<>(authorities) ;
    }

    public void setAuthorities(List<Authority> authorities) {
        if(authorities != null)
        {
            this.authorities =new ArrayList<>(authorities) ;
        } else {
            this.authorities = null;
        }
    }

    public Boolean getReadMsa() {
        return isReadMsa;
    }

    public void setReadMsa(Boolean readMsa) {
        isReadMsa = readMsa;
    }

    @Override
    public String toString() {
        return "CounterpartyMigrationRequestV1{" +
                "practiceIdCounterparty=" + practiceIdCounterparty +
                ", practiceIdClaims=" + practiceIdClaims +
                ", type=" + type +
                ", repairStatus=" + repairStatus +
                ", repairProcedure=" + repairProcedure +
                ", userCreate='" + userCreate + '\'' +
                ", createdAt=" + createdAt +
                ", userUpdate='" + userUpdate + '\'' +
                ", updateAt=" + updateAt +
                ", assignedTo=" + assignedTo +
                ", assignedAt=" + assignedAt +
                ", insured=" + insured +
                ", insuranceCompany=" + insuranceCompany +
                ", driver=" + driver +
                ", vehicle=" + vehicle +
                ", isCaiSigned=" + isCaiSigned +
                ", impactPoint=" + impactPoint +
                ", responsible=" + responsible +
                ", eligibility=" + eligibility +
                ", attachments=" + attachments +
                ", historicals=" + historicals +
                ", description='" + description + '\'' +
                ", lastContact=" + lastContact +
                ", manager='" + manager + '\'' +
                ", canalization=" + canalization +
                ", policyNumber='" + policyNumber + '\'' +
                ", policyBeginningValidity=" + policyBeginningValidity +
                ", policyEndValidity=" + policyEndValidity +
                ", managementType=" + managementType +
                ", askedForDamages=" + askedForDamages +
                ", legalOrConsultant=" + legalOrConsultant +
                ", dateRequestDamages=" + dateRequestDamages +
                ", expirationInstant=" + expirationInstant +
                ", legal='" + legal + '\'' +
                ", emailLegal='" + emailLegal + '\'' +
                ", claims='" + claims + '\'' +
                ", isAld=" + isAld +
                ", authorities=" + authorities +
                ", isReadMsa=" + isReadMsa +
                '}';
    }
}

