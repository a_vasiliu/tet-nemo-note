package com.doing.nemo.claims.controller.payload.response.claims;

import com.doing.nemo.claims.entity.jsonb.Address;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class FoundModelResponse implements Serializable {

    @JsonProperty("date")
    private String date;

    @JsonProperty("hour")
    private String hour;

    @JsonProperty("vehicle_co")
    private String vehicleCo;

    @JsonProperty("is_under_seizure")
    private Boolean isUnderSeizure;

    @JsonProperty("address")
    private Address address;

    @JsonProperty("founded_abroad")
    private Boolean foundedAbroad;

    @JsonProperty("police")
    private Boolean police;

    @JsonProperty("cc")
    private Boolean cc;

    @JsonProperty("vvuu")
    private Boolean vvuu;

    @JsonProperty("authority_data")
    private String authorityData;

    @JsonProperty("phone")
    private String phone;

    @JsonProperty("found_note")
    private String foundNote;

    public FoundModelResponse() {
    }

    public FoundModelResponse(String date, String hour, String vehicleCo, Boolean isUnderSeizure, Address address, Boolean foundedAbroad,
                              Boolean police, Boolean cc, Boolean vvuu, String authorityData, String phone, String foundNote) {
        this.date = date;
        this.hour = hour;
        this.vehicleCo = vehicleCo;
        this.isUnderSeizure = isUnderSeizure;
        this.address = address;
        this.foundedAbroad = foundedAbroad;
        this.police = police;
        this.cc = cc;
        this.vvuu = vvuu;
        this.authorityData = authorityData;
        this.phone = phone;
        this.foundNote = foundNote;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getVehicleCo() {
        return vehicleCo;
    }

    public void setVehicleCo(String vehicleCo) {
        this.vehicleCo = vehicleCo;
    }

    public Boolean getUnderSeizure() {
        return isUnderSeizure;
    }

    public void setUnderSeizure(Boolean underSeizure) {
        isUnderSeizure = underSeizure;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Boolean getFoundedAbroad() {
        return foundedAbroad;
    }

    public void setFoundedAbroad(Boolean foundedAbroad) {
        this.foundedAbroad = foundedAbroad;
    }

    public Boolean getPolice() {
        return police;
    }

    public void setPolice(Boolean police) {
        this.police = police;
    }

    public Boolean getCc() {
        return cc;
    }

    public void setCc(Boolean cc) {
        this.cc = cc;
    }

    public Boolean getVvuu() {
        return vvuu;
    }

    public void setVvuu(Boolean vvuu) {
        this.vvuu = vvuu;
    }

    public String getAuthorityData() {
        return authorityData;
    }

    public void setAuthorityData(String authorityData) {
        this.authorityData = authorityData;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFoundNote() {
        return foundNote;
    }

    public void setFoundNote(String foundNote) {
        this.foundNote = foundNote;
    }

    @Override
    public String toString() {
        return "foundModel{" +
                "date=" + date +
                ", hour='" + hour + '\'' +
                ", vehicleCo='" + vehicleCo + '\'' +
                ", isUnderSeizure=" + isUnderSeizure +
                ", address=" + address +
                ", foundedAbroad=" + foundedAbroad +
                ", police=" + police +
                ", cc=" + cc +
                ", vvuu=" + vvuu +
                ", authorityData='" + authorityData + '\'' +
                ", phone='" + phone + '\'' +
                ", foundNote='" + foundNote + '\'' +
                '}';
    }
}