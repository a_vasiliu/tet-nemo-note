package com.doing.nemo.claims.controller.payload.response.damaged;

import com.doing.nemo.claims.entity.enumerated.DamagedEnum.VehicleEnum.VehicleOwnershipEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Date;

public class VehicleResponse implements Serializable {

    @JsonProperty("fleet_vehicle_id")
    private Long fleetVehicleId;

    @JsonProperty("make")
    private String make;

    @JsonProperty("model")
    private String model;

    @JsonProperty("model_year")
    private String modelYear;

    @JsonProperty("kw")
    private String kw;

    @JsonProperty("cc_3")
    private String cc3;

    @JsonProperty("hp")
    private Integer hp;

    @JsonProperty("din_hp")
    private Integer dinHp;

    @JsonProperty("seats")
    private Integer seats;

    @JsonProperty("net_weight")
    private Double netWeight;

    @JsonProperty("max_weight")
    private Double maxWeight;

    @JsonProperty("doors")
    private String doors;

    @JsonProperty("body_style")
    private String bodyStyle;

    @JsonProperty("nature")
    private String nature;

    @JsonProperty("fuel_type")
    private String fuelType;

    @JsonProperty("co_2emission")
    private String co2emission;

    @JsonProperty("consumption")
    private String consumption;

    @JsonProperty("license_plate")
    private String licensePlate;

    @JsonProperty("net_book_value")
    private Double netBookValue;

    @JsonProperty("wreck")
    private Boolean wreck;

    @JsonProperty("ownership")
    private VehicleOwnershipEnum ownership;

    @JsonProperty("chassis_number")
    private String chassisNumber;

    @JsonProperty("registration_country")
    private String registrationCountry;

    @JsonProperty("registration_date")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date registrationDate;

    @JsonProperty("last_known_mileage")
    private String lastKnownMileage;

    @JsonProperty("external_color")
    private String externalColor;

    @JsonProperty("internal_color")
    private String internalColor;

    @JsonProperty("license_plate_trailer")
    private String licensePlateTrailer;

    @JsonProperty("chassis_number_trailer")
    private String chassisNumberTrailer;

    @JsonProperty("registration_country_trailer")
    private String registrationCountryTrailer;

    @JsonProperty("wreck_date")
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    private Date wreckDate;

    public Date getWreckDate() {
        if(wreckDate == null){
            return null;
        }
        return (Date)wreckDate.clone();
    }

    public void setWreckDate(Date wreckDate) {
        if(wreckDate != null)
        {
            this.wreckDate = (Date)wreckDate.clone();
        } else {
            this.wreckDate = null;
        }
    }

    public Double getNetBookValue() {
        return netBookValue;
    }

    public void setNetBookValue(Double netBookValue) {
        this.netBookValue = netBookValue;
    }

    public Boolean getWreck() {
        return wreck;
    }

    public void setWreck(Boolean wreck) {
        this.wreck = wreck;
    }

    public Long getFleetVehicleId() {
        return fleetVehicleId;
    }

    public void setFleetVehicleId(Long fleetVehicleId) {
        this.fleetVehicleId = fleetVehicleId;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getModelYear() {
        return modelYear;
    }

    public void setModelYear(String modelYear) {
        this.modelYear = modelYear;
    }

    public String getKw() {
        return kw;
    }

    public void setKw(String kw) {
        this.kw = kw;
    }

    public String getCc3() {
        return cc3;
    }

    public void setCc3(String cc3) {
        this.cc3 = cc3;
    }

    public Integer getHp() {
        return hp;
    }

    public void setHp(Integer hp) {
        this.hp = hp;
    }

    public Integer getDinHp() {
        return dinHp;
    }

    public void setDinHp(Integer dinHp) {
        this.dinHp = dinHp;
    }

    public Integer getSeats() {
        return seats;
    }

    public void setSeats(Integer seats) {
        this.seats = seats;
    }

    public Double getNetWeight() {
        return netWeight;
    }

    public void setNetWeight(Double netWeight) {
        this.netWeight = netWeight;
    }

    public Double getMaxWeight() {
        return maxWeight;
    }

    public void setMaxWeight(Double maxWeight) {
        this.maxWeight = maxWeight;
    }

    public String getDoors() {
        return doors;
    }

    public void setDoors(String doors) {
        this.doors = doors;
    }

    public String getBodyStyle() {
        return bodyStyle;
    }

    public void setBodyStyle(String bodyStyle) {
        this.bodyStyle = bodyStyle;
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    public String getCo2emission() {
        return co2emission;
    }

    public void setCo2emission(String co2emission) {
        this.co2emission = co2emission;
    }

    public String getConsumption() {
        return consumption;
    }

    public void setConsumption(String consumption) {
        this.consumption = consumption;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public VehicleOwnershipEnum getOwnership() {
        return ownership;
    }

    public void setOwnership(VehicleOwnershipEnum ownership) {
        this.ownership = ownership;
    }

    public String getChassisNumber() {
        return chassisNumber;
    }

    public void setChassisNumber(String chassisNumber) {
        this.chassisNumber = chassisNumber;
    }

    public String getRegistrationCountry() {
        return registrationCountry;
    }

    public void setRegistrationCountry(String registrationCountry) {
        this.registrationCountry = registrationCountry;
    }

    public Date getRegistrationDate() {
        if(registrationDate == null){
            return  null;
        }
        return (Date)registrationDate.clone();
    }

    public void setRegistrationDate(Date registrationDate) {
        if(registrationDate != null)
        {
            this.registrationDate = (Date)registrationDate.clone();
        } else {
            this.registrationDate = null;
        }
    }

    public String getLastKnownMileage() {
        return lastKnownMileage;
    }

    public void setLastKnownMileage(String lastKnownMileage) {
        this.lastKnownMileage = lastKnownMileage;
    }

    public String getExternalColor() {
        return externalColor;
    }

    public void setExternalColor(String externalColor) {
        this.externalColor = externalColor;
    }

    public String getInternalColor() {
        return internalColor;
    }

    public void setInternalColor(String internalColor) {
        this.internalColor = internalColor;
    }

    public String getLicensePlateTrailer() {
        return licensePlateTrailer;
    }

    public void setLicensePlateTrailer(String licensePlateTrailer) {
        this.licensePlateTrailer = licensePlateTrailer;
    }

    public String getChassisNumberTrailer() {
        return chassisNumberTrailer;
    }

    public void setChassisNumberTrailer(String chassisNumberTrailer) {
        this.chassisNumberTrailer = chassisNumberTrailer;
    }

    public String getRegistrationCountryTrailer() {
        return registrationCountryTrailer;
    }

    public void setRegistrationCountryTrailer(String registrationCountryTrailer) {
        this.registrationCountryTrailer = registrationCountryTrailer;
    }


    @Override
    public String toString() {
        return "Vehicle{" +
                "fleetVehicleId='" + fleetVehicleId + '\'' +
                ", make='" + make + '\'' +
                ", model='" + model + '\'' +
                ", modelYear='" + modelYear + '\'' +
                ", kw='" + kw + '\'' +
                ", cc3='" + cc3 + '\'' +
                ", hp='" + hp + '\'' +
                ", dinHp='" + dinHp + '\'' +
                ", seats='" + seats + '\'' +
                ", netWeight='" + netWeight + '\'' +
                ", maxWeight='" + maxWeight + '\'' +
                ", doors='" + doors + '\'' +
                ", bodyStyle='" + bodyStyle + '\'' +
                ", nature='" + nature + '\'' +
                ", fuelType='" + fuelType + '\'' +
                ", co2emission='" + co2emission + '\'' +
                ", consumption='" + consumption + '\'' +
                ", licensePlate='" + licensePlate + '\'' +
                ", ownership=" + ownership +
                ", chassisNumber='" + chassisNumber + '\'' +
                ", registrationCountry='" + registrationCountry + '\'' +
                ", registrationDate='" + registrationDate + '\'' +
                ", lastKnownMileage='" + lastKnownMileage + '\'' +
                ", externalColor='" + externalColor + '\'' +
                ", internalColor='" + internalColor + '\'' +
                ", licensePlateTrailer='" + licensePlateTrailer + '\'' +
                ", chassisNumberTrailer='" + chassisNumberTrailer + '\'' +
                ", registrationCountryTrailer='" + registrationCountryTrailer + '\'' +
                '}';
    }


}