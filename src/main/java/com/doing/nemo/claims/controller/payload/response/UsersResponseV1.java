package com.doing.nemo.claims.controller.payload.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;

import java.io.Serializable;

@ApiModel
public class UsersResponseV1 implements Serializable {

    @JsonProperty("id")
    private String id;

    @JsonProperty("is_active")
    private Boolean isActive;

    @JsonProperty("username")
    private String username;

    @JsonProperty("personal_details")
    private UsersPersonalDetailsResponseV1 personalDetails;

    public UsersResponseV1() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public UsersPersonalDetailsResponseV1 getPersonalDetails() {
        return personalDetails;
    }

    public void setPersonalDetails(UsersPersonalDetailsResponseV1 personalDetails) {
        this.personalDetails = personalDetails;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("UsersResponseV1{");
        sb.append("id='").append(id).append('\'');
        sb.append(", isActive=").append(isActive);
        sb.append(", username='").append(username).append('\'');
        sb.append(", personalDetails=").append(personalDetails);
        sb.append('}');
        return sb.toString();
    }

}
