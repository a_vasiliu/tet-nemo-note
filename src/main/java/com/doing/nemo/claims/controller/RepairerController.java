package com.doing.nemo.claims.controller;

import com.doing.nemo.claims.adapter.RepairerAdapter;
import com.doing.nemo.claims.controller.payload.request.ListRequestV1;
import com.doing.nemo.claims.controller.payload.request.RepairerRequestV1;
import com.doing.nemo.claims.controller.payload.response.ClaimsResponsePaginationV1;
import com.doing.nemo.claims.controller.payload.response.IdResponseV1;
import com.doing.nemo.claims.controller.payload.response.RepairerResponseV1;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.settings.RepairerEntity;
import com.doing.nemo.claims.service.RepairerService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/v1")
@Api("Repairer")
public class RepairerController {

    @Autowired
    private RepairerService repairerService;

    @Autowired
    private RepairerAdapter repairerAdapter;

    @PostMapping("/repairer")
    @Transactional
    @ApiOperation(value = "Insert Repairer", notes = "Insert insurance repairer using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = IdResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<IdResponseV1> insertRepairer(
            @ApiParam(value = "Body of the Repairer to be created", required = true)
            @RequestBody RepairerRequestV1 repairerRequest) {

        RepairerEntity repairerEntity = repairerService.insertRepairer(RepairerAdapter.adptFromRepairerRequestToRepairerEntity(repairerRequest));
        IdResponseV1 responseV1 = new IdResponseV1(repairerEntity.getId());
        return new ResponseEntity<>(responseV1, HttpStatus.CREATED);

    }

    @PostMapping("/repairers")
    @Transactional
    @ApiOperation(value = "Insert a list of Repairers", notes = "Insert a list of insurance repairers using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = IdResponseV1.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<RepairerResponseV1>> insertRepairers(
            @ApiParam(value = "List of the bodies of the Insurance Companies to be created", required = true)
            @RequestBody ListRequestV1<RepairerRequestV1> repairersRequest) {

        List<RepairerResponseV1> repairerResponseV1List = new ArrayList<>();
        for (RepairerRequestV1 repairerRequestV1 : repairersRequest.getData()) {

            RepairerEntity repairerEntity = RepairerAdapter.adptFromRepairerRequestToRepairerEntity(repairerRequestV1);
            repairerEntity = repairerService.insertRepairer(repairerEntity);
            RepairerResponseV1 responseV1 = RepairerAdapter.adptFromRepairerEntityToRepairerResponse(repairerEntity);
            repairerResponseV1List.add(responseV1);
        }
        return new ResponseEntity<>(repairerResponseV1List, HttpStatus.CREATED);

    }

    @GetMapping("/repairer/{UUID}")
    @Transactional
    @ApiOperation(value = "Recover Repairer", notes = "Returns a Repairer using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = RepairerResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<RepairerResponseV1> getRepairers(
            @ApiParam(value = "UUID of the Repairer to be found", required = true)
            @PathVariable UUID UUID) {

        RepairerEntity repairerEntity = repairerService.selectRepairer(UUID);
        RepairerResponseV1 responseV1 = RepairerAdapter.adptFromRepairerEntityToRepairerResponse(repairerEntity);
        return new ResponseEntity<>(responseV1, HttpStatus.OK);
    }

    @GetMapping("/repairer")
    @Transactional
    @ApiOperation(value = "Recover All Repairers", notes = "Returns all Repairers")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = RepairerResponseV1.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<RepairerResponseV1>> getAllRepairers() {

        List repairerEntityList = repairerService.selectAllRepairer();

        List<RepairerResponseV1> responseV1 = RepairerAdapter.adptFromRepairerEntityToRepairerResponseList(repairerEntityList);
        return new ResponseEntity<>(responseV1, HttpStatus.OK);
    }

    @PutMapping("/repairer/{UUID}")
    @Transactional
    @ApiOperation(value = "Upload Repairer", notes = "Upload Repairer using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = RepairerResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<RepairerResponseV1> putRepairers(
            @ApiParam(value = "UUID that identifies the Repairer to be modified", required = true)
            @PathVariable UUID UUID,
            @ApiParam(value = "Updated body of the Repairer", required = true)
            @RequestBody RepairerRequestV1 repairersRequest) {


        RepairerEntity repairerEntity = repairerAdapter.adptFromRepairerRequestToRepairerEntityWithID(repairersRequest, UUID);

        repairerEntity = repairerService.updateRepairer(repairerEntity);
        RepairerResponseV1 responseV1 = RepairerAdapter.adptFromRepairerEntityToRepairerResponse(repairerEntity);
        return new ResponseEntity<>(responseV1, HttpStatus.OK);

    }

    @DeleteMapping("/repairer/{UUID}")
    @Transactional
    @ApiOperation(value = "Delete Repairer", notes = "Delete Repairer using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = RepairerResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity deleteRepairers(
            @ApiParam(value = "UUID that identifies the Repairer to be deleted", required = true)
            @PathVariable UUID UUID) {
        repairerService.deleteRepairer(UUID);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PatchMapping("/repairer/{UUID}/active")
    @Transactional
    @ApiOperation(value = "Patch status active Repairer", notes = "Patch status active Repairer using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = RepairerResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<RepairerResponseV1> patchStatusRepairer(
            @PathVariable UUID UUID
    ) {
        RepairerEntity repairerEntity = repairerService.patchActive(UUID);
        RepairerResponseV1 responseV1 = RepairerAdapter.adptFromRepairerEntityToRepairerResponse(repairerEntity);
        return new ResponseEntity<>(responseV1, HttpStatus.OK);
    }

    @ApiOperation(value="Repairer Pagination", notes = "It retrieves Repairer pagination")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = ClaimsResponsePaginationV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @GetMapping(value="/repairer/pagination", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<ClaimsResponsePaginationV1> repairerPagination(
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @RequestParam(value = "page_size", defaultValue = "20") Integer pageSize
    ){

        Pagination<RepairerEntity> repairerPagination = repairerService.paginationRepairer(page, pageSize);

        return new ResponseEntity<>(repairerAdapter.adptRepairerPaginationToClaimsResponsePagination(repairerPagination), HttpStatus.OK);
    }
}
