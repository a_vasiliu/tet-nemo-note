package com.doing.nemo.claims.controller.payload.request.damaged;

import com.doing.nemo.claims.entity.jsonb.Address;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class FleetManagerRequest implements Serializable {

    @JsonProperty("id")
    private Long id;

    @JsonProperty("identification")
    private String identification;

    @JsonProperty("official_registration")
    private String officialRegistration;

    @JsonProperty("first_name")
    private String firstName;

    @JsonProperty("last_name")
    private String lastName;

    @JsonProperty("title")
    private String title;

    @JsonProperty("email")
    private String email;

    @JsonProperty("phone_prefix")
    private String phonePrefix;

    @JsonProperty("phone")
    private String phone;

    @JsonProperty("secondary_phone_prefix")
    private String secondaryPhonePrefix;

    @JsonProperty("secondary_phone")
    private String secondaryPhone;

    @JsonProperty("sex")
    private String sex;

    @JsonProperty("customer_id")
    private Long customerId;

    @JsonProperty("main_address")
    private Address mainAddress;

    @JsonProperty("is_imported")
    private Boolean isImported;

    @JsonProperty("disable_notification")
    private Boolean disableNotification;

    public FleetManagerRequest() {
    }

    public FleetManagerRequest(Long id, String identification, String officialRegistration, String firstName, String lastName, String title, String email, String phonePrefix, String phone, String secondaryPhonePrefix, String secondaryPhone, String sex, Long customerId, Address mainAddress) {
        this.id = id;
        this.identification = identification;
        this.officialRegistration = officialRegistration;
        this.firstName = firstName;
        this.lastName = lastName;
        this.title = title;
        this.email = email;
        this.phonePrefix = phonePrefix;
        this.phone = phone;
        this.secondaryPhonePrefix = secondaryPhonePrefix;
        this.secondaryPhone = secondaryPhone;
        this.sex = sex;
        this.customerId = customerId;
        this.mainAddress = mainAddress;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public String getOfficialRegistration() {
        return officialRegistration;
    }

    public void setOfficialRegistration(String officialRegistration) {
        this.officialRegistration = officialRegistration;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhonePrefix() {
        return phonePrefix;
    }

    public void setPhonePrefix(String phonePrefix) {
        this.phonePrefix = phonePrefix;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSecondaryPhonePrefix() {
        return secondaryPhonePrefix;
    }

    public void setSecondaryPhonePrefix(String secondaryPhonePrefix) {
        this.secondaryPhonePrefix = secondaryPhonePrefix;
    }

    public String getSecondaryPhone() {
        return secondaryPhone;
    }

    public void setSecondaryPhone(String secondaryPhone) {
        this.secondaryPhone = secondaryPhone;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public Address getMainAddress() {
        return mainAddress;
    }

    public void setMainAddress(Address mainAddress) {
        this.mainAddress = mainAddress;
    }

    public Boolean getIsImported() { return this.isImported;}

    public void setIsImported(Boolean isImported) { this.isImported = isImported; }

    public void setDisableNotification(Boolean disableNotification){
        if(disableNotification == null){
            this.disableNotification = false;
        }else{
            this.disableNotification = disableNotification;
        }
    }

    public Boolean isDisableNotification(){
        if(this.disableNotification == null){
            return false;
        }else{
            return this.disableNotification;
        }
    }

    @Override
    public String toString() {
        return "FleetManagerRequest{" +
                "id=" + id +
                ", identification='" + identification + '\'' +
                ", officialRegistration='" + officialRegistration + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", title='" + title + '\'' +
                ", email='" + email + '\'' +
                ", phonePrefix='" + phonePrefix + '\'' +
                ", phone='" + phone + '\'' +
                ", secondaryPhonePrefix='" + secondaryPhonePrefix + '\'' +
                ", secondaryPhone='" + secondaryPhone + '\'' +
                ", sex='" + sex + '\'' +
                ", customerId=" + customerId + '\'' +
                ", mainAddress=" + mainAddress +
                ", isImported=" + isImported +
                '}';
    }

}
