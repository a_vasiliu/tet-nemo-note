package com.doing.nemo.claims.controller.payload.response;

import com.doing.nemo.claims.controller.payload.response.registration.PlateRegistrationHistoryItem;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public class VehicleRegistrationResponse implements Serializable {

    private static final long serialVersionUID = 3923808137039795821L;
    @JsonProperty("plate")
    private String plate;
    @JsonProperty("current_plate")
    private String currentPlate;
    @JsonProperty("plate_history")
    private List<PlateRegistrationHistoryItem> plateHistoryItems;

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public String getCurrentPlate() {
        return currentPlate;
    }

    public void setCurrentPlate(String currentPlate) {
        this.currentPlate = currentPlate;
    }

    public List<PlateRegistrationHistoryItem> getPlateHistoryItems() {
        return plateHistoryItems;
    }

    public void setPlateHistoryItems(List<PlateRegistrationHistoryItem> plateHistoryItems) {
        this.plateHistoryItems = plateHistoryItems;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VehicleRegistrationResponse that = (VehicleRegistrationResponse) o;
        return plate.equals(that.plate) && currentPlate.equals(that.currentPlate) && plateHistoryItems.equals(that.plateHistoryItems);
    }

    @Override
    public int hashCode() {
        return Objects.hash(plate, currentPlate, plateHistoryItems);
    }
}
