package com.doing.nemo.claims.controller;


import com.doing.nemo.claims.adapter.ContractPersonalDataAdapter;
import com.doing.nemo.claims.adapter.CustomTypeRiskAdapter;
import com.doing.nemo.claims.adapter.InsurancePolicyPersonalDataAdapter;
import com.doing.nemo.claims.adapter.PersonalDataAdapter;
import com.doing.nemo.claims.common.validation.MessageCode;
import com.doing.nemo.claims.common.validation.RequestValidator;
import com.doing.nemo.claims.controller.payload.request.*;
import com.doing.nemo.claims.controller.payload.response.PaginationResponseV1;
import com.doing.nemo.claims.controller.payload.response.IdResponseV1;
import com.doing.nemo.claims.controller.payload.response.PersonalDataResponseV1;
import com.doing.nemo.claims.entity.settings.ContractEntity;
import com.doing.nemo.claims.entity.settings.CustomTypeRiskEntity;
import com.doing.nemo.claims.entity.settings.InsurancePolicyPersonalDataEntity;
import com.doing.nemo.claims.entity.settings.PersonalDataEntity;
import com.doing.nemo.claims.repository.PersonalDataRepositoryV1;
import com.doing.nemo.claims.service.PersonalDataService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/v1")
@Api("PersonalData")
public class PersonalDataController {

    @Autowired
    private PersonalDataService personalDataService;

    @Autowired
    private PersonalDataAdapter personalDataAdapter;

    @Autowired
    private ContractPersonalDataAdapter contractPersonalDataAdapter;

    @Autowired
    private CustomTypeRiskAdapter customTypeRiskAdapter;

    @Autowired
    private InsurancePolicyPersonalDataAdapter insurancePolicyPersonalDataAdapter;

    @Autowired
    private RequestValidator requestValidator;

    @Autowired
    private PersonalDataRepositoryV1 personalDataRepositoryV1;

    @PostMapping("/personaldata")
    @Transactional
    @ApiOperation(value = "Insert PersonalData", notes = "Insert PersonalData using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = IdResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<IdResponseV1> postPersonalData(@ApiParam(value = "Body of the PersonalData to be created", required = true) @RequestBody PersonalDataRequestV1 personalDataRequestV1) {
        requestValidator.validateRequest(personalDataRequestV1, MessageCode.E00X_1000);
        PersonalDataEntity personalDataEntity = personalDataAdapter.adptFromPersonalDataRequestToPersonalDataEntity(personalDataRequestV1);
        personalDataEntity = personalDataService.attachFatherToPersonalDataInsert(personalDataEntity);
        //recupero i fleet manager


        personalDataEntity = personalDataService.insertPersonalData(personalDataEntity);
        IdResponseV1 personalDataResponseIdV1 = new IdResponseV1(personalDataEntity.getId());
        return new ResponseEntity(personalDataResponseIdV1, HttpStatus.CREATED);
    }

    @PostMapping("/personaldata/customtyperisk/{UUID}")
    @Transactional
    @ApiOperation(value = "Insert CustomTypeRisks for PersonalData", notes = "Insert customTypeRisks for personalData using body in the request")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = PersonalDataResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<PersonalDataResponseV1> postCustomTypeRisksForPersonalData(@PathVariable(name = "UUID") UUID id,
                                                                                     @ApiParam(value = "Body of the Rule to be created", required = true) @RequestBody ListRequestV1<CustomTypeRiskRequestV1> customTypeRiskRequestV1List) {
        List<CustomTypeRiskEntity> customTypeRiskEntity = customTypeRiskAdapter.adptFromCustomTypeRiskRequestToCustomTypeRiskEntityList(customTypeRiskRequestV1List.getData());
        PersonalDataEntity personalDataEntity = personalDataService.insertPersonalDataCustomTypeRisk(customTypeRiskEntity, id);

        PersonalDataResponseV1 responseV1 = PersonalDataAdapter.adptFromPersonalDataEntityToPersonalDataResponseV1(personalDataEntity);
        return new ResponseEntity<>(responseV1, HttpStatus.CREATED);
    }


    @PostMapping("/personaldata/insurance_policy/{UUID}")
    @Transactional
    @ApiOperation(value = "Insert InsurancePolicyPersonalDatas for PersonalData", notes = "Insert insurancePolicyPersonalDatas for personalData using body in the request")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = PersonalDataResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<PersonalDataResponseV1> postInsurancePolicyPersonalDatasForPersonalData(@PathVariable(name = "UUID") UUID id,
                                                                                                  @ApiParam(value = "Body of the Rule to be created", required = true) @RequestBody ListRequestV1<InsurancePolicyPersonalDataRequestV1> insurancePolicyPersonalDataRequestV1List) {
        List<InsurancePolicyPersonalDataEntity> insurancePolicyPersonalDataEntity = insurancePolicyPersonalDataAdapter.adptFromInsurancePolicyPersonalDataRequestToInsurancePolicyPersonalDataEntityList(insurancePolicyPersonalDataRequestV1List.getData());
        PersonalDataEntity personalDataEntity = personalDataService.insertPersonalDataInsurancePolicyPersonalData(insurancePolicyPersonalDataEntity, id);

        PersonalDataResponseV1 responseV1 = PersonalDataAdapter.adptFromPersonalDataEntityToPersonalDataResponseV1(personalDataEntity);
        return new ResponseEntity<>(responseV1, HttpStatus.CREATED);
    }


    @PostMapping("/personaldata/contract/{UUID}")
    @Transactional
    @ApiOperation(value = "Insert Contracts for PersonalData", notes = "Insert contracts for personalData using body in the request")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = PersonalDataResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<PersonalDataResponseV1> postContractsForPersonalData(@PathVariable(name = "UUID") UUID id,
                                                                               @ApiParam(value = "Body of the Rule to be created", required = true) @RequestBody ListRequestV1<ContractRequestV1> contractRequestV1List) {
        List<ContractEntity> contractEntity = contractPersonalDataAdapter.adptFromContractRequestToContractEntityList(contractRequestV1List.getData());
        PersonalDataEntity personalDataEntity = personalDataService.insertPersonalDataContract(contractEntity, id);

        PersonalDataResponseV1 responseV1 = PersonalDataAdapter.adptFromPersonalDataEntityToPersonalDataResponseV1(personalDataEntity);
        return new ResponseEntity<>(responseV1, HttpStatus.CREATED);
    }


    @PostMapping("/personaldatas")
    @Transactional
    @ApiOperation(value = "Insert More PersonalDatas", notes = "Insert PersonalDatas using info passed in the bodies")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = PersonalDataResponseV1.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<PersonalDataResponseV1>> postPersonalDatas(@ApiParam(value = "Bodies of the PersonalDatas to be created", required = true) @RequestBody ListRequestV1<PersonalDataRequestV1> personalDataListRequestV1) {

        List<PersonalDataResponseV1> personalDataResponseV1List = new ArrayList<>();
        for (PersonalDataRequestV1 personalDataRequestV1 : personalDataListRequestV1.getData()) {
            requestValidator.validateRequest(personalDataRequestV1, MessageCode.E00X_1000);
            PersonalDataEntity personalDataEntity = personalDataAdapter.adptFromPersonalDataRequestToPersonalDataEntity(personalDataRequestV1);
            personalDataEntity = personalDataService.insertPersonalData(personalDataEntity);
            PersonalDataResponseV1 personalDataResponseV1 = PersonalDataAdapter.adptFromPersonalDataEntityToPersonalDataResponseV1(personalDataEntity);
            personalDataResponseV1List.add(personalDataResponseV1);
        }
        return new ResponseEntity(personalDataResponseV1List, HttpStatus.CREATED);
    }

    @PutMapping("/personaldata/{UUID}")
    @Transactional
    @ApiOperation(value = "Upload PersonalData", notes = "Upload PersonalData using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = PersonalDataResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<PersonalDataResponseV1> putPersonalData(
            @ApiParam(value = "UUID that identifies the PersonalData to be modified", required = true)
            @PathVariable(name = "UUID") UUID id,
            @ApiParam(value = "Updated body of the PersonalData", required = true)
            @RequestBody PersonalDataRequestV1 personalDataRequestV1) {
        requestValidator.validateRequest(personalDataRequestV1, MessageCode.E00X_1000);
        PersonalDataEntity personalDataEntity = personalDataService.selectPersonalData(id);
        String precFatherId = personalDataEntity.getPersonalFatherId();
        PersonalDataEntity personalDataEntityNew = personalDataAdapter.adptFromPersonalDataRequestToPersonalDataEntity(personalDataRequestV1);
        personalDataEntityNew.setId(personalDataEntity.getId());
        personalDataEntityNew = personalDataService.attachFatherToPersonalDataUpdate(personalDataEntityNew, precFatherId);
        personalDataEntityNew = personalDataService.updatePersonalData(personalDataEntityNew, id);
        PersonalDataResponseV1 personalDataResponseV1 = PersonalDataAdapter.adptFromPersonalDataEntityToPersonalDataResponseV1(personalDataEntityNew);
        return new ResponseEntity<>(personalDataResponseV1, HttpStatus.OK);
    }

    @GetMapping("/personaldata/{UUID}")
    @ApiOperation(value = "Recover PersonalData", notes = "Return a PersonalData using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = PersonalDataResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<PersonalDataResponseV1> getPersonalData(@ApiParam(value = "UUID of the PersonalData to be found", required = true) @PathVariable(name = "UUID") UUID id) {
        PersonalDataEntity personalDataEntity = personalDataService.getPersonalDataAndUpdateFleetManagerList(id);
        PersonalDataResponseV1 personalDataResponseV1 = PersonalDataAdapter.adptFromPersonalDataEntityToPersonalDataResponseV1(personalDataEntity);
        return new ResponseEntity<>(personalDataResponseV1, HttpStatus.OK);
    }

    @GetMapping("/personaldata")
    @Transactional
    @ApiOperation(value = "Recover All PersonalDatas", notes = "Returns all PersonalDatas")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = PersonalDataResponseV1.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<PersonalDataResponseV1>> getAllPersonalData() {
        List<PersonalDataEntity> personalDataEntityList = personalDataService.selectAllPersonalData();
        List<PersonalDataResponseV1> personalDataResponseV1List = PersonalDataAdapter.adptFromPersonalDataEntityToPersonalDataResponseV1List(personalDataEntityList);
        return new ResponseEntity(personalDataResponseV1List, HttpStatus.OK);
    }

    @DeleteMapping("/personaldata/{UUID}")
    @Transactional
    @ApiOperation(value = "Delete PersonalData", notes = "Delete a PersonalData using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = PersonalDataResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<PersonalDataResponseV1> deletePersonalData(@ApiParam(value = "UUID of the PersonalData to be deleted", required = true) @PathVariable(name = "UUID") UUID id) {
        PersonalDataResponseV1 personalDataResponseV1 = personalDataService.deletePersonalData(id);
        return new ResponseEntity<>(personalDataResponseV1, HttpStatus.OK);
    }

    @PatchMapping("/personaldata/{UUID}/active")
    @Transactional
    @ApiOperation(value = "Patch active status PersonalData", notes = "Patch active status of PersonalData using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = PersonalDataResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<PersonalDataResponseV1> patchStatusPersonalData(
            @PathVariable UUID UUID
    ) {
        PersonalDataEntity personalDataEntity = personalDataService.updateStatus(UUID);
        PersonalDataResponseV1 responseV1 = PersonalDataAdapter.adptFromPersonalDataEntityToPersonalDataResponseV1(personalDataEntity);
        return new ResponseEntity<>(responseV1, HttpStatus.OK);
    }

    @ApiOperation(value="Search PersonalData", notes = "It retrieves personalData data through one of more parameters")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = PaginationResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @GetMapping(value="/personaldata/pagination", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<PaginationResponseV1<PersonalDataResponseV1>> searchPersonalData(
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @RequestParam(value = "page_size", defaultValue = "20") Integer pageSize,
            @RequestParam(value = "order_by", required = false) String orderBy,
            @RequestParam(value = "asc", required = false, defaultValue = "true") Boolean asc,
            @RequestParam(value = "personal_code", required = false) Long personalCode,
            @RequestParam(value = "personal_group", required = false) String personalGroup,
            @RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "city", required = false) String city,
            @RequestParam(value = "customer_id", required = false) String customerId,
            @RequestParam(value = "province", required = false) String province,
            @RequestParam(value = "phone_number", required = false) String phoneNumber,
            @RequestParam(value = "fax", required = false) String fax,
            @RequestParam(value = "vat_number", required = false) String vatNumber,
            @RequestParam(value = "is_active", required = false) Boolean isActive
    ){

        List<PersonalDataEntity> listPersonalData = personalDataService.findPersonalDatasFilteredAscName(page,pageSize, orderBy, asc, personalCode, personalGroup, name, city, province, phoneNumber, fax, vatNumber, isActive, customerId);
        Long itemCount = personalDataService.countPersonalDataFiltered(personalCode, personalGroup, name, city, province, phoneNumber, fax, vatNumber, isActive, customerId);

        PaginationResponseV1<PersonalDataResponseV1> paginationResponseV1 = PersonalDataAdapter.adptPagination(listPersonalData, page, pageSize, itemCount);

        return new ResponseEntity<>(paginationResponseV1, HttpStatus.OK);
    }

}