package com.doing.nemo.claims.controller.payload.request.authority.repair;

import com.doing.nemo.claims.entity.enumerated.AuthorityEnum.AuthorityRepairStatusEnum;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class AuthorityRepairStatusRequestV1 implements Serializable {
    @JsonProperty("repair_status")
    private AuthorityRepairStatusEnum repairStatus;

    public AuthorityRepairStatusRequestV1(AuthorityRepairStatusEnum repairStatus) {
        this.repairStatus = repairStatus;
    }

    public AuthorityRepairStatusRequestV1() {
    }

    public AuthorityRepairStatusEnum getRepairStatus() {
        return repairStatus;
    }

    public void setRepairStatus(AuthorityRepairStatusEnum repairStatus) {
        this.repairStatus = repairStatus;
    }
}
