package com.doing.nemo.claims.controller.payload.request.driver;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class TestRequest  implements Serializable {

    @JsonProperty("test")
    private String test;

    public TestRequest() {
    }

    public TestRequest(String test) {
        this.test = test;
    }

    public String getTest() {
        return test;
    }

    public void setTest(String test) {
        this.test = test;
    }

    @Override
    public String toString() {
        return "TestRequestV1{" +
                "test='" + test + '\'' +
                '}';
    }

}