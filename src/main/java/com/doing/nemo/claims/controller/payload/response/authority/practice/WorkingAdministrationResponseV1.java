package com.doing.nemo.claims.controller.payload.response.authority.practice;

import com.doing.nemo.claims.controller.payload.response.authority.claims.CommodityDetailsResponseV1;
import com.doing.nemo.claims.entity.enumerated.AuthorityEnum.AdministrativePracticeTypeEnum;
import com.doing.nemo.claims.entity.enumerated.AuthorityEnum.AuthorityPracticeStatusEnum;
import com.doing.nemo.claims.entity.enumerated.AuthorityEnum.AuthorityWorkingTypeEnum;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WorkingAdministrationResponseV1 implements Serializable {

    @JsonProperty("authority_dossier_id")
    private String authorityDossierId;

    @JsonProperty("authority_dossier_number")
    private String authorityDossierNumber;

    @JsonProperty("authority_working_id")
    private String authorityWorkingId;

    @JsonProperty("working_number")
    private String workingNumber;

    @JsonProperty("plate")
    private String plate;

    @JsonProperty("chassis")
    private String chassis;

    @JsonProperty("end_working_date")
    private Instant endWorkingDate;

    @JsonProperty("type")
    private List<AdministrativePracticeTypeEnum> type;

    @JsonProperty("status")
    private AuthorityPracticeStatusEnum status;

    @JsonProperty("commodity_details")
    private CommodityDetailsResponseV1 commodityDetails;

    @JsonProperty("working_type")
    private AuthorityWorkingTypeEnum workingType;

    public String getAuthorityDossierId() {
        return authorityDossierId;
    }

    public void setAuthorityDossierId(String authorityDossierId) {
        this.authorityDossierId = authorityDossierId;
    }

    public String getAuthorityDossierNumber() {
        return authorityDossierNumber;
    }

    public void setAuthorityDossierNumber(String authorityDossierNumber) {
        this.authorityDossierNumber = authorityDossierNumber;
    }

    public String getAuthorityWorkingId() {
        return authorityWorkingId;
    }

    public void setAuthorityWorkingId(String authorityWorkingId) {
        this.authorityWorkingId = authorityWorkingId;
    }

    public String getWorkingNumber() {
        return workingNumber;
    }

    public void setWorkingNumber(String workingNumber) {
        this.workingNumber = workingNumber;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public Instant getEndWorkingDate() {
        return endWorkingDate;
    }

    public void setEndWorkingDate(Instant endWorkingDate) {
        this.endWorkingDate = endWorkingDate;
    }

    public CommodityDetailsResponseV1 getCommodityDetails() {
        return commodityDetails;
    }

    public void setCommodityDetails(CommodityDetailsResponseV1 commodityDetails) {
        this.commodityDetails = commodityDetails;
    }

    public AuthorityPracticeStatusEnum getStatus() {
        return status;
    }

    public void setStatus(AuthorityPracticeStatusEnum status) {
        this.status = status;
    }

    public List<AdministrativePracticeTypeEnum> getType() {
        if(type == null){
            return null;
        }
        return new ArrayList<>(type);
    }

    public void setType(List<AdministrativePracticeTypeEnum> type) {
        if(type != null)
        {
            this.type = new ArrayList<>(type);
        } else {
            this.type = null;
        }
    }

    public String getChassis() {
        return chassis;
    }

    public void setChassis(String chassis) {
        this.chassis = chassis;
    }

    public AuthorityWorkingTypeEnum getWorkingType() {
        return workingType;
    }

    public void setWorkingType(AuthorityWorkingTypeEnum workingType) {
        this.workingType = workingType;
    }
}
