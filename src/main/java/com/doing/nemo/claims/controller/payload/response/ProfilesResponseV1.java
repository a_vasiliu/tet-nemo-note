package com.doing.nemo.claims.controller.payload.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@JsonIgnoreProperties(ignoreUnknown = true)
public class ProfilesResponseV1 implements Serializable {

    private static final long serialVersionUID = -8704709523011364028L;

    @JsonProperty("profiles")
    private List<ProfileResponseV1> profiles = new ArrayList<>();

    public List<ProfileResponseV1> getProfiles() {
        if(profiles == null){
            return null;
        }
        return new ArrayList<>(profiles);
    }

    public void setProfiles(List<ProfileResponseV1> profiles) {
        if(profiles != null)
        {
            this.profiles = new ArrayList<>(profiles);
        } else {
            this.profiles = null;
        }
    }
}
