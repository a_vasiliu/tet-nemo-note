package com.doing.nemo.claims.controller.payload.response;


import com.doing.nemo.claims.entity.enumerated.AntitheftEnum.AntitheftTypeEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class AntiTheftServiceResponseV1 implements Serializable {

    @JsonProperty("id")
    private UUID id;

    @JsonProperty("code_anti_theft_service")
    private String codeAntiTheftService;

    @JsonProperty("name")
    private String name;

    @JsonProperty("business_name")
    private String businessName;

    @JsonProperty("supplier_code")
    private Integer supplierCode;

    @JsonProperty("email")
    private String email;

    @JsonProperty("phone_number")
    private String phoneNumber;

    @JsonProperty("provider_type")
    private String providerType;

    @JsonProperty("time_out_in_seconds")
    private Integer timeOutInSeconds;

    @JsonProperty("end_point_url")
    private String endPointUrl;

    @JsonProperty("username")
    private String username;

    @JsonProperty("password")
    private String password;

    @JsonProperty("company_code")
    private String companyCode;

    @JsonProperty("active")
    private Boolean active;

    @JsonProperty("is_active")
    private Boolean isActive;

    @JsonProperty("type")
    private AntitheftTypeEnum type;

    @JsonProperty("anti_theft_request_list")
    private List<AntiTheftRequestResponseV1> antiTheftRequestResponseV1list;

    @JsonProperty("registry_list")
    private List<RegistryResponseV1> registryResponseV1List;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getCodeAntiTheftService() {
        return codeAntiTheftService;
    }

    public void setCodeAntiTheftService(String codeAntiTheftService) {
        this.codeAntiTheftService = codeAntiTheftService;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public Integer getSupplierCode() {
        return supplierCode;
    }

    public void setSupplierCode(Integer supplierCode) {
        this.supplierCode = supplierCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getProviderType() {
        return providerType;
    }

    public void setProviderType(String providerType) {
        this.providerType = providerType;
    }

    public Integer getTimeOutInSeconds() {
        return timeOutInSeconds;
    }

    public void setTimeOutInSeconds(Integer timeOutInSeconds) {
        this.timeOutInSeconds = timeOutInSeconds;
    }

    public String getEndPointUrl() {
        return endPointUrl;
    }

    public void setEndPointUrl(String endPointUrl) {
        this.endPointUrl = endPointUrl;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    @JsonIgnore
    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean active) {
        this.isActive = active;
    }

    public AntitheftTypeEnum getType() {
        return type;
    }

    public void setType(AntitheftTypeEnum type) {
        this.type = type;
    }

    @JsonIgnore
    public List<AntiTheftRequestResponseV1> getAntiTheftRequestResponseV1list() {
        if(antiTheftRequestResponseV1list == null){
            return null;
        }
        return new ArrayList<>(antiTheftRequestResponseV1list);
    }

    public void setAntiTheftRequestResponseV1list(List<AntiTheftRequestResponseV1> antiTheftRequestResponseV1list) {
        if(antiTheftRequestResponseV1list != null)
        {
            this.antiTheftRequestResponseV1list = new ArrayList<>(antiTheftRequestResponseV1list);
        } else {
            this.antiTheftRequestResponseV1list = null;
        }
    }

    public List<RegistryResponseV1> getRegistryResponseV1List() {
        if(registryResponseV1List == null){
            return null;
        }
        return new ArrayList<>(registryResponseV1List);
    }

    public void setRegistryResponseV1List(List<RegistryResponseV1> registryResponseV1List) {
        if(registryResponseV1List != null)
        {
            this.registryResponseV1List = new ArrayList<>(registryResponseV1List);
        } else {
            this.registryResponseV1List = null;
        }
    }

    @Override
    public String toString() {
        return "AntiTheftServiceResponseV1{" +
                "id=" + id +
                ", codeAntiTheftService='" + codeAntiTheftService + '\'' +
                ", name='" + name + '\'' +
                ", businessName='" + businessName + '\'' +
                ", supplierCode=" + supplierCode +
                ", email='" + email + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", providerType='" + providerType + '\'' +
                ", timeOutInSeconds=" + timeOutInSeconds +
                ", endPointUrl='" + endPointUrl + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", companyCode='" + companyCode + '\'' +
                ", active=" + active +
                ", isActive=" + isActive +
                ", type=" + type +
                ", antiTheftRequestResponseV1list=" + antiTheftRequestResponseV1list +
                ", registryResponseV1List=" + registryResponseV1List +
                '}';
    }
}
