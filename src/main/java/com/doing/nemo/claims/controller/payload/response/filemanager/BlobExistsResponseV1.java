package com.doing.nemo.claims.controller.payload.response.filemanager;

import com.doing.nemo.claims.entity.enumerated.DTO.Status;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel
public class BlobExistsResponseV1 implements Serializable {

    private static final long serialVersionUID = 5373615915937304469L;

    @ApiModelProperty(example = "897g34s3-0ca7-4226-b10b-c5db10003257")
    private String id;

    @ApiModelProperty(example = "orders")
    private String resourceType;

    @ApiModelProperty(example = "129347")
    private String resourceId;

    @ApiModelProperty(example = "897g34s3-0ca7-4226-b10b-c5db10003257.png")
    private String blobName;

    @ApiModelProperty(example = "adc")
    private String blobType;

    @ApiModelProperty(example = "28579")
    private Integer fileSize;

    @ApiModelProperty(example = "supermario.png")
    private String fileName;

    @ApiModelProperty(example = "image/png")
    private String mimeType;

    @ApiModelProperty(example = "Whooa there's a description field, too!")
    private String description;

    @ApiModelProperty(example = "2018-07-05T16:00:00")
    private String createdAt;

    @ApiModelProperty(example = "PROCESSED")
    private Status status;

    @ApiModelProperty(example = "897g34s3-0ca7-4226-b10b-c5db10003257")
    private String parentId;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public String getResourceId() {
        return resourceId;
    }

    public void setResourceId(String resourceId) {
        this.resourceId = resourceId;
    }

    public String getBlobName() {
        return blobName;
    }

    public void setBlobName(String blobName) {
        this.blobName = blobName;
    }

    public String getBlobType() {
        return blobType;
    }

    public void setBlobType(String blobType) {
        this.blobType = blobType;
    }

    public Integer getFileSize() {
        return fileSize;
    }

    public void setFileSize(Integer fileSize) {
        this.fileSize = fileSize;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    @Override
    public String toString() {
        return "BlobExistsResponseV1{" +
                "id='" + id + '\'' +
                ", resourceType='" + resourceType + '\'' +
                ", resourceId='" + resourceId + '\'' +
                ", blobName='" + blobName + '\'' +
                ", blobType='" + blobType + '\'' +
                ", fileSize=" + fileSize +
                ", fileName='" + fileName + '\'' +
                ", mimeType='" + mimeType + '\'' +
                ", description='" + description + '\'' +
                ", createdAt='" + createdAt + '\'' +
                ", status=" + status +
                ", parentId='" + parentId + '\'' +
                '}';
    }

}
