package com.doing.nemo.claims.controller.payload.request.claims;

import com.doing.nemo.claims.entity.enumerated.NotesEnum.NotesTypeEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;

import java.io.Serializable;

public class NoteRequest implements Serializable {

    @JsonProperty("title")
    private String noteTitle;

    @JsonProperty("note_type")
    private NotesTypeEnum noteType;

    @JsonProperty("note_id")
    private String noteId;

    @JsonProperty("is_important")
    private Boolean isImportant = false;

    @JsonProperty("description")
    private String description;

    @JsonProperty("hidden")
    private Boolean hidden = false;

    public NoteRequest() {
    }

    public NoteRequest(String noteTitle, NotesTypeEnum noteType, String noteId, Boolean isImportant, String description, Boolean hidden) {
        this.noteTitle = noteTitle;
        this.noteType = noteType;
        this.noteId = noteId;
        this.isImportant = isImportant;
        this.description = description;
        this.hidden = hidden;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @JsonIgnore
    public Boolean getImportant() {
        return isImportant;
    }

    public void setImportant(Boolean important) {
        isImportant = important;
    }

    public String getNoteTitle() {
        return noteTitle;
    }

    public void setNoteTitle(String noteTitle) {
        this.noteTitle = noteTitle;
    }

    public NotesTypeEnum getNoteType() {
        return noteType;
    }

    public void setNoteType(NotesTypeEnum noteType) {
        this.noteType = noteType;
    }

    public Boolean getHidden() {
        return hidden;
    }

    @JsonSetter
    public void setHidden(Boolean hidden) {
        if (hidden != null)
            this.hidden = hidden;
    }

    public String getNoteId() {
        return noteId;
    }

    public void setNoteId(String noteId) {
        this.noteId = noteId;
    }

    @Override
    public String toString() {
        return "NoteRequest{" +
                "noteTitle='" + noteTitle + '\'' +
                ", noteType=" + noteType +
                ", noteId='" + noteId + '\'' +
                ", isImportant=" + isImportant +
                ", description='" + description + '\'' +
                ", hidden=" + hidden +
                '}';
    }
}
