package com.doing.nemo.claims.controller;


import com.doing.nemo.claims.controller.payload.response.ClosingResponseV1;
import com.doing.nemo.claims.service.ClosingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@CrossOrigin
@RequestMapping(value = "/api/v1")
@Api("Closing")
public class ClosingController {


    @Autowired
    private ClosingService closingService;

    //REFACTOR
    @ApiOperation(value="Search contract", notes = "It retrieves contract data through one of more parameters")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = ClosingResponseV1.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @GetMapping(value="/closing/{ID_CONTRACT}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<ClosingResponseV1>> searchClosing(
            @PathVariable("ID_CONTRACT") String contractId
    ){
        return new ResponseEntity(closingService.findClosing(contractId), HttpStatus.OK);
    }
}
