package com.doing.nemo.claims.controller.payload.request.claims;

import com.doing.nemo.claims.entity.enumerated.DamagedEnum.WoundedEnum.WoundedTypeEnum;
import com.doing.nemo.claims.entity.enumerated.DamagedEnum.WoundedEnum.WoundedWoundEnum;
import com.doing.nemo.claims.entity.jsonb.Address;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class WoundedRequest implements Serializable {

    @JsonProperty("firstname")
    private String firstname;

    @JsonProperty("lastname")
    private String lastname;

    @JsonProperty("address")
    private Address address;

    @JsonProperty("emergency_room")
    private Boolean emergencyRoom = false;

    @JsonProperty("wound")
    private WoundedWoundEnum wound;

    @JsonProperty("type")
    private WoundedTypeEnum type;

    @JsonProperty("is_privacy_accepted")
    private Boolean isPrivacyAccepted = false;

    @JsonProperty("attachment_list")
    private List<String> attachmentList;

    public Boolean getPrivacyAccepted() {
        return isPrivacyAccepted;
    }

    public void setPrivacyAccepted(Boolean privacyAccepted) {
        isPrivacyAccepted = privacyAccepted;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Boolean getEmergencyRoom() {
        return emergencyRoom;
    }

    public void setEmergencyRoom(Boolean emergencyRoom) {
        this.emergencyRoom = emergencyRoom;
    }

    public WoundedWoundEnum getWound() {
        return wound;
    }

    public void setWound(WoundedWoundEnum wound) {
        this.wound = wound;
    }

    public WoundedTypeEnum getType() {
        return type;
    }

    public void setType(WoundedTypeEnum type) {
        this.type = type;
    }

    public List<String> getAttachmentList() {
        if(attachmentList == null){
            return null;
        }
        return new ArrayList<>(attachmentList);
    }

    public void setAttachmentList(List<String> attachmentList) {
        if(attachmentList != null)
        {
            this.attachmentList = new ArrayList<>(attachmentList);
        } else {
            this.attachmentList = null;
        }
    }

    @Override
    public String toString() {
        return "WoundedRequest{" +
                "firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", address=" + address +
                ", emergencyRoom=" + emergencyRoom +
                ", wound=" + wound +
                ", type=" + type +
                ", attachmentList=" + attachmentList +
                '}';
    }
}