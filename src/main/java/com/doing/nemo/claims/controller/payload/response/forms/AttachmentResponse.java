package com.doing.nemo.claims.controller.payload.response.forms;

import com.doing.nemo.claims.controller.payload.response.AttachmentTypeResponseV1;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class AttachmentResponse implements Serializable {

    @JsonProperty("file_manager_id")
    private String fileManagerId;

    @JsonProperty("file_manager_name")
    private String fileManagerName;

    @JsonProperty("blob_name")
    private String blobName;

    @JsonProperty("blob_size")
    private Integer blobSize;

    @JsonProperty("mime_type")
    private String mimeType;

    @JsonProperty("description")
    private String description;

    @JsonProperty("created_at")
    private String createdAt;

    @JsonProperty("hidden")
    private Boolean hidden;

    @JsonProperty("validate")
    private Boolean validate;

    @JsonProperty("user")
    private String user;

    @JsonProperty("user_name")
    private String userName;

    @JsonProperty("external_id")
    private String externalId;

    @JsonProperty("thumbnail_id")
    private String thumbnailId;

    @JsonProperty("attachment_type")
    private AttachmentTypeResponseV1 attachmentTypeResponse;

    @JsonProperty("is_msa_downloaded")
    private Boolean isMsaDownloaded;

    public AttachmentResponse() {
    }

    @JsonIgnore
    public Boolean getMsaDownloaded() {
        return isMsaDownloaded;
    }

    public void setMsaDownloaded(Boolean msaDownloaded) {
        isMsaDownloaded = msaDownloaded;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getThumbnailId() {
        return thumbnailId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setThumbnailId(String thumbnailId) {
        this.thumbnailId = thumbnailId;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Boolean getValidate() {
        return validate;
    }

    public void setValidate(Boolean validate) {
        this.validate = validate;
    }

    public String getFileManagerId() {
        return fileManagerId;
    }

    public void setFileManagerId(String fileManagerId) {
        this.fileManagerId = fileManagerId;
    }

    public String getFileManagerName() {
        return fileManagerName;
    }

    public void setFileManagerName(String fileManagerName) {
        this.fileManagerName = fileManagerName;
    }

    public String getBlobName() {
        return blobName;
    }

    public void setBlobName(String blobName) {
        this.blobName = blobName;
    }

    public Integer getBlobSize() {
        return blobSize;
    }

    public void setBlobSize(Integer blobSize) {
        this.blobSize = blobSize;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Boolean getHidden() {
        return hidden;
    }

    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }

    public AttachmentTypeResponseV1 getAttachmentTypeResponse() {
        return attachmentTypeResponse;
    }

    public void setAttachmentTypeResponse(AttachmentTypeResponseV1 attachmentTypeResponse) {
        this.attachmentTypeResponse = attachmentTypeResponse;
    }

    @Override
    public String toString() {
        return "AttachmentResponse{" +
                "fileManagerId='" + fileManagerId + '\'' +
                ", fileManagerName='" + fileManagerName + '\'' +
                ", blobName='" + blobName + '\'' +
                ", blobSize=" + blobSize +
                ", mimeType='" + mimeType + '\'' +
                ", description='" + description + '\'' +
                ", createdAt='" + createdAt + '\'' +
                ", hidden=" + hidden +
                ", validate=" + validate +
                ", user='" + user + '\'' +
                ", userName='" + userName + '\'' +
                ", thumbnailId='" + thumbnailId + '\'' +
                ", attachmentTypeResponse=" + attachmentTypeResponse +
                '}';
    }
}
