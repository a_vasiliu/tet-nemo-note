package com.doing.nemo.claims.controller.payload.response.authority.claims;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class ComplaintAuthorityResponseV1  implements Serializable {
    @JsonProperty("data_accident")
    private DataAccidentAuthorityResponseV1 dataAccident;

    public DataAccidentAuthorityResponseV1 getDataAccident() {
        return dataAccident;
    }

    public void setDataAccident(DataAccidentAuthorityResponseV1 dataAccident) {
        this.dataAccident = dataAccident;
    }
}
