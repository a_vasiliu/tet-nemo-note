package com.doing.nemo.claims.controller.payload.response.insurancecompany;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class KaskoResponse implements Serializable {
    @JsonProperty("deductible_id")
    private Integer deductibleId;
    @JsonProperty("deductible_value")
    private String deductibleValue;

    public KaskoResponse() {
    }

    public KaskoResponse(Integer deductibleId, String deductibleValue) {
        this.deductibleId = deductibleId;
        this.deductibleValue = deductibleValue;
    }

    public Integer getDeductibleId() {
        return deductibleId;
    }

    public void setDeductibleId(Integer deductibleId) {
        this.deductibleId = deductibleId;
    }

    public String getDeductibleValue() {
        return deductibleValue;
    }

    public void setDeductibleValue(String deductibleValue) {
        this.deductibleValue = deductibleValue;
    }
}
