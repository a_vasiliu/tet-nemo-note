package com.doing.nemo.claims.controller.payload.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class AssigneeRepairResponseV1 implements Serializable {
    @JsonProperty("id")
    private String id;

    @JsonProperty("username")
    private String username;

    @JsonProperty("personal_details")
    private PersonalDetailsRepairResponseV1 personalDetails;

    public AssigneeRepairResponseV1() {
    }

    public AssigneeRepairResponseV1(String id, String username, PersonalDetailsRepairResponseV1 personalDetails) {
        this.id = id;
        this.username = username;
        this.personalDetails = personalDetails;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public PersonalDetailsRepairResponseV1 getPersonalDetails() {
        return personalDetails;
    }

    public void setPersonalDetails(PersonalDetailsRepairResponseV1 personalDetails) {
        this.personalDetails = personalDetails;
    }

    @Override
    public String toString() {
        return "AssigneeRepair{" +
                "id='" + id + '\'' +
                ", username='" + username + '\'' +
                ", personalDetails=" + personalDetails +
                '}';
    }
}
