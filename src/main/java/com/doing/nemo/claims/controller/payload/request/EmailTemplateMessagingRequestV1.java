package com.doing.nemo.claims.controller.payload.request;

import com.doing.nemo.claims.controller.payload.request.messaging.Identity;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class EmailTemplateMessagingRequestV1 implements Serializable {

    @JsonProperty("tos")
    private List<Identity> tos = new ArrayList<>();

    @JsonProperty("ccs")
    private List<Identity> ccs = new ArrayList<>();

    @JsonProperty("bccs")
    private List<Identity> bccs = new ArrayList<>();

    @JsonProperty("splitting_recipients_email")
    private Boolean splittingRecipientsEmail;

    @JsonProperty("object")
    private String object;

    @JsonProperty("heading")
    private String heading;

    @JsonProperty("body")
    private String body;

    @JsonProperty("foot")
    private String foot;

    @JsonProperty("attach_file")
    private Boolean attachFile;

    @JsonProperty("description")
    private String description;

    @JsonProperty("attachment_list")
    private List<String> attachmentList;

    public EmailTemplateMessagingRequestV1() {
    }

    public EmailTemplateMessagingRequestV1(List<Identity> tos, List<Identity> ccs, List<Identity> bccs, Boolean splittingRecipientsEmail, String object, String heading, String body, String foot, Boolean attachFile, String description, List<String> attachmentList) {
        if(tos != null)
        {
            this.tos = new ArrayList<>(tos);
        }
        if(ccs != null)
        {
            this.ccs = new ArrayList<>(ccs);
        }
        if(bccs != null)
        {
            this.bccs = new ArrayList<>(bccs);
        }
        this.splittingRecipientsEmail = splittingRecipientsEmail;
        this.object = object;
        this.heading = heading;
        this.body = body;
        this.foot = foot;
        this.attachFile = attachFile;
        this.description = description;
        if(attachmentList != null)
        {
            this.attachmentList = new ArrayList<>(attachmentList);
        }
    }

    public List<String> getAttachmentList() {
        if(attachmentList == null){
            return null;
        }
        return new ArrayList<>(attachmentList);
    }

    public void setAttachmentList(List<String> attachmentList) {
        if(attachmentList != null)
        {
            this.attachmentList = new ArrayList<>(attachmentList);
        } else  {
            this.attachmentList = null;
        }
    }

    public List<Identity> getTos() {
        if(tos == null){
            return null;
        }
        return new ArrayList<>(tos) ;
    }

    public void setTos(List<Identity> tos) {
        if(tos != null)
        {
            this.tos = new ArrayList<>(tos);
        } else {
            this.tos =null;
        }
    }

    public List<Identity> getCcs() {
        if(ccs == null){
            return null;
        }
        return new ArrayList<>(ccs);
    }

    public void setCcs(List<Identity> ccs) {
        if(ccs != null)
        {
            this.ccs = new ArrayList<>(ccs);
        } else {
            this.ccs = null;
        }
    }

    public List<Identity> getBccs() {
        if(bccs == null){
            return null;
        }
        return new ArrayList<>(bccs);
    }

    public void setBccs(List<Identity> bccs) {
        if(bccs != null)
        {
            this.bccs = new ArrayList<>(bccs);
        } else {
            this.bccs = null;
        }
    }

    public Boolean getSplittingRecipientsEmail() {
        return splittingRecipientsEmail;
    }

    public void setSplittingRecipientsEmail(Boolean splittingRecipientsEmail) {
        this.splittingRecipientsEmail = splittingRecipientsEmail;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getFoot() {
        return foot;
    }

    public void setFoot(String foot) {
        this.foot = foot;
    }

    public Boolean getAttachFile() {
        return attachFile;
    }

    public void setAttachFile(Boolean attachFile) {
        this.attachFile = attachFile;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "EmailTemplateMessagingRequestV1{" +
                "tos=" + tos +
                ", ccs=" + ccs +
                ", bccs=" + bccs +
                ", splittingRecipientsEmail=" + splittingRecipientsEmail +
                ", object='" + object + '\'' +
                ", heading='" + heading + '\'' +
                ", body='" + body + '\'' +
                ", foot='" + foot + '\'' +
                ", attachFile=" + attachFile +
                ", description='" + description + '\'' +
                '}';
    }
}
