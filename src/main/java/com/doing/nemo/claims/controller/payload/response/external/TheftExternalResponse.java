package com.doing.nemo.claims.controller.payload.response.external;

import com.doing.nemo.claims.entity.jsonb.Address;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class TheftExternalResponse implements Serializable {
    @JsonProperty("is_found")
    private Boolean isFound;

    @JsonProperty("operations_center_notified")
    private Boolean OperationsCenterNotified;

    @JsonProperty("theft_occurred_on_center_ald")
    private Boolean theftOccurredOnCenterAld;

    @JsonProperty("complaint_authority_police")
    private Boolean complaintAuthorityPolice;

    @JsonProperty("complaint_authority_cc")
    private Boolean complaintAuthorityCc;

    @JsonProperty("complaint_authority_vvuu")
    private Boolean complaintAuthorityVvuu;

    @JsonProperty("authority_data")
    private String authorityData;

    @JsonProperty("authority_telephone")
    private String authorityTelephone;

    @JsonProperty("find_date")
    private String findDate;

    @JsonProperty("hour")
    private String hour;

    @JsonProperty("found_abroad")
    private Boolean foundAbroad;

    @JsonProperty("vehicle_co")
    private String vehicleCo;

    @JsonProperty("sequestered")
    private Boolean sequestered;

    @JsonProperty("address")
    private Address address;

    @JsonProperty("find_authority_police")
    private Boolean findAuthorityPolice;

    @JsonProperty("find_authority_cc")
    private Boolean findAuthorityCc;

    @JsonProperty("find_authority_vvuu")
    private Boolean findAuthorityVvuu;

    @JsonProperty("find_authority_data")
    private String findAuthorityData;

    @JsonProperty("find_authority_telephone")
    private String findAuthorityTelephone;

    @JsonProperty("theft_notes")
    private String theftNotes;

    @JsonProperty("find_notes")
    private String findNotes;

    public TheftExternalResponse() {
    }

    public TheftExternalResponse(Boolean isFound, Boolean operationsCenterNotified, Boolean theftOccurredOnCenterAld, Boolean complaintAuthorityPolice, Boolean complaintAuthorityCc, Boolean complaintAuthorityVvuu, String authorityData, String authorityTelephone, String findDate, String hour, Boolean foundAbroad, String vehicleCo, Boolean sequestered, Address address, Boolean findAuthorityPolice, Boolean findAuthorityCc, Boolean findAuthorityVvuu, String findAuthorityData, String findAuthorityTelephone, String theftNotes, String findNotes) {
        this.isFound = isFound;
        OperationsCenterNotified = operationsCenterNotified;
        this.theftOccurredOnCenterAld = theftOccurredOnCenterAld;
        this.complaintAuthorityPolice = complaintAuthorityPolice;
        this.complaintAuthorityCc = complaintAuthorityCc;
        this.complaintAuthorityVvuu = complaintAuthorityVvuu;
        this.authorityData = authorityData;
        this.authorityTelephone = authorityTelephone;
        this.findDate = findDate;
        this.hour = hour;
        this.foundAbroad = foundAbroad;
        this.vehicleCo = vehicleCo;
        this.sequestered = sequestered;
        this.address = address;
        this.findAuthorityPolice = findAuthorityPolice;
        this.findAuthorityCc = findAuthorityCc;
        this.findAuthorityVvuu = findAuthorityVvuu;
        this.findAuthorityData = findAuthorityData;
        this.findAuthorityTelephone = findAuthorityTelephone;
        this.theftNotes = theftNotes;
        this.findNotes = findNotes;
    }

    public Boolean getFound() {
        return isFound;
    }

    public void setFound(Boolean found) {
        isFound = found;
    }

    public Boolean getOperationsCenterNotified() {
        return OperationsCenterNotified;
    }

    public void setOperationsCenterNotified(Boolean operationsCenterNotified) {
        OperationsCenterNotified = operationsCenterNotified;
    }

    public Boolean getTheftOccurredOnCenterAld() {
        return theftOccurredOnCenterAld;
    }

    public void setTheftOccurredOnCenterAld(Boolean theftOccurredOnCenterAld) {
        this.theftOccurredOnCenterAld = theftOccurredOnCenterAld;
    }

    public Boolean getComplaintAuthorityPolice() {
        return complaintAuthorityPolice;
    }

    public void setComplaintAuthorityPolice(Boolean complaintAuthorityPolice) {
        this.complaintAuthorityPolice = complaintAuthorityPolice;
    }

    public Boolean getComplaintAuthorityCc() {
        return complaintAuthorityCc;
    }

    public void setComplaintAuthorityCc(Boolean complaintAuthorityCc) {
        this.complaintAuthorityCc = complaintAuthorityCc;
    }

    public Boolean getComplaintAuthorityVvuu() {
        return complaintAuthorityVvuu;
    }

    public void setComplaintAuthorityVvuu(Boolean complaintAuthorityVvuu) {
        this.complaintAuthorityVvuu = complaintAuthorityVvuu;
    }

    public String getAuthorityData() {
        return authorityData;
    }

    public void setAuthorityData(String authorityData) {
        this.authorityData = authorityData;
    }

    public String getAuthorityTelephone() {
        return authorityTelephone;
    }

    public void setAuthorityTelephone(String authorityTelephone) {
        this.authorityTelephone = authorityTelephone;
    }

    public String getFindDate() {
        return findDate;
    }

    public void setFindDate(String findDate) {
        this.findDate = findDate;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public Boolean getFoundAbroad() {
        return foundAbroad;
    }

    public void setFoundAbroad(Boolean foundAbroad) {
        this.foundAbroad = foundAbroad;
    }

    public String getVehicleCo() {
        return vehicleCo;
    }

    public void setVehicleCo(String vehicleCo) {
        this.vehicleCo = vehicleCo;
    }

    public Boolean getSequestered() {
        return sequestered;
    }

    public void setSequestered(Boolean sequestered) {
        this.sequestered = sequestered;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Boolean getFindAuthorityPolice() {
        return findAuthorityPolice;
    }

    public void setFindAuthorityPolice(Boolean findAuthorityPolice) {
        this.findAuthorityPolice = findAuthorityPolice;
    }

    public Boolean getFindAuthorityCc() {
        return findAuthorityCc;
    }

    public void setFindAuthorityCc(Boolean findAuthorityCc) {
        this.findAuthorityCc = findAuthorityCc;
    }

    public Boolean getFindAuthorityVvuu() {
        return findAuthorityVvuu;
    }

    public void setFindAuthorityVvuu(Boolean findAuthorityVvuu) {
        this.findAuthorityVvuu = findAuthorityVvuu;
    }

    public String getFindAuthorityData() {
        return findAuthorityData;
    }

    public void setFindAuthorityData(String findAuthorityData) {
        this.findAuthorityData = findAuthorityData;
    }

    public String getFindAuthorityTelephone() {
        return findAuthorityTelephone;
    }

    public void setFindAuthorityTelephone(String findAuthorityTelephone) {
        this.findAuthorityTelephone = findAuthorityTelephone;
    }

    public String getTheftNotes() {
        return theftNotes;
    }

    public void setTheftNotes(String theftNotes) {
        this.theftNotes = theftNotes;
    }

    public String getFindNotes() {
        return findNotes;
    }

    public void setFindNotes(String findNotes) {
        this.findNotes = findNotes;
    }

    @Override
    public String toString() {
        return "TheftExternalResponse{" +
                "isFound=" + isFound +
                ", OperationsCenterNotified=" + OperationsCenterNotified +
                ", theftOccurredOnCenterAld=" + theftOccurredOnCenterAld +
                ", complaintAuthorityPolice=" + complaintAuthorityPolice +
                ", complaintAuthorityCc=" + complaintAuthorityCc +
                ", complaintAuthorityVvuu=" + complaintAuthorityVvuu +
                ", authorityData='" + authorityData + '\'' +
                ", authorityTelephone='" + authorityTelephone + '\'' +
                ", findDate=" + findDate +
                ", hour='" + hour + '\'' +
                ", foundAbroad=" + foundAbroad +
                ", vehicleCo='" + vehicleCo + '\'' +
                ", sequestered=" + sequestered +
                ", address=" + address +
                ", findAuthorityPolice=" + findAuthorityPolice +
                ", findAuthorityCc=" + findAuthorityCc +
                ", findAuthorityVvuu=" + findAuthorityVvuu +
                ", findAuthorityData='" + findAuthorityData + '\'' +
                ", findAuthorityTelephone='" + findAuthorityTelephone + '\'' +
                ", theftNotes='" + theftNotes + '\'' +
                ", findNotes='" + findNotes + '\'' +
                '}';
    }
}