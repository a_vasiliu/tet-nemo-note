package com.doing.nemo.claims.controller;

import com.doing.nemo.claims.adapter.MotivationTypeAdapter;
import com.doing.nemo.claims.common.validation.MessageCode;
import com.doing.nemo.claims.common.validation.RequestValidator;
import com.doing.nemo.claims.controller.payload.request.ListRequestV1;
import com.doing.nemo.claims.controller.payload.request.MotivationTypeRequestV1;
import com.doing.nemo.claims.controller.payload.response.IdResponseV1;
import com.doing.nemo.claims.controller.payload.response.MotivationTypeResponseV1;
import com.doing.nemo.claims.controller.payload.response.PaginationResponseV1;
import com.doing.nemo.claims.entity.settings.MotivationTypeEntity;
import com.doing.nemo.claims.service.MotivationTypeService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/v1")
@Api("Motivation TypeEnum")
public class MotivationTypeController {
    @Autowired
    private MotivationTypeService motivationTypeService;

    @Autowired
    private MotivationTypeAdapter motivationTypeAdapter;

    @Autowired
    private RequestValidator requestValidator;

    @PostMapping("/motivation/type")
    @Transactional
    @ApiOperation(value = "Insert Motivation TypeEnum", notes = "Insert motivation type using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = IdResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<IdResponseV1> postMotivationType(
            @ApiParam(value = "Body of the motivation type to be created", required = true)
            @RequestBody MotivationTypeRequestV1 motivationTypeRequestV1) {
        requestValidator.validateRequest(motivationTypeRequestV1, MessageCode.E00X_1000);
        MotivationTypeEntity motivationTypeEntity = motivationTypeService.insertMotivationType(MotivationTypeAdapter.adptFromMotivationTypeRequestToMotivationTypeEntity(motivationTypeRequestV1));
        IdResponseV1 motivationTypeIdResponseV1 = new IdResponseV1(motivationTypeEntity.getId());
        return new ResponseEntity<>(motivationTypeIdResponseV1, HttpStatus.CREATED);
    }

    @PostMapping("/motivation/types")
    @Transactional
    @ApiOperation(value = "Insert List of Motivation Types", notes = "Insert list of motivation types using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = MotivationTypeResponseV1.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<MotivationTypeResponseV1>> postMotivationTypes(
            @ApiParam(value = "Body of the list of Motivation Typed to be created", required = true)
            @RequestBody ListRequestV1<MotivationTypeRequestV1> motivationTypeListRequestV1) {
        List<MotivationTypeResponseV1> motivationTypeResponseV1List = new ArrayList<>();
        for (MotivationTypeRequestV1 motivationTypeRequestV1 : motivationTypeListRequestV1.getData()) {
            requestValidator.validateRequest(motivationTypeRequestV1, MessageCode.E00X_1000);
            MotivationTypeEntity motivationTypeEntity = MotivationTypeAdapter.adptFromMotivationTypeRequestToMotivationTypeEntity(motivationTypeRequestV1);
            motivationTypeEntity = motivationTypeService.insertMotivationType(motivationTypeEntity);
            MotivationTypeResponseV1 motivationTypeResponseV1 = MotivationTypeAdapter.adptFromMotivationTypeEntityToMotivationTypeResponse(motivationTypeEntity);
            motivationTypeResponseV1List.add(motivationTypeResponseV1);
        }
        return new ResponseEntity<>(motivationTypeResponseV1List, HttpStatus.OK);
    }

    @GetMapping("/motivation/type/{UUID}")
    @Transactional
    @ApiOperation(value = "Recover Motivation TypeEnum", notes = "Returns a motivation type using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = MotivationTypeResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<MotivationTypeResponseV1> getMotivationType(
            @ApiParam(value = "UUID of the Motivation TypeEnum to be found", required = true)
            @PathVariable UUID UUID) {
        MotivationTypeEntity motivationTypeEntity = motivationTypeService.selectMotivationType(UUID);
        MotivationTypeResponseV1 motivationTypeResponseV1 = motivationTypeAdapter.adptFromMotivationTypeEntityToMotivationTypeResponse(motivationTypeEntity);
        return new ResponseEntity<>(motivationTypeResponseV1, HttpStatus.OK);
    }

    @GetMapping("/motivation/types")
    @Transactional
    @ApiOperation(value = "Recover All Motivation Types", notes = "Returns all Motivation Types")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = MotivationTypeResponseV1.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<MotivationTypeResponseV1>> getAllMotivationType() {
        List<MotivationTypeEntity> motivationTypeEntities = motivationTypeService.selectAllMotivationType();
        List<MotivationTypeResponseV1> motivationTypeResponseV1List = motivationTypeAdapter.adptFromMotivationTypeEntityToMotivationTypeResponseList(motivationTypeEntities);
        return new ResponseEntity<>(motivationTypeResponseV1List, HttpStatus.OK);
    }

    @PutMapping("/motivation/type/{UUID}")
    @Transactional
    @ApiOperation(value = "Upload Insurance Policy", notes = "Upload Insurance Policy using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = MotivationTypeResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<MotivationTypeResponseV1> putMotivationType(
            @ApiParam(value = "Updated body of the Motivation TypeEnum", required = true)
            @RequestBody MotivationTypeRequestV1 motivationTypeRequestV1,
            @ApiParam(value = "UUID that identifies the Motivation TypeEnum to be modified", required = true)
            @PathVariable UUID UUID) {
        requestValidator.validateRequest(motivationTypeRequestV1, MessageCode.E00X_1000);

        MotivationTypeEntity motivationTypeEntity = motivationTypeAdapter.adptFromMotivationTypeRequestToMotivationTypeEntityWithID(motivationTypeRequestV1, UUID);
        motivationTypeEntity = motivationTypeService.updateMotivationType(motivationTypeEntity);
        MotivationTypeResponseV1 motivationTypeResponseV1 = MotivationTypeAdapter.adptFromMotivationTypeEntityToMotivationTypeResponse(motivationTypeEntity);
        return new ResponseEntity<>(motivationTypeResponseV1, HttpStatus.OK);
    }

    @DeleteMapping("/motivation/type/{UUID}")
    @Transactional
    @ApiOperation(value = "Delete Insurance Policy", notes = "Delete Insurance Policy using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = MotivationTypeResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<MotivationTypeResponseV1> deleteMotivationType(
            @ApiParam(value = "UUID that identifies the Motivation TypeEnum to be deleted", required = true)
            @PathVariable UUID UUID) {
        MotivationTypeEntity motivationTypeEntity = motivationTypeService.deleteMotivationType(UUID);
        return new ResponseEntity<>( HttpStatus.OK);
    }

    @PatchMapping("/motivation/type/{UUID}/active")
    @Transactional
    @ApiOperation(value = "Patch status active Motivation TypeEnum", notes = "Patch status active Motivation TypeEnum using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = MotivationTypeResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<MotivationTypeResponseV1> patchStatusMotivationType(
            @PathVariable UUID UUID
    ) {
        MotivationTypeEntity motivationTypeEntity = motivationTypeService.updateStatusMotivationType(UUID);
        MotivationTypeResponseV1 responseV1 = motivationTypeAdapter.adptFromMotivationTypeEntityToMotivationTypeResponse(motivationTypeEntity);
        return new ResponseEntity<>(responseV1, HttpStatus.OK);
    }

    @ApiOperation(value="Search MotivationType", notes = "It retrieves antitheftservice data through one of more parameters")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = PaginationResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @GetMapping(value="/motivationstype/pagination", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<PaginationResponseV1<MotivationTypeResponseV1>> searchMotivationType(
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @RequestParam(value = "page_size", defaultValue = "20") Integer pageSize,
            @RequestParam(value = "order_by", required = false) String orderBy,
            @RequestParam(value = "asc", required = false, defaultValue = "true") Boolean asc,
            @RequestParam(value = "motivation_type_id", required = false) Long motivationTypeId,
            @RequestParam(value = "description", required = false) String description,
            @RequestParam(value = "order", required = false) Integer orderId,
            @RequestParam(value = "is_active", required = false) Boolean isActive
            ){

        List<MotivationTypeEntity> listMotivationType = motivationTypeService.findMotivationTypesFiltered(page,pageSize, orderBy, asc, motivationTypeId, description, orderId, isActive);
        Long itemCount = motivationTypeService.countMotivationTypeFiltered(motivationTypeId, description, orderId, isActive);

        PaginationResponseV1<MotivationTypeResponseV1> paginationResponseV1 = motivationTypeAdapter.adptPagination(listMotivationType, page, pageSize, itemCount);

        return new ResponseEntity<>(paginationResponseV1, HttpStatus.OK);
    }
}
