package com.doing.nemo.claims.controller.payload.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class ClaimsResponseIdV1 implements Serializable {

    @JsonProperty("id")
    private String id;

    @JsonProperty("practice_id")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Long practiceId;

    public ClaimsResponseIdV1() {
    }

    public ClaimsResponseIdV1(String id, Long practiceId) {
        this.id = id;
        this.practiceId = practiceId;
    }

    public ClaimsResponseIdV1(String id) {
        this.id = id;
    }

    public Long getPracticeId() {
        return practiceId;
    }

    public void setPracticeId(Long practiceId) {
        this.practiceId = practiceId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "ClaimsResponseIdV1{" +
                "id='" + id + '\'' +
                ", practiceId='" + practiceId + '\'' +
                '}';
    }
}
