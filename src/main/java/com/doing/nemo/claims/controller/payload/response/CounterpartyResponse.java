package com.doing.nemo.claims.controller.payload.response;

import com.doing.nemo.claims.controller.payload.response.authority.repair.AuthorityRepairResponseV1;
import com.doing.nemo.claims.controller.payload.response.counterparty.CanalizationResponse;
import com.doing.nemo.claims.controller.payload.response.counterparty.HistoricalCounterpartyResponse;
import com.doing.nemo.claims.controller.payload.response.counterparty.InsuredResponse;
import com.doing.nemo.claims.controller.payload.response.counterparty.LastContactResponse;
import com.doing.nemo.claims.controller.payload.response.damaged.DriverResponse;
import com.doing.nemo.claims.controller.payload.response.damaged.ImpactPointResponse;
import com.doing.nemo.claims.controller.payload.response.damaged.VehicleResponse;
import com.doing.nemo.claims.controller.payload.response.forms.AttachmentResponse;
import com.doing.nemo.claims.entity.enumerated.DamagedEnum.VehicleEnum.VehicleTypeEnum;
import com.doing.nemo.claims.entity.enumerated.RepairEnum.ManagementTypeEnum;
import com.doing.nemo.claims.entity.enumerated.RepairEnum.RepairProcedureEnum;
import com.doing.nemo.claims.entity.enumerated.RepairEnum.RepairStatusEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

    public class CounterpartyResponse {

        @JsonProperty("counterparty_id")
        private String counterpartyId;

        @JsonProperty("practice_id_counterparty")
        private Long practiceIdCounterparty;

        @JsonProperty("type")
        private VehicleTypeEnum type;

        @JsonProperty("repair_status")
        private RepairStatusEnum repairStatus;

        @JsonProperty("procedure_repair")
        private RepairProcedureEnum repairProcedure;

        @JsonProperty("motivation")
        private String motivation;

        @JsonProperty("user_create")
        private String userCreate;

        @JsonProperty("created_at")
        private String createdAt;

        @JsonProperty("repair_created_at")
        private String repairCreatedAt;

        @JsonProperty("user_update")
        private String userUpdate;

        @JsonProperty("update_at")
        private String updateAt;

        @JsonProperty("assigned_to")
        private AssigneeRepairResponseV1 assignedTo;

        @JsonProperty("assigned_at")
        private String assignedAt;

        @JsonProperty("insured")
        private InsuredResponse insured;

        @JsonProperty("insurance_company")
        private InsuranceCompanyCounterpartyResponse insuranceCompany;

        @JsonProperty("driver")
        private DriverResponse driver;

        @JsonProperty("vehicle")
        private VehicleResponse vehicle;

        @JsonProperty("is_cai_signed")
        private Boolean isCaiSigned;

        @JsonProperty("impact_point")
        private ImpactPointResponse impactPoint;

        @JsonProperty("responsible")
        private Boolean responsible;

        @JsonProperty("is_complete_documentation")
        private Boolean isCompleteDocumentation;

        @JsonProperty("eligibility")
        private Boolean eligibility;

        @JsonProperty("replacement_car")
        private Boolean replacementCar;

        @JsonProperty("replacement_plate")
        private String replacementPlate;

        @JsonProperty("attachments")
        private List<AttachmentResponse> attachments;

        @JsonProperty("description")
        private String description;

        @JsonProperty("historicals")
        private List<HistoricalCounterpartyResponse> historicals;

        @JsonProperty("manager")
        private ManagerResponseV1 manager;

        @JsonProperty("canalization")
        private CanalizationResponse canalization;

        @JsonProperty("last_contact")
        private List<LastContactResponse> lastContact;

        @JsonProperty("policy_number")
        private String policyNumber;

        @JsonProperty("policy_beginning_validity")
        private String policyBeginningValidity;

        @JsonProperty("policy_end_validity")
        private String policyEndValidity;

        @JsonProperty("management_type")
        private ManagementTypeEnum managementType;

        @JsonProperty("asked_for_damages")
        private Boolean askedForDamages;

        @JsonProperty("legal_or_consultant")
        private Boolean legalOrConsultant;

        @JsonProperty("date_request_damages")
        private String dateRequestDamages;

        @JsonProperty("expiration_date")
        private String expirationDate;

        @JsonProperty("legal")
        private String legal;

        @JsonProperty("email_legal")
        private String emailLegal;

        @JsonProperty("is_ald")
        private Boolean isAld;

        @JsonProperty("authorities")
        private List<AuthorityRepairResponseV1> authorities;

        @JsonProperty("is_read_msa")
        private Boolean isRead;

        public Boolean getIsCompleteDocumentation() {
            return isCompleteDocumentation;
        }

        public void setIsCompleteDocumentation(Boolean completeDocumentation) {
            isCompleteDocumentation = completeDocumentation;
        }

        public String getReplacementPlate() {
            return replacementPlate;
        }

        public void setReplacementPlate(String replacementPlate) {
            this.replacementPlate = replacementPlate;
        }

        public String getMotivation() {
            return motivation;
        }

        public void setMotivation(String motivation) {
            this.motivation = motivation;
        }

        public String getRepairCreatedAt() {
            return repairCreatedAt;
        }

        public void setRepairCreatedAt(String repairCreatedAt) {
            this.repairCreatedAt = repairCreatedAt;
        }

        public Boolean getReplacementCar() {
            return replacementCar;
        }

        public void setReplacementCar(Boolean replacementCar) {
            this.replacementCar = replacementCar;
        }

        @JsonIgnore
        public Boolean getRead() {
            return isRead;
        }

        public void setRead(Boolean read) {
            isRead = read;
        }

        public CounterpartyResponse() {
        }

        public List<AuthorityRepairResponseV1> getAuthorities() {
            if(authorities == null){
                return null;
            }
            return new ArrayList<>(authorities);
        }

        public void setAuthorities(List<AuthorityRepairResponseV1> authorities) {
            if(authorities != null)
            {
                this.authorities = new ArrayList<>(authorities);
            } else {
                this.authorities = null;
            }
        }

        public Long getPracticeIdCounterparty() {
            return practiceIdCounterparty;
        }

        public void setPracticeIdCounterparty(Long practiceIdCounterparty) {
            this.practiceIdCounterparty = practiceIdCounterparty;
        }

        @JsonIgnore
        public Boolean getAld() {
            return isAld;
        }

        public void setAld(Boolean ald) {
            isAld = ald;
        }

        public String getPolicyNumber() {
            return policyNumber;
        }

        public void setPolicyNumber(String policyNumber) {
            this.policyNumber = policyNumber;
        }

        public String getPolicyBeginningValidity() {
            return policyBeginningValidity;
        }

        public void setPolicyBeginningValidity(String policyBeginningValidity) {
            this.policyBeginningValidity = policyBeginningValidity;
        }

        public String getPolicyEndValidity() {
            return policyEndValidity;
        }

        public void setPolicyEndValidity(String policyEndValidity) {
            this.policyEndValidity = policyEndValidity;
        }

        public ManagementTypeEnum getManagementType() {
            return managementType;
        }

        public void setManagementType(ManagementTypeEnum managementType) {
            this.managementType = managementType;
        }

        public Boolean getAskedForDamages() {
            return askedForDamages;
        }

        public void setAskedForDamages(Boolean askedForDamages) {
            this.askedForDamages = askedForDamages;
        }

        public Boolean getLegalOrConsultant() {
            return legalOrConsultant;
        }

        public void setLegalOrConsultant(Boolean legalOrConsultant) {
            this.legalOrConsultant = legalOrConsultant;
        }

        public String getDateRequestDamages() {
            return dateRequestDamages;
        }

        public void setDateRequestDamages(String dateRequestDamages) {
            this.dateRequestDamages = dateRequestDamages;
        }

        public String getExpirationDate() {
            return expirationDate;
        }

        public void setExpirationDate(String expirationDate) {
            this.expirationDate = expirationDate;
        }

        public String getLegal() {
            return legal;
        }

        public void setLegal(String legal) {
            this.legal = legal;
        }

        public String getEmailLegal() {
            return emailLegal;
        }

        public void setEmailLegal(String emailLegal) {
            this.emailLegal = emailLegal;
        }

        public ManagerResponseV1 getManager() {
            return manager;
        }

        public void setManager(ManagerResponseV1 manager) {
            this.manager = manager;
        }

        public CanalizationResponse getCanalization() {
            return canalization;
        }

        public void setCanalization(CanalizationResponse canalization) {
            this.canalization = canalization;
        }

        public List<LastContactResponse> getLastContact() {
            if(lastContact == null){
                return null;
            }
            return new ArrayList<>(lastContact);
        }

        public void setLastContact(List<LastContactResponse> lastContact) {
            if(lastContact != null)
            {
                this.lastContact = new ArrayList<>(lastContact);
            } else {
                this.lastContact = null;
            }
        }

        public String getCounterpartyId() {
            return counterpartyId;
        }

        public void setCounterpartyId(String counterpartyId) {
            this.counterpartyId = counterpartyId;
        }

        public VehicleTypeEnum getType() {
            return type;
        }

        public void setType(VehicleTypeEnum type) {
            this.type = type;
        }

        public RepairStatusEnum getRepairStatus() {
            return repairStatus;
        }

        public void setRepairStatus(RepairStatusEnum repairStatus) {
            this.repairStatus = repairStatus;
        }

        public InsuredResponse getInsured() {
            return insured;
        }

        public void setInsured(InsuredResponse insured) {
            this.insured = insured;
        }

        public InsuranceCompanyCounterpartyResponse getInsuranceCompany() {
            return insuranceCompany;
        }

        public void setInsuranceCompany(InsuranceCompanyCounterpartyResponse insuranceCompany) {
            this.insuranceCompany = insuranceCompany;
        }

        public DriverResponse getDriver() {
            return driver;
        }

        public void setDriver(DriverResponse driver) {
            this.driver = driver;
        }

        public VehicleResponse getVehicle() {
            return vehicle;
        }

        public void setVehicle(VehicleResponse vehicle) {
            this.vehicle = vehicle;
        }

        @JsonIgnore
        public Boolean getCaiSigned() {
            return isCaiSigned;
        }

        public void setCaiSigned(Boolean caiSigned) {
            isCaiSigned = caiSigned;
        }

        public ImpactPointResponse getImpactPoint() {
            return impactPoint;
        }

        public void setImpactPoint(ImpactPointResponse impactPoint) {
            this.impactPoint = impactPoint;
        }

        public Boolean getResponsible() {
            return responsible;
        }

        public void setResponsible(Boolean responsible) {
            this.responsible = responsible;
        }

        public List<AttachmentResponse> getAttachments() {
            if(attachments == null){
                return null;
            }
            return new ArrayList<>(attachments);
        }

        public void setAttachments(List<AttachmentResponse> attachments) {
            if(attachments != null)
            {
                this.attachments =new ArrayList<>(attachments);
            } else {
                this.attachments = null;
            }
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public RepairProcedureEnum getRepairProcedure() {
            return repairProcedure;
        }

        public void setRepairProcedure(RepairProcedureEnum repairProcedure) {
            this.repairProcedure = repairProcedure;
        }

        public String getUserCreate() {
            return userCreate;
        }

        public void setUserCreate(String userCreate) {
            this.userCreate = userCreate;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUserUpdate() {
            return userUpdate;
        }

        public void setUserUpdate(String userUpdate) {
            this.userUpdate = userUpdate;
        }

        public String getUpdateAt() {
            return updateAt;
        }

        public void setUpdateAt(String updateAt) {
            this.updateAt = updateAt;
        }

        public AssigneeRepairResponseV1 getAssignedTo() {
            return assignedTo;
        }

        public void setAssignedTo(AssigneeRepairResponseV1 assignedTo) {
            this.assignedTo = assignedTo;
        }

        public String getAssignedAt() {
            return assignedAt;
        }

        public void setAssignedAt(String assignedAt) {
            this.assignedAt = assignedAt;
        }

        public List<HistoricalCounterpartyResponse> getHistoricals() {
            if(historicals == null){
                return null;
            }
            return new ArrayList<>(historicals);
        }

        public void setHistoricals(List<HistoricalCounterpartyResponse> historicals) {
            if(historicals != null)
            {
                this.historicals = new ArrayList<>(historicals);
            } else {
                this.historicals = null;
            }
        }

        public Boolean getEligibility() {
            return eligibility;
        }

        public void setEligibility(Boolean eligibility) {
            this.eligibility = eligibility;
        }

        @Override
        public String toString() {
            return "CounterpartyResponse{" +
                    "counterpartyId='" + counterpartyId + '\'' +
                    ", type=" + type +
                    ", repairStatus=" + repairStatus +
                    ", repairProcedure=" + repairProcedure +
                    ", userCreate='" + userCreate + '\'' +
                    ", createdAt=" + createdAt +
                    ", userUpdate='" + userUpdate + '\'' +
                    ", updateAt=" + updateAt +
                    ", assignedTo='" + assignedTo + '\'' +
                    ", assignedAt=" + assignedAt +
                    ", insured=" + insured +
                    ", insuranceCompany=" + insuranceCompany +
                    ", driver=" + driver +
                    ", vehicle=" + vehicle +
                    ", isCaiSigned=" + isCaiSigned +
                    ", impactPoint=" + impactPoint +
                    ", responsible=" + responsible +
                    ", eligibility=" + eligibility +
                    ", attachments=" + attachments +
                    ", description='" + description + '\'' +
                    ", historicals=" + historicals +
                    ", manager=" + manager +
                    ", canalization=" + canalization +
                    ", lastContact=" + lastContact +
                    '}';
        }
    }

