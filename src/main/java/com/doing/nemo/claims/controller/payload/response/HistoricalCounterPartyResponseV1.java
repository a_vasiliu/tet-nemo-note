package com.doing.nemo.claims.controller.payload.response;

import com.doing.nemo.claims.controller.payload.response.counterparty.HistoricalCounterpartyResponse;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class HistoricalCounterPartyResponseV1 implements Serializable {


    @JsonProperty("historical")
    private List<HistoricalCounterpartyResponse> historicalResponseList;


    public HistoricalCounterPartyResponseV1(List<HistoricalCounterpartyResponse> historicalResponseList) {
        if(historicalResponseList != null)
        {
            this.historicalResponseList = new ArrayList<>(historicalResponseList);
        }
    }

    public List<HistoricalCounterpartyResponse> getHistoricalResponseList() {
        if(historicalResponseList == null){
            return null;
        }
        return new ArrayList<>(historicalResponseList);
    }

    public void setHistoricalResponseList(List<HistoricalCounterpartyResponse> historicalResponseList) {
        if(historicalResponseList != null)
        {
            this.historicalResponseList = new ArrayList<>(historicalResponseList);
        } else {
            this.historicalResponseList = null;
        }
    }

    @Override
    public String toString() {
        return "HistoricalCounterPartyResponseV1{" +
                "historicalResponseList=" + historicalResponseList +
                '}';
    }
}
