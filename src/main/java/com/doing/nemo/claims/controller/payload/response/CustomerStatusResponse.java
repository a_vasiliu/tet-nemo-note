package com.doing.nemo.claims.controller.payload.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class CustomerStatusResponse implements Serializable {
    @JsonProperty("customer_id")
    private String customerId;

    @JsonProperty("status")
    private String status;

    public CustomerStatusResponse() {
    }

    public CustomerStatusResponse(String customerId, String status) {
        this.customerId = customerId;
        this.status = status;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "CustomerStatusResponse{" +
                "customerId='" + customerId + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
