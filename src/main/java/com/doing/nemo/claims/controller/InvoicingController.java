package com.doing.nemo.claims.controller;


import com.doing.nemo.claims.service.AuthorityService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/v1")
@Api("Invocing")
public class InvoicingController {

    @Autowired
    private AuthorityService authorityService;


    //REFACTOR
    @GetMapping(value = "/invoicing/olsa",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "", notes = "")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created" /*, response = ClaimsResponsePaginationV1.class*/),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public void getClaimsInvocing(
    ) throws IOException {
        authorityService.getClaimsToInvoce();

    }


    //REFACTOR
    @GetMapping(value = "/invoicing/olsa/{CLAIMS_UUID}/{WORKING_UUID}",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "", notes = "")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created" /*, response = ClaimsResponsePaginationV1.class*/),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public void getClaimSingleInvocing(
            @PathVariable("CLAIMS_UUID") String claimsId,
            @PathVariable("WORKING_UUID") String workingId
    ) throws IOException {

        authorityService.getSingleClaimToInvoice(claimsId, workingId );

    }



}
