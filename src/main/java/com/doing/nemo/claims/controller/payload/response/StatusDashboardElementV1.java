package com.doing.nemo.claims.controller.payload.response;

import java.io.Serializable;

public class StatusDashboardElementV1 implements Serializable {

    private Integer ful = 0;
    private Integer fni = 0;
    private Integer fcm = 0;

    public StatusDashboardElementV1() {
    }

    public Integer getFul() {
        return ful;
    }

    public void setFul(Integer ful) {
        this.ful = ful;
    }

    public Integer getFni() {
        return fni;
    }

    public void setFni(Integer fni) {
        this.fni = fni;
    }

    public Integer getFcm() {
        return fcm;
    }

    public void setFcm(Integer fcm) {
        this.fcm = fcm;
    }

    @Override
    public String toString() {
        return "StatusDashboardElementV1{" +
                "ful=" + ful +
                ", fni=" + fni +
                ", fcm=" + fcm +
                '}';
    }
}
