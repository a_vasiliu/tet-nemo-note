package com.doing.nemo.claims.controller.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ClaimsTypeRequestV1 implements Serializable {

    @JsonProperty("data")
    private List<Type> data;

    public static class Type {

        @JsonProperty("id")
        private UUID id;

        @JsonProperty("type")
        private String type;

        @JsonProperty("claims_accident_type")
        private String claimsAccidentType;

        public Type() {
        }

        public Type(String type, String claimsAccidentType) {
            this.type = type;
            this.claimsAccidentType = claimsAccidentType;
        }

        public UUID getId() {
            return id;
        }

        public void setId(UUID id) {
            this.id = id;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getClaimsAccidentType() {
            return claimsAccidentType;
        }

        public void setClaimsAccidentType(String claimsAccidentType) {
            this.claimsAccidentType = claimsAccidentType;
        }

        @Override
        public String toString() {
            return "TypeEnum{" +
                    ", type='" + type + '\'' +
                    ", claimsAccidentType='" + claimsAccidentType + '\'' +
                    '}';
        }
    }

    public List<Type> getData() {
        if(data == null){
            return null;
        }
        return new ArrayList<>(data) ;
    }

    public void setData(List<Type> data) {
        if(data != null)
        {
            this.data = new ArrayList<>(data) ;
        } else {
            this.data = null;
        }
    }

    @Override
    public String toString() {
        return "ClaimsTypeRequestV1{" +
                "data=" + data +
                '}';
    }
}
