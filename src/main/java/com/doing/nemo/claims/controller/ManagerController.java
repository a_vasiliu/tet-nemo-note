package com.doing.nemo.claims.controller;

import com.doing.nemo.claims.adapter.ManagerAdapter;
import com.doing.nemo.claims.common.validation.MessageCode;
import com.doing.nemo.claims.common.validation.RequestValidator;
import com.doing.nemo.claims.controller.payload.request.ManagerRequestV1;
import com.doing.nemo.claims.controller.payload.response.IdResponseV1;
import com.doing.nemo.claims.controller.payload.response.ManagerResponseV1;
import com.doing.nemo.claims.controller.payload.response.PaginationResponseV1;
import com.doing.nemo.claims.entity.enumerated.ClaimsRepairEnum;
import com.doing.nemo.claims.entity.settings.ManagerEntity;
import com.doing.nemo.claims.service.ManagerService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/v1")
public class ManagerController {

    @Autowired
    private ManagerService managerService;

    @Autowired
    private ManagerAdapter managerAdapter;

    @Autowired
    private RequestValidator requestValidator;

    @PostMapping(value = "/manager",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Insert Manager TypeEnum", notes = "Insert manager using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = IdResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @Transactional
    public ResponseEntity<IdResponseV1> postInsertManager(
            @ApiParam(value = "Body of the Manager TypeEnum to be created", required = true)
            @RequestBody ManagerRequestV1 managerRequest,
            @ApiParam(value = "TypeEnum of complaint", required = true)
            @RequestParam(value = "type_complaint") ClaimsRepairEnum typeComplaint) {

        requestValidator.validateRequest(managerRequest, MessageCode.E00X_1000);
        ManagerEntity managerEntity = ManagerAdapter.getManagerEntity(managerRequest);
        managerEntity.setTypeComplaint(typeComplaint);
        managerEntity = managerService.insertManager(managerEntity);
        IdResponseV1 response = new IdResponseV1(managerEntity.getId());
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @PutMapping(value = "/manager/{UUID}",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Upload Manager TypeEnum", notes = "Upload manager using info passed in the body")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ManagerResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @Transactional
    public ResponseEntity<ManagerResponseV1> putManager(
            @ApiParam(value = "UUID that identifies the Manager TypeEnum to be modified", required = true)
            @PathVariable(name = "UUID") UUID uuid,
            @ApiParam(value = "Updated body of the Manager TypeEnum", required = true)
            @RequestBody ManagerRequestV1 managerRequest) {

        requestValidator.validateRequest(managerRequest, MessageCode.E00X_1000);
        ManagerEntity managerEntity = ManagerAdapter.getManagerEntity(managerRequest);
        managerEntity = managerService.updateManager(managerEntity, uuid);
        ManagerResponseV1 response = ManagerAdapter.getManagerAdapter(managerEntity);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping(value = "/manager/{UUID}",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Recover Manager TypeEnum", notes = "Returns a Manager TypeEnum using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ManagerResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ManagerResponseV1> getManager(
            @ApiParam(value = "UUID of the Manager TypeEnum to be found", required = true)
            @PathVariable(name = "UUID") UUID uuid) {

        ManagerEntity managerEntity = managerService.getManager(uuid);
        ManagerResponseV1 response = managerAdapter.getManagerAdapter(managerEntity);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping(value = "/manager",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Recover Manager Types", notes = "Returns all the Manager Types")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ManagerResponseV1.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<ManagerResponseV1>> getAllManager(
            @ApiParam(value = "TypeEnum of complaint", required = true)
            @RequestParam(value = "type_complaint") ClaimsRepairEnum typeComplaint
    ) {

        List<ManagerEntity> managerEntities = managerService.getAllManager(typeComplaint);
        List<ManagerResponseV1> response = managerAdapter.getManagerListAdapter(managerEntities);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @DeleteMapping(value = "/manager/{UUID}",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Delete Manager TypeEnum", notes = "Delete a Manager TypeEnum using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ManagerResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @Transactional
    public ResponseEntity deleteManager(@PathVariable(name = "UUID") UUID uuid) {

        ManagerEntity managerEntity = managerService.deleteManager(uuid);
        if (managerEntity == null)
            return new ResponseEntity<>(managerEntity, HttpStatus.OK);
        ManagerResponseV1 response = managerAdapter.getManagerAdapter(managerEntity);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PatchMapping("/manager/{UUID}/active")
    @Transactional
    @ApiOperation(value = "Patch status active Manager", notes = "Patch status active Manager using the UUID passed in the path")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ManagerResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<ManagerResponseV1> patchStatusManager(
            @PathVariable UUID UUID
    ) {
        ManagerEntity managerEntity = managerService.patchActive(UUID);
        ManagerResponseV1 responseV1 = managerAdapter.getManagerAdapter(managerEntity);
        return new ResponseEntity<>(responseV1, HttpStatus.OK);
    }


    @ApiOperation(value="Search Manager", notes = "It retrieves manager data through one of more parameters")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = PaginationResponseV1.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 406, message = "Not Acceptable"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @GetMapping(value="/manager/pagination", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<PaginationResponseV1<ManagerResponseV1>> searchManager(
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @RequestParam(value = "page_size", defaultValue = "20") Integer pageSize,
            //@RequestParam(value = "type_complaint", required = false) ClaimsRepairEnum claimsRepairEnum,
            @RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "prov", required = false) String prov,
            @RequestParam(value = "phone", required = false) String phone,
            @RequestParam(value = "fax", required = false) String fax,
            @RequestParam(value = "email", required = false) String email,
            @RequestParam(value = "external_export_id", required = false) String externalExportId,
            @RequestParam(value = "is_active", required = false) Boolean isActive,
            @RequestParam(value = "order_by", required = false) String orderBy,
            @RequestParam(value = "asc", required = false, defaultValue = "true") Boolean asc,
            @RequestParam(value = "address", required = false) String address,
            @RequestParam(value = "zip_code", required = false) String zipCode,
            @RequestParam(value = "locality", required = false) String locality,
            @RequestParam(value = "country", required = false) String country,
            @RequestParam(value = "code", required = false) Long code

            ){

        ClaimsRepairEnum claimsRepairEnum = ClaimsRepairEnum.REPAIR;
        List<ManagerEntity> listManager = managerService.findManagersFiltered(page,pageSize, claimsRepairEnum, name, prov, phone, fax, email, externalExportId, isActive, orderBy, asc, address, zipCode, locality, country, code);
        Long itemCount = managerService.countManagerFiltered(claimsRepairEnum, name, prov, phone, fax, email, externalExportId, isActive, address, zipCode, locality, country, code);

        PaginationResponseV1<ManagerResponseV1> paginationResponseV1 = managerAdapter.adptPagination(listManager, page, pageSize, itemCount);

        return new ResponseEntity<>(paginationResponseV1, HttpStatus.OK);
    }
}
