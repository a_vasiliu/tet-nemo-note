
package com.doing.nemo.claims.websin.websinws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java per MigrationDamaged complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="MigrationDamaged">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DamageNature" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RepManageType" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="PrincipalOpponent" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ComplaintID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="PrincipalPart" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="AssuredSurname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AssuredName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AssuredFiscalCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AssuredVat" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AssuredAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AssuredCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AssuredProvince" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AssuredZipCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AssuredCountry" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AssuredPhone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AssuredEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VehiclePlate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VehicleVIN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VehicleRegCountry" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VehicleRegDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="TrailerPlate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TrailerVIN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TrailerRegCountry" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InsuranceCompanyName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InsuranceCompanyID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="PolicyNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GreenCardNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PolicyStart" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="PolicyEnd" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="InsuranceAgencyName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InsuranceAgencyAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InsuranceAgencyCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InsuranceAgencyProvince" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InsuranceAgencyZipCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InsuranceAgencyPhone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InsuranceAgencyEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Kasko" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="DriverSurname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DriverName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DriverBirthDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="DriverFiscalCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DriverAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DriverCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DriverProvince" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DriverZipCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DriverCountry" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DriverPhone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DriverEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DriverLicenseNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DriverLicenseCategory" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DriverLicenseEnd" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="DriverLesions" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="DamagePosA1" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="DamagePosA2" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="DamagePosA3" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="DamagePosB1" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="DamagePosB2" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="DamagePosB3" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="DamagePosB4" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="DamagePosC1" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="DamagePosC2" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="DamagePosC3" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="DamagePosD" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="DamagePosE" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="DamagePosF" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="DamageDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DamageComment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Signature" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Condition01" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Condition02" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Condition03" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Condition04" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Condition05" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Condition06" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Condition07" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Condition08" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Condition09" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Condition10" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Condition11" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Condition12" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Condition13" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Condition14" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Condition15" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Condition16" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Condition17" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="TotCondition" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="VehicleDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CaiPart" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="DamagedType" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MigrationDamaged", propOrder = {
    "damageNature",
    "repManageType",
    "principalOpponent",
    "id",
    "complaintID",
    "principalPart",
    "assuredSurname",
    "assuredName",
    "assuredFiscalCode",
    "assuredVat",
    "assuredAddress",
    "assuredCity",
    "assuredProvince",
    "assuredZipCode",
    "assuredCountry",
    "assuredPhone",
    "assuredEmail",
    "vehiclePlate",
    "vehicleVIN",
    "vehicleRegCountry",
    "vehicleRegDate",
    "trailerPlate",
    "trailerVIN",
    "trailerRegCountry",
    "insuranceCompanyName",
    "insuranceCompanyID",
    "policyNumber",
    "greenCardNumber",
    "policyStart",
    "policyEnd",
    "insuranceAgencyName",
    "insuranceAgencyAddress",
    "insuranceAgencyCity",
    "insuranceAgencyProvince",
    "insuranceAgencyZipCode",
    "insuranceAgencyPhone",
    "insuranceAgencyEmail",
    "kasko",
    "driverSurname",
    "driverName",
    "driverBirthDate",
    "driverFiscalCode",
    "driverAddress",
    "driverCity",
    "driverProvince",
    "driverZipCode",
    "driverCountry",
    "driverPhone",
    "driverEmail",
    "driverLicenseNumber",
    "driverLicenseCategory",
    "driverLicenseEnd",
    "driverLesions",
    "damagePosA1",
    "damagePosA2",
    "damagePosA3",
    "damagePosB1",
    "damagePosB2",
    "damagePosB3",
    "damagePosB4",
    "damagePosC1",
    "damagePosC2",
    "damagePosC3",
    "damagePosD",
    "damagePosE",
    "damagePosF",
    "damageDescription",
    "damageComment",
    "signature",
    "condition01",
    "condition02",
    "condition03",
    "condition04",
    "condition05",
    "condition06",
    "condition07",
    "condition08",
    "condition09",
    "condition10",
    "condition11",
    "condition12",
    "condition13",
    "condition14",
    "condition15",
    "condition16",
    "condition17",
    "totCondition",
    "vehicleDescription",
    "caiPart",
    "damagedType"
})
public class MigrationDamaged
    implements Serializable
{

    @XmlElement(name = "DamageNature")
    protected String damageNature;
    @XmlElement(name = "RepManageType", required = true, type = Integer.class, nillable = true)
    protected Integer repManageType;
    @XmlElement(name = "PrincipalOpponent")
    protected int principalOpponent;
    @XmlElement(name = "ID")
    protected long id;
    @XmlElement(name = "ComplaintID")
    protected int complaintID;
    @XmlElement(name = "PrincipalPart")
    protected int principalPart;
    @XmlElement(name = "AssuredSurname")
    protected String assuredSurname;
    @XmlElement(name = "AssuredName")
    protected String assuredName;
    @XmlElement(name = "AssuredFiscalCode")
    protected String assuredFiscalCode;
    @XmlElement(name = "AssuredVat")
    protected String assuredVat;
    @XmlElement(name = "AssuredAddress")
    protected String assuredAddress;
    @XmlElement(name = "AssuredCity")
    protected String assuredCity;
    @XmlElement(name = "AssuredProvince")
    protected String assuredProvince;
    @XmlElement(name = "AssuredZipCode")
    protected String assuredZipCode;
    @XmlElement(name = "AssuredCountry")
    protected String assuredCountry;
    @XmlElement(name = "AssuredPhone")
    protected String assuredPhone;
    @XmlElement(name = "AssuredEmail")
    protected String assuredEmail;
    @XmlElement(name = "VehiclePlate")
    protected String vehiclePlate;
    @XmlElement(name = "VehicleVIN")
    protected String vehicleVIN;
    @XmlElement(name = "VehicleRegCountry")
    protected String vehicleRegCountry;
    @XmlElement(name = "VehicleRegDate", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar vehicleRegDate;
    @XmlElement(name = "TrailerPlate")
    protected String trailerPlate;
    @XmlElement(name = "TrailerVIN")
    protected String trailerVIN;
    @XmlElement(name = "TrailerRegCountry")
    protected String trailerRegCountry;
    @XmlElement(name = "InsuranceCompanyName")
    protected String insuranceCompanyName;
    @XmlElement(name = "InsuranceCompanyID", required = true, type = Integer.class, nillable = true)
    protected Integer insuranceCompanyID;
    @XmlElement(name = "PolicyNumber")
    protected String policyNumber;
    @XmlElement(name = "GreenCardNumber")
    protected String greenCardNumber;
    @XmlElement(name = "PolicyStart", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar policyStart;
    @XmlElement(name = "PolicyEnd", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar policyEnd;
    @XmlElement(name = "InsuranceAgencyName")
    protected String insuranceAgencyName;
    @XmlElement(name = "InsuranceAgencyAddress")
    protected String insuranceAgencyAddress;
    @XmlElement(name = "InsuranceAgencyCity")
    protected String insuranceAgencyCity;
    @XmlElement(name = "InsuranceAgencyProvince")
    protected String insuranceAgencyProvince;
    @XmlElement(name = "InsuranceAgencyZipCode")
    protected String insuranceAgencyZipCode;
    @XmlElement(name = "InsuranceAgencyPhone")
    protected String insuranceAgencyPhone;
    @XmlElement(name = "InsuranceAgencyEmail")
    protected String insuranceAgencyEmail;
    @XmlElement(name = "Kasko", required = true, type = Integer.class, nillable = true)
    protected Integer kasko;
    @XmlElement(name = "DriverSurname")
    protected String driverSurname;
    @XmlElement(name = "DriverName")
    protected String driverName;
    @XmlElement(name = "DriverBirthDate", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar driverBirthDate;
    @XmlElement(name = "DriverFiscalCode")
    protected String driverFiscalCode;
    @XmlElement(name = "DriverAddress")
    protected String driverAddress;
    @XmlElement(name = "DriverCity")
    protected String driverCity;
    @XmlElement(name = "DriverProvince")
    protected String driverProvince;
    @XmlElement(name = "DriverZipCode")
    protected String driverZipCode;
    @XmlElement(name = "DriverCountry")
    protected String driverCountry;
    @XmlElement(name = "DriverPhone")
    protected String driverPhone;
    @XmlElement(name = "DriverEmail")
    protected String driverEmail;
    @XmlElement(name = "DriverLicenseNumber")
    protected String driverLicenseNumber;
    @XmlElement(name = "DriverLicenseCategory")
    protected String driverLicenseCategory;
    @XmlElement(name = "DriverLicenseEnd", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar driverLicenseEnd;
    @XmlElement(name = "DriverLesions")
    protected int driverLesions;
    @XmlElement(name = "DamagePosA1", required = true, type = Integer.class, nillable = true)
    protected Integer damagePosA1;
    @XmlElement(name = "DamagePosA2", required = true, type = Integer.class, nillable = true)
    protected Integer damagePosA2;
    @XmlElement(name = "DamagePosA3", required = true, type = Integer.class, nillable = true)
    protected Integer damagePosA3;
    @XmlElement(name = "DamagePosB1", required = true, type = Integer.class, nillable = true)
    protected Integer damagePosB1;
    @XmlElement(name = "DamagePosB2", required = true, type = Integer.class, nillable = true)
    protected Integer damagePosB2;
    @XmlElement(name = "DamagePosB3", required = true, type = Integer.class, nillable = true)
    protected Integer damagePosB3;
    @XmlElement(name = "DamagePosB4", required = true, type = Integer.class, nillable = true)
    protected Integer damagePosB4;
    @XmlElement(name = "DamagePosC1", required = true, type = Integer.class, nillable = true)
    protected Integer damagePosC1;
    @XmlElement(name = "DamagePosC2", required = true, type = Integer.class, nillable = true)
    protected Integer damagePosC2;
    @XmlElement(name = "DamagePosC3", required = true, type = Integer.class, nillable = true)
    protected Integer damagePosC3;
    @XmlElement(name = "DamagePosD", required = true, type = Integer.class, nillable = true)
    protected Integer damagePosD;
    @XmlElement(name = "DamagePosE", required = true, type = Integer.class, nillable = true)
    protected Integer damagePosE;
    @XmlElement(name = "DamagePosF", required = true, type = Integer.class, nillable = true)
    protected Integer damagePosF;
    @XmlElement(name = "DamageDescription")
    protected String damageDescription;
    @XmlElement(name = "DamageComment")
    protected String damageComment;
    @XmlElement(name = "Signature", required = true, type = Integer.class, nillable = true)
    protected Integer signature;
    @XmlElement(name = "Condition01", required = true, type = Integer.class, nillable = true)
    protected Integer condition01;
    @XmlElement(name = "Condition02", required = true, type = Integer.class, nillable = true)
    protected Integer condition02;
    @XmlElement(name = "Condition03", required = true, type = Integer.class, nillable = true)
    protected Integer condition03;
    @XmlElement(name = "Condition04", required = true, type = Integer.class, nillable = true)
    protected Integer condition04;
    @XmlElement(name = "Condition05", required = true, type = Integer.class, nillable = true)
    protected Integer condition05;
    @XmlElement(name = "Condition06", required = true, type = Integer.class, nillable = true)
    protected Integer condition06;
    @XmlElement(name = "Condition07", required = true, type = Integer.class, nillable = true)
    protected Integer condition07;
    @XmlElement(name = "Condition08", required = true, type = Integer.class, nillable = true)
    protected Integer condition08;
    @XmlElement(name = "Condition09", required = true, type = Integer.class, nillable = true)
    protected Integer condition09;
    @XmlElement(name = "Condition10", required = true, type = Integer.class, nillable = true)
    protected Integer condition10;
    @XmlElement(name = "Condition11", required = true, type = Integer.class, nillable = true)
    protected Integer condition11;
    @XmlElement(name = "Condition12", required = true, type = Integer.class, nillable = true)
    protected Integer condition12;
    @XmlElement(name = "Condition13", required = true, type = Integer.class, nillable = true)
    protected Integer condition13;
    @XmlElement(name = "Condition14", required = true, type = Integer.class, nillable = true)
    protected Integer condition14;
    @XmlElement(name = "Condition15", required = true, type = Integer.class, nillable = true)
    protected Integer condition15;
    @XmlElement(name = "Condition16", required = true, type = Integer.class, nillable = true)
    protected Integer condition16;
    @XmlElement(name = "Condition17", required = true, type = Integer.class, nillable = true)
    protected Integer condition17;
    @XmlElement(name = "TotCondition", required = true, type = Integer.class, nillable = true)
    protected Integer totCondition;
    @XmlElement(name = "VehicleDescription")
    protected String vehicleDescription;
    @XmlElement(name = "CaiPart", required = true, type = Integer.class, nillable = true)
    protected Integer caiPart;
    @XmlElement(name = "DamagedType")
    protected int damagedType;

    /**
     * Recupera il valore della proprietà damageNature.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDamageNature() {
        return damageNature;
    }

    /**
     * Imposta il valore della proprietà damageNature.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDamageNature(String value) {
        this.damageNature = value;
    }

    /**
     * Recupera il valore della proprietà repManageType.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRepManageType() {
        return repManageType;
    }

    /**
     * Imposta il valore della proprietà repManageType.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRepManageType(Integer value) {
        this.repManageType = value;
    }

    /**
     * Recupera il valore della proprietà principalOpponent.
     * 
     */
    public int getPrincipalOpponent() {
        return principalOpponent;
    }

    /**
     * Imposta il valore della proprietà principalOpponent.
     * 
     */
    public void setPrincipalOpponent(int value) {
        this.principalOpponent = value;
    }

    /**
     * Recupera il valore della proprietà id.
     * 
     */
    public long getID() {
        return id;
    }

    /**
     * Imposta il valore della proprietà id.
     * 
     */
    public void setID(long value) {
        this.id = value;
    }

    /**
     * Recupera il valore della proprietà complaintID.
     * 
     */
    public int getComplaintID() {
        return complaintID;
    }

    /**
     * Imposta il valore della proprietà complaintID.
     * 
     */
    public void setComplaintID(int value) {
        this.complaintID = value;
    }

    /**
     * Recupera il valore della proprietà principalPart.
     * 
     */
    public int getPrincipalPart() {
        return principalPart;
    }

    /**
     * Imposta il valore della proprietà principalPart.
     * 
     */
    public void setPrincipalPart(int value) {
        this.principalPart = value;
    }

    /**
     * Recupera il valore della proprietà assuredSurname.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssuredSurname() {
        return assuredSurname;
    }

    /**
     * Imposta il valore della proprietà assuredSurname.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssuredSurname(String value) {
        this.assuredSurname = value;
    }

    /**
     * Recupera il valore della proprietà assuredName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssuredName() {
        return assuredName;
    }

    /**
     * Imposta il valore della proprietà assuredName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssuredName(String value) {
        this.assuredName = value;
    }

    /**
     * Recupera il valore della proprietà assuredFiscalCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssuredFiscalCode() {
        return assuredFiscalCode;
    }

    /**
     * Imposta il valore della proprietà assuredFiscalCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssuredFiscalCode(String value) {
        this.assuredFiscalCode = value;
    }

    /**
     * Recupera il valore della proprietà assuredVat.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssuredVat() {
        return assuredVat;
    }

    /**
     * Imposta il valore della proprietà assuredVat.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssuredVat(String value) {
        this.assuredVat = value;
    }

    /**
     * Recupera il valore della proprietà assuredAddress.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssuredAddress() {
        return assuredAddress;
    }

    /**
     * Imposta il valore della proprietà assuredAddress.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssuredAddress(String value) {
        this.assuredAddress = value;
    }

    /**
     * Recupera il valore della proprietà assuredCity.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssuredCity() {
        return assuredCity;
    }

    /**
     * Imposta il valore della proprietà assuredCity.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssuredCity(String value) {
        this.assuredCity = value;
    }

    /**
     * Recupera il valore della proprietà assuredProvince.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssuredProvince() {
        return assuredProvince;
    }

    /**
     * Imposta il valore della proprietà assuredProvince.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssuredProvince(String value) {
        this.assuredProvince = value;
    }

    /**
     * Recupera il valore della proprietà assuredZipCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssuredZipCode() {
        return assuredZipCode;
    }

    /**
     * Imposta il valore della proprietà assuredZipCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssuredZipCode(String value) {
        this.assuredZipCode = value;
    }

    /**
     * Recupera il valore della proprietà assuredCountry.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssuredCountry() {
        return assuredCountry;
    }

    /**
     * Imposta il valore della proprietà assuredCountry.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssuredCountry(String value) {
        this.assuredCountry = value;
    }

    /**
     * Recupera il valore della proprietà assuredPhone.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssuredPhone() {
        return assuredPhone;
    }

    /**
     * Imposta il valore della proprietà assuredPhone.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssuredPhone(String value) {
        this.assuredPhone = value;
    }

    /**
     * Recupera il valore della proprietà assuredEmail.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssuredEmail() {
        return assuredEmail;
    }

    /**
     * Imposta il valore della proprietà assuredEmail.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssuredEmail(String value) {
        this.assuredEmail = value;
    }

    /**
     * Recupera il valore della proprietà vehiclePlate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVehiclePlate() {
        return vehiclePlate;
    }

    /**
     * Imposta il valore della proprietà vehiclePlate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVehiclePlate(String value) {
        this.vehiclePlate = value;
    }

    /**
     * Recupera il valore della proprietà vehicleVIN.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVehicleVIN() {
        return vehicleVIN;
    }

    /**
     * Imposta il valore della proprietà vehicleVIN.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVehicleVIN(String value) {
        this.vehicleVIN = value;
    }

    /**
     * Recupera il valore della proprietà vehicleRegCountry.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVehicleRegCountry() {
        return vehicleRegCountry;
    }

    /**
     * Imposta il valore della proprietà vehicleRegCountry.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVehicleRegCountry(String value) {
        this.vehicleRegCountry = value;
    }

    /**
     * Recupera il valore della proprietà vehicleRegDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getVehicleRegDate() {
        return vehicleRegDate;
    }

    /**
     * Imposta il valore della proprietà vehicleRegDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setVehicleRegDate(XMLGregorianCalendar value) {
        this.vehicleRegDate = value;
    }

    /**
     * Recupera il valore della proprietà trailerPlate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTrailerPlate() {
        return trailerPlate;
    }

    /**
     * Imposta il valore della proprietà trailerPlate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTrailerPlate(String value) {
        this.trailerPlate = value;
    }

    /**
     * Recupera il valore della proprietà trailerVIN.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTrailerVIN() {
        return trailerVIN;
    }

    /**
     * Imposta il valore della proprietà trailerVIN.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTrailerVIN(String value) {
        this.trailerVIN = value;
    }

    /**
     * Recupera il valore della proprietà trailerRegCountry.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTrailerRegCountry() {
        return trailerRegCountry;
    }

    /**
     * Imposta il valore della proprietà trailerRegCountry.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTrailerRegCountry(String value) {
        this.trailerRegCountry = value;
    }

    /**
     * Recupera il valore della proprietà insuranceCompanyName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInsuranceCompanyName() {
        return insuranceCompanyName;
    }

    /**
     * Imposta il valore della proprietà insuranceCompanyName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInsuranceCompanyName(String value) {
        this.insuranceCompanyName = value;
    }

    /**
     * Recupera il valore della proprietà insuranceCompanyID.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getInsuranceCompanyID() {
        return insuranceCompanyID;
    }

    /**
     * Imposta il valore della proprietà insuranceCompanyID.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setInsuranceCompanyID(Integer value) {
        this.insuranceCompanyID = value;
    }

    /**
     * Recupera il valore della proprietà policyNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolicyNumber() {
        return policyNumber;
    }

    /**
     * Imposta il valore della proprietà policyNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolicyNumber(String value) {
        this.policyNumber = value;
    }

    /**
     * Recupera il valore della proprietà greenCardNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGreenCardNumber() {
        return greenCardNumber;
    }

    /**
     * Imposta il valore della proprietà greenCardNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGreenCardNumber(String value) {
        this.greenCardNumber = value;
    }

    /**
     * Recupera il valore della proprietà policyStart.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPolicyStart() {
        return policyStart;
    }

    /**
     * Imposta il valore della proprietà policyStart.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPolicyStart(XMLGregorianCalendar value) {
        this.policyStart = value;
    }

    /**
     * Recupera il valore della proprietà policyEnd.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPolicyEnd() {
        return policyEnd;
    }

    /**
     * Imposta il valore della proprietà policyEnd.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPolicyEnd(XMLGregorianCalendar value) {
        this.policyEnd = value;
    }

    /**
     * Recupera il valore della proprietà insuranceAgencyName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInsuranceAgencyName() {
        return insuranceAgencyName;
    }

    /**
     * Imposta il valore della proprietà insuranceAgencyName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInsuranceAgencyName(String value) {
        this.insuranceAgencyName = value;
    }

    /**
     * Recupera il valore della proprietà insuranceAgencyAddress.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInsuranceAgencyAddress() {
        return insuranceAgencyAddress;
    }

    /**
     * Imposta il valore della proprietà insuranceAgencyAddress.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInsuranceAgencyAddress(String value) {
        this.insuranceAgencyAddress = value;
    }

    /**
     * Recupera il valore della proprietà insuranceAgencyCity.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInsuranceAgencyCity() {
        return insuranceAgencyCity;
    }

    /**
     * Imposta il valore della proprietà insuranceAgencyCity.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInsuranceAgencyCity(String value) {
        this.insuranceAgencyCity = value;
    }

    /**
     * Recupera il valore della proprietà insuranceAgencyProvince.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInsuranceAgencyProvince() {
        return insuranceAgencyProvince;
    }

    /**
     * Imposta il valore della proprietà insuranceAgencyProvince.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInsuranceAgencyProvince(String value) {
        this.insuranceAgencyProvince = value;
    }

    /**
     * Recupera il valore della proprietà insuranceAgencyZipCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInsuranceAgencyZipCode() {
        return insuranceAgencyZipCode;
    }

    /**
     * Imposta il valore della proprietà insuranceAgencyZipCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInsuranceAgencyZipCode(String value) {
        this.insuranceAgencyZipCode = value;
    }

    /**
     * Recupera il valore della proprietà insuranceAgencyPhone.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInsuranceAgencyPhone() {
        return insuranceAgencyPhone;
    }

    /**
     * Imposta il valore della proprietà insuranceAgencyPhone.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInsuranceAgencyPhone(String value) {
        this.insuranceAgencyPhone = value;
    }

    /**
     * Recupera il valore della proprietà insuranceAgencyEmail.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInsuranceAgencyEmail() {
        return insuranceAgencyEmail;
    }

    /**
     * Imposta il valore della proprietà insuranceAgencyEmail.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInsuranceAgencyEmail(String value) {
        this.insuranceAgencyEmail = value;
    }

    /**
     * Recupera il valore della proprietà kasko.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getKasko() {
        return kasko;
    }

    /**
     * Imposta il valore della proprietà kasko.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setKasko(Integer value) {
        this.kasko = value;
    }

    /**
     * Recupera il valore della proprietà driverSurname.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDriverSurname() {
        return driverSurname;
    }

    /**
     * Imposta il valore della proprietà driverSurname.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDriverSurname(String value) {
        this.driverSurname = value;
    }

    /**
     * Recupera il valore della proprietà driverName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDriverName() {
        return driverName;
    }

    /**
     * Imposta il valore della proprietà driverName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDriverName(String value) {
        this.driverName = value;
    }

    /**
     * Recupera il valore della proprietà driverBirthDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDriverBirthDate() {
        return driverBirthDate;
    }

    /**
     * Imposta il valore della proprietà driverBirthDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDriverBirthDate(XMLGregorianCalendar value) {
        this.driverBirthDate = value;
    }

    /**
     * Recupera il valore della proprietà driverFiscalCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDriverFiscalCode() {
        return driverFiscalCode;
    }

    /**
     * Imposta il valore della proprietà driverFiscalCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDriverFiscalCode(String value) {
        this.driverFiscalCode = value;
    }

    /**
     * Recupera il valore della proprietà driverAddress.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDriverAddress() {
        return driverAddress;
    }

    /**
     * Imposta il valore della proprietà driverAddress.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDriverAddress(String value) {
        this.driverAddress = value;
    }

    /**
     * Recupera il valore della proprietà driverCity.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDriverCity() {
        return driverCity;
    }

    /**
     * Imposta il valore della proprietà driverCity.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDriverCity(String value) {
        this.driverCity = value;
    }

    /**
     * Recupera il valore della proprietà driverProvince.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDriverProvince() {
        return driverProvince;
    }

    /**
     * Imposta il valore della proprietà driverProvince.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDriverProvince(String value) {
        this.driverProvince = value;
    }

    /**
     * Recupera il valore della proprietà driverZipCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDriverZipCode() {
        return driverZipCode;
    }

    /**
     * Imposta il valore della proprietà driverZipCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDriverZipCode(String value) {
        this.driverZipCode = value;
    }

    /**
     * Recupera il valore della proprietà driverCountry.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDriverCountry() {
        return driverCountry;
    }

    /**
     * Imposta il valore della proprietà driverCountry.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDriverCountry(String value) {
        this.driverCountry = value;
    }

    /**
     * Recupera il valore della proprietà driverPhone.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDriverPhone() {
        return driverPhone;
    }

    /**
     * Imposta il valore della proprietà driverPhone.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDriverPhone(String value) {
        this.driverPhone = value;
    }

    /**
     * Recupera il valore della proprietà driverEmail.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDriverEmail() {
        return driverEmail;
    }

    /**
     * Imposta il valore della proprietà driverEmail.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDriverEmail(String value) {
        this.driverEmail = value;
    }

    /**
     * Recupera il valore della proprietà driverLicenseNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDriverLicenseNumber() {
        return driverLicenseNumber;
    }

    /**
     * Imposta il valore della proprietà driverLicenseNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDriverLicenseNumber(String value) {
        this.driverLicenseNumber = value;
    }

    /**
     * Recupera il valore della proprietà driverLicenseCategory.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDriverLicenseCategory() {
        return driverLicenseCategory;
    }

    /**
     * Imposta il valore della proprietà driverLicenseCategory.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDriverLicenseCategory(String value) {
        this.driverLicenseCategory = value;
    }

    /**
     * Recupera il valore della proprietà driverLicenseEnd.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDriverLicenseEnd() {
        return driverLicenseEnd;
    }

    /**
     * Imposta il valore della proprietà driverLicenseEnd.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDriverLicenseEnd(XMLGregorianCalendar value) {
        this.driverLicenseEnd = value;
    }

    /**
     * Recupera il valore della proprietà driverLesions.
     * 
     */
    public int getDriverLesions() {
        return driverLesions;
    }

    /**
     * Imposta il valore della proprietà driverLesions.
     * 
     */
    public void setDriverLesions(int value) {
        this.driverLesions = value;
    }

    /**
     * Recupera il valore della proprietà damagePosA1.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDamagePosA1() {
        return damagePosA1;
    }

    /**
     * Imposta il valore della proprietà damagePosA1.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDamagePosA1(Integer value) {
        this.damagePosA1 = value;
    }

    /**
     * Recupera il valore della proprietà damagePosA2.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDamagePosA2() {
        return damagePosA2;
    }

    /**
     * Imposta il valore della proprietà damagePosA2.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDamagePosA2(Integer value) {
        this.damagePosA2 = value;
    }

    /**
     * Recupera il valore della proprietà damagePosA3.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDamagePosA3() {
        return damagePosA3;
    }

    /**
     * Imposta il valore della proprietà damagePosA3.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDamagePosA3(Integer value) {
        this.damagePosA3 = value;
    }

    /**
     * Recupera il valore della proprietà damagePosB1.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDamagePosB1() {
        return damagePosB1;
    }

    /**
     * Imposta il valore della proprietà damagePosB1.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDamagePosB1(Integer value) {
        this.damagePosB1 = value;
    }

    /**
     * Recupera il valore della proprietà damagePosB2.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDamagePosB2() {
        return damagePosB2;
    }

    /**
     * Imposta il valore della proprietà damagePosB2.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDamagePosB2(Integer value) {
        this.damagePosB2 = value;
    }

    /**
     * Recupera il valore della proprietà damagePosB3.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDamagePosB3() {
        return damagePosB3;
    }

    /**
     * Imposta il valore della proprietà damagePosB3.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDamagePosB3(Integer value) {
        this.damagePosB3 = value;
    }

    /**
     * Recupera il valore della proprietà damagePosB4.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDamagePosB4() {
        return damagePosB4;
    }

    /**
     * Imposta il valore della proprietà damagePosB4.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDamagePosB4(Integer value) {
        this.damagePosB4 = value;
    }

    /**
     * Recupera il valore della proprietà damagePosC1.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDamagePosC1() {
        return damagePosC1;
    }

    /**
     * Imposta il valore della proprietà damagePosC1.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDamagePosC1(Integer value) {
        this.damagePosC1 = value;
    }

    /**
     * Recupera il valore della proprietà damagePosC2.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDamagePosC2() {
        return damagePosC2;
    }

    /**
     * Imposta il valore della proprietà damagePosC2.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDamagePosC2(Integer value) {
        this.damagePosC2 = value;
    }

    /**
     * Recupera il valore della proprietà damagePosC3.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDamagePosC3() {
        return damagePosC3;
    }

    /**
     * Imposta il valore della proprietà damagePosC3.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDamagePosC3(Integer value) {
        this.damagePosC3 = value;
    }

    /**
     * Recupera il valore della proprietà damagePosD.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDamagePosD() {
        return damagePosD;
    }

    /**
     * Imposta il valore della proprietà damagePosD.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDamagePosD(Integer value) {
        this.damagePosD = value;
    }

    /**
     * Recupera il valore della proprietà damagePosE.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDamagePosE() {
        return damagePosE;
    }

    /**
     * Imposta il valore della proprietà damagePosE.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDamagePosE(Integer value) {
        this.damagePosE = value;
    }

    /**
     * Recupera il valore della proprietà damagePosF.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDamagePosF() {
        return damagePosF;
    }

    /**
     * Imposta il valore della proprietà damagePosF.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDamagePosF(Integer value) {
        this.damagePosF = value;
    }

    /**
     * Recupera il valore della proprietà damageDescription.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDamageDescription() {
        return damageDescription;
    }

    /**
     * Imposta il valore della proprietà damageDescription.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDamageDescription(String value) {
        this.damageDescription = value;
    }

    /**
     * Recupera il valore della proprietà damageComment.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDamageComment() {
        return damageComment;
    }

    /**
     * Imposta il valore della proprietà damageComment.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDamageComment(String value) {
        this.damageComment = value;
    }

    /**
     * Recupera il valore della proprietà signature.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSignature() {
        return signature;
    }

    /**
     * Imposta il valore della proprietà signature.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSignature(Integer value) {
        this.signature = value;
    }

    /**
     * Recupera il valore della proprietà condition01.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCondition01() {
        return condition01;
    }

    /**
     * Imposta il valore della proprietà condition01.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCondition01(Integer value) {
        this.condition01 = value;
    }

    /**
     * Recupera il valore della proprietà condition02.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCondition02() {
        return condition02;
    }

    /**
     * Imposta il valore della proprietà condition02.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCondition02(Integer value) {
        this.condition02 = value;
    }

    /**
     * Recupera il valore della proprietà condition03.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCondition03() {
        return condition03;
    }

    /**
     * Imposta il valore della proprietà condition03.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCondition03(Integer value) {
        this.condition03 = value;
    }

    /**
     * Recupera il valore della proprietà condition04.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCondition04() {
        return condition04;
    }

    /**
     * Imposta il valore della proprietà condition04.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCondition04(Integer value) {
        this.condition04 = value;
    }

    /**
     * Recupera il valore della proprietà condition05.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCondition05() {
        return condition05;
    }

    /**
     * Imposta il valore della proprietà condition05.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCondition05(Integer value) {
        this.condition05 = value;
    }

    /**
     * Recupera il valore della proprietà condition06.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCondition06() {
        return condition06;
    }

    /**
     * Imposta il valore della proprietà condition06.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCondition06(Integer value) {
        this.condition06 = value;
    }

    /**
     * Recupera il valore della proprietà condition07.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCondition07() {
        return condition07;
    }

    /**
     * Imposta il valore della proprietà condition07.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCondition07(Integer value) {
        this.condition07 = value;
    }

    /**
     * Recupera il valore della proprietà condition08.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCondition08() {
        return condition08;
    }

    /**
     * Imposta il valore della proprietà condition08.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCondition08(Integer value) {
        this.condition08 = value;
    }

    /**
     * Recupera il valore della proprietà condition09.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCondition09() {
        return condition09;
    }

    /**
     * Imposta il valore della proprietà condition09.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCondition09(Integer value) {
        this.condition09 = value;
    }

    /**
     * Recupera il valore della proprietà condition10.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCondition10() {
        return condition10;
    }

    /**
     * Imposta il valore della proprietà condition10.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCondition10(Integer value) {
        this.condition10 = value;
    }

    /**
     * Recupera il valore della proprietà condition11.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCondition11() {
        return condition11;
    }

    /**
     * Imposta il valore della proprietà condition11.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCondition11(Integer value) {
        this.condition11 = value;
    }

    /**
     * Recupera il valore della proprietà condition12.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCondition12() {
        return condition12;
    }

    /**
     * Imposta il valore della proprietà condition12.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCondition12(Integer value) {
        this.condition12 = value;
    }

    /**
     * Recupera il valore della proprietà condition13.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCondition13() {
        return condition13;
    }

    /**
     * Imposta il valore della proprietà condition13.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCondition13(Integer value) {
        this.condition13 = value;
    }

    /**
     * Recupera il valore della proprietà condition14.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCondition14() {
        return condition14;
    }

    /**
     * Imposta il valore della proprietà condition14.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCondition14(Integer value) {
        this.condition14 = value;
    }

    /**
     * Recupera il valore della proprietà condition15.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCondition15() {
        return condition15;
    }

    /**
     * Imposta il valore della proprietà condition15.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCondition15(Integer value) {
        this.condition15 = value;
    }

    /**
     * Recupera il valore della proprietà condition16.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCondition16() {
        return condition16;
    }

    /**
     * Imposta il valore della proprietà condition16.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCondition16(Integer value) {
        this.condition16 = value;
    }

    /**
     * Recupera il valore della proprietà condition17.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCondition17() {
        return condition17;
    }

    /**
     * Imposta il valore della proprietà condition17.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCondition17(Integer value) {
        this.condition17 = value;
    }

    /**
     * Recupera il valore della proprietà totCondition.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTotCondition() {
        return totCondition;
    }

    /**
     * Imposta il valore della proprietà totCondition.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTotCondition(Integer value) {
        this.totCondition = value;
    }

    /**
     * Recupera il valore della proprietà vehicleDescription.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVehicleDescription() {
        return vehicleDescription;
    }

    /**
     * Imposta il valore della proprietà vehicleDescription.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVehicleDescription(String value) {
        this.vehicleDescription = value;
    }

    /**
     * Recupera il valore della proprietà caiPart.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCaiPart() {
        return caiPart;
    }

    /**
     * Imposta il valore della proprietà caiPart.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCaiPart(Integer value) {
        this.caiPart = value;
    }

    /**
     * Recupera il valore della proprietà damagedType.
     * 
     */
    public int getDamagedType() {
        return damagedType;
    }

    /**
     * Imposta il valore della proprietà damagedType.
     * 
     */
    public void setDamagedType(int value) {
        this.damagedType = value;
    }

}
