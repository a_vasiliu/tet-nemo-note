
package com.doing.nemo.claims.websin.websinws;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per ArrayOfMigrationInjured complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfMigrationInjured">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MigrationInjured" type="{http://automotivedn.com/WebsinWS}MigrationInjured" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfMigrationInjured", propOrder = {
    "migrationInjured"
})
public class ArrayOfMigrationInjured
    implements Serializable
{

    @XmlElement(name = "MigrationInjured", nillable = true)
    protected List<MigrationInjured> migrationInjured;

    /**
     * Gets the value of the migrationInjured property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the migrationInjured property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMigrationInjured().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MigrationInjured }
     * 
     * 
     */
    public List<MigrationInjured> getMigrationInjured() {
        if (migrationInjured == null) {
            migrationInjured = new ArrayList<MigrationInjured>();
        }
        return this.migrationInjured;
    }

}
