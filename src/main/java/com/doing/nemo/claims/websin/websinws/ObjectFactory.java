
package com.doing.nemo.claims.websin.websinws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.automotivedn.websinws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AuthHeader_QNAME = new QName("http://automotivedn.com/WebsinWS", "AuthHeader");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.automotivedn.websinws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AuthHeader }
     * 
     */
    public AuthHeader createAuthHeader() {
        return new AuthHeader();
    }

    /**
     * Create an instance of {@link UploadFile }
     * 
     */
    public UploadFile createUploadFile() {
        return new UploadFile();
    }

    /**
     * Create an instance of {@link UploadFileRequest }
     * 
     */
    public UploadFileRequest createUploadFileRequest() {
        return new UploadFileRequest();
    }

    /**
     * Create an instance of {@link PutComplaintResponse }
     * 
     */
    public PutComplaintResponse createPutComplaintResponse() {
        return new PutComplaintResponse();
    }

    /**
     * Create an instance of {@link PutComplaintResponseType }
     * 
     */
    public PutComplaintResponseType createPutComplaintResponseType() {
        return new PutComplaintResponseType();
    }

    /**
     * Create an instance of {@link PutComplaint }
     * 
     */
    public PutComplaint createPutComplaint() {
        return new PutComplaint();
    }

    /**
     * Create an instance of {@link PutComplaintRequest }
     * 
     */
    public PutComplaintRequest createPutComplaintRequest() {
        return new PutComplaintRequest();
    }

    /**
     * Create an instance of {@link UploadFileResponse }
     * 
     */
    public UploadFileResponse createUploadFileResponse() {
        return new UploadFileResponse();
    }

    /**
     * Create an instance of {@link UploadFileResponseType }
     * 
     */
    public UploadFileResponseType createUploadFileResponseType() {
        return new UploadFileResponseType();
    }

    /**
     * Create an instance of {@link MigrationRepairDossier }
     * 
     */
    public MigrationRepairDossier createMigrationRepairDossier() {
        return new MigrationRepairDossier();
    }

    /**
     * Create an instance of {@link ResponseWS }
     * 
     */
    public ResponseWS createResponseWS() {
        return new ResponseWS();
    }

    /**
     * Create an instance of {@link ArrayOfMigrationInjured }
     * 
     */
    public ArrayOfMigrationInjured createArrayOfMigrationInjured() {
        return new ArrayOfMigrationInjured();
    }

    /**
     * Create an instance of {@link MigrationInjured }
     * 
     */
    public MigrationInjured createMigrationInjured() {
        return new MigrationInjured();
    }

    /**
     * Create an instance of {@link ArrayOfMigrationDamaged }
     * 
     */
    public ArrayOfMigrationDamaged createArrayOfMigrationDamaged() {
        return new ArrayOfMigrationDamaged();
    }

    /**
     * Create an instance of {@link MigrationDamaged }
     * 
     */
    public MigrationDamaged createMigrationDamaged() {
        return new MigrationDamaged();
    }

    /**
     * Create an instance of {@link MigrationComplaint }
     * 
     */
    public MigrationComplaint createMigrationComplaint() {
        return new MigrationComplaint();
    }

    /**
     * Create an instance of {@link ArrayOfMigrationRepairDossier }
     * 
     */
    public ArrayOfMigrationRepairDossier createArrayOfMigrationRepairDossier() {
        return new ArrayOfMigrationRepairDossier();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AuthHeader }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://automotivedn.com/WebsinWS", name = "AuthHeader")
    public JAXBElement<AuthHeader> createAuthHeader(AuthHeader value) {
        return new JAXBElement<AuthHeader>(_AuthHeader_QNAME, AuthHeader.class, null, value);
    }

}
