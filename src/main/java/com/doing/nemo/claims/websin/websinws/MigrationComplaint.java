
package com.doing.nemo.claims.websin.websinws;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java per MigrationComplaint complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="MigrationComplaint">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="RegisterNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ModuleType" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="AccidentDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="AccidentHour" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AccidentCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AccidentProvince" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AccidentAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AccidentForeing" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="AccidentInjureds" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="OldMotoPlate" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="DamageOtherVehicles" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="DamageObject" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="WitnessesDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PresenceAuth" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="PresencePolice" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="PresenceCC" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="PresenceVVUU" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="PresenceAuthDes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AccidentType" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ExternalAccidentNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="FlowType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CompanyAccidentType" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CompanyAccidentDef" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="ExpertID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="RepairStatus" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="InsuranceCompanyID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="LawyerID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="FeturedDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="ExternalInspectorate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExternalNote" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExternalAccidentType" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ExternalExpert" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LiqValue" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="LiqDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="LiqNote" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LiqWait" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="NotyTypeID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="NotRepairable" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Recoverable" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="PerRecoverable" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="VehicleValue" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="ThirdParies" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="RepairLock" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ReasonTypeID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ReicevedDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="ExcessLostKeys" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="ExcessEvents" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="CommitDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="CommitTo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="InsuranceManagerID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="InsuranceBrokerID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="InsuranceAdjusterID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="OfferConfirm" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="RepairerDes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AccidentRegion" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="RepairIsChanged" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="RepairLog" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AntiTheftServiceID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AntiTheftAlerted" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="TheftOnBranch" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="TheftOnBranchCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FindDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="FindHour" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FindForeing" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="FindCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FindProvince" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FindAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FindAuthPolice" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="FindAuthCC" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="FindAuthVVUU" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="FindAuthDes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FindAuthPhone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FindAuthSeized" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="IsRobbery" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="IsKeysManage" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="IsAccountingManage" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="IsCkAdminPosition" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="RxFirstKey" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="RxSecondKey" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="RxOriComplaint" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="RxCpyComplaint" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="RxOriVerbFind" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="RxCpyVerbFind" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="ReqLoss" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="RxReqLoss" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="ReqRepossession" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="RxReqRepossession" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="FindSPointEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsSummons" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ToNotified" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="LastNotification" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="PaiInsurancePolicyID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="PaiCommitDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="ReasonsDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AntiTheftActivate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="TheftDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="SPCenterID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="PresenceAuthPhone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FindVehicleCO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PayType1" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="PayDate1" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="PayLawyerCost1" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="PayDamageValue1" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="PayTechBlock1" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="PayType2" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="PayDate2" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="PayLawyerCost2" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="PayDamageValue2" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="PayTechBlock2" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="PayType3" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="PayDate3" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="PayLawyerCost3" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="PayDamageValue3" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="PayTechBlock3" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="PayTot1" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="PayTot2" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="PayTot3" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="ResidualValue" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="DeductibleAmount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="PayAmount1" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="PayAmount2" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="PayAmount3" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="IsEndOfLeasing" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="DeductEnd1" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="DeductPaid1" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="DeductImported1" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="DeductEnd2" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="DeductPaid2" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="DeductImported2" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="DeductTotalPaid" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="IsWitnessConfirm" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="AtfLastCheck" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="AtfResResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AtfVoucherID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="AtfSrvProvider" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="AtfResCode" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="AtfResMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AtfResRepNum" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="AtfResGForce" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AtfResNumCrash" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="AtfResAnomaly" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="IsReRegistration" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ReRegistrationReq" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="ReRegistration" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="CRMCaseReReg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsDuplRegistration" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="DuplRegistrationReq" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="DuplRegistration" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="CRMCaseDuplReg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsReVIN" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ReVINReq" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="ReVIN" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="CRMCaseReVIN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsDemolition" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="DemolitionReq" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="Demolition" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="CRMCaseDemolition" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SeizureID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MigrationComplaint", propOrder = {
    "id",
    "registerNumber",
    "moduleType",
    "accidentDate",
    "accidentHour",
    "accidentCity",
    "accidentProvince",
    "accidentAddress",
    "accidentForeing",
    "accidentInjureds",
    "oldMotoPlate",
    "damageOtherVehicles",
    "damageObject",
    "witnessesDescription",
    "presenceAuth",
    "presencePolice",
    "presenceCC",
    "presenceVVUU",
    "presenceAuthDes",
    "accidentType",
    "externalAccidentNumber",
    "status",
    "flowType",
    "companyAccidentType",
    "companyAccidentDef",
    "expertID",
    "repairStatus",
    "insuranceCompanyID",
    "lawyerID",
    "feturedDate",
    "externalInspectorate",
    "externalNote",
    "externalAccidentType",
    "externalExpert",
    "liqValue",
    "liqDate",
    "liqNote",
    "liqWait",
    "notyTypeID",
    "notRepairable",
    "recoverable",
    "perRecoverable",
    "vehicleValue",
    "thirdParies",
    "repairLock",
    "reasonTypeID",
    "reicevedDate",
    "excessLostKeys",
    "excessEvents",
    "commitDate",
    "commitTo",
    "insuranceManagerID",
    "insuranceBrokerID",
    "insuranceAdjusterID",
    "offerConfirm",
    "repairerDes",
    "accidentRegion",
    "repairIsChanged",
    "repairLog",
    "antiTheftServiceID",
    "antiTheftAlerted",
    "theftOnBranch",
    "theftOnBranchCode",
    "findDate",
    "findHour",
    "findForeing",
    "findCity",
    "findProvince",
    "findAddress",
    "findAuthPolice",
    "findAuthCC",
    "findAuthVVUU",
    "findAuthDes",
    "findAuthPhone",
    "findAuthSeized",
    "isRobbery",
    "isKeysManage",
    "isAccountingManage",
    "isCkAdminPosition",
    "rxFirstKey",
    "rxSecondKey",
    "rxOriComplaint",
    "rxCpyComplaint",
    "rxOriVerbFind",
    "rxCpyVerbFind",
    "reqLoss",
    "rxReqLoss",
    "reqRepossession",
    "rxReqRepossession",
    "findSPointEmail",
    "isSummons",
    "toNotified",
    "lastNotification",
    "paiInsurancePolicyID",
    "paiCommitDate",
    "reasonsDesc",
    "antiTheftActivate",
    "theftDate",
    "spCenterID",
    "presenceAuthPhone",
    "findVehicleCO",
    "payType1",
    "payDate1",
    "payLawyerCost1",
    "payDamageValue1",
    "payTechBlock1",
    "payType2",
    "payDate2",
    "payLawyerCost2",
    "payDamageValue2",
    "payTechBlock2",
    "payType3",
    "payDate3",
    "payLawyerCost3",
    "payDamageValue3",
    "payTechBlock3",
    "payTot1",
    "payTot2",
    "payTot3",
    "residualValue",
    "deductibleAmount",
    "payAmount1",
    "payAmount2",
    "payAmount3",
    "isEndOfLeasing",
    "deductEnd1",
    "deductPaid1",
    "deductImported1",
    "deductEnd2",
    "deductPaid2",
    "deductImported2",
    "deductTotalPaid",
    "isWitnessConfirm",
    "atfLastCheck",
    "atfResResult",
    "atfVoucherID",
    "atfSrvProvider",
    "atfResCode",
    "atfResMessage",
    "atfResRepNum",
    "atfResGForce",
    "atfResNumCrash",
    "atfResAnomaly",
    "isReRegistration",
    "reRegistrationReq",
    "reRegistration",
    "crmCaseReReg",
    "isDuplRegistration",
    "duplRegistrationReq",
    "duplRegistration",
    "crmCaseDuplReg",
    "isReVIN",
    "reVINReq",
    "reVIN",
    "crmCaseReVIN",
    "isDemolition",
    "demolitionReq",
    "demolition",
    "crmCaseDemolition",
    "seizureID"
})
public class MigrationComplaint
    implements Serializable
{

    @XmlElement(name = "ID")
    protected int id;
    @XmlElement(name = "RegisterNumber")
    protected String registerNumber;
    @XmlElement(name = "ModuleType")
    protected int moduleType;
    @XmlElement(name = "AccidentDate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar accidentDate;
    @XmlElement(name = "AccidentHour")
    protected String accidentHour;
    @XmlElement(name = "AccidentCity")
    protected String accidentCity;
    @XmlElement(name = "AccidentProvince")
    protected String accidentProvince;
    @XmlElement(name = "AccidentAddress")
    protected String accidentAddress;
    @XmlElement(name = "AccidentForeing")
    protected int accidentForeing;
    @XmlElement(name = "AccidentInjureds")
    protected int accidentInjureds;
    @XmlElement(name = "OldMotoPlate")
    protected int oldMotoPlate;
    @XmlElement(name = "DamageOtherVehicles")
    protected int damageOtherVehicles;
    @XmlElement(name = "DamageObject")
    protected int damageObject;
    @XmlElement(name = "WitnessesDescription")
    protected String witnessesDescription;
    @XmlElement(name = "PresenceAuth")
    protected int presenceAuth;
    @XmlElement(name = "PresencePolice")
    protected int presencePolice;
    @XmlElement(name = "PresenceCC")
    protected int presenceCC;
    @XmlElement(name = "PresenceVVUU")
    protected int presenceVVUU;
    @XmlElement(name = "PresenceAuthDes")
    protected String presenceAuthDes;
    @XmlElement(name = "AccidentType")
    protected int accidentType;
    @XmlElement(name = "ExternalAccidentNumber")
    protected String externalAccidentNumber;
    @XmlElement(name = "Status")
    protected int status;
    @XmlElement(name = "FlowType")
    protected String flowType;
    @XmlElement(name = "CompanyAccidentType", required = true, type = Integer.class, nillable = true)
    protected Integer companyAccidentType;
    @XmlElement(name = "CompanyAccidentDef", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar companyAccidentDef;
    @XmlElement(name = "ExpertID", required = true, type = Integer.class, nillable = true)
    protected Integer expertID;
    @XmlElement(name = "RepairStatus")
    protected int repairStatus;
    @XmlElement(name = "InsuranceCompanyID", required = true, type = Integer.class, nillable = true)
    protected Integer insuranceCompanyID;
    @XmlElement(name = "LawyerID", required = true, type = Integer.class, nillable = true)
    protected Integer lawyerID;
    @XmlElement(name = "FeturedDate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar feturedDate;
    @XmlElement(name = "ExternalInspectorate")
    protected String externalInspectorate;
    @XmlElement(name = "ExternalNote")
    protected String externalNote;
    @XmlElement(name = "ExternalAccidentType", required = true, type = Integer.class, nillable = true)
    protected Integer externalAccidentType;
    @XmlElement(name = "ExternalExpert")
    protected String externalExpert;
    @XmlElement(name = "LiqValue", required = true, nillable = true)
    protected BigDecimal liqValue;
    @XmlElement(name = "LiqDate", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar liqDate;
    @XmlElement(name = "LiqNote")
    protected String liqNote;
    @XmlElement(name = "LiqWait", required = true, nillable = true)
    protected BigDecimal liqWait;
    @XmlElement(name = "NotyTypeID")
    protected int notyTypeID;
    @XmlElement(name = "NotRepairable")
    protected int notRepairable;
    @XmlElement(name = "Recoverable")
    protected int recoverable;
    @XmlElement(name = "PerRecoverable")
    protected int perRecoverable;
    @XmlElement(name = "VehicleValue", required = true, nillable = true)
    protected BigDecimal vehicleValue;
    @XmlElement(name = "ThirdParies")
    protected int thirdParies;
    @XmlElement(name = "RepairLock")
    protected int repairLock;
    @XmlElement(name = "ReasonTypeID", required = true, type = Integer.class, nillable = true)
    protected Integer reasonTypeID;
    @XmlElement(name = "ReicevedDate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar reicevedDate;
    @XmlElement(name = "ExcessLostKeys", required = true)
    protected BigDecimal excessLostKeys;
    @XmlElement(name = "ExcessEvents", required = true)
    protected BigDecimal excessEvents;
    @XmlElement(name = "CommitDate", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar commitDate;
    @XmlElement(name = "CommitTo", required = true, type = Integer.class, nillable = true)
    protected Integer commitTo;
    @XmlElement(name = "InsuranceManagerID", required = true, type = Integer.class, nillable = true)
    protected Integer insuranceManagerID;
    @XmlElement(name = "InsuranceBrokerID", required = true, type = Integer.class, nillable = true)
    protected Integer insuranceBrokerID;
    @XmlElement(name = "InsuranceAdjusterID", required = true, type = Integer.class, nillable = true)
    protected Integer insuranceAdjusterID;
    @XmlElement(name = "OfferConfirm", required = true, type = Integer.class, nillable = true)
    protected Integer offerConfirm;
    @XmlElement(name = "RepairerDes")
    protected String repairerDes;
    @XmlElement(name = "AccidentRegion", required = true, type = Integer.class, nillable = true)
    protected Integer accidentRegion;
    @XmlElement(name = "RepairIsChanged")
    protected int repairIsChanged;
    @XmlElement(name = "RepairLog")
    protected String repairLog;
    @XmlElement(name = "AntiTheftServiceID")
    protected String antiTheftServiceID;
    @XmlElement(name = "AntiTheftAlerted", required = true, type = Integer.class, nillable = true)
    protected Integer antiTheftAlerted;
    @XmlElement(name = "TheftOnBranch", required = true, type = Integer.class, nillable = true)
    protected Integer theftOnBranch;
    @XmlElement(name = "TheftOnBranchCode")
    protected String theftOnBranchCode;
    @XmlElement(name = "FindDate", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar findDate;
    @XmlElement(name = "FindHour")
    protected String findHour;
    @XmlElement(name = "FindForeing", required = true, type = Integer.class, nillable = true)
    protected Integer findForeing;
    @XmlElement(name = "FindCity")
    protected String findCity;
    @XmlElement(name = "FindProvince")
    protected String findProvince;
    @XmlElement(name = "FindAddress")
    protected String findAddress;
    @XmlElement(name = "FindAuthPolice", required = true, type = Integer.class, nillable = true)
    protected Integer findAuthPolice;
    @XmlElement(name = "FindAuthCC", required = true, type = Integer.class, nillable = true)
    protected Integer findAuthCC;
    @XmlElement(name = "FindAuthVVUU", required = true, type = Integer.class, nillable = true)
    protected Integer findAuthVVUU;
    @XmlElement(name = "FindAuthDes")
    protected String findAuthDes;
    @XmlElement(name = "FindAuthPhone")
    protected String findAuthPhone;
    @XmlElement(name = "FindAuthSeized", required = true, type = Integer.class, nillable = true)
    protected Integer findAuthSeized;
    @XmlElement(name = "IsRobbery", required = true, type = Integer.class, nillable = true)
    protected Integer isRobbery;
    @XmlElement(name = "IsKeysManage", required = true, type = Integer.class, nillable = true)
    protected Integer isKeysManage;
    @XmlElement(name = "IsAccountingManage", required = true, type = Integer.class, nillable = true)
    protected Integer isAccountingManage;
    @XmlElement(name = "IsCkAdminPosition", required = true, type = Integer.class, nillable = true)
    protected Integer isCkAdminPosition;
    @XmlElement(name = "RxFirstKey", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar rxFirstKey;
    @XmlElement(name = "RxSecondKey", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar rxSecondKey;
    @XmlElement(name = "RxOriComplaint", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar rxOriComplaint;
    @XmlElement(name = "RxCpyComplaint", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar rxCpyComplaint;
    @XmlElement(name = "RxOriVerbFind", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar rxOriVerbFind;
    @XmlElement(name = "RxCpyVerbFind", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar rxCpyVerbFind;
    @XmlElement(name = "ReqLoss", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar reqLoss;
    @XmlElement(name = "RxReqLoss", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar rxReqLoss;
    @XmlElement(name = "ReqRepossession", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar reqRepossession;
    @XmlElement(name = "RxReqRepossession", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar rxReqRepossession;
    @XmlElement(name = "FindSPointEmail")
    protected String findSPointEmail;
    @XmlElement(name = "IsSummons", required = true, type = Integer.class, nillable = true)
    protected Integer isSummons;
    @XmlElement(name = "ToNotified", required = true, type = Integer.class, nillable = true)
    protected Integer toNotified;
    @XmlElement(name = "LastNotification", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar lastNotification;
    @XmlElement(name = "PaiInsurancePolicyID", required = true, type = Integer.class, nillable = true)
    protected Integer paiInsurancePolicyID;
    @XmlElement(name = "PaiCommitDate", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar paiCommitDate;
    @XmlElement(name = "ReasonsDesc")
    protected String reasonsDesc;
    @XmlElement(name = "AntiTheftActivate", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar antiTheftActivate;
    @XmlElement(name = "TheftDate", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar theftDate;
    @XmlElement(name = "SPCenterID", required = true, type = Integer.class, nillable = true)
    protected Integer spCenterID;
    @XmlElement(name = "PresenceAuthPhone")
    protected String presenceAuthPhone;
    @XmlElement(name = "FindVehicleCO")
    protected String findVehicleCO;
    @XmlElement(name = "PayType1", required = true, type = Integer.class, nillable = true)
    protected Integer payType1;
    @XmlElement(name = "PayDate1", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar payDate1;
    @XmlElement(name = "PayLawyerCost1", required = true, nillable = true)
    protected BigDecimal payLawyerCost1;
    @XmlElement(name = "PayDamageValue1", required = true, nillable = true)
    protected BigDecimal payDamageValue1;
    @XmlElement(name = "PayTechBlock1", required = true, nillable = true)
    protected BigDecimal payTechBlock1;
    @XmlElement(name = "PayType2", required = true, type = Integer.class, nillable = true)
    protected Integer payType2;
    @XmlElement(name = "PayDate2", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar payDate2;
    @XmlElement(name = "PayLawyerCost2", required = true, nillable = true)
    protected BigDecimal payLawyerCost2;
    @XmlElement(name = "PayDamageValue2", required = true, nillable = true)
    protected BigDecimal payDamageValue2;
    @XmlElement(name = "PayTechBlock2", required = true, nillable = true)
    protected BigDecimal payTechBlock2;
    @XmlElement(name = "PayType3", required = true, type = Integer.class, nillable = true)
    protected Integer payType3;
    @XmlElement(name = "PayDate3", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar payDate3;
    @XmlElement(name = "PayLawyerCost3", required = true, nillable = true)
    protected BigDecimal payLawyerCost3;
    @XmlElement(name = "PayDamageValue3", required = true, nillable = true)
    protected BigDecimal payDamageValue3;
    @XmlElement(name = "PayTechBlock3", required = true, nillable = true)
    protected BigDecimal payTechBlock3;
    @XmlElement(name = "PayTot1", required = true, nillable = true)
    protected BigDecimal payTot1;
    @XmlElement(name = "PayTot2", required = true, nillable = true)
    protected BigDecimal payTot2;
    @XmlElement(name = "PayTot3", required = true, nillable = true)
    protected BigDecimal payTot3;
    @XmlElement(name = "ResidualValue", required = true, nillable = true)
    protected BigDecimal residualValue;
    @XmlElement(name = "DeductibleAmount", required = true, nillable = true)
    protected BigDecimal deductibleAmount;
    @XmlElement(name = "PayAmount1", required = true, nillable = true)
    protected BigDecimal payAmount1;
    @XmlElement(name = "PayAmount2", required = true, nillable = true)
    protected BigDecimal payAmount2;
    @XmlElement(name = "PayAmount3", required = true, nillable = true)
    protected BigDecimal payAmount3;
    @XmlElement(name = "IsEndOfLeasing", required = true, type = Integer.class, nillable = true)
    protected Integer isEndOfLeasing;
    @XmlElement(name = "DeductEnd1", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar deductEnd1;
    @XmlElement(name = "DeductPaid1", required = true, nillable = true)
    protected BigDecimal deductPaid1;
    @XmlElement(name = "DeductImported1", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar deductImported1;
    @XmlElement(name = "DeductEnd2", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar deductEnd2;
    @XmlElement(name = "DeductPaid2", required = true, nillable = true)
    protected BigDecimal deductPaid2;
    @XmlElement(name = "DeductImported2", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar deductImported2;
    @XmlElement(name = "DeductTotalPaid", required = true, nillable = true)
    protected BigDecimal deductTotalPaid;
    @XmlElement(name = "IsWitnessConfirm", required = true, type = Integer.class, nillable = true)
    protected Integer isWitnessConfirm;
    @XmlElement(name = "AtfLastCheck", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar atfLastCheck;
    @XmlElement(name = "AtfResResult")
    protected String atfResResult;
    @XmlElement(name = "AtfVoucherID", required = true, type = Integer.class, nillable = true)
    protected Integer atfVoucherID;
    @XmlElement(name = "AtfSrvProvider", required = true, type = Integer.class, nillable = true)
    protected Integer atfSrvProvider;
    @XmlElement(name = "AtfResCode", required = true, type = Integer.class, nillable = true)
    protected Integer atfResCode;
    @XmlElement(name = "AtfResMessage")
    protected String atfResMessage;
    @XmlElement(name = "AtfResRepNum", required = true, type = Integer.class, nillable = true)
    protected Integer atfResRepNum;
    @XmlElement(name = "AtfResGForce")
    protected String atfResGForce;
    @XmlElement(name = "AtfResNumCrash", required = true, type = Integer.class, nillable = true)
    protected Integer atfResNumCrash;
    @XmlElement(name = "AtfResAnomaly", required = true, type = Integer.class, nillable = true)
    protected Integer atfResAnomaly;
    @XmlElement(name = "IsReRegistration", required = true, type = Integer.class, nillable = true)
    protected Integer isReRegistration;
    @XmlElement(name = "ReRegistrationReq", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar reRegistrationReq;
    @XmlElement(name = "ReRegistration", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar reRegistration;
    @XmlElement(name = "CRMCaseReReg")
    protected String crmCaseReReg;
    @XmlElement(name = "IsDuplRegistration", required = true, type = Integer.class, nillable = true)
    protected Integer isDuplRegistration;
    @XmlElement(name = "DuplRegistrationReq", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar duplRegistrationReq;
    @XmlElement(name = "DuplRegistration", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar duplRegistration;
    @XmlElement(name = "CRMCaseDuplReg")
    protected String crmCaseDuplReg;
    @XmlElement(name = "IsReVIN", required = true, type = Integer.class, nillable = true)
    protected Integer isReVIN;
    @XmlElement(name = "ReVINReq", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar reVINReq;
    @XmlElement(name = "ReVIN", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar reVIN;
    @XmlElement(name = "CRMCaseReVIN")
    protected String crmCaseReVIN;
    @XmlElement(name = "IsDemolition", required = true, type = Integer.class, nillable = true)
    protected Integer isDemolition;
    @XmlElement(name = "DemolitionReq", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar demolitionReq;
    @XmlElement(name = "Demolition", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar demolition;
    @XmlElement(name = "CRMCaseDemolition")
    protected String crmCaseDemolition;
    @XmlElement(name = "SeizureID", required = true, type = Integer.class, nillable = true)
    protected Integer seizureID;

    /**
     * Recupera il valore della proprietà id.
     * 
     */
    public int getID() {
        return id;
    }

    /**
     * Imposta il valore della proprietà id.
     * 
     */
    public void setID(int value) {
        this.id = value;
    }

    /**
     * Recupera il valore della proprietà registerNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegisterNumber() {
        return registerNumber;
    }

    /**
     * Imposta il valore della proprietà registerNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegisterNumber(String value) {
        this.registerNumber = value;
    }

    /**
     * Recupera il valore della proprietà moduleType.
     * 
     */
    public int getModuleType() {
        return moduleType;
    }

    /**
     * Imposta il valore della proprietà moduleType.
     * 
     */
    public void setModuleType(int value) {
        this.moduleType = value;
    }

    /**
     * Recupera il valore della proprietà accidentDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAccidentDate() {
        return accidentDate;
    }

    /**
     * Imposta il valore della proprietà accidentDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAccidentDate(XMLGregorianCalendar value) {
        this.accidentDate = value;
    }

    /**
     * Recupera il valore della proprietà accidentHour.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccidentHour() {
        return accidentHour;
    }

    /**
     * Imposta il valore della proprietà accidentHour.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccidentHour(String value) {
        this.accidentHour = value;
    }

    /**
     * Recupera il valore della proprietà accidentCity.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccidentCity() {
        return accidentCity;
    }

    /**
     * Imposta il valore della proprietà accidentCity.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccidentCity(String value) {
        this.accidentCity = value;
    }

    /**
     * Recupera il valore della proprietà accidentProvince.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccidentProvince() {
        return accidentProvince;
    }

    /**
     * Imposta il valore della proprietà accidentProvince.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccidentProvince(String value) {
        this.accidentProvince = value;
    }

    /**
     * Recupera il valore della proprietà accidentAddress.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccidentAddress() {
        return accidentAddress;
    }

    /**
     * Imposta il valore della proprietà accidentAddress.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccidentAddress(String value) {
        this.accidentAddress = value;
    }

    /**
     * Recupera il valore della proprietà accidentForeing.
     * 
     */
    public int getAccidentForeing() {
        return accidentForeing;
    }

    /**
     * Imposta il valore della proprietà accidentForeing.
     * 
     */
    public void setAccidentForeing(int value) {
        this.accidentForeing = value;
    }

    /**
     * Recupera il valore della proprietà accidentInjureds.
     * 
     */
    public int getAccidentInjureds() {
        return accidentInjureds;
    }

    /**
     * Imposta il valore della proprietà accidentInjureds.
     * 
     */
    public void setAccidentInjureds(int value) {
        this.accidentInjureds = value;
    }

    /**
     * Recupera il valore della proprietà oldMotoPlate.
     * 
     */
    public int getOldMotoPlate() {
        return oldMotoPlate;
    }

    /**
     * Imposta il valore della proprietà oldMotoPlate.
     * 
     */
    public void setOldMotoPlate(int value) {
        this.oldMotoPlate = value;
    }

    /**
     * Recupera il valore della proprietà damageOtherVehicles.
     * 
     */
    public int getDamageOtherVehicles() {
        return damageOtherVehicles;
    }

    /**
     * Imposta il valore della proprietà damageOtherVehicles.
     * 
     */
    public void setDamageOtherVehicles(int value) {
        this.damageOtherVehicles = value;
    }

    /**
     * Recupera il valore della proprietà damageObject.
     * 
     */
    public int getDamageObject() {
        return damageObject;
    }

    /**
     * Imposta il valore della proprietà damageObject.
     * 
     */
    public void setDamageObject(int value) {
        this.damageObject = value;
    }

    /**
     * Recupera il valore della proprietà witnessesDescription.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWitnessesDescription() {
        return witnessesDescription;
    }

    /**
     * Imposta il valore della proprietà witnessesDescription.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWitnessesDescription(String value) {
        this.witnessesDescription = value;
    }

    /**
     * Recupera il valore della proprietà presenceAuth.
     * 
     */
    public int getPresenceAuth() {
        return presenceAuth;
    }

    /**
     * Imposta il valore della proprietà presenceAuth.
     * 
     */
    public void setPresenceAuth(int value) {
        this.presenceAuth = value;
    }

    /**
     * Recupera il valore della proprietà presencePolice.
     * 
     */
    public int getPresencePolice() {
        return presencePolice;
    }

    /**
     * Imposta il valore della proprietà presencePolice.
     * 
     */
    public void setPresencePolice(int value) {
        this.presencePolice = value;
    }

    /**
     * Recupera il valore della proprietà presenceCC.
     * 
     */
    public int getPresenceCC() {
        return presenceCC;
    }

    /**
     * Imposta il valore della proprietà presenceCC.
     * 
     */
    public void setPresenceCC(int value) {
        this.presenceCC = value;
    }

    /**
     * Recupera il valore della proprietà presenceVVUU.
     * 
     */
    public int getPresenceVVUU() {
        return presenceVVUU;
    }

    /**
     * Imposta il valore della proprietà presenceVVUU.
     * 
     */
    public void setPresenceVVUU(int value) {
        this.presenceVVUU = value;
    }

    /**
     * Recupera il valore della proprietà presenceAuthDes.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPresenceAuthDes() {
        return presenceAuthDes;
    }

    /**
     * Imposta il valore della proprietà presenceAuthDes.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPresenceAuthDes(String value) {
        this.presenceAuthDes = value;
    }

    /**
     * Recupera il valore della proprietà accidentType.
     * 
     */
    public int getAccidentType() {
        return accidentType;
    }

    /**
     * Imposta il valore della proprietà accidentType.
     * 
     */
    public void setAccidentType(int value) {
        this.accidentType = value;
    }

    /**
     * Recupera il valore della proprietà externalAccidentNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalAccidentNumber() {
        return externalAccidentNumber;
    }

    /**
     * Imposta il valore della proprietà externalAccidentNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalAccidentNumber(String value) {
        this.externalAccidentNumber = value;
    }

    /**
     * Recupera il valore della proprietà status.
     * 
     */
    public int getStatus() {
        return status;
    }

    /**
     * Imposta il valore della proprietà status.
     * 
     */
    public void setStatus(int value) {
        this.status = value;
    }

    /**
     * Recupera il valore della proprietà flowType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlowType() {
        return flowType;
    }

    /**
     * Imposta il valore della proprietà flowType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlowType(String value) {
        this.flowType = value;
    }

    /**
     * Recupera il valore della proprietà companyAccidentType.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCompanyAccidentType() {
        return companyAccidentType;
    }

    /**
     * Imposta il valore della proprietà companyAccidentType.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCompanyAccidentType(Integer value) {
        this.companyAccidentType = value;
    }

    /**
     * Recupera il valore della proprietà companyAccidentDef.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCompanyAccidentDef() {
        return companyAccidentDef;
    }

    /**
     * Imposta il valore della proprietà companyAccidentDef.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCompanyAccidentDef(XMLGregorianCalendar value) {
        this.companyAccidentDef = value;
    }

    /**
     * Recupera il valore della proprietà expertID.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getExpertID() {
        return expertID;
    }

    /**
     * Imposta il valore della proprietà expertID.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setExpertID(Integer value) {
        this.expertID = value;
    }

    /**
     * Recupera il valore della proprietà repairStatus.
     * 
     */
    public int getRepairStatus() {
        return repairStatus;
    }

    /**
     * Imposta il valore della proprietà repairStatus.
     * 
     */
    public void setRepairStatus(int value) {
        this.repairStatus = value;
    }

    /**
     * Recupera il valore della proprietà insuranceCompanyID.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getInsuranceCompanyID() {
        return insuranceCompanyID;
    }

    /**
     * Imposta il valore della proprietà insuranceCompanyID.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setInsuranceCompanyID(Integer value) {
        this.insuranceCompanyID = value;
    }

    /**
     * Recupera il valore della proprietà lawyerID.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getLawyerID() {
        return lawyerID;
    }

    /**
     * Imposta il valore della proprietà lawyerID.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setLawyerID(Integer value) {
        this.lawyerID = value;
    }

    /**
     * Recupera il valore della proprietà feturedDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFeturedDate() {
        return feturedDate;
    }

    /**
     * Imposta il valore della proprietà feturedDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFeturedDate(XMLGregorianCalendar value) {
        this.feturedDate = value;
    }

    /**
     * Recupera il valore della proprietà externalInspectorate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalInspectorate() {
        return externalInspectorate;
    }

    /**
     * Imposta il valore della proprietà externalInspectorate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalInspectorate(String value) {
        this.externalInspectorate = value;
    }

    /**
     * Recupera il valore della proprietà externalNote.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalNote() {
        return externalNote;
    }

    /**
     * Imposta il valore della proprietà externalNote.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalNote(String value) {
        this.externalNote = value;
    }

    /**
     * Recupera il valore della proprietà externalAccidentType.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getExternalAccidentType() {
        return externalAccidentType;
    }

    /**
     * Imposta il valore della proprietà externalAccidentType.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setExternalAccidentType(Integer value) {
        this.externalAccidentType = value;
    }

    /**
     * Recupera il valore della proprietà externalExpert.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalExpert() {
        return externalExpert;
    }

    /**
     * Imposta il valore della proprietà externalExpert.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalExpert(String value) {
        this.externalExpert = value;
    }

    /**
     * Recupera il valore della proprietà liqValue.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLiqValue() {
        return liqValue;
    }

    /**
     * Imposta il valore della proprietà liqValue.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLiqValue(BigDecimal value) {
        this.liqValue = value;
    }

    /**
     * Recupera il valore della proprietà liqDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLiqDate() {
        return liqDate;
    }

    /**
     * Imposta il valore della proprietà liqDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLiqDate(XMLGregorianCalendar value) {
        this.liqDate = value;
    }

    /**
     * Recupera il valore della proprietà liqNote.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLiqNote() {
        return liqNote;
    }

    /**
     * Imposta il valore della proprietà liqNote.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLiqNote(String value) {
        this.liqNote = value;
    }

    /**
     * Recupera il valore della proprietà liqWait.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLiqWait() {
        return liqWait;
    }

    /**
     * Imposta il valore della proprietà liqWait.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLiqWait(BigDecimal value) {
        this.liqWait = value;
    }

    /**
     * Recupera il valore della proprietà notyTypeID.
     * 
     */
    public int getNotyTypeID() {
        return notyTypeID;
    }

    /**
     * Imposta il valore della proprietà notyTypeID.
     * 
     */
    public void setNotyTypeID(int value) {
        this.notyTypeID = value;
    }

    /**
     * Recupera il valore della proprietà notRepairable.
     * 
     */
    public int getNotRepairable() {
        return notRepairable;
    }

    /**
     * Imposta il valore della proprietà notRepairable.
     * 
     */
    public void setNotRepairable(int value) {
        this.notRepairable = value;
    }

    /**
     * Recupera il valore della proprietà recoverable.
     * 
     */
    public int getRecoverable() {
        return recoverable;
    }

    /**
     * Imposta il valore della proprietà recoverable.
     * 
     */
    public void setRecoverable(int value) {
        this.recoverable = value;
    }

    /**
     * Recupera il valore della proprietà perRecoverable.
     * 
     */
    public int getPerRecoverable() {
        return perRecoverable;
    }

    /**
     * Imposta il valore della proprietà perRecoverable.
     * 
     */
    public void setPerRecoverable(int value) {
        this.perRecoverable = value;
    }

    /**
     * Recupera il valore della proprietà vehicleValue.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVehicleValue() {
        return vehicleValue;
    }

    /**
     * Imposta il valore della proprietà vehicleValue.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVehicleValue(BigDecimal value) {
        this.vehicleValue = value;
    }

    /**
     * Recupera il valore della proprietà thirdParies.
     * 
     */
    public int getThirdParies() {
        return thirdParies;
    }

    /**
     * Imposta il valore della proprietà thirdParies.
     * 
     */
    public void setThirdParies(int value) {
        this.thirdParies = value;
    }

    /**
     * Recupera il valore della proprietà repairLock.
     * 
     */
    public int getRepairLock() {
        return repairLock;
    }

    /**
     * Imposta il valore della proprietà repairLock.
     * 
     */
    public void setRepairLock(int value) {
        this.repairLock = value;
    }

    /**
     * Recupera il valore della proprietà reasonTypeID.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getReasonTypeID() {
        return reasonTypeID;
    }

    /**
     * Imposta il valore della proprietà reasonTypeID.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setReasonTypeID(Integer value) {
        this.reasonTypeID = value;
    }

    /**
     * Recupera il valore della proprietà reicevedDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getReicevedDate() {
        return reicevedDate;
    }

    /**
     * Imposta il valore della proprietà reicevedDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setReicevedDate(XMLGregorianCalendar value) {
        this.reicevedDate = value;
    }

    /**
     * Recupera il valore della proprietà excessLostKeys.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getExcessLostKeys() {
        return excessLostKeys;
    }

    /**
     * Imposta il valore della proprietà excessLostKeys.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setExcessLostKeys(BigDecimal value) {
        this.excessLostKeys = value;
    }

    /**
     * Recupera il valore della proprietà excessEvents.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getExcessEvents() {
        return excessEvents;
    }

    /**
     * Imposta il valore della proprietà excessEvents.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setExcessEvents(BigDecimal value) {
        this.excessEvents = value;
    }

    /**
     * Recupera il valore della proprietà commitDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCommitDate() {
        return commitDate;
    }

    /**
     * Imposta il valore della proprietà commitDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCommitDate(XMLGregorianCalendar value) {
        this.commitDate = value;
    }

    /**
     * Recupera il valore della proprietà commitTo.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCommitTo() {
        return commitTo;
    }

    /**
     * Imposta il valore della proprietà commitTo.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCommitTo(Integer value) {
        this.commitTo = value;
    }

    /**
     * Recupera il valore della proprietà insuranceManagerID.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getInsuranceManagerID() {
        return insuranceManagerID;
    }

    /**
     * Imposta il valore della proprietà insuranceManagerID.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setInsuranceManagerID(Integer value) {
        this.insuranceManagerID = value;
    }

    /**
     * Recupera il valore della proprietà insuranceBrokerID.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getInsuranceBrokerID() {
        return insuranceBrokerID;
    }

    /**
     * Imposta il valore della proprietà insuranceBrokerID.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setInsuranceBrokerID(Integer value) {
        this.insuranceBrokerID = value;
    }

    /**
     * Recupera il valore della proprietà insuranceAdjusterID.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getInsuranceAdjusterID() {
        return insuranceAdjusterID;
    }

    /**
     * Imposta il valore della proprietà insuranceAdjusterID.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setInsuranceAdjusterID(Integer value) {
        this.insuranceAdjusterID = value;
    }

    /**
     * Recupera il valore della proprietà offerConfirm.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOfferConfirm() {
        return offerConfirm;
    }

    /**
     * Imposta il valore della proprietà offerConfirm.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOfferConfirm(Integer value) {
        this.offerConfirm = value;
    }

    /**
     * Recupera il valore della proprietà repairerDes.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRepairerDes() {
        return repairerDes;
    }

    /**
     * Imposta il valore della proprietà repairerDes.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRepairerDes(String value) {
        this.repairerDes = value;
    }

    /**
     * Recupera il valore della proprietà accidentRegion.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAccidentRegion() {
        return accidentRegion;
    }

    /**
     * Imposta il valore della proprietà accidentRegion.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAccidentRegion(Integer value) {
        this.accidentRegion = value;
    }

    /**
     * Recupera il valore della proprietà repairIsChanged.
     * 
     */
    public int getRepairIsChanged() {
        return repairIsChanged;
    }

    /**
     * Imposta il valore della proprietà repairIsChanged.
     * 
     */
    public void setRepairIsChanged(int value) {
        this.repairIsChanged = value;
    }

    /**
     * Recupera il valore della proprietà repairLog.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRepairLog() {
        return repairLog;
    }

    /**
     * Imposta il valore della proprietà repairLog.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRepairLog(String value) {
        this.repairLog = value;
    }

    /**
     * Recupera il valore della proprietà antiTheftServiceID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAntiTheftServiceID() {
        return antiTheftServiceID;
    }

    /**
     * Imposta il valore della proprietà antiTheftServiceID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAntiTheftServiceID(String value) {
        this.antiTheftServiceID = value;
    }

    /**
     * Recupera il valore della proprietà antiTheftAlerted.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAntiTheftAlerted() {
        return antiTheftAlerted;
    }

    /**
     * Imposta il valore della proprietà antiTheftAlerted.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAntiTheftAlerted(Integer value) {
        this.antiTheftAlerted = value;
    }

    /**
     * Recupera il valore della proprietà theftOnBranch.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTheftOnBranch() {
        return theftOnBranch;
    }

    /**
     * Imposta il valore della proprietà theftOnBranch.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTheftOnBranch(Integer value) {
        this.theftOnBranch = value;
    }

    /**
     * Recupera il valore della proprietà theftOnBranchCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTheftOnBranchCode() {
        return theftOnBranchCode;
    }

    /**
     * Imposta il valore della proprietà theftOnBranchCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTheftOnBranchCode(String value) {
        this.theftOnBranchCode = value;
    }

    /**
     * Recupera il valore della proprietà findDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFindDate() {
        return findDate;
    }

    /**
     * Imposta il valore della proprietà findDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFindDate(XMLGregorianCalendar value) {
        this.findDate = value;
    }

    /**
     * Recupera il valore della proprietà findHour.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFindHour() {
        return findHour;
    }

    /**
     * Imposta il valore della proprietà findHour.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFindHour(String value) {
        this.findHour = value;
    }

    /**
     * Recupera il valore della proprietà findForeing.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getFindForeing() {
        return findForeing;
    }

    /**
     * Imposta il valore della proprietà findForeing.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFindForeing(Integer value) {
        this.findForeing = value;
    }

    /**
     * Recupera il valore della proprietà findCity.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFindCity() {
        return findCity;
    }

    /**
     * Imposta il valore della proprietà findCity.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFindCity(String value) {
        this.findCity = value;
    }

    /**
     * Recupera il valore della proprietà findProvince.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFindProvince() {
        return findProvince;
    }

    /**
     * Imposta il valore della proprietà findProvince.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFindProvince(String value) {
        this.findProvince = value;
    }

    /**
     * Recupera il valore della proprietà findAddress.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFindAddress() {
        return findAddress;
    }

    /**
     * Imposta il valore della proprietà findAddress.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFindAddress(String value) {
        this.findAddress = value;
    }

    /**
     * Recupera il valore della proprietà findAuthPolice.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getFindAuthPolice() {
        return findAuthPolice;
    }

    /**
     * Imposta il valore della proprietà findAuthPolice.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFindAuthPolice(Integer value) {
        this.findAuthPolice = value;
    }

    /**
     * Recupera il valore della proprietà findAuthCC.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getFindAuthCC() {
        return findAuthCC;
    }

    /**
     * Imposta il valore della proprietà findAuthCC.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFindAuthCC(Integer value) {
        this.findAuthCC = value;
    }

    /**
     * Recupera il valore della proprietà findAuthVVUU.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getFindAuthVVUU() {
        return findAuthVVUU;
    }

    /**
     * Imposta il valore della proprietà findAuthVVUU.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFindAuthVVUU(Integer value) {
        this.findAuthVVUU = value;
    }

    /**
     * Recupera il valore della proprietà findAuthDes.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFindAuthDes() {
        return findAuthDes;
    }

    /**
     * Imposta il valore della proprietà findAuthDes.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFindAuthDes(String value) {
        this.findAuthDes = value;
    }

    /**
     * Recupera il valore della proprietà findAuthPhone.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFindAuthPhone() {
        return findAuthPhone;
    }

    /**
     * Imposta il valore della proprietà findAuthPhone.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFindAuthPhone(String value) {
        this.findAuthPhone = value;
    }

    /**
     * Recupera il valore della proprietà findAuthSeized.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getFindAuthSeized() {
        return findAuthSeized;
    }

    /**
     * Imposta il valore della proprietà findAuthSeized.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFindAuthSeized(Integer value) {
        this.findAuthSeized = value;
    }

    /**
     * Recupera il valore della proprietà isRobbery.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIsRobbery() {
        return isRobbery;
    }

    /**
     * Imposta il valore della proprietà isRobbery.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIsRobbery(Integer value) {
        this.isRobbery = value;
    }

    /**
     * Recupera il valore della proprietà isKeysManage.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIsKeysManage() {
        return isKeysManage;
    }

    /**
     * Imposta il valore della proprietà isKeysManage.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIsKeysManage(Integer value) {
        this.isKeysManage = value;
    }

    /**
     * Recupera il valore della proprietà isAccountingManage.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIsAccountingManage() {
        return isAccountingManage;
    }

    /**
     * Imposta il valore della proprietà isAccountingManage.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIsAccountingManage(Integer value) {
        this.isAccountingManage = value;
    }

    /**
     * Recupera il valore della proprietà isCkAdminPosition.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIsCkAdminPosition() {
        return isCkAdminPosition;
    }

    /**
     * Imposta il valore della proprietà isCkAdminPosition.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIsCkAdminPosition(Integer value) {
        this.isCkAdminPosition = value;
    }

    /**
     * Recupera il valore della proprietà rxFirstKey.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRxFirstKey() {
        return rxFirstKey;
    }

    /**
     * Imposta il valore della proprietà rxFirstKey.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRxFirstKey(XMLGregorianCalendar value) {
        this.rxFirstKey = value;
    }

    /**
     * Recupera il valore della proprietà rxSecondKey.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRxSecondKey() {
        return rxSecondKey;
    }

    /**
     * Imposta il valore della proprietà rxSecondKey.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRxSecondKey(XMLGregorianCalendar value) {
        this.rxSecondKey = value;
    }

    /**
     * Recupera il valore della proprietà rxOriComplaint.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRxOriComplaint() {
        return rxOriComplaint;
    }

    /**
     * Imposta il valore della proprietà rxOriComplaint.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRxOriComplaint(XMLGregorianCalendar value) {
        this.rxOriComplaint = value;
    }

    /**
     * Recupera il valore della proprietà rxCpyComplaint.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRxCpyComplaint() {
        return rxCpyComplaint;
    }

    /**
     * Imposta il valore della proprietà rxCpyComplaint.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRxCpyComplaint(XMLGregorianCalendar value) {
        this.rxCpyComplaint = value;
    }

    /**
     * Recupera il valore della proprietà rxOriVerbFind.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRxOriVerbFind() {
        return rxOriVerbFind;
    }

    /**
     * Imposta il valore della proprietà rxOriVerbFind.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRxOriVerbFind(XMLGregorianCalendar value) {
        this.rxOriVerbFind = value;
    }

    /**
     * Recupera il valore della proprietà rxCpyVerbFind.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRxCpyVerbFind() {
        return rxCpyVerbFind;
    }

    /**
     * Imposta il valore della proprietà rxCpyVerbFind.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRxCpyVerbFind(XMLGregorianCalendar value) {
        this.rxCpyVerbFind = value;
    }

    /**
     * Recupera il valore della proprietà reqLoss.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getReqLoss() {
        return reqLoss;
    }

    /**
     * Imposta il valore della proprietà reqLoss.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setReqLoss(XMLGregorianCalendar value) {
        this.reqLoss = value;
    }

    /**
     * Recupera il valore della proprietà rxReqLoss.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRxReqLoss() {
        return rxReqLoss;
    }

    /**
     * Imposta il valore della proprietà rxReqLoss.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRxReqLoss(XMLGregorianCalendar value) {
        this.rxReqLoss = value;
    }

    /**
     * Recupera il valore della proprietà reqRepossession.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getReqRepossession() {
        return reqRepossession;
    }

    /**
     * Imposta il valore della proprietà reqRepossession.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setReqRepossession(XMLGregorianCalendar value) {
        this.reqRepossession = value;
    }

    /**
     * Recupera il valore della proprietà rxReqRepossession.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRxReqRepossession() {
        return rxReqRepossession;
    }

    /**
     * Imposta il valore della proprietà rxReqRepossession.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRxReqRepossession(XMLGregorianCalendar value) {
        this.rxReqRepossession = value;
    }

    /**
     * Recupera il valore della proprietà findSPointEmail.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFindSPointEmail() {
        return findSPointEmail;
    }

    /**
     * Imposta il valore della proprietà findSPointEmail.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFindSPointEmail(String value) {
        this.findSPointEmail = value;
    }

    /**
     * Recupera il valore della proprietà isSummons.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIsSummons() {
        return isSummons;
    }

    /**
     * Imposta il valore della proprietà isSummons.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIsSummons(Integer value) {
        this.isSummons = value;
    }

    /**
     * Recupera il valore della proprietà toNotified.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getToNotified() {
        return toNotified;
    }

    /**
     * Imposta il valore della proprietà toNotified.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setToNotified(Integer value) {
        this.toNotified = value;
    }

    /**
     * Recupera il valore della proprietà lastNotification.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLastNotification() {
        return lastNotification;
    }

    /**
     * Imposta il valore della proprietà lastNotification.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLastNotification(XMLGregorianCalendar value) {
        this.lastNotification = value;
    }

    /**
     * Recupera il valore della proprietà paiInsurancePolicyID.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPaiInsurancePolicyID() {
        return paiInsurancePolicyID;
    }

    /**
     * Imposta il valore della proprietà paiInsurancePolicyID.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPaiInsurancePolicyID(Integer value) {
        this.paiInsurancePolicyID = value;
    }

    /**
     * Recupera il valore della proprietà paiCommitDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPaiCommitDate() {
        return paiCommitDate;
    }

    /**
     * Imposta il valore della proprietà paiCommitDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPaiCommitDate(XMLGregorianCalendar value) {
        this.paiCommitDate = value;
    }

    /**
     * Recupera il valore della proprietà reasonsDesc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReasonsDesc() {
        return reasonsDesc;
    }

    /**
     * Imposta il valore della proprietà reasonsDesc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReasonsDesc(String value) {
        this.reasonsDesc = value;
    }

    /**
     * Recupera il valore della proprietà antiTheftActivate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAntiTheftActivate() {
        return antiTheftActivate;
    }

    /**
     * Imposta il valore della proprietà antiTheftActivate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAntiTheftActivate(XMLGregorianCalendar value) {
        this.antiTheftActivate = value;
    }

    /**
     * Recupera il valore della proprietà theftDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTheftDate() {
        return theftDate;
    }

    /**
     * Imposta il valore della proprietà theftDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTheftDate(XMLGregorianCalendar value) {
        this.theftDate = value;
    }

    /**
     * Recupera il valore della proprietà spCenterID.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSPCenterID() {
        return spCenterID;
    }

    /**
     * Imposta il valore della proprietà spCenterID.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSPCenterID(Integer value) {
        this.spCenterID = value;
    }

    /**
     * Recupera il valore della proprietà presenceAuthPhone.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPresenceAuthPhone() {
        return presenceAuthPhone;
    }

    /**
     * Imposta il valore della proprietà presenceAuthPhone.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPresenceAuthPhone(String value) {
        this.presenceAuthPhone = value;
    }

    /**
     * Recupera il valore della proprietà findVehicleCO.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFindVehicleCO() {
        return findVehicleCO;
    }

    /**
     * Imposta il valore della proprietà findVehicleCO.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFindVehicleCO(String value) {
        this.findVehicleCO = value;
    }

    /**
     * Recupera il valore della proprietà payType1.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPayType1() {
        return payType1;
    }

    /**
     * Imposta il valore della proprietà payType1.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPayType1(Integer value) {
        this.payType1 = value;
    }

    /**
     * Recupera il valore della proprietà payDate1.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPayDate1() {
        return payDate1;
    }

    /**
     * Imposta il valore della proprietà payDate1.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPayDate1(XMLGregorianCalendar value) {
        this.payDate1 = value;
    }

    /**
     * Recupera il valore della proprietà payLawyerCost1.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPayLawyerCost1() {
        return payLawyerCost1;
    }

    /**
     * Imposta il valore della proprietà payLawyerCost1.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPayLawyerCost1(BigDecimal value) {
        this.payLawyerCost1 = value;
    }

    /**
     * Recupera il valore della proprietà payDamageValue1.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPayDamageValue1() {
        return payDamageValue1;
    }

    /**
     * Imposta il valore della proprietà payDamageValue1.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPayDamageValue1(BigDecimal value) {
        this.payDamageValue1 = value;
    }

    /**
     * Recupera il valore della proprietà payTechBlock1.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPayTechBlock1() {
        return payTechBlock1;
    }

    /**
     * Imposta il valore della proprietà payTechBlock1.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPayTechBlock1(BigDecimal value) {
        this.payTechBlock1 = value;
    }

    /**
     * Recupera il valore della proprietà payType2.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPayType2() {
        return payType2;
    }

    /**
     * Imposta il valore della proprietà payType2.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPayType2(Integer value) {
        this.payType2 = value;
    }

    /**
     * Recupera il valore della proprietà payDate2.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPayDate2() {
        return payDate2;
    }

    /**
     * Imposta il valore della proprietà payDate2.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPayDate2(XMLGregorianCalendar value) {
        this.payDate2 = value;
    }

    /**
     * Recupera il valore della proprietà payLawyerCost2.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPayLawyerCost2() {
        return payLawyerCost2;
    }

    /**
     * Imposta il valore della proprietà payLawyerCost2.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPayLawyerCost2(BigDecimal value) {
        this.payLawyerCost2 = value;
    }

    /**
     * Recupera il valore della proprietà payDamageValue2.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPayDamageValue2() {
        return payDamageValue2;
    }

    /**
     * Imposta il valore della proprietà payDamageValue2.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPayDamageValue2(BigDecimal value) {
        this.payDamageValue2 = value;
    }

    /**
     * Recupera il valore della proprietà payTechBlock2.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPayTechBlock2() {
        return payTechBlock2;
    }

    /**
     * Imposta il valore della proprietà payTechBlock2.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPayTechBlock2(BigDecimal value) {
        this.payTechBlock2 = value;
    }

    /**
     * Recupera il valore della proprietà payType3.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPayType3() {
        return payType3;
    }

    /**
     * Imposta il valore della proprietà payType3.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPayType3(Integer value) {
        this.payType3 = value;
    }

    /**
     * Recupera il valore della proprietà payDate3.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPayDate3() {
        return payDate3;
    }

    /**
     * Imposta il valore della proprietà payDate3.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPayDate3(XMLGregorianCalendar value) {
        this.payDate3 = value;
    }

    /**
     * Recupera il valore della proprietà payLawyerCost3.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPayLawyerCost3() {
        return payLawyerCost3;
    }

    /**
     * Imposta il valore della proprietà payLawyerCost3.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPayLawyerCost3(BigDecimal value) {
        this.payLawyerCost3 = value;
    }

    /**
     * Recupera il valore della proprietà payDamageValue3.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPayDamageValue3() {
        return payDamageValue3;
    }

    /**
     * Imposta il valore della proprietà payDamageValue3.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPayDamageValue3(BigDecimal value) {
        this.payDamageValue3 = value;
    }

    /**
     * Recupera il valore della proprietà payTechBlock3.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPayTechBlock3() {
        return payTechBlock3;
    }

    /**
     * Imposta il valore della proprietà payTechBlock3.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPayTechBlock3(BigDecimal value) {
        this.payTechBlock3 = value;
    }

    /**
     * Recupera il valore della proprietà payTot1.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPayTot1() {
        return payTot1;
    }

    /**
     * Imposta il valore della proprietà payTot1.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPayTot1(BigDecimal value) {
        this.payTot1 = value;
    }

    /**
     * Recupera il valore della proprietà payTot2.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPayTot2() {
        return payTot2;
    }

    /**
     * Imposta il valore della proprietà payTot2.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPayTot2(BigDecimal value) {
        this.payTot2 = value;
    }

    /**
     * Recupera il valore della proprietà payTot3.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPayTot3() {
        return payTot3;
    }

    /**
     * Imposta il valore della proprietà payTot3.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPayTot3(BigDecimal value) {
        this.payTot3 = value;
    }

    /**
     * Recupera il valore della proprietà residualValue.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getResidualValue() {
        return residualValue;
    }

    /**
     * Imposta il valore della proprietà residualValue.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setResidualValue(BigDecimal value) {
        this.residualValue = value;
    }

    /**
     * Recupera il valore della proprietà deductibleAmount.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDeductibleAmount() {
        return deductibleAmount;
    }

    /**
     * Imposta il valore della proprietà deductibleAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDeductibleAmount(BigDecimal value) {
        this.deductibleAmount = value;
    }

    /**
     * Recupera il valore della proprietà payAmount1.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPayAmount1() {
        return payAmount1;
    }

    /**
     * Imposta il valore della proprietà payAmount1.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPayAmount1(BigDecimal value) {
        this.payAmount1 = value;
    }

    /**
     * Recupera il valore della proprietà payAmount2.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPayAmount2() {
        return payAmount2;
    }

    /**
     * Imposta il valore della proprietà payAmount2.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPayAmount2(BigDecimal value) {
        this.payAmount2 = value;
    }

    /**
     * Recupera il valore della proprietà payAmount3.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPayAmount3() {
        return payAmount3;
    }

    /**
     * Imposta il valore della proprietà payAmount3.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPayAmount3(BigDecimal value) {
        this.payAmount3 = value;
    }

    /**
     * Recupera il valore della proprietà isEndOfLeasing.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIsEndOfLeasing() {
        return isEndOfLeasing;
    }

    /**
     * Imposta il valore della proprietà isEndOfLeasing.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIsEndOfLeasing(Integer value) {
        this.isEndOfLeasing = value;
    }

    /**
     * Recupera il valore della proprietà deductEnd1.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDeductEnd1() {
        return deductEnd1;
    }

    /**
     * Imposta il valore della proprietà deductEnd1.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDeductEnd1(XMLGregorianCalendar value) {
        this.deductEnd1 = value;
    }

    /**
     * Recupera il valore della proprietà deductPaid1.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDeductPaid1() {
        return deductPaid1;
    }

    /**
     * Imposta il valore della proprietà deductPaid1.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDeductPaid1(BigDecimal value) {
        this.deductPaid1 = value;
    }

    /**
     * Recupera il valore della proprietà deductImported1.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDeductImported1() {
        return deductImported1;
    }

    /**
     * Imposta il valore della proprietà deductImported1.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDeductImported1(XMLGregorianCalendar value) {
        this.deductImported1 = value;
    }

    /**
     * Recupera il valore della proprietà deductEnd2.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDeductEnd2() {
        return deductEnd2;
    }

    /**
     * Imposta il valore della proprietà deductEnd2.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDeductEnd2(XMLGregorianCalendar value) {
        this.deductEnd2 = value;
    }

    /**
     * Recupera il valore della proprietà deductPaid2.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDeductPaid2() {
        return deductPaid2;
    }

    /**
     * Imposta il valore della proprietà deductPaid2.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDeductPaid2(BigDecimal value) {
        this.deductPaid2 = value;
    }

    /**
     * Recupera il valore della proprietà deductImported2.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDeductImported2() {
        return deductImported2;
    }

    /**
     * Imposta il valore della proprietà deductImported2.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDeductImported2(XMLGregorianCalendar value) {
        this.deductImported2 = value;
    }

    /**
     * Recupera il valore della proprietà deductTotalPaid.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDeductTotalPaid() {
        return deductTotalPaid;
    }

    /**
     * Imposta il valore della proprietà deductTotalPaid.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDeductTotalPaid(BigDecimal value) {
        this.deductTotalPaid = value;
    }

    /**
     * Recupera il valore della proprietà isWitnessConfirm.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIsWitnessConfirm() {
        return isWitnessConfirm;
    }

    /**
     * Imposta il valore della proprietà isWitnessConfirm.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIsWitnessConfirm(Integer value) {
        this.isWitnessConfirm = value;
    }

    /**
     * Recupera il valore della proprietà atfLastCheck.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAtfLastCheck() {
        return atfLastCheck;
    }

    /**
     * Imposta il valore della proprietà atfLastCheck.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAtfLastCheck(XMLGregorianCalendar value) {
        this.atfLastCheck = value;
    }

    /**
     * Recupera il valore della proprietà atfResResult.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAtfResResult() {
        return atfResResult;
    }

    /**
     * Imposta il valore della proprietà atfResResult.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAtfResResult(String value) {
        this.atfResResult = value;
    }

    /**
     * Recupera il valore della proprietà atfVoucherID.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAtfVoucherID() {
        return atfVoucherID;
    }

    /**
     * Imposta il valore della proprietà atfVoucherID.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAtfVoucherID(Integer value) {
        this.atfVoucherID = value;
    }

    /**
     * Recupera il valore della proprietà atfSrvProvider.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAtfSrvProvider() {
        return atfSrvProvider;
    }

    /**
     * Imposta il valore della proprietà atfSrvProvider.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAtfSrvProvider(Integer value) {
        this.atfSrvProvider = value;
    }

    /**
     * Recupera il valore della proprietà atfResCode.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAtfResCode() {
        return atfResCode;
    }

    /**
     * Imposta il valore della proprietà atfResCode.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAtfResCode(Integer value) {
        this.atfResCode = value;
    }

    /**
     * Recupera il valore della proprietà atfResMessage.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAtfResMessage() {
        return atfResMessage;
    }

    /**
     * Imposta il valore della proprietà atfResMessage.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAtfResMessage(String value) {
        this.atfResMessage = value;
    }

    /**
     * Recupera il valore della proprietà atfResRepNum.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAtfResRepNum() {
        return atfResRepNum;
    }

    /**
     * Imposta il valore della proprietà atfResRepNum.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAtfResRepNum(Integer value) {
        this.atfResRepNum = value;
    }

    /**
     * Recupera il valore della proprietà atfResGForce.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAtfResGForce() {
        return atfResGForce;
    }

    /**
     * Imposta il valore della proprietà atfResGForce.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAtfResGForce(String value) {
        this.atfResGForce = value;
    }

    /**
     * Recupera il valore della proprietà atfResNumCrash.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAtfResNumCrash() {
        return atfResNumCrash;
    }

    /**
     * Imposta il valore della proprietà atfResNumCrash.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAtfResNumCrash(Integer value) {
        this.atfResNumCrash = value;
    }

    /**
     * Recupera il valore della proprietà atfResAnomaly.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAtfResAnomaly() {
        return atfResAnomaly;
    }

    /**
     * Imposta il valore della proprietà atfResAnomaly.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAtfResAnomaly(Integer value) {
        this.atfResAnomaly = value;
    }

    /**
     * Recupera il valore della proprietà isReRegistration.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIsReRegistration() {
        return isReRegistration;
    }

    /**
     * Imposta il valore della proprietà isReRegistration.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIsReRegistration(Integer value) {
        this.isReRegistration = value;
    }

    /**
     * Recupera il valore della proprietà reRegistrationReq.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getReRegistrationReq() {
        return reRegistrationReq;
    }

    /**
     * Imposta il valore della proprietà reRegistrationReq.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setReRegistrationReq(XMLGregorianCalendar value) {
        this.reRegistrationReq = value;
    }

    /**
     * Recupera il valore della proprietà reRegistration.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getReRegistration() {
        return reRegistration;
    }

    /**
     * Imposta il valore della proprietà reRegistration.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setReRegistration(XMLGregorianCalendar value) {
        this.reRegistration = value;
    }

    /**
     * Recupera il valore della proprietà crmCaseReReg.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCRMCaseReReg() {
        return crmCaseReReg;
    }

    /**
     * Imposta il valore della proprietà crmCaseReReg.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCRMCaseReReg(String value) {
        this.crmCaseReReg = value;
    }

    /**
     * Recupera il valore della proprietà isDuplRegistration.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIsDuplRegistration() {
        return isDuplRegistration;
    }

    /**
     * Imposta il valore della proprietà isDuplRegistration.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIsDuplRegistration(Integer value) {
        this.isDuplRegistration = value;
    }

    /**
     * Recupera il valore della proprietà duplRegistrationReq.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDuplRegistrationReq() {
        return duplRegistrationReq;
    }

    /**
     * Imposta il valore della proprietà duplRegistrationReq.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDuplRegistrationReq(XMLGregorianCalendar value) {
        this.duplRegistrationReq = value;
    }

    /**
     * Recupera il valore della proprietà duplRegistration.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDuplRegistration() {
        return duplRegistration;
    }

    /**
     * Imposta il valore della proprietà duplRegistration.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDuplRegistration(XMLGregorianCalendar value) {
        this.duplRegistration = value;
    }

    /**
     * Recupera il valore della proprietà crmCaseDuplReg.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCRMCaseDuplReg() {
        return crmCaseDuplReg;
    }

    /**
     * Imposta il valore della proprietà crmCaseDuplReg.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCRMCaseDuplReg(String value) {
        this.crmCaseDuplReg = value;
    }

    /**
     * Recupera il valore della proprietà isReVIN.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIsReVIN() {
        return isReVIN;
    }

    /**
     * Imposta il valore della proprietà isReVIN.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIsReVIN(Integer value) {
        this.isReVIN = value;
    }

    /**
     * Recupera il valore della proprietà reVINReq.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getReVINReq() {
        return reVINReq;
    }

    /**
     * Imposta il valore della proprietà reVINReq.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setReVINReq(XMLGregorianCalendar value) {
        this.reVINReq = value;
    }

    /**
     * Recupera il valore della proprietà reVIN.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getReVIN() {
        return reVIN;
    }

    /**
     * Imposta il valore della proprietà reVIN.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setReVIN(XMLGregorianCalendar value) {
        this.reVIN = value;
    }

    /**
     * Recupera il valore della proprietà crmCaseReVIN.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCRMCaseReVIN() {
        return crmCaseReVIN;
    }

    /**
     * Imposta il valore della proprietà crmCaseReVIN.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCRMCaseReVIN(String value) {
        this.crmCaseReVIN = value;
    }

    /**
     * Recupera il valore della proprietà isDemolition.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIsDemolition() {
        return isDemolition;
    }

    /**
     * Imposta il valore della proprietà isDemolition.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIsDemolition(Integer value) {
        this.isDemolition = value;
    }

    /**
     * Recupera il valore della proprietà demolitionReq.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDemolitionReq() {
        return demolitionReq;
    }

    /**
     * Imposta il valore della proprietà demolitionReq.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDemolitionReq(XMLGregorianCalendar value) {
        this.demolitionReq = value;
    }

    /**
     * Recupera il valore della proprietà demolition.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDemolition() {
        return demolition;
    }

    /**
     * Imposta il valore della proprietà demolition.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDemolition(XMLGregorianCalendar value) {
        this.demolition = value;
    }

    /**
     * Recupera il valore della proprietà crmCaseDemolition.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCRMCaseDemolition() {
        return crmCaseDemolition;
    }

    /**
     * Imposta il valore della proprietà crmCaseDemolition.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCRMCaseDemolition(String value) {
        this.crmCaseDemolition = value;
    }

    /**
     * Recupera il valore della proprietà seizureID.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSeizureID() {
        return seizureID;
    }

    /**
     * Imposta il valore della proprietà seizureID.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSeizureID(Integer value) {
        this.seizureID = value;
    }

}
