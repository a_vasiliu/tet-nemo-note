
package com.doing.nemo.claims.websin.websinws;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java per MigrationRepairDossier complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="MigrationRepairDossier">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WtyInfoCarPlate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WtyInfoEstimate" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="WtyInfoKm" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="WtyInfoVehicleDes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="WtyCreated" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="WtyNetworkOperator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WtyTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WtyTypeDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WtyPONumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WtyPOAmount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="WtyAuthDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="WtyNoAuthDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="WtySupplierVat" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WtySupplierName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WtySupplierAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WtySupplierZipCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WtySupplierCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WtySupplierProv" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WtySupplierPhone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WtySupplierFax" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WtySupplierEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WtySupplierCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WtyTotalLoss" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="WtyTotalLossNote" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FR_Numcharge" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MigrationRepairDossier", propOrder = {
    "wtyInfoCarPlate",
    "wtyInfoEstimate",
    "wtyInfoKm",
    "wtyInfoVehicleDes",
    "id",
    "wtyCreated",
    "wtyNetworkOperator",
    "wtyTypeCode",
    "wtyTypeDescription",
    "wtyPONumber",
    "wtyPOAmount",
    "wtyAuthDate",
    "wtyNoAuthDate",
    "wtySupplierVat",
    "wtySupplierName",
    "wtySupplierAddress",
    "wtySupplierZipCode",
    "wtySupplierCity",
    "wtySupplierProv",
    "wtySupplierPhone",
    "wtySupplierFax",
    "wtySupplierEmail",
    "wtySupplierCode",
    "wtyTotalLoss",
    "wtyTotalLossNote",
    "frNumcharge"
})
public class MigrationRepairDossier
    implements Serializable
{

    @XmlElement(name = "WtyInfoCarPlate")
    protected String wtyInfoCarPlate;
    @XmlElement(name = "WtyInfoEstimate", required = true, nillable = true)
    protected BigDecimal wtyInfoEstimate;
    @XmlElement(name = "WtyInfoKm", required = true, type = Integer.class, nillable = true)
    protected Integer wtyInfoKm;
    @XmlElement(name = "WtyInfoVehicleDes")
    protected String wtyInfoVehicleDes;
    @XmlElement(name = "ID")
    protected int id;
    @XmlElement(name = "WtyCreated", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar wtyCreated;
    @XmlElement(name = "WtyNetworkOperator")
    protected String wtyNetworkOperator;
    @XmlElement(name = "WtyTypeCode")
    protected String wtyTypeCode;
    @XmlElement(name = "WtyTypeDescription")
    protected String wtyTypeDescription;
    @XmlElement(name = "WtyPONumber")
    protected String wtyPONumber;
    @XmlElement(name = "WtyPOAmount", required = true, nillable = true)
    protected BigDecimal wtyPOAmount;
    @XmlElement(name = "WtyAuthDate", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar wtyAuthDate;
    @XmlElement(name = "WtyNoAuthDate", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar wtyNoAuthDate;
    @XmlElement(name = "WtySupplierVat")
    protected String wtySupplierVat;
    @XmlElement(name = "WtySupplierName")
    protected String wtySupplierName;
    @XmlElement(name = "WtySupplierAddress")
    protected String wtySupplierAddress;
    @XmlElement(name = "WtySupplierZipCode")
    protected String wtySupplierZipCode;
    @XmlElement(name = "WtySupplierCity")
    protected String wtySupplierCity;
    @XmlElement(name = "WtySupplierProv")
    protected String wtySupplierProv;
    @XmlElement(name = "WtySupplierPhone")
    protected String wtySupplierPhone;
    @XmlElement(name = "WtySupplierFax")
    protected String wtySupplierFax;
    @XmlElement(name = "WtySupplierEmail")
    protected String wtySupplierEmail;
    @XmlElement(name = "WtySupplierCode")
    protected String wtySupplierCode;
    @XmlElement(name = "WtyTotalLoss", required = true, type = Integer.class, nillable = true)
    protected Integer wtyTotalLoss;
    @XmlElement(name = "WtyTotalLossNote")
    protected String wtyTotalLossNote;
    @XmlElement(name = "FR_Numcharge")
    protected int frNumcharge;

    /**
     * Recupera il valore della proprietà wtyInfoCarPlate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWtyInfoCarPlate() {
        return wtyInfoCarPlate;
    }

    /**
     * Imposta il valore della proprietà wtyInfoCarPlate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWtyInfoCarPlate(String value) {
        this.wtyInfoCarPlate = value;
    }

    /**
     * Recupera il valore della proprietà wtyInfoEstimate.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getWtyInfoEstimate() {
        return wtyInfoEstimate;
    }

    /**
     * Imposta il valore della proprietà wtyInfoEstimate.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setWtyInfoEstimate(BigDecimal value) {
        this.wtyInfoEstimate = value;
    }

    /**
     * Recupera il valore della proprietà wtyInfoKm.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getWtyInfoKm() {
        return wtyInfoKm;
    }

    /**
     * Imposta il valore della proprietà wtyInfoKm.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setWtyInfoKm(Integer value) {
        this.wtyInfoKm = value;
    }

    /**
     * Recupera il valore della proprietà wtyInfoVehicleDes.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWtyInfoVehicleDes() {
        return wtyInfoVehicleDes;
    }

    /**
     * Imposta il valore della proprietà wtyInfoVehicleDes.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWtyInfoVehicleDes(String value) {
        this.wtyInfoVehicleDes = value;
    }

    /**
     * Recupera il valore della proprietà id.
     * 
     */
    public int getID() {
        return id;
    }

    /**
     * Imposta il valore della proprietà id.
     * 
     */
    public void setID(int value) {
        this.id = value;
    }

    /**
     * Recupera il valore della proprietà wtyCreated.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getWtyCreated() {
        return wtyCreated;
    }

    /**
     * Imposta il valore della proprietà wtyCreated.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setWtyCreated(XMLGregorianCalendar value) {
        this.wtyCreated = value;
    }

    /**
     * Recupera il valore della proprietà wtyNetworkOperator.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWtyNetworkOperator() {
        return wtyNetworkOperator;
    }

    /**
     * Imposta il valore della proprietà wtyNetworkOperator.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWtyNetworkOperator(String value) {
        this.wtyNetworkOperator = value;
    }

    /**
     * Recupera il valore della proprietà wtyTypeCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWtyTypeCode() {
        return wtyTypeCode;
    }

    /**
     * Imposta il valore della proprietà wtyTypeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWtyTypeCode(String value) {
        this.wtyTypeCode = value;
    }

    /**
     * Recupera il valore della proprietà wtyTypeDescription.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWtyTypeDescription() {
        return wtyTypeDescription;
    }

    /**
     * Imposta il valore della proprietà wtyTypeDescription.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWtyTypeDescription(String value) {
        this.wtyTypeDescription = value;
    }

    /**
     * Recupera il valore della proprietà wtyPONumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWtyPONumber() {
        return wtyPONumber;
    }

    /**
     * Imposta il valore della proprietà wtyPONumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWtyPONumber(String value) {
        this.wtyPONumber = value;
    }

    /**
     * Recupera il valore della proprietà wtyPOAmount.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getWtyPOAmount() {
        return wtyPOAmount;
    }

    /**
     * Imposta il valore della proprietà wtyPOAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setWtyPOAmount(BigDecimal value) {
        this.wtyPOAmount = value;
    }

    /**
     * Recupera il valore della proprietà wtyAuthDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getWtyAuthDate() {
        return wtyAuthDate;
    }

    /**
     * Imposta il valore della proprietà wtyAuthDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setWtyAuthDate(XMLGregorianCalendar value) {
        this.wtyAuthDate = value;
    }

    /**
     * Recupera il valore della proprietà wtyNoAuthDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getWtyNoAuthDate() {
        return wtyNoAuthDate;
    }

    /**
     * Imposta il valore della proprietà wtyNoAuthDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setWtyNoAuthDate(XMLGregorianCalendar value) {
        this.wtyNoAuthDate = value;
    }

    /**
     * Recupera il valore della proprietà wtySupplierVat.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWtySupplierVat() {
        return wtySupplierVat;
    }

    /**
     * Imposta il valore della proprietà wtySupplierVat.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWtySupplierVat(String value) {
        this.wtySupplierVat = value;
    }

    /**
     * Recupera il valore della proprietà wtySupplierName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWtySupplierName() {
        return wtySupplierName;
    }

    /**
     * Imposta il valore della proprietà wtySupplierName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWtySupplierName(String value) {
        this.wtySupplierName = value;
    }

    /**
     * Recupera il valore della proprietà wtySupplierAddress.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWtySupplierAddress() {
        return wtySupplierAddress;
    }

    /**
     * Imposta il valore della proprietà wtySupplierAddress.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWtySupplierAddress(String value) {
        this.wtySupplierAddress = value;
    }

    /**
     * Recupera il valore della proprietà wtySupplierZipCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWtySupplierZipCode() {
        return wtySupplierZipCode;
    }

    /**
     * Imposta il valore della proprietà wtySupplierZipCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWtySupplierZipCode(String value) {
        this.wtySupplierZipCode = value;
    }

    /**
     * Recupera il valore della proprietà wtySupplierCity.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWtySupplierCity() {
        return wtySupplierCity;
    }

    /**
     * Imposta il valore della proprietà wtySupplierCity.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWtySupplierCity(String value) {
        this.wtySupplierCity = value;
    }

    /**
     * Recupera il valore della proprietà wtySupplierProv.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWtySupplierProv() {
        return wtySupplierProv;
    }

    /**
     * Imposta il valore della proprietà wtySupplierProv.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWtySupplierProv(String value) {
        this.wtySupplierProv = value;
    }

    /**
     * Recupera il valore della proprietà wtySupplierPhone.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWtySupplierPhone() {
        return wtySupplierPhone;
    }

    /**
     * Imposta il valore della proprietà wtySupplierPhone.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWtySupplierPhone(String value) {
        this.wtySupplierPhone = value;
    }

    /**
     * Recupera il valore della proprietà wtySupplierFax.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWtySupplierFax() {
        return wtySupplierFax;
    }

    /**
     * Imposta il valore della proprietà wtySupplierFax.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWtySupplierFax(String value) {
        this.wtySupplierFax = value;
    }

    /**
     * Recupera il valore della proprietà wtySupplierEmail.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWtySupplierEmail() {
        return wtySupplierEmail;
    }

    /**
     * Imposta il valore della proprietà wtySupplierEmail.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWtySupplierEmail(String value) {
        this.wtySupplierEmail = value;
    }

    /**
     * Recupera il valore della proprietà wtySupplierCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWtySupplierCode() {
        return wtySupplierCode;
    }

    /**
     * Imposta il valore della proprietà wtySupplierCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWtySupplierCode(String value) {
        this.wtySupplierCode = value;
    }

    /**
     * Recupera il valore della proprietà wtyTotalLoss.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getWtyTotalLoss() {
        return wtyTotalLoss;
    }

    /**
     * Imposta il valore della proprietà wtyTotalLoss.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setWtyTotalLoss(Integer value) {
        this.wtyTotalLoss = value;
    }

    /**
     * Recupera il valore della proprietà wtyTotalLossNote.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWtyTotalLossNote() {
        return wtyTotalLossNote;
    }

    /**
     * Imposta il valore della proprietà wtyTotalLossNote.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWtyTotalLossNote(String value) {
        this.wtyTotalLossNote = value;
    }

    /**
     * Recupera il valore della proprietà frNumcharge.
     * 
     */
    public int getFRNumcharge() {
        return frNumcharge;
    }

    /**
     * Imposta il valore della proprietà frNumcharge.
     * 
     */
    public void setFRNumcharge(int value) {
        this.frNumcharge = value;
    }

}
