package com.doing.nemo.claims.websin;

import com.doing.nemo.claims.adapter.CounterpartyAdapter;
import com.doing.nemo.claims.adapter.DateAdapter;
import com.doing.nemo.claims.adapter.InspectorateAdapter;
import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.ClaimsNewEntity;
import com.doing.nemo.claims.entity.CounterpartyEntity;
import com.doing.nemo.claims.entity.CounterpartyNewEntity;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.ComplaintModEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.EntrustedEnum.EntrustedEnum;
import com.doing.nemo.claims.entity.enumerated.DamagedEnum.ImpactPointEnum.ImpactPointDetectionImpactPointEnum;
import com.doing.nemo.claims.entity.enumerated.DamagedEnum.VehicleEnum.VehicleTypeEnum;
import com.doing.nemo.claims.entity.enumerated.DamagedEnum.WoundedEnum.WoundedTypeEnum;
import com.doing.nemo.claims.entity.enumerated.DamagedEnum.WoundedEnum.WoundedWoundEnum;
import com.doing.nemo.claims.entity.enumerated.EventTypeEnum;
import com.doing.nemo.claims.entity.enumerated.RefundEnum.RefundTypeEnum;
import com.doing.nemo.claims.entity.jsonb.UploadFile;
import com.doing.nemo.claims.entity.jsonb.*;
import com.doing.nemo.claims.entity.jsonb.cai.Cai;
import com.doing.nemo.claims.entity.jsonb.cai.CaiDetails;
import com.doing.nemo.claims.entity.jsonb.complaint.Complaint;
import com.doing.nemo.claims.entity.jsonb.complaint.jsonbComplaint.Entrusted;
import com.doing.nemo.claims.entity.jsonb.complaint.jsonbComplaint.FromCompany;
import com.doing.nemo.claims.entity.jsonb.complaint.jsonbComplaint.Repair;
import com.doing.nemo.claims.entity.jsonb.complaint.jsonbComplaint.jsonbDataAccidents.DataAccident;
import com.doing.nemo.claims.entity.jsonb.damaged.Damaged;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.Customer;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.ImpactPoint;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.InsuranceCompany;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.Insured;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbAntiTheftService.AntiTheftService;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbAntiTheftService.Registry;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbDriver.Driver;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbDriver.DrivingLicense;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbInsuranceCompany.Pai;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbInsuranceCompany.Tpl;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbVehicle.Vehicle;
import com.doing.nemo.claims.entity.jsonb.refund.Refund;
import com.doing.nemo.claims.entity.jsonb.refund.Split;
import com.doing.nemo.claims.entity.settings.*;
import com.doing.nemo.claims.exception.WebSinClaimCommunicationException;
import com.doing.nemo.claims.exception.WebSinFileCommunicationException;
import com.doing.nemo.claims.repository.*;
import com.doing.nemo.claims.service.ConverterClaimsService;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.claims.websin.websinws.*;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.*;

@Service
public class WebSinServiceManagerImpl implements WebSinServiceManager {
    private static final Logger LOGGER = LoggerFactory.getLogger(WebSinServiceManagerImpl.class);
    private static final int WEBSIN_TRUE = 1;
    private static final int WEBSIN_FALSE = 0;
    private static final String CAI_SIDE_A = "A";
    private static final String CAI_SIDE_B = "B";

    @Value("${websin.ws.url}")
    private String url;
    @Value("${websin.ws.username}")
    private String username;
    @Value("${websin.ws.password}")
    private String password;
    @Value("${time.zone}")
    private String timeZone;

     @Autowired
     private CounterpartyNewRepository counterpartyNewRepository;

     @Autowired
     private ClaimsNewRepository claimsNewRepository;

     @Autowired
     private  LegalRepository legalRepository;

     @Autowired
     private InsuranceManagerRepository insuranceManagerRepository;

      @Autowired
      private InsurancePolicyRepository insurancePolicyRepository;

     @Autowired
     ConverterClaimsService converterClaimsService;

     @Autowired
     InsuranceCompanyRepository insuranceCompanyRepository;

     @Autowired
     InspectorateRepository inspectorateRepository;


    private static Map<DataAccidentTypeAccidentEnum, Integer> accidentTypeClaimsToWebsin =
            new HashMap<DataAccidentTypeAccidentEnum,Integer>(){{
        put(DataAccidentTypeAccidentEnum.RC_NON_VERIFICABILE, 0 );
        put(DataAccidentTypeAccidentEnum.RC_ATTIVA, 1 );
        put(DataAccidentTypeAccidentEnum.RC_PASSIVA, 2 );
        put(DataAccidentTypeAccidentEnum.RC_CONCORSUALE, 3 );
        put(DataAccidentTypeAccidentEnum.CARD_NON_VERIFICABILE_FIRMA_SINGOLA, 10 );
        put(DataAccidentTypeAccidentEnum.CARD_ATTIVA_FIRMA_SINGOLA, 11 );
        put(DataAccidentTypeAccidentEnum.CARD_PASSIVA_FIRMA_SINGOLA, 12 );
        put(DataAccidentTypeAccidentEnum.CARD_CONCORSUALE_FIRMA_SINGOLA, 13 );
        put(DataAccidentTypeAccidentEnum.CARD_NON_VERIFICABILE_DOPPIA_FIRMA, 14 );
        put(DataAccidentTypeAccidentEnum.CARD_ATTIVA_DOPPIA_FIRMA, 15 );
        put(DataAccidentTypeAccidentEnum.CARD_PASSIVA_DOPPIA_FIRMA, 16 );
        put(DataAccidentTypeAccidentEnum.CARD_CONCORSUALE_DOPPIA_FIRMA, 17 );
        put(DataAccidentTypeAccidentEnum.KASKO_URTO_CONTRO_OGGETTI_FISSI, 101 );
        put(DataAccidentTypeAccidentEnum.ATTO_VANDALICO, 102 );
        put(DataAccidentTypeAccidentEnum.DANNO_RITROVATO_IN_PARCHEGGIO, 103 );
        put(DataAccidentTypeAccidentEnum.EVENTI_NATURALI, 104 );
        put(DataAccidentTypeAccidentEnum.CRISTALLI, 105 );
        put(DataAccidentTypeAccidentEnum.INCENDIO, 106 );
        put(DataAccidentTypeAccidentEnum.CHIAVI_SMARRITE_O_RUBATE, 107 );
        put(DataAccidentTypeAccidentEnum.TENTATO_FURTO, 108 );
        put(DataAccidentTypeAccidentEnum.FURTO_PARZIALE, 109 );
        put(DataAccidentTypeAccidentEnum.FURTO_E_RITROVAMENTO, 110 );
        put(DataAccidentTypeAccidentEnum.FURTO_TOTALE, 200 );
    }};

    private static Map<String,Integer> regionOfAccident =
            new HashMap<String,Integer>(){{
               put("Abruzzo",1);
                put("Basilicata",2);
                put("Calabria",3);
                put("Campania",4);
                put("Emilia-Romagna",5);
                put("Friuli-Venezia-Giulia",6);
                put("Lazio",7);
                put("Liguria",8);
                put("Lombardia",9);
                put("Marche",10);
                put("Molise",11);
                put("Piemonte",12);
                put("Puglia",13);
                put("Sardegna",14);
                put("Sicilia",15);
                put("Toscana",16);
                put("Trentino-Alto Adige",17);
                put("Umbria",18);
                put("Valle d'Aosta",19);
                put("Veneto",20);
            }};


private static Map<ClaimsStatusEnum, Integer> statusEnumClaimsToWebSin = new HashMap<ClaimsStatusEnum, Integer>(){{
        put(ClaimsStatusEnum.WAITING_FOR_VALIDATION, 1 );
        put(ClaimsStatusEnum.WAITING_FOR_AUTHORITY, 4 );
        put(ClaimsStatusEnum.DRAFT, 0 );
        put(ClaimsStatusEnum.CHECK_DRAFT, 0 );
        put(ClaimsStatusEnum.DELETED, 12 );
        put(ClaimsStatusEnum.TO_ENTRUST, 5 );
        put(ClaimsStatusEnum.CLOSED, 11 );
        put(ClaimsStatusEnum.CLOSED_PRACTICE, 13 );
        put(ClaimsStatusEnum.INCOMPLETE, 2 );
        put(ClaimsStatusEnum.ENTRUSTED_AT, 0 );
        put(ClaimsStatusEnum.REJECTED, 3 );
        put(ClaimsStatusEnum.MANAGED, 99 );
        put(ClaimsStatusEnum.PROPOSED_ACCEPTED, 0 );
        put(ClaimsStatusEnum.WAIT_LOST_POSSESSION, 0 );
        put(ClaimsStatusEnum.LOST_POSSESSION, 0 );
        put(ClaimsStatusEnum.WAIT_TO_RETURN_POSSESSION, 0 );
        put(ClaimsStatusEnum.RETURN_TO_POSSESSION, 0 );
        put(ClaimsStatusEnum.WAITING_FOR_REFUND, 6 );
        put(ClaimsStatusEnum.KASKO, 0 );
        put(ClaimsStatusEnum.TO_REREGISTER, 0 );
        put(ClaimsStatusEnum.BOOK_DUPLICATION, 0 );
        put(ClaimsStatusEnum.STAMP, 0 );
        put(ClaimsStatusEnum.DEMOLITION, 0 );
        put(ClaimsStatusEnum.RECEPTIONS_TO_BE_CONFIRMED, 0 );
        put(ClaimsStatusEnum.TO_WORK, 0 );
        put(ClaimsStatusEnum.CLOSED_PARTIAL_REFUND, 8 );
        put(ClaimsStatusEnum.CLOSED_TOTAL_REFUND, 9 );
        put(ClaimsStatusEnum.SEND_TO_CLIENT, 0 );
        put(ClaimsStatusEnum.VALIDATED, 0 );
        put(ClaimsStatusEnum.STAMP_CLOSED, 0 );
        put(ClaimsStatusEnum.DEMOLITION_CLOSED, 0 );
        put(ClaimsStatusEnum.BOOK_DUPLICATION_CLOSED, 0 );
        put(ClaimsStatusEnum.TO_REREGISTER_CLOSED, 0 );
        put(ClaimsStatusEnum.TO_SEND_TO_CUSTOMER, 7);
        put(ClaimsStatusEnum.PO_VARIATION, 0 );
        put(ClaimsStatusEnum.WAITING_FOR_NUMBER_SX, 0 );
        put(ClaimsStatusEnum.ACCEPTED, 0 );
        put(ClaimsStatusEnum.UNDER_PROCESSING, 0 );
    }};

    public static Map<String, Integer> externalAccidentTypeToWebSin = new HashMap<String, Integer>(){{
        put("RC_NON_VERIFICABILE" , 0);
        put("RC_ATTIVA" , 1);
        put("RC_PASSIVA" , 2);
        put("RC_CONCORSUALE" , 3);
        put("CARD_NON_VERIFICABILE_FIRMA_SINGOLA" , 10);
        put("CARD_ATTIVA_FIRMA_SINGOLA" , 11);
        put("CARD_PASSIVA_FIRMA_SINGOLA" , 12);
        put("CARD_CONCORSUALE_FIRMA_SINGOLA" , 13);
        put("CARD_NON_VERIFICABILE_DOPPIA_FIRMA" , 14);
        put("CARD_ATTIVA_DOPPIA_FIRMA" , 15);
        put("CARD_PASSIVA_DOPPIA_FIRMA" , 16);
        put("CARD_CONCORSUALE_DOPPIA_FIRMA" , 17);
        put("KASKO_URTO_CONTRO_OGGETTI_FISSI" , 101);
        put("ATTO_VANDALICO" , 102);
        put("DANNO_RITROVATO_IN_PARCHEGGIO" , 103);
        put("EVENTI_NATURALI" , 104);
        put("CRISTALLI" , 105);
        put("INCENDIO" , 106);
        put("CHIAVI_SMARRITE_O_RUBATE" , 107);
        put("TENTATO_FURTO" , 108);
        put("FURTO_PARZIALE" , 109);
        put("FURTO_E_RITROVAMENTO" , 110);
        put("FURTO_TOTALE" , 200);
    }};

    public static Map<String, Integer> notificationClaimsToWebSin= new HashMap<String, Integer>(){{
        put("Denuncia Sinistro",1);
        put("Denuncia Sinistro Web",2);
        put("Richiesta della Compagnia",3);
        put("Richiesta danni da controparte",4);
        put("Citazione",5);
        put("Dichiarazione di irreparabilità del mezzo",6);
        put("Offerta reale",7);
        put("Altro",8);
        put("Denuncia Autorità",9);
        put("Soccorso stradale",10);
        put("WINITY",11);
        put("Riparazione",12);
        put("Perizia da stato d uso",13);
        put("Richiesta danni (DEKRA)",14);
        put("Denuncia Sinistro + Citazione",15);
        put("Denuncia Sinistro Web + Citazione",16);
        put("Richiesta danni da controparte + Citazione",17);
        put("Soccorso stradale + Citazione",18);
        put("Richiesta danni (DEKRA) + Citazione",19);
        put("Richiesta danni (MSA)",20);
        put("Richiesta danni (MSA) + Citazione",21);
        put("Comunicazione telefonica",22);
        put("Apertura da MSA",23);
        put("Appropriazione indebita",24);
        put("Furto per rapina",25);
        put("Furto",26);
        put("Appropriazione indebita subnoleggio",27);
        put("Apertura WS da Enjoy",30);
    }};

    public static Map<RefundTypeEnum, Integer> refundTypeToWebSin= new HashMap<RefundTypeEnum, Integer>(){{
        put(RefundTypeEnum.ALLOWANCE, 1);
        put(RefundTypeEnum.BANK_TRANSFER, 2);
    }};

    public static Map<VehicleTypeEnum, Integer> vehicleTypeEnumToWebSin= new HashMap<VehicleTypeEnum, Integer>(){{
        put(VehicleTypeEnum.VEHICLE, 1);
        put(VehicleTypeEnum.OTHER, 2);
    }};

    public static Map<WoundedWoundEnum, Integer> woundedWoundEnumToWebSin= new HashMap<WoundedWoundEnum, Integer>(){{
        put(WoundedWoundEnum.NO, 1);
        put(WoundedWoundEnum.MINOR, 2);
        put(WoundedWoundEnum.SERIOUS, 3);
    }};

    public static Map<ComplaintModEnum, Integer> complaintModEnumToWebSin= new HashMap<ComplaintModEnum, Integer>(){{
        put(ComplaintModEnum.CAI, 1);
        put(ComplaintModEnum.CAI_LIGHT, 1);
        put(ComplaintModEnum.LIT, 1);
        put(ComplaintModEnum.ARD, 2);
        put(ComplaintModEnum.NTW, 4);
        put(ComplaintModEnum.CAL, 5);
        put(ComplaintModEnum.GEF, 6);
        put(ComplaintModEnum.MODULO_MANAGER, 7);
        put(ComplaintModEnum.CAM, 11);
        put(ComplaintModEnum.ARM, 12);
        put(ComplaintModEnum.GEM, 13);
        put(ComplaintModEnum.NTM, 14);
        put(ComplaintModEnum.CLM, 15);
        put(ComplaintModEnum.GFM, 16);
        put(ComplaintModEnum.GEN, 3);
    }};

    public static Integer NOTY_TYPE_ALTRO = 8;

    @Override
    public PutComplaintResponseType putComplaint(PutComplaintRequest putComplaintRequest) throws WebSinClaimCommunicationException {
        AuthHeader authHeader = new AuthHeader();
        authHeader.setWsUsernName(this.username);
        authHeader.setWsPassword(this.password);

        try {
            WebSinWS webSinWS = new WebSinWS(new URL(this.url));
            WebSinWSSoap webSinWSSoap = webSinWS.getWebSinWSSoap();

            PutComplaintResponseType responseType = webSinWSSoap.putComplaint(putComplaintRequest, authHeader);
            if (responseType != null) {
                if (responseType.getCode() == 0) {
                    return responseType;
                } else {
                    throw new WebSinClaimCommunicationException(responseType.getMessage(), null, putComplaintRequest);
                }
            } else {
                throw new WebSinClaimCommunicationException("Unknown Error", null, putComplaintRequest);
            }
        } catch (Exception e) {
            throw new WebSinClaimCommunicationException(e.getMessage(), e, putComplaintRequest);
        }
    }

    @Override
    public PutComplaintRequest buildPutComplaintRequest(String claimsId){
        LOGGER.info("buildPutComplaintRequest");
        Optional<ClaimsNewEntity> optionalClaimsNewEntity = claimsNewRepository.findById(claimsId);
        if(!optionalClaimsNewEntity.isPresent()) {
            throw new BadRequestException(MessageCode.CLAIMS_1010);
        }
        ClaimsNewEntity claimsNewEntity = optionalClaimsNewEntity.get();
        ClaimsEntity claimsEntity = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntity);

        List<CounterpartyNewEntity> counterpartyNewEntityList = counterpartyNewRepository.findAllByClaimsId(claimsEntity.getId());
        List<CounterpartyEntity> counterpartyEntityList = CounterpartyAdapter.adptCounterpartyNewToCounterpartyOldList(counterpartyNewEntityList);

        PutComplaintRequest putComplaintRequest = new PutComplaintRequest();

        putComplaintRequest.setMComplaint(buildMigrationComplaint(claimsEntity));
        putComplaintRequest.setMDamageds(buildArrayOfMigrationDamaged(claimsEntity, counterpartyEntityList));

        MigrationDamaged driverAldMigrationDamaged = findAldDriverFromArrayOfMigrationDamaged(putComplaintRequest.getMDamageds());

        putComplaintRequest.setMInjureds(buildArrayOfMigrationInjured(claimsEntity, driverAldMigrationDamaged));
        putComplaintRequest.setMRepairDossiers(null); // non generiamo il RepairDossiers
        return putComplaintRequest;
    }

    private MigrationDamaged findAldDriverFromArrayOfMigrationDamaged(ArrayOfMigrationDamaged arrayOfMigrationDamaged) {
        if (arrayOfMigrationDamaged != null && arrayOfMigrationDamaged.getMigrationDamaged() != null) {
            for (MigrationDamaged migrationDamaged : arrayOfMigrationDamaged.getMigrationDamaged()) {
                if (migrationDamaged!=null && WEBSIN_TRUE == migrationDamaged.getPrincipalPart()){
                    return migrationDamaged;
                }
            }
        }
        return null;
    }

    @Override
    public UploadFileResponseType uploadFile(UploadFileRequest uploadFileRequest) throws WebSinFileCommunicationException{
        AuthHeader authHeader = new AuthHeader();
        authHeader.setWsUsernName(this.username);
        authHeader.setWsPassword(this.password);

        try {
            WebSinWS webSinWS = new WebSinWS(new URL(this.url));
            WebSinWSSoap webSinWSSoap = webSinWS.getWebSinWSSoap();
            // Decodifico il contenuto del file da Base64 in Byte[] per l'invio
            if(uploadFileRequest.getFileContent()!=null) {
                try {
                    uploadFileRequest.setFileContent(Base64.getDecoder().decode(uploadFileRequest.getFileContent()));
                }
                catch(IllegalArgumentException e){
                    uploadFileRequest.setFileContent(uploadFileRequest.getFileContent());
                }
            }
            UploadFileResponseType uploadFileResponseType = webSinWSSoap.uploadFile(uploadFileRequest, authHeader);
            if (uploadFileResponseType != null) {
                if (uploadFileResponseType.getCode() == 0) {
                    return uploadFileResponseType;
                } else {
                    // Codifico il contenuto del file da binario a Base64 per consentire di salvarlo come JSON nella
                    // Tabella di recupero
                    if(uploadFileRequest.getFileContent()!=null) {
                        uploadFileRequest.setFileContent(Base64.getEncoder().encode(uploadFileRequest.getFileContent()));
                    }
                    throw new WebSinFileCommunicationException(uploadFileResponseType.getMessage(), null, uploadFileRequest);
                }
            } else {
                uploadFileRequest.setFileContent(Base64.getEncoder().encode(uploadFileRequest.getFileContent()));
                throw new WebSinFileCommunicationException("Unknown Error", null, uploadFileRequest);
            }
        } catch (Exception e) {
            throw new WebSinFileCommunicationException(e.getMessage(), e, uploadFileRequest);
        }
    }

    @Override
    public UploadFileRequest buildUploadFileRequest(UploadFile uploadFile, String claimsId){
        ClaimsNewEntity claimsNewEntity = claimsNewRepository.getOne(claimsId);
        Long practiceID = claimsNewEntity.getPracticeId();

        UploadFileRequest uploadFileRequest = new UploadFileRequest();

        String fileExtension = FilenameUtils.getExtension(uploadFile.getFileName());
        if(StringUtils.isEmpty(fileExtension)){
            fileExtension=uploadFile.getBlobType();
            uploadFile.setFileName(uploadFile.getFileName() + "." + fileExtension);
        }
        uploadFileRequest.setExtFile(fileExtension);
        uploadFileRequest.setCatalog("Complaints"); // Catalog FISSO come indicato da SystemData
        if(uploadFile.getFileName()!=null) {
            uploadFileRequest.setOriFileName(uploadFile.getFileName().replaceAll("[\\\\/:*?\"<>|]", "_"));
        }
        uploadFileRequest.setRefID(practiceID.intValue());
        uploadFileRequest.setFileContent(uploadFile.getFileContent().getBytes(StandardCharsets.UTF_8));

        return uploadFileRequest;
    }

    /**
     * Popola la classe {@link MigrationComplaint} di WebSin a partire dalla {@link ClaimsEntity}
     * eseguendo il mapping sulle tipologiche Claims e WebSin.
     * @param claimsEntity
     * @return
     */
    public MigrationComplaint buildMigrationComplaint(ClaimsEntity claimsEntity){
        LOGGER.info("buildMigrationComplaint");
        MigrationComplaint migrationComplaint = new MigrationComplaint();
        migrationComplaint.setID(claimsEntity.getPracticeId().intValue());

        Historical historicalEntrustPai =findEventInHistorical(claimsEntity.getHistorical(),EventTypeEnum.ENTRUST_PAI);
        if(historicalEntrustPai != null){
            migrationComplaint.setPaiCommitDate(dateToXmlGregorianCalendar(historicalEntrustPai.getUpdateAt()));
        }



        //Exemption
        if(claimsEntity.getExemption()!=null){
            Exemption exemption = claimsEntity.getExemption();

            migrationComplaint.setDeductEnd1(dateToXmlGregorianCalendar(exemption.getDeductEnd1()));
            migrationComplaint.setDeductPaid1(nullSafeDoubleToBigDecimal(exemption.getDeductPaid1()));
            migrationComplaint.setDeductImported1(dateToXmlGregorianCalendar(exemption.getDeductImported1()));

            migrationComplaint.setDeductEnd2(dateToXmlGregorianCalendar(exemption.getDeductEnd2()));
            migrationComplaint.setDeductPaid2(nullSafeDoubleToBigDecimal(exemption.getDeductPaid2()));
            migrationComplaint.setDeductImported2(dateToXmlGregorianCalendar(exemption.getDeductImported2()));

            migrationComplaint.setDeductTotalPaid(nullSafeDoubleToBigDecimal(exemption.getDeductTotalPaid()));
        }

        // Theft
        if(claimsEntity.getTheft()!=null){
            Theft theft = claimsEntity.getTheft();
            migrationComplaint.setTheftOnBranchCode(theft.getSupplierCode());
            migrationComplaint.setAntiTheftAlerted(booleanToWebSinInteger(theft.getOperationsCenterNotified()));
            migrationComplaint.setTheftOnBranch(booleanToWebSinInteger(theft.getTheftOccurredOnCenterAld()));
            migrationComplaint.setFindDate(dateToXmlGregorianCalendar(theft.getFindDate()));
            migrationComplaint.setFindHour(extractHourMinute(DateAdapter.adaptDateZone(theft.getFindDate(), timeZone)));
            migrationComplaint.setFindForeing(booleanToWebSinInteger(theft.getFoundAbroad()));
            migrationComplaint.setFindAuthPolice(booleanToWebSinInteger(theft.getFindAuthorityPolice()));
            migrationComplaint.setFindAuthCC(booleanToWebSinInteger(theft.getFindAuthorityCc()));
            migrationComplaint.setFindAuthVVUU(booleanToWebSinInteger(theft.getFindAuthorityVvuu()));
            migrationComplaint.setFindAuthDes(theft.getFindAuthorityData());
            migrationComplaint.setFindAuthPhone(theft.getFindAuthorityTelephone());
            migrationComplaint.setFindAuthSeized(booleanToWebSinInteger(theft.getSequestered()));
            migrationComplaint.setIsRobbery(booleanToWebSinInteger(theft.getRobbery()));
            migrationComplaint.setIsKeysManage(booleanToWebSinInteger(theft.getKeyManagement()));
            migrationComplaint.setIsAccountingManage(booleanToWebSinInteger(theft.getAccountManagement()));
            migrationComplaint.setIsCkAdminPosition(booleanToWebSinInteger(theft.getAdministrativePosition()));
            migrationComplaint.setRxFirstKey(dateToXmlGregorianCalendar(theft.getFirstKeyReception()));
            migrationComplaint.setRxSecondKey(dateToXmlGregorianCalendar(theft.getSecondKeyReception()));
            migrationComplaint.setRxOriComplaint(dateToXmlGregorianCalendar(theft.getOriginalComplaintReception()));
            migrationComplaint.setRxCpyComplaint(dateToXmlGregorianCalendar(theft.getCopyComplaintReception()));
            migrationComplaint.setRxOriVerbFind(dateToXmlGregorianCalendar(theft.getOriginalReportReception()));
            migrationComplaint.setRxOriVerbFind(dateToXmlGregorianCalendar(theft.getCopyReportReception()));
            if(claimsEntity.getComplaint()!=null && claimsEntity.getComplaint().getDataAccident()!=null){
                migrationComplaint.setTheftDate(dateToXmlGregorianCalendar(DateUtil.convertIS08601StringToUTCInstant(DateUtil.convertUTCDateToIS08601String(claimsEntity.getComplaint().getDataAccident().getDateAccident()))));
            }
            migrationComplaint.setPresenceAuthPhone(theft.getAuthorityTelephone());
            migrationComplaint.setFindVehicleCO(theft.getVehicleCo());

            if(theft.getAddress()!=null){
                migrationComplaint.setFindAddress(theft.getAddress().getFormattedAddress());
            }

        }

        // Damaged
        if(claimsEntity.getDamaged()!=null){
            Damaged damaged = claimsEntity.getDamaged();
            // Vehicle
            if(damaged.getVehicle()!=null){
                Vehicle vehicle = damaged.getVehicle();
                migrationComplaint.setNotRepairable(booleanToWebSinInteger(vehicle.getWreck()));

            }
            if(damaged.getAntiTheftService()!=null){
                AntiTheftService antiTheftService = damaged.getAntiTheftService();
                if(antiTheftService.getCodeAntiTheftService()!=null){
                    migrationComplaint.setAntiTheftServiceID(antiTheftService.getCodeAntiTheftService());
                }
                // Registry
                if(antiTheftService.getRegistryList()!=null && !antiTheftService.getRegistryList().isEmpty()){
                    Registry registryList = antiTheftService.getRegistryList().get(0);
                    migrationComplaint.setAtfVoucherID(registryList.getVoucherId());
                    migrationComplaint.setAtfSrvProvider(registryList.getCodProvider());
                }
            }

            // InsuranceCompany
            if(damaged.getInsuranceCompany()!=null){
                InsuranceCompany insuranceCompany = damaged.getInsuranceCompany();

                // PAI
                if(insuranceCompany.getPai()!=null){
                    Pai pai = insuranceCompany.getPai();
                    if(pai.getInsurancePolicyId()!=null) {
                        Optional<InsurancePolicyEntity> insurancePolicyEntityOptional= insurancePolicyRepository.findById(pai.getInsurancePolicyId());
                        if(insurancePolicyEntityOptional.isPresent() && insurancePolicyEntityOptional.get().getInsurancePolicyId()!=null){
                            migrationComplaint.setPaiInsurancePolicyID(insurancePolicyEntityOptional.get().getInsurancePolicyId().intValue());
                        }
                    }
                }
            }

        }

        if(claimsEntity.getWoundedList()!=null){
            // verifico se sono presenti feriti controlalndo se la lista dei wounded è maggiore di 0
            migrationComplaint.setAccidentInjureds(booleanToWebSinInteger(claimsEntity.getWoundedList().size() > 0));
        }
        //Old Moto Plate sempre false
        migrationComplaint.setOldMotoPlate(WEBSIN_FALSE);
        migrationComplaint.setStatus(statusEnumClaimsToWebSin.get(claimsEntity.getStatus()));
        if(ClaimsStatusEnum.INCOMPLETE.equals(claimsEntity.getStatus())){
            migrationComplaint.setReasonTypeID(10); // Tipo ragione da vedere nella descrizione
            migrationComplaint.setReasonsDesc(claimsEntity.getMotivation());
        }

        if(claimsEntity.getType()!=null) {
            migrationComplaint.setFlowType(claimsEntity.getType().toString().toUpperCase());
        }

        // Refound
        if(claimsEntity.getRefund()!=null){
            StringBuilder liqNote = new StringBuilder();
            Refund refund = claimsEntity.getRefund();
            migrationComplaint.setResidualValue(nullSafeDoubleToBigDecimal(refund.getWreckValuePost()));
            migrationComplaint.setDeductibleAmount(nullSafeDoubleToBigDecimal(refund.getFranchiseAmountFcm()));
            migrationComplaint.setLiqValue(nullSafeDoubleToBigDecimal(refund.getTotalLiquidationReceived()));
            migrationComplaint.setLiqDate(dateToXmlGregorianCalendar(refund.getDefinitionDate()));
            migrationComplaint.setLiqWait(nullSafeDoubleToBigDecimal(refund.getTotalRefundExpected()));

            if(refund.getSplitList()!=null){
                List<Split> splitList = refund.getSplitList();
                if(splitList.size()>=1) {
                    Split split = splitList.get(0);
                    migrationComplaint.setPayType1(refundTypeToWebSin.get(split.getRefundType()));
                    migrationComplaint.setPayDate1(dateToXmlGregorianCalendar(split.getIssueDate()));
                    migrationComplaint.setPayLawyerCost1(nullSafeDoubleToBigDecimal(split.getLegalFees()));
                    migrationComplaint.setPayDamageValue1(nullSafeDoubleToBigDecimal(split.getMaterialAmount()));
                    migrationComplaint.setPayTechBlock1(nullSafeDoubleToBigDecimal(split.getTechnicalStop()));
                    migrationComplaint.setPayTot1(nullSafeDoubleToBigDecimal(split.getTotalPaid()));
                    migrationComplaint.setPayAmount1(nullSafeDoubleToBigDecimal(split.getReceivedSum()));
                    liqNote.append(split.getNote());
                }

                if(splitList.size()>=2) {
                    Split split = splitList.get(1);
                    migrationComplaint.setPayType2(refundTypeToWebSin.get(split.getRefundType()));
                    migrationComplaint.setPayDate2(dateToXmlGregorianCalendar(split.getIssueDate()));
                    migrationComplaint.setPayLawyerCost2(nullSafeDoubleToBigDecimal(split.getLegalFees()));
                    migrationComplaint.setPayDamageValue2(nullSafeDoubleToBigDecimal(split.getMaterialAmount()));
                    migrationComplaint.setPayTechBlock2(nullSafeDoubleToBigDecimal(split.getTechnicalStop()));
                    migrationComplaint.setPayTot2(nullSafeDoubleToBigDecimal(split.getTotalPaid()));
                    migrationComplaint.setPayAmount2(nullSafeDoubleToBigDecimal(split.getReceivedSum()));
                    liqNote.append(" " + split.getNote());
                }
                if(splitList.size()>=3) {
                    Split split = splitList.get(2);
                    migrationComplaint.setPayType3(refundTypeToWebSin.get(split.getRefundType()));
                    migrationComplaint.setPayDate3(dateToXmlGregorianCalendar(split.getIssueDate()));
                    migrationComplaint.setPayLawyerCost3(nullSafeDoubleToBigDecimal(split.getLegalFees()));
                    migrationComplaint.setPayDamageValue3(nullSafeDoubleToBigDecimal(split.getMaterialAmount()));
                    migrationComplaint.setPayTechBlock3(nullSafeDoubleToBigDecimal(split.getTechnicalStop()));
                    migrationComplaint.setPayTot3(nullSafeDoubleToBigDecimal(split.getTotalPaid()));
                    migrationComplaint.setPayAmount3(nullSafeDoubleToBigDecimal(split.getReceivedSum()));
                    liqNote.append(" " + split.getNote());
                }
                migrationComplaint.setLiqNote(liqNote.toString());
            }
        }

        // Complaint
        if(claimsEntity.getComplaint()!=null){
            Complaint complaint= claimsEntity.getComplaint();

            migrationComplaint.setIsSummons(booleanToWebSinInteger(complaint.getQuote()));

            if(complaint.getMod()!=null){
                migrationComplaint.setModuleType(complaintModEnumToWebSin.get(complaint.getMod()));
            }

            //Noty Type ID
            if(complaint.getNotification()!=null
                    && notificationClaimsToWebSin.containsKey(complaint.getNotification())){
                migrationComplaint.setNotyTypeID(notificationClaimsToWebSin.get(complaint.getNotification()));
            }else{
                // Se non riesco a trovare il mapping, inserisco come NotyTypeID il valore ALTRO
                migrationComplaint.setNotyTypeID(NOTY_TYPE_ALTRO);
            }

            // Data Accident
            if(complaint.getDataAccident()!=null) {
                DataAccident dataAccident = complaint.getDataAccident();

                if(dataAccident.getDateAccident()!=null){
                    migrationComplaint.setAccidentDate(dateToXmlGregorianCalendar(DateAdapter.adaptDateZone(dataAccident.getDateAccident(), timeZone)));
                    migrationComplaint.setAccidentHour(extractHourMinute(DateAdapter.adaptDateZone(dataAccident.getDateAccident(), timeZone)));
                }
                if ( dataAccident.getAddress() != null) {
                    Address address = dataAccident.getAddress();
                    migrationComplaint.setAccidentCity(address.getLocality());
                    migrationComplaint.setAccidentProvince(address.getProvince());
                    migrationComplaint.setAccidentRegion(address.getRegion()!=null?regionOfAccident.get(address.getRegion()):null);
                    migrationComplaint.setAccidentAddress(address.getFormattedAddress());
                }
                migrationComplaint.setAccidentForeing(booleanToWebSinInteger(dataAccident.getHappenedAbroad()));
                migrationComplaint.setDamageOtherVehicles(booleanToWebSinInteger(dataAccident.getDamageToVehicles()));
                migrationComplaint.setDamageObject(booleanToWebSinInteger(dataAccident.getDamageToObjects()));

                if( dataAccident.getCc()!=null && dataAccident.getCc()
                        || dataAccident.getVvuu()!=null && dataAccident.getVvuu()
                        || dataAccident.getPolice()!=null && dataAccident.getPolice()){
                    migrationComplaint.setPresenceAuth(WEBSIN_TRUE);
                }else{
                    migrationComplaint.setPresenceAuth(WEBSIN_FALSE);
                }

                migrationComplaint.setPresencePolice(booleanToWebSinInteger(dataAccident.getPolice()));
                migrationComplaint.setPresenceCC(booleanToWebSinInteger(dataAccident.getCc()));
                migrationComplaint.setPresenceVVUU(booleanToWebSinInteger(dataAccident.getVvuu()));
                migrationComplaint.setPresenceAuthDes(dataAccident.getAuthorityData());

                migrationComplaint.setRecoverable(booleanToWebSinInteger(dataAccident.getRecoverability()));
                if(dataAccident.getRecoverabilityPercent()!=null){
                    migrationComplaint.setPerRecoverable(dataAccident.getRecoverabilityPercent().intValue());
                }

                if(dataAccident.getTypeAccident()!=null) {
                    migrationComplaint.setAccidentType(accidentTypeClaimsToWebsin.get(dataAccident.getTypeAccident()));
                }
            }

            //Repair
            if(complaint.getRepair()!=null){
                Repair repair = complaint.getRepair();
                migrationComplaint.setRepairLock(booleanToWebSinInteger(repair.getBlocksRepair()));
            }

            //Entrusted
            if(complaint.getEntrusted()!=null){
                Entrusted entrusted = complaint.getEntrusted();
                migrationComplaint.setCommitDate(dateToXmlGregorianCalendar(entrusted.getEntrustedDay()));
                if(EntrustedEnum.LEGAL.equals(entrusted.getEntrustedType())){
                    migrationComplaint.setCommitTo(2);
                    Optional<LegalEntity> legalEntityOptional = legalRepository.findById(entrusted.getIdEntrustedTo());
                    if(legalEntityOptional.isPresent() && legalEntityOptional.get().getCode()!=null){
                        migrationComplaint.setLawyerID(legalEntityOptional.get().getCode().intValue());
                    }
                } else  if(EntrustedEnum.MANAGER.equals(entrusted.getEntrustedType())){
                    migrationComplaint.setCommitTo(3);
                    Optional<InsuranceManagerEntity> insuranceManagerEntityOptional =  insuranceManagerRepository.findById(entrusted.getIdEntrustedTo());
                    if(insuranceManagerEntityOptional.isPresent() && insuranceManagerEntityOptional.get().getRifCode()!=null){
                        migrationComplaint.setInsuranceManagerID(insuranceManagerEntityOptional.get().getRifCode().intValue());
                    }
                }else  if(EntrustedEnum.INSPECTORATE.equals(entrusted.getEntrustedType())){
                    migrationComplaint.setCommitTo(1);
                    Optional<InspectorateEntity> inspectorateEntityOptional =  inspectorateRepository.findById(entrusted.getIdEntrustedTo());
                    if(inspectorateEntityOptional.isPresent() && inspectorateEntityOptional.get().getCode()!=null){
                        migrationComplaint.setExpertID(inspectorateEntityOptional.get().getCode().intValue());
                    }
                }
            }
            // FromCompany
            if( complaint.getFromCompany()!=null) {

                FromCompany fromCompany = complaint.getFromCompany();

                migrationComplaint.setExternalExpert(fromCompany.getExpert());

                if (fromCompany.getNumberSx() != null) {
                    migrationComplaint.setExternalAccidentNumber(fromCompany.getNumberSx().trim());
                }
                if(fromCompany.getTypeSx()!=null && externalAccidentTypeToWebSin.containsKey(fromCompany.getTypeSx())) {
                    migrationComplaint.setExternalAccidentType(externalAccidentTypeToWebSin.get(fromCompany.getTypeSx()));
                }
                migrationComplaint.setExternalInspectorate(fromCompany.getInspectorate());
                migrationComplaint.setExternalNote(fromCompany.getNote());
                if(fromCompany.getCompany()!=null){
                    List<InsuranceCompanyEntity> insuranceCompanyEntityList = insuranceCompanyRepository.findAllByCardCompanyByName(fromCompany.getCompany());
                    if(insuranceCompanyEntityList!=null && insuranceCompanyEntityList.size()>0){
                        migrationComplaint.setInsuranceCompanyID(insuranceCompanyEntityList.get(0).getCode()!=null ? insuranceCompanyEntityList.get(0).getCode().intValue() : null);
                    }
                }
            }

        }


        migrationComplaint.setRepairStatus(0);

        migrationComplaint.setThirdParies(booleanToWebSinInteger(claimsEntity.getWithCounterparty()));
        migrationComplaint.setReicevedDate(dateToXmlGregorianCalendar(DateUtil.convertIS08601StringToUTCInstant(DateUtil.convertUTCDateToIS08601String(DateUtil.convertIS08601StringToUTCDate(DateUtil.convertUTCInstantToIS08601String(claimsEntity.getCreatedAt()))))));
        migrationComplaint.setFeturedDate(dateToXmlGregorianCalendar(DateUtil.convertIS08601StringToUTCInstant(DateUtil.convertUTCDateToIS08601String(DateUtils.addDays(Date.from(claimsEntity.getCreatedAt()),15)))));

        return migrationComplaint;
    }

    /**
     * Genera {@link ArrayOfMigrationInjured} partendo dal claims
     * @param claimsEntity
     * @return
     */
    public ArrayOfMigrationInjured buildArrayOfMigrationInjured(ClaimsEntity claimsEntity, MigrationDamaged driverAldMigrationDamaged){
        LOGGER.info("buildArrayOfMigrationInjured");
        ArrayOfMigrationInjured arrayOfMigrationInjured = new ArrayOfMigrationInjured();

        if(claimsEntity.getWoundedList()!=null){
            for(int i=0;i<claimsEntity.getWoundedList().size(); i++) {
                Wounded wounded = claimsEntity.getWoundedList().get(i);

                if(WoundedTypeEnum.DRIVER.equals(wounded.getType())){
                    if(driverAldMigrationDamaged!=null && wounded!=null && wounded.getWound()!=null){
                        LOGGER.info("Setto le lesioni del Driver ALD");
                        driverAldMigrationDamaged.setDriverLesions(woundedWoundEnumToWebSin.get(wounded.getWound()));
                    }
                }

                MigrationInjured migrationInjured = buildMigrationInjured(wounded, i);
                arrayOfMigrationInjured.getMigrationInjured().add(migrationInjured);
            }
        }
        return arrayOfMigrationInjured;
    }

    public MigrationInjured buildMigrationInjured(Wounded wounded,int index){
        MigrationInjured migrationInjured = new MigrationInjured();
//        migrationInjured.setDamagedID(index);
        if(wounded.getWound()!=null) {
            migrationInjured.setInjType(woundedWoundEnumToWebSin.get(wounded.getWound()));
        }

        migrationInjured.setInjSurname(wounded.getLastname());
        migrationInjured.setInjName(wounded.getFirstname());
        migrationInjured.setInjER(booleanToWebSinInteger(wounded.getEmergencyRoom()));

        if(wounded.getAddress()!=null){
            Address address = wounded.getAddress();
            migrationInjured.setInjAddress(address.getFormattedAddress());
            migrationInjured.setInjCity(address.getLocality());
            migrationInjured.setInjProvince(address.getProvince());
            migrationInjured.setInjZipCode(address.getZip());
            migrationInjured.setInjCountry(address.getState());
        }

        return migrationInjured;
    }


    /**
     * Genera {@link ArrayOfMigrationDamaged} partendo dal claims e dalla lista delle controparti
     * @param claimsEntity
     * @param counterpartyEntityList
     * @return
     */
    public ArrayOfMigrationDamaged buildArrayOfMigrationDamaged(ClaimsEntity claimsEntity, List<CounterpartyEntity> counterpartyEntityList){
        LOGGER.info("buildArrayOfMigrationDamaged");
        ArrayOfMigrationDamaged arrayOfMigrationDamaged = new ArrayOfMigrationDamaged();
        List<MigrationDamaged> migrationDamagedList = arrayOfMigrationDamaged.getMigrationDamaged();

        // il MigrationDamaged principal è costruito a partire dal driver ALD presente in Damaged
        MigrationDamaged migrationDamagedPrincipal = buildMigrationDamagedPrincipalPart(claimsEntity);
        migrationDamagedList.add(migrationDamagedPrincipal);

        if(counterpartyEntityList.size()>1) {
            // Per ogni Controparte del sinistro viene costruito un MigrationDamaged.
            for (CounterpartyEntity counterpartyEntity : counterpartyEntityList) {
                MigrationDamaged migrationDamaged = buildMigrationDamagedCounterparty(counterpartyEntity, migrationDamagedPrincipal.getComplaintID());

                migrationDamagedList.add(migrationDamaged);
            }
        }else if(counterpartyEntityList.size()==1) {
            // Caso Singola Controparte Devo Verificare se è presente il CAI e aggiornare di conseguenza il MigrationDamaged
            // IN caso di più controparti
            MigrationDamaged migrationDamaged = buildMigrationDamagedCounterparty(counterpartyEntityList.get(0), migrationDamagedPrincipal.getComplaintID());
            updateMigrationDamagedCAIForCounterParty(claimsEntity, counterpartyEntityList.get(0), migrationDamaged);
            migrationDamagedList.add(migrationDamaged);
        }

        return arrayOfMigrationDamaged;
    }


    private void updateMigrationDamagedCAIForCounterParty(ClaimsEntity claimsEntity, CounterpartyEntity counterpartyEntity, MigrationDamaged migrationDamaged) {
        if(claimsEntity!=null && counterpartyEntity!=null && migrationDamaged!=null) {
            if (claimsEntity.getCaiDetails() != null) {
                Cai cai = claimsEntity.getCaiDetails();
                if (CAI_SIDE_A.equals(cai.getDriverSide())) {
                    updateMigrationDamagedFromCaiDetails(cai.getVehicleB(), 2, counterpartyEntity.isCaiSigned(), migrationDamaged);
                } else if (CAI_SIDE_B.equals(cai.getDriverSide())) {
                    updateMigrationDamagedFromCaiDetails(cai.getVehicleA(), 1, counterpartyEntity.isCaiSigned(), migrationDamaged);
                } else {
                    LOGGER.error("Cai Side not valid " + cai.getDriverSide());
                }
            }
        }
    }

    /**
     * Costruisce il {@link MigrationDamaged} partendo dalle informazioni del cliente ALD definita come
     * PrincipalPart in WebSin
     *
     * @param claimsEntity
     * @return
     */
    private MigrationDamaged buildMigrationDamagedPrincipalPart(ClaimsEntity claimsEntity){
        MigrationDamaged migrationDamaged = new MigrationDamaged();
        if(claimsEntity.getPracticeId()!=null) {
            migrationDamaged.setComplaintID(claimsEntity.getPracticeId().intValue());
        }
        migrationDamaged.setPrincipalPart(WEBSIN_TRUE);
        Damaged damaged= claimsEntity.getDamaged();
        updatemigrationDamagedFromVehicle(damaged.getVehicle(), migrationDamaged);
        updatemigrationDamagedFromDriver(damaged.getDriver(), migrationDamaged);

        Theft theft = claimsEntity.getTheft();
        updatemigrationDamagedFromImpactPoint(damaged.getImpactPoint(), migrationDamaged, theft);


        if(damaged.getInsuranceCompany()!=null) {
            InsuranceCompany insuranceCompany = damaged.getInsuranceCompany();
            // Se l'oggetto Kasko è diverso da null allora è presente la kasko.
            migrationDamaged.setKasko(booleanToWebSinInteger(insuranceCompany.getKasko() != null));

            // tpl
            if (insuranceCompany.getTpl() != null) {
                Tpl tpl = insuranceCompany.getTpl();
                migrationDamaged.setInsuranceCompanyName(tpl.getCompany());
                migrationDamaged.setPolicyNumber(tpl.getPolicyNumber());

                if(tpl.getInsuranceCompanyId()!=null){
                    Optional<InsuranceCompanyEntity> insuranceCompanyEntityOptional = insuranceCompanyRepository.findById(tpl.getInsuranceCompanyId());
                    if(insuranceCompanyEntityOptional.isPresent() && insuranceCompanyEntityOptional.get().getCode()!=null ){
                        migrationDamaged.setInsuranceCompanyID(insuranceCompanyEntityOptional.get().getCode().intValue());
                    }
                }

                if(tpl.getInsurancePolicyId()!=null) {
                    try {
                        Optional<InsurancePolicyEntity> insurancePolicyEntityOptional = insurancePolicyRepository.findById(tpl.getInsurancePolicyId());
                        if(insurancePolicyEntityOptional.isPresent()){
                            InsurancePolicyEntity insurancePolicyEntity = insurancePolicyEntityOptional.get();
                            migrationDamaged.setPolicyStart(dateToXmlGregorianCalendar(insurancePolicyEntity.getBeginningValidity()));
                            migrationDamaged.setPolicyEnd(dateToXmlGregorianCalendar(insurancePolicyEntity.getEndValidity()));
                        }

                    }catch (Exception ex){
                        LOGGER.error("Error find Insurance by id: " + tpl.getInsurancePolicyId());
                    }
                }
            }

        }

        if(damaged.getCustomer()!=null) {
            Customer customer = damaged.getCustomer();

            if (customer.getLegalName()!=null){
                migrationDamaged.setAssuredSurname(customer.getLegalName().substring(0, Math.min(customer.getLegalName().length(), 50)));
            }

            migrationDamaged.setAssuredVat(customer.getVatNumber());
            migrationDamaged.setAssuredPhone(customer.getPhonenr());
            migrationDamaged.setAssuredEmail(customer.getEmail());

            if(customer.getMainAddress()!=null){
                Address  address = customer.getMainAddress();
                migrationDamaged.setAssuredAddress(address.getFormattedAddress());
                migrationDamaged.setAssuredCity(address.getLocality());
                migrationDamaged.setAssuredProvince(address.getProvince());
                migrationDamaged.setAssuredZipCode(address.getZip());
                migrationDamaged.setAssuredCountry(address.getState());
            }

        }

        if(claimsEntity.getCaiDetails() !=null){
            Cai cai = claimsEntity.getCaiDetails();
            if(CAI_SIDE_A.equals(cai.getDriverSide())){
                updateMigrationDamagedFromCaiDetails(cai.getVehicleA(), 1, claimsEntity.isCaiSigned(), migrationDamaged);
            }else if(CAI_SIDE_B.equals(cai.getDriverSide())){
                updateMigrationDamagedFromCaiDetails(cai.getVehicleB(), 2, claimsEntity.isCaiSigned(), migrationDamaged);
            }else{
                LOGGER.error("Cai Side not valid " +  cai.getDriverSide());
            }

        }

        if(claimsEntity.getComplaint()!=null
                && claimsEntity.getComplaint().getDataAccident()!=null
                && claimsEntity.getComplaint().getDataAccident().getResponsible()!=null) {
            migrationDamaged.setPrincipalOpponent(booleanToWebSinInteger(claimsEntity.getComplaint().getDataAccident().getResponsible()));
        }


        return migrationDamaged;
    }

    /**
     * Costruisce {@link MigrationDamaged} partendo da {@link CounterpartyEntity}
     * @param counterpartyEntity
     * @param practiceId
     * @return
     */
    private MigrationDamaged buildMigrationDamagedCounterparty(CounterpartyEntity counterpartyEntity, int practiceId){
        MigrationDamaged migrationDamaged = new MigrationDamaged();
        migrationDamaged.setPrincipalPart(WEBSIN_FALSE);
        migrationDamaged.setComplaintID(practiceId);

        if(counterpartyEntity.getType()!=null) {
            migrationDamaged.setDamagedType(vehicleTypeEnumToWebSin.get(counterpartyEntity.getType()));
        }

        if (counterpartyEntity.getCounterpartyId()!=null){
            migrationDamaged.setID(counterpartyEntity.getPracticeIdCounterparty());
        }

        if(counterpartyEntity.getInsuranceCompany()!=null && counterpartyEntity.getInsuranceCompany().getEntity()!=null) {
            InsuranceCompanyEntity insuranceCompany = counterpartyEntity.getInsuranceCompany().getEntity();
            migrationDamaged.setInsuranceCompanyName(insuranceCompany.getName());
            migrationDamaged.setInsuranceCompanyID(insuranceCompany.getCode()!=null? insuranceCompany.getCode().intValue() : null);
        }

        migrationDamaged.setPolicyNumber(counterpartyEntity.getPolicyNumber());
        migrationDamaged.setPolicyStart(dateToXmlGregorianCalendar(counterpartyEntity.getPolicyBeginningValidity()));
        migrationDamaged.setPolicyEnd(dateToXmlGregorianCalendar(counterpartyEntity.getPolicyEndValidity()));

        if (counterpartyEntity.getDescription()!=null){
            migrationDamaged.setDamageNature(counterpartyEntity.getDescription().substring(0, Math.min(counterpartyEntity.getDescription().length(), 50)));
        }


        updatemigrationDamagedFromImpactPoint(counterpartyEntity.getImpactPoint(),migrationDamaged, null);
        updatemigrationDamagedFromVehicle(counterpartyEntity.getVehicle(),migrationDamaged);
        updatemigrationDamagedFromDriver(counterpartyEntity.getDriver(), migrationDamaged);
        migrationDamaged.setPrincipalOpponent(booleanToWebSinInteger(counterpartyEntity.getResponsible()));


        if(counterpartyEntity.getInsured()!=null){
            Insured insured = counterpartyEntity.getInsured();
            if (insured.getLastname()!=null){
                migrationDamaged.setAssuredSurname(insured.getLastname().substring(0, Math.min(insured.getLastname().length(), 50)));
            }

            if (insured.getFirstname()!=null){
                migrationDamaged.setAssuredName(insured.getFirstname().substring(0, Math.min(insured.getFirstname().length(), 50)));
            }

            migrationDamaged.setAssuredFiscalCode(insured.getFiscalCode());
            migrationDamaged.setAssuredPhone(insured.getPhone());
            migrationDamaged.setAssuredEmail(insured.getEmail());

            if(insured.getAddress()!=null){
                Address  address = insured.getAddress();
                migrationDamaged.setAssuredAddress(address.getFormattedAddress());
                migrationDamaged.setAssuredCity(address.getLocality());
                migrationDamaged.setAssuredProvince(address.getProvince());
                migrationDamaged.setAssuredZipCode(address.getZip());
                migrationDamaged.setAssuredCountry(address.getState());
            }
        }

        return migrationDamaged;
    }

    private void updatemigrationDamagedFromVehicle(Vehicle vehicle, MigrationDamaged migrationDamaged){
        if(vehicle==null){
            return;
        }
        migrationDamaged.setVehiclePlate(vehicle.getLicensePlate());
        migrationDamaged.setVehicleVIN(vehicle.getChassisNumber());
        migrationDamaged.setVehicleRegCountry(StringUtils.substring(vehicle.getRegistrationCountry(),0,3));
        migrationDamaged.setVehicleRegDate(dateToXmlGregorianCalendar(vehicle.getRegistrationDate()));

        migrationDamaged.setTrailerPlate(vehicle.getLicensePlateTrailer());
        migrationDamaged.setTrailerVIN(vehicle.getChassisNumberTrailer());
        migrationDamaged.setTrailerRegCountry(StringUtils.substring(vehicle.getRegistrationCountryTrailer(),0,3));

        migrationDamaged.setVehicleDescription(buildVehicleDescription(vehicle));
    }

    private void updatemigrationDamagedFromDriver(Driver driver, MigrationDamaged migrationDamaged){
        if(driver==null){
            return;
        }
        migrationDamaged.setDriverName(driver.getFirstname());
        migrationDamaged.setDriverSurname(driver.getLastname());
        migrationDamaged.setDriverEmail(driver.getEmail());
        migrationDamaged.setDriverBirthDate(dateToXmlGregorianCalendar(driver.getDateOfBirth()));
        migrationDamaged.setDriverFiscalCode(driver.getFiscalCode());
        migrationDamaged.setDriverPhone(driver.getPhone());
        if(driver.getDrivingLicense()!=null) {
            DrivingLicense drivingLicense = driver.getDrivingLicense();
            migrationDamaged.setDriverLicenseNumber(drivingLicense.getNumber());
            migrationDamaged.setDriverLicenseEnd(dateToXmlGregorianCalendar(drivingLicense.getValidTo()));
            if(drivingLicense.getCategory()!=null){
                migrationDamaged.setDriverLicenseCategory(drivingLicense.getCategory().getValue());
            }
        }
        if(driver.getMainAddress()!=null){
            Address address = driver.getMainAddress();
            migrationDamaged.setDriverAddress(address.getFormattedAddress());
            migrationDamaged.setDriverCity(address.getLocality());
            migrationDamaged.setDriverProvince(address.getProvince());
        }
    }

    private void  rsetDamagedPosition(MigrationDamaged migrationDamaged){
        migrationDamaged.setDamagePosA1(WEBSIN_FALSE);
        migrationDamaged.setDamagePosA2(WEBSIN_FALSE);
        migrationDamaged.setDamagePosA3(WEBSIN_FALSE);
        migrationDamaged.setDamagePosB1(WEBSIN_FALSE);
        migrationDamaged.setDamagePosB2(WEBSIN_FALSE);
        migrationDamaged.setDamagePosB3(WEBSIN_FALSE);
        migrationDamaged.setDamagePosB4(WEBSIN_FALSE);
        migrationDamaged.setDamagePosC1(WEBSIN_FALSE);
        migrationDamaged.setDamagePosC2(WEBSIN_FALSE);
        migrationDamaged.setDamagePosC3(WEBSIN_FALSE);
        migrationDamaged.setDamagePosD(WEBSIN_FALSE);
        migrationDamaged.setDamagePosE(WEBSIN_FALSE);
        migrationDamaged.setDamagePosF(WEBSIN_FALSE);
    }

    private void updatemigrationDamagedFromImpactPoint(ImpactPoint impactPoint, MigrationDamaged migrationDamaged, Theft theft){
        if(impactPoint==null){
            return;
        }

        StringBuilder damageDescription = new StringBuilder();
        StringBuilder damageComment = new StringBuilder();
        if(theft != null){
            if(theft.getTheftNotes()!=null) {
                damageComment.append(theft.getTheftNotes());
            }
            if(theft.getFindNotes()!=null) {
                damageDescription.append(theft.getFindNotes());
            }
        }

        if(impactPoint.getDamageToVehicle()!=null) {
            if (damageDescription.length()>0){
                damageDescription.append(" ; ").append(impactPoint.getDamageToVehicle());
            }else{
                damageDescription.append(impactPoint.getDamageToVehicle());
            }

        }
        if(impactPoint.getIncidentDescription()!=null) {
            if (damageComment.length()>0){
                damageComment.append(" ; ").append(impactPoint.getIncidentDescription());
            }else{
                damageComment.append(impactPoint.getIncidentDescription());
            }

        }

        migrationDamaged.setDamageDescription(damageDescription.toString());
        migrationDamaged.setDamageComment(damageComment.toString());
        //migrationDamaged.setDamageDescription(impactPoint.getDamageToVehicle());
        //migrationDamaged.setDamageComment(impactPoint.getIncidentDescription());


        rsetDamagedPosition(migrationDamaged);
        if(impactPoint.getDetectionsImpactPoint()!=null){
            for (ImpactPointDetectionImpactPointEnum impactPointEnum : impactPoint.getDetectionsImpactPoint()){
                switch (impactPointEnum){
                    case REAR:
                        migrationDamaged.setDamagePosC1(WEBSIN_TRUE);
                        break;
                    case ROOF:
                        migrationDamaged.setDamagePosF(WEBSIN_TRUE);
                        break;
                    case FRONT:
                        migrationDamaged.setDamagePosA1(WEBSIN_TRUE);
                        break;
                    case REAR_L:
                        migrationDamaged.setDamagePosC2(WEBSIN_TRUE);
                        break;
                    case REAR_R:
                        migrationDamaged.setDamagePosC3(WEBSIN_TRUE);
                        break;
                    case FRONT_L:
                        migrationDamaged.setDamagePosA2(WEBSIN_TRUE);
                        break;
                    case FRONT_R:
                        migrationDamaged.setDamagePosA3(WEBSIN_TRUE);
                        break;
                    case WINDSHIELD:
                        migrationDamaged.setDamagePosD(WEBSIN_TRUE);
                        break;
                    case REAR_WINDOW:
                        migrationDamaged.setDamagePosE(WEBSIN_TRUE);
                        break;
                    case SIDE_REAR_L:
                        migrationDamaged.setDamagePosB4(WEBSIN_TRUE);
                        break;
                    case SIDE_REAR_R:
                        migrationDamaged.setDamagePosB3(WEBSIN_TRUE);
                        break;
                    case SIDE_FRONT_L:
                        migrationDamaged.setDamagePosB2(WEBSIN_TRUE);
                        break;
                    case SIDE_FRONT_R:
                        migrationDamaged.setDamagePosB1(WEBSIN_TRUE);
                        break;
                }
            }
        }


    }

    private void updateMigrationDamagedFromCaiDetails(CaiDetails caiDetails,Integer caiPart, boolean signed, MigrationDamaged migrationDamaged){
        if(caiDetails==null){
            return;
        }
        int totCondition = 0;
        if(caiDetails.getCondition1()!=null){
            totCondition++;
            migrationDamaged.setCondition01(booleanToWebSinInteger(caiDetails.getCondition1().getValue()));
        }
        if(caiDetails.getCondition2()!=null){
            totCondition++;
            migrationDamaged.setCondition02(booleanToWebSinInteger(caiDetails.getCondition2().getValue()));
        }
        if(caiDetails.getCondition3()!=null){
            totCondition++;
            migrationDamaged.setCondition03(booleanToWebSinInteger(caiDetails.getCondition3().getValue()));
        }
        if(caiDetails.getCondition4()!=null){
            totCondition++;
            migrationDamaged.setCondition04(booleanToWebSinInteger(caiDetails.getCondition4().getValue()));
        }
        if(caiDetails.getCondition5()!=null){
            totCondition++;
            migrationDamaged.setCondition05(booleanToWebSinInteger(caiDetails.getCondition5().getValue()));
        }
        if(caiDetails.getCondition6()!=null){
            totCondition++;
            migrationDamaged.setCondition06(booleanToWebSinInteger(caiDetails.getCondition6().getValue()));
        }
        if(caiDetails.getCondition7()!=null){
            totCondition++;
            migrationDamaged.setCondition07(booleanToWebSinInteger(caiDetails.getCondition7().getValue()));
        }
        if(caiDetails.getCondition8()!=null){
            totCondition++;
            migrationDamaged.setCondition08(booleanToWebSinInteger(caiDetails.getCondition8().getValue()));
        }
        if(caiDetails.getCondition9()!=null){
            totCondition++;
            migrationDamaged.setCondition09(booleanToWebSinInteger(caiDetails.getCondition9().getValue()));
        }
        if(caiDetails.getCondition10()!=null){
            totCondition++;
            migrationDamaged.setCondition10(booleanToWebSinInteger(caiDetails.getCondition10().getValue()));
        }
        if(caiDetails.getCondition11()!=null){
            totCondition++;
            migrationDamaged.setCondition11(booleanToWebSinInteger(caiDetails.getCondition11().getValue()));
        }
        if(caiDetails.getCondition12()!=null){
            totCondition++;
            migrationDamaged.setCondition12(booleanToWebSinInteger(caiDetails.getCondition12().getValue()));
        }
        if(caiDetails.getCondition13()!=null){
            totCondition++;
            migrationDamaged.setCondition13(booleanToWebSinInteger(caiDetails.getCondition13().getValue()));
        }
        if(caiDetails.getCondition14()!=null){
            totCondition++;
            migrationDamaged.setCondition14(booleanToWebSinInteger(caiDetails.getCondition14().getValue()));
        }
        if(caiDetails.getCondition15()!=null){
            totCondition++;
            migrationDamaged.setCondition15(booleanToWebSinInteger(caiDetails.getCondition15().getValue()));
        }
        if(caiDetails.getCondition16()!=null){
            totCondition++;
            migrationDamaged.setCondition16(booleanToWebSinInteger(caiDetails.getCondition16().getValue()));
        }
        if(caiDetails.getCondition17()!=null){
            totCondition++;
            migrationDamaged.setCondition17(booleanToWebSinInteger(caiDetails.getCondition17().getValue()));
        }
        migrationDamaged.setSignature(booleanToWebSinInteger(signed));
        migrationDamaged.setTotCondition(totCondition);
        migrationDamaged.setCaiPart(caiPart);
    }

    /**
     * Trasforma da {@link Date} ad {@link XMLGregorianCalendar}.
     * in caso di errore o ser la data di input è NULL viene tornato NULL
     * @param date
     * @return
     */
    private XMLGregorianCalendar dateToXmlGregorianCalendar(Date date){
        if(date==null){
            return null;
        }
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setTime(date);
        try {
            return DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);
        } catch (DatatypeConfigurationException e) {
            LOGGER.error(e.getMessage());
        }
        return null;
    }

    /**
     * Trasforma da {@link Instant} ad {@link XMLGregorianCalendar}.
     * in caso di errore o ser la data di input è NULL viene tornato NULL
     * @param instant
     * @return
     */
    private XMLGregorianCalendar dateToXmlGregorianCalendar(Instant instant){
        if(instant==null){
            return null;
        }
        Date date = Date.from(instant);
        return dateToXmlGregorianCalendar(date);
    }


    /**
     * trasforma la Stringa dateString in una data formato {@link XMLGregorianCalendar} utilizzando
     * per il parse il formato "YYYY-mm-dd".
     * @param dateString stringa da convertire
     * @return la data in formato {@link XMLGregorianCalendar} <br></br>
     *          NULL se la dateString è null
     * @throws ParseException nel caso in cui non è possibile eseguire il parse della data.
     */
    private XMLGregorianCalendar dateToXmlGregorianCalendar(String dateString) throws ParseException {
        if(dateString == null){
            return null;
        }
        return dateToXmlGregorianCalendar(dateString, "YYYY-mm-dd");
    }


    /**
     * trasform la Stringa dateString in una data formato {@link XMLGregorianCalendar} utilizzando
     * come parser il formato passato in dateFormatString.
     *
     * @param dateString
     * @param dateFormatString
     * @return la data in formato {@link XMLGregorianCalendar} <br></br>
     *          NULL se dateString opputr dateFormatString sono NULL<br></br>
     * @throws ParseException se la dateString non è un formato valido
     */
    private XMLGregorianCalendar dateToXmlGregorianCalendar(String dateString, String dateFormatString) throws ParseException {
        if(dateString == null || dateFormatString==null){
            return null;
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormatString);
        Date date = simpleDateFormat.parse(dateString);
        return dateToXmlGregorianCalendar(date);
    }


    /**
     * Genera la stringa in formato HHmm indicando Ore e Minuti
     * a partire dal tipo {@link Date}
     * @param date
     * @return
     */
    private String extractHourMinute(Date date){
        if(date==null){
            return null;
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HHmm");
        return simpleDateFormat.format(date);
    }

    private String extractHourMinute(Instant instant){
        if(instant==null){
            return null;
        }
        Date date = Date.from(instant);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HHmm");
        return simpleDateFormat.format(date);
    }

    /**
     * Traduce un booleano nel integer corrispondente per WEBSIN.<br></br>
     * nel caso in cui parametro di input è null viene tornato null.
     * @param input
     * @return
     */
    private int booleanToWebSinInteger(Boolean input){
        if(input==null){
            return WEBSIN_FALSE;
        }
        return input? WEBSIN_TRUE : WEBSIN_FALSE;
    }

    /**
     * Crea una descrizione del {@link Vehicle}
     * @param vehicle
     * @return descrizione veicolo
     */
    private String buildVehicleDescription(Vehicle vehicle){
        if(vehicle==null){
            return "";
        }

        StringBuilder sb = new StringBuilder();
        sb.append(vehicle.getMake());
        sb.append(" ");
        sb.append(vehicle.getModel());
        sb.append(" ");
        sb.append(vehicle.getFuelType());
        return sb.toString();
    }


    private Historical findEventInHistorical(List<Historical> historicalList, EventTypeEnum eventTypeEnum){
        if(historicalList==null || eventTypeEnum == null){
            return null;
        }

        for(Historical historical : historicalList){
            if(eventTypeEnum.equals(historical.getEventType())){
                return historical;
            }
        }
        return null;
    }

    private BigDecimal nullSafeDoubleToBigDecimal(Double aDouble){
        if(aDouble==null){
            return null;
        }else{
            try {
                return BigDecimal.valueOf(aDouble);
            }catch (Exception ex){
                return null;
            }
        }
    }

}
