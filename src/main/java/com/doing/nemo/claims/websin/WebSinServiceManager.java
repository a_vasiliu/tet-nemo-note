package com.doing.nemo.claims.websin;


import com.doing.nemo.claims.controller.payload.UploadFileClaimsRequestV1;
import com.doing.nemo.claims.entity.jsonb.UploadFile;
import com.doing.nemo.claims.exception.WebSinClaimCommunicationException;
import com.doing.nemo.claims.exception.WebSinFileCommunicationException;
import com.doing.nemo.claims.websin.websinws.PutComplaintRequest;
import com.doing.nemo.claims.websin.websinws.PutComplaintResponseType;
import com.doing.nemo.claims.websin.websinws.UploadFileRequest;
import com.doing.nemo.claims.websin.websinws.UploadFileResponseType;

public interface WebSinServiceManager {

    PutComplaintResponseType putComplaint(PutComplaintRequest putComplaintRequest) throws WebSinClaimCommunicationException;

    PutComplaintRequest buildPutComplaintRequest(String claimsId);

    UploadFileRequest buildUploadFileRequest(UploadFile uploadFile, String claimsId);

    UploadFileResponseType uploadFile(UploadFileRequest uploadFileRequest) throws WebSinFileCommunicationException;
}
