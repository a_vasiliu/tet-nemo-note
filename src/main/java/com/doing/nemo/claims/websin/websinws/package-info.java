/**
 * WebSinWS 2.18.01 - Servizi Web utilizzati per comunicazione con WebSin  da Winity, Wincar, WebCar, Gestore Esterno e WebSinRepair
 * 
 */
@javax.xml.bind.annotation.XmlSchema(namespace = "http://automotivedn.com/WebsinWS", elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED)
package com.doing.nemo.claims.websin.websinws;
