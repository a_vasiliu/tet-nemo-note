
package com.doing.nemo.claims.websin.websinws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per anonymous complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PutComplaintResult" type="{http://automotivedn.com/WebsinWS}PutComplaintResponseType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "putComplaintResult"
})
@XmlRootElement(name = "PutComplaintResponse")
public class PutComplaintResponse
    implements Serializable
{

    @XmlElement(name = "PutComplaintResult")
    protected PutComplaintResponseType putComplaintResult;

    /**
     * Recupera il valore della proprietà putComplaintResult.
     * 
     * @return
     *     possible object is
     *     {@link PutComplaintResponseType }
     *     
     */
    public PutComplaintResponseType getPutComplaintResult() {
        return putComplaintResult;
    }

    /**
     * Imposta il valore della proprietà putComplaintResult.
     * 
     * @param value
     *     allowed object is
     *     {@link PutComplaintResponseType }
     *     
     */
    public void setPutComplaintResult(PutComplaintResponseType value) {
        this.putComplaintResult = value;
    }

}
