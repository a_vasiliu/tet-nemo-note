
package com.doing.nemo.claims.websin.websinws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per UploadFileRequest complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="UploadFileRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Catalog" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RefID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="OriFileName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SizeFile" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ExtFile" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FileContent" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UploadFileRequest", propOrder = {
    "catalog",
    "refID",
    "oriFileName",
    "sizeFile",
    "extFile",
    "fileContent"
})
public class UploadFileRequest
    implements Serializable
{

    @XmlElement(name = "Catalog")
    protected String catalog;
    @XmlElement(name = "RefID")
    protected int refID;
    @XmlElement(name = "OriFileName")
    protected String oriFileName;
    @XmlElement(name = "SizeFile")
    protected int sizeFile;
    @XmlElement(name = "ExtFile")
    protected String extFile;
    @XmlElement(name = "FileContent")
    protected byte[] fileContent;

    /**
     * Recupera il valore della proprietà catalog.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCatalog() {
        return catalog;
    }

    /**
     * Imposta il valore della proprietà catalog.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCatalog(String value) {
        this.catalog = value;
    }

    /**
     * Recupera il valore della proprietà refID.
     * 
     */
    public int getRefID() {
        return refID;
    }

    /**
     * Imposta il valore della proprietà refID.
     * 
     */
    public void setRefID(int value) {
        this.refID = value;
    }

    /**
     * Recupera il valore della proprietà oriFileName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriFileName() {
        return oriFileName;
    }

    /**
     * Imposta il valore della proprietà oriFileName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriFileName(String value) {
        this.oriFileName = value;
    }

    /**
     * Recupera il valore della proprietà sizeFile.
     * 
     */
    public int getSizeFile() {
        return sizeFile;
    }

    /**
     * Imposta il valore della proprietà sizeFile.
     * 
     */
    public void setSizeFile(int value) {
        this.sizeFile = value;
    }

    /**
     * Recupera il valore della proprietà extFile.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtFile() {
        return extFile;
    }

    /**
     * Imposta il valore della proprietà extFile.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtFile(String value) {
        this.extFile = value;
    }

    /**
     * Recupera il valore della proprietà fileContent.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getFileContent() {
        return fileContent;
    }

    /**
     * Imposta il valore della proprietà fileContent.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setFileContent(byte[] value) {
        this.fileContent = value;
    }

}
