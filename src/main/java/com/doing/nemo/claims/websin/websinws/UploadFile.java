
package com.doing.nemo.claims.websin.websinws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per anonymous complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Req" type="{http://automotivedn.com/WebsinWS}UploadFileRequest" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "req"
})
@XmlRootElement(name = "UploadFile")
public class UploadFile
    implements Serializable
{

    @XmlElement(name = "Req")
    protected UploadFileRequest req;

    /**
     * Recupera il valore della proprietà req.
     * 
     * @return
     *     possible object is
     *     {@link UploadFileRequest }
     *     
     */
    public UploadFileRequest getReq() {
        return req;
    }

    /**
     * Imposta il valore della proprietà req.
     * 
     * @param value
     *     allowed object is
     *     {@link UploadFileRequest }
     *     
     */
    public void setReq(UploadFileRequest value) {
        this.req = value;
    }

}
