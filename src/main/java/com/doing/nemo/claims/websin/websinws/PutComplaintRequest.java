
package com.doing.nemo.claims.websin.websinws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per PutComplaintRequest complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="PutComplaintRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MComplaint" type="{http://automotivedn.com/WebsinWS}MigrationComplaint" minOccurs="0"/>
 *         &lt;element name="MInjureds" type="{http://automotivedn.com/WebsinWS}ArrayOfMigrationInjured" minOccurs="0"/>
 *         &lt;element name="MDamageds" type="{http://automotivedn.com/WebsinWS}ArrayOfMigrationDamaged" minOccurs="0"/>
 *         &lt;element name="MRepairDossiers" type="{http://automotivedn.com/WebsinWS}ArrayOfMigrationRepairDossier" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PutComplaintRequest", propOrder = {
    "mComplaint",
    "mInjureds",
    "mDamageds",
    "mRepairDossiers"
})
public class PutComplaintRequest
    implements Serializable
{

    @XmlElement(name = "MComplaint")
    protected MigrationComplaint mComplaint;
    @XmlElement(name = "MInjureds")
    protected ArrayOfMigrationInjured mInjureds;
    @XmlElement(name = "MDamageds")
    protected ArrayOfMigrationDamaged mDamageds;
    @XmlElement(name = "MRepairDossiers")
    protected ArrayOfMigrationRepairDossier mRepairDossiers;

    /**
     * Recupera il valore della proprietà mComplaint.
     * 
     * @return
     *     possible object is
     *     {@link MigrationComplaint }
     *     
     */
    public MigrationComplaint getMComplaint() {
        return mComplaint;
    }

    /**
     * Imposta il valore della proprietà mComplaint.
     * 
     * @param value
     *     allowed object is
     *     {@link MigrationComplaint }
     *     
     */
    public void setMComplaint(MigrationComplaint value) {
        this.mComplaint = value;
    }

    /**
     * Recupera il valore della proprietà mInjureds.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfMigrationInjured }
     *     
     */
    public ArrayOfMigrationInjured getMInjureds() {
        return mInjureds;
    }

    /**
     * Imposta il valore della proprietà mInjureds.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfMigrationInjured }
     *     
     */
    public void setMInjureds(ArrayOfMigrationInjured value) {
        this.mInjureds = value;
    }

    /**
     * Recupera il valore della proprietà mDamageds.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfMigrationDamaged }
     *     
     */
    public ArrayOfMigrationDamaged getMDamageds() {
        return mDamageds;
    }

    /**
     * Imposta il valore della proprietà mDamageds.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfMigrationDamaged }
     *     
     */
    public void setMDamageds(ArrayOfMigrationDamaged value) {
        this.mDamageds = value;
    }

    /**
     * Recupera il valore della proprietà mRepairDossiers.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfMigrationRepairDossier }
     *     
     */
    public ArrayOfMigrationRepairDossier getMRepairDossiers() {
        return mRepairDossiers;
    }

    /**
     * Imposta il valore della proprietà mRepairDossiers.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfMigrationRepairDossier }
     *     
     */
    public void setMRepairDossiers(ArrayOfMigrationRepairDossier value) {
        this.mRepairDossiers = value;
    }

}
