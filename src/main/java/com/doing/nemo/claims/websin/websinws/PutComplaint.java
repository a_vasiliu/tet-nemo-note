
package com.doing.nemo.claims.websin.websinws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per anonymous complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ReqPut" type="{http://automotivedn.com/WebsinWS}PutComplaintRequest" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "reqPut"
})
@XmlRootElement(name = "PutComplaint")
public class PutComplaint
    implements Serializable
{

    @XmlElement(name = "ReqPut")
    protected PutComplaintRequest reqPut;

    /**
     * Recupera il valore della proprietà reqPut.
     * 
     * @return
     *     possible object is
     *     {@link PutComplaintRequest }
     *     
     */
    public PutComplaintRequest getReqPut() {
        return reqPut;
    }

    /**
     * Imposta il valore della proprietà reqPut.
     * 
     * @param value
     *     allowed object is
     *     {@link PutComplaintRequest }
     *     
     */
    public void setReqPut(PutComplaintRequest value) {
        this.reqPut = value;
    }

}
