
package com.doing.nemo.claims.websin.websinws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per PutComplaintResponseType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="PutComplaintResponseType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://automotivedn.com/WebsinWS}ResponseWS">
 *       &lt;sequence>
 *         &lt;element name="ComplaintID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PutComplaintResponseType", propOrder = {
    "complaintID"
})
public class PutComplaintResponseType
    extends ResponseWS
    implements Serializable
{

    @XmlElement(name = "ComplaintID")
    protected int complaintID;

    /**
     * Recupera il valore della proprietà complaintID.
     * 
     */
    public int getComplaintID() {
        return complaintID;
    }

    /**
     * Imposta il valore della proprietà complaintID.
     * 
     */
    public void setComplaintID(int value) {
        this.complaintID = value;
    }

}
