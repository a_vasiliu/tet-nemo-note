
package com.doing.nemo.claims.websin.websinws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per anonymous complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="UploadFileResult" type="{http://automotivedn.com/WebsinWS}UploadFileResponseType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "uploadFileResult"
})
@XmlRootElement(name = "UploadFileResponse")
public class UploadFileResponse
    implements Serializable
{

    @XmlElement(name = "UploadFileResult")
    protected UploadFileResponseType uploadFileResult;

    /**
     * Recupera il valore della proprietà uploadFileResult.
     * 
     * @return
     *     possible object is
     *     {@link UploadFileResponseType }
     *     
     */
    public UploadFileResponseType getUploadFileResult() {
        return uploadFileResult;
    }

    /**
     * Imposta il valore della proprietà uploadFileResult.
     * 
     * @param value
     *     allowed object is
     *     {@link UploadFileResponseType }
     *     
     */
    public void setUploadFileResult(UploadFileResponseType value) {
        this.uploadFileResult = value;
    }

}
