
package com.doing.nemo.claims.websin.websinws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java per MigrationInjured complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="MigrationInjured">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="DamagedID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="InjType" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="InjSurname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InjName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InjAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InjCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InjProvince" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InjZipCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InjCountry" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InjFiscalCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InjBirthDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="InjBirthCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InjER" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="InjERName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InjERCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MigrationInjured", propOrder = {
    "id",
    "damagedID",
    "injType",
    "injSurname",
    "injName",
    "injAddress",
    "injCity",
    "injProvince",
    "injZipCode",
    "injCountry",
    "injFiscalCode",
    "injBirthDate",
    "injBirthCity",
    "injER",
    "injERName",
    "injERCity"
})
public class MigrationInjured
    implements Serializable
{

    @XmlElement(name = "ID")
    protected int id;
    @XmlElement(name = "DamagedID")
    protected int damagedID;
    @XmlElement(name = "InjType")
    protected int injType;
    @XmlElement(name = "InjSurname")
    protected String injSurname;
    @XmlElement(name = "InjName")
    protected String injName;
    @XmlElement(name = "InjAddress")
    protected String injAddress;
    @XmlElement(name = "InjCity")
    protected String injCity;
    @XmlElement(name = "InjProvince")
    protected String injProvince;
    @XmlElement(name = "InjZipCode")
    protected String injZipCode;
    @XmlElement(name = "InjCountry")
    protected String injCountry;
    @XmlElement(name = "InjFiscalCode")
    protected String injFiscalCode;
    @XmlElement(name = "InjBirthDate", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar injBirthDate;
    @XmlElement(name = "InjBirthCity")
    protected String injBirthCity;
    @XmlElement(name = "InjER", required = true, type = Integer.class, nillable = true)
    protected Integer injER;
    @XmlElement(name = "InjERName")
    protected String injERName;
    @XmlElement(name = "InjERCity")
    protected String injERCity;

    /**
     * Recupera il valore della proprietà id.
     * 
     */
    public int getID() {
        return id;
    }

    /**
     * Imposta il valore della proprietà id.
     * 
     */
    public void setID(int value) {
        this.id = value;
    }

    /**
     * Recupera il valore della proprietà damagedID.
     * 
     */
    public int getDamagedID() {
        return damagedID;
    }

    /**
     * Imposta il valore della proprietà damagedID.
     * 
     */
    public void setDamagedID(int value) {
        this.damagedID = value;
    }

    /**
     * Recupera il valore della proprietà injType.
     * 
     */
    public int getInjType() {
        return injType;
    }

    /**
     * Imposta il valore della proprietà injType.
     * 
     */
    public void setInjType(int value) {
        this.injType = value;
    }

    /**
     * Recupera il valore della proprietà injSurname.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInjSurname() {
        return injSurname;
    }

    /**
     * Imposta il valore della proprietà injSurname.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInjSurname(String value) {
        this.injSurname = value;
    }

    /**
     * Recupera il valore della proprietà injName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInjName() {
        return injName;
    }

    /**
     * Imposta il valore della proprietà injName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInjName(String value) {
        this.injName = value;
    }

    /**
     * Recupera il valore della proprietà injAddress.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInjAddress() {
        return injAddress;
    }

    /**
     * Imposta il valore della proprietà injAddress.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInjAddress(String value) {
        this.injAddress = value;
    }

    /**
     * Recupera il valore della proprietà injCity.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInjCity() {
        return injCity;
    }

    /**
     * Imposta il valore della proprietà injCity.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInjCity(String value) {
        this.injCity = value;
    }

    /**
     * Recupera il valore della proprietà injProvince.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInjProvince() {
        return injProvince;
    }

    /**
     * Imposta il valore della proprietà injProvince.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInjProvince(String value) {
        this.injProvince = value;
    }

    /**
     * Recupera il valore della proprietà injZipCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInjZipCode() {
        return injZipCode;
    }

    /**
     * Imposta il valore della proprietà injZipCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInjZipCode(String value) {
        this.injZipCode = value;
    }

    /**
     * Recupera il valore della proprietà injCountry.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInjCountry() {
        return injCountry;
    }

    /**
     * Imposta il valore della proprietà injCountry.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInjCountry(String value) {
        this.injCountry = value;
    }

    /**
     * Recupera il valore della proprietà injFiscalCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInjFiscalCode() {
        return injFiscalCode;
    }

    /**
     * Imposta il valore della proprietà injFiscalCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInjFiscalCode(String value) {
        this.injFiscalCode = value;
    }

    /**
     * Recupera il valore della proprietà injBirthDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getInjBirthDate() {
        return injBirthDate;
    }

    /**
     * Imposta il valore della proprietà injBirthDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setInjBirthDate(XMLGregorianCalendar value) {
        this.injBirthDate = value;
    }

    /**
     * Recupera il valore della proprietà injBirthCity.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInjBirthCity() {
        return injBirthCity;
    }

    /**
     * Imposta il valore della proprietà injBirthCity.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInjBirthCity(String value) {
        this.injBirthCity = value;
    }

    /**
     * Recupera il valore della proprietà injER.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getInjER() {
        return injER;
    }

    /**
     * Imposta il valore della proprietà injER.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setInjER(Integer value) {
        this.injER = value;
    }

    /**
     * Recupera il valore della proprietà injERName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInjERName() {
        return injERName;
    }

    /**
     * Imposta il valore della proprietà injERName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInjERName(String value) {
        this.injERName = value;
    }

    /**
     * Recupera il valore della proprietà injERCity.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInjERCity() {
        return injERCity;
    }

    /**
     * Imposta il valore della proprietà injERCity.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInjERCity(String value) {
        this.injERCity = value;
    }

}
