package com.doing.nemo.claims.common.util.operation.impl;

import com.doing.nemo.claims.common.util.operation.Operation;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.InternalException;
import org.apache.commons.lang3.StringUtils;
import org.flywaydb.core.Flyway;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.Driver;

@Component("DatabaseMigrationOperation")
public class DatabaseMigrationOperation implements Operation {

    private static final Logger LOGGER = LoggerFactory.getLogger(DatabaseMigrationOperation.class);

    @Value("${spring.datasource.driverClassName:#{null}}")
    private String dbDriver;

    @Value("${spring.datasource.url:#{null}}")
    private String dbURL;

    @Value(("${spring.datasource.username:#{null}}"))
    private String username;

    @Value(("${spring.datasource.password:#{null}}"))
    private String password;

    @Value("${migration.init:false}")
    private boolean initOnMigrate;

    @Value("${migration.outOfOrder}")
    private boolean outOfOrder;

    @Override
    public void run() throws Exception {

        if (StringUtils.isAnyEmpty(dbDriver, dbURL)) {
            throw new InternalException("You need to correctly define the following system properties: "
                    + "'spring.datasource.driverClassName', "
                    + "'spring.datasource.url'", MessageCode.CLAIMS_2000);
        }

        if(username != null && password != null){
            migrate(dbDriver, dbURL, username, password);
        }else {
            migrate(dbDriver, dbURL);
        }

        LOGGER.debug("Migration completed!");
    }

    private void migrate(String driverName, String jdbcUrl) throws Exception {
        Class driver = Class.forName(driverName);
        Driver jdbcDriver = (Driver) driver.newInstance();
        DataSource dataSource = new SimpleDriverDataSource(jdbcDriver, jdbcUrl);
        Flyway flyway = new Flyway();
        flyway.setDataSource(dataSource);
        flyway.setInitOnMigrate(initOnMigrate);
        flyway.setOutOfOrder(outOfOrder);
        flyway.migrate();
    }

    private void migrate(String driverName, String jdbcUrl, String username, String password) throws Exception {
        Class driver = Class.forName(driverName);
        Driver jdbcDriver = (Driver) driver.newInstance();
        DataSource dataSource = new SimpleDriverDataSource(jdbcDriver, jdbcUrl, username, password);
        Flyway flyway = new Flyway();
        flyway.setDataSource(dataSource);
        flyway.setOutOfOrder(outOfOrder);
        flyway.setInitOnMigrate(initOnMigrate);
        flyway.migrate();
    }
}
