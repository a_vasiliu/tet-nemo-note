package com.doing.nemo.claims.common.commandBus;

import com.doing.nemo.claims.exception.ForbiddenException;
import com.doing.nemo.claims.validation.MessageCode;
import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.beans.Introspector;
import java.io.IOException;
import java.util.Map;

@Service
public class CommandBus {

    @Autowired
    private ListableBeanFactory beanFactory;

    private Map<String, CommandHandler> commandHandlerMap;

    @PostConstruct
    public void init() {
        commandHandlerMap = beanFactory.getBeansOfType(CommandHandler.class);

    }

    public void execute(Command command) throws IOException {

        String commandHandlerName = Introspector.decapitalize(command.getClass().getSimpleName() + "Handler");

        CommandHandler commandHandler = commandHandlerMap.get(commandHandlerName);

        if (commandHandler == null) {
            throw new ForbiddenException.IllegalArgumentException("Command Handler not found for [" + commandHandlerName + "]", MessageCode.CLAIMS_1010);
        }

        commandHandler.handle(command);
    }
}
