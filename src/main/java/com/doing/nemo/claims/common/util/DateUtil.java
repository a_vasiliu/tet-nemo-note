package com.doing.nemo.claims.common.util;

import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.DateTimeParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

@Component
public class DateUtil {

    public static String convertUTCDateToIS08601String(Date date) {

        if (date == null) {
            return null;
        }

        LocalDateTime ldt = LocalDateTime.ofInstant(date.toInstant(), ZoneOffset.UTC);
        ldt = ldt.withNano(0);

        ZonedDateTime dateTime = ZonedDateTime.ofInstant(ldt.toInstant(ZoneOffset.UTC), ZoneId.of("UCT"));
        return dateTime.format(new DateTimeFormatterBuilder().append(DateTimeFormatter.ISO_LOCAL_DATE_TIME).optionalStart().appendOffset("+HH:MM", "+00:00").optionalEnd().toFormatter());
    }

    public static Date convertIS08601StringToUTCDate(String dateString) {

        if (dateString == null || dateString.isEmpty()) {
            return null;
        }
        try{

            dateString = dateString.replace("+0000", "Z");
            LocalDateTime dateTime = LocalDateTime.parse(dateString, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
            dateTime = dateTime.withNano(0);
            return Date.from(dateTime.toInstant(ZoneOffset.UTC));

        }catch (DateTimeParseException e){

            LocalDateTime dateTime = LocalDateTime.parse(dateString, DateTimeFormatter.ISO_INSTANT);
            dateTime = dateTime.withNano(0);
            return Date.from(dateTime.toInstant(ZoneOffset.UTC));
        }
    }

    public static String convertUTCInstantToIS08601String(Instant date) {

        if (date == null) {
            return null;
        }

        LocalDateTime ldt = LocalDateTime.ofInstant(date, ZoneOffset.UTC);
        ldt = ldt.withNano(0);

        ZonedDateTime dateTime = ZonedDateTime.ofInstant(ldt.toInstant(ZoneOffset.UTC), ZoneId.of("UCT"));
        return dateTime.format(new DateTimeFormatterBuilder().append(DateTimeFormatter.ISO_LOCAL_DATE_TIME).optionalStart().appendOffset("+HH:MM", "+00:00").optionalEnd().toFormatter());
    }

    public static Instant convertIS08601StringToUTCInstant(String dateString) {

        if (dateString == null || dateString.isEmpty()) {
            return null;
        }

        try{

            dateString = dateString.replace("+0000", "Z");
            LocalDateTime dateTime = LocalDateTime.parse(dateString, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
            return Instant.from(dateTime.toInstant(ZoneOffset.UTC));

        }catch (DateTimeParseException e){

            Instant dateTime = Instant.parse(dateString);
            return Instant.from(dateTime.atOffset(ZoneOffset.UTC));
        }
    }

    public static Date getNowDate() {

        LocalDateTime dateTime = LocalDateTime.parse(LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
        return Date.from(dateTime.toInstant(ZoneOffset.UTC));
    }

    public static Instant getNowInstant() {

        LocalDateTime dateTime = LocalDateTime.parse(LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
        dateTime = dateTime.withNano(0);
        return dateTime.toInstant(ZoneOffset.UTC);
    }

    //Data una DATA in formato 01/01/2019 la trasformo in 1012019 senza caratteri separatori
    public static String getDateStringWithoutSeparationCharacters(Date date){

        if(date  == null)
            date = getNowDate();

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        Integer day = calendar.get(Calendar.DAY_OF_MONTH);
        Integer month = calendar.get(Calendar.MONTH) + 1;
        Integer year = calendar.get(Calendar.YEAR);

        String dateString = day.toString();
        if(month<10)
            dateString += "0";

        return dateString + month.toString() + year.toString();
    }

    public static String getDateStringWithoutSeparationCharacters(Instant instant){
        return getDateStringWithoutSeparationCharacters(Date.from(instant));
    }

    public static String getDateStringWithSeparationCharacters(Date date){

        if(date  == null)
            date = getNowDate();

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));
        return simpleDateFormat.format(date);
        /*Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        Integer day = calendar.get(Calendar.DAY_OF_MONTH);
        Integer month = calendar.get(Calendar.MONTH) + 1;
        Integer year = calendar.get(Calendar.YEAR);

        String dateString = day.toString();
        String monthString = month.toString();
        if(month<10)
            monthString = "0" + monthString;

        return dateString +"/"+ monthString +"/"+ year.toString();*/
    }

    public static LocalDate convertIS08601StringToLocalDate(String dateString){

        if(dateString == null)
            return null;

        Instant instantDate = convertIS08601StringToUTCInstant(dateString);
        if(instantDate == null){
            return null;
        }
        return instantDate.atZone(ZoneId.systemDefault()).toLocalDate();

    }

    public static LocalDate convertUTCDateToLocalDate(Date date){

        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

    }

    public static LocalDate convertUTCInstantToLocalDate(Instant date){

        return date.atZone(ZoneId.systemDefault()).toLocalDate();

    }

    public static LocalDateTime convertUTCDateToLocalDateTime(Date date){

        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();

    }

    public static LocalDateTime convertUTCInstantToLocalDateTime(Instant date){

        return date.atZone(ZoneId.systemDefault()).toLocalDateTime();

    }

    public static Date convertLocalDateToDate(LocalDate localDate){

        return Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());

    }

    public static Instant convertLocalDateToInstant(LocalDate localDate){

        return localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant();
    }


    public static Date convertLocalDateToDate(LocalDateTime localDate){

        return Date.from(localDate.atZone(ZoneId.systemDefault()).toInstant());

    }

    public static Instant convertLocalDateToInstant(LocalDateTime localDate){

        return localDate.atZone(ZoneId.systemDefault()).toInstant();
    }


    public static String convertLocalDateToString(LocalDate localDate){

        return localDate.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
    }

    public static String convertLocalDateTimeToIS08601String(LocalDateTime localDateTime){

        localDateTime = localDateTime.withNano(0);

        ZonedDateTime dateTime = ZonedDateTime.ofInstant(localDateTime.toInstant(ZoneOffset.UTC), ZoneId.of("UCT"));
        return dateTime.format(new DateTimeFormatterBuilder().append(DateTimeFormatter.ISO_LOCAL_DATE_TIME).optionalStart().appendOffset("+HH:MM", "+00:00").optionalEnd().toFormatter());

    }

}
