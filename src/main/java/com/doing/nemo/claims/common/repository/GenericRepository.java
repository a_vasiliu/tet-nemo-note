package com.doing.nemo.claims.common.repository;

import com.doing.nemo.claims.dto.FilterView;
import com.doing.nemo.claims.entity.view.SearchDashboardClaimsView;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

@NoRepositoryBean
public interface GenericRepository<T, ID extends Serializable> extends JpaRepository<T, ID>, JpaSpecificationExecutor<T> {

    <S extends T> S save(S entity);

    Optional<T> findById(ID primaryKey);

    List<T> findAll();

    long count();

    void delete(T entity);

    boolean existsById(ID primaryKey);

    List<T> findAll(Sort sort);

    Page<T> findAll(Pageable pageable);


}