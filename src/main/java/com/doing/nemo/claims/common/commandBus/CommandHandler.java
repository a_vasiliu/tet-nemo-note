package com.doing.nemo.claims.common.commandBus;

import java.io.IOException;

public interface CommandHandler<C extends Command> {
    void handle(Command c) throws IOException;
}
