package com.doing.nemo.claims.common.config;

import com.doing.nemo.claims.common.controller.payload.GitInfo;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource(value = "classpath:git.properties", ignoreResourceNotFound = true)
public class GitInfoConfig {

    @Value("${git.branch:#{null}}")
    private String gitBranch;

    @Value("${git.commit.id.abbrev:#{null}}")
    private String gitCommitId;

    @Value("${git.commit.time:#{null}}")
    private String gitCommitDate;

    @Value("${git.build.time:#{null}}")
    private String gitBuildDate;

    @Bean
    public GitInfo gitInfo() {

        GitInfo info = new GitInfo();
        info.setGitBranch(gitBranch);
        info.setGitCommitId(gitCommitId);
        info.setGitCommitDate(gitCommitDate);
        info.setGitBuildDate(gitBuildDate);

        return info;
    }
}