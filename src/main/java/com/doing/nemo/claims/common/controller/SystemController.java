package com.doing.nemo.claims.common.controller;

import com.doing.nemo.claims.common.controller.payload.GitInfo;
import com.doing.nemo.claims.common.controller.payload.PingResponse;
import com.doing.nemo.claims.common.controller.payload.VersionResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
@CrossOrigin
@RequestMapping(value = "/system")
public class SystemController {

    @Value("${info.app.groupId}")
    private String groupId;

    @Value("${info.app.artifactId}")
    private String artifactId;

    @Value("${info.app.name}")
    private String name;

    @Value("${info.app.version}")
    private String version;

    @Autowired
    private GitInfo gitInfo;

    @GetMapping("/ping")
    public ResponseEntity<PingResponse> getPing() {

        return new ResponseEntity<>(new PingResponse(new Date()), HttpStatus.OK);
    }

    @GetMapping("/version")
    public ResponseEntity<VersionResponse> getVersion() {

        VersionResponse response = new VersionResponse(groupId, artifactId, name, version);
        response.enrichWithGitInfo(gitInfo);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
