package com.doing.nemo.claims.common.controller.payload;

public class GitInfo {

    private String gitBranch;
    private String gitCommitId;
    private String gitCommitDate;
    private String gitBuildDate;

    public String getGitBranch() {
        return gitBranch;
    }

    public void setGitBranch(String gitBranch) {
        this.gitBranch = gitBranch;
    }

    public String getGitCommitId() {
        return gitCommitId;
    }

    public void setGitCommitId(String gitCommitId) {
        this.gitCommitId = gitCommitId;
    }

    public String getGitCommitDate() {
        return gitCommitDate;
    }

    public void setGitCommitDate(String gitCommitDate) {
        this.gitCommitDate = gitCommitDate;
    }

    public String getGitBuildDate() {
        return gitBuildDate;
    }

    public void setGitBuildDate(String gitBuildDate) {
        this.gitBuildDate = gitBuildDate;
    }
}
