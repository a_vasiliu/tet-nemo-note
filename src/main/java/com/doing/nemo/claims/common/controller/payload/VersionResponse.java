package com.doing.nemo.claims.common.controller.payload;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class VersionResponse {

    @JsonProperty("group-id")
    private String groupId;

    @JsonProperty("artifact-id")
    private String artifactId;

    @JsonProperty("name")
    private String name;

    @JsonProperty("version")
    private String version;

    @JsonProperty("git-branch")
    private String gitBranch;

    @JsonProperty("git-commit-id")
    private String gitCommitId;

    @JsonProperty("git-commit-date")
    private String gitCommitDate;

    @JsonProperty("git-build-date")
    private String gitBuildDate;


    public VersionResponse(String groupId, String artifactId, String name, String version) {
        this.groupId = groupId;
        this.artifactId = artifactId;
        this.name = name;
        this.version = version;
    }


    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getArtifactId() {
        return artifactId;
    }

    public void setArtifactId(String artifactId) {
        this.artifactId = artifactId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public void enrichWithGitInfo(GitInfo gitInfo) {

        if (gitInfo != null) {
            this.gitBranch = gitInfo.getGitBranch();
            this.gitCommitId = gitInfo.getGitCommitId();
            this.gitCommitDate = gitInfo.getGitCommitDate();
            this.gitBuildDate = gitInfo.getGitBuildDate();
        }
    }
}
