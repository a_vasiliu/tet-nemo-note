package com.doing.nemo.claims.common.util;


import com.doing.nemo.commons.exception.AbstractException;
import com.doing.nemo.commons.exception.InternalException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

@Component
public class ServiceUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(ServiceUtil.class);

    private static final String SORT_FIELD_DIRECTION_SEPARATOR = "__";


    /* Build JPA pagination checking input parameters */
    public Pageable buildPagination(Integer page, Integer pageSize, String orderBy, Boolean asc,
                                    Integer defaultPage, Integer defaultPageSize, String defaultOrderByField, Boolean defaultASCOrder,
                                    Map<String, String> supportedOrderByFields) {

        if (!ObjectUtils.allNotNull(defaultPage, defaultPageSize, defaultASCOrder, supportedOrderByFields) || StringUtils.isEmpty(defaultOrderByField)) {

            String message = "Required no empty fields: 'defaultPage', 'defaultPageSize', 'defaultASCOrder', 'supportedOrderByFields', 'defaultOrderByField'";
            LOGGER.error(message);
            throw new InternalException(message);
        }

        // 0 base paging for Spring Repository
        page = (page != null && page > 0) ? page - 1 : defaultPage;

        if (pageSize != null && pageSize == -1) {
            pageSize = Integer.MAX_VALUE;
        } else if (pageSize == null || pageSize < 1) {
            pageSize = defaultPageSize;
        }

        if (asc == null) {
            asc = defaultASCOrder;
        }

        if (orderBy == null || supportedOrderByFields.get(orderBy) == null) {
            orderBy = defaultOrderByField;
        } else {
            orderBy = supportedOrderByFields.get(orderBy);
        }

        Sort.Direction sortDirection = asc ? Sort.Direction.ASC : Sort.Direction.DESC;
        Sort sort = Sort.by(new Sort.Order(sortDirection, orderBy));
        return PageRequest.of(page, pageSize, sort);
    }

    /* Build JPA pagination checking input parameters */
    public Pageable buildPaginationWithFinalDefaultSort(Integer page, Integer pageSize, String orderBy, Boolean asc,
                                                        Integer defaultPage, Integer defaultPageSize, String defaultOrderByField, Boolean defaultASCOrder,
                                                        Map<String, String> supportedOrderByFields, Map<String, String> defaultSortToAppend) {

        if (!ObjectUtils.allNotNull(defaultPage, defaultPageSize, defaultASCOrder, supportedOrderByFields, defaultSortToAppend) || StringUtils.isEmpty(defaultOrderByField)) {

            String message = "Required no empty fields: 'defaultPage', 'defaultPageSize', 'defaultASCOrder', 'supportedOrderByFields', 'defaultOrderByField'";
            LOGGER.error(message);
            throw new InternalException(message);
        }

        // 0 base paging for Spring Repository
        page = (page != null && page > 0) ? page - 1 : defaultPage;

        if (pageSize != null && pageSize == -1) {
            pageSize = Integer.MAX_VALUE;
        } else if (pageSize == null || pageSize < 1) {
            pageSize = defaultPageSize;
        }

        if (asc == null) {
            asc = defaultASCOrder;
        }

        if (orderBy == null || supportedOrderByFields.get(orderBy) == null) {
            orderBy = defaultOrderByField;
        } else {
            orderBy = supportedOrderByFields.get(orderBy);
        }

        List<Sort.Order> sortOrders = new ArrayList<>(defaultSortToAppend.size() + 1);
        Sort.Direction sortDirection = asc ? Sort.Direction.ASC : Sort.Direction.DESC;
        sortOrders.add(new Sort.Order(sortDirection, orderBy));

        String finalOrderBy = orderBy;
        defaultSortToAppend.forEach((key, value) -> {
            if (StringUtils.isNotBlank(key) && !key.equalsIgnoreCase(finalOrderBy)) {
                Optional<Sort.Direction> optionalDirection = StringUtils.isNotBlank(value) ? Sort.Direction.fromOptionalString(value) : Optional.empty();
                sortOrders.add(new Sort.Order(optionalDirection.orElse(sortDirection), key));
            }
        });

        Sort sort = Sort.by(sortOrders);

        return PageRequest.of(page, pageSize, sort);
    }

    /* Build JPA pagination with multiple sort */
    public Pageable buildPagination(Integer page, Integer pageSize, Set<String> sortFieldsWithDirection, Boolean asc,
                                    Integer defaultPage, Integer defaultPageSize, Map<String, String> defaultSortFields,
                                    Boolean defaultASCOrder, Map<String, String> supportedSortFields) {

        if (!ObjectUtils.allNotNull(defaultPage, defaultPageSize, defaultASCOrder, supportedSortFields) || MapUtils.isEmpty(defaultSortFields)) {

            String message = "Required no empty fields: 'defaultPage', 'defaultPageSize', 'defaultASCOrder', 'supportedSortFields', 'defaultSortFields'";
            LOGGER.error(message);
            throw new InternalException(message);
        }

        // 0 base paging for Spring Repository
        page = (page != null && page > 0) ? page - 1 : defaultPage;

        if (pageSize != null && pageSize == -1) {
            pageSize = Integer.MAX_VALUE;
        } else if (pageSize == null || pageSize < 1) {
            pageSize = defaultPageSize;
        }

        if (asc == null) {
            asc = defaultASCOrder;
        }

        List<Sort.Order> sortOrders = new ArrayList<>(5);
        Sort.Direction defaultSortDirection = asc ? Sort.Direction.ASC : Sort.Direction.DESC;

        if(CollectionUtils.isNotEmpty(sortFieldsWithDirection)) {
            sortFieldsWithDirection.forEach(item -> {
                String[] splitString = item.split(SORT_FIELD_DIRECTION_SEPARATOR);
                String orderBy = (ArrayUtils.isNotEmpty(splitString) &&  StringUtils.isNotBlank(splitString[0])) ? supportedSortFields.get(splitString[0]) : null;
                if(StringUtils.isNotBlank(orderBy)) {
                    Optional<Sort.Direction> optionalDirection = splitString.length > 1 ? Sort.Direction.fromOptionalString(splitString[1]) : Optional.empty();
                    sortOrders.add(new Sort.Order(optionalDirection.orElse(defaultSortDirection), orderBy));
                }
            });
        }

        if(CollectionUtils.isEmpty(sortFieldsWithDirection) || CollectionUtils.isEmpty(sortOrders)) {
            defaultSortFields.forEach((key, value) -> {
                Optional<Sort.Direction> optionalDirection = Sort.Direction.fromOptionalString(value);
                sortOrders.add(new Sort.Order(optionalDirection.orElse(defaultSortDirection), key));
            });

        }
        Sort sort = Sort.by(sortOrders);
        return PageRequest.of(page, pageSize, sort);
    }

    /* Retrieve Object from Future */
    public <T> T getCompletedFromFuture(Future<T> future) {

        if (future == null) {
            return null;
        }

        try {
            return future.get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof AbstractException) {
                throw (AbstractException) e.getCause();
            } else {
                LOGGER.error(e.getMessage(), e);
                throw new InternalException(e.getMessage(), e);
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new InternalException(e.getMessage(), e);
        }
    }


    /* Retrieve Lsit of Object from List of Future */
    public <T> List<T> getCompletedFromFuture(List<CompletableFuture<T>> futures) {

        CompletableFuture[] cfs = futures.toArray(new CompletableFuture[0]);

        return getCompletedFromFuture(CompletableFuture.allOf(cfs)
                .thenApply(ignored -> futures.stream()
                        .map(CompletableFuture::join)
                        .collect(Collectors.toList())
                ));
    }
}

