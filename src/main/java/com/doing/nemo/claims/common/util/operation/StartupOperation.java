package com.doing.nemo.claims.common.util.operation;

import com.doing.nemo.commons.exception.InternalException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class StartupOperation {

    private static Logger LOGGER = LoggerFactory.getLogger(StartupOperation.class);

    @Value("${migration.enable:false}")
    private boolean migrationEnabled;

    @Autowired(required = false)
    @Qualifier("DatabaseMigrationOperation")
    private Operation databaseMigration;

    @EventListener(ApplicationReadyEvent.class)
    public void runOnStartup() throws Exception {

        LOGGER.debug("Check startup operations...");

        if (!migrationEnabled) {
            LOGGER.debug("No startup operations to execute");
            return;
        }

        if (migrationEnabled) {
            if (databaseMigration == null) {
                throw new InternalException("Required 'DatabaseMigrationOperation' Component to execute Database Migration");
            }
            LOGGER.debug("Start Database migration");
            databaseMigration.run();
            LOGGER.debug("Stop Database migration");
        }

        // Stop Spring Application to avoid forgetting operations properties on true mode
        System.exit(0);
    }
}