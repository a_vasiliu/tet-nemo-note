package com.doing.nemo.claims.common.util.operation;

public interface Operation {
    void run() throws Exception;
}
