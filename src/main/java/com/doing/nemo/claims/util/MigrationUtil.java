package com.doing.nemo.claims.util;

import com.doing.nemo.claims.entity.jsonb.forms.Forms;
import com.doing.nemo.claims.entity.jsonb.forms.jsonbForms.Attachment;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
@Component
public class MigrationUtil {
    private static Logger LOGGER = LoggerFactory.getLogger(MigrationUtil.class);

    public static Forms getDistinctAttachmentList(Forms claimsNewForms){
        Forms forms = new Forms();
        List<Attachment> attachmentList = null;
        if (claimsNewForms==null) return null;

        if (claimsNewForms.getAttachment()!=null&&!claimsNewForms.getAttachment().isEmpty()){
            attachmentList = claimsNewForms.getAttachment()
                    .stream()
                    .filter( distinctByNameFile(p -> p.getFileManagerName()) )
                    .collect( Collectors.toList() );
            forms.setAttachment(attachmentList);
        }
        return forms;
    }

    //Utility function
    public static <T> Predicate<T> distinctByNameFile(Function<? super T, Object> keyExtractor)
    {
        Map<Object, Boolean> map = new ConcurrentHashMap<>();
        return t -> map.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }
}
