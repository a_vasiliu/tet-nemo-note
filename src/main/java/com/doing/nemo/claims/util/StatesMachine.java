package com.doing.nemo.claims.util;

import com.doing.nemo.claims.command.ClaimsPatchStatusCommand;
import com.doing.nemo.claims.controller.payload.request.CounterpartyRequest;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsFlowEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsUserEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.jsonb.damaged.Damaged;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.InsuranceCompany;
import com.doing.nemo.claims.service.InsuranceCompanyService;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.BadParametersException;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class StatesMachine {

    private static Logger LOGGER = LoggerFactory.getLogger(StatesMachine.class);
    @Autowired
    private InsuranceCompanyService insuranceCompanyService;

    public void changeStatus(ClaimsEntity claimsEntity, ClaimsPatchStatusCommand claimsPatchStatusCommand) {

        ClaimsFlowEnum typeEntity = claimsEntity.getType();
        ClaimsStatusEnum statusEntity = claimsEntity.getStatus();
        ClaimsStatusEnum nextStatus = claimsPatchStatusCommand.getNextStatus();

        try {
            if (statusEntity.equals(ClaimsStatusEnum.DRAFT) && nextStatus.equals(ClaimsStatusEnum.WAITING_FOR_VALIDATION)) {
                statusEntity = checkClaimsInDraftStatus(claimsEntity);
            } else if (claimsEntity.getComplaint().getDataAccident().getTypeAccident().equals(DataAccidentTypeAccidentEnum.FURTO_TOTALE)) {
                statusEntity = checkNextStatusTheft(claimsEntity, nextStatus);
            } else if (typeEntity.equals(ClaimsFlowEnum.FUL)) {
                statusEntity = checkNextStatusFULL(claimsEntity, nextStatus, claimsPatchStatusCommand.getPracticeManager());
            } else if (typeEntity.equals(ClaimsFlowEnum.FNI)) {
                statusEntity = checkNextStatusFNI(claimsEntity, nextStatus);
            } else if (typeEntity.equals(ClaimsFlowEnum.FCM)) {
                statusEntity = checkNextStatusFCM(claimsEntity, nextStatus, claimsPatchStatusCommand.getPracticeManager());
            }

            claimsEntity.setStatus(statusEntity);

        } catch (BadParametersException ex) {
            LOGGER.debug("StateMachine : " + ex.getMessage());
            throw new BadParametersException(MessageCode.CLAIMS_2000, ex);
        }
    }

    private ClaimsStatusEnum checkClaimsInDraftStatus(ClaimsEntity claimsEntity) {

        Damaged damaged = claimsEntity.getDamaged();
        if (damaged != null) {
            InsuranceCompany insuranceCompany = damaged.getInsuranceCompany();
            if (insuranceCompany != null) {
                String denomination = insuranceCompany.getTpl().getCompany();

                insuranceCompany.setCard(insuranceCompanyService.getCardCompanyByName(denomination));
                return ClaimsStatusEnum.WAITING_FOR_VALIDATION;

            }
        }
        LOGGER.debug("in this calims, Damaged is not present.");
        throw new NotFoundException("in this calims, Damaged is not present.", MessageCode.CLAIMS_1010);
    }

    //Cambio stato pratica con Furto
    private static ClaimsStatusEnum checkNextStatusTheft(ClaimsEntity claimsEntity, ClaimsStatusEnum nextStatus) {

        ClaimsStatusEnum currentStatus = claimsEntity.getStatus();
        String error = "In this flow, you can not go from " + currentStatus.getValue() + " state to ";

        switch (currentStatus) {

            case WAITING_FOR_AUTHORITY:
                if (nextStatus.equals(ClaimsStatusEnum.WAIT_LOST_POSSESSION) ||
                        nextStatus.equals(ClaimsStatusEnum.CLOSED)) {
                    return nextStatus;
                }
                LOGGER.debug(error + nextStatus.getValue() + " state");
                throw new BadParametersException(error + nextStatus.getValue() + " state", MessageCode.CLAIMS_2000);

            case WAIT_LOST_POSSESSION:
                if (nextStatus.equals(ClaimsStatusEnum.CLOSED)) {
                    return nextStatus;
                }
                LOGGER.debug(error + nextStatus.getValue() + " state");
                throw new BadParametersException(error + nextStatus.getValue() + " state", MessageCode.CLAIMS_2000);

            case CLOSED:
                //Si deve effettuare un controllo sulle date per poter permettere queso tipo di passaggio di stato
                if (nextStatus.equals(ClaimsStatusEnum.RETURN_TO_POSSESSION)) {
                    return nextStatus;
                }
                LOGGER.debug(error + nextStatus.getValue() + " state");
                throw new BadParametersException(error + nextStatus.getValue() + " state", MessageCode.CLAIMS_2000);

            case RETURN_TO_POSSESSION:
                if (nextStatus.equals(ClaimsStatusEnum.CLOSED)) {
                    return nextStatus;
                }
                LOGGER.debug(error + nextStatus.getValue() + " state");
                throw new BadParametersException(error + nextStatus.getValue() + " state", MessageCode.CLAIMS_2000);
            default:
                LOGGER.debug("Status not valid");
                throw new BadParametersException("Status not valid", MessageCode.CLAIMS_2000);
        }
    }

    private static ClaimsStatusEnum checkNextStatusFULL(ClaimsEntity claimsEntity, ClaimsStatusEnum nextStatus, String practiceManager) throws BadParametersException {

        ClaimsStatusEnum currentStatus = claimsEntity.getStatus();
        String error = "In the FUL stream, you can not go from " + currentStatus.getValue() + " state to ";

        switch (currentStatus) {

            case DRAFT:
                if (nextStatus.equals(ClaimsStatusEnum.DELETED)) {
                    return nextStatus;
                }
                LOGGER.debug(error + nextStatus.getValue() + " state");
                throw new BadParametersException(error + nextStatus.getValue() + " state", MessageCode.CLAIMS_2000);

            case WAITING_FOR_VALIDATION:
                if (nextStatus.equals(ClaimsStatusEnum.INCOMPLETE) ||
                        nextStatus.equals(ClaimsStatusEnum.REJECTED) ||
                        nextStatus.equals(ClaimsStatusEnum.WAITING_FOR_AUTHORITY)) {
                    return nextStatus;
                }
                LOGGER.debug(error + nextStatus.getValue() + " state");
                throw new BadParametersException(error + nextStatus.getValue() + " state", MessageCode.CLAIMS_2000);

            case INCOMPLETE:
                if (nextStatus.equals(ClaimsStatusEnum.WAITING_FOR_AUTHORITY) ||
                        nextStatus.equals(ClaimsStatusEnum.WAITING_FOR_VALIDATION)) {
                    return nextStatus;
                }
                LOGGER.debug(error + nextStatus.getValue() + " state");
                throw new BadParametersException(error + nextStatus.getValue() + " state", MessageCode.CLAIMS_2000);

            case WAITING_FOR_AUTHORITY:
                if (nextStatus.equals(ClaimsStatusEnum.TO_ENTRUST)) {
                    return nextStatus;
                }
                LOGGER.debug(error + nextStatus.getValue() + " state");
                throw new BadParametersException(error + nextStatus.getValue() + " state", MessageCode.CLAIMS_2000);

            case ENTRUSTED_AT:
                if (nextStatus.equals(ClaimsStatusEnum.CLOSED)) {
                    return nextStatus;
                }
                LOGGER.debug(error + nextStatus.getValue() + " state");
                throw new BadParametersException(error + nextStatus.getValue() + " state", MessageCode.CLAIMS_2000);

            case TO_ENTRUST:
                if ((nextStatus.equals(ClaimsStatusEnum.CLOSED_PRACTICE) && claimsEntity.getComplaint().getDataAccident().getResponsible()) ||
                        (nextStatus.equals(ClaimsStatusEnum.WAITING_FOR_REFUND) && !claimsEntity.getComplaint().getDataAccident().getResponsible())) {
                    if (nextStatus.equals(ClaimsStatusEnum.WAITING_FOR_REFUND) && claimsEntity.getComplaint().getEntrusted() == null) {
                        LOGGER.debug(error + nextStatus.getValue() + " state without set entrusted attribute");
                        throw new BadParametersException(error + nextStatus.getValue() + " state without set entrusted attribute", MessageCode.CLAIMS_2000);
                    }

                    return nextStatus;
                }
                LOGGER.debug(error + nextStatus.getValue() + " state with responsible variable set at " + claimsEntity.getComplaint().getDataAccident().getResponsible());
                throw new BadParametersException(error + nextStatus.getValue() + " state with responsible variable set at " + claimsEntity.getComplaint().getDataAccident().getResponsible(), MessageCode.CLAIMS_2000);

            case WAITING_FOR_REFUND:
                if (nextStatus.equals(ClaimsStatusEnum.CLOSED)) {
                    return nextStatus;
                }
                LOGGER.debug(error + nextStatus.getValue() + " state");
                throw new BadParametersException(error + nextStatus.getValue() + " state", MessageCode.CLAIMS_2000);

            case CLOSED_PRACTICE:
                if (nextStatus.equals(ClaimsStatusEnum.MANAGED)) {
                    if (practiceManager == null) {
                        LOGGER.debug(error + nextStatus.getValue() + " state without set variable practice_manager");
                        throw new BadParametersException(error + nextStatus.getValue() + " state without set variable practice_manager", MessageCode.CLAIMS_2000);
                    }
                    claimsEntity.setPracticeManager(practiceManager);
                    return nextStatus;
                }
                LOGGER.debug(error + nextStatus.getValue() + " state");
                throw new BadParametersException(error + nextStatus.getValue() + " state", MessageCode.CLAIMS_2000);

            case MANAGED:
                if ((nextStatus.equals(ClaimsStatusEnum.PROPOSED_ACCEPTED) && claimsEntity.getPracticeManager().equalsIgnoreCase("ALD")) ||
                        (nextStatus.equals(ClaimsStatusEnum.CLOSED) && claimsEntity.getPracticeManager().equalsIgnoreCase("MSA"))) {
                    return nextStatus;
                }
                //SI VERIFICA QUANDO SI CAMBIA GESTORE DELLA PRATICA (PER INTENDERCI IL CAPPIO NEL FLUSSO)
                if (nextStatus.equals(ClaimsStatusEnum.MANAGED)) {
                    claimsEntity.setPracticeManager(practiceManager);
                    return nextStatus;
                }
                LOGGER.debug(error + nextStatus.getValue() + " state");
                throw new BadParametersException(error + nextStatus.getValue() + " state", MessageCode.CLAIMS_2000);

            case PROPOSED_ACCEPTED:
                if (nextStatus.equals(ClaimsStatusEnum.CLOSED)) {
                    return nextStatus;
                }
                LOGGER.debug(error + nextStatus.getValue() + " state");
                throw new BadParametersException(error + nextStatus.getValue() + " state", MessageCode.CLAIMS_2000);

            case CLOSED:
            case REJECTED:
            case DELETED:
                LOGGER.debug(error + nextStatus.getValue() + " state");
                throw new BadParametersException(error + nextStatus.getValue() + " state", MessageCode.CLAIMS_2000);
            default:
                LOGGER.debug("Status not valid");
                throw new BadParametersException("Status not valid", MessageCode.CLAIMS_2000);
        }

    }

    private static ClaimsStatusEnum checkNextStatusFCM(ClaimsEntity claimsEntity, ClaimsStatusEnum nextStatus, String practiceManager) throws BadParametersException {

        ClaimsStatusEnum currentStatus = claimsEntity.getStatus();
        String error = "In the FCM stream, you can not go from " + currentStatus.getValue() + " state to ";

        switch (currentStatus) {

            case WAITING_FOR_VALIDATION:
                if (nextStatus.equals(ClaimsStatusEnum.REJECTED) ||
                        nextStatus.equals(ClaimsStatusEnum.INCOMPLETE) ||
                        nextStatus.equals(ClaimsStatusEnum.WAITING_FOR_AUTHORITY)) {
                    return nextStatus;
                }
                LOGGER.debug(error + nextStatus.getValue() + " state");
                throw new BadParametersException(error + nextStatus.getValue() + " state", MessageCode.CLAIMS_2000);

            case INCOMPLETE:
                if (nextStatus.equals(ClaimsStatusEnum.WAITING_FOR_AUTHORITY)) {
                    return nextStatus;
                }
                LOGGER.debug(error + nextStatus.getValue() + " state");
                throw new BadParametersException(error + nextStatus.getValue() + " state", MessageCode.CLAIMS_2000);

            case WAITING_FOR_AUTHORITY:
                if (nextStatus.equals(ClaimsStatusEnum.TO_ENTRUST)) {
                    return nextStatus;
                }
                LOGGER.debug(error + nextStatus.getValue() + " state");
                throw new BadParametersException(error + nextStatus.getValue() + " state", MessageCode.CLAIMS_2000);

                //entrustedAt può assumere solo i valori STUDIO LEGALE E BROKER/GESTORE

            case ENTRUSTED_AT:
                if (nextStatus.equals(ClaimsStatusEnum.CLOSED)) {
                    return nextStatus;
                }
                LOGGER.debug(error + nextStatus.getValue() + " state");
                throw new BadParametersException(error + nextStatus.getValue() + " state", MessageCode.CLAIMS_2000);

            case TO_ENTRUST:
                if (nextStatus.equals(ClaimsStatusEnum.WAITING_FOR_REFUND)) {
                    if (claimsEntity.getComplaint().getEntrusted() != null) {
                        return nextStatus;
                    } else {
                        LOGGER.debug(error + nextStatus.getValue() + " state without set entrusted attribute");
                        throw new BadParametersException(error + nextStatus.getValue() + " state without set entrusted attribute", MessageCode.CLAIMS_2000);
                    }
                }
                LOGGER.debug(error + nextStatus.getValue() + " state");
                throw new BadParametersException(error + nextStatus.getValue() + " state", MessageCode.CLAIMS_2000);

            case WAITING_FOR_REFUND:
                if (nextStatus.equals(ClaimsStatusEnum.CLOSED)) {
                    return nextStatus;
                }
                LOGGER.debug(error + nextStatus.getValue() + " state");
                throw new BadParametersException(error + nextStatus.getValue() + " state", MessageCode.CLAIMS_2000);

            case CLOSED:
            case REJECTED:
            case DELETED:
                LOGGER.debug(error + nextStatus.getValue() + " state");
                throw new BadParametersException(error + nextStatus.getValue() + " state", MessageCode.CLAIMS_2000);
            default:
                LOGGER.debug("Status not valid");
                throw new BadParametersException("Status not valid", MessageCode.CLAIMS_2000);
        }
    }

    private static ClaimsStatusEnum checkNextStatusFNI(ClaimsEntity claimsEntity, ClaimsStatusEnum nextStatus) throws BadParametersException {

        ClaimsStatusEnum currentStatus = claimsEntity.getStatus();
        String error = "In the FNI stream, you can not go from " + currentStatus.getValue() + " state to ";

        switch (currentStatus) {

            case DRAFT:
                if (nextStatus.equals(ClaimsStatusEnum.DELETED)) {
                    return nextStatus;
                }
                LOGGER.debug(error + nextStatus.getValue() + " state");
                throw new BadParametersException(error + nextStatus.getValue() + " state", MessageCode.CLAIMS_2000);

            case WAITING_FOR_VALIDATION:
                if (nextStatus.equals(ClaimsStatusEnum.REJECTED) ||
                        nextStatus.equals(ClaimsStatusEnum.WAITING_FOR_AUTHORITY)) {
                    return nextStatus;
                }
                LOGGER.debug(error + nextStatus.getValue() + " state");
                throw new BadParametersException(error + nextStatus.getValue() + " state", MessageCode.CLAIMS_2000);

            case WAITING_FOR_AUTHORITY:
                if (nextStatus.equals(ClaimsStatusEnum.CLOSED)) {
                    return nextStatus;
                }
                LOGGER.debug(error + nextStatus.getValue() + " state");
                throw new BadParametersException(error + nextStatus.getValue() + " state", MessageCode.CLAIMS_2000);

            case CLOSED:
            case REJECTED:
                LOGGER.debug(error + nextStatus.getValue() + " state");
                throw new BadParametersException(error + nextStatus.getValue() + " state", MessageCode.CLAIMS_2000);
            default:
                LOGGER.debug("Status not valid");
                throw new BadParametersException("Status not valid", MessageCode.CLAIMS_2000);
        }
    }

    public static ClaimsStatusEnum setStatusWithFlowType(ClaimsFlowEnum typeEntity, ClaimsUserEnum userEntity, List<CounterpartyRequest> counterpart, DataAccidentTypeAccidentEnum typeAccident) {

        if (userEntity.equals(ClaimsUserEnum.LOJACK) && typeAccident.equals(DataAccidentTypeAccidentEnum.FURTO_TOTALE)) {
            return ClaimsStatusEnum.WAITING_FOR_AUTHORITY;
        } else if (userEntity.equals(ClaimsUserEnum.LOJACK) ||
                ((counterpart == null || counterpart.size() == 0) && !(typeEntity.equals(ClaimsFlowEnum.FCM)) && (userEntity.equals(ClaimsUserEnum.MYALD)))) {
            return ClaimsStatusEnum.WAITING_FOR_AUTHORITY;
        } else if (userEntity.equals(ClaimsUserEnum.CUSTOMER_SERVICE) || (typeEntity.equals(ClaimsFlowEnum.FCM) && userEntity.equals(ClaimsUserEnum.MYALD)) || ((counterpart != null && counterpart.size() > 0) && !(typeEntity.equals(ClaimsFlowEnum.FCM)) && (userEntity.equals(ClaimsUserEnum.MYALD)))) {
            return ClaimsStatusEnum.DRAFT;
        } else if (userEntity.equals(ClaimsUserEnum.MSA) && !(typeEntity.equals(ClaimsFlowEnum.FCM))) {
            return ClaimsStatusEnum.WAITING_FOR_VALIDATION;
        }
        LOGGER.debug("It's not possible insert a claim with type " + typeEntity + ", user " + userEntity + " and " + (counterpart != null ? counterpart.size() > 0 ? counterpart.size() + " counterpaties" : "0 counterparty" : "0 counterparty"));
        throw new BadRequestException("It's not possible insert a claim with type " + typeEntity + ", user " + userEntity + " and " + (counterpart != null ? counterpart.size() > 0 ? counterpart.size() + " counterpaties" : "0 counterparty" : "0 counterparty"), MessageCode.CLAIMS_2000);
    }

}
