package com.doing.nemo.claims.util;

import com.doing.nemo.claims.controller.ClaimsController;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsFlowEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.ComplaintModEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.ComplaintNotificationEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.ComplaintPropertyEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.RepairEnum.RepairStatusRepairEnum;
import com.doing.nemo.claims.entity.enumerated.DamagedEnum.DriverEnum.DriverCategoryEnum;
import com.doing.nemo.claims.entity.enumerated.DamagedEnum.ImpactPointEnum.ImpactPointDetectionImpactPointEnum;
import com.doing.nemo.claims.entity.enumerated.DamagedEnum.VehicleEnum.VehicleOwnershipEnum;
import com.doing.nemo.claims.repository.ClaimsRepositoryV1;
import com.doing.nemo.commons.exception.BadParametersException;
import org.everit.json.schema.Schema;
import org.everit.json.schema.ValidationException;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Component
public class Util {

    private static Logger LOGGER = LoggerFactory.getLogger(Util.class);

    private static Integer indice = 0;

    /**
     * @param listEnumVal
     * @param enumClass
     * @param <T>
     */
    private static <T> void checkEnum(List<String> listEnumVal, Class<T> enumClass) {
        String outString = "";

        if (enumClass == ClaimsStatusEnum.class) {
            for (String stringa : listEnumVal) {
                outString = stringa;
                stringa = stringa.replace('-', '_');
                stringa = stringa.replace(' ', '_');
                ClaimsStatusEnum.valueOf(stringa);
            }
        } else if (enumClass == ClaimsFlowEnum.class) {
            for (String stringa : listEnumVal) {
                outString = stringa;
                ClaimsFlowEnum.valueOf(stringa);
            }
        } else if (enumClass == DataAccidentTypeAccidentEnum.class) {
            for (String stringa : listEnumVal) {
                outString = stringa;
                DataAccidentTypeAccidentEnum.valueOf(stringa);
            }
        } else if (enumClass == RepairStatusRepairEnum.class) {
            for (String stringa : listEnumVal) {
                outString = stringa;
                RepairStatusRepairEnum.valueOf(stringa);
            }
        } else if (enumClass == ComplaintModEnum.class) {
            for (String stringa : listEnumVal) {
                outString = stringa;
                ComplaintModEnum.valueOf(stringa);
            }
        } else if (enumClass == ComplaintNotificationEnum.class) {
            for (String stringa : listEnumVal) {
                outString = stringa;
                ComplaintNotificationEnum.valueOf(stringa);
            }
        } else if (enumClass == ComplaintPropertyEnum.class) {
            for (String stringa : listEnumVal) {
                outString = stringa;
                ComplaintPropertyEnum.valueOf(stringa);
            }
        } else if (enumClass == DriverCategoryEnum.class) {
            for (String stringa : listEnumVal) {
                outString = stringa;
                DriverCategoryEnum.valueOf(stringa);
            }
        } else if (enumClass == ImpactPointDetectionImpactPointEnum.class) {
            for (String stringa : listEnumVal) {
                outString = stringa;
                ImpactPointDetectionImpactPointEnum.valueOf(stringa);
            }
        } else if (enumClass == VehicleOwnershipEnum.class) {
            for (String stringa : listEnumVal) {
                outString = stringa;
                VehicleOwnershipEnum.valueOf(stringa);
            }
        }


    }

    private static <T> List<String> addEnum(Class<T> enumClass) {

        List<String> returnList = new LinkedList<>();

        if (enumClass == ClaimsFlowEnum.class) {
            for (ClaimsFlowEnum enumEntity : ClaimsFlowEnum.values()) {
                returnList.add(enumEntity.getValue());
            }
        } else if (enumClass == ClaimsStatusEnum.class) {
            for (ClaimsStatusEnum enumEntity : ClaimsStatusEnum.values()) {
                returnList.add(enumEntity.getValue());
            }
        } else if (enumClass == DataAccidentTypeAccidentEnum.class) {
            for (DataAccidentTypeAccidentEnum enumEntity : DataAccidentTypeAccidentEnum.values()) {
                returnList.add(enumEntity.getValue());
            }
        } else if (enumClass == RepairStatusRepairEnum.class) {
            for (RepairStatusRepairEnum enumEntity : RepairStatusRepairEnum.values()) {
                returnList.add(enumEntity.getValue());
            }
        } else if (enumClass == ComplaintModEnum.class) {
            for (ComplaintModEnum enumEntity : ComplaintModEnum.values()) {
                returnList.add(enumEntity.getValue());
            }
        } else if (enumClass == ComplaintNotificationEnum.class) {
            for (ComplaintNotificationEnum enumEntity : ComplaintNotificationEnum.values()) {
                returnList.add(enumEntity.getValue());
            }
        } else if (enumClass == ComplaintPropertyEnum.class) {
            for (ComplaintPropertyEnum enumEntity : ComplaintPropertyEnum.values()) {
                returnList.add(enumEntity.getValue());
            }
        } else if (enumClass == DriverCategoryEnum.class) {
            for (DriverCategoryEnum enumEntity : DriverCategoryEnum.values()) {
                returnList.add(enumEntity.getValue());
            }
        } else if (enumClass == ImpactPointDetectionImpactPointEnum.class) {
            for (ImpactPointDetectionImpactPointEnum enumEntity : ImpactPointDetectionImpactPointEnum.values()) {
                returnList.add(enumEntity.getValue());
            }
        } else if (enumClass == VehicleOwnershipEnum.class) {
            for (VehicleOwnershipEnum enumEntity : VehicleOwnershipEnum.values()) {
                returnList.add(enumEntity.getValue());
            }
        }

        return returnList;
    }

    public <T> List<String> splitRequest(String requestString, Class<T> enumClass) {

        List<String> listRequest;

        if (requestString != null && !requestString.equals("")) {
            requestString = replaceStringForEnumValue(requestString);
            listRequest = Arrays.asList(requestString.toUpperCase().split(","));
            checkEnum(listRequest, enumClass);
        } else {
            listRequest = addEnum(enumClass);
        }
        return listRequest;
    }

    /**
     * Used to generate the current date starting from the ISO8601 format
     *
     * @return Date
     * @throws ParseException
     */
    public static Date nowDateISO8601() throws ParseException {
        TimeZone timeZone = TimeZone.getTimeZone("UTC");
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        dateFormat.setTimeZone(timeZone);
        String nowAsISO = dateFormat.format(new Date());

        return dateFormat.parse(nowAsISO);
    }

    public static Date fromStringToDate(String dataString) {

        Calendar cal = Calendar.getInstance();

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        simpleDateFormat.setLenient(false);

        try {
            cal.setTime(simpleDateFormat.parse(dataString));
            return cal.getTime();

        } catch (ParseException e) {
            try {
                simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                simpleDateFormat.setLenient(false);
                cal.setTime(simpleDateFormat.parse(dataString));
                return cal.getTime();
            } catch (ParseException ex) {
                LOGGER.debug("The inserted date is wrong ");
                throw new BadParametersException("The inserted date is wrong ");
            }
        }
    }

    public void schemaValidation(Map jsonObject) throws ValidationException {

        indice = 0;
        JSONObject jsonSchema = new JSONObject(new JSONTokener(ClaimsController.class.getResourceAsStream("/jsonSchemaValidatorClaim.json")));

        Schema schema = SchemaLoader.load(jsonSchema);
        schema.validate(new JSONObject(jsonObject));

    }

    public LinkedHashMap<String, Object> createValidationExceptionResponse(ValidationException ex, LinkedHashMap<String, Object> responseMap) {

        if (ex.getCausingExceptions().size() > 1) {
            for (ValidationException val : ex.getCausingExceptions()) {
                responseMap = this.createValidationExceptionResponse(val, responseMap);
            }
        } else {
            responseMap.put("message-" + (indice), ex.getMessage());
            indice += 1;
        }

        return responseMap;
    }

    public static String replaceStringForEnumValue(String value) {
        value = value.replace('-', '_');
        value = value.replace(' ', '_');

        return value;
    }

    public static boolean isNotEmpty(String field) {
        if (field != null && !field.isEmpty())
            return true;
        return false;
    }

    public static String replaceNull(String field) {
        if (field != null)
            return field;
        return "";
    }

}
