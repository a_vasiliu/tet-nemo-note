package com.doing.nemo.claims.util;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.TextNode;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;

/**
 * Classe di deserializzazione per la classe {@link Instant} precedentemente serializzata
 * tramite {@link com.doing.nemo.middleware.client.config.NemoInstantSerializerWithZoneEuropeRome} utilizzata
 * per l'invio a Miles.
 * tipo data in input:
 * 2020-01-01T11:11:00.00+01:00
 * oppure
 * 2020-01-01
 */
public class NemoInstantDeserializerWithZoneEuropeRome extends StdDeserializer<Instant> {
    // utilizzato per il parse delle stringhe che presentano data e ora.
    private DateTimeFormatter fmt = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSXXX").withZone(ZoneId.of("Europe/Rome"));


    public NemoInstantDeserializerWithZoneEuropeRome() {
        this(null);
    }

    protected NemoInstantDeserializerWithZoneEuropeRome(Class<?> vc) {
        super(vc);
    }

    @Override
    public Instant deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        String dateString = ((TextNode) p.getCodec().readTree(p)).asText();
        if(dateString.length()>10) {
            TemporalAccessor temporalAccessor = fmt.parse(dateString);
            return Instant.from(temporalAccessor);
        }else{
            LocalDate localDate = LocalDate.parse(dateString);
            LocalDateTime localDateTime = localDate.atStartOfDay();
            Instant instant = localDateTime.toInstant(ZoneOffset.UTC);
            return instant;
        }
    }
}
