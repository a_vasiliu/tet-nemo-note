package com.doing.nemo.claims.entity.esb;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RegistryESB implements Serializable {

    private static final long serialVersionUID = 4471604721890506515L;


    @JsonProperty("id_transaction")
    private Long idTransaction;

    @JsonProperty("cod_imei")
    private String codImei;

    @JsonProperty("cod_pack")
    private String codPack;

    @JsonProperty("cod_provider")
    private Integer codProvider;

    @JsonProperty("provider")
    private String provider;

    @JsonProperty("date_activation")
    private Date dateActivation;

    @JsonProperty("date_end")
    private Date dateEnd;

    @JsonProperty("cod_serialnumber")
    private String codSerialnumber;

    @JsonProperty("voucher_id")
    private Integer voucherId;


    public Long getIdTransaction() {
        return idTransaction;
    }

    public void setIdTransaction(Long idTransaction) {
        this.idTransaction = idTransaction;
    }

    public String getCodImei() {
        return codImei;
    }

    public void setCodImei(String codImei) {
        this.codImei = codImei;
    }

    public String getCodPack() {
        return codPack;
    }

    public void setCodPack(String codPack) {
        this.codPack = codPack;
    }

    public Integer getCodProvider() {
        return codProvider;
    }

    public void setCodProvider(Integer codProvider) {
        this.codProvider = codProvider;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public Date getDateActivation() {
        if(dateActivation == null){
            return null;
        }
        return (Date)dateActivation.clone();
    }

    public void setDateActivation(Date dateActivation) {
        if(dateActivation != null)
        {
            this.dateActivation =(Date)dateActivation.clone();
        } else {
            this.dateActivation = null;
        }
    }

    public Date getDateEnd() {
        if(dateEnd == null){
            return null;
        }
        return (Date)dateEnd.clone();
    }

    public void setDateEnd(Date dateEnd) {
        if(dateEnd != null)
        {
            this.dateEnd =(Date)dateEnd.clone();
        } else {
            this.dateEnd = null;
        }
    }

    public String getCodSerialnumber() {
        return codSerialnumber;
    }

    public void setCodSerialnumber(String codSerialnumber) {
        this.codSerialnumber = codSerialnumber;
    }

    public Integer getVoucherId() {
        return voucherId;
    }

    public void setVoucherId(Integer voucherId) {
        this.voucherId = voucherId;
    }


}
