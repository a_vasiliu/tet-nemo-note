package com.doing.nemo.claims.entity.enumerated.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class PageStats implements Serializable {

    @JsonProperty("current_page")
    private int currentPage;
    @JsonProperty("item_count")
    private int itemCount;
    @JsonProperty("page_size")
    private int pageSize;
    @JsonProperty("page_count")
    private long pageCount;

    public PageStats() {
    }

    public PageStats(int currentPage, int itemCount, int pageSize, long pageCount) {
        this.currentPage = currentPage;
        this.itemCount = itemCount;
        this.pageSize = pageSize;
        this.pageCount = pageCount;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getItemCount() {
        return itemCount;
    }

    public void setItemCount(int itemCount) {
        this.itemCount = itemCount;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public long getPageCount() {
        return pageCount;
    }

    public void setPageCount(long pageCount) {
        this.pageCount = pageCount;
    }
}