package com.doing.nemo.claims.entity.settings;

import com.doing.nemo.claims.entity.enumerated.DamagedEnum.VehicleEnum.VehicleTypeEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "rule_legal",
        uniqueConstraints = @UniqueConstraint(
                name = "uc_name_legal",
                columnNames =
                        {
                                "selection_name",
                                "legal_id"
                        }
        ))
public class AutomaticAffiliationRuleLegalEntity implements Serializable {

    @Id
    @JsonIgnore
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )

    @Type(type = "org.hibernate.type.PostgresUUIDType")
    private UUID id;

    @Column(name = "selection_name")
    private String selectionName;

    @Column(name = "monthly_volume")
    private Integer monthlyVolume;

    @Column(name = "current_month")
    private String currentMonth;

    @Column(name = "current_volume")
    private Integer currentVolume;

    @Column(name = "detail")
    private Integer detail;

    @Column(name = "annotations")
    private String annotations;

    @Column(name = "complaints_without_ctp")
    private Boolean complaintsWithoutCtp;

    @Column(name = "foreign_country_claim")
    private Boolean foreignCountryClaim;

    @Enumerated(EnumType.STRING)
    @Column(name = "counterpart_type")
    private VehicleTypeEnum counterpartType;

    @Column(name = "claim_with_injured")
    private Boolean claimWithInjured;

    @Column(name = "damage_can_not_be_repaired")
    private Boolean damageCanNotBeRepaired;

    @Column(name = "min_import")
    private Double minImport;

    @Column(name = "max_import")
    private Double maxImport;

    @ManyToMany
    @JoinTable(name = "rule_legal_claims_type",
            joinColumns = @JoinColumn(name = "rule_legal_id"),
            inverseJoinColumns = @JoinColumn(name = "claims_type_id"))
    @Column(name = "claims_type")
    private List<ClaimsTypeEntity> claimsType;

    @ManyToMany
    @JoinTable(name = "rule_legal_damaged",
            joinColumns = @JoinColumn(name = "rule_legal_id"),
            inverseJoinColumns = @JoinColumn(name = "damaged_company_id"))
    @Column(name = "damaged_company")
    private List<InsuranceCompanyEntity> damagedInsuranceCompanyList;

    @ManyToMany
    @JoinTable(name = "rule_legal_counterparty",
            joinColumns = @JoinColumn(name = "rule_legal_id"),
            inverseJoinColumns = @JoinColumn(name = "counterpart_company_id"))
    @Column(name = "counterpart_company")
    private List<InsuranceCompanyEntity> counterpartInsuranceCompanyList;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getSelectionName() {
        return selectionName;
    }

    public void setSelectionName(String selectionName) {
        this.selectionName = selectionName;
    }

    public Integer getMonthlyVolume() {
        return monthlyVolume;
    }

    public void setMonthlyVolume(Integer monthlyVolume) {
        this.monthlyVolume = monthlyVolume;
    }

    public String getCurrentMonth() {
        return currentMonth;
    }

    public void setCurrentMonth(String currentMonth) {
        this.currentMonth = currentMonth;
    }

    public Integer getCurrentVolume() {
        return currentVolume;
    }

    public void setCurrentVolume(Integer currentVolume) {
        this.currentVolume = currentVolume;
    }

    public Integer getDetail() {
        return detail;
    }

    public void setDetail(Integer detail) {
        this.detail = detail;
    }

    public String getAnnotations() {
        return annotations;
    }

    public void setAnnotations(String annotations) {
        this.annotations = annotations;
    }

    public Boolean getComplaintsWithoutCtp() {
        return complaintsWithoutCtp;
    }

    public void setComplaintsWithoutCtp(Boolean complaintsWithoutCtp) {
        this.complaintsWithoutCtp = complaintsWithoutCtp;
    }

    public Boolean getForeignCountryClaim() {
        return foreignCountryClaim;
    }

    public void setForeignCountryClaim(Boolean foreignCountryClaim) {
        this.foreignCountryClaim = foreignCountryClaim;
    }

    public VehicleTypeEnum getCounterpartType() {
        return counterpartType;
    }

    public void setCounterpartType(VehicleTypeEnum counterpartType) {
        this.counterpartType = counterpartType;
    }

    public Boolean getClaimWithInjured() {
        return claimWithInjured;
    }

    public void setClaimWithInjured(Boolean claimWithInjured) {
        this.claimWithInjured = claimWithInjured;
    }

    public Boolean getDamageCanNotBeRepaired() {
        return damageCanNotBeRepaired;
    }

    public void setDamageCanNotBeRepaired(Boolean damageCanNotBeRepaired) {
        this.damageCanNotBeRepaired = damageCanNotBeRepaired;
    }

    public List<ClaimsTypeEntity> getClaimsType() {
        if(claimsType == null){
            return null;
        }
        return new ArrayList<>(claimsType);
    }

    public void setClaimsType(List<ClaimsTypeEntity> claimsType) {
        if(claimsType != null)
        {
            this.claimsType =new ArrayList<>(claimsType);
        }else {
            this.claimsType = null;
        }
    }

    public List<InsuranceCompanyEntity> getDamagedInsuranceCompanyList() {
        if(damagedInsuranceCompanyList == null){
            return null;
        }
        return new ArrayList<>(damagedInsuranceCompanyList);
    }

    public void setDamagedInsuranceCompanyList(List<InsuranceCompanyEntity> damagedInsuranceCompanyList) {
        if(damagedInsuranceCompanyList != null)
        {
            this.damagedInsuranceCompanyList = new ArrayList<>(damagedInsuranceCompanyList);
        } else {
            this.damagedInsuranceCompanyList = null;
        }
    }

    public List<InsuranceCompanyEntity> getCounterpartInsuranceCompanyList() {
        if(counterpartInsuranceCompanyList == null){
            return null;
        }
        return new ArrayList<>(counterpartInsuranceCompanyList);
    }

    public void setCounterpartInsuranceCompanyList(List<InsuranceCompanyEntity> counterpartInsuranceCompanyList) {
        if(counterpartInsuranceCompanyList != null)
        {
            this.counterpartInsuranceCompanyList = new ArrayList<>(counterpartInsuranceCompanyList);
        } else {
            this.counterpartInsuranceCompanyList = null;
        }
    }

    public Double getMinImport() {
        return minImport;
    }

    public void setMinImport(Double minImport) {
        this.minImport = minImport;
    }

    public Double getMaxImport() {
        return maxImport;
    }

    public void setMaxImport(Double maxImport) {
        this.maxImport = maxImport;
    }

    @Override
    public String toString() {
        return "AutomaticAffiliationRuleLegalEntity{" +
                "id=" + id +
                ", selectionName='" + selectionName + '\'' +
                ", monthlyVolume=" + monthlyVolume +
                ", currentMonth='" + currentMonth + '\'' +
                ", currentVolume=" + currentVolume +
                ", detail=" + detail +
                ", annotations='" + annotations + '\'' +
                ", complaintsWithoutCtp=" + complaintsWithoutCtp +
                ", foreignCountryClaim=" + foreignCountryClaim +
                ", counterpartType='" + counterpartType + '\'' +
                ", claimWithInjured=" + claimWithInjured +
                ", damageCanNotBeRepaired=" + damageCanNotBeRepaired +
                ", minImport=" + minImport +
                ", maxImport=" + maxImport +
                ", claimsType=" + claimsType +
                ", damagedInsuranceCompanyList=" + damagedInsuranceCompanyList +
                ", counterpartInsuranceCompanyList=" + counterpartInsuranceCompanyList +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AutomaticAffiliationRuleLegalEntity that = (AutomaticAffiliationRuleLegalEntity) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getSelectionName(), that.getSelectionName()) &&
                Objects.equals(getMonthlyVolume(), that.getMonthlyVolume()) &&
                Objects.equals(getCurrentMonth(), that.getCurrentMonth()) &&
                Objects.equals(getCurrentVolume(), that.getCurrentVolume()) &&
                Objects.equals(getDetail(), that.getDetail()) &&
                Objects.equals(getAnnotations(), that.getAnnotations()) &&
                Objects.equals(getComplaintsWithoutCtp(), that.getComplaintsWithoutCtp()) &&
                Objects.equals(getForeignCountryClaim(), that.getForeignCountryClaim()) &&
                getCounterpartType() == that.getCounterpartType() &&
                Objects.equals(getClaimWithInjured(), that.getClaimWithInjured()) &&
                Objects.equals(getDamageCanNotBeRepaired(), that.getDamageCanNotBeRepaired()) &&
                Objects.equals(getMinImport(), that.getMinImport()) &&
                Objects.equals(getMaxImport(), that.getMaxImport()) &&
                Objects.equals(getClaimsType(), that.getClaimsType()) &&
                Objects.equals(getDamagedInsuranceCompanyList(), that.getDamagedInsuranceCompanyList()) &&
                Objects.equals(getCounterpartInsuranceCompanyList(), that.getCounterpartInsuranceCompanyList());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getSelectionName(), getMonthlyVolume(), getCurrentMonth(), getCurrentVolume(), getDetail(), getAnnotations(), getComplaintsWithoutCtp(), getForeignCountryClaim(), getCounterpartType(), getClaimWithInjured(), getDamageCanNotBeRepaired(), getMinImport(), getMaxImport(), getClaimsType(), getDamagedInsuranceCompanyList(), getCounterpartInsuranceCompanyList());
    }
}