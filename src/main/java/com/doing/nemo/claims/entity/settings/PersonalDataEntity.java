package com.doing.nemo.claims.entity.settings;

import com.doing.nemo.claims.entity.jsonb.dataType.JsonDataFleetManagerType;
import com.doing.nemo.claims.entity.jsonb.personalData.FleetManagerPersonalData;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "personal_data")
@TypeDefs({
        @TypeDef(name = "JsonDataFleetManagerType", typeClass = JsonDataFleetManagerType.class)
})
public class PersonalDataEntity implements Serializable {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Type(type = "org.hibernate.type.PostgresUUIDType")
    private UUID id;

    @Column(name = "personal_code", unique = true, insertable = false, updatable = false)
    private Long personalCode;

    @Column(name = "customer_id")
    private String customerId;

    @Column(name = "name")
    private String name;

    @Column(name = "address")
    private String address;

    @Column(name = "zip_code")
    private String zipCode;

    @Column(name = "city")
    private String city;

    @Column(name = "province")
    private String province;

    @Column(name = "state")
    private String state;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "fax")
    private String fax;

    @Column(name = "email")
    private String email;

    @Column(name = "pec")
    private String pec;

    @Column(name = "web_site")
    private String webSite;

    @Column(name = "personal_group")
    private String personalGroup;

    @Column(name = "contact")
    private String contact;

    @Column(name = "vat_number")
    private String vatNumber;

    @Column(name = "fiscal_code")
    private String fiscalCode;

    @Type(type = "JsonDataFleetManagerType")
    @Column(name = "fleet_manager")
    private List<FleetManagerPersonalData> fleetManagerPersonalData;

    @OneToMany(
            cascade = CascadeType.ALL
    )
    @JoinColumn(name = "personal_data_id")
    private List<ContractEntity> contractEntityList;

    @OneToMany(
            cascade = CascadeType.ALL
    )
    @JoinColumn(name = "personal_data_id")
    private List<InsurancePolicyPersonalDataEntity> insurancePolicyPersonalDataEntityList;

    @OneToMany(
            cascade = CascadeType.ALL
    )
    @JoinColumn(name = "personal_data_id")
    private List<CustomTypeRiskEntity> customTypeRiskEntityList;

    @Column(name = "is_active")
    private Boolean isActive;

    @Column(name = "personal_father_id")
    private String personalFatherId;

    @Column(name = "personal_father_name")
    private String personalFatherName;

    @Column(name = "created_at")
    private Instant createdAt;

    public String getPersonalFatherId() {
        return personalFatherId;
    }

    public void setPersonalFatherId(String personalFatherId) {
        this.personalFatherId = personalFatherId;
    }

    public String getPersonalFatherName() {
        return personalFatherName;
    }

    public void setPersonalFatherName(String personalFatherName) {
        this.personalFatherName = personalFatherName;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Long getPersonalCode() {
        return personalCode;
    }

    public void setPersonalCode(Long personalCode) {
        this.personalCode = personalCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebSite() {
        return webSite;
    }

    public void setWebSite(String webSite) {
        this.webSite = webSite;
    }

    public String getPersonalGroup() {
        return personalGroup;
    }

    public void setPersonalGroup(String personalGroup) {
        this.personalGroup = personalGroup;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getVatNumber() {
        return vatNumber;
    }

    public void setVatNumber(String vatNumber) {
        this.vatNumber = vatNumber;
    }

    public String getFiscalCode() {
        return fiscalCode;
    }

    public void setFiscalCode(String fiscalCode) {
        this.fiscalCode = fiscalCode;
    }

    public List<FleetManagerPersonalData> getFleetManagerPersonalData() {
        if(fleetManagerPersonalData == null){
            return null;
        }
        return new ArrayList<>(fleetManagerPersonalData);
    }

    public void setFleetManagerPersonalData(List<FleetManagerPersonalData> fleetManagerPersonalData) {
        if(fleetManagerPersonalData != null)
        {
            this.fleetManagerPersonalData = new ArrayList<>(fleetManagerPersonalData);
        } else {
            this.fleetManagerPersonalData = null;
        }
    }

    public List<ContractEntity> getContractEntityList() {
        return contractEntityList;
    }

    public void setContractEntityList(List<ContractEntity> contractEntityList) {
        this.contractEntityList = contractEntityList;
    }

    public List<InsurancePolicyPersonalDataEntity> getInsurancePolicyPersonalDataEntityList() {
        return insurancePolicyPersonalDataEntityList;
    }

    public void setInsurancePolicyPersonalDataEntityList(List<InsurancePolicyPersonalDataEntity> insurancePolicyPersonalDataEntityList) {
        this.insurancePolicyPersonalDataEntityList = insurancePolicyPersonalDataEntityList;
    }

    public List<CustomTypeRiskEntity> getCustomTypeRiskEntityList() {
        return customTypeRiskEntityList;
    }

    public void setCustomTypeRiskEntityList(List<CustomTypeRiskEntity> customTypeRiskEntityList) {
        this.customTypeRiskEntityList = customTypeRiskEntityList;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public String getPec() {
        return pec;
    }

    public void setPec(String pec) {
        this.pec = pec;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public String toString() {
        return "PersonalDataEntity{" +
                "id=" + id +
                ", personalCode=" + personalCode +
                ", customerId='" + customerId + '\'' +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", zipCode='" + zipCode + '\'' +
                ", city='" + city + '\'' +
                ", province='" + province + '\'' +
                ", state='" + state + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", fax='" + fax + '\'' +
                ", email='" + email + '\'' +
                ", pec='" + pec + '\'' +
                ", webSite='" + webSite + '\'' +
                ", personalGroup='" + personalGroup + '\'' +
                ", contact='" + contact + '\'' +
                ", vatNumber='" + vatNumber + '\'' +
                ", fiscalCode='" + fiscalCode + '\'' +
                ", fleetManagerPersonalData=" + fleetManagerPersonalData +
                ", contractEntityList=" + contractEntityList +
                ", insurancePolicyPersonalDataEntityList=" + insurancePolicyPersonalDataEntityList +
                ", customTypeRiskEntityList=" + customTypeRiskEntityList +
                ", isActive=" + isActive +
                ", personalFatherId='" + personalFatherId + '\'' +
                ", personalFatherName='" + personalFatherName + '\'' +
                ", createdAt=" + createdAt +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PersonalDataEntity that = (PersonalDataEntity) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getPersonalCode(), that.getPersonalCode()) &&
                Objects.equals(getCustomerId(), that.getCustomerId()) &&
                Objects.equals(getName(), that.getName()) &&
                Objects.equals(getAddress(), that.getAddress()) &&
                Objects.equals(getZipCode(), that.getZipCode()) &&
                Objects.equals(getCity(), that.getCity()) &&
                Objects.equals(getProvince(), that.getProvince()) &&
                Objects.equals(getState(), that.getState()) &&
                Objects.equals(getPhoneNumber(), that.getPhoneNumber()) &&
                Objects.equals(getFax(), that.getFax()) &&
                Objects.equals(getEmail(), that.getEmail()) &&
                Objects.equals(getPec(), that.getPec()) &&
                Objects.equals(getWebSite(), that.getWebSite()) &&
                Objects.equals(getPersonalGroup(), that.getPersonalGroup()) &&
                Objects.equals(getContact(), that.getContact()) &&
                Objects.equals(getVatNumber(), that.getVatNumber()) &&
                Objects.equals(getFiscalCode(), that.getFiscalCode()) &&
                Objects.equals(getFleetManagerPersonalData(), that.getFleetManagerPersonalData()) &&
                Objects.equals(getContractEntityList(), that.getContractEntityList()) &&
                Objects.equals(getInsurancePolicyPersonalDataEntityList(), that.getInsurancePolicyPersonalDataEntityList()) &&
                Objects.equals(getCustomTypeRiskEntityList(), that.getCustomTypeRiskEntityList()) &&
                Objects.equals(getActive(), that.getActive()) &&
                Objects.equals(getPersonalFatherId(), that.getPersonalFatherId()) &&
                Objects.equals(getPersonalFatherName(), that.getPersonalFatherName()) &&
                Objects.equals(getCreatedAt(), that.getCreatedAt());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getPersonalCode(), getCustomerId(), getName(), getAddress(), getZipCode(), getCity(), getProvince(), getState(), getPhoneNumber(), getFax(), getEmail(), getPec(), getWebSite(), getPersonalGroup(), getContact(), getVatNumber(), getFiscalCode(), getFleetManagerPersonalData(), getContractEntityList(), getInsurancePolicyPersonalDataEntityList(), getCustomTypeRiskEntityList(), getActive(), getPersonalFatherId(), getPersonalFatherName(), getCreatedAt());
    }
}
