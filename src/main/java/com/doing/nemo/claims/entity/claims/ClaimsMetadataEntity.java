package com.doing.nemo.claims.entity.claims;

import com.doing.nemo.claims.entity.ClaimsNewEntity;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@JsonIgnoreProperties({"hibernateLazyInitializer", "hibernate_lazy_initializer", "handler"})
@Entity
@Table(name = "claims_metadata")
public class ClaimsMetadataEntity implements Serializable {

    @Id
    @Column(name = "id", updatable = false, nullable = false)
    private String id;

    @Column(name="role")
    private String role;

    @Column(name="email")
    private String email;

    @Column(name="user_id")
    private Integer userId;

    @Column(name="url_mail")
    private String urlMail;

    @Column(name="cod_plate")
    private String codPlate;

    @Column(name="contract_id")
    private Integer contractId;

    @Column(name="customer_id")
    private String customerId;

    @Column(name="fiscal_code")
    private String fiscalCode;

    @Column(name="plate_customer_id")
    private Integer plateCustomerId;

    @Column(name="user_session_token")
    private String userSessionToken;

    @OneToOne
    @JoinColumn(name = "id")
    @MapsId("id")
    @PrimaryKeyJoinColumn
    private ClaimsNewEntity claim;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUrlMail() {
        return urlMail;
    }

    public void setUrlMail(String urlMail) {
        this.urlMail = urlMail;
    }

    public String getCodPlate() {
        return codPlate;
    }

    public void setCodPlate(String codPlate) {
        this.codPlate = codPlate;
    }

    public Integer getContractId() {
        return contractId;
    }

    public void setContractId(Integer contractId) {
        this.contractId = contractId;
    }

    public String getFiscalCode() {
        return fiscalCode;
    }

    public void setFiscalCode(String fiscalCode) {
        this.fiscalCode = fiscalCode;
    }

    public Integer getPlateCustomerId() {
        return plateCustomerId;
    }

    public void setPlateCustomerId(Integer plateCustomerId) {
        this.plateCustomerId = plateCustomerId;
    }

    public String getUserSessionToken() {
        return userSessionToken;
    }

    public void setUserSessionToken(String userSessionToken) {
        this.userSessionToken = userSessionToken;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public ClaimsNewEntity getClaim() {
        return claim;
    }

    public void setClaim(ClaimsNewEntity claim) {
        this.claim = claim;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ClaimsMetadataEntity{");
        sb.append("id='").append(id).append('\'');
        sb.append(", role='").append(role).append('\'');
        sb.append(", email='").append(email).append('\'');
        sb.append(", userId=").append(userId);
        sb.append(", urlMail='").append(urlMail).append('\'');
        sb.append(", codPlate='").append(codPlate).append('\'');
        sb.append(", contractId=").append(contractId);
        sb.append(", customerId='").append(customerId).append('\'');
        sb.append(", fiscalCode='").append(fiscalCode).append('\'');
        sb.append(", plateCustomerId=").append(plateCustomerId);
        sb.append(", userSessionToken='").append(userSessionToken).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClaimsMetadataEntity that = (ClaimsMetadataEntity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(role, that.role) &&
                Objects.equals(email, that.email) &&
                Objects.equals(userId, that.userId) &&
                Objects.equals(urlMail, that.urlMail) &&
                Objects.equals(codPlate, that.codPlate) &&
                Objects.equals(contractId, that.contractId) &&
                Objects.equals(customerId, that.customerId) &&
                Objects.equals(fiscalCode, that.fiscalCode) &&
                Objects.equals(plateCustomerId, that.plateCustomerId) &&
                Objects.equals(userSessionToken, that.userSessionToken) &&
                Objects.equals(claim, that.claim);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, role, email, userId, urlMail, codPlate, contractId, customerId, fiscalCode, plateCustomerId, userSessionToken, claim);
    }
}
