package com.doing.nemo.claims.entity;

import com.doing.nemo.claims.entity.jsonb.dataType.JsonDataUploadFileRequest;
import com.doing.nemo.claims.websin.websinws.UploadFileRequest;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("WEBSIN_FILE")
@TypeDefs({
        @TypeDef(name = "JsonDataUploadFileRequest", typeClass = JsonDataUploadFileRequest.class)
})
public class WebSinFileExternalEntity extends ExternalCommunicationEntity {

    @Type(type = "JsonDataUploadFileRequest")
    @Column(name = "request")
    UploadFileRequest request;

    public UploadFileRequest getRequest() {
        return request;
    }

    public void setRequest(UploadFileRequest request) { this.request = request; }
}