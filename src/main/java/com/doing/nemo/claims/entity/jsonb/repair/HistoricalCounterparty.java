package com.doing.nemo.claims.entity.jsonb.repair;

import com.doing.nemo.claims.entity.enumerated.EventTypeEnum;
import com.doing.nemo.claims.entity.enumerated.RepairEnum.RepairStatusEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class HistoricalCounterparty implements Serializable {

    private static final long serialVersionUID = 3264859418501365708L;

    @JsonProperty("old_status")
    private RepairStatusEnum statusEntityOld;

    @JsonProperty("new_status")
    private RepairStatusEnum statusEntityNew;

    @JsonProperty("description")
    private String description;

    @JsonProperty("update_at")
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    private Date updateAt;

    @JsonProperty("user_id")
    private String userId;

    @JsonProperty("user_name")
    private String userName;

    @JsonProperty("comunication_description")
    private String comunicationDescription;

    @JsonProperty("event_type")
    private EventTypeEnum eventType;

    public HistoricalCounterparty() {
    }

    public HistoricalCounterparty(RepairStatusEnum statusEntityOld, RepairStatusEnum statusEntityNew, String description, Date updateAt, String userId, String userName, String comunicationDescription, EventTypeEnum eventType) {
        this.statusEntityOld = statusEntityOld;
        this.statusEntityNew = statusEntityNew;
        this.description = description;
        if(updateAt != null)
        {
            this.updateAt = (Date)updateAt.clone();
        }
        this.userId = userId;
        this.userName = userName;
        this.comunicationDescription = comunicationDescription;
        this.eventType = eventType;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public RepairStatusEnum getStatusEntityOld() {
        return statusEntityOld;
    }

    public void setStatusEntityOld(RepairStatusEnum statusEntityOld) {
        this.statusEntityOld = statusEntityOld;
    }

    public RepairStatusEnum getStatusEntityNew() {
        return statusEntityNew;
    }

    public void setStatusEntityNew(RepairStatusEnum statusEntityNew) {
        this.statusEntityNew = statusEntityNew;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getUpdateAt() {
        if(updateAt == null){
            return null;
        }
        return (Date)updateAt.clone();
    }

    public void setUpdateAt(Date updateAt) {
        if(updateAt != null)
        {
            this.updateAt = (Date)updateAt.clone();
        } else {
            this.updateAt = null;
        }
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getComunicationDescription() {
        return comunicationDescription;
    }

    public void setComunicationDescription(String comunicationDescription) {
        this.comunicationDescription = comunicationDescription;
    }

    public EventTypeEnum getEventType() {
        return eventType;
    }

    public void setEventType(EventTypeEnum eventType) {
        this.eventType = eventType;
    }

    @Override
    public String toString() {
        return "HistoricalCounterparty{" +
                "statusEntityOld=" + statusEntityOld +
                ", statusEntityNew=" + statusEntityNew +
                ", description='" + description + '\'' +
                ", updateAt=" + updateAt +
                ", userId='" + userId + '\'' +
                ", userName='" + userName + '\'' +
                ", comunicationDescription='" + comunicationDescription + '\'' +
                ", eventType=" + eventType +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        HistoricalCounterparty that = (HistoricalCounterparty) o;

        return new EqualsBuilder().append(statusEntityOld, that.statusEntityOld).append(statusEntityNew, that.statusEntityNew)
                .append(description, that.description).append(updateAt, that.updateAt).append(userId, that.userId).append(userName, that.userName)
                .append(comunicationDescription, that.comunicationDescription).append(eventType, that.eventType).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(statusEntityOld).append(statusEntityNew).append(description)
                .append(updateAt).append(userId).append(userName).append(comunicationDescription).append(eventType).toHashCode();
    }
}
