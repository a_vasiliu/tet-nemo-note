package com.doing.nemo.claims.entity.claims;

import com.doing.nemo.claims.entity.ClaimsNewEntity;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@JsonIgnoreProperties({"hibernateLazyInitializer", "hibernate_lazy_initializer", "handler"})
@Entity
@Table(name = "claims_damaged_contract")
public class ClaimsDamagedContractEntity implements Serializable {

    @Id
    @Column(name = "id", updatable = false, nullable = false)
    private String id;

    @Column(name="contract_id")
    private Long contractId;

    @Column(name="status")
    private String status;

    @Column(name="contract_version_id")
    private Long contractVersionId;

    @Column(name="mileage")
    private Double mileage;

    @Column(name="duration")
    private Integer duration;

    @Column(name="contract_type")
    private String contractType;

    @Column(name="start_date")
    //@JsonFormat(pattern = "yyyy-MM-dd")
    private Date startDate;

    @Column(name="end_date")
    //@JsonFormat(pattern = "yyyy-MM-dd")
    private Date endDate;

    @Column(name="takein_date")
    //@JsonFormat(pattern = "yyyy-MM-dd")
    private Date takeinDate;

    @Column(name="leasing_company_id")
    private Long leasingCompanyId;

    @Column(name="leasing_company")
    private String leasingCompany;

    @Column(name="succeeding_contract_id")
    private Long succeedingContractId;

    @Column(name="fleet_vehicle_id")
    private Long fleetVehicleId;

    @Column(name="license_plate")
    private String licensePlate;

    @OneToOne
    @JoinColumn(name = "id")
    @MapsId("id")
    @PrimaryKeyJoinColumn
    private ClaimsNewEntity claim;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getContractId() {
        return contractId;
    }

    public void setContractId(Long contractId) {
        this.contractId = contractId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getContractVersionId() {
        return contractVersionId;
    }

    public void setContractVersionId(Long contractVersionId) {
        this.contractVersionId = contractVersionId;
    }

    public Double getMileage() {
        return mileage;
    }

    public void setMileage(Double mileage) {
        this.mileage = mileage;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getContractType() {
        return contractType;
    }

    public void setContractType(String contractType) {
        this.contractType = contractType;
    }

    public Date getStartDate() {
        if(startDate == null){
            return null;
        }
        return (Date)startDate.clone();
    }

    public void setStartDate(Date startDate) {
        if(startDate != null)
        {
            this.startDate =(Date)startDate.clone();
        } else {
            this.startDate = null;
        }
    }

    public Date getEndDate() {
        if(endDate == null){
            return null;
        }
        return (Date)endDate.clone();
    }

    public void setEndDate(Date endDate) {
        if(endDate != null)
        {
            this.endDate = (Date)endDate.clone();
        } else {
            this.endDate = null;
        }
    }

    public Date getTakeinDate() {
        if(takeinDate == null){
            return null;
        }
        return (Date)takeinDate.clone();
    }

    public void setTakeinDate(Date takeinDate) {
        if(takeinDate != null)
        {
            this.takeinDate = (Date)takeinDate.clone();
        } else {
            this.takeinDate = null;
        }
    }

    public Long getLeasingCompanyId() {
        return leasingCompanyId;
    }

    public void setLeasingCompanyId(Long leasingCompanyId) {
        this.leasingCompanyId = leasingCompanyId;
    }

    public String getLeasingCompany() {
        return leasingCompany;
    }

    public void setLeasingCompany(String leasingCompany) {
        this.leasingCompany = leasingCompany;
    }

    public Long getSucceedingContractId() {
        return succeedingContractId;
    }

    public void setSucceedingContractId(Long succeedingContractId) {
        this.succeedingContractId = succeedingContractId;
    }

    public Long getFleetVehicleId() {
        return fleetVehicleId;
    }

    public void setFleetVehicleId(Long fleetVehicleId) {
        this.fleetVehicleId = fleetVehicleId;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public ClaimsNewEntity getClaim() {
        return claim;
    }

    public void setClaim(ClaimsNewEntity claim) {
        this.claim = claim;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ClaimsDamagedContractEntity{");
        sb.append("id='").append(id).append('\'');
        sb.append(", contractId=").append(contractId);
        sb.append(", status='").append(status).append('\'');
        sb.append(", contractVersionId=").append(contractVersionId);
        sb.append(", mileage=").append(mileage);
        sb.append(", duration=").append(duration);
        sb.append(", contractType='").append(contractType).append('\'');
        sb.append(", startDate=").append(startDate);
        sb.append(", endDate=").append(endDate);
        sb.append(", takeinDate=").append(takeinDate);
        sb.append(", leasingCompanyId=").append(leasingCompanyId);
        sb.append(", leasingCompany='").append(leasingCompany).append('\'');
        sb.append(", succeedingContractId=").append(succeedingContractId);
        sb.append(", fleetVehicleId=").append(fleetVehicleId);
        sb.append(", licensePlate='").append(licensePlate).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClaimsDamagedContractEntity that = (ClaimsDamagedContractEntity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(contractId, that.contractId) &&
                Objects.equals(status, that.status) &&
                Objects.equals(contractVersionId, that.contractVersionId) &&
                Objects.equals(mileage, that.mileage) &&
                Objects.equals(duration, that.duration) &&
                Objects.equals(contractType, that.contractType) &&
                Objects.equals(startDate, that.startDate) &&
                Objects.equals(endDate, that.endDate) &&
                Objects.equals(takeinDate, that.takeinDate) &&
                Objects.equals(leasingCompanyId, that.leasingCompanyId) &&
                Objects.equals(leasingCompany, that.leasingCompany) &&
                Objects.equals(succeedingContractId, that.succeedingContractId) &&
                Objects.equals(fleetVehicleId, that.fleetVehicleId) &&
                Objects.equals(licensePlate, that.licensePlate) &&
                Objects.equals(claim, that.claim);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, contractId, status, contractVersionId, mileage, duration, contractType, startDate, endDate, leasingCompanyId, leasingCompany, succeedingContractId, fleetVehicleId, licensePlate, claim);
    }
}
