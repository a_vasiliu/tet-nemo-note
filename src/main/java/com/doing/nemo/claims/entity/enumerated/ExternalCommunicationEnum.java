package com.doing.nemo.claims.entity.enumerated;

import com.doing.nemo.commons.exception.BadParametersException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.io.Serializable;

public enum ExternalCommunicationEnum implements Serializable {
    TO_BE_RECOVERED("TO_BE_RECOVERED"),
    RECOVERED("RECOVERED"),
    OUTDATED("OUTDATED"),
    SEND_OK("SEND_OK");

    private static Logger LOGGER = LoggerFactory.getLogger(RiskEnum.class);
    private String external;

    private ExternalCommunicationEnum(String external) {
        this.external = external;
    }

    @JsonCreator
    public static ExternalCommunicationEnum create(String external) {

        external = external.replace(" - ", "_");
        external = external.replace('-', '_');
        external = external.replace(' ', '_');

        if (external != null) {
            for (ExternalCommunicationEnum val : ExternalCommunicationEnum.values()) {
                if (external.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        LOGGER.debug("Bad parameters exception. Enum class " + ExternalCommunicationEnum.class.getSimpleName() + " doesn't accept this value: " + external);
        throw new BadParametersException("Bad parameters exception. Enum class " + ExternalCommunicationEnum.class.getSimpleName() + " doesn't accept this value: " + external);
    }

    public String getValue() {
        return this.external.toUpperCase();
    }

    @JsonValue
    public String toValue() {
        for (RiskEnum val : RiskEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }
}
