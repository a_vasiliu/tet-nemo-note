package com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AdditionalCosts implements Serializable {

    private static final long serialVersionUID = 1956119285000486873L;

    @JsonProperty("typology")
    private String typology;

    @JsonProperty("amount")
    private Double amount;

    @JsonProperty("date")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date date;

    @JsonProperty("attachment_list")
    private List<String> attachments;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getTypology() {
        return typology;
    }

    public void setTypology(String typology) {
        this.typology = typology;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Date getDate() {
        if(date == null){
            return null;
        }
        return (Date)date.clone();
    }

    public void setDate(Date date) {
        if(date != null)
        {
            this.date = (Date)date.clone();
        } else {
            this.date = null;
        }
    }

    public List<String> getAttachments() {
        if(attachments == null){
            return null;
        }
        return new ArrayList<>(attachments) ;
    }

    public void setAttachments(List<String> attachments) {
        if(attachments != null){
            this.attachments = new ArrayList<>(attachments) ;
        } else {
            this.attachments = null;
        }
    }

    @Override
    public String toString() {
        return "AdditionalCosts{" +
                "typology='" + typology + '\'' +
                ", amount=" + amount +
                ", date=" + date +
                ", attachments_link='" + attachments + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        AdditionalCosts that = (AdditionalCosts) o;

        return new EqualsBuilder().append(typology, that.typology).append(amount, that.amount)
                .append(date, that.date).append(attachments, that.attachments).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(typology)
                .append(amount).append(date).append(attachments).toHashCode();
    }
}
