package com.doing.nemo.claims.entity.jsonb;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UploadFile {

    private String fileContent;

    private String fileName;

    private String description;

    private String uuid;

    private String blobType;

    public UploadFile() {
    }

    public UploadFile(String fileContent, String fileName, String description, String uuid, String blobType) {
        this.fileContent = fileContent;
        this.fileName = fileName;
        this.description = description;
        this.uuid = uuid;
        this.blobType = blobType;
    }

    public String getFileContent() {
        return fileContent;
    }

    public void setFileContent(String fileContent) {
        this.fileContent = fileContent;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getBlobType() {
        return blobType;
    }

    public void setBlobType(String blobType) {
        this.blobType = blobType;
    }

    @Override
    public String toString() {
        return "UploadFile{" +
                "fileContent='" + fileContent + '\'' +
                ", fileName='" + fileName + '\'' +
                ", description='" + description + '\'' +
                ", uuid='" + uuid + '\'' +
                ", blobType='" + blobType + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        UploadFile that = (UploadFile) o;

        return new EqualsBuilder().append(fileContent, that.fileContent).append(fileName, that.fileName)
                .append(description, that.description).append(uuid, that.uuid).append(blobType, that.blobType).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(fileContent).append(fileName)
                .append(description).append(uuid).append(blobType).toHashCode();
    }
}
