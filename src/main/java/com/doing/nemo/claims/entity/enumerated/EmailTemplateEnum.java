package com.doing.nemo.claims.entity.enumerated;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;

public enum EmailTemplateEnum implements Serializable {
    GENERIC("generic"),
    THEFT("theft"),
    REPAIR("repair");

    private static Logger LOGGER = LoggerFactory.getLogger(EmailTemplateEnum.class);
    private String emailTemplateType;

    private EmailTemplateEnum(String emailTemplateType) {
        this.emailTemplateType = emailTemplateType;
    }

    @JsonCreator
    public static EmailTemplateEnum create(String emailTemplateType) {

        if (emailTemplateType != null) {

            emailTemplateType = emailTemplateType.replace(" - ", "_");
            emailTemplateType = emailTemplateType.replace('-', '_');
            emailTemplateType = emailTemplateType.replace(' ', '_');

            for (EmailTemplateEnum val : EmailTemplateEnum.values()) {
                if (emailTemplateType.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        LOGGER.debug("Bad parameters exception. Enum class " + EmailTemplateEnum.class.getSimpleName() + " doesn't accept this value: " + emailTemplateType);
        throw new BadParametersException("Bad parameters exception. Enum class " + EmailTemplateEnum.class.getSimpleName() + " doesn't accept this value: " + emailTemplateType);
    }

    public String getValue() {
        return this.emailTemplateType.toUpperCase();
    }

    @JsonValue
    public String toValue() {
        for (EmailTemplateEnum val : EmailTemplateEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }

}
