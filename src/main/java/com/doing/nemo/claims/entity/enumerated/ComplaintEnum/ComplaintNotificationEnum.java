package com.doing.nemo.claims.entity.enumerated.ComplaintEnum;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;

public enum ComplaintNotificationEnum implements Serializable {
    CLAIM_COMPLAINT("claim_complaint"),
    CLAIM_COMPLAINT_WEB("claim_complaint_web"),
    COMPANY_REQUEST("company_request"),
    REQUEST_DAMAGES_FROM_COUNTERPARTY("request_damages_from_counterparty"),
    QUOTE("quote"),
    DECLARATION_IRREPARABILITY_VEHICLE("declaration_irreparability_vehicle"),
    REAL_OFFER("real_offer"),
    OTHER("other"),
    COMPLAINT_AUTHORITY("complaint_authority"),
    REPAIR("repair"),
    EMERGENCY_SERVICES("emergency_services"),
    EVALUTATION_STATE_OF_USE("evalutation_state_of_use");


    private static Logger LOGGER = LoggerFactory.getLogger(ComplaintNotificationEnum.class);
    private String notificationEntity;

    private ComplaintNotificationEnum(String notificationEntity) {
        this.notificationEntity = notificationEntity;
    }

    @JsonCreator
    public static ComplaintNotificationEnum create(String notificationEntity) {

        notificationEntity = notificationEntity.replace(" - ", "_");
        notificationEntity = notificationEntity.replace('-', '_');
        notificationEntity = notificationEntity.replace(' ', '_');

        if (notificationEntity != null) {
            for (ComplaintNotificationEnum val : ComplaintNotificationEnum.values()) {
                if (notificationEntity.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        LOGGER.debug("Bad parameters exception. Enum class " + ComplaintNotificationEnum.class.getSimpleName() + " doesn't accept this value: " + notificationEntity);
        throw new BadParametersException("Bad parameters exception. Enum class " + ComplaintNotificationEnum.class.getSimpleName() + " doesn't accept this value: " + notificationEntity);
    }

    public String getValue() {
        return this.notificationEntity.toUpperCase();
    }

    @JsonValue
    public String toValue() {
        for (ComplaintNotificationEnum val : ComplaintNotificationEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }
}
