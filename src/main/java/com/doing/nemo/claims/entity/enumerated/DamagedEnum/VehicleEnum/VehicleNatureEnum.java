package com.doing.nemo.claims.entity.enumerated.DamagedEnum.VehicleEnum;

import java.io.Serializable;

public enum VehicleNatureEnum implements Serializable {
    MOTOR_VEHICLE, MOTOR_CYCLE, VAN
}
