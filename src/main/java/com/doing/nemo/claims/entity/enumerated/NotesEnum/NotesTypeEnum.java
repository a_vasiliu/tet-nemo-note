package com.doing.nemo.claims.entity.enumerated.NotesEnum;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;

public enum NotesTypeEnum implements Serializable {
    WARNING("warning"),
    ERROR("error"),
    INFO("info"),
    REFUND("refund");

    private static Logger LOGGER = LoggerFactory.getLogger(NotesTypeEnum.class);
    private String noteTypeEntity;

    private NotesTypeEnum(String noteTypeEntity) {
        this.noteTypeEntity = noteTypeEntity;
    }

    @JsonCreator
    public static NotesTypeEnum create(String noteTypeEntity) {

        noteTypeEntity = noteTypeEntity.replace(" - ", "_");
        noteTypeEntity = noteTypeEntity.replace('-', '_');
        noteTypeEntity = noteTypeEntity.replace(' ', '_');

        if (noteTypeEntity != null) {
            for (NotesTypeEnum val : NotesTypeEnum.values()) {
                if (noteTypeEntity.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        LOGGER.debug("Bad parameters exception. Enum class " + NotesTypeEnum.class.getSimpleName() + " doesn't accept this value: " + noteTypeEntity);
        throw new BadParametersException("Bad parameters exception. Enum class " + NotesTypeEnum.class.getSimpleName() + " doesn't accept this value: " + noteTypeEntity);
    }

    public String getValue() {
        return this.noteTypeEntity.toUpperCase();
    }

    @JsonValue
    public String toValue() {
        for (NotesTypeEnum val : NotesTypeEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }
}
