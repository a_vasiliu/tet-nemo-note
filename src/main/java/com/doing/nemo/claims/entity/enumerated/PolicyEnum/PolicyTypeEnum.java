package com.doing.nemo.claims.entity.enumerated.PolicyEnum;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;

public enum PolicyTypeEnum implements Serializable {
    RCA("rca"),
    PAI("pai"),
    LEGAL("legal");

    private static Logger LOGGER = LoggerFactory.getLogger(PolicyTypeEnum.class);
    private String policyTypeEnum;

    private PolicyTypeEnum(String policyTypeEnum) {
        this.policyTypeEnum = policyTypeEnum;
    }

    @JsonCreator
    public static PolicyTypeEnum create(String policyTypeEnum) {

        if (policyTypeEnum != null) {

            policyTypeEnum = policyTypeEnum.replace(" - ", "_");
            policyTypeEnum = policyTypeEnum.replace('-', '_');
            policyTypeEnum = policyTypeEnum.replace(' ', '_');

            for (PolicyTypeEnum val : PolicyTypeEnum.values()) {
                if (policyTypeEnum.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        LOGGER.debug("Bad parameters exception. Enum class " + PolicyTypeEnum.class.getSimpleName() + " doesn't accept this value: " + policyTypeEnum);
        throw new BadParametersException("Bad parameters exception. Enum class " + PolicyTypeEnum.class.getSimpleName() + " doesn't accept this value: " + policyTypeEnum);
    }

    public String getValue() {
        return this.policyTypeEnum.toUpperCase();
    }

    @JsonValue
    public String toValue() {
        for (PolicyTypeEnum val : PolicyTypeEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }
}
