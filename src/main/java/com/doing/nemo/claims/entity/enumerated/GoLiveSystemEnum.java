package com.doing.nemo.claims.entity.enumerated;

import com.doing.nemo.commons.exception.BadParametersException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.io.Serializable;

public enum GoLiveSystemEnum implements Serializable {
    NEMO("nemo"),
    WEBSIN("websin");

    private String system;

    private static Logger LOGGER = LoggerFactory.getLogger(GoLiveSystemEnum.class);

    private GoLiveSystemEnum(String system) {
        this.system = system;
    }

    @JsonCreator
    public static GoLiveSystemEnum create(String system) {

        system = system.replace(" - ", "_");
        system = system.replace('-', '_');
        system = system.replace(' ', '_');

        if (system != null) {
            for (GoLiveSystemEnum val : GoLiveSystemEnum.values()) {
                if (system.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        LOGGER.info("Bad parameters exception. Enum class " + GoLiveSystemEnum.class.getSimpleName() + " doesn't accept this value: " + system);
        throw new BadParametersException("Bad parameters exception. Enum class " + GoLiveSystemEnum.class.getSimpleName() + " doesn't accept this value: " + system);
    }

    public String getValue() {
        return this.system.toUpperCase();
    }

    @JsonValue
    public String toValue() {
        for (GoLiveSystemEnum val : GoLiveSystemEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }
}
