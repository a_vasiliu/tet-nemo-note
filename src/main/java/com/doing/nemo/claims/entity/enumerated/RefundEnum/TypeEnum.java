package com.doing.nemo.claims.entity.enumerated.RefundEnum;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;

public enum TypeEnum implements Serializable {
    ACC("acc"),
    BALANCE("balance");//SALDO

    private static Logger LOGGER = LoggerFactory.getLogger(TypeEnum.class);
    private String typeEnum;

    private TypeEnum(String typeEnum) {
        this.typeEnum = typeEnum;
    }

    @JsonCreator
    public static TypeEnum create(String typeEnum) {

        if (typeEnum != null) {
            for (TypeEnum val : TypeEnum.values()) {
                if (typeEnum.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        LOGGER.debug("Bad parameters exception. Enum class " + TypeEnum.class.getSimpleName() + " doesn't accept this value: " + typeEnum);
        throw new BadParametersException("Bad parameters exception. Enum class " + TypeEnum.class.getSimpleName() + " doesn't accept this value: " + typeEnum);
    }

    public String getValue() {
        return this.typeEnum.toUpperCase();
    }

    @JsonValue
    public String toValue() {
        for (TypeEnum val : TypeEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }
}
