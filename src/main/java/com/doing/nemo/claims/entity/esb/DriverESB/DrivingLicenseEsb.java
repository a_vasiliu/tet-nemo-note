package com.doing.nemo.claims.entity.esb.DriverESB;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DrivingLicenseEsb implements Serializable {

    private static final long serialVersionUID = 4234804574560424952L;

    @JsonProperty("number")
    private String number;

    @JsonProperty("issuing_country")
    private String issuingCountry;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @JsonProperty("valid_from")
    private Date validFrom;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @JsonProperty("valid_to")
    private Date validTo;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getIssuingCountry() {
        return issuingCountry;
    }

    public void setIssuingCountry(String issuingCountry) {
        this.issuingCountry = issuingCountry;
    }

    public Date getValidFrom() {
        if(validFrom == null){
            return null;
        }
        return (Date)validFrom.clone();
    }

    public void setValidFrom(Date validFrom) {
        if(validFrom != null)
        {
            this.validFrom = (Date)validFrom.clone();
        } else {
            this.validFrom = null;
        }
    }

    public Date getValidTo() {
        if(validTo == null){
            return null;
        }
        return (Date)validTo.clone();
    }

    public void setValidTo(Date validTo) {
        if(validTo != null)
        {
            this.validTo = (Date)validTo.clone();
        } else {
            this.validTo = null;
        }
    }

    @Override
    public String toString() {
        return "DrivingLicenseEsb{" +
                "number='" + number + '\'' +
                ", issuingCountry='" + issuingCountry + '\'' +
                ", validFrom='" + validFrom + '\'' +
                ", validTo='" + validTo + '\'' +
                '}';
    }
}