package com.doing.nemo.claims.entity.settings;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "custom_type_risk")
public class CustomTypeRiskEntity implements Serializable {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Type(type = "org.hibernate.type.PostgresUUIDType")
    private UUID id;

    @OneToOne
    @JoinColumn(name = "claims_type_id")
    @JsonProperty("claims_type")
    private ClaimsTypeEntity claimsTypeEntity;

    @Column(name = "flow")
    private String flow;

    @Column(name = "type_of_chargeback")
    private String typeOfChargeback;

    public CustomTypeRiskEntity() {
    }

    public CustomTypeRiskEntity(ClaimsTypeEntity claimsTypeEntity, String flow, String typeOfChargeback) {
        this.claimsTypeEntity = claimsTypeEntity;
        this.flow = flow;
        this.typeOfChargeback = typeOfChargeback;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public ClaimsTypeEntity getClaimsTypeEntity() {
        return claimsTypeEntity;
    }

    public void setClaimsTypeEntity(ClaimsTypeEntity claimsTypeEntity) {
        this.claimsTypeEntity = claimsTypeEntity;
    }

    public String getFlow() {
        return flow;
    }

    public void setFlow(String flow) {
        this.flow = flow;
    }

    public String getTypeOfChargeback() {
        return typeOfChargeback;
    }

    public void setTypeOfChargeback(String typeOfChargeback) {
        this.typeOfChargeback = typeOfChargeback;
    }

    @Override
    public String toString() {
        return "CustomTypeRiskEntity{" +
                "id=" + id +
                ", claimsTypeEntity=" + claimsTypeEntity +
                ", flow='" + flow + '\'' +
                ", typeOfChargeback='" + typeOfChargeback + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CustomTypeRiskEntity that = (CustomTypeRiskEntity) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getClaimsTypeEntity(), that.getClaimsTypeEntity()) &&
                Objects.equals(getFlow(), that.getFlow()) &&
                Objects.equals(getTypeOfChargeback(), that.getTypeOfChargeback());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getClaimsTypeEntity(), getFlow(), getTypeOfChargeback());
    }
}

