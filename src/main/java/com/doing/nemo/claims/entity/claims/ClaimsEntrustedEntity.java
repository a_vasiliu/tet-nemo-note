package com.doing.nemo.claims.entity.claims;

import com.doing.nemo.claims.entity.ClaimsNewEntity;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.EntrustedEnum.EntrustedEnum;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;
import java.util.UUID;

@JsonIgnoreProperties({"hibernateLazyInitializer", "hibernate_lazy_initializer", "handler"})
@Entity
@Table(name = "claims_entrusted")
public class ClaimsEntrustedEntity implements Serializable {

    @Id
    @Column(name = "id", updatable = false, nullable = false)
    private String id;

    @Column(name = "auto_entrust")
    private Boolean autoEntrust;

    @Column(name = "id_entrusted_to")
    private UUID idEntrustedTo;

    @Column(name = "entrusted_to")
    private String entrustedTo;

    //@JsonFormat(pattern = "yyyy-MM-dd")
    @Column(name = "entrusted_day")
    private Instant entrustedDay;

    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    private EntrustedEnum entrustedType;

    @Column(name = "description")
    private String description;

    @Column(name = "entrusted_email")
    private String entrustedEmail;

    @Column(name = "expert")
    private Boolean expert;

    @Column(name = "number_sx_counterparty")
    private String numberSxCounterparty;

    @Column(name = "dwl_man")
    //@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    private Instant dwlMan;

    @Column(name = "status")
    private String status;

    @OneToOne
    @JoinColumn(name = "id")
    @MapsId("id")
    @PrimaryKeyJoinColumn
    private ClaimsNewEntity claim;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNumberSxCounterparty() {
        return numberSxCounterparty;
    }

    public void setNumberSxCounterparty(String numberSxCounterparty) {
        this.numberSxCounterparty = numberSxCounterparty;
    }

    public void setEntrustedDay(Instant entrustedDay) {
        this.entrustedDay = entrustedDay;
    }

    public Instant getDwlMan() {
        return dwlMan;
    }

    public void setDwlMan(Instant dwlMan) {
        this.dwlMan = dwlMan;
    }

    public UUID getIdEntrustedTo() {
        return idEntrustedTo;
    }

    public void setIdEntrustedTo(UUID idEntrustedTo) {
        this.idEntrustedTo = idEntrustedTo;
    }

    public Boolean getAutoEntrust() {
        return autoEntrust;
    }

    public void setAutoEntrust(Boolean autoEntrust) {
        this.autoEntrust = autoEntrust;
    }

    public String getEntrustedTo() {
        return entrustedTo;
    }

    public void setEntrustedTo(String entrustedTo) {
        this.entrustedTo = entrustedTo;
    }

    public Instant getEntrustedDay() {
        return entrustedDay;
    }

    public EntrustedEnum getEntrustedType() {
        return entrustedType;
    }

    public void setEntrustedType(EntrustedEnum entrustedType) {
        this.entrustedType = entrustedType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getExpert() {
        return expert;
    }

    public void setExpert(Boolean expert) {
        this.expert = expert;
    }

    public String getEntrustedEmail() {
        return entrustedEmail;
    }

    public void setEntrustedEmail(String entrustedEmail) {
        this.entrustedEmail = entrustedEmail;
    }

    public ClaimsNewEntity getClaim() {
        return claim;
    }

    public void setClaim(ClaimsNewEntity claim) {
        this.claim = claim;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ClaimsEntrustedEntity{");
        sb.append("id='").append(id).append('\'');
        sb.append(", autoEntrust=").append(autoEntrust);
        sb.append(", idEntrustedTo=").append(idEntrustedTo);
        sb.append(", entrustedTo='").append(entrustedTo).append('\'');
        sb.append(", entrustedDay=").append(entrustedDay);
        sb.append(", entrustedType=").append(entrustedType);
        sb.append(", description='").append(description).append('\'');
        sb.append(", entrustedEmail='").append(entrustedEmail).append('\'');
        sb.append(", expert=").append(expert);
        sb.append(", numberSxCounterparty='").append(numberSxCounterparty).append('\'');
        sb.append(", dwlMan=").append(dwlMan);
        sb.append(", status='").append(status).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClaimsEntrustedEntity that = (ClaimsEntrustedEntity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(autoEntrust, that.autoEntrust) &&
                Objects.equals(idEntrustedTo, that.idEntrustedTo) &&
                Objects.equals(entrustedTo, that.entrustedTo) &&
                Objects.equals(entrustedDay, that.entrustedDay) &&
                entrustedType == that.entrustedType &&
                Objects.equals(description, that.description) &&
                Objects.equals(entrustedEmail, that.entrustedEmail) &&
                Objects.equals(expert, that.expert) &&
                Objects.equals(numberSxCounterparty, that.numberSxCounterparty) &&
                Objects.equals(dwlMan, that.dwlMan) &&
                Objects.equals(status, that.status) &&
                Objects.equals(claim, that.claim);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, autoEntrust, idEntrustedTo, entrustedTo, entrustedDay, entrustedType, description, entrustedEmail, expert, numberSxCounterparty, dwlMan, status, claim);
    }
}
