package com.doing.nemo.claims.entity.jsonb.foundModel;

import com.doing.nemo.claims.entity.jsonb.Address;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.Date;
@JsonIgnoreProperties(ignoreUnknown = true)
public class FoundModel implements Serializable {

    private static final long serialVersionUID = -8107940883550566465L;

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    private Date date;

    @JsonProperty("hour")
    private String hour;

    @JsonProperty("vehicle_co")
    private String vehicleCo;

    @JsonProperty("is_under_seizure")
    private Boolean isUnderSeizure;

    @JsonProperty("address")
    private Address address;

    @JsonProperty("founded_abroad")
    private Boolean foundedAbroad;

    @JsonProperty("police")
    private Boolean police;

    @JsonProperty("cc")
    private Boolean cc;

    @JsonProperty("vvuu")
    private Boolean vvuu;

    @JsonProperty("authority_data")
    private String authorityData;

    @JsonProperty("phone")
    private String phone;

    @JsonProperty("found_note")
    private String foundNote;


    public Date getDate() {
        if(date == null){
            return null;
        }
        return (Date)date.clone();
    }

    public void setDate(Date date) {
        if(date != null)
        {
            this.date = (Date)date.clone();
        } else {
            this.date = null;
        }
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getVehicleCo() {
        return vehicleCo;
    }

    public void setVehicleCo(String vehicleCo) {
        this.vehicleCo = vehicleCo;
    }

    public Boolean getUnderSeizure() {
        return isUnderSeizure;
    }

    public void setUnderSeizure(Boolean underSeizure) {
        isUnderSeizure = underSeizure;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Boolean getFoundedAbroad() {
        return foundedAbroad;
    }

    public void setFoundedAbroad(Boolean foundedAbroad) {
        this.foundedAbroad = foundedAbroad;
    }

    public Boolean getPolice() {
        return police;
    }

    public void setPolice(Boolean police) {
        this.police = police;
    }

    public Boolean getCc() {
        return cc;
    }

    public void setCc(Boolean cc) {
        this.cc = cc;
    }

    public Boolean getVvuu() {
        return vvuu;
    }

    public void setVvuu(Boolean vvuu) {
        this.vvuu = vvuu;
    }

    public String getAuthorityData() {
        return authorityData;
    }

    public void setAuthorityData(String authorityData) {
        this.authorityData = authorityData;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFoundNote() {
        return foundNote;
    }

    public void setFoundNote(String foundNote) {
        this.foundNote = foundNote;
    }

    @Override
    public String toString() {
        return "foundModel{" +
                "date=" + date +
                ", hour='" + hour + '\'' +
                ", vehicleCo='" + vehicleCo + '\'' +
                ", isUnderSeizure=" + isUnderSeizure +
                ", address=" + address +
                ", foundedAbroad=" + foundedAbroad +
                ", police=" + police +
                ", cc=" + cc +
                ", vvuu=" + vvuu +
                ", authorityData='" + authorityData + '\'' +
                ", phone='" + phone + '\'' +
                ", foundNote='" + foundNote + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        FoundModel that = (FoundModel) o;

        return new EqualsBuilder().append(date, that.date).append(hour, that.hour).append(vehicleCo, that.vehicleCo)
                .append(isUnderSeizure, that.isUnderSeizure).append(address, that.address).append(foundedAbroad, that.foundedAbroad)
                .append(police, that.police).append(cc, that.cc).append(vvuu, that.vvuu).append(authorityData, that.authorityData)
                .append(phone, that.phone).append(foundNote, that.foundNote).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(date).append(hour).append(vehicleCo)
                .append(isUnderSeizure).append(address).append(foundedAbroad).append(police).append(cc).append(vvuu)
                .append(authorityData).append(phone).append(foundNote).toHashCode();
    }
}
