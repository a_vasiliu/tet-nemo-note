package com.doing.nemo.claims.entity.enumerated.RepairEnum;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;

public enum RepairProcedureEnum implements Serializable {
    ALD_REPAIRING("ald_repairing"),
    STANDARD("standard");

    private static Logger LOGGER = LoggerFactory.getLogger(RepairProcedureEnum.class);
    private String repairProcedureEntity;

    RepairProcedureEnum(String repairProcedureEntity) {
        this.repairProcedureEntity = repairProcedureEntity;
    }

    @JsonCreator
    public static RepairProcedureEnum create(String repairProcedureEntity) {

        repairProcedureEntity = repairProcedureEntity.replace(" - ", "_");
        repairProcedureEntity = repairProcedureEntity.replace('-', '_');
        repairProcedureEntity = repairProcedureEntity.replace(' ', '_');

        if (repairProcedureEntity != null) {
            for (RepairProcedureEnum val : RepairProcedureEnum.values()) {
                if (repairProcedureEntity.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        LOGGER.debug("Bad parameters exception. Enum class " + RepairProcedureEnum.class.getSimpleName() + " doesn't accept this value: " + repairProcedureEntity);
        throw new BadParametersException("Bad parameters exception. Enum class " + RepairProcedureEnum.class.getSimpleName() + " doesn't accept this value: " + repairProcedureEntity);
    }

    public String getValue() {
        return this.repairProcedureEntity.toUpperCase();
    }

    @JsonValue
    public String toValue() {
        for (RepairProcedureEnum val : RepairProcedureEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }
}
