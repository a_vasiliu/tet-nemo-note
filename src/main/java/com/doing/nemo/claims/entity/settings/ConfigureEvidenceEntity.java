package com.doing.nemo.claims.entity.settings;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "configure_evidence")
@JsonIgnoreProperties({"hibernate_lazy_initializer", "handler"})
public class ConfigureEvidenceEntity implements Serializable {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Type(type = "org.hibernate.type.PostgresUUIDType")
    private UUID id;

    @Column(name = "practical_state_it")
    private String practicalStateIt;

    @Column(name = "practical_state_en")
    private String practicalStateEn;

    @Column(name = "days_of_stay_in_the_state")
    private Long daysOfStayInTheState;

    @Column(name = "is_active")
    private Boolean isActive;

    public ConfigureEvidenceEntity() {
    }

    public ConfigureEvidenceEntity(String practicalStateIt, String practicalStateEn, Long daysOfStayInTheState, Boolean isActive) {
        this.practicalStateIt = practicalStateIt;
        this.practicalStateEn = practicalStateEn;
        this.daysOfStayInTheState = daysOfStayInTheState;
        this.isActive = isActive;
    }

    public UUID getId() {
        return id;
    }

    public String getPracticalStateIt() {
        return practicalStateIt;
    }

    public void setPracticalStateIt(String practicalStateIt) {
        this.practicalStateIt = practicalStateIt;
    }

    public String getPracticalStateEn() {
        return practicalStateEn;
    }

    public void setPracticalStateEn(String practicalStateEn) {
        this.practicalStateEn = practicalStateEn;
    }

    public Long getDaysOfStayInTheState() {
        return daysOfStayInTheState;
    }

    public void setDaysOfStayInTheState(Long daysOfStayInTheState) {
        this.daysOfStayInTheState = daysOfStayInTheState;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    @JsonIgnore
    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ConfigureEvidenceEntity that = (ConfigureEvidenceEntity) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getPracticalStateIt(), that.getPracticalStateIt()) &&
                Objects.equals(getPracticalStateEn(), that.getPracticalStateEn()) &&
                Objects.equals(getDaysOfStayInTheState(), that.getDaysOfStayInTheState()) &&
                Objects.equals(getActive(), that.getActive());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getPracticalStateIt(), getPracticalStateEn(), getDaysOfStayInTheState(), getActive());
    }

    @Override
    public String toString() {
        return "ConfigureEvidenceEntity{" +
                "id=" + id +
                ", practicalStateIt='" + practicalStateIt + '\'' +
                ", practicalStateEn='" + practicalStateEn + '\'' +
                ", daysOfStayInTheState=" + daysOfStayInTheState +
                ", isActive=" + isActive +
                '}';
    }
}
