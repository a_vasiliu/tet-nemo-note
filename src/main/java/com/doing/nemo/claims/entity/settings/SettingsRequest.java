package com.doing.nemo.claims.entity.settings;

import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsFlowEnum;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;

import java.util.ArrayList;
import java.util.List;

public class SettingsRequest {

    @JsonProperty("cod_contract_type")
    private String codContractType;

    @JsonProperty("description")
    private String description;

    @JsonProperty("unlocking_entry")
    private Boolean flagWS;

    @JsonProperty("events_deductible")
    private Double eventsDeductible;

    @JsonProperty("loss_deductible_keys")
    private Double lossDeductibleKeys;

    @JsonProperty("ownership")
    private String ownership;

    @JsonProperty("ctrnote")
    private String ctrnote;

    @JsonProperty("ricaricar")
    private Boolean ricaricar;

    @JsonProperty("franchise")
    private Double franchise;

    @JsonProperty("flow_contract_type")
    private List<FlowContract> flowContracts;

    @JsonProperty("default_flow")
    private ClaimsFlowEnum defaultFlow;

    @JsonProperty("is_active")
    private Boolean isActive = true;

    public String getCodContractType() {
        return codContractType;
    }

    public void setCodContractType(String codContractType) {
        this.codContractType = codContractType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getFlagWS() {
        return flagWS;
    }

    public void setFlagWS(Boolean flagWS) {
        this.flagWS = flagWS;
    }

    public Double getEventsDeductible() {
        return eventsDeductible;
    }

    public void setEventsDeductible(Double eventsDeductible) {
        this.eventsDeductible = eventsDeductible;
    }

    public Double getLossDeductibleKeys() {
        return lossDeductibleKeys;
    }

    public void setLossDeductibleKeys(Double lossDeductibleKeys) {
        this.lossDeductibleKeys = lossDeductibleKeys;
    }

    public String getOwnership() {
        return ownership;
    }

    public void setOwnership(String ownership) {
        this.ownership = ownership;
    }

    public String getCtrnote() {
        return ctrnote;
    }

    public void setCtrnote(String ctrnote) {
        this.ctrnote = ctrnote;
    }

    public List<FlowContract> getFlowContracts() {
        if(flowContracts == null){
            return null;
        }
        return new ArrayList<>(flowContracts);
    }

    public void setFlowContracts(List<FlowContract> flowContracts) {
        if(flowContracts != null)
        {
            this.flowContracts = new ArrayList<>(flowContracts);
        } else {
            this.flowContracts = null;
        }
    }

    public ClaimsFlowEnum getDefaultFlow() {
        return defaultFlow;
    }

    public void setDefaultFlow(ClaimsFlowEnum defaultFlow) {
        this.defaultFlow = defaultFlow;
    }

    public Boolean getRicaricar() {
        return ricaricar;
    }

    public void setRicaricar(Boolean ricaricar) {
        this.ricaricar = ricaricar;
    }

    public Double getFranchise() {
        return franchise;
    }

    public void setFranchise(Double franchise) {
        this.franchise = franchise;
    }

    public Boolean getActive() {
        return isActive;
    }

    @JsonSetter
    public void setActive(Boolean active) {
        if(active != null)
        isActive = active;
    }

    @Override
    public String toString() {
        return "SettingsRequest{" +
                "codContractType='" + codContractType + '\'' +
                ", description='" + description + '\'' +
                ", flagWS=" + flagWS +
                ", eventsDeductible=" + eventsDeductible +
                ", lossDeductibleKeys=" + lossDeductibleKeys +
                ", ownership='" + ownership + '\'' +
                ", ctrnote='" + ctrnote + '\'' +
                ", ricaricar=" + ricaricar +
                ", franchise=" + franchise +
                ", flowContracts=" + flowContracts +
                ", defaultFlow=" + defaultFlow +
                '}';
    }
}
