package com.doing.nemo.claims.entity.jsonb.dataType;

import com.doing.nemo.claims.common.hibernate.JsonDataType;
import com.doing.nemo.claims.util.NemoInstantDeserializerWithZoneEuropeRome;
import com.doing.nemo.middleware.client.payload.request.MiddlewareUpdateIncidentsRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;

public class JsonDataMiddlewareUpdateIncidentsRequest extends JsonDataType {
    @Override
    public Object nullSafeGet(final ResultSet rs, final String[] names, final SharedSessionContractImplementor session,
                              final Object owner) throws HibernateException, SQLException {
        final String cellContent = rs.getString(names[0]);
        if (cellContent == null) {
            return null;
        }
        try {
            final ObjectMapper mapper = new ObjectMapper();
            NemoInstantDeserializerWithZoneEuropeRome nemoInstantDeserializerWithZoneEuropeRome = new NemoInstantDeserializerWithZoneEuropeRome();
            SimpleModule instatDeserializerModule = new SimpleModule();
            instatDeserializerModule.addDeserializer(Instant.class, nemoInstantDeserializerWithZoneEuropeRome);
            mapper.registerModule(instatDeserializerModule);
            return mapper.readValue(cellContent.getBytes("UTF-8"), MiddlewareUpdateIncidentsRequest.class);
        } catch (final Exception ex) {
            throw new RuntimeException("Failed to convert String to MiddlewareUpdateIncidentsRequest: " + ex.getMessage(), ex);
        }
    }

}
