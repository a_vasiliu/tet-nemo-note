package com.doing.nemo.claims.entity;

import com.doing.nemo.claims.dto.AssigneeRepair;
import com.doing.nemo.claims.entity.enumerated.DamagedEnum.VehicleEnum.VehicleTypeEnum;
import com.doing.nemo.claims.entity.enumerated.RepairEnum.ManagementTypeEnum;
import com.doing.nemo.claims.entity.enumerated.RepairEnum.RepairProcedureEnum;
import com.doing.nemo.claims.entity.enumerated.RepairEnum.RepairStatusEnum;
import com.doing.nemo.claims.entity.jsonb.Authority;
import com.doing.nemo.claims.entity.jsonb.counterparty.InsuranceCompanyCounterparty;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.ImpactPoint;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.Insured;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbDriver.Driver;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbVehicle.Vehicle;
import com.doing.nemo.claims.entity.jsonb.dataType.*;
import com.doing.nemo.claims.entity.jsonb.forms.jsonbForms.Attachment;
import com.doing.nemo.claims.entity.jsonb.repair.Canalization;
import com.doing.nemo.claims.entity.jsonb.repair.HistoricalCounterparty;
import com.doing.nemo.claims.entity.jsonb.repair.LastContact;
import com.doing.nemo.claims.entity.settings.ManagerEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

@Entity
@DynamicInsert
@JsonIgnoreProperties({"hibernateLazyInitializer", "hibernate_lazy_initializer", "handler"})
@Table(name = "counterparty_new")
@TypeDefs({
        @TypeDef(name = "JsonDataAssignedToType", typeClass = JsonDataAssignedToType.class),
        @TypeDef(name = "JsonDataAttachmentListType", typeClass = JsonDataAttachmentListType.class),
        @TypeDef(name = "JsonDataCanalizationType", typeClass = JsonDataCanalizationType.class),
        @TypeDef(name = "JsonDataHistoricalCounterpartyType", typeClass = JsonDataHistoricalCounterpartyType.class),
        @TypeDef(name = "JsonDataImpactPointType", typeClass = JsonDataImpactPointType.class),
        @TypeDef(name = "JsonDataInsuranceCompanyCounterpartyType", typeClass = JsonDataInsuranceCompanyCounterpartyType.class),
        @TypeDef(name = "JsonDataInsuredType", typeClass = JsonDataInsuredType.class),
        @TypeDef(name = "JsonDataLastContactType", typeClass = JsonDataLastContactType.class),
        @TypeDef(name = "JsonDataVehicleType", typeClass = JsonDataVehicleType.class),
        @TypeDef(name = "JsonDataDriverType", typeClass = JsonDataDriverType.class),
        @TypeDef(name = "JsonDataAuthorityType", typeClass = JsonDataAuthorityType.class)

})
@SqlResultSetMapping(name="CounterpartyNewResults",
        entities={
                @EntityResult(entityClass= CounterpartyNewEntity.class)}
)
public class CounterpartyNewEntity implements Serializable {

    @Id
    @Column(name = "counterparty_id")
    private String counterpartyId;

    @Column(name = "practice_id_counterparty", unique = true, nullable = false , updatable = false)
    //@Generated(GenerationTime.INSERT)
    private Long practiceIdCounterparty;

    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private VehicleTypeEnum type;

    @Enumerated(EnumType.STRING)
    @Column(name = "status_repair")
    private RepairStatusEnum repairStatus;

    @Enumerated(EnumType.STRING)
    @Column(name = "procedure_repair")
    private RepairProcedureEnum repairProcedure;

    @Column(name = "motivation")
    private String motivation;

    @Column(name = "user_create")
    private String userCreate;

    @Column(name = "created_at")
    private Instant createdAt;

    @Column(name = "repair_created_at")
    private Instant repairCreatedAt;

    @Column(name = "user_update")
    private String userUpdate;

    @Column(name = "updated_at")
    private Instant updateAt;

    @Type(type = "JsonDataAssignedToType")
    @Column(name = "assigned_to")
    private AssigneeRepair assignedTo;

    @Column(name = "assigned_at")
    private Instant assignedAt;

    @Type(type = "JsonDataInsuredType")
    @Column(name = "insured")
    private Insured insured;

    @Type(type = "JsonDataInsuranceCompanyCounterpartyType")
    @Column(name = "insurance_company")
    private InsuranceCompanyCounterparty insuranceCompany;

    @Type(type = "JsonDataDriverType")
    @Column(name = "driver")
    private Driver driver;

    @Type(type = "JsonDataVehicleType")
    @Column(name = "vehicle")
    private Vehicle vehicle;

    @JsonProperty("is_cai_signed")
    @Column(name = "is_cai_signed")
    private Boolean isCaiSigned;

    @Type(type = "JsonDataImpactPointType")
    @Column(name = "impact_point")
    private ImpactPoint impactPoint;

    @Column(name = "responsible")
    private Boolean responsible;

    @Column(name = "eligibility")
    private Boolean eligibility;

    @Column(name = "is_complete_documentation")
    private Boolean isCompleteDocumentation;

    @Column(name = "replacement_car")
    private Boolean replacementCar;

    @Column(name = "replacement_plate")
    private String replacementPlate;

    @Type(type = "JsonDataAttachmentListType")
    @Column(name = "attachments")
    private List<Attachment> attachments;

    @Type(type = "JsonDataHistoricalCounterpartyType")
    @Column(name = "historicals")
    private List<HistoricalCounterparty> historicals;

    @Column(name = "description")
    private String description;

    @Type(type = "JsonDataLastContactType")
    @Column(name = "last_contact")
    private List<LastContact> lastContact;

    @OneToOne
    @JoinColumn(name = "manager")
    private ManagerEntity manager;

    @Type(type = "JsonDataCanalizationType")
    @Column(name = "canalization")
    private Canalization canalization;

    @Column(name = "policy_number")
    private String policyNumber;

    @Column(name = "policy_beginning_validity")
    private Instant policyBeginningValidity;

    @Column(name = "policy_end_validity")
    private Instant policyEndValidity;

    @Enumerated(EnumType.STRING)
    @Column(name = "management_type")
    private ManagementTypeEnum managementType;

    @Column(name = "asked_for_damages")
    private Boolean askedForDamages;

    @Column(name = "legal_or_consultant")
    private Boolean legalOrConsultant;

    @Column(name = "date_request_damages")
    private Instant dateRequestDamages;

    @Column(name = "expiration_date")
    private Instant expirationInstant;

    @Column(name = "legal")
    private String legal;

    @Column(name = "email_legal")
    private String emailLegal;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "claims_id")
    private ClaimsNewEntity claims;

    @Column(name = "is_ald")
    private Boolean isAld;

    @Type(type = "JsonDataAuthorityType")
    @Column(name = "authorities")
    private List<Authority> authorities;

    @Column(name = "is_read_msa")
    private Boolean isReadMsa;

    public String getReplacementPlate() {
        return replacementPlate;
    }

    public void setReplacementPlate(String replacementPlate) {
        this.replacementPlate = replacementPlate;
    }

    public String getMotivation() {
        return motivation;
    }

    public void setMotivation(String motivation) {
        this.motivation = motivation;
    }

    public Boolean getIsReadMsa() {
        return isReadMsa;
    }

    public void setIsReadMsa(Boolean readMsa) {
        isReadMsa = readMsa;
    }

    public List<Authority> getAuthorities() {
        if(authorities == null){
            return null;
        }
        return new ArrayList<>(authorities);
    }

    public void setAuthorities(List<Authority> authorities) {
        if(authorities != null)
        {
            this.authorities =  new ArrayList<>(authorities);
        } else {
            this.authorities = null;
        }
    }

    public Long getPracticeIdCounterparty() {
        return practiceIdCounterparty;
    }

    public void setPracticeIdCounterparty(Long practiceIdCounterparty) {
        this.practiceIdCounterparty = practiceIdCounterparty;
    }

    public Boolean getIsAld() {
        return isAld;
    }

    public void setIsAld(Boolean isAld) {
        this.isAld = isAld;
    }

    public ClaimsNewEntity getClaims() {
        return claims;
    }

    public void setClaims(ClaimsNewEntity claims) {
        this.claims = claims;
    }

    public ManagementTypeEnum getManagementType() {
        return managementType;
    }

    public void setManagementType(ManagementTypeEnum managementType) {
        this.managementType = managementType;
    }

    public Boolean getAskedForDamages() {
        return askedForDamages;
    }

    public void setAskedForDamages(Boolean askedForDamages) {
        this.askedForDamages = askedForDamages;
    }

    public Boolean getLegalOrConsultant() {
        return legalOrConsultant;
    }

    public void setLegalOrConsultant(Boolean legalOrConsultant) {
        this.legalOrConsultant = legalOrConsultant;
    }

    public Instant getInstantRequestDamages() {
        return dateRequestDamages;
    }

    public void setInstantRequestDamages(Instant dateRequestDamages) {
        this.dateRequestDamages = dateRequestDamages;
    }

    public Instant getExpirationInstant() {
        return expirationInstant;
    }

    public void setExpirationInstant(Instant expirationInstant) {
        this.expirationInstant = expirationInstant;
    }

    public String getLegal() {
        return legal;
    }

    public void setLegal(String legal) {
        this.legal = legal;
    }

    public String getEmailLegal() {
        return emailLegal;
    }

    public void setEmailLegal(String emailLegal) {
        this.emailLegal = emailLegal;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public Instant getPolicyBeginningValidity() {
        return policyBeginningValidity;
    }

    public void setPolicyBeginningValidity(Instant policyBeginningValidity) {
        this.policyBeginningValidity = policyBeginningValidity;
    }

    public Instant getPolicyEndValidity() {
        return policyEndValidity;
    }

    public void setPolicyEndValidity(Instant policyEndValidity) {
        this.policyEndValidity = policyEndValidity;
    }

    public String getCounterpartyId() {
        return counterpartyId;
    }

    public void setCounterpartyId(String counterpartyId) {
        this.counterpartyId = counterpartyId;
    }

    public VehicleTypeEnum getType() {
        return type;
    }

    public void setType(VehicleTypeEnum type) {
        this.type = type;
    }

    public RepairStatusEnum getRepairStatus() {
        return repairStatus;
    }

    @JsonIgnore
    public void setRepairStatus(RepairStatusEnum repairStatus) {
        this.repairStatus = repairStatus;
    }

    public Insured getInsured() {
        return insured;
    }

    public void setInsured(Insured insured) {
        this.insured = insured;
    }

    public InsuranceCompanyCounterparty getInsuranceCompany() {
        return insuranceCompany;
    }

    public void setInsuranceCompany(InsuranceCompanyCounterparty insuranceCompany) {
        this.insuranceCompany = insuranceCompany;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public Boolean getCaiSigned() {
        return isCaiSigned;
    }

    public void setCaiSigned(Boolean isCaiSigned) {
        this.isCaiSigned = isCaiSigned;
    }

    public ImpactPoint getImpactPoint() {
        return impactPoint;
    }

    public void setImpactPoint(ImpactPoint impactPoint) {
        this.impactPoint = impactPoint;
    }

    public Boolean getResponsible() {
        return responsible;
    }

    public void setResponsible(Boolean responsible) {
        this.responsible = responsible;
    }

    public List<Attachment> getAttachments() {
        if(attachments == null){
            return null;
        }
        return new ArrayList<>(attachments);
    }

    public void setAttachments(List<Attachment> attachments) {
        if(attachments != null){
            this.attachments = new ArrayList<>(attachments);
        } else {
            this.attachments = null;
        }
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public RepairProcedureEnum getRepairProcedure() {
        return repairProcedure;
    }

    public void setRepairProcedure(RepairProcedureEnum repairProcedure) {
        this.repairProcedure = repairProcedure;
    }

    public String getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public String getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(String userUpdate) {
        this.userUpdate = userUpdate;
    }

    public Instant getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Instant updateAt) {
        this.updateAt = updateAt;
    }

    public AssigneeRepair getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(AssigneeRepair assignedTo) {
        this.assignedTo = assignedTo;
    }

    public Instant getAssignedAt() {
        return assignedAt;
    }

    public void setAssignedAt(Instant assignedAt) {
        this.assignedAt = assignedAt;
    }

    public Boolean getIsCompleteDocumentation() {
        return isCompleteDocumentation;
    }

    public void setIsCompleteDocumentation(Boolean completeDocumentation) {
        isCompleteDocumentation = completeDocumentation;
    }

    public List<HistoricalCounterparty> getHistoricals() {
        if(historicals == null){
            return null;
        }
        return new ArrayList<>(historicals);
    }

    public void setHistoricals(List<HistoricalCounterparty> historicals) {
        if(historicals != null)
        {
            this.historicals = new ArrayList<>(historicals);
        } else {
            this.historicals = null;
        }
    }

    public void addHistorical(HistoricalCounterparty historical) {
        if (this.historicals == null) {
            setHistoricals(new LinkedList<HistoricalCounterparty>());
        }
        this.historicals.add(historical);
    }

    public List<LastContact> getLastContact() {
        if(lastContact == null){
            return null;
        }
        return new ArrayList<>(lastContact);
    }

    public void setLastContact(List<LastContact> lastContact) {
        if(lastContact != null)
        {
            this.lastContact = new ArrayList<>(lastContact);
        } else {
            this.lastContact = null;
        }
    }

    public ManagerEntity getManager() {
        return manager;
    }

    public void setManager(ManagerEntity manager) {
        this.manager = manager;
    }

    public Canalization getCanalization() {
        return canalization;
    }

    public void setCanalization(Canalization canalization) {
        this.canalization = canalization;
    }

    public Boolean getEligibility() {
        return eligibility;
    }

    public void setEligibility(Boolean eligibility) {
        this.eligibility = eligibility;
    }

    public Boolean getAld() {
        return isAld;
    }

    public void setAld(Boolean ald) {
        isAld = ald;
    }

    public Instant getRepairCreatedAt() {
        return repairCreatedAt;
    }

    public void setRepairCreatedAt(Instant repairCreatedAt) {
        this.repairCreatedAt = repairCreatedAt;
    }

    public Boolean getReplacementCar() {
        return replacementCar;
    }

    public void setReplacementCar(Boolean replacementCar) {
        this.replacementCar = replacementCar;
    }

    public Boolean getCompleteDocumentation() {
        return isCompleteDocumentation;
    }

    public void setCompleteDocumentation(Boolean completeDocumentation) {
        isCompleteDocumentation = completeDocumentation;
    }

    public Instant getDateRequestDamages() {
        return dateRequestDamages;
    }

    public void setDateRequestDamages(Instant dateRequestDamages) {
        this.dateRequestDamages = dateRequestDamages;
    }

    public Boolean getReadMsa() {
        return isReadMsa;
    }

    public void setReadMsa(Boolean readMsa) {
        isReadMsa = readMsa;
    }

    public Boolean isWebsinRepair() {
        if(this.getRepairStatus()!=null && this.getRepairStatus().equals(RepairStatusEnum.WSREPAIR)){
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "CounterpartyEntity{" +
                "counterpartyId='" + counterpartyId + '\'' +
                ", practiceIdCounterparty=" + practiceIdCounterparty +
                ", type=" + type +
                ", repairStatus=" + repairStatus +
                ", repairProcedure=" + repairProcedure +
                ", motivation='" + motivation + '\'' +
                ", userCreate='" + userCreate + '\'' +
                ", createdAt=" + createdAt +
                ", repairCreatedAt=" + repairCreatedAt +
                ", userUpdate='" + userUpdate + '\'' +
                ", updateAt=" + updateAt +
                ", assignedTo=" + assignedTo +
                ", assignedAt=" + assignedAt +
                ", insured=" + insured +
                ", insuranceCompany=" + insuranceCompany +
                ", driver=" + driver +
                ", vehicle=" + vehicle +
                ", isCaiSigned=" + isCaiSigned +
                ", impactPoint=" + impactPoint +
                ", responsible=" + responsible +
                ", eligibility=" + eligibility +
                ", isCompleteDocumentation=" + isCompleteDocumentation +
                ", replacementCar=" + replacementCar +
                ", replacementPlate='" + replacementPlate + '\'' +
                ", attachments=" + attachments +
                ", historicals=" + historicals +
                ", description='" + description + '\'' +
                ", lastContact=" + lastContact +
                ", manager=" + manager +
                ", canalization=" + canalization +
                ", policyNumber='" + policyNumber + '\'' +
                ", policyBeginningValidity=" + policyBeginningValidity +
                ", policyEndValidity=" + policyEndValidity +
                ", managementType=" + managementType +
                ", askedForDamages=" + askedForDamages +
                ", legalOrConsultant=" + legalOrConsultant +
                ", dateRequestDamages=" + dateRequestDamages +
                ", expirationInstant=" + expirationInstant +
                ", legal='" + legal + '\'' +
                ", emailLegal='" + emailLegal + '\'' +
                ", claims=no info logged " + (claims != null ? "- " + claims.getPracticeId(): "")  +
                ", isAld=" + isAld +
                ", authorities=" + authorities +
                ", isReadMsa=" + isReadMsa +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CounterpartyNewEntity that = (CounterpartyNewEntity) o;
        return Objects.equals(getCounterpartyId(), that.getCounterpartyId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCounterpartyId(), getPracticeIdCounterparty());
    }
}
