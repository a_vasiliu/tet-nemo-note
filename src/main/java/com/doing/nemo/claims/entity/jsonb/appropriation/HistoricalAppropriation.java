package com.doing.nemo.claims.entity.jsonb.appropriation;

import com.doing.nemo.claims.entity.enumerated.AppropriationEnum.AppropriationStatusEnum;
import com.doing.nemo.claims.entity.enumerated.EventTypeEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class HistoricalAppropriation implements Serializable {

    private static final long serialVersionUID = -5899684862852726273L;

    @JsonProperty("event_type")
    private EventTypeEnum eventType;

    @JsonProperty("old_status")
    private AppropriationStatusEnum statusEntityOld;

    @JsonProperty("new_status")
    private AppropriationStatusEnum statusEntityNew;

    @JsonProperty("update_at")
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    private Date updateAt;

    @JsonProperty("user_id")
    private String userId;

    @JsonProperty("user_name")
    private String userName;

    @JsonProperty("comunication_description")
    private String comunicationDescription;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public AppropriationStatusEnum getStatusEntityOld() {
        return statusEntityOld;
    }

    public void setStatusEntityOld(AppropriationStatusEnum statusEntityOld) {
        this.statusEntityOld = statusEntityOld;
    }

    public AppropriationStatusEnum getStatusEntityNew() {
        return statusEntityNew;
    }

    public void setStatusEntityNew(AppropriationStatusEnum statusEntityNew) {
        this.statusEntityNew = statusEntityNew;
    }

    public Date getUpdateAt() {
        if(updateAt == null){
            return null;
        }
        return (Date)updateAt.clone();
    }

    public void setUpdateAt(Date updateAt) {
        if(updateAt != null){
            this.updateAt =(Date)updateAt.clone();
        } else {
            this.updateAt = null;
        }
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public EventTypeEnum getEventType() {
        return eventType;
    }

    public void setEventType(EventTypeEnum eventType) {
        this.eventType = eventType;
    }

    public String getComunicationDescription() {
        return comunicationDescription;
    }

    public void setComunicationDescription(String comunicationDescription) {
        this.comunicationDescription = comunicationDescription;
    }

    @Override
    public String toString() {
        return "HistoricalAppropriation{" +
                "eventType=" + eventType +
                ", statusEntityOld=" + statusEntityOld +
                ", statusEntityNew=" + statusEntityNew +
                ", updateAt=" + updateAt +
                ", userId='" + userId + '\'' +
                ", comunicationDescription='" + comunicationDescription + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        HistoricalAppropriation that = (HistoricalAppropriation) o;

        return new EqualsBuilder().append(eventType, that.eventType)
                .append(statusEntityOld, that.statusEntityOld)
                .append(statusEntityNew, that.statusEntityNew)
                .append(updateAt, that.updateAt).append(userId, that.userId)
                .append(userName, that.userName).append(comunicationDescription, that.comunicationDescription).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(eventType)
                .append(statusEntityOld).append(statusEntityNew).append(updateAt).append(userId)
                .append(userName).append(comunicationDescription).toHashCode();
    }
}
