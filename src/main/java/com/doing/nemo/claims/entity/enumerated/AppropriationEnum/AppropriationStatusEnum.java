package com.doing.nemo.claims.entity.enumerated.AppropriationEnum;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public enum AppropriationStatusEnum implements Serializable {
    SEQUESTERED("sequestered"),
    RELEASED("released"),
    WAIT_LOST_POSSESSION("wait_lost_possession"),
    LOST_POSSESSION("lost_possession"),
    WAIT_TO_RETURN_POSSESSION("wait_to_return_possession"),
    RETURN_TO_POSSESSION("return_to_possession"),
    TO_REREGISTER("to_reregister"),
    TO_WORK("to_work"),
    BOOK_DUPLICATION("book_duplication"),
    STAMP("stamp"),
    DEMOLITION("demolition"),
    CLOSE("close"),
    STAMP_CLOSED("stamp_closed"),
    DEMOLITION_CLOSED("demolition_closed"),
    BOOK_DUPLICATION_CLOSED("book_duplication_closed"),
    TO_REREGISTER_CLOSED("to_reregister_closed");

    private static Logger LOGGER = LoggerFactory.getLogger(AppropriationStatusEnum.class);

    private String statusEntity;

    private AppropriationStatusEnum(String statusEntity) {
        this.statusEntity = statusEntity;
    }

    @JsonCreator
    public static AppropriationStatusEnum create(String statusEntity) {

        statusEntity = statusEntity.replace(" - ", "_");
        statusEntity = statusEntity.replace('-', '_');
        statusEntity = statusEntity.replace(' ', '_');

        if (statusEntity != null) {
            for (AppropriationStatusEnum val : AppropriationStatusEnum.values()) {
                if (statusEntity.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }

        LOGGER.debug("Bad parameters exception. Enum class " + AppropriationStatusEnum.class.getSimpleName() + " doesn't accept this value: " + statusEntity);
        throw new BadParametersException("Bad parameters exception. Enum class " + AppropriationStatusEnum.class.getSimpleName() + " doesn't accept this value: " + statusEntity);
    }

    public static List<AppropriationStatusEnum> getStatusListFromString(List<String> statusListString) {

        List<AppropriationStatusEnum> statusList = new LinkedList<>();
        for (String att : statusListString) {
            statusList.add(AppropriationStatusEnum.create(att));
        }
        return statusList;
    }

    public String getValue() {
        return this.statusEntity.toUpperCase();
    }

    @JsonValue
    public String toValue() {
        for (AppropriationStatusEnum val : AppropriationStatusEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }
}