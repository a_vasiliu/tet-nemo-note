package com.doing.nemo.claims.entity.jsonb.metadata;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Metadata implements Serializable {

    private static final long serialVersionUID = -260081523602151498L;

    @JsonProperty("plate")
    private String plate;

    @JsonProperty("option_id")
    private Integer optionId;

    public Metadata() {
    }

    public Metadata(String plate, Integer optionId) {
        this.plate = plate;
        this.optionId = optionId;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public Integer getOptionId() {
        return optionId;
    }

    public void setOptionId(Integer optionId) {
        this.optionId = optionId;
    }

    @Override
    public String toString() {
        return "metadata{" +
                "plate='" + plate + '\'' +
                ", option_id=" + optionId +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Metadata metadata = (Metadata) o;

        return new EqualsBuilder().append(plate, metadata.plate).append(optionId, metadata.optionId).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(plate).append(optionId).toHashCode();
    }
}
