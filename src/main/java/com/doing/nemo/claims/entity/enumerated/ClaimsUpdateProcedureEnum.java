package com.doing.nemo.claims.entity.enumerated;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;

public enum ClaimsUpdateProcedureEnum implements Serializable {
    UPDATE("update"),
    CLAIM_TRANSFER("claim_transfer"),
    CHANGE_POLICY("change_policy");

    private String updateProcedure;

    private static Logger LOGGER = LoggerFactory.getLogger(ClaimsUpdateProcedureEnum.class);

    ClaimsUpdateProcedureEnum(String updateProcedure) {
        this.updateProcedure = updateProcedure;
    }

    public String getValue() {
        return this.updateProcedure.toUpperCase();
    }

    @JsonCreator
    public static ClaimsUpdateProcedureEnum create(String updateProcedure) {

        updateProcedure = updateProcedure.replace(" - ", "_");
        updateProcedure = updateProcedure.replace('-', '_');
        updateProcedure = updateProcedure.replace(' ', '_');

        if (updateProcedure != null) {
            for (ClaimsUpdateProcedureEnum val : ClaimsUpdateProcedureEnum.values()) {
                if (updateProcedure.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        LOGGER.debug("Bad parameters exception. Enum class " + ClaimsUpdateProcedureEnum.class.getSimpleName() + " doesn't accept this value: " + updateProcedure);
        throw new BadParametersException("Bad parameters exception. Enum class " + ClaimsUpdateProcedureEnum.class.getSimpleName() + " doesn't accept this value: " + updateProcedure);
    }

    @JsonValue
    public String toValue() {
        for (ClaimsUpdateProcedureEnum val : ClaimsUpdateProcedureEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }
}
