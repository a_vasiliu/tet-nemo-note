package com.doing.nemo.claims.entity.settings;


import com.doing.nemo.claims.entity.enumerated.RiskEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "risk")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class RiskEntity implements Serializable {

    @Id
    @JsonIgnore
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )

    @Type(type = "org.hibernate.type.PostgresUUIDType")
    private UUID id;


    @Column(name = "risk_id", unique = true, insertable = false, updatable = false)
    private Long riskId;

    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private RiskEnum type;

    @Column(name = "description")
    private String description;

    @Column(name = "maximal")
    private Double maximal;

    @Column(name = "exemption")
    private Double exemption;

    @Column(name = "overdraft")
    private Double overdraft;

    @Column(name = "active")
    private Boolean active;

    public RiskEntity() {
    }

    public RiskEntity(Long riskId, RiskEnum type, String description, Double maximal, Double exemption, Double overdraft, Boolean active) {
        this.riskId = riskId;
        this.type = type;
        this.description = description;
        this.maximal = maximal;
        this.exemption = exemption;
        this.overdraft = overdraft;
        this.active = active;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Long getRiskId() {
        return riskId;
    }

    public void setRiskId(Long riskId) {
        this.riskId = riskId;
    }

    public RiskEnum getType() {
        return type;
    }

    public void setType(RiskEnum type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getMaximal() {
        return maximal;
    }

    public void setMaximal(Double maximal) {
        this.maximal = maximal;
    }

    public Double getExemption() {
        return exemption;
    }

    public void setExemption(Double exemption) {
        this.exemption = exemption;
    }

    public Double getOverdraft() {
        return overdraft;
    }

    public void setOverdraft(Double overdraft) {
        this.overdraft = overdraft;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return "RiskEntity{" +
                "id=" + id +
                ", riskId=" + riskId +
                ", type='" + type + '\'' +
                ", description='" + description + '\'' +
                ", maximal=" + maximal +
                ", exemption=" + exemption +
                ", overdraft=" + overdraft +
                ", active=" + active +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RiskEntity that = (RiskEntity) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getRiskId(), that.getRiskId()) &&
                getType() == that.getType() &&
                Objects.equals(getDescription(), that.getDescription()) &&
                Objects.equals(getMaximal(), that.getMaximal()) &&
                Objects.equals(getExemption(), that.getExemption()) &&
                Objects.equals(getOverdraft(), that.getOverdraft()) &&
                Objects.equals(getActive(), that.getActive());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getRiskId(), getType(), getDescription(), getMaximal(), getExemption(), getOverdraft(), getActive());
    }
}
