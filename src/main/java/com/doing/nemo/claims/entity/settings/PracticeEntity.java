package com.doing.nemo.claims.entity.settings;

import com.doing.nemo.claims.entity.enumerated.PracticeStatusEnum;
import com.doing.nemo.claims.entity.enumerated.PracticeTypeEnum;
import com.doing.nemo.claims.entity.jsonb.Authority;
import com.doing.nemo.claims.entity.jsonb.AuthorityPractice;
import com.doing.nemo.claims.entity.jsonb.Processing;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.Contract;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.Customer;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.FleetManager;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbVehicle.Vehicle;
import com.doing.nemo.claims.entity.jsonb.dataType.*;
import com.doing.nemo.claims.entity.jsonb.forms.jsonbForms.Attachment;
import com.doing.nemo.claims.entity.jsonb.practice.*;
import org.hibernate.annotations.*;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "practice")
@TypeDefs({
        @TypeDef(name = "JsonDataAttachmentListType", typeClass = JsonDataAttachmentListType.class),
        @TypeDef(name = "JsonDataProcessingListType", typeClass = JsonDataProcessingListType.class),
        @TypeDef(name = "JsonDataVehicleEsbType", typeClass = JsonDataVehicleEsbType.class),
        @TypeDef(name = "JsonDataCustomerEsbType", typeClass = JsonDataCustomerEsbType.class),
        @TypeDef(name = "JsonDataFleetManagerListType", typeClass = JsonDataFleetManagerListType.class),
        @TypeDef(name = "JsonDataFindingType", typeClass = JsonDataFindingType.class),
        @TypeDef(name = "JsonDataMisappropriationType", typeClass = JsonDataMisappropriationType.class),
        @TypeDef(name = "JsonDataSeizureType", typeClass = JsonDataSeizureType.class),
        @TypeDef(name = "JsonDataReleaseFromSeizureType", typeClass = JsonDataReleaseFromSeizureType.class),
        @TypeDef(name = "JsonDataContractEsbType", typeClass = JsonDataContractEsbType.class),
        @TypeDef(name = "JsonDataAuthorityPracticeType", typeClass = JsonDataAuthorityPracticeType.class),
        @TypeDef(name = "JsonDataTheftPracticeType", typeClass = JsonDataTheftPracticeType.class)
})
public class PracticeEntity implements Serializable {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Type(type = "org.hibernate.type.PostgresUUIDType")
    private UUID id;

    @Column(name = "parent_id")
    private UUID parentId;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private PracticeStatusEnum status;

    @Enumerated(EnumType.STRING)
    @Column(name = "practice_type")
    private PracticeTypeEnum practiceType;

    @Column(name = "practice_id", updatable = false)
    private Long practiceId;

    @Column(name = "created_at")
    private Instant createdAt;

    @Column(name = "updated_at")
    private Instant updatedAt;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "claims_id")
    private UUID claimsId;

    @Type(type = "JsonDataProcessingListType")
    @Column(name = "processing_list")
    private List<Processing> processingList;

    @Type(type = "JsonDataAttachmentListType")
    @Column(name = "attachment_list")
    private List<Attachment> attachmentList;

    @Type(type = "JsonDataVehicleEsbType")
    @Column(name = "vehicle")
    private Vehicle vehicle;

    @Type(type = "JsonDataCustomerEsbType")
    @Column(name = "customer")
    private Customer customer;

    @Type(type = "JsonDataContractEsbType")
    @Column(name = "contract")
    private Contract contract;

    @Type(type = "JsonDataFleetManagerListType")
    @Column(name = "fleet_managers")
    private List<FleetManager> fleetManagers;

    @Type(type = "JsonDataFindingType")
    @Column(name = "finding")
    private Finding finding;

    @Type(type = "JsonDataTheftPracticeType")
    @Column(name = "theft")
    private TheftPractice theft;

    @Type(type = "JsonDataMisappropriationType")
    @Column(name = "misappropriation")
    private Misappropriation misappropriation;

    @Type(type = "JsonDataSeizureType")
    @Column(name = "seizure")
    private Seizure seizure;

    @Type(type = "JsonDataReleaseFromSeizureType")
    @Column(name = "release_from_seizure")
    private ReleaseFromSeizure releaseFromSeizure;

    public PracticeEntity() {
    }

    public PracticeEntity(UUID parentId, PracticeStatusEnum status, PracticeTypeEnum practiceType, Long practiceId, Instant createdAt, Instant updatedAt, String createdBy, UUID claimsId, List<Processing> processingList, List<Attachment> attachmentList, Vehicle vehicle, Customer customer, Contract contract, List<FleetManager> fleetManagers, Finding finding, TheftPractice theft, Misappropriation misappropriation, Seizure seizure, ReleaseFromSeizure releaseFromSeizure) {
        this.parentId = parentId;
        this.status = status;
        this.practiceType = practiceType;
        this.practiceId = practiceId;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.createdBy = createdBy;
        this.claimsId = claimsId;
        if(processingList != null)
        {
            this.processingList = new ArrayList<>(processingList);
        }
        if(attachmentList != null)
        {
            this.attachmentList = new ArrayList<>(attachmentList);
        }
        this.vehicle = vehicle;
        this.customer = customer;
        this.contract = contract;
        if(fleetManagers != null)
        {
            this.fleetManagers = new ArrayList<>(fleetManagers);
        }
        this.finding = finding;
        this.theft = theft;
        this.misappropriation = misappropriation;
        this.seizure = seizure;
        this.releaseFromSeizure = releaseFromSeizure;
    }

    public List<FleetManager> getFleetManagers() {
        if(fleetManagers == null){
            return null;
        }
        return new ArrayList<>(fleetManagers);
    }

    public void setFleetManagers(List<FleetManager> fleetManagers) {
        if(fleetManagers != null)
        {
            this.fleetManagers =  new ArrayList<>(fleetManagers);
        } else {
            this.fleetManagers = null;
        }
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public PracticeStatusEnum getStatus() {
        return status;
    }

    public void setStatus(PracticeStatusEnum status) {
        this.status = status;
    }

    public PracticeTypeEnum getPracticeType() {
        return practiceType;
    }

    public void setPracticeType(PracticeTypeEnum practiceType) {
        this.practiceType = practiceType;
    }

    public Long getPracticeId() {
        return practiceId;
    }

    public void setPracticeId(Long practiceId) {
        this.practiceId = practiceId;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public Instant getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Instant updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public UUID getClaimsId() {
        return claimsId;
    }

    public void setClaimsId(UUID claimsId) {
        this.claimsId = claimsId;
    }

    public List<Processing> getProcessingList() {
        if(processingList == null){
            return null;
        }
        return new ArrayList<>(processingList);
    }

    public void setProcessingList(List<Processing> processingList) {
        if(processingList != null)
        {
            this.processingList = new ArrayList<>(processingList);
        } else {
            this.processingList = null;
        }
    }

    public List<Attachment> getAttachmentList() {
        if(attachmentList == null){
            return null;
        }
        return new ArrayList<>(attachmentList);
    }

    public void setAttachmentList(List<Attachment> attachmentList) {
        if(attachmentList != null)
        {
            this.attachmentList = new ArrayList<>(attachmentList);
        } else {
            this.attachmentList = null;
        }
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Contract getContract() {
        return contract;
    }

    public void setContract(Contract contract) {
        this.contract = contract;
    }

    public UUID getParentId() {
        return parentId;
    }

    public void setParentId(UUID parentId) {
        this.parentId = parentId;
    }

    public Finding getFinding() {
        return finding;
    }

    public void setFinding(Finding finding) {
        this.finding = finding;
    }

    public Misappropriation getMisappropriation() {
        return misappropriation;
    }

    public void setMisappropriation(Misappropriation misappropriation) {
        this.misappropriation = misappropriation;
    }

    public Seizure getSeizure() {
        return seizure;
    }

    public void setSeizure(Seizure seizure) {
        this.seizure = seizure;
    }

    public ReleaseFromSeizure getReleaseFromSeizure() {
        return releaseFromSeizure;
    }

    public void setReleaseFromSeizure(ReleaseFromSeizure releaseFromSeizure) {
        this.releaseFromSeizure = releaseFromSeizure;
    }

    public TheftPractice getTheft() {
        return theft;
    }

    public void setTheft(TheftPractice theft) {
        this.theft = theft;
    }

    @Override
    public String toString() {
        return "PracticeEntity{" +
                "id=" + id +
                ", parentId=" + parentId +
                ", status=" + status +
                ", practiceType=" + practiceType +
                ", practiceId=" + practiceId +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                ", createdBy='" + createdBy + '\'' +
                ", claimsId=" + claimsId +
                ", processingList=" + processingList +
                ", attachmentList=" + attachmentList +
                ", vehicle=" + vehicle +
                ", customer=" + customer +
                ", contract=" + contract +
                ", fleetManagers=" + fleetManagers +
                ", finding=" + finding +
                ", theft=" + theft +
                ", misappropriation=" + misappropriation +
                ", seizure=" + seizure +
                ", releaseFromSeizure=" + releaseFromSeizure +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PracticeEntity)) return false;
        PracticeEntity that = (PracticeEntity) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getParentId(), that.getParentId()) &&
                getStatus() == that.getStatus() &&
                getPracticeType() == that.getPracticeType() &&
                Objects.equals(getPracticeId(), that.getPracticeId()) &&
                Objects.equals(getCreatedAt(), that.getCreatedAt()) &&
                Objects.equals(getUpdatedAt(), that.getUpdatedAt()) &&
                Objects.equals(getCreatedBy(), that.getCreatedBy()) &&
                Objects.equals(getClaimsId(), that.getClaimsId()) &&
                Objects.equals(getProcessingList(), that.getProcessingList()) &&
                Objects.equals(getAttachmentList(), that.getAttachmentList()) &&
                Objects.equals(getVehicle(), that.getVehicle()) &&
                Objects.equals(getCustomer(), that.getCustomer()) &&
                Objects.equals(getContract(), that.getContract()) &&
                Objects.equals(getFleetManagers(), that.getFleetManagers()) &&
                Objects.equals(getFinding(), that.getFinding()) &&
                Objects.equals(getTheft(), that.getTheft()) &&
                Objects.equals(getMisappropriation(), that.getMisappropriation()) &&
                Objects.equals(getSeizure(), that.getSeizure()) &&
                Objects.equals(getReleaseFromSeizure(), that.getReleaseFromSeizure());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getParentId(), getStatus(), getPracticeType(), getPracticeId(), getCreatedAt(), getUpdatedAt(), getCreatedBy(), getClaimsId(), getProcessingList(), getAttachmentList(), getVehicle(), getCustomer(), getContract(), getFleetManagers(), getFinding(), getTheft(), getMisappropriation(), getSeizure(), getReleaseFromSeizure());
    }
}
