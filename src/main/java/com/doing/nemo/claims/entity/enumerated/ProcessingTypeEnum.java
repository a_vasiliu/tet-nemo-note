package com.doing.nemo.claims.entity.enumerated;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;

public enum ProcessingTypeEnum implements Serializable {
    BOOK_DUPLICATION("book_duplication"),
    TO_RE_REGISTER("to_re_register"),
    STAMP("stamp"),
    DEMOLITION("demolition"),
    KEY_DUPLICATION("key_duplication"),
    UNIQUE_DOCUMENT("unique_document"),
    DUPLICATE_SINGLE_DOCUMENT("duplicate_single_document");

    private static Logger LOGGER = LoggerFactory.getLogger(ProcessingTypeEnum.class);
    private String processingTypeEnum;

    private ProcessingTypeEnum(String processingTypeEnum) {
        this.processingTypeEnum = processingTypeEnum;
    }

    @JsonCreator
    public static ProcessingTypeEnum create(String processingTypeEnum) {

        processingTypeEnum = processingTypeEnum.replace(" - ", "_");
        processingTypeEnum = processingTypeEnum.replace('-', '_');
        processingTypeEnum = processingTypeEnum.replace(' ', '_');

        if (processingTypeEnum != null) {
            for (ProcessingTypeEnum val : ProcessingTypeEnum.values()) {
                if (processingTypeEnum.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        LOGGER.debug("Bad parameters exception. Enum class " + ProcessingTypeEnum.class.getSimpleName() + " doesn't accept this value: " + processingTypeEnum);
        throw new BadParametersException("Bad parameters exception. Enum class " + ProcessingTypeEnum.class.getSimpleName() + " doesn't accept this value: " + processingTypeEnum);
    }

    public String getValue() {
        return this.processingTypeEnum.toUpperCase();
    }

    @JsonValue
    public String toValue() {
        for (ProcessingTypeEnum val : ProcessingTypeEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }
}
