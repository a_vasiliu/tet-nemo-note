package com.doing.nemo.claims.entity.jsonb.repair;

import com.doing.nemo.claims.entity.CenterEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Canalization implements Serializable {

    private static final long serialVersionUID = 3794286085596446737L;



    @JsonProperty("center")
    private CenterEntity centerEntity;

    @JsonProperty("date")
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    private Date date;


    public CenterEntity getCenterEntity() {
        return centerEntity;
    }

    public void setCenterEntity(CenterEntity centerEntity) {
        this.centerEntity = centerEntity;
    }

    public Date getDate() {
        if(date == null){
            return  null;
        }
        return (Date)date.clone();
    }

    public void setDate(Date date) {
        if(date != null)
        {
            this.date = (Date)date.clone();
        } else {
            this.date = null;
        }
    }

    @Override
    public String toString() {
        return "Canalization{" +
                "centerEntity=" + centerEntity +
                ", date=" + date +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Canalization that = (Canalization) o;

        return new EqualsBuilder().append(centerEntity, that.centerEntity).append(date, that.date).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(centerEntity).append(date).toHashCode();
    }
}
