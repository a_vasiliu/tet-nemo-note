package com.doing.nemo.claims.entity.jsonb;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Test implements Serializable {

    private static final long serialVersionUID = 3190943670273798502L;

    @JsonProperty("test")
    private String test;

    public Test() {
    }

    public Test(String test) {
        this.test = test;
    }

    public String getTest() {
        return test;
    }

    public void setTest(String test) {
        this.test = test;
    }

    @Override
    public String toString() {
        return "TestEsb{" +
                "test='" + test + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Test test1 = (Test) o;
        return this.test.equalsIgnoreCase(test1.test);
    }

    @Override
    public int hashCode() {
        return Objects.hash(test);
    }
}
