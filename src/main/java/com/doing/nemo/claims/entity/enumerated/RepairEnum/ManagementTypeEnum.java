package com.doing.nemo.claims.entity.enumerated.RepairEnum;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;

public enum ManagementTypeEnum implements Serializable {
    PREVENTIVE("preventive"),
    REPAIR("repair");

    private static Logger LOGGER = LoggerFactory.getLogger(ManagementTypeEnum.class);
    private String managementType;

    private ManagementTypeEnum(String managementType) {
        this.managementType = managementType;
    }

    @JsonCreator
    public static ManagementTypeEnum create(String managementType) {

        managementType = managementType.replace(" - ", "_");
        managementType = managementType.replace('-', '_');
        managementType = managementType.replace(' ', '_');

        if (managementType != null) {
            for (ManagementTypeEnum val : ManagementTypeEnum.values()) {
                if (managementType.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        LOGGER.debug("Bad parameters exception. Enum class " + ManagementTypeEnum.class.getSimpleName() + " doesn't accept this value: " + managementType);
        throw new BadParametersException("Bad parameters exception. Enum class " + ManagementTypeEnum.class.getSimpleName() + " doesn't accept this value: " + managementType);
    }

    public String getValue() {
        return this.managementType.toUpperCase();
    }

    @JsonValue
    public String toValue() {
        for (ManagementTypeEnum val : ManagementTypeEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }
}
