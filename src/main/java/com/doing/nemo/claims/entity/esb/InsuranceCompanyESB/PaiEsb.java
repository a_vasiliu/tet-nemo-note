package com.doing.nemo.claims.entity.esb.InsuranceCompanyESB;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PaiEsb implements Serializable {
    @JsonProperty("service_id")
    private String serviceId;
    @JsonProperty("service")
    private String service;
    @JsonProperty("max_coverage")
    private String maxCoverage;
    @JsonProperty("deductible_id")
    private String deductibleId;
    @JsonProperty("deductible")
    private String deductible;
    @JsonProperty("company_id")
    private String companyId;
    @JsonProperty("company")
    private String company;
    @JsonProperty("tariff_id")
    private String tariffId;
    @JsonProperty("tariff")
    private String tariff;
    @JsonProperty("tariff_code")
    private String tariffCode;
    @JsonProperty("percentage")
    private String percentage;
    @JsonProperty("start_date")
    private String startDate;
    private String endDate;
    @JsonProperty("policy_number")
    private String policyNumber;
    @JsonProperty("medical_costs")
    private String medicalCosts;

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getMaxCoverage() {
        return maxCoverage;
    }

    public void setMaxCoverage(String maxCoverage) {
        this.maxCoverage = maxCoverage;
    }

    public String getDeductibleId() {
        return deductibleId;
    }

    public void setDeductibleId(String deductibleId) {
        this.deductibleId = deductibleId;
    }

    public String getDeductible() {
        return deductible;
    }

    public void setDeductible(String deductible) {
        this.deductible = deductible;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }


    public String getTariffId() {
        return tariffId;
    }


    public void setTariffId(String tariffId) {
        this.tariffId = tariffId;
    }

    public String getTariff() {
        return tariff;
    }

    public void setTariff(String tariff) {
        this.tariff = tariff;
    }

    @JsonProperty("tariff_code")
    public String getTariffCode() {
        return tariffCode;
    }

    @JsonProperty("tariff_code")
    public void setTariffCode(String tariffCode) {
        this.tariffCode = tariffCode;
    }

    public String getPercentage() {
        return percentage;
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    @JsonProperty("end_date")
    public String getEndDate() {
        return endDate;
    }

    @JsonProperty("enddate")
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public String getMedicalCosts() {
        return medicalCosts;
    }

    public void setMedicalCosts(String medicalCosts) {
        this.medicalCosts = medicalCosts;
    }

    @Override
    public String toString() {
        return "PaiEsb{" +
                "serviceId='" + serviceId + '\'' +
                ", service='" + service + '\'' +
                ", maxCoverage='" + maxCoverage + '\'' +
                ", deductibleId='" + deductibleId + '\'' +
                ", deductible='" + deductible + '\'' +
                ", companyId='" + companyId + '\'' +
                ", company='" + company + '\'' +
                ", tariffId='" + tariffId + '\'' +
                ", tariff='" + tariff + '\'' +
                ", tariffCode='" + tariffCode + '\'' +
                ", percentage='" + percentage + '\'' +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", policyNumber='" + policyNumber + '\'' +
                ", medicalCosts='" + medicalCosts + '\'' +
                '}';
    }
}
