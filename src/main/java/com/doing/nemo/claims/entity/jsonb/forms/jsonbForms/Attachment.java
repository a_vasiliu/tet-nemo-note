package com.doing.nemo.claims.entity.jsonb.forms.jsonbForms;

import com.doing.nemo.claims.entity.settings.AttachmentTypeEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Attachment implements Serializable {

    private static final long serialVersionUID = 86750615196733629L;

    @JsonProperty("file_manager_id")
    private String fileManagerId;

    @JsonProperty("file_manager_name")
    private String fileManagerName;

    @JsonProperty("blob_name")
    private String blobName;

    @JsonProperty("blob_size")
    private Integer blobSize;

    @JsonProperty("mime_type")
    private String mimeType;

    @JsonProperty("description")
    private String description;

    @JsonProperty("created_at")
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    private Date createdAt;

    @JsonProperty("hidden")
    private Boolean hidden = false;

    @JsonProperty("validate")
    private Boolean validate = false;

    @JsonProperty("user_id")
    private String userId;

    @JsonProperty("user_name")
    private String userName;

    @JsonProperty("thumbnail")
    private String thumbnail;

    @JsonProperty("external_id")
    private String externalId;

    @JsonProperty("attachment_type")
    private AttachmentTypeEntity attachmentType;

    @JsonProperty("is_msa_downloaded")
    private Boolean isMsaDownloaded;


    public Attachment() {
    }

    public Attachment(String fileManagerId, String fileManagerName, String blobName, Integer blobSize, String mimeType, String description, Date createdAt, String userId, String userName, String thumbnail, Boolean hidden, String externalId, Boolean isMsaDownloaded) {
        this.fileManagerId = fileManagerId;
        this.fileManagerName = fileManagerName;
        this.blobName = blobName;
        this.blobSize = blobSize;
        this.mimeType = mimeType;
        this.description = description;
        if(createdAt != null)
        {
            this.createdAt = (Date)createdAt.clone();
        }
        this.userId = userId;
        this.userName = userName;
        this.thumbnail = thumbnail;
        this.hidden = hidden;
        this.externalId = externalId;
        this.isMsaDownloaded = isMsaDownloaded;
    }

    public Attachment(String fileManagerId, String fileManagerName, String blobName, Integer blobSize, String mimeType, String description, Date createdAt, String userId, String userName, String thumbnail, Boolean hidden, String externalId, Boolean isMsaDownloaded, Boolean validate) {
        this.fileManagerId = fileManagerId;
        this.fileManagerName = fileManagerName;
        this.blobName = blobName;
        this.blobSize = blobSize;
        this.mimeType = mimeType;
        this.description = description;
        if(createdAt != null)
        {
            this.createdAt = (Date)createdAt.clone();
        }
        this.userId = userId;
        this.userName = userName;
        this.thumbnail = thumbnail;
        this.hidden = hidden;
        this.externalId = externalId;
        this.isMsaDownloaded = isMsaDownloaded;
        this.validate = validate;
    }

    public Attachment(String fileManagerId, String fileManagerName, String blobName, Integer blobSize, String mimeType, String description, Date createdAt, String userId, String userName, String thumbnail, Boolean hidden, Boolean validate, String externalId, Boolean isMsaDownloaded) {
        this.fileManagerId = fileManagerId;
        this.fileManagerName = fileManagerName;
        this.blobName = blobName;
        this.blobSize = blobSize;
        this.mimeType = mimeType;
        this.description = description;
        if(createdAt != null)
        {
            this.createdAt = (Date)createdAt.clone();
        }
        this.userId = userId;
        this.userName = userName;
        this.thumbnail = thumbnail;
        this.hidden = hidden;
        this.validate = validate;
        this.externalId = externalId;
        this.isMsaDownloaded = isMsaDownloaded;
    }


    public Boolean getMsaDownloaded() {
        return isMsaDownloaded;
    }

    public void setMsaDownloaded(Boolean msaDownloaded) {
        isMsaDownloaded = msaDownloaded;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getFileManagerId() {
        return fileManagerId;
    }

    public void setFileManagerId(String fileManagerId) {
        this.fileManagerId = fileManagerId;
    }

    public String getFileManagerName() {
        return fileManagerName;
    }

    public void setFileManagerName(String fileManagerName) {
        this.fileManagerName = fileManagerName;
    }

    public String getBlobName() {
        return blobName;
    }

    public void setBlobName(String blobName) {
        this.blobName = blobName;
    }

    public Integer getBlobSize() {
        return blobSize;
    }

    public void setBlobSize(Integer blobSize) {
        this.blobSize = blobSize;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreatedAt() {
        if(createdAt == null){
            return null;
        }
        return (Date)createdAt.clone();
    }

    public void setCreatedAt(Date createdAt) {
        if(createdAt != null)
        {
            this.createdAt = (Date)createdAt.clone();
        } else {
            this.createdAt = null;
        }
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Boolean getHidden() {
        return hidden;
    }

    @JsonSetter
    public void setHidden(Boolean hidden) {
        if (hidden != null)
            this.hidden = hidden;
    }

    public Boolean getValidate() {
        return validate;
    }
    @JsonSetter
    public void setValidate(Boolean validate) {
        if(validate != null)
        this.validate = validate;
    }

    public AttachmentTypeEntity getAttachmentType() {
        return attachmentType;
    }

    public void setAttachmentType(AttachmentTypeEntity attachmentType) {
        this.attachmentType = attachmentType;
    }

    @Override
    public String toString() {
        return "Attachment{" +
                "fileManagerId='" + fileManagerId + '\'' +
                ", fileManagerName='" + fileManagerName + '\'' +
                ", blobName='" + blobName + '\'' +
                ", blobSize=" + blobSize +
                ", mimeType='" + mimeType + '\'' +
                ", description='" + description + '\'' +
                ", createdAt=" + createdAt +
                ", hidden=" + hidden +
                ", validate=" + validate +
                ", userId='" + userId + '\'' +
                ", userName='" + userName + '\'' +
                ", thumbnail='" + thumbnail + '\'' +
                ", attachmentType=" + attachmentType +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Attachment that = (Attachment) o;

        return new EqualsBuilder().append(fileManagerId, that.fileManagerId).append(fileManagerName, that.fileManagerName)
                .append(blobName, that.blobName).append(blobSize, that.blobSize).append(mimeType, that.mimeType)
                .append(description, that.description).append(createdAt, that.createdAt).append(hidden, that.hidden)
                .append(validate, that.validate).append(userId, that.userId).append(userName, that.userName)
                .append(thumbnail, that.thumbnail).append(externalId, that.externalId).append(attachmentType, that.attachmentType)
                .append(isMsaDownloaded, that.isMsaDownloaded).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(fileManagerId).append(fileManagerName)
                .append(blobName).append(blobSize).append(mimeType).append(description).append(createdAt).append(hidden)
                .append(validate).append(userId).append(userName).append(thumbnail).append(externalId).append(attachmentType)
                .append(isMsaDownloaded).toHashCode();
    }
}
