package com.doing.nemo.claims.entity.enumerated.DamagedEnum.WoundedEnum;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;

public enum WoundedTypeEnum implements Serializable {
    DRIVER("driver"),
    PASSENGER("passenger"),
    OTHER("other");

    private static Logger LOGGER = LoggerFactory.getLogger(WoundedTypeEnum.class);
    private String typeEntity;

    private WoundedTypeEnum(String typeEntity) {
        this.typeEntity = typeEntity;
    }

    @JsonCreator
    public static WoundedTypeEnum create(String typeEntity) {

        typeEntity = typeEntity.replace(" - ", "_");
        ;
        typeEntity = typeEntity.replace('-', '_');
        typeEntity = typeEntity.replace(' ', '_');

        if (typeEntity != null) {
            for (WoundedTypeEnum val : WoundedTypeEnum.values()) {
                if (typeEntity.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        LOGGER.debug("Bad parameters exception. Enum class " + WoundedTypeEnum.class.getSimpleName() + " doesn't accept this value: " + typeEntity);
        throw new BadParametersException("Bad parameters exception. Enum class " + WoundedTypeEnum.class.getSimpleName() + " doesn't accept this value: " + typeEntity);
    }

    public String getValue() {
        return this.typeEntity.toUpperCase();
    }

    @JsonValue
    public String toValue() {
        for (WoundedTypeEnum val : WoundedTypeEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }
}
