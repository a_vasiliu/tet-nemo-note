package com.doing.nemo.claims.entity.settings;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "dm_systems")
public class DmSystemsEntity implements Serializable {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )

    @Type(type = "org.hibernate.type.PostgresUUIDType")
    private UUID id;

    @Column(name = "system_name")
    private String systemName;

    public DmSystemsEntity() {
    }

    public DmSystemsEntity(String systemName) {
        this.systemName = systemName;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getSystemName() {
        return systemName;
    }

    public void setSystemName(String systemName) {
        this.systemName = systemName;
    }

    @Override
    public String toString() {
        return "DmSystemsEntity{" +
                "id=" + id +
                ", systemName='" + systemName + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DmSystemsEntity that = (DmSystemsEntity) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getSystemName(), that.getSystemName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getSystemName());
    }
}
