package com.doing.nemo.claims.entity.esb.InsuranceCompanyESB;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;


@JsonIgnoreProperties(ignoreUnknown = true)
public class TheftEsb implements Serializable {
    @JsonProperty("service_id")
    private Long serviceId;
    @JsonProperty("service")
    private String service;
    @JsonProperty("deductible_id")
    private Long deductibleId;
    @JsonProperty("deductible")
    private String deductible;

    public Long getServiceId() {
        return serviceId;
    }

    public void setServiceId(Long serviceId) {
        this.serviceId = serviceId;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public Long getDeductibleId() {
        return deductibleId;
    }

    public void setDeductibleId(Long deductibleId) {
        this.deductibleId = deductibleId;
    }

    public String getDeductible() {
        return deductible;
    }

    public void setDeductible(String deductible) {
        this.deductible = deductible;
    }
}
