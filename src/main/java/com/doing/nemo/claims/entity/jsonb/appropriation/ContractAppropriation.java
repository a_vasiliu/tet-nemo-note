package com.doing.nemo.claims.entity.jsonb.appropriation;

import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.Contract;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.Customer;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.FleetManager;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.InsuranceCompany;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbAntiTheftService.Registry;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbDriver.Driver;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbVehicle.Vehicle;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ContractAppropriation implements Serializable {

    private static final long serialVersionUID = 3799824157128482608L;

    @JsonProperty("contract")
    private Contract contract;

    @JsonProperty("customer")
    private Customer customer;

    @JsonProperty("insurance_company")
    private InsuranceCompany insuranceCompany;

    @JsonProperty("driver")
    private Driver driver;

    @JsonProperty("vehicle")
    private Vehicle vehicle;

    @JsonProperty("fleet_managers")
    private List<FleetManager> fleetManagerList;

    @JsonProperty("registry_list")
    private List<Registry> registryList;

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    @JsonProperty("created_at")
    private Date createdAt;

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    @JsonProperty("update_at")
    private Date updateAt;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Contract getContract() {
        return contract;
    }

    public void setContract(Contract contract) {
        this.contract = contract;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public InsuranceCompany getInsuranceCompany() {
        return insuranceCompany;
    }

    public void setInsuranceCompany(InsuranceCompany insuranceCompany) {
        this.insuranceCompany = insuranceCompany;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public List<FleetManager> getFleetManagerList() {
        if(fleetManagerList == null){
            return null;
        }
        return new ArrayList<>(fleetManagerList);
    }

    public void setFleetManagerList(List<FleetManager> fleetManagerList) {
        if(fleetManagerList != null)
        {
            this.fleetManagerList =new ArrayList<>(fleetManagerList);
        } else {
            this.fleetManagerList = null;
        }
    }

    public List<Registry> getRegistryList() {
        if(registryList == null){
            return null;
        }
        return new ArrayList<>(registryList);
    }

    public void setRegistryList(List<Registry> registryList) {
        if(registryList != null)
        {
            this.registryList = new ArrayList<>(registryList);
        } else {
            this.registryList = null;
        }
    }

    public Date getCreatedAt() {
        if(createdAt == null){
            return null;
        }
        return (Date)createdAt.clone();
    }

    public void setCreatedAt(Date createdAt) {
        if(createdAt != null)
        {
            this.createdAt = (Date)createdAt.clone();
        } else {
            this.createdAt = null;
        }
    }

    public Date getUpdateAt() {
        if(updateAt == null){
            return null;
        }
        return (Date)updateAt.clone();
    }

    public void setUpdateAt(Date updateAt) {
        if(updateAt != null)
        {
            this.updateAt = (Date)updateAt.clone();
        } else {
            this.updateAt = null;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        ContractAppropriation that = (ContractAppropriation) o;

        return new EqualsBuilder().append(contract, that.contract)
                .append(customer, that.customer).append(insuranceCompany, that.insuranceCompany)
                .append(driver, that.driver).append(vehicle, that.vehicle)
                .append(fleetManagerList, that.fleetManagerList).append(registryList, that.registryList)
                .append(createdAt, that.createdAt).append(updateAt, that.updateAt).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(contract)
                .append(customer).append(insuranceCompany).append(driver).append(vehicle)
                .append(fleetManagerList).append(registryList).append(createdAt).append(updateAt).toHashCode();
    }
}
