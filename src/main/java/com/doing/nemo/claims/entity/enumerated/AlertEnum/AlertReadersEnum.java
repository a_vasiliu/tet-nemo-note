package com.doing.nemo.claims.entity.enumerated.AlertEnum;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;


public enum AlertReadersEnum implements Serializable {
    NEMO("nemo"),
    ALD("ald");

    private static Logger LOGGER = LoggerFactory.getLogger(AlertReadersEnum.class);
    private String readersEntity;

    private AlertReadersEnum(String readersEntity) {
        this.readersEntity = readersEntity;
    }

    @JsonCreator
    public static AlertReadersEnum create(String readersEntity) {

        if (readersEntity != null) {
            for (AlertReadersEnum val : AlertReadersEnum.values()) {
                if (readersEntity.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }

        LOGGER.debug("Bad parameters exception. Enum class " + AlertReadersEnum.class.getSimpleName() + " doesn't accept this value: " + readersEntity);
        throw new BadParametersException("Bad parameters exception. Enum class " + AlertReadersEnum.class.getSimpleName() + " doesn't accept this value: " + readersEntity);
    }

    public String getValue() {
        return this.readersEntity.toUpperCase();
    }

    @JsonValue
    public String toValue() {
        for (AlertReadersEnum val : AlertReadersEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }
}
