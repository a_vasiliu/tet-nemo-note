package com.doing.nemo.claims.entity.esb.VehicleESB;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class VehicleEsb implements Serializable {
    private static Logger LOGGER = LoggerFactory.getLogger(VehicleEsb.class);

    private static final long serialVersionUID = 7504058282907704988L;

    @JsonProperty("fleet_vehicle_id")
    private Long fleetVehicleId;

    @JsonProperty("make")
    private String make;

    @JsonProperty("model")
    private String model;

    @JsonProperty("model_year")
    private String modelYear;

    @JsonProperty("kw")
    private String kw;

    @JsonProperty("cc_3")
    private String cc3;

    @JsonProperty("hp")
    private Integer hp;

    @JsonProperty("din_hp")
    private Integer dinHp;

    @JsonProperty("seats")
    private Integer seats;

    @JsonProperty("net_weight")
    private Double netWeight;

    @JsonProperty("max_weight")
    private Double maxWeight;

    @JsonProperty("doors")
    private String doors;

    @JsonProperty("body_style")
    private String bodyStyle;

    @JsonProperty("nature")
    private String nature;

    @JsonProperty("fuel_type")
    private String fuelType;

    @JsonProperty("co_2emission")
    private String co2emission;

    @JsonProperty("consumption")
    private String consumption;

    @JsonProperty("license_plate")
    private String licensePlate;

    @JsonProperty("chassis_number")
    private String chassisNumber;

    @JsonProperty("last_known_mileage")
    private String lastKnownMileage;

    @JsonProperty("registration_date")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date registrationDate;

    @JsonProperty("external_color")
    private String externalColor;

    @JsonProperty("internal_color")
    private String internalColor;

    @JsonProperty("net_book_value")
    private Double netBookValue;

    @JsonProperty("related_contracts")
    private List<RelatedContractEsb> relatedContractsList;

    @JsonIgnore
    public Long getIdActiveContract(String accidentDate) {
        if (relatedContractsList == null) {
            return null;
        }
        String startCancelled = "1800-01-01";
        String endCancelled = "3000-12-31";

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        Date dateStartCancelled = null;
        Date dateEndCancelled = null;


        try {
            date = simpleDateFormat.parse(accidentDate);
            dateStartCancelled = simpleDateFormat.parse(startCancelled);
            dateEndCancelled = simpleDateFormat.parse(endCancelled);
        } catch (ParseException e) {
            LOGGER.debug(e.getMessage());
        }
        Long idContract = null;
        for (RelatedContractEsb relatedContract : relatedContractsList) {
            //if (relatedContract.getActive()) {
             if (date != null && date.compareTo(relatedContract.getStartDate()) >= 0 && date.compareTo(relatedContract.getEndDate()) <= 0) {
                    if (relatedContract.getStartDate().compareTo(dateStartCancelled)==0&&relatedContract.getEndDate().compareTo(dateEndCancelled)==0){
                        LOGGER.debug("Contract cancelled");
                    }else{
                       idContract = relatedContract.getId();
                       break;
                    }
                }
            //}
        }

     /*   if(idContract == null){
            Date maxDate = null;
            for (RelatedContractEsb relatedContract : relatedContractsList) {
                if (relatedContract.getActive()) {
                    if (maxDate == null) {
                        maxDate = relatedContract.getEndDate();
                        idContract = relatedContract.getId();
                    } else if (maxDate.compareTo(relatedContract.getEndDate()) < 0) {
                        maxDate = relatedContract.getEndDate();
                        idContract = relatedContract.getId();
                    }
                }
            }
        }*/

        return idContract;
    }

    public Double getNetBookValue() {
        return netBookValue;
    }

    public void setNetBookValue(Double netBookValue) {
        this.netBookValue = netBookValue;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getFleetVehicleId() {
        return fleetVehicleId;
    }

    public void setFleetVehicleId(Long fleetVehicleId) {
        this.fleetVehicleId = fleetVehicleId;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getModelYear() {
        return modelYear;
    }

    public void setModelYear(String modelYear) {
        this.modelYear = modelYear;
    }

    public String getKw() {
        return kw;
    }

    public void setKw(String kw) {
        this.kw = kw;
    }

    public String getCc3() {
        return cc3;
    }

    public void setCc3(String cc3) {
        this.cc3 = cc3;
    }

    public Integer getHp() {
        return hp;
    }

    public void setHp(Integer hp) {
        this.hp = hp;
    }

    public Integer getDinHp() {
        return dinHp;
    }

    public void setDinHp(Integer dinHp) {
        this.dinHp = dinHp;
    }

    public Integer getSeats() {
        return seats;
    }

    public void setSeats(Integer seats) {
        this.seats = seats;
    }

    public Double getNetWeight() {
        return netWeight;
    }

    public void setNetWeight(Double netWeight) {
        this.netWeight = netWeight;
    }

    public Double getMaxWeight() {
        return maxWeight;
    }

    public void setMaxWeight(Double maxWeight) {
        this.maxWeight = maxWeight;
    }

    public String getDoors() {
        return doors;
    }

    public void setDoors(String doors) {
        this.doors = doors;
    }

    public String getBodyStyle() {
        return bodyStyle;
    }

    public void setBodyStyle(String bodyStyle) {
        this.bodyStyle = bodyStyle;
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    public String getCo2emission() {
        return co2emission;
    }

    public void setCo2emission(String co2emission) {
        this.co2emission = co2emission;
    }

    public String getConsumption() {
        return consumption;
    }

    public void setConsumption(String consumption) {
        this.consumption = consumption;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public String getChassisNumber() {
        return chassisNumber;
    }

    public void setChassisNumber(String chassisNumber) {
        this.chassisNumber = chassisNumber;
    }

    public String getLastKnownMileage() {
        return lastKnownMileage;
    }

    public void setLastKnownMileage(String lastKnownMileage) {
        this.lastKnownMileage = lastKnownMileage;
    }

    public Date getRegistrationDate() {
        if(registrationDate == null){
            return null;
        }
        return (Date)registrationDate.clone();
    }

    public void setRegistrationDate(Date registrationDate) {
        if(registrationDate != null)
        {
            this.registrationDate =(Date)registrationDate.clone();
        } else {
            this.registrationDate = null;
        }
    }

    public String getExternalColor() {
        return externalColor;
    }

    public void setExternalColor(String externalColor) {
        this.externalColor = externalColor;
    }

    public String getInternalColor() {
        return internalColor;
    }

    public void setInternalColor(String internalColor) {
        this.internalColor = internalColor;
    }

    public List<RelatedContractEsb> getRelatedContractsList() {
        if(relatedContractsList == null){
            return null;
        }
        return new ArrayList<>(relatedContractsList);
    }

    public void setRelatedContractsList(List<RelatedContractEsb> relatedContractsList) {
        if(relatedContractsList != null)
        {
            this.relatedContractsList =new ArrayList<>(relatedContractsList);
        } else {
            this.relatedContractsList = null;
        }
    }

    @Override
    public String toString() {
        return "VehicleEsb{" +
                "fleetVehicleId='" + fleetVehicleId + '\'' +
                ", make='" + make + '\'' +
                ", model='" + model + '\'' +
                ", modelYear='" + modelYear + '\'' +
                ", kw='" + kw + '\'' +
                ", cc3='" + cc3 + '\'' +
                ", hp='" + hp + '\'' +
                ", dinHp='" + dinHp + '\'' +
                ", seats='" + seats + '\'' +
                ", netWeight='" + netWeight + '\'' +
                ", maxWeight='" + maxWeight + '\'' +
                ", doors='" + doors + '\'' +
                ", bodyStyle='" + bodyStyle + '\'' +
                ", nature='" + nature + '\'' +
                ", fuelType='" + fuelType + '\'' +
                ", co2emission='" + co2emission + '\'' +
                ", consumption='" + consumption + '\'' +
                ", licensePlate='" + licensePlate + '\'' +
                ", chassisNumber='" + chassisNumber + '\'' +
                ", lastKnownMileage='" + lastKnownMileage + '\'' +
                ", registrationDate='" + registrationDate + '\'' +
                ", externalColor='" + externalColor + '\'' +
                ", internalColor='" + internalColor + '\'' +
                ", relatedContractsList=" + relatedContractsList +
                '}';
    }
}
