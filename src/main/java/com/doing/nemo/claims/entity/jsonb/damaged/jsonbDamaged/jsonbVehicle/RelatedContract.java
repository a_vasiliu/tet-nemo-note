package com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbVehicle;

import com.doing.nemo.claims.entity.jsonb.Link;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RelatedContract implements Serializable {

    private static final long serialVersionUID = 2949976603039377457L;

    @JsonProperty("id")
    private Integer id;

    @JsonProperty("start_date")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date startDate;

    @JsonProperty("end_date")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date endDate;

    @JsonProperty("is_active")
    private Boolean isActive;

    @JsonProperty("links")
    private List<Link> linksList;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getStartDate() {
        if(startDate == null){
            return null;
        }
        return (Date)startDate.clone();
    }

    public void setStartDate(Date startDate) {
        if(startDate != null)
        {
            this.startDate = (Date)startDate.clone();
        } else {
            this.startDate = null;
        }
    }

    public Date getEndDate() {
        if(endDate == null){
            return null;
        }
        return (Date)endDate.clone();
    }

    public void setEndDate(Date endDate) {
        if(endDate != null)
        {
            this.endDate = (Date)endDate.clone();
        } else {
            this.endDate = null;
        }
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public List<Link> getLinksList() {
        if(linksList == null){
            return null;
        }
        return new ArrayList<>(linksList);
    }

    public void setLinksList(List<Link> linksList) {
        if(linksList != null){
            this.linksList = new ArrayList<>(linksList);
        } else {
            this.linksList = null;
        }

    }

    @Override
    public String toString() {
        return "RelatedContract{" +
                "id='" + id + '\'' +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", isActive=" + isActive +
                ", linksList=" + linksList +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        RelatedContract that = (RelatedContract) o;

        return new EqualsBuilder().append(id, that.id).append(startDate, that.startDate)
                .append(endDate, that.endDate).append(isActive, that.isActive).append(linksList, that.linksList).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(id).append(startDate)
                .append(endDate).append(isActive).append(linksList).toHashCode();
    }
}
