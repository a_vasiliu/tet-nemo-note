package com.doing.nemo.claims.entity.jsonb.complaint.jsonbComplaint;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FromCompany implements Serializable {

    private static final long serialVersionUID = -8100121665894123648L;

    @JsonProperty("company")
    private String company;

    @JsonProperty("number_sx")
    private String numberSx;

    @JsonProperty("type_sx")
    private String typeSx;

    @JsonProperty("inspectorate")
    private String inspectorate;

    @JsonProperty("expert")
    private String expert;

    @JsonProperty("note")
    private String note;

    @JsonProperty("dwl_man")
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    private Date dwlMan;

    @JsonProperty("last_update")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date lastUpdate;

    @JsonProperty("status")
    private String status;

    @JsonProperty("global_reserve")
    private Double globalReserve;

    @JsonProperty("total_paid")
    private Double totalPaid;

    public String getStatus() {
        return status;
    }

    public FromCompany setStatus(String status) {
        this.status = status;
        return this;
    }

    public Double getGlobalReserve() {
        return globalReserve;
    }

    public FromCompany setGlobalReserve(Double globalReserve) {
        this.globalReserve = globalReserve;
        return this;
    }

    public Double getTotalPaid() {
        return totalPaid;
    }

    public FromCompany setTotalPaid(Double totalPaid) {
        this.totalPaid = totalPaid;
        return this;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getCompany() {
        return company;
    }

    public FromCompany setCompany(String company) {
        this.company = company;
        return this;
    }

    public String getNumberSx() {
        return numberSx;
    }

    public FromCompany setNumberSx(String numberSx) {
        this.numberSx = numberSx;
        return this;
    }

    public String getTypeSx() {
        return typeSx;
    }

    public FromCompany setTypeSx(String typeSx) {
        this.typeSx = typeSx;
        return this;
    }

    public String getInspectorate() {
        return inspectorate;
    }

    public FromCompany setInspectorate(String inspectorate) {
        this.inspectorate = inspectorate;
        return this;
    }

    public String getExpert() {
        return expert;
    }

    public FromCompany setExpert(String expert) {
        this.expert = expert;
        return this;
    }

    public String getNote() {
        return note;
    }

    public FromCompany setNote(String note) {
        this.note = note;
        return this;
    }

    public Date getDwlMan() {
        if(dwlMan == null){
            return null;
        }
        return (Date)dwlMan.clone();
    }

    public FromCompany setDwlMan(Date dwlMan) {
        if(dwlMan != null) {
            this.dwlMan = (Date)dwlMan.clone();
        } else {
            this.dwlMan = null;
        }
        return this;
    }

    public Date getLastUpdate() {
        if(lastUpdate == null){
            return null;
        }
        return (Date)lastUpdate.clone();
    }

    public FromCompany setLastUpdate(Date lastUpdate) {
        if(lastUpdate != null){
            this.lastUpdate = (Date)lastUpdate.clone();
        } else {
            this.lastUpdate = null;
        }
        return this;
    }

    @Override
    public String toString() {
        return "FromCompany{" +
                "company='" + company + '\'' +
                ", numberSx='" + numberSx + '\'' +
                ", typeSx='" + typeSx + '\'' +
                ", inspectorate='" + inspectorate + '\'' +
                ", expert='" + expert + '\'' +
                ", note='" + note + '\'' +
                ", dwlMan=" + dwlMan +
                ", lastUpdate=" + lastUpdate +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        FromCompany that = (FromCompany) o;

        return new EqualsBuilder().append(company, that.company).append(numberSx, that.numberSx)
                .append(typeSx, that.typeSx).append(inspectorate, that.inspectorate).append(expert, that.expert)
                .append(note, that.note).append(dwlMan, that.dwlMan).append(lastUpdate, that.lastUpdate)
                .append(status, that.status).append(globalReserve, that.globalReserve).append(totalPaid, that.totalPaid).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(company)
                .append(numberSx).append(typeSx).append(inspectorate).append(expert)
                .append(note).append(dwlMan).append(lastUpdate).append(status).append(globalReserve).append(totalPaid).toHashCode();
    }
}
