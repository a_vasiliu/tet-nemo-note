package com.doing.nemo.claims.entity.claims;

import com.doing.nemo.claims.entity.ClaimsNewEntity;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

@JsonIgnoreProperties({"hibernateLazyInitializer", "hibernate_lazy_initializer", "handler"})
@Entity
@Table(name = "claims_damaged_insurance_info")
public class ClaimsDamagedInsuranceInfoEntity implements Serializable {

    @Id
    @Column(name = "id", updatable = false, nullable = false)
    private String id;
    @Column(name="insurance_info_id")
    private String insuranceInfoId;
    @JsonProperty("is_card")
    private Boolean isCard;

    //TPL
    @Column(name="tpl_service_id")
    private String tplServiceId;
    @Column(name="tpl_service")
    private String tplService;
    @Column(name="tpl_max_coverage")
    private String tplMaxCoverage;
    @Column(name="tpl_deductible")
    private String tplDeductible;
    @Column(name="tpl_company_id")
    private String tplCompanyId;
    @Column(name="tpl_company")
    private String tplCompany;
    @Column(name="tpl_tariff_id")
    private String tplTariffId;
    @Column(name="tpl_tariff")
    private String tplTariff;
    @Column(name="tpl_tariff_code")
    private String tplTariffCode;
    @Column(name="tpl_start_date")
    private String tplStartDate;
    @Column(name="tpl_end_date")
    private String tplEndDate;
    @Column(name="tpl_policy_number")
    private String tplPolicyNumber;
    @Column(name="tpl_company_description")
    private String tplCompanyDescription;
    @Column(name="tpl_insurance_company_id")
    private UUID tplInsuranceCompanyId;
    @Column(name="tpl_insurance_policy_id")
    private UUID tplInsurancePolicyId;

    //THEFT
    @Column(name="theft_service_id")
    private Long theftServiceId;
    @Column(name="theft_service")
    private String theftService;
    @Column(name="theft_deductible_id")
    private Long theftDeductibleId;
    @Column(name="theft_deductible")
    private String theftDeductible;

    //PAI
    @Column(name="pai_service_id")
    private String paiServiceId;
    @Column(name="pai_service")
    private String paiService;
    @Column(name="pai_max_coverage")
    private String paiMaxCoverage;
    @Column(name="pai_deductible_id")
    private String paiDeductibleId;
    @Column(name="pai_deductible")
    private String paiDeductible;
    @Column(name="pai_company_id")
    private String paiCompanyId;
    @Column(name="pai_company")
    private String paiCompany;
    @Column(name="pai_tariff_id")
    private String paiTariffId;
    @Column(name="pai_tariff")
    private String paiTariff;
    @Column(name="pai_tariff_code")
    private String paiTariffCode;
    @Column(name="pai_percentage")
    private String paiPercentage;
    @Column(name="pai_start_date")
    private String paiStartDate;
    @Column(name="pai_end_date")
    private String paiEndDate;
    @Column(name="pai_policy_number")
    private String paiPolicyNumber;
    @Column(name="pai_medical_costs")
    private String paiMedicalCosts;
    @Column(name="pai_insurance_company_id")
    private UUID paiInsuranceCompanyId;
    @Column(name="pai_insurance_policy_id")
    private UUID paiInsurancePolicyId;

    //LEGAL COST
    @Column(name="legal_cost_service_id")
    private String legalCostServiceId;
    @Column(name="legal_cost_service")
    private String legalCostService;
    @Column(name="legal_cost_company_id")
    private String legalCostCompanyId;
    @Column(name="legal_cost_company")
    private String legalCostCompany;
    @Column(name="legal_cost_tariff")
    private String legalCostTariff;
    @Column(name="legal_cost_tariff_id")
    private String legalCostTariffId;
    @Column(name="legal_cost_tariff_code")
    private String legalCostTariffCode;
    @Column(name="legal_cost_policy_number")
    private String legalCostPolicyNumber;
    @Column(name="legal_cost_start_date")
    private String legalCostStartDate;
    @Column(name="legal_cost_end_date")
    private String legalCostEndDate;
    @Column(name="legal_cost_insurance_company_id")
    private UUID legalCostInsuranceCompanyId;
    @Column(name="legal_cost_insurance_policy_id")
    private UUID legalCostInsurancePolicyId;

    //KASKO
    @Column(name="kasko_deductible_id")
    private Integer kaskoDeductibleId;
    @Column(name="kasko_deductible_value")
    private String kaskoDeductibleValue;

    @OneToOne
    @JoinColumn(name = "id")
    @MapsId("id")
    @PrimaryKeyJoinColumn
    private ClaimsNewEntity claim;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInsuranceInfoId() {
        return insuranceInfoId;
    }

    public void setInsuranceInfoId(String insuranceInfoId) {
        this.insuranceInfoId = insuranceInfoId;
    }

    public Boolean getCard() {
        return isCard;
    }

    public void setCard(Boolean card) {
        isCard = card;
    }

    public String getTplServiceId() {
        return tplServiceId;
    }

    public void setTplServiceId(String tplServiceId) {
        this.tplServiceId = tplServiceId;
    }

    public String getTplService() {
        return tplService;
    }

    public void setTplService(String tplService) {
        this.tplService = tplService;
    }

    public String getTplMaxCoverage() {
        return tplMaxCoverage;
    }

    public void setTplMaxCoverage(String tplMaxCoverage) {
        this.tplMaxCoverage = tplMaxCoverage;
    }

    public String getTplDeductible() {
        return tplDeductible;
    }

    public void setTplDeductible(String tplDeductible) {
        this.tplDeductible = tplDeductible;
    }

    public String getTplCompanyId() {
        return tplCompanyId;
    }

    public void setTplCompanyId(String tplCompanyId) {
        this.tplCompanyId = tplCompanyId;
    }

    public String getTplCompany() {
        return tplCompany;
    }

    public void setTplCompany(String tplCompany) {
        this.tplCompany = tplCompany;
    }

    public String getTplTariffId() {
        return tplTariffId;
    }

    public void setTplTariffId(String tplTariffId) {
        this.tplTariffId = tplTariffId;
    }

    public String getTplTariff() {
        return tplTariff;
    }

    public void setTplTariff(String tplTariff) {
        this.tplTariff = tplTariff;
    }

    public String getTplTariffCode() {
        return tplTariffCode;
    }

    public void setTplTariffCode(String tplTariffCode) {
        this.tplTariffCode = tplTariffCode;
    }

    public String getTplStartDate() {
        return tplStartDate;
    }

    public void setTplStartDate(String tplStartDate) {
        this.tplStartDate = tplStartDate;
    }

    public String getTplEndDate() {
        return tplEndDate;
    }

    public void setTplEndDate(String tplEndDate) {
        this.tplEndDate = tplEndDate;
    }

    public String getTplPolicyNumber() {
        return tplPolicyNumber;
    }

    public void setTplPolicyNumber(String tplPolicyNumber) {
        this.tplPolicyNumber = tplPolicyNumber;
    }

    public String getTplCompanyDescription() {
        return tplCompanyDescription;
    }

    public void setTplCompanyDescription(String tplCompanyDescription) {
        this.tplCompanyDescription = tplCompanyDescription;
    }

    public UUID getTplInsuranceCompanyId() {
        return tplInsuranceCompanyId;
    }

    public void setTplInsuranceCompanyId(UUID tplInsuranceCompanyId) {
        this.tplInsuranceCompanyId = tplInsuranceCompanyId;
    }

    public UUID getTplInsurancePolicyId() {
        return tplInsurancePolicyId;
    }

    public void setTplInsurancePolicyId(UUID tplInsurancePolicyId) {
        this.tplInsurancePolicyId = tplInsurancePolicyId;
    }

    public Long getTheftServiceId() {
        return theftServiceId;
    }

    public void setTheftServiceId(Long theftServiceId) {
        this.theftServiceId = theftServiceId;
    }

    public String getTheftService() {
        return theftService;
    }

    public void setTheftService(String theftService) {
        this.theftService = theftService;
    }

    public Long getTheftDeductibleId() {
        return theftDeductibleId;
    }

    public void setTheftDeductibleId(Long theftDeductibleId) {
        this.theftDeductibleId = theftDeductibleId;
    }

    public String getTheftDeductible() {
        return theftDeductible;
    }

    public void setTheftDeductible(String theftDeductible) {
        this.theftDeductible = theftDeductible;
    }

    public String getPaiServiceId() {
        return paiServiceId;
    }

    public void setPaiServiceId(String paiServiceId) {
        this.paiServiceId = paiServiceId;
    }

    public String getPaiService() {
        return paiService;
    }

    public void setPaiService(String paiService) {
        this.paiService = paiService;
    }

    public String getPaiMaxCoverage() {
        return paiMaxCoverage;
    }

    public void setPaiMaxCoverage(String paiMaxCoverage) {
        this.paiMaxCoverage = paiMaxCoverage;
    }

    public String getPaiDeductibleId() {
        return paiDeductibleId;
    }

    public void setPaiDeductibleId(String paiDeductibleId) {
        this.paiDeductibleId = paiDeductibleId;
    }

    public String getPaiDeductible() {
        return paiDeductible;
    }

    public void setPaiDeductible(String paiDeductible) {
        this.paiDeductible = paiDeductible;
    }

    public String getPaiCompanyId() {
        return paiCompanyId;
    }

    public void setPaiCompanyId(String paiCompanyId) {
        this.paiCompanyId = paiCompanyId;
    }

    public String getPaiCompany() {
        return paiCompany;
    }

    public void setPaiCompany(String paiCompany) {
        this.paiCompany = paiCompany;
    }

    public String getPaiTariffId() {
        return paiTariffId;
    }

    public void setPaiTariffId(String paiTariffId) {
        this.paiTariffId = paiTariffId;
    }

    public String getPaiTariff() {
        return paiTariff;
    }

    public void setPaiTariff(String paiTariff) {
        this.paiTariff = paiTariff;
    }

    public String getPaiTariffCode() {
        return paiTariffCode;
    }

    public void setPaiTariffCode(String paiTariffCode) {
        this.paiTariffCode = paiTariffCode;
    }

    public String getPaiPercentage() {
        return paiPercentage;
    }

    public void setPaiPercentage(String paiPercentage) {
        this.paiPercentage = paiPercentage;
    }

    public String getPaiStartDate() {
        return paiStartDate;
    }

    public void setPaiStartDate(String paiStartDate) {
        this.paiStartDate = paiStartDate;
    }

    public String getPaiEndDate() {
        return paiEndDate;
    }

    public void setPaiEndDate(String paiEndDate) {
        this.paiEndDate = paiEndDate;
    }

    public String getPaiPolicyNumber() {
        return paiPolicyNumber;
    }

    public void setPaiPolicyNumber(String paiPolicyNumber) {
        this.paiPolicyNumber = paiPolicyNumber;
    }

    public String getPaiMedicalCosts() {
        return paiMedicalCosts;
    }

    public void setPaiMedicalCosts(String paiMedicalCosts) {
        this.paiMedicalCosts = paiMedicalCosts;
    }

    public UUID getPaiInsuranceCompanyId() {
        return paiInsuranceCompanyId;
    }

    public void setPaiInsuranceCompanyId(UUID paiInsuranceCompanyId) {
        this.paiInsuranceCompanyId = paiInsuranceCompanyId;
    }

    public UUID getPaiInsurancePolicyId() {
        return paiInsurancePolicyId;
    }

    public void setPaiInsurancePolicyId(UUID paiInsurancePolicyId) {
        this.paiInsurancePolicyId = paiInsurancePolicyId;
    }

    public String getLegalCostServiceId() {
        return legalCostServiceId;
    }

    public void setLegalCostServiceId(String legalCostServiceId) {
        this.legalCostServiceId = legalCostServiceId;
    }

    public String getLegalCostService() {
        return legalCostService;
    }

    public void setLegalCostService(String legalCostService) {
        this.legalCostService = legalCostService;
    }

    public String getLegalCostCompanyId() {
        return legalCostCompanyId;
    }

    public void setLegalCostCompanyId(String legalCostCompanyId) {
        this.legalCostCompanyId = legalCostCompanyId;
    }

    public String getLegalCostCompany() {
        return legalCostCompany;
    }

    public void setLegalCostCompany(String legalCostCompany) {
        this.legalCostCompany = legalCostCompany;
    }

    public String getLegalCostTariff() {
        return legalCostTariff;
    }

    public void setLegalCostTariff(String legalCostTariff) {
        this.legalCostTariff = legalCostTariff;
    }

    public String getLegalCostTariffId() {
        return legalCostTariffId;
    }

    public void setLegalCostTariffId(String legalCostTariffId) {
        this.legalCostTariffId = legalCostTariffId;
    }

    public String getLegalCostTariffCode() {
        return legalCostTariffCode;
    }

    public void setLegalCostTariffCode(String legalCostTariffCode) {
        this.legalCostTariffCode = legalCostTariffCode;
    }

    public String getLegalCostPolicyNumber() {
        return legalCostPolicyNumber;
    }

    public void setLegalCostPolicyNumber(String legalCostPolicyNumber) {
        this.legalCostPolicyNumber = legalCostPolicyNumber;
    }

    public String getLegalCostStartDate() {
        return legalCostStartDate;
    }

    public void setLegalCostStartDate(String legalCostStartDate) {
        this.legalCostStartDate = legalCostStartDate;
    }

    public String getLegalCostEndDate() {
        return legalCostEndDate;
    }

    public void setLegalCostEndDate(String legalCostEndDate) {
        this.legalCostEndDate = legalCostEndDate;
    }

    public UUID getLegalCostInsuranceCompanyId() {
        return legalCostInsuranceCompanyId;
    }

    public void setLegalCostInsuranceCompanyId(UUID legalCostInsuranceCompanyId) {
        this.legalCostInsuranceCompanyId = legalCostInsuranceCompanyId;
    }

    public UUID getLegalCostInsurancePolicyId() {
        return legalCostInsurancePolicyId;
    }

    public void setLegalCostInsurancePolicyId(UUID legalCostInsurancePolicyId) {
        this.legalCostInsurancePolicyId = legalCostInsurancePolicyId;
    }

    public Integer getKaskoDeductibleId() {
        return kaskoDeductibleId;
    }

    public void setKaskoDeductibleId(Integer kaskoDeductibleId) {
        this.kaskoDeductibleId = kaskoDeductibleId;
    }

    public String getKaskoDeductibleValue() {
        return kaskoDeductibleValue;
    }

    public void setKaskoDeductibleValue(String kaskoDeductibleValue) {
        this.kaskoDeductibleValue = kaskoDeductibleValue;
    }

    public ClaimsNewEntity getClaim() {
        return claim;
    }

    public void setClaim(ClaimsNewEntity claim) {
        this.claim = claim;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ClaimsDamagedInsuranceInfoEntity{");
        sb.append("id='").append(id).append('\'');
        sb.append(", insuranceInfoId='").append(insuranceInfoId).append('\'');
        sb.append(", isCard=").append(isCard);
        sb.append(", tplServiceId='").append(tplServiceId).append('\'');
        sb.append(", tplService='").append(tplService).append('\'');
        sb.append(", tplMaxCoverage='").append(tplMaxCoverage).append('\'');
        sb.append(", tplDeductible='").append(tplDeductible).append('\'');
        sb.append(", tplCompanyId='").append(tplCompanyId).append('\'');
        sb.append(", tplCompany='").append(tplCompany).append('\'');
        sb.append(", tplTariffId='").append(tplTariffId).append('\'');
        sb.append(", tplTariff='").append(tplTariff).append('\'');
        sb.append(", tplTariffCode='").append(tplTariffCode).append('\'');
        sb.append(", tplStartDate='").append(tplStartDate).append('\'');
        sb.append(", tplEndDate='").append(tplEndDate).append('\'');
        sb.append(", tplPolicyNumber='").append(tplPolicyNumber).append('\'');
        sb.append(", tplCompanyDescription='").append(tplCompanyDescription).append('\'');
        sb.append(", tplInsuranceCompanyId=").append(tplInsuranceCompanyId);
        sb.append(", tplInsurancePolicyId=").append(tplInsurancePolicyId);
        sb.append(", theftServiceId=").append(theftServiceId);
        sb.append(", theftService='").append(theftService).append('\'');
        sb.append(", theftDeductibleId=").append(theftDeductibleId);
        sb.append(", theftDeductible='").append(theftDeductible).append('\'');
        sb.append(", paiServiceId='").append(paiServiceId).append('\'');
        sb.append(", paiService='").append(paiService).append('\'');
        sb.append(", paiMaxCoverage='").append(paiMaxCoverage).append('\'');
        sb.append(", paiDeductibleId='").append(paiDeductibleId).append('\'');
        sb.append(", paiDeductible='").append(paiDeductible).append('\'');
        sb.append(", paiCompanyId='").append(paiCompanyId).append('\'');
        sb.append(", paiCompany='").append(paiCompany).append('\'');
        sb.append(", paiTariffId='").append(paiTariffId).append('\'');
        sb.append(", paiTariff='").append(paiTariff).append('\'');
        sb.append(", paiTariffCode='").append(paiTariffCode).append('\'');
        sb.append(", paiPercentage='").append(paiPercentage).append('\'');
        sb.append(", paiStartDate='").append(paiStartDate).append('\'');
        sb.append(", paiEndDate='").append(paiEndDate).append('\'');
        sb.append(", paiPolicyNumber='").append(paiPolicyNumber).append('\'');
        sb.append(", paiMedicalCosts='").append(paiMedicalCosts).append('\'');
        sb.append(", paiInsuranceCompanyId=").append(paiInsuranceCompanyId);
        sb.append(", paiInsurancePolicyId=").append(paiInsurancePolicyId);
        sb.append(", legalCostServiceId='").append(legalCostServiceId).append('\'');
        sb.append(", legalCostService='").append(legalCostService).append('\'');
        sb.append(", legalCostCompanyId='").append(legalCostCompanyId).append('\'');
        sb.append(", legalCostCompany='").append(legalCostCompany).append('\'');
        sb.append(", legalCostTariff='").append(legalCostTariff).append('\'');
        sb.append(", legalCostTariffId='").append(legalCostTariffId).append('\'');
        sb.append(", legalCostTariffCode='").append(legalCostTariffCode).append('\'');
        sb.append(", legalCostPolicyNumber='").append(legalCostPolicyNumber).append('\'');
        sb.append(", legalCostStartDate='").append(legalCostStartDate).append('\'');
        sb.append(", legalCostEndDate='").append(legalCostEndDate).append('\'');
        sb.append(", legalCostInsuranceCompanyId=").append(legalCostInsuranceCompanyId);
        sb.append(", legalCostInsurancePolicyId=").append(legalCostInsurancePolicyId);
        sb.append(", kaskoDeductibleId=").append(kaskoDeductibleId);
        sb.append(", kaskoDeductibleValue='").append(kaskoDeductibleValue).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClaimsDamagedInsuranceInfoEntity that = (ClaimsDamagedInsuranceInfoEntity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(insuranceInfoId, that.insuranceInfoId) &&
                Objects.equals(isCard, that.isCard) &&
                Objects.equals(tplServiceId, that.tplServiceId) &&
                Objects.equals(tplService, that.tplService) &&
                Objects.equals(tplMaxCoverage, that.tplMaxCoverage) &&
                Objects.equals(tplDeductible, that.tplDeductible) &&
                Objects.equals(tplCompanyId, that.tplCompanyId) &&
                Objects.equals(tplCompany, that.tplCompany) &&
                Objects.equals(tplTariffId, that.tplTariffId) &&
                Objects.equals(tplTariff, that.tplTariff) &&
                Objects.equals(tplTariffCode, that.tplTariffCode) &&
                Objects.equals(tplStartDate, that.tplStartDate) &&
                Objects.equals(tplEndDate, that.tplEndDate) &&
                Objects.equals(tplPolicyNumber, that.tplPolicyNumber) &&
                Objects.equals(tplCompanyDescription, that.tplCompanyDescription) &&
                Objects.equals(tplInsuranceCompanyId, that.tplInsuranceCompanyId) &&
                Objects.equals(tplInsurancePolicyId, that.tplInsurancePolicyId) &&
                Objects.equals(theftServiceId, that.theftServiceId) &&
                Objects.equals(theftService, that.theftService) &&
                Objects.equals(theftDeductibleId, that.theftDeductibleId) &&
                Objects.equals(theftDeductible, that.theftDeductible) &&
                Objects.equals(paiServiceId, that.paiServiceId) &&
                Objects.equals(paiService, that.paiService) &&
                Objects.equals(paiMaxCoverage, that.paiMaxCoverage) &&
                Objects.equals(paiDeductibleId, that.paiDeductibleId) &&
                Objects.equals(paiDeductible, that.paiDeductible) &&
                Objects.equals(paiCompanyId, that.paiCompanyId) &&
                Objects.equals(paiCompany, that.paiCompany) &&
                Objects.equals(paiTariffId, that.paiTariffId) &&
                Objects.equals(paiTariff, that.paiTariff) &&
                Objects.equals(paiTariffCode, that.paiTariffCode) &&
                Objects.equals(paiPercentage, that.paiPercentage) &&
                Objects.equals(paiStartDate, that.paiStartDate) &&
                Objects.equals(paiEndDate, that.paiEndDate) &&
                Objects.equals(paiPolicyNumber, that.paiPolicyNumber) &&
                Objects.equals(paiMedicalCosts, that.paiMedicalCosts) &&
                Objects.equals(paiInsuranceCompanyId, that.paiInsuranceCompanyId) &&
                Objects.equals(paiInsurancePolicyId, that.paiInsurancePolicyId) &&
                Objects.equals(legalCostServiceId, that.legalCostServiceId) &&
                Objects.equals(legalCostService, that.legalCostService) &&
                Objects.equals(legalCostCompanyId, that.legalCostCompanyId) &&
                Objects.equals(legalCostCompany, that.legalCostCompany) &&
                Objects.equals(legalCostTariff, that.legalCostTariff) &&
                Objects.equals(legalCostTariffId, that.legalCostTariffId) &&
                Objects.equals(legalCostTariffCode, that.legalCostTariffCode) &&
                Objects.equals(legalCostPolicyNumber, that.legalCostPolicyNumber) &&
                Objects.equals(legalCostStartDate, that.legalCostStartDate) &&
                Objects.equals(legalCostEndDate, that.legalCostEndDate) &&
                Objects.equals(legalCostInsuranceCompanyId, that.legalCostInsuranceCompanyId) &&
                Objects.equals(legalCostInsurancePolicyId, that.legalCostInsurancePolicyId) &&
                Objects.equals(kaskoDeductibleId, that.kaskoDeductibleId) &&
                Objects.equals(kaskoDeductibleValue, that.kaskoDeductibleValue) &&
                Objects.equals(claim, that.claim);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, insuranceInfoId, isCard, tplServiceId, tplService, tplMaxCoverage, tplDeductible, tplCompanyId, tplCompany, tplTariffId, tplTariff, tplTariffCode, tplStartDate, tplEndDate, tplPolicyNumber, tplCompanyDescription, tplInsuranceCompanyId, tplInsurancePolicyId, theftServiceId, theftService, theftDeductibleId, theftDeductible, paiServiceId, paiService, paiMaxCoverage, paiDeductibleId, paiDeductible, paiCompanyId, paiCompany, paiTariffId, paiTariff, paiTariffCode, paiPercentage, paiStartDate, paiEndDate, paiPolicyNumber, paiMedicalCosts, paiInsuranceCompanyId, paiInsurancePolicyId, legalCostServiceId, legalCostService, legalCostCompanyId, legalCostCompany, legalCostTariff, legalCostTariffId, legalCostTariffCode, legalCostPolicyNumber, legalCostStartDate, legalCostEndDate, legalCostInsuranceCompanyId, legalCostInsurancePolicyId, kaskoDeductibleId, kaskoDeductibleValue, claim);
    }
}
