package com.doing.nemo.claims.entity.enumerated.AuthorityEnum;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;

public enum AuthorityVehicleStatusEnum implements Serializable {
    NOT_UNDER_MAINTENANCE("not_under_maintenance"),
    UNDER_MAINTENANCE("under_maintenance");

    private static Logger LOGGER = LoggerFactory.getLogger(AuthorityVehicleStatusEnum.class);
    private String authorityStatus;

    private AuthorityVehicleStatusEnum(String authorityStatus) {
        this.authorityStatus = authorityStatus;
    }

    @JsonCreator
    public static AuthorityVehicleStatusEnum create(String authorityStatus) {

        authorityStatus = authorityStatus.replace(" - ", "_");
        authorityStatus = authorityStatus.replace('-', '_');
        authorityStatus = authorityStatus.replace(' ', '_');

        if (authorityStatus != null) {
            for (AuthorityVehicleStatusEnum val : AuthorityVehicleStatusEnum.values()) {
                if (authorityStatus.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        LOGGER.debug("Bad parameters exception. Enum class " + AuthorityVehicleStatusEnum.class.getSimpleName() + " doesn't accept this value: " + authorityStatus);
        throw new BadParametersException("Bad parameters exception. Enum class " + AuthorityVehicleStatusEnum.class.getSimpleName() + " doesn't accept this value: " + authorityStatus);
    }

    public String getValue() {
        return this.authorityStatus.toUpperCase();
    }

    @JsonValue
    public String toValue() {
        for (AuthorityVehicleStatusEnum val : AuthorityVehicleStatusEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }
}
