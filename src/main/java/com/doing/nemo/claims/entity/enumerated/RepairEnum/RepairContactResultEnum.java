package com.doing.nemo.claims.entity.enumerated.RepairEnum;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;

public enum RepairContactResultEnum implements Serializable {
    PENDING("pending"),
    ACCEPTANCE("acceptance"),
    DENIAL("denial"),
    TO_RECONTACT("to_recontact"),
    REPAIR_ACCEPTED("repair_accepted"),
    LIQUIDATION_ACCEPTED("liquidation_accepted"),
    REPAIR_FLOW_ACCEPTED("repair_flow_accepted");

    private static Logger LOGGER = LoggerFactory.getLogger(RepairContactResultEnum.class);
    private String repairContactResultEntity;

    RepairContactResultEnum(String repairContactResultEntity) {
        this.repairContactResultEntity = repairContactResultEntity;
    }

    @JsonCreator
    public static RepairContactResultEnum create(String repairContactResultEntity) {

        repairContactResultEntity = repairContactResultEntity.replace(" - ", "_");
        repairContactResultEntity = repairContactResultEntity.replace('-', '_');
        repairContactResultEntity = repairContactResultEntity.replace(' ', '_');

        if (repairContactResultEntity != null) {
            for (RepairContactResultEnum val : RepairContactResultEnum.values()) {
                if (repairContactResultEntity.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        LOGGER.debug("Bad parameters exception. Enum class " + RepairContactResultEnum.class.getSimpleName() + " doesn't accept this value: " + repairContactResultEntity);
        throw new BadParametersException("Bad parameters exception. Enum class " + RepairContactResultEnum.class.getSimpleName() + " doesn't accept this value: " + repairContactResultEntity);
    }

    public String getValue() {
        return this.repairContactResultEntity.toUpperCase();
    }

    @JsonValue
    public String toValue() {
        for (RepairContactResultEnum val : RepairContactResultEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }
}
