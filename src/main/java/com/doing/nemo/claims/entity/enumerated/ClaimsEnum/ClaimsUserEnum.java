package com.doing.nemo.claims.entity.enumerated.ClaimsEnum;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;

public enum ClaimsUserEnum implements Serializable {
    LOJACK("lojack"),
    MYALD("myald"),
    CUSTOMER_SERVICE("customer_service"),
    MSA("msa");

    private static Logger LOGGER = LoggerFactory.getLogger(ClaimsUserEnum.class);
    private String userEntity;

    private ClaimsUserEnum(String userEntity) {
        this.userEntity = userEntity;
    }

    @JsonCreator
    public static ClaimsUserEnum create(String userEntity) {

        userEntity = userEntity.replace(" - ", "_");
        userEntity = userEntity.replace('-', '_');
        userEntity = userEntity.replace(' ', '_');

        if (userEntity != null) {
            for (ClaimsUserEnum val : ClaimsUserEnum.values()) {
                if (userEntity.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        LOGGER.debug("Bad parameters exception. Enum class " + ClaimsUserEnum.class.getSimpleName() + " doesn't accept this value: " + userEntity);
        throw new BadParametersException("Bad parameters exception. Enum class " + ClaimsUserEnum.class.getSimpleName() + " doesn't accept this value: " + userEntity);
    }

    public String getValue() {
        return this.userEntity.toUpperCase();
    }

    @JsonValue
    public String toValue() {
        for (ClaimsUserEnum val : ClaimsUserEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }
}
