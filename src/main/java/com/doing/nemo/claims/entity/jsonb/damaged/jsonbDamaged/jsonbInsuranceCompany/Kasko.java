package com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbInsuranceCompany;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Kasko implements Serializable {

    private static final long serialVersionUID = -8507932932012699109L;

    @JsonProperty("deductible_id")
    private Integer deductibleId;
    @JsonProperty("deductible_value")
    private String deductibleValue;

    public Kasko() {
    }

    public Kasko(Integer deductibleId, String deductibleValue) {
        this.deductibleId = deductibleId;
        this.deductibleValue = deductibleValue;
    }

    public Integer getDeductibleId() {
        return deductibleId;
    }

    public void setDeductibleId(Integer deductibleId) {
        this.deductibleId = deductibleId;
    }

    public String getDeductibleValue() {
        return deductibleValue;
    }

    public void setDeductibleValue(String deductibleValue) {
        this.deductibleValue = deductibleValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Kasko kasko = (Kasko) o;

        return new EqualsBuilder().append(deductibleId, kasko.deductibleId).append(deductibleValue, kasko.deductibleValue).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(deductibleId).append(deductibleValue).toHashCode();
    }
}
