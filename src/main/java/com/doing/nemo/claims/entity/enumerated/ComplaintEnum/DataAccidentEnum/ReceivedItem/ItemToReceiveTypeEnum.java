package com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.ReceivedItem;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;

public enum ItemToReceiveTypeEnum implements Serializable {
    FIRST_KEY("first_key"),
    SECOND_KEY("second_key"),
    ORIGINAL_COMPLAINT("original_complaint");

    private static Logger LOGGER = LoggerFactory.getLogger(ItemToReceiveTypeEnum.class);
    private String itemToReceiveTypeEntity;

    private ItemToReceiveTypeEnum(String itemToReceiveTypeEntity) {
        this.itemToReceiveTypeEntity = itemToReceiveTypeEntity;
    }

    @JsonCreator
    public static ItemToReceiveTypeEnum create(String itemToReceiveTypeEntity) {

        itemToReceiveTypeEntity = itemToReceiveTypeEntity.replace(" - ", "_");
        itemToReceiveTypeEntity = itemToReceiveTypeEntity.replace('-', '_');
        itemToReceiveTypeEntity = itemToReceiveTypeEntity.replace(' ', '_');

        if (itemToReceiveTypeEntity != null) {
            for (ItemToReceiveTypeEnum val : ItemToReceiveTypeEnum.values()) {
                if (itemToReceiveTypeEntity.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        LOGGER.debug("Error in CostItem, duplicate code not admitted");
        throw new BadParametersException("Bad parameters exception. Enum class " + ItemToReceiveTypeEnum.class.getSimpleName() + " doesn't accept this value: " + itemToReceiveTypeEntity);
    }

    public String getValue() {
        return this.itemToReceiveTypeEntity.toUpperCase();
    }

    @JsonValue
    public String toValue() {
        for (ItemToReceiveTypeEnum val : ItemToReceiveTypeEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }
}
