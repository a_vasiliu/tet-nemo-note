package com.doing.nemo.claims.entity.enumerated.DTO;


import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class ClaimsStats implements Serializable {

    @JsonProperty("waiting_for_validation")
    private double waitingForValidation;
    @JsonProperty("waiting_for_authority")
    private double waitingForauthority;
    @JsonProperty("draft")
    private double draft;
    @JsonProperty("deleted")
    private double deleted;
    @JsonProperty("to_entrust")
    private double toEntrust;
    @JsonProperty("closed")
    private double closed;
    @JsonProperty("closed_practice")
    private double closedPractice;
    @JsonProperty("incomplete")
    private double incomplete;
    @JsonProperty("entrusted_at")
    private double entrustedAt;
    @JsonProperty("rejected")
    private double rejected;
    @JsonProperty("managed")
    private double managed;
    @JsonProperty("proposed_accepted")
    private double proposedAccepted;
    @JsonProperty("waiting_for_refund")
    private double waitingForRefund;
    @JsonProperty("kasko")
    private double kasko;



    public ClaimsStats() {
    }

    public ClaimsStats(double waitingForValidation, double waitingForauthority, double draft, double deleted, double toEntrust, double closed, double closedPractice, double incomplete, double entrustedAt, double rejected, double managed, double proposedAccepted, double waitingForRefund, double kasko) {
        this.waitingForValidation = waitingForValidation;
        this.waitingForauthority = waitingForauthority;
        this.draft = draft;
        this.deleted = deleted;
        this.toEntrust = toEntrust;
        this.closed = closed;
        this.closedPractice = closedPractice;
        this.incomplete = incomplete;
        this.entrustedAt = entrustedAt;
        this.rejected = rejected;
        this.managed = managed;
        this.proposedAccepted = proposedAccepted;
        this.waitingForRefund = waitingForRefund;
        this.kasko = kasko;
    }

    public double getWaitingForauthority() {
        return waitingForauthority;
    }

    public void setWaitingForauthority(double waitingForauthority) {
        this.waitingForauthority = waitingForauthority;
    }

    public double getDraft() {
        return draft;
    }

    public void setDraft(double draft) {
        this.draft = draft;
    }

    public double getDeleted() {
        return deleted;
    }

    public void setDeleted(double deleted) {
        this.deleted = deleted;
    }

    public double getToEntrust() {
        return toEntrust;
    }

    public void setToEntrust(double toEntrust) {
        this.toEntrust = toEntrust;
    }

    public double getClosedPractice() {
        return closedPractice;
    }

    public void setClosedPractice(double closedPractice) {
        this.closedPractice = closedPractice;
    }

    public double getIncomplete() {
        return incomplete;
    }

    public void setIncomplete(double incomplete) {
        this.incomplete = incomplete;
    }

    public double getEntrustedAt() {
        return entrustedAt;
    }

    public void setEntrustedAt(double entrustedAt) {
        this.entrustedAt = entrustedAt;
    }

    public double getRejected() {
        return rejected;
    }

    public void setRejected(double rejected) {
        this.rejected = rejected;
    }

    public double getManaged() {
        return managed;
    }

    public void setManaged(double managed) {
        this.managed = managed;
    }

    public double getProposedAccepted() {
        return proposedAccepted;
    }

    public void setProposedAccepted(double proposedAccepted) {
        this.proposedAccepted = proposedAccepted;
    }

    public double getWaitingForRefund() {
        return waitingForRefund;
    }

    public void setWaitingForRefund(double waitingForRefund) {
        this.waitingForRefund = waitingForRefund;
    }

    public double getKasko() {
        return kasko;
    }

    public void setKasko(double kasko) {
        this.kasko = kasko;
    }

    public double getClosed() {
        return closed;
    }

    public void setClosed(double closed) {
        this.closed = closed;
    }

    public double getWaitingForValidation() {
        return waitingForValidation;
    }

    public void setWaitingForValidation(double waitingForValidation) {
        this.waitingForValidation = waitingForValidation;
    }
}

