package com.doing.nemo.claims.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;


@Embeddable
public class ClaimsAuthorityId implements Serializable {

    @Column(name = "claims_id")
    private String claimsId;

    @Column(name = "authority_id")
    private String authorityId;


    public ClaimsAuthorityId() {}

    public ClaimsAuthorityId(String claimsId, String authorityId) {
        this.claimsId = claimsId;
        this.authorityId = authorityId;
    }

    public String getClaimsId() {
        return claimsId;
    }

    public void setClaimsId(String claimsId) {
        this.claimsId = claimsId;
    }

    public String getAuthorityId() {
        return authorityId;
    }

    public void setAuthorityId(String authorityId) {
        this.authorityId = authorityId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ClaimsAuthorityId)) return false;
        ClaimsAuthorityId that = (ClaimsAuthorityId) o;
        return Objects.equals(getClaimsId(), that.getClaimsId()) &&
                Objects.equals(getAuthorityId(), that.getAuthorityId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getClaimsId(), getAuthorityId());
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ClaimsAuthorityId{");
        sb.append("claimsId='").append(claimsId).append('\'');
        sb.append(", authorityId='").append(authorityId).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
