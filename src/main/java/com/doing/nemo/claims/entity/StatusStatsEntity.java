package com.doing.nemo.claims.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class StatusStatsEntity {


    private Integer count;
    @JsonIgnore
    private String status;
    @JsonIgnore
    private String date;

    public StatusStatsEntity() {
    }

    public StatusStatsEntity(Integer count, String status, String date) {
        this.count = count;
        this.status = status;
        this.date = date;
    }

    @JsonIgnore
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @JsonIgnore
    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @JsonIgnore
    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "StatusStatsEntity{" +
                "count=" + count +
                ", status='" + status + '\'' +
                ", date='" + date + '\'' +
                '}';
    }
}
