package com.doing.nemo.claims.entity.esb.VehicleESB;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TestEsb implements Serializable {

    private static final long serialVersionUID = -891521245837264882L;

    @JsonProperty("test")
    private String test;

    public TestEsb() {
    }

    public TestEsb(String test) {
        this.test = test;
    }

    public String getTest() {
        return test;
    }

    public void setTest(String test) {
        this.test = test;
    }

    @Override
    public String toString() {
        return "TestEsb{" +
                "test='" + test + '\'' +
                '}';
    }
}
