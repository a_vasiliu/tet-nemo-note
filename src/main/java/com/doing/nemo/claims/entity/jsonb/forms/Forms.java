package com.doing.nemo.claims.entity.jsonb.forms;

import com.doing.nemo.claims.entity.jsonb.forms.jsonbForms.Attachment;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Forms implements Serializable {

    private static final long serialVersionUID = 8503649522247790749L;

    @JsonProperty("attachment")
    private List<Attachment> attachment;

    public List<Attachment> getAttachment() {
        if(attachment == null){
            return null;
        }
        return new ArrayList<>(attachment);
    }

    public void setAttachment(List<Attachment> attachment) {
        if(attachment != null)
        {
            this.attachment = new ArrayList<>(attachment);
        } else {
            this.attachment = null;
        }
    }

    @Override
    public String toString() {
        return "forms{" +
                "attachment=" + attachment +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Forms forms = (Forms) o;

        return new EqualsBuilder().append(attachment, forms.attachment).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(attachment).toHashCode();
    }
}
