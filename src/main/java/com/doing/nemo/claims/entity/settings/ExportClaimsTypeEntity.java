package com.doing.nemo.claims.entity.settings;

import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "export_claims_type")
public class ExportClaimsTypeEntity implements Serializable {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Type(type = "org.hibernate.type.PostgresUUIDType")
    private UUID id;

    @Enumerated(EnumType.STRING)
    @Column(name = "claims_type")
    private DataAccidentTypeAccidentEnum claimsType;

    @OneToOne
    @JoinColumn(name = "dm_system_id", nullable = true)
    private DmSystemsEntity dmSystem;

    @Column(name = "code")
    private String code;

    @Column(name = "description")
    private String description;

    @Column(name = "is_active")
    private Boolean isActive;

    public ExportClaimsTypeEntity() {
    }

    public ExportClaimsTypeEntity(DataAccidentTypeAccidentEnum claimsType, DmSystemsEntity dmSystem, String code, String description, Boolean isActive) {
        this.claimsType = claimsType;
        this.dmSystem = dmSystem;
        this.code = code;
        this.description = description;
        this.isActive = isActive;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public DataAccidentTypeAccidentEnum getClaimsType() {
        return claimsType;
    }

    public void setClaimsType(DataAccidentTypeAccidentEnum claimsType) {
        this.claimsType = claimsType;
    }

    public DmSystemsEntity getDmSystem() {
        return dmSystem;
    }

    public void setDmSystem(DmSystemsEntity dmSystem) {
        this.dmSystem = dmSystem;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    @Override
    public String toString() {
        return "ExportClaimsTypeEntity{" +
                "id=" + id +
                ", claimsType=" + claimsType +
                ", dmSystem=" + dmSystem +
                ", code='" + code + '\'' +
                ", description='" + description + '\'' +
                ", isActive=" + isActive +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ExportClaimsTypeEntity that = (ExportClaimsTypeEntity) o;
        return Objects.equals(getId(), that.getId()) &&
                getClaimsType() == that.getClaimsType() &&
                Objects.equals(getDmSystem(), that.getDmSystem()) &&
                Objects.equals(getCode(), that.getCode()) &&
                Objects.equals(getDescription(), that.getDescription()) &&
                Objects.equals(getActive(), that.getActive());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getClaimsType(), getDmSystem(), getCode(), getDescription(), getActive());
    }
}
