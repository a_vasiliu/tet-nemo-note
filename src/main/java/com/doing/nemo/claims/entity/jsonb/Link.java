package com.doing.nemo.claims.entity.jsonb;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Link implements Serializable {

    private static final long serialVersionUID = -8772054718951863776L;

    @JsonProperty("rel")
    private String rel;

    @JsonProperty("href")
    private String href;

    @JsonProperty("test")
    private List<Test> testList;

    public String getRel() {
        return rel;
    }

    public void setRel(String rel) {
        this.rel = rel;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public List<Test> getTestList() {
        if(testList == null){
            return null;
        }
        return new ArrayList<>(testList);
    }

    public void setTestList(List<Test> testList) {
        if(testList != null)
        {
            this.testList = new ArrayList<>(testList);
        } else {
            this.testList = null;
        }
    }

    @Override
    public String toString() {
        return "Link{" +
                "rel='" + rel + '\'' +
                ", href='" + href + '\'' +
                ", testList=" + testList +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Link link = (Link) o;

        return new EqualsBuilder().append(rel, link.rel).append(href, link.href).append(testList, link.testList).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(rel).append(href).append(testList).toHashCode();
    }
}
