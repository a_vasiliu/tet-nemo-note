package com.doing.nemo.claims.entity.enumerated.RepairEnum;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;

public enum RepairStatusEnum implements Serializable {
    TO_SORT("to_sort"),//DA SMISTARE
    TO_CONTACT("to_contact"),
    TO_RECONTACT("to_recontact"),
    WAITING_FOR_QUOTATION("waiting_for_quotation"),
    QUOTATION_APPROVED("quotation_approved"),
    UNDER_REPAIR("under_repair"),
    UNDER_LIQUIDATION("under_liquidation"),
    REPAIRED("repaired"),
    CLOSED_LIQUIDATED("closed_liquidated"),
    CLOSED_REPAIRED("closed_repaired"),
    CLOSED_DENIAL("closed_denial"),
    REPAIRING("repairing"),
    TO_CONTACT_AGAIN("to_contact_again"),
    WAITING_DOCUMENTS("waiting_documents"),
    MANAGEABLE("manageable"),
    MSA_MANAGEMENT("msa_management"),
    WAITING_FOR_NETWORK_TO_REPAIR("waiting_for_network_to_repair"),
    WAITING_FOR_NETWORK_TO_LIQUIDATE("waiting_for_network_to_liquidate"),
    WAITING_FOR_INVOICE("waiting_for_invoice"),
    CONFIRMED_LIQUIDATE("confirmed_liquidate"),
    CLOSED("closed"),
    CANCELLED("cancelled"),
    WRECK("wreck"),
    WSREPAIR ("wsrepair");
    private static Logger LOGGER = LoggerFactory.getLogger(RepairStatusEnum.class);
    private String repairStatusEntity;

    private RepairStatusEnum(String repairStatusEntity) {
        this.repairStatusEntity = repairStatusEntity;
    }

    @JsonCreator
    public static RepairStatusEnum create(String repairStatusEntity) {

        repairStatusEntity = repairStatusEntity.replace(" - ", "_");
        repairStatusEntity = repairStatusEntity.replace('-', '_');
        repairStatusEntity = repairStatusEntity.replace(' ', '_');

        if (repairStatusEntity != null) {
            for (RepairStatusEnum val : RepairStatusEnum.values()) {
                if (repairStatusEntity.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        LOGGER.debug("Bad parameters exception. Enum class " + RepairStatusEnum.class.getSimpleName() + " doesn't accept this value: " + repairStatusEntity);
        throw new BadParametersException("Bad parameters exception. Enum class " + RepairStatusEnum.class.getSimpleName() + " doesn't accept this value: " + repairStatusEntity);
    }

    public String getValue() {
        return this.repairStatusEntity.toUpperCase();
    }

    @JsonValue
    public String toValue() {
        for (RepairStatusEnum val : RepairStatusEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }
}
