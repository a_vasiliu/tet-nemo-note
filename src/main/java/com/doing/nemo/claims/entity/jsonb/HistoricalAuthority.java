package com.doing.nemo.claims.entity.jsonb;

import com.doing.nemo.claims.entity.enumerated.AuthorityEnum.AuthorityEventTypeEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class HistoricalAuthority implements Serializable {

    @JsonProperty("id")
    private Long id;

    @JsonProperty("event_type")
    private AuthorityEventTypeEnum eventType;

    @JsonProperty("update_at")
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    private Date updateAt;

    @JsonProperty("user_id")
    private String userId;

    @JsonProperty("user_name")
    private String userName;

    @JsonProperty("description")
    private String description;

    @JsonProperty("old_total")
    private Double oldTotal;

    @JsonProperty("new_total")
    private Double newTotal;

    public Double getOldTotal() {
        return oldTotal;
    }

    public void setOldTotal(Double oldTotal) {
        this.oldTotal = oldTotal;
    }

    public Double getNewTotal() {
        return newTotal;
    }

    public void setNewTotal(Double newTotal) {
        this.newTotal = newTotal;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AuthorityEventTypeEnum getEventType() {
        return eventType;
    }

    public void setEventType(AuthorityEventTypeEnum eventType) {
        this.eventType = eventType;
    }

    public Date getUpdateAt() {
        if(updateAt == null){
            return null;
        }
        return (Date)updateAt.clone();
    }

    public void setUpdateAt(Date updateAt) {
        if(updateAt != null)
        {
            this.updateAt = (Date)updateAt.clone();
        } else {
            this.updateAt = null;
        }
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "HistoricalAuthority{" +
                "id=" + id +
                ", eventType=" + eventType +
                ", updateAt=" + updateAt +
                ", userId='" + userId + '\'' +
                ", userName='" + userName + '\'' +
                ", description='" + description + '\'' +
                ", oldTotal=" + oldTotal +
                ", newTotal=" + newTotal +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        HistoricalAuthority that = (HistoricalAuthority) o;

        return new EqualsBuilder().append(id, that.id).append(eventType, that.eventType)
                .append(updateAt, that.updateAt).append(userId, that.userId).append(userName, that.userName)
                .append(description, that.description).append(oldTotal, that.oldTotal).append(newTotal, that.newTotal).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(id).append(eventType)
                .append(updateAt).append(userId).append(userName).append(description).append(oldTotal).append(newTotal).toHashCode();
    }
}
