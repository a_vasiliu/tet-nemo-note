package com.doing.nemo.claims.entity.settings;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "supplier_code", uniqueConstraints = @UniqueConstraint(
        name = "uc_exportsuppliercode",
        columnNames =
            {
                "vat_number"
            }
))
public class ExportSupplierCodeEntity {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )

    @Type(type = "org.hibernate.type.PostgresUUIDType")
    private UUID id;

    @Column(name = "vat_number")
    private String vatNumber;

    @Column(name = "denomination")
    private String denomination;

    @Column(name = "code_to_export")
    private String codeToExport;

    @Column(name = "is_active")
    private Boolean isActive;

    public ExportSupplierCodeEntity() {
    }

    public ExportSupplierCodeEntity(String vatNumber, String denomination, String codeToExport, Boolean isActive) {
        this.vatNumber = vatNumber;
        this.denomination = denomination;
        this.codeToExport = codeToExport;
        this.isActive = isActive;
    }

    public String getVatNumber() {
        return vatNumber;
    }

    public void setVatNumber(String vatNumber) {
        this.vatNumber = vatNumber;
    }

    public String getDenomination() {
        return denomination;
    }

    public void setDenomination(String denomination) {
        this.denomination = denomination;
    }

    public String getCodeToExport() {
        return codeToExport;
    }

    public void setCodeToExport(String codeToExport) {
        this.codeToExport = codeToExport;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    @Override
    public String toString() {
        return "ExportSupplierCodeEntity{" +
                "id=" + id +
                ", vatNumber='" + vatNumber + '\'' +
                ", denomination='" + denomination + '\'' +
                ", codeToExport='" + codeToExport + '\'' +
                ", isActive=" + isActive +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ExportSupplierCodeEntity that = (ExportSupplierCodeEntity) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getVatNumber(), that.getVatNumber()) &&
                Objects.equals(getDenomination(), that.getDenomination()) &&
                Objects.equals(getCodeToExport(), that.getCodeToExport()) &&
                Objects.equals(getActive(), that.getActive());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getVatNumber(), getDenomination(), getCodeToExport(), getActive());
    }
}
