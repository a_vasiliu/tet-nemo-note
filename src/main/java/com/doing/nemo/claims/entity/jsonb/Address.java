package com.doing.nemo.claims.entity.jsonb;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Address implements Serializable {

    private static final long serialVersionUID = 3368771332707959650L;

    @JsonProperty("street")
    private String street;

    @JsonProperty("street_nr")
    private String streetNr;

    @JsonProperty("zip")
    private String zip;

    @JsonProperty("locality")
    private String locality;

    @JsonProperty("province")
    private String province;

    @JsonProperty("region")
    private String region;

    @JsonProperty("state")
    private String state;

    @JsonProperty("formatted_address")
    private String formattedAddress;

    public Address() {
    }

    public Address(String street, String streetNr, String zip, String locality, String province, String region, String state) {
        this.street = street;
        this.streetNr = streetNr;
        this.zip = zip;
        this.locality = locality;
        this.province = province;
        this.region = region;
        this.state = state;
        this.formattedAddress = this.street + " " + this.streetNr + ", " + this.zip + " " + this.locality + ", " + this.state;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getStreetNr() {
        return streetNr;
    }

    public void setStreetNr(String streetNr) {
        this.streetNr = streetNr;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getFormattedAddress() {
        return formattedAddress;
    }

    public void setFormattedAddress(String formattedAddress) {
        this.formattedAddress = formattedAddress;
    }

    @Override
    public String toString() {
        return "AddressEsb{" +
                "street='" + street + '\'' +
                ", streetNr='" + streetNr + '\'' +
                ", zip='" + zip + '\'' +
                ", locality='" + locality + '\'' +
                ", province='" + province + '\'' +
                ", region='" + region + '\'' +
                ", state='" + state + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Address address = (Address) o;

        return new EqualsBuilder().append(street, address.street).append(streetNr, address.streetNr)
                .append(zip, address.zip).append(locality, address.locality).append(province, address.province)
                .append(region, address.region).append(state, address.state).append(formattedAddress, address.formattedAddress).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(street).append(streetNr).append(zip)
                .append(locality).append(province).append(region).append(state).append(formattedAddress).toHashCode();
    }
}
