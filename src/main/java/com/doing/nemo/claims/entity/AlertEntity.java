package com.doing.nemo.claims.entity;

import com.doing.nemo.claims.entity.enumerated.AlertEnum.AlertReadersEnum;
import com.doing.nemo.claims.entity.jsonb.dataType.JsonDataAlertReadersType;
import com.doing.nemo.claims.entity.jsonb.dataType.JsonDataAttachmentType;
import com.doing.nemo.claims.entity.jsonb.dataType.JsonDataMetadataType;
import com.doing.nemo.claims.entity.jsonb.forms.jsonbForms.Attachment;
import com.doing.nemo.claims.entity.jsonb.metadata.Metadata;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "alert")
@TypeDefs({
        @TypeDef(name = "JsonDataAlertReadersType", typeClass = JsonDataAlertReadersType.class),
        @TypeDef(name = "JsonDataAttachmentType", typeClass = JsonDataAttachmentType.class),
        @TypeDef(name = "JsonDataMetadataType", typeClass = JsonDataMetadataType.class)
})
public class AlertEntity {

    @Id
    @Type(type = "org.hibernate.type.PostgresUUIDType")
    @Column(name = "id")
    private UUID alertId;

    @Column(name = "module")
    private String module;//Potrebbe essere un enum

    @Column(name = "created_by")
    private String createdBy;                   //Obbligatorio

    @Column(name = "user_id")
    @Type(type = "org.hibernate.type.PostgresUUIDType")
    private UUID userId;

    @Type(type = "JsonDataAlertReadersType")
    @Column(name = "readers")
    private List<AlertReadersEnum> readers;   //Obbligatorio

    @Column(name = "commodity_id")
    @Type(type = "org.hibernate.type.PostgresUUIDType")
    private UUID commodityId;                  //Obbligatorio

    @Column(name = "ald_commodity_id")
    private Integer aldCommodityId;           //Obbligatorio

    @Column(name = "ald_order_id")
    private Integer aldOrderId;               //Obbligatorio

    @Column(name = "subject")
    private String subject;                     //Obbligatorio

    @Column(name = "text")
    private String text;                        //Obbligatorio

    @Type(type = "JsonDataMetadataType")
    private Metadata metadata;                  //Obbligatorio

    @Type(type = "JsonDataAttachmentType")
    private List<Attachment> attachments;

    public UUID getAlertId() {
        return alertId;
    }

    public void setAlertId(UUID alertId) {
        this.alertId = alertId;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public List<AlertReadersEnum> getReaders() {
        if(readers == null){
            return null;
        }
        return new ArrayList<>(readers);
    }

    public void setReaders(List<AlertReadersEnum> readers) {
        if(readers != null)
        {
            this.readers = new ArrayList<>(readers);
        } else {
            this.readers = null;
        }
    }

    public UUID getCommodityId() {
        return commodityId;
    }

    public void setCommodityId(UUID commodityId) {
        this.commodityId = commodityId;
    }

    public Integer getAldCommodityId() {
        return aldCommodityId;
    }

    public void setAldCommodityId(Integer aldCommodityId) {
        this.aldCommodityId = aldCommodityId;
    }

    public Integer getAldOrderId() {
        return aldOrderId;
    }

    public void setAldOrderId(Integer aldOrderId) {
        this.aldOrderId = aldOrderId;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Metadata getMetadata() {
        return metadata;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

    public List<Attachment> getAttachments() {
        if(attachments == null){
            return null;
        }
        return new ArrayList<>(attachments);
    }

    public void setAttachments(List<Attachment> attachments) {
        if(attachments != null)
        {
            this.attachments = new ArrayList<>(attachments);
        } else {
            this.attachments = null;
        }
    }

    @Override
    public String toString() {
        return "AlertEntity{" +
                "alertId=" + alertId +
                ", module='" + module + '\'' +
                ", createdBy='" + createdBy + '\'' +
                ", userId=" + userId +
                ", readers=" + readers +
                ", commodityId=" + commodityId +
                ", aldCommodityId=" + aldCommodityId +
                ", aldOrderId=" + aldOrderId +
                ", subject='" + subject + '\'' +
                ", text='" + text + '\'' +
                ", metadata=" + metadata +
                ", attachments=" + attachments +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AlertEntity that = (AlertEntity) o;
        return Objects.equals(getAlertId(), that.getAlertId()) &&
                Objects.equals(getModule(), that.getModule()) &&
                Objects.equals(getCreatedBy(), that.getCreatedBy()) &&
                Objects.equals(getUserId(), that.getUserId()) &&
                Objects.equals(getReaders(), that.getReaders()) &&
                Objects.equals(getCommodityId(), that.getCommodityId()) &&
                Objects.equals(getAldCommodityId(), that.getAldCommodityId()) &&
                Objects.equals(getAldOrderId(), that.getAldOrderId()) &&
                Objects.equals(getSubject(), that.getSubject()) &&
                Objects.equals(getText(), that.getText()) &&
                Objects.equals(getMetadata(), that.getMetadata()) &&
                Objects.equals(getAttachments(), that.getAttachments());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAlertId(), getModule(), getCreatedBy(), getUserId(), getReaders(), getCommodityId(), getAldCommodityId(), getAldOrderId(), getSubject(), getText(), getMetadata(), getAttachments());
    }
}
