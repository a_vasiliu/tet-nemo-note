package com.doing.nemo.claims.entity.enumerated.DTO;


import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class ClaimsTheftStats implements Serializable {

    @JsonProperty("wait_lost_possession")
    private Integer waitLostPossession;
    @JsonProperty("lost_possession")
    private Integer lostPossession;
    @JsonProperty("wait_to_return_possession")
    private Integer waitToReturnPossession;
    @JsonProperty("return_to_possession")
    private Integer returnToPossession;
    @JsonProperty("to_reregister")
    private Integer toReRegister;
    @JsonProperty("to_work")
    private Integer toWork;
    @JsonProperty("book_duplication")
    private Integer bookDuplication;
    @JsonProperty("stamp")
    private Integer stamp;
    @JsonProperty("demolition")
    private Integer demolition;
    @JsonProperty("receptions_to_be_confirmed")
    private Integer receptionsToBeConfirmed;
    @JsonProperty("with_continuation")
    private Integer withContinuation;
    @JsonProperty("closed")
    private Integer closed;


    public ClaimsTheftStats() {
    }

    public ClaimsTheftStats(Integer waitLostPossession, Integer lostPossession, Integer waitToReturnPossession, Integer returnToPossession, Integer toReRegister, Integer toWork, Integer bookDuplication, Integer stamp, Integer demolition, Integer receptionsToBeConfirmed, Integer withContinuation, Integer closed) {
        this.waitLostPossession = waitLostPossession;
        this.lostPossession = lostPossession;
        this.waitToReturnPossession = waitToReturnPossession;
        this.returnToPossession = returnToPossession;
        this.toReRegister = toReRegister;
        this.toWork = toWork;
        this.bookDuplication = bookDuplication;
        this.stamp = stamp;
        this.demolition = demolition;
        this.receptionsToBeConfirmed = receptionsToBeConfirmed;
        this.withContinuation = withContinuation;
        this.closed = closed;
    }

    public Integer getWithContinuation() {
        return withContinuation;
    }

    public void setWithContinuation(Integer withContinuation) {
        this.withContinuation = withContinuation;
    }

    public Integer getWaitLostPossession() {
        return waitLostPossession;
    }

    public void setWaitLostPossession(Integer waitLostPossession) {
        this.waitLostPossession = waitLostPossession;
    }

    public Integer getLostPossession() {
        return lostPossession;
    }

    public void setLostPossession(Integer lostPossession) {
        this.lostPossession = lostPossession;
    }

    public Integer getWaitToReturnPossession() {
        return waitToReturnPossession;
    }

    public void setWaitToReturnPossession(Integer waitToReturnPossession) {
        this.waitToReturnPossession = waitToReturnPossession;
    }

    public Integer getReturnToPossession() {
        return returnToPossession;
    }

    public void setReturnToPossession(Integer returnToPossession) {
        this.returnToPossession = returnToPossession;
    }

    public Integer getToReRegister() {
        return toReRegister;
    }

    public void setToReRegister(Integer toReRegister) {
        this.toReRegister = toReRegister;
    }

    public Integer getToWork() {
        return toWork;
    }

    public void setToWork(Integer toWork) {
        this.toWork = toWork;
    }

    public Integer getBookDuplication() {
        return bookDuplication;
    }

    public void setBookDuplication(Integer bookDuplication) {
        this.bookDuplication = bookDuplication;
    }

    public Integer getStamp() {
        return stamp;
    }

    public void setStamp(Integer stamp) {
        this.stamp = stamp;
    }

    public Integer getDemolition() {
        return demolition;
    }

    public void setDemolition(Integer demolition) {
        this.demolition = demolition;
    }

    public Integer getReceptionsToBeConfirmed() {
        return receptionsToBeConfirmed;
    }

    public void setReceptionsToBeConfirmed(Integer receptionsToBeConfirmed) {
        this.receptionsToBeConfirmed = receptionsToBeConfirmed;
    }

    public Integer getClosed() {
        return closed;
    }

    public void setClosed(Integer closed) {
        this.closed = closed;
    }
}

