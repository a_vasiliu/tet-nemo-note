package com.doing.nemo.claims.entity.jsonb;

import com.doing.nemo.claims.entity.enumerated.ProcessingTypeEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProcessingAuthorityResponse implements Serializable {

    private static final long serialVersionUID = 816814485243599654L;

    @JsonProperty("processing_type")
    private ProcessingTypeEnum processingType;

    @JsonProperty("processing_date")
    private Date processingDate;

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    @JsonProperty("processing_authority_date")
    private Date processingAuthorityDate;

    public ProcessingTypeEnum getProcessingType() {
        return processingType;
    }

    public void setProcessingType(ProcessingTypeEnum processingType) {
        this.processingType = processingType;
    }

    public Date getProcessingDate() {
        if(processingDate == null){
            return null;
        }
        return (Date)processingDate.clone();
    }

    public void setProcessingDate(Date processingDate) {
        if(processingDate != null)
        {
            this.processingDate = (Date)processingDate.clone();
        } else {
            this.processingDate = null;
        }
    }

    public Date getProcessingAuthorityDate() {
        if(processingAuthorityDate == null){
            return null;
        }
        return (Date)processingAuthorityDate.clone();
    }

    public void setProcessingAuthorityDate(Date processingAuthorityDate) {
        if(processingAuthorityDate != null)
        {
            this.processingAuthorityDate = (Date)processingAuthorityDate.clone();
        } else {
            this.processingAuthorityDate = null;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        ProcessingAuthorityResponse that = (ProcessingAuthorityResponse) o;

        return new EqualsBuilder().append(processingType, that.processingType).append(processingDate, that.processingDate)
                .append(processingAuthorityDate, that.processingAuthorityDate).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(processingType).append(processingDate)
                .append(processingAuthorityDate).toHashCode();
    }
}
