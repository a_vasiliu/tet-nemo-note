package com.doing.nemo.claims.entity.esb.FleetManagerESB;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RoleEsb implements Serializable {

    private static final long serialVersionUID = -6458845458403357620L;

    @JsonProperty("role_id")
    private Long roleId;

    @JsonProperty("role")
    private String role;

    public RoleEsb() {
    }

    public RoleEsb(Long roleId, String role) {
        this.roleId = roleId;
        this.role = role;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "RoleEsb{" +
                "roleId=" + roleId +
                ", role='" + role + '\'' +
                '}';
    }
}
