package com.doing.nemo.claims.entity.jsonb.cai;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CaiDetails implements Serializable {

    private static final long serialVersionUID = 4987530269322813830L;

    @JsonProperty("condition_1")
    private Pair condition1;

    @JsonProperty("condition_2")
    private Pair condition2;

    @JsonProperty("condition_3")
    private Pair condition3;

    @JsonProperty("condition_4")
    private Pair condition4;

    @JsonProperty("condition_5")
    private Pair condition5;

    @JsonProperty("condition_6")
    private Pair condition6;

    @JsonProperty("condition_7")
    private Pair condition7;

    @JsonProperty("condition_8")
    private Pair condition8;

    @JsonProperty("condition_9")
    private Pair condition9;

    @JsonProperty("condition_10")
    private Pair condition10;

    @JsonProperty("condition_11")
    private Pair condition11;

    @JsonProperty("condition_12")
    private Pair condition12;

    @JsonProperty("condition_13")
    private Pair condition13;

    @JsonProperty("condition_14")
    private Pair condition14;

    @JsonProperty("condition_15")
    private Pair condition15;

    @JsonProperty("condition_16")
    private Pair condition16;

    @JsonProperty("condition_17")
    private Pair condition17;

    public CaiDetails() {
        condition1 = new Pair("1", false);
        condition2 = new Pair("2", false);
        condition3 = new Pair("3", false);
        condition4 = new Pair("4", false);
        condition5 = new Pair("5", false);
        condition6 = new Pair("6", false);
        condition7 = new Pair("7", false);
        condition8 = new Pair("8", false);
        condition9 = new Pair("9", false);
        condition10 = new Pair("10", false);
        condition11 = new Pair("11", false);
        condition12 = new Pair("12", false);
        condition13 = new Pair("13", false);
        condition14 = new Pair("14", false);
        condition15 = new Pair("15", false);
        condition16 = new Pair("16", false);
        condition17 = new Pair("17", false);
    }


    public static class Pair implements Serializable {

        private static final long serialVersionUID = 1048708565450840540L;

        private String name;
        private Boolean value = false;

        public Pair() {
        }

        public Pair(String name, Boolean value) {
            this.name = name;
            this.value = value;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Boolean getValue() {
            return value;
        }

        public void setValue(Boolean value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return "Pair{" +
                    "name='" + name + '\'' +
                    ", value=" + value +
                    '}';
        }
    }

    public CaiDetails(Pair condition1, Pair condition2, Pair condition3, Pair condition4, Pair condition5, Pair condition6, Pair condition7, Pair condition8, Pair condition9, Pair condition10, Pair condition11, Pair condition12, Pair condition13, Pair condition14, Pair condition15, Pair condition16, Pair condition17) {
        this.condition1 = condition1;
        this.condition2 = condition2;
        this.condition3 = condition3;
        this.condition4 = condition4;
        this.condition5 = condition5;
        this.condition6 = condition6;
        this.condition7 = condition7;
        this.condition8 = condition8;
        this.condition9 = condition9;
        this.condition10 = condition10;
        this.condition11 = condition11;
        this.condition12 = condition12;
        this.condition13 = condition13;
        this.condition14 = condition14;
        this.condition15 = condition15;
        this.condition16 = condition16;
        this.condition17 = condition17;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Pair getCondition1() {
        return condition1;
    }

    public void setCondition1(Pair condition1) {
        this.condition1 = condition1;
    }

    public Pair getCondition2() {
        return condition2;
    }

    public void setCondition2(Pair condition2) {
        this.condition2 = condition2;
    }

    public Pair getCondition3() {
        return condition3;
    }

    public void setCondition3(Pair condition3) {
        this.condition3 = condition3;
    }

    public Pair getCondition4() {
        return condition4;
    }

    public void setCondition4(Pair condition4) {
        this.condition4 = condition4;
    }

    public Pair getCondition5() {
        return condition5;
    }

    public void setCondition5(Pair condition5) {
        this.condition5 = condition5;
    }

    public Pair getCondition6() {
        return condition6;
    }

    public void setCondition6(Pair condition6) {
        this.condition6 = condition6;
    }

    public Pair getCondition7() {
        return condition7;
    }

    public void setCondition7(Pair condition7) {
        this.condition7 = condition7;
    }

    public Pair getCondition8() {
        return condition8;
    }

    public void setCondition8(Pair condition8) {
        this.condition8 = condition8;
    }

    public Pair getCondition9() {
        return condition9;
    }

    public void setCondition9(Pair condition9) {
        this.condition9 = condition9;
    }

    public Pair getCondition10() {
        return condition10;
    }

    public void setCondition10(Pair condition10) {
        this.condition10 = condition10;
    }

    public Pair getCondition11() {
        return condition11;
    }

    public void setCondition11(Pair condition11) {
        this.condition11 = condition11;
    }

    public Pair getCondition12() {
        return condition12;
    }

    public void setCondition12(Pair condition12) {
        this.condition12 = condition12;
    }

    public Pair getCondition13() {
        return condition13;
    }

    public void setCondition13(Pair condition13) {
        this.condition13 = condition13;
    }

    public Pair getCondition14() {
        return condition14;
    }

    public void setCondition14(Pair condition14) {
        this.condition14 = condition14;
    }

    public Pair getCondition15() {
        return condition15;
    }

    public void setCondition15(Pair condition15) {
        this.condition15 = condition15;
    }

    public Pair getCondition16() {
        return condition16;
    }

    public void setCondition16(Pair condition16) {
        this.condition16 = condition16;
    }

    public Pair getCondition17() {
        return condition17;
    }

    public void setCondition17(Pair condition17) {
        this.condition17 = condition17;
    }

    @Override
    public String toString() {
        return "CaiDetails{" +
                "condition1=" + condition1 +
                ", condition2=" + condition2 +
                ", condition3=" + condition3 +
                ", condition4=" + condition4 +
                ", condition5=" + condition5 +
                ", condition6=" + condition6 +
                ", condition7=" + condition7 +
                ", condition8=" + condition8 +
                ", condition9=" + condition9 +
                ", condition10=" + condition10 +
                ", condition11=" + condition11 +
                ", condition12=" + condition12 +
                ", condition13=" + condition13 +
                ", condition14=" + condition14 +
                ", condition15=" + condition15 +
                ", condition16=" + condition16 +
                ", condition17=" + condition17 +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        CaiDetails that = (CaiDetails) o;

        return new EqualsBuilder().append(condition1, that.condition1).append(condition2, that.condition2)
                .append(condition3, that.condition3).append(condition4, that.condition4)
                .append(condition5, that.condition5).append(condition6, that.condition6)
                .append(condition7, that.condition7).append(condition8, that.condition8)
                .append(condition9, that.condition9).append(condition10, that.condition10)
                .append(condition11, that.condition11).append(condition12, that.condition12)
                .append(condition13, that.condition13).append(condition14, that.condition14)
                .append(condition15, that.condition15).append(condition16, that.condition16).append(condition17, that.condition17).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(condition1)
                .append(condition2).append(condition3).append(condition4).append(condition5)
                .append(condition6).append(condition7).append(condition8).append(condition9)
                .append(condition10).append(condition11).append(condition12).append(condition13)
                .append(condition14).append(condition15).append(condition16).append(condition17).toHashCode();
    }
}
