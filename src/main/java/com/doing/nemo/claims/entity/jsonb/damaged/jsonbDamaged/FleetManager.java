package com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged;

import com.doing.nemo.claims.entity.esb.AddressEsb;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
@JsonIgnoreProperties(ignoreUnknown = true)
public class FleetManager implements Serializable {

    private static final long serialVersionUID = -4859906374586453108L;

    @JsonProperty("id")
    private Long id;

    @JsonProperty("identification")
    private String identification;

    @JsonProperty("official_registration")
    private String officialRegistration;

    @JsonProperty("first_name")
    private String firstName;

    @JsonProperty("last_name")
    private String lastName;

    @JsonProperty("title")
    private String title;

    @JsonProperty("email")
    private String email;

    @JsonProperty("phone_prefix")
    private String phonePrefix;

    @JsonProperty("phone")
    private String phone;

    @JsonProperty("secondary_phone_prefix")
    private String secondaryPhonePrefix;

    @JsonProperty("secondary_phone")
    private String secondaryPhone;

    @JsonProperty("sex")
    private String sex;

    @JsonProperty("customer_id")
    private Long customerId;

    @JsonProperty("main_address")
    private AddressEsb mainAddress;

    @JsonProperty("is_imported")
    private Boolean isImported = false;

    @JsonProperty("disable_notification")
    private Boolean disableNotification;

    public FleetManager() {
    }

    public FleetManager(Long id, String identification, String officialRegistration, String firstName, String lastName, String title, String email, String phonePrefix, String phone, String secondaryPhonePrefix, String secondaryPhone, String sex, Long customerId, AddressEsb mainAddress, Boolean isImported) {
        this.id = id;
        this.identification = identification;
        this.officialRegistration = officialRegistration;
        this.firstName = firstName;
        this.lastName = lastName;
        this.title = title;
        this.email = email;
        this.phonePrefix = phonePrefix;
        this.phone = phone;
        this.secondaryPhonePrefix = secondaryPhonePrefix;
        this.secondaryPhone = secondaryPhone;
        this.sex = sex;
        this.customerId = customerId;
        this.mainAddress = mainAddress;
        this.isImported = isImported;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public String getOfficialRegistration() {
        return officialRegistration;
    }

    public void setOfficialRegistration(String officialRegistration) {
        this.officialRegistration = officialRegistration;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhonePrefix() {
        return phonePrefix;
    }

    public void setPhonePrefix(String phonePrefix) {
        this.phonePrefix = phonePrefix;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSecondaryPhonePrefix() {
        return secondaryPhonePrefix;
    }

    public void setSecondaryPhonePrefix(String secondaryPhonePrefix) {
        this.secondaryPhonePrefix = secondaryPhonePrefix;
    }

    public String getSecondaryPhone() {
        return secondaryPhone;
    }

    public void setSecondaryPhone(String secondaryPhone) {
        this.secondaryPhone = secondaryPhone;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public AddressEsb getMainAddress() {
        return mainAddress;
    }

    public void setMainAddress(AddressEsb mainAddress) {
        this.mainAddress = mainAddress;
    }

    public void setIsImported(Boolean isImported){ this.isImported = isImported; }

    public Boolean getIsImported() { return isImported; }

    public void setDisableNotifiaction(Boolean disableNotification){
        if(disableNotification == null){
            this.disableNotification = false;
        }else{
            this.disableNotification = disableNotification;
        }
    }

    public Boolean isDisableNotification(){
        if(this.disableNotification == null){
            return false;
        }else{
            return this.disableNotification;
        }
    }

    @Override
    public String toString() {
        return "FleetManagerPersonalData{" +
                "id=" + id +
                ", identification='" + identification + '\'' +
                ", officialRegistration='" + officialRegistration + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", title='" + title + '\'' +
                ", email='" + email + '\'' +
                ", phonePrefix='" + phonePrefix + '\'' +
                ", phone='" + phone + '\'' +
                ", secondaryPhonePrefix='" + secondaryPhonePrefix + '\'' +
                ", secondaryPhone='" + secondaryPhone + '\'' +
                ", sex='" + sex + '\'' +
                ", customerId=" + customerId + '\'' +
                ", mainAddress=" + mainAddress +
                ", isImported=" + isImported +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        FleetManager that = (FleetManager) o;

        return new EqualsBuilder().append(id, that.id).append(identification, that.identification)
                .append(officialRegistration, that.officialRegistration).append(firstName, that.firstName)
                .append(lastName, that.lastName).append(title, that.title).append(email, that.email)
                .append(phonePrefix, that.phonePrefix).append(phone, that.phone).append(secondaryPhonePrefix, that.secondaryPhonePrefix)
                .append(secondaryPhone, that.secondaryPhone).append(sex, that.sex).append(customerId, that.customerId)
                .append(mainAddress, that.mainAddress).append(isImported, that.isImported).append(disableNotification, that.disableNotification).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(id).append(identification)
                .append(officialRegistration).append(firstName).append(lastName).append(title).append(email)
                .append(phonePrefix).append(phone).append(secondaryPhonePrefix).append(secondaryPhone).append(sex)
                .append(customerId).append(mainAddress).append(isImported).append(disableNotification).toHashCode();
    }
}
