package com.doing.nemo.claims.entity.settings;

import com.doing.nemo.claims.entity.enumerated.ClaimsRepairEnum;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "motivation", uniqueConstraints = @UniqueConstraint(
        name = "uc_motivation",
        columnNames = {
                "answer_type", "description"
        })
)
public class MotivationEntity implements Serializable {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )

    @Type(type = "org.hibernate.type.PostgresUUIDType")
    private UUID id;

    @Column(name = "answer_type")
    private String answerType;

    @Column(name = "description")
    private String description;

    @Enumerated(EnumType.STRING)
    @Column(name = "type_complaint")
    private ClaimsRepairEnum typeComplaint;

    @Column(name = "is_active")
    private Boolean isActive;

    public MotivationEntity() {

    }

    public MotivationEntity(String answerType, String description, ClaimsRepairEnum typeComplaint, Boolean isActive) {
        this.answerType = answerType;
        this.description = description;
        this.typeComplaint = typeComplaint;
        this.isActive = isActive;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getAnswerType() {
        return answerType;
    }

    public void setAnswerType(String answerType) {
        this.answerType = answerType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public ClaimsRepairEnum getTypeComplaint() {
        return typeComplaint;
    }

    public void setTypeComplaint(ClaimsRepairEnum typeComplaint) {
        this.typeComplaint = typeComplaint;
    }

    @Override
    public String toString() {
        return "MotivationEntity{" +
                "id=" + id +
                ", answerType='" + answerType + '\'' +
                ", description='" + description + '\'' +
                ", typeComplaint=" + typeComplaint +
                ", isActive=" + isActive +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MotivationEntity that = (MotivationEntity) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getAnswerType(), that.getAnswerType()) &&
                Objects.equals(getDescription(), that.getDescription()) &&
                getTypeComplaint() == that.getTypeComplaint() &&
                Objects.equals(getActive(), that.getActive());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getAnswerType(), getDescription(), getTypeComplaint(), getActive());
    }
}
