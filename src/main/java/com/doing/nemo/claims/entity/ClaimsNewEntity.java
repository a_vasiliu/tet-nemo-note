package com.doing.nemo.claims.entity;

import com.doing.nemo.claims.entity.claims.*;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsFlowEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.ComplaintModEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.ComplaintPropertyEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.jsonb.*;
import com.doing.nemo.claims.entity.jsonb.cai.Cai;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.AdditionalCosts;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.ImpactPoint;
import com.doing.nemo.claims.entity.jsonb.dataType.*;
import com.doing.nemo.claims.entity.jsonb.forms.Forms;
import com.doing.nemo.claims.entity.settings.AntiTheftServiceEntity;
import com.doing.nemo.claims.listener.ClaimsNewEntityListener;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.*;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "hibernate_lazy_initializer", "handler"})
@DynamicInsert
@EntityListeners(ClaimsNewEntityListener.class)
@Table(name = "claims_new")
@TypeDefs({
        @TypeDef(name = "JsonDataCaiType", typeClass = JsonDataCaiType.class),
        @TypeDef(name = "JsonDataDeponentType", typeClass = JsonDataDeponentType.class),
        @TypeDef(name = "JsonDataFormsType", typeClass = JsonDataFormsType.class),
        @TypeDef(name = "JsonDataHistoricalType", typeClass = JsonDataHistoricalType.class),
        @TypeDef(name = "JsonDataAdditionalCostsType", typeClass = JsonDataAdditionalCostsType.class),
        @TypeDef(name = "JsonDataImpactPointType", typeClass = JsonDataImpactPointType.class),
        @TypeDef(name = "JsonDataWoundedType", typeClass = JsonDataWoundedType.class),
        @TypeDef(name = "JsonDataExemptionType", typeClass = JsonDataExemptionType.class),
        @TypeDef(name = "JsonDataWoundedType", typeClass = JsonDataWoundedType.class),

})
public class ClaimsNewEntity implements Serializable {

    @Id
    private String id;

    @Column(name = "created_at")
    private Instant createdAt;

    @Column(name = "updated_at")
    private Instant updateAt;

    @Column(name = "status_updated_at")
    private Instant statusUpdatedAt;

    //region COMPLAINT
    @Column(name = "client_id")
    private String clientId;

    @Column(name = "plate")
    private String plate;

    @Column(name = "claim_locator")
    private String claimLocator;

    @Column(name = "activation")
    private String activation;

    @Column(name = "property")
    @Enumerated(EnumType.STRING)
    private ComplaintPropertyEnum property;

    @Column(name = "mod")
    @Enumerated(EnumType.STRING)
    private ComplaintModEnum mod;

    @Column(name = "notification")
    private String notification;

    @Column(name = "quote")
    private Boolean quote;
    //endregion COMPLAINT

    //region COMPLAINT->DATA ACCIDENT
    @Column(name = "type_accident")
    @Enumerated(EnumType.STRING)
    private DataAccidentTypeAccidentEnum typeAccident;

    @Column(name = "responsible")
    private Boolean responsible;

    @Column(name = "date_accident")
    private Instant dateAccident;

    @Column(name = "happened_abroad")
    private Boolean happenedAbroad;

    @Column(name = "damage_to_objects")
    private Boolean damageToObjects;

    @Column(name = "damage_to_vehicles")
    private Boolean damageToVehicles;

    //Da eliminare probabilmente
    @Column(name = "old_motorcycle_plates")
    private String oldMotorcyclePlates;

    @Column(name = "intervention_authority")
    private Boolean interventionAuthority;

    @Column(name = "police")
    private Boolean police;

    @Column(name = "cc")
    private Boolean cc;

    @Column(name = "vvuu")
    private Boolean vvuu;

    @Column(name = "authority_data")
    private String authorityData;

    @Column(name = "witness_description")
    private String witnessDescription;

    @Column(name = "recoverability")
    private Boolean recoverability;

    @Column(name = "recoverability_percent")
    private Double recoverabilityPercent;

    @Column(name = "incomplete_motivation")
    private String incompleteMotivation;

    @Column(name = "is_robbery")
    private Boolean isRobbery;

    @Column(name = "center_notified")
    private Boolean centerNotified;

    @Column(name = "happened_on_center")
    private Boolean happenedOnCenter;

    @Column(name = "provider_code")
    private String providerCode;

    @Column(name = "theft_description")
    private String theftDescription;

    @Column(name = "is_with_counterparty_internal")
    private Boolean isWithCounterpartyInternal;

    @Column(name = "is_with_counterparty")
    private Boolean isWithCounterparty;
    //endregion COMPLAINT->DATA ACCIDENT

    //region COMPLAINT->DATA ACCIDENT->CLAIM ADDRESS
    @Column(name="claim_address_street")
    private String claimAddressStreet;

    @Column(name="claim_address_street_nr")
    private String claimAddressStreetNr;

    @Column(name="claim_address_zip")
    private String claimAddressStreetZip;

    @Column(name="claim_address_locality")
    private String claimAddressStreetLocality;

    @Column(name="claim_address_province")
    private String claimAddressStreetProvince;

    @Column(name="claim_address_region")
    private String claimAddressStreetRegion;

    @Column(name="claim_address_state")
    private String claimAddressStreetState;

    @Column(name="claim_address_formatted")
    private String claimAddressFormatted;
    //endregion COMPLAINT->DATA ACCIDENT->ADDRESS

    //region COMPLAINT->DATA ACCIDENT->FROM COMPANY
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "claim")
    @JsonBackReference(value = "claimsFromCompanyEntity")
    private ClaimsFromCompanyEntity claimsFromCompanyEntity;
    //endregion COMPLAINT->DATA ACCIDENT->FROM COMPANY

    //region COMPLAINT->DATA ACCIDENT->ENTRUSTED
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "claim")
    private ClaimsEntrustedEntity claimsEntrustedEntity;
    //endregion COMPLAINT->DATA ACCIDENT->ENTRUSTED

    //region DAMAGED
    @Column(name = "is_cai_signed")
    private Boolean isCaiSigned;

    @Type(type = "JsonDataImpactPointType")
    @Column(name = "impact_point")
    private ImpactPoint impactPoint;

    @Type(type = "JsonDataAdditionalCostsType")
    @Column(name = "additional_costs")
    private List<AdditionalCosts> additionalCosts;
    //endregion DAMAGED

    //region DAMAGED->CONTRACT
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "claim")
    private ClaimsDamagedContractEntity claimsDamagedContractEntity;
    //endregion DAMAGED->CONTRACT

    //region DAMAGED->CUSTOMER
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "claim")
    private ClaimsDamagedCustomerEntity claimsDamagedCustomerEntity;
    //endregion DAMAGED->CUSTOMER

    //region DAMAGED->DRIVER
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "claim")
    private ClaimsDamagedDriverEntity claimsDamagedDriverEntity;
    //endregion DAMAGED->DRIVER

    //region DAMAGED->VEHICLE
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "claim")
    private ClaimsDamagedVehicleEntity claimsDamagedVehicleEntity;
    //endregion DAMAGED->VEHICLE


    //region DAMAGED->INSURANCE INFO
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "claim")
    private ClaimsDamagedInsuranceInfoEntity claimsDamagedInsuranceInfoEntity;
    //endregion DAMAGED->INSURANCE INFO

    //region DAMAGED->FLEET MANAGER
    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "claims_to_claims_damaged_fleet_manager",
            joinColumns = @JoinColumn(name = "claims_id"),
            inverseJoinColumns = @JoinColumn(name = "damaged_fleet_manager_id")
    )
    private List<ClaimsDamagedFleetManagerEntity> claimsDamagedFleetManagerEntityList;
    //endregion DAMAGED->FLEET MANAGER


        //region DAMAGED->ANTI THEFT SERVICE
        @ManyToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "damaged_anti_theft_service_id", nullable = true)
        @JsonBackReference(value = "damaged_anti_theft_service_id")
        private AntiTheftServiceEntity antiTheftServiceEntity;
        //endregion DAMAGED->ANTI THEFT SERVICE

        //region DAMAGED->REGISTRY LIST
        @OneToMany(
                cascade = CascadeType.ALL
        )
        @JoinColumn(name = "claim_id")
        @Column(name = "registry")
        @JsonManagedReference(value = "claims-damaged-registry")
        private List<ClaimsDamagedRegistryEntity> claimsDamagedRegistryEntityList;
        //endregion DAMAGED->REGISTRY LIST

    //region REFUND
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "claim")
    private ClaimsRefundEntity claimsRefundEntity;
    //endregion REFUND

    //region THEFT
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "claim")
    private ClaimsTheftEntity claimsTheftEntity;
    //endregion THEFT

    //region METADATA
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "claim")
    private ClaimsMetadataEntity claimsMetadataEntity;
    //endregion THEFT

    //region AUTHORITY
    /*@ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "claims_to_claims_authority",
            joinColumns = @JoinColumn(name = "claims_id"),
            inverseJoinColumns = @JoinColumn(name = "authority_id"))*/

    @OneToMany(mappedBy = "claims", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference(value = "claims-authority-embedded-entities")
    private List<ClaimsAuthorityEmbeddedEntity> claimsAuthorityEmbeddedEntities;
    //endregion AUTHORITY

    @Column(name = "practice_id", unique = true, nullable = false , updatable = false)
    private Long practiceId;

    @Column(name = "practice_manager")
    private String practiceManager;

    @Column(name = "pai_comunication")
    private Boolean paiComunication;

    @Column(name = "forced")
    private Boolean forced;

    @Column(name = "in_evidence")
    private Boolean inEvidence;

    @Column(name = "user_entity")
    private String userId;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private ClaimsStatusEnum status;

    @Column(name = "motivation")
    private String motivation;

    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private ClaimsFlowEnum type;

    @Column(name = "documentation")
    private Boolean isCompleteDocumentation;



    @Type(type = "JsonDataFormsType")
    @Column(name = "forms")
    private Forms forms;



    @Type(type = "JsonDataCaiType")
    @Column(name = "cai_details")
    private Cai caiDetails;

    @OneToMany(
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @JoinColumn(name = "claims_id")
    @Column(name = "counterparty")
    private List<CounterpartyNewEntity> counterparts;

    @Type(type = "JsonDataDeponentType")
    private List<Deponent> deponentList;

    @Type(type = "JsonDataWoundedType")
    private List<Wounded> woundedList;

    @OneToMany(
            cascade = CascadeType.ALL/*,
            orphanRemoval = true*/
    )
    @JoinColumn(name = "claims_id")
    @Column(name = "notes")
    private List<NoteEntity> notes;

    @Type(type = "JsonDataHistoricalType")
    @Column(name = "historical")
    private List<Historical> historical;

    @Type(type = "JsonDataExemptionType")
    @Column(name = "exemption")
    private Exemption exemption;

    @Column(name = "id_saleforce")
    private Long idSaleforce;

    @Column(name = "with_continuation")
    private Boolean withContinuation;



    @Column(name="is_read_msa")
    private Boolean isRead;



    @Column(name = "is_authority_linkable")
    private Boolean isAuthorityLinkable;

    @Column(name = "po_variation")
    private Boolean poVariation;

    @Column(name = "legal_comunication")
    private Boolean legalComunication;

    @Column(name = "total_penalty")
    private Long totalPenalty;

    @Column(name="is_read_acclaims")
    private Boolean isReadAcclaims;

    @Column(name = "is_migrated")
    private Boolean isMigrated;


    @OneToMany(
           mappedBy = "claim", cascade = CascadeType.ALL,orphanRemoval = true
    )
    @JsonManagedReference(value = "anti-theft-request-new-entities")
    private List<AntiTheftRequestNewEntity> antiTheftRequestEntities;


    public ClaimsNewEntity() {
    }


    public Boolean getMigrated() {
        return isMigrated;
    }

    public void setMigrated(Boolean migrated) {
        isMigrated = migrated;
    }

    public Boolean getReadAcclaims() {
        return isReadAcclaims;
    }

    public void setReadAcclaims(Boolean readAcclaims) {
        isReadAcclaims = readAcclaims;
    }

    public Long getTotalPenalty() {
        return totalPenalty;
    }

    public void setTotalPenalty(Long totalPenalty) {
        this.totalPenalty = totalPenalty;
    }

    public Boolean getLegalComunication() {
        return legalComunication;
    }

    public void setLegalComunication(Boolean legalComunication) {
        this.legalComunication = legalComunication;
    }

    public Boolean getAuthorityLinkable() {
        return isAuthorityLinkable;
    }

    public void setAuthorityLinkable(Boolean authorityLinkable) {
        isAuthorityLinkable = authorityLinkable;
    }



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public Instant getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Instant updateAt) {
        this.updateAt = updateAt;
    }

    public ClaimsStatusEnum getStatus() {
        return status;
    }

    public void setStatus(ClaimsStatusEnum status) {
        this.status = status;
    }

    public ClaimsFlowEnum getType() {
        return type;
    }

    @JsonIgnore
    public Boolean getCompleteDocumentation() {
        return isCompleteDocumentation;
    }

    public void setCompleteDocumentation(Boolean completeDocumentation) {
        isCompleteDocumentation = completeDocumentation;
    }

    public void setType(ClaimsFlowEnum type) {
        this.type = type;
    }



    public Forms getForms() {
        return forms;
    }

    public void setForms(Forms forms) {
        this.forms = forms;
    }


    public Cai getCaiDetails() {
        return caiDetails;
    }

    public void setCaiDetails(Cai caiDetails) {
        this.caiDetails = caiDetails;
    }

    public List<CounterpartyNewEntity> getCounterparts() {
        return counterparts;
    }

    public void setCounterparts(List<CounterpartyNewEntity> counterparts) {
        this.counterparts = counterparts;
    }

    @JsonIgnore
    public String getPracticeManager() {
        return practiceManager;
    }

    public void setPracticeManager(String practiceManager) {
        this.practiceManager = practiceManager;
    }

    @JsonIgnore
    public String getUserEntity() {
        return userId;
    }

    public void setUserEntity(String userId) {
        this.userId = userId;
    }

    public Long getPracticeId() {
        return practiceId;
    }

    public void setPracticeId(Long practiceId) {
        this.practiceId = practiceId;
    }

    public List<Deponent> getDeponentList() {
        if(deponentList == null){
            return null;
        }
        return new ArrayList<>(deponentList);
    }

    public void setDeponentList(List<Deponent> deponentList) {
        if(deponentList != null)
        {
            this.deponentList = new ArrayList<>(deponentList);
        } else {
            this.deponentList = null;
        }
    }

    public List<Wounded> getWoundedList() {
        if(woundedList == null){
            return null;
        }
        return new ArrayList<>(woundedList);
    }

    public void setWoundedList(List<Wounded> woundedList) {
        if(woundedList != null)
        {
            this.woundedList = new ArrayList<>(woundedList);
        } else {
            this.woundedList = null;
        }
    }

    public List<NoteEntity> getNotes() {
        if(notes == null){
            return null;
        }
        return new ArrayList<>(notes);
    }

    public void setNotes(List<NoteEntity> notes) {
        if(notes != null)
        {
            this.notes = new ArrayList<>(notes);
            for (NoteEntity note: this.notes) {
                note.setClaims(this);
            }
        } else {
            this.notes = null;
        }
    }

    public void addNotes(NoteEntity note) {

        if (this.notes == null) {
            setNotes(new LinkedList<>());
        }

        this.notes.add(note);

    }

    public List<Historical> getHistorical() {
        if(historical == null){
            return null;
        }
        return new ArrayList<>(historical);
    }

    public void setHistorical(List<Historical> historical) {
        if(historical != null)
        {
            this.historical = new ArrayList<>(historical);
        } else {
            this.historical = null;
        }
    }

    public void addHistorical(Historical historical) {
        if (this.historical == null) {
            setHistorical(new LinkedList<>());
        }
        this.historical.add(historical);
    }

    public Boolean getPaiComunication() {
        return paiComunication;
    }

    public void setPaiComunication(Boolean paiComunication) {
        if (paiComunication == null)
            paiComunication = false;
        this.paiComunication = paiComunication;
    }

    public Boolean getForced() {
        return forced;
    }

    public void setForced(Boolean forced) {
        if (forced == null)
            forced = false;
        this.forced = forced;
    }

    public Boolean getInEvidence() {
        return inEvidence;
    }

    public void setInEvidence(Boolean inEvidence) {
        if (inEvidence == null)
            inEvidence = false;
        this.inEvidence = inEvidence;
    }


    public Boolean getWithCounterparty() {
        return isWithCounterparty;
    }

    public void setWithCounterparty(Boolean withCounterparty) {
        if (withCounterparty == null)
            withCounterparty = false;
        isWithCounterparty = withCounterparty;
    }

    public Long getIdSaleforce() {
        return idSaleforce;
    }

    public void setIdSaleforce(Long idSaleforce) {
        this.idSaleforce = idSaleforce;
    }

    public Boolean getWithContinuation() {
        return withContinuation;
    }

    public Exemption getExemption() {
        return exemption;
    }

    public void setExemption(Exemption exemption) {
        this.exemption = exemption;
    }

    public void setWithContinuation(Boolean withContinuation) {
        this.withContinuation = withContinuation;
    }



    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }


    public String getMotivation() {
        return motivation;
    }

    public void setMotivation(String motivation) {
        this.motivation = motivation;
    }

    public Boolean getRead() {
        return isRead;
    }

    public void setRead(Boolean read) {
        isRead = read;
    }



    public Boolean getPoVariation() {
        return poVariation;
    }

    public void setPoVariation(Boolean poVariation) {
        this.poVariation = poVariation;
    }

    public List<AntiTheftRequestNewEntity> getAntiTheftRequestEntities() {
        return antiTheftRequestEntities;
    }

    public void setAntiTheftRequestEntities(List<AntiTheftRequestNewEntity> antiTheftRequestEntities) {
        this.antiTheftRequestEntities = antiTheftRequestEntities;
    }

    public Instant getStatusUpdatedAt() {
        return statusUpdatedAt;
    }

    public void setStatusUpdatedAt(Instant statusUpdatedAt) {
        this.statusUpdatedAt = statusUpdatedAt;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public String getLocator() {
        return claimLocator;
    }

    public void setLocator(String locator) {
        this.claimLocator = locator;
    }

    public String getActivation() {
        return activation;
    }

    public void setActivation(String activation) {
        this.activation = activation;
    }

    public ComplaintPropertyEnum getProperty() {
        return property;
    }

    public void setProperty(ComplaintPropertyEnum property) {
        this.property = property;
    }

    public ComplaintModEnum getMod() {
        return mod;
    }

    public void setMod(ComplaintModEnum mod) {
        this.mod = mod;
    }

    public String getNotification() {
        return notification;
    }

    public void setNotification(String notification) {
        this.notification = notification;
    }

    public Boolean getQuote() {
        return quote;
    }

    public void setQuote(Boolean quote) {
        this.quote = quote;
    }

    public DataAccidentTypeAccidentEnum getTypeAccident() {
        return typeAccident;
    }

    public void setTypeAccident(DataAccidentTypeAccidentEnum typeAccident) {
        this.typeAccident = typeAccident;
    }

    public Boolean getResponsible() {
        return responsible;
    }

    public void setResponsible(Boolean responsible) {
        this.responsible = responsible;
    }

    public Instant getDateAccident() {
        return dateAccident;
    }

    public void setDateAccident(Instant dateAccident) {
        this.dateAccident = dateAccident;
    }

    public Boolean getHappenedAbroad() {
        return happenedAbroad;
    }

    public void setHappenedAbroad(Boolean happenedAbroad) {
        this.happenedAbroad = happenedAbroad;
    }

    public Boolean getDamageToObjects() {
        return damageToObjects;
    }

    public void setDamageToObjects(Boolean damageToObjects) {
        this.damageToObjects = damageToObjects;
    }

    public Boolean getDamageToVehicles() {
        return damageToVehicles;
    }

    public void setDamageToVehicles(Boolean damageToVehicles) {
        this.damageToVehicles = damageToVehicles;
    }

    public String getOldMotorcyclePlates() {
        return oldMotorcyclePlates;
    }

    public void setOldMotorcyclePlates(String oldMotorcyclePlates) {
        this.oldMotorcyclePlates = oldMotorcyclePlates;
    }

    public Boolean getInterventionAuthority() {
        return interventionAuthority;
    }

    public void setInterventionAuthority(Boolean interventionAuthority) {
        this.interventionAuthority = interventionAuthority;
    }

    public Boolean getPolice() {
        return police;
    }

    public void setPolice(Boolean police) {
        this.police = police;
    }

    public Boolean getCc() {
        return cc;
    }

    public void setCc(Boolean cc) {
        this.cc = cc;
    }

    public Boolean getVvuu() {
        return vvuu;
    }

    public void setVvuu(Boolean vvuu) {
        this.vvuu = vvuu;
    }

    public String getAuthorityData() {
        return authorityData;
    }

    public void setAuthorityData(String authorityData) {
        this.authorityData = authorityData;
    }

    public String getWitnessDescription() {
        return witnessDescription;
    }

    public void setWitnessDescription(String witnessDescription) {
        this.witnessDescription = witnessDescription;
    }

    public Boolean getRecoverability() {
        return recoverability;
    }

    public void setRecoverability(Boolean recoverability) {
        this.recoverability = recoverability;
    }

    public Double getRecoverabilityPercent() {
        return recoverabilityPercent;
    }

    public void setRecoverabilityPercent(Double recoverabilityPercent) {
        this.recoverabilityPercent = recoverabilityPercent;
    }

    public String getIncompleteMotivation() {
        return incompleteMotivation;
    }

    public void setIncompleteMotivation(String incompleteMotivation) {
        this.incompleteMotivation = incompleteMotivation;
    }

    public Boolean getRobbery() {
        return isRobbery;
    }

    public void setRobbery(Boolean robbery) {
        isRobbery = robbery;
    }

    public Boolean getCenterNotified() {
        return centerNotified;
    }

    public void setCenterNotified(Boolean centerNotified) {
        this.centerNotified = centerNotified;
    }

    public Boolean getHappenedOnCenter() {
        return happenedOnCenter;
    }

    public void setHappenedOnCenter(Boolean happenedOnCenter) {
        this.happenedOnCenter = happenedOnCenter;
    }

    public String getProviderCode() {
        return providerCode;
    }

    public void setProviderCode(String providerCode) {
        this.providerCode = providerCode;
    }

    public String getTheftDescription() {
        return theftDescription;
    }

    public void setTheftDescription(String theftDescription) {
        this.theftDescription = theftDescription;
    }

    public String getClaimAddressStreet() {
        return claimAddressStreet;
    }

    public void setClaimAddressStreet(String claimAddressStreet) {
        this.claimAddressStreet = claimAddressStreet;
    }

    public String getClaimAddressStreetNr() {
        return claimAddressStreetNr;
    }

    public void setClaimAddressStreetNr(String claimAddressStreetNr) {
        this.claimAddressStreetNr = claimAddressStreetNr;
    }

    public String getClaimAddressStreetZip() {
        return claimAddressStreetZip;
    }

    public void setClaimAddressStreetZip(String claimAddressStreetZip) {
        this.claimAddressStreetZip = claimAddressStreetZip;
    }

    public String getClaimAddressStreetLocality() {
        return claimAddressStreetLocality;
    }

    public void setClaimAddressStreetLocality(String claimAddressStreetLocality) {
        this.claimAddressStreetLocality = claimAddressStreetLocality;
    }

    public String getClaimAddressStreetProvince() {
        return claimAddressStreetProvince;
    }

    public void setClaimAddressStreetProvince(String claimAddressStreetProvince) {
        this.claimAddressStreetProvince = claimAddressStreetProvince;
    }

    public String getClaimAddressStreetRegion() {
        return claimAddressStreetRegion;
    }

    public void setClaimAddressStreetRegion(String claimAddressStreetRegion) {
        this.claimAddressStreetRegion = claimAddressStreetRegion;
    }

    public String getClaimAddressStreetState() {
        return claimAddressStreetState;
    }

    public void setClaimAddressStreetState(String claimAddressStreetState) {
        this.claimAddressStreetState = claimAddressStreetState;
    }

    public String getClaimAddressFormatted() {
        return claimAddressFormatted;
    }

    public void setClaimAddressFormatted(String claimAddressFormatted) {
        this.claimAddressFormatted = claimAddressFormatted;
    }

    public String getClaimLocator() {
        return claimLocator;
    }

    public void setClaimLocator(String claimLocator) {
        this.claimLocator = claimLocator;
    }

    public ClaimsFromCompanyEntity getClaimsFromCompanyEntity() {
        return claimsFromCompanyEntity;
    }

    public void setClaimsFromCompanyEntity(ClaimsFromCompanyEntity claimsFromCompanyEntity) {
        this.claimsFromCompanyEntity = claimsFromCompanyEntity;
        if(claimsFromCompanyEntity != null){
            claimsFromCompanyEntity.setClaim(this);
            claimsFromCompanyEntity.setId(this.getId());
        }
    }

    public ClaimsEntrustedEntity getClaimsEntrustedEntity() {
        return claimsEntrustedEntity;
    }

    public void setClaimsEntrustedEntity(ClaimsEntrustedEntity claimsEntrustedEntity) {
        this.claimsEntrustedEntity = claimsEntrustedEntity;
        if(claimsEntrustedEntity != null){
            claimsEntrustedEntity.setClaim(this);
            claimsEntrustedEntity.setId(this.getId());
        }
    }

    public Boolean getCaiSigned() {
        return isCaiSigned;
    }

    public void setCaiSigned(Boolean caiSigned) {
        isCaiSigned = caiSigned;
    }

    public ImpactPoint getImpactPoint() {
        return impactPoint;
    }

    public void setImpactPoint(ImpactPoint impactPoint) {
        this.impactPoint = impactPoint;
    }

    public List<AdditionalCosts> getAdditionalCosts() {
        return additionalCosts;
    }

    public void setAdditionalCosts(List<AdditionalCosts> additionalCosts) {
        this.additionalCosts = additionalCosts;
    }

    public ClaimsDamagedContractEntity getClaimsDamagedContractEntity() {
        return claimsDamagedContractEntity;
    }

    public void setClaimsDamagedContractEntity(ClaimsDamagedContractEntity claimsDamagedContractEntity) {
        this.claimsDamagedContractEntity = claimsDamagedContractEntity;
        if(claimsDamagedContractEntity != null){
            claimsDamagedContractEntity.setClaim(this);
            claimsDamagedContractEntity.setId(this.getId());
        }
    }

    public ClaimsDamagedCustomerEntity getClaimsDamagedCustomerEntity() {
        return claimsDamagedCustomerEntity;
    }

    public void setClaimsDamagedCustomerEntity(ClaimsDamagedCustomerEntity claimsDamagedCustomerEntity) {
        this.claimsDamagedCustomerEntity = claimsDamagedCustomerEntity;
        if(claimsDamagedCustomerEntity != null){
            claimsDamagedCustomerEntity.setClaim(this);
            claimsDamagedCustomerEntity.setId(this.getId());
        }
    }

    public ClaimsDamagedDriverEntity getClaimsDamagedDriverEntity() {
        return claimsDamagedDriverEntity;
    }

    public void setClaimsDamagedDriverEntity(ClaimsDamagedDriverEntity claimsDamagedDriverEntity) {
        this.claimsDamagedDriverEntity = claimsDamagedDriverEntity;
        if(claimsDamagedDriverEntity != null){
            claimsDamagedDriverEntity.setClaim(this);
            claimsDamagedDriverEntity.setId(this.getId());
        }
    }

    public ClaimsDamagedVehicleEntity getClaimsDamagedVehicleEntity() {
        return claimsDamagedVehicleEntity;
    }

    public void setClaimsDamagedVehicleEntity(ClaimsDamagedVehicleEntity claimsDamagedVehicleEntity) {
        this.claimsDamagedVehicleEntity = claimsDamagedVehicleEntity;
        if(claimsDamagedVehicleEntity != null){
            claimsDamagedVehicleEntity.setClaim(this);
            claimsDamagedVehicleEntity.setId(this.getId());
        }
    }


    public List<ClaimsDamagedFleetManagerEntity> getClaimsDamagedFleetManagerEntityList() {
      return claimsDamagedFleetManagerEntityList;
    }

    public void setClaimsDamagedFleetManagerEntityList(List<ClaimsDamagedFleetManagerEntity> claimsDamagedFleetManagerEntityList) {
      this.claimsDamagedFleetManagerEntityList = claimsDamagedFleetManagerEntityList;
    }

     public AntiTheftServiceEntity getAntiTheftServiceEntity() {
         return antiTheftServiceEntity;
     }

     public void setAntiTheftServiceEntity(AntiTheftServiceEntity antiTheftServiceEntity) {
         this.antiTheftServiceEntity = antiTheftServiceEntity;
     }

    public List<ClaimsDamagedRegistryEntity> getClaimsDamagedRegistryEntityList() {
        return claimsDamagedRegistryEntityList;
    }

    public void setClaimsDamagedRegistryEntityList(List<ClaimsDamagedRegistryEntity> claimsDamagedRegistryEntityList) {
        this.claimsDamagedRegistryEntityList = claimsDamagedRegistryEntityList;
    }

    public ClaimsRefundEntity getClaimsRefundEntity() {
        return claimsRefundEntity;
    }

    public void setClaimsRefundEntity(ClaimsRefundEntity claimsRefundEntity) {
        this.claimsRefundEntity = claimsRefundEntity;
        if(claimsRefundEntity != null){
            claimsRefundEntity.setClaim(this);
            claimsRefundEntity.setId(this.getId());
        }
    }

    public ClaimsTheftEntity getClaimsTheftEntity() {
        return claimsTheftEntity;
    }

    public void setClaimsTheftEntity(ClaimsTheftEntity claimsTheftEntity) {
        this.claimsTheftEntity = claimsTheftEntity;
        if(claimsTheftEntity != null){
            claimsTheftEntity.setClaim(this);
            claimsTheftEntity.setId(this.getId());
        }
    }

    public List<ClaimsAuthorityEmbeddedEntity> getClaimsAuthorityEmbeddedEntities() {
        return claimsAuthorityEmbeddedEntities;
    }

    public void setClaimsAuthorityEmbeddedEntities(List<ClaimsAuthorityEmbeddedEntity> claimsAuthorityEmbeddedEntities) {
        if(claimsAuthorityEmbeddedEntities != null) {
            this.claimsAuthorityEmbeddedEntities = claimsAuthorityEmbeddedEntities;
        }else{
            this.claimsAuthorityEmbeddedEntities = new LinkedList<>();
        }

    }

    public ClaimsMetadataEntity getClaimsMetadataEntity() {
        return claimsMetadataEntity;
    }

    public void setClaimsMetadataEntity(ClaimsMetadataEntity claimsMetadataEntity) {
        this.claimsMetadataEntity = claimsMetadataEntity;
        if(claimsMetadataEntity != null){
            claimsMetadataEntity.setClaim(this);
            claimsMetadataEntity.setId(this.getId());
        }
    }


    public Boolean getWithCounterpartyInternal() {
        return isWithCounterpartyInternal;
    }

    public void setWithCounterpartyInternal(Boolean withCounterpartyInternal) {
        isWithCounterpartyInternal = withCounterpartyInternal;
    }

    public ClaimsDamagedInsuranceInfoEntity getClaimsDamagedInsuranceInfoEntity() {
        return claimsDamagedInsuranceInfoEntity;
    }

    public void setClaimsDamagedInsuranceInfoEntity(ClaimsDamagedInsuranceInfoEntity claimsDamagedInsuranceInfoEntity) {
        this.claimsDamagedInsuranceInfoEntity = claimsDamagedInsuranceInfoEntity;
        if(claimsDamagedInsuranceInfoEntity != null){
            claimsDamagedInsuranceInfoEntity.setClaim(this);
            claimsDamagedInsuranceInfoEntity.setId(this.getId());
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ClaimsNewEntity)) return false;
        ClaimsNewEntity that = (ClaimsNewEntity) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getCreatedAt(), that.getCreatedAt()) &&
                Objects.equals(getUpdateAt(), that.getUpdateAt()) &&
                Objects.equals(getStatusUpdatedAt(), that.getStatusUpdatedAt()) &&
                Objects.equals(getClientId(), that.getClientId()) &&
                Objects.equals(getPlate(), that.getPlate()) &&
                Objects.equals(getClaimLocator(), that.getClaimLocator()) &&
                Objects.equals(getActivation(), that.getActivation()) &&
                getProperty() == that.getProperty() &&
                getMod() == that.getMod() &&
                Objects.equals(getNotification(), that.getNotification()) &&
                Objects.equals(getQuote(), that.getQuote()) &&
                getTypeAccident() == that.getTypeAccident() &&
                Objects.equals(getResponsible(), that.getResponsible()) &&
                Objects.equals(getDateAccident(), that.getDateAccident()) &&
                Objects.equals(getHappenedAbroad(), that.getHappenedAbroad()) &&
                Objects.equals(getDamageToObjects(), that.getDamageToObjects()) &&
                Objects.equals(getDamageToVehicles(), that.getDamageToVehicles()) &&
                Objects.equals(getOldMotorcyclePlates(), that.getOldMotorcyclePlates()) &&
                Objects.equals(getInterventionAuthority(), that.getInterventionAuthority()) &&
                Objects.equals(getPolice(), that.getPolice()) &&
                Objects.equals(getCc(), that.getCc()) &&
                Objects.equals(getVvuu(), that.getVvuu()) &&
                Objects.equals(getAuthorityData(), that.getAuthorityData()) &&
                Objects.equals(getWitnessDescription(), that.getWitnessDescription()) &&
                Objects.equals(getRecoverability(), that.getRecoverability()) &&
                Objects.equals(getRecoverabilityPercent(), that.getRecoverabilityPercent()) &&
                Objects.equals(getIncompleteMotivation(), that.getIncompleteMotivation()) &&
                Objects.equals(isRobbery, that.isRobbery) &&
                Objects.equals(getCenterNotified(), that.getCenterNotified()) &&
                Objects.equals(getHappenedOnCenter(), that.getHappenedOnCenter()) &&
                Objects.equals(getProviderCode(), that.getProviderCode()) &&
                Objects.equals(getTheftDescription(), that.getTheftDescription()) &&
                Objects.equals(isWithCounterpartyInternal, that.isWithCounterpartyInternal) &&
                Objects.equals(isWithCounterparty, that.isWithCounterparty) &&
                Objects.equals(getClaimAddressStreet(), that.getClaimAddressStreet()) &&
                Objects.equals(getClaimAddressStreetNr(), that.getClaimAddressStreetNr()) &&
                Objects.equals(getClaimAddressStreetZip(), that.getClaimAddressStreetZip()) &&
                Objects.equals(getClaimAddressStreetLocality(), that.getClaimAddressStreetLocality()) &&
                Objects.equals(getClaimAddressStreetProvince(), that.getClaimAddressStreetProvince()) &&
                Objects.equals(getClaimAddressStreetRegion(), that.getClaimAddressStreetRegion()) &&
                Objects.equals(getClaimAddressStreetState(), that.getClaimAddressStreetState()) &&
                Objects.equals(getClaimAddressFormatted(), that.getClaimAddressFormatted()) &&
                Objects.equals(getClaimsFromCompanyEntity(), that.getClaimsFromCompanyEntity()) &&
                Objects.equals(getClaimsEntrustedEntity(), that.getClaimsEntrustedEntity()) &&
                Objects.equals(isCaiSigned, that.isCaiSigned) &&
                Objects.equals(getImpactPoint(), that.getImpactPoint()) &&
                Objects.equals(getAdditionalCosts(), that.getAdditionalCosts()) &&
                Objects.equals(getClaimsDamagedContractEntity(), that.getClaimsDamagedContractEntity()) &&
                Objects.equals(getClaimsDamagedCustomerEntity(), that.getClaimsDamagedCustomerEntity()) &&
                Objects.equals(getClaimsDamagedDriverEntity(), that.getClaimsDamagedDriverEntity()) &&
                Objects.equals(getClaimsDamagedVehicleEntity(), that.getClaimsDamagedVehicleEntity()) &&
                Objects.equals(getClaimsDamagedInsuranceInfoEntity(), that.getClaimsDamagedInsuranceInfoEntity()) &&
                Objects.equals(getClaimsDamagedFleetManagerEntityList(), that.getClaimsDamagedFleetManagerEntityList()) &&
                Objects.equals(getAntiTheftServiceEntity(), that.getAntiTheftServiceEntity()) &&
                Objects.equals(getClaimsDamagedRegistryEntityList(), that.getClaimsDamagedRegistryEntityList()) &&
                Objects.equals(getClaimsRefundEntity(), that.getClaimsRefundEntity()) &&
                Objects.equals(getClaimsTheftEntity(), that.getClaimsTheftEntity()) &&
                Objects.equals(getClaimsMetadataEntity(), that.getClaimsMetadataEntity()) &&
                Objects.equals(getClaimsAuthorityEmbeddedEntities(), that.getClaimsAuthorityEmbeddedEntities()) &&
                Objects.equals(getPracticeId(), that.getPracticeId()) &&
                Objects.equals(getPracticeManager(), that.getPracticeManager()) &&
                Objects.equals(getPaiComunication(), that.getPaiComunication()) &&
                Objects.equals(getForced(), that.getForced()) &&
                Objects.equals(getInEvidence(), that.getInEvidence()) &&
                Objects.equals(getUserId(), that.getUserId()) &&
                getStatus() == that.getStatus() &&
                Objects.equals(getMotivation(), that.getMotivation()) &&
                getType() == that.getType() &&
                Objects.equals(isCompleteDocumentation, that.isCompleteDocumentation) &&
                Objects.equals(getForms(), that.getForms()) &&
                Objects.equals(getCaiDetails(), that.getCaiDetails()) &&
                Objects.equals(getCounterparts(), that.getCounterparts()) &&
                Objects.equals(getDeponentList(), that.getDeponentList()) &&
                Objects.equals(getWoundedList(), that.getWoundedList()) &&
                Objects.equals(getNotes(), that.getNotes()) &&
                Objects.equals(getHistorical(), that.getHistorical()) &&
                Objects.equals(getExemption(), that.getExemption()) &&
                Objects.equals(getIdSaleforce(), that.getIdSaleforce()) &&
                Objects.equals(getWithContinuation(), that.getWithContinuation()) &&
                Objects.equals(isRead, that.isRead) &&
                Objects.equals(isAuthorityLinkable, that.isAuthorityLinkable) &&
                Objects.equals(getPoVariation(), that.getPoVariation()) &&
                Objects.equals(getLegalComunication(), that.getLegalComunication()) &&
                Objects.equals(getTotalPenalty(), that.getTotalPenalty()) &&
                Objects.equals(isReadAcclaims, that.isReadAcclaims) &&
                Objects.equals(isMigrated, that.isMigrated) &&
                Objects.equals(getAntiTheftRequestEntities(), that.getAntiTheftRequestEntities());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getCreatedAt(), getUpdateAt(), getStatusUpdatedAt(), getClientId(), getPlate(), getClaimLocator(), getActivation(), getProperty(), getMod(), getNotification(), getQuote(), getTypeAccident(), getResponsible(), getDateAccident(), getHappenedAbroad(), getDamageToObjects(), getDamageToVehicles(), getOldMotorcyclePlates(), getInterventionAuthority(), getPolice(), getCc(), getVvuu(), getAuthorityData(), getWitnessDescription(), getRecoverability(), getRecoverabilityPercent(), getIncompleteMotivation(), isRobbery, getCenterNotified(), getHappenedOnCenter(), getProviderCode(), getTheftDescription(), isWithCounterpartyInternal, isWithCounterparty, getClaimAddressStreet(), getClaimAddressStreetNr(), getClaimAddressStreetZip(), getClaimAddressStreetLocality(), getClaimAddressStreetProvince(), getClaimAddressStreetRegion(), getClaimAddressStreetState(), getClaimAddressFormatted(), getClaimsFromCompanyEntity(), getClaimsEntrustedEntity(), isCaiSigned, getImpactPoint(), getAdditionalCosts(), getClaimsDamagedContractEntity(), getClaimsDamagedCustomerEntity(), getClaimsDamagedDriverEntity(), getClaimsDamagedVehicleEntity(), getClaimsDamagedInsuranceInfoEntity(), getClaimsDamagedFleetManagerEntityList(), getAntiTheftServiceEntity(), getClaimsDamagedRegistryEntityList(), getClaimsRefundEntity(), getClaimsTheftEntity(), getClaimsMetadataEntity(), getClaimsAuthorityEmbeddedEntities(), getPracticeId(), getPracticeManager(), getPaiComunication(), getForced(), getInEvidence(), getUserId(), getStatus(), getMotivation(), getType(), isCompleteDocumentation, getForms(), getCaiDetails(), getCounterparts(), getDeponentList(), getWoundedList(), getNotes(), getHistorical(), getExemption(), getIdSaleforce(), getWithContinuation(), isRead, isAuthorityLinkable, getPoVariation(), getLegalComunication(), getTotalPenalty(), isReadAcclaims, isMigrated, getAntiTheftRequestEntities());
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ClaimsNewEntity{");
        sb.append("id='").append(id).append('\'');
        sb.append(", createdAt=").append(createdAt);
        sb.append(", updateAt=").append(updateAt);
        sb.append(", statusUpdatedAt=").append(statusUpdatedAt);
        sb.append(", clientId='").append(clientId).append('\'');
        sb.append(", plate='").append(plate).append('\'');
        sb.append(", claimLocator='").append(claimLocator).append('\'');
        sb.append(", activation='").append(activation).append('\'');
        sb.append(", property=").append(property);
        sb.append(", mod=").append(mod);
        sb.append(", notification='").append(notification).append('\'');
        sb.append(", quote=").append(quote);
        sb.append(", typeAccident=").append(typeAccident);
        sb.append(", responsible=").append(responsible);
        sb.append(", dateAccident=").append(dateAccident);
        sb.append(", happenedAbroad=").append(happenedAbroad);
        sb.append(", damageToObjects=").append(damageToObjects);
        sb.append(", damageToVehicles=").append(damageToVehicles);
        sb.append(", oldMotorcyclePlates='").append(oldMotorcyclePlates).append('\'');
        sb.append(", interventionAuthority=").append(interventionAuthority);
        sb.append(", police=").append(police);
        sb.append(", cc=").append(cc);
        sb.append(", vvuu=").append(vvuu);
        sb.append(", authorityData='").append(authorityData).append('\'');
        sb.append(", witnessDescription='").append(witnessDescription).append('\'');
        sb.append(", recoverability=").append(recoverability);
        sb.append(", recoverabilityPercent=").append(recoverabilityPercent);
        sb.append(", incompleteMotivation='").append(incompleteMotivation).append('\'');
        sb.append(", isRobbery=").append(isRobbery);
        sb.append(", centerNotified=").append(centerNotified);
        sb.append(", happenedOnCenter=").append(happenedOnCenter);
        sb.append(", providerCode='").append(providerCode).append('\'');
        sb.append(", theftDescription='").append(theftDescription).append('\'');
        sb.append(", isWithCounterpartyInternal=").append(isWithCounterpartyInternal);
        sb.append(", isWithCounterparty=").append(isWithCounterparty);
        sb.append(", claimAddressStreet='").append(claimAddressStreet).append('\'');
        sb.append(", claimAddressStreetNr='").append(claimAddressStreetNr).append('\'');
        sb.append(", claimAddressStreetZip='").append(claimAddressStreetZip).append('\'');
        sb.append(", claimAddressStreetLocality='").append(claimAddressStreetLocality).append('\'');
        sb.append(", claimAddressStreetProvince='").append(claimAddressStreetProvince).append('\'');
        sb.append(", claimAddressStreetRegion='").append(claimAddressStreetRegion).append('\'');
        sb.append(", claimAddressStreetState='").append(claimAddressStreetState).append('\'');
        sb.append(", claimAddressFormatted='").append(claimAddressFormatted).append('\'');
        sb.append(", claimsFromCompanyEntity=").append(claimsFromCompanyEntity);
        sb.append(", claimsEntrustedEntity=").append(claimsEntrustedEntity);
        sb.append(", isCaiSigned=").append(isCaiSigned);
        sb.append(", impactPoint=").append(impactPoint);
        sb.append(", additionalCosts=").append(additionalCosts);
        sb.append(", claimsDamagedContractEntity=").append(claimsDamagedContractEntity);
        sb.append(", claimsDamagedCustomerEntity=").append(claimsDamagedCustomerEntity);
        sb.append(", claimsDamagedDriverEntity=").append(claimsDamagedDriverEntity);
        sb.append(", claimsDamagedVehicleEntity=").append(claimsDamagedVehicleEntity);
        sb.append(", claimsDamagedInsuranceInfoEntity=").append(claimsDamagedInsuranceInfoEntity);
        sb.append(", claimsDamagedFleetManagerEntityList=").append(claimsDamagedFleetManagerEntityList);
        sb.append(", antiTheftServiceEntity=").append(antiTheftServiceEntity);
        sb.append(", claimsDamagedRegistryEntityList=").append(claimsDamagedRegistryEntityList);
        sb.append(", claimsRefundEntity=").append(claimsRefundEntity);
        sb.append(", claimsTheftEntity=").append(claimsTheftEntity);
        sb.append(", claimsMetadataEntity=").append(claimsMetadataEntity);
        sb.append(", claimsAuthorityEmbeddedEntities=").append(claimsAuthorityEmbeddedEntities);
        sb.append(", practiceId=").append(practiceId);
        sb.append(", practiceManager='").append(practiceManager).append('\'');
        sb.append(", paiComunication=").append(paiComunication);
        sb.append(", forced=").append(forced);
        sb.append(", inEvidence=").append(inEvidence);
        sb.append(", userId='").append(userId).append('\'');
        sb.append(", status=").append(status);
        sb.append(", motivation='").append(motivation).append('\'');
        sb.append(", type=").append(type);
        sb.append(", isCompleteDocumentation=").append(isCompleteDocumentation);
        sb.append(", forms=").append(forms);
        sb.append(", caiDetails=").append(caiDetails);
        sb.append(", counterparts=").append(counterparts);
        sb.append(", deponentList=").append(deponentList);
        sb.append(", woundedList=").append(woundedList);
        sb.append(", notes=").append(notes);
        sb.append(", historical=").append(historical);
        sb.append(", exemption=").append(exemption);
        sb.append(", idSaleforce=").append(idSaleforce);
        sb.append(", withContinuation=").append(withContinuation);
        sb.append(", isRead=").append(isRead);
        sb.append(", isAuthorityLinkable=").append(isAuthorityLinkable);
        sb.append(", poVariation=").append(poVariation);
        sb.append(", legalComunication=").append(legalComunication);
        sb.append(", totalPenalty=").append(totalPenalty);
        sb.append(", isReadAcclaims=").append(isReadAcclaims);
        sb.append(", isMigrated=").append(isMigrated);
        sb.append(", antiTheftRequestEntities=").append(antiTheftRequestEntities);
        sb.append('}');
        return sb.toString();
    }
}
