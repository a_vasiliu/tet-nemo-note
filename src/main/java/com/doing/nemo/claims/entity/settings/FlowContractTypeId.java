package com.doing.nemo.claims.entity.settings;

import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

@Embeddable
public class FlowContractTypeId implements Serializable {

    @Column(name = "contract_type_entity_id")
    @Type(type = "org.hibernate.type.PostgresUUIDType")
    private UUID contractTypeId;

    @Column(name = "claims_type_entity_id")
    @Type(type = "org.hibernate.type.PostgresUUIDType")
    private UUID claimsTypeId;

    public FlowContractTypeId() {
    }

    public FlowContractTypeId(UUID contractTypeId, UUID claimsTypeId) {
        this.contractTypeId = contractTypeId;
        this.claimsTypeId = claimsTypeId;
    }

    public UUID getContractTypeId() {
        return contractTypeId;
    }

    public void setContractTypeId(UUID contractTypeId) {
        this.contractTypeId = contractTypeId;
    }

    public UUID getClaimsTypeId() {
        return claimsTypeId;
    }

    public void setClaimsTypeId(UUID claimsTypeId) {
        this.claimsTypeId = claimsTypeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass())
            return false;

        FlowContractTypeId that = (FlowContractTypeId) o;
        return Objects.equals(this.getContractTypeId(), that.getContractTypeId()) &&
                Objects.equals(this.getClaimsTypeId(), that.getClaimsTypeId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.getContractTypeId(), this.getClaimsTypeId());
    }


    @Override
    public String toString() {
        return "FlowContractTypeId{" +
                "contractTypeId=" + contractTypeId +
                ", claimsTypeId=" + claimsTypeId +
                '}';
    }
}
