package com.doing.nemo.claims.entity.settings;

import java.io.Serializable;
import java.time.LocalDate;

public class ContractPatchEntity implements Serializable {

    private LocalDate startDate;

    private LocalDate endDate;

    private Integer voucherId;

    private String codPack;

    public ContractPatchEntity() {
    }

    public ContractPatchEntity(LocalDate startDate, LocalDate endDate, Integer voucherId, String codPack) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.voucherId = voucherId;
        this.codPack = codPack;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public Integer getVoucherId() {
        return voucherId;
    }

    public void setVoucherId(Integer voucherId) {
        this.voucherId = voucherId;
    }

    public String getCodPack() {
        return codPack;
    }

    public void setCodPack(String codPack) {
        this.codPack = codPack;
    }
}
