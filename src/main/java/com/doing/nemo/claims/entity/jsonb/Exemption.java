package com.doing.nemo.claims.entity.jsonb;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.Date;
@JsonIgnoreProperties(ignoreUnknown = true)
public class Exemption implements Serializable {

    private static final long serialVersionUID = 2085523956773855678L;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date deductEnd1;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date deductEnd2;

    private Double deductPaid1;
    private Double deductPaid2;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date deductImported1;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date deductImported2;

    private Double deductTotalPaid;
    private String companyReference;
    private String policyNumber;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Date getDeductEnd1() {
        if(deductEnd1 == null){
            return null;
        }
        return (Date)deductEnd1.clone();
    }

    public void setDeductEnd1(Date deductEnd1) {
        if(deductEnd1 != null)
        {
            this.deductEnd1 = (Date)deductEnd1.clone();
        } else {
            this.deductEnd1 = null;
        }
    }

    public Date getDeductEnd2() {
        if(deductEnd2 == null){
            return null;
        }
        return (Date)deductEnd2.clone();
    }

    public void setDeductEnd2(Date deductEnd2) {
        if(deductEnd2 != null)
        {
            this.deductEnd2 = (Date)deductEnd2.clone();
        } else {
            this.deductEnd2 = null;
        }
    }

    public Double getDeductPaid1() {
        return deductPaid1;
    }

    public void setDeductPaid1(Double deductPaid1) {
        this.deductPaid1 = deductPaid1;
    }

    public Double getDeductPaid2() {
        return deductPaid2;
    }

    public void setDeductPaid2(Double deductPaid2) {
        this.deductPaid2 = deductPaid2;
    }

    public Date getDeductImported1() {
        if(deductImported1 == null){
            return null;
        }
        return (Date)deductImported1.clone();

    }

    public void setDeductImported1(Date deductImported1) {
        if(deductImported1 != null)
        {
            this.deductImported1 = (Date)deductImported1.clone();
        } else {
            this.deductImported1 = null;
        }
    }

    public Date getDeductImported2() {
        if(deductImported2 == null){
            return null;
        }
        return (Date)deductImported2.clone();
    }

    public void setDeductImported2(Date deductImported2) {
        if(deductImported2 != null)
        {
            this.deductImported2 = (Date)deductImported2.clone();
        } else {
            this.deductImported2 = null;
        }
    }

    public Double getDeductTotalPaid() {
        return deductTotalPaid;
    }

    public void setDeductTotalPaid(Double deductTotalPaid) {
        this.deductTotalPaid = deductTotalPaid;
    }

    public String getCompanyReference() {
        return companyReference;
    }

    public void setCompanyReference(String companyReference) {
        this.companyReference = companyReference;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    @Override
    public String toString() {
        return "Exemption{" +
                "deductEnd1=" + deductEnd1 +
                ", deductEnd2=" + deductEnd2 +
                ", deductPaid1=" + deductPaid1 +
                ", deductPaid2=" + deductPaid2 +
                ", deductImported1=" + deductImported1 +
                ", deductImported2=" + deductImported2 +
                ", deductTotalPaid=" + deductTotalPaid +
                ", companyReference='" + companyReference + '\'' +
                ", policyNumber='" + policyNumber + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Exemption exemption = (Exemption) o;

        return new EqualsBuilder().append(deductEnd1, exemption.deductEnd1).append(deductEnd2, exemption.deductEnd2)
                .append(deductPaid1, exemption.deductPaid1).append(deductPaid2, exemption.deductPaid2)
                .append(deductImported1, exemption.deductImported1).append(deductImported2, exemption.deductImported2)
                .append(deductTotalPaid, exemption.deductTotalPaid).append(companyReference, exemption.companyReference)
                .append(policyNumber, exemption.policyNumber).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(deductEnd1).append(deductEnd2)
                .append(deductPaid1).append(deductPaid2).append(deductImported1).append(deductImported2)
                .append(deductTotalPaid).append(companyReference).append(policyNumber).toHashCode();
    }
}
