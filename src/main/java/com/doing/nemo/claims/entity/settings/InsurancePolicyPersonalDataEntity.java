package com.doing.nemo.claims.entity.settings;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.time.Instant;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "insurance_policy_personal_data")
public class InsurancePolicyPersonalDataEntity {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )

    @Type(type = "org.hibernate.type.PostgresUUIDType")
    private UUID id;

    @Column(name = "insurance_policy_id", unique = true, insertable = false, updatable = false)
    private Long insurancePolicyId;

    @Column(name = "type")
    private String type;

    @Column(name = "description")
    private String description;

    @OneToOne
    @JoinColumn(name = "insurance_company_id")
    private InsuranceCompanyEntity insuranceCompanyEntity;

    @Column(name = "number_policy")
    private Long numberPolicy;

    @Column(name = "beginning_validity")
    private Instant beginningValidity;

    @Column(name = "end_validity")
    private Instant endValidity;

    @OneToOne
    @JoinColumn(name = "broker_id")
    private BrokerEntity brokerEntity;

    @OneToOne
    @JoinColumn(name = "insurance_manager_id")
    private InsuranceManagerEntity insuranceManagerEntity;

    @Column(name = "annotations")
    private String annotations;

    @Column(name = "book_register")
    private Boolean bookRegister;

    @Column(name = "book_register_code")
    private String bookRegisterCode;

    @Column(name = "book_register_pai_code")
    private String bookRegisterPaiCode;

    @Column(name = "crystals_fr")
    private String crystalsFr;

    @Column(name = "furinc")
    private String furinc;

    @Column(name = "massimal")
    private String massimal;

    @Column(name = "furinc_uncover")
    private String furincUncover;

    @Column(name = "kasko_fr")
    private String kaskoFr;

    @Column(name = "crystals_max")
    private String crystalsMax;

    public InsurancePolicyPersonalDataEntity() {

    }


    public InsurancePolicyPersonalDataEntity(Long insurancePolicyId, String type, String description, InsuranceCompanyEntity insuranceCompanyEntity, Long numberPolicy, Instant beginningValidity, Instant endValidity, BrokerEntity brokerEntity, InsuranceManagerEntity insuranceManagerEntity, String annotations, Boolean bookRegister, String bookRegisterCode, String bookRegisterPaiCode, String crystalsFr, String furinc, String massimal, String furincUncover, String kaskoFr, String crystalsMax) {
        this.insurancePolicyId = insurancePolicyId;
        this.type = type;
        this.description = description;
        this.insuranceCompanyEntity = insuranceCompanyEntity;
        this.numberPolicy = numberPolicy;
        this.beginningValidity = beginningValidity;
        this.endValidity = endValidity;
        this.brokerEntity = brokerEntity;
        this.insuranceManagerEntity = insuranceManagerEntity;
        this.annotations = annotations;
        this.bookRegister = bookRegister;
        this.bookRegisterCode = bookRegisterCode;
        this.bookRegisterPaiCode = bookRegisterPaiCode;
        this.crystalsFr = crystalsFr;
        this.furinc = furinc;
        this.massimal = massimal;
        this.furincUncover = furincUncover;
        this.kaskoFr = kaskoFr;
        this.crystalsMax = crystalsMax;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Long getInsurancePolicyId() {
        return insurancePolicyId;
    }

    public void setInsurancePolicyId(Long insurancePolicyId) {
        this.insurancePolicyId = insurancePolicyId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public InsuranceCompanyEntity getInsuranceCompanyEntity() {
        return insuranceCompanyEntity;
    }

    public void setInsuranceCompanyEntity(InsuranceCompanyEntity insuranceCompanyEntity) {
        this.insuranceCompanyEntity = insuranceCompanyEntity;
    }

    public Long getNumberPolicy() {
        return numberPolicy;
    }

    public void setNumberPolicy(Long numberPolicy) {
        this.numberPolicy = numberPolicy;
    }

    public Instant getBeginningValidity() {
        return beginningValidity;
    }

    public void setBeginningValidity(Instant beginningValidity) {
        this.beginningValidity = beginningValidity;
    }

    public Instant getEndValidity() {
        return endValidity;
    }

    public void setEndValidity(Instant endValidity) {
        this.endValidity = endValidity;
    }

    public BrokerEntity getBrokerEntity() {
        return brokerEntity;
    }

    public void setBrokerEntity(BrokerEntity brokerEntity) {
        this.brokerEntity = brokerEntity;
    }

    public InsuranceManagerEntity getInsuranceManagerEntity() {
        return insuranceManagerEntity;
    }

    public void setInsuranceManagerEntity(InsuranceManagerEntity insuranceManagerEntity) {
        this.insuranceManagerEntity = insuranceManagerEntity;
    }

    public String getAnnotations() {
        return annotations;
    }

    public void setAnnotations(String annotations) {
        this.annotations = annotations;
    }

    public Boolean getBookRegister() {
        return bookRegister;
    }

    public void setBookRegister(Boolean bookRegister) {
        this.bookRegister = bookRegister;
    }

    public String getBookRegisterCode() {
        return bookRegisterCode;
    }

    public void setBookRegisterCode(String bookRegisterCode) {
        this.bookRegisterCode = bookRegisterCode;
    }

    public String getBookRegisterPaiCode() {
        return bookRegisterPaiCode;
    }

    public void setBookRegisterPaiCode(String bookRegisterPaiCode) {
        this.bookRegisterPaiCode = bookRegisterPaiCode;
    }

    public String getCrystalsFr() {
        return crystalsFr;
    }

    public void setCrystalsFr(String crystalsFr) {
        this.crystalsFr = crystalsFr;
    }

    public String getFurinc() {
        return furinc;
    }

    public void setFurinc(String furinc) {
        this.furinc = furinc;
    }

    public String getMassimal() {
        return massimal;
    }

    public void setMassimal(String massimal) {
        this.massimal = massimal;
    }

    public String getFurincUncover() {
        return furincUncover;
    }

    public void setFurincUncover(String furincUncover) {
        this.furincUncover = furincUncover;
    }

    public String getKaskoFr() {
        return kaskoFr;
    }

    public void setKaskoFr(String kaskoFr) {
        this.kaskoFr = kaskoFr;
    }

    public String getCrystalsMax() {
        return crystalsMax;
    }

    public void setCrystalsMax(String crystalsMax) {
        this.crystalsMax = crystalsMax;
    }

    @Override
    public String toString() {
        return "InsurancePolicyPersonalDataEntity{" +
                "id=" + id +
                ", insurancePolicyId=" + insurancePolicyId +
                ", type='" + type + '\'' +
                ", description='" + description + '\'' +
                ", insuranceCompanyEntity=" + insuranceCompanyEntity +
                ", numberPolicy=" + numberPolicy +
                ", beginningValidity=" + beginningValidity +
                ", endValidity=" + endValidity +
                ", brokerEntity=" + brokerEntity +
                ", insuranceManagerEntity=" + insuranceManagerEntity +
                ", annotations='" + annotations + '\'' +
                ", bookRegister=" + bookRegister +
                ", bookRegisterCode='" + bookRegisterCode + '\'' +
                ", bookRegisterPaiCode='" + bookRegisterPaiCode + '\'' +
                ", crystalsFr='" + crystalsFr + '\'' +
                ", furinc='" + furinc + '\'' +
                ", massimal='" + massimal + '\'' +
                ", furincUncover='" + furincUncover + '\'' +
                ", kaskoFr='" + kaskoFr + '\'' +
                ", crystalsMax='" + crystalsMax + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InsurancePolicyPersonalDataEntity that = (InsurancePolicyPersonalDataEntity) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getInsurancePolicyId(), that.getInsurancePolicyId()) &&
                Objects.equals(getType(), that.getType()) &&
                Objects.equals(getDescription(), that.getDescription()) &&
                Objects.equals(getInsuranceCompanyEntity(), that.getInsuranceCompanyEntity()) &&
                Objects.equals(getNumberPolicy(), that.getNumberPolicy()) &&
                Objects.equals(getBeginningValidity(), that.getBeginningValidity()) &&
                Objects.equals(getEndValidity(), that.getEndValidity()) &&
                Objects.equals(getBrokerEntity(), that.getBrokerEntity()) &&
                Objects.equals(getInsuranceManagerEntity(), that.getInsuranceManagerEntity()) &&
                Objects.equals(getAnnotations(), that.getAnnotations()) &&
                Objects.equals(getBookRegister(), that.getBookRegister()) &&
                Objects.equals(getBookRegisterCode(), that.getBookRegisterCode()) &&
                Objects.equals(getBookRegisterPaiCode(), that.getBookRegisterPaiCode()) &&
                Objects.equals(getCrystalsFr(), that.getCrystalsFr()) &&
                Objects.equals(getFurinc(), that.getFurinc()) &&
                Objects.equals(getMassimal(), that.getMassimal()) &&
                Objects.equals(getFurincUncover(), that.getFurincUncover()) &&
                Objects.equals(getKaskoFr(), that.getKaskoFr()) &&
                Objects.equals(getCrystalsMax(), that.getCrystalsMax());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getInsurancePolicyId(), getType(), getDescription(), getInsuranceCompanyEntity(), getNumberPolicy(), getBeginningValidity(), getEndValidity(), getBrokerEntity(), getInsuranceManagerEntity(), getAnnotations(), getBookRegister(), getBookRegisterCode(), getBookRegisterPaiCode(), getCrystalsFr(), getFurinc(), getMassimal(), getFurincUncover(), getKaskoFr(), getCrystalsMax());
    }
}
