package com.doing.nemo.claims.entity.settings;

import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Entity
@Table(name = "claims_type")
@JsonIgnoreProperties({"hibernate_lazy_initializer", "handler"})
public class ClaimsTypeEntity implements Serializable {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Type(type = "org.hibernate.type.PostgresUUIDType")
    private UUID id;

    @Column(name = "type")
    private String type;

    @Enumerated(EnumType.STRING)
    @Column(name = "claims_accident_type")
    private DataAccidentTypeAccidentEnum claimsAccidentType;

    @OneToMany(
            mappedBy = "claimsTypeEntity",
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<FlowContractTypeEntity> flowContractTypeList = new LinkedList<>();

    public ClaimsTypeEntity() {
    }

    public ClaimsTypeEntity(UUID id, String type, DataAccidentTypeAccidentEnum claimsAccidentType, List<FlowContractTypeEntity> flowContractTypeList) {
        this.id = id;
        this.type = type;
        this.claimsAccidentType = claimsAccidentType;
        this.flowContractTypeList = flowContractTypeList;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @JsonIgnore
    public List<FlowContractTypeEntity> getFlowContractTypeList() {
        return flowContractTypeList;
    }

    public void setFlowContractTypeList(List<FlowContractTypeEntity> flowContractTypeList) {
        this.flowContractTypeList = flowContractTypeList;
    }

    public DataAccidentTypeAccidentEnum getClaimsAccidentType() {
        return claimsAccidentType;
    }

    public void setClaimsAccidentType(DataAccidentTypeAccidentEnum claimsAccidentType) {
        this.claimsAccidentType = claimsAccidentType;
    }

    public void addContractType(ContractTypeEntity contractTypeEntity, String flow, String rebilling) {
        FlowContractTypeEntity flowContractTypeEntity = new FlowContractTypeEntity(this, contractTypeEntity, flow, rebilling);
        flowContractTypeList.add(flowContractTypeEntity);
        contractTypeEntity.getFlowContractTypeList().add(flowContractTypeEntity);
    }

    public void removeContractType(ContractTypeEntity contractTypeEntity) {
        for (Iterator<FlowContractTypeEntity> iterator = flowContractTypeList.iterator();
             iterator.hasNext(); ) {
            FlowContractTypeEntity flowContractTypeEntity = iterator.next();

            if (flowContractTypeEntity.getClaimsTypeEntity().equals(this) &&
                    flowContractTypeEntity.getContractTypeEntity().equals(contractTypeEntity)) {
                iterator.remove();
                flowContractTypeEntity.getContractTypeEntity().getFlowContractTypeList().remove(flowContractTypeEntity);
                flowContractTypeEntity.setClaimsTypeEntity(null);
                flowContractTypeEntity.setContractTypeEntity(null);
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClaimsTypeEntity that = (ClaimsTypeEntity) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getType(), that.getType()) &&
                getClaimsAccidentType() == that.getClaimsAccidentType() &&
                Objects.equals(getFlowContractTypeList(), that.getFlowContractTypeList());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getType(), getClaimsAccidentType(), getFlowContractTypeList());
    }
}
