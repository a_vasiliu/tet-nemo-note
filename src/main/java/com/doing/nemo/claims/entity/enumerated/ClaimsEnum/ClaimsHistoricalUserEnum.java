package com.doing.nemo.claims.entity.enumerated.ClaimsEnum;

import com.doing.nemo.commons.exception.BadParametersException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.io.Serializable;

public enum ClaimsHistoricalUserEnum implements Serializable {
    SYSTEM_AUTOMATIC_ENTRUST("system_automatic_entrust"),
    SYSTEM_AUTOMATIC_SENDTOCLIENT("system_automatic_sendtoclient"),
    SYSTEM_ALDVSALD("system_aldvsald"),
    SYSTEM_AUTHORITY("system_authority"),
    SYSTEM_IMPORT_EXEMPTION_1("system_import_exemption_1"),
    SYSTEM_IMPORT_EXEMPTION_2("system_import_exemption_2"),
    SYSTEM_IMPORT_NUM_SX("system_import_num_sx"),
    SYSTEM_MASSIVE_ENTRUST("system_massive_entrust"),
    SYSTEM_MILES("system_miles"),
    SYSTEM_TO_ENTRUST("system_to_entrust"),
    SYSTEM_TO_ENTRUST_PO("system_to_entrust_po"),
    SYSTEM_TO_ENTRUST_ACCOUNT_MANAGER("system_to_entrust_account_manager");

    private static Logger LOGGER = LoggerFactory.getLogger(ClaimsHistoricalUserEnum.class);
    private String userEntity;

    private ClaimsHistoricalUserEnum(String userEntity) {
        this.userEntity = userEntity;
    }

    @JsonCreator
    public static ClaimsHistoricalUserEnum create(String userEntity) {

        userEntity = userEntity.replace(" - ", "_");
        userEntity = userEntity.replace('-', '_');
        userEntity = userEntity.replace(' ', '_');

        if (userEntity != null) {
            for (ClaimsHistoricalUserEnum val : ClaimsHistoricalUserEnum.values()) {
                if (userEntity.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        LOGGER.debug("Bad parameters exception. Enum class " + ClaimsHistoricalUserEnum.class.getSimpleName() + " doesn't accept this value: " + userEntity);
        throw new BadParametersException("Bad parameters exception. Enum class " + ClaimsHistoricalUserEnum.class.getSimpleName() + " doesn't accept this value: " + userEntity);
    }

    public String getValue() {
        return this.userEntity.toUpperCase();
    }

    @JsonValue
    public String toValue() {
        for (ClaimsHistoricalUserEnum val : ClaimsHistoricalUserEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }
}
