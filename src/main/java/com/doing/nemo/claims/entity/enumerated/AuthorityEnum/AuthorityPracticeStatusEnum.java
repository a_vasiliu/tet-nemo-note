package com.doing.nemo.claims.entity.enumerated.AuthorityEnum;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;

public enum AuthorityPracticeStatusEnum implements Serializable {
    TO_SEND("to_send"),
    ADMINISTRATIVE_WORKING("administrative_working"),
    ADMINISTRATIVE_WAITING_FOR_CLOSED("administrative_waiting_for_closed"),
    ADMINISTRATIVE_REJECTED("administrative_rejected"),
    CLOSED("closed")
    ;

    private static Logger LOGGER = LoggerFactory.getLogger(AuthorityPracticeStatusEnum.class);
    private String administrationPracticeType;

    AuthorityPracticeStatusEnum(String administrationPracticeType) {
        this.administrationPracticeType = administrationPracticeType;
    }

    @JsonCreator
    public static AuthorityPracticeStatusEnum create(String claimsRepairEnum) {

        claimsRepairEnum = claimsRepairEnum.replace(" - ", "_");
        claimsRepairEnum = claimsRepairEnum.replace('-', '_');
        claimsRepairEnum = claimsRepairEnum.replace(' ', '_');

        if (claimsRepairEnum != null) {
            for (AuthorityPracticeStatusEnum val : AuthorityPracticeStatusEnum.values()) {
                if (claimsRepairEnum.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        LOGGER.debug("Bad parameters exception. Enum class " + AuthorityPracticeStatusEnum.class.getSimpleName() + " doesn't accept this value: " + claimsRepairEnum);
        throw new BadParametersException("Bad parameters exception. Enum class " + AuthorityPracticeStatusEnum.class.getSimpleName() + " doesn't accept this value: " + claimsRepairEnum);
    }

    public String getValue() {
        return this.administrationPracticeType.toUpperCase();
    }

    @JsonValue
    public String toValue() {
        for (AuthorityPracticeStatusEnum val : AuthorityPracticeStatusEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }
}
