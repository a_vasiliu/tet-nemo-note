package com.doing.nemo.claims.entity.settings;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "data_management_export")
public class DataManagementExportEntity implements Serializable {

    @Id
    @Type(type = "org.hibernate.type.PostgresUUIDType")
    private UUID id;

    @Column(name = "user_import")
    private String user;

    @Column(name = "user_firstname")
    private String userFirstname;

    @Column(name = "user_lastname")
    private String userLastname;

    @Column(name = "user_role")
    private String role;

    @Column(name = "description")
    private String description;

    @Column(name = "result")
    private String result;

    @Column(name = "date")
    private Instant date;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "data_management_export_id", referencedColumnName = "id")
    private List<ExportingFile> processingFiles = new LinkedList<>();

    public DataManagementExportEntity() {
    }

    public DataManagementExportEntity(UUID id, String user, String userFirstname, String userLastname, String role, String description, String result, Instant date, List<ExportingFile> processingFiles) {
        this.id = id;
        this.user = user;
        this.userFirstname = userFirstname;
        this.userLastname = userLastname;
        this.role = role;
        this.description = description;
        this.result = result;
        this.date = date;
        this.processingFiles = processingFiles;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Instant getDate() {
        return date;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    public String getUserFirstname() {
        return userFirstname;
    }

    public void setUserFirstname(String userFirstname) {
        this.userFirstname = userFirstname;
    }

    public String getUserLastname() {
        return userLastname;
    }

    public void setUserLastname(String userLastname) {
        this.userLastname = userLastname;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @JsonIgnore
    public List<ExportingFile> getProcessingFiles() {
        return processingFiles;
    }

    public void setProcessingFiles(List<ExportingFile> processingFiles) {
        this.processingFiles = processingFiles;
    }

    @Override
    public String toString() {
        return "DataManagementExportEntity{" +
                "id=" + id +
                ", user='" + user + '\'' +
                ", userFirstname='" + userFirstname + '\'' +
                ", userLastname='" + userLastname + '\'' +
                ", role='" + role + '\'' +
                ", description='" + description + '\'' +
                ", result='" + result + '\'' +
                ", date=" + date +
                ", processingFiles=" + processingFiles +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DataManagementExportEntity that = (DataManagementExportEntity) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getUser(), that.getUser()) &&
                Objects.equals(getUserFirstname(), that.getUserFirstname()) &&
                Objects.equals(getUserLastname(), that.getUserLastname()) &&
                Objects.equals(getRole(), that.getRole()) &&
                Objects.equals(getDescription(), that.getDescription()) &&
                Objects.equals(getResult(), that.getResult()) &&
                Objects.equals(getDate(), that.getDate()) &&
                Objects.equals(getProcessingFiles(), that.getProcessingFiles());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getUser(), getUserFirstname(), getUserLastname(), getRole(), getDescription(), getResult(), getDate(), getProcessingFiles());
    }
}
