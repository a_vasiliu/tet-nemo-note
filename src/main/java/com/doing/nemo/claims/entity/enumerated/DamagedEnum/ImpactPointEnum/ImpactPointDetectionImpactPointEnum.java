package com.doing.nemo.claims.entity.enumerated.DamagedEnum.ImpactPointEnum;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;

public enum ImpactPointDetectionImpactPointEnum implements Serializable {
    FRONT("front"),//anteriore
    FRONT_R("front_r"),
    FRONT_L("front_l"),
    WINDSHIELD("windshield"),//parabrezza
    REAR("rear"),//posteriore
    REAR_R("rear_r"),
    REAR_L("rear_l"),
    REAR_WINDOW("rear_window"),//lunotto
    ROOF("roof"),//tetto
    SIDE_FRONT_L("side_front_l"),
    SIDE_REAR_L("side_rear_l"),
    SIDE_FRONT_R("side_front_r"),
    SIDE_REAR_R("side_rear_r");

    private static Logger LOGGER = LoggerFactory.getLogger(ImpactPointDetectionImpactPointEnum.class);
    private String detectionImpactPoint;

    private ImpactPointDetectionImpactPointEnum(String detectionImpactPoint) {
        this.detectionImpactPoint = detectionImpactPoint;
    }

    @JsonCreator
    public static ImpactPointDetectionImpactPointEnum create(String detectionImpactPoint) {

        detectionImpactPoint = detectionImpactPoint.replace(" - ", "_");
        detectionImpactPoint = detectionImpactPoint.replace('-', '_');
        detectionImpactPoint = detectionImpactPoint.replace(' ', '_');

        if (detectionImpactPoint != null) {
            for (ImpactPointDetectionImpactPointEnum val : ImpactPointDetectionImpactPointEnum.values()) {
                if (detectionImpactPoint.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        LOGGER.debug("Bad parameters exception. Enum class " + ImpactPointDetectionImpactPointEnum.class.getSimpleName() + " doesn't accept this value: " + detectionImpactPoint);
        throw new BadParametersException("Bad parameters exception. Enum class " + ImpactPointDetectionImpactPointEnum.class.getSimpleName() + " doesn't accept this value: " + detectionImpactPoint);
    }

    public String getValue() {
        return this.detectionImpactPoint.toUpperCase();
    }

    @JsonValue
    public String toValue() {
        for (ImpactPointDetectionImpactPointEnum val : ImpactPointDetectionImpactPointEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }
}
