package com.doing.nemo.claims.entity.esb.FleetManagerESB;

import com.doing.nemo.claims.entity.esb.AddressEsb;
import com.doing.nemo.claims.entity.esb.LinkEsb;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FleetManagerEsb implements Serializable {

    private static final long serialVersionUID = -5053468874127361422L;

    @JsonProperty("id")
    private Long id;

    @JsonProperty("identification")
    private String identification;

    @JsonProperty("official_registration")
    private String officialRegistration;

    @JsonProperty("first_name")
    private String firstName;

    @JsonProperty("last_name")
    private String lastName;

    @JsonProperty("title")
    private String title;

    @JsonProperty("email")
    private String email;

    @JsonProperty("phone_prefix")
    private String phonePrefix;

    @JsonProperty("phone")
    private String phone;

    @JsonProperty("secondary_phone_prefix")
    private String secondaryPhonePrefix;

    @JsonProperty("secondary_phone")
    private String secondaryPhone;

    @JsonProperty("sex")
    private String sex;

    @JsonProperty("customer_id")
    private Long customerId;

    @JsonProperty("main_address")
    private AddressEsb mainAddress;

    @JsonProperty("roles")
    private List<RoleEsb> rolesList;

    @JsonProperty("links")
    private List<LinkEsb> linksList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public String getOfficialRegistration() {
        return officialRegistration;
    }

    public void setOfficialRegistration(String officialRegistration) {
        this.officialRegistration = officialRegistration;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhonePrefix() {
        return phonePrefix;
    }

    public void setPhonePrefix(String phonePrefix) {
        this.phonePrefix = phonePrefix;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSecondaryPhonePrefix() {
        return secondaryPhonePrefix;
    }

    public void setSecondaryPhonePrefix(String secondaryPhonePrefix) {
        this.secondaryPhonePrefix = secondaryPhonePrefix;
    }

    public String getSecondaryPhone() {
        return secondaryPhone;
    }

    public void setSecondaryPhone(String secondaryPhone) {
        this.secondaryPhone = secondaryPhone;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public AddressEsb getMainAddress() {
        return mainAddress;
    }

    public void setMainAddress(AddressEsb mainAddress) {
        this.mainAddress = mainAddress;
    }

    public List<RoleEsb> getRolesList() {
        if(rolesList == null){
            return null;
        }
        return new ArrayList<>(rolesList);
    }

    public void setRolesList(List<RoleEsb> rolesList) {
        if(rolesList != null)
        {
            this.rolesList = new ArrayList<>(rolesList);
        } else {
            this.rolesList = null;
        }
    }

    public List<LinkEsb> getLinksList() {
        if(linksList == null){
            return null;
        }
        return new ArrayList<>(linksList);
    }

    public void setLinksList(List<LinkEsb> linksList) {
        if(linksList != null)
        {
            this.linksList =  new ArrayList<>(linksList);
        } else {
            this.linksList = null;
        }
    }

    @Override
    public String toString() {
        return "FleetManagerEsb{" +
                "id=" + id +
                ", identification='" + identification + '\'' +
                ", officialRegistration='" + officialRegistration + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", title='" + title + '\'' +
                ", email='" + email + '\'' +
                ", phonePrefix='" + phonePrefix + '\'' +
                ", phone='" + phone + '\'' +
                ", secondaryPhonePrefix='" + secondaryPhonePrefix + '\'' +
                ", secondaryPhone='" + secondaryPhone + '\'' +
                ", sex='" + sex + '\'' +
                ", customerId=" + customerId +
                ", mainAddress=" + mainAddress +
                ", rolesList=" + rolesList +
                ", linksList=" + linksList +
                '}';
    }
}
