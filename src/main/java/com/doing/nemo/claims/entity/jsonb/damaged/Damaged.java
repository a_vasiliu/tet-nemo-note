package com.doing.nemo.claims.entity.jsonb.damaged;

import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.*;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbAntiTheftService.AntiTheftService;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbDriver.Driver;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbVehicle.Vehicle;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Damaged implements Serializable {

    private static final long serialVersionUID = -2910848184350828971L;

    @JsonProperty("contract")
    private Contract contract;

    @JsonProperty("customer")
    private Customer customer;

    @JsonProperty("insurance_company")
    private InsuranceCompany insuranceCompany;

    @JsonProperty("driver")
    private Driver driver;

    @JsonProperty("vehicle")
    private Vehicle vehicle;

    @JsonProperty("fleet_managers")
    private List<FleetManager> fleetManagerList;

    @JsonProperty("is_cai_signed")
    private Boolean isCaiSigned;

    @JsonProperty("impact_point")
    private ImpactPoint impactPoint;

    @JsonProperty("additional_costs")
    private List<AdditionalCosts> additionalCosts;

    @JsonProperty("anti_theft_service")
    private AntiTheftService antiTheftService;

    public Contract getContract() {
        return contract;
    }

    public void setContract(Contract contract) {
        this.contract = contract;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public InsuranceCompany getInsuranceCompany() {
        return insuranceCompany;
    }

    public void setInsuranceCompany(InsuranceCompany insuranceCompany) {
        this.insuranceCompany = insuranceCompany;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    @JsonIgnore
    public Boolean getCaiSigned() {
        return isCaiSigned;
    }

    public void setCaiSigned(Boolean caiSigned) {
        isCaiSigned = caiSigned;
    }

    public ImpactPoint getImpactPoint() {
        return impactPoint;
    }

    public void setImpactPoint(ImpactPoint impactPoint) {
        this.impactPoint = impactPoint;
    }

    public List<AdditionalCosts> getAdditionalCosts() {
        if(additionalCosts == null){
            return null;
        }
        return new ArrayList<>(additionalCosts);
    }

    public void setAdditionalCosts(List<AdditionalCosts> additionalCosts) {
        if(additionalCosts != null)
        {
            this.additionalCosts = new ArrayList<>(additionalCosts);
        } else {
            this.additionalCosts = null;
        }
    }

    public List<FleetManager> getFleetManagerList() {
        if(fleetManagerList == null){
            return null;
        }
        return new ArrayList<>(fleetManagerList);
    }

    public void setFleetManagerList(List<FleetManager> fleetManagerList) {
        if(fleetManagerList != null)
        {
            this.fleetManagerList = new ArrayList<>(fleetManagerList);
        } else {
            this.fleetManagerList = null;
        }
    }

    public AntiTheftService getAntiTheftService() {
        return antiTheftService;
    }

    public void setAntiTheftService(AntiTheftService antiTheftService) {
        this.antiTheftService = antiTheftService;
    }

    @Override
    public String toString() {
        return "Damaged{" +
                "contract=" + contract +
                ", customer=" + customer +
                ", insuranceCompany=" + insuranceCompany +
                ", driver=" + driver +
                ", vehicle=" + vehicle +
                ", fleetManagerList=" + fleetManagerList +
                ", isCaiSigned=" + isCaiSigned +
                ", impactPoint=" + impactPoint +
                ", additionalCosts=" + additionalCosts +
                ", antiTheftService=" + antiTheftService +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Damaged damaged = (Damaged) o;

        return new EqualsBuilder().append(contract, damaged.contract).append(customer, damaged.customer)
                .append(insuranceCompany, damaged.insuranceCompany).append(driver, damaged.driver)
                .append(vehicle, damaged.vehicle).append(fleetManagerList, damaged.fleetManagerList)
                .append(isCaiSigned, damaged.isCaiSigned).append(impactPoint, damaged.impactPoint)
                .append(additionalCosts, damaged.additionalCosts).append(antiTheftService, damaged.antiTheftService).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(contract).append(customer)
                .append(insuranceCompany).append(driver).append(vehicle).append(fleetManagerList).append(isCaiSigned)
                .append(impactPoint).append(additionalCosts).append(antiTheftService).toHashCode();
    }
}
