package com.doing.nemo.claims.entity.enumerated;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;

public enum FileExportTypeEnum implements Serializable {
    NEW_DAMAGED("new_damaged"),
    AGG_DAMAGED("agg_damaged"),
    PO_DAMAGED("po_damaged"),
    THEFT("theft");

    private static Logger LOGGER = LoggerFactory.getLogger(FileExportTypeEnum.class);
    private String fileExportType;

    private FileExportTypeEnum(String fileExportType) {
        this.fileExportType = fileExportType;
    }

    @JsonCreator
    public static FileExportTypeEnum create(String fileExportType) {

        fileExportType = fileExportType.replace(" - ", "_");
        fileExportType = fileExportType.replace('-', '_');
        fileExportType = fileExportType.replace(' ', '_');

        if (fileExportType != null) {
            for (FileExportTypeEnum val : FileExportTypeEnum.values()) {
                if (fileExportType.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        LOGGER.debug("Bad parameters exception. Enum class " + FileExportTypeEnum.class.getSimpleName() + " doesn't accept this value: " + fileExportType);
        throw new BadParametersException("Bad parameters exception. Enum class " + FileExportTypeEnum.class.getSimpleName() + " doesn't accept this value: " + fileExportType);
    }

    public String getValue() {
        return this.fileExportType.toUpperCase();
    }

    @JsonValue
    public String toValue() {
        for (FileExportTypeEnum val : FileExportTypeEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }


}
