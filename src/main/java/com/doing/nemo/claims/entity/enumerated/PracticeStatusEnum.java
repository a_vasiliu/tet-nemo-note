package com.doing.nemo.claims.entity.enumerated;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;

public enum PracticeStatusEnum implements Serializable {
    OPEN("open"),
    CLOSED("closed");

    private static Logger LOGGER = LoggerFactory.getLogger(PracticeStatusEnum.class);
    private String practiceTypeEnum;

    private PracticeStatusEnum(String practiceTypeEnum) {
        this.practiceTypeEnum = practiceTypeEnum;
    }

    @JsonCreator
    public static PracticeStatusEnum create(String practiceTypeEnum) {

        practiceTypeEnum = practiceTypeEnum.replace(" - ", "_");
        practiceTypeEnum = practiceTypeEnum.replace('-', '_');
        practiceTypeEnum = practiceTypeEnum.replace(' ', '_');

        if (practiceTypeEnum != null) {
            for (PracticeStatusEnum val : PracticeStatusEnum.values()) {
                if (practiceTypeEnum.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        LOGGER.debug("Bad parameters exception. Enum class " + PracticeStatusEnum.class.getSimpleName() + " doesn't accept this value: " + practiceTypeEnum);
        throw new BadParametersException("Bad parameters exception. Enum class " + PracticeStatusEnum.class.getSimpleName() + " doesn't accept this value: " + practiceTypeEnum);
    }

    public String getValue() {
        return this.practiceTypeEnum.toUpperCase();
    }

    @JsonValue
    public String toValue() {
        for (PracticeStatusEnum val : PracticeStatusEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }
}
