package com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.Date;
@JsonIgnoreProperties(ignoreUnknown = true)
public class Contract implements Serializable {

    private static final long serialVersionUID = 2045429742982722929L;

    @JsonProperty("contract_id")
    private Long contractId;

    @JsonProperty("status")
    private String status;

    @JsonProperty("contract_version_id")
    private Long contractVersionId;

    @JsonProperty("mileage")
    private Double mileage;

    @JsonProperty("duration")
    private Integer duration;

    @JsonProperty("contract_type")
    private String contractType;

    @JsonProperty("start_date")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date startDate;

    @JsonProperty("end_date")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date endDate;

    @JsonProperty("takein_date")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date takeinDate;

    @JsonProperty("leasing_company_id")
    private Long leasingCompanyId;

    @JsonProperty("leasing_company")
    private String leasingCompany;

    @JsonProperty("succeeding_contract_id")
    private Long succeedingContractId;

    @JsonProperty("fleet_vehicle_id")
    private Long fleetVehicleId;

    @JsonProperty("license_plate")
    private String licensePlate;

    public Long getContractId() {
        return contractId;
    }

    public void setContractId(Long contractId) {
        this.contractId = contractId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getContractVersionId() {
        return contractVersionId;
    }

    public void setContractVersionId(Long contractVersionId) {
        this.contractVersionId = contractVersionId;
    }

    public Double getMileage() {
        return mileage;
    }

    public void setMileage(Double mileage) {
        this.mileage = mileage;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getContractType() {
        return contractType;
    }

    public void setContractType(String contractType) {
        this.contractType = contractType;
    }

    public Date getStartDate() {
        if(startDate == null){
            return null;
        }
        return (Date)startDate.clone();
    }

    public void setStartDate(Date startDate) {
        if(startDate != null)
        {
            this.startDate =(Date)startDate.clone();
        } else {
            this.startDate = null;
        }
    }

    public Date getEndDate() {
        if(endDate == null){
            return null;
        }
        return (Date)endDate.clone();
    }

    public void setEndDate(Date endDate) {
        if(endDate != null)
        {
            this.endDate = (Date)endDate.clone();
        } else {
            this.endDate = null;
        }
    }

    public Date getTakeinDate() {
        if(takeinDate == null){
            return null;
        }
        return (Date)takeinDate.clone();
    }

    public void setTakeinDate(Date takeinDate) {
        if(takeinDate != null)
        {
            this.takeinDate = (Date)takeinDate.clone();
        } else {
            this.takeinDate = null;
        }
    }

    public Long getLeasingCompanyId() {
        return leasingCompanyId;
    }

    public void setLeasingCompanyId(Long leasingCompanyId) {
        this.leasingCompanyId = leasingCompanyId;
    }

    public String getLeasingCompany() {
        return leasingCompany;
    }

    public void setLeasingCompany(String leasingCompany) {
        this.leasingCompany = leasingCompany;
    }

    public Long getSucceedingContractId() {
        return succeedingContractId;
    }

    public void setSucceedingContractId(Long succeedingContractId) {
        this.succeedingContractId = succeedingContractId;
    }

    public Long getFleetVehicleId() {
        return fleetVehicleId;
    }

    public void setFleetVehicleId(Long fleetVehicleId) {
        this.fleetVehicleId = fleetVehicleId;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    @Override
    public String toString() {
        return "Contract{" +
                "contractId=" + contractId +
                ", status='" + status + '\'' +
                ", contractVersionId=" + contractVersionId +
                ", mileage=" + mileage +
                ", duration=" + duration +
                ", contractType='" + contractType + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", takeinDate=" + takeinDate +
                ", leasingCompanyId=" + leasingCompanyId +
                ", leasingCompany='" + leasingCompany + '\'' +
                ", succeedingContractId=" + succeedingContractId +
                ", fleetVehicleId=" + fleetVehicleId +
                ", licensePlate='" + licensePlate + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Contract contract = (Contract) o;

        return new EqualsBuilder().append(contractId, contract.contractId).append(status, contract.status)
                .append(contractVersionId, contract.contractVersionId).append(mileage, contract.mileage)
                .append(duration, contract.duration).append(contractType, contract.contractType)
                .append(startDate, contract.startDate).append(endDate, contract.endDate).append(takeinDate, contract.takeinDate)
                .append(leasingCompanyId, contract.leasingCompanyId).append(leasingCompany, contract.leasingCompany)
                .append(succeedingContractId, contract.succeedingContractId).append(fleetVehicleId, contract.fleetVehicleId)
                .append(licensePlate, contract.licensePlate).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(contractId).append(status)
                .append(contractVersionId).append(mileage).append(duration).append(contractType).append(startDate)
                .append(endDate).append(takeinDate).append(leasingCompanyId).append(leasingCompany).append(succeedingContractId)
                .append(fleetVehicleId).append(licensePlate).toHashCode();
    }
}
