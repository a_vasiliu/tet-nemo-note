package com.doing.nemo.claims.entity.settings;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "motivation_type", uniqueConstraints = @UniqueConstraint(
        name = "uc_motivation_type",
        columnNames = {
                "description"
        }))
public class MotivationTypeEntity implements Serializable {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )

    @Type(type = "org.hibernate.type.PostgresUUIDType")
    private UUID id;

    @Column(name = "motivation_type_id", unique = true, insertable = false, updatable = false)
    private Long motivationTypeId;

    @Column(name = "description")
    private String description;

    @Column(name = "order_id")
    private Integer orderId;

    @Column(name = "is_active")
    private Boolean isActive;

    public MotivationTypeEntity(Long motivationTypeId, String description, Integer orderId, Boolean isActive) {
        this.motivationTypeId = motivationTypeId;
        this.description = description;
        this.orderId = orderId;
        this.isActive = isActive;
    }

    public MotivationTypeEntity() {
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Long getMotivationTypeId() {
        return motivationTypeId;
    }

    public void setMotivationTypeId(Long motivationTypeId) {
        this.motivationTypeId = motivationTypeId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer order) {
        this.orderId = order;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    @Override
    public String toString() {
        return "MotivationTypeEntity{" +
                "id=" + id +
                ", motivationTypeId=" + motivationTypeId +
                ", description='" + description + '\'' +
                ", orderId=" + orderId +
                ", isActive=" + isActive +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MotivationTypeEntity that = (MotivationTypeEntity) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getMotivationTypeId(), that.getMotivationTypeId()) &&
                Objects.equals(getDescription(), that.getDescription()) &&
                Objects.equals(getOrderId(), that.getOrderId()) &&
                Objects.equals(getActive(), that.getActive());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getMotivationTypeId(), getDescription(), getOrderId(), getActive());
    }
}
