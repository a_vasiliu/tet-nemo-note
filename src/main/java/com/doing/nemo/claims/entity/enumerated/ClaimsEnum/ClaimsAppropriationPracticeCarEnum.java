package com.doing.nemo.claims.entity.enumerated.ClaimsEnum;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public enum ClaimsAppropriationPracticeCarEnum implements Serializable {
    APPROPRIATION("appropriation"),
    PRACTICE_CAR("practice_car");

    private static Logger LOGGER = LoggerFactory.getLogger(ClaimsAppropriationPracticeCarEnum.class);
    private String typrEntity;

    private ClaimsAppropriationPracticeCarEnum(String typeEntity) {
        this.typrEntity = typeEntity;
    }

    @JsonCreator
    public static ClaimsAppropriationPracticeCarEnum create(String typeEntity) {

        typeEntity = typeEntity.replace(" - ", "_");
        typeEntity = typeEntity.replace('-', '_');
        typeEntity = typeEntity.replace(' ', '_');

        if (typeEntity != null) {
            for (ClaimsAppropriationPracticeCarEnum val : ClaimsAppropriationPracticeCarEnum.values()) {
                if (typeEntity.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        LOGGER.debug("Bad parameters exception. Enum class " + ClaimsAppropriationPracticeCarEnum.class.getSimpleName() + " doesn't accept this value: " + typeEntity);
        throw new BadParametersException("Bad parameters exception. Enum class " + ClaimsAppropriationPracticeCarEnum.class.getSimpleName() + " doesn't accept this value: " + typeEntity);
    }

    public static List<ClaimsAppropriationPracticeCarEnum> getFlowListFromString(List<String> flowListString) {

        List<ClaimsAppropriationPracticeCarEnum> flowsList = new LinkedList<>();
        for (String att : flowListString) {
            flowsList.add(ClaimsAppropriationPracticeCarEnum.create(att));
        }
        return flowsList;
    }

    public String getValue() {
        return this.typrEntity.toUpperCase();
    }

    @JsonValue
    public String toValue() {
        for (ClaimsAppropriationPracticeCarEnum val : ClaimsAppropriationPracticeCarEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }
}
