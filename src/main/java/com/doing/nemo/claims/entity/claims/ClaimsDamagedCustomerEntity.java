package com.doing.nemo.claims.entity.claims;

import com.doing.nemo.claims.entity.ClaimsNewEntity;
import com.doing.nemo.claims.entity.jsonb.Address;
import com.doing.nemo.claims.entity.jsonb.dataType.JsonDataAddressType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@JsonIgnoreProperties({"hibernateLazyInitializer", "hibernate_lazy_initializer", "handler"})
@Entity
@Table(name = "claims_damaged_customer")
@TypeDefs({
        @TypeDef(name = "JsonDataAddressType", typeClass = JsonDataAddressType.class)
})
public class ClaimsDamagedCustomerEntity implements Serializable {

    @Id
    @Column(name = "id", updatable = false, nullable = false)
    private String id;

    @Column(name="customer_id")
    private Long customerId;

    @Column(name="trading_name")
    private String tradingName;

    @Column(name="status")
    private String status;

    @Column(name="legal_name")
    private String legalName;

    @Column(name="vat_number")
    private String vatNumber;

    @Column(name="official_registration")
    private String officialRegistration;

    @Column(name="email")
    private String email;

    @Column(name="phonenr")
    private String phonenr;

    @Column(name="pec")
    private String pec;

    @Column(name="main_address")
    @Type(type = "JsonDataAddressType")
    private Address mainAddress;

    @OneToOne
    @JoinColumn(name = "id")
    @MapsId("id")
    @PrimaryKeyJoinColumn
    private ClaimsNewEntity claim;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getTradingName() {
        return tradingName;
    }

    public void setTradingName(String tradingName) {
        this.tradingName = tradingName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLegalName() {
        return legalName;
    }

    public void setLegalName(String legalName) {
        this.legalName = legalName;
    }

    public String getVatNumber() {
        return vatNumber;
    }

    public void setVatNumber(String vatNumber) {
        this.vatNumber = vatNumber;
    }

    public String getOfficialRegistration() {
        return officialRegistration;
    }

    public void setOfficialRegistration(String officialRegistration) {
        this.officialRegistration = officialRegistration;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhonenr() {
        return phonenr;
    }

    public void setPhonenr(String phonenr) {
        this.phonenr = phonenr;
    }

    public String getPec() {
        return pec;
    }

    public void setPec(String pec) {
        this.pec = pec;
    }

    public Address getMainAddress() {
        return mainAddress;
    }

    public void setMainAddress(Address mainAddress) {
        this.mainAddress = mainAddress;
    }

    public ClaimsNewEntity getClaim() {
        return claim;
    }

    public void setClaim(ClaimsNewEntity claim) {
        this.claim = claim;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ClaimsDamagedCustomerEntity{");
        sb.append("id='").append(id).append('\'');
        sb.append(", customerId=").append(customerId);
        sb.append(", tradingName='").append(tradingName).append('\'');
        sb.append(", status='").append(status).append('\'');
        sb.append(", legalName='").append(legalName).append('\'');
        sb.append(", vatNumber='").append(vatNumber).append('\'');
        sb.append(", officialRegistration='").append(officialRegistration).append('\'');
        sb.append(", email='").append(email).append('\'');
        sb.append(", phonenr='").append(phonenr).append('\'');
        sb.append(", pec='").append(pec).append('\'');
        sb.append(", mainAddress=").append(mainAddress);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClaimsDamagedCustomerEntity that = (ClaimsDamagedCustomerEntity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(customerId, that.customerId) &&
                Objects.equals(tradingName, that.tradingName) &&
                Objects.equals(status, that.status) &&
                Objects.equals(legalName, that.legalName) &&
                Objects.equals(vatNumber, that.vatNumber) &&
                Objects.equals(officialRegistration, that.officialRegistration) &&
                Objects.equals(email, that.email) &&
                Objects.equals(phonenr, that.phonenr) &&
                Objects.equals(pec, that.pec) &&
                Objects.equals(mainAddress, that.mainAddress) &&
                Objects.equals(claim, that.claim);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, customerId, tradingName, status, legalName, vatNumber, officialRegistration, email, phonenr, pec, mainAddress, claim);
    }
}
