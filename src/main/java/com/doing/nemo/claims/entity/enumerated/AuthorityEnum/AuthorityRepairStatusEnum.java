package com.doing.nemo.claims.entity.enumerated.AuthorityEnum;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;

public enum AuthorityRepairStatusEnum implements Serializable {
    LIQUIDATION("liquidation"),
    REPARATION("reparation"),
    REJECTION("rejection");

    private static Logger LOGGER = LoggerFactory.getLogger(AuthorityRepairStatusEnum.class);
    private String authorityRepairStatus;

    private AuthorityRepairStatusEnum(String authorityRepairStatus) {
        this.authorityRepairStatus = authorityRepairStatus;
    }

    @JsonCreator
    public static AuthorityRepairStatusEnum create(String authorityRepairStatus) {

        authorityRepairStatus = authorityRepairStatus.replace(" - ", "_");
        authorityRepairStatus = authorityRepairStatus.replace('-', '_');
        authorityRepairStatus = authorityRepairStatus.replace(' ', '_');

        if (authorityRepairStatus != null) {
            for (AuthorityRepairStatusEnum val : AuthorityRepairStatusEnum.values()) {
                if (authorityRepairStatus.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        LOGGER.debug("Bad parameters exception. Enum class " + AuthorityRepairStatusEnum.class.getSimpleName() + " doesn't accept this value: " + authorityRepairStatus);
        throw new BadParametersException("Bad parameters exception. Enum class " + AuthorityRepairStatusEnum.class.getSimpleName() + " doesn't accept this value: " + authorityRepairStatus);
    }

    public String getValue() {
        return this.authorityRepairStatus.toUpperCase();
    }

    @JsonValue
    public String toValue() {
        for (AuthorityRepairStatusEnum val : AuthorityRepairStatusEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }
}
