package com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbInsuranceCompany;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Tpl implements Serializable {

    private static final long serialVersionUID = -3536918114361935945L;
    @JsonProperty("service_id")
    private String serviceId;
    @JsonProperty("service")
    private String service;
    @JsonProperty("max_coverage")
    private String maxCoverage;
    @JsonProperty("deductible")
    private String deductible;
    @JsonProperty("company_id")
    private String companyId;
    @JsonProperty("company")
    private String company;
    @JsonProperty("tariff_id")
    private String tariffId;
    @JsonProperty("tariff")
    private String tariff;
    @JsonProperty("tariff_code")
    private String tariffCode;
    @JsonProperty("start_date")
    private String startDate;
    @JsonProperty("end_date")
    private String endDate;
    @JsonProperty("policy_number")
    private String policyNumber;
    @JsonProperty("company_description")
    private String companyDescription;


    @JsonProperty("insurance_company_id")
    private UUID insuranceCompanyId;

    @JsonProperty("insurance_policy_id")
    private UUID insurancePolicyId;

    public UUID getInsurancePolicyId() {
        return insurancePolicyId;
    }

    public void setInsurancePolicyId(UUID insurancePolicyId) {
        this.insurancePolicyId = insurancePolicyId;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getMaxCoverage() {
        return maxCoverage;
    }

    public void setMaxCoverage(String maxCoverage) {
        this.maxCoverage = maxCoverage;
    }

    public String getDeductible() {
        return deductible;
    }

    public void setDeductible(String deductible) {
        this.deductible = deductible;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    @JsonProperty("tariff_id")
    public String getTariffId() {
        return tariffId;
    }

    @JsonProperty("tarifid")
    public void setTariffId(String tariffId) {
        this.tariffId = tariffId;
    }

    public String getTariff() {
        return tariff;
    }


    public void setTariff(String tariff) {
        this.tariff = tariff;
    }

    @JsonProperty("tariff_code")
    public String getTariffCode() {
        return tariffCode;
    }

    @JsonProperty("tarifcode")
    public void setTariffCode(String tariffCode) {
        this.tariffCode = tariffCode;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    @JsonProperty("start_date")
    public String getStartDate() {
        return startDate;
    }

    @JsonProperty("startdate")
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    @JsonProperty("end_date")
    public String getEndDate() {
        return endDate;
    }

    @JsonProperty("enddate")
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getCompanyDescription() {
        return companyDescription;
    }

    public void setCompanyDescription(String companyDescription) {
        this.companyDescription = companyDescription;
    }

    public UUID getInsuranceCompanyId() {
        return insuranceCompanyId;
    }

    public void setInsuranceCompanyId(UUID insuranceCompanyId) {
        this.insuranceCompanyId = insuranceCompanyId;
    }

    @Override
    public String toString() {
        return "Tpl{" +
                "serviceId='" + serviceId + '\'' +
                ", service='" + service + '\'' +
                ", maxCoverage='" + maxCoverage + '\'' +
                ", deductible='" + deductible + '\'' +
                ", companyId='" + companyId + '\'' +
                ", company='" + company + '\'' +
                ", tariffId='" + tariffId + '\'' +
                ", tariff='" + tariff + '\'' +
                ", tariffCode='" + tariffCode + '\'' +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", policyNumber='" + policyNumber + '\'' +
                ", companyDescription='" + companyDescription + '\'' +
                ", insuranceCompanyId=" + insuranceCompanyId +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Tpl tpl = (Tpl) o;

        return new EqualsBuilder().append(serviceId, tpl.serviceId).append(service, tpl.service)
                .append(maxCoverage, tpl.maxCoverage).append(deductible, tpl.deductible)
                .append(companyId, tpl.companyId).append(company, tpl.company).append(tariffId, tpl.tariffId)
                .append(tariff, tpl.tariff).append(tariffCode, tpl.tariffCode).append(startDate, tpl.startDate)
                .append(endDate, tpl.endDate).append(policyNumber, tpl.policyNumber).append(companyDescription, tpl.companyDescription)
                .append(insuranceCompanyId, tpl.insuranceCompanyId).append(insurancePolicyId, tpl.insurancePolicyId).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(serviceId).append(service)
                .append(maxCoverage).append(deductible).append(companyId).append(company).append(tariffId)
                .append(tariff).append(tariffCode).append(startDate).append(endDate).append(policyNumber)
                .append(companyDescription).append(insuranceCompanyId).append(insurancePolicyId).toHashCode();
    }
}
