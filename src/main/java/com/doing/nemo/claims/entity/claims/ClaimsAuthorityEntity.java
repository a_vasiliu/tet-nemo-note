package com.doing.nemo.claims.entity.claims;

import com.doing.nemo.claims.entity.ClaimsAuthorityEmbeddedEntity;
import com.doing.nemo.claims.entity.enumerated.AuthorityEnum.AuthorityEventTypeEnum;
import com.doing.nemo.claims.entity.enumerated.AuthorityEnum.AuthorityStatusEnum;
import com.doing.nemo.claims.entity.enumerated.AuthorityEnum.AuthorityTypeEnum;
import com.doing.nemo.claims.entity.enumerated.WorkingStatusEnum;
import com.doing.nemo.claims.entity.jsonb.*;
import com.doing.nemo.claims.entity.jsonb.dataType.JsonDataAddressType;
import com.doing.nemo.claims.entity.jsonb.dataType.JsonDataCommodityDetailsType;
import com.doing.nemo.claims.entity.jsonb.dataType.JsonDataHistoricalAuthorityType;
import com.doing.nemo.claims.entity.jsonb.dataType.JsonDataPoDetailsType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;

@JsonIgnoreProperties({"hibernateLazyInitializer", "hibernate_lazy_initializer", "handler"})
@Entity
@Table(name = "claims_authority")
@TypeDefs({
        @TypeDef(name = "JsonDataHistoricalAuthorityType", typeClass = JsonDataHistoricalAuthorityType.class),
        @TypeDef(name = "JsonDataCommodityDetailsType", typeClass = JsonDataCommodityDetailsType.class),
        @TypeDef(name = "JsonDataPoDetailsType", typeClass = JsonDataPoDetailsType.class)
})
public class ClaimsAuthorityEntity implements Serializable {

    @Id
    @Column(name = "id", updatable = false, nullable = false)
    private String id;

    @OneToMany(mappedBy = "claimsAuthorityEntity", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference(value = "claims-authority-entity")
    private List<ClaimsAuthorityEmbeddedEntity> claimsAuthorityEntities;
    //endregion AUTHORITY

    @Column(name="is_invoiced")
    private Boolean isInvoiced = false;

    @Column(name="is_migrated")
    private Boolean isMigrated;

    @Column(name="authority_dossier_id")
    private String authorityDossierId;

    @Column(name="authority_working_id")
    private String authorityWorkingId;

    @Column(name="working_number")
    private String workingNumber;

    @Column(name="authority_dossier_number")
    private String authorityDossierNumber;

    @Column(name="total")
    private Double total;

    //@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    @Column(name="accepting_date")
    private Instant acceptingDate;

    //@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    @Column(name="authorization_date")
    private Instant authorizationDate;

    //@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    @Column(name="rejection_date")
    private Instant rejectionDate;

    @Column(name="rejection")
    private Boolean rejection;

    @Column(name="note_rejection")
    private String noteRejection;

    //@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    @Column(name="working_created_at")
    private Instant workingCreatedAt;

    @Column(name="franchise_number")
    private Long numberFranchise;

    @Column(name="is_wreck")
    private Boolean isWreck;

    @Column(name="wreck_casual")
    private String wreckCasual;

    //@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    @Column(name="event_date")
    private Instant eventDate;

    @Column(name="event_type")
    @Enumerated(EnumType.STRING)
    private AuthorityEventTypeEnum eventType;

    @Column(name="po_details")
    @Type(type = "JsonDataPoDetailsType")
    private List<PODetails> poDetails;

    @Column(name="status")
    @Enumerated(EnumType.STRING)
    private AuthorityStatusEnum status;

    @Column(name="type")
    @Enumerated(EnumType.STRING)
    private AuthorityTypeEnum type;

    @Column(name="is_not_duplicate")
    private Boolean isNotDuplicate;

    @Column(name="oldest")
    private Boolean oldest;

    @Column(name="commodity_details")
    @Type(type = "JsonDataCommodityDetailsType")
    private CommodityDetails commodityDetails;

    @Column(name="working_status")
    @Enumerated(EnumType.STRING)
    private WorkingStatusEnum workingStatus;

    @Column(name="user_id")
    private String userId;

    @Column(name="firstname")
    private String firstname;

    @Column(name="lastname")
    private String lastname;

    @Column(name="NBV")
    private BigDecimal nbv;

    @Column(name="wreck_value")
    private String wreckValue;

    @Column(name="claims_linked_size")
    private Long claimsLinkedSize;

    @Column(name="historical")
    @Type(type = "JsonDataHistoricalAuthorityType")
    private List<HistoricalAuthority> historical;

    @Column(name="is_msa_downloaded")
    private Boolean isMsaDownloaded;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Boolean getInvoiced() {
        return isInvoiced;
    }

    public void setInvoiced(Boolean invoiced) {
        isInvoiced = invoiced;
    }

    public Boolean getMigrated() {
        return isMigrated;
    }

    public void setMigrated(Boolean migrated) {
        isMigrated = migrated;
    }

    public String getAuthorityDossierId() {
        return authorityDossierId;
    }

    public void setAuthorityDossierId(String authorityDossierId) {
        this.authorityDossierId = authorityDossierId;
    }

    public String getAuthorityWorkingId() {
        return authorityWorkingId;
    }

    public void setAuthorityWorkingId(String authorityWorkingId) {
        this.authorityWorkingId = authorityWorkingId;
    }

    public String getWorkingNumber() {
        return workingNumber;
    }

    public void setWorkingNumber(String workingNumber) {
        this.workingNumber = workingNumber;
    }

    public String getAuthorityDossierNumber() {
        return authorityDossierNumber;
    }

    public void setAuthorityDossierNumber(String authorityDossierNumber) {
        this.authorityDossierNumber = authorityDossierNumber;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Instant getAcceptingDate() {
        return acceptingDate;
    }

    public void setAcceptingDate(Instant acceptingDate) {
        this.acceptingDate = acceptingDate;
    }

    public Instant getAuthorizationDate() {
        return authorizationDate;
    }

    public void setAuthorizationDate(Instant authorizationDate) {
        this.authorizationDate = authorizationDate;
    }

    public Instant getRejectionDate() {
        return rejectionDate;
    }

    public void setRejectionDate(Instant rejectionDate) {
        this.rejectionDate = rejectionDate;
    }

    public Boolean getRejection() {
        return rejection;
    }

    public void setRejection(Boolean rejection) {
        this.rejection = rejection;
    }

    public String getNoteRejection() {
        return noteRejection;
    }

    public void setNoteRejection(String noteRejection) {
        this.noteRejection = noteRejection;
    }

    public Instant getWorkingCreatedAt() {
        return workingCreatedAt;
    }

    public void setWorkingCreatedAt(Instant workingCreatedAt) {
        this.workingCreatedAt = workingCreatedAt;
    }

    public Long getNumberFranchise() {
        return numberFranchise;
    }

    public void setNumberFranchise(Long numberFranchise) {
        this.numberFranchise = numberFranchise;
    }

    public Boolean getWreck() {
        return isWreck;
    }

    public void setWreck(Boolean wreck) {
        isWreck = wreck;
    }

    public String getWreckCasual() {
        return wreckCasual;
    }

    public void setWreckCasual(String wreckCasual) {
        this.wreckCasual = wreckCasual;
    }

    public Instant getEventDate() {
        return eventDate;
    }

    public void setEventDate(Instant eventDate) {
        this.eventDate = eventDate;
    }

    public AuthorityEventTypeEnum getEventType() {
        return eventType;
    }

    public void setEventType(AuthorityEventTypeEnum eventType) {
        this.eventType = eventType;
    }

    public List<PODetails> getPoDetails() {
        return poDetails;
    }

    public void setPoDetails(List<PODetails> poDetails) {
        this.poDetails = poDetails;
    }

    public AuthorityStatusEnum getStatus() {
        return status;
    }

    public void setStatus(AuthorityStatusEnum status) {
        this.status = status;
    }

    public AuthorityTypeEnum getType() {
        return type;
    }

    public void setType(AuthorityTypeEnum type) {
        this.type = type;
    }

    public Boolean getNotDuplicate() {
        return isNotDuplicate;
    }

    public void setNotDuplicate(Boolean notDuplicate) {
        isNotDuplicate = notDuplicate;
    }

    public Boolean getOldest() {
        return oldest;
    }

    public void setOldest(Boolean oldest) {
        this.oldest = oldest;
    }

    public CommodityDetails getCommodityDetails() {
        return commodityDetails;
    }

    public void setCommodityDetails(CommodityDetails commodityDetails) {
        this.commodityDetails = commodityDetails;
    }

    public WorkingStatusEnum getWorkingStatus() {
        return workingStatus;
    }

    public void setWorkingStatus(WorkingStatusEnum workingStatus) {
        this.workingStatus = workingStatus;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public BigDecimal getNbv() {
        return nbv;
    }

    public void setNbv(BigDecimal nbv) {
        this.nbv = nbv;
    }

    public String getWreckValue() {
        return wreckValue;
    }

    public void setWreckValue(String wreckValue) {
        this.wreckValue = wreckValue;
    }

    public Long getClaimsLinkedSize() {
        return claimsLinkedSize;
    }

    public void setClaimsLinkedSize(Long claimsLinkedSize) {
        this.claimsLinkedSize = claimsLinkedSize;
    }

    public List<HistoricalAuthority> getHistorical() {
        return historical;
    }

    public void setHistorical(List<HistoricalAuthority> historical) {
        this.historical = historical;
    }

    public Boolean getMsaDownloaded() {
        return isMsaDownloaded;
    }

    public void setMsaDownloaded(Boolean msaDownloaded) {
        isMsaDownloaded = msaDownloaded;
    }

    public List<ClaimsAuthorityEmbeddedEntity> getClaimsAuthorityEntities() {
        return claimsAuthorityEntities;
    }

    public void setClaimsAuthorityEntities(List<ClaimsAuthorityEmbeddedEntity> claimsAuthorityEntities) {
        this.claimsAuthorityEntities = claimsAuthorityEntities;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ClaimsAuthorityEntity{");
        sb.append("id='").append(id).append('\'');
        sb.append(", isInvoiced=").append(isInvoiced);
        sb.append(", isMigrated=").append(isMigrated);
        sb.append(", authorityDossierId='").append(authorityDossierId).append('\'');
        sb.append(", authorityWorkingId='").append(authorityWorkingId).append('\'');
        sb.append(", workingNumber='").append(workingNumber).append('\'');
        sb.append(", authorityDossierNumber='").append(authorityDossierNumber).append('\'');
        sb.append(", total=").append(total);
        sb.append(", acceptingDate=").append(acceptingDate);
        sb.append(", authorizationDate=").append(authorizationDate);
        sb.append(", rejectionDate=").append(rejectionDate);
        sb.append(", rejection=").append(rejection);
        sb.append(", noteRejection='").append(noteRejection).append('\'');
        sb.append(", workingCreatedAt=").append(workingCreatedAt);
        sb.append(", numberFranchise=").append(numberFranchise);
        sb.append(", isWreck=").append(isWreck);
        sb.append(", wreckCasual='").append(wreckCasual).append('\'');
        sb.append(", eventDate=").append(eventDate);
        sb.append(", eventType=").append(eventType);
        sb.append(", poDetails=").append(poDetails);
        sb.append(", status=").append(status);
        sb.append(", type=").append(type);
        sb.append(", isNotDuplicate=").append(isNotDuplicate);
        sb.append(", oldest=").append(oldest);
        sb.append(", commodityDetails=").append(commodityDetails);
        sb.append(", workingStatus=").append(workingStatus);
        sb.append(", userId='").append(userId).append('\'');
        sb.append(", firstname='").append(firstname).append('\'');
        sb.append(", lastname='").append(lastname).append('\'');
        sb.append(", nbv=").append(nbv);
        sb.append(", wreckValue='").append(wreckValue).append('\'');
        sb.append(", claimsLinkedSize=").append(claimsLinkedSize);
        sb.append(", historical=").append(historical);
        sb.append(", isMsaDownloaded=").append(isMsaDownloaded);
        sb.append('}');
        return sb.toString();
    }
}
