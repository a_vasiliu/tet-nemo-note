package com.doing.nemo.claims.entity.settings;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
@Table(name = "insurance_company",
        uniqueConstraints = @UniqueConstraint(
                name = "uc_insurancecompany",
                columnNames =
                        {
                                "name"
                        }
        ))
public class InsuranceCompanyEntity implements Serializable {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )

    @Type(type = "org.hibernate.type.PostgresUUIDType")
    private UUID id;

    @Column(name = "code", unique = true, insertable = false, updatable = false)
    private Long code;

    @Column(name = "name")
    private String name;

    @Column(name = "address")
    private String address;

    @Column(name = "zip_code")
    private String zipCode;

    @Column(name = "city")
    private String city;

    @Column(name = "province")
    private String province;

    @Column(name = "state")
    private String state;

    @Column(name = "telephone")
    private String telephone;

    @Column(name = "fax")
    private String fax;

    @Column(name = "email")
    private String email;

    @Column(name = "web_site")
    private String webSite;

    @Column(name = "contact")
    private String contact;

    @Column(name = "ania_code")
    private Integer aniaCode;

    @Column(name = "join_card")
    private Boolean joinCard;

    @Column(name = "contact_pai")
    private String contactPai;

    @Column(name = "email_pai")
    private String emailPai;

    @Column(name = "is_active")
    private Boolean isActive;

    public InsuranceCompanyEntity() {
    }

    public InsuranceCompanyEntity(Long code, String name, String address, String zipCode, String city, String province, String state, String telephone, String fax, String email, String webSite, String contact, Integer aniaCode, Boolean joinCard, String contactPai, String emailPai, Boolean isActive) {
        this.code = code;
        this.name = name;
        this.address = address;
        this.zipCode = zipCode;
        this.city = city;
        this.province = province;
        this.state = state;
        this.telephone = telephone;
        this.fax = fax;
        this.email = email;
        this.webSite = webSite;
        this.contact = contact;
        this.aniaCode = aniaCode;
        this.joinCard = joinCard;
        this.contactPai = contactPai;
        this.emailPai = emailPai;
        this.isActive = isActive;
    }

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebSite() {
        return webSite;
    }

    public void setWebSite(String webSite) {
        this.webSite = webSite;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public Integer getAniaCode() {
        return aniaCode;
    }

    public void setAniaCode(Integer aniaCode) {
        this.aniaCode = aniaCode;
    }

    public Boolean getJoinCard() {
        return joinCard;
    }

    public void setJoinCard(Boolean joinCard) {
        this.joinCard = joinCard;
    }

    public String getContactPai() {
        return contactPai;
    }

    public void setContactPai(String contactPai) {
        this.contactPai = contactPai;
    }

    public String getEmailPai() {
        return emailPai;
    }

    public void setEmailPai(String emailPai) {
        this.emailPai = emailPai;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    @Override
    public String toString() {
        return "InsuranceCompanyEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", zipCode=" + zipCode +
                ", city='" + city + '\'' +
                ", province='" + province + '\'' +
                ", state='" + state + '\'' +
                ", telephone='" + telephone + '\'' +
                ", fax='" + fax + '\'' +
                ", email='" + email + '\'' +
                ", webSite='" + webSite + '\'' +
                ", contact='" + contact + '\'' +
                ", aniaCode=" + aniaCode +
                ", joinCard=" + joinCard +
                ", contactPai='" + contactPai + '\'' +
                ", emailPai='" + emailPai + '\'' +
                ", isActive=" + isActive +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InsuranceCompanyEntity that = (InsuranceCompanyEntity) o;
        return Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
