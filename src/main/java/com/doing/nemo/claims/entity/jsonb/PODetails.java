package com.doing.nemo.claims.entity.jsonb;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PODetails implements Serializable {

    @JsonProperty("created_at")
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    private Date createdAt;

    @JsonProperty("po_number")
    private String poNumber;

    @JsonProperty("po_amount")
    private BigDecimal poAmount;

    @JsonProperty("po_target")
    private String poTarget;

    @JsonProperty("id_filemanager")
    private String idFilemanager;

    public Date getCreatedAt() {
        if(createdAt == null){
            return null;
        }
        return (Date)createdAt.clone();
    }

    public void setCreatedAt(Date createdAt) {
        if(createdAt != null)
        {
            this.createdAt =  (Date)createdAt.clone();
        } else {
            this.createdAt = null;
        }
    }

    public String getPoNumber() {
        return poNumber;
    }

    public void setPoNumber(String poNumber) {
        this.poNumber = poNumber;
    }

    public BigDecimal getPoAmount() {
        return poAmount;
    }

    public void setPoAmount(BigDecimal poAmount) {
        this.poAmount = poAmount;
    }

    public String getPoTarget() {
        return poTarget;
    }

    public void setPoTarget(String poTarget) {
        this.poTarget = poTarget;
    }

    public String getIdFilemanager() {
        return idFilemanager;
    }

    public void setIdFilemanager(String idFilemanager) {
        this.idFilemanager = idFilemanager;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        PODetails poDetails = (PODetails) o;

        return new EqualsBuilder().append(createdAt, poDetails.createdAt).append(poNumber, poDetails.poNumber)
                .append(poAmount, poDetails.poAmount).append(poTarget, poDetails.poTarget).append(idFilemanager, poDetails.idFilemanager).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(createdAt).append(poNumber)
                .append(poAmount).append(poTarget).append(idFilemanager).toHashCode();
    }
}
