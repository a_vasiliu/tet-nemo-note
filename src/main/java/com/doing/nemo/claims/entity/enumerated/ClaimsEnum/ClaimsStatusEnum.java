package com.doing.nemo.claims.entity.enumerated.ClaimsEnum;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public enum ClaimsStatusEnum implements Serializable {
    WAITING_FOR_VALIDATION("waiting_for_validation"),
    WAITING_FOR_AUTHORITY("waiting_for_authority"),
    DRAFT("draft"),
    CHECK_DRAFT("check_draft"),
    DELETED("deleted"),
    TO_ENTRUST("to_entrust"),
    CLOSED("closed"),
    CLOSED_PRACTICE("closed_practice"),
    INCOMPLETE("incomplete"),
    ENTRUSTED_AT("entrusted_at"),
    REJECTED("rejected"),
    MANAGED("managed"),
    PROPOSED_ACCEPTED("proposed_accepted"),
    WAIT_LOST_POSSESSION("wait_lost_possession"),
    LOST_POSSESSION("lost_possession"),
    WAIT_TO_RETURN_POSSESSION("wait_to_return_possession"),
    RETURN_TO_POSSESSION("return_to_possession"),
    WAITING_FOR_REFUND("waiting_for_refund"),
    KASKO("kasko"),
    TO_REREGISTER("to_reregister"),
    BOOK_DUPLICATION("book_duplication"),
    STAMP("stamp"),
    DEMOLITION("demolition"),
    RECEPTIONS_TO_BE_CONFIRMED("receptions_to_be_confirmed"),
    TO_WORK("to_work"),
    CLOSED_PARTIAL_REFUND("closed_partial_refund"),
    CLOSED_TOTAL_REFUND("closed_total_refund"),
    SEND_TO_CLIENT("send_to_client"),
    VALIDATED("validated"),
    STAMP_CLOSED("stamp_closed"),
    DEMOLITION_CLOSED("demolition_closed"),
    BOOK_DUPLICATION_CLOSED("book_duplication_closed"),
    TO_REREGISTER_CLOSED("to_reregister_closed"),
    TO_SEND_TO_CUSTOMER("to_send_to_customer"),
    PO_VARIATION("po_variation"),
    WAITING_FOR_NUMBER_SX("waiting_for_number_sx"),

    /* Per il MyAld */
    ACCEPTED("accepted"),
    UNDER_PROCESSING("under processing");

    private static Logger LOGGER = LoggerFactory.getLogger(ClaimsStatusEnum.class);
    private String statusEntity;

    private ClaimsStatusEnum(String statusEntity) {
        this.statusEntity = statusEntity;
    }

    @JsonCreator
    public static ClaimsStatusEnum create(String statusEntity) {

        statusEntity = statusEntity.replace(" - ", "_");
        statusEntity = statusEntity.replace('-', '_');
        statusEntity = statusEntity.replace(' ', '_');

        if (statusEntity != null) {
            for (ClaimsStatusEnum val : ClaimsStatusEnum.values()) {
                if (statusEntity.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        LOGGER.debug("Bad parameters exception. Enum class " + ClaimsStatusEnum.class.getSimpleName() + " doesn't accept this value: " + statusEntity);
        throw new BadParametersException("Bad parameters exception. Enum class " + ClaimsStatusEnum.class.getSimpleName() + " doesn't accept this value: " + statusEntity);
    }

    public static List<ClaimsStatusEnum> getStatusListFromString(List<String> statusListString) {

        List<ClaimsStatusEnum> statusList = new LinkedList<>();
        for (String att : statusListString) {
            statusList.add(ClaimsStatusEnum.create(att));
        }
        return statusList;
    }

    public String getValue() {
        return this.statusEntity.toUpperCase();
    }

    @JsonValue
    public String toValue() {
        for (ClaimsStatusEnum val : ClaimsStatusEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }

    public static List<String> getStringListFromStatusList(List<ClaimsStatusEnum> statusList) {

        List<String> statusListString = new LinkedList<>();
        for (ClaimsStatusEnum att : statusList) {
            statusListString.add(att.getValue());
        }
        return statusListString;
    }
}

