package com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbInsuranceCompany;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MaterialDamage implements Serializable {

    private static final long serialVersionUID = 4382252490710091224L;

    @JsonProperty("service_id")
    private String serviceId;
    @JsonProperty("service")
    private String service;
    @JsonProperty("deductible_id")
    private String deductibleId;
    @JsonProperty("deductible")
    private String deductible;

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getDeductibleId() {
        return deductibleId;
    }

    public void setDeductibleId(String deductibleId) {
        this.deductibleId = deductibleId;
    }

    public String getDeductible() {
        return deductible;
    }

    public void setDeductible(String deductible) {
        this.deductible = deductible;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        MaterialDamage that = (MaterialDamage) o;

        return new EqualsBuilder().append(serviceId, that.serviceId).append(service, that.service)
                .append(deductibleId, that.deductibleId).append(deductible, that.deductible).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(serviceId).append(service)
                .append(deductibleId).append(deductible).toHashCode();
    }
}
