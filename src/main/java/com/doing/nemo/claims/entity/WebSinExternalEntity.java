package com.doing.nemo.claims.entity;

import com.doing.nemo.claims.entity.jsonb.dataType.JsonDataPutComplaiantRequest;
import com.doing.nemo.claims.websin.websinws.PutComplaintRequest;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("WEBSIN")
@TypeDefs({
        @TypeDef(name = "JsonDataPutComplaiantRequest", typeClass = JsonDataPutComplaiantRequest.class)
})
public class WebSinExternalEntity extends ExternalCommunicationEntity {

    @Type(type = "JsonDataPutComplaiantRequest")
    @Column(name = "request")
    PutComplaintRequest request;

    public PutComplaintRequest getRequest() {
        return request;
    }

    public void setRequest(PutComplaintRequest request) { this.request = request; }
}