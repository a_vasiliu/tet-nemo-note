package com.doing.nemo.claims.entity.jsonb.cai;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Cai implements Serializable {

    private static final long serialVersionUID = 7216293277047655147L;

    @JsonProperty("A")
    private CaiDetails vehicleA;

    @JsonProperty("B")
    private CaiDetails vehicleB;

    @JsonProperty("driver_side")
    private String driverSide;

    @JsonProperty("note")
    private String note;

    public Cai() {
    }

    public Cai(CaiDetails vehicleA, CaiDetails vehicleB, String driverSide, String note) {
        this.vehicleA = vehicleA;
        this.vehicleB = vehicleB;
        this.driverSide = driverSide;
        this.note = note;
    }

    public CaiDetails getVehicleA() {
        return vehicleA;
    }

    public void setVehicleA(CaiDetails vehicleA) {
        this.vehicleA = vehicleA;
    }

    public CaiDetails getVehicleB() {
        return vehicleB;
    }

    public void setVehicleB(CaiDetails vehicleB) {
        this.vehicleB = vehicleB;
    }

    public String getDriverSide() {
        return driverSide;
    }

    public void setDriverSide(String driverSide) {
        this.driverSide = driverSide;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    @Override
    public String toString() {
        return "Cai{" +
                "vehicleA=" + vehicleA +
                ", vehicleB=" + vehicleB +
                ", driverSide='" + driverSide + '\'' +
                ", note='" + note + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Cai cai = (Cai) o;

        return new EqualsBuilder().append(vehicleA, cai.vehicleA)
                .append(vehicleB, cai.vehicleB).append(driverSide, cai.driverSide).append(note, cai.note).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(vehicleA)
                .append(vehicleB).append(driverSide).append(note).toHashCode();
    }
}
