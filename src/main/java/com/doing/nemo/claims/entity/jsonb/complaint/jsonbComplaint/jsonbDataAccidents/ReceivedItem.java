package com.doing.nemo.claims.entity.jsonb.complaint.jsonbComplaint.jsonbDataAccidents;

import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.ReceivedItem.ItemToReceiveTypeEnum;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.Date;


@JsonIgnoreProperties(ignoreUnknown = true)
public class ReceivedItem implements Serializable {

    private static final long serialVersionUID = 1699736087145326835L;

    @JsonProperty("is_received")
    private Boolean isReceived;

    @JsonProperty("type")
    private ItemToReceiveTypeEnum type;

    @JsonProperty("date_received")
    private Date dateReceived;

    @JsonProperty("report")
    private String report;

    @JsonProperty("note")
    private String note;

    public Boolean getReceived() {
        return isReceived;
    }

    public void setReceived(Boolean received) {
        isReceived = received;
    }

    public ItemToReceiveTypeEnum getType() {
        return type;
    }

    public void setType(ItemToReceiveTypeEnum type) {
        this.type = type;
    }

    public Date getDateReceived() {
        if(dateReceived == null){
            return null;
        }
        return (Date)dateReceived.clone();
    }

    public void setDateReceived(Date dateReceived) {
        if(dateReceived != null)
        {
            this.dateReceived = (Date)dateReceived.clone();
        } else {
            this.dateReceived = null;
        }
    }

    public String getReport() {
        return report;
    }

    public void setReport(String report) {
        this.report = report;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public String toString() {
        return "ReceivedItem{" +
                "isReceived=" + isReceived +
                ", type=" + type +
                ", dateReceived=" + dateReceived +
                ", report='" + report + '\'' +
                ", note='" + note + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        ReceivedItem that = (ReceivedItem) o;

        return new EqualsBuilder().append(isReceived, that.isReceived).append(type, that.type)
                .append(dateReceived, that.dateReceived).append(report, that.report).append(note, that.note).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(isReceived)
                .append(type).append(dateReceived).append(report).append(note).toHashCode();
    }
}
