package com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public enum DataAccidentTypeAccidentEnum implements Serializable {
    RC_NON_VERIFICABILE("rc_non_verificabile"),
    RC_ATTIVA("rc_attiva"),
    RC_PASSIVA("rc_passiva"),
    RC_CONCORSUALE("rc_concorsuale"),
    CARD_NON_VERIFICABILE_FIRMA_SINGOLA("card_non_verificabile_firma_singola"),
    CARD_ATTIVA_FIRMA_SINGOLA("card_attiva_firma_singola"),
    CARD_PASSIVA_FIRMA_SINGOLA("card_passiva_firma_singola"),
    CARD_CONCORSUALE_FIRMA_SINGOLA("card_concorsuale_firma_singola"),
    CARD_NON_VERIFICABILE_DOPPIA_FIRMA("card_non_verificabile_doppia_firma"),
    CARD_ATTIVA_DOPPIA_FIRMA("card_attiva_doppia_firma"),
    CARD_PASSIVA_DOPPIA_FIRMA("card_passiva_doppia_firma"),
    CARD_CONCORSUALE_DOPPIA_FIRMA("card_concorsuale_doppia_firma"),
    KASKO_URTO_CONTRO_OGGETTI_FISSI("kasko_urto_contro_oggetti_fissi"),
    ATTO_VANDALICO("atto_vandalico"),
    DANNO_RITROVATO_IN_PARCHEGGIO("danno_ritrovato_in_parcheggio"),
    EVENTI_NATURALI("eventi_naturali"),
    CRISTALLI("cristalli"),
    INCENDIO("incendio"),
    CHIAVI_SMARRITE_O_RUBATE("chiavi_smarrite_o_rubate"),
    TENTATO_FURTO("tentato_furto"),
    FURTO_PARZIALE("furto_parziale"),
    FURTO_E_RITROVAMENTO("furto_e_ritrovamento"),
    FURTO_TOTALE("furto_totale"),
    PAI("pai");

    private static Logger LOGGER = LoggerFactory.getLogger(DataAccidentTypeAccidentEnum.class);
    private String typeAccidentEntity;

    private DataAccidentTypeAccidentEnum(String typeAccidentEntity) {
        this.typeAccidentEntity = typeAccidentEntity;
    }

    @JsonCreator
    public static DataAccidentTypeAccidentEnum create(String typeAccidentEntity) {

        typeAccidentEntity = typeAccidentEntity.replace(" - ", "_");
        typeAccidentEntity = typeAccidentEntity.replace('-', '_');
        typeAccidentEntity = typeAccidentEntity.replace(' ', '_');

        if (typeAccidentEntity != null) {
            for (DataAccidentTypeAccidentEnum val : DataAccidentTypeAccidentEnum.values()) {
                if (typeAccidentEntity.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        LOGGER.debug("Error in CostItem, duplicate code not admitted");
        throw new BadParametersException("Bad parameters exception. Enum class " + DataAccidentTypeAccidentEnum.class.getSimpleName() + " doesn't accept this value: " + typeAccidentEntity);
    }

    public static List<DataAccidentTypeAccidentEnum> getTypeAccidentListFromString(List<String> typeAccidentListString) {

        List<DataAccidentTypeAccidentEnum> typeAccidentList = new LinkedList<>();
        for (String att : typeAccidentListString) {
            typeAccidentList.add(DataAccidentTypeAccidentEnum.create(att));
        }
        return typeAccidentList;
    }

    public String getValue() {
        return this.typeAccidentEntity.toUpperCase();
    }

    @JsonValue
    public String toValue() {
        for (DataAccidentTypeAccidentEnum val : DataAccidentTypeAccidentEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }

    public static List<String> getStringListFromTypeAccidentList(List<DataAccidentTypeAccidentEnum> typeAccidentList) {

        List<String> typeAccidentListString = new LinkedList<>();
        for (DataAccidentTypeAccidentEnum att : typeAccidentList) {
            typeAccidentListString.add(att.getValue());
        }
        return typeAccidentListString;
    }
}
