package com.doing.nemo.claims.entity.jsonb;

import com.doing.nemo.claims.entity.enumerated.DamagedEnum.WoundedEnum.WoundedTypeEnum;
import com.doing.nemo.claims.entity.enumerated.DamagedEnum.WoundedEnum.WoundedWoundEnum;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Wounded implements Serializable {

    private static final long serialVersionUID = -6825756800700387055L;

    @JsonProperty("firstname")
    private String firstname;

    @JsonProperty("lastname")
    private String lastname;

    @JsonProperty("main_address")
    private Address address;

    @JsonProperty("emergency_room")
    private Boolean emergencyRoom;

    @JsonProperty("wound")
    private WoundedWoundEnum wound;

    @JsonProperty("type")
    private WoundedTypeEnum type;

    @JsonProperty("is_privacy_accepted")
    private Boolean isPrivacyAccepted;

    @JsonProperty("attachment_list")
    private List<String> attachmentList;

    public Boolean getPrivacyAccepted() {
        return isPrivacyAccepted;
    }

    public void setPrivacyAccepted(Boolean privacyAccepted) {
        isPrivacyAccepted = privacyAccepted;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Boolean getEmergencyRoom() {
        return emergencyRoom;
    }

    public void setEmergencyRoom(Boolean emergencyRoom) {
        this.emergencyRoom = emergencyRoom;
    }

    public WoundedWoundEnum getWound() {
        return wound;
    }

    public void setWound(WoundedWoundEnum wound) {
        this.wound = wound;
    }

    public WoundedTypeEnum getType() {
        return type;
    }

    public void setType(WoundedTypeEnum type) {
        this.type = type;
    }

    public List<String> getAttachmentList() {
        if(attachmentList == null){
            return null;
        }
        return new ArrayList<>(attachmentList);
    }

    public void setAttachmentList(List<String> attachmentList) {
        if(attachmentList != null)
        {
            this.attachmentList = new ArrayList<>(attachmentList);
        } else {
            this.attachmentList = null;
        }
    }

    @Override
    public String toString() {
        return "Wounded{" +
                "firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", address=" + address +
                ", emergencyRoom=" + emergencyRoom +
                ", wound=" + wound +
                ", type=" + type +
                ", attachmentList=" + attachmentList +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Wounded wounded = (Wounded) o;

        return new EqualsBuilder().append(firstname, wounded.firstname).append(lastname, wounded.lastname)
                .append(address, wounded.address).append(emergencyRoom, wounded.emergencyRoom)
                .append(wound, wounded.wound).append(type, wounded.type).append(isPrivacyAccepted, wounded.isPrivacyAccepted)
                .append(attachmentList, wounded.attachmentList).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(firstname).append(lastname).append(address)
                .append(emergencyRoom).append(wound).append(type).append(isPrivacyAccepted).append(attachmentList).toHashCode();
    }
}
