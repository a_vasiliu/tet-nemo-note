package com.doing.nemo.claims.entity.claims;

import com.doing.nemo.claims.entity.ClaimsNewEntity;
import com.doing.nemo.claims.entity.jsonb.Address;
import com.doing.nemo.claims.entity.jsonb.dataType.JsonDataAddressType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

@JsonIgnoreProperties({"hibernateLazyInitializer", "hibernate_lazy_initializer", "handler"})
@Entity
@Table(name = "claims_theft")
@TypeDefs({
        @TypeDef(name = "JsonDataAddressType", typeClass = JsonDataAddressType.class)
})
public class ClaimsTheftEntity implements Serializable {

    @Id
    @Column(name = "id", updatable = false, nullable = false)
    private String id;

    @Column(name="is_found")
    private boolean isFound;

    @Column(name="operations_center_notified")
    private Boolean OperationsCenterNotified;

    @Column(name="theft_occurred_on_center_ald")
    private Boolean theftOccurredOnCenterAld;

    @Column(name="supplier_code")
    private String supplierCode;

    @Column(name="complaint_authority_police")
    private Boolean complaintAuthorityPolice;

    @Column(name="complaint_authority_cc")
    private Boolean complaintAuthorityCc;

    @Column(name="complaint_authority_vvuu")
    private Boolean complaintAuthorityVvuu;

    @Column(name="authority_data")
    private String authorityData;

    @Column(name="authority_telephone")
    private String authorityTelephone;

    //JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    @Column(name="find_date")
    private Instant findDate;

    @Column(name="hour")
    private String hour;

    @Column(name="found_abroad")
    private Boolean foundAbroad;

    @Column(name="vehicle_co")
    private String vehicleCo;

    @Column(name="sequestered")
    private Boolean sequestered;

    @Column(name="address")
    @Type(type ="JsonDataAddressType" )
    private Address address;

    @Column(name="find_authority_police")
    private Boolean findAuthorityPolice;

    @Column(name="find_authority_cc")
    private Boolean findAuthorityCc;

    @Column(name="find_authority_vvuu")
    private Boolean findAuthorityVvuu;

    @Column(name="find_authority_data")
    private String findAuthorityData;

    @Column(name="find_authority_telephone")
    private String findAuthorityTelephone;

    @Column(name="theft_notes")
    private String theftNotes;

    @Column(name="find_notes")
    private String findNotes;

    //JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    @Column(name="first_key_reception")
    private Instant firstKeyReception;

    //JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    @Column(name="second_key_reception")
    private Instant secondKeyReception;

    //JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    @Column(name="original_complaint_reception")
    private Instant originalComplaintReception;

    //JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    @Column(name="copy_complaint_reception")
    private Instant copyComplaintReception;

    //JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    @Column(name="original_report_reception")
    private Instant originalReportReception;

    //JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    @Column(name="copy_report_reception")
    private Instant copyReportReception;

    //JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    @Column(name="loss_possession_request")
    private Instant lossPossessionRequest;

    //JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    @Column(name="loss_possession_reception")
    private Instant lossPossessionReception;

    //JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    @Column(name="return_possession_request")
    private Instant returnPossessionRequest;

    //JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    @Column(name="return_possession_reception")
    private Instant returnPossessionReception;

    @Column(name="robbery")
    private Boolean robbery;

    @Column(name="key_management")
    private Boolean keyManagement;

    @Column(name="account_management")
    private Boolean accountManagement;

    @Column(name="administrative_position")
    private Boolean administrativePosition;

    @Column(name="practice_id")
    private Long practiceId;

    @Column(name="re_registration")
    private Boolean reRegistration;

    //JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    @Column(name="re_registration_request")
    private Instant reRegistrationRequest;

    //JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    @Column(name="re_registration_at")
    private Instant reRegistrationAt;

    @Column(name="re_registration_salesforce_case")
    private String reRegistrationSaleforcesCase;

    @Column(name="certificate_duplication")
    private Boolean certificateDuplication;

    //JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    @Column(name="certificate_duplication_request")
    private Instant certificateDuplicationRequest;

    //JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    @Column(name="certificate_duplication_at")
    private Instant certificateDuplicationAt;

    @Column(name="certificate_duplication_salesforce_case")
    private String certificateDuplicationSalesforceCase;

    @Column(name="stamp")
    private Boolean stamp;

    //JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    @Column(name="stamp_request")
    private Instant stampRequest;

    //JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    @Column(name="stamp_at")
    private Instant stampAt;

    @Column(name="stamp_salesforce_case")
    private String stampSalesforceCase;

    @Column(name="demolition")
    private Boolean demolition;

    //JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    @Column(name="demolition_request")
    private Instant demolitionRequest;

    //JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    @Column(name="demolition_at")
    private Instant demolitionAt;

    @Column(name="demolition_salesforce_case")
    private String demolitionSalesforceCase;

    @Column(name="is_with_receptions")
    private Boolean isWithReceptions;

    @Column(name="is_under_seizure")
    private Boolean isUnderSeizure;

    @OneToOne
    @JoinColumn(name = "id")
    @MapsId("id")
    @PrimaryKeyJoinColumn
    private ClaimsNewEntity claim;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isFound() {
        return isFound;
    }

    public void setFound(boolean found) {
        isFound = found;
    }

    public Boolean getOperationsCenterNotified() {
        return OperationsCenterNotified;
    }

    public void setOperationsCenterNotified(Boolean operationsCenterNotified) {
        OperationsCenterNotified = operationsCenterNotified;
    }

    public Boolean getTheftOccurredOnCenterAld() {
        return theftOccurredOnCenterAld;
    }

    public void setTheftOccurredOnCenterAld(Boolean theftOccurredOnCenterAld) {
        this.theftOccurredOnCenterAld = theftOccurredOnCenterAld;
    }

    public String getSupplierCode() {
        return supplierCode;
    }

    public void setSupplierCode(String supplierCode) {
        this.supplierCode = supplierCode;
    }

    public Boolean getComplaintAuthorityPolice() {
        return complaintAuthorityPolice;
    }

    public void setComplaintAuthorityPolice(Boolean complaintAuthorityPolice) {
        this.complaintAuthorityPolice = complaintAuthorityPolice;
    }

    public Boolean getComplaintAuthorityCc() {
        return complaintAuthorityCc;
    }

    public void setComplaintAuthorityCc(Boolean complaintAuthorityCc) {
        this.complaintAuthorityCc = complaintAuthorityCc;
    }

    public Boolean getComplaintAuthorityVvuu() {
        return complaintAuthorityVvuu;
    }

    public void setComplaintAuthorityVvuu(Boolean complaintAuthorityVvuu) {
        this.complaintAuthorityVvuu = complaintAuthorityVvuu;
    }

    public String getAuthorityData() {
        return authorityData;
    }

    public void setAuthorityData(String authorityData) {
        this.authorityData = authorityData;
    }

    public String getAuthorityTelephone() {
        return authorityTelephone;
    }

    public void setAuthorityTelephone(String authorityTelephone) {
        this.authorityTelephone = authorityTelephone;
    }

    public Instant getFindDate() {
        return findDate;
    }

    public void setFindDate(Instant findDate) {
        this.findDate = findDate;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public Boolean getFoundAbroad() {
        return foundAbroad;
    }

    public void setFoundAbroad(Boolean foundAbroad) {
        this.foundAbroad = foundAbroad;
    }

    public String getVehicleCo() {
        return vehicleCo;
    }

    public void setVehicleCo(String vehicleCo) {
        this.vehicleCo = vehicleCo;
    }

    public Boolean getSequestered() {
        return sequestered;
    }

    public void setSequestered(Boolean sequestered) {
        this.sequestered = sequestered;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Boolean getFindAuthorityPolice() {
        return findAuthorityPolice;
    }

    public void setFindAuthorityPolice(Boolean findAuthorityPolice) {
        this.findAuthorityPolice = findAuthorityPolice;
    }

    public Boolean getFindAuthorityCc() {
        return findAuthorityCc;
    }

    public void setFindAuthorityCc(Boolean findAuthorityCc) {
        this.findAuthorityCc = findAuthorityCc;
    }

    public Boolean getFindAuthorityVvuu() {
        return findAuthorityVvuu;
    }

    public void setFindAuthorityVvuu(Boolean findAuthorityVvuu) {
        this.findAuthorityVvuu = findAuthorityVvuu;
    }

    public String getFindAuthorityData() {
        return findAuthorityData;
    }

    public void setFindAuthorityData(String findAuthorityData) {
        this.findAuthorityData = findAuthorityData;
    }

    public String getFindAuthorityTelephone() {
        return findAuthorityTelephone;
    }

    public void setFindAuthorityTelephone(String findAuthorityTelephone) {
        this.findAuthorityTelephone = findAuthorityTelephone;
    }

    public String getTheftNotes() {
        return theftNotes;
    }

    public void setTheftNotes(String theftNotes) {
        this.theftNotes = theftNotes;
    }

    public String getFindNotes() {
        return findNotes;
    }

    public void setFindNotes(String findNotes) {
        this.findNotes = findNotes;
    }

    public Instant getFirstKeyReception() {
        return firstKeyReception;
    }

    public void setFirstKeyReception(Instant firstKeyReception) {
        this.firstKeyReception = firstKeyReception;
    }

    public Instant getSecondKeyReception() {
        return secondKeyReception;
    }

    public void setSecondKeyReception(Instant secondKeyReception) {
        this.secondKeyReception = secondKeyReception;
    }

    public Instant getOriginalComplaintReception() {
        return originalComplaintReception;
    }

    public void setOriginalComplaintReception(Instant originalComplaintReception) {
        this.originalComplaintReception = originalComplaintReception;
    }

    public Instant getCopyComplaintReception() {
        return copyComplaintReception;
    }

    public void setCopyComplaintReception(Instant copyComplaintReception) {
        this.copyComplaintReception = copyComplaintReception;
    }

    public Instant getOriginalReportReception() {
        return originalReportReception;
    }

    public void setOriginalReportReception(Instant originalReportReception) {
        this.originalReportReception = originalReportReception;
    }

    public Instant getCopyReportReception() {
        return copyReportReception;
    }

    public void setCopyReportReception(Instant copyReportReception) {
        this.copyReportReception = copyReportReception;
    }

    public Instant getLossPossessionRequest() {
        return lossPossessionRequest;
    }

    public void setLossPossessionRequest(Instant lossPossessionRequest) {
        this.lossPossessionRequest = lossPossessionRequest;
    }

    public Instant getLossPossessionReception() {
        return lossPossessionReception;
    }

    public void setLossPossessionReception(Instant lossPossessionReception) {
        this.lossPossessionReception = lossPossessionReception;
    }

    public Instant getReturnPossessionRequest() {
        return returnPossessionRequest;
    }

    public void setReturnPossessionRequest(Instant returnPossessionRequest) {
        this.returnPossessionRequest = returnPossessionRequest;
    }

    public Instant getReturnPossessionReception() {
        return returnPossessionReception;
    }

    public void setReturnPossessionReception(Instant returnPossessionReception) {
        this.returnPossessionReception = returnPossessionReception;
    }

    public Boolean getRobbery() {
        return robbery;
    }

    public void setRobbery(Boolean robbery) {
        this.robbery = robbery;
    }

    public Boolean getKeyManagement() {
        return keyManagement;
    }

    public void setKeyManagement(Boolean keyManagement) {
        this.keyManagement = keyManagement;
    }

    public Boolean getAccountManagement() {
        return accountManagement;
    }

    public void setAccountManagement(Boolean accountManagement) {
        this.accountManagement = accountManagement;
    }

    public Boolean getAdministrativePosition() {
        return administrativePosition;
    }

    public void setAdministrativePosition(Boolean administrativePosition) {
        this.administrativePosition = administrativePosition;
    }

    public Long getPracticeId() {
        return practiceId;
    }

    public void setPracticeId(Long practiceId) {
        this.practiceId = practiceId;
    }

    public Boolean getReRegistration() {
        return reRegistration;
    }

    public void setReRegistration(Boolean reRegistration) {
        this.reRegistration = reRegistration;
    }

    public Instant getReRegistrationRequest() {
        return reRegistrationRequest;
    }

    public void setReRegistrationRequest(Instant reRegistrationRequest) {
        this.reRegistrationRequest = reRegistrationRequest;
    }

    public Instant getReRegistrationAt() {
        return reRegistrationAt;
    }

    public void setReRegistrationAt(Instant reRegistrationAt) {
        this.reRegistrationAt = reRegistrationAt;
    }

    public String getReRegistrationSaleforcesCase() {
        return reRegistrationSaleforcesCase;
    }

    public void setReRegistrationSaleforcesCase(String reRegistrationSaleforcesCase) {
        this.reRegistrationSaleforcesCase = reRegistrationSaleforcesCase;
    }

    public Boolean getCertificateDuplication() {
        return certificateDuplication;
    }

    public void setCertificateDuplication(Boolean certificateDuplication) {
        this.certificateDuplication = certificateDuplication;
    }

    public Instant getCertificateDuplicationRequest() {
        return certificateDuplicationRequest;
    }

    public void setCertificateDuplicationRequest(Instant certificateDuplicationRequest) {
        this.certificateDuplicationRequest = certificateDuplicationRequest;
    }

    public Instant getCertificateDuplicationAt() {
        return certificateDuplicationAt;
    }

    public void setCertificateDuplicationAt(Instant certificateDuplicationAt) {
        this.certificateDuplicationAt = certificateDuplicationAt;
    }

    public String getCertificateDuplicationSalesforceCase() {
        return certificateDuplicationSalesforceCase;
    }

    public void setCertificateDuplicationSalesforceCase(String certificateDuplicationSalesforceCase) {
        this.certificateDuplicationSalesforceCase = certificateDuplicationSalesforceCase;
    }

    public Boolean getStamp() {
        return stamp;
    }

    public void setStamp(Boolean stamp) {
        this.stamp = stamp;
    }

    public Instant getStampRequest() {
        return stampRequest;
    }

    public void setStampRequest(Instant stampRequest) {
        this.stampRequest = stampRequest;
    }

    public Instant getStampAt() {
        return stampAt;
    }

    public void setStampAt(Instant stampAt) {
        this.stampAt = stampAt;
    }

    public String getStampSalesforceCase() {
        return stampSalesforceCase;
    }

    public void setStampSalesforceCase(String stampSalesforceCase) {
        this.stampSalesforceCase = stampSalesforceCase;
    }

    public Boolean getDemolition() {
        return demolition;
    }

    public void setDemolition(Boolean demolition) {
        this.demolition = demolition;
    }

    public Instant getDemolitionRequest() {
        return demolitionRequest;
    }

    public void setDemolitionRequest(Instant demolitionRequest) {
        this.demolitionRequest = demolitionRequest;
    }

    public Instant getDemolitionAt() {
        return demolitionAt;
    }

    public void setDemolitionAt(Instant demolitionAt) {
        this.demolitionAt = demolitionAt;
    }

    public String getDemolitionSalesforceCase() {
        return demolitionSalesforceCase;
    }

    public void setDemolitionSalesforceCase(String demolitionSalesforceCase) {
        this.demolitionSalesforceCase = demolitionSalesforceCase;
    }

    public Boolean getWithReceptions() {
        return isWithReceptions;
    }

    public void setWithReceptions(Boolean withReceptions) {
        isWithReceptions = withReceptions;
    }

    public Boolean getUnderSeizure() {
        return isUnderSeizure;
    }

    public void setUnderSeizure(Boolean underSeizure) {
        isUnderSeizure = underSeizure;
    }

    public ClaimsNewEntity getClaim() {
        return claim;
    }

    public void setClaim(ClaimsNewEntity claim) {
        this.claim = claim;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ClaimsTheftEntity{");
        sb.append("id='").append(id).append('\'');
        sb.append(", isFound=").append(isFound);
        sb.append(", OperationsCenterNotified=").append(OperationsCenterNotified);
        sb.append(", theftOccurredOnCenterAld=").append(theftOccurredOnCenterAld);
        sb.append(", supplierCode='").append(supplierCode).append('\'');
        sb.append(", complaintAuthorityPolice=").append(complaintAuthorityPolice);
        sb.append(", complaintAuthorityCc=").append(complaintAuthorityCc);
        sb.append(", complaintAuthorityVvuu=").append(complaintAuthorityVvuu);
        sb.append(", authorityData='").append(authorityData).append('\'');
        sb.append(", authorityTelephone='").append(authorityTelephone).append('\'');
        sb.append(", findDate=").append(findDate);
        sb.append(", hour='").append(hour).append('\'');
        sb.append(", foundAbroad=").append(foundAbroad);
        sb.append(", vehicleCo='").append(vehicleCo).append('\'');
        sb.append(", sequestered=").append(sequestered);
        sb.append(", address=").append(address);
        sb.append(", findAuthorityPolice=").append(findAuthorityPolice);
        sb.append(", findAuthorityCc=").append(findAuthorityCc);
        sb.append(", findAuthorityVvuu=").append(findAuthorityVvuu);
        sb.append(", findAuthorityData='").append(findAuthorityData).append('\'');
        sb.append(", findAuthorityTelephone='").append(findAuthorityTelephone).append('\'');
        sb.append(", theftNotes='").append(theftNotes).append('\'');
        sb.append(", findNotes='").append(findNotes).append('\'');
        sb.append(", firstKeyReception=").append(firstKeyReception);
        sb.append(", secondKeyReception=").append(secondKeyReception);
        sb.append(", originalComplaintReception=").append(originalComplaintReception);
        sb.append(", copyComplaintReception=").append(copyComplaintReception);
        sb.append(", originalReportReception=").append(originalReportReception);
        sb.append(", copyReportReception=").append(copyReportReception);
        sb.append(", lossPossessionRequest=").append(lossPossessionRequest);
        sb.append(", lossPossessionReception=").append(lossPossessionReception);
        sb.append(", returnPossessionRequest=").append(returnPossessionRequest);
        sb.append(", returnPossessionReception=").append(returnPossessionReception);
        sb.append(", robbery=").append(robbery);
        sb.append(", keyManagement=").append(keyManagement);
        sb.append(", accountManagement=").append(accountManagement);
        sb.append(", administrativePosition=").append(administrativePosition);
        sb.append(", practiceId=").append(practiceId);
        sb.append(", reRegistration=").append(reRegistration);
        sb.append(", reRegistrationRequest=").append(reRegistrationRequest);
        sb.append(", reRegistrationAt=").append(reRegistrationAt);
        sb.append(", reRegistrationSaleforcesCase='").append(reRegistrationSaleforcesCase).append('\'');
        sb.append(", certificateDuplication=").append(certificateDuplication);
        sb.append(", certificateDuplicationRequest=").append(certificateDuplicationRequest);
        sb.append(", certificateDuplicationAt=").append(certificateDuplicationAt);
        sb.append(", certificateDuplicationSalesforceCase='").append(certificateDuplicationSalesforceCase).append('\'');
        sb.append(", stamp=").append(stamp);
        sb.append(", stampRequest=").append(stampRequest);
        sb.append(", stampAt=").append(stampAt);
        sb.append(", stampSalesforceCase='").append(stampSalesforceCase).append('\'');
        sb.append(", demolition=").append(demolition);
        sb.append(", demolitionRequest=").append(demolitionRequest);
        sb.append(", demolitionAt=").append(demolitionAt);
        sb.append(", demolitionSalesforceCase='").append(demolitionSalesforceCase).append('\'');
        sb.append(", isWithReceptions=").append(isWithReceptions);
        sb.append(", isUnderSeizure=").append(isUnderSeizure);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClaimsTheftEntity that = (ClaimsTheftEntity) o;
        return isFound == that.isFound &&
                Objects.equals(id, that.id) &&
                Objects.equals(OperationsCenterNotified, that.OperationsCenterNotified) &&
                Objects.equals(theftOccurredOnCenterAld, that.theftOccurredOnCenterAld) &&
                Objects.equals(supplierCode, that.supplierCode) &&
                Objects.equals(complaintAuthorityPolice, that.complaintAuthorityPolice) &&
                Objects.equals(complaintAuthorityCc, that.complaintAuthorityCc) &&
                Objects.equals(complaintAuthorityVvuu, that.complaintAuthorityVvuu) &&
                Objects.equals(authorityData, that.authorityData) &&
                Objects.equals(authorityTelephone, that.authorityTelephone) &&
                Objects.equals(findDate, that.findDate) &&
                Objects.equals(hour, that.hour) &&
                Objects.equals(foundAbroad, that.foundAbroad) &&
                Objects.equals(vehicleCo, that.vehicleCo) &&
                Objects.equals(sequestered, that.sequestered) &&
                Objects.equals(address, that.address) &&
                Objects.equals(findAuthorityPolice, that.findAuthorityPolice) &&
                Objects.equals(findAuthorityCc, that.findAuthorityCc) &&
                Objects.equals(findAuthorityVvuu, that.findAuthorityVvuu) &&
                Objects.equals(findAuthorityData, that.findAuthorityData) &&
                Objects.equals(findAuthorityTelephone, that.findAuthorityTelephone) &&
                Objects.equals(theftNotes, that.theftNotes) &&
                Objects.equals(findNotes, that.findNotes) &&
                Objects.equals(firstKeyReception, that.firstKeyReception) &&
                Objects.equals(secondKeyReception, that.secondKeyReception) &&
                Objects.equals(originalComplaintReception, that.originalComplaintReception) &&
                Objects.equals(copyComplaintReception, that.copyComplaintReception) &&
                Objects.equals(originalReportReception, that.originalReportReception) &&
                Objects.equals(copyReportReception, that.copyReportReception) &&
                Objects.equals(lossPossessionRequest, that.lossPossessionRequest) &&
                Objects.equals(lossPossessionReception, that.lossPossessionReception) &&
                Objects.equals(returnPossessionRequest, that.returnPossessionRequest) &&
                Objects.equals(returnPossessionReception, that.returnPossessionReception) &&
                Objects.equals(robbery, that.robbery) &&
                Objects.equals(keyManagement, that.keyManagement) &&
                Objects.equals(accountManagement, that.accountManagement) &&
                Objects.equals(administrativePosition, that.administrativePosition) &&
                Objects.equals(practiceId, that.practiceId) &&
                Objects.equals(reRegistration, that.reRegistration) &&
                Objects.equals(reRegistrationRequest, that.reRegistrationRequest) &&
                Objects.equals(reRegistrationAt, that.reRegistrationAt) &&
                Objects.equals(reRegistrationSaleforcesCase, that.reRegistrationSaleforcesCase) &&
                Objects.equals(certificateDuplication, that.certificateDuplication) &&
                Objects.equals(certificateDuplicationRequest, that.certificateDuplicationRequest) &&
                Objects.equals(certificateDuplicationAt, that.certificateDuplicationAt) &&
                Objects.equals(certificateDuplicationSalesforceCase, that.certificateDuplicationSalesforceCase) &&
                Objects.equals(stamp, that.stamp) &&
                Objects.equals(stampRequest, that.stampRequest) &&
                Objects.equals(stampAt, that.stampAt) &&
                Objects.equals(stampSalesforceCase, that.stampSalesforceCase) &&
                Objects.equals(demolition, that.demolition) &&
                Objects.equals(demolitionRequest, that.demolitionRequest) &&
                Objects.equals(demolitionAt, that.demolitionAt) &&
                Objects.equals(demolitionSalesforceCase, that.demolitionSalesforceCase) &&
                Objects.equals(isWithReceptions, that.isWithReceptions) &&
                Objects.equals(isUnderSeizure, that.isUnderSeizure) &&
                Objects.equals(claim, that.claim);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, isFound, OperationsCenterNotified, theftOccurredOnCenterAld, supplierCode, complaintAuthorityPolice, complaintAuthorityCc, complaintAuthorityVvuu, authorityData, authorityTelephone, findDate, hour, foundAbroad, vehicleCo, sequestered, address, findAuthorityPolice, findAuthorityCc, findAuthorityVvuu, findAuthorityData, findAuthorityTelephone, theftNotes, findNotes, firstKeyReception, secondKeyReception, originalComplaintReception, copyComplaintReception, originalReportReception, copyReportReception, lossPossessionRequest, lossPossessionReception, returnPossessionRequest, returnPossessionReception, robbery, keyManagement, accountManagement, administrativePosition, practiceId, reRegistration, reRegistrationRequest, reRegistrationAt, reRegistrationSaleforcesCase, certificateDuplication, certificateDuplicationRequest, certificateDuplicationAt, certificateDuplicationSalesforceCase, stamp, stampRequest, stampAt, stampSalesforceCase, demolition, demolitionRequest, demolitionAt, demolitionSalesforceCase, isWithReceptions, isUnderSeizure, claim);
    }
}
