package com.doing.nemo.claims.entity.settings;

import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "export_claims_type")
public class ImportClaimsTypeEntity implements Serializable {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )

    @Type(type = "org.hibernate.type.PostgresUUIDType")
    private UUID id;

    @Enumerated(EnumType.STRING)
    @Column(name = "claims_type")
    private DataAccidentTypeAccidentEnum claimsType;

    @Column(name = "description")
    private String description;

    public ImportClaimsTypeEntity() {
    }

    public ImportClaimsTypeEntity(DataAccidentTypeAccidentEnum claimsType, DmSystemsEntity dmSystem, String code, String description) {
        this.claimsType = claimsType;
        this.description = description;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public DataAccidentTypeAccidentEnum getClaimsType() {
        return claimsType;
    }

    public void setClaimsType(DataAccidentTypeAccidentEnum claimsType) {
        this.claimsType = claimsType;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "ExportClaimsTypeEntity{" +
                "id=" + id +
                ", claimsType=" + claimsType +
                ", description='" + description + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ImportClaimsTypeEntity that = (ImportClaimsTypeEntity) o;
        return Objects.equals(getId(), that.getId()) &&
                getClaimsType() == that.getClaimsType() &&
                Objects.equals(getDescription(), that.getDescription());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getClaimsType(), getDescription());
    }
}
