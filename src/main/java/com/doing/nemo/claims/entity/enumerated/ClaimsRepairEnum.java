package com.doing.nemo.claims.entity.enumerated;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;

public enum ClaimsRepairEnum implements Serializable {
    CLAIMS("claims"),
    REPAIR("repair"),
    THEFT("theft");

    private static Logger LOGGER = LoggerFactory.getLogger(ClaimsRepairEnum.class);
    private String claimsRepairEnum;

    private ClaimsRepairEnum(String claimsRepairEnum) {
        this.claimsRepairEnum = claimsRepairEnum;
    }

    @JsonCreator
    public static ClaimsRepairEnum create(String claimsRepairEnum) {

        claimsRepairEnum = claimsRepairEnum.replace(" - ", "_");
        claimsRepairEnum = claimsRepairEnum.replace('-', '_');
        claimsRepairEnum = claimsRepairEnum.replace(' ', '_');

        if (claimsRepairEnum != null) {
            for (ClaimsRepairEnum val : ClaimsRepairEnum.values()) {
                if (claimsRepairEnum.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        LOGGER.debug("Bad parameters exception. Enum class " + ClaimsRepairEnum.class.getSimpleName() + " doesn't accept this value: " + claimsRepairEnum);
        throw new BadParametersException("Bad parameters exception. Enum class " + ClaimsRepairEnum.class.getSimpleName() + " doesn't accept this value: " + claimsRepairEnum);
    }

    public String getValue() {
        return this.claimsRepairEnum.toUpperCase();
    }

    @JsonValue
    public String toValue() {
        for (ClaimsRepairEnum val : ClaimsRepairEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }
}
