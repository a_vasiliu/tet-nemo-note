package com.doing.nemo.claims.entity.enumerated;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;

public enum LawyerPaymentsStatusEnum implements Serializable {
    WAITING_TO_ASSIGNED("waiting_to_assigned"),
    ASSIGNED("assigned"),
    ERROR("error");

    private static Logger LOGGER = LoggerFactory.getLogger(LawyerPaymentsStatusEnum.class);
    private String statusEntity;

    private LawyerPaymentsStatusEnum(String statusEntity) {
        this.statusEntity = statusEntity;
    }

    @JsonCreator
    public static LawyerPaymentsStatusEnum create(String statusEntity) {

        statusEntity = statusEntity.replace(" - ", "_");
        statusEntity = statusEntity.replace('-', '_');
        statusEntity = statusEntity.replace(' ', '_');

        if (statusEntity != null) {
            for (LawyerPaymentsStatusEnum val : LawyerPaymentsStatusEnum.values()) {
                if (statusEntity.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        LOGGER.debug("Bad parameters exception. Enum class " + LawyerPaymentsStatusEnum.class.getSimpleName() + " doesn't accept this value: " + statusEntity);
        throw new BadParametersException("Bad parameters exception. Enum class " + LawyerPaymentsStatusEnum.class.getSimpleName() + " doesn't accept this value: " + statusEntity);
    }

    public String getValue() {
        return this.statusEntity.toUpperCase();
    }

    @JsonValue
    public String toValue() {
        for (LawyerPaymentsStatusEnum val : LawyerPaymentsStatusEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }
}
