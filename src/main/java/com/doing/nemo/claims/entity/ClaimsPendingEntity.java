package com.doing.nemo.claims.entity;

import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.*;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "hibernate_lazy_initializer", "handler"})
@Table(name = "claims_pending")
public class ClaimsPendingEntity implements Serializable {

    @Id
    @Column(name = "practice_id")
    private Long practiceId;

    @Column(name = "damaged_drivers_plate")
    private String damagedDriversPlate;

    @Column(name = "date_accident")
    private Instant dateAccident;

    @Column(name = "type_accident")
    //@Enumerated(EnumType.STRING)
    private String typeAccidentWebSin;

    @Column(name = "counterparty_insured_name")
    private String counterpartyInsuredName;

    @Column(name = "counterparty_insured_surname")
    private String counterpartyInsuredSurname;

    @Column(name = "counterparty_plate")
    private String counterpartyPlate;

    public ClaimsPendingEntity() {
    }

    public ClaimsPendingEntity(Long practiceId, String damagedDriversPlate, Instant dateAccident, String typeAccident, String counterpartyInsuredName, String counterpartyInsuredSurname, String counterpartyPlate) {
        this.practiceId = practiceId;
        this.damagedDriversPlate = damagedDriversPlate;
        this.dateAccident = dateAccident;
        this.typeAccidentWebSin = typeAccident;
        this.counterpartyInsuredName = counterpartyInsuredName;
        this.counterpartyInsuredSurname = counterpartyInsuredSurname;
        this.counterpartyPlate = counterpartyPlate;
    }

    public Long getPracticeId() {
        return practiceId;
    }

    public void setPracticeId(Long practiceId) {
        this.practiceId = practiceId;
    }

    public String getDamagedDriversPlate() {
        return damagedDriversPlate;
    }

    public void setDamagedDriversPlate(String damagedDriversPlate) {
        this.damagedDriversPlate = damagedDriversPlate;
    }

    public Instant getDateAccident() {
        return dateAccident;
    }

    public void setDateAccident(Instant dateAccident) {
        this.dateAccident = dateAccident;
    }

    public String getTypeAccidentWebSin() {
        return typeAccidentWebSin;
    }

    public void setTypeAccidentWebSin(String typeAccidentWebSin) {
        this.typeAccidentWebSin = typeAccidentWebSin;
    }

    public String getCounterpartyInsuredName() {
        return counterpartyInsuredName;
    }

    public void setCounterpartyInsuredName(String counterpartyInsuredName) {
        this.counterpartyInsuredName = counterpartyInsuredName;
    }

    public String getCounterpartyInsuredSurname() {
        return counterpartyInsuredSurname;
    }

    public void setCounterpartyInsuredSurname(String counterpartyInsuredSurname) {
        this.counterpartyInsuredSurname = counterpartyInsuredSurname;
    }

    public String getCounterpartyPlate() {
        return counterpartyPlate;
    }

    public void setCounterpartyPlate(String counterpartyPlate) {
        this.counterpartyPlate = counterpartyPlate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClaimsPendingEntity that = (ClaimsPendingEntity) o;
        return Objects.equals(getPracticeId(), that.getPracticeId()) && Objects.equals(getDamagedDriversPlate(), that.getDamagedDriversPlate()) && Objects.equals(getDateAccident(), that.getDateAccident()) && getTypeAccidentWebSin() == that.getTypeAccidentWebSin() && Objects.equals(getCounterpartyInsuredName(), that.getCounterpartyInsuredName()) && Objects.equals(getCounterpartyInsuredSurname(), that.getCounterpartyInsuredSurname()) && Objects.equals(getCounterpartyPlate(), that.getCounterpartyPlate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPracticeId(), getDamagedDriversPlate(), getDateAccident(), getTypeAccidentWebSin(), getCounterpartyInsuredName(), getCounterpartyInsuredSurname(), getCounterpartyPlate());
    }

    @Override
    public String toString() {
        return "ClaimsPendingEntity{" +
                "practiceId=" + practiceId +
                ", damagedDriversPlate='" + damagedDriversPlate + '\'' +
                ", dateAccident=" + dateAccident +
                ", typeAccident=" + typeAccidentWebSin +
                ", counterpartyInsuredName='" + counterpartyInsuredName + '\'' +
                ", counterpartyInsuredSurname='" + counterpartyInsuredSurname + '\'' +
                ", counterpartyPlate='" + counterpartyPlate + '\'' +
                '}';
    }
}
