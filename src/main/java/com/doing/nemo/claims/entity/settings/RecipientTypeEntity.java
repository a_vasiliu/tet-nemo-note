package com.doing.nemo.claims.entity.settings;

import com.doing.nemo.claims.entity.enumerated.ClaimsRepairEnum;
import com.doing.nemo.claims.entity.enumerated.RecipientEnum;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "recipient_type")
public class RecipientTypeEntity {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Type(type = "org.hibernate.type.PostgresUUIDType")
    private UUID id;

    @Column(name = "description")
    private String description;

    @Column(name = "fixed")
    private Boolean fixed;

    @Column(name = "email")
    private String email;

    @Column(name = "db_field_email")
    private String dbFieldEmail;

    @Column(name = "is_active")
    private Boolean isActive;

    @Enumerated(EnumType.STRING)
    @Column(name = "recipient_type")
    private RecipientEnum recipientType;

    @Enumerated(EnumType.STRING)
    @Column(name = "type_complaint")
    private ClaimsRepairEnum typeComplaint;

    public RecipientTypeEntity() {
    }

    public RecipientTypeEntity(String description, Boolean fixed, String email, String dbFieldEmail, Boolean isActive, RecipientEnum recipientType, ClaimsRepairEnum typeComplaint) {
        this.description = description;
        this.fixed = fixed;
        this.email = email;
        this.dbFieldEmail = dbFieldEmail;
        this.isActive = isActive;
        this.recipientType = recipientType;
        this.typeComplaint = typeComplaint;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public RecipientEnum getRecipientType() {
        return recipientType;
    }

    public void setRecipientType(RecipientEnum recipientType) {
        this.recipientType = recipientType;
    }

    public Boolean getFixed() {
        return fixed;
    }

    public void setFixed(Boolean fixed) {
        this.fixed = fixed;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDbFieldEmail() {
        return dbFieldEmail;
    }

    public void setDbFieldEmail(String dbFieldEmail) {
        this.dbFieldEmail = dbFieldEmail;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public ClaimsRepairEnum getTypeComplaint() {
        return typeComplaint;
    }

    public void setTypeComplaint(ClaimsRepairEnum typeComplaint) {
        this.typeComplaint = typeComplaint;
    }

    @Override
    public String toString() {
        return "RecipientTypeEntity{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", fixed=" + fixed +
                ", email='" + email + '\'' +
                ", dbFieldEmail='" + dbFieldEmail + '\'' +
                ", recipientType=" + recipientType +
                ", isActive=" + isActive +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RecipientTypeEntity that = (RecipientTypeEntity) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getDescription(), that.getDescription()) &&
                Objects.equals(getFixed(), that.getFixed()) &&
                Objects.equals(getEmail(), that.getEmail()) &&
                Objects.equals(getDbFieldEmail(), that.getDbFieldEmail()) &&
                Objects.equals(getActive(), that.getActive()) &&
                getRecipientType() == that.getRecipientType() &&
                getTypeComplaint() == that.getTypeComplaint();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getDescription(), getFixed(), getEmail(), getDbFieldEmail(), getActive(), getRecipientType(), getTypeComplaint());
    }
}
