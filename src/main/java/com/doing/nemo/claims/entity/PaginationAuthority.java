package com.doing.nemo.claims.entity;


import com.doing.nemo.claims.controller.payload.response.PaginationStats;
import com.doing.nemo.claims.entity.enumerated.DTO.PageStats;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class PaginationAuthority<T> {


    @JsonProperty("stats")
    private PageStats stats;

    @JsonProperty("working_list")
    private List<T> items;

    public PageStats getStats() {
        return stats;
    }

    public void setStats(PageStats stats) {
        this.stats = stats;
    }

    public List<T> getItems() {
        if(items == null){
            return null;
        }
        return new ArrayList<>(items);
    }

    public void setItems(List<T> items) {
        if(items != null)
        {
            this.items =new ArrayList<>(items);
        } else {
            this.items = null;
        }
    }

    @Override
    public String toString() {
        return "ClaimsResponsePaginationV1{" +
                "stats=" + stats +
                ", items=" + items +
                '}';
    }

}
