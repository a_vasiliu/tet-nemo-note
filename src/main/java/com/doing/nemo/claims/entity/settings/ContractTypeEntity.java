package com.doing.nemo.claims.entity.settings;

import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsFlowEnum;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "contract_type")
public class ContractTypeEntity implements Serializable {

    private static final long serialVersionUID = 1453683651302785506L;

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Type(type = "org.hibernate.type.PostgresUUIDType")
    private UUID id;

    @Column(name = "cod_contract_type")
    private String codContractType;

    @Column(name = "description")
    private String description;

    @Column(name = "flag_WS")
    private Boolean flagWS;

    @Enumerated(EnumType.STRING)
    @Column(name = "default_flow")
    private ClaimsFlowEnum defaultFlow;

    @Column(name = "ctrnote")
    private String ctrnote;

    @Column(name = "ricaricar")
    private Boolean ricaricar;

    @Column(name = "franchise")
    private Double franchise;

    @Column(name = "is_active")
    private Boolean isActive;


    @OneToMany(
            mappedBy = "contractTypeEntity",
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            orphanRemoval = true
    )
    private List<FlowContractTypeEntity> flowContractTypeList = new LinkedList<>();

    public ContractTypeEntity() {
    }

    public ContractTypeEntity(String codContractType, String description, Boolean flagWS, ClaimsFlowEnum defaultFlow, String ctrnote, Boolean ricaricar, Double franchise, Boolean isActive, List<FlowContractTypeEntity> flowContractTypeList) {
        this.codContractType = codContractType;
        this.description = description;
        this.flagWS = flagWS;
        this.defaultFlow = defaultFlow;
        this.ctrnote = ctrnote;
        this.ricaricar = ricaricar;
        this.franchise = franchise;
        this.isActive = isActive;
        this.flowContractTypeList = flowContractTypeList;
    }

    public ContractTypeEntity(UUID id, String codContractType, String description, Boolean flagWS, ClaimsFlowEnum defaultFlow, String ctrnote, Boolean ricaricar, Double franchise, List<FlowContractTypeEntity> flowContractTypeList) {
        this.id = id;
        this.codContractType = codContractType;
        this.description = description;
        this.flagWS = flagWS;
        this.defaultFlow = defaultFlow;
        this.ctrnote = ctrnote;
        this.ricaricar = ricaricar;
        this.franchise = franchise;
        this.flowContractTypeList = flowContractTypeList;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getCodContractType() {
        return codContractType;
    }

    public void setCodContractType(String codContractType) {
        this.codContractType = codContractType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public Boolean getFlagWS() {
        return flagWS;
    }

    public void setFlagWS(Boolean flagWS) {
        this.flagWS = flagWS;
    }


    public String getCtrnote() {
        return ctrnote;
    }

    public void setCtrnote(String ctrnote) {
        this.ctrnote = ctrnote;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public ClaimsFlowEnum getDefaultFlow() {
        return defaultFlow;
    }

    public void setDefaultFlow(ClaimsFlowEnum defaultFlow) {
        this.defaultFlow = defaultFlow;
    }

    public Boolean getRicaricar() {
        return ricaricar;
    }

    public void setRicaricar(Boolean ricaricar) {
        this.ricaricar = ricaricar;
    }

    public Double getFranchise() {
        return franchise;
    }

    public void setFranchise(Double franchise) {
        this.franchise = franchise;
    }

    public List<FlowContractTypeEntity> getFlowContractTypeList() {
        return flowContractTypeList;
    }

    public void setFlowContractTypeList(List<FlowContractTypeEntity> flowContractTypeList) {
        this.flowContractTypeList.clear();
        this.flowContractTypeList.addAll(flowContractTypeList);
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    @Override
    public String toString() {
        return "ContractTypeEntity{" +
                "id=" + id +
                ", codContractType='" + codContractType + '\'' +
                ", description='" + description + '\'' +
                ", flagWS=" + flagWS +
                ", defaultFlow=" + defaultFlow +
                ", ctrnote='" + ctrnote + '\'' +
                ", ricaricar=" + ricaricar +
                ", franchise=" + franchise +
                ", flowContractTypeList=" + flowContractTypeList +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ContractTypeEntity that = (ContractTypeEntity) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getCodContractType(), that.getCodContractType()) &&
                Objects.equals(getDescription(), that.getDescription()) &&
                Objects.equals(getFlagWS(), that.getFlagWS()) &&
                getDefaultFlow() == that.getDefaultFlow() &&
                Objects.equals(getCtrnote(), that.getCtrnote()) &&
                Objects.equals(getRicaricar(), that.getRicaricar()) &&
                Objects.equals(getFranchise(), that.getFranchise()) &&
                Objects.equals(getActive(), that.getActive()) &&
                Objects.equals(getFlowContractTypeList(), that.getFlowContractTypeList());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getCodContractType(), getDescription(), getFlagWS(), getDefaultFlow(), getCtrnote(), getRicaricar(), getFranchise(), getActive(), getFlowContractTypeList());
    }
}
