package com.doing.nemo.claims.entity.jsonb;

import com.doing.nemo.claims.controller.payload.request.EmailTemplateMessagingRequestV1;
import com.doing.nemo.claims.entity.enumerated.EventTypeEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Theft implements Serializable {

    private static final long serialVersionUID = 1530992688285574130L;

    @JsonProperty("is_found")
    private boolean isFound;

    @JsonProperty("operations_center_notified")
    private Boolean OperationsCenterNotified;

    @JsonProperty("theft_occurred_on_center_ald")
    private Boolean theftOccurredOnCenterAld;

    @JsonProperty("supplier_code")
    private String supplierCode;

    @JsonProperty("complaint_authority_police")
    private Boolean complaintAuthorityPolice;

    @JsonProperty("complaint_authority_cc")
    private Boolean complaintAuthorityCc;

    @JsonProperty("complaint_authority_vvuu")
    private Boolean complaintAuthorityVvuu;

    @JsonProperty("authority_data")
    private String authorityData;

    @JsonProperty("authority_telephone")
    private String authorityTelephone;

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    @JsonProperty("find_date")
    private Date findDate;

    @JsonProperty("hour")
    private String hour;

    @JsonProperty("found_abroad")
    private Boolean foundAbroad;

    @JsonProperty("vehicle_co")
    private String vehicleCo;

    @JsonProperty("sequestered")
    private Boolean sequestered;

    @JsonProperty("address")
    private Address address;

    @JsonProperty("find_authority_police")
    private Boolean findAuthorityPolice;

    @JsonProperty("find_authority_cc")
    private Boolean findAuthorityCc;

    @JsonProperty("find_authority_vvuu")
    private Boolean findAuthorityVvuu;

    @JsonProperty("find_authority_data")
    private String findAuthorityData;

    @JsonProperty("find_authority_telephone")
    private String findAuthorityTelephone;

    @JsonProperty("theft_notes")
    private String theftNotes;

    @JsonProperty("find_notes")
    private String findNotes;

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    @JsonProperty("first_key_reception")
    private Date firstKeyReception;

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    @JsonProperty("second_key_reception")
    private Date secondKeyReception;

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    @JsonProperty("original_complaint_reception")
    private Date originalComplaintReception;

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    @JsonProperty("copy_complaint_reception")
    private Date copyComplaintReception;

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    @JsonProperty("original_report_reception")
    private Date originalReportReception;

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    @JsonProperty("copy_report_reception")
    private Date copyReportReception;

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    @JsonProperty("loss_possession_request")
    private Date lossPossessionRequest;

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    @JsonProperty("loss_possession_reception")
    private Date lossPossessionReception;

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    @JsonProperty("return_possession_request")
    private Date returnPossessionRequest;

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    @JsonProperty("return_possession_reception")
    private Date returnPossessionReception;

    @JsonProperty("robbery")
    private Boolean robbery;

    @JsonProperty("key_management")
    private Boolean keyManagement;

    @JsonProperty("account_management")
    private Boolean accountManagement;

    @JsonProperty("administrative_position")
    private Boolean administrativePosition;

    @JsonProperty("practice_id")
    private Long practiceId;

    @JsonProperty("re_registration")
    private Boolean reRegistration;

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    @JsonProperty("re_registration_request")
    private Date reRegistrationRequest;

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    @JsonProperty("re_registration_at")
    private Date reRegistrationAt;

    @JsonProperty("re_registration_salesforce_case")
    private String reRegistrationSaleforcesCase;

    @JsonProperty("certificate_duplication")
    private Boolean certificateDuplication;

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    @JsonProperty("certificate_duplication_request")
    private Date certificateDuplicationRequest;

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    @JsonProperty("certificate_duplication_at")
    private Date certificateDuplicationAt;

    @JsonProperty("certificate_duplication_salesforce_case")
    private String certificateDuplicationSalesforceCase;

    @JsonProperty("stamp")
    private Boolean stamp;

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    @JsonProperty("stamp_request")
    private Date stampRequest;

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    @JsonProperty("stamp_at")
    private Date stampAt;

    @JsonProperty("stamp_salesforce_case")
    private String stampSalesforceCase;

    @JsonProperty("demolition")
    private Boolean demolition;

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    @JsonProperty("demolition_request")
    private Date demolitionRequest;

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    @JsonProperty("demolition_at")
    private Date demolitionAt;

    @JsonProperty("demolition_salesforce_case")
    private String demolitionSalesforceCase;

    @JsonProperty("is_with_receptions")
    private Boolean isWithReceptions;

    @JsonProperty("is_under_seizure")
    private Boolean isUnderSeizure;

    @JsonProperty("event_type")
    private EventTypeEnum eventType;

    @JsonProperty("motivation")
    private String motivation;

    @JsonProperty("template_list")
    private List<EmailTemplateMessagingRequestV1> templateList;

    public EventTypeEnum getEventType() {
        return eventType;
    }

    public void setEventType(EventTypeEnum eventType) {
        this.eventType = eventType;
    }

    public String getMotivation() {
        return motivation;
    }

    public void setMotivation(String motivation) {
        this.motivation = motivation;
    }

    public List<EmailTemplateMessagingRequestV1> getTemplateList() {
        return templateList;
    }

    public void setTemplateList(List<EmailTemplateMessagingRequestV1> templateList) {
        this.templateList = templateList;
    }

    public Boolean getUnderSeizure() {
        return isUnderSeizure;
    }

    public void setUnderSeizure(Boolean underSeizure) {
        isUnderSeizure = underSeizure;
    }

    public Boolean getWithReceptions() {
        return isWithReceptions;
    }

    public void setWithReceptions(Boolean withReceptions) {
        isWithReceptions = withReceptions;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Boolean getOperationsCenterNotified() {
        return OperationsCenterNotified;
    }

    public void setOperationsCenterNotified(Boolean operationsCenterNotified) {
        OperationsCenterNotified = operationsCenterNotified;
    }

    public Boolean getTheftOccurredOnCenterAld() {
        return theftOccurredOnCenterAld;
    }

    public void setTheftOccurredOnCenterAld(Boolean theftOccurredOnCenterAld) {
        this.theftOccurredOnCenterAld = theftOccurredOnCenterAld;
    }

    public String getSupplierCode() {
        return supplierCode;
    }

    public void setSupplierCode(String supplierCode) {
        this.supplierCode = supplierCode;
    }

    public Boolean getComplaintAuthorityPolice() {
        return complaintAuthorityPolice;
    }

    public void setComplaintAuthorityPolice(Boolean complaintAuthorityPolice) {
        this.complaintAuthorityPolice = complaintAuthorityPolice;
    }

    public Boolean getComplaintAuthorityCc() {
        return complaintAuthorityCc;
    }

    public void setComplaintAuthorityCc(Boolean complaintAuthorityCc) {
        this.complaintAuthorityCc = complaintAuthorityCc;
    }

    public Boolean getComplaintAuthorityVvuu() {
        return complaintAuthorityVvuu;
    }

    public void setComplaintAuthorityVvuu(Boolean complaintAuthorityVvuu) {
        this.complaintAuthorityVvuu = complaintAuthorityVvuu;
    }

    public String getAuthorityData() {
        return authorityData;
    }

    public void setAuthorityData(String authorityData) {
        this.authorityData = authorityData;
    }

    public String getAuthorityTelephone() {
        return authorityTelephone;
    }

    public void setAuthorityTelephone(String authorityTelephone) {
        this.authorityTelephone = authorityTelephone;
    }

    public Date getFindDate() {
        if(findDate == null){
            return null;
        }
        return (Date)findDate.clone();
    }

    public void setFindDate(Date findDate) {
        if(findDate != null)
        {
            this.findDate = (Date)findDate.clone();
        } else {
            this.findDate = null;
        }
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public Boolean getFoundAbroad() {
        return foundAbroad;
    }

    public void setFoundAbroad(Boolean foundAbroad) {
        this.foundAbroad = foundAbroad;
    }

    public String getVehicleCo() {
        return vehicleCo;
    }

    public void setVehicleCo(String vehicleCo) {
        this.vehicleCo = vehicleCo;
    }

    public Boolean getSequestered() {
        return sequestered;
    }

    public void setSequestered(Boolean sequestered) {
        this.sequestered = sequestered;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Boolean getFindAuthorityPolice() {
        return findAuthorityPolice;
    }

    public void setFindAuthorityPolice(Boolean findAuthorityPolice) {
        this.findAuthorityPolice = findAuthorityPolice;
    }

    public Boolean getFindAuthorityCc() {
        return findAuthorityCc;
    }

    public void setFindAuthorityCc(Boolean findAuthorityCc) {
        this.findAuthorityCc = findAuthorityCc;
    }

    public Boolean getFindAuthorityVvuu() {
        return findAuthorityVvuu;
    }

    public void setFindAuthorityVvuu(Boolean findAuthorityVvuu) {
        this.findAuthorityVvuu = findAuthorityVvuu;
    }

    public String getFindAuthorityData() {
        return findAuthorityData;
    }

    public void setFindAuthorityData(String findAuthorityData) {
        this.findAuthorityData = findAuthorityData;
    }

    public String getFindAuthorityTelephone() {
        return findAuthorityTelephone;
    }

    public void setFindAuthorityTelephone(String findAuthorityTelephone) {
        this.findAuthorityTelephone = findAuthorityTelephone;
    }

    public String getTheftNotes() {
        return theftNotes;
    }

    public void setTheftNotes(String theftNotes) {
        this.theftNotes = theftNotes;
    }

    public String getFindNotes() {
        return findNotes;
    }

    public void setFindNotes(String findNotes) {
        this.findNotes = findNotes;
    }

    public Date getFirstKeyReception() {
        if(firstKeyReception == null){
            return null;
        }
        return (Date)firstKeyReception.clone();
    }

    public void setFirstKeyReception(Date firstKeyReception) {
        if(firstKeyReception != null)
        {
            this.firstKeyReception = (Date)firstKeyReception.clone();
        } else {
            this.firstKeyReception = null;
        }
    }

    public Date getSecondKeyReception() {
        if(secondKeyReception == null){
            return  null;
        }
        return (Date)secondKeyReception.clone();
    }

    public void setSecondKeyReception(Date secondKeyReception) {
        if(secondKeyReception != null)
        {
            this.secondKeyReception = (Date)secondKeyReception.clone();
        } else {
            this.secondKeyReception = null;
        }
    }

    public Date getOriginalComplaintReception() {
        if(originalComplaintReception == null){
            return null;
        }
        return (Date)originalComplaintReception.clone();
    }

    public void setOriginalComplaintReception(Date originalComplaintReception) {
        if(originalComplaintReception != null)
        {
            this.originalComplaintReception = (Date)originalComplaintReception.clone();
        } else {
            this.originalComplaintReception = null;
        }
    }

    public Date getCopyComplaintReception() {
        if(copyComplaintReception == null){
            return null;
        }
        return (Date)copyComplaintReception.clone();
    }

    public void setCopyComplaintReception(Date copyComplaintReception) {
        if(copyComplaintReception != null)
        {
            this.copyComplaintReception =(Date)copyComplaintReception.clone();
        } else {
            this.copyComplaintReception = null;
        }
    }

    public Date getOriginalReportReception() {
        if(originalReportReception == null){
            return null;
        }
        return (Date)originalReportReception.clone();
    }

    public void setOriginalReportReception(Date originalReportReception) {
        if(originalReportReception != null)
        {
            this.originalReportReception = (Date)originalReportReception.clone();
        } else {
            this.originalReportReception = null;
        }
    }

    public Date getCopyReportReception() {
        if(copyReportReception == null){
            return null;
        }
        return (Date)copyReportReception.clone();
    }

    public void setCopyReportReception(Date copyReportReception) {
        if(copyReportReception != null)
        {
            this.copyReportReception = (Date)copyReportReception.clone();
        } else {
            this.copyReportReception = null;
        }
    }

    public Date getLossPossessionRequest() {
        if(lossPossessionRequest == null){
            return null;
        }
        return (Date)lossPossessionRequest.clone();
    }

    public void setLossPossessionRequest(Date lossPossessionRequest) {
        if(lossPossessionRequest != null)
        {
            this.lossPossessionRequest = (Date)lossPossessionRequest.clone();
        } else {
            this.lossPossessionRequest = null;
        }
    }

    public Date getLossPossessionReception() {
        if(lossPossessionReception == null){
            return null;
        }
        return (Date)lossPossessionReception.clone();
    }

    public void setLossPossessionReception(Date lossPossessionReception) {
        if(lossPossessionReception != null)
        {
            this.lossPossessionReception = (Date)lossPossessionReception.clone();
        } else {
            this.lossPossessionReception = null;
        }
    }

    public Date getReturnPossessionRequest() {
        if(returnPossessionRequest == null){
            return null;
        }
        return (Date)returnPossessionRequest.clone();
    }

    public void setReturnPossessionRequest(Date returnPossessionRequest) {
        if(returnPossessionRequest != null)
        {
            this.returnPossessionRequest = (Date)returnPossessionRequest.clone();
        } else {
            this.returnPossessionRequest = null;
        }
    }

    public Date getReturnPossessionReception() {
        if(returnPossessionReception == null){
            return null;
        }
        return (Date)returnPossessionReception.clone();
    }

    public void setReturnPossessionReception(Date returnPossessionReception) {
        if(returnPossessionReception != null)
        {
            this.returnPossessionReception = (Date)returnPossessionReception.clone();
        } else {
            this.returnPossessionReception = null;
        }
    }

    public Boolean getRobbery() {
        return robbery;
    }

    public void setRobbery(Boolean robbery) {
        this.robbery = robbery;
    }

    public Boolean getKeyManagement() {
        return keyManagement;
    }

    public void setKeyManagement(Boolean keyManagement) {
        this.keyManagement = keyManagement;
    }

    public Boolean getAccountManagement() {
        return accountManagement;
    }

    public void setAccountManagement(Boolean accountManagement) {
        this.accountManagement = accountManagement;
    }

    public Boolean getAdministrativePosition() {
        return administrativePosition;
    }

    public void setAdministrativePosition(Boolean administrativePosition) {
        this.administrativePosition = administrativePosition;
    }

    public Long getPracticeId() {
        return practiceId;
    }

    public void setPracticeId(Long practiceId) {
        this.practiceId = practiceId;
    }

    public Boolean getReRegistration() {
        return reRegistration;
    }

    public void setReRegistration(Boolean reRegistration) {
        this.reRegistration = reRegistration;
    }

    public Date getReRegistrationRequest() {
        if(reRegistrationRequest == null){
            return null;
        }
        return (Date)reRegistrationRequest.clone();
    }

    public void setReRegistrationRequest(Date reRegistrationRequest) {
        if(reRegistrationRequest != null)
        {
            this.reRegistrationRequest = (Date)reRegistrationRequest.clone();
        } else {
            this.reRegistrationRequest = null;
        }
    }

    public Date getReRegistrationAt() {
        if(reRegistrationAt == null){
            return null;
        }
        return (Date)reRegistrationAt.clone();
    }

    public void setReRegistrationAt(Date reRegistrationAt) {
        if(reRegistrationAt != null)
        {
            this.reRegistrationAt = (Date)reRegistrationAt.clone();
        } else {
            this.reRegistrationAt = null;
        }
    }

    public String getReRegistrationSaleforcesCase() {
        return reRegistrationSaleforcesCase;
    }

    public void setReRegistrationSaleforcesCase(String reRegistrationSaleforcesCase) {
        this.reRegistrationSaleforcesCase = reRegistrationSaleforcesCase;
    }

    public Boolean getCertificateDuplication() {
        return certificateDuplication;
    }

    public void setCertificateDuplication(Boolean certificateDuplication) {
        this.certificateDuplication = certificateDuplication;
    }

    public Date getCertificateDuplicationRequest() {
        if(certificateDuplicationRequest == null){
            return null;
        }
        return (Date)certificateDuplicationRequest.clone();
    }

    public void setCertificateDuplicationRequest(Date certificateDuplicationRequest) {
        if(certificateDuplicationRequest != null)
        {
            this.certificateDuplicationRequest =(Date)certificateDuplicationRequest.clone();
        } else {
            this.certificateDuplicationRequest = null;
        }
    }

    public Date getCertificateDuplicationAt() {
        if(certificateDuplicationAt == null){
            return null;
        }
        return (Date)certificateDuplicationAt.clone();
    }

    public void setCertificateDuplicationAt(Date certificateDuplicationAt) {
        if(certificateDuplicationAt != null)
        {
            this.certificateDuplicationAt =(Date)certificateDuplicationAt.clone();
        } else {
            this.certificateDuplicationAt = null;
        }
    }

    public String getCertificateDuplicationSalesforceCase() {
        return certificateDuplicationSalesforceCase;
    }

    public void setCertificateDuplicationSalesforceCase(String certificateDuplicationSalesforceCase) {
        this.certificateDuplicationSalesforceCase = certificateDuplicationSalesforceCase;
    }

    public Boolean getStamp() {
        return stamp;
    }

    public void setStamp(Boolean stamp) {
        this.stamp = stamp;
    }

    public Date getStampRequest() {
        if(stampRequest == null){
            return null;
        }
        return (Date)stampRequest.clone();
    }

    public void setStampRequest(Date stampRequest) {
        if(stampRequest != null)
        {
            this.stampRequest = (Date)stampRequest.clone();
        } else {
            this.stampRequest = null;
        }
    }

    public Date getStampAt() {
        if(stampAt == null){
            return null;
        }
        return (Date)stampAt.clone();
    }

    public void setStampAt(Date stampAt) {
        if(stampAt != null)
        {
            this.stampAt = (Date)stampAt.clone();
        } else {
            this.stampAt = null;
        }
    }

    public String getStampSalesforceCase() {
        return stampSalesforceCase;
    }

    public void setStampSalesforceCase(String stampSalesforceCase) {
        this.stampSalesforceCase = stampSalesforceCase;
    }

    public Boolean getDemolition() {
        return demolition;
    }

    public void setDemolition(Boolean demolition) {
        this.demolition = demolition;
    }

    public Date getDemolitionRequest() {
        if(demolitionRequest == null){
            return null;
        }
        return (Date)demolitionRequest.clone();
    }

    public void setDemolitionRequest(Date demolitionRequest) {
        if(demolitionRequest != null)
        {
            this.demolitionRequest = (Date)demolitionRequest.clone();
        } else {
            this.demolitionAt = null;
        }
    }

    public Date getDemolitionAt() {
        if(demolitionAt == null){
            return null;
        }
        return (Date)demolitionAt.clone();
    }

    public void setDemolitionAt(Date demolitionAt) {
        if(demolitionAt != null)
        {
            this.demolitionAt = (Date)demolitionAt.clone();
        } else {
            this.demolitionAt = null;
        }
    }

    public String getDemolitionSalesforceCase() {
        return demolitionSalesforceCase;
    }

    public void setDemolitionSalesforceCase(String demolitionSalesforceCase) {
        this.demolitionSalesforceCase = demolitionSalesforceCase;
    }

    public boolean isFound() {
        return isFound;
    }

    public void setFound(boolean found) {
        isFound = found;
    }

    @Override
    public String toString() {
        return "Theft{" +
                "isFound=" + isFound +
                ", OperationsCenterNotified=" + OperationsCenterNotified +
                ", theftOccurredOnCenterAld=" + theftOccurredOnCenterAld +
                ", supplierCode='" + supplierCode + '\'' +
                ", complaintAuthorityPolice=" + complaintAuthorityPolice +
                ", complaintAuthorityCc=" + complaintAuthorityCc +
                ", complaintAuthorityVvuu=" + complaintAuthorityVvuu +
                ", authorityData='" + authorityData + '\'' +
                ", authorityTelephone='" + authorityTelephone + '\'' +
                ", findDate=" + findDate +
                ", hour='" + hour + '\'' +
                ", foundAbroad=" + foundAbroad +
                ", vehicleCo='" + vehicleCo + '\'' +
                ", sequestered=" + sequestered +
                ", address=" + address +
                ", findAuthorityPolice=" + findAuthorityPolice +
                ", findAuthorityCc=" + findAuthorityCc +
                ", findAuthorityVvuu=" + findAuthorityVvuu +
                ", findAuthorityData='" + findAuthorityData + '\'' +
                ", findAuthorityTelephone='" + findAuthorityTelephone + '\'' +
                ", theftNotes='" + theftNotes + '\'' +
                ", findNotes='" + findNotes + '\'' +
                ", firstKeyReception=" + firstKeyReception +
                ", secondKeyReception=" + secondKeyReception +
                ", originalComplaintReception=" + originalComplaintReception +
                ", copyComplaintReception=" + copyComplaintReception +
                ", originalReportReception=" + originalReportReception +
                ", copyReportReception=" + copyReportReception +
                ", lossPossessionRequest=" + lossPossessionRequest +
                ", lossPossessionReception=" + lossPossessionReception +
                ", returnPossessionRequest=" + returnPossessionRequest +
                ", returnPossessionReception=" + returnPossessionReception +
                ", robbery=" + robbery +
                ", keyManagement=" + keyManagement +
                ", accountManagement=" + accountManagement +
                ", administrativePosition=" + administrativePosition +
                ", practiceId=" + practiceId +
                ", reRegistration=" + reRegistration +
                ", reRegistrationRequest=" + reRegistrationRequest +
                ", reRegistrationAt=" + reRegistrationAt +
                ", reRegistrationSaleforcesCase='" + reRegistrationSaleforcesCase + '\'' +
                ", certificateDuplication=" + certificateDuplication +
                ", certificateDuplicationRequest=" + certificateDuplicationRequest +
                ", certificateDuplicationAt=" + certificateDuplicationAt +
                ", certificateDuplicationSalesforceCase='" + certificateDuplicationSalesforceCase + '\'' +
                ", stamp=" + stamp +
                ", stampRequest=" + stampRequest +
                ", stampAt=" + stampAt +
                ", stampSalesforceCase='" + stampSalesforceCase + '\'' +
                ", demolition=" + demolition +
                ", demolitionRequest=" + demolitionRequest +
                ", demolitionAt=" + demolitionAt +
                ", demolitionSalesforceCase='" + demolitionSalesforceCase + '\'' +
                ", isWithReceptions=" + isWithReceptions +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Theft theft = (Theft) o;

        return new EqualsBuilder().append(isFound, theft.isFound).append(OperationsCenterNotified, theft.OperationsCenterNotified)
                .append(theftOccurredOnCenterAld, theft.theftOccurredOnCenterAld).append(supplierCode, theft.supplierCode)
                .append(complaintAuthorityPolice, theft.complaintAuthorityPolice).append(complaintAuthorityCc, theft.complaintAuthorityCc)
                .append(complaintAuthorityVvuu, theft.complaintAuthorityVvuu).append(authorityData, theft.authorityData)
                .append(authorityTelephone, theft.authorityTelephone).append(findDate, theft.findDate).append(hour, theft.hour)
                .append(foundAbroad, theft.foundAbroad).append(vehicleCo, theft.vehicleCo).append(sequestered, theft.sequestered)
                .append(address, theft.address).append(findAuthorityPolice, theft.findAuthorityPolice).append(findAuthorityCc, theft.findAuthorityCc)
                .append(findAuthorityVvuu, theft.findAuthorityVvuu).append(findAuthorityData, theft.findAuthorityData)
                .append(findAuthorityTelephone, theft.findAuthorityTelephone).append(theftNotes, theft.theftNotes)
                .append(findNotes, theft.findNotes).append(firstKeyReception, theft.firstKeyReception)
                .append(secondKeyReception, theft.secondKeyReception).append(originalComplaintReception, theft.originalComplaintReception)
                .append(copyComplaintReception, theft.copyComplaintReception).append(originalReportReception, theft.originalReportReception)
                .append(copyReportReception, theft.copyReportReception).append(lossPossessionRequest, theft.lossPossessionRequest)
                .append(lossPossessionReception, theft.lossPossessionReception).append(returnPossessionRequest, theft.returnPossessionRequest)
                .append(returnPossessionReception, theft.returnPossessionReception).append(robbery, theft.robbery)
                .append(keyManagement, theft.keyManagement).append(accountManagement, theft.accountManagement)
                .append(administrativePosition, theft.administrativePosition).append(practiceId, theft.practiceId)
                .append(reRegistration, theft.reRegistration).append(reRegistrationRequest, theft.reRegistrationRequest)
                .append(reRegistrationAt, theft.reRegistrationAt).append(reRegistrationSaleforcesCase, theft.reRegistrationSaleforcesCase)
                .append(certificateDuplication, theft.certificateDuplication).append(certificateDuplicationRequest, theft.certificateDuplicationRequest)
                .append(certificateDuplicationAt, theft.certificateDuplicationAt)
                .append(certificateDuplicationSalesforceCase, theft.certificateDuplicationSalesforceCase)
                .append(stamp, theft.stamp).append(stampRequest, theft.stampRequest).append(stampAt, theft.stampAt)
                .append(stampSalesforceCase, theft.stampSalesforceCase).append(demolition, theft.demolition)
                .append(demolitionRequest, theft.demolitionRequest).append(demolitionAt, theft.demolitionAt)
                .append(demolitionSalesforceCase, theft.demolitionSalesforceCase).append(isWithReceptions, theft.isWithReceptions)
                .append(isUnderSeizure, theft.isUnderSeizure).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(isFound)
                .append(OperationsCenterNotified).append(theftOccurredOnCenterAld).append(supplierCode)
                .append(complaintAuthorityPolice).append(complaintAuthorityCc).append(complaintAuthorityVvuu)
                .append(authorityData).append(authorityTelephone).append(findDate).append(hour).append(foundAbroad)
                .append(vehicleCo).append(sequestered).append(address).append(findAuthorityPolice).append(findAuthorityCc)
                .append(findAuthorityVvuu).append(findAuthorityData).append(findAuthorityTelephone).append(theftNotes)
                .append(findNotes).append(firstKeyReception).append(secondKeyReception).append(originalComplaintReception)
                .append(copyComplaintReception).append(originalReportReception).append(copyReportReception)
                .append(lossPossessionRequest).append(lossPossessionReception).append(returnPossessionRequest)
                .append(returnPossessionReception).append(robbery).append(keyManagement).append(accountManagement)
                .append(administrativePosition).append(practiceId).append(reRegistration).append(reRegistrationRequest)
                .append(reRegistrationAt).append(reRegistrationSaleforcesCase).append(certificateDuplication)
                .append(certificateDuplicationRequest).append(certificateDuplicationAt).append(certificateDuplicationSalesforceCase)
                .append(stamp).append(stampRequest).append(stampAt).append(stampSalesforceCase).append(demolition)
                .append(demolitionRequest).append(demolitionAt).append(demolitionSalesforceCase).append(isWithReceptions)
                .append(isUnderSeizure).toHashCode();
    }
}
