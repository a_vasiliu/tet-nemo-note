package com.doing.nemo.claims.entity.enumerated;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;

public enum PracticeTypeEnum implements Serializable {

    SEIZURE("seizure"),
    RELEASE_FROM_SEIZURE("release_from_seizure"),
    LOSS_POSSESSION("loss_possession"),
    RETURN_POSSESSION("return_possession"),
    MISAPPROPRIATION("misappropriation"),
    RECOVERY("recovery"),
    GENERIC("generic");


    private String practiceTypeEnum;

    private static Logger LOGGER = LoggerFactory.getLogger(PracticeTypeEnum.class);

    private PracticeTypeEnum(String practiceTypeEnum) {
        this.practiceTypeEnum = practiceTypeEnum;
    }

    public String getValue() {
        return this.practiceTypeEnum.toUpperCase();
    }

    @JsonCreator
    public static PracticeTypeEnum create(String practiceTypeEnum) {

        practiceTypeEnum = practiceTypeEnum.replace(" - ", "_");
        practiceTypeEnum = practiceTypeEnum.replace('-', '_');
        practiceTypeEnum = practiceTypeEnum.replace(' ', '_');

        if (practiceTypeEnum != null) {
            for (PracticeTypeEnum val : PracticeTypeEnum.values()) {
                if (practiceTypeEnum.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        LOGGER.debug("Bad parameters exception. Enum class " + PracticeTypeEnum.class.getSimpleName() + " doesn't accept this value: " + practiceTypeEnum);
        throw new BadParametersException("Bad parameters exception. Enum class " + PracticeTypeEnum.class.getSimpleName() + " doesn't accept this value: " + practiceTypeEnum);
    }

    @JsonValue
    public String toValue() {
        for (PracticeTypeEnum val : PracticeTypeEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }
}
