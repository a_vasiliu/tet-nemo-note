package com.doing.nemo.claims.entity.esb;

import com.doing.nemo.claims.entity.esb.VehicleESB.TestEsb;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LinkEsb implements Serializable {

    private static final long serialVersionUID = -3460726120333751971L;

    @JsonProperty("rel")
    private String rel;

    @JsonProperty("href")
    private String href;

    @JsonProperty("test")
    private List<TestEsb> testList;

    public String getRel() {
        return rel;
    }

    public void setRel(String rel) {
        this.rel = rel;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public List<TestEsb> getTestList() {
        if(testList == null){
            return null;
        }
        return new ArrayList<>(testList);
    }

    public void setTestList(List<TestEsb> testList) {
        if(testList != null)
        {
            this.testList = new ArrayList<>(testList);
        } else {
            this.testList = null;
        }
    }

    @Override
    public String toString() {
        return "LinkEsb{" +
                "rel='" + rel + '\'' +
                ", href='" + href + '\'' +
                ", testList=" + testList +
                '}';
    }
}