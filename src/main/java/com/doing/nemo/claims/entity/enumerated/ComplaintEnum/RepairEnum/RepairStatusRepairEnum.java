package com.doing.nemo.claims.entity.enumerated.ComplaintEnum.RepairEnum;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;

public enum RepairStatusRepairEnum implements Serializable {
    CONFIRM("confirm"),
    TO_CONFIRM("to_confirm");

    private static Logger LOGGER = LoggerFactory.getLogger(RepairStatusRepairEnum.class);
    private String statusRepairEntity;

    private RepairStatusRepairEnum(String statusRepairEntity) {
        this.statusRepairEntity = statusRepairEntity;
    }

    @JsonCreator
    public static RepairStatusRepairEnum create(String statusRepairEntity) {

        statusRepairEntity = statusRepairEntity.replace(" - ", "_");
        statusRepairEntity = statusRepairEntity.replace('-', '_');
        statusRepairEntity = statusRepairEntity.replace(' ', '_');

        if (statusRepairEntity != null) {
            for (RepairStatusRepairEnum val : RepairStatusRepairEnum.values()) {
                if (statusRepairEntity.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        LOGGER.debug("Bad parameters exception. Enum class " + RepairStatusRepairEnum.class.getSimpleName() + " doesn't accept this value: " + statusRepairEntity);
        throw new BadParametersException("Bad parameters exception. Enum class " + RepairStatusRepairEnum.class.getSimpleName() + " doesn't accept this value: " + statusRepairEntity);

    }

    public String getValue() {
        return this.statusRepairEntity.toUpperCase();
    }

    @JsonValue
    public String toValue() {
        for (RepairStatusRepairEnum val : RepairStatusRepairEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }
}
