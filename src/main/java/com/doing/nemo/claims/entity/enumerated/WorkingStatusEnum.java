package com.doing.nemo.claims.entity.enumerated;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;

public enum WorkingStatusEnum implements Serializable {
    TO_ESTIMATE("to_estimate"),
    TO_SEND("to_send"),
    PRE_AUTHORITY_REJECTED("pre_authority_rejected"),
    TO_CHANNEL("to_channel"),
    APPROVING("approving"),
    DECLINED("declined"),
    WAITING_FOR_UNFREEZE("waiting_for_unfreeze"),
    WAITING_FOR_ACCEPTANCE("waiting_for_acceptance"),
    SUPERVISOR_APPROVING("supervisor_approving"),
    SUPERVISOR_REJECTED("supervisor_rejected"),
    WAITING_PARTS("waiting_parts"),
    RE_APPROVING("re_approving"),
    RE_DECLINED("re_declined"),
    C_FLOW_APPROVING("c_flow_approving"),
    C_FLOW_REJECTED("c_flow_rejected"),
    WAITING_FOR_WORKING("waiting_for_working"),
    WORKING("working"),
    INTEGRATION_TO_SEND("integration_to_send"),
    CLOSED_WITH_LIQUIDATION("closed_with_liquidation"),
    CLOSED_WITH_REJECTION("closed_with_rejection"),
    CLOSED("closed"),
    AUTHORIZED("authorized"),
    REJECTED("rejected");

    private String workingStatus;

    private static Logger LOGGER = LoggerFactory.getLogger(WorkingStatusEnum.class);

    private WorkingStatusEnum(String workingStatus) {
        this.workingStatus = workingStatus;
    }

    public String getValue() {
        return this.workingStatus.toUpperCase();
    }

    @JsonCreator
    public static WorkingStatusEnum create(String workingStatus) {

        workingStatus = workingStatus.replace(" - ", "_");
        workingStatus = workingStatus.replace('-', '_');
        workingStatus = workingStatus.replace(' ', '_');

        if (workingStatus != null) {
            for (WorkingStatusEnum val : WorkingStatusEnum.values()) {
                if (workingStatus.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        LOGGER.debug("Bad parameters exception. Enum class " + WorkingStatusEnum.class.getSimpleName() + " doesn't accept this value: " + workingStatus);
        throw new BadParametersException("Bad parameters exception. Enum class " + WorkingStatusEnum.class.getSimpleName() + " doesn't accept this value: " + workingStatus);
    }

    @JsonValue
    public String toValue() {
        for (WorkingStatusEnum val : WorkingStatusEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }

}
