package com.doing.nemo.claims.entity.esb.CustomerESB;

import com.doing.nemo.claims.entity.esb.AddressEsb;
import com.doing.nemo.claims.entity.esb.LinkEsb;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomerEsb implements Serializable {

    private static final long serialVersionUID = 278978953606424531L;

    @JsonProperty("customer_id")
    private String customerId;

    @JsonProperty("trading_name")
    private String tradingName;

    @JsonProperty("legal_name")
    private String legalName;

    @JsonProperty("vat_number")
    private String vatNumber;

    @JsonProperty("official_registration")
    private String officialRegistration;

    @JsonProperty("status")
    private String status;

    @JsonProperty("legal_entity")
    private String legalEntity;

    @JsonProperty("email")
    private String email;

    @JsonProperty("phonenr")
    private String phonenr;

    @JsonProperty("pec")
    private String pec;

    @JsonProperty("main_address")
    private AddressEsb mainAddress;

    @JsonProperty("leasing_companies")
    private List<LeasingCompanieEsb> leasingCompanieList;

    @JsonProperty("links")
    private List<LinkEsb> linksList;

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getTradingName() {
        return tradingName;
    }

    public void setTradingName(String tradingName) {
        this.tradingName = tradingName;
    }

    public String getLegalName() {
        return legalName;
    }

    public void setLegalName(String legalName) {
        this.legalName = legalName;
    }

    public String getVatNumber() {
        return vatNumber;
    }

    public void setVatNumber(String vatNumber) {
        this.vatNumber = vatNumber;
    }

    public String getOfficialRegistration() {
        return officialRegistration;
    }

    public void setOfficialRegistration(String officialRegistration) {
        this.officialRegistration = officialRegistration;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLegalEntity() {
        return legalEntity;
    }

    public void setLegalEntity(String legalEntity) {
        this.legalEntity = legalEntity;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhonenr() {
        return phonenr;
    }

    public void setPhonenr(String phonenr) {
        this.phonenr = phonenr;
    }

    public String getPec() {
        return pec;
    }

    public void setPec(String pec) {
        this.pec = pec;
    }

    public AddressEsb getMainAddress() {
        return mainAddress;
    }

    public void setMainAddress(AddressEsb mainAddress) {
        this.mainAddress = mainAddress;
    }

    public List<LeasingCompanieEsb> getLeasingCompanieList() {
        if(leasingCompanieList == null){
            return null;
        }
        return new ArrayList<>(leasingCompanieList);
    }

    public void setLeasingCompanieList(List<LeasingCompanieEsb> leasingCompanieList) {
        if(leasingCompanieList != null)
        {
            this.leasingCompanieList = new ArrayList<>(leasingCompanieList);
        } else {
            this.leasingCompanieList = null;
        }
    }

    public List<LinkEsb> getLinksList() {
        if(linksList == null){
            return null;
        }
        return new ArrayList<>(linksList);
    }

    public void setLinksList(List<LinkEsb> linksList) {
        if(linksList != null)
        {
            this.linksList =  new ArrayList<>(linksList);
        } else {
            this.linksList = null;
        }
    }

    @Override
    public String toString() {
        return "CustomerEsb{" +
                "customerId='" + customerId + '\'' +
                ", tradingName='" + tradingName + '\'' +
                ", legalName='" + legalName + '\'' +
                ", vatNumber='" + vatNumber + '\'' +
                ", officialRegistration='" + officialRegistration + '\'' +
                ", status='" + status + '\'' +
                ", legalEntity='" + legalEntity + '\'' +
                ", email='" + email + '\'' +
                ", phonenr='" + phonenr + '\'' +
                ", pec='" + pec + '\'' +
                ", mainAddress=" + mainAddress +
                ", leasingCompanieList=" + leasingCompanieList +
                ", linksList=" + linksList +
                '}';
    }
}
