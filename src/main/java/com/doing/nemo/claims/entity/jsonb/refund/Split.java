package com.doing.nemo.claims.entity.jsonb.refund;

import com.doing.nemo.claims.entity.enumerated.RefundEnum.RefundTypeEnum;
import com.doing.nemo.claims.entity.enumerated.RefundEnum.TypeEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Split implements Serializable, Comparable<Split> {

    @JsonProperty
    private UUID id;

    @JsonProperty("issue_date")
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    private Date issueDate;

    @JsonProperty("type")
    private TypeEnum type;

    @JsonProperty("refund_type")
    private RefundTypeEnum refundType;

    @JsonProperty("legal_fees")
    private Double legalFees;

    @JsonProperty("technical_stop")
    private Double technicalStop;

    @JsonProperty("material_amount")
    private Double materialAmount;

    @JsonProperty("received_sum")
    private Double receivedSum;

    @JsonProperty("total_paid")
    private Double totalPaid;

    @JsonProperty("note")
    private String note;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Date getIssueDate() {
        if(issueDate == null){
            return null;
        }
        return (Date)issueDate.clone();
    }

    public void setIssueDate(Date issueDate) {
        if(issueDate != null)
        {
            this.issueDate = (Date)issueDate.clone();
        } else {
            this.issueDate = null;
        }
    }

    public TypeEnum getType() {
        return type;
    }

    public void setType(TypeEnum type) {
        this.type = type;
    }

    public RefundTypeEnum getRefundType() {
        return refundType;
    }

    public void setRefundType(RefundTypeEnum refundType) {
        this.refundType = refundType;
    }

    public Double getReceivedSum() {
        return receivedSum;
    }

    public void setReceivedSum(Double receivedSum) {
        this.receivedSum = receivedSum;
    }

    public Double getTotalPaid() {
        return totalPaid;
    }

    public void setTotalPaid(Double totalPaid) {
        this.totalPaid = totalPaid;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Double getLegalFees() {
        return legalFees;
    }

    public void setLegalFees(Double legalFees) {
        this.legalFees = legalFees;
    }

    public Double getTechnicalStop() {
        return technicalStop;
    }

    public void setTechnicalStop(Double technicalStop) {
        this.technicalStop = technicalStop;
    }

    public Double getMaterialAmount() {
        return materialAmount;
    }

    public void setMaterialAmount(Double materialAmount) {
        this.materialAmount = materialAmount;
    }

    @Override
    public String toString() {
        return "Split{" +
                "id=" + id +
                ", issueDate=" + issueDate +
                ", type=" + type +
                ", refundType=" + refundType +
                ", legalFees=" + legalFees +
                ", technicalStop=" + technicalStop +
                ", materialAmount=" + materialAmount +
                ", receivedSum=" + receivedSum +
                ", totalPaid=" + totalPaid +
                ", note='" + note + '\'' +
                '}';
    }

    @Override
    public int compareTo(Split split) {
        return this.issueDate.compareTo(split.issueDate);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Split split = (Split) o;

        return new EqualsBuilder().append(id, split.id).append(issueDate, split.issueDate).append(type, split.type)
                .append(refundType, split.refundType).append(legalFees, split.legalFees).append(technicalStop, split.technicalStop)
                .append(materialAmount, split.materialAmount).append(receivedSum, split.receivedSum).append(totalPaid, split.totalPaid)
                .append(note, split.note).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(id).append(issueDate).append(type)
                .append(refundType).append(legalFees).append(technicalStop).append(materialAmount).append(receivedSum)
                .append(totalPaid).append(note).toHashCode();
    }
}
