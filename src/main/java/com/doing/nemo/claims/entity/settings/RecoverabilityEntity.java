package com.doing.nemo.claims.entity.settings;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "recoverability")
public class RecoverabilityEntity implements Serializable {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Type(type = "org.hibernate.type.PostgresUUIDType")
    private UUID id;

    @Column(name = "management")
    private String management;

    @OneToOne
    @JoinColumn(name = "claims_type")
    private ClaimsTypeEntity claimsType;

    @Column(name = "counterpart")
    private Boolean counterpart;

    @Column(name = "recoverability")
    private Boolean recoverability;

    @Column(name = "percent_recoverability")
    private Double percentRecoverability;

    @Column(name = "is_active")
    private Boolean isActive;

    public RecoverabilityEntity() {
    }

    public RecoverabilityEntity(String management, ClaimsTypeEntity claimsType, Boolean counterpart, Boolean recoverability, Double percentRecoverability, Boolean isActive) {
        this.management = management;
        this.claimsType = claimsType;
        this.counterpart = counterpart;
        this.recoverability = recoverability;
        this.percentRecoverability = percentRecoverability;
        this.isActive = isActive;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getManagement() {
        return management;
    }

    public void setManagement(String management) {
        this.management = management;
    }

    public ClaimsTypeEntity getClaimsType() {
        return claimsType;
    }

    public void setClaimsType(ClaimsTypeEntity claimsType) {
        this.claimsType = claimsType;
    }

    public Boolean getCounterpart() {
        return counterpart;
    }

    public void setCounterpart(Boolean counterpart) {
        this.counterpart = counterpart;
    }

    public Boolean getRecoverability() {
        return recoverability;
    }

    public void setRecoverability(Boolean recoverability) {
        this.recoverability = recoverability;
    }

    public Double getPercentRecoverability() {
        return percentRecoverability;
    }

    public void setPercentRecoverability(Double percentRecoverability) {
        this.percentRecoverability = percentRecoverability;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    @Override
    public String toString() {
        return "RecoverabilityEntity{" +
                "id=" + id +
                ", management='" + management + '\'' +
                ", claimsType=" + claimsType +
                ", counterpart=" + counterpart +
                ", recoverability=" + recoverability +
                ", percentRecoverability=" + percentRecoverability +
                ", isActive=" + isActive +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RecoverabilityEntity that = (RecoverabilityEntity) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getManagement(), that.getManagement()) &&
                Objects.equals(getClaimsType(), that.getClaimsType()) &&
                Objects.equals(getCounterpart(), that.getCounterpart()) &&
                Objects.equals(getRecoverability(), that.getRecoverability()) &&
                Objects.equals(getPercentRecoverability(), that.getPercentRecoverability()) &&
                Objects.equals(getActive(), that.getActive());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getManagement(), getClaimsType(), getCounterpart(), getRecoverability(), getPercentRecoverability(), getActive());
    }
}
