package com.doing.nemo.claims.entity.jsonb;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Deponent implements Serializable {

    private static final long serialVersionUID = -470984649183741652L;

    @JsonProperty("firstname")
    private String firstname;

    @JsonProperty("lastname")
    private String lastname;

    @JsonProperty("main_address")
    private Address address;

    @JsonProperty("phone")
    private String phone;

    @JsonProperty("email")
    private String email;

    @JsonProperty("fiscal_code")
    private String fiscalCode;

    @JsonProperty("attachment_list")
    private List<String> attachments;


    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFiscalCode() {
        return fiscalCode;
    }

    public void setFiscalCode(String fiscalCode) {
        this.fiscalCode = fiscalCode;
    }

    public List<String> getAttachments() {
        if(attachments == null){
            return null;
        }
        return new ArrayList<>(attachments);
    }

    public void setAttachments(List<String> attachments) {
        if(attachments != null)
        {
            this.attachments = new ArrayList<>(attachments);
        } else {
            this.attachments = null;
        }
    }

    @Override
    public String toString() {
        return "Deponent{" +
                "firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", address=" + address +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", fiscalCode='" + fiscalCode + '\'' +
                ", attachments=" + attachments +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Deponent deponent = (Deponent) o;

        return new EqualsBuilder().append(firstname, deponent.firstname).append(lastname, deponent.lastname)
                .append(address, deponent.address).append(phone, deponent.phone).append(email, deponent.email)
                .append(fiscalCode, deponent.fiscalCode).append(attachments, deponent.attachments).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(firstname).append(lastname)
                .append(address).append(phone).append(email).append(fiscalCode).append(attachments).toHashCode();
    }
}
