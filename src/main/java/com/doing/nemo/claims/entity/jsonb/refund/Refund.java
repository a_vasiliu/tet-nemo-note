package com.doing.nemo.claims.entity.jsonb.refund;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Refund implements Serializable {

    private static final long serialVersionUID = 8034674077331837526L;

    @JsonProperty("po_sum")
    private Double poSum;

    @JsonProperty("wreck")
    private Boolean wreck;

    @JsonProperty("wreck_value_pre")
    private Double wreckValuePre;

    @JsonProperty("wreck_value_post")
    private Double wreckValuePost;

    @JsonProperty("NBV")
    private String NBV;

    @JsonProperty("franchise_amount_fcm")
    private Double franchiseAmountFcm;

    @JsonProperty("total_refund_expected")
    private Double totalRefundExpected;

    @JsonProperty("total_liquidation_received")
    private Double totalLiquidationReceived;

    @JsonProperty("definition_date")
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    private Date definitionDate;


    @JsonProperty("blu_eurotax")
    private String bluEurotax;

    @JsonProperty("amount_to_be_debited")
    private Double amountToBeDebited;

    @JsonProperty("delta_debit_date")
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    private Date deltaDebitDate;


    @JsonProperty("split_list")
    private List<Split> splitList;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Double getPoSum() {
        return poSum;
    }

    public void setPoSum(Double poSum) {
        this.poSum = poSum;
    }

    public Boolean getWreck() {
        return wreck;
    }

    public Boolean isWreck() {
        return wreck == null ? false : wreck;
    }

    public void setWreck(Boolean wreck) {
        this.wreck = wreck;
    }

    public Double getWreckValuePre() {
        return wreckValuePre;
    }

    public void setWreckValuePre(Double wreckValuePre) {
        this.wreckValuePre = wreckValuePre;
    }

    public Double getWreckValuePost() {
        return wreckValuePost;
    }

    public void setWreckValuePost(Double wreckValuePost) {
        this.wreckValuePost = wreckValuePost;
    }

    public String getNBV() {
        return NBV;
    }

    public void setNBV(String NBV) {
        this.NBV = NBV;
    }

    public Double getFranchiseAmountFcm() {
        return franchiseAmountFcm;
    }

    public void setFranchiseAmountFcm(Double franchiseAmountFcm) {
        this.franchiseAmountFcm = franchiseAmountFcm;
    }

    public Double getTotalRefundExpected() {
        return totalRefundExpected;
    }

    public void setTotalRefundExpected(Double totalRefundExpected) {
        this.totalRefundExpected = totalRefundExpected;
    }

    public Double getTotalLiquidationReceived() {
        return totalLiquidationReceived;
    }

    public void setTotalLiquidationReceived(Double totalLiquidationReceived) {
        this.totalLiquidationReceived = totalLiquidationReceived;
    }

    public Date getDefinitionDate() {
        if(definitionDate == null){
            return null;
        }
        return (Date)definitionDate.clone();
    }

    public void setDefinitionDate(Date definitionDate) {
        if(definitionDate != null)
        {
            this.definitionDate = (Date)definitionDate.clone();
        } else {
            this.definitionDate = null;
        }
    }

    public List<Split> getSplitList() {
        if(splitList == null){
            return null;
        }
        return new ArrayList<>(splitList);
    }

    public void setSplitList(List<Split> splitList) {
        if(splitList != null)
        {
            this.splitList = new ArrayList<>(splitList);
        } else {
            this.splitList = null;
        }
    }

    public String getBluEurotax() {
        return bluEurotax;
    }

    public void setBluEurotax(String bluEurotax) {
        this.bluEurotax = bluEurotax;
    }

    public Double getAmountToBeDebited() {
        return amountToBeDebited;
    }

    public void setAmountToBeDebited(Double amountToBeDebited) {
        this.amountToBeDebited = amountToBeDebited;
    }

    public Date getDeltaDebitDate() {
        if(deltaDebitDate == null){
            return null;
        }
        return (Date)deltaDebitDate.clone();
    }

    public void setDeltaDebitDate(Date deltaDebitDate) {
        if(deltaDebitDate != null)
        {
            this.deltaDebitDate = (Date)deltaDebitDate.clone();
        } else {
            this.deltaDebitDate = null;
        }
    }

    @Override
    public String toString() {
        return "Refund{" +
                "poSum=" + poSum +
                ", wreck=" + wreck +
                ", wreckValuePre=" + wreckValuePre +
                ", wreckValuePost=" + wreckValuePost +
                ", NBV='" + NBV + '\'' +
                ", franchiseAmountFcm=" + franchiseAmountFcm +
                ", totalRefundExpected=" + totalRefundExpected +
                ", totalLiquidationReceived=" + totalLiquidationReceived +
                ", definitionDate=" + definitionDate +
                ", bluEurotax='" + bluEurotax + '\'' +
                ", amountToBeDebited=" + amountToBeDebited +
                ", deltaDebitDate=" + deltaDebitDate +
                ", splitList=" + splitList +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Refund refund = (Refund) o;

        return new EqualsBuilder().append(poSum, refund.poSum).append(wreck, refund.wreck)
                .append(wreckValuePre, refund.wreckValuePre).append(wreckValuePost, refund.wreckValuePost)
                .append(NBV, refund.NBV).append(franchiseAmountFcm, refund.franchiseAmountFcm).append(totalRefundExpected, refund.totalRefundExpected)
                .append(totalLiquidationReceived, refund.totalLiquidationReceived).append(definitionDate, refund.definitionDate)
                .append(bluEurotax, refund.bluEurotax).append(amountToBeDebited, refund.amountToBeDebited).append(deltaDebitDate, refund.deltaDebitDate)
                .append(splitList, refund.splitList).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(poSum).append(wreck).append(wreckValuePre)
                .append(wreckValuePost).append(NBV).append(franchiseAmountFcm).append(totalRefundExpected)
                .append(totalLiquidationReceived).append(definitionDate).append(bluEurotax).append(amountToBeDebited)
                .append(deltaDebitDate).append(splitList).toHashCode();
    }
}
