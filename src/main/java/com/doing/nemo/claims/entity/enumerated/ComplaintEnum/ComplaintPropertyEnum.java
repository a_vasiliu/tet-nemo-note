package com.doing.nemo.claims.entity.enumerated.ComplaintEnum;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;

public enum ComplaintPropertyEnum implements Serializable {
    OUR("our"),
    OTHERS("others");

    private static Logger LOGGER = LoggerFactory.getLogger(ComplaintPropertyEnum.class);
    private String propertyEntity;

    private ComplaintPropertyEnum(String propertyEntity) {
        this.propertyEntity = propertyEntity;
    }

    @JsonCreator
    public static ComplaintPropertyEnum create(String propertyEntity) {

        propertyEntity = propertyEntity.replace(" - ", "_");
        propertyEntity = propertyEntity.replace('-', '_');
        propertyEntity = propertyEntity.replace(' ', '_');

        if (propertyEntity != null) {
            for (ComplaintPropertyEnum val : ComplaintPropertyEnum.values()) {
                if (propertyEntity.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        LOGGER.debug("Error in CostItem, duplicate code not admitted");
        throw new BadParametersException("Bad parameters exception. Enum class " + ComplaintPropertyEnum.class.getSimpleName() + " doesn't accept this value: " + propertyEntity);
    }

    public String getValue() {
        return this.propertyEntity.toUpperCase();
    }

    @JsonValue
    public String toValue() {
        for (ComplaintPropertyEnum val : ComplaintPropertyEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }
}
