package com.doing.nemo.claims.entity.enumerated.PolicyEnum;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;

public enum PolicySuffixEnum implements Serializable {
    //"P" = polizza
    P("p"),
    //"C" = Contract Tipe
    C("c");

    private static Logger LOGGER = LoggerFactory.getLogger(PolicySuffixEnum.class);
    private String policySufEnum;

    private PolicySuffixEnum(String policySufEnum) {
        this.policySufEnum = policySufEnum;
    }

    @JsonCreator
    public static PolicySuffixEnum create(String policySufEnum) {

        if (policySufEnum != null) {

            for (PolicySuffixEnum val : PolicySuffixEnum.values()) {
                if (policySufEnum.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        LOGGER.debug("Bad parameters exception. Enum class " + PolicySuffixEnum.class.getSimpleName() + " doesn't accept this value: " + policySufEnum);
        throw new BadParametersException("Bad parameters exception. Enum class " + PolicySuffixEnum.class.getSimpleName() + " doesn't accept this value: " + policySufEnum);
    }

    public String getValue() {
        return this.policySufEnum.toUpperCase();
    }

    @JsonValue
    public String toValue() {
        for (PolicySuffixEnum val : PolicySuffixEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }
}
