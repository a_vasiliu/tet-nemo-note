package com.doing.nemo.claims.entity.enumerated.ClaimsEnum;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public enum ClaimsFlowEnum implements Serializable {
    FUL("ful"),
    FNI("fni"),
    FCM("fcm"),
    NO("no");

    private static Logger LOGGER = LoggerFactory.getLogger(ClaimsFlowEnum.class);
    private String typrEntity;

    private ClaimsFlowEnum(String typeEntity) {
        this.typrEntity = typeEntity;
    }

    @JsonCreator
    public static ClaimsFlowEnum create(String typeEntity) {

        typeEntity = typeEntity.replace(" - ", "_");
        typeEntity = typeEntity.replace('-', '_');
        typeEntity = typeEntity.replace(' ', '_');

        if (typeEntity != null) {
            for (ClaimsFlowEnum val : ClaimsFlowEnum.values()) {
                if (typeEntity.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        LOGGER.debug("Bad parameters exception. Enum class " + ClaimsFlowEnum.class.getSimpleName() + " doesn't accept this value: " + typeEntity);
        throw new BadParametersException("Bad parameters exception. Enum class " + ClaimsFlowEnum.class.getSimpleName() + " doesn't accept this value: " + typeEntity);
    }

    public static List<ClaimsFlowEnum> getFlowListFromString(List<String> flowListString) {

        List<ClaimsFlowEnum> flowsList = new LinkedList<>();
        for (String att : flowListString) {
            flowsList.add(ClaimsFlowEnum.create(att));
        }
        return flowsList;
    }

    public String getValue() {
        return this.typrEntity.toUpperCase();
    }

    @JsonValue
    public String toValue() {
        for (ClaimsFlowEnum val : ClaimsFlowEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }


    public static List<String> getStringListFromFlowList(List<ClaimsFlowEnum> flowList) {

        List<String> flowsListString = new LinkedList<>();
        for (ClaimsFlowEnum att : flowList) {
            flowsListString.add(att.getValue());
        }
        return flowsListString;
    }
}
