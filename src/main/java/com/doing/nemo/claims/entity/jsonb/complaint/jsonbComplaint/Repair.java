package com.doing.nemo.claims.entity.jsonb.complaint.jsonbComplaint;

import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.RepairEnum.RepairStatusRepairEnum;
import com.doing.nemo.claims.entity.jsonb.Address;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Repair implements Serializable {

    private static final long serialVersionUID = -4583404268671829796L;

    @JsonProperty("status_repair")
    private RepairStatusRepairEnum statusRepair;

    @JsonProperty("vehicle_value")
    private Double vehicleValue;

    @JsonProperty("blocks_repair")
    private Boolean blocksRepair;

    @JsonProperty("unrepairable")
    private Boolean unrepairable;

    @JsonProperty("motivation")
    private String motivation;

    @JsonProperty("repairer")
    private String repairer;

    @JsonProperty("phone")
    private String phone;

    @JsonProperty("email")
    private String email;

    @JsonProperty("address")
    private Address address;

    public Repair() {
    }

    public Repair(RepairStatusRepairEnum statusRepair, Double vehicleValue, Boolean blocksRepair, Boolean unrepairable,
                  String motivation, String repairer, String phone, String email, Address address) {

        this.statusRepair = statusRepair;
        this.vehicleValue = vehicleValue;
        this.blocksRepair = blocksRepair;
        this.unrepairable = unrepairable;
        this.motivation = motivation;
        this.repairer = repairer;
        this.phone = phone;
        this.email = email;
        this.address = address;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public RepairStatusRepairEnum getStatusRepair() {
        return statusRepair;
    }

    public void setStatusRepair(RepairStatusRepairEnum statusRepair) {
        this.statusRepair = statusRepair;
    }

    public Double getVehicleValue() {
        return vehicleValue;
    }

    public void setVehicleValue(Double vehicleValue) {
        this.vehicleValue = vehicleValue;
    }

    public Boolean getBlocksRepair() {
        return blocksRepair;
    }

    public void setBlocksRepair(Boolean blocksRepair) {
        this.blocksRepair = blocksRepair;
    }

    public Boolean getUnrepairable() {
        return unrepairable;
    }

    public void setUnrepairable(Boolean unrepairable) {
        this.unrepairable = unrepairable;
    }

    public String getMotivation() {
        return motivation;
    }

    public void setMotivation(String motivation) {
        this.motivation = motivation;
    }

    public String getRepairer() {
        return repairer;
    }

    public void setRepairer(String repairer) {
        this.repairer = repairer;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Repair{" +
                "statusRepair=" + statusRepair +
                ", vehicleValue=" + vehicleValue +
                ", blocksRepair=" + blocksRepair +
                ", unrepairable=" + unrepairable +
                ", motivation='" + motivation + '\'' +
                ", repairer='" + repairer + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", address=" + address +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Repair repair = (Repair) o;

        return new EqualsBuilder().append(statusRepair, repair.statusRepair).append(vehicleValue, repair.vehicleValue)
                .append(blocksRepair, repair.blocksRepair).append(unrepairable, repair.unrepairable).append(motivation, repair.motivation)
                .append(repairer, repair.repairer).append(phone, repair.phone).append(email, repair.email).append(address, repair.address).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(statusRepair).append(vehicleValue)
                .append(blocksRepair).append(unrepairable).append(motivation).append(repairer).append(phone)
                .append(email).append(address).toHashCode();
    }
}
