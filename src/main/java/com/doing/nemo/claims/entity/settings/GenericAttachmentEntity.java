package com.doing.nemo.claims.entity.settings;

import com.doing.nemo.claims.entity.enumerated.ClaimsRepairEnum;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "generic_attachment")
public class GenericAttachmentEntity implements Serializable {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Type(type = "org.hibernate.type.PostgresUUIDType")
    private UUID id;

    @Column(name = "original_name")
    private String originalName;

    @Column(name = "description")
    private String description;

    @Column(name = "attachment_id")
    private String attachmentId;

    @Column(name = "created_at")
    private Instant createdAt;

    @Column(name = "is_active")
    private Boolean isActive;

    @Enumerated(EnumType.STRING)
    @Column(name = "type_complaint")
    private ClaimsRepairEnum typeComplaint;

    public GenericAttachmentEntity() {
    }

    public GenericAttachmentEntity(String originalName, String description, String attachmentId, Instant createdAt, Boolean isActive, ClaimsRepairEnum typeComplaint) {
        this.originalName = originalName;
        this.description = description;
        this.attachmentId = attachmentId;
        this.createdAt = createdAt;
        this.isActive = isActive;
        this.typeComplaint = typeComplaint;
    }

    public ClaimsRepairEnum getTypeComplaint() {
        return typeComplaint;
    }

    public void setTypeComplaint(ClaimsRepairEnum typeComplaint) {
        this.typeComplaint = typeComplaint;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getOriginalName() {
        return originalName;
    }

    public void setOriginalName(String originalName) {
        this.originalName = originalName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAttachmentId() {
        return attachmentId;
    }

    public void setAttachmentId(String attachmentId) {
        this.attachmentId = attachmentId;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    @Override
    public String toString() {
        return "GenericAttachmentEntity{" +
                "id=" + id +
                ", originalName='" + originalName + '\'' +
                ", description='" + description + '\'' +
                ", attachmentId='" + attachmentId + '\'' +
                ", createdAt=" + createdAt +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GenericAttachmentEntity that = (GenericAttachmentEntity) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getOriginalName(), that.getOriginalName()) &&
                Objects.equals(getDescription(), that.getDescription()) &&
                Objects.equals(getAttachmentId(), that.getAttachmentId()) &&
                Objects.equals(getCreatedAt(), that.getCreatedAt());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getOriginalName(), getDescription(), getAttachmentId(), getCreatedAt());
    }
}
