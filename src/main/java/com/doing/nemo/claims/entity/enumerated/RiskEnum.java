package com.doing.nemo.claims.entity.enumerated;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;

public enum RiskEnum implements Serializable {
    KASKO_URTO_CONTRO_OGGETTI_FISSI("kasko_urto_contro_oggetti_fissi"),
    ATTO_VANDALICO("atto_vandalico"),
    DANNO_RITROVATO_IN_PARCHEGGIO("danno_ritrovato_in_parcheggio"),
    EVENTI_NATURALI("eventi_naturali"),
    CRISTALLI("cristalli"),
    CHIAVI_SMARRITE_O_RUBATE("chiavi_smarrite_o_rubate"),
    TENTATO_FURTO("tentato_furto"),
    FURTO_PARZIALE("furto_parziale"),
    FURTO_RITROVAMENTO("furto_ritrovamento"),
    FURTO_TOTALE("furto_totale"),
    INCENDIO("incendio"),
    PAI("pai"),
    RESPONSABILITA_CIVILE_AUTO_RCA("responsabilita_civile_auto_rca");

    private static Logger LOGGER = LoggerFactory.getLogger(RiskEnum.class);
    private String risk;

    private RiskEnum(String risk) {
        this.risk = risk;
    }

    @JsonCreator
    public static RiskEnum create(String risk) {

        risk = risk.replace(" - ", "_");
        risk = risk.replace('-', '_');
        risk = risk.replace(' ', '_');

        if (risk != null) {
            for (RiskEnum val : RiskEnum.values()) {
                if (risk.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        LOGGER.debug("Bad parameters exception. Enum class " + RiskEnum.class.getSimpleName() + " doesn't accept this value: " + risk);
        throw new BadParametersException("Bad parameters exception. Enum class " + RiskEnum.class.getSimpleName() + " doesn't accept this value: " + risk);
    }

    public String getValue() {
        return this.risk.toUpperCase();
    }

    @JsonValue
    public String toValue() {
        for (RiskEnum val : RiskEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }
}
