package com.doing.nemo.claims.entity.jsonb;

import org.apache.commons.lang3.StringUtils;

public enum JsonbProcessingFunctions {

    JSONB_EXTRACT_PATH_TEXT("jsonb_extract_path_text"),
    JSONB_ARRAY_ELEMENTS("jsonb_array_elements"),
    JSONB_ARRAY_ELEMENTS_TEXT("jsonb_array_elements_text"),
    JSONB_ARRAY_LENGTH("jsonb_array_length"),
    JSONB_EACH("jsonb_each"),
    JSONB_EACH_TEXT("jsonb_each_text"),
    JSONB_EXTRACT_PATH("jsonb_extract_path"),
    JSONB_INSERT("jsonb_insert"),
    JSONB_OBJECT("jsonb_object"),
    JSONB_OBJECT_KEYS("jsonb_object_keys"),
    JSONB_POPULATE_RECORD("jsonb_populate_record"),
    JSONB_POPULATE_RECORDSET("jsonb_populate_recordset"),
    JSONB_PRETTY("jsonb_pretty"),
    JSONB_SET("jsonb_set"),
    JSONB_STRIP_NULLS("jsonb_strip_nulls"),
    JSONB_TO_RECORD("jsonb_to_record"),
    JSONB_TO_RECORDSET("jsonb_to_recordset"),
    JSONB_TYPEOF("jsonb_typeof"),
    TO_JSONB("to_jsonb");

    private String value;

    JsonbProcessingFunctions(String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }

    public static JsonbProcessingFunctions fromValue(String v) {
        if (StringUtils.isNotBlank(v)) {
            for (JsonbProcessingFunctions c : JsonbProcessingFunctions.values()) {
                if (StringUtils.equalsIgnoreCase(c.value, v)) {
                    return c;
                }
            }
        }
        return null;
    }
}