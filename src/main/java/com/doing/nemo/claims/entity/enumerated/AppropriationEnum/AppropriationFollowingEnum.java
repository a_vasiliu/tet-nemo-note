package com.doing.nemo.claims.entity.enumerated.AppropriationEnum;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public enum AppropriationFollowingEnum implements Serializable {
    UNDEFINED("undefined"),
    ACCIDENT("accident"),
    FINDING("finding"),
    INVESTIGATION("investigation");

    private static Logger LOGGER = LoggerFactory.getLogger(AppropriationFollowingEnum.class);

    private String statusEntity;

    private AppropriationFollowingEnum(String statusEntity) {
        this.statusEntity = statusEntity;
    }

    @JsonCreator
    public static AppropriationFollowingEnum create(String statusEntity) {

        statusEntity = statusEntity.replace(" - ", "_");
        statusEntity = statusEntity.replace('-', '_');
        statusEntity = statusEntity.replace(' ', '_');

        if (statusEntity != null) {
            for (AppropriationFollowingEnum val : AppropriationFollowingEnum.values()) {
                if (statusEntity.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }

        LOGGER.debug("Bad parameters exception. Enum class " + AppropriationFollowingEnum.class.getSimpleName() + " doesn't accept this value: " + statusEntity);
        throw new BadParametersException("Bad parameters exception. Enum class " + AppropriationFollowingEnum.class.getSimpleName() + " doesn't accept this value: " + statusEntity);
    }

    public static List<AppropriationFollowingEnum> getStatusListFromString(List<String> statusListString) {

        List<AppropriationFollowingEnum> statusList = new LinkedList<>();
        for (String att : statusListString) {
            statusList.add(AppropriationFollowingEnum.create(att));
        }
        return statusList;
    }

    public String getValue() {
        return this.statusEntity.toUpperCase();
    }

    @JsonValue
    public String toValue() {
        for (AppropriationFollowingEnum val : AppropriationFollowingEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }
}