package com.doing.nemo.claims.entity.jsonb.complaint.jsonbComplaint.jsonbDataAccidents;

import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.jsonb.Address;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.beans.factory.annotation.Value;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DataAccident implements Serializable {

    private static final long serialVersionUID = 7955028028064770246L;

    @JsonProperty("type_accident")
    private DataAccidentTypeAccidentEnum typeAccident;

    @Value("false")
    @JsonProperty("responsible")
    private Boolean responsible;

    @JsonProperty("date_accident")
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    private Date dateAccident;

    @JsonProperty("happened_abroad")
    private Boolean happenedAbroad;

    @JsonProperty("address")
    private Address address;

    @JsonProperty("damage_to_objects")
    private Boolean damageToObjects;

    @JsonProperty("damage_to_vehicles")
    private Boolean damageToVehicles;

    //Da eliminare probabilmente
    @JsonProperty("old_motorcycle_plates")
    private String oldMotorcyclePlates;

    @JsonProperty("intervention_authority")
    private Boolean interventionAuthority;

    @JsonProperty("police")
    private Boolean police;

    @JsonProperty("cc")
    private Boolean cc;

    @JsonProperty("vvuu")
    private Boolean vvuu;

    @JsonProperty("authority_data")
    private String authorityData;

    @JsonProperty("witness_description")
    private String witnessDescription;

    @JsonProperty("recoverability")
    private Boolean recoverability;

    @JsonProperty("recoverability_percent")
    private Double recoverabilityPercent;

    @JsonProperty("incomplete_motivation")
    private String incompleteMotivation;

    @JsonProperty("is_robbery")
    private Boolean isRobbery;

    @JsonProperty("center_notified")
    private Boolean centerNotified;

    @JsonProperty("happened_on_center")
    private Boolean happenedOnCenter;

    @JsonProperty("provider_code")
    private String providerCode;

    @JsonProperty("theft_description")
    private String theftDescription;

    @JsonProperty("item_to_receive")
    private List<ReceivedItem> itemToReceive;

    @JsonProperty("is_with_counterparty")
    private Boolean isWithCounterparty;


    public DataAccidentTypeAccidentEnum getTypeAccident() {
        return typeAccident;
    }

    public void setTypeAccident(DataAccidentTypeAccidentEnum typeAccident) {
        this.typeAccident = typeAccident;
    }

    public Boolean getResponsible() {
        return responsible;
    }

    public void setResponsible(Boolean responsible) {
        this.responsible = responsible;
    }

    public Date getDateAccident() {
        if(dateAccident == null){
            return null;
        }
        return (Date)dateAccident.clone();
    }

    public void setDateAccident(Date dateAccident) {
        if(dateAccident != null)
        {
            this.dateAccident = (Date)dateAccident.clone();
        } else {
            this.dateAccident = null;
        }
    }

    public Boolean getHappenedAbroad() {
        return happenedAbroad;
    }

    public void setHappenedAbroad(Boolean happenedAbroad) {
        this.happenedAbroad = happenedAbroad;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Boolean getDamageToObjects() {
        return damageToObjects;
    }

    public void setDamageToObjects(Boolean damageToObjects) {
        this.damageToObjects = damageToObjects;
    }

    public Boolean getDamageToVehicles() {
        return damageToVehicles;
    }

    public void setDamageToVehicles(Boolean damageToVehicles) {
        this.damageToVehicles = damageToVehicles;
    }

    public String getOldMotorcyclePlates() {
        return oldMotorcyclePlates;
    }

    public void setOldMotorcyclePlates(String oldMotorcyclePlates) {
        this.oldMotorcyclePlates = oldMotorcyclePlates;
    }

    public Boolean getInterventionAuthority() {
        return interventionAuthority;
    }

    public void setInterventionAuthority(Boolean interventionAuthority) {
        this.interventionAuthority = interventionAuthority;
    }

    public Boolean getPolice() {
        return police;
    }

    public void setPolice(Boolean police) {
        this.police = police;
    }

    public Boolean getCc() {
        return cc;
    }

    public void setCc(Boolean cc) {
        this.cc = cc;
    }

    public Boolean getVvuu() {
        return vvuu;
    }

    public void setVvuu(Boolean vvuu) {
        this.vvuu = vvuu;
    }

    public String getAuthorityData() {
        return authorityData;
    }

    public void setAuthorityData(String authorityData) {
        this.authorityData = authorityData;
    }

    public String getWitnessDescription() {
        return witnessDescription;
    }

    public void setWitnessDescription(String witnessDescription) {
        this.witnessDescription = witnessDescription;
    }

    public Boolean getRecoverability() {
        return recoverability;
    }

    public void setRecoverability(Boolean recoverability) {
        this.recoverability = recoverability;
    }

    public Double getRecoverabilityPercent() {
        return recoverabilityPercent;
    }

    public void setRecoverabilityPercent(Double recoverabilityPercent) {
        this.recoverabilityPercent = recoverabilityPercent;
    }

    public String getIncompleteMotivation() {
        return incompleteMotivation;
    }

    public void setIncompleteMotivation(String incompleteMotivation) {
        this.incompleteMotivation = incompleteMotivation;
    }

    @JsonIgnore
    public Boolean getRobbery() {
        return isRobbery;
    }

    public void setRobbery(Boolean robbery) {
        isRobbery = robbery;
    }

    public Boolean getCenterNotified() {
        return centerNotified;
    }

    public void setCenterNotified(Boolean centerNotified) {
        this.centerNotified = centerNotified;
    }

    public Boolean getHappenedOnCenter() {
        return happenedOnCenter;
    }

    public void setHappenedOnCenter(Boolean happenedOnCenter) {
        this.happenedOnCenter = happenedOnCenter;
    }

    public String getProviderCode() {
        return providerCode;
    }

    public void setProviderCode(String providerCode) {
        this.providerCode = providerCode;
    }

    public String getTheftDescription() {
        return theftDescription;
    }

    public void setTheftDescription(String theftDescription) {
        this.theftDescription = theftDescription;
    }

    public List<ReceivedItem> getItemToReceive() {
        if(itemToReceive == null){
            return null;
        }
        return new ArrayList<>(itemToReceive);
    }

    public void setItemToReceive(List<ReceivedItem> itemToReceive) {
        if(itemToReceive != null)
        {
            this.itemToReceive = new ArrayList<>(itemToReceive);
        } else {
            this.itemToReceive = null;
        }
    }

    @JsonIgnore
    public Boolean getWithCounterparty() {
        return isWithCounterparty;
    }

    public void setWithCounterparty(Boolean withCounterparty) {
        isWithCounterparty = withCounterparty;
    }

    @Override
    public String toString() {
        return "DataAccident{" +
                "typeAccident=" + typeAccident +
                ", responsible=" + responsible +
                ", dateAccident=" + dateAccident +
                ", happenedAbroad=" + happenedAbroad +
                ", address=" + address +
                ", damageToObjects=" + damageToObjects +
                ", damageToVehicles=" + damageToVehicles +
                ", oldMotorcyclePlates='" + oldMotorcyclePlates + '\'' +
                ", interventionAuthority=" + interventionAuthority +
                ", police=" + police +
                ", cc=" + cc +
                ", vvuu=" + vvuu +
                ", authorityData='" + authorityData + '\'' +
                ", witnessDescription='" + witnessDescription + '\'' +
                ", recoverability=" + recoverability +
                ", recoverabilityPercent=" + recoverabilityPercent +
                ", incompleteMotivation='" + incompleteMotivation + '\'' +
                ", isRobbery=" + isRobbery +
                ", centerNotified=" + centerNotified +
                ", happenedOnCenter=" + happenedOnCenter +
                ", providerCode='" + providerCode + '\'' +
                ", theftDescription='" + theftDescription + '\'' +
                ", itemToReceive=" + itemToReceive +
                ", isWithCounterparty=" + isWithCounterparty +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        DataAccident that = (DataAccident) o;

        return new EqualsBuilder().append(typeAccident, that.typeAccident)
                .append(responsible, that.responsible).append(dateAccident, that.dateAccident)
                .append(happenedAbroad, that.happenedAbroad).append(address, that.address)
                .append(damageToObjects, that.damageToObjects).append(damageToVehicles, that.damageToVehicles)
                .append(oldMotorcyclePlates, that.oldMotorcyclePlates).append(interventionAuthority, that.interventionAuthority)
                .append(police, that.police).append(cc, that.cc).append(vvuu, that.vvuu).append(authorityData, that.authorityData)
                .append(witnessDescription, that.witnessDescription).append(recoverability, that.recoverability)
                .append(recoverabilityPercent, that.recoverabilityPercent).append(incompleteMotivation, that.incompleteMotivation)
                .append(isRobbery, that.isRobbery).append(centerNotified, that.centerNotified).append(happenedOnCenter, that.happenedOnCenter)
                .append(providerCode, that.providerCode).append(theftDescription, that.theftDescription).append(itemToReceive, that.itemToReceive)
                .append(isWithCounterparty, that.isWithCounterparty).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(typeAccident)
                .append(responsible).append(dateAccident).append(happenedAbroad).append(address)
                .append(damageToObjects).append(damageToVehicles).append(oldMotorcyclePlates)
                .append(interventionAuthority).append(police).append(cc).append(vvuu).append(authorityData)
                .append(witnessDescription).append(recoverability).append(recoverabilityPercent).append(incompleteMotivation)
                .append(isRobbery).append(centerNotified).append(happenedOnCenter).append(providerCode).append(theftDescription)
                .append(itemToReceive).append(isWithCounterparty).toHashCode();
    }
}
