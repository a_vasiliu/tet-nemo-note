package com.doing.nemo.claims.entity.esb.CustomerESB;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LeasingCompanieEsb implements Serializable {

    private static final long serialVersionUID = 1413177455128039035L;

    @JsonProperty("leasing_company_id")
    private String leasingCompanyId;

    @JsonProperty("leasing_company")
    private String leasingCompany;

    public LeasingCompanieEsb() {
    }

    public LeasingCompanieEsb(String leasingCompanyId, String leasingCompany) {
        this.leasingCompanyId = leasingCompanyId;
        this.leasingCompany = leasingCompany;
    }

    public String getLeasingCompanyId() {
        return leasingCompanyId;
    }

    public void setLeasingCompanyId(String leasingCompanyId) {
        this.leasingCompanyId = leasingCompanyId;
    }

    public String getLeasingCompany() {
        return leasingCompany;
    }

    public void setLeasingCompany(String leasingCompany) {
        this.leasingCompany = leasingCompany;
    }

    @Override
    public String toString() {
        return "LeasingCompanieEsb{" +
                "leasingCompanyId='" + leasingCompanyId + '\'' +
                ", leasingCompany='" + leasingCompany + '\'' +
                '}';
    }
}
