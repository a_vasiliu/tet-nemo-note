package com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbAntiTheftService;

import com.doing.nemo.claims.entity.enumerated.AntitheftEnum.AntitheftTypeEnum;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Registry implements Serializable {

    @JsonProperty("type")
    private AntitheftTypeEnum typeEntity;

    @JsonProperty("id_transaction")
    private Long idTransaction;

    @JsonProperty("cod_imei")
    private String codImei;

    @JsonProperty("cod_pack")
    private String codPack;

    @JsonProperty("cod_provider")
    private Integer codProvider;

    @JsonProperty("provider")
    private String provider;

    @JsonProperty("date_activation")
    private Date dateActivation;

    @JsonProperty("date_end")
    private Date dateEnd;

    @JsonProperty("cod_serialnumber")
    private String codSerialnumber;

    @JsonProperty("voucher_id")
    private Integer voucherId;

    public AntitheftTypeEnum getTypeEntity() {
        return typeEntity;
    }

    public void setTypeEntity(AntitheftTypeEnum typeEntity) {
        this.typeEntity = typeEntity;
    }

    public Long getIdTransaction() {
        return idTransaction;
    }

    public void setIdTransaction(Long idTransaction) {
        this.idTransaction = idTransaction;
    }

    public String getCodImei() {
        return codImei;
    }

    public void setCodImei(String codImei) {
        this.codImei = codImei;
    }

    public String getCodPack() {
        return codPack;
    }

    public void setCodPack(String codPack) {
        this.codPack = codPack;
    }

    public Integer getCodProvider() {
        return codProvider;
    }

    public void setCodProvider(Integer codProvider) {
        this.codProvider = codProvider;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public Date getDateActivation() {
        if(dateActivation == null){
            return null;
        }
        return (Date)dateActivation.clone();
    }

    public void setDateActivation(Date dateActivation) {
        if(dateActivation != null)
        {
            this.dateActivation = (Date)dateActivation.clone();
        } else {
            this.dateActivation = null;
        }
    }

    public Date getDateEnd() {
        if(dateEnd == null){
            return null;
        }
        return (Date)dateEnd.clone();
    }

    public void setDateEnd(Date dateEnd) {
        if(dateEnd != null)
        {
            this.dateEnd =(Date)dateEnd.clone();
        } else {
            this.dateEnd = null;
        }
    }

    public String getCodSerialnumber() {
        return codSerialnumber;
    }

    public void setCodSerialnumber(String codSerialnumber) {
        this.codSerialnumber = codSerialnumber;
    }

    public Integer getVoucherId() {
        return voucherId;
    }

    public void setVoucherId(Integer voucherId) {
        this.voucherId = voucherId;
    }

    @Override
    public String toString() {
        return "Registry{" +
                "typeEntity=" + typeEntity +
                ", idTransaction=" + idTransaction +
                ", codImei='" + codImei + '\'' +
                ", codPack='" + codPack + '\'' +
                ", codProvider=" + codProvider +
                ", provider='" + provider + '\'' +
                ", dateActivation=" + dateActivation +
                ", dateEnd=" + dateEnd +
                ", codSerialnumber='" + codSerialnumber + '\'' +
                ", voucherId=" + voucherId +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Registry registry = (Registry) o;

        return new EqualsBuilder().append(typeEntity, registry.typeEntity)
                .append(idTransaction, registry.idTransaction).append(codImei, registry.codImei)
                .append(codPack, registry.codPack).append(codProvider, registry.codProvider).append(provider, registry.provider)
                .append(dateActivation, registry.dateActivation).append(dateEnd, registry.dateEnd)
                .append(codSerialnumber, registry.codSerialnumber).append(voucherId, registry.voucherId).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(typeEntity).append(idTransaction)
                .append(codImei).append(codPack).append(codProvider).append(provider).append(dateActivation)
                .append(dateEnd).append(codSerialnumber).append(voucherId).toHashCode();
    }
}
