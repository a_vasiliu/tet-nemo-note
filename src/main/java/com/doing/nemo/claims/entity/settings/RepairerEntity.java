package com.doing.nemo.claims.entity.settings;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "repairer")
public class RepairerEntity implements Serializable {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Type(type = "org.hibernate.type.PostgresUUIDType")
    private UUID id;

    @Column(name = "supplier_code")
    private String supplierCode;

    @Column(name = "business_name")
    private String businessName;

    @Column(name = "address")
    private String address;

    @Column(name = "zip_code")
    private String zipCode;

    @Column(name = "locality")
    private String locality;

    @Column(name = "prov")
    private String prov;

    @Column(name = "reference_person")
    private String referencePerson;

    @Column(name = "phone")
    private String phone;

    @Column(name = "fax")
    private String fax;

    @Column(name = "email")
    private String email;

    @Column(name = "web_site")
    private String webSite;

    @Column(name = "vat_number")
    private String vatNumber;

    @Column(name = "fiscal_code")
    private String fiscalCode;

    @Column(name = "legal_representative")
    private String legalRepresentative;

    @Column(name = "is_active")
    private Boolean isActive;

    public RepairerEntity() {
    }

    public RepairerEntity(String supplierCode, String businessName, String address, String zipCode, String locality, String prov, String referencePerson, String phone, String fax, String email, String webSite, String vatNumber, String fiscalCode, String legalRepresentative, Boolean isActive) {
        this.supplierCode = supplierCode;
        this.businessName = businessName;
        this.address = address;
        this.zipCode = zipCode;
        this.locality = locality;
        this.prov = prov;
        this.referencePerson = referencePerson;
        this.phone = phone;
        this.fax = fax;
        this.email = email;
        this.webSite = webSite;
        this.vatNumber = vatNumber;
        this.fiscalCode = fiscalCode;
        this.legalRepresentative = legalRepresentative;
        this.isActive = isActive;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getSupplierCode() {
        return supplierCode;
    }

    public void setSupplierCode(String supplierCode) {
        this.supplierCode = supplierCode;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getProv() {
        return prov;
    }

    public void setProv(String prov) {
        this.prov = prov;
    }

    public String getReferencePerson() {
        return referencePerson;
    }

    public void setReferencePerson(String referencePerson) {
        this.referencePerson = referencePerson;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebSite() {
        return webSite;
    }

    public void setWebSite(String webSite) {
        this.webSite = webSite;
    }

    public String getVatNumber() {
        return vatNumber;
    }

    public void setVatNumber(String vatNumber) {
        this.vatNumber = vatNumber;
    }

    public String getFiscalCode() {
        return fiscalCode;
    }

    public void setFiscalCode(String fiscalCode) {
        this.fiscalCode = fiscalCode;
    }

    public String getLegalRepresentative() {
        return legalRepresentative;
    }

    public void setLegalRepresentative(String legalRepresentative) {
        this.legalRepresentative = legalRepresentative;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    @Override
    public String toString() {
        return "RepairerEntity{" +
                "id=" + id +
                ", supplierCode='" + supplierCode + '\'' +
                ", businessName='" + businessName + '\'' +
                ", address='" + address + '\'' +
                ", zipCode=" + zipCode +
                ", locality='" + locality + '\'' +
                ", prov='" + prov + '\'' +
                ", referencePerson='" + referencePerson + '\'' +
                ", phone='" + phone + '\'' +
                ", fax='" + fax + '\'' +
                ", email='" + email + '\'' +
                ", webSite='" + webSite + '\'' +
                ", vatNumber='" + vatNumber + '\'' +
                ", fiscalCode='" + fiscalCode + '\'' +
                ", legalRepresentative='" + legalRepresentative + '\'' +
                ", isActive=" + isActive +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RepairerEntity that = (RepairerEntity) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getSupplierCode(), that.getSupplierCode()) &&
                Objects.equals(getBusinessName(), that.getBusinessName()) &&
                Objects.equals(getAddress(), that.getAddress()) &&
                Objects.equals(getZipCode(), that.getZipCode()) &&
                Objects.equals(getLocality(), that.getLocality()) &&
                Objects.equals(getProv(), that.getProv()) &&
                Objects.equals(getReferencePerson(), that.getReferencePerson()) &&
                Objects.equals(getPhone(), that.getPhone()) &&
                Objects.equals(getFax(), that.getFax()) &&
                Objects.equals(getEmail(), that.getEmail()) &&
                Objects.equals(getWebSite(), that.getWebSite()) &&
                Objects.equals(getVatNumber(), that.getVatNumber()) &&
                Objects.equals(getFiscalCode(), that.getFiscalCode()) &&
                Objects.equals(getLegalRepresentative(), that.getLegalRepresentative()) &&
                Objects.equals(getActive(), that.getActive());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getSupplierCode(), getBusinessName(), getAddress(), getZipCode(), getLocality(), getProv(), getReferencePerson(), getPhone(), getFax(), getEmail(), getWebSite(), getVatNumber(), getFiscalCode(), getLegalRepresentative(), getActive());
    }
}
