package com.doing.nemo.claims.entity;

import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Stats implements Serializable {

    @JsonProperty("stats")
    private List<StatsElement> stats;

    public Stats() {
        this.stats = new LinkedList<>();
    }

    public List<StatsElement> getStats() {
        if(stats == null){
            return null;
        }
        return new ArrayList<>(stats);
    }

    public void setStats(List<StatsElement> statsElem) {
        if(statsElem != null)
        {
            this.stats = new ArrayList<>(statsElem);
        } else {
            this.stats = null;
        }
    }

    public StatsElement getStats(ClaimsStatusEnum claimsStatus) {

        for (StatsElement sta : this.getStats()) {
            if (sta.getClaimsStatus().equals(claimsStatus)) {
                return sta;
            }
        }
        return null;
    }

    public static class StatsElement implements Serializable {

        @JsonProperty("claims_status")
        private ClaimsStatusEnum claimsStatus;

        @JsonProperty("ful")
        private InternalCounter ful;

        @JsonProperty("fni")
        private InternalCounter fni;

        @JsonProperty("fcm")
        private InternalCounter fcm;

        public StatsElement() {
        }

        public ClaimsStatusEnum getClaimsStatus() {
            return claimsStatus;
        }

        public void setClaimsStatus(ClaimsStatusEnum claimsStatus) {
            this.claimsStatus = claimsStatus;
        }

        public InternalCounter getFul() {
            return ful;
        }

        public void setFul(InternalCounter ful) {
            this.ful = ful;
        }

        public InternalCounter getFni() {
            return fni;
        }

        public void setFni(InternalCounter fni) {
            this.fni = fni;
        }

        public InternalCounter getFcm() {
            return fcm;
        }

        public void setFcm(InternalCounter fcm) {
            this.fcm = fcm;
        }

        @Override
        public String toString() {
            return "StatsElement{" +
                    "claimsStatus=" + claimsStatus +
                    ", ful=" + ful +
                    ", fni=" + fni +
                    ", fcm=" + fcm +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "Stats{" +
                "stats=" + stats +
                '}';
    }

    public static class InternalCounter implements Serializable{

        @JsonProperty("tot_claims")
        private Integer total;

        @JsonProperty("in_evidence")
        private Integer inEvidence;

        @JsonProperty("with_continuation")
        private Integer withContinuation;

        @JsonProperty("with_continuation_in_evidence")
        private Integer withContinuationInEvidence;

        public InternalCounter() {
            this.total = 0;
            this.inEvidence = 0;
            this.withContinuation = 0;
            this.withContinuationInEvidence = 0;
        }

        public InternalCounter(Integer total, Integer inEvidence, Integer withContinuation, Integer withContinuationInEvidence) {
            this.total = total;
            this.inEvidence = inEvidence;
            this.withContinuation = withContinuation;
            this.withContinuationInEvidence = withContinuationInEvidence;
        }

        public Integer getTotal() {
            return total;
        }

        public void setTotal(Integer total) {
            this.total = total;
        }

        public Integer getInEvidence() {
            return inEvidence;
        }

        public void setInEvidence(Integer inEvidence) {
            this.inEvidence = inEvidence;
        }

        public Integer getWithContinuation() {
            return withContinuation;
        }

        public void setWithContinuation(Integer withContinuation) {
            this.withContinuation = withContinuation;
        }

        public Integer getWithContinuationInEvidence() {
            return withContinuationInEvidence;
        }

        public void setWithContinuationInEvidence(Integer withContinuationInEvidence) {
            this.withContinuationInEvidence = withContinuationInEvidence;
        }

        @Override
        public String toString() {
            return "InternalCounter{" +
                    "total=" + total +
                    ", inEvidence=" + inEvidence +
                    ", withContinuation=" + withContinuation +
                    ", withContinuationInEvidence=" + withContinuationInEvidence +
                    '}';
        }
    }
}
