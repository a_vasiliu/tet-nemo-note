package com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbDriver;

import com.doing.nemo.claims.entity.jsonb.Address;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Driver implements Serializable {

    private static final long serialVersionUID = 9100921069404924622L;

    @JsonProperty("id")
    private Integer id;

    @JsonProperty("identification")
    private String identification;

    @JsonProperty("official_registration")
    private String officialRegistration;

    @JsonProperty("trading_name")
    private String tradingName;

    @JsonProperty("firstname")
    private String firstname;

    @JsonProperty("lastname")
    private String lastname;

    @JsonProperty("fiscal_code")
    private String fiscalCode;

    @JsonProperty("main_address")
    private Address mainAddress;

    @JsonProperty("date_of_birth")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date dateOfBirth;

    @JsonProperty("phone")
    private String phone;

    @JsonProperty("email")
    private String email;

    @JsonProperty("pec")
    private String pec;

    @JsonProperty("sex")
    private String sex;

    @JsonProperty("customer_id")
    private String customerId;

    @JsonProperty("driver_injury")
    private Boolean driverInjury;

    @JsonProperty("driving_license")
    private DrivingLicense drivingLicense;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public String getOfficialRegistration() {
        return officialRegistration;
    }

    public void setOfficialRegistration(String officialRegistration) {
        this.officialRegistration = officialRegistration;
    }

    public String getTradingName() {
        return tradingName;
    }

    public void setTradingName(String tradingName) {
        this.tradingName = tradingName;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFiscalCode() {
        return fiscalCode;
    }

    public void setFiscalCode(String fiscalCode) {
        this.fiscalCode = fiscalCode;
    }

    public Address getMainAddress() {
        return mainAddress;
    }

    public void setMainAddress(Address mainAddress) {
        this.mainAddress = mainAddress;
    }

    public Date getDateOfBirth() {
        if(dateOfBirth == null){
            return null;
        }
        return (Date)dateOfBirth.clone();
    }

    public void setDateOfBirth(Date dateOfBirth) {
        if(dateOfBirth != null)
        {
            this.dateOfBirth =(Date)dateOfBirth.clone();
        } else {
            this.dateOfBirth = null;
        }
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPec() {
        return pec;
    }

    public void setPec(String pec) {
        this.pec = pec;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public DrivingLicense getDrivingLicense() {
        return drivingLicense;
    }

    public void setDrivingLicense(DrivingLicense drivingLicense) {
        this.drivingLicense = drivingLicense;
    }

    public Boolean getDriverInjury() {
        return driverInjury;
    }

    public void setDriverInjury(Boolean driverInjury) {
        this.driverInjury = driverInjury;
    }

    @Override
    public String toString() {
        return "Driver{" +
                "id=" + id +
                ", identification='" + identification + '\'' +
                ", officialRegistration='" + officialRegistration + '\'' +
                ", tradingName='" + tradingName + '\'' +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", fiscalCode='" + fiscalCode + '\'' +
                ", mainAddress=" + mainAddress +
                ", dateOfBirth=" + dateOfBirth +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", pec='" + pec + '\'' +
                ", sex='" + sex + '\'' +
                ", customerId='" + customerId + '\'' +
                ", driverInjury=" + driverInjury +
                ", drivingLicense=" + drivingLicense +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Driver driver = (Driver) o;
        return Objects.equals(getId(), driver.getId()) &&
                Objects.equals(getIdentification(), driver.getIdentification()) &&
                Objects.equals(getOfficialRegistration(), driver.getOfficialRegistration()) &&
                Objects.equals(getTradingName(), driver.getTradingName()) &&
                Objects.equals(getFirstname(), driver.getFirstname()) &&
                Objects.equals(getLastname(), driver.getLastname()) &&
                Objects.equals(getFiscalCode(), driver.getFiscalCode()) &&
                Objects.equals(getMainAddress(), driver.getMainAddress()) &&
                Objects.equals(getDateOfBirth(), driver.getDateOfBirth()) &&
                Objects.equals(getPhone(), driver.getPhone()) &&
                Objects.equals(getEmail(), driver.getEmail()) &&
                Objects.equals(getPec(), driver.getPec()) &&
                Objects.equals(getSex(), driver.getSex()) &&
                Objects.equals(getCustomerId(), driver.getCustomerId()) &&
                Objects.equals(getDriverInjury(), driver.getDriverInjury()) &&
                Objects.equals(getDrivingLicense(), driver.getDrivingLicense());
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, identification, officialRegistration, tradingName, firstname, lastname, fiscalCode, mainAddress, dateOfBirth, phone, email, pec, sex, customerId, driverInjury, drivingLicense);
    }
}
