package com.doing.nemo.claims.entity.enumerated.RefundEnum;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;

public enum RefundTypeEnum implements Serializable {
    ALLOWANCE("allowance"),//ASSEGNO
    BANK_TRANSFER("bank_transfer");//BONIFICO

    private static Logger LOGGER = LoggerFactory.getLogger(RefundTypeEnum.class);
    private String refundTypeEnum;

    private RefundTypeEnum(String refundTypeEnum) {
        this.refundTypeEnum = refundTypeEnum;
    }

    @JsonCreator
    public static RefundTypeEnum create(String refundTypeEnum) {

        if (refundTypeEnum != null) {

            refundTypeEnum = refundTypeEnum.replace(" - ", "_");
            refundTypeEnum = refundTypeEnum.replace('-', '_');
            refundTypeEnum = refundTypeEnum.replace(' ', '_');

            for (RefundTypeEnum val : RefundTypeEnum.values()) {
                if (refundTypeEnum.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        LOGGER.debug("Bad parameters exception. Enum class " + RefundTypeEnum.class.getSimpleName() + " doesn't accept this value: " + refundTypeEnum);
        throw new BadParametersException("Bad parameters exception. Enum class " + RefundTypeEnum.class.getSimpleName() + " doesn't accept this value: " + refundTypeEnum);
    }

    public String getValue() {
        return this.refundTypeEnum.toUpperCase();
    }

    @JsonValue
    public String toValue() {
        for (RefundTypeEnum val : RefundTypeEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }
}
