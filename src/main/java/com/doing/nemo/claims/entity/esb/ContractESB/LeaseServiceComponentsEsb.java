package com.doing.nemo.claims.entity.esb.ContractESB;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LeaseServiceComponentsEsb implements Serializable {

    private static final long serialVersionUID = 710071922968169452L;

    @JsonProperty("id")
    private Long id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("product_id")
    private Long productId;

    @JsonProperty("qualifiers")
    private List<QualifiersEsb> qualifiersList;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public List<QualifiersEsb> getQualifiersList() {
        if(qualifiersList == null){
            return null;
        }
        return new ArrayList<>(qualifiersList);
    }

    public void setQualifiersList(List<QualifiersEsb> qualifiersList) {
        if(qualifiersList != null)
        {
            this.qualifiersList =new ArrayList<>(qualifiersList);
        } else {
            this.qualifiersList = null;
        }
    }

    @Override
    public String toString() {
        return "LeaseServiceComponentsEsb{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", product_id='" + productId + '\'' +
                ", qualifiersList=" + qualifiersList +
                '}';
    }
}