package com.doing.nemo.claims.entity.jsonb;

import com.doing.nemo.claims.entity.enumerated.AuthorityEnum.AuthorityEventTypeEnum;
import com.doing.nemo.claims.entity.enumerated.AuthorityEnum.AuthorityPracticeTypeEnum;
import com.doing.nemo.claims.entity.enumerated.AuthorityEnum.AuthorityStatusEnum;
import com.doing.nemo.claims.entity.enumerated.AuthorityEnum.AuthorityTypeEnum;
import com.doing.nemo.claims.entity.enumerated.WorkingStatusEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;


@JsonIgnoreProperties(ignoreUnknown = true)
public class AuthorityPractice implements Serializable {

    private static final long serialVersionUID = -61589083904741009L;

    @JsonProperty("dossier_id")
    private String dossierId;

    @JsonProperty("working_id")
    private String workingId;

    @JsonProperty("working_number")
    private String workingNumber;

    @JsonProperty("dossier_number")
    private String dossierNumber;

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    @JsonProperty("processing_authority_date")
    private Date processingAuthorityDate;

    @JsonProperty("type")
    private AuthorityPracticeTypeEnum type;

    @JsonProperty("commodity_details")
    private CommodityDetails commodityDetails;


    public String getDossierId() {
        return dossierId;
    }

    public void setDossierId(String dossierId) {
        this.dossierId = dossierId;
    }

    public String getWorkingId() {
        return workingId;
    }

    public void setWorkingId(String workingId) {
        this.workingId = workingId;
    }

    public String getWorkingNumber() {
        return workingNumber;
    }

    public void setWorkingNumber(String workingNumber) {
        this.workingNumber = workingNumber;
    }

    public String getDossierNumber() {
        return dossierNumber;
    }

    public void setDossierNumber(String dossierNumber) {
        this.dossierNumber = dossierNumber;
    }

    public Date getProcessingAuthorityDate() {
        if(processingAuthorityDate == null){
            return null;
        }
        return (Date)processingAuthorityDate.clone();
    }

    public void setProcessingAuthorityDate(Date processingAuthorityDate) {
        if(processingAuthorityDate != null)
        {
            this.processingAuthorityDate =(Date)processingAuthorityDate.clone();
        } else {
            this.processingAuthorityDate = null;
        }
    }

    public AuthorityPracticeTypeEnum getType() {
        return type;
    }

    public void setType(AuthorityPracticeTypeEnum type) {
        this.type = type;
    }

    public CommodityDetails getCommodityDetails() {
        return commodityDetails;
    }

    public void setCommodityDetails(CommodityDetails commodityDetails) {
        this.commodityDetails = commodityDetails;
    }

    @Override
    public String toString() {
        return "AuthorityPractice{" +
                "dossierId='" + dossierId + '\'' +
                ", workingId='" + workingId + '\'' +
                ", workingNumber='" + workingNumber + '\'' +
                ", dossierNumber='" + dossierNumber + '\'' +
                ", processingAuthorityDate=" + processingAuthorityDate +
                ", type=" + type +
                ", commodityDetails=" + commodityDetails +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        AuthorityPractice that = (AuthorityPractice) o;

        return new EqualsBuilder().append(dossierId, that.dossierId).append(workingId, that.workingId)
                .append(workingNumber, that.workingNumber).append(dossierNumber, that.dossierNumber)
                .append(processingAuthorityDate, that.processingAuthorityDate).append(type, that.type)
                .append(commodityDetails, that.commodityDetails).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(dossierId).append(workingId)
                .append(workingNumber).append(dossierNumber).append(processingAuthorityDate)
                .append(type).append(commodityDetails).toHashCode();
    }
}
