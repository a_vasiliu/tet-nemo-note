package com.doing.nemo.claims.entity.jsonb;

import com.doing.nemo.claims.entity.enumerated.AuthorityEnum.AuthorityPracticeStatusEnum;
import com.doing.nemo.claims.entity.enumerated.AuthorityEnum.AuthorityPracticeTypeEnum;
import com.doing.nemo.claims.entity.enumerated.AuthorityEnum.AuthorityWorkingTypeEnum;
import com.doing.nemo.claims.entity.enumerated.ProcessingTypeEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Processing implements Serializable {

    private static final long serialVersionUID = 816814485243599654L;

    @JsonProperty("processing_type")
    private ProcessingTypeEnum processingType;

    @JsonProperty("processing_date")
    private Date processingDate;

    @JsonProperty("dossier_id")
    private String dossierId;

    @JsonProperty("working_id")
    private String workingId;

    @JsonProperty("working_number")
    private String workingNumber;

    @JsonProperty("dossier_number")
    private String dossierNumber;

    @JsonProperty("processing_authority_date")
    private Date processingAuthorityDate;

    @JsonProperty("commodity_details")
    private CommodityDetails commodityDetails;

    @JsonProperty("status")
    private AuthorityPracticeStatusEnum status;

    @JsonProperty("working_type")
    private AuthorityWorkingTypeEnum workingType;

    public AuthorityPracticeStatusEnum getStatus() {
        return status;
    }

    public void setStatus(AuthorityPracticeStatusEnum status) {
        this.status = status;
    }

    public AuthorityWorkingTypeEnum getWorkingType() {
        return workingType;
    }

    public void setWorkingType(AuthorityWorkingTypeEnum workingType) {
        this.workingType = workingType;
    }

    public ProcessingTypeEnum getProcessingType() {
        return processingType;
    }

    public void setProcessingType(ProcessingTypeEnum processingType) {
        this.processingType = processingType;
    }

    public Date getProcessingDate() {
        if(processingDate == null){
            return null;
        }
        return (Date)processingDate.clone();
    }

    public void setProcessingDate(Date processingDate) {
        if(processingDate != null)
        {
            this.processingDate = (Date)processingDate.clone();
        } else {
            this.processingDate = null;
        }
    }

    public Date getProcessingAuthorityDate() {
        if(processingAuthorityDate == null){
            return null;
        }
        return (Date)processingAuthorityDate.clone();
    }

    public void setProcessingAuthorityDate(Date processingAuthorityDate) {
        if(processingAuthorityDate != null)
        {
            this.processingAuthorityDate =  (Date)processingAuthorityDate.clone();
        } else {
            this.processingAuthorityDate = null;
        }
    }

    public String getDossierId() {
        return dossierId;
    }

    public void setDossierId(String dossierId) {
        this.dossierId = dossierId;
    }

    public String getWorkingId() {
        return workingId;
    }

    public void setWorkingId(String workingId) {
        this.workingId = workingId;
    }

    public String getWorkingNumber() {
        return workingNumber;
    }

    public void setWorkingNumber(String workingNumber) {
        this.workingNumber = workingNumber;
    }

    public String getDossierNumber() {
        return dossierNumber;
    }

    public void setDossierNumber(String dossierNumber) {
        this.dossierNumber = dossierNumber;
    }

    public CommodityDetails getCommodityDetails() {
        return commodityDetails;
    }

    public void setCommodityDetails(CommodityDetails commodityDetails) {
        this.commodityDetails = commodityDetails;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Processing that = (Processing) o;

        return new EqualsBuilder().append(processingType, that.processingType).append(processingDate, that.processingDate)
                .append(dossierId, that.dossierId).append(workingId, that.workingId).append(workingNumber, that.workingNumber)
                .append(dossierNumber, that.dossierNumber).append(processingAuthorityDate, that.processingAuthorityDate)
                .append(commodityDetails, that.commodityDetails).append(status, that.status).append(workingType, that.workingType).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(processingType).append(processingDate)
                .append(dossierId).append(workingId).append(workingNumber).append(dossierNumber).append(processingAuthorityDate)
                .append(commodityDetails).append(status).append(workingType).toHashCode();
    }
}
