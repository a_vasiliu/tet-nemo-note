package com.doing.nemo.claims.entity.settings;


import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "subgroup_ids", uniqueConstraints = {
        @UniqueConstraint(name = "uk_subgroup_id", columnNames = {"subgroup_id"})})
public class SubgroupIdsEntity {
    @Id
    @Type(type = "org.hibernate.type.PostgresUUIDType")
    @JsonProperty("id")
    private UUID id;

    @Column(name = "user_id")
    private Long userId;

    @JoinColumn(name = "subgroup_id")
    @OneToOne(fetch = FetchType.LAZY)
    private SubgroupMappingEntity subgroupMappingEntity;

    public SubgroupIdsEntity() {
    }

    public SubgroupIdsEntity(UUID id, Long userId, SubgroupMappingEntity subgroupMappingEntity) {
        this.id = id;
        this.userId = userId;
        this.subgroupMappingEntity = subgroupMappingEntity;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public SubgroupMappingEntity getSubgroupMappingEntity() {
        return subgroupMappingEntity;
    }

    public void setSubgroupMappingEntity(SubgroupMappingEntity subgroupMappingEntity) {
        this.subgroupMappingEntity = subgroupMappingEntity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SubgroupIdsEntity that = (SubgroupIdsEntity) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getUserId(), that.getUserId()) &&
                Objects.equals(getSubgroupMappingEntity(), that.getSubgroupMappingEntity());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getUserId(), getSubgroupMappingEntity());
    }
}
