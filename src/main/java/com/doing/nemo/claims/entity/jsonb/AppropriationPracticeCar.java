package com.doing.nemo.claims.entity.jsonb;

import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsAppropriationPracticeCarEnum;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AppropriationPracticeCar implements Serializable {

    private static final long serialVersionUID = 1106823669813903571L;

    @JsonProperty("id")
    private String id;

    @JsonProperty("type")
    private ClaimsAppropriationPracticeCarEnum type;



    public AppropriationPracticeCar() {
    }

    public AppropriationPracticeCar(String id, ClaimsAppropriationPracticeCarEnum type) {
        this.id = id;
        this.type = type;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ClaimsAppropriationPracticeCarEnum getType() {
        return type;
    }

    public void setType(ClaimsAppropriationPracticeCarEnum type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        AppropriationPracticeCar that = (AppropriationPracticeCar) o;

        return new EqualsBuilder().append(id, that.id).append(type, that.type).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(id).append(type).toHashCode();
    }
}
