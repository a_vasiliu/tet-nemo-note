package com.doing.nemo.claims.entity.enumerated.AuthorityEnum;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;

public enum AuthorityEventTypeEnum implements Serializable {
    SEND("send"),
    APPROVED("approved"),
    REJECT("reject"),
    UPDATED_FROM_DEALER("updated_from_dealer"),
    START_WORKING("start_working"),
    RETURN_VEHICLE("return_vehicle"),
    CANCEL_REJECT("cancel_reject"),
    END_WORKING("end_working"),
    DECLINE("decline");

    private static Logger LOGGER = LoggerFactory.getLogger(AuthorityEventTypeEnum.class);
    private String authorityStatus;

    private AuthorityEventTypeEnum(String authorityStatus) {
        this.authorityStatus = authorityStatus;
    }

    @JsonCreator
    public static AuthorityEventTypeEnum create(String authorityStatus) {

        authorityStatus = authorityStatus.replace(" - ", "_");
        authorityStatus = authorityStatus.replace('-', '_');
        authorityStatus = authorityStatus.replace(' ', '_');

        if (authorityStatus != null) {
            for (AuthorityEventTypeEnum val : AuthorityEventTypeEnum.values()) {
                if (authorityStatus.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        LOGGER.debug("Bad parameters exception. Enum class " + AuthorityEventTypeEnum.class.getSimpleName() + " doesn't accept this value: " + authorityStatus);
        throw new BadParametersException("Bad parameters exception. Enum class " + AuthorityEventTypeEnum.class.getSimpleName() + " doesn't accept this value: " + authorityStatus);
    }

    public String getValue() {
        return this.authorityStatus.toUpperCase();
    }

    @JsonValue
    public String toValue() {
        for (AuthorityEventTypeEnum val : AuthorityEventTypeEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }
}
