package com.doing.nemo.claims.entity;

import com.doing.nemo.claims.entity.jsonb.dataType.JsonDataMiddlewareUpdateIncidentsRequest;
import com.doing.nemo.middleware.client.payload.request.MiddlewareUpdateIncidentsRequest;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("MILES")
@TypeDefs({
        @TypeDef(name = "JsonDataMiddlewareUpdateIncidentsRequest", typeClass = JsonDataMiddlewareUpdateIncidentsRequest.class)
})
public class MilesExternalEntity extends ExternalCommunicationEntity{

    @Type(type = "JsonDataMiddlewareUpdateIncidentsRequest")
    @Column(name = "request")
    MiddlewareUpdateIncidentsRequest request;

    public MiddlewareUpdateIncidentsRequest getRequest() {
        return request;
    }

    public void setRequest(MiddlewareUpdateIncidentsRequest request) {
        this.request = request;
    }
}
