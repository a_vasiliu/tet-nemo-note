package com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbAntiTheftService;

import com.doing.nemo.claims.entity.enumerated.AntitheftEnum.AntitheftTypeEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AntiTheftService implements Serializable {

    @JsonProperty("id")
    private UUID id;

    @JsonProperty("code_anti_theft_service")
    private String codeAntiTheftService = "";

    @JsonProperty("name")
    private String name;

    @JsonProperty("business_name")
    private String businessName;

    @JsonProperty("supplier_code")
    private Integer supplierCode;

    @JsonProperty("email")
    private String email = "";

    @JsonProperty("phone_number")
    private String phoneNumber;

    @JsonProperty("provider_type")
    private String providerType = "";

    @JsonProperty("time_out_in_seconds")
    private Integer timeOutInSeconds;

    @JsonProperty("end_point_url")
    private String endPointUrl = "";

    @JsonProperty("username")
    private String username = "";

    @JsonProperty("password")
    private String password = "";

    @JsonProperty("company_code")
    private String companyCode = "";

    @JsonProperty("active")
    private Boolean active;

    @JsonProperty("type")
    private AntitheftTypeEnum type;

    @JsonProperty("is_active")
    private Boolean IsActive=true;

    @JsonProperty("anti_theft_request_list")
    private List<AntiTheftRequest> antiTheftList;

    @JsonProperty("registry_list")
    private List<Registry> registryList;

    public AntiTheftService() {
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getCodeAntiTheftService() {
        return codeAntiTheftService;
    }

    public void setCodeAntiTheftService(String codeAntiTheftService) {
        this.codeAntiTheftService = codeAntiTheftService;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public Integer getSupplierCode() {
        return supplierCode;
    }

    public void setSupplierCode(Integer supplierCode) {
        this.supplierCode = supplierCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getProviderType() {
        return providerType;
    }

    public void setProviderType(String providerType) {
        this.providerType = providerType;
    }

    public Integer getTimeOutInSeconds() {
        return timeOutInSeconds;
    }

    public void setTimeOutInSeconds(Integer timeOutInSeconds) {
        this.timeOutInSeconds = timeOutInSeconds;
    }

    public String getEndPointUrl() {
        return endPointUrl;
    }

    public void setEndPointUrl(String endPointUrl) {
        this.endPointUrl = endPointUrl;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean getIsActive() {
        return IsActive;
    }

    public void setIsActive(Boolean active) {
        this.IsActive = active;
    }

    @JsonIgnore
    public List<AntiTheftRequest> getAntiTheftList() {
        if(antiTheftList == null){
            return null;
        }
        return new ArrayList<>(antiTheftList);
    }

    public void setAntiTheftList(List<AntiTheftRequest> antiTheftList) {
        if(antiTheftList != null)
        {
            this.antiTheftList = new ArrayList<>(antiTheftList);
        } else {
            this.antiTheftList = null;
        }
    }

    public List<Registry> getRegistryList() {
        if(registryList == null){
            return null;
        }
        return new ArrayList<>(registryList);
    }

    public void setRegistryList(List<Registry> registryList) {
        if(registryList != null)
        {
            this.registryList = new ArrayList<>(registryList);
        } else {
            this.registryList = null;
        }
    }

    public AntitheftTypeEnum getType() {
        return type;
    }

    public void setType(AntitheftTypeEnum type) {
        this.type = type;
    }



    @Override
    public String toString() {
        return "AntiTheftService{" +
                "id=" + id +
                ", codeAntiTheftService='" + codeAntiTheftService + '\'' +
                ", name='" + name + '\'' +
                ", businessName='" + businessName + '\'' +
                ", supplierCode=" + supplierCode +
                ", email='" + email + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", providerType='" + providerType + '\'' +
                ", timeOutInSeconds=" + timeOutInSeconds +
                ", endPointUrl='" + endPointUrl + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", companyCode='" + companyCode + '\'' +
                ", active=" + active +
                ", type=" + type +
                ", IsActive=" + IsActive +
                ", antiTheftList=" + antiTheftList +
                ", registryList=" + registryList +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        AntiTheftService that = (AntiTheftService) o;

        return new EqualsBuilder().append(id, that.id).append(codeAntiTheftService, that.codeAntiTheftService)
                .append(name, that.name).append(businessName, that.businessName).append(supplierCode, that.supplierCode)
                .append(email, that.email).append(phoneNumber, that.phoneNumber).append(providerType, that.providerType)
                .append(timeOutInSeconds, that.timeOutInSeconds).append(endPointUrl, that.endPointUrl).append(username, that.username)
                .append(password, that.password).append(companyCode, that.companyCode).append(active, that.active).append(type, that.type)
                .append(IsActive, that.IsActive).append(antiTheftList, that.antiTheftList).append(registryList, that.registryList).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(id).append(codeAntiTheftService)
                .append(name).append(businessName).append(supplierCode).append(email).append(phoneNumber).append(providerType)
                .append(timeOutInSeconds).append(endPointUrl).append(username).append(password).append(companyCode)
                .append(active).append(type).append(IsActive).append(antiTheftList).append(registryList).toHashCode();
    }
}
