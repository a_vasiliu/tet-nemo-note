package com.doing.nemo.claims.entity.jsonb.counterparty;

import com.doing.nemo.claims.entity.settings.InsuranceCompanyEntity;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class InsuranceCompanyCounterparty implements Serializable {

    @JsonProperty("entity")
    private InsuranceCompanyEntity entity;

    @JsonProperty("name")
    private String name;

    public InsuranceCompanyCounterparty() {
    }

    public InsuranceCompanyCounterparty(InsuranceCompanyEntity entity, String name) {
        this.entity = entity;
        this.name = name;
    }

    public InsuranceCompanyEntity getEntity() {
        return entity;
    }

    public void setEntity(InsuranceCompanyEntity entity) {
        this.entity = entity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "InsuranceCompanyCounterparty{" +
                "entity=" + entity +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        InsuranceCompanyCounterparty that = (InsuranceCompanyCounterparty) o;

        return new EqualsBuilder().append(entity, that.entity).append(name, that.name).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(entity).append(name).toHashCode();
    }
}
