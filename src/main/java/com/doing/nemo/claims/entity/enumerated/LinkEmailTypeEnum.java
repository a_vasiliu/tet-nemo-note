package com.doing.nemo.claims.entity.enumerated;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;

public enum LinkEmailTypeEnum implements Serializable {

    EXTERNAL("<%REPOSPUBLINK_EXTERNAL%>"),
    MYALD("<%REPOSPUBLINK_MYALD%>");


    private String type;

    private static Logger LOGGER = LoggerFactory.getLogger(LinkEmailTypeEnum.class);

    private LinkEmailTypeEnum(String practiceTypeEnum) {
        this.type = practiceTypeEnum;
    }

    public String getValue() {
        return this.type.toUpperCase();
    }

    @JsonCreator
    public static LinkEmailTypeEnum create(String type) {

        type = type.replace(" - ", "_");
        type = type.replace('-', '_');
        type = type.replace(' ', '_');

        if (type != null) {
            for (LinkEmailTypeEnum val : LinkEmailTypeEnum.values()) {
                if (type.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        LOGGER.debug("Bad parameters exception. Enum class " + LinkEmailTypeEnum.class.getSimpleName() + " doesn't accept this value: " + type);
        throw new BadParametersException("Bad parameters exception. Enum class " + LinkEmailTypeEnum.class.getSimpleName() + " doesn't accept this value: " + type);
    }

    @JsonValue
    public String toValue() {
        for (LinkEmailTypeEnum val : LinkEmailTypeEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }
}
