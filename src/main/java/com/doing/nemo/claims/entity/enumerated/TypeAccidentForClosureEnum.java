package com.doing.nemo.claims.entity.enumerated;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;

public enum TypeAccidentForClosureEnum implements Serializable {
    COMPETITION("competition"),
    NOT_VERIFIABLE("not_verifiable"),
    FCM_WITHOUT_COUNTERPARTY("fcm_without_counterparty"),
    FNI_WITHOUT_COUNTERPARTY("fni_without_counterparty"),
    FUL_WITHOUT_COUNTERPARTY("ful_without_counterparty"),
    FUL_ACTIVE("ful_active"),
    FUL_PASSIVE("ful_passive"),
    FNI_ACTIVE("fni_active"),
    FNI_PASSIVE("fni_passive"),
    FCM_ACTIVE_PASSIVE("fcm_active_passive");


    private static Logger LOGGER = LoggerFactory.getLogger(TypeAccidentForClosureEnum.class);
    private String claimsRepairEnum;

    private TypeAccidentForClosureEnum(String claimsRepairEnum) {
        this.claimsRepairEnum = claimsRepairEnum;
    }

    @JsonCreator
    public static TypeAccidentForClosureEnum create(String claimsRepairEnum) {

        claimsRepairEnum = claimsRepairEnum.replace(" - ", "_");
        claimsRepairEnum = claimsRepairEnum.replace('-', '_');
        claimsRepairEnum = claimsRepairEnum.replace(' ', '_');

        if (claimsRepairEnum != null) {
            for (TypeAccidentForClosureEnum val : TypeAccidentForClosureEnum.values()) {
                if (claimsRepairEnum.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        LOGGER.debug("Bad parameters exception. Enum class " + TypeAccidentForClosureEnum.class.getSimpleName() + " doesn't accept this value: " + claimsRepairEnum);
        throw new BadParametersException("Bad parameters exception. Enum class " + TypeAccidentForClosureEnum.class.getSimpleName() + " doesn't accept this value: " + claimsRepairEnum);

    }

    public String getValue() {
        return this.claimsRepairEnum.toUpperCase();
    }

    @JsonValue
    public String toValue() {
        for (TypeAccidentForClosureEnum val : TypeAccidentForClosureEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }
}
