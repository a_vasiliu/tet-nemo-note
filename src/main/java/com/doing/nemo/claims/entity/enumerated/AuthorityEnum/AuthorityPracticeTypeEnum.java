package com.doing.nemo.claims.entity.enumerated.AuthorityEnum;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;

public enum AuthorityPracticeTypeEnum implements Serializable {
    STAMP("stamp"),
    TO_RE_REGISTER("to_re_register");

    private static Logger LOGGER = LoggerFactory.getLogger(AuthorityPracticeTypeEnum.class);
    private String authorityTypeEnum;

    private AuthorityPracticeTypeEnum(String authorityTypeEnum) {
        this.authorityTypeEnum = authorityTypeEnum;
    }

    @JsonCreator
    public static AuthorityPracticeTypeEnum create(String authorityTypeEnum) {

        authorityTypeEnum = authorityTypeEnum.replace(" - ", "_");
        authorityTypeEnum = authorityTypeEnum.replace('-', '_');
        authorityTypeEnum = authorityTypeEnum.replace(' ', '_');

        if (authorityTypeEnum != null) {
            for (AuthorityPracticeTypeEnum val : AuthorityPracticeTypeEnum.values()) {
                if (authorityTypeEnum.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        LOGGER.debug("Bad parameters exception. Enum class " + AuthorityPracticeTypeEnum.class.getSimpleName() + " doesn't accept this value: " + authorityTypeEnum);
        throw new BadParametersException("Bad parameters exception. Enum class " + AuthorityPracticeTypeEnum.class.getSimpleName() + " doesn't accept this value: " + authorityTypeEnum);
    }

    public String getValue() {
        return this.authorityTypeEnum.toUpperCase();
    }

    @JsonValue
    public String toValue() {
        for (AuthorityPracticeTypeEnum val : AuthorityPracticeTypeEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }
}
