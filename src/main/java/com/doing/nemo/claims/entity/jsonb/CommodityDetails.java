package com.doing.nemo.claims.entity.jsonb;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
@JsonIgnoreProperties(ignoreUnknown = true)
public class CommodityDetails implements Serializable {

    @JsonProperty("commodity_id")
    private String commodityId;

    @JsonProperty("name")
    private String name;

    @JsonProperty("address")
    private String address;

    @JsonProperty("address_number")
    private String addressNumber;

    @JsonProperty("location")
    private String location;

    @JsonProperty("zip_code")
    private String zipCode;

    @JsonProperty("province_code")
    private String provinceCode;

    @JsonProperty("phone")
    private String phone;

    @JsonProperty("fax")
    private String fax;

    @JsonProperty("email")
    private String email;

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCommodityId() {
        return commodityId;
    }

    public void setCommodityId(String commodityId) {
        this.commodityId = commodityId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddressNumber() {
        return addressNumber;
    }

    public void setAddressNumber(String addressNumber) {
        this.addressNumber = addressNumber;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getProvinceCode() {
        return provinceCode;
    }

    public void setProvinceCode(String provinceCode) {
        this.provinceCode = provinceCode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        CommodityDetails that = (CommodityDetails) o;

        return new EqualsBuilder().append(commodityId, that.commodityId).append(name, that.name)
                .append(address, that.address).append(addressNumber, that.addressNumber)
                .append(location, that.location).append(zipCode, that.zipCode).append(provinceCode, that.provinceCode)
                .append(phone, that.phone).append(fax, that.fax).append(email, that.email).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(commodityId).append(name)
                .append(address).append(addressNumber).append(location).append(zipCode).append(provinceCode)
                .append(phone).append(fax).append(email).toHashCode();
    }
}
