package com.doing.nemo.claims.entity.settings;


import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "subgroup_mapping", uniqueConstraints = {
        @UniqueConstraint(name = "uk_subgroup_id", columnNames = {"subgroup_id"}),
        @UniqueConstraint(name = "uk_name", columnNames = {"name"})})
public class SubgroupMappingEntity {

    @Id
    @Type(type = "org.hibernate.type.PostgresUUIDType")
    @JsonProperty("id")
    private UUID id;

    @Column(name = "name")
    private String name;

    @Column(name = "subgroup_id")
    private Long subgroupId;

    public SubgroupMappingEntity() {
    }

    public SubgroupMappingEntity(UUID id, String name, Long subgroupId) {
        this.id = id;
        this.name = name;
        this.subgroupId = subgroupId;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getSubgroupId() {
        return subgroupId;
    }

    public void setSubgroupId(Long subgroupId) {
        this.subgroupId = subgroupId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SubgroupMappingEntity that = (SubgroupMappingEntity) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getName(), that.getName()) &&
                Objects.equals(getSubgroupId(), that.getSubgroupId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getSubgroupId());
    }
}
