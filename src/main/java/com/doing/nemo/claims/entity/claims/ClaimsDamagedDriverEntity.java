package com.doing.nemo.claims.entity.claims;

import com.doing.nemo.claims.entity.ClaimsNewEntity;
import com.doing.nemo.claims.entity.jsonb.Address;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbDriver.DrivingLicense;
import com.doing.nemo.claims.entity.jsonb.dataType.JsonDataAddressType;
import com.doing.nemo.claims.entity.jsonb.dataType.JsonDataDrivingLicenseType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@JsonIgnoreProperties({"hibernateLazyInitializer", "hibernate_lazy_initializer", "handler"})
@Entity
@Table(name = "claims_damaged_driver")
@TypeDefs({
        @TypeDef(name = "JsonDataAddressType", typeClass = JsonDataAddressType.class),
        @TypeDef(name = "JsonDataDrivingLicenseType", typeClass = JsonDataDrivingLicenseType.class)
})
public class ClaimsDamagedDriverEntity implements Serializable {

    @Id
    @Column(name = "id")
    private String id;

    @Column(name="driver_id")
    //EX id
    private Integer driverId;

    @Column(name="identification")
    private String identification;

    @Column(name="official_registration")
    private String officialRegistration;

    @Column(name="trading_name")
    private String tradingName;

    @Column(name="first_name")
    private String firstname;

    @Column(name="last_name")
    private String lastname;

    @Column(name="fiscal_code")
    private String fiscalCode;

    @Column(name="main_address")
    @Type(type ="JsonDataAddressType")
    private Address mainAddress;

    @Column(name="date_of_birth")
    //@JsonFormat(pattern = "yyyy-MM-dd")
    private Date dateOfBirth;

    @Column(name="phone")
    private String phone;

    @Column(name="email")
    private String email;

    @Column(name="pec")
    private String pec;

    @Column(name="sex")
    private String sex;

    @Column(name="customer_id")
    private String customerId;

    @Column(name="driver_injury")
    private Boolean driverInjury;

    @Column(name="driving_license")
    @Type(type = "JsonDataDrivingLicenseType")
    private DrivingLicense drivingLicense;

    @OneToOne
    @JoinColumn(name = "id")
    @MapsId("id")
    @PrimaryKeyJoinColumn
    private ClaimsNewEntity claim;

    public ClaimsDamagedDriverEntity() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getDriverId() {
        return driverId;
    }

    public void setDriverId(Integer driverId) {
        this.driverId = driverId;
    }

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public String getOfficialRegistration() {
        return officialRegistration;
    }

    public void setOfficialRegistration(String officialRegistration) {
        this.officialRegistration = officialRegistration;
    }

    public String getTradingName() {
        return tradingName;
    }

    public void setTradingName(String tradingName) {
        this.tradingName = tradingName;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFiscalCode() {
        return fiscalCode;
    }

    public void setFiscalCode(String fiscalCode) {
        this.fiscalCode = fiscalCode;
    }

    public Address getMainAddress() {
        return mainAddress;
    }

    public void setMainAddress(Address mainAddress) {
        this.mainAddress = mainAddress;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPec() {
        return pec;
    }

    public void setPec(String pec) {
        this.pec = pec;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public Boolean getDriverInjury() {
        return driverInjury;
    }

    public void setDriverInjury(Boolean driverInjury) {
        this.driverInjury = driverInjury;
    }

    public DrivingLicense getDrivingLicense() {
        return drivingLicense;
    }

    public void setDrivingLicense(DrivingLicense drivingLicense) {
        this.drivingLicense = drivingLicense;
    }

    public ClaimsNewEntity getClaim() {
        return claim;
    }

    public void setClaim(ClaimsNewEntity claim) {
        this.claim = claim;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ClaimsDamagedDriverEntity)) return false;
        ClaimsDamagedDriverEntity that = (ClaimsDamagedDriverEntity) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getDriverId(), that.getDriverId()) &&
                Objects.equals(getIdentification(), that.getIdentification()) &&
                Objects.equals(getOfficialRegistration(), that.getOfficialRegistration()) &&
                Objects.equals(getTradingName(), that.getTradingName()) &&
                Objects.equals(getFirstname(), that.getFirstname()) &&
                Objects.equals(getLastname(), that.getLastname()) &&
                Objects.equals(getFiscalCode(), that.getFiscalCode()) &&
                Objects.equals(getMainAddress(), that.getMainAddress()) &&
                Objects.equals(getDateOfBirth(), that.getDateOfBirth()) &&
                Objects.equals(getPhone(), that.getPhone()) &&
                Objects.equals(getEmail(), that.getEmail()) &&
                Objects.equals(getPec(), that.getPec()) &&
                Objects.equals(getSex(), that.getSex()) &&
                Objects.equals(getCustomerId(), that.getCustomerId()) &&
                Objects.equals(getDriverInjury(), that.getDriverInjury()) &&
                Objects.equals(getDrivingLicense(), that.getDrivingLicense()) &&
                Objects.equals(getClaim(), that.getClaim());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getDriverId(), getIdentification(), getOfficialRegistration(), getTradingName(), getFirstname(), getLastname(), getFiscalCode(), getMainAddress(), getDateOfBirth(), getPhone(), getEmail(), getPec(), getSex(), getCustomerId(), getDriverInjury(), getDrivingLicense(), getClaim());
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ClaimsDamagedDriverEntity{");
        sb.append("id='").append(id).append('\'');
        sb.append(", driverId=").append(driverId);
        sb.append(", identification='").append(identification).append('\'');
        sb.append(", officialRegistration='").append(officialRegistration).append('\'');
        sb.append(", tradingName='").append(tradingName).append('\'');
        sb.append(", firstname='").append(firstname).append('\'');
        sb.append(", lastname='").append(lastname).append('\'');
        sb.append(", fiscalCode='").append(fiscalCode).append('\'');
        sb.append(", mainAddress=").append(mainAddress);
        sb.append(", dateOfBirth=").append(dateOfBirth);
        sb.append(", phone='").append(phone).append('\'');
        sb.append(", email='").append(email).append('\'');
        sb.append(", pec='").append(pec).append('\'');
        sb.append(", sex='").append(sex).append('\'');
        sb.append(", customerId='").append(customerId).append('\'');
        sb.append(", driverInjury=").append(driverInjury);
        sb.append(", drivingLicense=").append(drivingLicense);
        sb.append('}');
        return sb.toString();
    }
}
