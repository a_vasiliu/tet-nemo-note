package com.doing.nemo.claims.entity.enumerated.AntitheftEnum;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;

public enum AntitheftTypeEnum implements Serializable {
    CRASH("crash"),
    THEFT("theft");

    private static Logger LOGGER = LoggerFactory.getLogger(AntitheftTypeEnum.class);

    private String antitheftEntity;

    private AntitheftTypeEnum(String antitheftEntity) {
        this.antitheftEntity = antitheftEntity;
    }

    @JsonCreator
    public static AntitheftTypeEnum create(String antitheftEntity) {

        if (antitheftEntity != null) {
            for (AntitheftTypeEnum val : AntitheftTypeEnum.values()) {
                if (antitheftEntity.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }

        LOGGER.debug("Bad parameters exception. Enum class " + AntitheftTypeEnum.class.getSimpleName() + " doesn't accept this value: " + antitheftEntity);
        throw new BadParametersException("Bad parameters exception. Enum class " + AntitheftTypeEnum.class.getSimpleName() + " doesn't accept this value: " + antitheftEntity);
    }

    public String getValue() {
        return this.antitheftEntity.toUpperCase();
    }

    @JsonValue
    public String toValue() {
        for (AntitheftTypeEnum val : AntitheftTypeEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }
}
