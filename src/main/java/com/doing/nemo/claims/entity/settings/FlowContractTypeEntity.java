package com.doing.nemo.claims.entity.settings;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "flow_contract_type")
public class FlowContractTypeEntity {

    @EmbeddedId
    private FlowContractTypeId id;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("claimsTypeId")
    private ClaimsTypeEntity claimsTypeEntity;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("contractTypeId")
    private ContractTypeEntity contractTypeEntity;

    @Column(name = "flow")
    private String flow;

    @Column(name = "rebilling")
    private String rebilling;

    public FlowContractTypeEntity() {
    }

    public FlowContractTypeEntity(ClaimsTypeEntity claimsTypeEntity, ContractTypeEntity contractTypeEntity) {
        this.id = new FlowContractTypeId(contractTypeEntity.getId(), claimsTypeEntity.getId());
        this.claimsTypeEntity = claimsTypeEntity;
        this.contractTypeEntity = contractTypeEntity;
    }

    public FlowContractTypeEntity(ClaimsTypeEntity claimsTypeEntity, ContractTypeEntity contractTypeEntity, String flow, String rebilling) {
        this.id = new FlowContractTypeId(contractTypeEntity.getId(), claimsTypeEntity.getId());
        this.claimsTypeEntity = claimsTypeEntity;
        this.contractTypeEntity = contractTypeEntity;
        this.flow = flow;
        this.rebilling = rebilling;
    }

    public UUID getId() {
        return id.getClaimsTypeId();
    }

    public void setId(FlowContractTypeId id) {
        this.id = id;
    }


    public ClaimsTypeEntity getClaimsTypeEntity() {
        return claimsTypeEntity;
    }

    public void setClaimsTypeEntity(ClaimsTypeEntity claimsTypeEntity) {
        this.claimsTypeEntity = claimsTypeEntity;
    }

    @JsonIgnore
    public ContractTypeEntity getContractTypeEntity() {
        return contractTypeEntity;
    }

    public void setContractTypeEntity(ContractTypeEntity contractTypeEntity) {
        this.contractTypeEntity = contractTypeEntity;
    }

    public String getFlow() {
        return flow;
    }

    public void setFlow(String flow) {
        this.flow = flow;
    }

    public String getRebilling() {
        return rebilling;
    }

    public void setRebilling(String rebilling) {
        this.rebilling = rebilling;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass())
            return false;

        FlowContractTypeEntity that = (FlowContractTypeEntity) o;
        return Objects.equals(getClaimsTypeEntity(), that.getClaimsTypeEntity()) &&
                Objects.equals(getContractTypeEntity(), that.getContractTypeEntity());
    }


    @Override
    public int hashCode() {
        return Objects.hash(getClaimsTypeEntity(), getContractTypeEntity());
    }

    @Override
    public String toString() {
        return "FlowContractTypeEntity{" +
                "id=" + id +
                ", flow='" + flow + '\'' +
                ", rebilling='" + rebilling + '\'' +
                '}';
    }
}
