package com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged;

import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbInsuranceCompany.*;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class InsuranceCompany implements Serializable {

    private static final long serialVersionUID = 5259870127924023409L;

    @JsonProperty("is_card")
    private Boolean isCard;

    /////////////////////////////////////////////////////////
    //da eliminare perchè sarebbe company
    //@JsonProperty("denomination")
    //private String denomination;
    /////////////////////////////////////////////////////////

    @JsonProperty("id")
    private String id;
    @JsonProperty("tpl")
    private Tpl tpl;
    @JsonProperty("theft")
    private Theft theft;
    @JsonProperty("material_damage")
    private MaterialDamage materialDamage;
    @JsonProperty("pai")
    private Pai pai;
    @JsonProperty("legal_cost")
    private LegalCost legalCost;
    @JsonProperty("kasko")
    private Kasko kasko;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Tpl getTpl() {
        return tpl;
    }

    public void setTpl(Tpl tpl) {
        this.tpl = tpl;
    }

    public Theft getTheft() {
        return theft;
    }

    public void setTheft(Theft theft) {
        this.theft = theft;
    }

    public MaterialDamage getMaterialDamage() {
        return materialDamage;
    }

    public void setMaterialDamage(MaterialDamage materialDamage) {
        this.materialDamage = materialDamage;
    }

    public Pai getPai() {
        return pai;
    }

    public void setPai(Pai pai) {
        this.pai = pai;
    }

    public LegalCost getLegalCost() {
        return legalCost;
    }

    public void setLegalCost(LegalCost legalCost) {
        this.legalCost = legalCost;
    }

    @JsonIgnore
    public Boolean getCard() {
        return isCard;
    }

    public void setCard(Boolean card) {
        isCard = card;
    }

    public Kasko getKasko() {
        return kasko;
    }

    public void setKasko(Kasko kasko) {
        this.kasko = kasko;
    }

    @Override
    public String toString() {
        return "InsuranceCompany{" +
                "isCard=" + isCard +
                ", id='" + id + '\'' +
                ", tpl=" + tpl +
                ", theft=" + theft +
                ", materialDamage=" + materialDamage +
                ", pai=" + pai +
                ", legalCost=" + legalCost +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        InsuranceCompany that = (InsuranceCompany) o;

        return new EqualsBuilder().append(isCard, that.isCard).append(id, that.id).append(tpl, that.tpl)
                .append(theft, that.theft).append(materialDamage, that.materialDamage).append(pai, that.pai)
                .append(legalCost, that.legalCost).append(kasko, that.kasko).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(isCard)
                .append(id).append(tpl).append(theft).append(materialDamage).append(pai).append(legalCost).append(kasko).toHashCode();
    }
}
