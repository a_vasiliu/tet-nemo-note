package com.doing.nemo.claims.entity.enumerated.DamagedEnum.DriverEnum;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;

public enum DriverCategoryEnum implements Serializable {
    A("a"), A1("a1"), A2("a2"), B("b"), B1("b1"), C("c"), C1("c1"), D("d"), D1("d1");

    private static Logger LOGGER = LoggerFactory.getLogger(DriverCategoryEnum.class);
    private String categoryEntity;

    private DriverCategoryEnum(String categoryEntity) {
        this.categoryEntity = categoryEntity;
    }

    @JsonCreator
    public static DriverCategoryEnum create(String categoryEntity) {

        categoryEntity = categoryEntity.replace(" - ", "_");
        categoryEntity = categoryEntity.replace('-', '_');
        categoryEntity = categoryEntity.replace(' ', '_');

        if (categoryEntity != null) {
            for (DriverCategoryEnum val : DriverCategoryEnum.values()) {
                if (categoryEntity.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        LOGGER.debug("Bad parameters exception. Enum class " + DriverCategoryEnum.class.getSimpleName() + " doesn't accept this value: " + categoryEntity);
        throw new BadParametersException("Bad parameters exception. Enum class " + DriverCategoryEnum.class.getSimpleName() + " doesn't accept this value: " + categoryEntity);
    }

    public String getValue() {
        return this.categoryEntity.toUpperCase();
    }

    @JsonValue
    public String toValue() {
        for (DriverCategoryEnum val : DriverCategoryEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }
}
