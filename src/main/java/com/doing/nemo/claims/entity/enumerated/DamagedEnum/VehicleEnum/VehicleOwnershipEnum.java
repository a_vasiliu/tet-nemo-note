package com.doing.nemo.claims.entity.enumerated.DamagedEnum.VehicleEnum;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;

public enum VehicleOwnershipEnum implements Serializable {
    OUR("our"),
    OTHERS("others");

    private static Logger LOGGER = LoggerFactory.getLogger(VehicleOwnershipEnum.class);
    private String ownershipEntity;

    private VehicleOwnershipEnum(String ownershipEntity) {
        this.ownershipEntity = ownershipEntity;
    }

    @JsonCreator
    public static VehicleOwnershipEnum create(String ownershipEntity) {

        ownershipEntity = ownershipEntity.replace(" - ", "_");
        ;
        ownershipEntity = ownershipEntity.replace('-', '_');
        ownershipEntity = ownershipEntity.replace(' ', '_');

        if (ownershipEntity != null) {
            for (VehicleOwnershipEnum val : VehicleOwnershipEnum.values()) {
                if (ownershipEntity.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        LOGGER.debug("Bad parameters exception. Enum class " + VehicleOwnershipEnum.class.getSimpleName() + " doesn't accept this value: " + ownershipEntity);
        throw new BadParametersException("Bad parameters exception. Enum class " + VehicleOwnershipEnum.class.getSimpleName() + " doesn't accept this value: " + ownershipEntity);
    }

    public String getValue() {
        return this.ownershipEntity.toUpperCase();
    }

    @JsonValue
    public String toValue() {
        for (VehicleOwnershipEnum val : VehicleOwnershipEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }
}
