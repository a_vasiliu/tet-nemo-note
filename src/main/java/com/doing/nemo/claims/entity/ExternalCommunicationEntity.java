package com.doing.nemo.claims.entity;

import com.doing.nemo.claims.entity.enumerated.ExternalCommunicationEnum;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity(name = "external_communication")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="external_service",
        discriminatorType = DiscriminatorType.STRING)

public class ExternalCommunicationEntity {
    @Id
    @Type(type = "org.hibernate.type.PostgresUUIDType")
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    private UUID id;

    @Column(name = "claims_id")
    private String claimsId;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_at", updatable = false)
    private Date createdAt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "sent_at")
    private Date sentAt;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private ExternalCommunicationEnum status;

    @Column(name="error_message")
    private String errorMessage;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="last_retry_date")
    private Date lastRetryDate;

    public Date getCreatedAt() {
        return createdAt;
    }

    public ExternalCommunicationEnum getStatus() {
        return status;
    }

    public Date getSentAt() {
        return sentAt;
    }

    public String getClaimsId() {
        return claimsId;
    }

    public UUID getId() {
        return id;
    }

    public void setClaimsId(String claimsId) {
        this.claimsId = claimsId;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public void setSentAt(Date sentAt) {
        this.sentAt = sentAt;
    }

    public void setStatus(ExternalCommunicationEnum status) {
        this.status = status;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Date getLastRetryDate() {
        return lastRetryDate;
    }

    public void setLastRetryDate(Date lastRetryDate) {
        this.lastRetryDate = lastRetryDate;
    }
}
