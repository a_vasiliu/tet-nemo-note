package com.doing.nemo.claims.entity.esb.InsuranceCompanyESB;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class InsuranceCompanyEsb implements Serializable {

    @JsonProperty("id")
    private String id;
    @JsonProperty("tpl")
    private TplEsb tpl;
    @JsonProperty("theft")
    private TheftEsb theft;
    @JsonProperty("material_damage")
    private MaterialDamageEsb materialDamage;
    @JsonProperty("pai")
    private PaiEsb pai;
    @JsonProperty("legal_cost")
    private LegalCostEsb legalCost;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public TplEsb getTpl() {
        return tpl;
    }

    public void setTpl(TplEsb tpl) {
        this.tpl = tpl;
    }

    public TheftEsb getTheft() {
        return theft;
    }

    public void setTheft(TheftEsb theft) {
        this.theft = theft;
    }

    public MaterialDamageEsb getMaterialDamage() {
        return materialDamage;
    }

    public void setMaterialDamage(MaterialDamageEsb materialDamage) {
        this.materialDamage = materialDamage;
    }

    public PaiEsb getPai() {
        return pai;
    }

    public void setPai(PaiEsb pai) {
        this.pai = pai;
    }

    public LegalCostEsb getLegalCost() {
        return legalCost;
    }

    public void setLegalCost(LegalCostEsb legalCost) {
        this.legalCost = legalCost;
    }


    @Override
    public String toString() {
        return "InsuranceCompanyEsb{" +
                "id='" + id + '\'' +
                ", tpl=" + tpl +
                ", theft=" + theft +
                ", materialDamage=" + materialDamage +
                ", pai=" + pai +
                ", legalCost=" + legalCost +
                '}';
    }
}
