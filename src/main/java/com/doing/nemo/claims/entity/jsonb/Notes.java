package com.doing.nemo.claims.entity.jsonb;

import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.ClaimsNewEntity;
import com.doing.nemo.claims.entity.enumerated.NotesEnum.NotesTypeEnum;
import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Notes implements Serializable {

    private static final long serialVersionUID = -9030198831870881282L;

    @JsonProperty("title")
    private String noteTitle;

    @JsonProperty("note_type")
    private NotesTypeEnum noteType;

    @JsonProperty("note_id")
    private String noteId;

    @JsonProperty("is_important")
    private Boolean isImportant;

    @JsonProperty("description")
    private String description;

    @JsonProperty("created_at")
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    private Date createdAt;

    @JsonProperty("user_id")
    private String userId;

    @JsonProperty("hidden")
    private Boolean hidden = false;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @JsonIgnore
    public Boolean getImportant() {
        return isImportant;
    }

    public void setImportant(Boolean important) {
        isImportant = important;
    }

    public String getNoteTitle() {
        return noteTitle;
    }

    public void setNoteTitle(String noteTitle) {
        this.noteTitle = noteTitle;
    }

    public NotesTypeEnum getNoteType() {
        return noteType;
    }

    public void setNoteType(NotesTypeEnum noteType) {
        this.noteType = noteType;
    }

    public String getNoteId() {
        return noteId;
    }

    public void setNoteId(String noteId) {
        this.noteId = noteId;
    }

    public Date getCreatedAt() {
        if(createdAt == null){
            return null;
        }
        return (Date)createdAt.clone();
    }

    public void setCreatedAt(Date createdAt) {
        if(createdAt != null)
        {
            this.createdAt = (Date)createdAt.clone();
        } else {
            this.createdAt = null;
        }
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Boolean getHidden() {
        return hidden;
    }

    @JsonSetter
    public void setHidden(Boolean hidden) {
        if (hidden != null) this.hidden = hidden;
    }

    @Override
    public String toString() {
        return "Notes{" +
                "noteTitle='" + noteTitle + '\'' +
                ", noteType=" + noteType +
                ", noteId='" + noteId + '\'' +
                ", isImportant=" + isImportant +
                ", description='" + description + '\'' +
                ", createdAt=" + createdAt +
                ", userId='" + userId + '\'' +
                ", hidden=" + hidden +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Notes notes = (Notes) o;

        return new EqualsBuilder().append(noteTitle, notes.noteTitle).append(noteType, notes.noteType)
                .append(noteId, notes.noteId).append(isImportant, notes.isImportant).append(description, notes.description)
                .append(createdAt, notes.createdAt).append(userId, notes.userId).append(hidden, notes.hidden).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(noteTitle).append(noteType)
                .append(noteId).append(isImportant).append(description).append(createdAt).append(userId)
                .append(hidden).toHashCode();
    }
}
