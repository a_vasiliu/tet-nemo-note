package com.doing.nemo.claims.entity.settings;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "inspectorate",
        uniqueConstraints = @UniqueConstraint(
                name = "uc_inspectorate",
                columnNames = {
                        "name"
                }
        )
)
public class InspectorateEntity {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )

    @Type(type = "org.hibernate.type.PostgresUUIDType")
    private UUID id;

    @Column(name = "code", unique = true, insertable = false, updatable = false)
    private Long code;

    @Column(name = "name")
    private String name;

    @Column(name = "email")
    private String email;

    @Column(name = "address")
    private String address;

    @Column(name = "zip_code")
    private String zipCode;

    @Column(name = "city")
    private String city;

    @Column(name = "province")
    private String province;

    @Column(name = "state")
    private String state;

    @Column(name = "vat_number")
    private String vatNumber;

    @Column(name = "fiscal_code")
    private String fiscalCode;

    @Column(name = "telephone")
    private String telephone;

    @Column(name = "fax")
    private String fax;

    @Column(name = "web_site")
    private String webSite;

    @Column(name = "reference_person")
    private String referencePerson;

    @Column(name = "attorney")
    private String attorney;

    @Column(name = "external_code")
    private String externalCode;

    @Column(name = "is_active")
    private Boolean isActive;

    @OneToMany(
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @JoinColumn(name = "inspectorate_id")
    private List<AutomaticAffiliationRuleInspectorateEntity> automaticAffiliationRuleEntityList;

    public InspectorateEntity() {

    }

    public InspectorateEntity(Long code, String name, String email, String address, String zipCode, String city, String province, String state, String vatNumber, String fiscalCode, String telephone, String fax, String webSite, String referencePerson, String attorney, String externalCode, Boolean isActive, List<AutomaticAffiliationRuleInspectorateEntity> automaticAffiliationRuleEntityList) {
        this.code = code;
        this.name = name;
        this.email = email;
        this.address = address;
        this.zipCode = zipCode;
        this.city = city;
        this.province = province;
        this.state = state;
        this.vatNumber = vatNumber;
        this.fiscalCode = fiscalCode;
        this.telephone = telephone;
        this.fax = fax;
        this.webSite = webSite;
        this.referencePerson = referencePerson;
        this.attorney = attorney;
        this.externalCode = externalCode;
        this.isActive = isActive;
        this.automaticAffiliationRuleEntityList = automaticAffiliationRuleEntityList;
    }

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getVatNumber() {
        return vatNumber;
    }

    public void setVatNumber(String vatNumber) {
        this.vatNumber = vatNumber;
    }

    public String getFiscalCode() {
        return fiscalCode;
    }

    public void setFiscalCode(String fiscalCode) {
        this.fiscalCode = fiscalCode;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getWebSite() {
        return webSite;
    }

    public void setWebSite(String webSite) {
        this.webSite = webSite;
    }

    public String getReferencePerson() {
        return referencePerson;
    }

    public void setReferencePerson(String referencePerson) {
        this.referencePerson = referencePerson;
    }

    public String getAttorney() {
        return attorney;
    }

    public void setAttorney(String attorney) {
        this.attorney = attorney;
    }

    public String getExternalCode() {
        return externalCode;
    }

    public void setExternalCode(String externalCode) {
        this.externalCode = externalCode;
    }

    public List<AutomaticAffiliationRuleInspectorateEntity> getAutomaticAffiliationRuleEntityList() {
        return automaticAffiliationRuleEntityList;
    }

    public void setAutomaticAffiliationRuleEntityList(List<AutomaticAffiliationRuleInspectorateEntity> automaticAffiliationRuleEntityList) {
        this.automaticAffiliationRuleEntityList = automaticAffiliationRuleEntityList;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    @Override
    public String toString() {
        return "InspectorateEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", address='" + address + '\'' +
                ", zipCode=" + zipCode +
                ", city='" + city + '\'' +
                ", province='" + province + '\'' +
                ", state='" + state + '\'' +
                ", vatNumber='" + vatNumber + '\'' +
                ", fiscalCode='" + fiscalCode + '\'' +
                ", telephone='" + telephone + '\'' +
                ", fax='" + fax + '\'' +
                ", webSite='" + webSite + '\'' +
                ", referencePerson='" + referencePerson + '\'' +
                ", attorney='" + attorney + '\'' +
                ", externalCode='" + externalCode + '\'' +
                ", isActive=" + isActive +
                ", automaticAffiliationRuleEntityList=" + automaticAffiliationRuleEntityList +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InspectorateEntity that = (InspectorateEntity) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getCode(), that.getCode()) &&
                Objects.equals(getName(), that.getName()) &&
                Objects.equals(getEmail(), that.getEmail()) &&
                Objects.equals(getAddress(), that.getAddress()) &&
                Objects.equals(getZipCode(), that.getZipCode()) &&
                Objects.equals(getCity(), that.getCity()) &&
                Objects.equals(getProvince(), that.getProvince()) &&
                Objects.equals(getState(), that.getState()) &&
                Objects.equals(getVatNumber(), that.getVatNumber()) &&
                Objects.equals(getFiscalCode(), that.getFiscalCode()) &&
                Objects.equals(getTelephone(), that.getTelephone()) &&
                Objects.equals(getFax(), that.getFax()) &&
                Objects.equals(getWebSite(), that.getWebSite()) &&
                Objects.equals(getReferencePerson(), that.getReferencePerson()) &&
                Objects.equals(getAttorney(), that.getAttorney()) &&
                Objects.equals(getExternalCode(), that.getExternalCode()) &&
                Objects.equals(getActive(), that.getActive()) &&
                Objects.equals(getAutomaticAffiliationRuleEntityList(), that.getAutomaticAffiliationRuleEntityList());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getCode(), getName(), getEmail(), getAddress(), getZipCode(), getCity(), getProvince(), getState(), getVatNumber(), getFiscalCode(), getTelephone(), getFax(), getWebSite(), getReferencePerson(), getAttorney(), getExternalCode(), getActive(), getAutomaticAffiliationRuleEntityList());
    }
}