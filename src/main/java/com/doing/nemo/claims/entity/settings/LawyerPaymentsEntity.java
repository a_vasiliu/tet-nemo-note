package com.doing.nemo.claims.entity.settings;

import com.doing.nemo.claims.entity.enumerated.LawyerPaymentsStatusEnum;
import com.doing.nemo.claims.entity.enumerated.RefundEnum.RefundTypeEnum;
import com.doing.nemo.claims.entity.enumerated.RefundEnum.TypeEnum;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.time.Instant;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "lawyer_payments")
@JsonIgnoreProperties({"hibernateLazyInitializer", "hibernate_lazy_initializer", "handler"})
public class LawyerPaymentsEntity {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Type(type = "org.hibernate.type.PostgresUUIDType")
    private UUID id;

    //Targa
    @Column(name = "plate")
    private String plate;

    //Numero sinistro
    @Column(name = "practice_id")
    private Long practiceId;

    //Data sinistro
    @Column(name = "date_accident")
    private Instant dateAccident;

    //Danno
    @Column(name = "damage")
    private Double damage;

    //Fermo tecnico
    @Column( name = "technical_shutdown")
    private Double technicalShutdown;

    //Onorari
    @Column(name = "fee")
    private Double fee;

    //Spese causa
    @Column(name = "expenses_suit")
    private Double expensesSuit;

    //Accredito/Saldo
    @Enumerated(EnumType.STRING)
    @Column(name = "acc_sale")
    private TypeEnum accSale;

    //Tipologia pagamento
    @Enumerated(EnumType.STRING)
    @Column(name = "paymentType")
    private RefundTypeEnum paymentType;

    //Importo
    @Column(name = "amount")
    private Double amount;

    //Numero Transazione
    @Column(name = "transaction_number")
    private String transactionNumber;

    //Banca
    @Column(name = "bank")
    private String bank;

    //Note
    @Column(name = "note")
    private String note;

    //Codice Avvocato
    @Column(name = "lawyer_code")
    private Long lawyerCode;

    @Enumerated(EnumType.STRING)
    @Column(name="status")
    private LawyerPaymentsStatusEnum status;

    public LawyerPaymentsEntity() {
    }

    public LawyerPaymentsEntity(UUID id, String plate, Long practiceId, Instant dateAccident, Double damage, Double technicalShutdown, Double fee, Double expensesSuit, TypeEnum accSale, RefundTypeEnum paymentType, Double amount, String transactionNumber, String bank, String note, Long lawyerCode, LawyerPaymentsStatusEnum status) {
        this.id = id;
        this.plate = plate;
        this.practiceId = practiceId;
        this.dateAccident = dateAccident;
        this.damage = damage;
        this.technicalShutdown = technicalShutdown;
        this.fee = fee;
        this.expensesSuit = expensesSuit;
        this.accSale = accSale;
        this.paymentType = paymentType;
        this.amount = amount;
        this.transactionNumber = transactionNumber;
        this.bank = bank;
        this.note = note;
        this.lawyerCode = lawyerCode;
        this.status = status;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public Long getPracticeId() {
        return practiceId;
    }

    public void setPracticeId(Long practiceId) {
        this.practiceId = practiceId;
    }

    public Instant getDateAccident() {
        return dateAccident;
    }

    public void setDateAccident(Instant dateAccident) {
        this.dateAccident = dateAccident;
    }

    public Double getDamage() {
        return damage;
    }

    public void setDamage(Double damage) {
        this.damage = damage;
    }

    public Double getTechnicalShutdown() {
        return technicalShutdown;
    }

    public void setTechnicalShutdown(Double technicalShutdown) {
        this.technicalShutdown = technicalShutdown;
    }

    public Double getFee() {
        return fee;
    }

    public void setFee(Double fee) {
        this.fee = fee;
    }

    public Double getExpensesSuit() {
        return expensesSuit;
    }

    public void setExpensesSuit(Double expensesSuit) {
        this.expensesSuit = expensesSuit;
    }

    public TypeEnum getAccSale() {
        return accSale;
    }

    public void setAccSale(TypeEnum accSale) {
        this.accSale = accSale;
    }

    public RefundTypeEnum getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(RefundTypeEnum paymentType) {
        this.paymentType = paymentType;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getTransactionNumber() {
        return transactionNumber;
    }

    public void setTransactionNumber(String transactionNumber) {
        this.transactionNumber = transactionNumber;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Long getLawyerCode() {
        return lawyerCode;
    }

    public void setLawyerCode(Long lawyerCode) {
        this.lawyerCode = lawyerCode;
    }

    public LawyerPaymentsStatusEnum getStatus() {
        return status;
    }

    public void setStatus(LawyerPaymentsStatusEnum status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "LawyerPaymentsEntity{" +
                "id=" + id +
                ", plate='" + plate + '\'' +
                ", practiceId=" + practiceId +
                ", dateAccident=" + dateAccident +
                ", damage=" + damage +
                ", technicalShutdown=" + technicalShutdown +
                ", fee=" + fee +
                ", expensesSuit=" + expensesSuit +
                ", accSale='" + accSale + '\'' +
                ", paymentType=" + paymentType +
                ", amount=" + amount +
                ", transactionNumber='" + transactionNumber + '\'' +
                ", bank='" + bank + '\'' +
                ", note='" + note + '\'' +
                ", lawyerCode=" + lawyerCode +
                ", status=" + status +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LawyerPaymentsEntity that = (LawyerPaymentsEntity) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getPlate(), that.getPlate()) &&
                Objects.equals(getPracticeId(), that.getPracticeId()) &&
                Objects.equals(getDateAccident(), that.getDateAccident()) &&
                Objects.equals(getDamage(), that.getDamage()) &&
                Objects.equals(getTechnicalShutdown(), that.getTechnicalShutdown()) &&
                Objects.equals(getFee(), that.getFee()) &&
                Objects.equals(getExpensesSuit(), that.getExpensesSuit()) &&
                getAccSale() == that.getAccSale() &&
                getPaymentType() == that.getPaymentType() &&
                Objects.equals(getAmount(), that.getAmount()) &&
                Objects.equals(getTransactionNumber(), that.getTransactionNumber()) &&
                Objects.equals(getBank(), that.getBank()) &&
                Objects.equals(getNote(), that.getNote()) &&
                Objects.equals(getLawyerCode(), that.getLawyerCode()) &&
                getStatus() == that.getStatus();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getPlate(), getPracticeId(), getDateAccident(), getDamage(), getTechnicalShutdown(), getFee(), getExpensesSuit(), getAccSale(), getPaymentType(), getAmount(), getTransactionNumber(), getBank(), getNote(), getLawyerCode(), getStatus());
    }
}
