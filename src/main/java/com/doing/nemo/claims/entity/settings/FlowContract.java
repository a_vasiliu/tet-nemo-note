package com.doing.nemo.claims.entity.settings;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FlowContract {

    @JsonProperty("id")
    private String id;

    @JsonProperty("flow")
    private String flow;

    @JsonProperty("rebilling")
    private String rebilling;

    public FlowContract() {
    }

    public FlowContract(String id, String flow, String rebilling) {
        this.id = id;
        this.flow = flow;
        this.rebilling = rebilling;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFlow() {
        return flow;
    }

    public void setFlow(String flow) {
        this.flow = flow;
    }

    public String getRebilling() {
        return rebilling;
    }

    public void setRebilling(String rebilling) {
        this.rebilling = rebilling;
    }

    @Override
    public String toString() {
        return "FlowContract{" +
                ", flow='" + flow + '\'' +
                ", rebilling='" + rebilling + '\'' +
                '}';
    }
}
