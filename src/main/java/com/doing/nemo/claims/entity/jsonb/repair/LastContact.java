package com.doing.nemo.claims.entity.jsonb.repair;

import com.doing.nemo.claims.entity.enumerated.RepairEnum.RepairContactResultEnum;
import com.doing.nemo.claims.entity.settings.MotivationEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LastContact implements Serializable {

    private static final long serialVersionUID = -2663458934494520427L;

    @JsonProperty("last_call")
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    private Date lastCall;

    @JsonProperty("result")
    private RepairContactResultEnum result;

    @JsonProperty("recall")
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    private Date recall;

    @JsonProperty("note")
    private String note;

    @JsonProperty("motivation")
    private MotivationEntity motivation;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Date getLastCall() {
        if(lastCall == null){
            return null;
        }
        return (Date)lastCall.clone();
    }

    public void setLastCall(Date lastCall) {
        if(lastCall != null)
        {
            this.lastCall = (Date)lastCall.clone();
        } else {
            this.lastCall = null;
        }
    }

    public RepairContactResultEnum getResult() {
        return result;
    }

    public void setResult(RepairContactResultEnum result) {
        this.result = result;
    }

    public Date getRecall() {
        if(recall == null) {
            return null;
        }
        return (Date)recall.clone();
    }

    public void setRecall(Date recall) {
        if(recall != null)
        {
            this.recall = (Date)recall.clone();
        } else {
            this.recall = null;
        }
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public MotivationEntity getMotivation() {
        return motivation;
    }

    public void setMotivation(MotivationEntity motivation) {
        this.motivation = motivation;
    }

    @Override
    public String toString() {
        return "LastContact{" +
                "lastCall=" + lastCall +
                ", result=" + result +
                ", recall=" + recall +
                ", note='" + note + '\'' +
                ", motivation=" + motivation +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        LastContact that = (LastContact) o;

        return new EqualsBuilder().append(lastCall, that.lastCall).append(result, that.result).append(recall, that.recall)
                .append(note, that.note).append(motivation, that.motivation).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(lastCall).append(result).append(recall)
                .append(note).append(motivation).toHashCode();
    }
}
