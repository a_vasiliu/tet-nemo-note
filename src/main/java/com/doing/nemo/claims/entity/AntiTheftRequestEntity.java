package com.doing.nemo.claims.entity;


import com.doing.nemo.claims.entity.jsonb.dataType.JsonDataAttachmentType;
import com.doing.nemo.claims.entity.jsonb.dataType.JsonDataCaiType;
import com.doing.nemo.claims.entity.jsonb.forms.jsonbForms.Attachment;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name = "anti_theft_request")
@TypeDefs({
        @TypeDef(name = "JsonDataAttachmentType", typeClass = JsonDataAttachmentType.class)
})
public class AntiTheftRequestEntity implements Serializable {

    @Id
    @Column(name="id")
    private String id;

    @Column(name="created_at")
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    private Date createdAt;

    @Column(name="updated_at")
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    private Date updatedAt;

    @Column(name="practice_id")
    private String practiceId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "claim_id")
    @JsonBackReference(value = "anti-theft-request-entities")
    private ClaimsEntity claim;

    @Column(name="id_transaction")
    private String idTransaction;

    @Column(name="provider_type")
    private String providerType;

    @Column(name="response_type")
    private String responseType;

    @Column(name="outcome")
    private String outcome;

    @Column(name="sub_report")
    private Integer subReport;

    @Column(name="g_power")
    private Double gPower;

    @Column(name="crash_number")
    private Integer crashNumber;

    @Column(name="anomaly")
    private Boolean anomaly;

    @Column(name="pdf")
    private String pdf;

    @Column(name = "crash_report")
    @Type(type = "JsonDataAttachmentType")
    private Attachment crashReport;

    @Column(name="response_message")
    private String responseMessage;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getPracticeId() {
        return practiceId;
    }

    public void setPracticeId(String practiceId) {
        this.practiceId = practiceId;
    }

    public ClaimsEntity getClaim() {
        return claim;
    }

    public void setClaim(ClaimsEntity claim) {
        this.claim = claim;
    }

    public String getIdTransaction() {
        return idTransaction;
    }

    public void setIdTransaction(String idTransaction) {
        this.idTransaction = idTransaction;
    }

    public String getProviderType() {
        return providerType;
    }

    public void setProviderType(String providerType) {
        this.providerType = providerType;
    }

    public String getResponseType() {
        return responseType;
    }

    public void setResponseType(String responseType) {
        this.responseType = responseType;
    }

    public String getOutcome() {
        return outcome;
    }

    public void setOutcome(String outcome) {
        this.outcome = outcome;
    }

    public Integer getSubReport() {
        return subReport;
    }

    public void setSubReport(Integer subReport) {
        this.subReport = subReport;
    }

    public Double getgPower() {
        return gPower;
    }

    public void setgPower(Double gPower) {
        this.gPower = gPower;
    }

    public Integer getCrashNumber() {
        return crashNumber;
    }

    public void setCrashNumber(Integer crashNumber) {
        this.crashNumber = crashNumber;
    }

    public Boolean getAnomaly() {
        return anomaly;
    }

    public void setAnomaly(Boolean anomaly) {
        this.anomaly = anomaly;
    }

    public String getPdf() {
        return pdf;
    }

    public void setPdf(String pdf) {
        this.pdf = pdf;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public Attachment getCrashReport() {
        return crashReport;
    }

    public void setCrashReport(Attachment crashReport) {
        this.crashReport = crashReport;
    }

    @Override
    public String toString() {
        return "AntiTheftRequestEntity{" +
                "id='" + id + '\'' +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                ", practiceId='" + practiceId + '\'' +
                ", claim='no info logged'" +
                ", idTransaction='" + idTransaction + '\'' +
                ", providerType='" + providerType + '\'' +
                ", responseType='" + responseType + '\'' +
                ", outcome='" + outcome + '\'' +
                ", subReport=" + subReport +
                ", gPower=" + gPower +
                ", crashNumber=" + crashNumber +
                ", anomaly=" + anomaly +
                ", pdf='no info logged'" +
                ", crashReport=" + crashReport +
                ", responseMessage='" + responseMessage + '\'' +
                '}';
    }
}
