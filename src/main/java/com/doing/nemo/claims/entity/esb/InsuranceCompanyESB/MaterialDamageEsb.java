package com.doing.nemo.claims.entity.esb.InsuranceCompanyESB;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MaterialDamageEsb implements Serializable {
    @JsonProperty("service_id")
    private String serviceId;
    @JsonProperty("service")
    private String service;
    @JsonProperty("deductible_id")
    private String deductibleId;
    @JsonProperty("deductible")
    private String deductible;

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getDeductibleId() {
        return deductibleId;
    }

    public void setDeductibleId(String deductibleId) {
        this.deductibleId = deductibleId;
    }

    public String getDeductible() {
        return deductible;
    }

    public void setDeductible(String deductible) {
        this.deductible = deductible;
    }
}
