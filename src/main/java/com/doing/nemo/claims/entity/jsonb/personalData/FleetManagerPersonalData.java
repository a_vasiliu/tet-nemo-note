package com.doing.nemo.claims.entity.jsonb.personalData;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FleetManagerPersonalData implements Serializable {

    private static final long serialVersionUID = 809173064960583663L;
    @JsonProperty("fleet_manager_id")
    private Long fleetManagerId;

    @JsonProperty("fleet_manager_name")
    private String fleetManagerName;

    @JsonProperty("fleet_manager_phone")
    private String fleetManagerPhone;

    @JsonProperty("fleet_manager_mobile_phone")
    private String fleetManagerMobilePhone;

    @JsonProperty("fleet_manager_primary_email")
    private String fleetManagerPrimaryEmail;

    @JsonProperty("fleet_manager_secondary_email")
    private String fleetManagerSecondaryEmail;

    @JsonProperty("is_imported")
    private Boolean isImported = false;

    @JsonProperty("disable_notification")
    private Boolean disableNotification;

    public FleetManagerPersonalData() {
    }

    public FleetManagerPersonalData(Long fleetManagerId) {
        this.fleetManagerId = fleetManagerId;
    }

    public FleetManagerPersonalData(Long fleetManagerId, String fleetManagerName, String fleetManagerPhone, String fleetManagerMobilePhone, String fleetManagerPrimaryEmail, String fleetManagerSecondaryEmail) {
        this.fleetManagerId = fleetManagerId;
        this.fleetManagerName = fleetManagerName;
        this.fleetManagerPhone = fleetManagerPhone;
        this.fleetManagerMobilePhone = fleetManagerMobilePhone;
        this.fleetManagerPrimaryEmail = fleetManagerPrimaryEmail;
        this.fleetManagerSecondaryEmail = fleetManagerSecondaryEmail;
    }

    public Long getFleetManagerId() {
        return fleetManagerId;
    }

    public void setFleetManagerId(Long fleetManagerId) {
        this.fleetManagerId = fleetManagerId;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getFleetManagerName() {
        return fleetManagerName;
    }

    public void setFleetManagerName(String fleetManagerName) {
        this.fleetManagerName = fleetManagerName;
    }

    public String getFleetManagerPhone() {
        return fleetManagerPhone;
    }

    public void setFleetManagerPhone(String fleetManagerPhone) {
        this.fleetManagerPhone = fleetManagerPhone;
    }

    public String getFleetManagerMobilePhone() {
        return fleetManagerMobilePhone;
    }

    public void setFleetManagerMobilePhone(String fleetManagerMobilePhone) {
        this.fleetManagerMobilePhone = fleetManagerMobilePhone;
    }

    public String getFleetManagerPrimaryEmail() {
        return fleetManagerPrimaryEmail;
    }

    public void setFleetManagerPrimaryEmail(String fleetManagerPrimaryEmail) {
        this.fleetManagerPrimaryEmail = fleetManagerPrimaryEmail;
    }

    public String getFleetManagerSecondaryEmail() {
        return fleetManagerSecondaryEmail;
    }

    public void setFleetManagerSecondaryEmail(String fleetManagerSecondaryEmail) {
        this.fleetManagerSecondaryEmail = fleetManagerSecondaryEmail;
    }

    public Boolean getIsImported(){
        return isImported;
    }

    public void setDisableNotification(Boolean disableNotification){
        if(disableNotification == null){
            this.disableNotification = false;
        }else{
            this.disableNotification = disableNotification;
        }
    }

    public Boolean isDisableNotification(){
        if(this.disableNotification == null){
            return false;
        }else{
            return this.disableNotification;
        }
    }

    public void setIsImported(Boolean imported) {
        isImported = imported;
    }

    @Override
    public String toString() {
        return "FleetManagerPersonalData{" +
                "fleetManagerId='" + fleetManagerId + '\'' +
                "fleetManagerName='" + fleetManagerName + '\'' +
                ", fleetManagerPhone='" + fleetManagerPhone + '\'' +
                ", fleetManagerMobilePhone='" + fleetManagerMobilePhone + '\'' +
                ", fleetManagerPrimaryEmail='" + fleetManagerPrimaryEmail + '\'' +
                ", fleetManagerSecondaryEmail='" + fleetManagerSecondaryEmail + '\'' +
                '}';
    }

    public boolean equalsFleetManager(Object o) {
        if (this == o) return true;
        if(o == null) return false;
        if (o.getClass() != this.getClass()) return false;
        FleetManagerPersonalData that = (FleetManagerPersonalData) o;
        return Objects.equals(getFleetManagerId(), that.getFleetManagerId());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        FleetManagerPersonalData that = (FleetManagerPersonalData) o;

        return new EqualsBuilder().append(fleetManagerId, that.fleetManagerId).append(fleetManagerName, that.fleetManagerName)
                .append(fleetManagerPhone, that.fleetManagerPhone).append(fleetManagerMobilePhone, that.fleetManagerMobilePhone)
                .append(fleetManagerPrimaryEmail, that.fleetManagerPrimaryEmail).append(fleetManagerSecondaryEmail, that.fleetManagerSecondaryEmail)
                .append(isImported, that.isImported).append(disableNotification, that.disableNotification).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(fleetManagerId).append(fleetManagerName)
                .append(fleetManagerPhone).append(fleetManagerMobilePhone).append(fleetManagerPrimaryEmail).append(fleetManagerSecondaryEmail)
                .append(isImported).append(disableNotification).toHashCode();
    }
}
