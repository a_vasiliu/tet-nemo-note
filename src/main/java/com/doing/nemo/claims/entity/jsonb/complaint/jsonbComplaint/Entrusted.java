package com.doing.nemo.claims.entity.jsonb.complaint.jsonbComplaint;

import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.EntrustedEnum.EntrustedEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Entrusted implements Serializable {

    private static final long serialVersionUID = 3966064765913915901L;

    @JsonProperty("auto_entrust")
    private Boolean autoEntrust;

    @JsonProperty("id_entrusted_to")
    private UUID idEntrustedTo;

    @JsonProperty("entrusted_to")
    private String entrustedTo;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @JsonProperty("entrusted_day")
    private Date entrustedDay;

    @JsonProperty("type")
    private EntrustedEnum entrustedType;

    @JsonProperty("description")
    private String description;

    @JsonProperty("entrusted_email")
    private String entrustedEmail;

    @JsonProperty("perito")
    private Boolean perito;

    @JsonProperty("number_sx_counterparty")
    private String numberSxCounterparty;

    @JsonProperty("dwl_man")
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    private Date dwlMan;

    @JsonProperty("status")
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNumberSxCounterparty() {
        return numberSxCounterparty;
    }

    public void setNumberSxCounterparty(String numberSxCounterparty) {
        this.numberSxCounterparty = numberSxCounterparty;
    }

    public Date getDwlMan() {
        if(dwlMan == null){
            return null;
        }
        return (Date)dwlMan.clone();
    }

    public void setDwlMan(Date dwlMan) {
        if(dwlMan != null)
        {
            this.dwlMan = (Date)dwlMan.clone();
        } else {
            this.dwlMan = null;
        }
    }

    public UUID getIdEntrustedTo() {
        return idEntrustedTo;
    }

    public void setIdEntrustedTo(UUID idEntrustedTo) {
        this.idEntrustedTo = idEntrustedTo;
    }

    public Boolean getAutoEntrust() {
        return autoEntrust;
    }

    public void setAutoEntrust(Boolean autoEntrust) {
        this.autoEntrust = autoEntrust;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getEntrustedTo() {
        return entrustedTo;
    }

    public void setEntrustedTo(String entrustedTo) {
        this.entrustedTo = entrustedTo;
    }

    public Date getEntrustedDay() {
        if(entrustedDay == null){
            return null;
        }
        return (Date)entrustedDay.clone();
    }

    public void setEntrustedDay(Date entrustedDay) {
        if(entrustedDay != null)
        {
            this.entrustedDay = (Date)entrustedDay.clone();
        } else {
            this.entrustedDay = null;
        }
    }

    public EntrustedEnum getEntrustedType() {
        return entrustedType;
    }

    public void setEntrustedType(EntrustedEnum entrustedType) {
        this.entrustedType = entrustedType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getPerito() {
        return perito;
    }

    public void setPerito(Boolean perito) {
        this.perito = perito;
    }

    public String getEntrustedEmail() {
        return entrustedEmail;
    }

    public void setEntrustedEmail(String entrustedEmail) {
        this.entrustedEmail = entrustedEmail;
    }

    @Override
    public String toString() {
        return "Entrusted{" +
                "autoEntrust=" + autoEntrust +
                ", entrustedTo='" + entrustedTo + '\'' +
                ", entrustedDay=" + entrustedDay +
                ", entrustedType=" + entrustedType +
                ", description='" + description + '\'' +
                ", entrustedEmail='" + entrustedEmail + '\'' +
                ", perito=" + perito +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Entrusted entrusted = (Entrusted) o;

        return new EqualsBuilder().append(autoEntrust, entrusted.autoEntrust).append(idEntrustedTo, entrusted.idEntrustedTo)
                .append(entrustedTo, entrusted.entrustedTo).append(entrustedDay, entrusted.entrustedDay).append(entrustedType, entrusted.entrustedType)
                .append(description, entrusted.description).append(entrustedEmail, entrusted.entrustedEmail).append(perito, entrusted.perito)
                .append(numberSxCounterparty, entrusted.numberSxCounterparty).append(dwlMan, entrusted.dwlMan)
                .append(status, entrusted.status).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(autoEntrust)
                .append(idEntrustedTo).append(entrustedTo).append(entrustedDay).append(entrustedType)
                .append(description).append(entrustedEmail).append(perito).append(numberSxCounterparty)
                .append(dwlMan).append(status).toHashCode();
    }
}
