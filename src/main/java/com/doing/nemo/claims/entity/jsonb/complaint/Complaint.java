package com.doing.nemo.claims.entity.jsonb.complaint;


import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.ComplaintModEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.ComplaintPropertyEnum;
import com.doing.nemo.claims.entity.jsonb.complaint.jsonbComplaint.Entrusted;
import com.doing.nemo.claims.entity.jsonb.complaint.jsonbComplaint.FromCompany;
import com.doing.nemo.claims.entity.jsonb.complaint.jsonbComplaint.Repair;
import com.doing.nemo.claims.entity.jsonb.complaint.jsonbComplaint.jsonbDataAccidents.DataAccident;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.Type;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Complaint implements Serializable {

    private static final long serialVersionUID = -461823480143137301L;

    @JsonProperty("client_id")
    private String clientId;

    @JsonProperty("plate")
    private String plate;

    @JsonProperty("locator")
    private String locator;

    @JsonProperty("activation")
    private String activation;

    @JsonProperty("property")
    private ComplaintPropertyEnum property;

    @JsonProperty("mod")
    private ComplaintModEnum mod;

    @JsonProperty("notification")
    private String notification;

    @JsonProperty("quote")
    private Boolean quote;

    @JsonProperty("data_accident")
    @Type(type = "jsonb")
    private DataAccident dataAccident;

    @JsonProperty("from_company")
    @Type(type = "jsonb")
    private FromCompany fromCompany;

    @Type(type = "jsonb")
    private Repair repair;

    @Type(type = "jsonb")
    private Entrusted entrusted;

    public Complaint() {
    }

    public Complaint(String clientId, String plate, String locator, String activation, ComplaintPropertyEnum property,
                     ComplaintModEnum mod, String notification, Boolean quote,
                     DataAccident dataAccident, FromCompany fromCompany, Repair repair, Entrusted entrusted) {

        this.clientId = clientId;
        this.plate = plate;
        this.locator = locator;
        this.activation = activation;
        this.property = property;
        this.mod = mod;
        this.notification = notification;
        this.quote = quote;
        this.dataAccident = dataAccident;
        this.fromCompany = fromCompany;
        this.repair = repair;
        this.entrusted = entrusted;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public String getLocator() {
        return locator;
    }

    public void setLocator(String locator) {
        this.locator = locator;
    }

    public String getActivation() {
        return activation;
    }

    public void setActivation(String activation) {
        this.activation = activation;
    }

    public ComplaintPropertyEnum getProperty() {
        return property;
    }

    public void setProperty(ComplaintPropertyEnum property) {
        this.property = property;
    }

    public ComplaintModEnum getMod() {
        return mod;
    }

    public void setMod(ComplaintModEnum mod) {
        this.mod = mod;
    }

    public String getNotification() {
        return notification;
    }

    public void setNotification(String notification) {
        this.notification = notification;
    }

    public Boolean getQuote() {
        return quote;
    }

    public void setQuote(Boolean quote) {
        this.quote = quote;
    }

    public DataAccident getDataAccident() {
        return dataAccident;
    }

    public void setDataAccident(DataAccident dataAccident) {
        this.dataAccident = dataAccident;
    }

    public FromCompany getFromCompany() {
        return fromCompany;
    }

    public void setFromCompany(FromCompany fromCompany) {
        this.fromCompany = fromCompany;
    }

    public Repair getRepair() {
        return repair;
    }

    public void setRepair(Repair repair) {
        this.repair = repair;
    }

    public Entrusted getEntrusted() {
        return entrusted;
    }

    public void setEntrusted(Entrusted entrusted) {
        this.entrusted = entrusted;
    }

    @Override
    public String toString() {
        return "complaint{" +
                "client_id='" + clientId + '\'' +
                "plate='" + plate + '\'' +
                ", locator='" + locator + '\'' +
                ", activation='" + activation + '\'' +
                ", property=" + property +
                ", mod=" + mod +
                ", notification=" + notification +
                ", quote=" + quote +
                ", dataAccident=" + dataAccident +
                ", fromCompany=" + fromCompany +
                ", repair=" + repair +
                ", entrusted=" + entrusted +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Complaint complaint = (Complaint) o;

        return new EqualsBuilder().append(clientId, complaint.clientId).append(plate, complaint.plate)
                .append(locator, complaint.locator).append(activation, complaint.activation)
                .append(property, complaint.property).append(mod, complaint.mod).append(notification, complaint.notification)
                .append(quote, complaint.quote).append(dataAccident, complaint.dataAccident).append(fromCompany, complaint.fromCompany)
                .append(repair, complaint.repair).append(entrusted, complaint.entrusted).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(clientId).append(plate)
                .append(locator).append(activation).append(property).append(mod).append(notification)
                .append(quote).append(dataAccident).append(fromCompany).append(repair).append(entrusted).toHashCode();
    }
}
