package com.doing.nemo.claims.entity.settings;

import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "exporting_file")
public class ExportingFile implements Serializable {

    @Id
    @Type(type = "org.hibernate.type.PostgresUUIDType")
    private UUID id;

    @Column(name = "attachment_id")
    private UUID attachmentId;

    @Column(name = "file_name")
    private String fileName;

    @Column(name = "file_size")
    private Integer fileSize;

    public ExportingFile() {
    }

    public ExportingFile(UUID id, UUID attachmentId, String fileName, Integer fileSize) {
        this.id = id;
        this.attachmentId = attachmentId;
        this.fileName = fileName;
        this.fileSize = fileSize;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getAttachmentId() {
        return attachmentId;
    }

    public void setAttachmentId(UUID attachmentId) {
        this.attachmentId = attachmentId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Integer getFileSize() {
        return fileSize;
    }

    public void setFileSize(Integer fileSize) {
        this.fileSize = fileSize;
    }

    @Override
    public String toString() {
        return "ExportingFile{" +
                "id=" + id +
                ", attachmentId=" + attachmentId +
                ", fileName='" + fileName + '\'' +
                ", fileSize=" + fileSize +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ExportingFile that = (ExportingFile) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getAttachmentId(), that.getAttachmentId()) &&
                Objects.equals(getFileName(), that.getFileName()) &&
                Objects.equals(getFileSize(), that.getFileSize());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getAttachmentId(), getFileName(), getFileSize());
    }
}
