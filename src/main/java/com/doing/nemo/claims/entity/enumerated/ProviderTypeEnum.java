package com.doing.nemo.claims.entity.enumerated;

import com.doing.nemo.commons.exception.BadParametersException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.io.Serializable;

public enum ProviderTypeEnum implements Serializable {
    OCTO("octo"),
    TEXA("texa");

    private String eventType;

    private static Logger LOGGER = LoggerFactory.getLogger(ProviderTypeEnum.class);

    private ProviderTypeEnum(String eventType) {
        this.eventType = eventType;
    }

    @JsonCreator
    public static ProviderTypeEnum create(String eventType) {

        eventType = eventType.replace(" - ", "_");
        eventType = eventType.replace('-', '_');
        eventType = eventType.replace(' ', '_');

        if (eventType != null) {
            for (ProviderTypeEnum val : ProviderTypeEnum.values()) {
                if (eventType.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        LOGGER.debug("Bad parameters exception. Enum class " + ProviderTypeEnum.class.getSimpleName() + " doesn't accept this value: " + eventType);
        throw new BadParametersException("Bad parameters exception. Enum class " + ProviderTypeEnum.class.getSimpleName() + " doesn't accept this value: " + eventType);
    }

    public String getValue() {
        return this.eventType.toUpperCase();
    }

    @JsonValue
    public String toValue() {
        for (ProviderTypeEnum val : ProviderTypeEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }
}
