package com.doing.nemo.claims.entity.esb.InsuranceCompanyESB;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LegalCostEsb implements Serializable {
    @JsonProperty("service_id")
    private String serviceId;
    @JsonProperty("service")
    private String service;
    @JsonProperty("company_id")
    private String companyId;
    @JsonProperty("company")
    private String company;
    @JsonProperty("tariff")
    private String tariff;
    @JsonProperty("tariff_id")
    private String tariffId;
    @JsonProperty("tariff_code")
    private String tariffCode;
    @JsonProperty("policy_number")
    private String policyNumber;
    private String startDate;
    private String endDate;

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }


    public String getTariff() {
        return tariff;
    }


    public String getTariffId() {
        return tariffId;
    }


    public void setTariffId(String tariffId) {
        this.tariffId = tariffId;
    }

    public void setTariff(String tariff) {
        this.tariff = tariff;
    }

    @JsonProperty("tariff_code")
    public String getTariffCode() {
        return tariffCode;
    }

    @JsonProperty("tariff_code")
    public void setTariffCode(String tariffCode) {
        this.tariffCode = tariffCode;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    @JsonProperty("start_date")
    public String getStartDate() {
        return startDate;
    }

    @JsonProperty("startdate")
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    @JsonProperty("end_date")
    public String getEndDate() {
        return endDate;
    }

    @JsonProperty("enddate")
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}
