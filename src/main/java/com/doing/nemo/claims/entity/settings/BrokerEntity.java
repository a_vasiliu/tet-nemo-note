package com.doing.nemo.claims.entity.settings;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "broker",
        uniqueConstraints = @UniqueConstraint(
                name = "uc_broker",
                columnNames = {
                        "business_name"
                }
        )
)
public class BrokerEntity implements Serializable {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Type(type = "org.hibernate.type.PostgresUUIDType")
    private UUID id;

    @Column(name = "business_name")
    private String businessName;

    @Column(name = "address")
    private String address;

    @Column(name = "zip_code")
    private String zipCode;

    @Column(name = "city")
    private String city;

    @Column(name = "province")
    private String province;

    @Column(name = "state")
    private String state;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "fax")
    private String fax;

    @Column(name = "email")
    private String email;

    @Column(name = "web_site")
    private String webSite;

    @Column(name = "contact")
    private String contact;

    @Column(name = "is_active")
    private Boolean isActive;

    public BrokerEntity() {

    }

    public BrokerEntity(String businessName, String address, String zipCode, String city, String province, String state, String phoneNumber, String fax, String email, String webSite, String contact, Boolean isActive) {
        this.businessName = businessName;
        this.address = address;
        this.zipCode = zipCode;
        this.city = city;
        this.province = province;
        this.state = state;
        this.phoneNumber = phoneNumber;
        this.fax = fax;
        this.email = email;
        this.webSite = webSite;
        this.contact = contact;
        this.isActive = isActive;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebSite() {
        return webSite;
    }

    public void setWebSite(String webSite) {
        this.webSite = webSite;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    @Override
    public String toString() {
        return "BrokerEntity{" +
                "id=" + id +
                ", businessName='" + businessName + '\'' +
                ", address='" + address + '\'' +
                ", zipCode=" + zipCode +
                ", city='" + city + '\'' +
                ", province='" + province + '\'' +
                ", state='" + state + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", fax='" + fax + '\'' +
                ", email='" + email + '\'' +
                ", webSite='" + webSite + '\'' +
                ", contact='" + contact + '\'' +
                ", isActive=" + isActive +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BrokerEntity that = (BrokerEntity) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getBusinessName(), that.getBusinessName()) &&
                Objects.equals(getAddress(), that.getAddress()) &&
                Objects.equals(getZipCode(), that.getZipCode()) &&
                Objects.equals(getCity(), that.getCity()) &&
                Objects.equals(getProvince(), that.getProvince()) &&
                Objects.equals(getState(), that.getState()) &&
                Objects.equals(getPhoneNumber(), that.getPhoneNumber()) &&
                Objects.equals(getFax(), that.getFax()) &&
                Objects.equals(getEmail(), that.getEmail()) &&
                Objects.equals(getWebSite(), that.getWebSite()) &&
                Objects.equals(getContact(), that.getContact()) &&
                Objects.equals(getActive(), that.getActive());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getBusinessName(), getAddress(), getZipCode(), getCity(), getProvince(), getState(), getPhoneNumber(), getFax(), getEmail(), getWebSite(), getContact(), getActive());
    }
}
