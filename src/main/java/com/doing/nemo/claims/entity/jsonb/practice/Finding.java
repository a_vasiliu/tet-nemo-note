package com.doing.nemo.claims.entity.jsonb.practice;

import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.enumerated.EventTypeEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Finding implements Serializable {

    private static final long serialVersionUID = 809173064960583663L;

    @JsonProperty("finding_date")
    private Date findingDate;

    @JsonProperty("finding_id")
    private String findingId;

    @JsonProperty("sequestered")
    private Boolean sequestered;

    @JsonProperty("provider")
    private String provider;

    @JsonProperty("provider_code")
    private String providerCode;

    @JsonProperty("complaint_police")
    private Boolean complaintPolice;

    @JsonProperty("complaint_cc")
    private Boolean complaintCc;

    @JsonProperty("complaint_vvuu")
    private Boolean complaintVvuu;

    @JsonProperty("complaint_gdf")
    private Boolean complaintGdf;

    @JsonProperty("authority_data")
    private String authorityData;

    @JsonProperty("note")
    private String note;

    @JsonProperty("happened_abroad")
    private Boolean happenedAbroad;

    @JsonProperty("found_location")
    private String foundLocation;

    @JsonProperty("province")
    private String province;

    @JsonProperty("state")
    private String state;

    @JsonProperty("double_key")
    private Boolean doubleKey;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Date getFindingDate() {
        if(findingDate == null){
            return null;
        }
        return (Date)findingDate.clone();
    }

    public void setFindingDate(Date findingDate) {
        if(findingDate != null)
        {
            this.findingDate = (Date)findingDate.clone();
        } else {
            this.findingDate = null;
        }
    }

    public String getFindingId() {
        return findingId;
    }

    public void setFindingId(String findingId) {
        this.findingId = findingId;
    }

    public Boolean getSequestered() {
        return sequestered;
    }

    public void setSequestered(Boolean sequestered) {
        this.sequestered = sequestered;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getProviderCode() {
        return providerCode;
    }

    public void setProviderCode(String providerCode) {
        this.providerCode = providerCode;
    }

    public Boolean getComplaintPolice() {
        return complaintPolice;
    }

    public void setComplaintPolice(Boolean complaintPolice) {
        this.complaintPolice = complaintPolice;
    }

    public Boolean getComplaintCc() {
        return complaintCc;
    }

    public void setComplaintCc(Boolean complaintCc) {
        this.complaintCc = complaintCc;
    }

    public Boolean getComplaintVvuu() {
        return complaintVvuu;
    }

    public void setComplaintVvuu(Boolean complaintVvuu) {
        this.complaintVvuu = complaintVvuu;
    }

    public Boolean getComplaintGdf() {
        return complaintGdf;
    }

    public void setComplaintGdf(Boolean complaintGdf) {
        this.complaintGdf = complaintGdf;
    }

    public String getAuthorityData() {
        return authorityData;
    }

    public void setAuthorityData(String authorityData) {
        this.authorityData = authorityData;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Boolean getHappenedAbroad() {
        return happenedAbroad;
    }

    public void setHappenedAbroad(Boolean happenedAbroad) {
        this.happenedAbroad = happenedAbroad;
    }

    public String getFoundLocation() {
        return foundLocation;
    }

    public void setFoundLocation(String foundLocation) {
        this.foundLocation = foundLocation;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Boolean getDoubleKey() {
        return doubleKey;
    }

    public void setDoubleKey(Boolean doubleKey) {
        this.doubleKey = doubleKey;
    }

    @Override
    public String toString() {
        return "Finding{" +
                "findingDate=" + findingDate +
                ", findingId='" + findingId + '\'' +
                ", sequestered=" + sequestered +
                ", provider='" + provider + '\'' +
                ", providerCode='" + providerCode + '\'' +
                ", complaintPolice=" + complaintPolice +
                ", complaintCc=" + complaintCc +
                ", complaintVvuu=" + complaintVvuu +
                ", complaintGdf=" + complaintGdf +
                ", authorityData='" + authorityData + '\'' +
                ", note='" + note + '\'' +
                ", happenedAbroad=" + happenedAbroad +
                ", foundLocation='" + foundLocation + '\'' +
                ", province='" + province + '\'' +
                ", state='" + state + '\'' +
                ", doubleKey=" + doubleKey +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Finding finding = (Finding) o;

        return new EqualsBuilder().append(findingDate, finding.findingDate).append(findingId, finding.findingId)
                .append(sequestered, finding.sequestered).append(provider, finding.provider).append(providerCode, finding.providerCode)
                .append(complaintPolice, finding.complaintPolice).append(complaintCc, finding.complaintCc)
                .append(complaintVvuu, finding.complaintVvuu).append(complaintGdf, finding.complaintGdf).append(authorityData, finding.authorityData)
                .append(note, finding.note).append(happenedAbroad, finding.happenedAbroad).append(foundLocation, finding.foundLocation)
                .append(province, finding.province).append(state, finding.state).append(doubleKey, finding.doubleKey).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(findingDate).append(findingId)
                .append(sequestered).append(provider).append(providerCode).append(complaintPolice).append(complaintCc)
                .append(complaintVvuu).append(complaintGdf).append(authorityData).append(note).append(happenedAbroad)
                .append(foundLocation).append(province).append(state).append(doubleKey).toHashCode();
    }
}
