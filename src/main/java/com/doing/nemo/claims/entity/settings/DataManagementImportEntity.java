package com.doing.nemo.claims.entity.settings;

import com.doing.nemo.claims.entity.enumerated.FileImportTypeEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "data_management_import")
public class DataManagementImportEntity implements Serializable {

    @Id
    @Type(type = "org.hibernate.type.PostgresUUIDType")
    private UUID id;

    @Column(name = "user_import")
    private String user;

    @Column(name = "description")
    private String description;

    @Enumerated(EnumType.STRING)
    @Column(name = "file_type")
    private FileImportTypeEnum fileType;

    @Column(name = "result")
    private String result;

    @Column(name = "date")
    private Instant date;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "data_management_import_id", referencedColumnName = "id")
    private List<ProcessingFile> processingFiles = new LinkedList<>();

    public DataManagementImportEntity() {
    }

    public DataManagementImportEntity(UUID id, String user, String description, FileImportTypeEnum fileType, String result, Instant date, List<ProcessingFile> processingFiles) {
        this.id = id;
        this.user = user;
        this.description = description;
        this.fileType = fileType;
        this.result = result;
        this.date = date;
        this.processingFiles = processingFiles;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Instant getDate() {
        return date;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    public FileImportTypeEnum getFileType() {
        return fileType;
    }

    public void setFileType(FileImportTypeEnum fileType) {
        this.fileType = fileType;
    }

    @JsonIgnore
    public List<ProcessingFile> getProcessingFiles() {
        return processingFiles;
    }

    public void setProcessingFiles(List<ProcessingFile> processingFiles) {
        this.processingFiles = processingFiles;
    }

    @Override
    public String toString() {
        return "DataManagementImportEntity{" +
                "id=" + id +
                ", user='" + user + '\'' +
                ", description='" + description + '\'' +
                ", fileType=" + fileType +
                ", result='" + result + '\'' +
                ", date=" + date +
                ", processingFiles=" + processingFiles +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DataManagementImportEntity that = (DataManagementImportEntity) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getUser(), that.getUser()) &&
                Objects.equals(getDescription(), that.getDescription()) &&
                getFileType() == that.getFileType() &&
                Objects.equals(getResult(), that.getResult()) &&
                Objects.equals(getDate(), that.getDate()) &&
                Objects.equals(getProcessingFiles(), that.getProcessingFiles());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getUser(), getDescription(), getFileType(), getResult(), getDate(), getProcessingFiles());
    }
}
