package com.doing.nemo.claims.entity;

import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsFlowEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.enumerated.DamagedEnum.VehicleEnum.VehicleNatureEnum;
import com.doing.nemo.claims.entity.enumerated.DamagedEnum.WoundedEnum.WoundedTypeEnum;
import com.doing.nemo.claims.entity.enumerated.RepairEnum.RepairStatusEnum;
import com.doing.nemo.claims.entity.jsonb.*;
import com.doing.nemo.claims.entity.jsonb.cai.Cai;
import com.doing.nemo.claims.entity.jsonb.complaint.Complaint;
import com.doing.nemo.claims.entity.jsonb.damaged.Damaged;
import com.doing.nemo.claims.entity.jsonb.dataType.*;
import com.doing.nemo.claims.entity.jsonb.forms.Forms;
import com.doing.nemo.claims.entity.jsonb.foundModel.FoundModel;
import com.doing.nemo.claims.entity.jsonb.refund.Refund;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.*;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.*;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "hibernate_lazy_initializer", "handler"})
@DynamicInsert
@Table(name = "claims")
@TypeDefs({
        @TypeDef(name = "JsonDataCaiType", typeClass = JsonDataCaiType.class),
        @TypeDef(name = "JsonDataComplaintType", typeClass = JsonDataComplaintType.class),
        // @TypeDef(name = "JsonDataCounterpartyType", typeClass = JsonDataCounterpartyType.class),
        @TypeDef(name = "JsonDataAuthorityType", typeClass = JsonDataAuthorityType.class),
        @TypeDef(name = "JsonDataDamagedType", typeClass = JsonDataDamagedType.class),
        @TypeDef(name = "JsonDataDeponentType", typeClass = JsonDataDeponentType.class),
        @TypeDef(name = "JsonDataFormsType", typeClass = JsonDataFormsType.class),
        @TypeDef(name = "JsonDataFoundModelType", typeClass = JsonDataFoundModelType.class),
        @TypeDef(name = "JsonDataHistoricalType", typeClass = JsonDataHistoricalType.class),
        @TypeDef(name = "JsonDataNotesType", typeClass = JsonDataNotesType.class),
        @TypeDef(name = "JsonDataWoundedType", typeClass = JsonDataWoundedType.class),
        @TypeDef(name = "JsonDataExemptionType", typeClass = JsonDataExemptionType.class),
        @TypeDef(name = "JsonDataWoundedType", typeClass = JsonDataWoundedType.class),
        @TypeDef(name = "JsonDataRefundType", typeClass = JsonDataRefundType.class),
        @TypeDef(name = "JsonDataMetadataClaimType", typeClass = JsonDataMetadataClaimType.class),
        @TypeDef(name = "JsonDataTheftType", typeClass = JsonDataTheftType.class)

})
@SqlResultSetMapping(name="authority", classes = {
        @ConstructorResult(targetClass = Authority.class,
                columns = {
                        @ColumnResult(name="authority_dossier_id"),
                        @ColumnResult(name="authority_working_id"),
                        @ColumnResult(name="working_number"),
                        @ColumnResult(name="authority_dossier_number"),
                        @ColumnResult(name="total", type = Double.class),
                        @ColumnResult(name="accepting_date"),
                        @ColumnResult(name="authorization_date"),
                        @ColumnResult(name="rejection_date"),
                        @ColumnResult(name="rejection", type = Boolean.class),
                        @ColumnResult(name="working_created_at"),
                        @ColumnResult(name="franchise_number", type = Long.class),
                        @ColumnResult(name="is_wreck", type = Boolean.class),
                        @ColumnResult(name="wreck_casual"),
                        @ColumnResult(name="event_date"),
                        @ColumnResult(name="event_type"),
                        @ColumnResult(name="po_details")
                })
})
public class ClaimsEntity implements Serializable {

    @Id
    @JsonProperty("id")
    private String id;

    @Column(name = "created_at")
    @JsonProperty("created_at")
    private Instant createdAt;

    @Column(name = "update_at")
    @JsonProperty("update_at")
    private Instant updateAt;

    @Column(name = "converted")
    private Boolean converted;

    @Column(name = "practice_id", unique = true, nullable = false , updatable = false)
    //@Generated(GenerationTime.INSERT)
    @JsonProperty("practice_id")
    private Long practiceId;

    @Column(name = "practice_manager")
    @JsonProperty("practice_manager")
    private String practiceManager;

    @Column(name = "pai_comunication")
    @JsonProperty("pai_comunication")
    private Boolean paiComunication;

    @Column(name = "forced")
    private Boolean forced;

    @Column(name = "in_evidence")
    @JsonProperty("in_evidence")
    private Boolean inEvidence;

    @Column(name = "user_entity")
    private String userId;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private ClaimsStatusEnum status;

    @Column(name = "motivation")
    private String motivation;

    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private ClaimsFlowEnum type;

    @Column(name = "documentation")
    private Boolean isCompleteDocumentation;

    @Type(type = "JsonDataComplaintType")
    @Column(name = "complaint")
    private Complaint complaint;

    @Type(type = "JsonDataFormsType")
    @Column(name = "forms")
    private Forms forms;

    @Type(type = "JsonDataDamagedType")
    @Column(name = "damaged")
    private Damaged damaged;

    @Type(type = "JsonDataCaiType")
    @Column(name = "cai_details")
    @JsonProperty("cai_details")
    private Cai caiDetails;

    @JsonProperty("is_with_counterparty")
    @Column(name = "is_with_counterparty")
    private Boolean isWithCounterparty;

    @OneToMany(
            cascade = CascadeType.ALL/*,
            orphanRemoval = true*/
    )
    @JoinColumn(name = "claims_id")
    @Column(name = "counterparty")
    @JsonManagedReference(value = "counterparty-entities")
    private List<CounterpartyEntity> counterparts;

    @Type(type = "JsonDataDeponentType")
    @JsonProperty("deponent")
    private List<Deponent> deponentList;

    @Type(type = "JsonDataWoundedType")
    @JsonProperty("wounded")
    private List<Wounded> woundedList;

    @Type(type = "JsonDataNotesType")
    @Column(name = "notes")
    private List<Notes> notes;

    @Type(type = "JsonDataHistoricalType")
    @Column(name = "historical")
    private List<Historical> historical;

    @Type(type = "JsonDataFoundModelType")
    @Column(name = "found_model")
    @JsonProperty("found_model")
    private FoundModel foundModel;

    @Type(type = "JsonDataExemptionType")
    @Column(name = "exemption")
    private Exemption exemption;

    @Column(name = "id_saleforce")
    private Long idSaleforce;

    @Column(name = "with_continuation")
    @JsonProperty("with_continuation")
    private Boolean withContinuation;

    @Type(type = "JsonDataRefundType")
    @Column(name = "refund")
    private Refund refund;

    @Type(type = "JsonDataMetadataClaimType")
    @Column(name="metadata")
    private MetadataClaim metadata;

    @Type(type = "JsonDataTheftType")
    @Column(name="theft")
    private Theft theft;

    @Column(name="is_read_msa")
    @JsonProperty("is_read_msa")
    private Boolean isRead;

    @Type(type = "JsonDataAuthorityType")
    @Column(name = "authority")
    @JsonProperty("authority")
    private List<Authority> authorities;

    @JsonProperty("is_authority_linkable")
    @Column(name = "is_authority_linkable")
    private Boolean isAuthorityLinkable;

    @Column(name = "po_variation")
    @JsonProperty("po_variation")
    private Boolean poVariation;

    @Column(name = "legal_comunication")
    @JsonProperty("legal_comunication")
    private Boolean legalComunication;

    @Column(name = "total_penalty")
    @JsonProperty("total_penalty")
    private Long totalPenalty;

    @Column(name="is_read_acclaims")
    @JsonProperty("is_read_acclaims")
    private Boolean isReadAcclaims;

    @Column(name = "is_migrated")
    @JsonProperty("is_migrated")
    private Boolean isMigrated;


    @OneToMany(
            cascade = CascadeType.ALL/*,
            orphanRemoval = true*/
    )
    @JoinColumn(name = "claim_id")
    @Column(name = "anti_theft_request")
    @JsonManagedReference(value = "anti-theft-request-entities")
    private List<AntiTheftRequestEntity> antiTheftRequestEntities;

    @Transient
    private Boolean isPending;


    public ClaimsEntity() {
    }


    public Boolean getMigrated() {
        return isMigrated;
    }

    public Boolean isMigrated() {
        if(this.getMigrated()!=null)
            return this.getMigrated();
        return false;
    }

    public Boolean isCaiSigned() {
        return
            this.getDamaged() != null &&
            this.getDamaged().getCaiSigned() != null &&
            this.getDamaged().getCaiSigned()
        ;
    }

    public void setMigrated(Boolean migrated) {
        isMigrated = migrated;
    }

    public Boolean getReadAcclaims() {
        return isReadAcclaims;
    }

    public void setReadAcclaims(Boolean readAcclaims) {
        isReadAcclaims = readAcclaims;
    }

    public Long getTotalPenalty() {
        return totalPenalty;
    }

    public void setTotalPenalty(Long totalPenalty) {
        this.totalPenalty = totalPenalty;
    }

    public Boolean getLegalComunication() {
        return legalComunication;
    }

    public void setLegalComunication(Boolean legalComunication) {
        this.legalComunication = legalComunication;
    }

    public Boolean getAuthorityLinkable() {
        return isAuthorityLinkable;
    }

    public void setAuthorityLinkable(Boolean authorityLinkable) {
        isAuthorityLinkable = authorityLinkable;
    }

    public Theft getTheft() {
        return theft;
    }

    public void setTheft(Theft theft) {
        this.theft = theft;
    }

    public void setMetadata(MetadataClaim metadata) {
        this.metadata = metadata;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public Instant getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Instant updateAt) {
        this.updateAt = updateAt;
    }

    public ClaimsStatusEnum getStatus() {
        return status;
    }

    public void setStatus(ClaimsStatusEnum status) {
        this.status = status;
    }

    public ClaimsFlowEnum getType() {
        return type;
    }

    @JsonIgnore
    public Boolean getCompleteDocumentation() {
        return isCompleteDocumentation;
    }

    public void setCompleteDocumentation(Boolean completeDocumentation) {
        isCompleteDocumentation = completeDocumentation;
    }

    public void setType(ClaimsFlowEnum type) {
        this.type = type;
    }

    public Complaint getComplaint() {
        return complaint;
    }

    public void setComplaint(Complaint complaint) {
        this.complaint = complaint;
    }

    public Forms getForms() {
        return forms;
    }

    public void setForms(Forms forms) {
        this.forms = forms;
    }

    public Damaged getDamaged() {
        return damaged;
    }

    public void setDamaged(Damaged damaged) {
        this.damaged = damaged;
    }

    public Cai getCaiDetails() {
        return caiDetails;
    }

    public void setCaiDetails(Cai caiDetails) {
        this.caiDetails = caiDetails;
    }

    public List<CounterpartyEntity> getCounterparts() {
        return counterparts;
    }

    public void setCounterparts(List<CounterpartyEntity> counterparts) {
        if (counterparts != null && counterparts.size() > 0) {
            for (CounterpartyEntity currentCunterparty : counterparts) {
                if (currentCunterparty.getCounterpartyId() == null) {

                    currentCunterparty.setCounterpartyId(UUID.randomUUID().toString());
                    //DA IMPOSTARE LO STATO DEL REPAIR IN BASE A QUELLE REGOLE DI ELEGIBILITA'
                    currentCunterparty.setRepairStatus(RepairStatusEnum.TO_SORT);

                }
            }
            this.isWithCounterparty = true;
        } else {
            this.isWithCounterparty = false;
        }
        this.counterparts = counterparts;
    }

    @JsonIgnore
    public String getPracticeManager() {
        return practiceManager;
    }

    public void setPracticeManager(String practiceManager) {
        this.practiceManager = practiceManager;
    }

    @JsonIgnore
    public String getUserEntity() {
        return userId;
    }

    public void setUserEntity(String userId) {
        this.userId = userId;
    }

    public Long getPracticeId() {
        return practiceId;
    }

    public void setPracticeId(Long practiceId) {
        this.practiceId = practiceId;
    }

    public List<Deponent> getDeponentList() {
        if(deponentList == null){
            return null;
        }
        return new ArrayList<>(deponentList);
    }

    public void setDeponentList(List<Deponent> deponentList) {
        if(deponentList != null)
        {
            this.deponentList = new ArrayList<>(deponentList);
        } else {
            this.deponentList = null;
        }
    }

    public List<Wounded> getWoundedList() {
        if(woundedList == null){
            return null;
        }
        return new ArrayList<>(woundedList);
    }

    public void setWoundedList(List<Wounded> woundedList) {
        if(woundedList != null)
        {
            this.woundedList = new ArrayList<>(woundedList);
        } else {
            this.woundedList = null;
        }
    }

    public List<Notes> getNotes() {
        if(notes == null){
            return null;
        }
        return new ArrayList<>(notes);
    }

    public void setNotes(List<Notes> notes) {
        if(notes != null)
        {
            this.notes = new ArrayList<>(notes);
        } else {
            this.notes = null;
        }
    }

    public void addNotes(Notes note) {

        if (this.notes == null) {
            setNotes(new LinkedList<Notes>());
        }

        this.notes.add(note);

    }

    public List<Historical> getHistorical() {
        if(historical == null){
            return null;
        }
        return new ArrayList<>(historical);
    }

    public void setHistorical(List<Historical> historical) {
        if(historical != null)
        {
            this.historical = new ArrayList<>(historical);
        } else {
            this.historical = null;
        }
    }

    public void addHistorical(Historical historical) {
        if (this.historical == null) {
            setHistorical(new LinkedList<>());
        }
        this.historical.add(historical);
    }

    public Boolean getPaiComunication() {
        return paiComunication;
    }

    public void setPaiComunication(Boolean paiComunication) {
        if (paiComunication == null)
            paiComunication = false;
        this.paiComunication = paiComunication;
    }

    public Boolean getForced() {
        return forced;
    }

    public void setForced(Boolean forced) {
        if (forced == null)
            forced = false;
        this.forced = forced;
    }

    public Boolean getInEvidence() {
        return inEvidence;
    }

    public void setInEvidence(Boolean inEvidence) {
        if (inEvidence == null)
            inEvidence = false;
        this.inEvidence = inEvidence;
    }

    public FoundModel getFoundModel() {
        return foundModel;
    }

    public void setFoundModel(FoundModel foundModel) {
        this.foundModel = foundModel;
    }

    public Boolean getWithCounterparty() {
        return isWithCounterparty;
    }

    public void setWithCounterparty(Boolean withCounterparty) {
        if (withCounterparty == null)
            withCounterparty = false;
        isWithCounterparty = withCounterparty;
    }

    public Long getIdSaleforce() {
        return idSaleforce;
    }

    public void setIdSaleforce(Long idSaleforce) {
        this.idSaleforce = idSaleforce;
    }

    public Boolean getWithContinuation() {
        return withContinuation;
    }

    public Exemption getExemption() {
        return exemption;
    }

    public void setExemption(Exemption exemption) {
        this.exemption = exemption;
    }

    public void setWithContinuation(Boolean withContinuation) {
        this.withContinuation = withContinuation;
    }

    public Refund getRefund() {
        return refund;
    }

    public void setRefund(Refund refund) {
        this.refund = refund;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public MetadataClaim getMetadata() {
        return metadata;
    }

    public String getMotivation() {
        return motivation;
    }

    public void setMotivation(String motivation) {
        this.motivation = motivation;
    }

    public Boolean getRead() {
        return isRead;
    }

    public void setRead(Boolean read) {
        isRead = read;
    }

    public List<Authority> getAuthorities() {
        if(authorities == null){
            return null;
        }
        return new ArrayList<>(authorities);
    }

    public void setAuthorities(List<Authority> authorities) {
        if(authorities != null)
        {
            this.authorities = new ArrayList<>(authorities);
        } else {
            this.authorities = null;
        }
    }

    public Boolean isInvoiced() {
        List<Authority> authorities = this.getAuthorities();
        if( authorities != null && !authorities.isEmpty() ) {
            for(Authority authority : authorities) {
                if( authority.getInvoiced() ) {
                    return true;
                }
            }
        }
        return false;
    }

    public Boolean getPoVariation() {
        return poVariation;
    }

    public void setPoVariation(Boolean poVariation) {
        this.poVariation = poVariation;
    }

    public List<AntiTheftRequestEntity> getAntiTheftRequestEntities() {
        return antiTheftRequestEntities;
    }

    public void setAntiTheftRequestEntities(List<AntiTheftRequestEntity> antiTheftRequestEntities) {
        this.antiTheftRequestEntities = antiTheftRequestEntities;
    }

    public Boolean getConverted() {
        return converted;
    }

    public void setConverted(Boolean converted) {
        this.converted = converted;
    }

    public Boolean isIncomplete() {
        if(this.getStatus()!=null && this.getStatus().equals(ClaimsStatusEnum.INCOMPLETE)){
            return true;
        }
        return false;
    }

    public Boolean isInsertedFromMyAld(){
/*        if(this.getMetadata()!=null &&
                this.getMetadata().getMetadata()!=null &&
                this.getMetadata().getMetadata().containsKey("user_id") &&
                this.getMetadata().getMetadata().get("user_id")!=null){
            return true;
        }*/
        if(this.getComplaint()!=null &&
                this.getComplaint().getNotification() != null &&
                "myald".equalsIgnoreCase(this.getComplaint().getNotification())){
            return true;
        }
        return false;
    }

    public Boolean getPending() {
        return isPending;
    }

    public void setPending(Boolean pending) {
        isPending = pending;
    }

    @Override
    public String toString() {
        return "ClaimsEntity{" +
                "id='" + id + '\'' +
                ", createdAt=" + createdAt +
                ", updateAt=" + updateAt +
                ", practiceId=" + practiceId +
                ", practiceManager='" + practiceManager + '\'' +
                ", paiComunication=" + paiComunication +
                ", forced=" + forced +
                ", inEvidence=" + inEvidence +
                ", userId='" + userId + '\'' +
                ", status=" + status +
                ", motivation='" + motivation + '\'' +
                ", type=" + type +
                ", isCompleteDocumentation=" + isCompleteDocumentation +
                ", complaint=" + complaint +
                ", forms=" + forms +
                ", damaged=" + damaged +
                ", caiDetails=" + caiDetails +
                ", isWithCounterparty=" + isWithCounterparty +
                ", counterparty=no info logged" +
                ", deponentList=" + deponentList +
                ", woundedList=" + woundedList +
                ", notes=" + notes +
                ", historical=" + historical +
                ", foundModel=" + foundModel +
                ", exemption=" + exemption +
                ", idSaleforce=" + idSaleforce +
                ", withContinuation=" + withContinuation +
                ", refund=" + refund +
                ", metadata=" + metadata +
                ", theft=" + theft +
                ", isRead=" + isRead +
                ", authorities=" + authorities +
                ", isAuthorityLinkable=" + isAuthorityLinkable +
                ", poVariation=" + poVariation +
                ", antitheftrequest=" + antiTheftRequestEntities +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClaimsEntity that = (ClaimsEntity) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getCreatedAt(), that.getCreatedAt()) &&
                Objects.equals(getUpdateAt(), that.getUpdateAt()) &&
                Objects.equals(getPracticeId(), that.getPracticeId()) &&
                Objects.equals(getPracticeManager(), that.getPracticeManager()) &&
                Objects.equals(getPaiComunication(), that.getPaiComunication()) &&
                Objects.equals(getForced(), that.getForced()) &&
                Objects.equals(getInEvidence(), that.getInEvidence()) &&
                Objects.equals(getUserId(), that.getUserId()) &&
                getStatus() == that.getStatus() &&
                Objects.equals(getMotivation(), that.getMotivation()) &&
                getType() == that.getType() &&
                Objects.equals(getCompleteDocumentation(), that.getCompleteDocumentation()) &&
                Objects.equals(getComplaint(), that.getComplaint()) &&
                Objects.equals(getForms(), that.getForms()) &&
                Objects.equals(getDamaged(), that.getDamaged()) &&
                Objects.equals(getCaiDetails(), that.getCaiDetails()) &&
                Objects.equals(getWithCounterparty(), that.getWithCounterparty()) &&
                Objects.equals(getCounterparts(), that.getCounterparts()) &&
                Objects.equals(getDeponentList(), that.getDeponentList()) &&
                Objects.equals(getWoundedList(), that.getWoundedList()) &&
                Objects.equals(getNotes(), that.getNotes()) &&
                Objects.equals(getHistorical(), that.getHistorical()) &&
                Objects.equals(getFoundModel(), that.getFoundModel()) &&
                Objects.equals(getExemption(), that.getExemption()) &&
                Objects.equals(getIdSaleforce(), that.getIdSaleforce()) &&
                Objects.equals(getWithContinuation(), that.getWithContinuation()) &&
                Objects.equals(getRefund(), that.getRefund()) &&
                Objects.equals(getMetadata(), that.getMetadata()) &&
                Objects.equals(getTheft(), that.getTheft()) &&
                Objects.equals(getRead(), that.getRead()) &&
                Objects.equals(getAuthorities(), that.getAuthorities()) &&
                Objects.equals(getAuthorityLinkable(), that.getAuthorityLinkable()) &&
                Objects.equals(getPoVariation(), that.getPoVariation()) &&
                Objects.equals(getLegalComunication(), that.getLegalComunication()) &&
                Objects.equals(getTotalPenalty(), that.getTotalPenalty()) &&
                Objects.equals(getAntiTheftRequestEntities(), that.getAntiTheftRequestEntities()) &&
                Objects.equals(getPending(), that.getPending());

    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getCreatedAt(), getUpdateAt(), getPracticeId(), getPracticeManager(), getPaiComunication(), getForced(), getInEvidence(), getUserId(), getStatus(), getMotivation(), getType(), getCompleteDocumentation(), getComplaint(), getForms(), getDamaged(), getCaiDetails(), getWithCounterparty(), getCounterparts(), getDeponentList(), getWoundedList(), getNotes(), getHistorical(), getFoundModel(), getExemption(), getIdSaleforce(), getWithContinuation(), getRefund(), getMetadata(), getTheft(), getRead(), getAuthorities(), getAuthorityLinkable(), getPoVariation(), getLegalComunication(), getTotalPenalty(), getPending());
    }

    public Boolean isRCPassive() {
        return
            this.getComplaint() != null &&
            this.getComplaint().getDataAccident() != null &&
            this.getComplaint().getDataAccident().getTypeAccident() != null &&
            this.getComplaint().getDataAccident().getTypeAccident().equals(DataAccidentTypeAccidentEnum.RC_PASSIVA)
        ;
    }

    public Boolean isCardPassive() {
        return
            this.getComplaint() != null &&
            this.getComplaint().getDataAccident() != null &&
            this.getComplaint().getDataAccident().getTypeAccident() != null &&
            this.getComplaint().getDataAccident().getTypeAccident().equals(DataAccidentTypeAccidentEnum.CARD_PASSIVA_DOPPIA_FIRMA)
        ;
    }

    // TODO il codice azienda assicuratrice deve finire nell'application.properties
    public Boolean isInsuranceCompanySogessour() {
        return
            this.getDamaged().getInsuranceCompany() != null &&
            this.getDamaged().getInsuranceCompany().getTpl() != null &&
            this.getDamaged().getInsuranceCompany().getTpl().getInsuranceCompanyId() != null &&
            this.getDamaged().getInsuranceCompany().getTpl().getInsuranceCompanyId().equals(UUID.fromString("a83be7e2-4a58-11e9-8646-d663bd873d93"))
       ;
    }

    public Wounded getWoundedByType(WoundedTypeEnum type) {
        for( Wounded wounded : this.getWoundedList() ) {
            if( wounded.getType().equals(type) ) {
                return wounded;
            }
        }
        return null;
    }

}
