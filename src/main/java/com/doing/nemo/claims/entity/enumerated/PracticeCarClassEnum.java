package com.doing.nemo.claims.entity.enumerated;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public enum PracticeCarClassEnum implements Serializable {
    STANDARD("standard"),
    MISAPPROPRIATION("misappropriation");

    private static Logger LOGGER = LoggerFactory.getLogger(PracticeCarClassEnum.class);
    private String statusEntity;

    private PracticeCarClassEnum(String statusEntity) {
        this.statusEntity = statusEntity;
    }

    @JsonCreator
    public static PracticeCarClassEnum create(String statusEntity) {

        statusEntity = statusEntity.replace(" - ", "_");
        statusEntity = statusEntity.replace('-', '_');
        statusEntity = statusEntity.replace(' ', '_');

        if (statusEntity != null) {
            for (PracticeCarClassEnum val : PracticeCarClassEnum.values()) {
                if (statusEntity.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        LOGGER.debug("Bad parameters exception. Enum class " + PracticeCarClassEnum.class.getSimpleName() + " doesn't accept this value: " + statusEntity);
        throw new BadParametersException("Bad parameters exception. Enum class " + PracticeCarClassEnum.class.getSimpleName() + " doesn't accept this value: " + statusEntity);
    }

    public static List<PracticeCarClassEnum> getStatusListFromString(List<String> statusListString) {

        List<PracticeCarClassEnum> statusList = new LinkedList<>();
        for (String att : statusListString) {
            statusList.add(PracticeCarClassEnum.create(att));
        }
        return statusList;
    }

    public String getValue() {
        return this.statusEntity.toUpperCase();
    }

    @JsonValue
    public String toValue() {
        for (PracticeCarClassEnum val : PracticeCarClassEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }
}