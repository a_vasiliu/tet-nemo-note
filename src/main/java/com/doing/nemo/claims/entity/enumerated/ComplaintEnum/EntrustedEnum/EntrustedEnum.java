package com.doing.nemo.claims.entity.enumerated.ComplaintEnum.EntrustedEnum;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;

public enum EntrustedEnum implements Serializable {
    INSPECTORATE("inspectorate"),
    LEGAL("legal"),
    MANAGER("manager"),
    BROKER("broker");

    private static Logger LOGGER = LoggerFactory.getLogger(EntrustedEnum.class);
    private String entrustedEntity;

    private EntrustedEnum(String entrustedEntity) {
        this.entrustedEntity = entrustedEntity;
    }

    @JsonCreator
    public static EntrustedEnum create(String entrustedEntity) {

        if (entrustedEntity != null) {
            for (EntrustedEnum val : EntrustedEnum.values()) {
                if (entrustedEntity.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        LOGGER.debug("Bad parameters exception. Enum class " + EntrustedEnum.class.getSimpleName() + " doesn't accept this value: " + entrustedEntity);
        throw new BadParametersException("Bad parameters exception. Enum class " + EntrustedEnum.class.getSimpleName() + " doesn't accept this value: " + entrustedEntity);
    }

    public String getValue() {
        return this.entrustedEntity.toUpperCase();
    }

    @JsonValue
    public String toValue() {
        for (EntrustedEnum val : EntrustedEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }
}
