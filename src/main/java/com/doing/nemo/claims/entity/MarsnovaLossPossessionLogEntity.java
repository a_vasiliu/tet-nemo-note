package com.doing.nemo.claims.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "marsnova_loss_possession_log")
public class MarsnovaLossPossessionLogEntity implements Serializable {

    private static final long serialVersionUID = 5816144815969309732L;

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Type(type = "org.hibernate.type.PostgresUUIDType")
    private UUID id;

    @Column(name = "created_at")
    private Instant createdAt;

    @Column(name = "plate")
    private String plate;

    @Column(name = "enum_id")
    private Integer enumId;

    @Column(name = "description")
    private String description;

    @Column(name = "result")
    private String result;

    public MarsnovaLossPossessionLogEntity() {
    }

    public MarsnovaLossPossessionLogEntity(UUID id, Instant createdAt, String plate, Integer enumId, String description, String result) {
        this.id = id;
        this.createdAt = createdAt;
        this.plate = plate;
        this.enumId = enumId;
        this.description = description;
        this.result = result;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public Integer getEnumId() {
        return enumId;
    }

    public void setEnumId(Integer enumId) {
        this.enumId = enumId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "MarsnovaLossPossessionLogEntity{" +
                "id=" + id +
                ", createdAt=" + createdAt +
                ", plate='" + plate + '\'' +
                ", enumId=" + enumId +
                ", description='" + description + '\'' +
                ", result='" + result + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MarsnovaLossPossessionLogEntity that = (MarsnovaLossPossessionLogEntity) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getCreatedAt(), that.getCreatedAt()) &&
                Objects.equals(getPlate(), that.getPlate()) &&
                Objects.equals(getEnumId(), that.getEnumId()) &&
                Objects.equals(getDescription(), that.getDescription()) &&
                Objects.equals(getResult(), that.getResult());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getCreatedAt(), getPlate(), getEnumId(), getDescription(), getResult());
    }
}
