package com.doing.nemo.claims.entity;

import com.doing.nemo.claims.entity.claims.ClaimsAuthorityEntity;
import com.doing.nemo.claims.entity.settings.ClaimsTypeEntity;
import com.doing.nemo.claims.entity.settings.ContractTypeEntity;
import com.doing.nemo.claims.entity.settings.FlowContractTypeId;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "claims_to_claims_authority")
public class ClaimsAuthorityEmbeddedEntity {

    @EmbeddedId
    private ClaimsAuthorityId id;

    @ManyToOne
    @JoinColumn(name= "claims_id")
    @MapsId("claimsId")
    @JsonBackReference(value = "claims-authority-embedded-entities")
    private ClaimsNewEntity claims;

    @ManyToOne(cascade= {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    //@ManyToOne(cascade= CascadeType.ALL)
    @JoinColumn(name= "authority_id")
    @MapsId("authorityId")
    @JsonBackReference(value = "claims-authority-entity")
    private ClaimsAuthorityEntity claimsAuthorityEntity;

    @Column(name = "oldest")
    private Boolean oldest;

    @Column(name = "is_not_duplicate")
    private Boolean isNotDuplicate;

    @Column(name = "franchise_number")
    private Long franchiseNumber;

    public ClaimsAuthorityEmbeddedEntity() {
    }

    public ClaimsAuthorityEmbeddedEntity(ClaimsAuthorityId id, ClaimsNewEntity claims, ClaimsAuthorityEntity claimsAuthorityEntity, Boolean oldest, Boolean isNotDuplicate, Long franchiseNumber) {
        this.id = id;
        this.claims = claims;
        this.claimsAuthorityEntity = claimsAuthorityEntity;
        this.oldest = oldest;
        this.isNotDuplicate = isNotDuplicate;
        this.franchiseNumber = franchiseNumber;
    }

    public ClaimsAuthorityId getId() {
        return id;
    }

    public void setId(ClaimsAuthorityId id) {
        this.id = id;
    }

    public ClaimsNewEntity getClaims() {
        return claims;
    }

    public void setClaims(ClaimsNewEntity claims) {
        this.claims = claims;
    }

    public ClaimsAuthorityEntity getClaimsAuthorityEntity() {
        return claimsAuthorityEntity;
    }

    public void setClaimsAuthorityEntity(ClaimsAuthorityEntity claimsAuthorityEntity) {
        this.claimsAuthorityEntity = claimsAuthorityEntity;
    }

    public Boolean getOldest() {
        return oldest;
    }

    public void setOldest(Boolean oldest) {
        this.oldest = oldest;
    }

    public Boolean getNotDuplicate() {
        return isNotDuplicate;
    }

    public void setNotDuplicate(Boolean notDuplicate) {
        isNotDuplicate = notDuplicate;
    }

    public Long getFranchiseNumber() {
        return franchiseNumber;
    }

    public void setFranchiseNumber(Long franchiseNumber) {
        this.franchiseNumber = franchiseNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ClaimsAuthorityEmbeddedEntity)) return false;
        ClaimsAuthorityEmbeddedEntity that = (ClaimsAuthorityEmbeddedEntity) o;
        return Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

    @Override
    public String toString() {
        return "ClaimsAuthorityEmbeddedEntity{" +
                "id=" + id +
                ", claimsAuthorityEntity=" + claimsAuthorityEntity +
                ", oldest=" + oldest +
                ", isNotDuplicate=" + isNotDuplicate +
                ", franchiseNumber=" + franchiseNumber +
                '}';
    }
}
