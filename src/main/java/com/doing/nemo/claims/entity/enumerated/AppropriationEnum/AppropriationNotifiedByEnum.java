package com.doing.nemo.claims.entity.enumerated.AppropriationEnum;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public enum AppropriationNotifiedByEnum implements Serializable {
    UNDEFINED("undefined"),
    AUTHORITY_EMAIL("authority_email"),
    CUSTOMER_SERVICE("customer_service"),
    FLEET_ADMINISTRATION("fleet_administration");

    private static Logger LOGGER = LoggerFactory.getLogger(AppropriationNotifiedByEnum.class);

    private String statusEntity;

    private AppropriationNotifiedByEnum(String statusEntity) {
        this.statusEntity = statusEntity;
    }

    @JsonCreator
    public static AppropriationNotifiedByEnum create(String statusEntity) {

        statusEntity = statusEntity.replace(" - ", "_");
        statusEntity = statusEntity.replace('-', '_');
        statusEntity = statusEntity.replace(' ', '_');

        if (statusEntity != null) {
            for (AppropriationNotifiedByEnum val : AppropriationNotifiedByEnum.values()) {
                if (statusEntity.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }

        LOGGER.debug("Bad parameters exception. Enum class " + AppropriationNotifiedByEnum.class.getSimpleName() + " doesn't accept this value: " + statusEntity);
        throw new BadParametersException("Bad parameters exception. Enum class " + AppropriationNotifiedByEnum.class.getSimpleName() + " doesn't accept this value: " + statusEntity);
    }

    public static List<AppropriationNotifiedByEnum> getStatusListFromString(List<String> statusListString) {

        List<AppropriationNotifiedByEnum> statusList = new LinkedList<>();
        for (String att : statusListString) {
            statusList.add(AppropriationNotifiedByEnum.create(att));
        }
        return statusList;
    }

    public String getValue() {
        return this.statusEntity.toUpperCase();
    }

    @JsonValue
    public String toValue() {
        for (AppropriationNotifiedByEnum val : AppropriationNotifiedByEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }
}