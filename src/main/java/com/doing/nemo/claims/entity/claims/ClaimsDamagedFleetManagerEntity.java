package com.doing.nemo.claims.entity.claims;

import com.doing.nemo.claims.entity.enumerated.DamagedEnum.VehicleEnum.VehicleNatureEnum;
import com.doing.nemo.claims.entity.enumerated.DamagedEnum.VehicleEnum.VehicleOwnershipEnum;
import com.doing.nemo.claims.entity.esb.AddressEsb;
import com.doing.nemo.claims.entity.jsonb.Address;
import com.doing.nemo.claims.entity.jsonb.dataType.JsonDataAddressType;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@JsonIgnoreProperties({"hibernateLazyInitializer", "hibernate_lazy_initializer", "handler"})
@Entity
@Table(name = "claims_damaged_fleet_manager")
@TypeDefs({
        @TypeDef(name = "JsonDataAddressType", typeClass = JsonDataAddressType.class)
})
public class ClaimsDamagedFleetManagerEntity implements Serializable {

    @Column(name ="id")
    @Id
    private String id;

    @Column(name="fleet_manager_id")
    private Long fleetManagerId;

    @Column(name="identification")
    private String identification;

    @Column(name="official_registration")
    private String officialRegistration;

    @Column(name="first_name")
    private String firstName;

    @Column(name="last_name")
    private String lastName;

    @Column(name="title")
    private String title;

    @Column(name="email")
    private String email;

    @Column(name="phone_prefix")
    private String phonePrefix;

    @Column(name="phone")
    private String phone;

    @Column(name="secondary_phone_prefix")
    private String secondaryPhonePrefix;

    @Column(name="secondary_phone")
    private String secondaryPhone;

    @Column(name="sex")
    private String sex;

    @Column(name="customer_id")
    private Long customerId;

    @Column(name="main_address")
    @Type(type = "JsonDataAddressType")
    private Address mainAddress;

    @Column(name = "disable_notification")
    private Boolean disableNotification;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getFleetManagerId() {
        return fleetManagerId;
    }

    public void setFleetManagerId(Long fleetManagerId) {
        this.fleetManagerId = fleetManagerId;
    }

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public String getOfficialRegistration() {
        return officialRegistration;
    }

    public void setOfficialRegistration(String officialRegistration) {
        this.officialRegistration = officialRegistration;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhonePrefix() {
        return phonePrefix;
    }

    public void setPhonePrefix(String phonePrefix) {
        this.phonePrefix = phonePrefix;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSecondaryPhonePrefix() {
        return secondaryPhonePrefix;
    }

    public void setSecondaryPhonePrefix(String secondaryPhonePrefix) {
        this.secondaryPhonePrefix = secondaryPhonePrefix;
    }

    public String getSecondaryPhone() {
        return secondaryPhone;
    }

    public void setSecondaryPhone(String secondaryPhone) {
        this.secondaryPhone = secondaryPhone;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public Address getMainAddress() {
        return mainAddress;
    }

    public void setMainAddress(Address mainAddress) {
        this.mainAddress = mainAddress;
    }

    public void setDisableNotification(Boolean disableNotification){
        if(disableNotification == null){
            this.disableNotification = false;
        }else{
            this.disableNotification = disableNotification;
        }
    }

    public Boolean isDisableNotification(){
        if(this.disableNotification == null){
            return false;
        }else{
            return this.disableNotification;
        }
    }

    @Override
    public String toString() {
        return "ClaimsDamagedFleetManagerEntity{" +
                "id='" + id + '\'' +
                ", fleetManagerId=" + fleetManagerId +
                ", identification='" + identification + '\'' +
                ", officialRegistration='" + officialRegistration + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", title='" + title + '\'' +
                ", email='" + email + '\'' +
                ", phonePrefix='" + phonePrefix + '\'' +
                ", phone='" + phone + '\'' +
                ", secondaryPhonePrefix='" + secondaryPhonePrefix + '\'' +
                ", secondaryPhone='" + secondaryPhone + '\'' +
                ", sex='" + sex + '\'' +
                ", customerId=" + customerId +
                ", mainAddress=" + mainAddress +
                ", disableNotification=" + disableNotification +
                '}';
    }
}
