package com.doing.nemo.claims.entity.enumerated;

import com.doing.nemo.commons.exception.BadParametersException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.io.Serializable;

public enum WorkabilitySystemEnum implements Serializable {
    W("w"),
    N("n"),
    D("d");

    private static Logger LOGGER = LoggerFactory.getLogger(WorkabilitySystemEnum.class);
    private String workabilitySystemType;

    private WorkabilitySystemEnum(String workabilitySystemType) {
        this.workabilitySystemType = workabilitySystemType;
    }

    @JsonCreator
    public static WorkabilitySystemEnum create(String workabilitySystemType) {

        if (workabilitySystemType != null) {

            workabilitySystemType = workabilitySystemType.replace(" - ", "_");
            workabilitySystemType = workabilitySystemType.replace('-', '_');
            workabilitySystemType = workabilitySystemType.replace(' ', '_');

            for (WorkabilitySystemEnum val : WorkabilitySystemEnum.values()) {
                if (workabilitySystemType.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        LOGGER.info("Bad parameters exception. Enum class " + WorkabilitySystemEnum.class.getSimpleName() + " doesn't accept this value: " + workabilitySystemType);
        throw new BadParametersException("Bad parameters exception. Enum class " + WorkabilitySystemEnum.class.getSimpleName() + " doesn't accept this value: " + workabilitySystemType);
    }

    public String getValue() {
        return this.workabilitySystemType.toUpperCase();
    }

    @JsonValue
    public String toValue() {
        for (WorkabilitySystemEnum val : WorkabilitySystemEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }

}
