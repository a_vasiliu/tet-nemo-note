package com.doing.nemo.claims.entity.claims;

import com.doing.nemo.claims.entity.ClaimsNewEntity;
import com.doing.nemo.claims.entity.jsonb.dataType.JsonDataSplitListType;
import com.doing.nemo.claims.entity.jsonb.refund.Split;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@JsonIgnoreProperties({"hibernateLazyInitializer", "hibernate_lazy_initializer", "handler"})
@Entity
@Table(name = "claims_refund")
@TypeDefs({
        @TypeDef(name = "JsonDataSplitListType", typeClass = JsonDataSplitListType.class)
})
public class ClaimsRefundEntity implements Serializable {

    @Id
    @Column(name = "id", updatable = false, nullable = false)
    private String id;

    @Column(name="po_sum")
    private Double poSum;

    @Column(name="wreck")
    private Boolean wreck;

    @Column(name="wreck_value_pre")
    private Double wreckValuePre;

    @Column(name="wreck_value_post")
    private Double wreckValuePost;

    @Column(name="NBV")
    private String NBV;

    @Column(name="franchise_amount_fcm")
    private Double franchiseAmountFcm;

    @Column(name="total_refund_expected")
    private Double totalRefundExpected;

    @Column(name="total_liquidation_received")
    private Double totalLiquidationReceived;

    @Column(name="definition_date")
    //@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    private Instant definitionDate;

    @Column(name="blu_eurotax")
    private String bluEurotax;

    @Column(name="amount_to_be_debited")
    private Double amountToBeDebited;

    @Column(name="delta_debit_date")
    //@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    private Date deltaDebitDate;

    @Column(name="split_list")
    @Type(type = "JsonDataSplitListType")
    private List<Split> splitList;

    @OneToOne
    @JoinColumn(name = "id")
    @MapsId("id")
    @PrimaryKeyJoinColumn
    private ClaimsNewEntity claim;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Double getPoSum() {
        return poSum;
    }

    public void setPoSum(Double poSum) {
        this.poSum = poSum;
    }

    public Boolean getWreck() {
        return wreck;
    }

    public void setWreck(Boolean wreck) {
        this.wreck = wreck;
    }

    public Double getWreckValuePre() {
        return wreckValuePre;
    }

    public void setWreckValuePre(Double wreckValuePre) {
        this.wreckValuePre = wreckValuePre;
    }

    public Double getWreckValuePost() {
        return wreckValuePost;
    }

    public void setWreckValuePost(Double wreckValuePost) {
        this.wreckValuePost = wreckValuePost;
    }

    public String getNBV() {
        return NBV;
    }

    public void setNBV(String NBV) {
        this.NBV = NBV;
    }

    public Double getFranchiseAmountFcm() {
        return franchiseAmountFcm;
    }

    public void setFranchiseAmountFcm(Double franchiseAmountFcm) {
        this.franchiseAmountFcm = franchiseAmountFcm;
    }

    public Double getTotalRefundExpected() {
        return totalRefundExpected;
    }

    public void setTotalRefundExpected(Double totalRefundExpected) {
        this.totalRefundExpected = totalRefundExpected;
    }

    public Double getTotalLiquidationReceived() {
        return totalLiquidationReceived;
    }

    public void setTotalLiquidationReceived(Double totalLiquidationReceived) {
        this.totalLiquidationReceived = totalLiquidationReceived;
    }

    public Instant getDefinitionDate() {
        return definitionDate;
    }

    public void setDefinitionDate(Instant definitionDate) {
        this.definitionDate = definitionDate;
    }

    public String getBluEurotax() {
        return bluEurotax;
    }

    public void setBluEurotax(String bluEurotax) {
        this.bluEurotax = bluEurotax;
    }

    public Double getAmountToBeDebited() {
        return amountToBeDebited;
    }

    public void setAmountToBeDebited(Double amountToBeDebited) {
        this.amountToBeDebited = amountToBeDebited;
    }

    public Date getDeltaDebitDate() {
        return deltaDebitDate;
    }

    public void setDeltaDebitDate(Date deltaDebitDate) {
        this.deltaDebitDate = deltaDebitDate;
    }

    public List<Split> getSplitList() {
        return splitList;
    }

    public void setSplitList(List<Split> splitList) {
        this.splitList = splitList;
    }

    public ClaimsNewEntity getClaim() {
        return claim;
    }

    public void setClaim(ClaimsNewEntity claim) {
        this.claim = claim;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ClaimsRefundEntity{");
        sb.append("id='").append(id).append('\'');
        sb.append(", poSum=").append(poSum);
        sb.append(", wreck=").append(wreck);
        sb.append(", wreckValuePre=").append(wreckValuePre);
        sb.append(", wreckValuePost=").append(wreckValuePost);
        sb.append(", NBV='").append(NBV).append('\'');
        sb.append(", franchiseAmountFcm=").append(franchiseAmountFcm);
        sb.append(", totalRefundExpected=").append(totalRefundExpected);
        sb.append(", totalLiquidationReceived=").append(totalLiquidationReceived);
        sb.append(", definitionDate=").append(definitionDate);
        sb.append(", bluEurotax='").append(bluEurotax).append('\'');
        sb.append(", amountToBeDebited=").append(amountToBeDebited);
        sb.append(", deltaDebitDate=").append(deltaDebitDate);
        sb.append(", splitList=").append(splitList);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClaimsRefundEntity that = (ClaimsRefundEntity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(poSum, that.poSum) &&
                Objects.equals(wreck, that.wreck) &&
                Objects.equals(wreckValuePre, that.wreckValuePre) &&
                Objects.equals(wreckValuePost, that.wreckValuePost) &&
                Objects.equals(NBV, that.NBV) &&
                Objects.equals(franchiseAmountFcm, that.franchiseAmountFcm) &&
                Objects.equals(totalRefundExpected, that.totalRefundExpected) &&
                Objects.equals(totalLiquidationReceived, that.totalLiquidationReceived) &&
                Objects.equals(definitionDate, that.definitionDate) &&
                Objects.equals(bluEurotax, that.bluEurotax) &&
                Objects.equals(amountToBeDebited, that.amountToBeDebited) &&
                Objects.equals(deltaDebitDate, that.deltaDebitDate) &&
                Objects.equals(splitList, that.splitList) &&
                Objects.equals(claim, that.claim);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, poSum, wreck, wreckValuePre, wreckValuePost, NBV, franchiseAmountFcm, totalRefundExpected, totalLiquidationReceived, definitionDate, bluEurotax, amountToBeDebited, deltaDebitDate, splitList, claim);
    }
}
