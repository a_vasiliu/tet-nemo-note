package com.doing.nemo.claims.entity.esb.ContractESB;

import com.doing.nemo.claims.entity.esb.LinkEsb;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ContractEsb implements Serializable {

    private static final long serialVersionUID = 3601224418435265997L;

    @JsonProperty("contract_id")
    private Long contractId;

    @JsonProperty("succeeding_contract_id")
    private Long succeedingContractId;

    @JsonProperty("contract_version_id")
    private Long contractVersionId;

    @JsonProperty("status")
    private String status;

    @JsonProperty("customer_id")
    private Long customerId;

    @JsonProperty("driver_id")
    private Long driverId;

    @JsonProperty("fleet_vehicle_id")
    private Long fleetVehicleId;

    @JsonProperty("milage")
    private Double mileage;

    @JsonProperty("duration")
    private Integer duration;

    @JsonProperty("contract_type")
    private String contractType;

    @JsonProperty("short_contract_type")
    private String shortContractType;

    @JsonProperty("start_date")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date startDate;

    @JsonProperty("end_date")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date endDate;

    @JsonProperty("takein_date")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date takeInDate;

    @JsonProperty("leasing_company_id")
    private Long leasingCompanyId;

    @JsonProperty("leasing_company")
    private String leasingCompany;

    @JsonProperty("delivery_location")
    private String deliveryLocation;

    @JsonProperty("links")
    private List<LinkEsb> linksList;

    @JsonProperty("lease_service_components")
    private List<LeaseServiceComponentsEsb> leaseServiceComponentsList;

    @JsonProperty("license_plate")
    private String licensePlate;

    public ContractEsb() {
    }



    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getContractId() {
        return contractId;
    }

    public void setContractId(Long contractId) {
        this.contractId = contractId;
    }

    public Long getSucceedingContractId() {
        return succeedingContractId;
    }

    public void setSucceedingContractId(Long succeedingContractId) {
        this.succeedingContractId = succeedingContractId;
    }

    public Long getContractVersionId() {
        return contractVersionId;
    }

    public void setContractVersionId(Long contractVersionId) {
        this.contractVersionId = contractVersionId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public Long getDriverId() {
        return driverId;
    }

    public void setDriverId(Long driverId) {
        this.driverId = driverId;
    }

    public Long getFleetVehicleId() {
        return fleetVehicleId;
    }

    public void setFleetVehicleId(Long fleetVehicleId) {
        this.fleetVehicleId = fleetVehicleId;
    }

    public Double getMileage() {
        return mileage;
    }

    public void setMileage(Double mileage) {
        this.mileage = mileage;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getContractType() {
        return contractType;
    }

    public void setContractType(String contractType) {
        this.contractType = contractType;
    }

    public Date getStartDate() {
        if(startDate == null){
            return null;
        }
        return (Date)startDate.clone();
    }

    public void setStartDate(Date startDate) {
        if(startDate != null)
        {
            this.startDate = (Date)startDate.clone();
        } else {
            this.startDate = null;
        }
    }

    public Date getEndDate() {
        if(endDate == null){
            return null;
        }
        return (Date)endDate.clone();
    }

    public void setEndDate(Date endDate) {
        if(endDate != null)
        {
            this.endDate = (Date)endDate.clone();
        } else {
            this.endDate = null;
        }
    }



    public Long getLeasingCompanyId() {
        return leasingCompanyId;
    }

    public void setLeasingCompanyId(Long leasingCompanyId) {
        this.leasingCompanyId = leasingCompanyId;
    }

    public String getLeasingCompany() {
        return leasingCompany;
    }

    public void setLeasingCompany(String leasingCompany) {
        this.leasingCompany = leasingCompany;
    }

    public String getDeliveryLocation() {
        return deliveryLocation;
    }

    public void setDeliveryLocation(String deliveryLocation) {
        this.deliveryLocation = deliveryLocation;
    }

    public List<LinkEsb> getLinksList() {
        if(linksList == null){
            return null;
        }
        return new ArrayList<>(linksList);
    }

    public void setLinksList(List<LinkEsb> linksList) {
        if(linksList != null)
        {
            this.linksList = new ArrayList<>(linksList);
        } else {
            this.linksList = null;
        }
    }

    public List<LeaseServiceComponentsEsb> getLeaseServiceComponentsList() {
        if(leaseServiceComponentsList == null){
            return  null;
        }
        return new ArrayList<>(leaseServiceComponentsList);
    }

    public void setLeaseServiceComponentsList(List<LeaseServiceComponentsEsb> leaseServiceComponentsList) {
        if(leaseServiceComponentsList != null)
        {
            this.leaseServiceComponentsList =  new ArrayList<>(leaseServiceComponentsList);
        } else {
            this.leaseServiceComponentsList = null;
        }
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public String getShortContractType() {
        return shortContractType;
    }

    public void setShortContractType(String shortContractType) {
        this.shortContractType = shortContractType;
    }

    public Date getTakeinDate() {
        if(takeInDate == null){
            return null;
        }
        return (Date)takeInDate.clone();
    }

    public void setTakeinDate(Date takeinDate) {
        if(takeInDate != null)
        {
            this.takeInDate = (Date)takeinDate.clone();
        } else {
            this.takeInDate = null;
        }
    }

    @Override
    public String toString() {
        return "ContractEsb{" +
                "contractId=" + contractId +
                ", succeedingContractId=" + succeedingContractId +
                ", contractVersionId=" + contractVersionId +
                ", status='" + status + '\'' +
                ", customerId=" + customerId +
                ", driverId=" + driverId +
                ", fleetVehicleId=" + fleetVehicleId +
                ", mileage=" + mileage +
                ", duration=" + duration +
                ", contractType='" + contractType + '\'' +
                ", shortContractType='" + shortContractType + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", takeInDate=" + takeInDate +
                ", leasingCompanyId=" + leasingCompanyId +
                ", leasingCompany='" + leasingCompany + '\'' +
                ", deliveryLocation='" + deliveryLocation + '\'' +
                ", linksList=" + linksList +
                ", leaseServiceComponentsList=" + leaseServiceComponentsList +
                ", licensePlate='" + licensePlate + '\'' +
                '}';
    }
}
