package com.doing.nemo.claims.entity.jsonb.practice;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Seizure implements Serializable {

    private static final long serialVersionUID = 809173064960583663L;

    @JsonProperty("seizure_date")
    private Date misappropriationDate;

    @JsonProperty("return_good_faith")
    private Boolean returnGoodFaith;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Date getMisappropriationDate() {
        if(misappropriationDate == null){
            return null;
        }
        return (Date)misappropriationDate.clone();
    }

    public void setMisappropriationDate(Date misappropriationDate) {
        if(misappropriationDate != null)
        {
            this.misappropriationDate =(Date)misappropriationDate.clone();
        } else {
            this.misappropriationDate = null;
        }
    }

    public Boolean getReturnGoodFaith() {
        return returnGoodFaith;
    }

    public void setReturnGoodFaith(Boolean returnGoodFaith) {
        this.returnGoodFaith = returnGoodFaith;
    }

    @Override
    public String toString() {
        return "Seizure{" +
                "misappropriationDate=" + misappropriationDate +
                ", returnGoodFaith=" + returnGoodFaith +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Seizure seizure = (Seizure) o;

        return new EqualsBuilder().append(misappropriationDate, seizure.misappropriationDate)
                .append(returnGoodFaith, seizure.returnGoodFaith).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(misappropriationDate).append(returnGoodFaith).toHashCode();
    }
}
