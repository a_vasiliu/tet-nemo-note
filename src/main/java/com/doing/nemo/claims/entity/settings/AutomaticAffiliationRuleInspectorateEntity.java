package com.doing.nemo.claims.entity.settings;

import com.doing.nemo.claims.entity.enumerated.DamagedEnum.VehicleEnum.VehicleTypeEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "rule_inspectorate",
        uniqueConstraints = @UniqueConstraint(
                name = "uc_automatic_affiliation_rule_inspectorate",
                columnNames = {
                        "selection_name", "inspectorate_id"
                }
        )
)
public class AutomaticAffiliationRuleInspectorateEntity implements Serializable {

    @Id
    @JsonIgnore
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )

    @Type(type = "org.hibernate.type.PostgresUUIDType")
    private UUID id;

    @Column(name = "selection_name")
    private String selectionName;

    @Column(name = "monthly_volume")
    private Integer monthlyVolume;

    @Column(name = "current_month")
    private String currentMonth;

    @Column(name = "current_volume")
    private Integer currentVolume;

    @Column(name = "detail")
    private Integer detail;

    @Column(name = "annotations")
    private String annotations;

    @Column(name = "select_only_complaints_without_ctp")
    private Boolean selectOnlyComplaintsWithoutCTP;

    @Column(name = "foreign_country_claim")
    private Boolean foreignCountryClaim;

    @JsonProperty("counterpart_type")
    @Enumerated(EnumType.STRING)
    @Column(name = "counterpart_type")
    private VehicleTypeEnum counterPartType;

    @Column(name = "claim_with_injured")
    private Boolean claimWithInjured;

    @Column(name = "damage_can_not_be_repaired")
    private Boolean damageCanNotBeRepaired;

    @Column(name = "min_import")
    private Double minImport;

    @Column(name = "max_import")
    private Double maxImport;

    @ManyToMany
    @JoinTable(name = "rule_inspectorate_claims_type",
            joinColumns = @JoinColumn(name = "rule_inspectorate_id"),
            inverseJoinColumns = @JoinColumn(name = "claims_type_id"))
    @Column(name = "claims_type_entity_list")
    private List<ClaimsTypeEntity> claimsTypeEntityList;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "rule_inspectorate_damaged",
            joinColumns = @JoinColumn(name = "rule_inspectorate_id"),
            inverseJoinColumns = @JoinColumn(name = "insurance_company_id"))
    @Column(name = "damaged_insurance_company_list")
    private List<InsuranceCompanyEntity> damagedInsuranceCompanyList;

    @ManyToMany
    @JoinTable(name = "rule_inspectorate_counterparty",
            joinColumns = @JoinColumn(name = "rule_inspectorate_id"),
            inverseJoinColumns = @JoinColumn(name = "insurance_company_id"))
    @Column(name = "counter_parter_insurance_company_list")
    private List<InsuranceCompanyEntity> counterParterInsuranceCompanyList;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getSelectionName() {
        return selectionName;
    }

    public void setSelectionName(String selectionName) {
        this.selectionName = selectionName;
    }

    public Integer getMonthlyVolume() {
        return monthlyVolume;
    }

    public void setMonthlyVolume(Integer monthlyVolume) {
        this.monthlyVolume = monthlyVolume;
    }

    public String getCurrentMonth() {
        return currentMonth;
    }

    public void setCurrentMonth(String currentMonth) {
        this.currentMonth = currentMonth;
    }

    public Integer getCurrentVolume() {
        return currentVolume;
    }

    public void setCurrentVolume(Integer currentVolume) {
        this.currentVolume = currentVolume;
    }

    public Integer getDetail() {
        return detail;
    }

    public void setDetail(Integer detail) {
        this.detail = detail;
    }

    public String getAnnotations() {
        return annotations;
    }

    public void setAnnotations(String annotations) {
        this.annotations = annotations;
    }

    public Boolean getSelectOnlyComplaintsWithoutCTP() {
        return selectOnlyComplaintsWithoutCTP;
    }

    public void setSelectOnlyComplaintsWithoutCTP(Boolean selectOnlyComplaintsWithoutCTP) {
        this.selectOnlyComplaintsWithoutCTP = selectOnlyComplaintsWithoutCTP;
    }

    public Boolean getForeignCountryClaim() {
        return foreignCountryClaim;
    }

    public void setForeignCountryClaim(Boolean foreignCountryClaim) {
        this.foreignCountryClaim = foreignCountryClaim;
    }

    public VehicleTypeEnum getCounterPartType() {
        return counterPartType;
    }

    public void setCounterPartType(VehicleTypeEnum counterPartType) {
        this.counterPartType = counterPartType;
    }

    public Boolean getClaimWithInjured() {
        return claimWithInjured;
    }

    public void setClaimWithInjured(Boolean claimWithInjured) {
        this.claimWithInjured = claimWithInjured;
    }

    public Boolean getDamageCanNotBeRepaired() {
        return damageCanNotBeRepaired;
    }

    public void setDamageCanNotBeRepaired(Boolean damageCanNotBeRepaired) {
        this.damageCanNotBeRepaired = damageCanNotBeRepaired;
    }

    public List<ClaimsTypeEntity> getClaimsTypeEntityList() {
        if(claimsTypeEntityList == null){
            return null;
        }
        return new ArrayList<>(claimsTypeEntityList);
    }

    public void setClaimsTypeEntityList(List<ClaimsTypeEntity> claimsTypeEntityList) {
        if(claimsTypeEntityList !=null)
        {
            this.claimsTypeEntityList = new ArrayList<>(claimsTypeEntityList);
        } else {
            this.claimsTypeEntityList = null;
        }
    }

    public List<InsuranceCompanyEntity> getDamagedInsuranceCompanyList() {
        if(damagedInsuranceCompanyList == null){
            return null;
        }
        return new ArrayList<>(damagedInsuranceCompanyList);
    }

    public void setDamagedInsuranceCompanyList(List<InsuranceCompanyEntity> damagedInsuranceCompanyList) {
        if(damagedInsuranceCompanyList != null)
        {
            this.damagedInsuranceCompanyList = new ArrayList<>(damagedInsuranceCompanyList);
        } else {
            this.damagedInsuranceCompanyList = null;
        }
    }

    public List<InsuranceCompanyEntity> getCounterParterInsuranceCompanyList() {
        if(counterParterInsuranceCompanyList == null){
            return null;
        }
        return new ArrayList<>(counterParterInsuranceCompanyList);
    }

    public void setCounterParterInsuranceCompanyList(List<InsuranceCompanyEntity> counterParterInsuranceCompanyList) {
        if(counterParterInsuranceCompanyList != null)
        {
            this.counterParterInsuranceCompanyList = new ArrayList<>(counterParterInsuranceCompanyList);
        } else {
            this.counterParterInsuranceCompanyList = null;
        }
    }

    public Double getMinImport() {
        return minImport;
    }

    public void setMinImport(Double minImport) {
        this.minImport = minImport;
    }

    public Double getMaxImport() {
        return maxImport;
    }

    public void setMaxImport(Double maxImport) {
        this.maxImport = maxImport;
    }

    @Override
    public String toString() {
        return "AutomaticAffiliationRuleInspectorateEntity{" +
                "id=" + id +
                ", selectionName='" + selectionName + '\'' +
                ", monthlyVolume=" + monthlyVolume +
                ", currentMonth='" + currentMonth + '\'' +
                ", currentVolume=" + currentVolume +
                ", detail=" + detail +
                ", annotations='" + annotations + '\'' +
                ", selectOnlyComplaintsWithoutCTP=" + selectOnlyComplaintsWithoutCTP +
                ", foreignCountryClaim=" + foreignCountryClaim +
                ", counterPartType='" + counterPartType + '\'' +
                ", claimWithInjured=" + claimWithInjured +
                ", damageCanNotBeRepaired=" + damageCanNotBeRepaired +
                ", minImport=" + minImport +
                ", maxImport=" + maxImport +
                ", claimsTypeEntityList=" + claimsTypeEntityList +
                ", damagedInsuranceCompanyList=" + damagedInsuranceCompanyList +
                ", counterParterInsuranceCompanyList=" + counterParterInsuranceCompanyList +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AutomaticAffiliationRuleInspectorateEntity that = (AutomaticAffiliationRuleInspectorateEntity) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getSelectionName(), that.getSelectionName()) &&
                Objects.equals(getMonthlyVolume(), that.getMonthlyVolume()) &&
                Objects.equals(getCurrentMonth(), that.getCurrentMonth()) &&
                Objects.equals(getCurrentVolume(), that.getCurrentVolume()) &&
                Objects.equals(getDetail(), that.getDetail()) &&
                Objects.equals(getAnnotations(), that.getAnnotations()) &&
                Objects.equals(getSelectOnlyComplaintsWithoutCTP(), that.getSelectOnlyComplaintsWithoutCTP()) &&
                Objects.equals(getForeignCountryClaim(), that.getForeignCountryClaim()) &&
                getCounterPartType() == that.getCounterPartType() &&
                Objects.equals(getClaimWithInjured(), that.getClaimWithInjured()) &&
                Objects.equals(getDamageCanNotBeRepaired(), that.getDamageCanNotBeRepaired()) &&
                Objects.equals(getMinImport(), that.getMinImport()) &&
                Objects.equals(getMaxImport(), that.getMaxImport()) &&
                Objects.equals(getClaimsTypeEntityList(), that.getClaimsTypeEntityList()) &&
                Objects.equals(getDamagedInsuranceCompanyList(), that.getDamagedInsuranceCompanyList()) &&
                Objects.equals(getCounterParterInsuranceCompanyList(), that.getCounterParterInsuranceCompanyList());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getSelectionName(), getMonthlyVolume(), getCurrentMonth(), getCurrentVolume(), getDetail(), getAnnotations(), getSelectOnlyComplaintsWithoutCTP(), getForeignCountryClaim(), getCounterPartType(), getClaimWithInjured(), getDamageCanNotBeRepaired(), getMinImport(), getMaxImport(), getClaimsTypeEntityList(), getDamagedInsuranceCompanyList(), getCounterParterInsuranceCompanyList());
    }
}