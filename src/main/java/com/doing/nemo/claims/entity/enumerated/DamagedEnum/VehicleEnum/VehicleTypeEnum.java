package com.doing.nemo.claims.entity.enumerated.DamagedEnum.VehicleEnum;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;

public enum VehicleTypeEnum implements Serializable {
    VEHICLE("vehicle"),
    OTHER("other");

    private static Logger LOGGER = LoggerFactory.getLogger(VehicleTypeEnum.class);
    private String vehicleNatureEntity;

    private VehicleTypeEnum(String vehicleNatureEntity) {
        this.vehicleNatureEntity = vehicleNatureEntity;
    }

    @JsonCreator
    public static VehicleTypeEnum create(String vehicleTypeEntity) {

        vehicleTypeEntity = vehicleTypeEntity.replace(" - ", "_");
        vehicleTypeEntity = vehicleTypeEntity.replace('-', '_');
        vehicleTypeEntity = vehicleTypeEntity.replace(' ', '_');

        if (vehicleTypeEntity != null) {
            for (VehicleTypeEnum val : VehicleTypeEnum.values()) {
                if (vehicleTypeEntity.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        LOGGER.debug("Bad parameters exception. Enum class " + VehicleTypeEnum.class.getSimpleName() + " doesn't accept this value: " + vehicleTypeEntity);
        throw new BadParametersException("Bad parameters exception. Enum class " + VehicleTypeEnum.class.getSimpleName() + " doesn't accept this value: " + vehicleTypeEntity);
    }

    public String getValue() {
        return this.vehicleNatureEntity.toUpperCase();
    }

    @JsonValue
    public String toValue() {
        for (VehicleTypeEnum val : VehicleTypeEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }
}
