package com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbAntiTheftService;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AntiTheftRequest implements Serializable, Cloneable {

    @JsonProperty("id_transaction")
    private String idTransaction;

    @JsonProperty("request")
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    private Date request;

    @JsonProperty("id_websin")
    private String idWebSin;

    @JsonProperty("provider_type")
    private String providerType;

    @JsonProperty("response")
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    private Date response;

    @JsonProperty("response_type")
    private String responseType;

    @JsonProperty("outcome")
    private String outcome;

    @JsonProperty("sub_report")
    private Integer subReport;

    @JsonProperty("g_power")
    private Double gPower;

    @JsonProperty("crash_number")
    private Integer crashNumber;

    @JsonProperty("anomaly")
    private Boolean anomaly;

    @JsonProperty("pdf")
    private String pdf;

    @JsonProperty("response_message")
    private String responseMessage;

    /*@JsonProperty("report_number")
    private Integer reportNumber;*/

   /* public Integer getReportNumber() {
        return reportNumber;
    }

    public void setReportNumber(Integer reportNumber) {
        this.reportNumber = reportNumber;
    }*/

    public String getIdTransaction() {
        return idTransaction;
    }

    public void setIdTransaction(String idTransaction) {
        this.idTransaction = idTransaction;
    }

    public Date getRequest() {
        if(request == null){
            return null;
        }
        return (Date)request.clone();
    }

    public void setRequest(Date request) {
        if(request != null)
        {
            this.request = (Date)request.clone();
        } else {
            this.request = null;
        }
    }

    public String getIdWebSin() {
        return idWebSin;
    }

    public void setIdWebSin(String idWebSin) {
        this.idWebSin = idWebSin;
    }

    public String getProviderType() {
        return providerType;
    }

    public void setProviderType(String providerType) {
        this.providerType = providerType;
    }

    public Date getResponse() {
        if(response == null){
            return null;
        }
        return (Date)response.clone();
    }

    public void setResponse(Date response) {
        if(response != null)
        {
            this.response = (Date)response.clone();
        } else {
            this.response = null;
        }
    }

    public String getResponseType() {
        return responseType;
    }

    public void setResponseType(String responseType) {
        this.responseType = responseType;
    }

    public String getOutcome() {
        return outcome;
    }

    public void setOutcome(String outcome) {
        this.outcome = outcome;
    }

    public Integer getSubReport() {
        return subReport;
    }

    public void setSubReport(Integer subReport) {
        this.subReport = subReport;
    }

    public Double getgPower() {
        return gPower;
    }

    public void setgPower(Double gPower) {
        this.gPower = gPower;
    }

    public Integer getCrashNumber() {
        return crashNumber;
    }

    public void setCrashNumber(Integer crashNumber) {
        this.crashNumber = crashNumber;
    }

    public Boolean getAnomaly() {
        return anomaly;
    }

    public void setAnomaly(Boolean anomaly) {
        this.anomaly = anomaly;
    }

    public String getPdf() {
        return pdf;
    }

    public void setPdf(String pdf) {
        this.pdf = pdf;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public String toString() {
        return "AntiTheftRequest{" +
                "idTransaction='" + idTransaction + '\'' +
                ", request=" + request +
                ", idWebSin='" + idWebSin + '\'' +
                ", providerType='" + providerType + '\'' +
                ", response=" + response +
                ", responseType='" + responseType + '\'' +
                ", outcome='" + outcome + '\'' +
                ", subReport=" + subReport +
                ", gPower=" + gPower +
                ", crashNumber=" + crashNumber +
                ", anomaly=" + anomaly +
                //", pdf='" + pdf + '\'' +
                ", responseMessage='" + responseMessage + '\'' +
                //", reportNumber=" + reportNumber +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        AntiTheftRequest that = (AntiTheftRequest) o;

        return new EqualsBuilder().append(idTransaction, that.idTransaction)
                .append(request, that.request).append(idWebSin, that.idWebSin).append(providerType, that.providerType)
                .append(response, that.response).append(responseType, that.responseType).append(outcome, that.outcome)
                .append(subReport, that.subReport).append(gPower, that.gPower).append(crashNumber, that.crashNumber)
                .append(anomaly, that.anomaly).append(pdf, that.pdf).append(responseMessage, that.responseMessage).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(idTransaction)
                .append(request).append(idWebSin).append(providerType).append(response).append(responseType)
                .append(outcome).append(subReport).append(gPower).append(crashNumber).append(anomaly)
                .append(pdf).append(responseMessage).toHashCode();
    }
}
