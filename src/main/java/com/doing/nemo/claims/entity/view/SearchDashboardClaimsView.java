package com.doing.nemo.claims.entity.view;

import com.doing.nemo.claims.entity.ClaimsAuthorityEmbeddedEntity;
import com.doing.nemo.claims.entity.claims.ClaimsFromCompanyEntity;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsFlowEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.settings.AntiTheftServiceEntity;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.List;

@Entity
@Immutable
@Table(name = "search_dashboard_claims_view")
public class SearchDashboardClaimsView implements Serializable {

    private static final long serialVersionUID = 8368043877813816987L;
    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "practice_id")
    private Long practiceId;

    @Column(name = "created_at")
    private Instant createdAt;

    @Column(name = "updated_at")
    private Instant updatedAt;

    @Column(name = "status_updated_at")
    private Instant statusUpdatedAt;

    @Column(name = "status")
    private String status;


    @Column(name = "type")
    private String type;

    @Column(name = "type_accident")
    private String typeAccident;

    @Column(name = "date_accident")
    private Instant dateAccident;

    @Column(name = "in_evidence")
    private Boolean inEvidence;

    @Column(name = "po_variation")
    private Boolean poVariation;


    @Column(name="client_id")
    private String clientId;

    /* "complaint" */
    @Column(name = "plate")
    private String plate;


    @Column(name="damaged_anti_theft_service_id")
    private String damagedAntiTheftService_id;

    //AntiTheftServiceEntity
    @Column(name = "business_name")
    private String businessName;

    /* "damaged" */
    /* "customer" */

    @Column(name="customer_id")
    private Long customerId;

     /* "entrusted" */
    @Column(name = "auto_entrust")
    private Boolean autoEntrust;

    @Column(name = "is_pending")
    private Boolean isPending;

    @Column(name = "with_continuation")
    private Boolean withContinuation;

    @Column(name = "response_type")
    private String responseType;
    @Column(name = "provider_type")
    private String providerType;

  /*  @Column(name = "crash_report")
    private String crashReport;*/



    @OneToMany(mappedBy = "claims", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference(value = "claims-authority-embedded-entities")
    private List<ClaimsAuthorityEmbeddedEntity> claimsAuthorityEmbeddedEntities;

    @OneToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY,optional = false, orphanRemoval = true, mappedBy = "claim")
    @JsonBackReference(value = "claimsFromCompanyEntity")
    private ClaimsFromCompanyEntity claimsFromCompanyEntity;

    public ClaimsFromCompanyEntity getClaimsFromCompanyEntity() {
        return claimsFromCompanyEntity;
    }

    public void setClaimsFromCompanyEntity(ClaimsFromCompanyEntity claimsFromCompanyEntity) {
        this.claimsFromCompanyEntity = claimsFromCompanyEntity;
    }

    public String getResponseType() {
        return responseType;
    }

    public void setResponseType(String responseType) {
        this.responseType = responseType;
    }

    public String getProviderType() {
        return providerType;
    }

    public void setProviderType(String providerType) {
        this.providerType = providerType;
    }


    public Boolean getWithContinuation() {
        return withContinuation;
    }

    public void setWithContinuation(Boolean withContinuation) {
        this.withContinuation = withContinuation;
    }

    public String getId() {
        return id;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public Instant getUpdatedAt() {
        return updatedAt;
    }

    public Instant getStatusUpdatedAt() {
        return statusUpdatedAt;
    }

    public Long getPracticeId() {
        return practiceId;
    }


    public Boolean getInEvidence() {
        return inEvidence;
    }

    public Boolean getPoVariation() {
        return poVariation;
    }

    public String getPlate() {
        return plate;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public List<ClaimsAuthorityEmbeddedEntity> getClaimsAuthorityEmbeddedEntities() {
        return claimsAuthorityEmbeddedEntities;
    }

    public void setClaimsAuthorityEmbeddedEntities(List<ClaimsAuthorityEmbeddedEntity> claimsAuthorityEmbeddedEntities) {
        this.claimsAuthorityEmbeddedEntities = claimsAuthorityEmbeddedEntities;
    }

    public Instant getDateAccident() {
        return dateAccident;
    }


    public Boolean getAutoEntrust() {
        return autoEntrust;
    }

    public Long getCustomerId() {
        return customerId;
    }


    public Boolean getPending() {
        return isPending;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public void setUpdatedAt(Instant updatedAt) {
        this.updatedAt = updatedAt;
    }

    public void setStatusUpdatedAt(Instant statusUpdatedAt) {
        this.statusUpdatedAt = statusUpdatedAt;
    }

    public void setPracticeId(Long practiceId) {
        this.practiceId = practiceId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setInEvidence(Boolean inEvidence) {
        this.inEvidence = inEvidence;
    }

    public void setPoVariation(Boolean poVariation) {
        this.poVariation = poVariation;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public void setDateAccident(Instant dateAccident) {
        this.dateAccident = dateAccident;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public void setAutoEntrust(Boolean autoEntrust) {
        this.autoEntrust = autoEntrust;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getDamagedAntiTheftService_id() {
        return damagedAntiTheftService_id;
    }

    public void setDamagedAntiTheftService_id(String damagedAntiTheftService_id) {
        this.damagedAntiTheftService_id = damagedAntiTheftService_id;
    }

    public void setPending(Boolean pending) {
        isPending = pending;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTypeAccident() {
        return typeAccident;
    }

    public void setTypeAccident(String typeAccident) {
        this.typeAccident = typeAccident;
    }
}
