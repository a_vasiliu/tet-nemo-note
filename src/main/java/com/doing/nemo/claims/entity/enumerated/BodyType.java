package com.doing.nemo.claims.entity.enumerated;

public enum BodyType {
    TEXT("text/plain"),
    HTML("text/html");

    private String value;

    BodyType(String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }
}
