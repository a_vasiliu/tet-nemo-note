package com.doing.nemo.claims.entity.enumerated.myAld;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;

public enum MyAldInsertedByEnum implements Serializable {
    ME("me"),
    OTHER("other"),
    ALD_OPERATOR("ald_operator");

    private static Logger LOGGER = LoggerFactory.getLogger(MyAldInsertedByEnum.class);
    private String insertedBy;

    private MyAldInsertedByEnum(String insertedBy) {
        this.insertedBy = insertedBy;
    }

    @JsonCreator
    public static MyAldInsertedByEnum create(String insertedBy) {

        if (insertedBy != null) {

            insertedBy = insertedBy.replace(" - ", "_");
            insertedBy = insertedBy.replace('-', '_');
            insertedBy = insertedBy.replace(' ', '_');

            for (MyAldInsertedByEnum val : MyAldInsertedByEnum.values()) {
                if (insertedBy.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        LOGGER.debug("Bad parameters exception. Enum class " + MyAldInsertedByEnum.class.getSimpleName() + " doesn't accept this value: " + insertedBy);
        throw new BadParametersException("Bad parameters exception. Enum class " + MyAldInsertedByEnum.class.getSimpleName() + " doesn't accept this value: " + insertedBy);
    }

    public String getValue() {
        return this.insertedBy.toUpperCase();
    }

    @JsonValue
    public String toValue() {
        for (MyAldInsertedByEnum val : MyAldInsertedByEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }
}
