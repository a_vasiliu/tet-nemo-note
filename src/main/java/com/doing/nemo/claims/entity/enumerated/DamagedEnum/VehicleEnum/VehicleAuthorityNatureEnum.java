package com.doing.nemo.claims.entity.enumerated.DamagedEnum.VehicleEnum;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;

public enum VehicleAuthorityNatureEnum implements Serializable {
    CAR("car"), VAN("van"), BIKE("bike"), SUV("suv");

    private static Logger LOGGER = LoggerFactory.getLogger(VehicleOwnershipEnum.class);
    private String vehicleAuthorityNature;

    private VehicleAuthorityNatureEnum(String vehicleAuthorityNature) {
        this.vehicleAuthorityNature = vehicleAuthorityNature;
    }

    @JsonCreator
    public static VehicleAuthorityNatureEnum create(String vehicleAuthorityNature) {

        vehicleAuthorityNature = vehicleAuthorityNature.replace(" - ", "_");
        ;
        vehicleAuthorityNature = vehicleAuthorityNature.replace('-', '_');
        vehicleAuthorityNature = vehicleAuthorityNature.replace(' ', '_');

        if (vehicleAuthorityNature != null) {
            for (VehicleAuthorityNatureEnum val : VehicleAuthorityNatureEnum.values()) {
                if (vehicleAuthorityNature.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        LOGGER.debug("Bad parameters exception. Enum class " + VehicleAuthorityNatureEnum.class.getSimpleName() + " doesn't accept this value: " + vehicleAuthorityNature);
        throw new BadParametersException("Bad parameters exception. Enum class " + VehicleAuthorityNatureEnum.class.getSimpleName() + " doesn't accept this value: " + vehicleAuthorityNature);
    }

    public String getValue() {
        return this.vehicleAuthorityNature.toUpperCase();
    }

    @JsonValue
    public String toValue() {
        for (VehicleAuthorityNatureEnum val : VehicleAuthorityNatureEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }
}
