package com.doing.nemo.claims.entity.settings;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "lock")
public class LockEntity {
    @Column(name = "user_id")
    private String userId;

    @Id
    @Column(name = "claim_id")
    private String claimId;

    @Column(name = "now")
    private Timestamp now;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getClaimId() {
        return claimId;
    }

    public void setClaimId(String claimId) {
        this.claimId = claimId;
    }

    public Timestamp getNow() {
        if(now == null){
            return null;
        }
        return (Timestamp) now.clone();
    }

    public void setNow(Timestamp now) {
        if(now != null){
            this.now = (Timestamp) now.clone();
        } else {
            this.now = null;
        }
    }

    @Override
    public String toString() {
        return "LockEntity{" +
                "userId='" + userId + '\'' +
                ", claimId='" + claimId + '\'' +
                ", now=" + now +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LockEntity that = (LockEntity) o;
        return Objects.equals(getUserId(), that.getUserId()) &&
                Objects.equals(getClaimId(), that.getClaimId()) &&
                Objects.equals(getNow(), that.getNow());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUserId(), getClaimId(), getNow());
    }
}
