package com.doing.nemo.claims.entity.settings;

import com.doing.nemo.claims.entity.AntiTheftRequestEntity;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.ClaimsNewEntity;
import com.doing.nemo.claims.entity.CounterpartyEntity;
import com.doing.nemo.claims.entity.enumerated.AntitheftEnum.AntitheftTypeEnum;
import com.doing.nemo.claims.entity.enumerated.ProviderTypeEnum;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "anti_theft_service")
public class AntiTheftServiceEntity implements Serializable {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Type(type = "org.hibernate.type.PostgresUUIDType")
    private UUID id;

    @Column(name = "code_anti_theft_service")
    private String codeAntiTheftService;

    @Column(name = "name")
    private String name;

    @Column(name = "business_name")
    private String businessName;

    @Column(name = "supplier_code")
    private Integer supplierCode;

    @Column(name = "email")
    private String email;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "provider_type")
    private String providerType;

    @Column(name = "time_out_in_seconds")
    private Integer timeOutInSeconds;

    @Column(name = "end_point_url")
    private String endPointUrl;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "company_code")
    private String companyCode;

    @Column(name = "active")
    private Boolean active;

    @Column(name = "is_active")
    private Boolean isActive;

    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private AntitheftTypeEnum type;

    @OneToMany
    @JoinColumn(name = "damaged_anti_theft_service_id")
    @Column(name = "claims")
    @JsonManagedReference(value = "damaged_anti_theft_service_id")
    private List<ClaimsNewEntity> claimsEntities;


    public AntiTheftServiceEntity() {

    }

    public AntiTheftServiceEntity(String codeAntiTheftService, String name, String businessName, Integer supplierCode, String email, String phoneNumber, String providerType, Integer timeOutInSeconds, String endPointUrl, String username, String password, String companyCode, Boolean active, Boolean isActive, AntitheftTypeEnum type) {
        this.codeAntiTheftService = codeAntiTheftService;
        this.name = name;
        this.businessName = businessName;
        this.supplierCode = supplierCode;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.providerType = providerType;
        this.timeOutInSeconds = timeOutInSeconds;
        this.endPointUrl = endPointUrl;
        this.username = username;
        this.password = password;
        this.companyCode = companyCode;
        this.active = active;
        this.isActive = isActive;
        this.type = type;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getCodeAntiTheftService() {
        return codeAntiTheftService;
    }

    public void setCodeAntiTheftService(String codeAntiTheftService) {
        this.codeAntiTheftService = codeAntiTheftService;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public Integer getSupplierCode() {
        return supplierCode;
    }

    public void setSupplierCode(Integer supplierCode) {
        this.supplierCode = supplierCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getProviderType() {
        return providerType;
    }

    public void setProviderType(String providerType) {
        this.providerType = providerType;
    }

    public Integer getTimeOutInSeconds() {
        return timeOutInSeconds;
    }

    public void setTimeOutInSeconds(Integer timeOutInSeconds) {
        this.timeOutInSeconds = timeOutInSeconds;
    }

    public String getEndPointUrl() {
        return endPointUrl;
    }

    public void setEndPointUrl(String endPointUrl) {
        this.endPointUrl = endPointUrl;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public AntitheftTypeEnum getType() {
        return type;
    }

    public void setType(AntitheftTypeEnum type) {
        this.type = type;
    }

    public List<ClaimsNewEntity> getClaimsEntities() {
        return claimsEntities;
    }

    public void setClaimsEntities(List<ClaimsNewEntity> claimsEntities) {
        this.claimsEntities = claimsEntities;
    }

    @Override
    public String toString() {
        return "AntiTheftServiceEntity{" +
                "id=" + id +
                ", codeAntiTheftService='" + codeAntiTheftService + '\'' +
                ", name='" + name + '\'' +
                ", businessName='" + businessName + '\'' +
                ", supplierCode=" + supplierCode +
                ", email='" + email + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", providerType='" + providerType + '\'' +
                ", timeOutInSeconds=" + timeOutInSeconds +
                ", endPointUrl='" + endPointUrl + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", companyCode='" + companyCode + '\'' +
                ", active=" + active +
                ", isActive=" + isActive +
                ", type=" + type +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AntiTheftServiceEntity that = (AntiTheftServiceEntity) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getCodeAntiTheftService(), that.getCodeAntiTheftService()) &&
                Objects.equals(getName(), that.getName()) &&
                Objects.equals(getBusinessName(), that.getBusinessName()) &&
                Objects.equals(getSupplierCode(), that.getSupplierCode()) &&
                Objects.equals(getEmail(), that.getEmail()) &&
                Objects.equals(getPhoneNumber(), that.getPhoneNumber()) &&
                Objects.equals(getProviderType(), that.getProviderType()) &&
                Objects.equals(getTimeOutInSeconds(), that.getTimeOutInSeconds()) &&
                Objects.equals(getEndPointUrl(), that.getEndPointUrl()) &&
                Objects.equals(getUsername(), that.getUsername()) &&
                Objects.equals(getPassword(), that.getPassword()) &&
                Objects.equals(getCompanyCode(), that.getCompanyCode()) &&
                Objects.equals(getActive(), that.getActive()) &&
                Objects.equals(getIsActive(), that.getIsActive()) &&
                getType() == that.getType();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getCodeAntiTheftService(), getName(), getBusinessName(), getSupplierCode(), getEmail(), getPhoneNumber(), getProviderType(), getTimeOutInSeconds(), getEndPointUrl(), getUsername(), getPassword(), getCompanyCode(), getActive(), getIsActive(), getType());
    }
}
