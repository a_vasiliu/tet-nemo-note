package com.doing.nemo.claims.entity.enumerated;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;

public enum EventTypeEnum implements Serializable {
    COMPLAINT_INSERTION("complaint_insertion"),
    COMPLAINT_VALIDATION("complaint_validation"),
    COMPLAINT_REJECTED("complaint_rejected"),
    INCOMPLETE("incomplete"),
    ENTRUST("entrust"),
    SEND_WITH_SEQUEL("send_with_sequel"),
    PARTIAL_REFUND("partial_refund"),
    REFUNDED("refunded"),
    TO_ENTRUSTED("to_entrusted"),
    TAKE_IN_CHARGE_INCOMPLETE("take_in_charge_incomplete"),
    SEQUEL_INCOMPLETE("sequel_incomplete"),
    REQUIRED_INFORMATION_INCOMPLETE("required_information_incomplete"),
    TAKE_IN_CHARGE_VALIDATED("take_in_charge_validated"),
    ACCOUNTING("accounting"),
    ENTRUST_TO_THE_LEGAL("entrust_to_the_legal"),
    ENTRUST_TO_THE_COMPANY("entrust_to_the_company"),
    ENTRUST_TO_THE_CLAIMS_MANAGER("entrust_to_the_claims_manager"),
    SEND_TO_CLIENT("send_to_client"),
    CLOSING_WITHOUT_SEQUEL("closing_without_sequel"),
    NOTE("note"),
    NOTIFYING_VARIATION_PO("notifying_variation_po"),
    TAKE_IN_CHARGE_THEFT_NOT_USED("take_in_charge_theft_not_used"),
    TAKE_IN_CHARGE_THEFT_AND_DISCOVERY_NOT_USED("take_in_charge_theft_and_discovery_not_used"),
    TAKE_IN_CHARGE_VALIDATED_THEFT("take_in_charge_validated_theft"),
    TAKE_IN_CHARGE_VALIDATED_THEFT_AND_DISCOVERY("take_in_charge_validated_theft_and_discovery"),
    DISCOVERED_VEHICLE_WITHIN_60GG("discovered_vehicle_within_60gg"),
    TASK_COLLECTION_VEHICLE("task_collection_vehicle"),
    POSSESION_LOSS_VEHICLE("possesion_loss_vehicle"),
    REENTRY_IN_POSSESSION_VEHICLE("reentry_in_possession_vehicle"),
    DOCUMENTATION_REQUEST_THEFT("documentation_request_theft"),
    PRINT_DSAN_PP("print_dsan_pp"),
    PRINT_DSAN_RP("print_dsan_rp"),
    CONFIRM_SEQUEL_WITHOUT_NOTIFICATION("confirm_sequel_without_notification"),
    CONFIRM_SEQUEL_WITH_NOTIFICATION("confirm_sequel_with_notification"),
    DISCOVERED_VEHICLE_BEYOND_60GG("discovered_vehicle_beyond_60gg"),
    ENTRUST_PAI("entrust_pai"),
    INCOMPLETE_FCM("incomplete_fcm"),
    INCOMPLETE_FNI("incomplete_fni"),
    TAKE_IN_CHARGE_INCOMPLETE_FCM("take_in_charge_incomplete_fcm"),
    TAKE_IN_CHARGE_INCOMPLETE_FNI("take_in_charge_incomplete_fni"),
    EDIT_PRACTICE_DATA("edit_practice_data"),
    EDIT_POLICY_DATA("edit_policy_data"),
    AUTOMATIC_CLOSING_COMPLAINT("automatic_closing_complaint"),
    POLICE_EXPIRATION_NOTICE("police_expiration_notice"),
    COMPLAINT_INSERTION_REPAIR("complaint_insertion_repair"),
    OPERATOR_ASSIGNMENT_REPAIR("operator_assignment_repair"),
    PRACTICAL_DATA_VARIATION_REPAIR("practical_data_variation_repair"),
    FLOW_VARIATION_REPAIR("flow_variation_repair"),
    STATUS_VARIATION_REPAIR("status_variation_repair"),
    MANAGEMENT_VARIATION_REPAIR("management_variation_repair"),
    COUNTERPARTY_CALL_REPAIR("counterparty_call_repair"),
    GENERIC_COUNTERPARTY_DRIVER_COMUNICATION("generic_counterparty_driver_comunication"),
    COMUNICATION_REPAIRER_COUNTERPARTY("comunication_repairer_counterparty"),
    GENERIC_REPAIRER_COMUNICATION("generic_repairer_comunication"),
    COMUNICATION_REPAIRER_CANALIZATION("comunication_repairer_canalization"),
    GENERIC_MANAGER_COMUNICATION("generic_manager_comunication"),
    GENERIC_LEGAL_COMUNICATION("generic_legal_comunication"),
    GENERIC_COUNTERPARTY_INSURED_COMUNICATION("generic_counterparty_insured_comunication"),
    IMPORT_FRANCHISE_1("import_franchise_1"),
    IMPORT_FRANCHISE_2("import_franchise_2"),
    IMPORT_NUMBER_SX("import_number_sx"),
    LEGAL_COMUNICATION("legal_comunication"),
    INCIDENT_SERVICE_INSERT("incident_service_insert"),
    INCIDENT_SERVICE_UPDATE("incident_service_update"),
    INCIDENT_SERVICE("incident_service"),
    ENTRUSTED_DOWNLOAD("entrusted_download"),
    AUTHORITY_GLASS_ASSOCIATION("authority_glass_association"),
    AUTHORITY_ASSOCIATION("authority_association"),
    AUTHORITY_DISASSOCIATION("authority_disassociation"),
    AUTHORITY_GLASS_DISASSOCIATION("authority_glass_disassociation"),
    FINDING("finding"),
    COMPLAINT_INSERTION_INCOMPLETE("complaint_insertion_incomplete");
    private String eventType;

    private static Logger LOGGER = LoggerFactory.getLogger(EventTypeEnum.class);

    private EventTypeEnum(String eventType) {
        this.eventType = eventType;
    }

    @JsonCreator
    public static EventTypeEnum create(String eventType) {

        eventType = eventType.replace(" - ", "_");
        eventType = eventType.replace('-', '_');
        eventType = eventType.replace(' ', '_');

        if (eventType != null) {
            for (EventTypeEnum val : EventTypeEnum.values()) {
                if (eventType.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        LOGGER.debug("Bad parameters exception. Enum class " + EventTypeEnum.class.getSimpleName() + " doesn't accept this value: " + eventType);
        throw new BadParametersException("Bad parameters exception. Enum class " + EventTypeEnum.class.getSimpleName() + " doesn't accept this value: " + eventType);
    }

    public String getValue() {
        return this.eventType.toUpperCase();
    }

    @JsonValue
    public String toValue() {
        for (EventTypeEnum val : EventTypeEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }
}
