package com.doing.nemo.claims.entity.enumerated.AuthorityEnum;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;

public enum AuthorityTypeEnum implements Serializable {
    CARBODY("carbody"),
    ADMINISTRATION("administration"),
    GLASS("glass"),
    MECHANIC("mechanic"),
    OTHER("other");

    private static Logger LOGGER = LoggerFactory.getLogger(AuthorityTypeEnum.class);
    private String authorityTypeEnum;

    private AuthorityTypeEnum(String authorityTypeEnum) {
        this.authorityTypeEnum = authorityTypeEnum;
    }

    @JsonCreator
    public static AuthorityTypeEnum create(String authorityTypeEnum) {

        authorityTypeEnum = authorityTypeEnum.replace(" - ", "_");
        authorityTypeEnum = authorityTypeEnum.replace('-', '_');
        authorityTypeEnum = authorityTypeEnum.replace(' ', '_');

        if (authorityTypeEnum != null) {
            for (AuthorityTypeEnum val : AuthorityTypeEnum.values()) {
                if (authorityTypeEnum.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        LOGGER.debug("Bad parameters exception. Enum class " + AuthorityTypeEnum.class.getSimpleName() + " doesn't accept this value: " + authorityTypeEnum);
        throw new BadParametersException("Bad parameters exception. Enum class " + AuthorityTypeEnum.class.getSimpleName() + " doesn't accept this value: " + authorityTypeEnum);
    }

    public String getValue() {
        return this.authorityTypeEnum.toUpperCase();
    }

    @JsonValue
    public String toValue() {
        for (AuthorityTypeEnum val : AuthorityTypeEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }
}
