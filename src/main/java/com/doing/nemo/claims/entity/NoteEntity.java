package com.doing.nemo.claims.entity;

import com.doing.nemo.claims.entity.enumerated.NotesEnum.NotesTypeEnum;
import com.fasterxml.jackson.annotation.*;
import org.hibernate.annotations.DynamicInsert;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "hibernate_lazy_initializer", "handler"})
@Table(name = "note")
public class NoteEntity implements Serializable {

    @Id
    @Column(name = "note_id")
    private String noteId;

    @Column(name = "title")
    private String noteTitle;

    @Column(name = "note_type")
    private NotesTypeEnum noteType;

    @Column(name = "is_important")
    private Boolean isImportant;

    @Column(name = "description")
    private String description;

    @Column(name = "created_at")
    private Date createdAt;

    @Column(name = "user_id")
    private String userId;

    @Column(name = "hidden")
    private Boolean hidden = false;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "claims_id")
    private ClaimsNewEntity claims;

    public NoteEntity() {

    }

    public NoteEntity(String noteTitle, NotesTypeEnum noteType, String noteId, Boolean isImportant, String description, Date createdAt, String userId, Boolean hidden, ClaimsNewEntity claims) {
        this.noteTitle = noteTitle;
        this.noteType = noteType;
        this.noteId = noteId;
        this.isImportant = isImportant;
        this.description = description;
        this.createdAt = createdAt;
        this.userId = userId;
        this.hidden = hidden;
        this.claims = claims;
    }

    public String getNoteId() {
        return noteId;
    }

    public void setNoteId(String noteId) {
        this.noteId = noteId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @JsonIgnore
    public Boolean getImportant() {
        return isImportant;
    }

    public void setImportant(Boolean important) {
        isImportant = important;
    }

    public String getNoteTitle() {
        return noteTitle;
    }

    public void setNoteTitle(String noteTitle) {
        this.noteTitle = noteTitle;
    }

    public NotesTypeEnum getNoteType() {
        return noteType;
    }

    public void setNoteType(NotesTypeEnum noteType) {
        this.noteType = noteType;
    }

    public Date getCreatedAt() {
        if(createdAt == null){
            return null;
        }
        return (Date)createdAt.clone();
    }

    public void setCreatedAt(Date createdAt) {
        if(createdAt != null)
        {
            this.createdAt = (Date)createdAt.clone();
        } else {
            this.createdAt = null;
        }
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Boolean getHidden() {
        return hidden;
    }

    @JsonSetter
    public void setHidden(Boolean hidden) {
        if (hidden != null) this.hidden = hidden;
    }

    public ClaimsNewEntity getClaims() {
        return claims;
    }

    public void setClaims(ClaimsNewEntity claims) {
        this.claims = claims;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("NoteEntity{");
        sb.append("noteId='").append(noteId).append('\'');
        sb.append(", noteTitle='").append(noteTitle).append('\'');
        sb.append(", noteType=").append(noteType);
        sb.append(", isImportant=").append(isImportant);
        sb.append(", description='").append(description).append('\'');
        sb.append(", createdAt=").append(createdAt);
        sb.append(", userId='").append(userId).append('\'');
        sb.append(", hidden=").append(hidden);
        sb.append(", claims=").append(claims.getId());
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NoteEntity that = (NoteEntity) o;
        return Objects.equals(noteId, that.noteId) &&
                Objects.equals(noteTitle, that.noteTitle) &&
                noteType == that.noteType &&
                Objects.equals(isImportant, that.isImportant) &&
                Objects.equals(description, that.description) &&
                Objects.equals(createdAt, that.createdAt) &&
                Objects.equals(userId, that.userId) &&
                Objects.equals(hidden, that.hidden) &&
                Objects.equals(claims, that.claims);
    }

    @Override
    public int hashCode() {
        return Objects.hash(noteId, noteTitle, noteType, isImportant, description, createdAt, userId, hidden, claims);
    }
}
