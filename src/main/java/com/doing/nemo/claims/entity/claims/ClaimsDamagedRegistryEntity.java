package com.doing.nemo.claims.entity.claims;

import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.ClaimsNewEntity;
import com.doing.nemo.claims.entity.enumerated.AntitheftEnum.AntitheftTypeEnum;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.Date;

@JsonIgnoreProperties({"hibernateLazyInitializer", "hibernate_lazy_initializer", "handler"})
@Entity
@Table(name = "claims_damaged_registry")
public class ClaimsDamagedRegistryEntity implements Serializable {

    @Id
    @Column(name = "id", updatable = false, nullable = false)
    private String id;

    @Column(name="type")
    @Enumerated(EnumType.STRING)
    private AntitheftTypeEnum type;

    @Column(name="id_transaction")
    private Long idTransaction;

    @Column(name="cod_imei")
    private String codImei;

    @Column(name="cod_pack")
    private String codPack;

    @Column(name="cod_provider")
    private Integer codProvider;

    @Column(name="provider")
    private String provider;

    @Column(name="date_activation")
    private Instant dateActivation;

    @Column(name="date_end")
    private Instant dateEnd;

    @Column(name="cod_serialnumber")
    private String codSerialnumber;

    @Column(name="voucher_id")
    private Integer voucherId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "claim_id")
    @JsonBackReference(value = "claims-damaged-registry")
    private ClaimsNewEntity claim;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public AntitheftTypeEnum getType() {
        return type;
    }

    public void setType(AntitheftTypeEnum type) {
        this.type = type;
    }

    public Long getIdTransaction() {
        return idTransaction;
    }

    public void setIdTransaction(Long idTransaction) {
        this.idTransaction = idTransaction;
    }

    public String getCodImei() {
        return codImei;
    }

    public void setCodImei(String codImei) {
        this.codImei = codImei;
    }

    public String getCodPack() {
        return codPack;
    }

    public void setCodPack(String codPack) {
        this.codPack = codPack;
    }

    public Integer getCodProvider() {
        return codProvider;
    }

    public void setCodProvider(Integer codProvider) {
        this.codProvider = codProvider;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public Instant getDateActivation() {
        return dateActivation;
    }

    public void setDateActivation(Instant dateActivation) {
        this.dateActivation = dateActivation;
    }

    public Instant getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Instant dateEnd) {
        this.dateEnd = dateEnd;
    }

    public String getCodSerialnumber() {
        return codSerialnumber;
    }

    public void setCodSerialnumber(String codSerialnumber) {
        this.codSerialnumber = codSerialnumber;
    }

    public Integer getVoucherId() {
        return voucherId;
    }

    public void setVoucherId(Integer voucherId) {
        this.voucherId = voucherId;
    }

    public ClaimsNewEntity getClaim() {
        return claim;
    }

    public void setClaim(ClaimsNewEntity claim) {
        this.claim = claim;
    }

    @Override
    public String toString() {
        return "ClaimsDamagedRegistryEntity{" +
                "id='" + id + '\'' +
                ", type=" + type +
                ", idTransaction=" + idTransaction +
                ", codImei='" + codImei + '\'' +
                ", codPack='" + codPack + '\'' +
                ", codProvider=" + codProvider +
                ", provider='" + provider + '\'' +
                ", dateActivation=" + dateActivation +
                ", dateEnd=" + dateEnd +
                ", codSerialnumber='" + codSerialnumber + '\'' +
                ", voucherId=" + voucherId +
                '}';
    }
}
