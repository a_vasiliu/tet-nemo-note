package com.doing.nemo.claims.entity.settings;

import com.doing.nemo.claims.entity.enumerated.PolicyEnum.PolicyTypeEnum;
import com.doing.nemo.claims.entity.jsonb.dataType.JsonDataAttachmentListType;
import com.doing.nemo.claims.entity.jsonb.forms.jsonbForms.Attachment;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "insurance_policy")
@TypeDefs({
        @TypeDef(name = "JsonDataAttachmentListType", typeClass = JsonDataAttachmentListType.class)
})
@JsonIgnoreProperties({"hibernateLazyInitializer", "hibernate_lazy_initializer", "handler"})
public class InsurancePolicyEntity implements Serializable {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )

    @Type(type = "org.hibernate.type.PostgresUUIDType")
    private UUID id;

    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private PolicyTypeEnum type;

    @Column(name = "description")
    private String description;

    @Column(name = "number_policy")
    private String numberPolicy;

    @Column(name = "insurance_policy_id", unique = true, insertable = false, updatable = false)
    private Long insurancePolicyId;

    @OneToOne
    @JoinColumn(name = "insurance_company")
    private InsuranceCompanyEntity insuranceCompanyEntity;

    @OneToOne
    @JoinColumn(name = "insurance_manager")
    private InsuranceManagerEntity insuranceManagerEntity;

    @Column(name = "client_code")
    private Long clientCode;

    @Column(name = "client")
    private String client;

    @Column(name = "annotations")
    private String annotations;

    @Column(name = "beginning_validity")
    private Instant beginningValidity;

    @Column(name = "end_validity")
    private Instant endValidity;

    @Column(name = "book_register")
    private Boolean bookRegister;

    @Column(name = "book_register_code")
    private String bookRegisterCode;

    @Column(name = "last_renewal_notification")
    private Instant lastRenewalNotification;

    @Column(name = "renewal_notification")
    private Boolean renewalNotification;

    @OneToMany(
            cascade = CascadeType.ALL
    )
    @JoinColumn(name = "insurance_policy_id")
    @Column(name = "risk")
    private List<RiskEntity> riskEntityList;

    @Type(type = "JsonDataAttachmentListType")
    @Column(name = "attachment")
    private List<Attachment> attachmentList;

    @OneToMany(
            cascade = CascadeType.ALL
    )
    @JoinColumn(name = "insurance_policy_id")
    @Column(name = "event")
    private List<EventEntity> eventEntityList;

    @Column(name = "is_active")
    private Boolean isActive;

    @Column(name = "franchise")
    private Double franchise;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public PolicyTypeEnum getType() {
        return type;
    }

    public void setType(PolicyTypeEnum type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNumberPolicy() {
        return numberPolicy;
    }

    public void setNumberPolicy(String numberPolicy) {
        this.numberPolicy = numberPolicy;
    }

    public Long getInsurancePolicyId() {
        return insurancePolicyId;
    }

    public void setInsurancePolicyId(Long insurancePolicyId) {
        this.insurancePolicyId = insurancePolicyId;
    }

    public InsuranceCompanyEntity getInsuranceCompanyEntity() {
        return insuranceCompanyEntity;
    }

    public void setInsuranceCompanyEntity(InsuranceCompanyEntity insuranceCompanyEntity) {
        this.insuranceCompanyEntity = insuranceCompanyEntity;
    }

    public InsuranceManagerEntity getInsuranceManagerEntity() {
        return insuranceManagerEntity;
    }

    public void setInsuranceManagerEntity(InsuranceManagerEntity insuranceManagerEntity) {
        this.insuranceManagerEntity = insuranceManagerEntity;
    }

    public Long getClientCode() {
        return clientCode;
    }

    public void setClientCode(Long clientCode) {
        this.clientCode = clientCode;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getAnnotations() {
        return annotations;
    }

    public void setAnnotations(String annotations) {
        this.annotations = annotations;
    }

    public Instant getBeginningValidity() {
        return beginningValidity;
    }

    public void setBeginningValidity(Instant beginningValidity) {
        this.beginningValidity = beginningValidity;
    }

    public Instant getEndValidity() {
        return endValidity;
    }

    public void setEndValidity(Instant endValidity) {
        this.endValidity = endValidity;
    }

    public Boolean getBookRegister() {
        return bookRegister;
    }

    public void setBookRegister(Boolean bookRegister) {
        this.bookRegister = bookRegister;
    }

    public String getBookRegisterCode() {
        return bookRegisterCode;
    }

    public void setBookRegisterCode(String bookRegisterCode) {
        this.bookRegisterCode = bookRegisterCode;
    }

    public Instant getLastRenewalNotification() {
        return lastRenewalNotification;
    }

    public void setLastRenewalNotification(Instant lastRenewalNotification) {
        this.lastRenewalNotification = lastRenewalNotification;
    }

    public Boolean getRenewalNotification() {
        return renewalNotification;
    }

    public void setRenewalNotification(Boolean renewalNotification) {
        this.renewalNotification = renewalNotification;
    }

    public List<RiskEntity> getRiskEntityList() {
        return riskEntityList;
    }

    public void setRiskEntityList(List<RiskEntity> riskEntityList) {
        this.riskEntityList = riskEntityList;
    }

    public List<Attachment> getAttachmentList() {
        if(attachmentList == null){
            return null;
        }
        return new ArrayList<>(attachmentList);
    }

    public void setAttachmentList(List<Attachment> attachmentList) {
        if(attachmentList != null)
        {
            this.attachmentList = new ArrayList<>(attachmentList);
        } else {
            this.attachmentList = null;
        }
    }

    public List<EventEntity> getEventEntityList() {
        return eventEntityList;
    }

    public void setEventEntityList(List<EventEntity> eventEntityList) {
        this.eventEntityList = eventEntityList;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public Double getFranchise() {
        return franchise;
    }

    public void setFranchise(Double franchise) {
        this.franchise = franchise;
    }

    @Override
    public String toString() {
        return "InsurancePolicyEntity{" +
                "id=" + id +
                ", type=" + type +
                ", description='" + description + '\'' +
                ", numberPolicy='" + numberPolicy + '\'' +
                ", insurancePolicyId=" + insurancePolicyId +
                ", insuranceCompanyEntity=" + insuranceCompanyEntity +
                ", insuranceManagerEntity=" + insuranceManagerEntity +
                ", clientCode=" + clientCode +
                ", client='" + client + '\'' +
                ", annotations='" + annotations + '\'' +
                ", beginningValidity=" + beginningValidity +
                ", endValidity=" + endValidity +
                ", bookRegister=" + bookRegister +
                ", bookRegisterCode='" + bookRegisterCode + '\'' +
                ", lastRenewalNotification=" + lastRenewalNotification +
                ", renewalNotification=" + renewalNotification +
                ", riskEntityList=" + riskEntityList +
                ", attachmentList=" + attachmentList +
                ", eventEntityList=" + eventEntityList +
                ", isActive=" + isActive +
                ", franchise=" + franchise +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InsurancePolicyEntity that = (InsurancePolicyEntity) o;
        return Objects.equals(getId(), that.getId()) &&
                getType() == that.getType() &&
                Objects.equals(getDescription(), that.getDescription()) &&
                Objects.equals(getNumberPolicy(), that.getNumberPolicy()) &&
                Objects.equals(getInsurancePolicyId(), that.getInsurancePolicyId()) &&
                Objects.equals(getInsuranceCompanyEntity(), that.getInsuranceCompanyEntity()) &&
                Objects.equals(getInsuranceManagerEntity(), that.getInsuranceManagerEntity()) &&
                Objects.equals(getClientCode(), that.getClientCode()) &&
                Objects.equals(getClient(), that.getClient()) &&
                Objects.equals(getAnnotations(), that.getAnnotations()) &&
                Objects.equals(getBeginningValidity(), that.getBeginningValidity()) &&
                Objects.equals(getEndValidity(), that.getEndValidity()) &&
                Objects.equals(getBookRegister(), that.getBookRegister()) &&
                Objects.equals(getBookRegisterCode(), that.getBookRegisterCode()) &&
                Objects.equals(getLastRenewalNotification(), that.getLastRenewalNotification()) &&
                Objects.equals(getRenewalNotification(), that.getRenewalNotification()) &&
                Objects.equals(getRiskEntityList(), that.getRiskEntityList()) &&
                Objects.equals(getAttachmentList(), that.getAttachmentList()) &&
                Objects.equals(getEventEntityList(), that.getEventEntityList()) &&
                Objects.equals(getActive(), that.getActive()) &&
                Objects.equals(getFranchise(), that.getFranchise());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getType(), getDescription(), getNumberPolicy(), getInsurancePolicyId(), getInsuranceCompanyEntity(), getInsuranceManagerEntity(), getClientCode(), getClient(), getAnnotations(), getBeginningValidity(), getEndValidity(), getBookRegister(), getBookRegisterCode(), getLastRenewalNotification(), getRenewalNotification(), getRiskEntityList(), getAttachmentList(), getEventEntityList(), getActive(), getFranchise());
    }
}
