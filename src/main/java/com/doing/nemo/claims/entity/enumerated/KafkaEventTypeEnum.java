package com.doing.nemo.claims.entity.enumerated;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;

public enum KafkaEventTypeEnum implements Serializable {
    THEFT("theft"),
    POSSESSION_LOSS("possession_loss"),
    FINDING("finding"),
    REMATRICULATION("rematriculation"),
    RECALL_CAMPAIGN("recall_campaign"),
    MAINTENANCE("maintenance"),
    WORKING_CANCELLATION("working_cancellation"),
    WRECK_OR_EXPIRED("wreck_or_expired"),
    WRECK_OR_EXPIRED_CANCELLATION("wreck_or_expired_cancellation"),
    EXPORT_CLAIMS("export_claims");

    private String kafkaEventEnum;

    private static Logger LOGGER = LoggerFactory.getLogger(KafkaEventTypeEnum.class);

    private KafkaEventTypeEnum(String kafkaEventEnum) {
        this.kafkaEventEnum = kafkaEventEnum;
    }

    public String getValue() {
        return this.kafkaEventEnum.toUpperCase();
    }

    @JsonCreator
    public static KafkaEventTypeEnum create(String kafkaEventEnum) {

        kafkaEventEnum = kafkaEventEnum.replace(" - ", "_");
        kafkaEventEnum = kafkaEventEnum.replace('-', '_');
        kafkaEventEnum = kafkaEventEnum.replace(' ', '_');

        if (kafkaEventEnum != null) {
            for (KafkaEventTypeEnum val : KafkaEventTypeEnum.values()) {
                if (kafkaEventEnum.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        LOGGER.debug("Bad parameters exception. Enum class " + KafkaEventTypeEnum.class.getSimpleName() + " doesn't accept this value: " + kafkaEventEnum);
        throw new BadParametersException("Bad parameters exception. Enum class " + KafkaEventTypeEnum.class.getSimpleName() + " doesn't accept this value: " + kafkaEventEnum);
    }

    @JsonValue
    public String toValue() {
        for (KafkaEventTypeEnum val : KafkaEventTypeEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }

}
