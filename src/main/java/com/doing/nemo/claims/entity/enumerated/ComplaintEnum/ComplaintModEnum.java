package com.doing.nemo.claims.entity.enumerated.ComplaintEnum;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;

public enum ComplaintModEnum implements Serializable {
    CAI("cai"),
    CAI_LIGHT("cai_light"),
    LIT("lit"),
    ARD("ard"),
    NTW("ntw"),
    CAL("cal"),
    GEF("gef"),
    MODULO_MANAGER("modulo_manager"),
    CAM("cam"),
    ARM("arm"),
    GEM("gem"),
    NTM("ntm"),
    CLM("clm"),
    GFM("gfm"),
    GEN("gen");

    private static Logger LOGGER = LoggerFactory.getLogger(ComplaintModEnum.class);
    private String modEntity;

    private ComplaintModEnum(String modEntity) {
        this.modEntity = modEntity;
    }

    @JsonCreator
    public static ComplaintModEnum create(String modEntity) {

        modEntity = modEntity.replace(" - ", "_");
        modEntity = modEntity.replace('-', '_');
        modEntity = modEntity.replace(' ', '_');

        if (modEntity != null) {
            for (ComplaintModEnum val : ComplaintModEnum.values()) {
                if (modEntity.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        LOGGER.debug("Bad parameters exception. Enum class " + ComplaintModEnum.class.getSimpleName() + " doesn't accept this value: " + modEntity);
        throw new BadParametersException("Bad parameters exception. Enum class " + ComplaintModEnum.class.getSimpleName() + " doesn't accept this value: " + modEntity);
    }

    public String getValue() {
        return this.modEntity.toUpperCase();
    }

    @JsonValue
    public String toValue() {
        for (ComplaintModEnum val : ComplaintModEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }

}
