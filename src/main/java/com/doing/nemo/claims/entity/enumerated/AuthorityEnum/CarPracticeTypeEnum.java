package com.doing.nemo.claims.entity.enumerated.AuthorityEnum;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;

public enum CarPracticeTypeEnum implements Serializable {
    RELEASE_FROM_SEIZURE("release_from_seizure"),
    RECOVERY("recovery");


    private String practiceTypeEnum;

    private static Logger LOGGER = LoggerFactory.getLogger(CarPracticeTypeEnum.class);

    private CarPracticeTypeEnum(String practiceTypeEnum) {
        this.practiceTypeEnum = practiceTypeEnum;
    }

    public String getValue() {
        return this.practiceTypeEnum.toUpperCase();
    }

    @JsonCreator
    public static CarPracticeTypeEnum create(String practiceTypeEnum) {

        practiceTypeEnum = practiceTypeEnum.replace(" - ", "_");
        practiceTypeEnum = practiceTypeEnum.replace('-', '_');
        practiceTypeEnum = practiceTypeEnum.replace(' ', '_');

        if (practiceTypeEnum != null) {
            for (CarPracticeTypeEnum val : CarPracticeTypeEnum.values()) {
                if (practiceTypeEnum.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        LOGGER.debug("Bad parameters exception. Enum class " + CarPracticeTypeEnum.class.getSimpleName() + " doesn't accept this value: " + practiceTypeEnum);
        throw new BadParametersException("Bad parameters exception. Enum class " + CarPracticeTypeEnum.class.getSimpleName() + " doesn't accept this value: " + practiceTypeEnum);
    }

    @JsonValue
    public String toValue() {
        for (CarPracticeTypeEnum val : CarPracticeTypeEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }
}
