package com.doing.nemo.claims.entity.enumerated.AuthorityEnum;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;

public enum AuthorityStatusEnum implements Serializable {
    WAITING_FOR_AUTHORIZATION("waiting_for_authorization"),
    UNAUTHORIZED("unauthorized"),
    AUTHORIZED("authorized");

    private static Logger LOGGER = LoggerFactory.getLogger(AuthorityStatusEnum.class);
    private String authorityStatus;

    private AuthorityStatusEnum(String authorityStatus) {
        this.authorityStatus = authorityStatus;
    }

    @JsonCreator
    public static AuthorityStatusEnum create(String authorityStatus) {

        authorityStatus = authorityStatus.replace(" - ", "_");
        authorityStatus = authorityStatus.replace('-', '_');
        authorityStatus = authorityStatus.replace(' ', '_');

        if (authorityStatus != null) {
            for (AuthorityStatusEnum val : AuthorityStatusEnum.values()) {
                if (authorityStatus.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        LOGGER.debug("Bad parameters exception. Enum class " + AuthorityStatusEnum.class.getSimpleName() + " doesn't accept this value: " + authorityStatus);
        throw new BadParametersException("Bad parameters exception. Enum class " + AuthorityStatusEnum.class.getSimpleName() + " doesn't accept this value: " + authorityStatus);
    }

    public String getValue() {
        return this.authorityStatus.toUpperCase();
    }

    @JsonValue
    public String toValue() {
        for (AuthorityStatusEnum val : AuthorityStatusEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }
}
