package com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbInsuranceCompany;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.UUID;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LegalCost implements Serializable {

    private static final long serialVersionUID = -4472720943450890087L;

    @JsonProperty("service_id")
    private String serviceId;

    @JsonProperty("service")
    private String service;

    @JsonProperty("company_id")
    private String companyId;

    @JsonProperty("company")
    private String company;

    @JsonProperty("tariff")
    private String tariff;

    @JsonProperty("tariff_id")
    private String tariffId;

    @JsonProperty("tariff_code")
    private String tariffCode;

    @JsonProperty("policy_number")
    private String policyNumber;

    @JsonProperty("start_date")
    private String startDate;

    @JsonProperty("end_date")
    private String endDate;

    @JsonProperty("insurance_company_id")
    private UUID insuranceCompanyId;

    @JsonProperty("insurance_policy_id")
    private UUID insurancePolicyId;

    public UUID getInsurancePolicyId() {
        return insurancePolicyId;
    }

    public void setInsurancePolicyId(UUID insurancePolicyId) {
        this.insurancePolicyId = insurancePolicyId;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }


    public String getTariff() {
        return tariff;
    }

    @JsonProperty("tariff_id")
    public String getTariffId() {
        return tariffId;
    }

    @JsonProperty("tarifid")
    public void setTariffId(String tariffId) {
        this.tariffId = tariffId;
    }

    public void setTariff(String tariff) {
        this.tariff = tariff;
    }

    @JsonProperty("tariff_code")
    public String getTariffCode() {
        return tariffCode;
    }

    @JsonProperty("tarifcode")
    public void setTariffCode(String tariffCode) {
        this.tariffCode = tariffCode;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    @JsonProperty("start_date")
    public String getStartDate() {
        return startDate;
    }

    @JsonProperty("startdate")
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    @JsonProperty("end_date")
    public String getEndDate() {
        return endDate;
    }

    @JsonProperty("enddate")
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public UUID getInsuranceCompanyId() {
        return insuranceCompanyId;
    }

    public void setInsuranceCompanyId(UUID insuranceCompanyId) {
        this.insuranceCompanyId = insuranceCompanyId;
    }

    @Override
    public String toString() {
        return "LegalCost{" +
                "serviceId='" + serviceId + '\'' +
                ", service='" + service + '\'' +
                ", companyId='" + companyId + '\'' +
                ", company='" + company + '\'' +
                ", tariff='" + tariff + '\'' +
                ", tariffId='" + tariffId + '\'' +
                ", tariffCode='" + tariffCode + '\'' +
                ", policyNumber='" + policyNumber + '\'' +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", insuranceCompanyId=" + insuranceCompanyId +
                ", insurancePolicyId=" + insurancePolicyId +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        LegalCost legalCost = (LegalCost) o;

        return new EqualsBuilder().append(serviceId, legalCost.serviceId).append(service, legalCost.service)
                .append(companyId, legalCost.companyId).append(company, legalCost.company).append(tariff, legalCost.tariff)
                .append(tariffId, legalCost.tariffId).append(tariffCode, legalCost.tariffCode).append(policyNumber, legalCost.policyNumber)
                .append(startDate, legalCost.startDate).append(endDate, legalCost.endDate).append(insuranceCompanyId, legalCost.insuranceCompanyId)
                .append(insurancePolicyId, legalCost.insurancePolicyId).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(serviceId).append(service)
                .append(companyId).append(company).append(tariff).append(tariffId).append(tariffCode)
                .append(policyNumber).append(startDate).append(endDate).append(insuranceCompanyId).append(insurancePolicyId).toHashCode();
    }
}
