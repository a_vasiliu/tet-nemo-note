package com.doing.nemo.claims.entity.enumerated.AppropriationEnum;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public enum AppropriationTypeEnum implements Serializable {
    APPROPRIATION("appropriation"),
    LOST_OF_POSSESSION("lost_of_possession"),
    MISAPPROPRIATION("misappropriation");

    private static Logger LOGGER = LoggerFactory.getLogger(AppropriationTypeEnum.class);
    private String statusEntity;

    private AppropriationTypeEnum(String statusEntity) {
        this.statusEntity = statusEntity;
    }

    @JsonCreator
    public static AppropriationTypeEnum create(String statusEntity) {

        statusEntity = statusEntity.replace(" - ", "_");
        statusEntity = statusEntity.replace('-', '_');
        statusEntity = statusEntity.replace(' ', '_');

        if (statusEntity != null) {
            for (AppropriationTypeEnum val : AppropriationTypeEnum.values()) {
                if (statusEntity.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        LOGGER.debug("Bad parameters exception. Enum class " + AppropriationTypeEnum.class.getSimpleName() + " doesn't accept this value: " + statusEntity);
        throw new BadParametersException("Bad parameters exception. Enum class " + AppropriationTypeEnum.class.getSimpleName() + " doesn't accept this value: " + statusEntity);
    }

    public static List<AppropriationTypeEnum> getStatusListFromString(List<String> statusListString) {

        List<AppropriationTypeEnum> statusList = new LinkedList<>();
        for (String att : statusListString) {
            statusList.add(AppropriationTypeEnum.create(att));
        }
        return statusList;
    }

    public String getValue() {
        return this.statusEntity.toUpperCase();
    }

    @JsonValue
    public String toValue() {
        for (AppropriationTypeEnum val : AppropriationTypeEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }
}