package com.doing.nemo.claims.entity;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "supplier",
        uniqueConstraints = @UniqueConstraint(
                name = "uc_supplier",
                columnNames = {
                        "cod_supplier"
                }
        )
)
public class SupplierEntity implements Serializable {

    private static final long serialVersionUID = 5816144815969309732L;

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Type(type = "org.hibernate.type.PostgresUUIDType")
    private UUID id;

    @Column(name = "cod_supplier")
    private String codSupplier;

    @Column(name = "name")
    private String name;

    @Column(name = "email")
    private String email;

    @Column(name = "is_active")
    private Boolean isActive;

    public SupplierEntity() {
    }

    public SupplierEntity(String codSupplier, String name, String email, Boolean isActive) {
        this.codSupplier = codSupplier;
        this.name = name;
        this.email = email;
        this.isActive = isActive;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getCodSupplier() {
        return codSupplier;
    }

    public void setCodSupplier(String codSupplier) {
        this.codSupplier = codSupplier;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SupplierEntity that = (SupplierEntity) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getCodSupplier(), that.getCodSupplier()) &&
                Objects.equals(getName(), that.getName()) &&
                Objects.equals(getEmail(), that.getEmail()) &&
                Objects.equals(getActive(), that.getActive());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getCodSupplier(), getName(), getEmail(), getActive());
    }
}
