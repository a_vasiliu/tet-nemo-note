package com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged;

import com.doing.nemo.claims.entity.jsonb.Address;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbDriver.DrivingLicense;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Insured implements Serializable {

    private static final long serialVersionUID = -1233732835120623079L;

    @JsonProperty("customer_id")
    private String customerId;

    @JsonProperty("firstname")
    private String firstname;

    @JsonProperty("lastname")
    private String lastname;

    @JsonProperty("fiscal_code")
    private String fiscalCode;

    @JsonProperty("address")
    private Address address;

    @JsonProperty("driving_license")
    private DrivingLicense drivingLicense;

    @JsonProperty("phone")
    private String phone;

    @JsonProperty("email")
    private String email;

    public Insured() {
    }

    public Insured(String customerId, String firstname, String lastname, String fiscalCode, Address address, DrivingLicense drivingLicense, String phone, String email) {
        this.customerId = customerId;
        this.firstname = firstname;
        this.lastname = lastname;
        this.fiscalCode = fiscalCode;
        this.address = address;
        this.drivingLicense = drivingLicense;
        this.phone = phone;
        this.email = email;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFiscalCode() {
        return fiscalCode;
    }

    public void setFiscalCode(String fiscalCode) {
        this.fiscalCode = fiscalCode;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public DrivingLicense getDrivingLicense() {
        return drivingLicense;
    }

    public void setDrivingLicense(DrivingLicense drivingLicense) {
        this.drivingLicense = drivingLicense;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Insured{" +
                "firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", fiscalCode='" + fiscalCode + '\'' +
                ", address=" + address +
                ", drivingLicense=" + drivingLicense +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Insured insured = (Insured) o;

        return new EqualsBuilder().append(customerId, insured.customerId).append(firstname, insured.firstname)
                .append(lastname, insured.lastname).append(fiscalCode, insured.fiscalCode).append(address, insured.address)
                .append(drivingLicense, insured.drivingLicense).append(phone, insured.phone).append(email, insured.email).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(customerId).append(firstname)
                .append(lastname).append(fiscalCode).append(address).append(drivingLicense).append(phone).append(email).toHashCode();
    }
}
