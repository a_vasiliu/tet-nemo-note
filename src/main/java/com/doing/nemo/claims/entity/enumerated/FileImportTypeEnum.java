package com.doing.nemo.claims.entity.enumerated;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;

public enum FileImportTypeEnum implements Serializable {
    FRANCHISE_1("franchise_1"), //FRANCHIGIA_1
    FRANCHISE_2("franchise_2"), //FRANCHIGIA_2
    NUMBER_SX("number_sx"), //NUMERO_SX
    THEFT("thef"), //FURTI
    APPROPRIATION("appropriation"), //SEQUESTRI
    PRACTICE_CAR("practice_car"), //PRATICHE_AUTO
    MASSIVE_ENTRUST("massive_entrust"), //AFFIDO_MASSIVO
    ALLOWANCE("allowance"), //ASSEGNO
    BANK_TRANSFER("bank_transfer"); //BONIFICO

    private static Logger LOGGER = LoggerFactory.getLogger(FileImportTypeEnum.class);
    private String fileImportType;

    private FileImportTypeEnum(String fileImportType) {
        this.fileImportType = fileImportType;
    }

    @JsonCreator
    public static FileImportTypeEnum create(String fileImportType) {

        fileImportType = fileImportType.replace(" - ", "_");
        fileImportType = fileImportType.replace('-', '_');
        fileImportType = fileImportType.replace(' ', '_');

        if (fileImportType != null) {
            for (FileImportTypeEnum val : FileImportTypeEnum.values()) {
                if (fileImportType.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        LOGGER.debug("Bad parameters exception. Enum class " + FileImportTypeEnum.class.getSimpleName() + " doesn't accept this value: " + fileImportType);
        throw new BadParametersException("Bad parameters exception. Enum class " + FileImportTypeEnum.class.getSimpleName() + " doesn't accept this value: " + fileImportType);
    }

    public String getValue() {
        return this.fileImportType.toUpperCase();
    }

    @JsonValue
    public String toValue() {
        for (FileImportTypeEnum val : FileImportTypeEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }
}
