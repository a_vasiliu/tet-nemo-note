package com.doing.nemo.claims.entity.claims;

import com.doing.nemo.claims.entity.ClaimsNewEntity;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

@JsonIgnoreProperties({"hibernateLazyInitializer", "hibernate_lazy_initializer", "handler"})
@Entity
@Table(name = "claims_from_company")
public class ClaimsFromCompanyEntity implements Serializable {

    @Id
    @Column(name = "id", updatable = false, nullable = false)
    private String id;

    @Column(name = "company")
    private String company;

    @Column(name = "number_sx")
    private String numberSx;

    @Column(name = "type_sx")
    private String typeSx;

    @Column(name = "inspectorate")
    private String inspectorate;

    @Column(name = "expert")
    private String expert;

    @Column(name = "note")
    private String note;

    @Column(name = "dwl_man")
    //@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    private Instant dwlMan;

    @Column(name = "last_update")
    //@JsonFormat(pattern = "yyyy-MM-dd")
    private Instant lastUpdate;

    @Column(name = "status")
    private String status;

    @Column(name = "global_reserve")
    private Double globalReserve;

    @Column(name = "total_paid")
    private Double totalPaid;

    @OneToOne
    @JoinColumn(name = "id")
    @MapsId("id")
    @PrimaryKeyJoinColumn
    @JsonManagedReference(value = "claimsFromCompanyEntity")
    private ClaimsNewEntity claim;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Double getGlobalReserve() {
        return globalReserve;
    }

    public void setGlobalReserve(Double globalReserve) {
        this.globalReserve = globalReserve;
    }

    public Double getTotalPaid() {
        return totalPaid;
    }

    public void setTotalPaid(Double totalPaid) {
        this.totalPaid = totalPaid;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getNumberSx() {
        return numberSx;
    }

    public void setNumberSx(String numberSx) {
        this.numberSx = numberSx;
    }

    public String getTypeSx() {
        return typeSx;
    }

    public void setTypeSx(String typeSx) {
        this.typeSx = typeSx;
    }

    public String getInspectorate() {
        return inspectorate;
    }

    public void setInspectorate(String inspectorate) {
        this.inspectorate = inspectorate;
    }

    public String getExpert() {
        return expert;
    }

    public void setExpert(String expert) {
        this.expert = expert;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Instant getDwlMan() {
        return dwlMan;
    }

    public void setDwlMan(Instant dwlMan) {
        this.dwlMan = dwlMan;
    }

    public Instant getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Instant lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public ClaimsNewEntity getClaim() {
        return claim;
    }

    public void setClaim(ClaimsNewEntity claim) {
        this.claim = claim;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ClaimsFromCompanyEntity{");
        sb.append("id='").append(id).append('\'');
        sb.append(", company='").append(company).append('\'');
        sb.append(", numberSx='").append(numberSx).append('\'');
        sb.append(", typeSx='").append(typeSx).append('\'');
        sb.append(", inspectorate='").append(inspectorate).append('\'');
        sb.append(", expert='").append(expert).append('\'');
        sb.append(", note='").append(note).append('\'');
        sb.append(", dwlMan=").append(dwlMan);
        sb.append(", lastUpdate=").append(lastUpdate);
        sb.append(", status='").append(status).append('\'');
        sb.append(", globalReserve=").append(globalReserve);
        sb.append(", totalPaid=").append(totalPaid);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClaimsFromCompanyEntity that = (ClaimsFromCompanyEntity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(company, that.company) &&
                Objects.equals(numberSx, that.numberSx) &&
                Objects.equals(typeSx, that.typeSx) &&
                Objects.equals(inspectorate, that.inspectorate) &&
                Objects.equals(expert, that.expert) &&
                Objects.equals(note, that.note) &&
                Objects.equals(dwlMan, that.dwlMan) &&
                Objects.equals(lastUpdate, that.lastUpdate) &&
                Objects.equals(status, that.status) &&
                Objects.equals(globalReserve, that.globalReserve) &&
                Objects.equals(totalPaid, that.totalPaid) &&
                Objects.equals(claim, that.claim);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, company, numberSx, typeSx, inspectorate, expert, note, dwlMan, lastUpdate, status, globalReserve, totalPaid, claim);
    }
}
