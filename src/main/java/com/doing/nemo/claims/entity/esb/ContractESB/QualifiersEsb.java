package com.doing.nemo.claims.entity.esb.ContractESB;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class QualifiersEsb implements Serializable {

    private static final long serialVersionUID = -987608038296823969L;

    @JsonProperty("attribute")
    private Long attribute;

    @JsonProperty("attribute_description")
    private String attributeDescription;

    @JsonProperty("value")
    private String value;

    @JsonProperty("value_description")
    private String valueDescription;

    public QualifiersEsb() {
    }

    public Long getAttribute() {
        return attribute;
    }

    public void setAttribute(Long attribute) {
        this.attribute = attribute;
    }

    public String getAttributeDescription() {
        return attributeDescription;
    }

    public void setAttributeDescription(String attributeDescription) {
        this.attributeDescription = attributeDescription;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getValueDescription() {
        return valueDescription;
    }

    public void setValueDescription(String valueDescription) {
        this.valueDescription = valueDescription;
    }

    @Override
    public String toString() {
        return "QualifiersEsb{" +
                "attribute='" + attribute + '\'' +
                ", attribute_description='" + attributeDescription + '\'' +
                ", value='" + value + '\'' +
                ", value_description='" + valueDescription + '\'' +
                '}';
    }
}
