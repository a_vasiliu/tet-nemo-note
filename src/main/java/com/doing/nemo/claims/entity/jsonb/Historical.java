package com.doing.nemo.claims.entity.jsonb;

import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.enumerated.EventTypeEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Historical implements Serializable {

    private static final long serialVersionUID = 809173064960583663L;

    @JsonProperty("event_type")
    private EventTypeEnum eventType;

    @JsonProperty("old_status")
    private ClaimsStatusEnum statusEntityOld;

    @JsonProperty("new_status")
    private ClaimsStatusEnum statusEntityNew;

    @JsonProperty("old_type")
    private DataAccidentTypeAccidentEnum oldType;

    @JsonProperty("new_type")
    private DataAccidentTypeAccidentEnum newType;

    @JsonProperty("update_at")
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    private Date updateAt;

    @JsonProperty("user_id")
    private String userId;

    @JsonProperty("user_name")
    private String userName;

    @JsonProperty("old_flow")
    private String oldFlow;

    @JsonProperty("new_flow")
    private String newFlow;

    @JsonProperty("comunication_description")
    private String comunicationDescription;


    public Historical() {
    }

    public Historical(EventTypeEnum eventType, ClaimsStatusEnum statusEntityOld, ClaimsStatusEnum statusEntityNew, DataAccidentTypeAccidentEnum oldType, DataAccidentTypeAccidentEnum newType, Date updateAt, String userId, String userName, String oldFlow, String newFlow, String comunicationDescription) {
        this.eventType = eventType;
        this.statusEntityOld = statusEntityOld;
        this.statusEntityNew = statusEntityNew;
        this.oldType = oldType;
        this.newType = newType;
        if(updateAt != null)
        {
            this.updateAt = (Date)updateAt.clone();
        }
        this.userId = userId;
        this.userName = userName;
        this.oldFlow = oldFlow;
        this.newFlow = newFlow;
        this.comunicationDescription = comunicationDescription;
    }

    public ClaimsStatusEnum getStatusEntityOld() {
        return statusEntityOld;
    }

    public void setStatusEntityOld(ClaimsStatusEnum statusEntityOld) {
        this.statusEntityOld = statusEntityOld;
    }

    public ClaimsStatusEnum getStatusEntityNew() {
        return statusEntityNew;
    }

    public void setStatusEntityNew(ClaimsStatusEnum statusEntityNew) {
        this.statusEntityNew = statusEntityNew;
    }

    public Date getUpdateAt() {
        if(updateAt == null){
            return null;
        }
        return (Date)updateAt.clone();
    }

    public void setUpdateAt(Date updateAt) {
        if(updateAt != null)
        {
            this.updateAt = (Date)updateAt.clone();
        } else {
            this.updateAt = null;
        }
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public EventTypeEnum getEventType() {
        return eventType;
    }

    public void setEventType(EventTypeEnum eventType) {
        this.eventType = eventType;
    }

    public String getComunicationDescription() {
        return comunicationDescription;
    }

    public void setComunicationDescription(String comunicationDescription) {
        this.comunicationDescription = comunicationDescription;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public DataAccidentTypeAccidentEnum getOldType() {
        return oldType;
    }

    public void setOldType(DataAccidentTypeAccidentEnum oldType) {
        this.oldType = oldType;
    }

    public DataAccidentTypeAccidentEnum getNewType() {
        return newType;
    }

    public void setNewType(DataAccidentTypeAccidentEnum newType) {
        this.newType = newType;
    }

    public String getOldFlow() {
        return oldFlow;
    }

    public void setOldFlow(String oldFlow) {
        this.oldFlow = oldFlow;
    }

    public String getNewFlow() {
        return newFlow;
    }

    public void setNewFlow(String newFlow) {
        this.newFlow = newFlow;
    }

    @Override
    public String toString() {
        return "Historical{" +
                "eventType=" + eventType +
                ", statusEntityOld=" + statusEntityOld +
                ", statusEntityNew=" + statusEntityNew +
                ", oldType=" + oldType +
                ", newType=" + newType +
                ", updateAt=" + updateAt +
                ", userId='" + userId + '\'' +
                ", userName='" + userName + '\'' +
                ", oldFlow='" + oldFlow + '\'' +
                ", newFlow='" + newFlow + '\'' +
                ", comunicationDescription='" + comunicationDescription + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Historical that = (Historical) o;

        return new EqualsBuilder().append(eventType, that.eventType).append(statusEntityOld, that.statusEntityOld)
                .append(statusEntityNew, that.statusEntityNew).append(oldType, that.oldType).append(newType, that.newType)
                .append(updateAt, that.updateAt).append(userId, that.userId).append(userName, that.userName)
                .append(oldFlow, that.oldFlow).append(newFlow, that.newFlow).append(comunicationDescription, that.comunicationDescription).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(eventType).append(statusEntityOld)
                .append(statusEntityNew).append(oldType).append(newType).append(updateAt).append(userId).append(userName)
                .append(oldFlow).append(newFlow).append(comunicationDescription).toHashCode();
    }
}
