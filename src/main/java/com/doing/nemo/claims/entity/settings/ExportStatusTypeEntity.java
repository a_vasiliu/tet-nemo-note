package com.doing.nemo.claims.entity.settings;

import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsFlowEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "export_status_type")
public class ExportStatusTypeEntity implements Serializable {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )

    @Type(type = "org.hibernate.type.PostgresUUIDType")
    private UUID id;

    @Enumerated(EnumType.STRING)
    @Column(name = "status_type")
    private ClaimsStatusEnum statusType;

    @Enumerated(EnumType.STRING)
    @Column(name = "flow_type")
    private ClaimsFlowEnum flowType;

    @OneToOne
    @JoinColumn(name = "dm_system_id", nullable = true)
    private DmSystemsEntity dmSystem;

    @Column(name = "code")
    private String code;

    @Column(name = "description")
    private String description;

    @Column(name = "is_active")
    private Boolean isActive;

    public ExportStatusTypeEntity() {
    }

    public ExportStatusTypeEntity(ClaimsStatusEnum statusType, ClaimsFlowEnum flowType, DmSystemsEntity dmSystem, String code, String description, Boolean isActive) {
        this.statusType = statusType;
        this.flowType = flowType;
        this.dmSystem = dmSystem;
        this.code = code;
        this.description = description;
        this.isActive = isActive;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public ClaimsStatusEnum getStatusType() {
        return statusType;
    }

    public void setStatusType(ClaimsStatusEnum statusType) {
        this.statusType = statusType;
    }

    public ClaimsFlowEnum getFlowType() {
        return flowType;
    }

    public void setFlowType(ClaimsFlowEnum flowType) {
        this.flowType = flowType;
    }

    public DmSystemsEntity getDmSystem() {
        return dmSystem;
    }

    public void setDmSystem(DmSystemsEntity dmSystem) {
        this.dmSystem = dmSystem;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    @Override
    public String toString() {
        return "ExportStatusTypeEntity{" +
                "id=" + id +
                ", statusType=" + statusType +
                ", flowType=" + flowType +
                ", dmSystem=" + dmSystem +
                ", code='" + code + '\'' +
                ", description='" + description + '\'' +
                ", isActive=" + isActive +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ExportStatusTypeEntity that = (ExportStatusTypeEntity) o;
        return Objects.equals(getId(), that.getId()) &&
                getStatusType() == that.getStatusType() &&
                getFlowType() == that.getFlowType() &&
                Objects.equals(getDmSystem(), that.getDmSystem()) &&
                Objects.equals(getCode(), that.getCode()) &&
                Objects.equals(getDescription(), that.getDescription()) &&
                Objects.equals(getActive(), that.getActive());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getStatusType(), getFlowType(), getDmSystem(), getCode(), getDescription(), getActive());
    }
}
