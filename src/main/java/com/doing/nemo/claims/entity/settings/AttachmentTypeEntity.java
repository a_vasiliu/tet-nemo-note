package com.doing.nemo.claims.entity.settings;

import com.doing.nemo.claims.entity.enumerated.AttachmentEnum.GroupsEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsRepairEnum;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "attachment_type")
@JsonIgnoreProperties({"hibernateLazyInitializer", "hibernate_lazy_initializer", "handler"})
public class AttachmentTypeEntity implements Serializable {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Type(type = "org.hibernate.type.PostgresUUIDType")
    private UUID id;

    @Column(name = "name")
    private String name;

    @Enumerated(EnumType.STRING)
    @Column(name = "groups")
    private GroupsEnum groups;

    @Column(name = "is_active")
    private Boolean isActive;

    @Enumerated(EnumType.STRING)
    @Column(name = "type_complaint")
    private ClaimsRepairEnum typeComplaint;

    public AttachmentTypeEntity() {
    }

    public AttachmentTypeEntity(String name, GroupsEnum groups, Boolean isActive) {
        this.name = name;
        this.groups = groups;
        this.isActive = isActive;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public GroupsEnum getGroups() {
        return groups;
    }

    public void setGroups(GroupsEnum groups) {
        this.groups = groups;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public ClaimsRepairEnum getTypeComplaint() {
        return typeComplaint;
    }

    public void setTypeComplaint(ClaimsRepairEnum typeComplaint) {
        this.typeComplaint = typeComplaint;
    }

    @Override
    public String toString() {
        return "AttachmentTypeEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", groups=" + groups +
                ", isActive=" + isActive +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AttachmentTypeEntity that = (AttachmentTypeEntity) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getName(), that.getName()) &&
                getGroups() == that.getGroups() &&
                Objects.equals(getActive(), that.getActive()) &&
                getTypeComplaint() == that.getTypeComplaint();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getGroups(), getActive(), getTypeComplaint());
    }
}
