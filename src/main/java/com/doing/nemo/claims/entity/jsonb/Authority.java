package com.doing.nemo.claims.entity.jsonb;

import com.doing.nemo.claims.entity.enumerated.AuthorityEnum.AuthorityEventTypeEnum;
import com.doing.nemo.claims.entity.enumerated.AuthorityEnum.AuthorityStatusEnum;
import com.doing.nemo.claims.entity.enumerated.AuthorityEnum.AuthorityTypeEnum;
import com.doing.nemo.claims.entity.enumerated.WorkingStatusEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;


@JsonIgnoreProperties(ignoreUnknown = true)
public class Authority implements Serializable {

    private static final long serialVersionUID = -61589083904741009L;

    @JsonProperty("is_invoiced")
    private Boolean isInvoiced = false;

    @JsonProperty("authority_dossier_id")
    private String authorityDossierId;

    @JsonProperty("authority_working_id")
    private String authorityWorkingId;

    @JsonProperty("working_number")
    private String workingNumber;

    @JsonProperty("authority_dossier_number")
    private String authorityDossierNumber;

    @JsonProperty("total")
    private Double total;

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    @JsonProperty("accepting_date")
    private Date acceptingDate;

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    @JsonProperty("authorization_date")
    private Date authorizationDate;

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    @JsonProperty("rejection_date")
    private Date rejectionDate;

    @JsonProperty("rejection")
    private Boolean rejection;

    @JsonProperty("note_rejection")
    private String noteRejection;

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    @JsonProperty("working_created_at")
    private Date workingCreatedAt;

    @JsonProperty("franchise_number")
    private Long numberFranchise;

    @JsonProperty("is_wreck")
    private Boolean isWreck;

    @JsonProperty("wreck_casual")
    private String wreckCasual;

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    @JsonProperty("event_date")
    private Date eventDate;

    @JsonProperty("event_type")
    private AuthorityEventTypeEnum eventType;

    @JsonProperty("po_details")
    private List<PODetails> poDetails;

    @JsonProperty("status")
    private AuthorityStatusEnum status;

    @JsonProperty("type")
    private AuthorityTypeEnum type;

    @JsonProperty("is_not_duplicate")
    private Boolean isNotDuplicate;

    @JsonProperty("oldest")
    private Boolean oldest;

    @JsonProperty("commodity_details")
    private CommodityDetails commodityDetails;

    @JsonProperty("working_status")
    private WorkingStatusEnum workingStatus;

    @JsonProperty("user_details")
    private UserDetails userDetails;

    @JsonProperty("NBV")
    private BigDecimal nbv;

    @JsonProperty("wreck_value")
    private String wreckValue;

    @JsonProperty("claims_linked_size")
    private Long claimsLinkedSize;

    @JsonProperty("historical")
    private List<HistoricalAuthority> historical;

    @JsonProperty("is_msa_downloaded")
    private Boolean isMsaDownloaded;

    @JsonIgnore
    public Boolean getMsaDownloaded() {
        return isMsaDownloaded;
    }

    public void setMsaDownloaded(Boolean msaDownloaded) {
        isMsaDownloaded = msaDownloaded;
    }

    public String getNoteRejection() {
        return noteRejection;
    }

    public void setNoteRejection(String noteRejection) {
        this.noteRejection = noteRejection;
    }

    @JsonIgnore
    public Boolean getInvoiced() {
        return isInvoiced;
    }

    public void setInvoiced(Boolean invoiced) {
        isInvoiced = invoiced;
    }

    public Long getClaimsLinkedSize() {
        return claimsLinkedSize;
    }

    public void setClaimsLinkedSize(Long claimsLinkedSize) {
        this.claimsLinkedSize = claimsLinkedSize;
    }

    public BigDecimal getNbv() {
        return nbv;
    }

    public void setNbv(BigDecimal nbv) {
        this.nbv = nbv;
    }

    public String getWreckValue() {
        return wreckValue;
    }

    public void setWreckValue(String wreckValue) {
        this.wreckValue = wreckValue;
    }

    public UserDetails getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(UserDetails userDetails) {
        this.userDetails = userDetails;
    }

    public WorkingStatusEnum getWorkingStatus() {
        return workingStatus;
    }

    public void setWorkingStatus(WorkingStatusEnum workingStatus) {
        this.workingStatus = workingStatus;
    }

    public String getAuthorityDossierNumber() {
        return authorityDossierNumber;
    }

    public void setAuthorityDossierNumber(String authorityDossierNumber) {
        this.authorityDossierNumber = authorityDossierNumber;
    }

    public CommodityDetails getCommodityDetails() {
        return commodityDetails;
    }

    public void setCommodityDetails(CommodityDetails commodityDetails) {
        this.commodityDetails = commodityDetails;
    }

    @JsonIgnore
    public Boolean getNotDuplicate() {
        return isNotDuplicate;
    }

    public void setNotDuplicate(Boolean notDuplicate) {
        isNotDuplicate = notDuplicate;
    }

    public AuthorityTypeEnum getType() {
        return type;
    }

    public void setType(AuthorityTypeEnum type) {
        this.type = type;
    }

    public AuthorityStatusEnum getStatus() {
        return status;
    }

    public void setStatus(AuthorityStatusEnum status) {
        this.status = status;
    }

    public String getAuthorityDossierId() {
        return authorityDossierId;
    }

    public void setAuthorityDossierId(String authorityDossierId) {
        this.authorityDossierId = authorityDossierId;
    }

    public String getAuthorityWorkingId() {
        return authorityWorkingId;
    }

    public void setAuthorityWorkingId(String authorityWorkingId) {
        this.authorityWorkingId = authorityWorkingId;
    }

    public String getWorkingNumber() {
        return workingNumber;
    }

    public void setWorkingNumber(String workingNumber) {
        this.workingNumber = workingNumber;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Date getAcceptingDate() {
        if(acceptingDate == null){
            return null;
        }
        return (Date)acceptingDate.clone();
    }

    public void setAcceptingDate(Date acceptingDate) {
        if(acceptingDate != null)
        {
            this.acceptingDate = (Date)acceptingDate.clone();
        } else {
            this.acceptingDate = null;
        }
    }

    public Date getAuthorizationDate() {
        if(authorizationDate == null){
            return null;
        }
        return (Date)authorizationDate.clone();
    }

    public void setAuthorizationDate(Date authorizationDate) {
        if(authorizationDate != null)
        {
            this.authorizationDate = (Date)authorizationDate.clone();
        } else {
            this.authorizationDate = null;
        }
    }

    public Date getRejectionDate() {
        if(rejectionDate == null){
            return null;
        }
        return (Date)rejectionDate.clone();
    }

    public void setRejectionDate(Date rejectionDate) {
        if(rejectionDate != null)
        {
            this.rejectionDate =(Date)rejectionDate.clone();
        } else {
            this.rejectionDate = null;
        }
    }

    public Boolean getRejection() {
        return rejection;
    }

    public void setRejection(Boolean rejection) {
        this.rejection = rejection;
    }

    public Date getWorkingCreatedAt() {
        if(workingCreatedAt == null){
            return null;
        }
        return (Date)workingCreatedAt.clone();
    }

    public void setWorkingCreatedAt(Date workingCreatedAt) {
        if(workingCreatedAt != null)
        {
            this.workingCreatedAt = (Date)workingCreatedAt.clone();
        } else {
            this.workingCreatedAt = null;
        }
    }

    public Long getNumberFranchise() {
        return numberFranchise;
    }

    public void setNumberFranchise(Long numberFranchise) {
        this.numberFranchise = numberFranchise;
    }

    public Boolean getWreck() {
        return isWreck;
    }

    public void setWreck(Boolean wreck) {
        isWreck = wreck;
    }

    public String getWreckCasual() {
        return wreckCasual;
    }

    public void setWreckCasual(String wreckCasual) {
        this.wreckCasual = wreckCasual;
    }

    public Date getEventDate() {
        if(eventDate == null){
            return null;
        }
        return (Date)eventDate.clone();
    }

    public void setEventDate(Date eventDate) {
        if(eventDate != null)
        {
            this.eventDate = (Date)eventDate.clone();
        } else {
            this.eventDate = null;
        }
    }

    public AuthorityEventTypeEnum getEventType() {
        return eventType;
    }

    public void setEventType(AuthorityEventTypeEnum eventType) {
        this.eventType = eventType;
    }

    public List<PODetails> getPoDetails() {
        if(poDetails == null){
            return null;
        }
        return new ArrayList<>(poDetails);
    }

    public void setPoDetails(List<PODetails> poDetails) {
        if(poDetails != null)
        {
            this.poDetails = new ArrayList<>(poDetails);
        } else {
            this.poDetails = null;
        }
    }

    public Boolean getOldest() {
        return oldest;
    }

    public void setOldest(Boolean oldest) {
        this.oldest = oldest;
    }

    public List<HistoricalAuthority> getHistorical() {
        if(historical == null){
            return  null;
        }
        return new ArrayList<>(historical);
    }

    public void setHistorical(List<HistoricalAuthority> historical) {
        if(historical != null)
        {
            this.historical = new ArrayList<>(historical);
        } else {
            this.historical = null;
        }
    }

    public void addHistorical(HistoricalAuthority historical) {
        if (this.historical == null) {
            setHistorical(new ArrayList<>());
        }
        this.historical.add(historical);
    }



    @Override
    public String toString() {
        return "Authority{" +
                "isInvoiced=" + isInvoiced +
                ", authorityDossierId='" + authorityDossierId + '\'' +
                ", authorityWorkingId='" + authorityWorkingId + '\'' +
                ", workingNumber='" + workingNumber + '\'' +
                ", authorityDossierNumber='" + authorityDossierNumber + '\'' +
                ", total=" + total +
                ", acceptingDate=" + acceptingDate +
                ", authorizationDate=" + authorizationDate +
                ", rejectionDate=" + rejectionDate +
                ", rejection=" + rejection +
                ", workingCreatedAt=" + workingCreatedAt +
                ", numberFranchise=" + numberFranchise +
                ", isWreck=" + isWreck +
                ", wreckCasual='" + wreckCasual + '\'' +
                ", eventDate=" + eventDate +
                ", eventType='" + eventType + '\'' +
                ", poDetails=" + poDetails +
                ", status=" + status +
                ", type=" + type +
                ", isNotDuplicate=" + isNotDuplicate +
                ", oldest=" + oldest +
                ", commodityDetails=" + commodityDetails +
                ", workingStatus=" + workingStatus +
                ", userDetails=" + userDetails +
                ", nbv=" + nbv +
                ", wreckValue='" + wreckValue + '\'' +
                ", claimsLinkedSize=" + claimsLinkedSize +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Authority authority = (Authority) o;

        return new EqualsBuilder().append(isInvoiced, authority.isInvoiced).append(authorityDossierId, authority.authorityDossierId)
                .append(authorityWorkingId, authority.authorityWorkingId).append(workingNumber, authority.workingNumber)
                .append(authorityDossierNumber, authority.authorityDossierNumber).append(total, authority.total)
                .append(acceptingDate, authority.acceptingDate).append(authorizationDate, authority.authorizationDate)
                .append(rejectionDate, authority.rejectionDate).append(rejection, authority.rejection)
                .append(noteRejection, authority.noteRejection).append(workingCreatedAt, authority.workingCreatedAt)
                .append(numberFranchise, authority.numberFranchise).append(isWreck, authority.isWreck)
                .append(wreckCasual, authority.wreckCasual).append(eventDate, authority.eventDate)
                .append(eventType, authority.eventType).append(poDetails, authority.poDetails)
                .append(status, authority.status).append(type, authority.type).append(isNotDuplicate, authority.isNotDuplicate)
                .append(oldest, authority.oldest).append(commodityDetails, authority.commodityDetails)
                .append(workingStatus, authority.workingStatus).append(userDetails, authority.userDetails)
                .append(nbv, authority.nbv).append(wreckValue, authority.wreckValue).append(claimsLinkedSize, authority.claimsLinkedSize)
                .append(historical, authority.historical).append(isMsaDownloaded, authority.isMsaDownloaded).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(isInvoiced).append(authorityDossierId)
                .append(authorityWorkingId).append(workingNumber).append(authorityDossierNumber).append(total)
                .append(acceptingDate).append(authorizationDate).append(rejectionDate).append(rejection)
                .append(noteRejection).append(workingCreatedAt).append(numberFranchise).append(isWreck)
                .append(wreckCasual).append(eventDate).append(eventType).append(poDetails).append(status)
                .append(type).append(isNotDuplicate).append(oldest).append(commodityDetails).append(workingStatus)
                .append(userDetails).append(nbv).append(wreckValue).append(claimsLinkedSize)
                .append(historical).append(isMsaDownloaded).toHashCode();
    }
}
