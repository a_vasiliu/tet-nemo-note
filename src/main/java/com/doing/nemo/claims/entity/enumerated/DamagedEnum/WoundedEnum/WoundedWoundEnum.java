package com.doing.nemo.claims.entity.enumerated.DamagedEnum.WoundedEnum;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;

public enum WoundedWoundEnum implements Serializable {
    NO("no"),
    MINOR("minor"),
    SERIOUS("serious");

    private static Logger LOGGER = LoggerFactory.getLogger(WoundedWoundEnum.class);
    private String woundEntity;

    private WoundedWoundEnum(String woundEntity) {
        this.woundEntity = woundEntity;
    }

    @JsonCreator
    public static WoundedWoundEnum create(String woundEntity) {

        woundEntity = woundEntity.replace(" - ", "_");
        woundEntity = woundEntity.replace('-', '_');
        woundEntity = woundEntity.replace(' ', '_');

        if (woundEntity != null) {
            for (WoundedWoundEnum val : WoundedWoundEnum.values()) {
                if (woundEntity.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        LOGGER.debug("Bad parameters exception. Enum class " + WoundedWoundEnum.class.getSimpleName() + " doesn't accept this value: " + woundEntity);
        throw new BadParametersException("Bad parameters exception. Enum class " + WoundedWoundEnum.class.getSimpleName() + " doesn't accept this value: " + woundEntity);
    }

    public String getValue() {
        return this.woundEntity.toUpperCase();
    }

    @JsonValue
    public String toValue() {
        for (WoundedWoundEnum val : WoundedWoundEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }
}
