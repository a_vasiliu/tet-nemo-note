package com.doing.nemo.claims.entity.enumerated;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;

public enum RecipientEnum implements Serializable {
    INSERT_CLAIM_USER("insert_claim_user"),
    LEGAL("legal"),
    INSURANCE_OFFICE("insurance_office"),
    INSPECTORATE("inspectorate"),
    COMPANY("company"),
    CLAIMS_MANAGER("claims_manager"),
    CLAIMS_MANAGER_ENTRUSTED("claims_manager_entrusted"),
    INSURED("insured"),
    REPOSITORY("repository"),
    REPOSITORY2("repository2"),
    ARCARI("arcari"),
    OLSA("olsa"),
    VODAFONE("vodafone"),
    PAI_MANAGER("pai_manager"),
    LEGAL_MANAGER("legal_manager"),
    PAI_COMPANY("pai_company"),
    LEGAL_COMPANY("legal_company"),
    SEQUEL("sequel"),
    ACI("aci"),
    ASAT("asat"),
    INSERT_USER_AND_FLEET("insert_user_and_fleet"),
    THEFT_AND_DISCOVERY("theft_and_discovery"),
    WITHDRAWAL_SERVICE_POINT("withdrawal_service_point"),
    USER_INPUT_PRACTICE("user_input_practice"),
    USER_MODIFICATION_PRACTICE("user_modification_practice"),
    OPERATOR_TO_WHOM_THE_PRACTICE("operatore_to_whom_the_practice"),
    DRIVER_COUNTERPARTY("driver_counterparty"),
    INSURED_COUNTERPARTY("insured_counterparty"),
    INSURANCE_COMPANY_COUNTERPARTY("insurance_company_counterparty"),
    PRACTICAL_REPAIRER("practical_repairer"),
    LEGAL_OR_CONSULTANT("legal_or_consultant"),
    CONTACT_CENTER_GROUP("contact_center_group"),
    CLAIMS_MANAGER_REPAIR("claims_manager_repair"),
    DRIVER("driver"),
    CUSTOM("custom");

    private static Logger LOGGER = LoggerFactory.getLogger(RecipientEnum.class);
    private String recipientType;

    private RecipientEnum(String recipientType) {
        this.recipientType = recipientType;
    }

    @JsonCreator
    public static RecipientEnum create(String recipientType) {

        recipientType = recipientType.replace(" - ", "_");
        recipientType = recipientType.replace('-', '_');
        recipientType = recipientType.replace(' ', '_');

        if (recipientType != null) {
            for (RecipientEnum val : RecipientEnum.values()) {
                if (recipientType.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        LOGGER.debug("Bad parameters exception. Enum class " + RecipientEnum.class.getSimpleName() + " doesn't accept this value: " + recipientType);
        throw new BadParametersException("Bad parameters exception. Enum class " + RecipientEnum.class.getSimpleName() + " doesn't accept this value: " + recipientType);
    }

    public String getValue() {
        return this.recipientType.toUpperCase();
    }

    @JsonValue
    public String toValue() {
        for (RecipientEnum val : RecipientEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }

}
