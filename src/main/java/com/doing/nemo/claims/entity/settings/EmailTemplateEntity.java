package com.doing.nemo.claims.entity.settings;

import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsFlowEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsRepairEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.enumerated.EmailTemplateEnum;
import com.doing.nemo.claims.entity.jsonb.dataType.JsonDataClaimTypeListType;
import com.doing.nemo.claims.entity.jsonb.dataType.JsonDataFlowListType;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "email_template")
@TypeDefs({
        @TypeDef(name = "JsonDataFlowListType", typeClass = JsonDataFlowListType.class),
        @TypeDef(name = "JsonDataClaimTypeListType", typeClass = JsonDataClaimTypeListType.class)
})
@SqlResultSetMapping(name="OrderResults",
        entities={
                @EntityResult(entityClass=EmailTemplateEntity.class)}
)
public class EmailTemplateEntity {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Type(type = "org.hibernate.type.PostgresUUIDType")
    private UUID id;

    @OneToOne
    @JoinColumn(name = "application_event_type_id")
    private EventTypeEntity applicationEvent;

    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private EmailTemplateEnum type;

    @Column(name = "description")
    private String description;

    @Column(name = "object")
    private String object;

    @Column(name = "heading")
    private String heading;

    @Column(name = "body")
    private String body;

    @Column(name = "foot")
    private String foot;

    @Column(name = "attach_file")
    private Boolean attachFile;

    @Column(name = "splitting_recipients_email")
    private Boolean splittingRecipientsEmail;

    @Column(name = "client_code")
    private String clientCode;

    @Enumerated(EnumType.STRING)
    @Column(name = "type_complaint")
    private ClaimsRepairEnum typeComplaint;

    @Type(type = "JsonDataFlowListType")
    @Column(name = "flows")
    private List<ClaimsFlowEnum> flows;

    @Type(type = "JsonDataClaimTypeListType")
    @Column(name = "accident_type_list")
    private List<DataAccidentTypeAccidentEnum> accidentTypeList;

    @ManyToMany
    @JoinTable(
            name = "email_template_recipient_type",
            joinColumns = {@JoinColumn(name = "email_template_id")},
            inverseJoinColumns = {@JoinColumn(name = "recipient_type_id")}
    )
    private List<RecipientTypeEntity> customers;

    @Column(name = "is_active")
    private Boolean isActive;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public EventTypeEntity getApplicationEvent() {
        return applicationEvent;
    }

    public void setApplicationEvent(EventTypeEntity applicationEvent) {
        this.applicationEvent = applicationEvent;
    }

    public EmailTemplateEnum getType() {
        return type;
    }

    public void setType(EmailTemplateEnum type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getFoot() {
        return foot;
    }

    public void setFoot(String foot) {
        this.foot = foot;
    }

    public Boolean getAttachFile() {
        return attachFile;
    }

    public void setAttachFile(Boolean attachFile) {
        this.attachFile = attachFile;
    }

    public Boolean getSplittingRecipientsEmail() {
        return splittingRecipientsEmail;
    }

    public void setSplittingRecipientsEmail(Boolean splittingRecipientsEmail) {
        this.splittingRecipientsEmail = splittingRecipientsEmail;
    }

    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    public List<RecipientTypeEntity> getCustomers() {
        return customers;
    }

    public void setCustomers(List<RecipientTypeEntity> customers) {
        this.customers = customers;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public ClaimsRepairEnum getTypeComplaint() {
        return typeComplaint;
    }

    public void setTypeComplaint(ClaimsRepairEnum typeComplaint) {
        this.typeComplaint = typeComplaint;
    }

    public List<ClaimsFlowEnum> getFlows() {
        if(flows == null){
            return null;
        }
        return new ArrayList<>(flows);
    }

    public void setFlows(List<ClaimsFlowEnum> flows) {
        if(flows != null)
        {
            this.flows = new ArrayList<>(flows);
        } else {
            this.flows = null;
        }
    }

    public List<DataAccidentTypeAccidentEnum> getAccidentTypeList() {
        if(accidentTypeList == null){
            return null;
        }
        return new ArrayList<>(accidentTypeList);    }

    public void setAccidentTypeList(List<DataAccidentTypeAccidentEnum> accidentTypeList) {
        if(accidentTypeList != null)
        {
            this.accidentTypeList = new ArrayList<>(accidentTypeList);
        } else {
            this.accidentTypeList = null;
        }    
    }

    @Override
    public String toString() {
        return "EmailTemplateEntity{" +
                "id=" + id +
                ", applicationEvent=" + applicationEvent +
                ", type=" + type +
                ", description='" + description + '\'' +
                ", object='" + object + '\'' +
                ", heading='" + heading + '\'' +
                ", body='" + body + '\'' +
                ", foot='" + foot + '\'' +
                ", attachFile=" + attachFile +
                ", splittingRecipientsEmail=" + splittingRecipientsEmail +
                ", clientCode='" + clientCode + '\'' +
                ", typeComplaint=" + typeComplaint +
                ", flows=" + flows +
                ", customers=" + customers +
                ", isActive=" + isActive +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EmailTemplateEntity that = (EmailTemplateEntity) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getApplicationEvent(), that.getApplicationEvent()) &&
                getType() == that.getType() &&
                Objects.equals(getDescription(), that.getDescription()) &&
                Objects.equals(getObject(), that.getObject()) &&
                Objects.equals(getHeading(), that.getHeading()) &&
                Objects.equals(getBody(), that.getBody()) &&
                Objects.equals(getFoot(), that.getFoot()) &&
                Objects.equals(getAttachFile(), that.getAttachFile()) &&
                Objects.equals(getSplittingRecipientsEmail(), that.getSplittingRecipientsEmail()) &&
                Objects.equals(getClientCode(), that.getClientCode()) &&
                getTypeComplaint() == that.getTypeComplaint() &&
                Objects.equals(getFlows(), that.getFlows()) &&
                Objects.equals(getCustomers(), that.getCustomers()) &&
                Objects.equals(getActive(), that.getActive());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getApplicationEvent(), getType(), getDescription(), getObject(), getHeading(), getBody(), getFoot(), getAttachFile(), getSplittingRecipientsEmail(), getClientCode(), getTypeComplaint(), getFlows(), getCustomers(), getActive());
    }
}
