package com.doing.nemo.claims.entity.settings;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "event")
public class EventEntity implements Serializable {

    @Id
    @JsonIgnore
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Type(type = "org.hibernate.type.PostgresUUIDType")
    private UUID id;


    @Column(name = "event_id", unique = true, insertable = false, updatable = false)
    private Long eventId;

    @OneToOne
    @JoinColumn(name = "event_type")
    private EventTypeEntity eventType;

    @Column(name = "created_at")
    private Instant createdAt;

    @Column(name = "user_id")
    private String userId;

    @Column(name = "description")
    private String description;

    public EventEntity(Long eventId, EventTypeEntity eventType, Instant createdAt, String user, String description) {
        this.eventId = eventId;
        this.eventType = eventType;
        this.createdAt = createdAt;
        this.userId = user;
        this.description = description;
    }

    public EventEntity() {
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Long getEventId() {
        return eventId;
    }

    public void setEventId(Long eventId) {
        this.eventId = eventId;
    }

    public EventTypeEntity getEventType() {
        return eventType;
    }

    public void setEventType(EventTypeEntity eventType) {
        this.eventType = eventType;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public String getUser() {
        return userId;
    }

    public void setUser(String user) {
        this.userId = user;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "EventEntity{" +
                "id=" + id +
                ", eventId=" + eventId +
                ", eventType=" + eventType +
                ", createdAt=" + createdAt +
                ", user=" + userId +
                ", description='" + description + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EventEntity that = (EventEntity) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getEventId(), that.getEventId()) &&
                Objects.equals(getEventType(), that.getEventType()) &&
                Objects.equals(getCreatedAt(), that.getCreatedAt()) &&
                Objects.equals(getUser(), that.getUser()) &&
                Objects.equals(getDescription(), that.getDescription());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getEventId(), getEventType(), getCreatedAt(), getUser(), getDescription());
    }
}
