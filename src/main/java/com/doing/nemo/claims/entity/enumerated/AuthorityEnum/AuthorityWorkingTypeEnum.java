package com.doing.nemo.claims.entity.enumerated.AuthorityEnum;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;

import java.io.Serializable;

public enum AuthorityWorkingTypeEnum implements Serializable {
    ADMINISTRATION("administration");

    private static Logger LOGGER = LoggerFactory.getLogger(AuthorityWorkingTypeEnum.class);
    private String administrationPracticeType;

    AuthorityWorkingTypeEnum(String administrationPracticeType) {
        this.administrationPracticeType = administrationPracticeType;
    }

    @JsonCreator
    public static AuthorityWorkingTypeEnum create(String claimsRepairEnum) {

        claimsRepairEnum = claimsRepairEnum.replace(" - ", "_");
        claimsRepairEnum = claimsRepairEnum.replace('-', '_');
        claimsRepairEnum = claimsRepairEnum.replace(' ', '_');

        if (claimsRepairEnum != null) {
            for (AuthorityWorkingTypeEnum val : AuthorityWorkingTypeEnum.values()) {
                if (claimsRepairEnum.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        LOGGER.debug("Bad parameters exception. Enum class " + AuthorityWorkingTypeEnum.class.getSimpleName() + " doesn't accept this value: " + claimsRepairEnum);
        throw new BadParametersException("Bad parameters exception. Enum class " + AuthorityWorkingTypeEnum.class.getSimpleName() + " doesn't accept this value: " + claimsRepairEnum);
    }

    public String getValue() {
        return this.administrationPracticeType.toUpperCase();
    }

    @JsonValue
    public String toValue() {
        for (AuthorityWorkingTypeEnum val : AuthorityWorkingTypeEnum.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }
}
