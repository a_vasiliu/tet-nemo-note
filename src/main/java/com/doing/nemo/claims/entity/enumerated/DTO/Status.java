package com.doing.nemo.claims.entity.enumerated.DTO;

import com.doing.nemo.commons.exception.BadParametersException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum Status {
    UPLOADED("uploaded"),
    PROCESSED("processed"),
    ERROR("error");

    private String statusEntity;

    private Status(String categoryEntity) {
        this.statusEntity = categoryEntity;
    }

    @JsonCreator
    public static Status create(String statusEntity) {

        statusEntity = statusEntity.replace('-', '_');
        statusEntity = statusEntity.replace(' ', '_');

        if (statusEntity != null) {
            for (Status val : Status.values()) {
                if (statusEntity.equalsIgnoreCase(val.getValue())) {
                    return val;
                }
            }
        }
        throw new BadParametersException("Bad parameters exception. Enum class " + Status.class.getSimpleName() + " doesn't accept this value: " + statusEntity);
    }

    public String getValue() {
        return this.statusEntity.toUpperCase();
    }

    @JsonValue
    public String toValue() {
        for (Status val : Status.values()) {
            if (this.getValue().equalsIgnoreCase(val.getValue())) {
                return val.getValue();
            }
        }
        return null;
    }
}

