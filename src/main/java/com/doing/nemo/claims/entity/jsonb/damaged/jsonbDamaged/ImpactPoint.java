package com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged;

import com.doing.nemo.claims.entity.enumerated.DamagedEnum.ImpactPointEnum.ImpactPointDetectionImpactPointEnum;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ImpactPoint implements Serializable {

    private static final long serialVersionUID = 7133835089959320252L;

    @JsonProperty("damage_to_vehicle")
    private String damageToVehicle;

    @JsonProperty("incident_description")
    private String incidentDescription;

    @JsonProperty("detections_impact_point")
    private List<ImpactPointDetectionImpactPointEnum> detectionsImpactPoint;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getDamageToVehicle() {
        return damageToVehicle;
    }

    public void setDamageToVehicle(String damageToVehicle) {
        this.damageToVehicle = damageToVehicle;
    }

    public String getIncidentDescription() {
        return incidentDescription;
    }

    public void setIncidentDescription(String incidentDescription) {
        this.incidentDescription = incidentDescription;
    }

    public List<ImpactPointDetectionImpactPointEnum> getDetectionsImpactPoint() {
        if(detectionsImpactPoint == null){
            return null;
        }
        return new ArrayList<>(detectionsImpactPoint);
    }

    public void setDetectionsImpactPoint(List<ImpactPointDetectionImpactPointEnum> detectionsImpactPoint) {
        if(detectionsImpactPoint != null)
        {
            this.detectionsImpactPoint = new ArrayList<>(detectionsImpactPoint);
        } else {
            this.detectionsImpactPoint = null;
        }
    }

    @Override
    public String toString() {
        return "ImpactPoint{" +
                "damageToVehicle='" + damageToVehicle + '\'' +
                ", incidentDescription='" + incidentDescription + '\'' +
                ", detectionsImpactPoint=" + detectionsImpactPoint +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        ImpactPoint that = (ImpactPoint) o;

        return new EqualsBuilder().append(damageToVehicle, that.damageToVehicle)
                .append(incidentDescription, that.incidentDescription).append(detectionsImpactPoint, that.detectionsImpactPoint).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(damageToVehicle)
                .append(incidentDescription).append(detectionsImpactPoint).toHashCode();
    }
}
