package com.doing.nemo.claims.entity.settings;

import com.doing.nemo.claims.entity.enumerated.ClaimsRepairEnum;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "manager")
public class ManagerEntity implements Serializable {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Type(type = "org.hibernate.type.PostgresUUIDType")
    private UUID id;

    @Column(name = "name")
    private String name;

    @Column(name = "address")
    private String address;

    @Column(name = "zip_code")
    private String zipCode;

    @Column(name = "locality")
    private String locality;

    @Column(name = "prov")
    private String prov;

    @Column(name = "country")
    private String country;

    @Column(name = "phone")
    private String phone;

    @Column(name = "fax")
    private String fax;

    @Column(name = "email")
    private String email;

    @Column(name = "website")
    private String website;

    @Column(name = "contact")
    private String contact;

    @Column(name = "external_export_id")
    private String externalExportId;

    @Column(name = "is_active")
    private Boolean isActive;

    @Enumerated(EnumType.STRING)
    @Column(name = "type_complaint")
    private ClaimsRepairEnum typeComplaint;

    public ManagerEntity() {
    }

    public ManagerEntity(String name, String address, String zipCode, String locality, String prov, String country, String phone, String fax, String email, String website, String contact, String externalExportId, Boolean isActive, ClaimsRepairEnum typeComplaint) {
        this.name = name;
        this.address = address;
        this.zipCode = zipCode;
        this.locality = locality;
        this.prov = prov;
        this.country = country;
        this.phone = phone;
        this.fax = fax;
        this.email = email;
        this.website = website;
        this.contact = contact;
        this.externalExportId = externalExportId;
        this.isActive = isActive;
        this.typeComplaint = typeComplaint;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getProv() {
        return prov;
    }

    public void setProv(String prov) {
        this.prov = prov;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getExternalExportId() {
        return externalExportId;
    }

    public void setExternalExportId(String externalExportId) {
        this.externalExportId = externalExportId;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public ClaimsRepairEnum getTypeComplaint() {
        return typeComplaint;
    }

    public void setTypeComplaint(ClaimsRepairEnum typeComplaint) {
        this.typeComplaint = typeComplaint;
    }

    @Override
    public String toString() {
        return "ManagerEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", zipCode=" + zipCode +
                ", locality='" + locality + '\'' +
                ", prov='" + prov + '\'' +
                ", country='" + country + '\'' +
                ", phone='" + phone + '\'' +
                ", fax='" + fax + '\'' +
                ", email='" + email + '\'' +
                ", website='" + website + '\'' +
                ", contact='" + contact + '\'' +
                ", externalExportId='" + externalExportId + '\'' +
                ", isActive=" + isActive +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ManagerEntity that = (ManagerEntity) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getName(), that.getName()) &&
                Objects.equals(getAddress(), that.getAddress()) &&
                Objects.equals(getZipCode(), that.getZipCode()) &&
                Objects.equals(getLocality(), that.getLocality()) &&
                Objects.equals(getProv(), that.getProv()) &&
                Objects.equals(getCountry(), that.getCountry()) &&
                Objects.equals(getPhone(), that.getPhone()) &&
                Objects.equals(getFax(), that.getFax()) &&
                Objects.equals(getEmail(), that.getEmail()) &&
                Objects.equals(getWebsite(), that.getWebsite()) &&
                Objects.equals(getContact(), that.getContact()) &&
                Objects.equals(getExternalExportId(), that.getExternalExportId()) &&
                Objects.equals(getActive(), that.getActive()) &&
                getTypeComplaint() == that.getTypeComplaint();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getAddress(), getZipCode(), getLocality(), getProv(), getCountry(), getPhone(), getFax(), getEmail(), getWebsite(), getContact(), getExternalExportId(), getActive(), getTypeComplaint());
    }
}
