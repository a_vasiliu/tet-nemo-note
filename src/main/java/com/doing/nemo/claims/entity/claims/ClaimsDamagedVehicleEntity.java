package com.doing.nemo.claims.entity.claims;

import com.doing.nemo.claims.entity.ClaimsNewEntity;
import com.doing.nemo.claims.entity.enumerated.DamagedEnum.VehicleEnum.VehicleNatureEnum;
import com.doing.nemo.claims.entity.enumerated.DamagedEnum.VehicleEnum.VehicleOwnershipEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.Date;
import java.util.Objects;

@JsonIgnoreProperties({"hibernateLazyInitializer", "hibernate_lazy_initializer", "handler"})
@Entity
@Table(name = "claims_damaged_vehicle")
public class ClaimsDamagedVehicleEntity implements Serializable {

    @Id
    @Column(name = "id", updatable = false, nullable = false)
    private String id;

    @Column(name="fleet_vehicle_id")
    private Long fleetVehicleId;

    @Column(name="make")
    private String make;

    @Column(name="model")
    private String model;

    @Column(name="model_year")
    private String modelYear;

    @Column(name="kw")
    private String kw;

    @Column(name="cc_3")
    private String cc3;

    @Column(name="hp")
    private Integer hp;

    @Column(name="din_hp")
    private Integer dinHp;

    @Column(name="seats")
    private Integer seats;

    @Column(name="net_weight")
    private Double netWeight;

    @Column(name="max_weight")
    private Double maxWeight;

    @Column(name="doors")
    private String doors;

    @Column(name="body_style")
    private String bodyStyle;

    @Column(name="nature")
    @Enumerated(EnumType.STRING)
    private VehicleNatureEnum nature;

    @Column(name="fuel_type")
    private String fuelType;

    @Column(name="co_2emission")
    private String co2emission;

    @Column(name="consumption")
    private String consumption;

    @Column(name="license_plate")
    private String licensePlate;

    @Column(name="ownership")
    @Enumerated(EnumType.STRING)
    private VehicleOwnershipEnum ownership;

    @Column(name="chassis_number")
    private String chassisNumber;

    @Column(name="registration_country")
    private String registrationCountry;

    @Column(name="registration_date")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date registrationDate;

    @Column(name="last_known_mileage")
    private String lastKnownMileage;

    @Column(name="external_color")
    private String externalColor;

    @Column(name="internal_color")
    private String internalColor;

    @Column(name="net_book_value")
    private Double netBookValue;

    @Column(name="wreck")
    private Boolean wreck;

    @Column(name="license_plate_trailer")
    private String licensePlateTrailer;

    @Column(name="chassis_number_trailer")
    private String chassisNumberTrailer;

    @Column(name="registration_country_trailer")
    private String registrationCountryTrailer;

    @Column(name="wreck_date")
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    private Instant wreckDate;

    @OneToOne
    @JoinColumn(name = "id")
    @MapsId("id")
    @PrimaryKeyJoinColumn
    private ClaimsNewEntity claim;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getFleetVehicleId() {
        return fleetVehicleId;
    }

    public void setFleetVehicleId(Long fleetVehicleId) {
        this.fleetVehicleId = fleetVehicleId;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getModelYear() {
        return modelYear;
    }

    public void setModelYear(String modelYear) {
        this.modelYear = modelYear;
    }

    public String getKw() {
        return kw;
    }

    public void setKw(String kw) {
        this.kw = kw;
    }

    public String getCc3() {
        return cc3;
    }

    public void setCc3(String cc3) {
        this.cc3 = cc3;
    }

    public Integer getHp() {
        return hp;
    }

    public void setHp(Integer hp) {
        this.hp = hp;
    }

    public Integer getDinHp() {
        return dinHp;
    }

    public void setDinHp(Integer dinHp) {
        this.dinHp = dinHp;
    }

    public Integer getSeats() {
        return seats;
    }

    public void setSeats(Integer seats) {
        this.seats = seats;
    }

    public Double getNetWeight() {
        return netWeight;
    }

    public void setNetWeight(Double netWeight) {
        this.netWeight = netWeight;
    }

    public Double getMaxWeight() {
        return maxWeight;
    }

    public void setMaxWeight(Double maxWeight) {
        this.maxWeight = maxWeight;
    }

    public String getDoors() {
        return doors;
    }

    public void setDoors(String doors) {
        this.doors = doors;
    }

    public String getBodyStyle() {
        return bodyStyle;
    }

    public void setBodyStyle(String bodyStyle) {
        this.bodyStyle = bodyStyle;
    }

    public VehicleNatureEnum getNature() {
        return nature;
    }

    public void setNature(VehicleNatureEnum nature) {
        this.nature = nature;
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    public String getCo2emission() {
        return co2emission;
    }

    public void setCo2emission(String co2emission) {
        this.co2emission = co2emission;
    }

    public String getConsumption() {
        return consumption;
    }

    public void setConsumption(String consumption) {
        this.consumption = consumption;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public VehicleOwnershipEnum getOwnership() {
        return ownership;
    }

    public void setOwnership(VehicleOwnershipEnum ownership) {
        this.ownership = ownership;
    }

    public String getChassisNumber() {
        return chassisNumber;
    }

    public void setChassisNumber(String chassisNumber) {
        this.chassisNumber = chassisNumber;
    }

    public String getRegistrationCountry() {
        return registrationCountry;
    }

    public void setRegistrationCountry(String registrationCountry) {
        this.registrationCountry = registrationCountry;
    }

    public Date getRegistrationDate() {
        if(registrationDate == null){
            return null;
        }
        return (Date)registrationDate.clone();
    }

    public void setRegistrationDate(Date registrationDate) {
        if(registrationDate != null)
        {
            this.registrationDate = (Date)registrationDate.clone();
        } else {
            this.registrationDate = null;
        }
    }

    public String getLastKnownMileage() {
        return lastKnownMileage;
    }

    public void setLastKnownMileage(String lastKnownMileage) {
        this.lastKnownMileage = lastKnownMileage;
    }

    public String getExternalColor() {
        return externalColor;
    }

    public void setExternalColor(String externalColor) {
        this.externalColor = externalColor;
    }

    public String getInternalColor() {
        return internalColor;
    }

    public void setInternalColor(String internalColor) {
        this.internalColor = internalColor;
    }

    public Double getNetBookValue() {
        return netBookValue;
    }

    public void setNetBookValue(Double netBookValue) {
        this.netBookValue = netBookValue;
    }

    public Boolean getWreck() {
        return wreck;
    }

    public void setWreck(Boolean wreck) {
        this.wreck = wreck;
    }

    public String getLicensePlateTrailer() {
        return licensePlateTrailer;
    }

    public void setLicensePlateTrailer(String licensePlateTrailer) {
        this.licensePlateTrailer = licensePlateTrailer;
    }

    public String getChassisNumberTrailer() {
        return chassisNumberTrailer;
    }

    public void setChassisNumberTrailer(String chassisNumberTrailer) {
        this.chassisNumberTrailer = chassisNumberTrailer;
    }

    public String getRegistrationCountryTrailer() {
        return registrationCountryTrailer;
    }

    public void setRegistrationCountryTrailer(String registrationCountryTrailer) {
        this.registrationCountryTrailer = registrationCountryTrailer;
    }

    public Instant getWreckDate() {
        return wreckDate;
    }

    public void setWreckDate(Instant wreckDate) {
        this.wreckDate = wreckDate;
    }

    public ClaimsNewEntity getClaim() {
        return claim;
    }

    public void setClaim(ClaimsNewEntity claim) {
        this.claim = claim;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ClaimsDamagedVehicleEntity{");
        sb.append("id='").append(id).append('\'');
        sb.append(", fleetVehicleId=").append(fleetVehicleId);
        sb.append(", make='").append(make).append('\'');
        sb.append(", model='").append(model).append('\'');
        sb.append(", modelYear='").append(modelYear).append('\'');
        sb.append(", kw='").append(kw).append('\'');
        sb.append(", cc3='").append(cc3).append('\'');
        sb.append(", hp=").append(hp);
        sb.append(", dinHp=").append(dinHp);
        sb.append(", seats=").append(seats);
        sb.append(", netWeight=").append(netWeight);
        sb.append(", maxWeight=").append(maxWeight);
        sb.append(", doors='").append(doors).append('\'');
        sb.append(", bodyStyle='").append(bodyStyle).append('\'');
        sb.append(", nature=").append(nature);
        sb.append(", fuelType='").append(fuelType).append('\'');
        sb.append(", co2emission='").append(co2emission).append('\'');
        sb.append(", consumption='").append(consumption).append('\'');
        sb.append(", licensePlate='").append(licensePlate).append('\'');
        sb.append(", ownership=").append(ownership);
        sb.append(", chassisNumber='").append(chassisNumber).append('\'');
        sb.append(", registrationCountry='").append(registrationCountry).append('\'');
        sb.append(", registrationDate=").append(registrationDate);
        sb.append(", lastKnownMileage='").append(lastKnownMileage).append('\'');
        sb.append(", externalColor='").append(externalColor).append('\'');
        sb.append(", internalColor='").append(internalColor).append('\'');
        sb.append(", netBookValue=").append(netBookValue);
        sb.append(", wreck=").append(wreck);
        sb.append(", licensePlateTrailer='").append(licensePlateTrailer).append('\'');
        sb.append(", chassisNumberTrailer='").append(chassisNumberTrailer).append('\'');
        sb.append(", registrationCountryTrailer='").append(registrationCountryTrailer).append('\'');
        sb.append(", wreckDate=").append(wreckDate);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClaimsDamagedVehicleEntity that = (ClaimsDamagedVehicleEntity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(fleetVehicleId, that.fleetVehicleId) &&
                Objects.equals(make, that.make) &&
                Objects.equals(model, that.model) &&
                Objects.equals(modelYear, that.modelYear) &&
                Objects.equals(kw, that.kw) &&
                Objects.equals(cc3, that.cc3) &&
                Objects.equals(hp, that.hp) &&
                Objects.equals(dinHp, that.dinHp) &&
                Objects.equals(seats, that.seats) &&
                Objects.equals(netWeight, that.netWeight) &&
                Objects.equals(maxWeight, that.maxWeight) &&
                Objects.equals(doors, that.doors) &&
                Objects.equals(bodyStyle, that.bodyStyle) &&
                nature == that.nature &&
                Objects.equals(fuelType, that.fuelType) &&
                Objects.equals(co2emission, that.co2emission) &&
                Objects.equals(consumption, that.consumption) &&
                Objects.equals(licensePlate, that.licensePlate) &&
                ownership == that.ownership &&
                Objects.equals(chassisNumber, that.chassisNumber) &&
                Objects.equals(registrationCountry, that.registrationCountry) &&
                Objects.equals(registrationDate, that.registrationDate) &&
                Objects.equals(lastKnownMileage, that.lastKnownMileage) &&
                Objects.equals(externalColor, that.externalColor) &&
                Objects.equals(internalColor, that.internalColor) &&
                Objects.equals(netBookValue, that.netBookValue) &&
                Objects.equals(wreck, that.wreck) &&
                Objects.equals(licensePlateTrailer, that.licensePlateTrailer) &&
                Objects.equals(chassisNumberTrailer, that.chassisNumberTrailer) &&
                Objects.equals(registrationCountryTrailer, that.registrationCountryTrailer) &&
                Objects.equals(wreckDate, that.wreckDate) &&
                Objects.equals(claim, that.claim);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, fleetVehicleId, make, model, modelYear, kw, cc3, hp, dinHp, seats, netWeight, maxWeight, doors, bodyStyle, nature, fuelType, co2emission, consumption, licensePlate, ownership, chassisNumber, registrationCountry, registrationDate, lastKnownMileage, externalColor, internalColor, netBookValue, wreck, licensePlateTrailer, chassisNumberTrailer, registrationCountryTrailer, wreckDate, claim);
    }
}
