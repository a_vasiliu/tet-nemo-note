package com.doing.nemo.claims.entity.settings;


import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "contract")
public class ContractEntity implements Serializable {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Type(type = "org.hibernate.type.PostgresUUIDType")
    private UUID id;

    @Column(name = "contract_code", unique = true, insertable = false, updatable = false)
    private Long contractCode;

    @OneToOne
    @JoinColumn(name = "contract_type_id", nullable = true)
    private ContractTypeEntity contractType;

    @Column(name = "beginning_of_validity")
    private Instant beginningOfValidity;

    @Column(name = "end_of_validity")
    private Instant endOfValidity;

    @Column(name = "plate_number")
    private String plateNumber;

    @Column(name = "delivery_center")
    private String delivertyCenter;

    @Column(name = "ex_plate_number")
    private String exPlateNumber;

    @Column(name = "brand")
    private String brand;

    @Column(name = "model")
    private String model;

    @Column(name = "hp")
    private Integer hp;

    @Column(name = "kw")
    private Integer kw;

    @Column(name = "weight")
    private Integer weight;

    @Column(name = "matriculation")
    private Date matriculation;

    @Column(name = "color")
    private String color;

    @Column(name = "frame")
    private String frame;

    @Column(name = "jato")
    private String jato;

    @Column(name = "eurotax")
    private String eurotax;

    @Column(name = "km_last_check")
    private Integer kmLastCheck;

    @Column(name = "book_register")
    private String bookRegister;

    @Column(name = "book_register_pai")
    private String bookRegisterPai;

    @Column(name = "insurance_theft_lc")
    private Double insuranceTheftLc;

    @Column(name = "insurance_kasko_md")
    private Double insuranceKaskoMd;

    @Column(name = "insurance_search_tpl")
    private Double insuranceSearchTpl;

    @Column(name = "insurance_ex_thf")
    private Double insuranceExThf;

    @Column(name = "insurance_pai_max")
    private Integer insurancePaiMax;

    @Column(name = "insurance_deduct_md")
    private Double insuranceDeductMd;

    @Column(name = "insurance_pai_pass")
    private Double insurancePaiPass;

    @Column(name = "insurance_ex_tpl")
    private Double insuranceExTpl;

    @Column(name = "insurance_max_pass")
    private Double insuranceMaxPass;

    @Column(name = "insurance_pai_ex")
    private Integer insurancePaiEx;

    @OneToOne
    @JoinColumn(name = "anti_theft_id", nullable = true)
    private AntiTheftServiceEntity antiTheft;

    @Column(name = "voucher_id")
    private Long voucherId;

    @Column(name = "activation")
    private Instant activation;

    @Column(name = "contract_area_business")
    private String contractAreaBusiness;

    @Column(name = "km_contract")
    private Integer kmContract;

    @Column(name = "months_duration")
    private Integer monthsDuration;

    @Column(name = "begin_contract")
    private Instant beginContract;

    @Column(name = "end_contract")
    private Instant endContract;

    @Column(name = "suspension")
    private Instant suspension;

    @Column(name = "reactivation")
    private Instant reactivation;

    @Column(name = "lengthened")
    private Instant lengthened;

    @Column(name = "cancellation")
    private Instant cancellation;

    @Column(name = "returned")
    private Instant returned;

    @Column(name = "lock_start")
    private Instant lockStart;

    @Column(name = "lock_end")
    private Instant lockEnd;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Long getContractCode() {
        return contractCode;
    }

    public void setContractCode(Long contractCode) {
        this.contractCode = contractCode;
    }

    public ContractTypeEntity getContractType() {
        return contractType;
    }

    public void setContractType(ContractTypeEntity contractType) {
        this.contractType = contractType;
    }

    public Instant getBeginningOfValidity() {
        return beginningOfValidity;
    }

    public void setBeginningOfValidity(Instant beginningOfValidity) {
        this.beginningOfValidity = beginningOfValidity;
    }

    public Instant getEndOfValidity() {
        return endOfValidity;
    }

    public void setEndOfValidity(Instant endOfValidity) {
        this.endOfValidity = endOfValidity;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public String getDelivertyCenter() {
        return delivertyCenter;
    }

    public void setDelivertyCenter(String delivertyCenter) {
        this.delivertyCenter = delivertyCenter;
    }

    public String getExPlateNumber() {
        return exPlateNumber;
    }

    public void setExPlateNumber(String exPlateNumber) {
        this.exPlateNumber = exPlateNumber;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Integer getHp() {
        return hp;
    }

    public void setHp(Integer hp) {
        this.hp = hp;
    }

    public Integer getKw() {
        return kw;
    }

    public void setKw(Integer kw) {
        this.kw = kw;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public Date getMatriculation() {
        if(matriculation == null){
            return null;
        }
        return (Date)matriculation.clone();
    }

    public void setMatriculation(Date matriculation) {
        if(matriculation != null)
        {
            this.matriculation = (Date)matriculation.clone();
        } else {
            this.matriculation = null;
        }
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getFrame() {
        return frame;
    }

    public void setFrame(String frame) {
        this.frame = frame;
    }

    public String getJato() {
        return jato;
    }

    public void setJato(String jato) {
        this.jato = jato;
    }

    public String getEurotax() {
        return eurotax;
    }

    public void setEurotax(String eurotax) {
        this.eurotax = eurotax;
    }

    public Integer getKmLastCheck() {
        return kmLastCheck;
    }

    public void setKmLastCheck(Integer kmLastCheck) {
        this.kmLastCheck = kmLastCheck;
    }

    public String getBookRegister() {
        return bookRegister;
    }

    public void setBookRegister(String bookRegister) {
        this.bookRegister = bookRegister;
    }

    public String getBookRegisterPai() {
        return bookRegisterPai;
    }

    public void setBookRegisterPai(String bookRegisterPai) {
        this.bookRegisterPai = bookRegisterPai;
    }

    public Double getInsuranceTheftLc() {
        return insuranceTheftLc;
    }

    public void setInsuranceTheftLc(Double insuranceTheftLc) {
        this.insuranceTheftLc = insuranceTheftLc;
    }

    public Double getInsuranceKaskoMd() {
        return insuranceKaskoMd;
    }

    public void setInsuranceKaskoMd(Double insuranceKaskoMd) {
        this.insuranceKaskoMd = insuranceKaskoMd;
    }

    public Double getInsuranceSearchTpl() {
        return insuranceSearchTpl;
    }

    public void setInsuranceSearchTpl(Double insuranceSearchTpl) {
        this.insuranceSearchTpl = insuranceSearchTpl;
    }

    public Double getInsuranceExThf() {
        return insuranceExThf;
    }

    public void setInsuranceExThf(Double insuranceExThf) {
        this.insuranceExThf = insuranceExThf;
    }

    public Integer getInsurancePaiMax() {
        return insurancePaiMax;
    }

    public void setInsurancePaiMax(Integer insurancePaiMax) {
        this.insurancePaiMax = insurancePaiMax;
    }

    public Double getInsuranceDeductMd() {
        return insuranceDeductMd;
    }

    public void setInsuranceDeductMd(Double insuranceDeductMd) {
        this.insuranceDeductMd = insuranceDeductMd;
    }

    public Double getInsurancePaiPass() {
        return insurancePaiPass;
    }

    public void setInsurancePaiPass(Double insurancePaiPass) {
        this.insurancePaiPass = insurancePaiPass;
    }

    public Double getInsuranceExTpl() {
        return insuranceExTpl;
    }

    public void setInsuranceExTpl(Double insuranceExTpl) {
        this.insuranceExTpl = insuranceExTpl;
    }

    public Double getInsuranceMaxPass() {
        return insuranceMaxPass;
    }

    public void setInsuranceMaxPass(Double insuranceMaxPass) {
        this.insuranceMaxPass = insuranceMaxPass;
    }

    public Integer getInsurancePaiEx() {
        return insurancePaiEx;
    }

    public void setInsurancePaiEx(Integer insurancePaiEx) {
        this.insurancePaiEx = insurancePaiEx;
    }

    public AntiTheftServiceEntity getAntiTheft() {
        return antiTheft;
    }

    public void setAntiTheft(AntiTheftServiceEntity antiTheft) {
        this.antiTheft = antiTheft;
    }

    public Long getVoucherId() {
        return voucherId;
    }

    public void setVoucherId(Long voucherId) {
        this.voucherId = voucherId;
    }

    public Instant getActivation() {
        return activation;
    }

    public void setActivation(Instant activation) {
        this.activation = activation;
    }

    public String getContractAreaBusiness() {
        return contractAreaBusiness;
    }

    public void setContractAreaBusiness(String contractAreaBusiness) {
        this.contractAreaBusiness = contractAreaBusiness;
    }

    public Integer getKmContract() {
        return kmContract;
    }

    public void setKmContract(Integer kmContract) {
        this.kmContract = kmContract;
    }

    public Integer getMonthsDuration() {
        return monthsDuration;
    }

    public void setMonthsDuration(Integer monthsDuration) {
        this.monthsDuration = monthsDuration;
    }

    public Instant getBeginContract() {
        return beginContract;
    }

    public void setBeginContract(Instant beginContract) {
        this.beginContract = beginContract;
    }

    public Instant getEndContract() {
        return endContract;
    }

    public void setEndContract(Instant endContract) {
        this.endContract = endContract;
    }

    public Instant getSuspension() {
        return suspension;
    }

    public void setSuspension(Instant suspension) {
        this.suspension = suspension;
    }

    public Instant getReactivation() {
        return reactivation;
    }

    public void setReactivation(Instant reactivation) {
        this.reactivation = reactivation;
    }

    public Instant getLengthened() {
        return lengthened;
    }

    public void setLengthened(Instant lengthened) {
        this.lengthened = lengthened;
    }

    public Instant getCancellation() {
        return cancellation;
    }

    public void setCancellation(Instant cancellation) {
        this.cancellation = cancellation;
    }

    public Instant getReturned() {
        return returned;
    }

    public void setReturned(Instant returned) {
        this.returned = returned;
    }

    public Instant getLockStart() {
        return lockStart;
    }

    public void setLockStart(Instant lockStart) {
        this.lockStart = lockStart;
    }

    public Instant getLockEnd() {
        return lockEnd;
    }

    public void setLockEnd(Instant lockEnd) {
        this.lockEnd = lockEnd;
    }

    @Override
    public String toString() {
        return "ContractEntity{" +
                "id=" + id +
                ", contractCode=" + contractCode +
                ", contractType=" + contractType +
                ", beginningOfValidity=" + beginningOfValidity +
                ", endOfValidity=" + endOfValidity +
                ", plateNumber='" + plateNumber + '\'' +
                ", delivertyCenter='" + delivertyCenter + '\'' +
                ", exPlateNumber='" + exPlateNumber + '\'' +
                ", brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", hp=" + hp +
                ", kw=" + kw +
                ", weight=" + weight +
                ", matriculation=" + matriculation +
                ", color='" + color + '\'' +
                ", frame='" + frame + '\'' +
                ", jato='" + jato + '\'' +
                ", eurotax='" + eurotax + '\'' +
                ", kmLastCheck=" + kmLastCheck +
                ", bookRegister='" + bookRegister + '\'' +
                ", bookRegisterPai='" + bookRegisterPai + '\'' +
                ", insuranceTheftLc=" + insuranceTheftLc +
                ", insuranceKaskoMd=" + insuranceKaskoMd +
                ", insuranceSearchTpl=" + insuranceSearchTpl +
                ", insuranceExThf=" + insuranceExThf +
                ", insurancePaiMax=" + insurancePaiMax +
                ", insuranceDeductMd=" + insuranceDeductMd +
                ", insurancePaiPass=" + insurancePaiPass +
                ", insuranceExTpl=" + insuranceExTpl +
                ", insuranceMaxPass=" + insuranceMaxPass +
                ", insurancePaiEx=" + insurancePaiEx +
                ", antiTheft=" + antiTheft +
                ", voucherId=" + voucherId +
                ", activation=" + activation +
                ", contractAreaBusiness='" + contractAreaBusiness + '\'' +
                ", kmContract=" + kmContract +
                ", monthsDuration=" + monthsDuration +
                ", beginContract=" + beginContract +
                ", endContract=" + endContract +
                ", suspension=" + suspension +
                ", reactivation=" + reactivation +
                ", lengthened=" + lengthened +
                ", cancellation=" + cancellation +
                ", returned=" + returned +
                ", lockStart=" + lockStart +
                ", lockEnd=" + lockEnd +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ContractEntity that = (ContractEntity) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getContractCode(), that.getContractCode()) &&
                Objects.equals(getContractType(), that.getContractType()) &&
                Objects.equals(getBeginningOfValidity(), that.getBeginningOfValidity()) &&
                Objects.equals(getEndOfValidity(), that.getEndOfValidity()) &&
                Objects.equals(getPlateNumber(), that.getPlateNumber()) &&
                Objects.equals(getDelivertyCenter(), that.getDelivertyCenter()) &&
                Objects.equals(getExPlateNumber(), that.getExPlateNumber()) &&
                Objects.equals(getBrand(), that.getBrand()) &&
                Objects.equals(getModel(), that.getModel()) &&
                Objects.equals(getHp(), that.getHp()) &&
                Objects.equals(getKw(), that.getKw()) &&
                Objects.equals(getWeight(), that.getWeight()) &&
                Objects.equals(getMatriculation(), that.getMatriculation()) &&
                Objects.equals(getColor(), that.getColor()) &&
                Objects.equals(getFrame(), that.getFrame()) &&
                Objects.equals(getJato(), that.getJato()) &&
                Objects.equals(getEurotax(), that.getEurotax()) &&
                Objects.equals(getKmLastCheck(), that.getKmLastCheck()) &&
                Objects.equals(getBookRegister(), that.getBookRegister()) &&
                Objects.equals(getBookRegisterPai(), that.getBookRegisterPai()) &&
                Objects.equals(getInsuranceTheftLc(), that.getInsuranceTheftLc()) &&
                Objects.equals(getInsuranceKaskoMd(), that.getInsuranceKaskoMd()) &&
                Objects.equals(getInsuranceSearchTpl(), that.getInsuranceSearchTpl()) &&
                Objects.equals(getInsuranceExThf(), that.getInsuranceExThf()) &&
                Objects.equals(getInsurancePaiMax(), that.getInsurancePaiMax()) &&
                Objects.equals(getInsuranceDeductMd(), that.getInsuranceDeductMd()) &&
                Objects.equals(getInsurancePaiPass(), that.getInsurancePaiPass()) &&
                Objects.equals(getInsuranceExTpl(), that.getInsuranceExTpl()) &&
                Objects.equals(getInsuranceMaxPass(), that.getInsuranceMaxPass()) &&
                Objects.equals(getInsurancePaiEx(), that.getInsurancePaiEx()) &&
                Objects.equals(getAntiTheft(), that.getAntiTheft()) &&
                Objects.equals(getVoucherId(), that.getVoucherId()) &&
                Objects.equals(getActivation(), that.getActivation()) &&
                Objects.equals(getContractAreaBusiness(), that.getContractAreaBusiness()) &&
                Objects.equals(getKmContract(), that.getKmContract()) &&
                Objects.equals(getMonthsDuration(), that.getMonthsDuration()) &&
                Objects.equals(getBeginContract(), that.getBeginContract()) &&
                Objects.equals(getEndContract(), that.getEndContract()) &&
                Objects.equals(getSuspension(), that.getSuspension()) &&
                Objects.equals(getReactivation(), that.getReactivation()) &&
                Objects.equals(getLengthened(), that.getLengthened()) &&
                Objects.equals(getCancellation(), that.getCancellation()) &&
                Objects.equals(getReturned(), that.getReturned()) &&
                Objects.equals(getLockStart(), that.getLockStart()) &&
                Objects.equals(getLockEnd(), that.getLockEnd());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getContractCode(), getContractType(), getBeginningOfValidity(), getEndOfValidity(), getPlateNumber(), getDelivertyCenter(), getExPlateNumber(), getBrand(), getModel(), getHp(), getKw(), getWeight(), getMatriculation(), getColor(), getFrame(), getJato(), getEurotax(), getKmLastCheck(), getBookRegister(), getBookRegisterPai(), getInsuranceTheftLc(), getInsuranceKaskoMd(), getInsuranceSearchTpl(), getInsuranceExThf(), getInsurancePaiMax(), getInsuranceDeductMd(), getInsurancePaiPass(), getInsuranceExTpl(), getInsuranceMaxPass(), getInsurancePaiEx(), getAntiTheft(), getVoucherId(), getActivation(), getContractAreaBusiness(), getKmContract(), getMonthsDuration(), getBeginContract(), getEndContract(), getSuspension(), getReactivation(), getLengthened(), getCancellation(), getReturned(), getLockStart(), getLockEnd());
    }
}


