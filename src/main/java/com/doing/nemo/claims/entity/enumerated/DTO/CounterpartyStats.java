package com.doing.nemo.claims.entity.enumerated.DTO;


import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class CounterpartyStats implements Serializable {

    @JsonProperty("waiting_for_quotation")
    private double waitingForQuotation;
    @JsonProperty("to_sort")
    private double toSort;
    @JsonProperty("to_contact")
    private double toContact;
    @JsonProperty("to_recontact")
    private double toRecontact;
    @JsonProperty("repaired")
    private double repaired;
    @JsonProperty("closed_repaired")
    private double closedRepaired;
    @JsonProperty("closed_liquidated")
    private double closedLiquidated;
    @JsonProperty("under_liquidation")
    private double underLiquidation;
    @JsonProperty("under_repair")
    private double underRepair;
    @JsonProperty("quotation_approved")
    private double quotationApproved;
    @JsonProperty("closed_denial")
    private double closedDenial;
    //aggiungere stats per wreck e cancelled
    //togliere conteggio su msaManagement manageable

    public CounterpartyStats() {

    }

    public CounterpartyStats(double waitingForQuotation, double toSort, double toContact, double toRecontact, double repaired, double closedRepaired, double closedLiquidated, double underLiquidation, double underRepair, double quotationApproved, double closedDenial) {
        this.waitingForQuotation = waitingForQuotation;
        this.toSort = toSort;
        this.toContact = toContact;
        this.toRecontact = toRecontact;
        this.repaired = repaired;
        this.closedRepaired = closedRepaired;
        this.closedLiquidated = closedLiquidated;
        this.underLiquidation = underLiquidation;
        this.underRepair = underRepair;
        this.quotationApproved = quotationApproved;
        this.closedDenial = closedDenial;
    }

    public double getWaitingForQuotation() {
        return waitingForQuotation;
    }

    public void setWaitingForQuotation(double waitingForQuotation) {
        this.waitingForQuotation = waitingForQuotation;
    }

    public double getToSort() {
        return toSort;
    }

    public void setToSort(double toSort) {
        this.toSort = toSort;
    }

    public double getToContact() {
        return toContact;
    }

    public void setToContact(double toContact) {
        this.toContact = toContact;
    }

    public double getToRecontact() {
        return toRecontact;
    }

    public void setToRecontact(double toRecontact) {
        this.toRecontact = toRecontact;
    }

    public double getRepaired() {
        return repaired;
    }

    public void setRepaired(double repaired) {
        this.repaired = repaired;
    }

    public double getClosedRepaired() {
        return closedRepaired;
    }

    public void setClosedRepaired(double closedRepaired) {
        this.closedRepaired = closedRepaired;
    }

    public double getClosedLiquidated() {
        return closedLiquidated;
    }

    public void setClosedLiquidated(double closedLiquidated) {
        this.closedLiquidated = closedLiquidated;
    }

    public double getUnderLiquidation() {
        return underLiquidation;
    }

    public void setUnderLiquidation(double underLiquidation) {
        this.underLiquidation = underLiquidation;
    }

    public double getUnderRepair() {
        return underRepair;
    }

    public void setUnderRepair(double underRepair) {
        this.underRepair = underRepair;
    }

    public double getQuotationApproved() {
        return quotationApproved;
    }

    public void setQuotationApproved(double quotationApproved) {
        this.quotationApproved = quotationApproved;
    }

    public double getClosedDenial() {
        return closedDenial;
    }

    public void setClosedDenial(double closedDenial) {
        this.closedDenial = closedDenial;
    }
}

