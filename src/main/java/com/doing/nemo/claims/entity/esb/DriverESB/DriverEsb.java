package com.doing.nemo.claims.entity.esb.DriverESB;

import com.doing.nemo.claims.entity.esb.AddressEsb;
import com.doing.nemo.claims.entity.esb.LinkEsb;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DriverEsb implements Serializable {

    private static final long serialVersionUID = 8542085657110453761L;

    @JsonProperty("id")
    private String id;

    @JsonProperty("identification")
    private String identification;

    @JsonProperty("official_registration")
    private String officialRegistration;

    @JsonProperty("trading_name")
    private String tradingName;

    private String firstname;

    private String lastname;

    @JsonProperty("email")
    private String email;

    @JsonProperty("phone")
    private String phone;

    @JsonProperty("pec")
    private String pec;

    @JsonProperty("sex")
    private String sex;

    @JsonProperty("customer_id")
    private String customerId;

    @JsonProperty("main_address")
    private AddressEsb mainAddress;

    @JsonProperty("driving_license")
    private DrivingLicenseEsb drivingLicense;

    @JsonProperty("links")
    private List<LinkEsb> linksList;


    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public String getOfficialRegistration() {
        return officialRegistration;
    }

    public void setOfficialRegistration(String officialRegistration) {
        this.officialRegistration = officialRegistration;
    }

    public String getTradingName() {
        return tradingName;
    }

    public void setTradingName(String tradingName) {
        this.tradingName = tradingName;
    }

    @JsonProperty("firstname")
    public String getFirstname() {
        return firstname;
    }

    @JsonProperty("first_name")
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    @JsonProperty("lastname")
    public String getLastname() {
        return lastname;
    }

    @JsonProperty("last_name")
    public void setLastname(String lastName) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPec() {
        return pec;
    }

    public void setPec(String pec) {
        this.pec = pec;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public AddressEsb getMainAddress() {
        return mainAddress;
    }

    public void setMainAddress(AddressEsb mainAddress) {
        this.mainAddress = mainAddress;
    }

    public DrivingLicenseEsb getDrivingLicense() {
        return drivingLicense;
    }

    public void setDrivingLicense(DrivingLicenseEsb drivingLicense) {
        this.drivingLicense = drivingLicense;
    }

    public List<LinkEsb> getLinksList() {
        if(linksList == null){
            return null;
        }
        return new ArrayList<>(linksList);
    }

    public void setLinksList(List<LinkEsb> linksList) {
        if(linksList != null)
        {
            this.linksList =new ArrayList<>(linksList);
        } else {
            this.linksList = null;
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "DriverEsb{" +
                "id='" + id + '\'' +
                ", identification='" + identification + '\'' +
                ", officialRegistration='" + officialRegistration + '\'' +
                ", tradingName='" + tradingName + '\'' +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", pec='" + pec + '\'' +
                ", sex='" + sex + '\'' +
                ", customerId='" + customerId + '\'' +
                ", mainAddress=" + mainAddress +
                ", drivingLicense=" + drivingLicense +
                ", linksList=" + linksList +
                '}';
    }
}
