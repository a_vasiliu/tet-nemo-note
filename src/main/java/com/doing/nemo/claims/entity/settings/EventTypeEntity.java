package com.doing.nemo.claims.entity.settings;

import com.doing.nemo.claims.entity.enumerated.ClaimsRepairEnum;
import com.doing.nemo.claims.entity.enumerated.EventTypeEnum;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "event_type")
public class EventTypeEntity {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Type(type = "org.hibernate.type.PostgresUUIDType")
    private UUID id;

    @Column(name = "description")
    private String description;

    @Enumerated(EnumType.STRING)
    @Column(name = "event_type")
    private EventTypeEnum eventType;

    @Column(name = "create_attachments")
    private Boolean createAttachments;

    @Column(name = "catalog")
    private String catalog;

    @Column(name = "attachments_type")
    private String attachmentsType;

    @Column(name = "only_last")
    private Boolean onlyLast;

    @Column(name = "is_active")
    private Boolean isActive;

    @Enumerated(EnumType.STRING)
    @Column(name = "type_complaint")
    private ClaimsRepairEnum typeComplaint;

    public EventTypeEntity() {
    }

    public EventTypeEntity(String description, EventTypeEnum eventType, Boolean createAttachments, String catalog, String attachmentsType, Boolean onlyLast, Boolean isActive, ClaimsRepairEnum typeComplaint) {
        this.description = description;
        this.eventType = eventType;
        this.createAttachments = createAttachments;
        this.catalog = catalog;
        this.attachmentsType = attachmentsType;
        this.onlyLast = onlyLast;
        this.isActive = isActive;
        this.typeComplaint = typeComplaint;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getCreateAttachments() {
        return createAttachments;
    }

    public void setCreateAttachments(Boolean createAttachments) {
        this.createAttachments = createAttachments;
    }

    public String getCatalog() {
        return catalog;
    }

    public void setCatalog(String catalog) {
        this.catalog = catalog;
    }

    public String getAttachmentsType() {
        return attachmentsType;
    }

    public void setAttachmentsType(String attachmentsType) {
        this.attachmentsType = attachmentsType;
    }

    public Boolean getOnlyLast() {
        return onlyLast;
    }

    public void setOnlyLast(Boolean onlyLast) {
        this.onlyLast = onlyLast;
    }

    public EventTypeEnum getEventType() {
        return eventType;
    }

    public void setEventType(EventTypeEnum eventType) {
        this.eventType = eventType;
    }

    public ClaimsRepairEnum getTypeComplaint() {
        return typeComplaint;
    }

    public void setTypeComplaint(ClaimsRepairEnum typeComplaint) {
        this.typeComplaint = typeComplaint;
    }

    @Override
    public String toString() {
        return "EventTypeEntity{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", eventType=" + eventType +
                ", createAttachments=" + createAttachments +
                ", catalog='" + catalog + '\'' +
                ", attachmentsType='" + attachmentsType + '\'' +
                ", onlyLast=" + onlyLast +
                ", isActive=" + isActive +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EventTypeEntity that = (EventTypeEntity) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getDescription(), that.getDescription()) &&
                getEventType() == that.getEventType() &&
                Objects.equals(getCreateAttachments(), that.getCreateAttachments()) &&
                Objects.equals(getCatalog(), that.getCatalog()) &&
                Objects.equals(getAttachmentsType(), that.getAttachmentsType()) &&
                Objects.equals(getOnlyLast(), that.getOnlyLast()) &&
                Objects.equals(getActive(), that.getActive()) &&
                getTypeComplaint() == that.getTypeComplaint();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getDescription(), getEventType(), getCreateAttachments(), getCatalog(), getAttachmentsType(), getOnlyLast(), getActive(), getTypeComplaint());
    }
}
