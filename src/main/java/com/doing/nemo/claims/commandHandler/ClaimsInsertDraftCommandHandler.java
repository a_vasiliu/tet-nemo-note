package com.doing.nemo.claims.commandHandler;

import com.doing.nemo.claims.adapter.*;
import com.doing.nemo.claims.command.ClaimsInsertDraftCommand;
import com.doing.nemo.claims.common.commandBus.Command;
import com.doing.nemo.claims.common.commandBus.CommandHandler;
import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.ClaimsNewEntity;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.jsonb.MetadataClaim;
import com.doing.nemo.claims.entity.jsonb.damaged.Damaged;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.Customer;
import com.doing.nemo.claims.entity.settings.PersonalDataEntity;
import com.doing.nemo.claims.repository.ClaimsNewRepository;
import com.doing.nemo.claims.repository.PersonalDataRepository;
import com.doing.nemo.claims.service.ClaimsService;
import com.doing.nemo.claims.service.ContractService;
import com.doing.nemo.claims.service.ConverterClaimsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Instant;
//REFACTOR
@Component
public class ClaimsInsertDraftCommandHandler implements CommandHandler {

    @Autowired
    private ClaimsNewRepository claimsRepository;

    @Autowired
    private CounterpartyAdapter counterpartyAdapter;

    @Autowired
    private FormsAdapter formsAdapter;

    @Autowired
    private ClaimsService claimsService;

    @Autowired
    private ContractService contractService;

    @Autowired
    private PersonalDataRepository personalDataRepository;

    @Autowired
    private ConverterClaimsService converterClaimsService;

    @Override
    public void handle(Command command) {

        ClaimsInsertDraftCommand claimsInsertDraftCommand = (ClaimsInsertDraftCommand) command;

        ClaimsEntity claimsEntity = new ClaimsEntity();
        claimsEntity.setId(claimsInsertDraftCommand.getClaimsId());
        claimsEntity.setUserEntity(claimsInsertDraftCommand.getUserId());
        claimsEntity.setStatus(ClaimsStatusEnum.DRAFT);

        if (claimsInsertDraftCommand.getForms() != null) {
            claimsEntity.setForms(formsAdapter.adptFormRequestToForms(claimsInsertDraftCommand.getForms()));
            claimsEntity.setWithContinuation(true);
        } else {
            claimsEntity.setWithContinuation(false);
        }

        Damaged damaged = DamagedAdapter.adptDamagedRequestToDamaged(claimsInsertDraftCommand.getDamaged());
        claimsEntity.setDamaged(damaged);
        if (claimsInsertDraftCommand.getComplaint() != null) {
            claimsEntity.setComplaint(ComplaintAdapter.adptComplaintRequestToComplaintUpdate(claimsInsertDraftCommand.getComplaint(), null));
        }

        if(damaged != null){
            Customer customer = damaged.getCustomer();
            if(customer != null){
                String customerId = customer.getCustomerId();
                if(customerId != null){
                    PersonalDataEntity customerDb = personalDataRepository.findByCustomerId(customerId);
                    //Setto i fleetManager, distinguendo tra quelli importati (passati da front-end) e quelli nel DB (presenti in customerDb)
                    damaged.setFleetManagerList(FleetManagerAdpter.adptFromFleetManagerPersonalDataToFleetManager
                            (customerDb.getFleetManagerPersonalData(), damaged.getFleetManagerList()));

                    customerDb = PersonalDataAdapter.adptFromCustomerToPersonalData(customerDb, customer, damaged.getFleetManagerList());
                    //Inserisco nella lista dei damaged i fleetmanager che non sono presenti nei fleet manager di customerdb
                    //damaged.setFleetManagerList(PersonalDataAdapter.buildFinalFleetManagerListofDamaged(customerDb.getFleetManagerPersonalData(),damaged.getFleetManagerList()));
                    personalDataRepository.save(customerDb);
                }
            }
        }

        claimsEntity.setCounterparts(counterpartyAdapter.adptCounterpartyRequestToCounterparty(claimsInsertDraftCommand.getCounterparty()));


        if (claimsInsertDraftCommand.getDeponent() != null) {
            claimsEntity.setDeponentList(DeponentAdapter.adptDeponentRequestToDeponent(claimsInsertDraftCommand.getDeponent()));
        }
        if (claimsInsertDraftCommand.getWounded() != null) {
            claimsEntity.setWoundedList(WoundedAdapter.adptWoundedRequestToWounded(claimsInsertDraftCommand.getWounded()));
        }
        claimsEntity.setCaiDetails(CaiAdapter.adptCaiToCaiResponse(claimsInsertDraftCommand.getCai()));
        if (claimsInsertDraftCommand.getWithCounterparty() &&
                claimsInsertDraftCommand.getCai() != null &&
                claimsInsertDraftCommand.getCai().getVehicleA() != null &&
                claimsInsertDraftCommand.getCai().getVehicleB() != null &&
                claimsInsertDraftCommand.getCai().getDriverSide() !=  null
        ) {
            String note = new String();
            note += "VEICOLO A: ";
            note += CaiAdapter.adptCaiConditionToCaiNote(CaiDetailsAdapter.adptCaiDetailsRequestToCaiDetails(claimsInsertDraftCommand.getCai().getVehicleA()));
            note += "\nVEICOLO B: ";
            note += CaiAdapter.adptCaiConditionToCaiNote(CaiDetailsAdapter.adptCaiDetailsRequestToCaiDetails(claimsInsertDraftCommand.getCai().getVehicleB()));
            claimsEntity.getDamaged().getImpactPoint().setIncidentDescription(note);
        } else if (!claimsInsertDraftCommand.getWithCounterparty()){
            claimsEntity.getDamaged().getImpactPoint().setIncidentDescription(claimsInsertDraftCommand.getDamaged().getImpactPoint().getIncidentDescription());
        }
        if((claimsInsertDraftCommand.getComplaint() != null && claimsInsertDraftCommand.getComplaint().getDataAccident() != null && claimsInsertDraftCommand.getComplaint().getDataAccident().getTypeAccident() == null)){
            if(claimsService.checkIsCard(claimsEntity.getDamaged(), claimsEntity.getCounterparts()))
                claimsEntity.getComplaint().getDataAccident().setTypeAccident(DataAccidentTypeAccidentEnum.CARD_NON_VERIFICABILE_FIRMA_SINGOLA);
            else
                claimsEntity.getComplaint().getDataAccident().setTypeAccident(DataAccidentTypeAccidentEnum.RC_NON_VERIFICABILE);
        }else {
            claimsEntity.getComplaint().getDataAccident().setTypeAccident(claimsInsertDraftCommand.getComplaint().getDataAccident().getTypeAccident());
        }
        claimsEntity.setType(contractService.getPersonalRiskFlowType(claimsInsertDraftCommand.getDamaged().getContract().getContractType(),
                claimsEntity.getComplaint().getDataAccident().getTypeAccident(),
                claimsInsertDraftCommand.getDamaged().getCustomer().toString()));

        if (claimsInsertDraftCommand.getPaiComunication() != null) {
            claimsEntity.setPaiComunication(claimsInsertDraftCommand.getPaiComunication());
        }
        if (claimsInsertDraftCommand.getWithCounterparty() != null) {
            claimsEntity.setWithCounterparty(claimsInsertDraftCommand.getWithCounterparty());
        }
        if (claimsInsertDraftCommand.getFoundModel() != null) {
            claimsEntity.setFoundModel(FoundModelAdapter.adptFoundModelRequestToFoundModel(claimsInsertDraftCommand.getFoundModel()));
        }
        if (claimsInsertDraftCommand.getIdSaleforce() != null) {
            claimsEntity.setIdSaleforce(claimsInsertDraftCommand.getIdSaleforce());
        }

        MetadataClaim metadataClaim = new MetadataClaim();
        metadataClaim.setMetadata(claimsInsertDraftCommand.getMetadata());
        claimsEntity.setMetadata(metadataClaim);
        Instant nowInstant = DateUtil.getNowInstant();
        claimsEntity.setCreatedAt(nowInstant);
        claimsEntity.setUpdateAt(nowInstant);

        claimsEntity.setForced(false);
        claimsEntity.setInEvidence(false);
        claimsEntity.setPoVariation(false);
        claimsEntity.setCreatedAt(DateUtil.getNowInstant());
        claimsEntity.setUpdateAt(claimsEntity.getCreatedAt());
        claimsEntity.setAuthorityLinkable(false);
        claimsEntity.setMigrated(false);
        if (claimsInsertDraftCommand.getRefund() != null) {
            claimsEntity.setRefund(RefundAdapter.adptFromRefundRequestToRefund(claimsInsertDraftCommand.getRefund()));
        }

        ClaimsNewEntity claimsNewEntity = converterClaimsService.convertOldEntityToNewClaimsEntity(claimsEntity);

        claimsRepository.save(claimsNewEntity);
    }
}
