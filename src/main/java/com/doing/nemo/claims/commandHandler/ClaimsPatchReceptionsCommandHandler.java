package com.doing.nemo.claims.commandHandler;

import com.doing.nemo.claims.command.ClaimsPatchReceptionsCommand;
import com.doing.nemo.claims.common.commandBus.Command;
import com.doing.nemo.claims.common.commandBus.CommandHandler;
import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.ClaimsNewEntity;
import com.doing.nemo.claims.entity.jsonb.Theft;
import com.doing.nemo.claims.repository.ClaimsNewRepository;
import com.doing.nemo.claims.repository.ClaimsRepository;
import com.doing.nemo.claims.service.ConverterClaimsService;
import com.doing.nemo.claims.service.DwhClaimsService;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ClaimsPatchReceptionsCommandHandler implements CommandHandler {

    private static Logger LOGGER = LoggerFactory.getLogger(ClaimsPatchReceptionsCommandHandler.class);

    @Autowired
    private ClaimsNewRepository claimsNewRepository;

    @Autowired
    private DwhClaimsService dwhClaimsService;

    @Autowired
    private ConverterClaimsService converterClaimsService;

    @Value("${dwh.call}")
    private Boolean dwhCall;


    @Override
    public void handle(Command command) {
        //REFACTOR

        ClaimsPatchReceptionsCommand claimsPatchReceptionsCommand = (ClaimsPatchReceptionsCommand) command;
        //recupero la nuova entità
        ClaimsNewEntity claimsNewEntity = claimsNewRepository.getOne(claimsPatchReceptionsCommand.getId());
        //conversione nella vecchia entità
        ClaimsEntity claimsEntity = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntity);

        if (claimsEntity == null) {
            LOGGER.debug("Claims with UUID " + claimsPatchReceptionsCommand.getId() + " not found ");
            throw new NotFoundException("Claims with UUID " + claimsPatchReceptionsCommand.getId() + " not found ", MessageCode.CLAIMS_1010);
        }

        if (claimsEntity.getTheft() == null)
            claimsEntity.setTheft(new Theft());

        if (claimsPatchReceptionsCommand.getFirstKeyReception() != null || claimsPatchReceptionsCommand.getSecondKeyReception() != null || claimsPatchReceptionsCommand.getOriginalComplaintReception() != null ||
                claimsPatchReceptionsCommand.getCopyComplaintReception() != null || claimsPatchReceptionsCommand.getOriginalReportReception() != null || claimsPatchReceptionsCommand.getCopyReportReception() != null) {
            claimsEntity.getTheft().setWithReceptions(true);
        }

        if (claimsPatchReceptionsCommand.getFirstKeyReception() != null)
            claimsEntity.getTheft().setFirstKeyReception(claimsPatchReceptionsCommand.getFirstKeyReception());

        if (claimsPatchReceptionsCommand.getSecondKeyReception() != null)
            claimsEntity.getTheft().setSecondKeyReception(claimsPatchReceptionsCommand.getSecondKeyReception());

        if (claimsPatchReceptionsCommand.getOriginalComplaintReception() != null)
            claimsEntity.getTheft().setOriginalComplaintReception(claimsPatchReceptionsCommand.getOriginalComplaintReception());

        if (claimsPatchReceptionsCommand.getCopyComplaintReception() != null)
            claimsEntity.getTheft().setCopyComplaintReception(claimsPatchReceptionsCommand.getCopyComplaintReception());

        if (claimsPatchReceptionsCommand.getOriginalReportReception() != null)
            claimsEntity.getTheft().setOriginalReportReception(claimsPatchReceptionsCommand.getOriginalReportReception());

        if (claimsPatchReceptionsCommand.getCopyReportReception() != null)
            claimsEntity.getTheft().setCopyReportReception(claimsPatchReceptionsCommand.getCopyReportReception());

        claimsEntity.setUpdateAt(DateUtil.getNowInstant());

        //conversione nella nuova entità
        claimsNewEntity = converterClaimsService.convertOldEntityToNewClaimsEntity(claimsEntity);
        claimsNewRepository.save(claimsNewEntity);

        if(dwhCall){
            dwhClaimsService.sendMessage(claimsEntity, com.doing.nemo.dwh.client.payload.request.EventTypeEnum.UPDATED);
        }


    }
}
