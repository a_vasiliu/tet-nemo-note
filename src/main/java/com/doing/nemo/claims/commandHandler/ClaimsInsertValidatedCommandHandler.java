package com.doing.nemo.claims.commandHandler;

import com.doing.nemo.claims.adapter.*;
import com.doing.nemo.claims.command.ClaimsInsertValidatedCommand;
import com.doing.nemo.claims.common.commandBus.Command;
import com.doing.nemo.claims.common.commandBus.CommandHandler;
import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.CounterpartyRequest;
import com.doing.nemo.claims.dto.KafkaEventDTO;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.ClaimsNewEntity;
import com.doing.nemo.claims.entity.CounterpartyEntity;
import com.doing.nemo.claims.entity.CounterpartyNewEntity;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsFlowEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.enumerated.KafkaEventTypeEnum;
import com.doing.nemo.claims.entity.jsonb.MetadataClaim;
import com.doing.nemo.claims.entity.jsonb.complaint.Complaint;
import com.doing.nemo.claims.entity.jsonb.complaint.jsonbComplaint.jsonbDataAccidents.DataAccident;
import com.doing.nemo.claims.entity.jsonb.damaged.Damaged;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.Customer;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbAntiTheftService.AntiTheftRequest;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbAntiTheftService.AntiTheftService;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbAntiTheftService.Registry;
import com.doing.nemo.claims.entity.settings.AntiTheftServiceEntity;
import com.doing.nemo.claims.entity.settings.ContractTypeEntity;
import com.doing.nemo.claims.entity.settings.PersonalDataEntity;
import com.doing.nemo.claims.entity.settings.RecoverabilityEntity;
import com.doing.nemo.claims.repository.*;
import com.doing.nemo.claims.service.ClaimsService;
import com.doing.nemo.claims.service.ContractService;
import com.doing.nemo.claims.service.IncidentService;
import com.doing.nemo.claims.service.RecoverabilityService;
import com.doing.nemo.claims.service.*;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.claims.websin.WebSinServiceManager;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.dwh.client.payload.request.EventTypeEnum;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.net.MalformedURLException;
import java.time.Instant;
import java.util.*;

//REFACTOR
@Component
public class ClaimsInsertValidatedCommandHandler implements CommandHandler {

    private static Logger LOGGER = LoggerFactory.getLogger(ClaimsInsertValidatedCommandHandler.class);
    private static Set<DataAccidentTypeAccidentEnum> activeSet = new HashSet<DataAccidentTypeAccidentEnum>() {{
        add(DataAccidentTypeAccidentEnum.RC_ATTIVA);
        add(DataAccidentTypeAccidentEnum.RC_CONCORSUALE);
        add(DataAccidentTypeAccidentEnum.CARD_ATTIVA_FIRMA_SINGOLA);
        add(DataAccidentTypeAccidentEnum.CARD_CONCORSUALE_FIRMA_SINGOLA);
        add(DataAccidentTypeAccidentEnum.CARD_ATTIVA_DOPPIA_FIRMA);
        add(DataAccidentTypeAccidentEnum.CARD_CONCORSUALE_DOPPIA_FIRMA);
    }};
    @Autowired
    private ClaimsNewRepository claimsNewRepository;
    @Autowired
    private ContractService contractService;
    @Autowired
    private PersonalDataRepository personalDataRepository;
    @Autowired
    private CounterpartyAdapter counterpartyAdapter;
    @Autowired
    private NotesAdapter notesAdapter;
    @Autowired
    private ClaimsService claimsService;
    @Autowired
    private AntiTheftServiceRepository antiTheftServiceRepository;
    @Autowired
    private RecoverabilityService recoverabilityService;
    @Autowired
    private IncidentService incidentService;
    @Autowired
    private DwhClaimsService dwhClaimsService;
    @Autowired
    private GoLiveStrategyService goLiveStrategyService;
    @Autowired
    private ExternalCommunicationService externalCommunicationService;

    @Autowired(required = false)
    private NemoEventProducerService nemoEventProducerService;

    @Value("${dwh.call}")
    private Boolean dwhCall;

    @Value("${kafka.topic}")
    private String kafkaTopic;

    @Autowired
    private WebSinServiceManager webSinServiceManager;


    @Autowired
    private CounterpartyNewRepository counterpartyNewRepository;

    @Autowired
    private CounterpartyService counterpartyService;

    @Autowired
    private CacheService cacheService;

    @Autowired
    private AntiTheftRequestService antiTheftRequestService;

    @Autowired
    private ConverterClaimsService converterClaimsService;

    private static final String CACHENAME = "claims.";
    private static final String STATSkEY = "claimsStats::";
    private static final String STATSEVIDENCE = "evidenceStats::";
    private static final String STATSTHEFT = "theftStats::";
    private static final String STATSSTATUS = "statusStats::";
    private static final String COUNTERPARTYKEY = "counterpartyStats::";
    private static final String STATSkEY2 = "claimsV2Stats::";
    private static final String STATSEVIDENCECONTINUATION = "evidenceContinuationStats::";

    @Override
    public void handle(Command command) {

        ClaimsInsertValidatedCommand claimsInsertCommand = (ClaimsInsertValidatedCommand) command;
        ClaimsEntity claimsEntity = this.saveClaim(claimsInsertCommand);

        List<CounterpartyEntity> counterpartyEntityList = new LinkedList<>();
        if(claimsInsertCommand.getCounterparty() != null) {
            for (CounterpartyRequest counterpartyRequest : claimsInsertCommand.getCounterparty()) {
                CounterpartyEntity counterpartyEntity = counterpartyAdapter.adptCounterpartyRequestToCounterparty(counterpartyRequest);
                UUID uuid = UUID.randomUUID();
                counterpartyEntity.setCounterpartyId(uuid.toString());
                counterpartyEntity.setClaims(claimsEntity);
                counterpartyEntityList.add(counterpartyEntity);
                if (counterpartyRequest.getUserCreate() == null || counterpartyRequest.getUserCreate().equalsIgnoreCase("")){
                    counterpartyEntity.setUserCreate(claimsInsertCommand.getUserId());
                }
                CounterpartyNewEntity counterpartyNewEntity = CounterpartyAdapter.adptCounterpartyOldToCounterpartyNew(counterpartyEntity);

                counterpartyNewRepository.save(counterpartyNewEntity);
            }
        }
        claimsService.checkALDvsALD(claimsEntity);
        ClaimsNewEntity claimsNewEntity1 = claimsNewRepository.getOne(claimsEntity.getId());
        claimsEntity = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntity1);
        if(!claimsInsertCommand.getIncomplete()) {
            if (!claimsEntity.getStatus().equals(ClaimsStatusEnum.INCOMPLETE) && !claimsEntity.getStatus().equals(ClaimsStatusEnum.WAITING_FOR_VALIDATION)) {
                counterpartyService.EligibilityCriterion(claimsEntity,claimsInsertCommand.getUserId(),claimsInsertCommand.getUserName());
                if (claimsEntity.getCounterparts() != null && !claimsEntity.getCounterparts().isEmpty() && claimsEntity.getCounterparts().get(0).getEligibility() != null && claimsEntity.getCounterparts().get(0).getEligibility()) {
                    cacheService.deleteClaimsStats(CACHENAME, COUNTERPARTYKEY);
                }
            }
        }
        if(dwhCall){
            dwhClaimsService.sendMessage(claimsEntity, EventTypeEnum.CREATED);
            if(claimsEntity.getCounterparts() != null) {
                for (CounterpartyEntity counterparty : claimsEntity.getCounterparts()) {
                    dwhClaimsService.sendMessage(counterparty, com.doing.nemo.dwh.client.payload.request.EventTypeEnum.CREATED);
                }
            }
        }

        cacheService.deleteClaimsStats(CACHENAME, STATSkEY);
        cacheService.deleteClaimsStats(CACHENAME, STATSEVIDENCE);

        cacheService.deleteClaimsStats(CACHENAME, STATSEVIDENCECONTINUATION);
        cacheService.deleteClaimsStats(CACHENAME, STATSkEY2);

        if(claimsEntity.getComplaint().getDataAccident().getTypeAccident().equals(DataAccidentTypeAccidentEnum.FURTO_TOTALE) &&
            claimsEntity.getDamaged() != null && claimsEntity.getDamaged().getContract() != null && claimsEntity.getDamaged().getContract().getContractType().equalsIgnoreCase("pol")
        ){
            LOGGER.debug("kakfa calling");
            KafkaEventDTO kafkaEventDTO = KafkaAdapter.adptFromClaimsToKafkaEvent(claimsEntity);
            System.out.println(kafkaEventDTO);
            nemoEventProducerService.produceEvent(kafkaEventDTO, kafkaTopic, KafkaEventTypeEnum.THEFT);
        }

        ClaimsNewEntity claimsNewEntity = claimsNewRepository.getOne(claimsInsertCommand.getClaimsId());

        if (claimsEntity.getDamaged().getAntiTheftService() != null) {

            // creazione della riga di storico di octo/texa
            String idAntiTheftRequest = UUID.randomUUID().toString();
            antiTheftRequestService.insertAntiTheftRequest(idAntiTheftRequest, claimsNewEntity,claimsEntity.getDamaged().getAntiTheftService().getProviderType(), "1");
            claimsService.callToOctoAsync(claimsNewEntity, claimsNewEntity.getStatus(), new AntiTheftRequest(), claimsInsertCommand.getUserId(), claimsInsertCommand.getUserName(), idAntiTheftRequest);
        }
/*        try {
            webSinServiceManager.putComplaint(claimsEntity);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }*/
    }

    @Transactional(isolation = Isolation.SERIALIZABLE)
    public ClaimsEntity saveClaim(ClaimsInsertValidatedCommand claimsInsertCommand){
        if(claimsInsertCommand.getComplaint().getDataAccident().getTypeAccident() == null && !claimsInsertCommand.getWithCounterparty()
        ) {
            LOGGER.debug(MessageCode.CLAIMS_1068.value());
            throw new BadRequestException(MessageCode.CLAIMS_1068);
        }


        ClaimsEntity claimsEntity = new ClaimsEntity();

        claimsEntity.setId(claimsInsertCommand.getClaimsId());
        String codAntiTheftService = "";

        //recupero delle informazioni legata alla compagnia assicuratrice CARD/NO CARD
        Damaged damaged = DamagedAdapter.adptDamagedRequestInsertToDamaged(claimsInsertCommand.getDamaged());
        claimsEntity.setDamaged(damaged);
        //mappatura del flusso attraverso il tipo contratto e il tipo sinistro ottenuti dalle tabelle di websin

        if(damaged != null){
            Customer customer = damaged.getCustomer();
            if(customer != null){
                String customerId = customer.getCustomerId();
                if(customerId != null){
                    PersonalDataEntity customerDb = personalDataRepository.findByCustomerId(customerId);
                    if(customerDb!=null) {
                        //Setto i fleetManager, distinguendo tra quelli importati (passati da front-end) e quelli nel DB (presenti in customerDb)
                        damaged.setFleetManagerList(FleetManagerAdpter.adptFromFleetManagerPersonalDataToFleetManager
                                (customerDb.getFleetManagerPersonalData(), damaged.getFleetManagerList()));

                        customerDb = PersonalDataAdapter.adptFromCustomerToPersonalData(customerDb, customer, damaged.getFleetManagerList());
                    }else{
                        customerDb = PersonalDataAdapter.adptFromCustomerToPersonalData(customerDb, customer, damaged.getFleetManagerList());
                        //Inserisco nella lista dei damaged i fleetmanager che non sono presenti nei fleet manager di customerdb
                        damaged.setFleetManagerList(PersonalDataAdapter.buildFinalFleetManagerListofDamaged(customerDb.getFleetManagerPersonalData(),damaged.getFleetManagerList()));
                    }
                    //Inserisco nella lista dei damaged i fleetmanager che non sono presenti nei fleet manager di customerdb
                    //damaged.setFleetManagerList(PersonalDataAdapter.buildFinalFleetManagerListofDamaged(customerDb.getFleetManagerPersonalData(),damaged.getFleetManagerList()));
                    personalDataRepository.save(customerDb);
                }
            }
        }

        claimsEntity.setMotivation(claimsInsertCommand.getMotivation());
        claimsEntity.setUserEntity(claimsInsertCommand.getUserId());

        //di defaul è in waiting_for_validation

        claimsEntity.setStatus(ClaimsStatusEnum.WAITING_FOR_AUTHORITY);
        claimsEntity.setAuthorityLinkable(true);

        claimsEntity.setNotes(NotesAdapter.adptNotesRequestToNotes(claimsInsertCommand.getNotes()));

        if (claimsInsertCommand.getCounterparty() != null) {
            String plate = null;
            if (claimsEntity.getDamaged() != null && claimsEntity.getDamaged().getVehicle() != null && claimsEntity.getDamaged().getVehicle().getLicensePlate() != null)
                plate = claimsEntity.getDamaged().getVehicle().getLicensePlate();
            if (plate != null) {
                for (CounterpartyRequest counterpartyRequest : claimsInsertCommand.getCounterparty()) {
                    if (counterpartyRequest.getVehicle() != null && counterpartyRequest.getVehicle().getLicensePlate() != null) {
                        if (counterpartyRequest.getVehicle().getLicensePlate().equals(plate)) {
                            LOGGER.debug(MessageCode.CLAIMS_1041.value());
                            throw new BadRequestException(MessageCode.CLAIMS_1041);
                        }
                    }
                }
            }
        }

        //claimsEntity.setCounterparts(counterpartyAdapter.adptCounterpartyRequestToCounterparty(claimsInsertCommand.getCounterparty()));

        //mappatura del flusso attraverso il tipo contratto e il tipo sinistro ottenuti dalle tabelle di websin
        claimsEntity.setComplaint(ComplaintAdapter.adptComplaintRequestToComplaintUpdate(claimsInsertCommand.getComplaint(), null));
        claimsEntity.setDeponentList(DeponentAdapter.adptDeponentRequestToDeponent(claimsInsertCommand.getDeponent()));
        claimsEntity.setWoundedList(WoundedAdapter.adptWoundedRequestToWounded(claimsInsertCommand.getWounded()));
        claimsEntity.setCaiDetails(CaiAdapter.adptCaiToCaiResponse(claimsInsertCommand.getCai()));
        if (claimsInsertCommand.getWithCounterparty() && claimsInsertCommand.getCai() != null &&
                claimsInsertCommand.getCai().getVehicleA() != null && claimsInsertCommand.getCai().getVehicleB() != null &&
                claimsInsertCommand.getCai().getDriverSide() != null
        ) {
            String note = new String();
            note += "VEICOLO A: ";
            note += CaiAdapter.adptCaiConditionToCaiNote(CaiDetailsAdapter.adptCaiDetailsRequestToCaiDetails(claimsInsertCommand.getCai().getVehicleA()));
            note += "\nVEICOLO B: ";
            note += CaiAdapter.adptCaiConditionToCaiNote(CaiDetailsAdapter.adptCaiDetailsRequestToCaiDetails(claimsInsertCommand.getCai().getVehicleB()));
            if (claimsEntity.getDamaged().getImpactPoint() != null)
                claimsEntity.getDamaged().getImpactPoint().setIncidentDescription(note);
            if (claimsInsertCommand.getComplaint() != null && claimsInsertCommand.getComplaint().getDataAccident() != null && claimsInsertCommand.getComplaint().getDataAccident().getTypeAccident() == null) {
                if (claimsInsertCommand.getCai().getVehicleA() != null && claimsInsertCommand.getCai().getVehicleB() != null) {
                    Boolean isCaiSignedA = false;
                    Boolean isCaiSignedB = false;
                    String companyDenomination = "";

                    if (claimsEntity.getDamaged() != null) {
                        isCaiSignedA = claimsEntity.getDamaged().getCaiSigned();
                        if (claimsEntity.getDamaged().getInsuranceCompany() != null && claimsEntity.getDamaged().getInsuranceCompany().getTpl() != null) {
                            companyDenomination = claimsEntity.getDamaged().getInsuranceCompany().getTpl().getCompany();
                        }
                    }
                    if (claimsInsertCommand.getCounterparty() != null && claimsInsertCommand.getCounterparty().size() > 0) {
                        isCaiSignedB = claimsEntity.getCounterparts().get(0).getCaiSigned();
                    }

                    if (isCaiSignedA == null) {
                        isCaiSignedA = false;
                    }

                    if (isCaiSignedB == null) {
                        isCaiSignedB = false;
                    }

                    Map<String, Object> caiResult = claimsService.getFlowDetailsByCai(claimsEntity.getCaiDetails(), claimsEntity.getDamaged().getContract().getContractType(), claimsEntity.getDamaged(), claimsEntity.getCounterparts(), isCaiSignedA, isCaiSignedB);

                    claimsEntity.setType((ClaimsFlowEnum) caiResult.get("flow"));
                    DataAccident dataAccident = claimsEntity.getComplaint().getDataAccident();

                    dataAccident.setTypeAccident((DataAccidentTypeAccidentEnum) caiResult.get("type"));
                    Complaint complaint = claimsEntity.getComplaint();
                    complaint.setDataAccident(dataAccident);
                    claimsEntity.setComplaint(complaint);
                }

            }
        } else if (!claimsInsertCommand.getWithCounterparty()) {
            claimsEntity.getDamaged().getImpactPoint().setIncidentDescription(claimsInsertCommand.getDamaged().getImpactPoint().getIncidentDescription());
        } else if ((claimsInsertCommand.getCai() == null ||
                (claimsInsertCommand.getCai().getVehicleA() == null && claimsInsertCommand.getCai().getVehicleB() == null &&
                        claimsInsertCommand.getCai().getDriverSide() == null)) && (claimsInsertCommand.getComplaint() != null && claimsInsertCommand.getComplaint().getDataAccident() != null && claimsInsertCommand.getComplaint().getDataAccident().getTypeAccident() == null)) {
            LOGGER.debug(MessageCode.CLAIMS_1040.value());
            throw new BadRequestException(MessageCode.CLAIMS_1040);
        }

        if(claimsEntity.getCounterparts() != null && claimsEntity.getCounterparts().size() == 1){
            if(activeSet.contains(claimsEntity.getComplaint().getDataAccident().getTypeAccident()))
                claimsEntity.getCounterparts().get(0).setResponsible(true);
        }

        try {
            ContractTypeEntity contractTypeEntity = contractService.getContractType(claimsInsertCommand.getDamaged().getContract().getContractType());

            if(contractTypeEntity == null){
                LOGGER.debug(MessageCode.CLAIMS_1010.value());
                throw new BadRequestException(MessageCode.CLAIMS_1010);
            }

            if (contractTypeEntity.getFlagWS() != null && !contractTypeEntity.getFlagWS()) {
                LOGGER.debug(MessageCode.CLAIMS_1042.value());
                throw new BadRequestException(MessageCode.CLAIMS_1042);
            } else {
                claimsEntity.setType(contractService.getPersonalRiskFlowType(claimsInsertCommand.getDamaged().getContract().getContractType(),
                        claimsEntity.getComplaint().getDataAccident().getTypeAccident(),
                        claimsInsertCommand.getDamaged().getCustomer().getCustomerId().toString())); /* tipo di flusso */
                if (claimsEntity.getType().equals(ClaimsFlowEnum.NO)) {
                    LOGGER.debug("It's not possible insert a claim with NO flow");
                    throw new BadRequestException("It's not possible insert a claim with NO flow", MessageCode.CLAIMS_1057);
                }

            }
        } catch (BadRequestException e) {
            LOGGER.debug(e.getMessage());
            throw e;
        }
        claimsEntity.setIdSaleforce(claimsInsertCommand.getIdSaleforce());
        claimsEntity.setWithCounterparty(claimsInsertCommand.getWithCounterparty());
        claimsEntity.setPaiComunication(claimsInsertCommand.getCaiComunication());
        claimsEntity.setForced(claimsInsertCommand.getForced());
        claimsEntity.setInEvidence(claimsInsertCommand.getInEvidence());
        claimsEntity.setWithContinuation(false);
        claimsEntity.setPoVariation(false);
        claimsEntity.setMigrated(false);
        if (claimsInsertCommand.getRefund() != null) {
            claimsEntity.setRefund(RefundAdapter.adptFromRefundRequestToRefund(claimsInsertCommand.getRefund()));
        }

        if (claimsInsertCommand.getTheft() != null) {
            claimsEntity.setTheft(TheftClaimsAdapter.adptTheftRequestV1ToTheft(claimsInsertCommand.getTheft()));
            claimsEntity.getTheft().setWithReceptions(false);
            if (claimsEntity.getTheft().getFirstKeyReception() != null || claimsEntity.getTheft().getSecondKeyReception() != null
                    || claimsEntity.getTheft().getOriginalComplaintReception() != null || claimsEntity.getTheft().getOriginalReportReception() != null) {
                claimsEntity.getTheft().setWithReceptions(true);
            }
        }

        //aggiunta metadati

        MetadataClaim metadataClaim = new MetadataClaim();
        metadataClaim.setMetadata(claimsInsertCommand.getMetadata());
        claimsEntity.setMetadata(metadataClaim);

        Instant nowInstant = DateUtil.getNowInstant();
        claimsEntity.setCreatedAt(nowInstant);
        claimsEntity.setUpdateAt(nowInstant);


        if (claimsEntity.getType() != null && claimsEntity.getComplaint() != null && claimsEntity.getComplaint().getDataAccident() != null && claimsEntity.getComplaint().getDataAccident().getTypeAccident() != null) {
            RecoverabilityEntity recoverabilityEntity = recoverabilityService.getRecoverabilityByFlowAndTypeClaim(claimsEntity.getType().getValue().toUpperCase(), claimsEntity.getComplaint().getDataAccident().getTypeAccident());
            LOGGER.info("[RECOVERABILITY] "  + recoverabilityEntity);
            if(recoverabilityEntity != null) {
                claimsEntity.getComplaint().getDataAccident().setRecoverability(recoverabilityEntity.getRecoverability());
                claimsEntity.getComplaint().getDataAccident().setRecoverabilityPercent(recoverabilityEntity.getPercentRecoverability());
            }
        }

        damaged = claimsEntity.getDamaged();
        AntiTheftService antiTheftService = null;
        if (damaged.getAntiTheftService() != null) {

            //recupero localizzatore da tabella setting AntiTheftServiceEntity
            //da capire se avremo mai un localizzatore per il furto e se ci viene passato un tipo
            if (damaged.getAntiTheftService().getRegistryList() != null && !damaged.getAntiTheftService().getRegistryList().isEmpty()) {

                //for(Registry currentRegistry : damaged.getAntiTheftService().getRegistryList()){
                Registry currentRegistry = damaged.getAntiTheftService().getRegistryList().get(0);
                codAntiTheftService = AntiTheftServiceAdapter.adptProviderToAntiTheftServiceEntity(currentRegistry.getCodPack());
                AntiTheftServiceEntity antiTheftServiceEntity = antiTheftServiceRepository.findAntiTheftByCodAntiTheftService(codAntiTheftService);
                antiTheftService = AntiTheftServiceAdapter.adptAntiTheftServiceEntityToAntiTheftService(antiTheftServiceEntity);
                if(antiTheftService != null) {
                    antiTheftService.setRegistryList(damaged.getAntiTheftService().getRegistryList());
                    claimsEntity.getDamaged().setAntiTheftService(antiTheftService);
                }

            }
        }


        claimsEntity.setWithCounterparty(claimsInsertCommand.getWithCounterparty());
        ClaimsNewEntity claimsNewEntity =  converterClaimsService.convertOldEntityToNewClaimsEntity(claimsEntity);
        goLiveStrategyService.goLiveStrategyChecksForInsert(claimsEntity.getDamaged().getCustomer().getCustomerId(),claimsEntity.getComplaint().getDataAccident().getTypeAccident());
        //Save del nuovo sinistro
         claimsNewRepository.saveAndFlush(claimsNewEntity);

        claimsNewEntity = claimsNewRepository.getOne(claimsNewEntity.getId());

        claimsEntity = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntity);
        claimsEntity.setPracticeId(claimsNewEntity.getPracticeId());

        claimsEntity.setCounterparts(counterpartyAdapter.adptCounterpartyRequestToCounterparty(claimsInsertCommand.getCounterparty()));
        claimsEntity.setWithCounterparty(claimsInsertCommand.getWithCounterparty());
        claimsEntity = claimsService.checkStatusByFlowInternal(claimsEntity, claimsInsertCommand.getUserName(), claimsInsertCommand.getEmailTemplateListRequestV1List(), claimsInsertCommand.getIncomplete());
        claimsEntity.setCounterparts(new LinkedList<>());
        claimsNewEntity = converterClaimsService.convertOldEntityToNewClaimsEntity(claimsEntity);
        claimsNewRepository.save(claimsNewEntity);


        //claimsEntity = claimsRepository.getOne(claimsInsertCommand.getClaimsId());
        //claimsService.checkALDvsALD(claimsEntity);
        //claimsEntity = claimsRepository.getOne(claimsInsertCommand.getClaimsId());





        try {
            //intervento 6 - R
            externalCommunicationService.insertIncidentAsync(claimsInsertCommand.getClaimsId(),null);
        } catch (IOException e) {
            throw new BadRequestException(MessageCode.CLAIMS_1112);
        }

        claimsEntity = claimsService.findById(claimsInsertCommand.getClaimsId());
        //claimsRepository.save(claimsEntity);
        return claimsEntity;
    }
}
