package com.doing.nemo.claims.commandHandler;

import com.doing.nemo.claims.adapter.*;
import com.doing.nemo.claims.command.CounterpartyPatchCommand;
import com.doing.nemo.claims.common.commandBus.Command;
import com.doing.nemo.claims.common.commandBus.CommandHandler;
import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.CounterpartyRequest;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.ClaimsNewEntity;
import com.doing.nemo.claims.entity.CounterpartyEntity;
import com.doing.nemo.claims.entity.CounterpartyNewEntity;
import com.doing.nemo.claims.repository.ClaimsNewRepository;
import com.doing.nemo.claims.repository.CounterpartyNewRepository;
import com.doing.nemo.claims.service.ConverterClaimsService;
import com.doing.nemo.claims.service.DwhClaimsService;
import com.doing.nemo.claims.service.ManagerService;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.dwh.client.payload.request.EventTypeEnum;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.Optional;

@Component
public class CounterpartyPatchCommandHandler implements CommandHandler {

    private static Logger LOGGER = LoggerFactory.getLogger(CounterpartyPatchCommandHandler.class);

    @Autowired
    private ClaimsNewRepository claimsRepository;

    @Autowired
    private CounterpartyNewRepository counterpartyRepository;

    @Autowired
    private InsuranceCompanyCounterpartyAdapter insuranceCompanyCounterpartyAdapter;

    @Autowired
    private ConverterClaimsService converterClaimsService;

    @Autowired
    private LastContactAdapter lastContactAdapter;

    @Autowired
    private ManagerService managerService;

    @Autowired
    private AssigneeRepairAdapter assigneeRepairAdapter;

    @Autowired
    private DwhClaimsService dwhClaimsService;

    @Value("${dwh.call}")
    private Boolean dwhCall;

    @Override
    public void handle(Command command) {
        CounterpartyPatchCommand counterpartyPatchCommand = (CounterpartyPatchCommand) command;
        Optional<CounterpartyNewEntity> counterpartyNewEntityOptional = counterpartyRepository.findById(counterpartyPatchCommand.getCounterpartyId());

        if (!counterpartyNewEntityOptional.isPresent()) {
            throw new NotFoundException("Counterparty with UUID " + counterpartyPatchCommand.getCounterpartyId() + " not found", MessageCode.CLAIMS_1010);
        }

        CounterpartyNewEntity counterparty = counterpartyNewEntityOptional.get();
        ClaimsEntity claimsEntity = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(counterpartyNewEntityOptional.get().getClaims());

        String plate = null;
        if(claimsEntity.getDamaged() != null && claimsEntity.getDamaged().getVehicle() != null && claimsEntity.getDamaged().getVehicle().getLicensePlate() != null)
            plate = claimsEntity.getDamaged().getVehicle().getLicensePlate();
        if (counterpartyPatchCommand.getVehicle() != null) {
            if(counterpartyPatchCommand.getVehicle().getLicensePlate() != null){
                if(plate != null){
                    if(counterpartyPatchCommand.getVehicle().getLicensePlate().equals(plate))
                        throw new BadRequestException(MessageCode.CLAIMS_1041);
                }
            }
            counterparty.setVehicle(VehicleAdapter.adptVehicleRequestToVehicle(counterpartyPatchCommand.getVehicle()));
        }
        if (counterpartyPatchCommand.getType() != null) {
            counterparty.setType(counterpartyPatchCommand.getType());
        }
        if (counterpartyPatchCommand.getRepairProcedure() != null) {
            counterparty.setRepairProcedure(counterpartyPatchCommand.getRepairProcedure());
        }
        if (counterpartyPatchCommand.getUserUpdate() != null) {
            counterparty.setUserUpdate(counterpartyPatchCommand.getUserUpdate());
        }
        if (counterpartyPatchCommand.getAssignedTo() != null) {
            counterparty.setAssignedTo(AssigneeRepairAdapter.adptFromAssignationRepairRequestToAssignationRepair(counterpartyPatchCommand.getAssignedTo()));
            counterparty.setAssignedAt(DateUtil.getNowInstant());
        }
        if (counterpartyPatchCommand.getInsured() != null) {
            counterparty.setInsured(InsuredAdapter.adptInsuredRequestToInsured(counterpartyPatchCommand.getInsured()));
        }
        if (counterpartyPatchCommand.getInsuranceCompany() != null) {
            counterparty.setInsuranceCompany(insuranceCompanyCounterpartyAdapter.adptFromCompanyCounterpartyRequestToCompanyCounterparty(counterpartyPatchCommand.getInsuranceCompany()));
        }
        if (counterpartyPatchCommand.getDriver() != null) {
            counterparty.setDriver(DriverAdapter.adptDriverRequestToDriver(counterpartyPatchCommand.getDriver()));
        }
        if (counterpartyPatchCommand.getCaiSigned() != null) {
            counterparty.setCaiSigned(counterpartyPatchCommand.getCaiSigned());
        }
        if (counterpartyPatchCommand.getImpactPoint() != null) {
            counterparty.setImpactPoint(ImpactPointAdapter.adptImpactPointRequestToImpactPoint(counterpartyPatchCommand.getImpactPoint()));
        }
        if (counterpartyPatchCommand.getResponsible() != null) {
            counterparty.setResponsible(counterpartyPatchCommand.getResponsible());
        }
        if (counterpartyPatchCommand.getEligibility() != null) {
            counterparty.setEligibility(counterpartyPatchCommand.getEligibility());
        }
        if (counterpartyPatchCommand.getDescription() != null) {
            counterparty.setDescription(counterpartyPatchCommand.getDescription());
        }
        if (counterpartyPatchCommand.getLastContact() != null) {
            counterparty.setLastContact(lastContactAdapter.adtpFromLastContactListRequestToLastContactListEntity(counterpartyPatchCommand.getLastContact()));
        }
        if (counterpartyPatchCommand.getManager() != null) {
            counterparty.setManager(managerService.getManager(counterpartyPatchCommand.getManager().getId()));
        }
        if (counterpartyPatchCommand.getCanalization() != null) {
            counterparty.setCanalization(CanalizationAdapter.adptFromCanalizationRequestToCanalization(counterpartyPatchCommand.getCanalization()));
        }
        if(counterpartyPatchCommand.getPolicyNumber() != null){
            counterparty.setPolicyNumber(counterpartyPatchCommand.getPolicyNumber());
        }
        if(counterpartyPatchCommand.getPolicyBeginningValidity() != null){
            String beginningValidity = counterpartyPatchCommand.getPolicyBeginningValidity();
            if(!beginningValidity.contains("T"))
                beginningValidity+="T00:00:00Z";
            Instant beginningValidityDate = DateUtil.convertIS08601StringToUTCInstant(beginningValidity);
            counterparty.setPolicyBeginningValidity(beginningValidityDate);
        }
        if(counterpartyPatchCommand.getPolicyEndValidity() != null){
            String endValidity = counterpartyPatchCommand.getPolicyEndValidity();
            if(!endValidity.contains("T"))
                endValidity+="T00:00:00Z";
            Instant endValidityDate = DateUtil.convertIS08601StringToUTCInstant(endValidity);
            counterparty.setPolicyEndValidity(endValidityDate);
        }
        if(counterpartyPatchCommand.getManagementType() != null){
            counterparty.setManagementType(counterpartyPatchCommand.getManagementType());
        }

        if(counterpartyPatchCommand.getAskedForDamages() != null){
            counterparty.setAskedForDamages(counterpartyPatchCommand.getAskedForDamages());
        }

        if(counterpartyPatchCommand.getLegalOrConsultant() != null){
            counterparty.setLegalOrConsultant(counterpartyPatchCommand.getLegalOrConsultant());
        }
        if(counterpartyPatchCommand.getDateRequestDamages() != null){
            counterparty.setInstantRequestDamages(DateUtil.convertIS08601StringToUTCInstant(counterpartyPatchCommand.getDateRequestDamages()));
        }
        if(counterpartyPatchCommand.getExpirationDate() != null){
            counterparty.setExpirationInstant(DateUtil.convertIS08601StringToUTCInstant(counterpartyPatchCommand.getExpirationDate()));
        }
        if(counterpartyPatchCommand.getLegal() != null){
            counterparty.setLegal(counterpartyPatchCommand.getLegal());
        }
        if(counterpartyPatchCommand.getEmailLegal() != null){
            counterparty.setEmailLegal(counterpartyPatchCommand.getEmailLegal());
        }

        if( counterpartyPatchCommand.getAld() != null){
            counterparty.setAld(counterpartyPatchCommand.getAld());
        }
        counterparty.setUpdateAt(DateUtil.getNowInstant());

        if(counterparty.getIsReadMsa() != null && counterparty.getIsReadMsa()){
            counterparty.setIsReadMsa(false);
        }

        if(counterpartyPatchCommand.getReplacementCar() != null) {
            counterparty.setReplacementCar(counterpartyPatchCommand.getReplacementCar());
        }

        if(counterpartyPatchCommand.getReplacementPlate() != null){
            counterparty.setReplacementPlate(counterpartyPatchCommand.getReplacementPlate());
        }

        if(counterpartyPatchCommand.getCompleteDocumentation() != null){
            counterparty.setIsCompleteDocumentation(counterpartyPatchCommand.getCompleteDocumentation());
        }

        counterpartyRepository.save(counterparty);

        CounterpartyEntity counterpartyOld = CounterpartyAdapter.adptCounterpartyNewToCounterpartyOld(counterparty);
        dwhClaimsService.sendMessage(counterpartyOld, EventTypeEnum.UPDATED);

        if(counterparty.getClaims()!=null){
            ClaimsNewEntity claimsNewEntity = counterparty.getClaims();
            Instant updatedAt = DateUtil.getNowInstant();
            claimsEntity.setUpdateAt(updatedAt);
            claimsNewEntity.setUpdateAt(updatedAt);
            if(claimsNewEntity.getCounterparts() != null) {
                CounterpartyNewEntity toFind =null;
                for(CounterpartyNewEntity counterpartyCurrent : claimsNewEntity.getCounterparts()){
                    if(counterpartyCurrent.getCounterpartyId().equalsIgnoreCase(counterparty.getCounterpartyId())){
                        toFind = counterpartyCurrent;
                    }
                }
                claimsNewEntity.getCounterparts().remove(toFind);
                claimsNewEntity.getCounterparts().add(counterparty);
            }
            claimsRepository.save(claimsNewEntity);
            if(dwhCall){
                dwhClaimsService.sendMessage(claimsEntity, com.doing.nemo.dwh.client.payload.request.EventTypeEnum.UPDATED);
            }
        }

    }
}
