package com.doing.nemo.claims.commandHandler;

import com.doing.nemo.claims.command.ClaimsInsertAttachmentsCommand;
import com.doing.nemo.claims.common.commandBus.Command;
import com.doing.nemo.claims.common.commandBus.CommandHandler;
import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.jsonb.forms.jsonbForms.Attachment;
import com.doing.nemo.claims.repository.ClaimsRepository;
import com.doing.nemo.claims.service.DwhClaimsService;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.dwh.client.payload.request.EventTypeEnum;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class ClaimsInsertAttachmentsCommandHandler implements CommandHandler {

    private static Logger LOGGER = LoggerFactory.getLogger(ClaimsInsertAttachmentsCommandHandler.class);
    @Autowired
    private ClaimsRepository claimsRepository;

    @Autowired
    private DwhClaimsService dwhClaimsService;

    @Value("${dwh.call}")
    private Boolean dwhCall;

    @Override
    public void handle(Command command) {

        ClaimsInsertAttachmentsCommand claimsInsertAttachmentsCommand = (ClaimsInsertAttachmentsCommand) command;

        Optional<ClaimsEntity> claimsEntityOptional = claimsRepository.findById(claimsInsertAttachmentsCommand.getId());
        if (!claimsEntityOptional.isPresent()) {
            LOGGER.debug("Claims with UUID " + claimsInsertAttachmentsCommand.getId() + " not found ");
            throw new NotFoundException("Claims with UUID " + claimsInsertAttachmentsCommand.getId() + " not found ", MessageCode.CLAIMS_1010);
        }
        ClaimsEntity claimsEntity = claimsEntityOptional.get();
        List<Attachment> fileManagerList = claimsEntity.getForms().getAttachment();
        fileManagerList.addAll(claimsInsertAttachmentsCommand.getFileManagerList());
        /*
        for (Attachment fm : claimsInsertAttachmentsCommand.getFileManagerList()) {
            fileManagerList.add(fm);
        }*/

        claimsEntity.getForms().setAttachment(fileManagerList);
        claimsEntity.setUpdateAt(DateUtil.getNowInstant());
        claimsRepository.save(claimsEntity);

        if(dwhCall){
            dwhClaimsService.sendMessage(claimsEntity, EventTypeEnum.UPDATED);
        }
    }
}
