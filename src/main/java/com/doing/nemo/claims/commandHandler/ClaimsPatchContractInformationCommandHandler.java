package com.doing.nemo.claims.commandHandler;

import com.doing.nemo.claims.adapter.AntiTheftServiceAdapter;
import com.doing.nemo.claims.adapter.DamagedAdapter;
import com.doing.nemo.claims.command.ClaimsPatchContractInformationCommand;
import com.doing.nemo.claims.common.commandBus.Command;
import com.doing.nemo.claims.common.commandBus.CommandHandler;
import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.ClaimsNewEntity;
import com.doing.nemo.claims.entity.enumerated.EventTypeEnum;
import com.doing.nemo.claims.entity.jsonb.Historical;
import com.doing.nemo.claims.entity.jsonb.damaged.Damaged;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.Contract;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.InsuranceCompany;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbAntiTheftService.AntiTheftRequest;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbAntiTheftService.AntiTheftService;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbAntiTheftService.Registry;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbInsuranceCompany.LegalCost;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbInsuranceCompany.Pai;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbInsuranceCompany.Tpl;
import com.doing.nemo.claims.entity.settings.AntiTheftServiceEntity;
import com.doing.nemo.claims.entity.settings.InsurancePolicyEntity;
import com.doing.nemo.claims.repository.AntiTheftServiceRepository;
import com.doing.nemo.claims.repository.ClaimsNewRepository;
import com.doing.nemo.claims.repository.InsurancePolicyRepository;
import com.doing.nemo.claims.service.*;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.Period;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
public class ClaimsPatchContractInformationCommandHandler implements CommandHandler {

    private static Logger LOGGER = LoggerFactory.getLogger(ClaimsPatchContractInformationCommandHandler.class);

    @Autowired
    private ClaimsNewRepository claimsNewRepository;

    @Autowired
    private ConverterClaimsService converterClaimsService;

    @Autowired
    private ClaimsService claimsService;

    @Autowired
    private AntiTheftServiceRepository antiTheftServiceRepository;

    @Autowired
    private InsurancePolicyRepository insurancePolicyRepository;

    @Autowired
    private DwhClaimsService dwhClaimsService;

    @Value("${dwh.call}")
    private Boolean dwhCall;

    @Autowired
    private CacheService cacheService;

    @Autowired
    private AntiTheftRequestService antiTheftRequestService;

    private static final String CACHENAME = "claims.";
    private static final String STATSkEY = "claimsStats::";
    private static final String STATSEVIDENCE = "evidenceStats::";
    private static final String STATSTHEFT = "theftStats::";
    private static final String STATSSTATUS = "statusStats::";
    private static final String COUNTERPARTYKEY = "counterpartyStats::";
    private static final String STATSkEY2 = "claimsV2Stats::";
    private static final String STATSEVIDENCECONTINUATION = "evidenceContinuationStats::";

    @Override
    public void handle(Command command) {
        //REFACTOR

        ClaimsPatchContractInformationCommand claimsPatchCommand = (ClaimsPatchContractInformationCommand) command;
        Optional<ClaimsNewEntity> claimsNewEntityOptional = claimsNewRepository.findById(claimsPatchCommand.getId());


        if (!claimsNewEntityOptional.isPresent()) {
            LOGGER.debug("Claims with UUID " + claimsPatchCommand.getId() + " not found ");
            throw new NotFoundException("Claims with UUID " + claimsPatchCommand.getId() + " not found ", MessageCode.CLAIMS_1010);
        }



        ClaimsNewEntity claimsNewEntity = claimsNewEntityOptional.get();


        ClaimsEntity claims = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntity);

        Damaged damagedPatch = DamagedAdapter.adptDamagedRequestToDamaged(claimsPatchCommand.getDamaged());
        Damaged damagedOld = null;

        AntiTheftServiceEntity antiTheftServiceEntity = null;
        AntiTheftService antiTheftService = null;

        if(claims.getDamaged() != null) {
            damagedOld = claims.getDamaged();
        }

        String description = "";

        if(damagedOld != null){
            AntiTheftService antiTheftServiceOld = damagedOld.getAntiTheftService();
            /**---------------- CONTROLLO INFORMAZIONI CONTRATTUALI ----------------*/
            if(damagedOld.getContract() != null){

                Contract contractOld = claims.getDamaged().getContract();

                Contract contractPatch = damagedPatch.getContract();
                if(contractPatch != null) {
                    if (contractPatch.getStartDate() != null) {

                        if (contractOld.getStartDate() != null && !contractOld.getStartDate().equals(contractPatch.getStartDate())) {

                            description = claimsService.createChangeInfoDescriptinForLog(description, "Inizio Contratto", DateUtil.getDateStringWithSeparationCharacters(contractOld.getStartDate()), DateUtil.getDateStringWithSeparationCharacters(contractPatch.getStartDate()));
                            contractOld.setStartDate(contractPatch.getStartDate());

                        } else {

                            if (contractOld.getStartDate() == null && contractPatch.getStartDate() != null) {

                                contractOld.setStartDate(contractPatch.getStartDate());
                                description = claimsService.createChangeInfoDescriptinForLog(description, "Inizio Contratto", "Nessuno", DateUtil.getDateStringWithSeparationCharacters(contractPatch.getStartDate()));
                            }
                        }
                    }

                    if (contractPatch.getEndDate() != null) {

                        if (contractOld.getEndDate() != null && !contractOld.getEndDate().equals(contractPatch.getEndDate())) {

                            description = claimsService.createChangeInfoDescriptinForLog(description, "Fine Contratto", DateUtil.getDateStringWithSeparationCharacters(contractOld.getEndDate()), DateUtil.getDateStringWithSeparationCharacters(contractPatch.getEndDate()));
                            contractOld.setEndDate(contractPatch.getEndDate());

                        } else {

                            if (contractOld.getEndDate() == null && contractPatch.getEndDate() != null) {

                                contractOld.setEndDate(contractPatch.getEndDate());
                                description = claimsService.createChangeInfoDescriptinForLog(description, "Fine Contratto", "Nessuno", DateUtil.getDateStringWithSeparationCharacters(contractPatch.getEndDate()));
                            }
                        }
                    }

                    if (contractOld.getStartDate() != null && contractOld.getEndDate() != null) {

                        if (contractOld.getEndDate().compareTo(contractOld.getStartDate()) < 0) {
                            LOGGER.debug(MessageCode.CLAIMS_1104.value());
                            throw new BadRequestException(MessageCode.CLAIMS_1104);
                        }

                        int months = Period.between(DateUtil.convertUTCDateToLocalDate(contractOld.getStartDate()), DateUtil.convertUTCDateToLocalDate(contractOld.getEndDate())).getMonths();
                        int years = Period.between(DateUtil.convertUTCDateToLocalDate(contractOld.getStartDate()), DateUtil.convertUTCDateToLocalDate(contractOld.getEndDate())).getYears() * 12;
                        Integer newDuration = months + years;

                        if (contractOld.getDuration() != null && !contractOld.getDuration().equals(newDuration)) {

                            description = claimsService.createChangeInfoDescriptinForLog(description, "Durata Contratto", contractOld.getDuration().toString(), newDuration.toString());
                        } else if (contractOld.getDuration() == null && !contractOld.getDuration().equals(newDuration)) {
                            description = claimsService.createChangeInfoDescriptinForLog(description, "Durata Contratto", "Nessuno", newDuration.toString());
                        }

                        contractOld.setDuration(months + years);
                    }
                    claims.getDamaged().setContract(contractOld);
                }
            }
            /**---------------------------------------------------------------------*/

            /**---------------- CONTROLLO INFORMAZIONI RELATIVE ALL'APPARATO TELEMATICO E AL VOUCHER ID ----------------*/
            Registry registry = null;
            String antitheftDescription = "";
            if(damagedOld.getAntiTheftService() != null && damagedOld.getAntiTheftService().getRegistryList() != null && !damagedOld.getAntiTheftService().getRegistryList().isEmpty()) {
                registry = damagedOld.getAntiTheftService().getRegistryList().get(0);
            } else {
                registry = new Registry();
            }

            Registry registryPatch = null;
            if (damagedPatch.getAntiTheftService() != null && damagedPatch.getAntiTheftService().getRegistryList() != null && !damagedPatch.getAntiTheftService().getRegistryList().isEmpty())
                registryPatch = damagedPatch.getAntiTheftService().getRegistryList().get(0);


            if(registryPatch != null){

                if (registryPatch.getCodPack() != null) {

                    if (registry.getCodPack() != null && !registry.getCodPack().equalsIgnoreCase(registryPatch.getCodPack())) {

                        antitheftDescription = claimsService.createChangeInfoDescriptinForLog(antitheftDescription, "Apparato Telematico", registry.getCodPack(), registryPatch.getCodPack());
                        registry.setCodPack(registryPatch.getCodPack());
                        //damaged.getAntiTheftService().getRegistryList().get(0).setCodPack(registryPatch.getCodPack());

                    } else if (registry.getCodPack() == null && registryPatch.getCodPack() != null) {
                        registry.setCodPack(registryPatch.getCodPack());
                        antitheftDescription = claimsService.createChangeInfoDescriptinForLog(antitheftDescription, "Apparato Telematico", "Nessuno", registryPatch.getCodPack());
                    }
                }

                if (registryPatch.getVoucherId() != null) {

                    if (registry.getVoucherId() != null && !registry.getVoucherId().equals(registryPatch.getVoucherId())) {

                        antitheftDescription = claimsService.createChangeInfoDescriptinForLog(antitheftDescription, "Voucher ID", registry.getVoucherId().toString(), registryPatch.getVoucherId().toString());
                        registry.setVoucherId(registryPatch.getVoucherId());

                    } else if (registry.getVoucherId() == null && registryPatch.getVoucherId() != null) {
                        registry.setVoucherId(registryPatch.getVoucherId());
                        antitheftDescription = claimsService.createChangeInfoDescriptinForLog(antitheftDescription, "Voucher ID", "Nessuno", registryPatch.getVoucherId().toString());
                    }
                }

                if(!antitheftDescription.isEmpty()){
                    if(registry.getCodPack() != null){
                        String codAntiTheftService = AntiTheftServiceAdapter.adptProviderToAntiTheftServiceEntity(registry.getCodPack());
                        antiTheftServiceEntity = antiTheftServiceRepository.findAntiTheftByCodAntiTheftService(codAntiTheftService);
                        registry.setProvider(codAntiTheftService);
                        antiTheftService = AntiTheftServiceAdapter.adptAntiTheftServiceEntityToAntiTheftService(antiTheftServiceEntity);
                        if(antiTheftServiceOld != null) {
                            if (antiTheftService.getAntiTheftList() == null) {
                                antiTheftService.setAntiTheftList(antiTheftServiceOld.getAntiTheftList());
                            } else {
                                List<AntiTheftRequest> antiTheftRequests = antiTheftService.getAntiTheftList();
                                antiTheftRequests.addAll(antiTheftServiceOld.getAntiTheftList());
                                antiTheftService.setAntiTheftList(antiTheftRequests);
                            }
                        }
                    }
                }
            }

            /**---------------------------------------------------------------------------------------------------------*/


            /**---------------- CONTROLLO IFORMAZIONI RELATIVE ALLE POLIZZE ----------------*/

            InsuranceCompany insuranceCompany = damagedOld.getInsuranceCompany();
            InsuranceCompany insuranceCompanyPatch = damagedPatch.getInsuranceCompany();

            if(insuranceCompanyPatch != null){

                if(insuranceCompany == null)
                    insuranceCompany = new InsuranceCompany();

                /**----------------- CONTROLLO TPL -----------------*/
                //Controllo info TPL
                Tpl tplPatch = insuranceCompanyPatch.getTpl();
                Tpl tpl = insuranceCompany.getTpl();
                //Se nella richiesta è presente la TPL
                if(tplPatch != null && !(tpl.getInsurancePolicyId() == null && tplPatch.getInsurancePolicyId() == null)){

                    //Se ho nel body della patch l'uuid della polizza
                    Optional<InsurancePolicyEntity> optInsurancePolicyPatch = null;
                    InsurancePolicyEntity insurancePolicyPatch = null;

                    if (tplPatch.getInsurancePolicyId() != null)
                    {
                        optInsurancePolicyPatch = insurancePolicyRepository.findById(tplPatch.getInsurancePolicyId());

                        if(!optInsurancePolicyPatch.isPresent())
                        {
                            throw new BadRequestException(MessageCode.CLAIMS_1010);
                        }

                        insurancePolicyPatch = optInsurancePolicyPatch.get();

                        if(tpl == null || tpl.getInsurancePolicyId() == null || !tpl.getInsurancePolicyId().equals(tplPatch.getInsurancePolicyId())){

                            if(tpl == null) {
                                tpl = new Tpl();
                                description += "Inserimento di una nuova polizza RCA, con Policy Number: " + tplPatch.getPolicyNumber() +". <br>";
                            }

                            if(insurancePolicyPatch.getInsuranceCompanyEntity() != null) {
                                tpl.setCompany(insurancePolicyPatch.getInsuranceCompanyEntity().getName());
                                tpl.setInsuranceCompanyId(insurancePolicyPatch.getInsuranceCompanyEntity().getId());
                            }
                            tpl.setTariffId(insurancePolicyPatch.getBookRegisterCode());
                            tpl.setInsurancePolicyId(tplPatch.getInsurancePolicyId());
                            tpl.setStartDate(DateUtil.convertUTCInstantToIS08601String(insurancePolicyPatch.getBeginningValidity()));
                            tpl.setEndDate(DateUtil.convertUTCInstantToIS08601String(insurancePolicyPatch.getEndValidity()));

                            description = claimsService.createChangeInfoDescriptinForLog(description, "RCA->Policy Number", tpl.getPolicyNumber(), insurancePolicyPatch.getNumberPolicy());

                            tpl.setPolicyNumber(insurancePolicyPatch.getNumberPolicy());

                            claims.getDamaged().getInsuranceCompany().setTpl(tpl);

                        }
                    }
                }
                /**-------------------------------------------------*/

                /**----------------- CONTROLLO PAI -----------------*/
                //Controllo info TPL
                Pai paiPatch = insuranceCompanyPatch.getPai();
                Pai pai = insuranceCompany.getPai();
                //Se nella richiesta è presente la PAI
                if(paiPatch != null && !(pai.getInsurancePolicyId() == null && paiPatch.getInsurancePolicyId() == null)){

                    //Se ho nel body della patch l'uuid della polizza
                    Optional<InsurancePolicyEntity> optInsurancePolicyPatch = null;
                    InsurancePolicyEntity insurancePolicyPatch = null;

                    if(paiPatch.getInsurancePolicyId() != null)
                    {
                        optInsurancePolicyPatch = insurancePolicyRepository.findById(paiPatch.getInsurancePolicyId());

                        if(!optInsurancePolicyPatch.isPresent())
                            throw new BadRequestException(MessageCode.CLAIMS_1010);

                        insurancePolicyPatch = optInsurancePolicyPatch.get();

                        if(pai == null || pai.getInsurancePolicyId() == null || !pai.getInsurancePolicyId().equals(paiPatch.getInsurancePolicyId())) {

                            if (pai == null) {
                                pai = new Pai();
                                description += "Inserimento di una nuova polizza PAI, con Policy Number: " + insurancePolicyPatch.getNumberPolicy() + ". <br>";
                            }

                            if (insurancePolicyPatch.getInsuranceCompanyEntity() != null) {
                                pai.setCompany(insurancePolicyPatch.getInsuranceCompanyEntity().getName());
                                pai.setInsuranceCompanyId(insurancePolicyPatch.getInsuranceCompanyEntity().getId());
                            }
                            pai.setTariffId(insurancePolicyPatch.getBookRegisterCode());
                            pai.setInsurancePolicyId(paiPatch.getInsurancePolicyId());
                            pai.setStartDate(DateUtil.convertUTCInstantToIS08601String(insurancePolicyPatch.getBeginningValidity()));
                            pai.setEndDate(DateUtil.convertUTCInstantToIS08601String(insurancePolicyPatch.getEndValidity()));

                            description = claimsService.createChangeInfoDescriptinForLog(description, "PAI->Policy Number", pai.getPolicyNumber(), insurancePolicyPatch.getNumberPolicy());

                            pai.setPolicyNumber(insurancePolicyPatch.getNumberPolicy());

                            claims.getDamaged().getInsuranceCompany().setPai(pai);
                        }

                    }
                }
                /**-------------------------------------------------*/

                /**----------------- CONTROLLO LEGAL -----------------*/
                //Controllo info TPL
                LegalCost legalCostPatch = insuranceCompanyPatch.getLegalCost();
                LegalCost legalCost = insuranceCompany.getLegalCost();
                //Se nella richiesta è presente la LEGAL
                if(legalCostPatch != null  && !(legalCost.getInsurancePolicyId() == null && legalCostPatch.getInsurancePolicyId() == null)){

                    //Se ho nel body della patch l'uuid della polizza
                    Optional<InsurancePolicyEntity> optInsurancePolicyPatch = null;
                    InsurancePolicyEntity insurancePolicyPatch = null;

                    if(legalCostPatch.getInsurancePolicyId() != null) {
                        optInsurancePolicyPatch = insurancePolicyRepository.findById(legalCostPatch.getInsurancePolicyId());


                        //Per le pratiche vecchie, dove non era previsto il campo insurance_policy_id
                        if(!optInsurancePolicyPatch.isPresent())
                            throw new BadRequestException("Policy not found",MessageCode.CLAIMS_1010);

                        insurancePolicyPatch = optInsurancePolicyPatch.get();

                        if(legalCost == null || legalCost.getInsurancePolicyId() == null || !legalCost.getInsurancePolicyId().equals(legalCostPatch.getInsurancePolicyId())){

                            if(legalCost == null) {
                                legalCost = new LegalCost();
                                description += "Inserimento di una nuova polizza LEGAL, con Policy Number: " + insurancePolicyPatch.getNumberPolicy() +". <br>";
                            }

                            if(insurancePolicyPatch.getInsuranceCompanyEntity() != null) {
                                legalCost.setCompany(insurancePolicyPatch.getInsuranceCompanyEntity().getName());
                                legalCost.setInsuranceCompanyId(insurancePolicyPatch.getInsuranceCompanyEntity().getId());
                            }
                            legalCost.setTariffId(insurancePolicyPatch.getBookRegisterCode());
                            legalCost.setInsurancePolicyId(legalCostPatch.getInsurancePolicyId());
                            legalCost.setStartDate(DateUtil.convertUTCInstantToIS08601String(insurancePolicyPatch.getBeginningValidity()));
                            legalCost.setEndDate(DateUtil.convertUTCInstantToIS08601String(insurancePolicyPatch.getEndValidity()));

                            description = claimsService.createChangeInfoDescriptinForLog(description, "LEGAL COST->Policy Number", legalCost.getPolicyNumber(), insurancePolicyPatch.getNumberPolicy());

                            legalCost.setPolicyNumber(insurancePolicyPatch.getNumberPolicy());

                            claims.getDamaged().getInsuranceCompany().setLegalCost(legalCost);
                        }
                    }
                }
                /**-------------------------------------------------*/

            }
            /**-----------------------------------------------------------------------------*/

            //Inserisco il log nel caso in cui siano state modificate delle proprietà
            if(!description.isEmpty() || !antitheftDescription.isEmpty()){

                Historical historical = new Historical();

                if( claims.getHistorical() == null)
                    claims.setHistorical(new LinkedList<>());

                historical.setUserId(claimsPatchCommand.getUserId());
                historical.setUserName(claimsPatchCommand.getUserName());
                historical.setEventType(EventTypeEnum.EDIT_PRACTICE_DATA);
                historical.setStatusEntityOld(claims.getStatus());
                historical.setStatusEntityNew(claims.getStatus());
                historical.setOldType(claims.getComplaint().getDataAccident().getTypeAccident());
                historical.setNewType(claims.getComplaint().getDataAccident().getTypeAccident());
                historical.setOldFlow(claims.getType().getValue());
                historical.setNewFlow(claims.getType().getValue());
                historical.setUpdateAt(DateUtil.getNowDate());

                if(dwhCall){
                    dwhClaimsService.sendMessage(claims, com.doing.nemo.dwh.client.payload.request.EventTypeEnum.UPDATED);
                }

                //Devo necessariamente fare la chiamata all'antitheft qui, perchè altrimenti l'istorical viene perso
                if (antiTheftService != null) {

                    antiTheftService.setRegistryList(new LinkedList<>());
                    List<Registry> registries = antiTheftService.getRegistryList();
                    registries.add(registry);
                    antiTheftService.setRegistryList(registries);
                    claims.getDamaged().setAntiTheftService(antiTheftService);

                    description += antitheftDescription;

                    historical.setComunicationDescription(description);
                    List<Historical> historicalList = claims.getHistorical();
                    historicalList.add(historical);
                    claims.setHistorical(historicalList);

                    //converto il vecchio nel nuovo e salvo
                    claimsNewEntity = converterClaimsService.convertOldEntityToNewClaimsEntity(claims);
                    claimsNewRepository.save(claimsNewEntity);

                    // creazione della riga di storico di octo/texa
                    String idAntiTheftRequest = UUID.randomUUID().toString();

                    antiTheftRequestService.insertAntiTheftRequest(idAntiTheftRequest, claimsNewEntity, antiTheftService.getProviderType(), "1");
                    claimsService.callToOctoAsync(claimsNewEntity, claimsNewEntity.getStatus(), new AntiTheftRequest(), claimsPatchCommand.getUserId(), claimsPatchCommand.getUserName(),idAntiTheftRequest);
                }else{

                    historical.setComunicationDescription(description);
                    List<Historical> historicalList = claims.getHistorical();
                    historicalList.add(historical);
                    claims.setHistorical(historicalList);

                    //converto il vecchio nel nuovo e salvo
                    claimsNewEntity = converterClaimsService.convertOldEntityToNewClaimsEntity(claims);
                    claimsNewRepository.save(claimsNewEntity);

                }
            }
        }
        cacheService.deleteClaimsStats(CACHENAME, STATSkEY);
        cacheService.deleteClaimsStats(CACHENAME, STATSEVIDENCE);
        cacheService.deleteClaimsStats(CACHENAME, STATSTHEFT);
        cacheService.deleteClaimsStats(CACHENAME, STATSSTATUS);

        cacheService.deleteClaimsStats(CACHENAME, STATSEVIDENCECONTINUATION);
        cacheService.deleteClaimsStats(CACHENAME, STATSkEY2);
    }
}
