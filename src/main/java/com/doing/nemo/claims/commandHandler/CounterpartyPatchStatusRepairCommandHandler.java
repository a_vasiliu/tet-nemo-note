package com.doing.nemo.claims.commandHandler;

import com.doing.nemo.claims.adapter.CounterpartyAdapter;
import com.doing.nemo.claims.command.CounterpartyPatchStatusRepairCommand;
import com.doing.nemo.claims.common.commandBus.Command;
import com.doing.nemo.claims.common.commandBus.CommandHandler;
import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.authority.repair.AuthorityRepairStatusRequestV1;
import com.doing.nemo.claims.entity.ClaimsNewEntity;
import com.doing.nemo.claims.entity.CounterpartyEntity;
import com.doing.nemo.claims.entity.CounterpartyNewEntity;
import com.doing.nemo.claims.entity.enumerated.AuthorityEnum.AuthorityEventTypeEnum;
import com.doing.nemo.claims.entity.enumerated.AuthorityEnum.AuthorityRepairStatusEnum;
import com.doing.nemo.claims.entity.enumerated.RepairEnum.RepairStatusEnum;
import com.doing.nemo.claims.entity.enumerated.WorkingStatusEnum;
import com.doing.nemo.claims.entity.jsonb.Authority;
import com.doing.nemo.claims.entity.jsonb.repair.HistoricalCounterparty;
import com.doing.nemo.claims.repository.ClaimsNewRepository;
import com.doing.nemo.claims.repository.ClaimsRepository;
import com.doing.nemo.claims.repository.CounterpartyNewRepository;
import com.doing.nemo.claims.repository.CounterpartyRepository;
import com.doing.nemo.claims.service.*;
import com.doing.nemo.claims.service.DwhClaimsService;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.AbstractException;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.dwh.client.payload.request.EventTypeEnum;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Optional;

@Component
public class CounterpartyPatchStatusRepairCommandHandler implements CommandHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(ClaimsPatchStatusCommandHandler.class);

    @Autowired
    private ConverterClaimsService converterClaimsService;

    @Autowired
    private CounterpartyNewRepository counterpartyNewRepository;

    @Autowired
    private ClaimsNewRepository claimsRepository;

    @Autowired
    private MessagingService messagingService;

    @Autowired
    private DwhClaimsService dwhClaimsService;

    @Autowired
    private AuthorityService authorityService;

    @Value("${dwh.call}")
    private Boolean dwhCall;

    @Autowired
    private CacheService cacheService;

    @Autowired
    private CounterpartyService counterpartyService;

    private static final String CACHENAME = "claims.";
    private static final String STATSkEY = "claimsStats::";
    private static final String STATSEVIDENCE = "evidenceStats::";
    private static final String STATSTHEFT = "theftStats::";
    private static final String STATSSTATUS = "statusStats::";
    private static final String COUNTERPARTYKEY = "counterpartyStats::";


    @Override
    public void handle(Command command) throws IOException {
        CounterpartyPatchStatusRepairCommand counterpartyPatchStatusRepairCommand = (CounterpartyPatchStatusRepairCommand) command;

        Optional<CounterpartyNewEntity> counterpartyNewEntityOptional = counterpartyNewRepository.findById(counterpartyPatchStatusRepairCommand.getCounterpartyId());

        if (!counterpartyNewEntityOptional.isPresent()) {
            throw new NotFoundException("Counterparty with UUID " + counterpartyPatchStatusRepairCommand.getCounterpartyId() + " not found", MessageCode.CLAIMS_1010);
        }

        CounterpartyNewEntity counterparty = counterpartyNewEntityOptional.get();
        counterpartyService.isWorkable(counterparty);
        String description = "";
        String smallDescription = "In Attesa di Preventivo";
        try{
            if(counterpartyPatchStatusRepairCommand.getCounterpartyPatchStatusRequestV1() != null && counterpartyPatchStatusRepairCommand.getCounterpartyPatchStatusRequestV1().getTemplateList() != null) {
                description = messagingService.sendMailAndCreateLogs(counterpartyPatchStatusRepairCommand.getCounterpartyPatchStatusRequestV1().getTemplateList(), null, counterparty.getCreatedAt() );
            }
        } catch (Exception e){
            LOGGER.error(e.getMessage());
        }

        HistoricalCounterparty historicalCounterparty = new HistoricalCounterparty(counterparty.getRepairStatus(), counterpartyPatchStatusRepairCommand.getNextStatusRepair(), smallDescription, DateUtil.getNowDate(), counterpartyPatchStatusRepairCommand.getUserId(),counterpartyPatchStatusRepairCommand.getUserName()+ " "+counterpartyPatchStatusRepairCommand.getUserLastName(), description, com.doing.nemo.claims.entity.enumerated.EventTypeEnum.STATUS_VARIATION_REPAIR);
        if(counterparty.getHistoricals() == null){
            counterparty.setHistoricals(new ArrayList<>());
        }
        counterparty.addHistorical(historicalCounterparty);
        counterparty.setRepairStatus(counterpartyPatchStatusRepairCommand.getNextStatusRepair());

        if(counterparty.getAuthorities() != null && !counterparty.getAuthorities().isEmpty()){
            AuthorityRepairStatusRequestV1 authorityRepairStatusRequestV1 = new AuthorityRepairStatusRequestV1();
            if(counterpartyPatchStatusRepairCommand.getNextStatusRepair().equals(RepairStatusEnum.UNDER_LIQUIDATION)){
                authorityRepairStatusRequestV1.setRepairStatus(AuthorityRepairStatusEnum.LIQUIDATION);
                for(Authority authority : counterparty.getAuthorities()){
                    authorityService.setAuthorityRepairStatus(authority.getAuthorityDossierId(), authority.getAuthorityWorkingId(), authorityRepairStatusRequestV1, counterpartyPatchStatusRepairCommand.getUserId(), counterpartyPatchStatusRepairCommand.getUserName(), counterpartyPatchStatusRepairCommand.getUserLastName(), counterpartyPatchStatusRepairCommand.getProfiles());
                    authority.setEventType(AuthorityEventTypeEnum.END_WORKING);
                    authority = authorityService.addHistoricalRepairClosedStatus(authority, WorkingStatusEnum.CLOSED_WITH_LIQUIDATION);
                    authority.setWorkingStatus(WorkingStatusEnum.CLOSED_WITH_LIQUIDATION);
                }
            } else if(counterpartyPatchStatusRepairCommand.getNextStatusRepair().equals(RepairStatusEnum.UNDER_REPAIR)){
                authorityRepairStatusRequestV1.setRepairStatus(AuthorityRepairStatusEnum.REPARATION);
                for(Authority authority : counterparty.getAuthorities()){
                    authorityService.setAuthorityRepairStatus(authority.getAuthorityDossierId(), authority.getAuthorityWorkingId(), authorityRepairStatusRequestV1, counterpartyPatchStatusRepairCommand.getUserId(), counterpartyPatchStatusRepairCommand.getUserName(), counterpartyPatchStatusRepairCommand.getUserLastName(), counterpartyPatchStatusRepairCommand.getProfiles());
                }
            } else if(counterpartyPatchStatusRepairCommand.getNextStatusRepair().equals(RepairStatusEnum.CLOSED_DENIAL)){
                authorityRepairStatusRequestV1.setRepairStatus(AuthorityRepairStatusEnum.REJECTION);
                for(Authority authority : counterparty.getAuthorities()){
                    authorityService.setAuthorityRepairStatus(authority.getAuthorityDossierId(), authority.getAuthorityWorkingId(), authorityRepairStatusRequestV1, counterpartyPatchStatusRepairCommand.getUserId(), counterpartyPatchStatusRepairCommand.getUserName(), counterpartyPatchStatusRepairCommand.getUserLastName(), counterpartyPatchStatusRepairCommand.getProfiles());
                    authority.setEventType(AuthorityEventTypeEnum.REJECT);
                    authority = authorityService.addHistoricalRepairClosedStatus(authority, WorkingStatusEnum.REJECTED);
                    authority.setWorkingStatus(WorkingStatusEnum.REJECTED);
                }
            }
        }

        if(counterparty.getIsReadMsa() != null && counterparty.getIsReadMsa()){
            counterparty.setIsReadMsa(false);
        }
        counterpartyNewRepository.save(counterparty);

        CounterpartyEntity counterpartyOld = CounterpartyAdapter.adptCounterpartyNewToCounterpartyOld(counterparty);
        dwhClaimsService.sendMessage(counterpartyOld, EventTypeEnum.UPDATED);

        if(counterparty.getClaims()!=null){
            ClaimsNewEntity claimsNewEntity = counterparty.getClaims();
            claimsNewEntity.setUpdateAt(DateUtil.getNowInstant());
            claimsRepository.save(claimsNewEntity);
            if(dwhCall){
                dwhClaimsService.sendMessage(converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntity), com.doing.nemo.dwh.client.payload.request.EventTypeEnum.UPDATED);
            }
        }


        cacheService.deleteClaimsStats(CACHENAME, COUNTERPARTYKEY);
    }
}
