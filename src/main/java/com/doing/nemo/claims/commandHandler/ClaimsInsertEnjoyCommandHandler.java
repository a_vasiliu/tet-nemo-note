package com.doing.nemo.claims.commandHandler;

import com.doing.nemo.claims.adapter.*;
import com.doing.nemo.claims.command.ClaimsInsertEnjoyCommand;
import com.doing.nemo.claims.common.commandBus.Command;
import com.doing.nemo.claims.common.commandBus.CommandHandler;
import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.ClaimsInsertEnjoyRequest.CounterpartyEnjoyRequest;
import com.doing.nemo.claims.controller.payload.request.ClaimsInsertExternalRequest;
import com.doing.nemo.claims.controller.payload.request.EmailTemplateMessagingRequestV1;
import com.doing.nemo.claims.controller.payload.request.InsuranceCompanyCounterpartyRequest;
import com.doing.nemo.claims.controller.payload.request.InsuranceCompanyRequestV1;
import com.doing.nemo.claims.controller.payload.response.ContractInfoResponseV1;
import com.doing.nemo.claims.controller.payload.response.DogeResponseV1;
import com.doing.nemo.claims.controller.payload.response.authority.claims.VehicleStatusAuthorityResponseV1;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.ClaimsNewEntity;
import com.doing.nemo.claims.entity.CounterpartyEntity;
import com.doing.nemo.claims.entity.enumerated.AuthorityEnum.AuthorityVehicleStatusEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsFlowEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.doing.nemo.claims.entity.enumerated.EventTypeEnum;
import com.doing.nemo.claims.entity.jsonb.Historical;
import com.doing.nemo.claims.entity.jsonb.complaint.jsonbComplaint.FromCompany;
import com.doing.nemo.claims.entity.jsonb.damaged.Damaged;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.Customer;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbAntiTheftService.AntiTheftRequest;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbAntiTheftService.AntiTheftService;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbAntiTheftService.Registry;
import com.doing.nemo.claims.entity.settings.*;
import com.doing.nemo.claims.exception.ForbiddenException;
import com.doing.nemo.claims.repository.*;
import com.doing.nemo.claims.service.*;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.AbstractException;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.exception.InternalException;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.*;

@Component
public class ClaimsInsertEnjoyCommandHandler implements CommandHandler {

    private static Logger LOGGER = LoggerFactory.getLogger(ClaimsInsertEnjoyCommandHandler.class);

    @Autowired
    private ESBService esbService;

    @Autowired
    private PersonalDataRepository personalDataRepository;

    @Autowired
    private ClaimsService claimsService;

    @Autowired
    private CounterpartyAdapter counterpartyAdapter;

    @Autowired
    private EnjoyService enjoyService;

    @Autowired
    private InsuranceCompanyRepository insuranceCompanyRepository;

    @Value("${company.no.system}")
    private UUID noCensitaCompanyUUID;

    @Autowired
    private IncidentService incidentService;

    @Autowired
    private AntiTheftRequestService antiTheftRequestService;

    @Autowired
    private ClaimsNewRepository claimsNewEntityRepository;

    @Autowired
    private ConverterClaimsService converterClaimsService;


    @Override
    public void handle(Command command) throws IOException {
        ClaimsInsertEnjoyCommand claimsInsertEnjoyCommand = (ClaimsInsertEnjoyCommand)command;

        if (claimsInsertEnjoyCommand.getComplaint().getDataAccident().getTypeAccident() == null && !claimsInsertEnjoyCommand.getWithCounterparty()
        ) {
            LOGGER.debug(MessageCode.CLAIMS_1068.value());
            throw new BadRequestException(MessageCode.CLAIMS_1068);
        }

        ClaimsEntity claimsEntity = new ClaimsEntity();

        claimsEntity.setUserEntity(claimsInsertEnjoyCommand.getUserId());
        claimsEntity.setId(claimsInsertEnjoyCommand.getClaimsId());

        Damaged damaged = DamagedAdapter.adptDamagedEnjoyRequestToDamaged(claimsInsertEnjoyCommand.getDamaged());
        ContractInfoResponseV1 contractInfoResponseV1 = null;

        //QUI RECUPERIAMO INFO CONTRACT, VEHICLE, CUSTOMER, INSURANCE
        try {
            //DA CAMBIARE QUESTA


        contractInfoResponseV1 = esbService.getContractInfoEnjoy(claimsInsertEnjoyCommand.getComplaint().getPlate(), claimsInsertEnjoyCommand.getComplaint().getDataAccident().getDateAccident());



            //ContractInfoResponseV1 contractInfoResponseV1 = esbService.getContractByContractIdOrPlate(claimsInsertEnjoyCommand.getComplaint().getPlate(), claimsInsertEnjoyCommand.getComplaint().getDataAccident().getDateAccident());
            if(contractInfoResponseV1 != null){

                //damaged.setDriver(DriverAdapter.adptDriverResponseToDriver(contractInfoResponseV1.getDriverResponse()));
                damaged.setContract(ContractAdapter.adptContractResponseToContract(contractInfoResponseV1.getContractResponse()));
                damaged.setVehicle(VehicleAdapter.adptVehicleResponseToVehicle(contractInfoResponseV1.getVehicleResponse()));
                damaged.setCustomer(CustomerAdapter.adptCustomerResponseToCustomer(contractInfoResponseV1.getCustomerResponse()));
                //damaged.setFleetManagerList(FleetManagerAdpter.adptFromFMResponseToFM(contractInfoResponseV1.getFleetManagerResponseList()));
                damaged.setInsuranceCompany(InsuranceCompanyAdapter.adptInsuranceCompanyResponseToInsuranceCompany(contractInfoResponseV1.getInsuranceCompany()));
                //damaged.setAntiTheftService(new AntiTheftService());
                //damaged.getAntiTheftService().setRegistryList(RegistryAdapter.adptFromRegistryResponseToRegistryList(contractInfoResponseV1.getRegistryResponseList()));
            }

        } catch (NotFoundException | ForbiddenException e) {
            LOGGER.debug(e.getMessage());
            throw e;
        } catch (AbstractException | ParseException e) {
            LOGGER.info(MessageCode.CLAIMS_1018.toString());
            throw new InternalException(MessageCode.CLAIMS_1018, e);
        }


        claimsEntity.setDamaged(damaged);
        claimsEntity.setWithCounterparty(claimsInsertEnjoyCommand.getWithCounterparty());


        if (claimsInsertEnjoyCommand.getComplaint() != null) {
            claimsEntity.setComplaint(ComplaintAdapter.adtpFromEnjoyComplaintToComplaint(claimsInsertEnjoyCommand.getComplaint()));
            if(damaged != null && damaged.getCustomer() != null)
            {
                claimsEntity.getComplaint().setClientId(damaged.getCustomer().getCustomerId());
            }
        }

        //recupero il flusso
        try {
            ContractTypeEntity contractTypeEntity = claimsService.getContractType(claimsEntity.getDamaged().getContract().getContractType());

            if(contractTypeEntity == null){
                LOGGER.debug(MessageCode.CLAIMS_1010.value());
                throw new BadRequestException(MessageCode.CLAIMS_1010);
            }

            if (contractTypeEntity.getFlagWS() != null && !contractTypeEntity.getFlagWS()) {
                LOGGER.debug(MessageCode.CLAIMS_1042.value());
                throw new BadRequestException(MessageCode.CLAIMS_1042);
            } else {
                claimsEntity.setType(claimsService.getPersonalRiskFlowType(claimsEntity.getDamaged().getContract().getContractType(),
                        claimsEntity.getComplaint().getDataAccident().getTypeAccident(),
                        claimsEntity.getDamaged().getCustomer().toString())); /* tipo di flusso */
                if (claimsEntity.getType().equals(ClaimsFlowEnum.NO)) {
                    LOGGER.debug("It's not possible insert a claim with NO flow");
                    throw new BadRequestException("It's not possible insert a claim with NO flow", MessageCode.CLAIMS_1057);
                }

            }
        } catch (BadRequestException e) {
            LOGGER.debug(e.getMessage());
            throw e;

        }

        if (claimsInsertEnjoyCommand.getCounterparty() != null) {
            String plate = null;
            if (claimsEntity.getDamaged() != null && claimsEntity.getDamaged().getVehicle() != null && claimsEntity.getDamaged().getVehicle().getLicensePlate() != null)
                plate = claimsEntity.getDamaged().getVehicle().getLicensePlate();
            if (plate != null) {
                for (CounterpartyEnjoyRequest counterpartyRequest : claimsInsertEnjoyCommand.getCounterparty()) {
                    if (counterpartyRequest.getVehicle() != null && counterpartyRequest.getVehicle().getLicensePlate() != null) {
                        if (counterpartyRequest.getVehicle().getLicensePlate().equals(plate)) {
                            LOGGER.debug(MessageCode.CLAIMS_1041.value());
                            throw new BadRequestException(MessageCode.CLAIMS_1041);
                        }
                    }

                    //check se compagnia controparte è diversa da null altrimenti viene recuperata quella non censita

                    Optional<InsuranceCompanyEntity> noCensitaCompanyOptional = insuranceCompanyRepository.findById(noCensitaCompanyUUID);
                    if(counterpartyRequest.getInsuranceCompany() == null || counterpartyRequest.getInsuranceCompany().getEntity() == null){

                        if(noCensitaCompanyOptional.isPresent() ){
                            InsuranceCompanyCounterpartyRequest insuranceCompanyCounterpartyRequest = counterpartyRequest.getInsuranceCompany();
                            if(insuranceCompanyCounterpartyRequest == null){
                                insuranceCompanyCounterpartyRequest = new InsuranceCompanyCounterpartyRequest();
                            }
                            InsuranceCompanyEntity noCensitaCompany = noCensitaCompanyOptional.get();
                            InsuranceCompanyRequestV1 entity = new InsuranceCompanyRequestV1(noCensitaCompany.getName());
                            entity.setActive(noCensitaCompany.getActive());
                            entity.setAniaCode(noCensitaCompany.getAniaCode());
                            entity.setId(noCensitaCompany.getId());
                            entity.setEmail(noCensitaCompany.getEmail());
                            insuranceCompanyCounterpartyRequest.setEntity(entity);
                            counterpartyRequest.setInsuranceCompany(insuranceCompanyCounterpartyRequest);
                        }
                    }



                }
            }
            claimsEntity.setCounterparts(counterpartyAdapter.adptCounterpartyEnjoyRequestToCounterparty(claimsInsertEnjoyCommand.getCounterparty()));
        }

        if (claimsInsertEnjoyCommand.getDeponent() != null)
            claimsEntity.setDeponentList(DeponentAdapter.adptDeponentRequestToDeponent(claimsInsertEnjoyCommand.getDeponent()));

        if (claimsInsertEnjoyCommand.getWounded() != null)
            claimsEntity.setWoundedList(WoundedAdapter.adptWoundedRequestToWounded(claimsInsertEnjoyCommand.getWounded()));

        if (claimsInsertEnjoyCommand.getCaiDetails() != null)
            claimsEntity.setCaiDetails(CaiAdapter.adptCaiToCaiResponse(claimsInsertEnjoyCommand.getCaiDetails()));

        claimsEntity.setIdSaleforce(claimsInsertEnjoyCommand.getIdSaleforce());

        if (claimsInsertEnjoyCommand.getTheftRequest() != null)
            claimsEntity.setTheft(TheftClaimsAdapter.adptTheftEnjoyRequestToTheft(claimsInsertEnjoyCommand.getTheftRequest()));

        claimsEntity.setPoVariation(false);
        Instant nowInstant = DateUtil.getNowInstant();
        claimsEntity.setCreatedAt(nowInstant);
        claimsEntity.setUpdateAt(nowInstant);

        claimsEntity.setCompleteDocumentation(claimsInsertEnjoyCommand.getCompleteDocumentation());
        
        claimsEntity.setPaiComunication(false);
        claimsEntity.setForced(false);
        claimsEntity.setInEvidence(false);
        claimsEntity.setWithContinuation(false);
        claimsEntity.setLegalComunication(false);

        /*****************************************************************
           lo stato iniziale del sx di enjoy non è piu ClaimsStatusEnum.DRAFT
            //claimsEntity.setStatus(ClaimsStatusEnum.DRAFT);
         ****************************************************************/
        claimsEntity.setStatus(ClaimsStatusEnum.WAITING_FOR_VALIDATION);
        ClaimsNewEntity claimsNewEntity = converterClaimsService.convertOldEntityToNewClaimsEntity(claimsEntity);
        claimsNewEntityRepository.save(claimsNewEntity);
        LOGGER.info("START ENJOY FLOW");
        enjoyService.enjoyFlow(claimsInsertEnjoyCommand,claimsEntity.getId(),contractInfoResponseV1.getContractResponse().getDriverId(),contractInfoResponseV1.getCustomerResponse().getCustomerId(),contractInfoResponseV1.getContractResponse().getContractId().toString(),claimsInsertEnjoyCommand.getComplaint().getDataAccident().getDateAccident());
        LOGGER.info("END ENJOY FLOW");

    }
}
