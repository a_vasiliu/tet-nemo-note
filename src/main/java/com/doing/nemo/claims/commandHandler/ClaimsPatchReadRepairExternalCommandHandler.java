package com.doing.nemo.claims.commandHandler;

import com.doing.nemo.claims.adapter.CounterpartyAdapter;
import com.doing.nemo.claims.command.ClaimsPatchReadExternalCommand;
import com.doing.nemo.claims.command.ClaimsPatchReadRepairExternalCommand;
import com.doing.nemo.claims.common.commandBus.Command;
import com.doing.nemo.claims.common.commandBus.CommandHandler;
import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.IdRequestV1;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.CounterpartyEntity;
import com.doing.nemo.claims.entity.CounterpartyNewEntity;
import com.doing.nemo.claims.entity.enumerated.WorkingStatusEnum;
import com.doing.nemo.claims.entity.jsonb.Authority;
import com.doing.nemo.claims.entity.jsonb.complaint.jsonbComplaint.Entrusted;
import com.doing.nemo.claims.entity.jsonb.complaint.jsonbComplaint.FromCompany;
import com.doing.nemo.claims.entity.jsonb.forms.jsonbForms.Attachment;
import com.doing.nemo.claims.repository.ClaimsRepository;
import com.doing.nemo.claims.repository.CounterpartyNewRepository;
import com.doing.nemo.claims.repository.CounterpartyRepository;
import com.doing.nemo.claims.service.DwhClaimsService;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class ClaimsPatchReadRepairExternalCommandHandler implements CommandHandler {
    private static Logger LOGGER = LoggerFactory.getLogger(ClaimsPatchReadRepairExternalCommandHandler.class);
    @Autowired
    private CounterpartyNewRepository counterpartyRepository;

    @Value("${dwh.call}")
    private Boolean dwhCall;

    @Autowired
    private DwhClaimsService dwhClaimsService;

    @Override
    public void handle(Command command) {
        //REFACTOR

        ClaimsPatchReadRepairExternalCommand claimsPatchReadExternalCommand = (ClaimsPatchReadRepairExternalCommand) command;
        if (claimsPatchReadExternalCommand.getClaimsIdListRequest() != null) {
            for (IdRequestV1 claimsIdRequestV1 : claimsPatchReadExternalCommand.getClaimsIdListRequest().getData()) {
                //Recupero la nuova entità
                Optional<CounterpartyNewEntity> counterpartyEntityOptional = counterpartyRepository.findById(claimsIdRequestV1.getId());
                if (!counterpartyEntityOptional.isPresent()) {
                    LOGGER.debug("Claims with UUID " + claimsIdRequestV1.getId() + " not found ");
                    throw new NotFoundException("Claims with UUID " + claimsIdRequestV1.getId() + " not found ");
                }
                //converto nella vecchia entità
                CounterpartyEntity counterpartyEntity = CounterpartyAdapter.adptCounterpartyNewToCounterpartyOld(counterpartyEntityOptional.get());

                counterpartyEntity.setIsReadMsa(true);
                if(counterpartyEntity.getAttachments() != null){
                    for(Attachment attachment : counterpartyEntity.getAttachments()){
                        attachment.setMsaDownloaded(true);
                    }
                }
                List<Authority> authorities = counterpartyEntity.getAuthorities();
                if(authorities != null){
                    for(Authority authority : authorities){
                        if((authority.getWorkingStatus() != null && (authority.getWorkingStatus().equals(WorkingStatusEnum.CLOSED) || authority.getWorkingStatus().equals(WorkingStatusEnum.CLOSED_WITH_LIQUIDATION) || authority.getWorkingStatus().equals(WorkingStatusEnum.CLOSED_WITH_REJECTION))) && (authority.getMsaDownloaded() == null || !authority.getMsaDownloaded()))
                        {
                            authority.setMsaDownloaded(true);
                        }
                    }
                }
                //converto nella nuova entità
                CounterpartyNewEntity counterpartyNewEntity = CounterpartyAdapter.adptCounterpartyOldToCounterpartyNew(counterpartyEntity);

                counterpartyRepository.save(counterpartyNewEntity);

                if(dwhCall){
                    dwhClaimsService.sendMessage(counterpartyEntity, com.doing.nemo.dwh.client.payload.request.EventTypeEnum.UPDATED);
                }
            }
        }
    }
}
