package com.doing.nemo.claims.commandHandler;

import com.doing.nemo.claims.adapter.*;
import com.doing.nemo.claims.command.ClaimsPatchCommand;
import com.doing.nemo.claims.common.commandBus.Command;
import com.doing.nemo.claims.common.commandBus.CommandHandler;
import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.CounterpartyRequest;
import com.doing.nemo.claims.controller.payload.request.IncidentRequestV1;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.ClaimsNewEntity;
import com.doing.nemo.claims.entity.CounterpartyEntity;
import com.doing.nemo.claims.entity.CounterpartyNewEntity;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsFlowEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.enumerated.EventTypeEnum;
import com.doing.nemo.claims.entity.jsonb.Historical;
import com.doing.nemo.claims.entity.jsonb.complaint.Complaint;
import com.doing.nemo.claims.entity.jsonb.complaint.jsonbComplaint.jsonbDataAccidents.DataAccident;
import com.doing.nemo.claims.repository.ClaimsNewRepository;
import com.doing.nemo.claims.repository.CounterpartyNewRepository;
import com.doing.nemo.claims.service.ClaimsService;
import com.doing.nemo.claims.service.CounterpartyService;
import com.doing.nemo.claims.service.IncidentService;
import com.doing.nemo.claims.service.LockService;
import com.doing.nemo.claims.service.*;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Component
public class ClaimsPatchCommandHandler implements CommandHandler {
    private static Logger LOGGER = LoggerFactory.getLogger(ClaimsPatchCommandHandler.class);
    @Autowired
    private ClaimsNewRepository claimsNewRepository;
    @Autowired
    private ConverterClaimsService converterClaimsService;
    @Autowired
    private CounterpartyNewRepository counterpartyNewRepository;
    @Autowired
    private CounterpartyAdapter counterpartyAdapter;
    @Autowired
    private ClaimsService claimsService;
    @Autowired
    private LockService lockService;
    @Autowired
    private CounterpartyService counterpartyService;
    @Autowired
    private IncidentService incidentService;
    @Autowired
    private IncidentAdapter incidentAdapter;
    @Autowired
    private DwhClaimsService dwhClaimsService;
    @Autowired
    private ExternalCommunicationService externalCommunicationService;

    @Value("${dwh.call}")
    private Boolean dwhCall;



    @Override
    public void handle(Command command) {

        ClaimsPatchCommand claimsPatchCommand = (ClaimsPatchCommand) command;
        Optional<ClaimsNewEntity> claimsNewEntityOptional = claimsNewRepository.findById(claimsPatchCommand.getId());

        if (!claimsNewEntityOptional.isPresent()) {
            LOGGER.debug("Claims with UUID " + claimsPatchCommand.getId() + " not found ");
            throw new NotFoundException("Claims with UUID " + claimsPatchCommand.getId() + " not found ", MessageCode.CLAIMS_1010);
        }
        ClaimsNewEntity claimsNewEntity = claimsNewEntityOptional.get();
        ClaimsEntity claimsEntity = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntity);
        // da provare, serve per il log di incident per sapere vecchi campi e nuovi campi
        ClaimsEntity oldClaims =  converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntity);

        ClaimsStatusEnum oldStatus = claimsEntity.getStatus();
        IncidentRequestV1 oldIncident = incidentAdapter.adptFromClaimsEntityToIncidentRequest(claimsEntity,oldStatus,false,false);
        DataAccidentTypeAccidentEnum oldType = claimsEntity.getComplaint().getDataAccident().getTypeAccident();

        if (!claimsEntity.getStatus().equals(ClaimsStatusEnum.DRAFT)) {
            lockService.checkLock(claimsPatchCommand.getUserId(), claimsEntity.getId());
        }

        if (claimsPatchCommand.getDataUpdate().compareTo(claimsEntity.getUpdateAt()) > 0) {

            if (claimsPatchCommand.getDamaged() != null) {
                claimsEntity.setDamaged(DamagedAdapter.adptDamagedRequestToDamaged(claimsPatchCommand.getDamaged()));
            }
            if (claimsPatchCommand.getComplaint() != null) {
                claimsEntity.setComplaint(ComplaintAdapter.adptComplaintRequestToComplaintUpdate(claimsPatchCommand.getComplaint(), claimsEntity.getComplaint()));
            }

            if (claimsPatchCommand.getCounterparty() != null) {
                String plate = null;
                if (claimsEntity.getDamaged() != null && claimsEntity.getDamaged().getVehicle() != null && claimsEntity.getDamaged().getVehicle().getLicensePlate() != null)
                    plate = claimsEntity.getDamaged().getVehicle().getLicensePlate();
                if(plate != null) {
                    for (CounterpartyRequest counterpartyRequest : claimsPatchCommand.getCounterparty()) {
                        if (counterpartyRequest.getVehicle() != null && counterpartyRequest.getVehicle().getLicensePlate() != null) {
                            if (counterpartyRequest.getVehicle().getLicensePlate().equals(plate)) {
                                LOGGER.debug(MessageCode.CLAIMS_1041.value());
                                throw new BadRequestException(MessageCode.CLAIMS_1041);
                            }
                        }
                        if (counterpartyRequest.getUserCreate() == null || counterpartyRequest.getUserCreate().equalsIgnoreCase(""))
                            counterpartyRequest.setUserCreate(claimsPatchCommand.getUserId());
                    }
                }
                if(claimsEntity.getCounterparts() != null) {
                    for(CounterpartyEntity counterparty : claimsEntity.getCounterparts()){
                        CounterpartyRequest c = new CounterpartyRequest();
                        c.setCounterpartyId(counterparty.getCounterpartyId());
                        if(claimsPatchCommand.getCounterparty() != null && !claimsPatchCommand.getCounterparty().contains(c))
                            counterpartyNewRepository.deleteById(counterparty.getCounterpartyId());
                    }
                }
                List<CounterpartyEntity> counterpartyList = new ArrayList<>();
                if(claimsPatchCommand.getCounterparty() != null && !claimsPatchCommand.getCounterparty().isEmpty()) {
                    counterpartyList = new ArrayList<>();
                    for (CounterpartyRequest counterpartyRequest : claimsPatchCommand.getCounterparty()) {
                        LOGGER.debug("[Counterparty] Counterparty id " + counterpartyRequest.getCounterpartyId());
                        LOGGER.debug("[Counterparty] Counterparty status " + counterpartyRequest.getRepairStatus());
                        CounterpartyEntity counterparty1 = counterpartyAdapter.adptCounterpartyRequestToCounterpartyUpdate(counterpartyRequest);
                        CounterpartyNewEntity counterpartyNewEntity = CounterpartyAdapter.adptCounterpartyOldToCounterpartyNew(counterparty1);
                        counterpartyNewRepository.save(counterpartyNewEntity);
                        counterpartyList.add(counterparty1);
                    }
                }

                claimsEntity.setCounterparts(counterpartyList);

                //claimsEntity.setCounterparts(counterpartyAdapter.adptCounterpartyRequestToCounterparty(claimsPatchCommand.getCounterparty()));
            }

            if (claimsPatchCommand.getDeponent() != null) {
                claimsEntity.setDeponentList(DeponentAdapter.adptDeponentRequestToDeponent(claimsPatchCommand.getDeponent()));
            }
            if (claimsPatchCommand.getWounded() != null) {
                claimsEntity.setWoundedList(WoundedAdapter.adptWoundedRequestToWounded(claimsPatchCommand.getWounded()));
            }

            if (claimsPatchCommand.getCaiDetails() != null)
                claimsEntity.setCaiDetails(CaiAdapter.adptCaiToCaiResponse(claimsPatchCommand.getCaiDetails()));

            if (claimsPatchCommand.getExemption() != null) {
                claimsEntity.setExemption(ExemptionAdapter.adptExemptionRequestToExemption(claimsPatchCommand.getExemption()));
            }

            if (claimsPatchCommand.getWithCounterparty() != null) {
                claimsEntity.setWithCounterparty(claimsPatchCommand.getWithCounterparty());
            }

            if (!claimsEntity.getWithCounterparty() && claimsPatchCommand.getDamaged() != null && claimsPatchCommand.getDamaged().getImpactPoint() != null && claimsPatchCommand.getDamaged().getImpactPoint().getIncidentDescription() != null) {
                claimsEntity.getDamaged().getImpactPoint().setIncidentDescription(claimsPatchCommand.getDamaged().getImpactPoint().getIncidentDescription());

            } else if (claimsEntity.getWithCounterparty() && claimsPatchCommand.getCaiDetails() != null &&
                    claimsPatchCommand.getCaiDetails().getVehicleA() != null &&
                    claimsPatchCommand.getCaiDetails().getVehicleB() != null
            ) {

                String note = new String();
                note += "VEICOLO A: ";
                note += CaiAdapter.adptCaiConditionToCaiNote(CaiDetailsAdapter.adptCaiDetailsRequestToCaiDetails(claimsPatchCommand.getCaiDetails().getVehicleA()));
                note += "\nVEICOLO B: ";
                note += CaiAdapter.adptCaiConditionToCaiNote(CaiDetailsAdapter.adptCaiDetailsRequestToCaiDetails(claimsPatchCommand.getCaiDetails().getVehicleB()));
                claimsEntity.getDamaged().getImpactPoint().setIncidentDescription(note);
                //prova della definizione della logica di determinazione in base al cai
                if (claimsPatchCommand.getCaiDetails().getVehicleA() != null && claimsPatchCommand.getCaiDetails().getVehicleB() != null) {
                    Boolean isCaiSignedA = false;
                    Boolean isCaiSignedB = false;
                    String companyDenomination = "";

                    if (claimsEntity.getDamaged() != null) {
                        isCaiSignedA = claimsEntity.getDamaged().getCaiSigned();
                        if (claimsEntity.getDamaged().getInsuranceCompany() != null && claimsEntity.getDamaged().getInsuranceCompany().getTpl() != null) {
                            companyDenomination = claimsEntity.getDamaged().getInsuranceCompany().getTpl().getCompany();
                        }
                    }
                    if (claimsEntity.getCounterparts() != null && claimsEntity.getCounterparts().size() > 0) {
                        isCaiSignedB = claimsEntity.getCounterparts().get(0).getCaiSigned();
                    }

                    if (isCaiSignedA == null) {
                        isCaiSignedA = false;
                    }

                    if (isCaiSignedB == null) {
                        isCaiSignedB = false;
                    }

                    Map<String, Object> caiResult = claimsService.getFlowDetailsByCai(claimsEntity.getCaiDetails(), claimsEntity.getDamaged().getContract().getContractType(), claimsEntity.getDamaged(), claimsEntity.getCounterparts(), isCaiSignedA, isCaiSignedB);

                    claimsEntity.setType((ClaimsFlowEnum) caiResult.get("flow"));
                    DataAccident dataAccident = claimsEntity.getComplaint().getDataAccident();

                    dataAccident.setTypeAccident((DataAccidentTypeAccidentEnum) caiResult.get("type"));
                    Complaint complaint = claimsEntity.getComplaint();
                    complaint.setDataAccident(dataAccident);

                    if (!complaint.getDataAccident().getTypeAccident().equals(oldType)) {
                        String description = "La tipologia è cambiata da " + oldType.getValue().replaceAll("_", " ") + " a " + complaint.getDataAccident().getTypeAccident().getValue().replaceAll("_", " ");
                        Historical historical = new Historical(EventTypeEnum.EDIT_PRACTICE_DATA, claimsEntity.getStatus(), claimsEntity.getStatus(), oldType, complaint.getDataAccident().getTypeAccident(), DateUtil.getNowDate(), claimsPatchCommand.getUserId(), claimsPatchCommand.getUserName(), claimsEntity.getType().getValue(), claimsEntity.getType().getValue(), description);
                        claimsEntity.addHistorical(historical);
                    }
                    claimsEntity.setComplaint(complaint);

                }

            } else {
                if (claimsPatchCommand.getComplaint() != null && claimsPatchCommand.getComplaint().getDataAccident() != null && claimsPatchCommand.getComplaint().getDataAccident().getTypeAccident() != null &&
                        !claimsPatchCommand.getComplaint().getDataAccident().getTypeAccident().equals(oldType)) {
                    String description = "La tipologia è cambiata da " + oldType.getValue().replaceAll("_", " ") + " a " + claimsPatchCommand.getComplaint().getDataAccident().getTypeAccident().getValue().replaceAll("_", " ");
                    Historical historical = new Historical(EventTypeEnum.EDIT_PRACTICE_DATA, claimsEntity.getStatus(), claimsEntity.getStatus(), oldType, claimsPatchCommand.getComplaint().getDataAccident().getTypeAccident(), DateUtil.getNowDate(), claimsPatchCommand.getUserId(), claimsPatchCommand.getUserName(), claimsEntity.getType().getValue(), claimsEntity.getType().getValue(), description);
                    claimsEntity.addHistorical(historical);
                }

            }

            if (claimsPatchCommand.getTheft() != null) {
                claimsEntity.setTheft(TheftClaimsAdapter.adptTheftRequestV1ToTheft(claimsPatchCommand.getTheft()));
            }

            if (claimsPatchCommand.getPaiComunication() != null) {
                claimsEntity.setPaiComunication(claimsPatchCommand.getPaiComunication());
                //aggiungere il cambio di stato
            }

            if (claimsPatchCommand.getLegalComunication() != null) {
                claimsEntity.setLegalComunication(claimsPatchCommand.getLegalComunication());
                //aggiungere il cambio di stato
            }

            if (claimsPatchCommand.getFoundModel() != null) {
                claimsEntity.setFoundModel(FoundModelAdapter.adptFoundModelRequestToFoundModel(claimsPatchCommand.getFoundModel()));
            }
            if (claimsPatchCommand.getIdSaleforce() != null) {
                claimsEntity.setIdSaleforce(claimsPatchCommand.getIdSaleforce());
            }

            if (claimsPatchCommand.getRefund() != null) {
                claimsEntity.setRefund(RefundAdapter.adptFromRefundRequestToRefund(claimsPatchCommand.getRefund()));
            }

            if (claimsPatchCommand.getPoVariation() != null) {
                claimsEntity.setPoVariation(claimsPatchCommand.getPoVariation());
            }
            
            if(claimsPatchCommand.getCompleteDocumentation() != null){
                claimsEntity.setCompleteDocumentation(claimsPatchCommand.getCompleteDocumentation());
            }

            claimsEntity.setUpdateAt(DateUtil.getNowInstant());

            if (!claimsEntity.getStatus().equals(ClaimsStatusEnum.INCOMPLETE) && !claimsEntity.getStatus().equals(ClaimsStatusEnum.WAITING_FOR_VALIDATION)) {
                counterpartyService.EligibilityCriterion(claimsEntity,claimsPatchCommand.getUserId(),claimsPatchCommand.getUserName() );
            }
            claimsNewEntity = converterClaimsService.convertOldEntityToNewClaimsEntity(claimsEntity);
            claimsNewRepository.save(claimsNewEntity);

            if(dwhCall){
                dwhClaimsService.sendMessage(claimsEntity, com.doing.nemo.dwh.client.payload.request.EventTypeEnum.UPDATED);
                if(claimsNewEntity.getCounterparts() != null) {
                    for (CounterpartyNewEntity counterpartyNew : claimsNewEntity.getCounterparts()) {
                        CounterpartyEntity counterparty = CounterpartyAdapter.adptCounterpartyNewToCounterpartyOld(counterpartyNew);
                        dwhClaimsService.sendMessage(counterparty, com.doing.nemo.dwh.client.payload.request.EventTypeEnum.UPDATED);
                    }
                }
            }
        }

        //Intervento 6 - C
        try {
            externalCommunicationService.modifyIncidentAsync(claimsPatchCommand.getId(),oldStatus, false,oldIncident,false, oldClaims);
        } catch (IOException e) {
            throw new BadRequestException(MessageCode.CLAIMS_1112);
        }

    }
}
