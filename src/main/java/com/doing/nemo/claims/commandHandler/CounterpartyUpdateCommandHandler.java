package com.doing.nemo.claims.commandHandler;

import com.doing.nemo.claims.adapter.*;
import com.doing.nemo.claims.command.CounterpartyUpdateCommand;
import com.doing.nemo.claims.common.commandBus.Command;
import com.doing.nemo.claims.common.commandBus.CommandHandler;
import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.counterparty.LastContactRequest;
import com.doing.nemo.claims.entity.*;

import com.doing.nemo.claims.entity.jsonb.repair.HistoricalCounterparty;
import com.doing.nemo.claims.entity.jsonb.repair.LastContact;
import com.doing.nemo.claims.repository.ClaimsNewRepository;
import com.doing.nemo.claims.repository.CounterpartyNewRepository;
import com.doing.nemo.claims.repository.CounterpartyRepository;
import com.doing.nemo.claims.service.ConverterClaimsService;
import com.doing.nemo.claims.service.DwhClaimsService;
import com.doing.nemo.claims.service.ManagerService;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.dwh.client.payload.request.EventTypeEnum;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class CounterpartyUpdateCommandHandler implements CommandHandler {
    private static Logger LOGGER = LoggerFactory.getLogger(CounterpartyUpdateCommandHandler.class);
    @Autowired
    private ClaimsNewRepository claimsRepository;

    @Autowired
    private CounterpartyNewRepository counterpartyRepository;

    @Autowired
    private ConverterClaimsService converterClaimsService;

    @Autowired
    private LastContactAdapter lastContactAdapter;
    @Autowired
    private InsuranceCompanyCounterpartyAdapter insuranceCompanyCounterpartyAdapter;
    @Autowired
    private ManagerService managerService;
    @Autowired
    private DwhClaimsService dwhClaimsService;

    @Value("${dwh.call}")
    private Boolean dwhCall;


    @Override
    public void handle(Command command) {
        CounterpartyUpdateCommand counterpartyUpdateCommand = (CounterpartyUpdateCommand) command;
        Optional<CounterpartyNewEntity> counterpartyNewEntityOptional = counterpartyRepository.findById(counterpartyUpdateCommand.getCounterpartyId());

        if (!counterpartyNewEntityOptional.isPresent()) {
            throw new NotFoundException("Counterparty with UUID " + counterpartyUpdateCommand.getCounterpartyId() + " not found", MessageCode.CLAIMS_1010);
        }

        CounterpartyNewEntity counterparty = counterpartyNewEntityOptional.get();
        ClaimsEntity claimsEntity = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(counterpartyNewEntityOptional.get().getClaims());

        /***********************************************************
         *   Inizio Verifica tipo aggiornamento soggetto a log Repair
         ***********************************************************/
        String smallDescription = "Variazione Dati";
        StringBuilder description = new StringBuilder();
        if (counterpartyUpdateCommand.getEligibility()) { // se è una pratica di riparazione

            if (counterpartyUpdateCommand.getInsured() != null) {
                String insuredNewPhone = counterpartyUpdateCommand.getInsured().getPhone() != null ? counterpartyUpdateCommand.getInsured().getPhone() : "";
                String insuredNewMail = counterpartyUpdateCommand.getInsured().getEmail() != null ? counterpartyUpdateCommand.getInsured().getEmail() : "";
                String insuredOldPhone = "";
                String insuredOldMail = "";
                if (counterparty.getInsured() != null) {
                    insuredOldPhone = counterparty.getInsured().getPhone() != null ? counterparty.getInsured().getPhone() : "";
                    insuredOldMail = counterparty.getInsured().getEmail() != null ? counterparty.getInsured().getEmail() : "";
                }
                if (!insuredNewPhone.equalsIgnoreCase(insuredOldPhone)) {
                    description.append("Telefono Assicurato precedente : ").append(insuredOldPhone).append("<br>");
                    description.append("Telefono Assicurato attuale : ").append(insuredNewPhone).append("<br>");
                }
                if (!insuredNewMail.equalsIgnoreCase(insuredOldMail)) {
                    description.append("Mail Assicurato precedente : ").append(insuredOldMail).append("<br>");
                    description.append("Mail Assicurato attuale : ").append(insuredNewMail).append("<br>");
                }
            }

            if (counterpartyUpdateCommand.getDriver() != null) {
                String driverNewPhone = counterpartyUpdateCommand.getDriver().getPhone() != null ? counterpartyUpdateCommand.getDriver().getPhone() : "";
                String driverNewMail = counterpartyUpdateCommand.getDriver().getEmail() != null ? counterpartyUpdateCommand.getDriver().getEmail() : "";
                String driverOldPhone = "";
                String driverOldMail = "";
                if (counterparty.getDriver() != null) {
                    driverOldPhone = counterparty.getDriver().getPhone() != null ? counterparty.getDriver().getPhone() : "";
                    driverOldMail = counterparty.getDriver().getEmail() != null ? counterparty.getDriver().getEmail() : "";
                }
                if (!driverNewPhone.equalsIgnoreCase(driverOldPhone)) {
                    description.append("Telefono Conducente precedente : ").append(driverOldPhone).append("<br>");
                    description.append("Telefono Conducente attuale : ").append(driverNewPhone).append("<br>");
                }
                if (!driverNewMail.equalsIgnoreCase(driverOldMail)) {
                    description.append("Mail Conducente precedente : ").append(driverOldMail).append("<br>");
                    description.append("Mail Conducente attuale : ").append(driverNewMail).append("<br>");
                }
            }

            if (counterpartyUpdateCommand.getLastContact() != null && !counterpartyUpdateCommand.getLastContact().isEmpty()) {


                List<LastContactRequest> sortedLastContactRequest = counterpartyUpdateCommand.getLastContact().stream()
                        .sorted(Comparator.comparing(LastContactRequest::getLastCall).reversed())
                        .collect(Collectors.toList());

                LastContactRequest lastNewContact = sortedLastContactRequest.get(0);

                LastContact lastOldContact = new LastContact();
                if (counterparty.getLastContact() != null && !counterparty.getLastContact().isEmpty()) {
                    lastOldContact = Collections.min(counterparty.getLastContact(), Comparator.comparing(LastContact::getLastCall));
                }

                Date dateNewLastCall = DateUtil.convertIS08601StringToUTCDate(lastNewContact.getLastCall());
                if (!dateNewLastCall.equals(lastOldContact.getLastCall())) {
                    if (description.length() > 0)
                        description.append("<br>").append("Data Call : ").append(lastNewContact.getLastCall()).append("<br>");
                    else
                        description.append("Data della Call : ").append(lastNewContact.getLastCall()).append("<br>");

                     if (lastNewContact.getResult()!=null){
                         switch (lastNewContact.getResult().getValue()) {
                          case "DENIAL":
                              smallDescription = "Diniego della Controparte";
                              description.append("Esito : ").append("Diniego").append("<br>");
                               break;
                             // Case 2
                             case "REPAIR_FLOW_ACCEPTED":
                                 smallDescription = "Accettazione della Controparte";
                                 description.append("Esito : ").append("Preventivo Accettato").append("<br>");
                                 break;
                             // Case 3
                             case "TO_RECONTACT":
                                 smallDescription = "Chiamata alla Controparte";
                                 description.append("Esito : ").append("Da Ricontattare").append("<br>");
                                 break;
                             default:
                                 description.append("Esito : ").append(" ").append("<br>");
                         }

                     }
                     if (lastNewContact.getMotivation()!=null&&lastNewContact.getMotivation().getDescription()!=null){
                         description.append("Motivazione : ").append(lastNewContact.getMotivation().getDescription()).append("<br>");
                     }
                     if (lastNewContact.getNote()!=null){
                         description.append("Note : ").append(lastNewContact.getNote());
                     }
                }

            }


            if (counterpartyUpdateCommand.getCanalization() != null && counterpartyUpdateCommand.getCanalization().getCenter() != null) {
                StringBuilder addressCenterOld = new StringBuilder();

                if (counterparty.getCanalization() != null && counterparty.getCanalization().getCenterEntity() != null) {
                    CenterEntity centerOLD = counterparty.getCanalization().getCenterEntity();

                    if (!centerOLD.getName().equalsIgnoreCase(counterpartyUpdateCommand.getCanalization().getCenter().getName())) {

                        /*************************** Vecchio Riparatore *********************************************/
                        if (counterparty.getCanalization().getCenterEntity().getAddress() != null)
                            if (addressCenterOld.length() > 0)
                                addressCenterOld.append(",").append(counterparty.getCanalization().getCenterEntity().getAddress());
                            else
                                addressCenterOld.append(counterparty.getCanalization().getCenterEntity().getAddress());

                        if (counterparty.getCanalization().getCenterEntity().getAddressNumber() != null)
                            if (addressCenterOld.length() > 0)
                                addressCenterOld.append(",").append(counterparty.getCanalization().getCenterEntity().getAddressNumber());
                            else
                                addressCenterOld.append(counterparty.getCanalization().getCenterEntity().getAddressNumber());
                        if (counterparty.getCanalization().getCenterEntity().getMunicipality() != null)
                            if (addressCenterOld.length() > 0)
                                addressCenterOld.append(",").append(counterparty.getCanalization().getCenterEntity().getMunicipality());
                            else
                                addressCenterOld.append(counterparty.getCanalization().getCenterEntity().getMunicipality());
                        if (counterparty.getCanalization().getCenterEntity().getRegion() != null)
                            if (addressCenterOld.length() > 0)
                                addressCenterOld.append(",").append(counterparty.getCanalization().getCenterEntity().getRegion());
                            else
                                addressCenterOld.append(counterparty.getCanalization().getCenterEntity().getRegion());


                        if (counterparty.getCanalization().getCenterEntity().getName() != null) {
                            if (description.length() > 0)
                                description.append("<br>").append("Vecchio Riparatore : ").append(counterparty.getCanalization().getCenterEntity().getName()).append("<br>");
                            else
                                description.append("Vecchio Riparatore : ").append(counterparty.getCanalization().getCenterEntity().getName()).append("<br>");
                        }

                        if (description.length() > 0)
                            description.append("<br>").append("Vecchio Indirizzo Riparatore : ").append(addressCenterOld.toString()).append("<br>");
                        else
                            description.append("Vecchio Indirizzo Riparatore : ").append(addressCenterOld.toString()).append("<br>");

                        /*************************** NUOVO Riparatore *********************************************/
                        StringBuilder addressCenterNew = new StringBuilder();
                        if (counterpartyUpdateCommand.getCanalization().getCenter().getAddress() != null)
                            if (addressCenterNew.length() > 0)
                                addressCenterNew.append(",").append(counterpartyUpdateCommand.getCanalization().getCenter().getAddress());
                            else
                                addressCenterNew.append(counterpartyUpdateCommand.getCanalization().getCenter().getAddress());

                        if (counterpartyUpdateCommand.getCanalization().getCenter().getAddressNumber() != null)
                            if (addressCenterNew.length() > 0)
                                addressCenterNew.append(",").append(counterpartyUpdateCommand.getCanalization().getCenter().getAddressNumber());
                            else
                                addressCenterNew.append(counterpartyUpdateCommand.getCanalization().getCenter().getAddressNumber());
                        if (counterpartyUpdateCommand.getCanalization().getCenter().getMunicipality() != null)
                            if (addressCenterNew.length() > 0)
                                addressCenterNew.append(",").append(counterpartyUpdateCommand.getCanalization().getCenter().getMunicipality());
                            else
                                addressCenterNew.append(counterpartyUpdateCommand.getCanalization().getCenter().getMunicipality());
                        if (counterpartyUpdateCommand.getCanalization().getCenter().getRegion() != null)
                            if (addressCenterNew.length() > 0)
                                addressCenterNew.append(",").append(counterpartyUpdateCommand.getCanalization().getCenter().getRegion());
                            else
                                addressCenterNew.append(counterpartyUpdateCommand.getCanalization().getCenter().getRegion());


                        if (counterpartyUpdateCommand.getCanalization().getCenter().getName() != null) {
                            if (description.length() > 0)
                                description.append("<br>").append("Nuovo Riparatore : ").append(counterpartyUpdateCommand.getCanalization().getCenter().getName()).append("<br>");
                            else
                                description.append("Nuovo Riparatore : ").append(counterpartyUpdateCommand.getCanalization().getCenter().getName()).append("<br>");
                        }

                        if (description.length() > 0)
                            description.append("<br>").append("Nuovo Indirizzo Riparatore : ").append(addressCenterNew.toString()).append("<br>");
                        else
                            description.append("Nuovo Indirizzo Riparatore : ").append(addressCenterNew.toString()).append("<br>");

                    }
                }
            }

            /***********************************************************
             *   GESTIONE
             ***********************************************************/
            if (counterpartyUpdateCommand.getManagementType()!=null) {
                if (counterpartyUpdateCommand.getManagementType().getValue() != null && !counterpartyUpdateCommand.getManagementType().getValue().isEmpty()) {
                    if (counterparty.getManagementType()==null||counterparty.getManagementType().getValue() == null || counterparty.getManagementType().getValue().isEmpty()) {
                        if (description.length() > 0)
                            description.append("<br>").append("Tipo Gestione : ").append(counterpartyUpdateCommand.getManagementType().getValue()).append("<br>");
                        else
                            description.append("Tipo Gestione : ").append(counterpartyUpdateCommand.getManagementType().getValue()).append("<br>");
                    } else if (!counterpartyUpdateCommand.getManagementType().getValue().equalsIgnoreCase(counterparty.getManagementType().getValue())) {
                        if (description.length() > 0)
                            description.append("<br>").append("Tipo Gestione : ").append(counterpartyUpdateCommand.getManagementType().getValue()).append("<br>");
                        else
                            description.append("Tipo Gestione : ").append(counterpartyUpdateCommand.getManagementType().getValue()).append("<br>");
                    }
                }
            }
            if (counterpartyUpdateCommand.getAskedForDamages()!=null){
                if (counterparty.getAskedForDamages()==null) {
                    if (description.length() > 0)
                        description.append("<br>").append("Richiesta Danni : ").append(counterpartyUpdateCommand.getAskedForDamages()?"Si":"No").append("<br>");
                    else
                        description.append("Richiesta Danni : ").append(counterpartyUpdateCommand.getAskedForDamages()?"Si":"No").append("<br>");
                }else if (!counterpartyUpdateCommand.getAskedForDamages().equals(counterparty.getAskedForDamages())){
                    if (description.length() > 0)
                        description.append("<br>").append("Richiesta Danni : ").append(counterpartyUpdateCommand.getAskedForDamages()?"Si":"No").append("<br>");
                    else
                        description.append("Richiesta Danni : ").append(counterpartyUpdateCommand.getAskedForDamages()?"Si":"No").append("<br>");
                }
            }

            if (counterpartyUpdateCommand.getLegalOrConsultant()!=null){
                if (counterparty.getLegalOrConsultant()==null) {
                    if (description.length() > 0)
                        description.append("<br>").append("Legale o Consulente : ").append(counterpartyUpdateCommand.getLegalOrConsultant()?"Si":"No").append("<br>");
                    else
                        description.append("Legale o Consulente : ").append(counterpartyUpdateCommand.getLegalOrConsultant()?"Si":"No").append("<br>");
                }else if (!counterpartyUpdateCommand.getLegalOrConsultant().equals(counterparty.getLegalOrConsultant())){
                    if (description.length() > 0)
                        description.append("<br>").append("Legale o Consulente : ").append(counterpartyUpdateCommand.getLegalOrConsultant()?"Si":"No").append("<br>");
                    else
                        description.append("Legale o Consulente : ").append(counterpartyUpdateCommand.getLegalOrConsultant()?"Si":"No").append("<br>");
                }
            }

            if (counterpartyUpdateCommand.getDateRequestDamages()!=null&&!counterpartyUpdateCommand.getDateRequestDamages().isEmpty()){
                if (counterparty.getDateRequestDamages()==null) {
                    if (description.length() > 0)
                        description.append("<br>").append("Data Richiesta Danni : ").append(counterpartyUpdateCommand.getDateRequestDamages()).append("<br>");
                    else
                        description.append("Data Richiesta Danni : ").append(counterpartyUpdateCommand.getDateRequestDamages()).append("<br>");
                }else if (!counterpartyUpdateCommand.getDateRequestDamages().equalsIgnoreCase(counterparty.getDateRequestDamages().toString())){
                    if (description.length() > 0)
                        description.append("<br>").append("Data Richiesta Danni : ").append(counterpartyUpdateCommand.getDateRequestDamages()).append("<br>");
                    else
                        description.append("Data Richiesta Danni : ").append(counterpartyUpdateCommand.getDateRequestDamages()).append("<br>");
                }
            }

            if (counterpartyUpdateCommand.getExpirationDate()!=null&&!counterpartyUpdateCommand.getExpirationDate().isEmpty()){
                if (counterparty.getExpirationInstant()==null) {
                    if (description.length() > 0)
                        description.append("<br>").append("Data Scadenza Termini : ").append(counterpartyUpdateCommand.getExpirationDate()).append("<br>");
                    else
                        description.append("Data Scadenza Termini : ").append(counterpartyUpdateCommand.getExpirationDate()).append("<br>");
                }else if (!counterpartyUpdateCommand.getExpirationDate().equalsIgnoreCase(counterparty.getExpirationInstant().toString())){
                    if (description.length() > 0)
                        description.append("<br>").append("Data Scadenza Termini : ").append(counterpartyUpdateCommand.getExpirationDate()).append("<br>");
                    else
                        description.append("Data Scadenza Termini : ").append(counterpartyUpdateCommand.getExpirationDate()).append("<br>");
                }
            }

            if (counterpartyUpdateCommand.getCompleteDocumentation()!=null){
                if (counterparty.getCompleteDocumentation()==null) {
                    if (description.length() > 0)
                        description.append("<br>").append("Documentazione Completa : ").append(counterpartyUpdateCommand.getCompleteDocumentation()?"Si":"No").append("<br>");
                    else
                        description.append("Documentazione Completa : ").append(counterpartyUpdateCommand.getCompleteDocumentation()?"Si":"No").append("<br>");
                }else if (!counterpartyUpdateCommand.getCompleteDocumentation().equals(counterparty.getCompleteDocumentation())){
                    if (description.length() > 0)
                        description.append("<br>").append("Documentazione Completa : ").append(counterpartyUpdateCommand.getCompleteDocumentation()?"Si":"No").append("<br>");
                    else
                        description.append("Documentazione Completa : ").append(counterpartyUpdateCommand.getCompleteDocumentation()?"Si":"No").append("<br>");
                }
            }

            if (counterpartyUpdateCommand.getLegal()!=null&&!counterpartyUpdateCommand.getLegal().isEmpty()){
                if (counterparty.getLegal()==null||counterparty.getLegal().isEmpty()) {
                    if (description.length() > 0)
                        description.append("<br>").append("Legale : ").append(counterpartyUpdateCommand.getLegal()).append("<br>");
                    else
                        description.append("Legale : ").append(counterpartyUpdateCommand.getLegal()).append("<br>");
                }else if (!counterpartyUpdateCommand.getLegal().equalsIgnoreCase(counterparty.getLegal())){
                    if (description.length() > 0)
                        description.append("<br>").append("Legale : ").append(counterpartyUpdateCommand.getLegal()).append("<br>");
                    else
                        description.append("Legale : ").append(counterpartyUpdateCommand.getLegal()).append("<br>");
                }
            }

            if (counterpartyUpdateCommand.getEmailLegal()!=null&&!counterpartyUpdateCommand.getEmailLegal().isEmpty()){
                if (counterparty.getEmailLegal()==null||counterparty.getEmailLegal().isEmpty()) {
                    if (description.length() > 0)
                        description.append("<br>").append("Email Legale : ").append(counterpartyUpdateCommand.getEmailLegal()).append("<br>");
                    else
                        description.append("Email Legale : ").append(counterpartyUpdateCommand.getEmailLegal()).append("<br>");
                }else if (!counterpartyUpdateCommand.getEmailLegal().equalsIgnoreCase(counterparty.getEmailLegal())){
                    if (description.length() > 0)
                        description.append("<br>").append("Email Legale : ").append(counterpartyUpdateCommand.getEmailLegal()).append("<br>");
                    else
                        description.append("Email Legale : ").append(counterpartyUpdateCommand.getEmailLegal()).append("<br>");
                }
            }

            if (counterpartyUpdateCommand.getReplacementCar()!=null){
                if (counterparty.getReplacementCar()==null) {
                    if (description.length() > 0)
                        description.append("<br>").append("Auto Sostitutiva : ").append(counterpartyUpdateCommand.getReplacementCar()?"Si":"No").append("<br>");
                    else
                        description.append("Auto Sostitutiva : ").append(counterpartyUpdateCommand.getReplacementCar()?"Si":"No").append("<br>");
                }else if (!counterpartyUpdateCommand.getReplacementCar().equals(counterparty.getReplacementCar())){
                    if (description.length() > 0)
                        description.append("<br>").append("Auto Sostitutiva : ").append(counterpartyUpdateCommand.getReplacementCar()?"Si":"No").append("<br>");
                    else
                        description.append("Auto Sostitutiva : ").append(counterpartyUpdateCommand.getReplacementCar()?"Si":"No").append("<br>");
                }
            }

            if (counterpartyUpdateCommand.getReplacementPlate()!=null&&!counterpartyUpdateCommand.getReplacementPlate().isEmpty()){
                if (counterparty.getReplacementPlate()==null||counterparty.getReplacementPlate().isEmpty()) {
                    if (description.length() > 0)
                        description.append("<br>").append("Targa Auto Sostitutiva : ").append(counterpartyUpdateCommand.getReplacementPlate()).append("<br>");
                    else
                        description.append("Targa Auto Sostitutiva : ").append(counterpartyUpdateCommand.getReplacementPlate()).append("<br>");
                }else if (!counterpartyUpdateCommand.getReplacementPlate().equalsIgnoreCase(counterparty.getReplacementPlate())){
                    if (description.length() > 0)
                        description.append("<br>").append("Targa Auto Sostitutiva : ").append(counterpartyUpdateCommand.getReplacementPlate()).append("<br>");
                    else
                        description.append("Targa Auto Sostitutiva : ").append(counterpartyUpdateCommand.getReplacementPlate()).append("<br>");
                }
            }

        }
        /***********************************************************
         *   Fine Verifica tipo aggiornamento soggetto a log Repair
         ***********************************************************/

        String plate = null;
        if(claimsEntity.getDamaged() != null && claimsEntity.getDamaged().getVehicle() != null && claimsEntity.getDamaged().getVehicle().getLicensePlate() != null)
            plate = claimsEntity.getDamaged().getVehicle().getLicensePlate();
        if (counterpartyUpdateCommand.getVehicle() != null) {
            if(counterpartyUpdateCommand.getVehicle().getLicensePlate() != null){
                if(plate != null){
                    if(counterpartyUpdateCommand.getVehicle().getLicensePlate().equals(plate))
                        throw new BadRequestException(MessageCode.CLAIMS_1041);
                }
            }
        }
        counterparty.setVehicle(VehicleAdapter.adptVehicleRequestToVehicle(counterpartyUpdateCommand.getVehicle()));
        counterparty.setType(counterpartyUpdateCommand.getType());
        counterparty.setRepairProcedure(counterpartyUpdateCommand.getRepairProcedure());
        counterparty.setUserUpdate(counterpartyUpdateCommand.getUserIdUpdate());
        if(counterparty.getAssignedTo() == null && counterpartyUpdateCommand.getAssignedTo() != null){
            counterparty.setAssignedTo(AssigneeRepairAdapter.adptFromAssignationRepairRequestToAssignationRepair(counterpartyUpdateCommand.getAssignedTo()));
            counterparty.setAssignedAt(DateUtil.getNowInstant());
        } else {
            counterparty.setAssignedTo(AssigneeRepairAdapter.adptFromAssignationRepairRequestToAssignationRepair(counterpartyUpdateCommand.getAssignedTo()));
            counterparty.setAssignedAt(DateUtil.convertIS08601StringToUTCInstant(counterpartyUpdateCommand.getAssignedAt()));
        }
        counterparty.setInsured(InsuredAdapter.adptInsuredRequestToInsured(counterpartyUpdateCommand.getInsured()));
        counterparty.setInsuranceCompany(insuranceCompanyCounterpartyAdapter.adptFromCompanyCounterpartyRequestToCompanyCounterparty(counterpartyUpdateCommand.getInsuranceCompany()));
        counterparty.setDriver(DriverAdapter.adptDriverRequestToDriver(counterpartyUpdateCommand.getDriver()));
        counterparty.setCaiSigned(counterpartyUpdateCommand.getCaiSigned());
        counterparty.setImpactPoint(ImpactPointAdapter.adptImpactPointRequestToImpactPoint(counterpartyUpdateCommand.getImpactPoint()));
        counterparty.setResponsible(counterpartyUpdateCommand.getResponsible());
        counterparty.setEligibility(counterpartyUpdateCommand.getEligibility());
        counterparty.setDescription(counterpartyUpdateCommand.getDescription());
        counterparty.setLastContact(lastContactAdapter.adtpFromLastContactListRequestToLastContactListEntity(counterpartyUpdateCommand.getLastContact()));
        if(counterpartyUpdateCommand.getManager() != null)
            counterparty.setManager(managerService.getManager(counterpartyUpdateCommand.getManager().getId()));
        counterparty.setCanalization(CanalizationAdapter.adptFromCanalizationRequestToCanalization(counterpartyUpdateCommand.getCanalization()));
        counterparty.setPolicyNumber(counterpartyUpdateCommand.getPolicyNumber());
        if(counterpartyUpdateCommand.getPolicyBeginningValidity() != null){
            String beginningValidity = counterpartyUpdateCommand.getPolicyBeginningValidity();
            if(!beginningValidity.contains("T"))
                beginningValidity+="T00:00:00Z";
            Instant beginningValidityDate = DateUtil.convertIS08601StringToUTCInstant(beginningValidity);
            counterparty.setPolicyBeginningValidity(beginningValidityDate);
        } else counterparty.setPolicyBeginningValidity(null);
        if(counterpartyUpdateCommand.getPolicyEndValidity() != null){
            String endValidity = counterpartyUpdateCommand.getPolicyEndValidity();
            if(!endValidity.contains("T"))
                endValidity+="T00:00:00Z";
            Instant endValidityDate = DateUtil.convertIS08601StringToUTCInstant(endValidity);
            counterparty.setPolicyEndValidity(endValidityDate);
        } else counterparty.setPolicyEndValidity(null);
        counterparty.setManagementType(counterpartyUpdateCommand.getManagementType());
        counterparty.setAskedForDamages(counterpartyUpdateCommand.getAskedForDamages());
        counterparty.setLegalOrConsultant(counterpartyUpdateCommand.getLegalOrConsultant());
        counterparty.setInstantRequestDamages(DateUtil.convertIS08601StringToUTCInstant(counterpartyUpdateCommand.getDateRequestDamages()));
        counterparty.setExpirationInstant(DateUtil.convertIS08601StringToUTCInstant(counterpartyUpdateCommand.getExpirationDate()));
        counterparty.setLegal(counterpartyUpdateCommand.getLegal());
        counterparty.setEmailLegal(counterpartyUpdateCommand.getEmailLegal());
        counterparty.setUpdateAt(DateUtil.getNowInstant());
        counterparty.setReplacementCar(counterpartyUpdateCommand.getReplacementCar());
        counterparty.setReplacementPlate(counterpartyUpdateCommand.getReplacementPlate());
        if(counterparty.getIsReadMsa() != null && counterparty.getIsReadMsa()){
            counterparty.setIsReadMsa(false);
        }
        counterparty.setIsCompleteDocumentation(counterpartyUpdateCommand.getCompleteDocumentation());

        /************************ LOG REPAIR ********************************************************************/
        if (counterparty.getEligibility()) {
            HistoricalCounterparty historicalCounterparty = new HistoricalCounterparty(counterparty.getRepairStatus(), counterparty.getRepairStatus(), smallDescription, DateUtil.getNowDate(), counterpartyUpdateCommand.getUserIdUpdate(), counterpartyUpdateCommand.getUserFirstNameUpdate() + " " + counterpartyUpdateCommand.getUserLastNameUpdate(), description.toString(), com.doing.nemo.claims.entity.enumerated.EventTypeEnum.STATUS_VARIATION_REPAIR);
            if (counterparty.getHistoricals() == null) {
                counterparty.setHistoricals(new ArrayList<>());
            }
            List<HistoricalCounterparty> historicalCounterparties = counterparty.getHistoricals();
            historicalCounterparties.add(historicalCounterparty);
            counterparty.setHistoricals(historicalCounterparties);
        }
        /************************ LOG REPAIR ********************************************************************/

        counterpartyRepository.save(counterparty);

        CounterpartyEntity counterpartyOld = CounterpartyAdapter.adptCounterpartyNewToCounterpartyOld(counterparty);
        dwhClaimsService.sendMessage(counterpartyOld, EventTypeEnum.UPDATED);

        if(counterparty.getClaims()!=null){
            ClaimsNewEntity claimsNewEntity = counterparty.getClaims();
            Instant updatedAt = DateUtil.getNowInstant();
            claimsEntity.setUpdateAt(updatedAt);
            claimsNewEntity.setUpdateAt(updatedAt);
            if(claimsNewEntity.getCounterparts() != null) {
                CounterpartyNewEntity toFind =null;
                for(CounterpartyNewEntity counterpartyCurrent : claimsNewEntity.getCounterparts()){
                    if(counterpartyCurrent.getCounterpartyId().equalsIgnoreCase(counterparty.getCounterpartyId())){
                        toFind = counterpartyCurrent;
                    }
                }
                claimsNewEntity.getCounterparts().remove(toFind);
                claimsNewEntity.getCounterparts().add(counterparty);
            }
            claimsRepository.save(claimsNewEntity);
            if(dwhCall){
                dwhClaimsService.sendMessage(claimsEntity, com.doing.nemo.dwh.client.payload.request.EventTypeEnum.UPDATED);
            }
        }

    }
}
