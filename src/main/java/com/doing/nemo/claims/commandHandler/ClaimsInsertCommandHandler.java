package com.doing.nemo.claims.commandHandler;

import com.doing.nemo.claims.adapter.*;
import com.doing.nemo.claims.command.ClaimsInsertCommand;
import com.doing.nemo.claims.common.commandBus.Command;
import com.doing.nemo.claims.common.commandBus.CommandHandler;
import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.CounterpartyRequest;
import com.doing.nemo.claims.controller.payload.request.EmailTemplateMessagingRequestV1;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.ClaimsNewEntity;
import com.doing.nemo.claims.entity.CounterpartyEntity;
import com.doing.nemo.claims.entity.CounterpartyNewEntity;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsFlowEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.enumerated.EventTypeEnum;
import com.doing.nemo.claims.entity.jsonb.Historical;
import com.doing.nemo.claims.entity.jsonb.MetadataClaim;
import com.doing.nemo.claims.entity.jsonb.complaint.Complaint;
import com.doing.nemo.claims.entity.jsonb.complaint.jsonbComplaint.jsonbDataAccidents.DataAccident;
import com.doing.nemo.claims.entity.jsonb.damaged.Damaged;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.Customer;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.FleetManager;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.InsuranceCompany;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbAntiTheftService.AntiTheftRequest;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbAntiTheftService.AntiTheftService;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbAntiTheftService.Registry;
import com.doing.nemo.claims.entity.settings.AntiTheftServiceEntity;
import com.doing.nemo.claims.entity.settings.ContractTypeEntity;
import com.doing.nemo.claims.entity.settings.PersonalDataEntity;
import com.doing.nemo.claims.entity.settings.RecoverabilityEntity;
import com.doing.nemo.claims.repository.*;
import com.doing.nemo.claims.service.*;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.Instant;
import java.util.*;
//REFACTOR

@Component
public class ClaimsInsertCommandHandler implements CommandHandler {

    private static Logger LOGGER = LoggerFactory.getLogger(ClaimsInsertCommandHandler.class);
    private static Set<DataAccidentTypeAccidentEnum> activeSet = new HashSet<DataAccidentTypeAccidentEnum>() {{
        add(DataAccidentTypeAccidentEnum.RC_ATTIVA);
        add(DataAccidentTypeAccidentEnum.RC_CONCORSUALE);
        add(DataAccidentTypeAccidentEnum.CARD_ATTIVA_FIRMA_SINGOLA);
        add(DataAccidentTypeAccidentEnum.CARD_CONCORSUALE_FIRMA_SINGOLA);
        add(DataAccidentTypeAccidentEnum.CARD_ATTIVA_DOPPIA_FIRMA);
        add(DataAccidentTypeAccidentEnum.CARD_CONCORSUALE_DOPPIA_FIRMA);
    }};

    @Autowired
    private ContractService contractService;
    @Autowired
    private CounterpartyAdapter counterpartyAdapter;
    @Autowired
    private ClaimsService claimsService;
    @Autowired
    private AntiTheftServiceRepository antiTheftServiceRepository;
    @Autowired
    private MessagingService messagingService;
    @Autowired
    private RecoverabilityService recoverabilityService;
    @Autowired
    private IncidentService incidentService;

    @Autowired
    private DwhClaimsService dwhClaimsService;
    @Autowired
    private GoLiveStrategyService goLiveStrategyService;

    @Autowired
    private PersonalDataRepository personalDataRepository;

    @Value("${dwh.call}")
    private Boolean dwhCall;

    @Autowired
    private CacheService cacheService;

    @Autowired
    private AntiTheftRequestService antiTheftRequestService;

    @Autowired
    private ClaimsNewRepository claimsNewRepository;

    @Autowired
    private ConverterClaimsService converterClaimsService;

    @Autowired
    private ExternalCommunicationService externalCommunicationService;

    private static final String CACHENAME = "claims.";
    private static final String STATSkEY = "claimsStats::";
    private static final String STATSEVIDENCE = "evidenceStats::";
    private static final String STATSTHEFT = "theftStats::";
    private static final String STATSSTATUS = "statusStats::";
    private static final String COUNTERPARTYKEY = "counterpartyStats::";
    private static final String STATSkEY2 = "claimsV2Stats::";
    private static final String STATSEVIDENCECONTINUATION = "evidenceContinuationStats::";

    @Override
    public void handle(Command command) {

        ClaimsInsertCommand claimsInsertCommand = (ClaimsInsertCommand) command;
        if (claimsInsertCommand.getComplaint().getDataAccident().getTypeAccident() == null && !claimsInsertCommand.getWithCounterparty()
        ) {
            LOGGER.debug(MessageCode.CLAIMS_1068.value());
            throw new BadRequestException(MessageCode.CLAIMS_1068);
        }


        ClaimsEntity claimsEntity = new ClaimsEntity();

        claimsEntity.setId(claimsInsertCommand.getClaimsId());

        String codAntiTheftService = "";


        Damaged damaged = DamagedAdapter.adptDamagedRequestInsertToDamaged(claimsInsertCommand.getDamaged());
        Complaint complaintClaims = ComplaintAdapter.adptComplaintRequestToComplaintUpdate(claimsInsertCommand.getComplaint(), null);
        if (damaged != null) {
            InsuranceCompany insuranceCompany = damaged.getInsuranceCompany();
            Customer customer = damaged.getCustomer();
            if (complaintClaims != null) {
                DataAccident dataAccident = complaintClaims.getDataAccident();
                if (dataAccident != null) {
                    Date date = dataAccident.getDateAccident();
                    if (!claimsService.checkPolicy(insuranceCompany, date, customer)) {
                        LOGGER.debug(MessageCode.CLAIMS_1054.value());
                        throw new BadRequestException(MessageCode.CLAIMS_1054);
                    }
                }
            }
        }

        claimsEntity.setDamaged(damaged);
        if (damaged != null) {
            if (claimsInsertCommand.getComplaint() != null) {
                if (damaged.getVehicle() != null)
                    claimsInsertCommand.getComplaint().setPlate(damaged.getVehicle().getLicensePlate());

                if (damaged.getCustomer() != null)
                    claimsInsertCommand.getComplaint().setClientId(damaged.getCustomer().getCustomerId());
            }
            Customer customer = damaged.getCustomer();
            if (customer != null) {
                String customerId = customer.getCustomerId();
                if (customerId != null) {
                    PersonalDataEntity customerDb = personalDataRepository.findByCustomerId(customerId);

/******************************************************************************************************/
                    if(customerDb!=null) {
                        //Setto i fleetManager, distinguendo tra quelli importati (passati da front-end) e quelli nel DB (presenti in customerDb)
                        damaged.setFleetManagerList(FleetManagerAdpter.adptFromFleetManagerPersonalDataToFleetManager
                                (customerDb.getFleetManagerPersonalData(), damaged.getFleetManagerList()));

                        customerDb = PersonalDataAdapter.adptFromCustomerToPersonalData(customerDb, customer, damaged.getFleetManagerList());

                        // System.out.println(customerDb.getFleetManagerPersonalData());
                        personalDataRepository.save(customerDb);
                    }else{
                        customerDb = PersonalDataAdapter.adptFromCustomerToPersonalData(customerDb, customer, damaged.getFleetManagerList());
                        //Inserisco nella lista dei damaged i fleetmanager che non sono presenti nei fleet manager di customerdb
                        damaged.setFleetManagerList(PersonalDataAdapter.buildFinalFleetManagerListofDamaged(customerDb.getFleetManagerPersonalData(),damaged.getFleetManagerList()));
                        personalDataRepository.save(customerDb);
                    }

/******************************************************************************************************/



                }
            }
        }


        //claimsEntity.setCounterparts(counterpartyAdapter.adptCounterpartyRequestToCounterparty(claimsInsertCommand.getCounterparty()));
        //mappatura del flusso attraverso il tipo contratto e il tipo sinistro ottenuti dalle tabelle di websin

        claimsEntity.setUserEntity(claimsInsertCommand.getUserId());

        //di defaul è in waiting_for_validation

        if (claimsInsertCommand.getCounterparty() != null && !claimsInsertCommand.getCounterparty().isEmpty()) {
            claimsEntity.setStatus(ClaimsStatusEnum.WAITING_FOR_VALIDATION);
            claimsEntity.setAuthorityLinkable(false);
        } else {
            //flusso senza controparti va in automatico in autogestione
            claimsEntity.setStatus(ClaimsStatusEnum.MANAGED);
            claimsEntity.setAuthorityLinkable(true);
        }

        if (claimsInsertCommand.getCounterparty() != null) {
            String plate = null;
            if (claimsEntity.getDamaged() != null && claimsEntity.getDamaged().getVehicle() != null && claimsEntity.getDamaged().getVehicle().getLicensePlate() != null)
                plate = claimsEntity.getDamaged().getVehicle().getLicensePlate();
            if (plate != null) {
                for (CounterpartyRequest counterpartyRequest : claimsInsertCommand.getCounterparty()) {
                    if (counterpartyRequest.getVehicle() != null && counterpartyRequest.getVehicle().getLicensePlate() != null) {
                        if (counterpartyRequest.getVehicle().getLicensePlate().equals(plate)) {
                            LOGGER.debug(MessageCode.CLAIMS_1041.value());
                            throw new BadRequestException(MessageCode.CLAIMS_1041);
                        }
                    }
                    if (counterpartyRequest.getUserCreate() == null || counterpartyRequest.getUserCreate().equalsIgnoreCase(""))
                        counterpartyRequest.setUserCreate(claimsInsertCommand.getUserId());
                }
            }
        }

        claimsEntity.setNotes(NotesAdapter.adptNotesRequestToNotes(claimsInsertCommand.getNotes()));

        claimsEntity.setCounterparts(counterpartyAdapter.adptCounterpartyRequestToCounterparty(claimsInsertCommand.getCounterparty()));

        //mappatura del flusso attraverso il tipo contratto e il tipo sinistro ottenuti dalle tabelle di websin
        claimsEntity.setComplaint(ComplaintAdapter.adptComplaintRequestToComplaintUpdate(claimsInsertCommand.getComplaint(), null));
        claimsEntity.setDeponentList(DeponentAdapter.adptDeponentRequestToDeponent(claimsInsertCommand.getDeponent()));
        claimsEntity.setWoundedList(WoundedAdapter.adptWoundedRequestToWounded(claimsInsertCommand.getWounded()));
        claimsEntity.setCaiDetails(CaiAdapter.adptCaiToCaiResponse(claimsInsertCommand.getCai()));

        if (claimsInsertCommand.getWithCounterparty() && claimsInsertCommand.getCai() != null &&
                claimsInsertCommand.getCai().getVehicleA() != null && claimsInsertCommand.getCai().getVehicleB() != null &&
                claimsInsertCommand.getCai().getDriverSide() != null
        ) {

            String note = new String();
            note += "VEICOLO A: ";
            note += CaiAdapter.adptCaiConditionToCaiNote(CaiDetailsAdapter.adptCaiDetailsRequestToCaiDetails(claimsInsertCommand.getCai().getVehicleA()));
            note += "\nVEICOLO B: ";
            note += CaiAdapter.adptCaiConditionToCaiNote(CaiDetailsAdapter.adptCaiDetailsRequestToCaiDetails(claimsInsertCommand.getCai().getVehicleB()));

            if (claimsEntity.getDamaged().getImpactPoint() != null)
                claimsEntity.getDamaged().getImpactPoint().setIncidentDescription(note);

            if (claimsInsertCommand.getComplaint() != null &&
                    claimsInsertCommand.getComplaint().getDataAccident() != null &&
                    claimsInsertCommand.getComplaint().getDataAccident().getTypeAccident() == null) {
                if (claimsInsertCommand.getCai().getVehicleA() != null && claimsInsertCommand.getCai().getVehicleB() != null) {

                    Boolean isCaiSignedA = false;
                    Boolean isCaiSignedB = false;
                    String companyDenomination = "";

                    if (claimsInsertCommand.getCai().getDriverSide() == null) {
                        LOGGER.debug(MessageCode.CLAIMS_1067.value());
                        throw new BadRequestException(MessageCode.CLAIMS_1067);
                    }

                    if (claimsInsertCommand.getCai().getDriverSide().equalsIgnoreCase("A")) {

                        if (claimsEntity.getDamaged() != null) {
                            isCaiSignedA = claimsEntity.getDamaged().getCaiSigned();
                            if (claimsEntity.getDamaged().getInsuranceCompany() != null && claimsEntity.getDamaged().getInsuranceCompany().getTpl() != null) {
                                companyDenomination = claimsEntity.getDamaged().getInsuranceCompany().getTpl().getCompany();
                            }
                        }

                        if (claimsEntity.getCounterparts() != null && claimsEntity.getCounterparts().size() > 0) {
                            isCaiSignedB = claimsEntity.getCounterparts().get(0).getCaiSigned();
                        }

                    } else if (claimsInsertCommand.getCai().getDriverSide().equalsIgnoreCase("B")) {

                        if (claimsEntity.getDamaged() != null) {
                            isCaiSignedB = claimsEntity.getDamaged().getCaiSigned();
                            if (claimsEntity.getDamaged().getInsuranceCompany() != null && claimsEntity.getDamaged().getInsuranceCompany().getTpl() != null) {
                                companyDenomination = claimsEntity.getDamaged().getInsuranceCompany().getTpl().getCompany();
                            }
                        }

                        if (claimsEntity.getCounterparts() != null && claimsEntity.getCounterparts().size() > 0) {
                            isCaiSignedA = claimsEntity.getCounterparts().get(0).getCaiSigned();
                        }

                    }


                    if (isCaiSignedA == null) {
                        isCaiSignedA = false;
                    }

                    if (isCaiSignedB == null) {
                        isCaiSignedB = false;
                    }

                    Map<String, Object> caiResult = claimsService.getFlowDetailsByCai(claimsEntity.getCaiDetails(), claimsEntity.getDamaged().getContract().getContractType(), claimsEntity.getDamaged(), claimsEntity.getCounterparts(), isCaiSignedA, isCaiSignedB);

                    claimsEntity.setType((ClaimsFlowEnum) caiResult.get("flow"));
                    DataAccident dataAccident = claimsEntity.getComplaint().getDataAccident();

                    dataAccident.setTypeAccident((DataAccidentTypeAccidentEnum) caiResult.get("type"));
                    Complaint complaint = claimsEntity.getComplaint();
                    complaint.setDataAccident(dataAccident);
                    claimsEntity.setComplaint(complaint);
                }

            }
        } else if (!claimsInsertCommand.getWithCounterparty()) {

            if (claimsInsertCommand.getDamaged() != null && claimsInsertCommand.getDamaged().getImpactPoint() != null)
                claimsEntity.getDamaged().getImpactPoint().setIncidentDescription(claimsInsertCommand.getDamaged().getImpactPoint().getIncidentDescription());
        } else if ((claimsInsertCommand.getCai() == null ||
                (claimsInsertCommand.getCai().getVehicleA() == null && claimsInsertCommand.getCai().getVehicleB() == null &&
                        claimsInsertCommand.getCai().getDriverSide() == null)) && (claimsInsertCommand.getComplaint() != null && claimsInsertCommand.getComplaint().getDataAccident() != null && claimsInsertCommand.getComplaint().getDataAccident().getTypeAccident() == null)) {
            if (claimsService.checkIsCard(claimsEntity.getDamaged(), claimsEntity.getCounterparts()))
                claimsEntity.getComplaint().getDataAccident().setTypeAccident(DataAccidentTypeAccidentEnum.CARD_NON_VERIFICABILE_FIRMA_SINGOLA);
            else
                claimsEntity.getComplaint().getDataAccident().setTypeAccident(DataAccidentTypeAccidentEnum.RC_NON_VERIFICABILE);
        }
        if (claimsEntity.getCounterparts() != null && claimsEntity.getCounterparts().size() == 1) {
            if (activeSet.contains(claimsEntity.getComplaint().getDataAccident().getTypeAccident()))
                claimsEntity.getCounterparts().get(0).setResponsible(true);
        }
        try {
            ContractTypeEntity contractTypeEntity = contractService.getContractType(claimsInsertCommand.getDamaged().getContract().getContractType());

            if (contractTypeEntity == null) {
                LOGGER.debug(MessageCode.CLAIMS_1010.value());
                throw new NotFoundException(MessageCode.CLAIMS_1010);
            }

            if (contractTypeEntity.getFlagWS() != null && !contractTypeEntity.getFlagWS()) {
                LOGGER.debug(MessageCode.CLAIMS_1042.value());
                throw new BadRequestException(MessageCode.CLAIMS_1042);
            } else {
                claimsEntity.setType(contractService.getPersonalRiskFlowType(claimsInsertCommand.getDamaged().getContract().getContractType(),
                        claimsEntity.getComplaint().getDataAccident().getTypeAccident(),
                        claimsInsertCommand.getDamaged().getCustomer().getCustomerId().toString())); /* tipo di flusso */
                if (claimsEntity.getType().equals(ClaimsFlowEnum.NO)) {
                    LOGGER.debug("It's not possible insert a claim with NO flow");
                    throw new BadRequestException("It's not possible insert a claim with NO flow", MessageCode.CLAIMS_1057);
                }

            }
        } catch (BadRequestException e) {
            LOGGER.debug(e.getMessage());
            throw e;

        }
        claimsEntity.setIdSaleforce(claimsInsertCommand.getIdSaleforce());
        claimsEntity.setWithCounterparty(claimsInsertCommand.getWithCounterparty());
        claimsEntity.setPaiComunication(claimsInsertCommand.getCaiComunication());
        claimsEntity.setForced(claimsInsertCommand.getForced());
        claimsEntity.setInEvidence(claimsInsertCommand.getInEvidence());
        claimsEntity.setWithContinuation(false);
        claimsEntity.setPoVariation(claimsInsertCommand.getPoVariation());
        claimsEntity.setCompleteDocumentation(claimsInsertCommand.getCompleteDocumentation());

        if (claimsInsertCommand.getRefund() != null) {
            claimsEntity.setRefund(RefundAdapter.adptFromRefundRequestToRefund(claimsInsertCommand.getRefund()));
        }

        if (claimsInsertCommand.getTheft() != null) {
            claimsEntity.setTheft(TheftClaimsAdapter.adptTheftRequestV1ToTheft(claimsInsertCommand.getTheft()));
            claimsEntity.getTheft().setWithReceptions(false);
        }

        //aggiunta metadati
        MetadataClaim metadataClaim = new MetadataClaim();
        metadataClaim.setMetadata(claimsInsertCommand.getMetadata());
        claimsEntity.setMetadata(metadataClaim);
        Instant nowInstant = DateUtil.getNowInstant();
        claimsEntity.setCreatedAt(nowInstant);
        claimsEntity.setUpdateAt(nowInstant);
        //ClaimsStatusEnum oldStatus = claimsEntity.getStatus();
        claimsEntity = claimsService.checkStatusByFlowExternalAndDraft(claimsEntity);
        claimsEntity.setMigrated(false);

        //controlli strategia go live
        goLiveStrategyService.goLiveStrategyChecksForInsert(claimsEntity.getDamaged().getCustomer().getCustomerId(),claimsEntity.getComplaint().getDataAccident().getTypeAccident());

        //recupero dati recoverability

        if (claimsEntity.getType() != null && claimsEntity.getComplaint() != null && claimsEntity.getComplaint().getDataAccident() != null && claimsEntity.getComplaint().getDataAccident().getTypeAccident() != null) {

            RecoverabilityEntity recoverabilityEntity = recoverabilityService.getRecoverabilityByFlowAndTypeClaim(claimsEntity.getType().getValue().toUpperCase(), claimsEntity.getComplaint().getDataAccident().getTypeAccident());
            LOGGER.info("[RECOVERABILITY] "  + recoverabilityEntity);
            if(recoverabilityEntity != null) {
                claimsEntity.getComplaint().getDataAccident().setRecoverability(recoverabilityEntity.getRecoverability());
                claimsEntity.getComplaint().getDataAccident().setRecoverabilityPercent(recoverabilityEntity.getPercentRecoverability());
            }
        }



        damaged = claimsEntity.getDamaged();
        AntiTheftService antiTheftService = null;
        if (damaged.getAntiTheftService() != null) {

            //recupero localizzatore da tabella setting AntiTheftServiceEntity
            //da capire se avremo mai un localizzatore per il furto e se ci viene passato un tipo
            if (damaged.getAntiTheftService().getRegistryList() != null && !damaged.getAntiTheftService().getRegistryList().isEmpty()) {

                //for(Registry currentRegistry : damaged.getAntiTheftService().getRegistryList()){
                Registry currentRegistry = damaged.getAntiTheftService().getRegistryList().get(0);
                codAntiTheftService = AntiTheftServiceAdapter.adptProviderToAntiTheftServiceEntity(currentRegistry.getCodPack());
                AntiTheftServiceEntity antiTheftServiceEntity = antiTheftServiceRepository.findAntiTheftByCodAntiTheftService(codAntiTheftService);
                antiTheftService = AntiTheftServiceAdapter.adptAntiTheftServiceEntityToAntiTheftService(antiTheftServiceEntity);
                if(antiTheftService != null) {
                    antiTheftService.setRegistryList(damaged.getAntiTheftService().getRegistryList());
                    claimsEntity.getDamaged().setAntiTheftService(antiTheftService);
                }

            }
        }




        //funzione di conversione

        ClaimsNewEntity claimsNewEntity = converterClaimsService.convertOldEntityToNewClaimsEntity(claimsEntity);
        claimsNewRepository.save(claimsNewEntity);


        //Per poter recuperare il practice id abbiamo bisogno di salvare prima la pratica è poi recuperarla, in tal modo gli viene assegnato un practice_id
        Optional<ClaimsNewEntity> claimsNewEntityOptional = claimsNewRepository.findById(claimsEntity.getId());

        if(!claimsNewEntityOptional.isPresent()){
            //lancia errore
        }

        claimsNewEntity = claimsNewEntityOptional.get();

        //settiamo il practice id nel tipo vecchio dell'entità per poter utilizzare la vecchia funzuone di getTemplateEmail
        claimsEntity.setPracticeId(claimsNewEntity.getPracticeId());


        List<EmailTemplateMessagingRequestV1> emailTemplateList = MessagingAdapter.adptEmailTemplateMessagingRequestToEmailTemplateMessagingResponseList(messagingService.getMailTemplateByTypeEvent(EventTypeEnum.COMPLAINT_INSERTION, claimsEntity));
        String description = messagingService.sendMailAndCreateLogs(emailTemplateList, null, claimsNewEntity.getCreatedAt());
        Historical historical = new Historical(EventTypeEnum.COMPLAINT_INSERTION, claimsEntity.getStatus(), claimsEntity.getStatus(), claimsEntity.getComplaint().getDataAccident().getTypeAccident(), claimsEntity.getComplaint().getDataAccident().getTypeAccident(), DateUtil.getNowDate(), claimsEntity.getUserId(), claimsInsertCommand.getUserName(), claimsEntity.getType().getValue(), claimsEntity.getType().getValue(), description);
        //aggiungiamo sia alla vecchia entità che alla nuova
        claimsEntity.addHistorical(historical);
        claimsNewEntity.addHistorical(historical);

        claimsNewEntity = claimsNewRepository.save(claimsNewEntity);


        try{
            if(dwhCall){
                dwhClaimsService.sendMessage(claimsEntity, com.doing.nemo.dwh.client.payload.request.EventTypeEnum.CREATED);
                if(claimsNewEntity.getCounterparts() != null) {
                    for (CounterpartyNewEntity newCounterparty : claimsNewEntity.getCounterparts()) {
                        CounterpartyEntity counterparty = CounterpartyAdapter.adptCounterpartyNewToCounterpartyOld(newCounterparty);
                        dwhClaimsService.sendMessage(counterparty, com.doing.nemo.dwh.client.payload.request.EventTypeEnum.CREATED);
                    }
                }
            }
        } catch (Exception e){
            LOGGER.debug(e.getMessage());
        }

        try {
            //intervento 6 - R
            externalCommunicationService.insertIncidentAsync(claimsInsertCommand.getClaimsId(), null);
        } catch (IOException e) {
            LOGGER.debug(MessageCode.CLAIMS_1112.value());
            throw new BadRequestException(MessageCode.CLAIMS_1112, e);
        }

        //claimsRepository.save(claimsEntity);
        claimsNewEntity = claimsNewRepository.getOne(claimsInsertCommand.getClaimsId());
        cacheService.deleteClaimsStats(CACHENAME, STATSkEY);
        cacheService.deleteClaimsStats(CACHENAME, STATSEVIDENCE);

        cacheService.deleteClaimsStats(CACHENAME, STATSEVIDENCECONTINUATION);
        cacheService.deleteClaimsStats(CACHENAME, STATSkEY2);
        if (claimsEntity.getStatus().equals(ClaimsStatusEnum.WAITING_FOR_VALIDATION) || claimsEntity.getStatus().equals(ClaimsStatusEnum.TO_ENTRUST)) {
            cacheService.deleteClaimsStats(CACHENAME, STATSSTATUS);
        }


        if (antiTheftService != null) {
            // creazione della riga di storico di octo/texa
            String idAntiTheftRequest = UUID.randomUUID().toString();
            antiTheftRequestService.insertAntiTheftRequest(idAntiTheftRequest, claimsNewEntity, antiTheftService.getProviderType(), "1");
            claimsService.callToOctoAsync(claimsNewEntity, claimsNewEntity.getStatus(), new AntiTheftRequest(), claimsInsertCommand.getUserId(), claimsInsertCommand.getUserName(), idAntiTheftRequest);


        }
        //}*/


    }
}
