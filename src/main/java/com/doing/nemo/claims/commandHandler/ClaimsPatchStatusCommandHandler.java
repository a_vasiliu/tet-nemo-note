package com.doing.nemo.claims.commandHandler;

import com.doing.nemo.claims.adapter.IncidentAdapter;
import com.doing.nemo.claims.adapter.KafkaAdapter;
import com.doing.nemo.claims.adapter.MessagingAdapter;
import com.doing.nemo.claims.command.ClaimsPatchStatusCommand;
import com.doing.nemo.claims.common.commandBus.Command;
import com.doing.nemo.claims.common.commandBus.CommandHandler;
import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.EmailTemplateMessagingRequestV1;
import com.doing.nemo.claims.controller.payload.request.IncidentRequestV1;
import com.doing.nemo.claims.dto.KafkaEventDTO;
import com.doing.nemo.claims.entity.AntiTheftRequestEntity;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.ClaimsNewEntity;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.enumerated.EventTypeEnum;
import com.doing.nemo.claims.entity.enumerated.KafkaEventTypeEnum;
import com.doing.nemo.claims.entity.jsonb.Historical;
import com.doing.nemo.claims.entity.jsonb.forms.Forms;
import com.doing.nemo.claims.entity.jsonb.forms.jsonbForms.Attachment;
import com.doing.nemo.claims.entity.settings.RecoverabilityEntity;
import com.doing.nemo.claims.repository.ClaimsNewRepository;
import com.doing.nemo.claims.service.*;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.*;


@Component
public class ClaimsPatchStatusCommandHandler implements CommandHandler {
    //REFACTOR

    private static final Logger LOGGER = LoggerFactory.getLogger(ClaimsPatchStatusCommandHandler.class);

    @Autowired
    private ClaimsNewRepository claimsNewRepository;

    @Autowired
    private ConverterClaimsService converterClaimsService;

    @Autowired
    private MessagingService messagingService;

    @Autowired
    private ClaimsService claimsService;

    @Autowired
    private CounterpartyService counterpartyService;

    @Autowired
    private RecoverabilityService recoverabilityService;

    @Autowired
    private IncidentService incidentService;

    @Autowired
    private IncidentAdapter incidentAdapter;

    @Autowired
    private DwhClaimsService dwhClaimsService;

    @Autowired(required = false)
    private NemoEventProducerService nemoEventProducerService;

    @Value("${dwh.call}")
    private Boolean dwhCall;

    @Value("${kafka.topic}")
    private String kafkaTopic;

    @Autowired
    private CacheService cacheService;

    @Autowired
    private ExternalCommunicationService externalCommunicationService;

    private static final String CACHENAME = "claims.";
    private static final String STATSkEY = "claimsStats::";
    private static final String STATSEVIDENCE = "evidenceStats::";
    private static final String STATSTHEFT = "theftStats::";
    private static final String STATSSTATUS = "statusStats::";
    private static final String COUNTERPARTYKEY = "counterpartyStats::";
    private static final String STATSkEY2 = "claimsV2Stats::";
    private static final String STATSEVIDENCECONTINUATION = "evidenceContinuationStats::";

    private Set<ClaimsStatusEnum> theftStatus = new HashSet<ClaimsStatusEnum>() {{
        add(ClaimsStatusEnum.DEMOLITION);
        add(ClaimsStatusEnum.WAIT_LOST_POSSESSION);
        add(ClaimsStatusEnum.LOST_POSSESSION);
        add(ClaimsStatusEnum.WAIT_TO_RETURN_POSSESSION);
        add(ClaimsStatusEnum.RETURN_TO_POSSESSION);
        add(ClaimsStatusEnum.TO_REREGISTER);
        add(ClaimsStatusEnum.TO_WORK);
        add(ClaimsStatusEnum.BOOK_DUPLICATION);
        add(ClaimsStatusEnum.STAMP);
        add(ClaimsStatusEnum.DEMOLITION);
        add(ClaimsStatusEnum.RECEPTIONS_TO_BE_CONFIRMED);
    }};


    @Override
    public void handle(Command command) throws IOException {

        ClaimsPatchStatusCommand claimsPatchStatusCommand = (ClaimsPatchStatusCommand) command;

        Optional<ClaimsNewEntity> claimsNewEntityOptional = claimsNewRepository.findById(claimsPatchStatusCommand.getId());
        if (!claimsNewEntityOptional.isPresent()) {
            LOGGER.debug("Claims with UUID " + claimsPatchStatusCommand.getId() + " not found ");
            throw new NotFoundException("Claims with UUID " + claimsPatchStatusCommand.getId() + " not found ", MessageCode.CLAIMS_1010);
        }
        ClaimsNewEntity claimsNewEntity = claimsNewEntityOptional.get();
        ClaimsEntity claimsEntity = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntity);
        ClaimsEntity oldClaims = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntity);

        ClaimsStatusEnum oldStatus = null;
        IncidentRequestV1 oldIncident = incidentAdapter.adptFromClaimsEntityToIncidentRequest(claimsEntity,null,false,false);
        ClaimsStatusEnum nextStatus = claimsPatchStatusCommand.getNextStatus();

        EventTypeEnum nextEvent = claimsPatchStatusCommand.getClaimsPatchStatusRequestV1().getEventType();
        if (nextStatus.equals(ClaimsStatusEnum.CLOSED_TOTAL_REFUND) || nextStatus.equals(ClaimsStatusEnum.CLOSED_PARTIAL_REFUND) || nextStatus.equals(ClaimsStatusEnum.CLOSED)) {
            claimsService.checkClosureByTypeAndStatus(claimsEntity, nextStatus,nextEvent);

        }

        if (claimsEntity.getStatus().equals(ClaimsStatusEnum.DRAFT)) {

            claimsEntity = claimsService.checkInfoClaimsForWaitingForValidation(claimsEntity, claimsPatchStatusCommand.getUserName());
            claimsEntity = claimsService.checkStatusByFlowExternalAndDraft(claimsEntity);
            nextStatus = claimsEntity.getStatus();
            oldStatus = ClaimsStatusEnum.DRAFT;

            if (claimsEntity.getType() != null && claimsEntity.getComplaint() != null && claimsEntity.getComplaint().getDataAccident() != null && claimsEntity.getComplaint().getDataAccident().getTypeAccident() != null) {
                RecoverabilityEntity recoverabilityEntity = recoverabilityService.getRecoverabilityByFlowAndTypeClaim(claimsEntity.getType().getValue().toUpperCase(), claimsEntity.getComplaint().getDataAccident().getTypeAccident());
                LOGGER.info("[RECOVERABILITY] "  + recoverabilityEntity);
                if(recoverabilityEntity != null) {
                    claimsEntity.getComplaint().getDataAccident().setRecoverability(recoverabilityEntity.getRecoverability());
                    claimsEntity.getComplaint().getDataAccident().setRecoverabilityPercent(recoverabilityEntity.getPercentRecoverability());
                }
            }
        }

        if (claimsPatchStatusCommand.getNextStatus().equals(ClaimsStatusEnum.VALIDATED) || claimsPatchStatusCommand.getNextStatus().equals(ClaimsStatusEnum.INCOMPLETE)) {
            if ((!claimsEntity.getStatus().equals(ClaimsStatusEnum.INCOMPLETE) &&
                    !claimsEntity.getStatus().equals(ClaimsStatusEnum.WAITING_FOR_VALIDATION)) && !claimsPatchStatusCommand.getNextStatus().equals(ClaimsStatusEnum.INCOMPLETE)
            ) {
                LOGGER.debug(MessageCode.CLAIMS_1037.value());
                throw new BadRequestException(MessageCode.CLAIMS_1037);
            }
                Forms forms = claimsEntity.getForms();
                if (forms != null) {
                    List<Attachment> attachments = forms.getAttachment();
                    if (attachments != null) {
                        for (Attachment attachment : attachments) {
                            attachment.setValidate(true);
                        }
                    }
                }
            claimsEntity.setWithContinuation(false);

            claimsEntity.setAuthorityLinkable(true);
            if(claimsPatchStatusCommand.getNextStatus().equals(ClaimsStatusEnum.INCOMPLETE)){
                nextStatus = ClaimsStatusEnum.INCOMPLETE;
            }else {
                nextStatus = claimsService.checkStatusByFlowValidated(claimsEntity);
            }
        }

        if (claimsPatchStatusCommand.getNextStatus().equals(ClaimsStatusEnum.VALIDATED) || claimsPatchStatusCommand.getNextStatus().equals(ClaimsStatusEnum.INCOMPLETE)) {
            if (claimsEntity.getAntiTheftRequestEntities() != null) {
                for (AntiTheftRequestEntity current : claimsEntity.getAntiTheftRequestEntities()) {
                    Attachment attachment = current.getCrashReport();
                    if (attachment != null && !attachment.getValidate()) {
                        attachment.setValidate(true);
                    }
                }
            }
        }

        List<ClaimsStatusEnum> claimsUnlinkableAuthorityStatusList = new ArrayList<>();
        claimsUnlinkableAuthorityStatusList.add(ClaimsStatusEnum.DRAFT);
        claimsUnlinkableAuthorityStatusList.add(ClaimsStatusEnum.WAITING_FOR_VALIDATION);
        claimsUnlinkableAuthorityStatusList.add(ClaimsStatusEnum.INCOMPLETE);
        claimsUnlinkableAuthorityStatusList.add(ClaimsStatusEnum.REJECTED);
        claimsUnlinkableAuthorityStatusList.add(ClaimsStatusEnum.DELETED);
        claimsUnlinkableAuthorityStatusList.add(ClaimsStatusEnum.CLOSED);

        if (!claimsUnlinkableAuthorityStatusList.contains(claimsPatchStatusCommand.getNextStatus()))
            claimsEntity.setAuthorityLinkable(true);
        else
            claimsEntity.setAuthorityLinkable(false);

        String description = "";


        if (claimsPatchStatusCommand.getClaimsPatchStatusRequestV1() != null && claimsPatchStatusCommand.getClaimsPatchStatusRequestV1().getTemplateList() != null) {
            boolean isIncomplete = ClaimsStatusEnum.INCOMPLETE.equals(claimsPatchStatusCommand.getNextStatus());
            List<EmailTemplateMessagingRequestV1> emailTemplateMessaging = messagingService.splitEmailIfContainsUrl(claimsPatchStatusCommand.getClaimsPatchStatusRequestV1().getTemplateList(), claimsPatchStatusCommand.getId(), isIncomplete);
            if(emailTemplateMessaging != null && !emailTemplateMessaging.isEmpty()) {
                description = messagingService.sendMailAndCreateLogs(emailTemplateMessaging, claimsPatchStatusCommand.getClaimsPatchStatusRequestV1().getMotivation(), claimsEntity.getCreatedAt());
            } else {
                description = messagingService.sendMailAndCreateLogs(claimsPatchStatusCommand.getClaimsPatchStatusRequestV1().getTemplateList(), claimsPatchStatusCommand.getClaimsPatchStatusRequestV1().getMotivation(), claimsEntity.getCreatedAt());
            }
        }


        Historical historical;


        if (oldStatus != null) {
            List<EmailTemplateMessagingRequestV1> emailTemplateList = MessagingAdapter.adptEmailTemplateMessagingRequestToEmailTemplateMessagingResponseList(messagingService.getMailTemplateByTypeEvent(EventTypeEnum.COMPLAINT_INSERTION, claimsEntity));
            description = messagingService.sendMailAndCreateLogs(emailTemplateList, null, claimsEntity.getCreatedAt());
            historical = new Historical(EventTypeEnum.COMPLAINT_INSERTION, oldStatus, claimsEntity.getStatus(), claimsEntity.getComplaint().getDataAccident().getTypeAccident(), claimsEntity.getComplaint().getDataAccident().getTypeAccident(), DateUtil.getNowDate(), claimsEntity.getUserId(), claimsPatchStatusCommand.getUserName(), claimsEntity.getType().getValue(), claimsEntity.getType().getValue(), description);
            //claimsEntity.addHistorical(historical);
        } else {
            if (claimsPatchStatusCommand.getClaimsPatchStatusRequestV1() != null)
                historical = new Historical(claimsPatchStatusCommand.getClaimsPatchStatusRequestV1().getEventType(), claimsEntity.getStatus(), nextStatus, claimsEntity.getComplaint().getDataAccident().getTypeAccident(), claimsEntity.getComplaint().getDataAccident().getTypeAccident(), DateUtil.getNowDate(), claimsPatchStatusCommand.getUserId(), claimsPatchStatusCommand.getUserName(), claimsEntity.getType().getValue(), claimsEntity.getType().getValue(), description);
            else
                historical = new Historical(null, claimsEntity.getStatus(), nextStatus, claimsEntity.getComplaint().getDataAccident().getTypeAccident(), claimsEntity.getComplaint().getDataAccident().getTypeAccident(), DateUtil.getNowDate(), claimsPatchStatusCommand.getUserId(), claimsPatchStatusCommand.getUserName(), claimsEntity.getType().getValue(), claimsEntity.getType().getValue(), description);
        }

        if (!claimsEntity.getStatus().equals(claimsPatchStatusCommand.getNextStatus())) {
            claimsEntity.setInEvidence(false);
        }

        claimsEntity.addHistorical(historical);
        claimsEntity.setStatus(nextStatus);


        claimsEntity.setMotivation(claimsPatchStatusCommand.getClaimsPatchStatusRequestV1().getMotivation());
        claimsEntity.setUpdateAt(DateUtil.getNowInstant());

        if (!claimsPatchStatusCommand.getNextStatus().equals(ClaimsStatusEnum.INCOMPLETE) && !claimsPatchStatusCommand.getNextStatus().equals(ClaimsStatusEnum.WAITING_FOR_VALIDATION)) {
            counterpartyService.EligibilityCriterion(claimsEntity, claimsPatchStatusCommand.getUserId(),claimsPatchStatusCommand.getUserName());
            if(claimsEntity.getCounterparts()!=null && !claimsEntity.getCounterparts().isEmpty() && claimsEntity.getCounterparts().get(0).getEligibility()!=null && claimsEntity.getCounterparts().get(0).getEligibility()){
                cacheService.deleteClaimsStats(CACHENAME, COUNTERPARTYKEY);
            }
        }


        //salvataggio del nuovo
        claimsNewEntity = converterClaimsService.convertOldEntityToNewClaimsEntity(claimsEntity);

        claimsNewRepository.save(claimsNewEntity);

        if (claimsPatchStatusCommand.getNextStatus().equals(ClaimsStatusEnum.VALIDATED)) {
           claimsEntity = claimsService.checkALDvsALD(claimsEntity);
        }

        if(dwhCall){
            dwhClaimsService.sendMessage(claimsEntity, com.doing.nemo.dwh.client.payload.request.EventTypeEnum.UPDATED);
        }

        if(claimsPatchStatusCommand.getNextStatus().equals(ClaimsStatusEnum.VALIDATED)){

            if(claimsEntity.getComplaint().getDataAccident().getTypeAccident().equals(DataAccidentTypeAccidentEnum.FURTO_TOTALE) &&
                    claimsEntity.getDamaged() != null && claimsEntity.getDamaged().getContract() != null && claimsEntity.getDamaged().getContract().getContractType().equalsIgnoreCase("pol")){
                KafkaEventDTO kafkaEventDTO = KafkaAdapter.adptFromClaimsToKafkaEvent(claimsEntity);
                nemoEventProducerService.produceEvent(kafkaEventDTO, kafkaTopic, KafkaEventTypeEnum.THEFT);
            }
        }

        //Intervento 6 - R
        try {
            externalCommunicationService.modifyIncidentAsync(claimsPatchStatusCommand.getId(),oldStatus, false,oldIncident,false, oldClaims);
        } catch (IOException e) {
            throw new BadRequestException(MessageCode.CLAIMS_1112);
        }

        if(theftStatus.contains(claimsEntity.getStatus())){
            cacheService.deleteClaimsStats(CACHENAME,STATSTHEFT);
        } else {
            cacheService.deleteClaimsStats(CACHENAME, STATSkEY);
            cacheService.deleteClaimsStats(CACHENAME, STATSEVIDENCE);
            cacheService.deleteClaimsStats(CACHENAME, STATSSTATUS);

            cacheService.deleteClaimsStats(CACHENAME, STATSEVIDENCECONTINUATION);
            cacheService.deleteClaimsStats(CACHENAME, STATSkEY2);
        }

    }

}
