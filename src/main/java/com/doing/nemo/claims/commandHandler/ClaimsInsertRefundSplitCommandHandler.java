package com.doing.nemo.claims.commandHandler;


import com.doing.nemo.claims.adapter.IncidentAdapter;
import com.doing.nemo.claims.command.ClaimsInsertRefundSplitCommand;
import com.doing.nemo.claims.common.commandBus.Command;
import com.doing.nemo.claims.common.commandBus.CommandHandler;
import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.IncidentRequestV1;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsFlowEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.doing.nemo.claims.entity.jsonb.refund.Refund;
import com.doing.nemo.claims.entity.jsonb.refund.Split;
import com.doing.nemo.claims.repository.ClaimsRepository;
import com.doing.nemo.claims.service.ClaimsService;
import com.doing.nemo.claims.service.DwhClaimsService;
import com.doing.nemo.claims.service.ExternalCommunicationService;
import com.doing.nemo.claims.service.IncidentService;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.dwh.client.payload.request.EventTypeEnum;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;
import java.util.List;
import java.util.UUID;

@Component
public class ClaimsInsertRefundSplitCommandHandler implements CommandHandler {

    private static Logger LOGGER = LoggerFactory.getLogger(ClaimsInsertRefundSplitCommandHandler.class);
    @Autowired
    private ClaimsRepository claimsRepository;
    @Autowired
    private ClaimsService claimsService;

    @Autowired
    private IncidentService incidentService;

    @Autowired
    private IncidentAdapter incidentAdapter;
    @Autowired
    private DwhClaimsService dwhClaimsService;

    @Autowired
    private ExternalCommunicationService externalCommunicationService;

    @Value("${dwh.call}")
    private Boolean dwhCall;

    @Override
    public void handle(Command command) {

        ClaimsInsertRefundSplitCommand claimsInsertRefundSplitCommand = (ClaimsInsertRefundSplitCommand) command;
        Optional<ClaimsEntity> claimsEntityOptional = claimsRepository.findById(claimsInsertRefundSplitCommand.getClaimsId());
        if (!claimsEntityOptional.isPresent()) {
            LOGGER.debug("Claims with UUID " + claimsInsertRefundSplitCommand.getClaimsId() + " not found ");
            throw new NotFoundException("Claims with UUID " + claimsInsertRefundSplitCommand.getClaimsId() + " not found ", MessageCode.CLAIMS_1010);
        }
        ClaimsEntity claimsEntity = claimsEntityOptional.get();
        ClaimsEntity oldClaims = claimsEntityOptional.get();
        ClaimsStatusEnum statusOld = claimsEntity.getStatus();
        IncidentRequestV1 oldIncident = incidentAdapter.adptFromClaimsEntityToIncidentRequest(claimsEntity,null,false,false);


        if (claimsEntity.getRefund() == null ) {
            claimsEntity.setRefund(new Refund());
        }
        if(claimsEntity.getRefund().getSplitList() == null)
            claimsEntity.getRefund().setSplitList(new ArrayList<>());


        if( claimsInsertRefundSplitCommand.getSplit() != null) {
            Split split = claimsInsertRefundSplitCommand.getSplit();
            split.setId(UUID.randomUUID());
            split.setIssueDate(DateUtil.getNowDate());
            Refund refund = claimsEntity.getRefund();
            List<Split> splits = refund.getSplitList();
            splits.add(split);
            refund.setSplitList(splits);
            refund.setDefinitionDate(split.getIssueDate());
            Double totalLiquidationReceived = 0.0;
            for(Split currentSplit : refund.getSplitList()){
                totalLiquidationReceived += currentSplit.getTotalPaid();
            }
            if(claimsEntity.getType() != null && claimsEntity.getType().equals(ClaimsFlowEnum.FCM) && refund.getPoSum() != null && refund.getTotalLiquidationReceived()!= null){
                refund.setAmountToBeDebited(refund.getPoSum() - refund.getTotalLiquidationReceived());
            }
            refund = claimsService.setRefundFranchiseAmountFcm(claimsEntity,refund);
            refund.setTotalLiquidationReceived(totalLiquidationReceived);
            claimsEntity.setRefund(refund);
            claimsEntity.setUpdateAt(DateUtil.getNowInstant());
            claimsRepository.save(claimsEntity);
            if(dwhCall){
                dwhClaimsService.sendMessage(claimsEntity, EventTypeEnum.UPDATED);
            }
        }

        //Intervento 6 - C
        try {
            externalCommunicationService.modifyIncidentAsync(claimsEntity.getId(),statusOld, false,oldIncident,false, oldClaims);
        } catch (IOException e) {
            throw new BadRequestException(MessageCode.CLAIMS_1112, e);
        }

    }
}
