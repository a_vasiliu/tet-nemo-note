package com.doing.nemo.claims.commandHandler;

import com.doing.nemo.claims.command.ClaimsPatchLockedCommand;
import com.doing.nemo.claims.common.commandBus.Command;
import com.doing.nemo.claims.common.commandBus.CommandHandler;
import com.doing.nemo.claims.entity.ClaimsNewEntity;
import com.doing.nemo.claims.repository.ClaimsNewRepository;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class ClaimsPatchLockedCommandHandler implements CommandHandler {

    //REFACTOR

    private static Logger LOGGER = LoggerFactory.getLogger(ClaimsPatchLockedCommandHandler.class);
    @Autowired
    private ClaimsNewRepository claimsNewRepository;

    @Override
    public void handle(Command command) {

        ClaimsPatchLockedCommand claimsPatchLockedCommand = (ClaimsPatchLockedCommand) command;

        Optional<ClaimsNewEntity> optClaims = claimsNewRepository.findById(claimsPatchLockedCommand.getId());

        if (!optClaims.isPresent()) {
            LOGGER.debug("Claims not found");
            throw new NotFoundException("Claims not found", MessageCode.CLAIMS_1010);
        }

        ClaimsNewEntity claimsEntity = optClaims.get();

        claimsNewRepository.save(claimsEntity);
    }
}
