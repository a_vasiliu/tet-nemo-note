package com.doing.nemo.claims.commandHandler;

import com.doing.nemo.claims.command.ClaimsPatchNoteStatusCommand;
import com.doing.nemo.claims.common.commandBus.Command;
import com.doing.nemo.claims.common.commandBus.CommandHandler;
import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.ClaimsNewEntity;
import com.doing.nemo.claims.entity.NoteEntity;
import com.doing.nemo.claims.entity.jsonb.Notes;
import com.doing.nemo.claims.repository.ClaimsNewRepository;
import com.doing.nemo.claims.repository.ClaimsRepository;
import com.doing.nemo.claims.repository.NoteRepository;
import com.doing.nemo.claims.service.ConverterClaimsService;
import com.doing.nemo.claims.service.DwhClaimsService;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class ClaimsPatchNoteStatusCommandHandler implements CommandHandler {
    //REFACTOR

    private static Logger LOGGER = LoggerFactory.getLogger(ClaimsPatchNoteStatusCommandHandler.class);
    @Autowired
    private ClaimsNewRepository claimsNewRepository;
    @Autowired
    private NoteRepository noteRepository;
    @Autowired
    private DwhClaimsService dwhClaimsService;
    @Autowired
    private ConverterClaimsService converterClaimsService;

    @Value("${dwh.call}")
    private Boolean dwhCall;

    @Override
    public void handle(Command command) {

        ClaimsPatchNoteStatusCommand claimsPatchNoteStatusCommand = (ClaimsPatchNoteStatusCommand) command;

        //recupero della nuova entità
        Optional<ClaimsNewEntity> claimsEntityOptional = claimsNewRepository.findById(claimsPatchNoteStatusCommand.getUuid());
        if (!claimsEntityOptional.isPresent()) {
            LOGGER.debug("Claims with UUID " + claimsPatchNoteStatusCommand.getUuid() + " not found ");
            throw new NotFoundException("Claims with UUID " + claimsPatchNoteStatusCommand.getUuid() + " not found ", MessageCode.CLAIMS_1010);
        }

        ClaimsNewEntity claimsNewEntity =claimsEntityOptional.get();

        Optional<NoteEntity> noteEntityOptional = noteRepository.findById(claimsPatchNoteStatusCommand.getNoteId());
        if(!noteEntityOptional.isPresent()){
            LOGGER.debug("Note not found");
            throw new NotFoundException("Note not found", MessageCode.CLAIMS_1010);
        }

        NoteEntity note = noteEntityOptional.get();

        if (note.getImportant()!= null && note.getImportant())
        {
            note.setImportant(false);
        }
        else
        {
            note.setImportant(true);
        }
        noteRepository.save(note);

        claimsNewEntity.setUpdateAt(DateUtil.getNowInstant());

        claimsNewRepository.save(claimsNewEntity);
        claimsNewEntity = claimsNewRepository.getOne(claimsNewEntity.getId());

        ClaimsEntity claimsEntity = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntity);
        if(dwhCall){
            dwhClaimsService.sendMessage(claimsEntity, com.doing.nemo.dwh.client.payload.request.EventTypeEnum.UPDATED);
        }
    }

}
