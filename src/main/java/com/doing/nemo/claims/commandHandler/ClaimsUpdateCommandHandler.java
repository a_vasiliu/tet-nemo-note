package com.doing.nemo.claims.commandHandler;

import com.doing.nemo.claims.adapter.*;
import com.doing.nemo.claims.command.ClaimsUpdateCommand;
import com.doing.nemo.claims.common.commandBus.Command;
import com.doing.nemo.claims.common.commandBus.CommandHandler;
import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.CounterpartyRequest;
import com.doing.nemo.claims.controller.payload.request.EmailTemplateMessagingRequestV1;
import com.doing.nemo.claims.controller.payload.request.IncidentRequestV1;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.ClaimsNewEntity;
import com.doing.nemo.claims.entity.CounterpartyEntity;
import com.doing.nemo.claims.entity.CounterpartyNewEntity;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsFlowEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.enumerated.EventTypeEnum;
import com.doing.nemo.claims.entity.jsonb.Historical;
import com.doing.nemo.claims.entity.jsonb.complaint.jsonbComplaint.FromCompany;
import com.doing.nemo.claims.entity.jsonb.damaged.Damaged;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.ImpactPoint;
import com.doing.nemo.claims.entity.settings.ContractTypeEntity;
import com.doing.nemo.claims.entity.settings.RecoverabilityEntity;
import com.doing.nemo.claims.repository.ClaimsNewRepository;
import com.doing.nemo.claims.repository.CounterpartyNewRepository;
import com.doing.nemo.claims.service.*;
import com.doing.nemo.claims.service.impl.ClaimsServiceImpl;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.*;

@Component
public class ClaimsUpdateCommandHandler implements CommandHandler {
    //REFACTOR

    private static Logger LOGGER = LoggerFactory.getLogger(ClaimsUpdateCommandHandler.class);

    @Autowired
    private ClaimsNewRepository claimsNewRepository;

    @Autowired
    private ConverterClaimsService converterClaimsService;

    @Autowired
    private CounterpartyNewRepository counterpartyNewRepository;

    @Autowired
    private CounterpartyAdapter counterpartyAdapter;

    @Autowired
    private ClaimsService claimsService;

    @Autowired
    private LockService lockService;

    @Autowired
    private ContractService contractService;

    @Autowired
    private RecoverabilityService recoverabilityService;

    @Autowired
    private MessagingService messagingService;

    @Autowired
    private CounterpartyService counterpartyService;

    @Autowired
    private IncidentService incidentService;

    @Autowired
    private IncidentAdapter incidentAdapter;

    @Autowired
    private PracticeService practiceService;

    @Autowired
    private DwhClaimsService dwhClaimsService;

    @Value("${dwh.call}")
    private Boolean dwhCall;

    @Autowired
    private CacheService cacheService;

    @Autowired
    private ExternalCommunicationService externalCommunicationService;

    private static final String CACHENAME = "claims.";
    private static final String STATSkEY = "claimsStats::";
    private static final String STATSEVIDENCE = "evidenceStats::";
    private static final String STATSTHEFT = "theftStats::";
    private static final String STATSSTATUS = "statusStats::";
    private static final String COUNTERPARTYKEY = "counterpartyStats::";
    private static final String STATSkEY2 = "claimsV2Stats::";
    private static final String STATSEVIDENCECONTINUATION = "evidenceContinuationStats::";

    @Override
//    @Transactional(propagation= Propagation.REQUIRED)
    public void handle(Command command) {

        ClaimsUpdateCommand claimsUpdateCommand = (ClaimsUpdateCommand) command;

        Optional<ClaimsNewEntity> claimsNewEntityOptional = claimsNewRepository.findById(claimsUpdateCommand.getUuid());

        if (!claimsNewEntityOptional.isPresent()) {
            LOGGER.debug("Claims with UUID " + claimsUpdateCommand.getUuid() + " not found ");
            throw new NotFoundException("Claims with UUID " + claimsUpdateCommand.getUuid() + " not found ", MessageCode.CLAIMS_1010);
        }
        ClaimsNewEntity claimsNewEntity = claimsNewEntityOptional.get();
        ClaimsEntity claimsEntity = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntity);
        // da provare, serve per il log di incident per sapere vecchi campi e nuovi campi
        ClaimsEntity oldClaims =  converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntity);


        FromCompany oldFromCompany = new FromCompany();
        oldFromCompany.setNumberSx(claimsEntity.getComplaint() != null && claimsEntity.getComplaint().getFromCompany() != null ? claimsEntity.getComplaint().getFromCompany().getNumberSx() : null );
        ClaimsFlowEnum oldFlow = claimsEntity.getType();
        ClaimsStatusEnum statusOld = claimsEntity.getStatus();
        Boolean oldWithCounterparty = claimsEntity.getWithCounterparty();
        IncidentRequestV1 oldIncident = incidentAdapter.adptFromClaimsEntityToIncidentRequest(claimsEntity,null,false,false);
        Boolean isVoltura = false;
        Boolean isInsuranceChanged = false;
        Damaged damaged = null;
        claimsEntity.setWithCounterparty(claimsUpdateCommand.getWithCounterparty());
        if (!claimsEntity.getStatus().equals(ClaimsStatusEnum.DRAFT)) {
            lockService.checkLock(claimsUpdateCommand.getUserId(), claimsEntity.getId());

            String contractCodeOld = claimsEntity.getDamaged().getContract().getContractType();
            DataAccidentTypeAccidentEnum typeAccidentOld = claimsEntity.getComplaint().getDataAccident().getTypeAccident();


            if (claimsUpdateCommand.getComplaint() != null &&
                    claimsUpdateCommand.getComplaint().getDataAccident().getDateAccident() != null &&
                    !DateUtil.convertIS08601StringToUTCDate(claimsUpdateCommand.getComplaint().getDataAccident().getDateAccident()).equals(claimsEntity.getComplaint().getDataAccident().getDateAccident())
            ) {
                try {

                    damaged = claimsService.getInfoChangeDate(claimsEntity, DateUtil.convertIS08601StringToUTCDate(claimsUpdateCommand.getComplaint().getDataAccident().getDateAccident()));

                    if (!damaged.getCustomer().getCustomerId().equals(claimsEntity.getDamaged().getCustomer().getCustomerId())) {
                        isVoltura = true;
                    }
                    if (claimsService.isInsuranceChanged(damaged, claimsEntity)) {
                        isInsuranceChanged = true;
                    }

                    if (claimsUpdateCommand.getDamaged() != null) {
                        damaged.setImpactPoint(ImpactPointAdapter.adptImpactPointRequestToImpactPoint(claimsUpdateCommand.getDamaged().getImpactPoint()));
                        damaged.setAdditionalCosts(AdditionalCostsAdapter.adptAdditionalCostsRequestToAdditionalCosts(claimsUpdateCommand.getDamaged().getAdditionalCosts()));
                    }
                } catch (IOException e) {
                    LOGGER.debug(e.getMessage(), e);
                }
            } else {



                damaged = DamagedAdapter.adptDamagedRequestToDamaged(claimsUpdateCommand.getDamaged());
            }
            if (!isVoltura)
                claimsEntity.setDamaged(damaged);
            claimsEntity.setComplaint(ComplaintAdapter.adptComplaintRequestToComplaintUpdate(claimsUpdateCommand.getComplaint(), claimsEntity.getComplaint()));

            ContractTypeEntity contractTypeEntity = contractService.getContractType(claimsUpdateCommand.getDamaged().getContract().getContractType());
            if(contractTypeEntity == null) {
                LOGGER.debug(MessageCode.CLAIMS_1042.value());
                throw new NotFoundException(MessageCode.CLAIMS_1010);
            }
            if (!contractTypeEntity.getFlagWS()) {
                LOGGER.debug(MessageCode.CLAIMS_1042.value());
                throw new BadRequestException(MessageCode.CLAIMS_1042);
            } else {

                if (claimsUpdateCommand.getComplaint().getDataAccident().getTypeAccident() == null) {
                    //se il tipo sinistro non è stato inserito recupero flusso di default
                    claimsEntity.setType(contractTypeEntity.getDefaultFlow());

                } else {
                    claimsEntity.setType(contractService.getPersonalRiskFlowType(claimsUpdateCommand.getDamaged().getContract().getContractType(),
                            claimsUpdateCommand.getComplaint().getDataAccident().getTypeAccident(),
                            claimsUpdateCommand.getDamaged().getCustomer().getCustomerId().toString())); /* tipo di flusso */
                }
            }


            String contractCodeNew = claimsEntity.getDamaged().getContract().getContractType();
            DataAccidentTypeAccidentEnum typeAccidentNew = claimsEntity.getComplaint().getDataAccident().getTypeAccident();

            if (!contractCodeOld.equals(contractCodeNew) || !typeAccidentOld.equals(typeAccidentNew)) {
                try {
                    claimsEntity.setStatus(claimsService.getStatusChangeFlow(claimsEntity, oldFlow,oldWithCounterparty)); //stato nuovo
                    //recuperabilità

                    if (claimsEntity.getType() != null && claimsEntity.getComplaint() != null && claimsEntity.getComplaint().getDataAccident() != null && claimsEntity.getComplaint().getDataAccident().getTypeAccident() != null) {
                        RecoverabilityEntity recoverabilityEntity = recoverabilityService.getRecoverabilityByFlowAndTypeClaim(claimsEntity.getType().getValue().toUpperCase(), claimsEntity.getComplaint().getDataAccident().getTypeAccident());
                        LOGGER.info("[RECOVERABILITY] "  + recoverabilityEntity);
                        if (recoverabilityEntity != null) {
                            claimsEntity.getComplaint().getDataAccident().setRecoverability(recoverabilityEntity.getRecoverability());
                            claimsEntity.getComplaint().getDataAccident().setRecoverabilityPercent(recoverabilityEntity.getPercentRecoverability());
                        }
                    }

                } catch (NoSuchMethodException e) {
                    LOGGER.debug(e.getMessage());
                }
            }

            if (!claimsEntity.getComplaint().getDataAccident().getTypeAccident().equals(typeAccidentOld)) {
                String description = "La tipologia sinistro è cambiata da  " + typeAccidentOld.getValue().replaceAll("_", " ") + " a " + claimsEntity.getComplaint().getDataAccident().getTypeAccident().getValue().replaceAll("_", " ");
                Historical historical = new Historical(EventTypeEnum.EDIT_PRACTICE_DATA, statusOld, claimsEntity.getStatus(), typeAccidentOld, claimsEntity.getComplaint().getDataAccident().getTypeAccident(), DateUtil.getNowDate(), claimsUpdateCommand.getUserId(), claimsUpdateCommand.getUserName(), oldFlow.getValue(), claimsEntity.getType().getValue(), description);
                claimsEntity.addHistorical(historical);
                statusOld = claimsEntity.getStatus();
            }

        } else {
            claimsEntity.setDamaged(DamagedAdapter.adptDamagedRequestToDamaged(claimsUpdateCommand.getDamaged()));
            claimsEntity.setComplaint(ComplaintAdapter.adptComplaintRequestToComplaintUpdate(claimsUpdateCommand.getComplaint(), claimsEntity.getComplaint()));
        }

    //controllore se è cambiato from company
        if ((oldFromCompany == null || oldFromCompany.getNumberSx() == null || oldFromCompany.getNumberSx().equals("")) &&
                (claimsEntity.getComplaint().getFromCompany() != null && claimsEntity.getComplaint().getFromCompany().getNumberSx() != null && !claimsEntity.getComplaint().getFromCompany().getNumberSx().equals(""))) {

            //controlla checkAuthority
            ClaimsStatusEnum nextStatus = null;
            if (claimsEntity.getStatus().equals(ClaimsStatusEnum.WAITING_FOR_AUTHORITY) &&  ClaimsServiceImpl.isAuthorityAndSxNumber(claimsEntity)) {

                if (claimsEntity.getType().equals(ClaimsFlowEnum.FNI)) {
                    nextStatus = ClaimsStatusEnum.TO_SEND_TO_CUSTOMER;
                } else {
                    nextStatus = ClaimsStatusEnum.TO_ENTRUST;
                }
                claimsEntity.setStatus(nextStatus);
                LOGGER.debug("[NUM SX]  change status in " + nextStatus);
                String description = "Lo stato è cambiato da  " + ClaimsAdapter.adptClaimsStatusEnumToItalian(statusOld) + " a " + ClaimsAdapter.adptClaimsStatusEnumToItalian(claimsEntity.getStatus());
                Historical historical = new Historical(EventTypeEnum.EDIT_PRACTICE_DATA, statusOld, claimsEntity.getStatus(), claimsEntity.getComplaint().getDataAccident().getTypeAccident(), claimsEntity.getComplaint().getDataAccident().getTypeAccident(), DateUtil.getNowDate(), claimsUpdateCommand.getUserId(), claimsUpdateCommand.getUserName(), claimsEntity.getType().getValue(), claimsEntity.getType().getValue(), description);
                claimsEntity.addHistorical(historical);
            }
        }


        if (claimsUpdateCommand.getCounterparty() != null) {
            String plate = null;
            if (claimsEntity.getDamaged() != null && claimsEntity.getDamaged().getVehicle() != null && claimsEntity.getDamaged().getVehicle().getLicensePlate() != null)
                plate = claimsEntity.getDamaged().getVehicle().getLicensePlate();
            if (plate != null) {
                for (CounterpartyRequest counterpartyRequest : claimsUpdateCommand.getCounterparty()) {
                    if (counterpartyRequest.getVehicle() != null && counterpartyRequest.getVehicle().getLicensePlate() != null) {
                        if (counterpartyRequest.getVehicle().getLicensePlate().equals(plate)) {
                            LOGGER.debug(MessageCode.CLAIMS_1041.value());
                            throw new BadRequestException(MessageCode.CLAIMS_1041);
                        }
                    }
                    if (counterpartyRequest.getUserCreate() == null || counterpartyRequest.getUserCreate().equalsIgnoreCase(""))
                        counterpartyRequest.setUserCreate(claimsUpdateCommand.getUserId());
                }
            }
        }

        claimsEntity.setIdSaleforce(claimsUpdateCommand.getIdSaleforce());
        if(claimsEntity.getCounterparts() != null) {
            for(CounterpartyEntity counterparty : claimsEntity.getCounterparts()){
                CounterpartyRequest c = new CounterpartyRequest();
                c.setCounterpartyId(counterparty.getCounterpartyId());
                if(claimsUpdateCommand.getCounterparty() != null && !claimsUpdateCommand.getCounterparty().contains(c))
                    counterpartyNewRepository.deleteById(counterparty.getCounterpartyId());
            }
        }
        List<CounterpartyEntity> counterpartyList = new LinkedList<>();
        if(claimsUpdateCommand.getCounterparty() != null && !claimsUpdateCommand.getCounterparty().isEmpty()) {
            for (CounterpartyRequest counterpartyRequest : claimsUpdateCommand.getCounterparty()) {
                LOGGER.debug("[Counterparty] Counterparty id " + counterpartyRequest.getCounterpartyId());
                LOGGER.debug("[Counterparty] Counterparty status " + counterpartyRequest.getRepairStatus());
                CounterpartyEntity counterparty1 = counterpartyAdapter.adptCounterpartyRequestToCounterpartyUpdate(counterpartyRequest);
                CounterpartyNewEntity counterpartyNewEntity = CounterpartyAdapter.adptCounterpartyOldToCounterpartyNew(counterparty1);
                counterpartyNewRepository.save(counterpartyNewEntity);
                counterpartyList.add(counterparty1);
            }
        }
        claimsEntity.setCounterparts(counterpartyList);

        claimsEntity.setDeponentList(DeponentAdapter.adptDeponentRequestToDeponent(claimsUpdateCommand.getDeponent()));
        claimsEntity.setWoundedList(WoundedAdapter.adptWoundedRequestToWounded(claimsUpdateCommand.getWounded()));
        claimsEntity.setPaiComunication(claimsUpdateCommand.getPaiComunication());
        claimsEntity.setLegalComunication(claimsUpdateCommand.getLegalComunication());
        claimsEntity.setFoundModel(FoundModelAdapter.adptFoundModelRequestToFoundModel(claimsUpdateCommand.getFoundModel()));
        claimsEntity.setNotes(NotesAdapter.adptNotesRequestToNotes(claimsUpdateCommand.getNotes()));
        claimsEntity.setWithCounterparty(claimsUpdateCommand.getWithCounterparty());
        claimsEntity.setExemption(ExemptionAdapter.adptExemptionRequestToExemption(claimsUpdateCommand.getExemption()));
        claimsEntity.setUpdateAt(DateUtil.getNowInstant());

        if (claimsUpdateCommand.getTheft() != null) {
            claimsEntity.setTheft(TheftClaimsAdapter.adptTheftRequestV1ToTheft(claimsUpdateCommand.getTheft()));
        }

        if (claimsUpdateCommand.getRefund() != null) {
            claimsEntity.setRefund(RefundAdapter.adptFromRefundRequestToRefund(claimsUpdateCommand.getRefund()));
        }

        if (claimsUpdateCommand.getWithCounterparty() != null && claimsUpdateCommand.getWithCounterparty() == false && claimsUpdateCommand.getDamaged() != null && claimsUpdateCommand.getDamaged().getImpactPoint() != null && claimsUpdateCommand.getDamaged().getImpactPoint().getIncidentDescription() != null) {
            claimsEntity.getDamaged().getImpactPoint().setIncidentDescription(claimsUpdateCommand.getDamaged().getImpactPoint().getIncidentDescription());

        } else if (claimsUpdateCommand.getWithCounterparty() != null && claimsUpdateCommand.getWithCounterparty() == true && claimsUpdateCommand.getCaiDetails() != null &&
                claimsUpdateCommand.getCaiDetails().getVehicleA() != null &&
                claimsUpdateCommand.getCaiDetails().getVehicleB() != null
        ) {
            //claimsEntity.setCaiDetails(CaiAdapter.adptCaiToCaiResponse(claimsUpdateCommand.getCaiDetails()));
            String note = new String();
            note += "VEICOLO A: ";
            note += CaiAdapter.adptCaiConditionToCaiNote(CaiDetailsAdapter.adptCaiDetailsRequestToCaiDetails(claimsUpdateCommand.getCaiDetails().getVehicleA()));
            note += "\nVEICOLO B: ";
            note += CaiAdapter.adptCaiConditionToCaiNote(CaiDetailsAdapter.adptCaiDetailsRequestToCaiDetails(claimsUpdateCommand.getCaiDetails().getVehicleB()));
            ImpactPoint impactPoint = claimsEntity.getDamaged().getImpactPoint();
            if (impactPoint == null) {
                impactPoint = new ImpactPoint();
            }
            impactPoint.setIncidentDescription(note);

            claimsEntity.getDamaged().setImpactPoint(impactPoint);
            //prova della definizione della logica di determinazione in base al cai

        }

        claimsEntity.setCaiDetails(CaiAdapter.adptCaiToCaiResponse(claimsUpdateCommand.getCaiDetails()));
        claimsEntity.setPoVariation(claimsUpdateCommand.getPoVariation());
        claimsEntity.setCompleteDocumentation(claimsUpdateCommand.getCompleteDocumentation());

        if (isInsuranceChanged) {
            statusOld = claimsEntity.getStatus();
            if (!statusOld.equals(ClaimsStatusEnum.WAITING_FOR_VALIDATION)) {
                claimsEntity.setStatus(ClaimsStatusEnum.WAITING_FOR_VALIDATION);
                String description = "Lo stato è cambiato da  " + ClaimsAdapter.adptClaimsStatusEnumToItalian(statusOld) + " a " + ClaimsAdapter.adptClaimsStatusEnumToItalian(claimsEntity.getStatus()) + " poiché è cambiata la copertura assicurativa.";
                Historical historical = new Historical(EventTypeEnum.EDIT_PRACTICE_DATA, statusOld, claimsEntity.getStatus(), claimsEntity.getComplaint().getDataAccident().getTypeAccident(), claimsEntity.getComplaint().getDataAccident().getTypeAccident(), DateUtil.getNowDate(), claimsUpdateCommand.getUserId(), claimsUpdateCommand.getUserName(), claimsEntity.getType().getValue(), claimsEntity.getType().getValue(), description);
                claimsEntity.addHistorical(historical);
            }
        }

        if (isVoltura) {
            statusOld = claimsEntity.getStatus();
            claimsEntity.setStatus(ClaimsStatusEnum.DELETED);
            String description = "Lo stato è cambiato da  " + ClaimsAdapter.adptClaimsStatusEnumToItalian(statusOld) + " a " + ClaimsAdapter.adptClaimsStatusEnumToItalian(claimsEntity.getStatus()) + " poiché si è verificato un cambio di cliente (Voltura)";
            Historical historical = new Historical(EventTypeEnum.EDIT_PRACTICE_DATA, statusOld, claimsEntity.getStatus(), claimsEntity.getComplaint().getDataAccident().getTypeAccident(), claimsEntity.getComplaint().getDataAccident().getTypeAccident(), DateUtil.getNowDate(), claimsUpdateCommand.getUserId(), claimsUpdateCommand.getUserName(), claimsEntity.getType().getValue(), claimsEntity.getType().getValue(), description);
            claimsEntity.addHistorical(historical);


            claimsNewEntity = converterClaimsService.convertOldEntityToNewClaimsEntity(claimsEntity);

            claimsNewRepository.save(claimsNewEntity);

            //Intervento 6 - C
            try {
                //refactoring
                externalCommunicationService.modifyIncidentAsync(claimsEntity.getId(),statusOld, false,oldIncident,false, oldClaims);
            } catch (IOException e) {
                throw new BadRequestException(MessageCode.CLAIMS_1112, e);
            }

            claimsEntity = claimsService.findById(claimsUpdateCommand.getUuid());
            claimsEntity.setId(claimsUpdateCommand.getUpdateId());
            claimsEntity.setStatus(ClaimsStatusEnum.WAITING_FOR_VALIDATION);
            claimsEntity.setCreatedAt(DateUtil.getNowInstant());
            claimsEntity.setDamaged(damaged);

            List<EmailTemplateMessagingRequestV1> emailTemplateList = MessagingAdapter.adptEmailTemplateMessagingRequestToEmailTemplateMessagingResponseList(messagingService.getMailTemplateByTypeEvent(EventTypeEnum.COMPLAINT_INSERTION, claimsEntity));
            description = messagingService.sendMailAndCreateLogs(emailTemplateList, null, claimsEntity.getCreatedAt());
            historical = new Historical(EventTypeEnum.COMPLAINT_INSERTION, claimsEntity.getStatus(), claimsEntity.getStatus(), claimsEntity.getComplaint().getDataAccident().getTypeAccident(), claimsEntity.getComplaint().getDataAccident().getTypeAccident(), DateUtil.getNowDate(), claimsEntity.getUserId(), claimsUpdateCommand.getUserName(), claimsEntity.getType().getValue(), claimsEntity.getType().getValue(), description);
            claimsEntity.setHistorical(new ArrayList<>());
            claimsEntity.addHistorical(historical);

        }


        if (!claimsEntity.getStatus().equals(ClaimsStatusEnum.INCOMPLETE) && !claimsEntity.getStatus().equals(ClaimsStatusEnum.WAITING_FOR_VALIDATION)) {
            counterpartyService.EligibilityCriterion(claimsEntity, claimsUpdateCommand.getUserId(), claimsUpdateCommand.getUserName());
            if(claimsEntity.getCounterparts()!=null && !claimsEntity.getCounterparts().isEmpty() && claimsEntity.getCounterparts().get(0).getEligibility() != null && claimsEntity.getCounterparts().get(0).getEligibility()){
                cacheService.deleteClaimsStats(CACHENAME, COUNTERPARTYKEY);
            }
        }

        if (claimsEntity.getDamaged()!=null &&
                claimsEntity.getDamaged().getDriver()==null &&
                oldClaims.getDamaged()!=null &&
                oldClaims.getDamaged().getDriver()!=null){
            claimsEntity.getDamaged().setDriver(oldClaims.getDamaged().getDriver());
        }
        claimsNewEntity = converterClaimsService.convertOldEntityToNewClaimsEntity(claimsEntity);
        claimsNewRepository.save(claimsNewEntity);;


        if(dwhCall){
            dwhClaimsService.sendMessage(claimsEntity, com.doing.nemo.dwh.client.payload.request.EventTypeEnum.UPDATED);
            if(claimsNewEntity.getCounterparts() != null) {
                for (CounterpartyNewEntity counterpartyNew : claimsNewEntity.getCounterparts()) {
                    CounterpartyEntity counterparty = CounterpartyAdapter.adptCounterpartyNewToCounterpartyOld(counterpartyNew);
                    dwhClaimsService.sendMessage(counterparty, com.doing.nemo.dwh.client.payload.request.EventTypeEnum.UPDATED);
                }
            }
        }

        //Intervento 6 - C
        try {
            externalCommunicationService.modifyIncidentSync(claimsUpdateCommand.getUuid(),statusOld, false,oldIncident,false,oldClaims);
        } catch (IOException e) {
            LOGGER.debug(MessageCode.CLAIMS_1112.value());
            throw new BadRequestException(MessageCode.CLAIMS_1112, e);
        }


        cacheService.deleteClaimsStats(CACHENAME, STATSkEY);
        cacheService.deleteClaimsStats(CACHENAME, STATSEVIDENCE);
        cacheService.deleteClaimsStats(CACHENAME, STATSSTATUS);

        cacheService.deleteClaimsStats(CACHENAME, STATSEVIDENCECONTINUATION);
        cacheService.deleteClaimsStats(CACHENAME, STATSkEY2);
    }
}
