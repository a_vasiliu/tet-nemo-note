package com.doing.nemo.claims.commandHandler;


import com.doing.nemo.claims.command.ClaimsInsertNoteCommand;
import com.doing.nemo.claims.common.commandBus.Command;
import com.doing.nemo.claims.common.commandBus.CommandHandler;
import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.ClaimsNewEntity;
import com.doing.nemo.claims.entity.jsonb.Notes;
import com.doing.nemo.claims.repository.ClaimsNewRepository;
import com.doing.nemo.claims.repository.ClaimsRepository;
import com.doing.nemo.claims.service.ClaimsService;
import com.doing.nemo.claims.service.ConverterClaimsService;
import com.doing.nemo.claims.service.DwhClaimsService;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.dwh.client.payload.request.EventTypeEnum;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class ClaimsInsertNoteCommandHandler implements CommandHandler {

    private static Logger LOGGER = LoggerFactory.getLogger(ClaimsInsertNoteCommandHandler.class);

    @Autowired
    private DwhClaimsService dwhClaimsService;

    @Value("${dwh.call}")
    private Boolean dwhCall;

    @Autowired
    private ClaimsNewRepository claimsNewRepository;

    @Autowired
    private ConverterClaimsService converterClaimsService;

    @Override
    public void handle(Command command) {

        //REFACTOR

        ClaimsInsertNoteCommand claimsInsertNoteCommand = (ClaimsInsertNoteCommand) command;
        Optional<ClaimsNewEntity> claimsEntityOptional = claimsNewRepository.findById(claimsInsertNoteCommand.getClaimsId());

        if (!claimsEntityOptional.isPresent()) {
            LOGGER.debug("Claims with UUID " + claimsInsertNoteCommand.getClaimsId() + " not found ");
            throw new NotFoundException("Claims with UUID " + claimsInsertNoteCommand.getClaimsId() + " not found ", MessageCode.CLAIMS_1010);
        }

        ClaimsEntity claimsEntity = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsEntityOptional.get());
        Notes note = claimsInsertNoteCommand.getNote();
        note.setCreatedAt(DateUtil.getNowDate());
        claimsEntity.addNotes(note);
        claimsEntity.setUpdateAt(DateUtil.getNowInstant());
        ClaimsNewEntity claimsNewEntity = converterClaimsService.convertOldEntityToNewClaimsEntity(claimsEntity);
        claimsNewRepository.save(claimsNewEntity);
        if(dwhCall){
            dwhClaimsService.sendMessage(claimsEntity, EventTypeEnum.UPDATED);
        }
    }
}
