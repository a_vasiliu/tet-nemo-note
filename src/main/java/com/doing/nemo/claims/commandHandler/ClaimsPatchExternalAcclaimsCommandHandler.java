package com.doing.nemo.claims.commandHandler;

import com.doing.nemo.claims.command.ClaimsPatchExternalAcclaimsCommand;
import com.doing.nemo.claims.common.commandBus.Command;
import com.doing.nemo.claims.common.commandBus.CommandHandler;
import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.ClaimsNewEntity;
import com.doing.nemo.claims.entity.jsonb.complaint.jsonbComplaint.Entrusted;
import com.doing.nemo.claims.repository.ClaimsNewRepository;
import com.doing.nemo.claims.service.ConverterClaimsService;
import com.doing.nemo.claims.service.DwhClaimsService;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.InternalException;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.Optional;

@Component
public class ClaimsPatchExternalAcclaimsCommandHandler implements CommandHandler {

    private static Logger LOGGER = LoggerFactory.getLogger(ClaimsPatchExternalAcclaimsCommandHandler.class);

    @Autowired
    private ClaimsNewRepository claimsNewRepository;

    @Autowired
    private DwhClaimsService dwhClaimsService;

    @Autowired
    private ConverterClaimsService converterClaimsService;

    @Value("${dwh.call}")
    private Boolean dwhCall;


    @Override
    public void handle(Command command) {
        //REFACTOR

        ClaimsPatchExternalAcclaimsCommand claimsPatchExternalMSACommand = (ClaimsPatchExternalAcclaimsCommand) command;
            //recupero della nuova entità
            Optional<ClaimsNewEntity> claimsNewEntityOptional = claimsNewRepository.findById(claimsPatchExternalMSACommand.getId());
            if(!claimsNewEntityOptional.isPresent()){
                LOGGER.debug(MessageCode.CLAIMS_1027.value());
                throw new NotFoundException(MessageCode.CLAIMS_1027);
            }
            //conversione nella vecchia entità
            ClaimsEntity claimsEntity = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntityOptional.get());
            if (claimsEntity.getComplaint() != null) {
                if(claimsEntity.getComplaint().getEntrusted() == null)
                    claimsEntity.getComplaint().setEntrusted(new Entrusted());

                claimsEntity.getComplaint().getEntrusted().setNumberSxCounterparty(claimsPatchExternalMSACommand.getNumberSxCounterparty());

                claimsEntity.getComplaint().getEntrusted().setStatus(claimsPatchExternalMSACommand.getStatus());
                System.out.println(claimsEntity.getComplaint().getEntrusted().getStatus());

            } else {
                LOGGER.debug(MessageCode.CLAIMS_1026.value());
                throw new InternalException(MessageCode.CLAIMS_1026);
            }


            Instant nowInstant = DateUtil.getNowInstant();
            claimsEntity.setUpdateAt(nowInstant);

            ClaimsNewEntity claimsNewEntity = converterClaimsService.convertOldEntityToNewClaimsEntity(claimsEntity);

            claimsNewRepository.save(claimsNewEntity);

            if(dwhCall){
                dwhClaimsService.sendMessage(claimsEntity, com.doing.nemo.dwh.client.payload.request.EventTypeEnum.UPDATED);
            }
        }

}
