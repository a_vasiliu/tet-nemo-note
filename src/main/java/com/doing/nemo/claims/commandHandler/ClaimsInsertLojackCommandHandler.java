package com.doing.nemo.claims.commandHandler;

import com.doing.nemo.claims.adapter.*;
import com.doing.nemo.claims.command.ClaimsInsertLojackCommand;
import com.doing.nemo.claims.common.commandBus.Command;
import com.doing.nemo.claims.common.commandBus.CommandHandler;
import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.CounterpartyRequest;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.ClaimsNewEntity;
import com.doing.nemo.claims.entity.CounterpartyEntity;
import com.doing.nemo.claims.entity.CounterpartyNewEntity;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsFlowEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.jsonb.MetadataClaim;
import com.doing.nemo.claims.entity.jsonb.damaged.Damaged;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.Customer;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbAntiTheftService.AntiTheftRequest;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbAntiTheftService.AntiTheftService;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbAntiTheftService.Registry;
import com.doing.nemo.claims.entity.settings.AntiTheftServiceEntity;
import com.doing.nemo.claims.entity.settings.ContractTypeEntity;
import com.doing.nemo.claims.entity.settings.PersonalDataEntity;
import com.doing.nemo.claims.entity.settings.RecoverabilityEntity;
import com.doing.nemo.claims.repository.AntiTheftServiceRepository;
import com.doing.nemo.claims.repository.ClaimsNewRepository;
import com.doing.nemo.claims.repository.ClaimsRepository;
import com.doing.nemo.claims.repository.PersonalDataRepository;
import com.doing.nemo.claims.service.*;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.Instant;
import java.util.UUID;

//REFACTOR
@Component
public class ClaimsInsertLojackCommandHandler implements CommandHandler {

    private static Logger LOGGER = LoggerFactory.getLogger(ClaimsInsertLojackCommandHandler.class);

    @Autowired
    private ClaimsNewRepository claimsNewRepository;

    @Autowired
    private ConverterClaimsService converterClaimsService;

    @Autowired
    private ClaimsService claimsService;

    @Autowired
    private ContractService contractService;

    @Autowired
    private CounterpartyAdapter counterpartyAdapter;

    @Autowired
    private AntiTheftServiceRepository antiTheftServiceRepository;
    @Autowired
    private RecoverabilityService recoverabilityService;
    @Autowired
    private IncidentService incidentService;

    @Autowired
    private DwhClaimsService dwhClaimsService;

    @Value("${dwh.call}")
    private Boolean dwhCall;

    @Autowired
    private PersonalDataRepository personalDataRepository;

    @Autowired
    private CacheService cacheService;

    @Autowired
    private AntiTheftRequestService antiTheftRequestService;

    private static final String CACHENAME = "claims.";
    private static final String STATSkEY = "claimsStats::";
    private static final String STATSEVIDENCE = "evidenceStats::";
    private static final String STATSTHEFT = "theftStats::";
    private static final String STATSSTATUS = "statusStats::";
    private static final String COUNTERPARTYKEY = "counterpartyStats::";

    @Override
    public void handle(Command command) {

        ClaimsInsertLojackCommand claimsInsertCommand = (ClaimsInsertLojackCommand) command;

        ClaimsEntity claimsEntity = new ClaimsEntity();

        claimsEntity.setId(claimsInsertCommand.getClaimsId());

        String codAntiTheftService = "";

        //recupero delle informazioni legata alla compagnia assicuratrice CARD/NO CARD
        Damaged damaged = DamagedAdapter.adptDamagedRequestInsertToDamaged(claimsInsertCommand.getDamaged());
        claimsEntity.setDamaged(damaged);
        if(damaged != null){
            Customer customer = damaged.getCustomer();
            if(customer != null){
                String customerId = customer.getCustomerId();
                if(customerId != null){

                    PersonalDataEntity customerDb = personalDataRepository.findByCustomerId(customerId);

/******************************************************************************************************/
                    if(customerDb!=null) {
                        //Setto i fleetManager, distinguendo tra quelli importati (passati da front-end) e quelli nel DB (presenti in customerDb)
                        damaged.setFleetManagerList(FleetManagerAdpter.adptFromFleetManagerPersonalDataToFleetManager
                                (customerDb.getFleetManagerPersonalData(), damaged.getFleetManagerList()));

                        customerDb = PersonalDataAdapter.adptFromCustomerToPersonalData(customerDb, customer, damaged.getFleetManagerList());

                        // System.out.println(customerDb.getFleetManagerPersonalData());
                        personalDataRepository.save(customerDb);
                    }else{
                        customerDb = PersonalDataAdapter.adptFromCustomerToPersonalData(customerDb, customer, damaged.getFleetManagerList());
                        //Inserisco nella lista dei damaged i fleetmanager che non sono presenti nei fleet manager di customerdb
                        damaged.setFleetManagerList(PersonalDataAdapter.buildFinalFleetManagerListofDamaged(customerDb.getFleetManagerPersonalData(),damaged.getFleetManagerList()));
                        personalDataRepository.save(customerDb);
                    }

/******************************************************************************************************/



                }
            }
        }
        claimsEntity.setCounterparts(counterpartyAdapter.adptCounterpartyRequestToCounterparty(claimsInsertCommand.getCounterparty()));
        //mappatura del flusso attraverso il tipo contratto e il tipo sinistro ottenuti dalle tabelle di websin
        try {
            ContractTypeEntity contractTypeEntity = contractService.getContractType(claimsInsertCommand.getDamaged().getContract().getContractType());
            if(contractTypeEntity == null){
                LOGGER.debug(MessageCode.CLAIMS_1010.value());
                throw new BadRequestException(MessageCode.CLAIMS_1010);
            }

            if (!contractTypeEntity.getFlagWS()) {
                LOGGER.debug(MessageCode.CLAIMS_1042.value());
                throw new BadRequestException(MessageCode.CLAIMS_1042);
            } else {

                if (claimsInsertCommand.getComplaint().getDataAccident().getTypeAccident() == null) {
                    //se il tipo sinistro non è stato inserito recupero flusso di default
                    claimsEntity.setType(contractTypeEntity.getDefaultFlow());

                } else {
                    claimsEntity.setType(contractService.getPersonalRiskFlowType(claimsInsertCommand.getDamaged().getContract().getContractType(),
                            claimsInsertCommand.getComplaint().getDataAccident().getTypeAccident(),
                            claimsInsertCommand.getDamaged().getCustomer().getCustomerId().toString())); /* tipo di flusso */
                    if (claimsEntity.getType().equals(ClaimsFlowEnum.NO)) {
                        LOGGER.debug("It's not possible insert a claim with NO flow");
                        throw new BadRequestException("It's not possible insert a claim with NO flow", MessageCode.CLAIMS_1057);
                    }
                }
            }
        } catch (BadRequestException e) {
            LOGGER.debug(e.getMessage());
            throw e;
        }

        claimsEntity.setUserEntity(claimsInsertCommand.getUserId());

        //di defaul è in waiting_for_validation

        if (claimsInsertCommand.getCounterparty() != null && !claimsInsertCommand.getCounterparty().isEmpty()) {
            LOGGER.debug(MessageCode.CLAIMS_1033.value());
            throw new BadRequestException(MessageCode.CLAIMS_1033);
        } else {

            claimsEntity.setStatus(ClaimsStatusEnum.INCOMPLETE);
            claimsEntity.setAuthorityLinkable(false);
        }

        //DA UNIRE
        if (claimsInsertCommand.getCounterparty() != null) {
            String plate = null;
            if (claimsEntity.getDamaged() != null && claimsEntity.getDamaged().getVehicle() != null && claimsEntity.getDamaged().getVehicle().getLicensePlate() != null)
                plate = claimsEntity.getDamaged().getVehicle().getLicensePlate();
            if (plate != null) {
                for (CounterpartyRequest counterpartyRequest : claimsInsertCommand.getCounterparty()) {
                    if (counterpartyRequest.getVehicle() != null && counterpartyRequest.getVehicle().getLicensePlate() != null) {
                        if (counterpartyRequest.getVehicle().getLicensePlate().equals(plate)) {
                            LOGGER.debug(MessageCode.CLAIMS_1041.value());
                            throw new BadRequestException(MessageCode.CLAIMS_1041);
                        }
                    }
                    if (counterpartyRequest.getUserCreate() == null || counterpartyRequest.getUserCreate().equalsIgnoreCase(""))
                        counterpartyRequest.setUserCreate(claimsInsertCommand.getUserId());
                }
            }
        }
        claimsEntity.setCounterparts(counterpartyAdapter.adptCounterpartyRequestToCounterparty(claimsInsertCommand.getCounterparty()));

        //mappatura del flusso attraverso il tipo contratto e il tipo sinistro ottenuti dalle tabelle di websin
        claimsEntity.setComplaint(ComplaintAdapter.adptComplaintRequestToComplaintUpdate(claimsInsertCommand.getComplaint(), null));
        if (claimsEntity.getComplaint() != null && claimsEntity.getComplaint().getDataAccident() != null)
            claimsEntity.getComplaint().getDataAccident().setTypeAccident(DataAccidentTypeAccidentEnum.FURTO_TOTALE);
        else {
            LOGGER.debug(MessageCode.CLAIMS_1046.value());
            throw new BadRequestException(MessageCode.CLAIMS_1046);
        }
        claimsEntity.setDeponentList(DeponentAdapter.adptDeponentRequestToDeponent(claimsInsertCommand.getDeponent()));
        claimsEntity.setWoundedList(WoundedAdapter.adptWoundedRequestToWounded(claimsInsertCommand.getWounded()));
        claimsEntity.setCaiDetails(CaiAdapter.adptCaiToCaiResponse(claimsInsertCommand.getCai()));
        if (claimsInsertCommand.getWithCounterparty() && claimsInsertCommand.getCai() != null &&
                claimsInsertCommand.getCai().getVehicleA() != null && claimsInsertCommand.getCai().getVehicleB() != null &&
                claimsInsertCommand.getCai().getDriverSide() != null
        ) {
            String note = new String();
            note += "VEICOLO A: ";
            note += CaiAdapter.adptCaiConditionToCaiNote(CaiDetailsAdapter.adptCaiDetailsRequestToCaiDetails(claimsInsertCommand.getCai().getVehicleA()));
            note += "\nVEICOLO B: ";
            note += CaiAdapter.adptCaiConditionToCaiNote(CaiDetailsAdapter.adptCaiDetailsRequestToCaiDetails(claimsInsertCommand.getCai().getVehicleB()));
            claimsEntity.getDamaged().getImpactPoint().setIncidentDescription(note);

        } else if (!claimsInsertCommand.getWithCounterparty()) {
            claimsEntity.getDamaged().getImpactPoint().setIncidentDescription(claimsInsertCommand.getDamaged().getImpactPoint().getIncidentDescription());
        }
        claimsEntity.setIdSaleforce(claimsInsertCommand.getIdSaleforce());
        claimsEntity.setWithCounterparty(claimsInsertCommand.getWithCounterparty());
        claimsEntity.setPaiComunication(claimsInsertCommand.getCaiComunication());
        claimsEntity.setForced(claimsInsertCommand.getForced());
        claimsEntity.setInEvidence(claimsInsertCommand.getInEvidence());
        claimsEntity.setWithContinuation(false);
        claimsEntity.setPoVariation(false);
        claimsEntity.setCompleteDocumentation(claimsInsertCommand.getCompleteDocumentation());

        if (claimsInsertCommand.getRefund() != null) {
            claimsEntity.setRefund(RefundAdapter.adptFromRefundRequestToRefund(claimsInsertCommand.getRefund()));
        }

        if (claimsInsertCommand.getTheft() != null) {
            claimsEntity.setTheft(TheftClaimsAdapter.adptTheftRequestV1ToTheft(claimsInsertCommand.getTheft()));
            //claimsEntity.getTheft().setWithReceptions(false);
            if (claimsEntity.getTheft().getFirstKeyReception() != null || claimsEntity.getTheft().getSecondKeyReception() != null
                    || claimsEntity.getTheft().getOriginalComplaintReception() != null || claimsEntity.getTheft().getOriginalReportReception() != null) {
                //claimsEntity.getTheft().setWithReceptions(true);
            }
        }

        //aggiunta metadati
        MetadataClaim metadataClaim = new MetadataClaim();
        metadataClaim.setMetadata(claimsInsertCommand.getMetadata());
        claimsEntity.setMetadata(metadataClaim);

        Instant nowInstant = DateUtil.getNowInstant();
        claimsEntity.setCreatedAt(nowInstant);
        claimsEntity.setUpdateAt(nowInstant);


        if (claimsEntity.getType() != null && claimsEntity.getComplaint() != null && claimsEntity.getComplaint().getDataAccident() != null && claimsEntity.getComplaint().getDataAccident().getTypeAccident() != null) {

            RecoverabilityEntity recoverabilityEntity = recoverabilityService.getRecoverabilityByFlowAndTypeClaim(claimsEntity.getType().getValue().toUpperCase(), claimsEntity.getComplaint().getDataAccident().getTypeAccident());
            claimsEntity.getComplaint().getDataAccident().setRecoverability(recoverabilityEntity.getRecoverability());
            claimsEntity.getComplaint().getDataAccident().setRecoverabilityPercent(recoverabilityEntity.getPercentRecoverability());
        }
        claimsEntity.setMigrated(false);

        damaged = claimsEntity.getDamaged();
        AntiTheftService antiTheftService = null;
        if (damaged.getAntiTheftService() != null) {

            //recupero localizzatore da tabella setting AntiTheftServiceEntity
            //da capire se avremo mai un localizzatore per il furto e se ci viene passato un tipo
            if (damaged.getAntiTheftService().getRegistryList() != null && !damaged.getAntiTheftService().getRegistryList().isEmpty()) {

                //for(Registry currentRegistry : damaged.getAntiTheftService().getRegistryList()){
                Registry currentRegistry = damaged.getAntiTheftService().getRegistryList().get(0);
                codAntiTheftService = AntiTheftServiceAdapter.adptProviderToAntiTheftServiceEntity(currentRegistry.getCodPack());
                AntiTheftServiceEntity antiTheftServiceEntity = antiTheftServiceRepository.findAntiTheftByCodAntiTheftService(codAntiTheftService);
                antiTheftService = AntiTheftServiceAdapter.adptAntiTheftServiceEntityToAntiTheftService(antiTheftServiceEntity);
                if(antiTheftService != null) {
                    antiTheftService.setRegistryList(damaged.getAntiTheftService().getRegistryList());
                    claimsEntity.getDamaged().setAntiTheftService(antiTheftService);
                }

            }
        }

        //funzione di conversione

        ClaimsNewEntity claimsNewEntity = converterClaimsService.convertOldEntityToNewClaimsEntity(claimsEntity);
        claimsNewRepository.save(claimsNewEntity);

        claimsNewEntity = claimsNewRepository.getOne(claimsNewEntity.getId());
        claimsEntity.setPracticeId(claimsNewEntity.getPracticeId());



        if(dwhCall){
            dwhClaimsService.sendMessage(claimsEntity, com.doing.nemo.dwh.client.payload.request.EventTypeEnum.CREATED);
            if(claimsNewEntity.getCounterparts() != null) {
                for (CounterpartyNewEntity newCounterparty : claimsNewEntity.getCounterparts()) {
                    CounterpartyEntity counterparty = CounterpartyAdapter.adptCounterpartyNewToCounterpartyOld(newCounterparty);
                    dwhClaimsService.sendMessage(counterparty, com.doing.nemo.dwh.client.payload.request.EventTypeEnum.CREATED);
                }
            }
        }

        /*
        try {
            // intervento 6
            //incidentService.insertIncident(claimsInsertCommand.getClaimsId(),null);
        } catch (IOException e) {
            throw new BadRequestException(MessageCode.CLAIMS_1112);
        }
         */


        //claimsRepository.save(claimsEntity);
        cacheService.deleteClaimsStats(CACHENAME, STATSkEY);
        cacheService.deleteClaimsStats(CACHENAME, STATSEVIDENCE);


        if (antiTheftService != null) {

            // creazione della riga di storico di octo/texa
            String idAntiTheftRequest = UUID.randomUUID().toString();
            antiTheftRequestService.insertAntiTheftRequest(idAntiTheftRequest, claimsNewEntity, antiTheftService.getProviderType(), "1");
            claimsService.callToOctoAsync(claimsNewEntity, claimsNewEntity.getStatus(), new AntiTheftRequest(), claimsInsertCommand.getUserId(), claimsInsertCommand.getUserName(), idAntiTheftRequest);


        }
    }
}
