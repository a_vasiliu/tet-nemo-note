package com.doing.nemo.claims.commandHandler;

import com.doing.nemo.claims.command.ClaimsPatchReadExternalCommand;
import com.doing.nemo.claims.common.commandBus.Command;
import com.doing.nemo.claims.common.commandBus.CommandHandler;
import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.IdRequestV1;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.ClaimsNewEntity;
import com.doing.nemo.claims.entity.jsonb.complaint.jsonbComplaint.Entrusted;
import com.doing.nemo.claims.entity.jsonb.complaint.jsonbComplaint.FromCompany;
import com.doing.nemo.claims.repository.ClaimsNewRepository;
import com.doing.nemo.claims.service.ConverterClaimsService;
import com.doing.nemo.claims.service.DwhClaimsService;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class ClaimsPatchReadExternalCommandHandler implements CommandHandler {

    private static Logger LOGGER = LoggerFactory.getLogger(ClaimsPatchReadExternalCommandHandler.class);

    @Autowired
    private ClaimsNewRepository claimsNewRepository;

    @Autowired
    private DwhClaimsService dwhClaimsService;

    @Autowired
    private ConverterClaimsService converterClaimsService;

    @Value("${dwh.call}")
    private Boolean dwhCall;

    @Override
    public void handle(Command command) {
        //REFACTOR

        ClaimsPatchReadExternalCommand claimsPatchReadExternalCommand = (ClaimsPatchReadExternalCommand) command;
        if (claimsPatchReadExternalCommand.getClaimsIdListRequest() != null) {
            for (IdRequestV1 claimsIdRequestV1 : claimsPatchReadExternalCommand.getClaimsIdListRequest().getData()) {
                //recupero della nuova entità
                Optional<ClaimsNewEntity> claimsNewEntityOptional = claimsNewRepository.findById(claimsIdRequestV1.getId());
                if (!claimsNewEntityOptional.isPresent()) {
                    LOGGER.debug("Claims with UUID " + claimsIdRequestV1.getId() + " not found ");
                    throw new NotFoundException("Claims with UUID " + claimsIdRequestV1.getId() + " not found ");
                }
                ClaimsNewEntity claimsNewEntity = claimsNewEntityOptional.get();
                //conversione nella vecchia entità
                ClaimsEntity claimsEntity =converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntity);
                List<String> nemoProfilesList = claimsPatchReadExternalCommand.getNemoProfiles();
                if(nemoProfilesList != null && !nemoProfilesList.isEmpty() && nemoProfilesList.contains("claims_insurance_supplier_acclaims")){
                    claimsEntity.setReadAcclaims(true);
                    //INSERIRE DATA DI DOWNLOAD NELL'ENTRUSTED
                    if(claimsEntity.getComplaint().getEntrusted() == null)
                        claimsEntity.getComplaint().setEntrusted(new Entrusted());
                    claimsEntity.getComplaint().getEntrusted().setDwlMan(DateUtil.getNowDate());
                }else if (nemoProfilesList != null && !nemoProfilesList.isEmpty() && nemoProfilesList.contains("claims_insurance_supplier_msa")){
                    claimsEntity.setRead(true);
                    if(claimsEntity.getComplaint().getFromCompany() == null)
                        claimsEntity.getComplaint().setFromCompany(new FromCompany());
                    claimsEntity.getComplaint().getFromCompany().setDwlMan(DateUtil.getNowDate());
                }
                claimsNewEntity = converterClaimsService.convertOldEntityToNewClaimsEntity(claimsEntity);
                claimsNewRepository.save(claimsNewEntity);
                if(dwhCall){
                    dwhClaimsService.sendMessage(claimsEntity, com.doing.nemo.dwh.client.payload.request.EventTypeEnum.UPDATED);
                }
            }
        }
    }
}
