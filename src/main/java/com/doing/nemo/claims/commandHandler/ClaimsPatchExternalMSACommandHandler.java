package com.doing.nemo.claims.commandHandler;

import com.doing.nemo.claims.adapter.ClaimsAdapter;
import com.doing.nemo.claims.adapter.FromCompanyAdapter;
import com.doing.nemo.claims.adapter.IncidentAdapter;
import com.doing.nemo.claims.command.ClaimsPatchExternalMSACommand;
import com.doing.nemo.claims.common.commandBus.Command;
import com.doing.nemo.claims.common.commandBus.CommandHandler;
import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.IncidentRequestV1;
import com.doing.nemo.claims.controller.payload.request.complaint.FromCompanyRequest;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.ClaimsNewEntity;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsFlowEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsHistoricalUserEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.doing.nemo.claims.entity.enumerated.EventTypeEnum;
import com.doing.nemo.claims.entity.jsonb.Historical;
import com.doing.nemo.claims.entity.jsonb.complaint.jsonbComplaint.FromCompany;
import com.doing.nemo.claims.entity.settings.InsuranceCompanyEntity;
import com.doing.nemo.claims.repository.ClaimsNewRepository;
import com.doing.nemo.claims.repository.ClaimsRepository;
import com.doing.nemo.claims.repository.InsuranceCompanyRepository;
import com.doing.nemo.claims.service.ConverterClaimsService;
import com.doing.nemo.claims.service.DwhClaimsService;
import com.doing.nemo.claims.service.ExternalCommunicationService;
import com.doing.nemo.claims.service.impl.ClaimsServiceImpl;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.InternalException;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

@Component
public class ClaimsPatchExternalMSACommandHandler implements CommandHandler {

    private static Logger LOGGER = LoggerFactory.getLogger(ClaimsPatchExternalMSACommandHandler.class);
    @Autowired
    private ClaimsRepository claimsRepository;

    @Autowired
    private ClaimsNewRepository claimsNewRepository;

    @Autowired
    private DwhClaimsService dwhClaimsService;

    @Autowired
    private IncidentAdapter incidentAdapter;

    @Autowired
    private ConverterClaimsService converterClaimsService;

    @Autowired
    private ExternalCommunicationService externalCommunicationService;

    @Value("${dwh.call}")
    private Boolean dwhCall;

    @Autowired
    private InsuranceCompanyRepository insuranceCompanyRepo;

    private FromCompany updateFromCompanyNameByAniaCode(FromCompany fromCompany, String aniaCode) {
        // imposta a null il nome della company
        fromCompany.setCompany(null);
        // se esiste, ed è unico, un record nella insurance_company, allora si usa il nome di quell'istanza
        if( aniaCode != null ) {
            try {
                List<InsuranceCompanyEntity> companies = insuranceCompanyRepo.searchByAniaCode( Integer.parseInt( aniaCode ) );
                if (companies.size() == 1) {
                    fromCompany.setCompany( companies.get(0).getName() );
                }
            } catch (Throwable throwable) {
                fromCompany.setCompany(null);
            }
        }
        return fromCompany;
    }

    @Override
    public void handle(Command command) {
        //REFACTOR
        ClaimsPatchExternalMSACommand claimsPatchExternalMSACommand = (ClaimsPatchExternalMSACommand) command;
        if (claimsPatchExternalMSACommand.getFromCompany() != null) {
            //recupero della vecchia entità
            Optional<ClaimsNewEntity> claimsNewEntityOptional = claimsNewRepository.findById(claimsPatchExternalMSACommand.getId());
            if(!claimsNewEntityOptional.isPresent()){
                LOGGER.debug(MessageCode.CLAIMS_1027.value());
                throw new NotFoundException(MessageCode.CLAIMS_1027);
            }
            //conversione nella vecchia entità
            ClaimsEntity claimsEntity = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntityOptional.get());
            ClaimsEntity claimsEntityOld = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntityOptional.get());
            IncidentRequestV1 oldIncident = incidentAdapter.adptFromClaimsEntityToIncidentRequest(claimsEntity,claimsEntityOld.getStatus(),false,false);

            if (claimsEntity.getComplaint() != null) {
                if(claimsPatchExternalMSACommand.getFromCompany() != null) {
                    FromCompany fromCompany = claimsEntity.getComplaint().getFromCompany();
                    // estrazione della request
                    FromCompanyRequest fromCompanyRequest = claimsPatchExternalMSACommand.getFromCompany();
                    // creazione del FromCompany in base alla request
                    FromCompany tmpFromCompany = FromCompanyAdapter.adptFromCompanyRequestToFromCompany(fromCompanyRequest);
                    tmpFromCompany = this.updateFromCompanyNameByAniaCode( tmpFromCompany, fromCompanyRequest.getCompany() );
                    // salvataggio della FormCompany in claims
                    claimsEntity.getComplaint().setFromCompany(tmpFromCompany);

                    if (fromCompany != null) {
                        claimsEntity.getComplaint().getFromCompany().setDwlMan(fromCompany.getDwlMan());
                    }
                    claimsEntity.getComplaint().getFromCompany().setLastUpdate(DateUtil.getNowDate());
                    if (ClaimsServiceImpl.isAuthorityAndSxNumber(claimsEntity)) {
                        ClaimsStatusEnum statusOld = claimsEntity.getStatus();
                        ClaimsStatusEnum nextStatus;
                        if (claimsEntity.getType().equals(ClaimsFlowEnum.FNI)) {
                            nextStatus = ClaimsStatusEnum.TO_SEND_TO_CUSTOMER;
                        } else {
                            nextStatus = ClaimsStatusEnum.TO_ENTRUST;
                        }
                        claimsEntity.setStatus(nextStatus);

                        Instant nowInstant = DateUtil.getNowInstant();
                        claimsEntity.setUpdateAt(nowInstant);

                        ClaimsNewEntity claimsNewEntity = converterClaimsService.convertOldEntityToNewClaimsEntity(claimsEntity);
                        claimsNewRepository.save(claimsNewEntity);

                        if(dwhCall){
                            dwhClaimsService.sendMessage(claimsEntity, com.doing.nemo.dwh.client.payload.request.EventTypeEnum.UPDATED);
                        }

                        try {
                            externalCommunicationService.modifyIncidentSync(claimsEntity.getId(),claimsEntityOld.getStatus(), null,oldIncident,null,claimsEntityOld);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        return;
                        /* claimsEntity.setStatus(nextStatus);
                        String description = "Lo stato è cambiato da " + ClaimsAdapter.adptClaimsStatusEnumToItalian(statusOld) + " a " + ClaimsAdapter.adptClaimsStatusEnumToItalian(claimsEntity.getStatus());
                                    if(dwhCall){                Historical historicalChangeStatus = new Historical(EventTypeEnum.EDIT_PRACTICE_DATA, statusOld, claimsEntity.getStatus(), claimsEntity.getComplaint().getDataAccident().getTypeAccident(), claimsEntity.getComplaint().getDataAccident().getTypeAccident(), DateUtil.getNowDate(), null, ClaimsHistoricalUserEnum.SYSTEM_TO_ENTRUST_ACCOUNT_MANAGER.getValue(), claimsEntity.getType().getValue(), claimsEntity.getType().getValue(), description);
                                        dwhClaimsService.sendMessage(claimsEntity, com.doing.nemo.dwh.client.payload.request.EventTypeEnum.UPDATED);claimsEntity.addHistorical(historicalChangeStatus);
                                    }*/
                    }
                }

                /*if(claimsPatchExternalMSACommand.getNumberSxCounterparty() != null){
                    if(claimsEntity.getComplaint().getEntrusted() == null)
                        claimsEntity.getComplaint().setEntrusted(new Entrusted());
                    claimsEntity.getComplaint().getEntrusted().setNumberSxCounterparty(claimsPatchExternalMSACommand.getNumberSxCounterparty());
                }*/

            } else {
                LOGGER.debug(MessageCode.CLAIMS_1026.value());
                throw new InternalException(MessageCode.CLAIMS_1026);
            }
            Instant nowInstant = DateUtil.getNowInstant();
            claimsEntity.setUpdateAt(nowInstant);

            ClaimsNewEntity claimsNewEntity = converterClaimsService.convertOldEntityToNewClaimsEntity(claimsEntity);
            claimsNewRepository.save(claimsNewEntity);
            if(dwhCall){
                dwhClaimsService.sendMessage(claimsEntity, com.doing.nemo.dwh.client.payload.request.EventTypeEnum.UPDATED);
            }
        }
    }
}
