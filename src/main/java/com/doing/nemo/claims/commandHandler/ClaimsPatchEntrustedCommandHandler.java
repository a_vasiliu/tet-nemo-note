package com.doing.nemo.claims.commandHandler;

import com.doing.nemo.claims.adapter.IncidentAdapter;
import com.doing.nemo.claims.command.ClaimsPatchEntrustedCommand;
import com.doing.nemo.claims.common.commandBus.Command;
import com.doing.nemo.claims.common.commandBus.CommandHandler;
import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.EmailTemplateMessagingRequestV1;
import com.doing.nemo.claims.controller.payload.request.IncidentRequestV1;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.ClaimsNewEntity;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.EntrustedEnum.EntrustedEnum;
import com.doing.nemo.claims.entity.jsonb.Historical;
import com.doing.nemo.claims.entity.jsonb.complaint.Complaint;
import com.doing.nemo.claims.entity.jsonb.complaint.jsonbComplaint.Entrusted;
import com.doing.nemo.claims.entity.settings.InspectorateEntity;
import com.doing.nemo.claims.entity.settings.InsuranceManagerEntity;
import com.doing.nemo.claims.entity.settings.LegalEntity;
import com.doing.nemo.claims.repository.*;
import com.doing.nemo.claims.service.*;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.*;

@Component
public class ClaimsPatchEntrustedCommandHandler implements CommandHandler {
    //REFACTOR
    private static final Logger LOGGER = LoggerFactory.getLogger(ClaimsPatchEntrustedCommandHandler.class);

    @Autowired
    private ClaimsNewRepository claimsNewRepository;

    @Autowired
    private ConverterClaimsService converterClaimsService;

    @Autowired
    private MessagingService messagingService;

    @Autowired
    private LegalRepository legalRepository;

    @Autowired
    private InspectorateRepository inspectorateRepository;

    @Autowired
    private InsuranceManagerRepository insuranceManagerRepository;

    @Autowired
    private IncidentService incidentService;

    @Autowired
    private IncidentAdapter incidentAdapter;

    @Autowired
    private DwhClaimsService dwhClaimsService;

    @Value("${dwh.call}")
    private Boolean dwhCall;

    @Autowired
    private CacheService cacheService;

    @Autowired
    private ExternalCommunicationService externalCommunicationService;

    private static final String CACHENAME = "claims.";
    private static final String STATSkEY = "claimsStats::";
    private static final String STATSEVIDENCE = "evidenceStats::";
    private static final String STATSTHEFT = "theftStats::";
    private static final String STATSSTATUS = "statusStats::";
    private static final String COUNTERPARTYKEY = "counterpartyStats::";
    private static final String STATSkEY2 = "claimsV2Stats::";
    private static final String STATSEVIDENCECONTINUATION = "evidenceContinuationStats::";


    @Override
    public void handle(Command command) {
        ClaimsPatchEntrustedCommand patchEntrustedCommand = (ClaimsPatchEntrustedCommand) command;

        //recupero nuova entità dal repository

        Optional<ClaimsNewEntity> claimsNewEntityOptional = claimsNewRepository.findById(patchEntrustedCommand.getId());


        if (!claimsNewEntityOptional.isPresent()) {
            LOGGER.debug("Claims with UUID " + patchEntrustedCommand.getId() + " not found ");
            throw new NotFoundException("Claims with UUID " + patchEntrustedCommand.getId() + " not found ", MessageCode.CLAIMS_1010);
        }

        //conversione nella nuova entità
        ClaimsEntity claimsEntity = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntityOptional.get());
        ClaimsEntity oldClaims = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntityOptional.get());
        ClaimsStatusEnum oldStatus = claimsEntity.getStatus();

        IncidentRequestV1 oldIncident = incidentAdapter.adptFromClaimsEntityToIncidentRequest(claimsEntity,oldStatus,false,false);

        Entrusted entrusted = claimsEntity.getComplaint().getEntrusted();

        if (patchEntrustedCommand.getEntrustedType() == null) {
            LOGGER.debug("Entrusted type is not present");
            throw new BadRequestException("Entrusted type is not present", MessageCode.CLAIMS_1010);
        }

        if (entrusted == null) {
            entrusted = new Entrusted();
        }

        entrusted.setEntrustedType(patchEntrustedCommand.getEntrustedType());
        //in qualsiasi caso cambio il tipo
        if (entrusted.getEntrustedType().equals(patchEntrustedCommand.getEntrustedType())) {
            if (entrusted.getEntrustedType().equals(EntrustedEnum.LEGAL) && patchEntrustedCommand.getLegal() == null) {
                LOGGER.debug("Legal not assigned");
                throw new BadRequestException("Legal not assigned", MessageCode.CLAIMS_1024);
            } else if (entrusted.getEntrustedType().equals(EntrustedEnum.LEGAL)) {

                Optional<LegalEntity> optLegalEntity = legalRepository.findById(patchEntrustedCommand.getLegal().getId());

                if (optLegalEntity.isPresent()) {

                    LegalEntity legalEntity = optLegalEntity.get();
                    entrusted.setIdEntrustedTo(legalEntity.getId());
                    entrusted.setEntrustedTo(legalEntity.getName());
                    entrusted.setEntrustedEmail(legalEntity.getEmail());
                } else {
                    LOGGER.debug("Legal with uuid:" + patchEntrustedCommand.getId() + " not found");
                    throw new BadRequestException("Legal with uuid:" + patchEntrustedCommand.getId() + " not found", MessageCode.CLAIMS_1010);
                }
            }

            if (entrusted.getEntrustedType().equals(EntrustedEnum.INSPECTORATE) && patchEntrustedCommand.getInspectorate() == null) {
                LOGGER.debug("Inspectorate not assigned");
                throw new BadRequestException("Inspectorate not assigned", MessageCode.CLAIMS_1024);
            } else if (entrusted.getEntrustedType().equals(EntrustedEnum.INSPECTORATE)) {

                Optional<InspectorateEntity> optInspectorateEntity = inspectorateRepository.findById(patchEntrustedCommand.getInspectorate().getId());

                if (optInspectorateEntity.isPresent()) {

                    InspectorateEntity inspectorateEntity = optInspectorateEntity.get();
                    entrusted.setIdEntrustedTo(inspectorateEntity.getId());
                    entrusted.setEntrustedTo(inspectorateEntity.getName());
                    entrusted.setEntrustedEmail(inspectorateEntity.getEmail());

                } else {
                    LOGGER.debug("Inspectorate with uuid:" + patchEntrustedCommand.getId() + " not found");
                    throw new BadRequestException("Inspectorate with uuid:" + patchEntrustedCommand.getId() + " not found", MessageCode.CLAIMS_1010);
                }
            }

            if (entrusted.getEntrustedType().equals(EntrustedEnum.MANAGER) && patchEntrustedCommand.getInsuranceManager() == null) {
                LOGGER.debug("Manager not assigned");
                throw new BadRequestException("Manager not assigned", MessageCode.CLAIMS_1024);
            } else if (entrusted.getEntrustedType().equals(EntrustedEnum.MANAGER)) {

                Optional<InsuranceManagerEntity> optManagerEntity = insuranceManagerRepository.findById(patchEntrustedCommand.getInsuranceManager().getId());

                if (optManagerEntity.isPresent()) {

                    InsuranceManagerEntity managerEntity = optManagerEntity.get();
                    entrusted.setIdEntrustedTo(managerEntity.getId());
                    entrusted.setEntrustedTo(managerEntity.getName());
                    entrusted.setEntrustedEmail(managerEntity.getEmail());

                } else {
                    LOGGER.debug("Manager with uuid:" + patchEntrustedCommand.getId() + " not found");
                    throw new BadRequestException("Manager with uuid:" + patchEntrustedCommand.getId() + " not found", MessageCode.CLAIMS_1010);
                }
            }

            entrusted.setEntrustedDay(new Date());
        }

        Complaint complaint = claimsEntity.getComplaint();
        complaint.setEntrusted(entrusted);
        claimsEntity.setComplaint(complaint);

        String description = "";


        if (patchEntrustedCommand.getClaimsPatchStatusRequestV1() != null && patchEntrustedCommand.getClaimsPatchStatusRequestV1().getTemplateList() != null) {
            List<EmailTemplateMessagingRequestV1> listTos = new LinkedList<>();
            List<EmailTemplateMessagingRequestV1> listNotTos = new LinkedList<>();
            for(EmailTemplateMessagingRequestV1 currentTemplate: patchEntrustedCommand.getClaimsPatchStatusRequestV1().getTemplateList()){
                if(currentTemplate.getTos() != null && !currentTemplate.getTos().isEmpty()){
                    listTos.add(currentTemplate);
                }else{
                    listNotTos.add(currentTemplate);
                }
            }

            List<EmailTemplateMessagingRequestV1> emailTemplateMessaging = messagingService.splitEmailIfContainsUrl(listTos, patchEntrustedCommand.getId());
            emailTemplateMessaging.addAll(listNotTos);
            description = messagingService.sendMailAndCreateLogs(emailTemplateMessaging, patchEntrustedCommand.getClaimsPatchStatusRequestV1().getMotivation(), claimsEntity.getCreatedAt());
        }

        Historical historical;
        if (patchEntrustedCommand.getClaimsPatchStatusRequestV1() != null) {
            historical = new Historical(patchEntrustedCommand.getClaimsPatchStatusRequestV1().getEventType(), claimsEntity.getStatus(), ClaimsStatusEnum.WAITING_FOR_REFUND, claimsEntity.getComplaint().getDataAccident().getTypeAccident(), claimsEntity.getComplaint().getDataAccident().getTypeAccident(), DateUtil.getNowDate(), patchEntrustedCommand.getUserId(), patchEntrustedCommand.getUserName(), claimsEntity.getType().getValue(), claimsEntity.getType().getValue(), description);
        }else {
            historical = new Historical(null, claimsEntity.getStatus(), ClaimsStatusEnum.WAITING_FOR_REFUND, claimsEntity.getComplaint().getDataAccident().getTypeAccident(), claimsEntity.getComplaint().getDataAccident().getTypeAccident(), DateUtil.getNowDate(), patchEntrustedCommand.getUserId(), patchEntrustedCommand.getUserName(), claimsEntity.getType().getValue(), claimsEntity.getType().getValue(), description);
        }
        claimsEntity.addHistorical(historical);
        claimsEntity.setMotivation(patchEntrustedCommand.getClaimsPatchStatusRequestV1().getMotivation());
        claimsEntity.setStatus(ClaimsStatusEnum.WAITING_FOR_REFUND);
        claimsEntity.setInEvidence(false);
        claimsEntity.setUpdateAt(DateUtil.getNowInstant());
        //conversione nella nuova entità
        ClaimsNewEntity claimsNewEntity = converterClaimsService.convertOldEntityToNewClaimsEntity(claimsEntity);
        claimsNewRepository.save(claimsNewEntity);

        if(dwhCall){
            dwhClaimsService.sendMessage(claimsEntity, com.doing.nemo.dwh.client.payload.request.EventTypeEnum.UPDATED);
        }

        //Intervento 6 - C
        try {
            externalCommunicationService.modifyIncidentAsync(patchEntrustedCommand.getId(),oldStatus, false,oldIncident,false, oldClaims);
        } catch (IOException e) {
            throw new BadRequestException(MessageCode.CLAIMS_1112);
        }

        cacheService.deleteClaimsStats(CACHENAME, STATSkEY);
        cacheService.deleteClaimsStats(CACHENAME, STATSEVIDENCE);
        cacheService.deleteClaimsStats(CACHENAME, STATSSTATUS);

        cacheService.deleteClaimsStats(CACHENAME, STATSEVIDENCECONTINUATION);
        cacheService.deleteClaimsStats(CACHENAME, STATSkEY2);

    }
}
