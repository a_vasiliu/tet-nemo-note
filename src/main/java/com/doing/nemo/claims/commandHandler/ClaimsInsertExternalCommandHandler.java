package com.doing.nemo.claims.commandHandler;

import com.doing.nemo.claims.adapter.*;
import com.doing.nemo.claims.command.ClaimsInsertExternalCommand;
import com.doing.nemo.claims.common.commandBus.Command;
import com.doing.nemo.claims.common.commandBus.CommandHandler;
import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.ClaimsInsertExternalRequest;
import com.doing.nemo.claims.controller.payload.request.EmailTemplateMessagingRequestV1;
import com.doing.nemo.claims.controller.payload.request.counterparty.ImpactPointRequest;
import com.doing.nemo.claims.controller.payload.response.ContractInfoResponseV1;
import com.doing.nemo.claims.controller.payload.response.authority.claims.VehicleStatusAuthorityResponseV1;
import com.doing.nemo.claims.controller.payload.response.DogeResponseV1;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.ClaimsNewEntity;
import com.doing.nemo.claims.entity.CounterpartyEntity;
import com.doing.nemo.claims.entity.CounterpartyNewEntity;
import com.doing.nemo.claims.entity.enumerated.AuthorityEnum.AuthorityVehicleStatusEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsFlowEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.enumerated.EventTypeEnum;
import com.doing.nemo.claims.entity.jsonb.Historical;
import com.doing.nemo.claims.entity.jsonb.complaint.jsonbComplaint.FromCompany;
import com.doing.nemo.claims.entity.jsonb.damaged.Damaged;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.Customer;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbAntiTheftService.AntiTheftRequest;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbAntiTheftService.AntiTheftService;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbAntiTheftService.Registry;
import com.doing.nemo.claims.entity.settings.AntiTheftServiceEntity;
import com.doing.nemo.claims.entity.settings.ContractTypeEntity;
import com.doing.nemo.claims.entity.settings.PersonalDataEntity;
import com.doing.nemo.claims.entity.settings.RecoverabilityEntity;
import com.doing.nemo.claims.exception.ForbiddenException;
import com.doing.nemo.claims.repository.AntiTheftServiceRepository;
import com.doing.nemo.claims.repository.ClaimsNewRepository;
import com.doing.nemo.claims.repository.PersonalDataRepository;
import com.doing.nemo.claims.service.*;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.exception.InternalException;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.Instant;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

@Component
public class ClaimsInsertExternalCommandHandler implements CommandHandler {

    private static Logger LOGGER = LoggerFactory.getLogger(ClaimsInsertExternalCommandHandler.class);

    @Autowired
    private ClaimsNewRepository claimsNewRepository;

    @Autowired
    private CounterpartyAdapter counterpartyAdapter;
    @Autowired
    private ESBService esbService;
    @Autowired
    private ClaimsService claimsService;
    @Autowired
    private MessagingService messagingService;
    @Autowired
    private AntiTheftServiceRepository antiTheftServiceRepository;
    @Autowired
    private RecoverabilityService recoverabilityService;
    @Autowired
    private AuthorityService authorityService;

    @Autowired
    private DwhClaimsService dwhClaimsService;

    @Autowired
    private PersonalDataRepository personalDataRepository;

    @Value("${dwh.call}")
    private Boolean dwhCall;

    @Autowired
    private CacheService cacheService;

    @Autowired
    private FileManagerService fileManagerService;

    @Autowired
    private DogeEnquService dogeEnquService;

    @Autowired
    private IncidentService incidentService;

    @Autowired
    private AntiTheftRequestService antiTheftRequestService;

    @Autowired
    private ConverterClaimsService converterClaimsService;

    @Autowired
    private ExternalCommunicationService externalCommunicationService;

    private static final String CACHENAME = "claims.";
    private static final String STATSkEY = "claimsStats::";
    private static final String STATSEVIDENCE = "evidenceStats::";
    private static final String STATSSTATUS = "statusStats::";


    @Autowired
    private ClaimsPendingService claimsPendingService;

    @Override
    public void handle(Command command) {
        //REFACTOR
        ClaimsInsertExternalCommand claimsInsertCommand = (ClaimsInsertExternalCommand) command;

        DataAccidentTypeAccidentEnum typeAccident = claimsInsertCommand.getComplaint().getDataAccident().getTypeAccident();


        if (typeAccident == null && !claimsInsertCommand.getWithCounterparty()
        ) {
            LOGGER.debug("[MSA] " + MessageCode.CLAIMS_1068.value());
            throw new BadRequestException(MessageCode.CLAIMS_1068);
        }



        ClaimsEntity claimsEntity = new ClaimsEntity();

        claimsEntity.setUserEntity(claimsInsertCommand.getUserId());

        claimsEntity.setId(claimsInsertCommand.getClaimsId());

        // Sostituisco i valori null di MSA con una stringa vuota
        if(claimsInsertCommand.getDamaged()!=null && claimsInsertCommand.getDamaged().getImpactPoint()!=null){
            ImpactPointRequest impactPointRequest = claimsInsertCommand.getDamaged().getImpactPoint();
            if(impactPointRequest!=null){
                if(impactPointRequest.getDamageToVehicle()==null){
                    impactPointRequest.setDamageToVehicle("");
                }
                if(impactPointRequest.getIncidentDescription()==null){
                    impactPointRequest.setIncidentDescription("");
                }
            }else{
                ImpactPointRequest impactPointRequest1 = new ImpactPointRequest();
                impactPointRequest1.setDamageToVehicle("");
                impactPointRequest1.setIncidentDescription("");
                claimsInsertCommand.getDamaged().setImpactPoint(impactPointRequest1);
            }
        }

        Damaged damaged = DamagedAdapter.adptDamagedExternalRequestToDamaged(claimsInsertCommand.getDamaged());
        String queryDate;
        Date dataToFormat = null;
        /*if(claimsInsertCommand.getComplaint() != null && claimsInsertCommand.getComplaint().getDataAccident() != null && claimsInsertCommand.getComplaint().getDataAccident().getDateAccident() != null) {

            dataToFormat = DateUtil.convertIS08601StringToUTCDate(claimsInsertCommand.getComplaint().getDataAccident().getDateAccident());
            queryDate = new SimpleDateFormat("yyyyMMdd").format(dataToFormat);

        } else {
            Calendar now = Calendar.getInstance();
            int year = now.get(Calendar.YEAR);
            int month = now.get(Calendar.MONTH) + 1; // Note: zero based!
            String mese = month +"";
            mese = mese.length()>1 ? mese : "0"+mese;
            int day = now.get(Calendar.DAY_OF_MONTH);
            queryDate = year + mese + day ;
        }*/
        try {
            ContractInfoResponseV1 contractInfoResponseV1 = esbService.getContractByContractIdOrPlate(claimsInsertCommand.getComplaint().getPlate(), claimsInsertCommand.getComplaint().getDataAccident().getDateAccident(),null);
            if(contractInfoResponseV1 != null){

             /**************** Controllo se il veicolo è in Manutenzione
                VehicleStatusAuthorityResponseV1 vehicleStatusAuthorityResponseV1 = null;
                try {
                    vehicleStatusAuthorityResponseV1 = authorityService.getVehicleStatus(contractInfoResponseV1.getVehicleResponse().getChassisNumber(), claimsInsertCommand.getComplaint().getDataAccident().getDateAccident());
                } catch (NotFoundException e){
                    LOGGER.debug("Vehicle not found on Authority");
                } catch (IOException e) {
                    LOGGER.debug("Error on Authority call");
                    throw new InternalException(MessageCode.CLAIMS_1103, e);
                }
                if(vehicleStatusAuthorityResponseV1 != null && AuthorityVehicleStatusEnum.UNDER_MAINTENANCE.equals(vehicleStatusAuthorityResponseV1.getStatus())){
                    LOGGER.debug("[MSA] Vehicle with chassis "+vehicleStatusAuthorityResponseV1.getChassis()+" is under maintenance");
                    String message = String.format(MessageCode.CLAIMS_1102.value(), vehicleStatusAuthorityResponseV1.getChassis());
                    throw new BadRequestException(message, MessageCode.CLAIMS_1102);
                } else if (claimsPendingService.checkIfExistsPendingWorking(claimsInsertCommand.getComplaint().getPlate(),claimsInsertCommand.getComplaint().getDataAccident().getDateAccident())) {
                    LOGGER.info("Vehicle with plate "+claimsInsertCommand.getComplaint().getPlate()+" is under maintenance");
                    String message = String.format(MessageCode.CLAIMS_1102.value(), claimsInsertCommand.getComplaint().getPlate());
                    throw new BadRequestException(message, MessageCode.CLAIMS_1102);
                }
                ***************************************/

                damaged.setDriver(DriverAdapter.adptDriverResponseToDriver(contractInfoResponseV1.getDriverResponse()));
                damaged.setContract(ContractAdapter.adptContractResponseToContract(contractInfoResponseV1.getContractResponse()));
                damaged.setVehicle(VehicleAdapter.adptVehicleResponseToVehicle(contractInfoResponseV1.getVehicleResponse()));
                damaged.setCustomer(CustomerAdapter.adptCustomerResponseToCustomer(contractInfoResponseV1.getCustomerResponse()));
                damaged.setFleetManagerList(FleetManagerAdpter.adptFromFMResponseToFM(contractInfoResponseV1.getFleetManagerResponseList()));
                damaged.setInsuranceCompany(InsuranceCompanyAdapter.adptInsuranceCompanyResponseToInsuranceCompany(contractInfoResponseV1.getInsuranceCompany()));
                damaged.setAntiTheftService(new AntiTheftService());
                damaged.getAntiTheftService().setRegistryList(RegistryAdapter.adptFromRegistryResponseToRegistryList(contractInfoResponseV1.getRegistryResponseList()));
            }

        } catch (NotFoundException | ForbiddenException e) {
            LOGGER.debug(e.getMessage());
            throw e;
        } catch (IOException e) {
            LOGGER.debug("[MSA] " + MessageCode.CLAIMS_1018.toString());
            throw new InternalException(MessageCode.CLAIMS_1018, e);
        }

        claimsEntity.setDamaged(damaged);
        if(damaged != null){
            Customer customer = damaged.getCustomer();
            if(customer != null){
                String customerId = customer.getCustomerId();
                if(customerId != null){
                    PersonalDataEntity customerDb = personalDataRepository.findByCustomerId(customerId);
                    //Setto i fleetManager, distinguendo tra quelli importati (passati da front-end) e quelli nel DB (presenti in customerDb)
                    if(customerDb!=null) {
                        damaged.setFleetManagerList(FleetManagerAdpter.adptFromFleetManagerPersonalDataToFleetManager
                                (customerDb.getFleetManagerPersonalData(), damaged.getFleetManagerList()));

                        customerDb = PersonalDataAdapter.adptFromCustomerToPersonalData(customerDb, customer, damaged.getFleetManagerList());
                        //Inserisco nella lista dei damaged i fleetmanager che non sono presenti nei fleet manager di customerdb
                        //damaged.setFleetManagerList(PersonalDataAdapter.buildFinalFleetManagerListofDamaged(customerDb.getFleetManagerPersonalData(),damaged.getFleetManagerList()));
                        personalDataRepository.save(customerDb);
                    }
                }
            }
        }
        claimsEntity.setWithCounterparty(claimsInsertCommand.getWithCounterparty());
        claimsEntity.setRead(true);


        if (claimsInsertCommand.getComplaint() != null) {
            claimsEntity.setComplaint(ComplaintAdapter.adptComplaintExternalRequestToComplaint(claimsInsertCommand.getComplaint()));
            claimsEntity.getComplaint().setClientId(damaged.getCustomer().getCustomerId());
            if(claimsEntity.getComplaint().getFromCompany() == null)
                claimsEntity.getComplaint().setFromCompany(new FromCompany());
            claimsEntity.getComplaint().getFromCompany().setDwlMan(DateUtil.getNowDate());
        }

        //recupero il flusso
        try {
            ContractTypeEntity contractTypeEntity = claimsService.getContractType(claimsEntity.getDamaged().getContract().getContractType());

            if(contractTypeEntity == null){
                LOGGER.debug("[MSA] " +  MessageCode.CLAIMS_1010.value());
                throw new BadRequestException(MessageCode.CLAIMS_1010);
            }

            if (contractTypeEntity.getFlagWS() != null && !contractTypeEntity.getFlagWS()) {
                LOGGER.debug("[MSA] " +  MessageCode.CLAIMS_1042.value());
                throw new BadRequestException(MessageCode.CLAIMS_1042);
            } else {
                claimsEntity.setType(claimsService.getPersonalRiskFlowType(claimsEntity.getDamaged().getContract().getContractType(),
                        claimsEntity.getComplaint().getDataAccident().getTypeAccident(),
                        claimsEntity.getDamaged().getCustomer().toString())); /* tipo di flusso */
                if (claimsEntity.getType().equals(ClaimsFlowEnum.NO)) {
                    LOGGER.debug("[MSA] It's not possible insert a claim with NO flow");
                    throw new BadRequestException("It's not possible insert a claim with NO flow", MessageCode.CLAIMS_1057);
                }

            }
        } catch (BadRequestException e) {
            LOGGER.debug(e.getMessage());
            throw e;

        }


        if (claimsInsertCommand.getCounterparty() != null) {
            String plate = null;
            if (claimsEntity.getDamaged() != null && claimsEntity.getDamaged().getVehicle() != null && claimsEntity.getDamaged().getVehicle().getLicensePlate() != null)
                plate = claimsEntity.getDamaged().getVehicle().getLicensePlate();
            if (plate != null) {
                for (ClaimsInsertExternalRequest.CounterpartyExternalRequest counterpartyRequest : claimsInsertCommand.getCounterparty()) {
                    if (counterpartyRequest.getVehicle() != null && counterpartyRequest.getVehicle().getLicensePlate() != null) {
                        if (counterpartyRequest.getVehicle().getLicensePlate().equals(plate)) {
                            LOGGER.debug("[MSA] " +  MessageCode.CLAIMS_1041.value());
                            throw new BadRequestException(MessageCode.CLAIMS_1041);
                        }
                    }
                }
            }
            claimsEntity.setCounterparts(counterpartyAdapter.adptCounterpartyExternalRequestToCounterparty(claimsInsertCommand.getCounterparty()));
        }

        if (claimsInsertCommand.getDeponent() != null)
            claimsEntity.setDeponentList(DeponentAdapter.adptDeponentRequestToDeponent(claimsInsertCommand.getDeponent()));

        if (claimsInsertCommand.getWounded() != null)
            claimsEntity.setWoundedList(WoundedAdapter.adptWoundedRequestToWounded(claimsInsertCommand.getWounded()));

        if (claimsInsertCommand.getCai() != null)
            claimsEntity.setCaiDetails(CaiAdapter.adptCaiToCaiResponse(claimsInsertCommand.getCai()));
        claimsEntity.setIdSaleforce(claimsInsertCommand.getIdSaleforce());

        if (claimsInsertCommand.getRefund() != null)
            claimsEntity.setRefund(RefundAdapter.adptFromRefundRequestToRefund(claimsInsertCommand.getRefund()));

        if (claimsInsertCommand.getTheft() != null)
            claimsEntity.setTheft(TheftClaimsAdapter.adptTheftExternalRequestToTheft(claimsInsertCommand.getTheft()));

        claimsEntity.setPoVariation(false);
        Instant nowInstant = DateUtil.getNowInstant();
        claimsEntity.setCreatedAt(nowInstant);
        claimsEntity.setUpdateAt(nowInstant);

        claimsEntity.setCompleteDocumentation(claimsInsertCommand.getCompleteDocumentation());
        ClaimsStatusEnum oldStatus = claimsEntity.getStatus();

        claimsEntity.setPaiComunication(false);
        claimsEntity.setForced(false);
        claimsEntity.setInEvidence(false);
        claimsEntity.setWithContinuation(false);
        claimsEntity.setLegalComunication(false);

        if(claimsInsertCommand.getDriverRequest() != null && claimsEntity.getDamaged() != null){
            claimsEntity.getDamaged().setDriver(DriverAdapter.adptDriverRequestToDriver(claimsInsertCommand.getDriverRequest()));
        }

        claimsEntity = claimsService.checkStatusByFlowExternalAndDraft(claimsEntity);
        //REFACTOR
        //conversione nella nuova entità
        ClaimsNewEntity claimsNewEntity = converterClaimsService.convertOldEntityToNewClaimsEntity(claimsEntity);

        claimsNewRepository.save(claimsNewEntity);
        //Per poter recuperare il practice id abbiamo bisogno di salvare prima la pratica è poi recuperarla, in tal modo gli viene assegnato un practice_id
        claimsNewEntity = claimsNewRepository.getOne(claimsNewEntity.getId());

        //converto la nuova entità nella vecchia versione
        claimsEntity = converterClaimsService.wrapFromClaimsNewEntityToClaimsEntity(claimsNewEntity);

        List<EmailTemplateMessagingRequestV1> emailTemplateList = MessagingAdapter.adptEmailTemplateMessagingRequestToEmailTemplateMessagingResponseList(messagingService.getMailTemplateByTypeEvent(EventTypeEnum.COMPLAINT_INSERTION, claimsEntity));
        String description = messagingService.sendMailAndCreateLogs(emailTemplateList, null, claimsEntity.getCreatedAt());
        Historical historical = new Historical(EventTypeEnum.COMPLAINT_INSERTION, claimsEntity.getStatus(), claimsEntity.getStatus(), claimsEntity.getComplaint().getDataAccident().getTypeAccident(), claimsEntity.getComplaint().getDataAccident().getTypeAccident(), DateUtil.getNowDate(), claimsEntity.getUserId(), claimsInsertCommand.getUserName(), claimsEntity.getType().getValue(), claimsEntity.getType().getValue(), description);
        claimsEntity.addHistorical(historical);

/*
       NE-1372  nel caso di feriti non deve essere inviata la PAI
        try {
            List<EmailTemplateMessagingRequestV1> emailTemplatePaiList = null;
            if (claimsService.isPai(claimsEntity)) {
                List<EmailTemplateMessagingRequestV1> listTos = new LinkedList<>();
                List<EmailTemplateMessagingRequestV1> listNotTos = new LinkedList<>();
                emailTemplatePaiList = MessagingAdapter.adptEmailTemplateMessagingRequestToEmailTemplateMessagingResponseList(messagingService.getMailTemplateByTypeEvent(EventTypeEnum.ENTRUST_PAI, claimsEntity));
                if (emailTemplatePaiList != null) {
                    for (EmailTemplateMessagingRequestV1 currentTemplate : emailTemplatePaiList) {
                        if (currentTemplate.getTos() != null && !currentTemplate.getTos().isEmpty() && currentTemplate.getObject()!= null && !currentTemplate.getObject().isEmpty()) {
                            listTos.add(currentTemplate);
                        } else {
                            listNotTos.add(currentTemplate);
                        }
                    }
                    List<EmailTemplateMessagingRequestV1> emailTemplateMessaging = messagingService.splitEmailIfContainsUrl(listTos, claimsInsertCommand.getClaimsId());
                    emailTemplateMessaging.addAll(listNotTos);

                    String descriptionPai = messagingService.sendMailAndCreateLogs(emailTemplateMessaging, null, claimsEntity.getCreatedAt());
                    claimsEntity.setPaiComunication(true);
                    Historical historicalPai = new Historical(EventTypeEnum.ENTRUST_PAI, claimsEntity.getStatus(), claimsEntity.getStatus(), claimsEntity.getComplaint().getDataAccident().getTypeAccident(), claimsEntity.getComplaint().getDataAccident().getTypeAccident(), DateUtil.getNowDate(), claimsEntity.getUserId(), claimsInsertCommand.getUserName(), claimsEntity.getType().getValue(), claimsEntity.getType().getValue(), descriptionPai);
                    claimsEntity.addHistorical(historicalPai);
                }

            }
        }catch(Exception ex){
            LOGGER.debug("[MSA] " +  MessageCode.CLAIMS_1151.value(), ex);
        }*/

        if (claimsEntity.getType() != null && claimsEntity.getComplaint() != null && claimsEntity.getComplaint().getDataAccident() != null && claimsEntity.getComplaint().getDataAccident().getTypeAccident() != null) {
            RecoverabilityEntity recoverabilityEntity = recoverabilityService.getRecoverabilityByFlowAndTypeClaim(claimsEntity.getType().getValue().toUpperCase(), claimsEntity.getComplaint().getDataAccident().getTypeAccident());
            LOGGER.info("[RECOVERABILITY] "  + recoverabilityEntity);
            if(recoverabilityEntity != null) {
                claimsEntity.getComplaint().getDataAccident().setRecoverability(recoverabilityEntity.getRecoverability());
                claimsEntity.getComplaint().getDataAccident().setRecoverabilityPercent(recoverabilityEntity.getPercentRecoverability());
            }
        }
        claimsEntity.setMigrated(false);

        String codAntiTheftService = "";
        damaged = claimsEntity.getDamaged();
        AntiTheftService antiTheftService = null;
        if (damaged.getAntiTheftService() != null) {

            //recupero localizzatore da tabella setting AntiTheftServiceEntity
            //da capire se avremo mai un localizzatore per il furto e se ci viene passato un tipo
            if (damaged.getAntiTheftService().getRegistryList() != null && !damaged.getAntiTheftService().getRegistryList().isEmpty()) {

                //for(Registry currentRegistry : damaged.getAntiTheftService().getRegistryList()){
                Registry currentRegistry = damaged.getAntiTheftService().getRegistryList().get(0);
                codAntiTheftService = AntiTheftServiceAdapter.adptProviderToAntiTheftServiceEntity(currentRegistry.getCodPack());
                AntiTheftServiceEntity antiTheftServiceEntity = antiTheftServiceRepository.findAntiTheftByCodAntiTheftService(codAntiTheftService);
                antiTheftService = AntiTheftServiceAdapter.adptAntiTheftServiceEntityToAntiTheftService(antiTheftServiceEntity);
                if(antiTheftService != null) {
                    antiTheftService.setRegistryList(damaged.getAntiTheftService().getRegistryList());
                    claimsEntity.getDamaged().setAntiTheftService(antiTheftService);
                }

            }
        }


        //converto nella nuova entità
        claimsNewEntity = converterClaimsService.convertOldEntityToNewClaimsEntity(claimsEntity);

        claimsNewRepository.save(claimsNewEntity);

        DogeResponseV1 dogeResponseV1 = null;
        try {
            if(
                    claimsEntity.getComplaint() != null &&
                    claimsEntity.getComplaint().getDataAccident() != null &&
                    claimsEntity.getComplaint().getDataAccident().getTypeAccident() != null &&
                    (claimsEntity.getComplaint().getDataAccident().getTypeAccident().equals(DataAccidentTypeAccidentEnum.FURTO_TOTALE) ||
                    claimsEntity.getComplaint().getDataAccident().getTypeAccident().equals(DataAccidentTypeAccidentEnum.FURTO_E_RITROVAMENTO))
            ){
                //REFACTOR
                dogeResponseV1 = dogeEnquService.enqueueFileTheftSummary(claimsInsertCommand.getClaimsId(),claimsInsertCommand.getUserName());
            }else{
                //REFACTOR
                dogeResponseV1 = dogeEnquService.enqueueClaimsWithoutCounterpart(claimsInsertCommand.getClaimsId());
            }

            fileManagerService.attachPdfFileClaim(claimsInsertCommand.getClaimsId(), dogeResponseV1.getDocumentId(), claimsInsertCommand.getUserId(), claimsInsertCommand.getUserName(), false, false);
        } catch (IOException e) {
            LOGGER.debug("[MSA] Error on doge call");
        }

        if(dwhCall){
            dwhClaimsService.sendMessage(claimsEntity, com.doing.nemo.dwh.client.payload.request.EventTypeEnum.CREATED);
            if(claimsNewEntity.getCounterparts() != null) {
                for (CounterpartyNewEntity newCounterparty : claimsNewEntity.getCounterparts()) {
                    CounterpartyEntity counterparty = CounterpartyAdapter.adptCounterpartyNewToCounterpartyOld(newCounterparty);
                    dwhClaimsService.sendMessage(counterparty, com.doing.nemo.dwh.client.payload.request.EventTypeEnum.CREATED);
                }
            }
        }

        try {
            //intervento 6 - R
            externalCommunicationService.insertIncidentAsync(claimsInsertCommand.getClaimsId(), null);
        } catch (IOException e) {
            LOGGER.debug(MessageCode.CLAIMS_1112.value());
            throw new BadRequestException(MessageCode.CLAIMS_1112, e);
        }

        //claimsRepository.save(claimsEntity);
        cacheService.deleteClaimsStats(CACHENAME, STATSkEY);
        cacheService.deleteClaimsStats(CACHENAME, STATSEVIDENCE);
        if (claimsEntity.getStatus().equals(ClaimsStatusEnum.WAITING_FOR_VALIDATION) || claimsEntity.getStatus().equals(ClaimsStatusEnum.TO_ENTRUST)) {
            cacheService.deleteClaimsStats(CACHENAME, STATSSTATUS);
        }

        claimsNewEntity = claimsNewRepository.getOne(claimsInsertCommand.getClaimsId());

        if (antiTheftService != null) {

            // creazione della riga di storico di octo/texa
            String idAntiTheftRequest = UUID.randomUUID().toString();
            antiTheftRequestService.insertAntiTheftRequest(idAntiTheftRequest, claimsNewEntity, antiTheftService.getProviderType(), "1");
            claimsService.callToOctoAsync(claimsNewEntity, claimsNewEntity.getStatus(), new AntiTheftRequest(), claimsInsertCommand.getUserId(), claimsInsertCommand.getUserName(), idAntiTheftRequest);


        }

    }
}
