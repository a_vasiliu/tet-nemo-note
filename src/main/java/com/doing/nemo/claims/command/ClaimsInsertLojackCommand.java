package com.doing.nemo.claims.command;

import com.doing.nemo.claims.common.commandBus.Command;
import com.doing.nemo.claims.controller.payload.request.CounterpartyRequest;
import com.doing.nemo.claims.controller.payload.request.claims.*;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ClaimsInsertLojackCommand implements Command {

    private String claimsId;
    private String userId;
    private String userName;
    private ClaimsStatusEnum status;
    private Boolean isWithCounterparty;
    private Boolean inEvidence;
    private ComplaintRequest complaint;
    private DamagedRequest damaged;
    private List<CounterpartyRequest> counterparty;
    private List<DeponentRequest> deponent;
    private List<WoundedRequest> wounded;
    private Boolean caiComunication;
    private Boolean forced;
    private CaiRequest cai;
    private Long idSaleforce;
    private RefundRequest refund;
    private Map<String,Object> metadata;
    private TheftRequestV1 theft;
    private Boolean isCompleteDocumentation ;


    public ClaimsInsertLojackCommand(String claimsId, String userId, String userName, ClaimsStatusEnum status, Boolean isWithCounterparty, Boolean inEvidence, ComplaintRequest complaint, DamagedRequest damaged, List<CounterpartyRequest> counterparty, List<DeponentRequest> deponent, List<WoundedRequest> wounded, Boolean caiComunication, Boolean forced, CaiRequest cai, Long idSaleforce, RefundRequest refund, Map<String, Object> metadata, TheftRequestV1 theft,Boolean isCompleteDocumentation ) {
        this.claimsId = claimsId;
        this.userId = userId;
        this.status = status;
        this.userName = userName;
        this.isWithCounterparty = isWithCounterparty;
        this.inEvidence = inEvidence;
        this.complaint = complaint;
        this.damaged = damaged;
        if(counterparty != null)
        {
            this.counterparty = new ArrayList<>(counterparty);
        }
        if(deponent != null)
        {
            this.deponent = new ArrayList<>(deponent);
        }
        if(wounded != null)
        {
            this.wounded = new ArrayList<>(wounded);
        }
        this.caiComunication = caiComunication;
        this.forced = forced;
        this.cai = cai;
        this.idSaleforce = idSaleforce;
        this.refund = refund;
        this.metadata = metadata;
        this.theft = theft;
        this.isCompleteDocumentation = isCompleteDocumentation ;
    }

    public Boolean getCompleteDocumentation() {
        return isCompleteDocumentation;
    }

    public void setCompleteDocumentation(Boolean completeDocumentation) {
        isCompleteDocumentation = completeDocumentation;
    }

    public TheftRequestV1 getTheft() {
        return theft;
    }

    public void setTheft(TheftRequestV1 theft) {
        this.theft = theft;
    }

    public String getClaimsId() {
        return claimsId;
    }

    public void setClaimsId(String claimsId) {
        this.claimsId = claimsId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public ClaimsStatusEnum getStatus() {
        return status;
    }

    public void setStatus(ClaimsStatusEnum status) {
        this.status = status;
    }

    public Boolean getWithCounterparty() {
        return isWithCounterparty;
    }

    public void setWithCounterparty(Boolean withCounterparty) {
        isWithCounterparty = withCounterparty;
    }

    public ComplaintRequest getComplaint() {
        return complaint;
    }

    public void setComplaint(ComplaintRequest complaint) {
        this.complaint = complaint;
    }

    public DamagedRequest getDamaged() {
        return damaged;
    }

    public void setDamaged(DamagedRequest damaged) {
        this.damaged = damaged;
    }

    public List<CounterpartyRequest> getCounterparty() {
        if(counterparty == null){
            return null;
        }
        return new ArrayList<>(counterparty);
    }

    public void setCounterparty(List<CounterpartyRequest> counterparty)
    {
        if(counterparty != null)
        {
            this.counterparty = new ArrayList<>(counterparty);
        }else {
            this.counterparty = null;
        }
    }

    public List<DeponentRequest> getDeponent() {
        if(deponent == null){
            return null;
        }
        return new ArrayList<>(deponent);
    }

    public void setDeponent(List<DeponentRequest> deponent) {
        if(deponent != null){
            this.deponent = new ArrayList<>(deponent);
        }else {
            this.deponent = null;
        }
    }

    public List<WoundedRequest> getWounded() {
        if(wounded == null){
            return null;
        }
        return new ArrayList<>(wounded);
    }

    public void setWounded(List<WoundedRequest> wounded) {
        if(wounded != null)
        {
            this.wounded =new ArrayList<>(wounded);
        }else {
            this.wounded = null;
        }
    }

    public Boolean getCaiComunication() {
        return caiComunication;
    }

    public void setCaiComunication(Boolean caiComunication) {
        this.caiComunication = caiComunication;
    }

    public Boolean getForced() {
        return forced;
    }

    public void setForced(Boolean forced) {
        this.forced = forced;
    }

    public Boolean getInEvidence() {
        return inEvidence;
    }

    public void setInEvidence(Boolean inEvidence) {
        this.inEvidence = inEvidence;
    }

    public Long getIdSaleforce() {
        return idSaleforce;
    }

    public void setIdSaleforce(Long idSaleforce) {
        this.idSaleforce = idSaleforce;
    }

    public CaiRequest getCai() {
        return cai;
    }

    public void setCai(CaiRequest cai) {
        this.cai = cai;
    }

    public RefundRequest getRefund() {
        return refund;
    }

    public void setRefund(RefundRequest refund) {
        this.refund = refund;
    }

    public Map<String,Object> getMetadata() {
        return metadata;
    }

    public void setMetadata(Map<String,Object> metadata) {
        this.metadata = metadata;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ClaimsInsertLojackCommand{");
        sb.append("claimsId='").append(claimsId).append('\'');
        sb.append(", userId='").append(userId).append('\'');
        sb.append(", userName='").append(userName).append('\'');
        sb.append(", status=").append(status);
        sb.append(", isWithCounterparty=").append(isWithCounterparty);
        sb.append(", inEvidence=").append(inEvidence);
        sb.append(", complaint=").append(complaint);
        sb.append(", damaged=").append(damaged);
        sb.append(", counterparty=").append(counterparty);
        sb.append(", deponent=").append(deponent);
        sb.append(", wounded=").append(wounded);
        sb.append(", caiComunication=").append(caiComunication);
        sb.append(", forced=").append(forced);
        sb.append(", cai=").append(cai);
        sb.append(", idSaleforce=").append(idSaleforce);
        sb.append(", refund=").append(refund);
        sb.append(", metadata=").append(metadata);
        sb.append(", theft=").append(theft);
        sb.append(", isCompleteDocumentation=").append(isCompleteDocumentation);
        sb.append('}');
        return sb.toString();
    }
}
