package com.doing.nemo.claims.command;

import com.doing.nemo.claims.common.commandBus.Command;
import com.doing.nemo.claims.controller.payload.request.CounterpartyPatchStatusRequestV1;
import com.doing.nemo.claims.dto.GatewayProfilesMap;
import com.doing.nemo.claims.entity.enumerated.RepairEnum.RepairStatusEnum;

public class CounterpartyPatchStatusRepairCommand implements Command {

    private String claimsId;
    private String counterpartyId;
    private String userId;
    private String userName;
    private String userLastName;
    private RepairStatusEnum nextStatusRepair;
    private CounterpartyPatchStatusRequestV1 counterpartyPatchStatusRequestV1;
    private String profiles;

    public CounterpartyPatchStatusRepairCommand() {
    }

    public CounterpartyPatchStatusRepairCommand(String claimsId, String counterpartyId, String userId, String userName, RepairStatusEnum nextStatusRepair, CounterpartyPatchStatusRequestV1 counterpartyPatchStatusRequestV1, String userLastName, String profiles) {
        this.claimsId = claimsId;
        this.counterpartyId = counterpartyId;
        this.userId = userId;
        this.userName = userName;
        this.nextStatusRepair = nextStatusRepair;
        this.counterpartyPatchStatusRequestV1 = counterpartyPatchStatusRequestV1;
        this.userLastName = userLastName;
        this.profiles = profiles;
    }

    public String getUserLastName() {
        return userLastName;
    }

    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getClaimsId() {
        return claimsId;
    }

    public void setClaimsId(String claimsId) {
        this.claimsId = claimsId;
    }

    public String getCounterpartyId() {
        return counterpartyId;
    }

    public void setCounterpartyId(String counterpartyId) {
        this.counterpartyId = counterpartyId;
    }

    public RepairStatusEnum getNextStatusRepair() {
        return nextStatusRepair;
    }

    public void setNextStatusRepair(RepairStatusEnum nextStatusRepair) {
        this.nextStatusRepair = nextStatusRepair;
    }

    public CounterpartyPatchStatusRequestV1 getCounterpartyPatchStatusRequestV1() {
        return counterpartyPatchStatusRequestV1;
    }

    public void setCounterpartyPatchStatusRequestV1(CounterpartyPatchStatusRequestV1 counterpartyPatchStatusRequestV1) {
        this.counterpartyPatchStatusRequestV1 = counterpartyPatchStatusRequestV1;
    }


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getProfiles() {
        return profiles;
    }

    public void setProfiles(String profiles) {
        this.profiles = profiles;
    }

    @Override
    public String toString() {
        return "CounterpartyPatchStatusRepairCommand{" +
                "claimsId='" + claimsId + '\'' +
                ", counterpartyId='" + counterpartyId + '\'' +
                ", nextStatusRepair=" + nextStatusRepair +
                ", counterpartyPatchStatusRequestV1=" + counterpartyPatchStatusRequestV1 +
                '}';
    }
}
