package com.doing.nemo.claims.command;

import com.doing.nemo.claims.common.commandBus.Command;
import com.doing.nemo.claims.controller.payload.request.claims.DamagedRequest;

public class ClaimsPatchContractInformationCommand implements Command {

    private String id;
    private DamagedRequest damaged;
    private String userId;
    private String userName;

    public ClaimsPatchContractInformationCommand(String id, DamagedRequest damaged, String userId, String userName) {
        this.id = id;
        this.damaged = damaged;
        this.userId = userId;
        this.userName = userName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public DamagedRequest getDamaged() {
        return damaged;
    }

    public void setDamaged(DamagedRequest damaged) {
        this.damaged = damaged;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public String toString() {
        return "ClaimsPatchContractInformationCommand{" +
                "id='" + id + '\'' +
                ", damaged=" + damaged +
                ", userId='" + userId + '\'' +
                ", userName='" + userName + '\'' +
                '}';
    }
}
