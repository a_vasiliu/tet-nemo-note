package com.doing.nemo.claims.command;

import com.doing.nemo.claims.common.commandBus.Command;
import com.doing.nemo.claims.entity.jsonb.Notes;

public class ClaimsInsertNoteCommand implements Command {

    private String claimsId;
    private String userId;
    private String userName;
    private String noteId;
    private Notes note;

    public ClaimsInsertNoteCommand() {
    }

    public ClaimsInsertNoteCommand(String claimsId, String userId, String userName, String noteId, Notes note) {
        this.claimsId = claimsId;
        this.userId = userId;
        this.userName = userName;
        this.noteId = noteId;
        this.note = note;
        this.note.setNoteId(noteId);
        this.note.setUserId(userId);
    }

    public String getClaimsId() {
        return claimsId;
    }

    public void setClaimsId(String claimsId) {
        this.claimsId = claimsId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getNoteId() {
        return noteId;
    }

    public void setNoteId(String noteId) {
        this.noteId = noteId;
    }

    public Notes getNote() {
        return note;
    }

    public void setNote(Notes note) {
        this.note = note;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public String toString() {
        return "ClaimsInsertNoteCommand{" +
                "claimsId='" + claimsId + '\'' +
                ", userId='" + userId + '\'' +
                ", userName='" + userName + '\'' +
                ", noteId='" + noteId + '\'' +
                ", note=" + note +
                '}';
    }
}
