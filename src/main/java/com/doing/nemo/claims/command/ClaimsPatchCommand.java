package com.doing.nemo.claims.command;

import com.doing.nemo.claims.common.commandBus.Command;
import com.doing.nemo.claims.controller.payload.request.CounterpartyRequest;
import com.doing.nemo.claims.controller.payload.request.claims.*;
import com.doing.nemo.claims.controller.payload.request.exemption.ExemptionRequest;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class ClaimsPatchCommand implements Command {


    private String id;
    private Instant dataUpdate;
    private Boolean paiComunication;
    private Boolean isWithCounterparty;
    private ComplaintRequest complaint;
    private FormRequest forms;
    private DamagedRequest damaged;
    private CaiRequest caiDetails;
    private ExemptionRequest exemption;
    private List<CounterpartyRequest> counterparty;
    private List<DeponentRequest> deponent;
    private List<WoundedRequest> wounded;
    private List<NoteRequest> notes;
    private FoundModelRequest foundModel;
    private Long idSaleforce;
    private RefundRequest refund;
    private TheftRequestV1 theft;
    private String userId;
    private String userName;
    private Boolean poVariation;
    private Boolean legalComunication;
    private Boolean isCompleteDocumentation ;
    

    public ClaimsPatchCommand(String id, Instant dataUpdate, Boolean paiComunication, Boolean isWithCounterparty, ComplaintRequest complaint, FormRequest forms, DamagedRequest damaged, CaiRequest caiDetails, ExemptionRequest exemption, List<CounterpartyRequest> counterparty, List<DeponentRequest> deponent, List<WoundedRequest> wounded, List<NoteRequest> notes, FoundModelRequest foundModel, Long idSaleforce, RefundRequest refund, TheftRequestV1 theft, String userId, String userName, Boolean poVariation, Boolean legalComunication,  Boolean isCompleteDocumentation) {
        this.id = id;
        this.dataUpdate = dataUpdate;
        this.paiComunication = paiComunication;
        this.isWithCounterparty = isWithCounterparty;
        this.complaint = complaint;
        this.forms = forms;
        this.damaged = damaged;
        this.caiDetails = caiDetails;
        this.exemption = exemption;
        if(counterparty != null)
        {
            this.counterparty = new ArrayList<>(counterparty);
        }
        if(deponent != null)
        {
            this.deponent = new ArrayList<>(deponent);
        }
        if(wounded != null)
        {
            this.wounded = new ArrayList<>(wounded);
        }
        if(notes != null)
        {
            this.notes = new ArrayList<>(notes);
        }
        this.foundModel = foundModel;
        this.idSaleforce = idSaleforce;
        this.refund = refund;
        this.theft = theft;
        this.userId=userId;
        this.userName = userName;
        this.poVariation = poVariation;
        this.legalComunication = legalComunication;
        this.isCompleteDocumentation = isCompleteDocumentation;
    }


    public Boolean getCompleteDocumentation() {
        return isCompleteDocumentation;
    }

    public void setCompleteDocumentation(Boolean completeDocumentation) {
        isCompleteDocumentation = completeDocumentation;
    }

    public Boolean getLegalComunication() {
        return legalComunication;
    }

    public void setLegalComunication(Boolean legalComunication) {
        this.legalComunication = legalComunication;
    }

    public Boolean getPoVariation() {
        return poVariation;
    }

    public void setPoVariation(Boolean poVariation) {
        this.poVariation = poVariation;
    }

    public ExemptionRequest getExemption() {
        return exemption;
    }

    public void setExemption(ExemptionRequest exemption) {
        this.exemption = exemption;
    }

    public TheftRequestV1 getTheft() {
        return theft;
    }

    public void setTheft(TheftRequestV1 theft) {
        this.theft = theft;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Instant getDataUpdate() {
        return dataUpdate;
    }

    public void setDataUpdate(Instant dataUpdate) {
        this.dataUpdate = dataUpdate;
    }

    public Boolean getPaiComunication() {
        return paiComunication;
    }

    public void setPaiComunication(Boolean paiComunication) {
        this.paiComunication = paiComunication;
    }

    public Boolean getWithCounterparty() {
        return isWithCounterparty;
    }

    public void setWithCounterparty(Boolean withCounterparty) {
        isWithCounterparty = withCounterparty;
    }

    public ComplaintRequest getComplaint() {
        return complaint;
    }

    public void setComplaint(ComplaintRequest complaint) {
        this.complaint = complaint;
    }

    public FormRequest getForms() {
        return forms;
    }

    public void setForms(FormRequest forms) {
        this.forms = forms;
    }

    public DamagedRequest getDamaged() {
        return damaged;
    }

    public void setDamaged(DamagedRequest damaged) {
        this.damaged = damaged;
    }

    public CaiRequest getCaiDetails() {
        return caiDetails;
    }

    public void setCaiDetails(CaiRequest caiDetails) {
        this.caiDetails = caiDetails;
    }

    public List<CounterpartyRequest> getCounterparty() {
        if(counterparty == null)
        {
            return null;
        }
        return new ArrayList<>(counterparty);
    }

    public void setCounterparty(List<CounterpartyRequest> counterparty) {
        if(counterparty != null)
        {
            this.counterparty = new ArrayList<>(counterparty);
        }else {
            this.counterparty = null;
        }
    }

    public List<DeponentRequest> getDeponent() {
        if(deponent == null){
            return null;
        }
        return new ArrayList<>(deponent);
    }

    public void setDeponent(List<DeponentRequest> deponent) {
        if(deponent != null)
        {
            this.deponent = new ArrayList<>(deponent);
        }else {
            this.deponent = null;
        }
    }

    public List<WoundedRequest> getWounded() {
        if(wounded == null){
            return null;
        }
        return new ArrayList<>(wounded);
    }

    public void setWounded(List<WoundedRequest> wounded) {
        if(wounded != null)
        {
            this.wounded = new ArrayList<>(wounded);
        }else {
            this.wounded = null;
        }
    }

    public List<NoteRequest> getNotes() {
        if(notes == null){
            return null;
        }
        return new ArrayList<>(notes);
    }

    public void setNotes(List<NoteRequest> notes) {
        if(notes != null){
            this.notes = new ArrayList<>(notes);
        }else {
            this.notes = null;
        }
    }

    public FoundModelRequest getFoundModel() {
        return foundModel;
    }

    public void setFoundModel(FoundModelRequest foundModel) {
        this.foundModel = foundModel;
    }

    public Long getIdSaleforce() {
        return idSaleforce;
    }

    public void setIdSaleforce(Long idSaleforce) {
        this.idSaleforce = idSaleforce;
    }

    public RefundRequest getRefund() {
        return refund;
    }

    public void setRefund(RefundRequest refund) {
        this.refund = refund;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ClaimsPatchCommand{");
        sb.append("id='").append(id).append('\'');
        sb.append(", dataUpdate=").append(dataUpdate);
        sb.append(", paiComunication=").append(paiComunication);
        sb.append(", isWithCounterparty=").append(isWithCounterparty);
        sb.append(", complaint=").append(complaint);
        sb.append(", forms=").append(forms);
        sb.append(", damaged=").append(damaged);
        sb.append(", caiDetails=").append(caiDetails);
        sb.append(", exemption=").append(exemption);
        sb.append(", counterparty=").append(counterparty);
        sb.append(", deponent=").append(deponent);
        sb.append(", wounded=").append(wounded);
        sb.append(", notes=").append(notes);
        sb.append(", foundModel=").append(foundModel);
        sb.append(", idSaleforce=").append(idSaleforce);
        sb.append(", refund=").append(refund);
        sb.append(", theft=").append(theft);
        sb.append(", userId='").append(userId).append('\'');
        sb.append(", userName='").append(userName).append('\'');
        sb.append(", poVariation=").append(poVariation);
        sb.append(", legalComunication=").append(legalComunication);
        sb.append(", isCompleteDocumentation=").append(isCompleteDocumentation);
        sb.append('}');
        return sb.toString();
    }
}
