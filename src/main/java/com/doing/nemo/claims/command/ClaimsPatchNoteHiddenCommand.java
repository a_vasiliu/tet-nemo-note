package com.doing.nemo.claims.command;

import com.doing.nemo.claims.common.commandBus.Command;

public class ClaimsPatchNoteHiddenCommand implements Command {

    private String claimsId;
    private String noteId;

    public ClaimsPatchNoteHiddenCommand() {
    }

    public ClaimsPatchNoteHiddenCommand(String claimsId, String noteId) {
        this.claimsId = claimsId;
        this.noteId = noteId;
    }

    public String getUuid() {
        return claimsId;
    }

    public void setUuid(String claimsId) {
        this.claimsId = claimsId;
    }

    public String getNoteId() {
        return noteId;
    }

    public void setNoteId(String noteId) {
        this.noteId = noteId;
    }

    @Override
    public String toString() {
        return "ClaimsPatchNoteStatusCommand{" +
                "claims_dd=" + claimsId +
                ", note_id=" + noteId +
                '}';
    }
}
