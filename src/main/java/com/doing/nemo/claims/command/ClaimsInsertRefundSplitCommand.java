package com.doing.nemo.claims.command;

import com.doing.nemo.claims.common.commandBus.Command;
import com.doing.nemo.claims.entity.jsonb.refund.Split;

public class ClaimsInsertRefundSplitCommand implements Command {

    private String claimsId;
    private Split split;

    public ClaimsInsertRefundSplitCommand() {
    }

    public ClaimsInsertRefundSplitCommand(String claimsId, Split split) {
        this.claimsId = claimsId;
        this.split = split;
    }

    public String getClaimsId() {
        return claimsId;
    }

    public void setClaimsId(String claimsId) {
        this.claimsId = claimsId;
    }

    public Split getSplit() {
        return split;
    }

    public void setSplit(Split split) {
        this.split = split;
    }


    @Override
    public String toString() {
        return "ClaimsInsertRefundSplitCommand{" +
                "claimsId='" + claimsId + '\'' +
                ", split=" + split +
                '}';
    }
}
