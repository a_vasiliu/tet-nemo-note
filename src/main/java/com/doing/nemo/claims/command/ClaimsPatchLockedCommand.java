package com.doing.nemo.claims.command;

import com.doing.nemo.claims.common.commandBus.Command;

public class ClaimsPatchLockedCommand implements Command {

    private String id;

    public ClaimsPatchLockedCommand(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "ClaimsPatchLockedCommand{" +
                "id='" + id + '\'' +
                '}';
    }
}
