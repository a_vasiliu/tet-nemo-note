package com.doing.nemo.claims.command;

import com.doing.nemo.claims.common.commandBus.Command;
import com.doing.nemo.claims.controller.payload.request.AssigneeRepairRequestV1;
import com.doing.nemo.claims.controller.payload.request.InsuranceCompanyCounterpartyRequest;
import com.doing.nemo.claims.controller.payload.request.ManagerRequestV1;
import com.doing.nemo.claims.controller.payload.request.counterparty.*;
import com.doing.nemo.claims.entity.enumerated.DamagedEnum.VehicleEnum.VehicleTypeEnum;
import com.doing.nemo.claims.entity.enumerated.RepairEnum.ManagementTypeEnum;
import com.doing.nemo.claims.entity.enumerated.RepairEnum.RepairProcedureEnum;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CounterpartyPatchCommand implements Command {
    private String claimsId;
    private String counterpartyId;
    private VehicleTypeEnum type;
    private RepairProcedureEnum repairProcedure;
    private String userUpdate;
    private AssigneeRepairRequestV1 assignedTo;
    private Date assignedAt;
    private InsuredRequest insured;
    private InsuranceCompanyCounterpartyRequest insuranceCompany;
    private DriverRequest driver;
    private VehicleRequest vehicle;
    private Boolean isCaiSigned;
    private ImpactPointRequest impactPoint;
    private Boolean responsible;
    private Boolean eligibility;
    private String description;
    private List<LastContactRequest> lastContact;
    private ManagerRequestV1 manager;
    private CanalizationRequest canalization;
    private String policyNumber;
    private String policyBeginningValidity;
    private String policyEndValidity;
    private ManagementTypeEnum managementType;
    private Boolean askedForDamages;
    private Boolean legalOrConsultant;
    private String dateRequestDamages;
    private String expirationDate;
    private String legal;
    private String emailLegal;
    private Boolean isAld;
    private Boolean replacementCar;
    private String replacementPlate;
    private Boolean isCompleteDocumentation;


    public CounterpartyPatchCommand(String claimsId, String counterpartyId, VehicleTypeEnum type, RepairProcedureEnum repairProcedure, String userUpdate, AssigneeRepairRequestV1 assignedTo, Date assignedAt, InsuredRequest insured, InsuranceCompanyCounterpartyRequest insuranceCompany, DriverRequest driver, VehicleRequest vehicle, Boolean isCaiSigned, ImpactPointRequest impactPoint, Boolean responsible, Boolean eligibility, String description, List<LastContactRequest> lastContact, ManagerRequestV1 manager, CanalizationRequest canalization, String policyNumber, String policyBeginningValidity, String policyEndValidity, ManagementTypeEnum managementType, Boolean askedForDamages, Boolean legalOrConsultant, String dateRequestDamages, String expirationDate, String legal, String emailLegal, Boolean isAld, Boolean replacementCar, String replacementPlate, Boolean isCompleteDocumentation ) {
        this.claimsId = claimsId;
        this.counterpartyId = counterpartyId;
        this.type = type;
        this.repairProcedure = repairProcedure;
        this.userUpdate = userUpdate;
        this.assignedTo = assignedTo;
        if(assignedAt != null)
        {
            this.assignedAt = (Date)assignedAt.clone();
        }
        this.insured = insured;
        this.insuranceCompany = insuranceCompany;
        this.driver = driver;
        this.vehicle = vehicle;
        this.isCaiSigned = isCaiSigned;
        this.impactPoint = impactPoint;
        this.responsible = responsible;
        this.eligibility = eligibility;
        this.description = description;
        if(lastContact != null)
        {
            this.lastContact = new ArrayList<>(lastContact);
        }
        this.manager = manager;
        this.canalization = canalization;
        this.policyNumber = policyNumber;
        this.policyBeginningValidity = policyBeginningValidity;
        this.policyEndValidity = policyEndValidity;
        this.managementType = managementType;
        this.askedForDamages = askedForDamages;
        this.legalOrConsultant = legalOrConsultant;
        this.dateRequestDamages = dateRequestDamages;
        this.expirationDate = expirationDate;
        this.legal = legal;
        this.emailLegal = emailLegal;
        this.isAld = isAld;
        this.replacementCar = replacementCar;
        this.replacementPlate = replacementPlate;
        this.isCompleteDocumentation = isCompleteDocumentation;
    }

    public Boolean getCompleteDocumentation() {
        return isCompleteDocumentation;
    }

    public void setCompleteDocumentation(Boolean completeDocumentation) {
        isCompleteDocumentation = completeDocumentation;
    }

    public String getReplacementPlate() {
        return replacementPlate;
    }

    public void setReplacementPlate(String replacementPlate) {
        this.replacementPlate = replacementPlate;
    }

    public Boolean getReplacementCar() {
        return replacementCar;
    }

    public void setReplacementCar(Boolean replacementCar) {
        this.replacementCar = replacementCar;
    }

    public ManagementTypeEnum getManagementType() {
        return managementType;
    }

    public void setManagementType(ManagementTypeEnum managementType) {
        this.managementType = managementType;
    }

    public Boolean getAskedForDamages() {
        return askedForDamages;
    }

    public void setAskedForDamages(Boolean askedForDamages) {
        this.askedForDamages = askedForDamages;
    }

    public Boolean getLegalOrConsultant() {
        return legalOrConsultant;
    }

    public void setLegalOrConsultant(Boolean legalOrConsultant) {
        this.legalOrConsultant = legalOrConsultant;
    }

    public String getDateRequestDamages() {
        return dateRequestDamages;
    }

    public void setDateRequestDamages(String dateRequestDamages) {
        this.dateRequestDamages = dateRequestDamages;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getLegal() {
        return legal;
    }

    public void setLegal(String legal) {
        this.legal = legal;
    }

    public String getEmailLegal() {
        return emailLegal;
    }

    public void setEmailLegal(String emailLegal) {
        this.emailLegal = emailLegal;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public String getPolicyBeginningValidity() {
        return policyBeginningValidity;
    }

    public void setPolicyBeginningValidity(String policyBeginningValidity) {
        this.policyBeginningValidity = policyBeginningValidity;
    }

    public String getPolicyEndValidity() {
        return policyEndValidity;
    }

    public void setPolicyEndValidity(String policyEndValidity) {
        this.policyEndValidity = policyEndValidity;
    }

    public String getClaimsId() {
        return claimsId;
    }

    public void setClaimsId(String claimsId) {
        this.claimsId = claimsId;
    }

    public String getCounterpartyId() {
        return counterpartyId;
    }

    public void setCounterpartyId(String counterpartyId) {
        this.counterpartyId = counterpartyId;
    }

    public VehicleTypeEnum getType() {
        return type;
    }

    public void setType(VehicleTypeEnum type) {
        this.type = type;
    }

    public RepairProcedureEnum getRepairProcedure() {
        return repairProcedure;
    }

    public void setRepairProcedure(RepairProcedureEnum repairProcedure) {
        this.repairProcedure = repairProcedure;
    }

    public String getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(String userUpdate) {
        this.userUpdate = userUpdate;
    }

    public AssigneeRepairRequestV1 getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(AssigneeRepairRequestV1 assignedTo) {
        this.assignedTo = assignedTo;
    }

    public Date getAssignedAt() {
        if(assignedAt == null){
            return null;
        }
        return (Date)assignedAt.clone();
    }

    public void setAssignedAt(Date assignedAt) {
        if(assignedAt != null)
        {
            this.assignedAt = (Date)assignedAt.clone();
        }else {
            this.assignedAt = null;
        }
    }

    public InsuredRequest getInsured() {
        return insured;
    }

    public void setInsured(InsuredRequest insured) {
        this.insured = insured;
    }

    public InsuranceCompanyCounterpartyRequest getInsuranceCompany() {
        return insuranceCompany;
    }

    public void setInsuranceCompany(InsuranceCompanyCounterpartyRequest insuranceCompany) {
        this.insuranceCompany = insuranceCompany;
    }

    public DriverRequest getDriver() {
        return driver;
    }

    public void setDriver(DriverRequest driver) {
        this.driver = driver;
    }

    public VehicleRequest getVehicle() {
        return vehicle;
    }

    public void setVehicle(VehicleRequest vehicle) {
        this.vehicle = vehicle;
    }

    public Boolean getCaiSigned() {
        return isCaiSigned;
    }

    public void setCaiSigned(Boolean caiSigned) {
        isCaiSigned = caiSigned;
    }

    public ImpactPointRequest getImpactPoint() {
        return impactPoint;
    }

    public void setImpactPoint(ImpactPointRequest impactPoint) {
        this.impactPoint = impactPoint;
    }

    public Boolean getResponsible() {
        return responsible;
    }

    public void setResponsible(Boolean responsible) {
        this.responsible = responsible;
    }

    public Boolean getEligibility() {
        return eligibility;
    }

    public void setEligibility(Boolean eligibility) {
        this.eligibility = eligibility;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<LastContactRequest> getLastContact() {
        if(lastContact == null){
            return null;
        }
        return new ArrayList<>(lastContact);
    }

    public void setLastContact(List<LastContactRequest> lastContact) {
        if(lastContact != null)
        {
            this.lastContact = new ArrayList<>(lastContact);
        }else {
            this.lastContact = null;
        }
    }

    public ManagerRequestV1 getManager() {
        return manager;
    }

    public void setManager(ManagerRequestV1 manager) {
        this.manager = manager;
    }

    public CanalizationRequest getCanalization() {
        return canalization;
    }

    public void setCanalization(CanalizationRequest canalization) {
        this.canalization = canalization;
    }

    public Boolean getAld() {
        return isAld;
    }

    public void setAld(Boolean ald) {
        isAld = ald;
    }
}
