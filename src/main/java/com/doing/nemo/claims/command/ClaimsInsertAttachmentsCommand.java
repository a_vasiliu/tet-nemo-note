package com.doing.nemo.claims.command;

import com.doing.nemo.claims.common.commandBus.Command;
import com.doing.nemo.claims.entity.jsonb.forms.jsonbForms.Attachment;

import java.util.ArrayList;
import java.util.List;

public class ClaimsInsertAttachmentsCommand implements Command {

    private String id;
    private List<Attachment> fileManagerList;

    public ClaimsInsertAttachmentsCommand(String id, List<Attachment> fileManagerList) {
        this.id = id;
        if(fileManagerList != null){
            this.fileManagerList = new ArrayList<>(fileManagerList);
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Attachment> getFileManagerList() {
        if(fileManagerList == null){
            return null;
        }
        return new ArrayList<>(fileManagerList);
    }

    public void setFileManagerList(List<Attachment> fileManagerList) {
        if(fileManagerList != null){
            this.fileManagerList = new ArrayList<>(fileManagerList);
        }else {
            this.fileManagerList = null;
        }
    }

    @Override
    public String toString() {
        return "ClaimsInsertAttachmentsCommand{" +
                "id=" + id +
                ", fileManagerList=" + fileManagerList +
                '}';
    }
}
