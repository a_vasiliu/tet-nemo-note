package com.doing.nemo.claims.command;

import com.doing.nemo.claims.common.commandBus.Command;
import com.doing.nemo.claims.controller.payload.request.complaint.FromCompanyRequest;

public class ClaimsPatchExternalAcclaimsCommand implements Command {
    private String id;
    private String numberSxCounterparty;
    private String status;

    public ClaimsPatchExternalAcclaimsCommand() {
    }

    public ClaimsPatchExternalAcclaimsCommand(String id, String numberSxCounterparty, String status) {
        this.id = id;
        this.numberSxCounterparty = numberSxCounterparty;
        this.status = status;
    }

    public String getNumberSxCounterparty() {
        return numberSxCounterparty;
    }

    public void setNumberSxCounterparty(String numberSxCounterparty) {
        this.numberSxCounterparty = numberSxCounterparty;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


}
