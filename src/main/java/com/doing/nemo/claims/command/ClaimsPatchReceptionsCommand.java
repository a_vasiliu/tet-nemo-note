package com.doing.nemo.claims.command;

import com.doing.nemo.claims.common.commandBus.Command;

import java.util.Date;

public class ClaimsPatchReceptionsCommand implements Command {

    private String id;
    private Date firstKeyReception;
    private Date secondKeyReception;
    private Date originalComplaintReception;
    private Date copyComplaintReception;
    private Date originalReportReception;
    private Date copyReportReception;

    public ClaimsPatchReceptionsCommand(String id, Date firstKeyReception, Date secondKeyReception, Date originalComplaintReception, Date copyComplaintReception, Date originalReportReception, Date copyReportReception) {
        this.id = id;
        if(firstKeyReception != null)
        {
            this.firstKeyReception =(Date) firstKeyReception.clone();
        }
        if(secondKeyReception != null)
        {
            this.secondKeyReception = (Date)secondKeyReception.clone();
        }
        if(originalComplaintReception != null)
        {
            this.originalComplaintReception = (Date)originalComplaintReception.clone();
        }
        if(copyComplaintReception != null)
        {
            this.copyComplaintReception = (Date)copyComplaintReception.clone();
        }
        if(originalReportReception != null)
        {
            this.originalReportReception = (Date)originalReportReception.clone();
        }
        if(copyReportReception != null)
        {
            this.copyReportReception = (Date) copyReportReception.clone();
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getFirstKeyReception() {
        if(firstKeyReception == null){
            return null;
        }
        return (Date) firstKeyReception.clone();
    }

    public void setFirstKeyReception(Date firstKeyReception) {
        if(firstKeyReception != null)
        {
            this.firstKeyReception =(Date) firstKeyReception.clone();
        }else {
            this.firstKeyReception = null;
        }
    }

    public Date getSecondKeyReception() {
        if(secondKeyReception == null){
            return null;
        }
        return (Date)secondKeyReception.clone();
    }

    public void setSecondKeyReception(Date secondKeyReception) {
        if(secondKeyReception != null)
        {
            this.secondKeyReception = (Date)secondKeyReception.clone();
        }else {
            this.secondKeyReception = null;
        }
    }

    public Date getOriginalComplaintReception() {
        if(originalComplaintReception == null){
            return null;
        }
        return (Date)originalComplaintReception.clone();
    }

    public void setOriginalComplaintReception(Date originalComplaintReception) {
        if(originalComplaintReception != null)
        {
            this.originalComplaintReception = (Date)originalComplaintReception.clone();
        }else {
            this.originalComplaintReception = null;
        }
    }

    public Date getCopyComplaintReception() {
        if(copyComplaintReception == null){
            return null;
        }
        return (Date)copyComplaintReception.clone();
    }

    public void setCopyComplaintReception(Date copyComplaintReception) {
        if(copyComplaintReception != null)
        {
            this.copyComplaintReception = (Date)copyComplaintReception.clone();
        }else {
            this.copyComplaintReception = null;
        }
    }




    public Date getOriginalReportReception() {
        if(originalReportReception == null){
            return null;
        }
        return (Date)originalReportReception.clone();
    }

    public void setOriginalReportReception(Date originalReportReception) {
        if(originalReportReception != null)
        {
            this.originalReportReception = (Date)originalReportReception.clone();
        }else {
            this.originalReportReception = null;
        }
    }

    public Date getCopyReportReception() {
        if(copyReportReception == null){
            return null;
        }
        return (Date) copyReportReception.clone();
    }

    public void setCopyReportReception(Date copyReportReception) {
        if(copyReportReception != null)
        {
            this.copyReportReception = (Date) copyReportReception.clone();
        }else {
            this.copyReportReception = null;
        }
    }

    @Override
    public String toString() {
        return "ClaimsPatchReceptionsCommand{" +
                "firstKeyReception=" + firstKeyReception +
                ", secondKeyReception=" + secondKeyReception +
                ", originalComplaintReception=" + originalComplaintReception +
                ", copyComplaintReception=" + copyComplaintReception +
                ", originalReportReception=" + originalReportReception +
                ", copyReportReception=" + copyReportReception +
                '}';
    }
}
