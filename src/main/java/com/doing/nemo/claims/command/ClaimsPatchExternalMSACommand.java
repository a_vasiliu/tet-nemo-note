package com.doing.nemo.claims.command;

import com.doing.nemo.claims.common.commandBus.Command;
import com.doing.nemo.claims.controller.payload.request.complaint.FromCompanyRequest;

public class ClaimsPatchExternalMSACommand implements Command {
    private String id;
    private FromCompanyRequest fromCompany;
    public ClaimsPatchExternalMSACommand() {
    }

    public ClaimsPatchExternalMSACommand(String id, FromCompanyRequest fromCompany) {
        this.id = id;
        this.fromCompany = fromCompany;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public FromCompanyRequest getFromCompany() {
        return fromCompany;
    }

    public void setFromCompany(FromCompanyRequest fromCompany) {
        this.fromCompany = fromCompany;
    }
}
