package com.doing.nemo.claims.command;

import com.doing.nemo.claims.common.commandBus.Command;
import com.doing.nemo.claims.controller.payload.request.CounterpartyRequest;
import com.doing.nemo.claims.controller.payload.request.claims.*;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class ClaimsInsertDraftCommand implements Command {

    private String claimsId;
    private String userId;
    private String userName;
    private ClaimsStatusEnum status;
    private Boolean isWithCounterparty;
    private ComplaintRequest complaint;
    private DamagedRequest damaged;
    private List<CounterpartyRequest> counterparty;
    private List<DeponentRequest> deponent;
    private List<WoundedRequest> wounded;
    private CaiRequest cai;
    private Long idSaleforce;
    private Boolean paiComunication;
    private FormRequest forms;
    private List<NoteRequest> notes;
    private FoundModelRequest foundModel;
    private Boolean locked;
    private RefundRequest refund;
    private Map<String,Object> metadata;

    public ClaimsInsertDraftCommand(String claimsId, String userId, String userName, ClaimsStatusEnum status, ComplaintRequest complaint, DamagedRequest damaged, List<CounterpartyRequest> counterparty, List<DeponentRequest> deponent, List<WoundedRequest> wounded, CaiRequest cai, Long idSaleforce, Boolean paiComunication, FormRequest forms, List<NoteRequest> notes, FoundModelRequest foundModel, RefundRequest refund, Map<String, Object> metadata, Boolean isWithCounterparty) {
        this.claimsId = claimsId;
        this.userId = userId;
        this.userName = userName;
        this.status = status;
        this.isWithCounterparty = isWithCounterparty;
        this.complaint = complaint;
        this.damaged = damaged;
        if(counterparty != null){
            this.counterparty = new ArrayList<>(counterparty);
        }
        if(deponent != null)
        {
            this.deponent = new ArrayList<>(deponent);
        }
        if(wounded != null)
        {
            this.wounded = new ArrayList<>(wounded);
        }
        this.cai = cai;
        this.idSaleforce = idSaleforce;
        this.paiComunication = paiComunication;
        this.forms = forms;
        if(notes != null)
        {
            this.notes = new ArrayList<>(notes);
        }
        this.foundModel = foundModel;
        this.refund = refund;
        this.metadata = metadata;
    }


    public String getClaimsId() {
        return claimsId;
    }

    public void setClaimsId(String claimsId) {
        this.claimsId = claimsId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public ClaimsStatusEnum getStatus() {
        return status;
    }

    public void setStatus(ClaimsStatusEnum status) {
        this.status = status;
    }

    public Boolean getWithCounterparty() {
        return isWithCounterparty;
    }

    public void setWithCounterparty(Boolean withCounterparty) {
        isWithCounterparty = withCounterparty;
    }

    public ComplaintRequest getComplaint() {
        return complaint;
    }

    public void setComplaint(ComplaintRequest complaint) {
        this.complaint = complaint;
    }

    public DamagedRequest getDamaged() {
        return damaged;
    }

    public void setDamaged(DamagedRequest damaged) {
        this.damaged = damaged;
    }

    public List<CounterpartyRequest> getCounterparty() {
        if(counterparty == null){
            return null;
        }
        return new ArrayList<>(counterparty);
    }

    public void setCounterparty(List<CounterpartyRequest> counterparty) {
        if(counterparty != null)
        {
            this.counterparty = new ArrayList<>(counterparty);
        }else {
            this.counterparty = null;
        }
    }

    public List<DeponentRequest> getDeponent() {
        if(deponent == null){
            return null;
        }
        return new ArrayList<>(deponent);
    }

    public void setDeponent(List<DeponentRequest> deponent) {
        if(deponent != null){
            this.deponent = new ArrayList<>(deponent);
        }else {
            this.deponent = null;
        }
    }

    public List<WoundedRequest> getWounded() {
        if(wounded == null){
            return null;
        }
        return new ArrayList<>(wounded);
    }

    public void setWounded(List<WoundedRequest> wounded) {
        if(wounded != null)
        {
            this.wounded =new ArrayList<>(wounded);
        }else {
            this.wounded = null;
        }
    }

    public CaiRequest getCai() {
        return cai;
    }

    public void setCai(CaiRequest cai) {
        this.cai = cai;
    }

    public Long getIdSaleforce() {
        return idSaleforce;
    }

    public void setIdSaleforce(Long idSaleforce) {
        this.idSaleforce = idSaleforce;
    }

    public Boolean getPaiComunication() {
        return paiComunication;
    }

    public void setPaiComunication(Boolean paiComunication) {
        this.paiComunication = paiComunication;
    }

    public FormRequest getForms() {
        return forms;
    }

    public void setForms(FormRequest forms) {
        this.forms = forms;
    }

    public List<NoteRequest> getNotes() {
        if(notes == null){
            return null;
        }
        return new ArrayList<>(notes);
    }

    public void setNotes(List<NoteRequest> notes) {
        if(notes != null)
        {
            this.notes = new ArrayList<>(notes);
        }else {
            this.notes = null;
        }
    }

    public FoundModelRequest getFoundModel() {
        return foundModel;
    }

    public void setFoundModel(FoundModelRequest foundModel) {
        this.foundModel = foundModel;
    }

    public Boolean getLocked() {
        return locked;
    }

    public void setLocked(Boolean locked) {
        this.locked = locked;
    }

    public RefundRequest getRefund() {
        return refund;
    }

    public void setRefund(RefundRequest refund) {
        this.refund = refund;
    }

    public Map<String, Object> getMetadata() {
        return metadata;
    }

    public void setMetadata(Map<String, Object> metadata) {
        this.metadata = metadata;
    }

    @Override
    public String toString() {
        return "ClaimsInsertDraftCommand{" +
                "claimsId='" + claimsId + '\'' +
                ", userId='" + userId + '\'' +
                ", userName='" + userName + '\'' +
                ", status=" + status +
                ", isWithCounterparty=" + isWithCounterparty +
                ", complaint=" + complaint +
                ", damaged=" + damaged +
                ", counterparty=" + counterparty +
                ", deponent=" + deponent +
                ", wounded=" + wounded +
                ", cai=" + cai +
                ", idSaleforce=" + idSaleforce +
                ", paiComunication=" + paiComunication +
                ", forms=" + forms +
                ", notes=" + notes +
                ", foundModel=" + foundModel +
                ", locked=" + locked +
                ", refund=" + refund +
                ", metadata=" + metadata +
                '}';
    }
}
