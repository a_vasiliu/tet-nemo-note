package com.doing.nemo.claims.command;


import com.doing.nemo.claims.common.commandBus.Command;
import com.doing.nemo.claims.controller.payload.request.*;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.EntrustedEnum.EntrustedEnum;

public class ClaimsPatchEntrustedCommand implements Command {
    private String id;
    private EntrustedEnum entrustedType;
    private LegalRequestV1 legal;
    private InspectorateRequestV1 inspectorate;
    private BrokerRequestV1 broker;
    private InsuranceManagerRequestV1 insuranceManager;
    private ClaimsPatchStatusRequestV1 claimsPatchStatusRequestV1;
    private String userId;
    private String userName;

    public ClaimsPatchEntrustedCommand(String id, EntrustedEnum entrustedType, LegalRequestV1 legal, InspectorateRequestV1 inspectorate, BrokerRequestV1 broker, InsuranceManagerRequestV1 insuranceManager, ClaimsPatchStatusRequestV1 claimsPatchStatusRequestV1, String userId, String userName) {
        this.id = id;
        this.entrustedType = entrustedType;
        this.legal = legal;
        this.inspectorate = inspectorate;
        this.broker = broker;
        this.insuranceManager = insuranceManager;
        this.claimsPatchStatusRequestV1 = claimsPatchStatusRequestV1;
        this.userId = userId;
        this.userName = userName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public ClaimsPatchStatusRequestV1 getClaimsPatchStatusRequestV1() {
        return claimsPatchStatusRequestV1;
    }

    public void setClaimsPatchStatusRequestV1(ClaimsPatchStatusRequestV1 claimsPatchStatusRequestV1) {
        this.claimsPatchStatusRequestV1 = claimsPatchStatusRequestV1;
    }

    public EntrustedEnum getEntrustedType() {
        return entrustedType;
    }

    public void setEntrustedType(EntrustedEnum entrustedType) {
        this.entrustedType = entrustedType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public LegalRequestV1 getLegal() {
        return legal;
    }

    public void setLegal(LegalRequestV1 legal) {
        this.legal = legal;
    }

    public InspectorateRequestV1 getInspectorate() {
        return inspectorate;
    }

    public void setInspectorate(InspectorateRequestV1 inspectorate) {
        this.inspectorate = inspectorate;
    }

    public BrokerRequestV1 getBroker() {
        return broker;
    }

    public void setBroker(BrokerRequestV1 broker) {
        this.broker = broker;
    }

    public InsuranceManagerRequestV1 getInsuranceManager() {
        return insuranceManager;
    }

    public void setInsuranceManager(InsuranceManagerRequestV1 insuranceManager) {
        this.insuranceManager = insuranceManager;
    }
}
