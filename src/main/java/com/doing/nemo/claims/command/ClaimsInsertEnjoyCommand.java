package com.doing.nemo.claims.command;

import com.doing.nemo.claims.common.commandBus.Command;
import com.doing.nemo.claims.controller.payload.request.ClaimsInsertEnjoyRequest.ComplaintEnjoyRequest;
import com.doing.nemo.claims.controller.payload.request.ClaimsInsertEnjoyRequest.CounterpartyEnjoyRequest;
import com.doing.nemo.claims.controller.payload.request.ClaimsInsertEnjoyRequest.DamagedEnjoyRequest;
import com.doing.nemo.claims.controller.payload.request.ClaimsInsertEnjoyRequest.TheftEnjoyRequest;
import com.doing.nemo.claims.controller.payload.request.claims.CaiRequest;
import com.doing.nemo.claims.controller.payload.request.claims.DeponentRequest;
import com.doing.nemo.claims.controller.payload.request.claims.WoundedRequest;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class ClaimsInsertEnjoyCommand implements Command {

    private String claimsId;
    private ComplaintEnjoyRequest complaint;
    private Boolean isWithCounterparty;
    private List<CounterpartyEnjoyRequest> counterparty;
    private List<DeponentRequest> deponent;
    private List<WoundedRequest> wounded;
    private DamagedEnjoyRequest damaged;
    private TheftEnjoyRequest theftRequest;
    private Long idSaleforce;
    private CaiRequest caiDetails;
    private String userName;
    private String userId;
    private Boolean isCompleteDocumentation;


    public ClaimsInsertEnjoyCommand(){}

    public ClaimsInsertEnjoyCommand(String claimsId, ComplaintEnjoyRequest complaint, Boolean isWithCounterparty, List<CounterpartyEnjoyRequest> counterparty, List<DeponentRequest> deponent, List<WoundedRequest> wounded, DamagedEnjoyRequest damaged, TheftEnjoyRequest theftRequest, Long idSaleforce, CaiRequest caiDetails, String userName, String userId,Boolean isCompleteDocumentation) {
        this.claimsId = claimsId;
        this.complaint = complaint;
        this.isWithCounterparty = isWithCounterparty;
        if(counterparty != null)
        {
            this.counterparty = new ArrayList<>(counterparty);
        }
        if(deponent != null)
        {
            this.deponent = new ArrayList<>(deponent);
        }
        if(wounded != null)
        {
            this.wounded = new ArrayList<>(wounded);
        }
        this.damaged = damaged;
        this.theftRequest = theftRequest;
        this.idSaleforce = idSaleforce;
        this.caiDetails = caiDetails;
        this.userName = userName;
        this.userId = userId;
        this.isCompleteDocumentation = isCompleteDocumentation;
    }

    public Boolean getCompleteDocumentation() {
        return isCompleteDocumentation;
    }

    public void setCompleteDocumentation(Boolean completeDocumentation) {
        isCompleteDocumentation = completeDocumentation;
    }

    public String getClaimsId() {
        return claimsId;
    }

    public void setClaimsId(String claimsId) {
        this.claimsId = claimsId;
    }

    public ComplaintEnjoyRequest getComplaint() {
        return complaint;
    }

    public void setComplaint(ComplaintEnjoyRequest complaint) {
        this.complaint = complaint;
    }

    public Boolean getWithCounterparty() {
        return isWithCounterparty;
    }

    public void setWithCounterparty(Boolean withCounterparty) {
        isWithCounterparty = withCounterparty;
    }

    public List<CounterpartyEnjoyRequest> getCounterparty() {
        if(counterparty == null){
            return null;
        }
        return new ArrayList<>(counterparty) ;
    }

    public void setCounterparty(List<CounterpartyEnjoyRequest> counterparty) {
        if( counterparty != null) {
            this.counterparty = new ArrayList<>(counterparty);
        }else{
            this.counterparty = null;
        }
    }

    public List<DeponentRequest> getDeponent() {
        if(deponent == null){
            return null;
        }
        return new ArrayList<>(deponent) ;
    }

    public void setDeponent(List<DeponentRequest> deponent) {
        if( deponent != null) {
            this.deponent = new ArrayList<>(deponent);
        }else{
            this.deponent = null;
        }
    }

    public List<WoundedRequest> getWounded() {
        if(wounded == null){
            return null;
        }
        return new ArrayList<>(wounded) ;
    }

    public void setWounded(List<WoundedRequest> wounded) {
        if(wounded != null ) {
            this.wounded = new ArrayList<>(wounded);
        }else{
            wounded = null;
        }
    }

    public DamagedEnjoyRequest getDamaged() {
        return damaged;
    }

    public void setDamaged(DamagedEnjoyRequest damaged) {
        this.damaged = damaged;
    }

    public TheftEnjoyRequest getTheftRequest() {
        return theftRequest;
    }

    public void setTheftRequest(TheftEnjoyRequest theftRequest) {
        this.theftRequest = theftRequest;
    }

    public Long getIdSaleforce() {
        return idSaleforce;
    }

    public void setIdSaleforce(Long idSaleforce) {
        this.idSaleforce = idSaleforce;
    }

    public CaiRequest getCaiDetails() {
        return caiDetails;
    }

    public void setCaiDetails(CaiRequest caiDetails) {
        this.caiDetails = caiDetails;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ClaimsInsertEnjoyCommand{");
        sb.append("claimsId='").append(claimsId).append('\'');
        sb.append(", complaint=").append(complaint);
        sb.append(", isWithCounterparty=").append(isWithCounterparty);
        sb.append(", counterparty=").append(counterparty);
        sb.append(", deponent=").append(deponent);
        sb.append(", wounded=").append(wounded);
        sb.append(", damaged=").append(damaged);
        sb.append(", theftRequest=").append(theftRequest);
        sb.append(", idSaleforce=").append(idSaleforce);
        sb.append(", caiDetails=").append(caiDetails);
        sb.append(", userName='").append(userName).append('\'');
        sb.append(", userId='").append(userId).append('\'');
        sb.append(", isCompleteDocumentation=").append(isCompleteDocumentation);
        sb.append('}');
        return sb.toString();
    }
}
