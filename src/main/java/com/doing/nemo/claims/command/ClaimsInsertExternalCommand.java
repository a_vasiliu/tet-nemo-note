package com.doing.nemo.claims.command;

import com.doing.nemo.claims.common.commandBus.Command;
import com.doing.nemo.claims.controller.payload.request.ClaimsInsertExternalRequest.ComplaintExternalRequest;
import com.doing.nemo.claims.controller.payload.request.ClaimsInsertExternalRequest.CounterpartyExternalRequest;
import com.doing.nemo.claims.controller.payload.request.ClaimsInsertExternalRequest.DamagedExternalRequest;
import com.doing.nemo.claims.controller.payload.request.ClaimsInsertExternalRequest.TheftExternalRequest;
import com.doing.nemo.claims.controller.payload.request.claims.CaiRequest;
import com.doing.nemo.claims.controller.payload.request.claims.DeponentRequest;
import com.doing.nemo.claims.controller.payload.request.claims.RefundRequest;
import com.doing.nemo.claims.controller.payload.request.claims.WoundedRequest;
import com.doing.nemo.claims.controller.payload.request.counterparty.DriverRequest;

import java.util.ArrayList;
import java.util.List;

public class ClaimsInsertExternalCommand implements Command {

    private String claimsId;
    private Boolean isWithCounterparty;
    private ComplaintExternalRequest complaint;
    private DamagedExternalRequest damaged;
    private List<CounterpartyExternalRequest> counterparty;
    private List<DeponentRequest> deponent;
    private List<WoundedRequest> wounded;
    private CaiRequest cai;
    private Long idSaleforce;
    private RefundRequest refund;
    private TheftExternalRequest theft;
    private Boolean isRead;
    private String userName;
    private String userId;
    private DriverRequest driverRequest;
    private Boolean isCompleteDocumentation ;

    public ClaimsInsertExternalCommand(String claimsId, Boolean isWithCounterparty, ComplaintExternalRequest complaint, DamagedExternalRequest damaged, List<CounterpartyExternalRequest> counterparty, List<DeponentRequest> deponent, List<WoundedRequest> wounded, CaiRequest cai, Long idSaleforce, RefundRequest refund, TheftExternalRequest theft, Boolean isRead, String userName, String userId, DriverRequest driverRequest,Boolean isCompleteDocumentation) {
        this.claimsId = claimsId;
        this.isWithCounterparty = isWithCounterparty;
        this.complaint = complaint;
        this.damaged = damaged;
        if(counterparty != null)
        {
            this.counterparty = new ArrayList<>(counterparty);
        }
        if(deponent != null)
        {
            this.deponent = new ArrayList<>(deponent);
        }
        if(wounded != null)
        {
            this.wounded = new ArrayList<>(wounded);
        }
        this.cai = cai;
        this.idSaleforce = idSaleforce;
        this.refund = refund;
        this.theft = theft;
        this.isRead = isRead;
        this.userName = userName;
        this.userId = userId;
        this.driverRequest = driverRequest;
        this.isCompleteDocumentation = isCompleteDocumentation;
    }

    public Boolean getCompleteDocumentation() {
        return isCompleteDocumentation;
    }

    public void setCompleteDocumentation(Boolean completeDocumentation) {
        isCompleteDocumentation = completeDocumentation;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getClaimsId() {
        return claimsId;
    }

    public void setClaimsId(String claimsId) {
        this.claimsId = claimsId;
    }

    public Boolean getWithCounterparty() {
        return isWithCounterparty;
    }

    public void setWithCounterparty(Boolean withCounterparty) {
        isWithCounterparty = withCounterparty;
    }

    public ComplaintExternalRequest getComplaint() {
        return complaint;
    }

    public void setComplaint(ComplaintExternalRequest complaint) {
        this.complaint = complaint;
    }

    public DamagedExternalRequest getDamaged() {
        return damaged;
    }

    public void setDamaged(DamagedExternalRequest damaged) {
        this.damaged = damaged;
    }

    public List<CounterpartyExternalRequest> getCounterparty() {
        if(counterparty == null){
            return null;
        }
        return new ArrayList<>(counterparty);
    }

    public void setCounterparty(List<CounterpartyExternalRequest> counterparty) {
        if(counterparty != null)
        {
            this.counterparty = new ArrayList<>(counterparty);
        }else {
            this.counterparty = null;
        }
    }

    public List<DeponentRequest> getDeponent() {
        if(deponent == null){
            return null;
        }
        return new ArrayList<>(deponent);
    }

    public void setDeponent(List<DeponentRequest> deponent) {
        if(deponent != null){
            this.deponent = new ArrayList<>(deponent);
        }else {
            this.deponent = null;
        }
    }

    public List<WoundedRequest> getWounded() {
        if(wounded == null){
            return null;
        }
        return new ArrayList<>(wounded);
    }

    public void setWounded(List<WoundedRequest> wounded) {
        if(wounded != null)
        {
            this.wounded =new ArrayList<>(wounded);
        }else {
            this.wounded = null;
        }
    }

    public CaiRequest getCai() {
        return cai;
    }

    public void setCai(CaiRequest cai) {
        this.cai = cai;
    }

    public Long getIdSaleforce() {
        return idSaleforce;
    }

    public void setIdSaleforce(Long idSaleforce) {
        this.idSaleforce = idSaleforce;
    }

    public RefundRequest getRefund() {
        return refund;
    }

    public void setRefund(RefundRequest refund) {
        this.refund = refund;
    }

    public TheftExternalRequest getTheft() {
        return theft;
    }

    public void setTheft(TheftExternalRequest theft) {
        this.theft = theft;
    }

    public Boolean getRead() {
        return isRead;
    }

    public void setRead(Boolean read) {
        isRead = read;
    }

    public DriverRequest getDriverRequest() {
        return driverRequest;
    }

    public void setDriverRequest(DriverRequest driverRequest) {
        this.driverRequest = driverRequest;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ClaimsInsertExternalCommand{");
        sb.append("claimsId='").append(claimsId).append('\'');
        sb.append(", isWithCounterparty=").append(isWithCounterparty);
        sb.append(", complaint=").append(complaint);
        sb.append(", damaged=").append(damaged);
        sb.append(", counterparty=").append(counterparty);
        sb.append(", deponent=").append(deponent);
        sb.append(", wounded=").append(wounded);
        sb.append(", cai=").append(cai);
        sb.append(", idSaleforce=").append(idSaleforce);
        sb.append(", refund=").append(refund);
        sb.append(", theft=").append(theft);
        sb.append(", isRead=").append(isRead);
        sb.append(", userName='").append(userName).append('\'');
        sb.append(", userId='").append(userId).append('\'');
        sb.append(", driverRequest=").append(driverRequest);
        sb.append(", isCompleteDocumentation=").append(isCompleteDocumentation);
        sb.append('}');
        return sb.toString();
    }
}
