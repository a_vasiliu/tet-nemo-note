package com.doing.nemo.claims.command;

import com.doing.nemo.claims.common.commandBus.Command;
import com.doing.nemo.claims.controller.payload.request.ClaimsPatchStatusRequestV1;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;

public class ClaimsPatchStatusCommand implements Command {

    private String id;
    private ClaimsStatusEnum nextStatus;
    private String practiceManager;
    private String userId;
    private String userName;
    private ClaimsPatchStatusRequestV1 claimsPatchStatusRequestV1;

    public ClaimsPatchStatusCommand(String id, ClaimsStatusEnum nextStatus, String practiceManager, String userId, String userName, ClaimsPatchStatusRequestV1 claimsPatchStatusRequestV1) {
        this.id = id;
        this.nextStatus = nextStatus;
        this.practiceManager = practiceManager;
        this.userId = userId;
        this.userName = userName;
        this.claimsPatchStatusRequestV1 = claimsPatchStatusRequestV1;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ClaimsStatusEnum getNextStatus() {
        return nextStatus;
    }

    public void setNextStatus(ClaimsStatusEnum nextStatus) {
        this.nextStatus = nextStatus;
    }

    public String getPracticeManager() {
        return practiceManager;
    }

    public void setPracticeManager(String practiceManager) {
        this.practiceManager = practiceManager;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public ClaimsPatchStatusRequestV1 getClaimsPatchStatusRequestV1() {
        return claimsPatchStatusRequestV1;
    }

    public void setClaimsPatchStatusRequestV1(ClaimsPatchStatusRequestV1 claimsPatchStatusRequestV1) {
        this.claimsPatchStatusRequestV1 = claimsPatchStatusRequestV1;
    }

    @Override
    public String toString() {
        return "ClaimsPatchStatusCommand{" +
                "id='" + id + '\'' +
                ", nextStatus=" + nextStatus +
                ", practiceManager='" + practiceManager + '\'' +
                ", userId='" + userId + '\'' +
                ", userName='" + userName + '\'' +
                ", claimsPatchStatusRequestV1=" + claimsPatchStatusRequestV1 +
                '}';
    }
}
