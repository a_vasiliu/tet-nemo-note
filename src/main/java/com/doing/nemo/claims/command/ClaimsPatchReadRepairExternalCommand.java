package com.doing.nemo.claims.command;

import com.doing.nemo.claims.common.commandBus.Command;
import com.doing.nemo.claims.controller.payload.request.IdRequestV1;
import com.doing.nemo.claims.controller.payload.request.ListRequestV1;

import java.util.ArrayList;
import java.util.List;

public class ClaimsPatchReadRepairExternalCommand implements Command {
    private ListRequestV1<IdRequestV1> claimsIdListRequest;
    private List<String> nemoProfiles;

    public ClaimsPatchReadRepairExternalCommand() {
    }

    public ClaimsPatchReadRepairExternalCommand(ListRequestV1<IdRequestV1> claimsIdListRequest, List<String> nemoProfiles) {
        this.claimsIdListRequest = claimsIdListRequest;
        if(nemoProfiles != null)
        {
            this.nemoProfiles = new ArrayList<>(nemoProfiles);;
        }
    }

    public List<String> getNemoProfiles() {
        if(nemoProfiles == null){
            return null;
        }
        return new ArrayList<>(nemoProfiles);
    }

    public void setNemoProfiles(List<String> nemoProfiles) {
        if(nemoProfiles != null)
        {
            this.nemoProfiles = new ArrayList<>(nemoProfiles);;
        }else {
            this.nemoProfiles = null;
        }
    }

    public ListRequestV1<IdRequestV1> getClaimsIdListRequest() {
        return claimsIdListRequest;
    }

    public void setClaimsIdListRequest(ListRequestV1<IdRequestV1> claimsIdListRequest) {
        this.claimsIdListRequest = claimsIdListRequest;
    }

    @Override
    public String toString() {
        return "ClaimsPatchReadExternalCommand{" +
                "RepairIdListRequestV1=" + claimsIdListRequest +
                '}';
    }
}
