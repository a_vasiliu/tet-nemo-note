package com.doing.nemo.claims.command;

import com.doing.nemo.claims.common.commandBus.Command;

public class ClaimsDeleteNoteCommand implements Command {

    private String claimId;
    private String noteId;

    public ClaimsDeleteNoteCommand() {
    }

    public ClaimsDeleteNoteCommand(String claimId, String noteId) {
        this.claimId = claimId;
        this.noteId = noteId;
    }

    public String getClaimId() {
        return claimId;
    }

    public void setClaimId(String claimId) {
        this.claimId = claimId;
    }

    public String getNoteId() {
        return noteId;
    }

    public void setNoteId(String noteId) {
        this.noteId = noteId;
    }

    @Override
    public String toString() {
        return "ClaimsDeleteNoteCommand{" +
                "claimId=" + claimId +
                ", noteId=" + noteId +
                '}';
    }
}
