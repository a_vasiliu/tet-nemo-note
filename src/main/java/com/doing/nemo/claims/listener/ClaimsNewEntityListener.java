package com.doing.nemo.claims.listener;

import com.doing.nemo.claims.cache.impl.RedisCacheHandlerImpl;
import com.doing.nemo.claims.entity.ClaimsNewEntity;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

public class ClaimsNewEntityListener {

    private RedisCacheHandlerImpl redisCacheHandler;

    private void init() {
        this.redisCacheHandler = BeanUtil.getBean(RedisCacheHandlerImpl.class);;
    }

    private void freeCache() {
        this.redisCacheHandler.truncateAll("claims.*");
    }

    @PrePersist
    @PreUpdate
    public void methodExecuteBeforeSave(ClaimsNewEntity claims) {
        this.init();
        this.freeCache();
    }

}
