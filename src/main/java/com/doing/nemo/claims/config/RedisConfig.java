package com.doing.nemo.claims.config;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.lettuce.core.ClientOptions;
import io.lettuce.core.SocketOptions;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.cache.CacheProperties;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurer;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.interceptor.CacheErrorHandler;
import org.springframework.cache.support.NoOpCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.RedisPassword;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceClientConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.*;

import java.time.Duration;

@Configuration
@EnableConfigurationProperties({RedisProperties.class, CacheProperties.class})
public class RedisConfig extends CachingConfigurerSupport implements CachingConfigurer {

    @Autowired
    private RedisProperties redisProperties;

    @Autowired
    private CacheProperties cacheProperties;

    @Value("${redis.cache.enabled}")
    private boolean cacheEnabled;

    @Value("${redis.command.timeout}")
    private long commandTimeout;

    @Bean
    public CacheManager redisCacheManager() {

        if (cacheEnabled) {
            return RedisCacheManager.RedisCacheManagerBuilder
                    .fromConnectionFactory(redisConnectionFactory())
                    .cacheDefaults(redisCacheConfiguration())
                    .build();
        } else {
            return new NoOpCacheManager();
        }
    }

    @Bean
    public LettuceConnectionFactory redisConnectionFactory() {

        return new LettuceConnectionFactory(redisStandaloneConfiguration(), lettuceClientConfiguration());
    }

    @Bean
    public RedisStandaloneConfiguration redisStandaloneConfiguration() {

        RedisStandaloneConfiguration redisStandaloneConfiguration = new RedisStandaloneConfiguration();
        redisStandaloneConfiguration.setHostName(redisProperties.getHost());
        redisStandaloneConfiguration.setPort(redisProperties.getPort());
        redisStandaloneConfiguration.setPassword(RedisPassword.of(redisProperties.getPassword()));
        redisStandaloneConfiguration.setDatabase(redisProperties.getDatabase());

        return redisStandaloneConfiguration;
    }

    @Bean
    public LettuceClientConfiguration lettuceClientConfiguration() {

        SocketOptions socketOptions = SocketOptions.builder().connectTimeout(redisProperties.getTimeout()).build();
        ClientOptions clientOptions =
                ClientOptions.builder().socketOptions(socketOptions).build();

        LettuceClientConfiguration.LettuceClientConfigurationBuilder builder = LettuceClientConfiguration.builder().clientOptions(clientOptions);
        if (redisProperties.isSsl()) {
            builder.useSsl();
        }

        return builder.commandTimeout(Duration.ofMillis(commandTimeout)).build();
    }

    @Bean
    public RedisCacheConfiguration redisCacheConfiguration() {

        return RedisCacheConfiguration.defaultCacheConfig()
                .entryTtl(cacheProperties.getRedis().getTimeToLive())
                .serializeKeysWith(RedisSerializationContext.SerializationPair.fromSerializer(new StringRedisSerializer()))
                .serializeValuesWith(RedisSerializationContext.SerializationPair.fromSerializer(new GenericJackson2JsonRedisSerializer()));
    }

    @Bean
    public RedisTemplate<String, Object> redisTemplate() {
        RedisTemplate<String, Object> template = new RedisTemplate<>();
        template.setConnectionFactory(redisConnectionFactory());
        template.setKeySerializer(new StringRedisSerializer());
        template.setHashKeySerializer(new StringRedisSerializer());
        template.setHashValueSerializer(new Jackson2JsonRedisSerializer<>(Object.class));
        template.setValueSerializer(new Jackson2JsonRedisSerializer<>(Object.class));
        return template;
    }

    @Override
    public CacheErrorHandler errorHandler() {
        return new RedisCacheErrorHandler();
    }

    public static class RedisCacheErrorHandler implements CacheErrorHandler {

        private static final Logger LOGGER = LoggerFactory.getLogger(RedisCacheErrorHandler.class);

        @Override
        public void handleCacheGetError(RuntimeException exception, Cache cache, Object key) {

            String objectKey = key == null ? "" : key.toString();

            LOGGER.warn("Unable to get key: '{}' from cache: '{}' - Exception message: '{}'",
                    objectKey, cache.getName(), exception.getMessage());
        }

        @Override
        public void handleCachePutError(RuntimeException exception, Cache cache, Object key, Object value) {

            String objectKey = null;
            if (key != null) {
                objectKey = key.toString();
            }

            String objectValue = null;
            if (value != null) {
                try {
                    objectValue = (new ObjectMapper()).writeValueAsString(value);
                } catch (JsonProcessingException e) {
                    LOGGER.warn("Unable to writeValueAsString for handleCachePutError logging");
                }
            }

            LOGGER.warn("Unable to put key: '{}' with value: '{}' into cache '{}' - Exception message: '{}'",
                    objectKey, objectValue, cache.getName(), exception.getMessage());
        }

        @Override
        public void handleCacheEvictError(RuntimeException exception, Cache cache, Object key) {

            String objectKey = null;
            if (key != null) {
                objectKey = key.toString();
            }

            LOGGER.warn("Unable to evict key: '{}' from cache: '{}' - Exception message: '{}'",
                    objectKey, cache.getName(), exception.getMessage());
        }

        @Override
        public void handleCacheClearError(RuntimeException exception, Cache cache) {
            LOGGER.warn("Unable to clean cache '{}' : '{}'", cache.getName(), exception.getMessage());
        }
    }
}
