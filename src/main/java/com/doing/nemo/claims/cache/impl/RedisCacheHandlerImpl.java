package com.doing.nemo.claims.cache.impl;

import com.doing.nemo.claims.cache.CacheHandler;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import java.util.Set;
import java.util.concurrent.TimeUnit;

@Repository
public class RedisCacheHandlerImpl implements CacheHandler {

    private static Logger LOGGER = LoggerFactory.getLogger(RedisCacheHandlerImpl.class);

    private static final String DEFAULT_KEY_PATTERN = "*";

    @Value("${redis.command.timeout:0}")
    private long cacheExpirationTimeout;

    @Value("${redis.cache.enabled:false}")
    private boolean cacheEnabled;

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;


    @Override
    public Object getValue(String key) {

        if (this.skipCaching() || StringUtils.isBlank(key)) {
            return null;
        }

        try {
            Object value = redisTemplate.opsForValue().get(key);
            if (value != null) {
                LOGGER.debug("Retrieved cache for id: " + key + ". Time left to expiration: "
                        + redisTemplate.getExpire(key) + " seconds");
            } else {
                LOGGER.debug("No cache for id: {}", key);
            }
            return value;
        } catch (Exception e) {
            LOGGER.warn(e.getMessage(), e);
            return null;
        }
    }

    @Override
    public void setValue(String key, Object value, long customCacheExpirationTimeout) {
        this.cache(key, value, customCacheExpirationTimeout);
    }

    @Override
    public void setValue(final String key, final Object value) {
        this.cache(key, value, cacheExpirationTimeout);
    }

    @Override
    public void deleteKey(String key) {

        if (this.skipCaching()) {
            return;
        }

        if (StringUtils.isNotBlank(key)) {
            try {
                redisTemplate.delete(key);
                LOGGER.debug("Deleted potential cache for id: {}", key);
            } catch (Exception e) {
                LOGGER.warn(e.getMessage(), e);
            }
        }
    }

    @Override
    public void deleteKeys(Set<String> keys) {

        if (this.skipCaching()) {
            return;
        }

        if (CollectionUtils.isNotEmpty(keys)) {
            try {
                redisTemplate.delete(keys);
                LOGGER.debug("Deleted potential cache for ids: {}", String.join(", ", keys));
            } catch (Exception e) {
                LOGGER.warn(e.getMessage(), e);
            }
        }
    }


    @Override
    public Set<String> getKeys(String pattern) {

        try {

            pattern = pattern == null ? DEFAULT_KEY_PATTERN : pattern;

            return redisTemplate.keys(pattern);

        } catch (Exception e) {
            LOGGER.warn(e.getMessage(), e);
            return null;
        }
    }

    private void cache(final String key, final Object value, long timeout) {

        if (this.skipCaching()) {
            return;
        }

        try {
            if (StringUtils.isBlank(key) || value == null) {
                return;
            }
            redisTemplate.opsForValue().set(key, value);
            redisTemplate.expire(key, timeout, TimeUnit.SECONDS);
            LOGGER.debug("Cached id: {} with expiration timeout of {} seconds", key, timeout);
        } catch (Exception e) {
            LOGGER.warn("Error caching. " + e.getMessage(), e);
        }
    }

    private boolean skipCaching() {
        if (!cacheEnabled || redisTemplate == null) {
            LOGGER.debug("Skip Cache: caching mode NO active");
            return true;
        }
        return false;
    }

    @Override
    public void truncateAll(String template) {
        redisTemplate.delete(this.getKeys(template));

    }
}
