package com.doing.nemo.claims.cache;

import java.util.Set;

public interface CacheHandler {

    Object getValue(String key);

    void setValue(String key, Object value, long customCacheExpirationTimeout);

    void setValue(String key, Object value);

    void deleteKey(String key);

    void deleteKeys(Set<String> keys);

    Set<String> getKeys(String pattern);

    void truncateAll(String template);

}
