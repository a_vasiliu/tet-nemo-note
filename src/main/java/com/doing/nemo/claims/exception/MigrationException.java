package com.doing.nemo.claims.exception;

import com.doing.nemo.commons.exception.AbstractException;
import com.doing.nemo.commons.exception.Code;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR, value = HttpStatus.INTERNAL_SERVER_ERROR)
public class MigrationException extends AbstractException {

    public MigrationException() {
        super();
    }

    public MigrationException(String message) {
        super(message);
    }

    public MigrationException(String message, Throwable e) {
        super(message, e);
    }

    public MigrationException(Code messageCode) {
        super(messageCode);
    }

    public MigrationException(String message, Code messageCode) {
        super(message, messageCode);
    }

    public MigrationException(Code messageCode, Throwable e) {
        super(messageCode, e);
    }

}
