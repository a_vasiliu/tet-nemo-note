package com.doing.nemo.claims.exception;

import com.doing.nemo.commons.exception.AbstractException;
import com.doing.nemo.commons.exception.Code;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR, value = HttpStatus.INTERNAL_SERVER_ERROR)
public class UnsupportedOperationException extends AbstractException {

    public UnsupportedOperationException() {
        super();
    }

    public UnsupportedOperationException(String message) {
        super(message);
    }

    public UnsupportedOperationException(String message, Throwable e) {
        super(message, e);
    }

    public UnsupportedOperationException(Code messageCode) {
        super(messageCode);
    }

    public UnsupportedOperationException(String message, Code messageCode) {
        super(message, messageCode);
    }

    public UnsupportedOperationException(Code messageCode, Throwable e) {
        super(messageCode, e);
    }
}
