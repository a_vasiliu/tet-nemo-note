package com.doing.nemo.claims.exception;

import com.doing.nemo.claims.websin.websinws.PutComplaintRequest;

public class WebSinClaimCommunicationException extends Exception {
    private PutComplaintRequest request;

    public WebSinClaimCommunicationException(String message, Throwable cause, PutComplaintRequest request){
        super(message,cause);
        this.setRequest(request);
    }

    public PutComplaintRequest getRequest() {
        return request;
    }

    public void setRequest(PutComplaintRequest request) {
        this.request = request;
    }

}
