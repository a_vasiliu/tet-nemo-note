package com.doing.nemo.claims.exception;

import com.doing.nemo.claims.websin.websinws.PutComplaintRequest;
import com.doing.nemo.claims.websin.websinws.UploadFileRequest;

public class WebSinFileCommunicationException extends Exception {
    private UploadFileRequest request;

    public WebSinFileCommunicationException(String message, Throwable cause, UploadFileRequest request){
        super(message,cause);
        this.setRequest(request);
    }

    public UploadFileRequest getRequest() {
        return request;
    }

    public void setRequest(UploadFileRequest request) {
        this.request = request;
    }

}
