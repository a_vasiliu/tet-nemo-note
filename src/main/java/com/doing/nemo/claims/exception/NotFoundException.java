package com.doing.nemo.claims.exception;

import com.doing.nemo.commons.exception.AbstractException;
import com.doing.nemo.commons.exception.Code;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, value = HttpStatus.NOT_FOUND)
public class NotFoundException extends AbstractException {

    public NotFoundException() {
        super();
    }

    public NotFoundException(String message) {
        super(message);
    }

    public NotFoundException(String message, Throwable e) {
        super(message, e);
    }

    public NotFoundException(Code messageCode) {
        super(messageCode);
    }

    public NotFoundException(String message, Code messageCode) {
        super(message, messageCode);
    }

    public NotFoundException(Code messageCode, Throwable e) {
        super(messageCode, e);
    }
}
