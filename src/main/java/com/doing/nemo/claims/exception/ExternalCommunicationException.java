package com.doing.nemo.claims.exception;

import com.doing.nemo.claims.controller.payload.response.IncidentResponseV1;
import com.doing.nemo.middleware.client.payload.request.MiddlewareUpdateIncidentsRequest;

public class ExternalCommunicationException extends Exception{
    private MiddlewareUpdateIncidentsRequest request;
    private IncidentResponseV1 incidentResponseV1;

    public ExternalCommunicationException(String message, Throwable cause, MiddlewareUpdateIncidentsRequest request, IncidentResponseV1 incidentResponseV1){
        super(message,cause);
        this.setRequest(request);
    }

    public ExternalCommunicationException(String message, MiddlewareUpdateIncidentsRequest request, IncidentResponseV1 incidentResponseV1){
        super(message);
        this.setRequest(request);
    }

    public MiddlewareUpdateIncidentsRequest getRequest() {
        return request;
    }

    public IncidentResponseV1 getIncidentResponseV1(){return incidentResponseV1;}

    public void setRequest(MiddlewareUpdateIncidentsRequest request) {
        this.request = request;
    }

    public void setIncidentResponseV1(IncidentResponseV1 incidentResponseV1) { this.incidentResponseV1 = incidentResponseV1; }
}
