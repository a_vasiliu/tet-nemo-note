package com.doing.nemo.claims.exception;

import com.doing.nemo.commons.exception.AbstractException;
import com.doing.nemo.commons.exception.Code;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.FORBIDDEN, value = HttpStatus.FORBIDDEN)
public class ForbiddenException extends AbstractException {

    public ForbiddenException() {
        super();
    }

    public ForbiddenException(String message) {
        super(message);
    }

    public ForbiddenException(String message, Throwable e) {
        super(message, e);
    }

    public ForbiddenException(Code messageCode) {
        super(messageCode);
    }

    public ForbiddenException(String message, Code messageCode) {
        super(message, messageCode);
    }

    public ForbiddenException(Code messageCode, Throwable e) {
        super(messageCode, e);
    }

    @ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR, value = HttpStatus.INTERNAL_SERVER_ERROR)
    public static class IllegalArgumentException extends AbstractException {

        public IllegalArgumentException() {
            super();
        }

        public IllegalArgumentException(String message) {
            super(message);
        }

        public IllegalArgumentException(String message, Throwable e) {
            super(message, e);
        }

        public IllegalArgumentException(Code messageCode) {
            super(messageCode);
        }

        public IllegalArgumentException(String message, Code messageCode) {
            super(message, messageCode);
        }

        public IllegalArgumentException(Code messageCode, Throwable e) {
            super(messageCode, e);
        }
    }
}
