package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.controller.payload.request.InsuranceManagerRequestV1;
import com.doing.nemo.claims.controller.payload.response.ClaimsResponsePaginationV1;
import com.doing.nemo.claims.controller.payload.response.InsuranceManagerResponseV1;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.settings.InsuranceManagerEntity;
import com.doing.nemo.claims.exception.NotFoundException;
import com.doing.nemo.claims.repository.InsuranceManagerRepository;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
public class InsuranceManagerAdapter {

    @Autowired
    private InsuranceManagerRepository insuranceManagerRepository;

    private static Logger LOGGER = LoggerFactory.getLogger(InsuranceManagerAdapter.class);

    public InsuranceManagerEntity adptFromIManaRequToIManaEntityWithID(InsuranceManagerRequestV1 insuranceManagerRequest, UUID uuid) {
        Optional<InsuranceManagerEntity> insuranceManagerEntity1 = insuranceManagerRepository.findById(uuid);
        if (!insuranceManagerEntity1.isPresent()) {
            LOGGER.debug("Insurance Manager not found");
            throw new NotFoundException("Insurance Manager not found", com.doing.nemo.claims.validation.MessageCode.CLAIMS_1010);
        }
        InsuranceManagerEntity insuranceManagerEntity = insuranceManagerEntity1.get();
        InsuranceManagerEntity insuranceManagerEntity2 = adptFromIManaRequToIManagEntity(insuranceManagerRequest);
        insuranceManagerEntity2.setId(insuranceManagerEntity.getId());
        return insuranceManagerEntity2;
    }


    public static InsuranceManagerEntity adptFromIManaRequToIManagEntity(InsuranceManagerRequestV1 insuranceManagerRequest) {
        InsuranceManagerEntity insuranceManagerEntity = new InsuranceManagerEntity();
        insuranceManagerEntity.setCode(insuranceManagerRequest.getCode());
        insuranceManagerEntity.setName(insuranceManagerRequest.getName());
        insuranceManagerEntity.setAddress(insuranceManagerRequest.getAddress());
        insuranceManagerEntity.setZipCode(insuranceManagerRequest.getZipCode());
        insuranceManagerEntity.setCity(insuranceManagerRequest.getCity());
        insuranceManagerEntity.setState(insuranceManagerRequest.getState());
        insuranceManagerEntity.setProvince(insuranceManagerRequest.getState());
        insuranceManagerEntity.setReferencePerson(insuranceManagerRequest.getReferencePerson());
        insuranceManagerEntity.setTelephone(insuranceManagerRequest.getTelephone());
        insuranceManagerEntity.setFax(insuranceManagerRequest.getFax());
        insuranceManagerEntity.setEmail(insuranceManagerRequest.getEmail());
        insuranceManagerEntity.setWebSite(insuranceManagerRequest.getWebSite());
        insuranceManagerEntity.setActive(insuranceManagerRequest.getActive());

        return insuranceManagerEntity;
    }

    public static InsuranceManagerResponseV1 adptFromIManaEntityToIManagRespo(InsuranceManagerEntity insuranceManagerEntity) {
        InsuranceManagerResponseV1 response = new InsuranceManagerResponseV1();
        response.setId(insuranceManagerEntity.getId());
        response.setCode(insuranceManagerEntity.getCode());
        response.setName(insuranceManagerEntity.getName());
        response.setAddress(insuranceManagerEntity.getAddress());
        response.setZipCode(insuranceManagerEntity.getZipCode());
        response.setCity(insuranceManagerEntity.getCity());
        response.setProvince(insuranceManagerEntity.getProvince());
        response.setState(insuranceManagerEntity.getState());
        response.setTelephone(insuranceManagerEntity.getTelephone());
        response.setFax(insuranceManagerEntity.getFax());
        response.setEmail(insuranceManagerEntity.getEmail());
        response.setWebSite(insuranceManagerEntity.getWebSite());
        response.setReferencePerson(insuranceManagerEntity.getReferencePerson());
        response.setActive(insuranceManagerEntity.getActive());
        response.setRifCode(insuranceManagerEntity.getRifCode());

        return response;
    }

    public static List<InsuranceManagerResponseV1> adptFromIManaEntityToIManagRespoList(List<InsuranceManagerEntity> insuranceManagerEntityList) {
        List<InsuranceManagerResponseV1> responseV1 = new LinkedList<>();
        for (InsuranceManagerEntity insuranceManagerEntity : insuranceManagerEntityList) {
            responseV1.add(adptFromIManaEntityToIManagRespo(insuranceManagerEntity));
        }
        return responseV1;
    }
    
    public static ClaimsResponsePaginationV1 adptInsuranceManagerPaginationToClaimsResponsePagination(Pagination<InsuranceManagerEntity> pagination) {

        ClaimsResponsePaginationV1<InsuranceManagerResponseV1> claimsPaginationResponse = new ClaimsResponsePaginationV1<>();
        claimsPaginationResponse.setStats(pagination.getStats());
        claimsPaginationResponse.setItems(adptFromIManaEntityToIManagRespoList(pagination.getItems()));

        return claimsPaginationResponse;
    }

}
