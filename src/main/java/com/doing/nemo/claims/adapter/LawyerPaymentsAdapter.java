package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.controller.payload.response.LawyerPaymentsResponseV1;
import com.doing.nemo.claims.entity.settings.LawyerPaymentsEntity;
import org.springframework.stereotype.Component;

@Component
public class LawyerPaymentsAdapter {

    public static LawyerPaymentsResponseV1 adptLawyerPaymentsToLawyerPaymentsResponse(LawyerPaymentsEntity lawyerPayments){

        LawyerPaymentsResponseV1 lawyerPaymentsResponse = new LawyerPaymentsResponseV1();

        lawyerPaymentsResponse.setId(lawyerPayments.getId());
        lawyerPaymentsResponse.setPracticeId(lawyerPayments.getPracticeId());
        lawyerPaymentsResponse.setPlate(lawyerPayments.getPlate());
        lawyerPaymentsResponse.setDateAccident(lawyerPayments.getDateAccident());
        lawyerPaymentsResponse.setPaymentType(lawyerPayments.getPaymentType());
        lawyerPaymentsResponse.setAmount(lawyerPayments.getAmount());
        lawyerPaymentsResponse.setBank(lawyerPayments.getBank());
        lawyerPaymentsResponse.setTechnicalShutdown(lawyerPayments.getTechnicalShutdown());
        lawyerPaymentsResponse.setAccSale(lawyerPayments.getAccSale());
        lawyerPaymentsResponse.setDamage(lawyerPayments.getDamage());
        lawyerPaymentsResponse.setExpensesSuit(lawyerPayments.getExpensesSuit());
        lawyerPaymentsResponse.setFee(lawyerPayments.getFee());
        lawyerPaymentsResponse.setLawyerCode(lawyerPayments.getLawyerCode());
        lawyerPaymentsResponse.setNote(lawyerPayments.getNote());
        lawyerPaymentsResponse.setTransactionNumber(lawyerPayments.getTransactionNumber());
        lawyerPaymentsResponse.setStatus(lawyerPayments.getStatus());

        return lawyerPaymentsResponse;

    }



}
