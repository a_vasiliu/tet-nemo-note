package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.PersonalDataRequestV1;
import com.doing.nemo.claims.controller.payload.response.PaginationResponseV1;
import com.doing.nemo.claims.controller.payload.response.PersonalDataResponseV1;
import com.doing.nemo.claims.entity.claims.ClaimsDamagedCustomerEntity;
import com.doing.nemo.claims.entity.claims.ClaimsDamagedFleetManagerEntity;
import com.doing.nemo.claims.entity.enumerated.DTO.PageStats;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.Customer;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.FleetManager;
import com.doing.nemo.claims.entity.jsonb.personalData.FleetManagerPersonalData;
import com.doing.nemo.claims.entity.settings.PersonalDataEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

@Component
public class PersonalDataAdapter {


    @Autowired
    private CustomTypeRiskAdapter customTypeRiskAdapter;

    @Autowired
    private InsurancePolicyPersonalDataAdapter insurancePolicyPersonalDataAdapter;

    @Autowired
    private ContractPersonalDataAdapter contractPersonalDataAdapter;

    public PersonalDataEntity adptFromPersonalDataRequestToPersonalDataEntity(PersonalDataRequestV1 personalDataRequestV1) {
        PersonalDataEntity personalDataEntity = new PersonalDataEntity();

        personalDataEntity.setCustomerId(personalDataRequestV1.getCustomerId());
        personalDataEntity.setName(personalDataRequestV1.getName());
        personalDataEntity.setAddress(personalDataRequestV1.getAddress());
        personalDataEntity.setZipCode(personalDataRequestV1.getZipCode());
        personalDataEntity.setCity(personalDataRequestV1.getCity());
        personalDataEntity.setProvince(personalDataRequestV1.getProvince());
        personalDataEntity.setState(personalDataRequestV1.getState());
        personalDataEntity.setPhoneNumber(personalDataRequestV1.getPhoneNumber());
        personalDataEntity.setFax(personalDataRequestV1.getFax());
        personalDataEntity.setEmail(personalDataRequestV1.getEmail());
        personalDataEntity.setPec(personalDataRequestV1.getPec());
        personalDataEntity.setWebSite(personalDataRequestV1.getWebSite());
        personalDataEntity.setVatNumber(personalDataRequestV1.getVatNumber());
        personalDataEntity.setFiscalCode(personalDataRequestV1.getFiscalCode());
        personalDataEntity.setContact(personalDataEntity.getContact());
        personalDataEntity.setPersonalGroup(personalDataRequestV1.getPersonalGroup());
        personalDataEntity.setPersonalFatherId(personalDataRequestV1.getPersonalFatherId());

        //Nel caso in cui entro in questo adapter sto inserendo un nuovo fleetManager da NEMO

        personalDataEntity.setFleetManagerPersonalData(personalDataRequestV1.getFleetManagerPersonalData());
        personalDataEntity.setActive(true);
        personalDataEntity.setCreatedAt(DateUtil.getNowInstant());


        if (personalDataRequestV1.getInsurancePolicyPersonalDataList() != null && !personalDataRequestV1.getInsurancePolicyPersonalDataList().isEmpty())
            personalDataEntity.setInsurancePolicyPersonalDataEntityList(insurancePolicyPersonalDataAdapter.adptFromInsurancePolicyPersonalDataRequestToInsurancePolicyPersonalDataEntityList(personalDataRequestV1.getInsurancePolicyPersonalDataList()));

        if (personalDataRequestV1.getContractList() != null && !personalDataRequestV1.getContractList().isEmpty())
            personalDataEntity.setContractEntityList(contractPersonalDataAdapter.adptFromContractRequestToContractEntityList(personalDataRequestV1.getContractList()));

        if (personalDataRequestV1.getCustomTypeRiskList() != null && !personalDataRequestV1.getCustomTypeRiskList().isEmpty())
            personalDataEntity.setCustomTypeRiskEntityList(customTypeRiskAdapter.adptFromCustomTypeRiskRequestToCustomTypeRiskEntityList(personalDataRequestV1.getCustomTypeRiskList()));

        return personalDataEntity;
    }


    public static PersonalDataResponseV1 adptFromPersonalDataEntityToPersonalDataResponseV1(PersonalDataEntity personalDataEntity) {
        PersonalDataResponseV1 personalDataResponseV1 = new PersonalDataResponseV1();
        if (personalDataEntity == null) return null;

        personalDataResponseV1.setId(personalDataEntity.getId());
        personalDataResponseV1.setPersonalCode(personalDataEntity.getPersonalCode());
        personalDataResponseV1.setCustomerId(personalDataEntity.getCustomerId());
        personalDataResponseV1.setName(personalDataEntity.getName());
        personalDataResponseV1.setAddress(personalDataEntity.getAddress());
        personalDataResponseV1.setZipCode(personalDataEntity.getZipCode());
        personalDataResponseV1.setCity(personalDataEntity.getCity());
        personalDataResponseV1.setProvince(personalDataEntity.getProvince());
        personalDataResponseV1.setState(personalDataEntity.getState());
        personalDataResponseV1.setPhoneNumber(personalDataEntity.getPhoneNumber());
        personalDataResponseV1.setFax(personalDataEntity.getFax());
        personalDataResponseV1.setEmail(personalDataEntity.getEmail());
        personalDataResponseV1.setPec(personalDataEntity.getPec());
        personalDataResponseV1.setWebSite(personalDataEntity.getWebSite());
        personalDataResponseV1.setVatNumber(personalDataEntity.getVatNumber());
        personalDataResponseV1.setFiscalCode(personalDataEntity.getFiscalCode());
        personalDataResponseV1.setContact(personalDataResponseV1.getContact());
        personalDataResponseV1.setPersonalGroup(personalDataEntity.getPersonalGroup());
        personalDataResponseV1.setPersonalFatherId(personalDataEntity.getPersonalFatherId());
        personalDataResponseV1.setPersonalFatherName(personalDataEntity.getPersonalFatherName());
        personalDataResponseV1.setFleetManagerPersonalData(personalDataEntity.getFleetManagerPersonalData());
        personalDataResponseV1.setContractEntityList(personalDataEntity.getContractEntityList());
        personalDataResponseV1.setInsurancePolicyPersonalDataEntityList(personalDataEntity.getInsurancePolicyPersonalDataEntityList());
        personalDataResponseV1.setCustomTypeRiskEntityList(personalDataEntity.getCustomTypeRiskEntityList());
        personalDataResponseV1.setActive(personalDataEntity.getActive());
        personalDataResponseV1.setCreatedAt(DateUtil.convertUTCInstantToIS08601String(personalDataEntity.getCreatedAt()));


        return personalDataResponseV1;
    }


    public static List<PersonalDataResponseV1> adptFromPersonalDataEntityToPersonalDataResponseV1List(List<PersonalDataEntity> personalDataEntityList) {
        List<PersonalDataResponseV1> personalDataResponseV1 = new LinkedList<>();
        for (PersonalDataEntity personalDatasEntity : personalDataEntityList) {
            personalDataResponseV1.add(adptFromPersonalDataEntityToPersonalDataResponseV1(personalDatasEntity));
        }
        return personalDataResponseV1;
    }

    public static PaginationResponseV1<PersonalDataResponseV1> adptPagination(List<PersonalDataEntity> personalDataEntityList, int page, int pageSize, Long itemCount) {

        double totPages = Math.ceil((double) itemCount.intValue() / pageSize);
        PageStats pageStats = new PageStats();
        pageStats.setCurrentPage(page);
        pageStats.setItemCount(itemCount.intValue());
        pageStats.setPageSize(pageSize);
        pageStats.setPageCount((long)totPages);

        PaginationResponseV1<PersonalDataResponseV1> paginationResponseV1 = new PaginationResponseV1(pageStats,adptFromPersonalDataEntityToPersonalDataResponseV1List(personalDataEntityList));

        return paginationResponseV1;
    }

    //DA ELIMINARE PROBABILMENTE
    public static PersonalDataEntity adptFromCustomerToPersonalData(PersonalDataEntity oldCustomer, Customer newCustomer, List<FleetManager> fleetManagers){
        if(oldCustomer == null){
            oldCustomer = new PersonalDataEntity();
        }
        oldCustomer.setName(newCustomer.getLegalName());
        if(newCustomer.getMainAddress() != null){
            oldCustomer.setAddress(newCustomer.getMainAddress().getFormattedAddress());
            oldCustomer.setZipCode(newCustomer.getMainAddress().getZip());
            oldCustomer.setCity(newCustomer.getMainAddress().getLocality());
            oldCustomer.setProvince(newCustomer.getMainAddress().getProvince());
            oldCustomer.setState(newCustomer.getMainAddress().getState());
        }
        oldCustomer.setPhoneNumber(newCustomer.getPhonenr());
        oldCustomer.setEmail(newCustomer.getEmail());
        oldCustomer.setPec(newCustomer.getPec());
        oldCustomer.setVatNumber(newCustomer.getVatNumber());
        if(oldCustomer.getCreatedAt() == null){
            oldCustomer.setCreatedAt(DateUtil.getNowInstant());
        }
        oldCustomer.setCustomerId(newCustomer.getCustomerId());
        oldCustomer.setActive(true);

        //SETTARE I FLEET MANAGER
        List<FleetManagerPersonalData> fleetManagerPersonalData = oldCustomer.getFleetManagerPersonalData();
        List<FleetManagerPersonalData> fleetManagerPersonalDataNew = new ArrayList<>();
        if(fleetManagers != null){
            for(FleetManager fleetManager : fleetManagers){
                FleetManagerPersonalData newFleetManager = new FleetManagerPersonalData();
                newFleetManager.setFleetManagerId(fleetManager.getId());
                if(fleetManagerPersonalData == null){
                    fleetManagerPersonalData = new ArrayList<>();
                }
                FleetManagerPersonalData oldFleetManager = getFleetManagerSearched(fleetManagerPersonalData, newFleetManager);
                if(oldFleetManager == null){
                    oldFleetManager = new FleetManagerPersonalData();
                }
                if(fleetManager.getLastName() != null && fleetManager.getFirstName() != null) {
                    oldFleetManager.setFleetManagerName(fleetManager.getLastName() + " " + fleetManager.getFirstName());
                }else{
                    oldFleetManager.setFleetManagerName(fleetManager.getFirstName());
                }
                oldFleetManager.setFleetManagerMobilePhone(fleetManager.getSecondaryPhone());
                oldFleetManager.setFleetManagerPhone(fleetManager.getPhone());
                oldFleetManager.setFleetManagerPrimaryEmail(fleetManager.getEmail());
                oldFleetManager.setFleetManagerId(fleetManager.getId());
                oldFleetManager.setIsImported(fleetManager.getIsImported());
                oldFleetManager.setDisableNotification(fleetManager.isDisableNotification());

                fleetManagerPersonalDataNew.add(oldFleetManager);
            }
            oldCustomer.setFleetManagerPersonalData(buildFinalFleetManagerList(fleetManagerPersonalData,fleetManagerPersonalDataNew));
        }
        return oldCustomer;
    }

    /*Questo metodo si occupa di aggiungere alla nuova lista dei fleet manager gli oggetti mancanti dalla precedente lista, ritornando la lista definitiva.*/
    private static List<FleetManagerPersonalData> buildFinalFleetManagerList(List<FleetManagerPersonalData> oldList, List<FleetManagerPersonalData> newList){
        if(oldList!=null && !oldList.isEmpty()){ //Se nella vecchia lista non avevo elementi, non ho nulla da aggiungere, altrimenti si.
            if(newList==null || newList.isEmpty()){ //Se la nuova lista è vuota, non devo far altro che aggiungere tutti gli elementi della vecchia.
                if(newList==null){ //Prima però controllo se deve essere istanziata.
                    newList = new LinkedList<>();
                }
                newList.addAll(oldList); //Aggiungo nella nuova lista tutti gli elementi della vecchia ed esco dall'if.
            } else { //Se la nuova lista non è vuota (in pratica sempre).
                for(FleetManagerPersonalData current: oldList){ //Scorro la vecchia lista per controllare tutti gli elementi.
                    if(!newList.contains(current)){ //Se la nuova lista non contiene l'elemento
                        newList.add(current); //allora lo aggiungo.
                    }
                }
            }

        }
        return newList;
    }

    //Inserisco nella lista dei damaged i fleetmanager che non sono presenti in finallist
    public static List<FleetManager> buildFinalFleetManagerListofDamaged(List<FleetManagerPersonalData> finalList, List<FleetManager> damagedList){

        //Controllo se finallist è null o vuota
        if (finalList != null && !finalList.isEmpty()) {
            //Itero su finallist
            for (FleetManagerPersonalData current:finalList){
                    //Se current non è già contenuto nella lista dei damagd lo aggiungo
                if (damagedList != null && !damagedList.isEmpty()) {

                    //Costruisco un oggetto Fleet Manager a partire da un Fleet Manager Personal Data
                    FleetManager currentFleetManager = adaptFromFleetManagerPersonalDataToFleetManager(current);

                    if (!damagedList.contains(currentFleetManager)){
                        damagedList.add(currentFleetManager);
                    }
                }
            }
        }
        return damagedList;
    }

    //Converto un FleetManagerPersonalData in un FleetManager
private static FleetManager adaptFromFleetManagerPersonalDataToFleetManager(FleetManagerPersonalData fleetManagerPersonalData){

        FleetManager fleetManager = new FleetManager();
        String[] name;
        if(fleetManagerPersonalData!=null){
            if(fleetManagerPersonalData.getFleetManagerName()!=null){
                name  = fleetManagerPersonalData.getFleetManagerName().trim().split("\\s+");
                fleetManager.setFirstName(name[0]);
                fleetManager.setLastName(name[1]);
            }
            fleetManager.setId(fleetManagerPersonalData.getFleetManagerId());
            fleetManager.setEmail(fleetManagerPersonalData.getFleetManagerPrimaryEmail());
            fleetManager.setPhone(fleetManagerPersonalData.getFleetManagerPhone());
            fleetManager.setIsImported(fleetManagerPersonalData.getIsImported());
            fleetManager.setDisableNotifiaction(fleetManager.isDisableNotification());
        }


        return fleetManager;
}

        private static FleetManagerPersonalData getFleetManagerSearched(List<FleetManagerPersonalData> fleetManagerPersonalData, FleetManagerPersonalData newFleetManager){
        if(fleetManagerPersonalData == null || fleetManagerPersonalData.isEmpty()){
            return null;
        }
        FleetManagerPersonalData fleetManagerFound = null;
        Iterator<FleetManagerPersonalData> iterator = fleetManagerPersonalData.iterator();
        boolean isFound = false;
        while(iterator.hasNext() && !isFound){
            FleetManagerPersonalData fleetManagerPersonalData1 = iterator.next();
            if(fleetManagerPersonalData1.equalsFleetManager(newFleetManager)){
                fleetManagerFound = fleetManagerPersonalData1;
                isFound = true;
            }
        }

        return fleetManagerFound;
    }



    //region NEW ADAPTER
    public static PersonalDataEntity adptFromCustomerToPersonalData(PersonalDataEntity oldCustomer, ClaimsDamagedCustomerEntity newCustomer, List<ClaimsDamagedFleetManagerEntity> fleetManagers){
        if(oldCustomer == null){
            oldCustomer = new PersonalDataEntity();
        }
        oldCustomer.setName(newCustomer.getLegalName());
        if(newCustomer.getMainAddress() != null){
            oldCustomer.setAddress(newCustomer.getMainAddress().getFormattedAddress());
            oldCustomer.setZipCode(newCustomer.getMainAddress().getZip());
            oldCustomer.setCity(newCustomer.getMainAddress().getLocality());
            oldCustomer.setProvince(newCustomer.getMainAddress().getProvince());
            oldCustomer.setState(newCustomer.getMainAddress().getState());
        }
        oldCustomer.setPhoneNumber(newCustomer.getPhonenr());
        oldCustomer.setEmail(newCustomer.getEmail());
        oldCustomer.setPec(newCustomer.getPec());
        oldCustomer.setVatNumber(newCustomer.getVatNumber());
        if(oldCustomer.getCreatedAt() == null){
            oldCustomer.setCreatedAt(DateUtil.getNowInstant());
        }
        oldCustomer.setCustomerId(newCustomer.getCustomerId().toString());
        oldCustomer.setActive(true);

        //SETTARE I FLEET MANAGER
        List<FleetManagerPersonalData> fleetManagerPersonalData = oldCustomer.getFleetManagerPersonalData();
        List<FleetManagerPersonalData> fleetManagerPersonalDataNew = new ArrayList<>();
        if(fleetManagers != null){
            for(ClaimsDamagedFleetManagerEntity fleetManager : fleetManagers){
                FleetManagerPersonalData newFleetManager = new FleetManagerPersonalData();
                newFleetManager.setFleetManagerId(fleetManager.getFleetManagerId());
                if(fleetManagerPersonalData == null){
                    fleetManagerPersonalData = new ArrayList<>();
                }
                FleetManagerPersonalData oldFleetManager = getFleetManagerSearched(fleetManagerPersonalData, newFleetManager);
                if(oldFleetManager == null){
                    oldFleetManager = new FleetManagerPersonalData();
                }
                oldFleetManager.setFleetManagerName(fleetManager.getLastName()+ " "+fleetManager.getFirstName());
                oldFleetManager.setFleetManagerMobilePhone(fleetManager.getSecondaryPhone());
                oldFleetManager.setFleetManagerPhone(fleetManager.getPhone());
                oldFleetManager.setFleetManagerPrimaryEmail(fleetManager.getEmail());
                oldFleetManager.setFleetManagerId(fleetManager.getFleetManagerId());

                fleetManagerPersonalDataNew.add(oldFleetManager);
            }
            oldCustomer.setFleetManagerPersonalData(fleetManagerPersonalDataNew);
        }
        return oldCustomer;
    }
    //endregion

}

