package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.complaint.FromCompanyRequest;
import com.doing.nemo.claims.controller.payload.response.complaint.FromCompanyResponse;
import com.doing.nemo.claims.entity.claims.ClaimsFromCompanyEntity;
import com.doing.nemo.claims.entity.jsonb.complaint.jsonbComplaint.FromCompany;
import org.springframework.stereotype.Service;

@Service
public class FromCompanyAdapter {

    public static FromCompany adptFromCompanyRequestToFromCompany(FromCompanyRequest fromCompanyRequest) {
        if (fromCompanyRequest != null) {
            FromCompany fromCompany = new FromCompany();
            fromCompany
                .setCompany( fromCompanyRequest.getCompany() )
                .setExpert(fromCompanyRequest.getExpert())
                .setInspectorate(fromCompanyRequest.getInspectorate())
                .setNumberSx(fromCompanyRequest.getNumberSx())
                .setTypeSx(fromCompanyRequest.getTypeSx())
                .setNote(fromCompanyRequest.getNote())
                .setStatus(fromCompanyRequest.getStatus())
                .setGlobalReserve(fromCompanyRequest.getGlobalReserve())
                .setTotalPaid(fromCompanyRequest.getTotalPaid())
            ;
            //fromCompany.setDwlMan(DateUtil.convertIS08601StringToUTCDate(fromCompanyRequest.getDwlMan()));
            //fromCompany.setLastUpdate(fromCompanyRequest.getLastUpdate());
            return fromCompany;
        }
        return null;
    }

    public static FromCompany adptFromCompanyRequestToFromCompanyAdmin(FromCompanyRequest fromCompanyRequest, FromCompany fromCompany){
        if(fromCompanyRequest != null){
            if(fromCompany == null)
                fromCompany = new FromCompany();
            fromCompany.setCompany(fromCompanyRequest.getCompany());
            fromCompany.setExpert(fromCompanyRequest.getExpert());
            fromCompany.setInspectorate(fromCompanyRequest.getInspectorate());
            fromCompany.setNumberSx(fromCompanyRequest.getNumberSx());
            fromCompany.setTypeSx(fromCompanyRequest.getTypeSx());
            fromCompany.setNote(fromCompanyRequest.getNote());
            return fromCompany;
        }

        return fromCompany;
    }

    public static FromCompanyResponse adptFromCompanyToFromCompanyFromCompanyResponse(FromCompany fromCompany) {

        if (fromCompany != null) {

            FromCompanyResponse fromCompanyResponse = new FromCompanyResponse();
            fromCompanyResponse.setCompany(fromCompany.getCompany());
            fromCompanyResponse.setDwlMan(DateUtil.convertUTCDateToIS08601String(fromCompany.getDwlMan()));
            fromCompanyResponse.setExpert(fromCompany.getExpert());
            fromCompanyResponse.setInspectorate(fromCompany.getInspectorate());
            fromCompanyResponse.setLastUpdate(fromCompany.getLastUpdate());
            fromCompanyResponse.setNumberSx(fromCompany.getNumberSx());
            fromCompanyResponse.setTypeSx(fromCompany.getTypeSx());
            fromCompanyResponse.setNote(fromCompany.getNote());
            fromCompanyResponse.setStatus(fromCompany.getStatus());
            fromCompanyResponse.setGlobalReserve(fromCompany.getGlobalReserve());
            fromCompanyResponse.setTotalPaid(fromCompany.getTotalPaid());

            return fromCompanyResponse;
        }
        return null;


    }

//NEW ADAPTER

    public static ClaimsFromCompanyEntity adptFromCompanyEntityToFromClaimsCompanyNewEntity(FromCompany fromCompanyEntity) {

        if (fromCompanyEntity != null) {

            ClaimsFromCompanyEntity fromCompany = new ClaimsFromCompanyEntity();
            fromCompany.setCompany(fromCompanyEntity.getCompany());
            fromCompany.setExpert(fromCompanyEntity.getExpert());
            fromCompany.setDwlMan(DateUtil.convertIS08601StringToUTCInstant(DateUtil.convertUTCDateToIS08601String(fromCompanyEntity.getDwlMan())));
            fromCompany.setLastUpdate(DateUtil.convertIS08601StringToUTCInstant(DateUtil.convertUTCDateToIS08601String(fromCompanyEntity.getLastUpdate())));
            fromCompany.setInspectorate(fromCompanyEntity.getInspectorate());
            fromCompany.setNumberSx(fromCompanyEntity.getNumberSx());
            fromCompany.setTypeSx(fromCompanyEntity.getTypeSx());
            fromCompany.setNote(fromCompanyEntity.getNote());
            fromCompany.setStatus(fromCompanyEntity.getStatus());
            fromCompany.setGlobalReserve(fromCompanyEntity.getGlobalReserve());
            fromCompany.setTotalPaid(fromCompanyEntity.getTotalPaid());

            return fromCompany;
        }
        return null;

    }




    public static FromCompany adptFromClaimsFromCompanyToFromClaimsJsonb(ClaimsFromCompanyEntity claimsFromCompanyEntity) {

        if (claimsFromCompanyEntity != null) {

            FromCompany fromCompanyJsonb = new FromCompany();
            fromCompanyJsonb.setCompany(claimsFromCompanyEntity.getCompany());
            fromCompanyJsonb.setExpert(claimsFromCompanyEntity.getExpert());
            fromCompanyJsonb.setInspectorate(claimsFromCompanyEntity.getInspectorate());
            fromCompanyJsonb.setNumberSx(claimsFromCompanyEntity.getNumberSx());
            fromCompanyJsonb.setTypeSx(claimsFromCompanyEntity.getTypeSx());
            fromCompanyJsonb.setNote(claimsFromCompanyEntity.getNote());
            fromCompanyJsonb.setStatus(claimsFromCompanyEntity.getStatus());
            fromCompanyJsonb.setGlobalReserve(claimsFromCompanyEntity.getGlobalReserve());
            fromCompanyJsonb.setTotalPaid(claimsFromCompanyEntity.getTotalPaid());
            fromCompanyJsonb.setDwlMan(DateUtil.convertIS08601StringToUTCDate(DateUtil.convertUTCInstantToIS08601String(claimsFromCompanyEntity.getDwlMan())));
            fromCompanyJsonb.setLastUpdate(DateUtil.convertIS08601StringToUTCDate(DateUtil.convertUTCInstantToIS08601String(claimsFromCompanyEntity.getLastUpdate())));

            return fromCompanyJsonb;
        }
        return null;

    }

}
