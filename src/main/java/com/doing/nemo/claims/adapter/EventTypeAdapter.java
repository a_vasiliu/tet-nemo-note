package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.controller.payload.request.EventTypeRequestV1;
import com.doing.nemo.claims.controller.payload.response.ClaimsResponsePaginationV1;
import com.doing.nemo.claims.controller.payload.response.EventTypeResponseV1;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.settings.EventTypeEntity;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

@Component
public class EventTypeAdapter {

    public static EventTypeEntity getEventTypeEntity(EventTypeRequestV1 eventTypeRequestV1) {

        EventTypeEntity eventTypeEntity = new EventTypeEntity();

        eventTypeEntity.setDescription(eventTypeRequestV1.getDescription());
        eventTypeEntity.setCreateAttachments(eventTypeRequestV1.getCreateAttachments());
        eventTypeEntity.setCatalog(eventTypeRequestV1.getCatalog());
        eventTypeEntity.setAttachmentsType(eventTypeRequestV1.getAttachmentsType());
        eventTypeEntity.setOnlyLast(eventTypeRequestV1.getOnlyLast());
        eventTypeEntity.setActive(eventTypeRequestV1.getActive());
        eventTypeEntity.setEventType(eventTypeRequestV1.getEventType());

        return eventTypeEntity;
    }

    public static EventTypeEntity getEventTypeEntityWithId(EventTypeRequestV1 eventTypeRequestV1) {

        EventTypeEntity eventTypeEntity = new EventTypeEntity();

        eventTypeEntity.setId(eventTypeRequestV1.getId());
        eventTypeEntity.setDescription(eventTypeRequestV1.getDescription());
        eventTypeEntity.setCreateAttachments(eventTypeRequestV1.getCreateAttachments());
        eventTypeEntity.setCatalog(eventTypeRequestV1.getCatalog());
        eventTypeEntity.setAttachmentsType(eventTypeRequestV1.getAttachmentsType());
        eventTypeEntity.setOnlyLast(eventTypeRequestV1.getOnlyLast());
        eventTypeEntity.setActive(eventTypeRequestV1.getActive());
        eventTypeEntity.setEventType(eventTypeRequestV1.getEventType());

        return eventTypeEntity;
    }

    public static EventTypeResponseV1 getEventTypeAdapter(EventTypeEntity eventTypeEntity) {

        EventTypeResponseV1 eventTypeResponseV1 = new EventTypeResponseV1();

        eventTypeResponseV1.setId(eventTypeEntity.getId());
        eventTypeResponseV1.setDescription(eventTypeEntity.getDescription());
        eventTypeResponseV1.setCreateAttachments(eventTypeEntity.getCreateAttachments());
        eventTypeResponseV1.setCatalog(eventTypeEntity.getCatalog());
        eventTypeResponseV1.setAttachmentsType(eventTypeEntity.getAttachmentsType());
        eventTypeResponseV1.setOnlyLast(eventTypeEntity.getOnlyLast());
        eventTypeResponseV1.setActive(eventTypeEntity.getActive());
        eventTypeResponseV1.setEventType(eventTypeEntity.getEventType());
        eventTypeResponseV1.setTypeComplaint(eventTypeEntity.getTypeComplaint());

        return eventTypeResponseV1;
    }

    public static List<EventTypeResponseV1> getEventTypeListAdapter(List<EventTypeEntity> eventTypeEntityList) {

        List<EventTypeResponseV1> eventTypeResponseV1List = new LinkedList<>();

        for (EventTypeEntity eventTypeEntity : eventTypeEntityList) {
            eventTypeResponseV1List.add(getEventTypeAdapter(eventTypeEntity));
        }

        return eventTypeResponseV1List;
    }

    public static ClaimsResponsePaginationV1 adptEventTypePaginationToClaimsResponsePagination(Pagination<EventTypeEntity> pagination) {

        ClaimsResponsePaginationV1<EventTypeResponseV1> claimsPaginationResponse = new ClaimsResponsePaginationV1<>();

        claimsPaginationResponse.setStats(pagination.getStats());
        claimsPaginationResponse.setItems(getEventTypeListAdapter(pagination.getItems()));

        return claimsPaginationResponse;
    }
}
