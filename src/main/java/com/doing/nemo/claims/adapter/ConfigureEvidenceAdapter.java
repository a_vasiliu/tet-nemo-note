package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.controller.payload.request.ConfigureEvidenceRequestV1;
import com.doing.nemo.claims.controller.payload.response.ConfigureEvidenceResponseV1;
import com.doing.nemo.claims.controller.payload.response.PaginationResponseV1;
import com.doing.nemo.claims.entity.enumerated.DTO.PageStats;
import com.doing.nemo.claims.entity.settings.ConfigureEvidenceEntity;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

@Component
public class ConfigureEvidenceAdapter {

    public static ConfigureEvidenceEntity adptConfigureEvidenceRequestToConfigureEvidence(ConfigureEvidenceRequestV1 configureEvidenceRequest){

        if(configureEvidenceRequest == null)
            return null;

        ConfigureEvidenceEntity configureEvidence = new ConfigureEvidenceEntity();
        configureEvidence.setDaysOfStayInTheState(configureEvidenceRequest.getDaysOfStayInTheState());
        configureEvidence.setPracticalStateEn(configureEvidenceRequest.getPracticalStateEn());
        configureEvidence.setPracticalStateIt(configureEvidenceRequest.getPracticalStateIt());
        configureEvidence.setActive(configureEvidenceRequest.getActive());

        return configureEvidence;
    }

    public static ConfigureEvidenceResponseV1 adptConfigureEvidenceToConfigureEvidenceResponse(ConfigureEvidenceEntity configureEvidence){

        if(configureEvidence == null)
            return null;

        ConfigureEvidenceResponseV1 configureEvidenceResponse = new ConfigureEvidenceResponseV1();
        configureEvidenceResponse.setId(configureEvidence.getId());
        configureEvidenceResponse.setDaysOfStayInTheState(configureEvidence.getDaysOfStayInTheState());
        configureEvidenceResponse.setPracticalStateEn(configureEvidence.getPracticalStateEn());
        configureEvidenceResponse.setPracticalStateIt(configureEvidence.getPracticalStateIt());
        configureEvidenceResponse.setActive(configureEvidence.getActive());

        return configureEvidenceResponse;
    }

    public static List<ConfigureEvidenceResponseV1> adptConfigureEvidenceToConfigureEvidenceResponse(List<ConfigureEvidenceEntity> configureEvidenceList){

        if(configureEvidenceList == null)
            return null;

        List<ConfigureEvidenceResponseV1> configureEvidenceResponseList = new LinkedList<>();

        for (ConfigureEvidenceEntity att : configureEvidenceList){
            configureEvidenceResponseList.add(adptConfigureEvidenceToConfigureEvidenceResponse(att));
        }
        return configureEvidenceResponseList;
    }

    public static PaginationResponseV1<ConfigureEvidenceResponseV1> adptPagination(List<ConfigureEvidenceEntity> configureEvidenceEntityList, int page, int pageSize, Long itemCount) {

        double totPages = Math.ceil((double) itemCount.intValue() / pageSize);
        PageStats pageStats = new PageStats();
        pageStats.setCurrentPage(page);
        pageStats.setItemCount(itemCount.intValue());
        pageStats.setPageSize(pageSize);
        pageStats.setPageCount((long)totPages);

        PaginationResponseV1<ConfigureEvidenceResponseV1> paginationResponseV1 = new PaginationResponseV1(pageStats,adptConfigureEvidenceToConfigureEvidenceResponse(configureEvidenceEntityList));

        return paginationResponseV1;
    }

}
