package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.ContractUpdateRequestV1;
import com.doing.nemo.claims.controller.payload.request.FlowContractUpdateRequestV1;
import com.doing.nemo.claims.controller.payload.request.damaged.ContractRequest;
import com.doing.nemo.claims.controller.payload.response.*;
import com.doing.nemo.claims.controller.payload.response.damaged.ContractResponse;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.claims.ClaimsDamagedContractEntity;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.esb.ContractESB.ContractEsb;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.Contract;
import com.doing.nemo.claims.entity.settings.ClaimsTypeEntity;
import com.doing.nemo.claims.entity.settings.ContractTypeEntity;
import com.doing.nemo.claims.entity.settings.FlowContract;
import com.doing.nemo.claims.entity.settings.FlowContractTypeEntity;
import com.doing.nemo.middleware.client.payload.response.MiddlewareContractDetailResponse;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Component
public class ContractAdapter {

    public static FlowContractResponseV1 getFlowContractTypeAdapter(FlowContractTypeEntity flowContractTypeEntity) {

        FlowContractResponseV1 flowContractResponseV1 = new FlowContractResponseV1();
        flowContractResponseV1.setId(flowContractTypeEntity.getId().toString());
        flowContractResponseV1.setClaimsTypeEntity(flowContractTypeEntity.getClaimsTypeEntity().getType());
        flowContractResponseV1.setCalimsAccident(flowContractTypeEntity.getClaimsTypeEntity().getClaimsAccidentType());
        flowContractResponseV1.setFlow(flowContractTypeEntity.getFlow());
        flowContractResponseV1.setRebilling(flowContractTypeEntity.getRebilling());

        return flowContractResponseV1;
    }

    public static List<FlowContractResponseV1> getFlowContractTypeListAdapter(List<FlowContractTypeEntity> flowContractTypeEntityList) {

        List<FlowContractResponseV1> flowContractResponseV1List = new LinkedList<>();

        for (FlowContractTypeEntity flowContractTypeEntity : flowContractTypeEntityList) {
            flowContractResponseV1List.add(getFlowContractTypeAdapter(flowContractTypeEntity));
        }

        return flowContractResponseV1List;
    }

    public static ContractTypeEntityResponseV1 getContractAdapter(ContractTypeEntity contractTypeEntity) {

        if (contractTypeEntity == null)
            return null;

        ContractTypeEntityResponseV1 contractTypeEntityResponseV1 = new ContractTypeEntityResponseV1();
        contractTypeEntityResponseV1.setCodContractType(contractTypeEntity.getCodContractType());
        contractTypeEntityResponseV1.setDescription(contractTypeEntity.getDescription());
        contractTypeEntityResponseV1.setFlagWS(contractTypeEntity.getFlagWS());
        contractTypeEntityResponseV1.setFlowContractResponseV1List(getFlowContractTypeListAdapter(contractTypeEntity.getFlowContractTypeList()));
        contractTypeEntityResponseV1.setId(contractTypeEntity.getId().toString());
        contractTypeEntityResponseV1.setDefaultFlow(contractTypeEntity.getDefaultFlow());
        contractTypeEntityResponseV1.setCtrnote(contractTypeEntity.getCtrnote());
        contractTypeEntityResponseV1.setFranchise(contractTypeEntity.getFranchise());
        contractTypeEntityResponseV1.setRicaricar(contractTypeEntity.getRicaricar());
        contractTypeEntityResponseV1.setActive(contractTypeEntity.getActive());

        return contractTypeEntityResponseV1;
    }

    public static List<ContractTypeEntityResponseV1> getContractListAdapter(List<ContractTypeEntity> contractTypeEntityList) {

        List<ContractTypeEntityResponseV1> contractTypeEntityResponseV1List = new LinkedList<>();

        for (ContractTypeEntity contractTypeEntity : contractTypeEntityList) {
            contractTypeEntityResponseV1List.add(getContractAdapter(contractTypeEntity));
        }

        return contractTypeEntityResponseV1List;
    }

    public static ContractTypeEntity getConntractTypeEntity(ContractUpdateRequestV1 contractUpdateRequestV1) {

        ContractTypeEntity contractTypeEntity = new ContractTypeEntity();
        contractTypeEntity.setCodContractType(contractUpdateRequestV1.getCodContractType());
        contractTypeEntity.setDescription(contractUpdateRequestV1.getDescription());
        contractTypeEntity.setFlagWS(contractUpdateRequestV1.getFlagWS());
        contractTypeEntity.setDefaultFlow(contractUpdateRequestV1.getDefaultFlow());
        contractTypeEntity.setCtrnote(contractUpdateRequestV1.getCtrnote());
        contractTypeEntity.setFranchise(contractUpdateRequestV1.getFranchise());
        contractTypeEntity.setRicaricar(contractUpdateRequestV1.getRicaricar());
        contractTypeEntity.setActive(contractUpdateRequestV1.getActive());

        return contractTypeEntity;
    }

    public static List<FlowContract> getFlowContractList(FlowContractUpdateRequestV1 contractUpdateRequestV1) {

        List<FlowContract> flowContractList = new LinkedList<>();
        for (FlowContract flowContractCurr : contractUpdateRequestV1.getFlowContractTypeList()) {

            FlowContract flowContract = new FlowContract();
            flowContract.setFlow(flowContractCurr.getFlow());
            flowContract.setRebilling(flowContractCurr.getRebilling());
            flowContract.setId(flowContractCurr.getId());
            flowContractList.add(flowContract);
        }

        return flowContractList;
    }

    public static Contract adptContractRequestToContract(ContractRequest contractRequest) {

        if (contractRequest != null) {

            Contract contract = new Contract();

            contract.setContractId(contractRequest.getContractId());
            contract.setStatus(contractRequest.getStatus());
            contract.setContractVersionId(contractRequest.getContractVersionId());
            contract.setMileage(contractRequest.getMileage());
            contract.setDuration(contractRequest.getDuration());
            contract.setContractType(contractRequest.getContractType());
            contract.setStartDate(contractRequest.getStartDate());
            contract.setEndDate(contractRequest.getEndDate());
            contract.setTakeinDate(contractRequest.getTakeInDate());
            contract.setLeasingCompanyId(contractRequest.getLeasingCompanyId());
            contract.setLeasingCompany(contractRequest.getLeasingCompany());
            contract.setSucceedingContractId(contractRequest.getSucceedingContractId());
            contract.setFleetVehicleId(contractRequest.getFleetVehicleId());
            contract.setLicensePlate(contractRequest.getLicensePlate());

            return contract;
        }

        return null;
    }

    public static ContractResponse adptContractToContractResponse(Contract contract) {

        if (contract != null) {

            ContractResponse contractResponse = new ContractResponse();

            contractResponse.setContractId(contract.getContractId());
            contractResponse.setStatus(contract.getStatus());
            contractResponse.setContractVersionId(contract.getContractVersionId());
            contractResponse.setMileage(contract.getMileage());
            contractResponse.setDuration(contract.getDuration());
            contractResponse.setContractType(contract.getContractType());
            contractResponse.setStartDate(contract.getStartDate());
            contractResponse.setEndDate(contract.getEndDate());
            contractResponse.setTakeinDate(contract.getTakeinDate());
            contractResponse.setLeasingCompanyId(contract.getLeasingCompanyId());
            contractResponse.setLeasingCompany(contract.getLeasingCompany());
            contractResponse.setSucceedingContractId(contract.getSucceedingContractId());
            contractResponse.setFleetVehicleId(contract.getFleetVehicleId());
            contractResponse.setLicensePlate(contract.getLicensePlate());

            return contractResponse;
        }
        return null;
    }

    public static Contract adptContractResponseToContract(ContractResponse contractResponse) {

        if (contractResponse != null) {

            Contract contract = new Contract();

            contract.setContractId(contractResponse.getContractId());
            contract.setStatus(contractResponse.getStatus());
            contract.setContractVersionId(contractResponse.getContractVersionId());
            contract.setMileage(contractResponse.getMileage());
            contract.setDuration(contractResponse.getDuration());
            contract.setContractType(contractResponse.getContractType());
            contract.setStartDate(contractResponse.getStartDate());
            contract.setEndDate(contractResponse.getEndDate());
            contract.setTakeinDate(contractResponse.getTakeinDate());
            contract.setLeasingCompanyId(contractResponse.getLeasingCompanyId());
            contract.setLeasingCompany(contractResponse.getLeasingCompany());
            contract.setSucceedingContractId(contractResponse.getSucceedingContractId());
            contract.setFleetVehicleId(contractResponse.getFleetVehicleId());

            return contract;
        }
        return null;
    }

    public static ContractResponse adptMiddlewareContractDetailResponseToContractResponse(ContractEsb contractEsb) {

        if (contractEsb != null) {

            ContractResponse contractResponse = new ContractResponse();

            contractResponse.setContractId(contractEsb.getContractId());
            contractResponse.setStatus(contractEsb.getStatus());
            contractResponse.setContractVersionId(contractEsb.getContractVersionId());
            contractResponse.setMileage(contractEsb.getMileage());
            contractResponse.setDuration(contractEsb.getDuration());
            contractResponse.setContractType(contractEsb.getShortContractType());
            contractResponse.setStartDate(contractEsb.getStartDate());
            contractResponse.setEndDate(contractEsb.getEndDate());
            contractResponse.setTakeinDate(contractEsb.getTakeinDate());
            contractResponse.setLeasingCompanyId(contractEsb.getLeasingCompanyId());
            contractResponse.setLeasingCompany(contractEsb.getLeasingCompany());
            contractResponse.setSucceedingContractId(contractEsb.getSucceedingContractId());
            contractResponse.setFleetVehicleId(contractEsb.getFleetVehicleId());
            contractResponse.setLicensePlate(contractEsb.getLicensePlate());
            contractResponse.setLeaseServiceComponentsList(LeaseServiceComponentsAdapter.adptFromLeaseServiceComponentsESBToLeaseServiceComponentsResponseList(contractEsb.getLeaseServiceComponentsList()));
            return contractResponse;
        }
        return null;
    }
    public static ContractResponse adptMiddlewareContractDetailResponseToContractResponse(MiddlewareContractDetailResponse contractEsb) throws ParseException {

        if (contractEsb != null) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");

            ContractResponse contractResponse = new ContractResponse();

            contractResponse.setContractId(contractEsb.getContractId());
            contractResponse.setStatus(contractEsb.getStatus());
            contractResponse.setContractVersionId(contractEsb.getContractVersionId());
            if(contractEsb.getDriverId() != null) {
                contractResponse.setDriverId(contractEsb.getDriverId().toString());
            }
            if(contractEsb.getMileage()!=null)
                contractResponse.setMileage(contractEsb.getMileage().doubleValue());
            if(contractEsb.getDuration()!=null)
                contractResponse.setDuration(contractEsb.getDuration().intValue());
            contractResponse.setContractType(contractEsb.getShortContractType());
            if(contractEsb.getStartDate() != null) {
                contractResponse.setStartDate(DateUtil.convertIS08601StringToUTCDate(contractEsb.getStartDate()));
            }
            if(contractEsb.getEndDate()!=null){
                contractResponse.setEndDate(DateUtil.convertIS08601StringToUTCDate(contractEsb.getEndDate()));
            }
            if(contractEsb.getTakeinDate()!=null){
                Date date1=new SimpleDateFormat("yyyy-MM-dd").parse(contractEsb.getTakeinDate());
                contractResponse.setTakeinDate(date1);
            }

            contractResponse.setLeasingCompanyId(contractEsb.getLeasingCompanyId());
            contractResponse.setLeasingCompany(contractEsb.getLeasingCompany());
            contractResponse.setSucceedingContractId(contractEsb.getSucceedingContractId());
            contractResponse.setFleetVehicleId(contractEsb.getFleetVehicleId());
            contractResponse.setLicensePlate(contractEsb.getLicensePlate());

            contractResponse.setLeaseServiceComponentsList(LeaseServiceComponentsAdapter.adptFromMiddlewareLeaseServiceComponentToLeaseServiceComponentsResponseList(contractEsb.getLeaseServiceComponents()));
            return contractResponse;
        }
        return null;
    }

    public static ClaimsTypeWithFlowResponseV1 adptObjectClaimsTypeWithFlowToClaimsTypeWithFlowResponse(Object[] claimsWithFlow) {

        ClaimsTypeWithFlowResponseV1 claimsTypeWithFlowResponse = new ClaimsTypeWithFlowResponseV1();

        claimsTypeWithFlowResponse.setType((String) claimsWithFlow[0]);
        claimsTypeWithFlowResponse.setClaimsAccidentType(DataAccidentTypeAccidentEnum.create((String) claimsWithFlow[1]));
        claimsTypeWithFlowResponse.setFlow((String) claimsWithFlow[2]);
        claimsTypeWithFlowResponse.setId(UUID.fromString((String) claimsWithFlow[3]));
        claimsTypeWithFlowResponse.setRebilling((String) claimsWithFlow[4]);

        return claimsTypeWithFlowResponse;
    }

    public static List<ClaimsTypeWithFlowResponseV1> adptObjectClaimsTypeWithFlowToClaimsTypeWithFlowResponse(List<Object[]> claimsWithFlowList) {

        List<ClaimsTypeWithFlowResponseV1> claimsTypeWithFlowResponseList = new LinkedList<>();
        for (Object[] att : claimsWithFlowList) {
            claimsTypeWithFlowResponseList.add(adptObjectClaimsTypeWithFlowToClaimsTypeWithFlowResponse(att));
        }
        return claimsTypeWithFlowResponseList;
    }

    public static ClaimsTypeResponseV1 adptClaimsTypeToClaimsTypeResponse(ClaimsTypeEntity claimsType) {

        ClaimsTypeResponseV1 claimsTypeResponse = new ClaimsTypeResponseV1();
        claimsTypeResponse.setId(claimsType.getId());
        claimsTypeResponse.setClaimsAccidentType(claimsType.getClaimsAccidentType());
        claimsTypeResponse.setType(claimsType.getType());

        return claimsTypeResponse;
    }

    public static List<ClaimsTypeResponseV1> adptClaimsTypeToClaimsTypeResponse(List<ClaimsTypeEntity> claimsTypeList) {

        List<ClaimsTypeResponseV1> claimsTypeResponseList = new LinkedList<>();
        for (ClaimsTypeEntity att : claimsTypeList) {
            claimsTypeResponseList.add(adptClaimsTypeToClaimsTypeResponse(att));
        }
        return claimsTypeResponseList;
    }

    public static ClaimsResponsePaginationV1 adptContractTypePaginationToClaimsResponsePagination(Pagination<ContractTypeEntity> pagination) {

        ClaimsResponsePaginationV1<ContractTypeEntityResponseV1> claimsPaginationResponse = new ClaimsResponsePaginationV1<>();

        claimsPaginationResponse.setStats(pagination.getStats());
        claimsPaginationResponse.setItems(getContractListAdapter(pagination.getItems()));

        return claimsPaginationResponse;
    }


    //region NUOVI ADAPTER
    public static ClaimsDamagedContractEntity adptFromContractEntityToClaimsDamagedContractNewEntity(Contract contractEntity) {

        if (contractEntity != null) {

            ClaimsDamagedContractEntity contract = new ClaimsDamagedContractEntity();
            //contract.setId(id);
            contract.setContractId(contractEntity.getContractId());
            contract.setStatus(contractEntity.getStatus());
            contract.setContractVersionId(contractEntity.getContractVersionId());
            contract.setMileage(contractEntity.getMileage());
            contract.setDuration(contractEntity.getDuration());
            contract.setContractType(contractEntity.getContractType());
            contract.setStartDate(contractEntity.getStartDate());
            contract.setEndDate(contractEntity.getEndDate());
            contract.setLeasingCompanyId(contractEntity.getLeasingCompanyId());
            contract.setLeasingCompany(contractEntity.getLeasingCompany());
            contract.setSucceedingContractId(contractEntity.getSucceedingContractId());
            contract.setFleetVehicleId(contractEntity.getFleetVehicleId());
            contract.setLicensePlate(contractEntity.getLicensePlate());
            contract.setTakeinDate(contractEntity.getTakeinDate());

            return contract;
        }

        return null;
    }

    public static Contract adptFromContractEntityToClaimsDamagedContractNewEntity(ClaimsDamagedContractEntity contractJsonb) {

        if (contractJsonb != null) {

            Contract contract = new Contract();
            contract.setContractId(contractJsonb.getContractId());
            contract.setStatus(contractJsonb.getStatus());
            contract.setContractVersionId(contractJsonb.getContractVersionId());
            contract.setMileage(contractJsonb.getMileage());
            contract.setDuration(contractJsonb.getDuration());
            contract.setContractType(contractJsonb.getContractType());
            contract.setStartDate(contractJsonb.getStartDate());
            contract.setEndDate(contractJsonb.getEndDate());
            contract.setLeasingCompanyId(contractJsonb.getLeasingCompanyId());
            contract.setLeasingCompany(contractJsonb.getLeasingCompany());
            contract.setSucceedingContractId(contractJsonb.getSucceedingContractId());
            contract.setFleetVehicleId(contractJsonb.getFleetVehicleId());
            contract.setLicensePlate(contractJsonb.getLicensePlate());
            contract.setTakeinDate(contractJsonb.getTakeinDate());

            return contract;
        }

        return null;
    }

    //endregion NUOVI ADAPTER
}
