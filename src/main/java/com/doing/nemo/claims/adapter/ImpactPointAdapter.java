package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.controller.payload.request.counterparty.ImpactPointRequest;
import com.doing.nemo.claims.controller.payload.response.damaged.ImpactPointResponse;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.ImpactPoint;
import org.springframework.stereotype.Component;

@Component
public class ImpactPointAdapter {

    public static ImpactPoint adptImpactPointRequestToImpactPoint(ImpactPointRequest impactPointRequest) {

        if (impactPointRequest != null) {

            ImpactPoint impactPoint = new ImpactPoint();
            impactPoint.setDamageToVehicle(impactPointRequest.getDamageToVehicle());
            impactPoint.setDetectionsImpactPoint(impactPointRequest.getDetectionsImpactPoint());
            impactPoint.setIncidentDescription(impactPointRequest.getIncidentDescription());

            return impactPoint;
        }

        return null;
    }

    public static ImpactPointResponse adptImpactPointToImpactPointResponse(ImpactPoint impactPoint) {

        ImpactPointResponse impactPointResponse = new ImpactPointResponse();

        if (impactPoint != null) {
            impactPointResponse.setDamageToVehicle(impactPoint.getDamageToVehicle());
            impactPointResponse.setDetectionsImpactPoint(impactPoint.getDetectionsImpactPoint());
            impactPointResponse.setIncidentDescription(impactPoint.getIncidentDescription());
        }else{
            impactPointResponse.setDamageToVehicle("Descrizione non presente");
            impactPointResponse.setDetectionsImpactPoint(null);
            impactPointResponse.setIncidentDescription("Descrizione non presente");

        }
        return impactPointResponse;
    }

}
