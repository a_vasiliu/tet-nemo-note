package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.RegistryRequestV1;
import com.doing.nemo.claims.controller.payload.response.RegistryResponseV1;
import com.doing.nemo.claims.entity.claims.ClaimsDamagedRegistryEntity;
import com.doing.nemo.claims.entity.esb.RegistryESB;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbAntiTheftService.Registry;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;

@Component
public class RegistryAdapter {

    public static Registry adptRegistryEsbToRegistry(RegistryESB registryESB){

        if( registryESB != null){

            Registry registryResponse = new Registry();
            registryResponse.setIdTransaction(registryESB.getIdTransaction());
            registryResponse.setCodImei(registryESB.getCodImei());
            registryResponse.setCodPack(registryESB.getCodPack());
            registryResponse.setCodProvider(registryESB.getCodProvider());
            registryResponse.setProvider(registryESB.getProvider());
            registryResponse.setDateActivation(registryESB.getDateActivation());
            registryResponse.setDateEnd(registryESB.getDateEnd());
            registryResponse.setCodSerialnumber(registryESB.getCodSerialnumber());
            registryResponse.setVoucherId(registryESB.getVoucherId());
            //registryResponse.setTypeEntity(registryESB.setT);

            return registryResponse;
        }
        return null;
    }

    public static List<Registry> adptFromRegistryEsbToRegistryList(List<RegistryESB> registryESBList)
    {
        List<Registry> registry = new LinkedList<>();
        for(RegistryESB registryESB : registryESBList)
        {
            registry.add(adptRegistryEsbToRegistry(registryESB));
        }
        return registry;
    }

    public static RegistryResponseV1 adptRegistryEsbToRegistryResponse(RegistryESB registryESB){

        if( registryESB != null){

            RegistryResponseV1 registryResponse = new RegistryResponseV1();
            registryResponse.setIdTransaction(registryESB.getIdTransaction());
            registryResponse.setCodImei(registryESB.getCodImei());
            registryResponse.setCodPack(registryESB.getCodPack());
            registryResponse.setCodProvider(registryESB.getCodProvider());
            registryResponse.setProvider(registryESB.getProvider());
            registryResponse.setDateActivation(DateUtil.convertUTCDateToIS08601String(registryESB.getDateActivation()));
            registryResponse.setDateEnd(DateUtil.convertUTCDateToIS08601String(registryESB.getDateEnd()));
            registryResponse.setCodSerialnumber(registryESB.getCodSerialnumber());
            registryResponse.setVoucherId(registryESB.getVoucherId());
            //registryResponse.setTypeEntity(registryESB.setT);

            return registryResponse;
        }
        return null;
    }

    public static List<RegistryResponseV1> adptFromRegistryEsbToRegistryResponseV1List(ArrayBlockingQueue<RegistryESB> registryESBList)
    {
        List<RegistryResponseV1> registryResponseV1 = new LinkedList<>();
        for(RegistryESB registryESB : registryESBList)
        {
            registryResponseV1.add(adptRegistryEsbToRegistryResponse(registryESB));
        }
        return registryResponseV1;
    }





    public static List<RegistryResponseV1> adptRegistryToRegistryResponseV1(List<Registry> registryList){

        if( registryList != null){

            List<RegistryResponseV1> registryListResponse = new LinkedList<>();
            for (Registry att : registryList){
                registryListResponse.add(adptRegistryToRegistryResponseV1(att));
            }

            return registryListResponse;
        }
        return null;
    }

    public static RegistryResponseV1 adptRegistryToRegistryResponseV1(Registry registry){

        RegistryResponseV1 registryResponseV1 = new RegistryResponseV1();
        registryResponseV1.setIdTransaction(registry.getIdTransaction());
        registryResponseV1.setCodImei(registry.getCodImei());
        registryResponseV1.setCodPack(registry.getCodPack());
        registryResponseV1.setCodProvider(registry.getCodProvider());
        registryResponseV1.setProvider(registry.getProvider());
        registryResponseV1.setDateActivation(DateUtil.convertUTCDateToIS08601String(registry.getDateActivation()));
        registryResponseV1.setDateEnd(DateUtil.convertUTCDateToIS08601String(registry.getDateEnd()));
        registryResponseV1.setCodSerialnumber(registry.getCodSerialnumber());
        registryResponseV1.setVoucherId(registry.getVoucherId());
        registryResponseV1.setTypeEntity(registry.getTypeEntity());
        return registryResponseV1;
    }

    public static List<Registry> adptRegistryResponseToRegistry(List<RegistryResponseV1> registryList){


            List<Registry> registryListResponse = new LinkedList<>();
            for (RegistryResponseV1 att : registryList){
                registryListResponse.add(adptRegistryResponseToRegistry(att));
            }

            return registryListResponse;
        }

    public static Registry adptRegistryResponseToRegistry(RegistryResponseV1 registryResponseV1){

        Registry registry = new Registry();
        registry.setIdTransaction(registryResponseV1.getIdTransaction());
        registry.setCodImei(registryResponseV1.getCodImei());
        registry.setCodPack(registryResponseV1.getCodPack());
        registry.setCodProvider(registryResponseV1.getCodProvider());
        registry.setProvider(registryResponseV1.getProvider());
        registry.setDateActivation(DateUtil.convertIS08601StringToUTCDate(registryResponseV1.getDateActivation()));
        registry.setDateEnd(DateUtil.convertIS08601StringToUTCDate(registryResponseV1.getDateEnd()));
        registry.setCodSerialnumber(registryResponseV1.getCodSerialnumber());
        registry.setVoucherId(registryResponseV1.getVoucherId());
        registry.setTypeEntity(registryResponseV1.getTypeEntity());
        return registry;
    }




    public static List<Registry> adptFromRegistryRequestToRegistryList(List<RegistryRequestV1> insurancePolicyPersonalDataRequestV1List)
    {
        List<Registry> insurancePolicyPersonalDataList = new LinkedList<>();
        if(insurancePolicyPersonalDataRequestV1List != null) {
            for (RegistryRequestV1 insurancePolicyPersonalDatasRequest : insurancePolicyPersonalDataRequestV1List) {
                insurancePolicyPersonalDataList.add(adptFromRegistryRequestToRegistry(insurancePolicyPersonalDatasRequest));
            }
        }
        return insurancePolicyPersonalDataList;
    }

    public static Registry adptFromRegistryRequestToRegistry(RegistryRequestV1 registryRequestV1){
        Registry registry = new Registry();
        registry.setIdTransaction(registryRequestV1.getIdTransaction());
        registry.setCodImei(registryRequestV1.getCodImei());
        registry.setCodPack(registryRequestV1.getCodPack());
        registry.setCodProvider(registryRequestV1.getCodProvider());
        registry.setProvider(registryRequestV1.getProvider());
        registry.setDateActivation(DateUtil.convertIS08601StringToUTCDate(registryRequestV1.getDateActivation()));
        registry.setDateEnd(DateUtil.convertIS08601StringToUTCDate(registryRequestV1.getDateEnd()));
        registry.setCodSerialnumber(registryRequestV1.getCodSerialnumber());
        registry.setVoucherId(registryRequestV1.getVoucherId());
        registry.setTypeEntity(registryRequestV1.getTypeEntity());

        return registry;
    }


    public static List<Registry> adptFromRegistryResponseToRegistryList(List<RegistryResponseV1> insurancePolicyPersonalDataResponsetV1List)
    {
        List<Registry> insurancePolicyPersonalDataList = new LinkedList<>();
        if(insurancePolicyPersonalDataResponsetV1List != null) {
            for (RegistryResponseV1 insurancePolicyPersonalDatasResponse : insurancePolicyPersonalDataResponsetV1List) {
                insurancePolicyPersonalDataList.add(adptFromRegistryResponseToRegistry(insurancePolicyPersonalDatasResponse));
            }
        }
        return insurancePolicyPersonalDataList;
    }

    public static Registry adptFromRegistryResponseToRegistry(RegistryResponseV1 registryResponseV1){
        Registry registry = new Registry();
        registry.setIdTransaction(registryResponseV1.getIdTransaction());
        registry.setCodImei(registryResponseV1.getCodImei());
        registry.setCodPack(registryResponseV1.getCodPack());
        registry.setCodProvider(registryResponseV1.getCodProvider());
        registry.setProvider(registryResponseV1.getProvider());
        registry.setDateActivation(DateUtil.convertIS08601StringToUTCDate(registryResponseV1.getDateActivation()));
        registry.setDateEnd(DateUtil.convertIS08601StringToUTCDate(registryResponseV1.getDateEnd()));
        registry.setCodSerialnumber(registryResponseV1.getCodSerialnumber());
        registry.setVoucherId(registryResponseV1.getVoucherId());
        registry.setTypeEntity(registryResponseV1.getTypeEntity());

        return registry;
    }

    //NEW Adapter



    public static List<ClaimsDamagedRegistryEntity> adptFromRegistryListJsonbToClaimsDamagedRegistryList(List<Registry> registryList)
    {
        List<ClaimsDamagedRegistryEntity> claimsDamagedRegistryEntities = new LinkedList<>();
        if(registryList != null) {
            for (Registry currentRegistry : registryList) {
                claimsDamagedRegistryEntities.add(adptFromRegistryJsonbToClaimsDamagedRegistry(currentRegistry));
            }
        }
        return claimsDamagedRegistryEntities;
    }

    public static ClaimsDamagedRegistryEntity adptFromRegistryJsonbToClaimsDamagedRegistry(Registry registryJsonb){
        ClaimsDamagedRegistryEntity registry = new ClaimsDamagedRegistryEntity();
        registry.setId(UUID.randomUUID().toString());
        registry.setIdTransaction(registryJsonb.getIdTransaction());
        registry.setCodImei(registryJsonb.getCodImei());
        registry.setCodPack(registryJsonb.getCodPack());
        registry.setCodProvider(registryJsonb.getCodProvider());
        registry.setProvider(registryJsonb.getProvider());
        registry.setDateActivation(DateUtil.convertIS08601StringToUTCInstant(DateUtil.convertUTCDateToIS08601String(registryJsonb.getDateActivation())));
        registry.setDateEnd(DateUtil.convertIS08601StringToUTCInstant(DateUtil.convertUTCDateToIS08601String(registryJsonb.getDateEnd())));
        registry.setCodSerialnumber(registryJsonb.getCodSerialnumber());
        registry.setVoucherId(registryJsonb.getVoucherId());
        registry.setType(registryJsonb.getTypeEntity());

        return registry;
    }

    public static List<Registry> adptFromClaimsRegistryListToRegistryListJsonb(List<ClaimsDamagedRegistryEntity> damagedRegistryEntities)
    {
        List<Registry> claimsDamagedRegistryEntities = new LinkedList<>();
        if(damagedRegistryEntities != null) {
            for (ClaimsDamagedRegistryEntity currentRegistry : damagedRegistryEntities) {
                claimsDamagedRegistryEntities.add(adptFromClaimsRegistryToRegistryJsonb(currentRegistry));
            }
        }
        return claimsDamagedRegistryEntities;
    }

    public static Registry adptFromClaimsRegistryToRegistryJsonb(ClaimsDamagedRegistryEntity claimsDamagedRegistryEntity){
        Registry registry = new Registry();

        registry.setIdTransaction(claimsDamagedRegistryEntity.getIdTransaction());
        registry.setCodImei(claimsDamagedRegistryEntity.getCodImei());
        registry.setCodPack(claimsDamagedRegistryEntity.getCodPack());
        registry.setCodProvider(claimsDamagedRegistryEntity.getCodProvider());
        registry.setProvider(claimsDamagedRegistryEntity.getProvider());
        registry.setDateActivation(DateUtil.convertIS08601StringToUTCDate(DateUtil.convertUTCInstantToIS08601String(claimsDamagedRegistryEntity.getDateActivation())));
        registry.setDateEnd(DateUtil.convertIS08601StringToUTCDate(DateUtil.convertUTCInstantToIS08601String(claimsDamagedRegistryEntity.getDateEnd())));
        registry.setCodSerialnumber(claimsDamagedRegistryEntity.getCodSerialnumber());
        registry.setVoucherId(claimsDamagedRegistryEntity.getVoucherId());
        registry.setTypeEntity(claimsDamagedRegistryEntity.getType());

        return registry;
    }

    
}
