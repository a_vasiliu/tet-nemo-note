package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.controller.payload.request.ClaimsTypeForRuleRequestV1;
import com.doing.nemo.claims.entity.settings.ClaimsTypeEntity;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

@Component

public class ClaimsTypeAdapter {

    public static ClaimsTypeEntity adptClaimsTypeRequestToClaimsType(ClaimsTypeForRuleRequestV1 claimsTypeRequest) {

        if (claimsTypeRequest != null) {

            ClaimsTypeEntity claimsType = new ClaimsTypeEntity();
            claimsType.setId(claimsTypeRequest.getId());
            claimsType.setType(claimsTypeRequest.getType());
            claimsType.setClaimsAccidentType(claimsTypeRequest.getClaimsAccidentType());

            return claimsType;
        }
        return null;
    }

    public static List<ClaimsTypeEntity> adptClaimsTyreRequestToClaimsType(List<ClaimsTypeForRuleRequestV1> claimsTypeRequestList) {
        List<ClaimsTypeEntity> requestV1 = new LinkedList<>();
        for (ClaimsTypeForRuleRequestV1 claimsTypeRequestV1 : claimsTypeRequestList) {
            requestV1.add(adptClaimsTypeRequestToClaimsType(claimsTypeRequestV1));
        }
        return requestV1;
    }
}
