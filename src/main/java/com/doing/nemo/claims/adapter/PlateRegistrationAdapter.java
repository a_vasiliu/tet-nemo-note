package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.controller.payload.response.registration.PlateRegistrationHistoryItem;
import com.doing.nemo.middleware.client.payload.request.items.PlateHistoryItem;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class PlateRegistrationAdapter {
    public PlateRegistrationHistoryItem adaptToResponse(PlateHistoryItem plateHistoryItem) {

        if (plateHistoryItem == null) {
            return null;
        }

        PlateRegistrationHistoryItem response = new PlateRegistrationHistoryItem();
        response.setPlate(plateHistoryItem.getPlate());
        response.setRegistrationDate(plateHistoryItem.getRegistrationDate());
        response.setValidFrom(plateHistoryItem.getValidFrom());
        response.setValidTo(plateHistoryItem.getValidTo());

        return response;

    }

    public List<PlateRegistrationHistoryItem> adaptToResponse(List<PlateHistoryItem> plateHistoryItems) {

        if (CollectionUtils.isEmpty(plateHistoryItems)) {
            return Collections.emptyList();
        }

        return plateHistoryItems.stream()
                .map(this::adaptToResponse)
                .collect(Collectors.toList());
    }

}
