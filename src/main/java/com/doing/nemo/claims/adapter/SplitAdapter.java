package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.refund.SplitRequest;
import com.doing.nemo.claims.controller.payload.response.refund.SplitResponse;
import com.doing.nemo.claims.entity.jsonb.refund.Split;
import com.doing.nemo.claims.util.Util;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Component
public class SplitAdapter {

    private static Logger LOGGER = LoggerFactory.getLogger(SplitAdapter.class);

    public static Split adptFromSplitRequestToSplit(SplitRequest splitRequest) {

        if (splitRequest != null) {

            Split split = new Split();
            try {
                Date date = Util.fromStringToDate(splitRequest.getIssueDate());
                String dateissue =  DateUtil.convertUTCDateToIS08601String(date);
                split.setIssueDate(Util.fromStringToDate(dateissue));
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
            }

            if(splitRequest.getId() == null)
                split.setId(UUID.randomUUID());
            else
                split.setId(splitRequest.getId());

            split.setType(splitRequest.getType());
            split.setRefundType(splitRequest.getRefundType());
            split.setNote(splitRequest.getNote());
            split.setReceivedSum(splitRequest.getReceivedSum());
            split.setLegalFees(splitRequest.getLegalFees());
            split.setMaterialAmount(splitRequest.getMaterialAmount());
            split.setTechnicalStop(splitRequest.getTechnicalStop());
            split.setTotalPaid(splitRequest.getTotalPaid());

            return split;
        }
        return null;
    }

    public static SplitResponse adptFromSplitToSplitResponse(Split split) {

        if (split != null) {

            SplitResponse splitResponse = new SplitResponse();

            splitResponse.setId(split.getId());
            splitResponse.setIssueDate(DateUtil.convertUTCDateToIS08601String(split.getIssueDate()));
            splitResponse.setNote(split.getNote());
            splitResponse.setReceivedSum(split.getReceivedSum());
            splitResponse.setTotalPaid(split.getTotalPaid());
            splitResponse.setType(split.getType());
            splitResponse.setRefundType(split.getRefundType());
            splitResponse.setLegalFees(split.getLegalFees());
            splitResponse.setMaterialAmount(split.getMaterialAmount());
            splitResponse.setTechnicalStop(split.getTechnicalStop());

            return splitResponse;
        }

        return null;
    }

    public static List<Split> adptFromSplitRequestListToSplitList(List<SplitRequest> splitRequestList) {

        if (splitRequestList != null) {

            List<Split> splitList = new ArrayList<>();

            for (SplitRequest splitRequest : splitRequestList) {
                splitList.add(adptFromSplitRequestToSplit(splitRequest));
            }
            return splitList;
        }
        return null;
    }

    public static List<SplitResponse> adptFromSplitListToSplitResponseList(List<Split> splitList) {

        if (splitList != null) {

            List<SplitResponse> splitResponseList = new ArrayList<>();

            for (Split split : splitList) {
                splitResponseList.add(adptFromSplitToSplitResponse(split));
            }
            return splitResponseList;
        }
        return null;
    }
}