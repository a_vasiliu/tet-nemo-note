package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.controller.payload.request.insurancecompany.MaterialDamageRequest;
import com.doing.nemo.claims.controller.payload.response.insurancecompany.MaterialDamageResponse;
import com.doing.nemo.claims.entity.esb.InsuranceCompanyESB.MaterialDamageEsb;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbInsuranceCompany.MaterialDamage;
import org.springframework.stereotype.Component;

@Component
public class MaterialDamageAdapter {

    public static MaterialDamage adptMaterialDamageRequestToMaterialDamage(MaterialDamageRequest materialDamageRequest) {

        if (materialDamageRequest != null) {

            MaterialDamage materialDamage = new MaterialDamage();

            materialDamage.setServiceId(materialDamageRequest.getServiceId());
            materialDamage.setService(materialDamageRequest.getService());
            materialDamage.setDeductibleId(materialDamageRequest.getDeductibleId());
            materialDamage.setDeductible(materialDamageRequest.getDeductible());

            return materialDamage;
        }
        return null;
    }

    public static MaterialDamageResponse adptMaterialDamageToMaterialDamageResponse(MaterialDamage materialDamage) {

        if (materialDamage != null) {

            MaterialDamageResponse materialDamageResponse = new MaterialDamageResponse();

            materialDamageResponse.setServiceId(materialDamage.getServiceId());
            materialDamageResponse.setService(materialDamage.getService());
            materialDamageResponse.setDeductibleId(materialDamage.getDeductibleId());
            materialDamageResponse.setDeductible(materialDamage.getDeductible());

            return materialDamageResponse;
        }
        return null;
    }

    public static MaterialDamage adptMaterialDamageResponseToMaterialDamage(MaterialDamageResponse materialDamageResponse) {

        if (materialDamageResponse != null) {

            MaterialDamage materialDamage = new MaterialDamage();

            materialDamage.setServiceId(materialDamageResponse.getServiceId());
            materialDamage.setService(materialDamageResponse.getService());
            materialDamage.setDeductibleId(materialDamageResponse.getDeductibleId());
            materialDamage.setDeductible(materialDamageResponse.getDeductible());

            return materialDamage;
        }
        return null;
    }

    public static MaterialDamageResponse adptMaterialDamageEsbToMaterialDamageResponse(MaterialDamageEsb materialDamageEsb) {

        if (materialDamageEsb != null) {

            MaterialDamageResponse materialDamageResponse = new MaterialDamageResponse();

            materialDamageResponse.setServiceId(materialDamageEsb.getServiceId());
            materialDamageResponse.setService(materialDamageEsb.getService());
            materialDamageResponse.setDeductibleId(materialDamageEsb.getDeductibleId());
            materialDamageResponse.setDeductible(materialDamageEsb.getDeductible());

            return materialDamageResponse;
        }
        return null;
    }
}
