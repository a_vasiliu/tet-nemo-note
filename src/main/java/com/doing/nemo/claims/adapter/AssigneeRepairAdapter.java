package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.controller.payload.request.AssigneeRepairRequestV1;
import com.doing.nemo.claims.controller.payload.response.AssigneeRepairResponseV1;
import com.doing.nemo.claims.dto.AssigneeRepair;
import org.springframework.stereotype.Component;

@Component
public class AssigneeRepairAdapter {

    public static AssigneeRepair adptFromAssignationRepairRequestToAssignationRepair(AssigneeRepairRequestV1 assigneeRepairRequestV1){
        if(assigneeRepairRequestV1 == null) return null;
        AssigneeRepair assigneeRepair = new AssigneeRepair();
        assigneeRepair.setId(assigneeRepairRequestV1.getId());
        assigneeRepair.setUsername(assigneeRepairRequestV1.getUsername());
        assigneeRepair.setPersonalDetails(PersonalDetailsAdapter.adptFromPersonalDetailsRepairRequestToPersonalDetailsRepair(assigneeRepairRequestV1.getPersonalDetails()));

        return assigneeRepair;
    }

    public static AssigneeRepairResponseV1 adptFromAssignationRepairToAssignationRepairResponse(AssigneeRepair assigneeRepair){
        if(assigneeRepair == null) return null;
        AssigneeRepairResponseV1 assigneeRepairResponseV1 = new AssigneeRepairResponseV1();
        assigneeRepairResponseV1.setId(assigneeRepair.getId());
        assigneeRepairResponseV1.setUsername(assigneeRepair.getUsername());
        assigneeRepairResponseV1.setPersonalDetails(PersonalDetailsAdapter.adptFromPersonalDetailsRepairToPersonalDetailsResponse(assigneeRepair.getPersonalDetails()));
        return assigneeRepairResponseV1;
    }
}
