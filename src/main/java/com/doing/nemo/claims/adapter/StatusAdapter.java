package com.doing.nemo.claims.adapter;


import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class StatusAdapter {


    public static ClaimsStatusEnum adptFromClaimsStatusToMyAldStatus(ClaimsStatusEnum claimStatus){
        switch(claimStatus){
            case WAITING_FOR_AUTHORITY:
            case WAITING_FOR_NUMBER_SX:
            case WAITING_FOR_REFUND:
            case TO_ENTRUST:
            case CLOSED_PARTIAL_REFUND:
            case CLOSED_TOTAL_REFUND:
            case TO_SEND_TO_CUSTOMER:
            case MANAGED:
                    return ClaimsStatusEnum.ACCEPTED;
            case WAITING_FOR_VALIDATION:
                return ClaimsStatusEnum.UNDER_PROCESSING;
            default:
                return claimStatus;

        }
    }

    public static List<String> adptFromMyAldStatusListToClaimsStatusListString(List<String> claimsStatusEnums){
        if(claimsStatusEnums == null || claimsStatusEnums.isEmpty()) return null;
        List<String> claimsStatus = new ArrayList<>();
        for(String statusEnum : claimsStatusEnums){
            switch(statusEnum.toUpperCase()){
                case "ACCEPTED":
                    claimsStatus.add(ClaimsStatusEnum.WAITING_FOR_AUTHORITY.getValue().toUpperCase());
                    claimsStatus.add(ClaimsStatusEnum.WAITING_FOR_NUMBER_SX.getValue().toUpperCase());
                    claimsStatus.add(ClaimsStatusEnum.WAITING_FOR_REFUND.getValue().toUpperCase());
                    claimsStatus.add(ClaimsStatusEnum.TO_ENTRUST.getValue().toUpperCase());
                    claimsStatus.add(ClaimsStatusEnum.CLOSED_PARTIAL_REFUND.getValue().toUpperCase());
                    claimsStatus.add(ClaimsStatusEnum.CLOSED_TOTAL_REFUND.getValue().toUpperCase());
                    claimsStatus.add(ClaimsStatusEnum.TO_SEND_TO_CUSTOMER.getValue().toUpperCase());
                    claimsStatus.add(ClaimsStatusEnum.MANAGED.getValue().toUpperCase());
                    break;
                case "UNDER_PROCESSING":
                    claimsStatus.add(ClaimsStatusEnum.WAITING_FOR_VALIDATION.getValue().toUpperCase());
                    break;
                default:
                    claimsStatus.add(ClaimsStatusEnum.create(statusEnum).getValue().toUpperCase());
                    break;
            }
        }
        return claimsStatus;
    }


}
