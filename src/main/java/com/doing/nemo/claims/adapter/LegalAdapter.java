package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.controller.payload.request.LegalRequestV1;
import com.doing.nemo.claims.controller.payload.response.ClaimsResponsePaginationV1;
import com.doing.nemo.claims.controller.payload.response.LegalResponseV1;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.settings.LegalEntity;
import com.doing.nemo.claims.exception.NotFoundException;
import com.doing.nemo.claims.repository.LegalRepository;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class LegalAdapter {

    @Autowired
    private AutomaticAffiliationRuleLegalAdapter automaticAffiliationRuleLegalAdapter;

    @Autowired
    private LegalRepository legalRepository;

    private static Logger LOGGER = LoggerFactory.getLogger(LegalAdapter.class);

    public LegalEntity adptFromLegalRequToLegalEntityWithID(LegalRequestV1 legalRequestV1, UUID uuid, HttpMethod httpMethod) {
        Optional<LegalEntity> legalEntity1 = legalRepository.findById(uuid);
        if (!legalEntity1.isPresent()) {
            LOGGER.debug("Legal not found");
            throw new NotFoundException("Legal not found",com.doing.nemo.claims.validation.MessageCode.CLAIMS_1010);
        }
        LegalEntity legalEntity = legalEntity1.get();
        LegalEntity legalEntity2 = adptFromLegalRequToLegalEntity(legalRequestV1, httpMethod);
        legalEntity2.setId(legalEntity.getId());
        return legalEntity2;
    }


    public LegalEntity adptFromLegalRequToLegalEntity(LegalRequestV1 legalRequestV1, HttpMethod httpMethod) {
        LegalEntity legalEntity = new LegalEntity();
        legalEntity.setName(legalRequestV1.getName());
        legalEntity.setAddress(legalRequestV1.getAddress());
        legalEntity.setEmail(legalRequestV1.getEmail());
        legalEntity.setZipCode(legalRequestV1.getZipCode());
        legalEntity.setState(legalRequestV1.getState());
        legalEntity.setCity(legalRequestV1.getCity());
        legalEntity.setProvince(legalRequestV1.getProvince());
        legalEntity.setVatNumber(legalRequestV1.getVatNumber());
        legalEntity.setFiscalCode(legalRequestV1.getFiscalCode());
        legalEntity.setTelephone(legalRequestV1.getTelephone());
        legalEntity.setFax(legalRequestV1.getFax());
        legalEntity.setWebSite(legalRequestV1.getWebSite());
        legalEntity.setReferencePerson(legalRequestV1.getReferencePerson());
        legalEntity.setExternalCode(legalRequestV1.getExternalCode());
        if (legalRequestV1.getAutomaticAffiliationRuleEntityList() != null && !legalRequestV1.getAutomaticAffiliationRuleEntityList().isEmpty())
            legalEntity.setAutomaticAffiliationRuleEntityList(automaticAffiliationRuleLegalAdapter.adptFromAARuleLegalRequToAARuleLegalEntityList(legalRequestV1.getAutomaticAffiliationRuleEntityList(), httpMethod));
        else legalEntity.setAutomaticAffiliationRuleEntityList(new ArrayList<>());
        legalEntity.setActive(legalRequestV1.getActive());

        return legalEntity;
    }

    public static LegalResponseV1 adptFromLegalEntityToLegalRespo(LegalEntity legalEntity) {
        if (legalEntity == null) return null;
        LegalResponseV1 responseV1 = new LegalResponseV1();
        responseV1.setId(legalEntity.getId());
        responseV1.setName(legalEntity.getName());
        responseV1.setAddress(legalEntity.getAddress());
        responseV1.setEmail(legalEntity.getEmail());
        responseV1.setZipCode(legalEntity.getZipCode());
        responseV1.setCity(legalEntity.getCity());
        responseV1.setProvince(legalEntity.getProvince());
        responseV1.setState(legalEntity.getState());
        responseV1.setVatNumber(legalEntity.getVatNumber());
        responseV1.setFiscalCode(legalEntity.getFiscalCode());
        responseV1.setTelephone(legalEntity.getTelephone());
        responseV1.setFax(legalEntity.getFax());
        responseV1.setWebSite(legalEntity.getWebSite());
        responseV1.setReferencePerson(legalEntity.getReferencePerson());
        responseV1.setExternalCode(legalEntity.getExternalCode());
        responseV1.setCode(legalEntity.getCode());
        responseV1.setAutomaticAffiliationRuleEntityList(legalEntity.getAutomaticAffiliationRuleEntityList());
        responseV1.setActive(legalEntity.getActive());
        return responseV1;
    }

    public static List<LegalResponseV1> adptFromLegalEntityToLegalRespoList(List<LegalEntity> legalEntityList) {


        List<LegalResponseV1> responseV1 = new LinkedList<>();
        for (LegalEntity legalEntity : legalEntityList) {
            responseV1.add(adptFromLegalEntityToLegalRespo(legalEntity));
        }
        return responseV1;
    }

    public static ClaimsResponsePaginationV1 adptLegalPaginationToClaimsResponsePagination(Pagination<LegalEntity> pagination) {

        ClaimsResponsePaginationV1<LegalResponseV1> claimsPaginationResponse = new ClaimsResponsePaginationV1<>();

        claimsPaginationResponse.setStats(pagination.getStats());
        claimsPaginationResponse.setItems(adptFromLegalEntityToLegalRespoList(pagination.getItems()));

        return claimsPaginationResponse;
    }
}
