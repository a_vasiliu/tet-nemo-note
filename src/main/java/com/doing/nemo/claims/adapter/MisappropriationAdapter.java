package com.doing.nemo.claims.adapter;


import com.doing.nemo.claims.controller.payload.request.practice.MisappropriationRequestV1;
import com.doing.nemo.claims.controller.payload.response.practice.MisappropriationResponseV1;
import com.doing.nemo.claims.entity.jsonb.practice.Misappropriation;
import org.springframework.stereotype.Component;

@Component
public class MisappropriationAdapter {

    public static Misappropriation adptFromMisappropriationRequestToMisappropriation(MisappropriationRequestV1 misappropriationRequest) {
        if(misappropriationRequest == null)
            return null;
        Misappropriation misappropriation = new Misappropriation();
        misappropriation.setMisappropriationDate(misappropriationRequest.getMisappropriationDate());
        misappropriation.setMisappropriationId(misappropriationRequest.getMisappropriationId());
        misappropriation.setProvider(misappropriationRequest.getProvider());
        misappropriation.setProviderCode(misappropriationRequest.getProviderCode());
        misappropriation.setComplaintPolice(misappropriationRequest.getComplaintPolice());
        misappropriation.setComplaintCc(misappropriationRequest.getComplaintCc());
        misappropriation.setComplaintVvuu(misappropriationRequest.getComplaintVvuu());
        misappropriation.setComplaintGdf(misappropriationRequest.getComplaintGdf());
        misappropriation.setAuthorityData(misappropriationRequest.getAuthorityData());
        misappropriation.setNote(misappropriationRequest.getNote());
        misappropriation.setHappenedAbroad(misappropriationRequest.getHappenedAbroad());
        misappropriation.setSubRental(misappropriationRequest.getSubRental());
        misappropriation.setPlate(misappropriationRequest.getPlate());
        return misappropriation;
    }


    public static MisappropriationResponseV1 adptFromMisappropriationToMisappropriationResponse(Misappropriation misappropriation) {

        if(misappropriation == null)
            return null;
        MisappropriationResponseV1 misappropriationResponse = new MisappropriationResponseV1();
        misappropriationResponse.setMisappropriationDate(misappropriation.getMisappropriationDate());
        misappropriationResponse.setMisappropriationId(misappropriation.getMisappropriationId());
        misappropriationResponse.setProvider(misappropriation.getProvider());
        misappropriationResponse.setProviderCode(misappropriation.getProviderCode());
        misappropriationResponse.setComplaintPolice(misappropriation.getComplaintPolice());
        misappropriationResponse.setComplaintCc(misappropriation.getComplaintCc());
        misappropriationResponse.setComplaintVvuu(misappropriation.getComplaintVvuu());
        misappropriationResponse.setComplaintGdf(misappropriation.getComplaintGdf());
        misappropriationResponse.setAuthorityData(misappropriation.getAuthorityData());
        misappropriationResponse.setNote(misappropriation.getNote());
        misappropriationResponse.setHappenedAbroad(misappropriation.getHappenedAbroad());
        misappropriationResponse.setSubRental(misappropriation.getSubRental());
        misappropriationResponse.setPlate(misappropriation.getPlate());
        return misappropriationResponse;
    }





}