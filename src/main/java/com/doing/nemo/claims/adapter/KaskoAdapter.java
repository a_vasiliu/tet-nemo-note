package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.controller.payload.request.insurancecompany.KaskoRequest;
import com.doing.nemo.claims.controller.payload.response.insurancecompany.KaskoResponse;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbInsuranceCompany.Kasko;
import org.springframework.stereotype.Component;

@Component
public class KaskoAdapter {
    public static Kasko adptFromKaskoRequestToKasko (KaskoRequest kaskoRequest){
        if(kaskoRequest != null){
            Kasko kasko = new Kasko();
            kasko.setDeductibleId(kaskoRequest.getDeductibleId());
            kasko.setDeductibleValue(kaskoRequest.getDeductibleValue());
            return kasko;
        }
        return null;
    }

    public static KaskoResponse adptFromKaskoToKaskoResponse (Kasko kasko){
        if(kasko != null){
            KaskoResponse kaskoResponse = new KaskoResponse();
            kaskoResponse.setDeductibleId(kasko.getDeductibleId());
            kaskoResponse.setDeductibleValue(kasko.getDeductibleValue());
            return kaskoResponse;
        }
        return null;
    }
}
