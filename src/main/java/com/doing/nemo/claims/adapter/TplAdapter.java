package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.controller.payload.request.insurancecompany.TplRequest;
import com.doing.nemo.claims.controller.payload.response.insurancecompany.TplResponse;
import com.doing.nemo.claims.entity.esb.InsuranceCompanyESB.TplEsb;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbInsuranceCompany.Tpl;
import com.doing.nemo.middleware.client.payload.response.MiddlewareTplResponse;
import org.springframework.stereotype.Component;

@Component
public class TplAdapter {

    public static Tpl adptTplRequestToTpl(TplRequest tplRequest) {

        if (tplRequest != null) {

            Tpl tpl = new Tpl();

            tpl.setServiceId(tplRequest.getServiceId());
            tpl.setService(tplRequest.getService());
            tpl.setMaxCoverage(tplRequest.getMaxCoverage());
            tpl.setDeductible(tplRequest.getDeductible());
            tpl.setCompanyId(tplRequest.getCompanyId());
            tpl.setCompany(tplRequest.getCompany());
            tpl.setTariffId(tplRequest.getTariffId());
            tpl.setTariff(tplRequest.getTariff());
            tpl.setTariffCode(tplRequest.getTariffCode());
            tpl.setStartDate(tplRequest.getStartDate());
            tpl.setEndDate(tplRequest.getEndDate());
            tpl.setPolicyNumber(tplRequest.getPolicyNumber());
            tpl.setCompanyDescription(tplRequest.getCompanyDescription());
            tpl.setInsuranceCompanyId(tplRequest.getInsuranceCompanyId());
            tpl.setInsurancePolicyId(tplRequest.getInsurancePolicyId());

            return tpl;
        }
        return null;
    }

    public static TplResponse adptTplToTplResponse(Tpl tpl) {

        if (tpl != null) {

            TplResponse tplResponse = new TplResponse();

            tplResponse.setServiceId(tpl.getServiceId());
            tplResponse.setService(tpl.getService());
            tplResponse.setMaxCoverage(tpl.getMaxCoverage());
            tplResponse.setDeductible(tpl.getDeductible());
            tplResponse.setCompanyId(tpl.getCompanyId());
            tplResponse.setCompany(tpl.getCompany());
            tplResponse.setTariffId(tpl.getTariffId());
            tplResponse.setTariff(tpl.getTariff());
            tplResponse.setTariffCode(tpl.getTariffCode());
            tplResponse.setStartDate(tpl.getStartDate());
            tplResponse.setEndDate(tpl.getEndDate());
            tplResponse.setPolicyNumber(tpl.getPolicyNumber());
            tplResponse.setCompanyDescription(tpl.getCompanyDescription());
            tplResponse.setInsuranceCompanyId(tpl.getInsuranceCompanyId());
            tplResponse.setInsurancePolicyId(tpl.getInsurancePolicyId());

            return tplResponse;
        }
        return null;
    }

    public static Tpl adptTplResponseToTpl(TplResponse tplResponse) {

        if (tplResponse != null) {

            Tpl tpl = new Tpl();

            tpl.setServiceId(tplResponse.getServiceId());
            tpl.setService(tplResponse.getService());
            tpl.setMaxCoverage(tplResponse.getMaxCoverage());
            tpl.setDeductible(tplResponse.getDeductible());
            tpl.setCompanyId(tplResponse.getCompanyId());
            tpl.setCompany(tplResponse.getCompany());
            tpl.setTariffId(tplResponse.getTariffId());
            tpl.setTariff(tplResponse.getTariff());
            tpl.setTariffCode(tplResponse.getTariffCode());
            tpl.setStartDate(tplResponse.getStartDate());
            tpl.setEndDate(tplResponse.getEndDate());
            tpl.setPolicyNumber(tplResponse.getPolicyNumber());
            tpl.setCompanyDescription(tplResponse.getCompanyDescription());
            tpl.setInsuranceCompanyId(tplResponse.getInsuranceCompanyId());
            tpl.setInsurancePolicyId(tplResponse.getInsurancePolicyId());

            return tpl;
        }
        return null;
    }

    public static TplResponse adptTplESBToTplResponse(TplEsb tplEsb) {

        if (tplEsb != null) {

            TplResponse tplResponse = new TplResponse();

            tplResponse.setServiceId(tplEsb.getServiceId());
            tplResponse.setService(tplEsb.getService());
            tplResponse.setMaxCoverage(tplEsb.getMaxCoverage());
            tplResponse.setDeductible(tplEsb.getDeductible());
            tplResponse.setCompanyId(tplEsb.getCompanyId());
            tplResponse.setCompany(tplEsb.getCompany());
            if(tplEsb.getTariffId() != null)
            tplResponse.setTariffId(tplEsb.getTariffId().toString());
            tplResponse.setTariff(tplEsb.getTariff());
            tplResponse.setTariffCode(tplEsb.getTariffCode());
            tplResponse.setStartDate(tplEsb.getStartDate());
            tplResponse.setEndDate(tplEsb.getEndDate());
            tplResponse.setPolicyNumber(tplEsb.getPolicyNumber());

            return tplResponse;
        }
        return null;
    }

    public static TplResponse adptTplMiddlewareToTplResponse(MiddlewareTplResponse tplEsb) {

        if (tplEsb != null) {

            TplResponse tplResponse = new TplResponse();

            if(tplEsb.getCompanyId() != null) {
                tplResponse.setCompanyId(tplEsb.getCompanyId().toString());
            }
            tplResponse.setCompany(tplEsb.getCompany());
            if(tplEsb.getTariffId() != null)
                tplResponse.setTariffId(tplEsb.getTariffId().toString());
            tplResponse.setTariff(tplEsb.getTariff());
            tplResponse.setTariffCode(tplEsb.getTariffCode());
            tplResponse.setStartDate(tplEsb.getStartdate());
            tplResponse.setEndDate(tplEsb.getEnddate());
            tplResponse.setPolicyNumber(tplEsb.getPolicyNumber());

            return tplResponse;
        }
        return null;
    }
}
