package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.damaged.CustomerRequest;
import com.doing.nemo.claims.controller.payload.response.damaged.CustomerResponse;
import com.doing.nemo.claims.entity.claims.ClaimsDamagedCustomerEntity;
import com.doing.nemo.claims.entity.esb.CustomerESB.CustomerEsb;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.Customer;
import com.doing.nemo.claims.entity.settings.PersonalDataEntity;
import com.doing.nemo.middleware.client.payload.response.MiddlewareCustomerResponse;
import com.google.common.math.DoubleMath;
import org.apache.commons.validator.routines.DoubleValidator;
import org.springframework.stereotype.Component;

@Component
public class CustomerAdapter {

    public static Customer adptCustomerRequestToCustomer(CustomerRequest customerRequest) {

        if (customerRequest != null) {

            Customer customer = new Customer();
            customer.setCustomerId(customerRequest.getCustomerId().toString());
            customer.setEmail(customerRequest.getEmail());
            customer.setLegalName(customerRequest.getLegalName());
            customer.setMainAddress(customerRequest.getMainAddress());
            customer.setOfficialRegistration(customerRequest.getOfficialRegistration());
            customer.setPec(customerRequest.getPec());
            customer.setPhonenr(customerRequest.getPhonenr());
            customer.setTradingName(customerRequest.getTradingName());
            customer.setVatNumber(customerRequest.getVatNumber());
            customer.setStatus(customerRequest.getStatus());
            return customer;
        }

        return null;
    }

    public static CustomerResponse adptCustomerToCustomerResposne(Customer customer) {

        if (customer != null) {

            CustomerResponse customerResponse = new CustomerResponse();


            customerResponse.setCustomerId(customer.getCustomerId());
            customerResponse.setEmail(customer.getEmail());
            customerResponse.setLegalName(customer.getLegalName());
            customerResponse.setMainAddress(customer.getMainAddress());
            customerResponse.setOfficialRegistration(customer.getOfficialRegistration());
            customerResponse.setPec(customer.getPec());
            customerResponse.setPhonenr(customer.getPhonenr());
            customerResponse.setTradingName(customer.getTradingName());
            customerResponse.setVatNumber(customer.getVatNumber());
            customerResponse.setStatus(customer.getStatus());

            return customerResponse;
        }
        return null;
    }

    public static Customer adptCustomerResponseToCustomer(CustomerResponse customerResponse) {

        if (customerResponse != null) {

            Customer customer = new Customer();
            customer.setCustomerId(customerResponse.getCustomerId().toString());
            customer.setEmail(customerResponse.getEmail());
            customer.setLegalName(customerResponse.getLegalName());
            customer.setMainAddress(customerResponse.getMainAddress());
            customer.setOfficialRegistration(customerResponse.getOfficialRegistration());
            customer.setPec(customerResponse.getPec());
            customer.setPhonenr(customerResponse.getPhonenr());
            customer.setTradingName(customerResponse.getTradingName());
            customer.setVatNumber(customerResponse.getVatNumber());
            customer.setStatus(customerResponse.getStatus());
            return customer;
        }
        return null;
    }

    public static CustomerResponse adptMiddlewareCustomerResponseToCustomerResposne(CustomerEsb customerEsb) {

        if (customerEsb != null) {

            CustomerResponse customerResponse = new CustomerResponse();
            customerResponse.setCustomerId(customerEsb.getCustomerId());
            customerResponse.setEmail(customerEsb.getEmail());
            customerResponse.setLegalName(customerEsb.getLegalName());
            customerResponse.setMainAddress(AddressAdapter.adptAddressESBToAddress(customerEsb.getMainAddress()));
            customerResponse.setOfficialRegistration(customerEsb.getOfficialRegistration());
            customerResponse.setPec(customerEsb.getPec());
            customerResponse.setPhonenr(customerEsb.getPhonenr());
            customerResponse.setTradingName(customerEsb.getTradingName());
            customerResponse.setVatNumber(customerEsb.getVatNumber());
            customerResponse.setStatus(customerEsb.getStatus());
            return customerResponse;
        }
        return null;
    }

    public static PersonalDataEntity adptCustomerResponseToPersonalDataEntity(CustomerResponse customerResponse) {
        PersonalDataEntity personalDataEntity = new PersonalDataEntity();
        if (customerResponse.getCustomerId() != null)
            personalDataEntity.setCustomerId(customerResponse.getCustomerId().toString());
        if (customerResponse.getTradingName() != null)
            personalDataEntity.setName(customerResponse.getTradingName());
        if (customerResponse.getVatNumber() != null)
            personalDataEntity.setVatNumber(customerResponse.getVatNumber());
        if (customerResponse.getPhonenr() != null)
            personalDataEntity.setPhoneNumber(customerResponse.getPhonenr());
        if (customerResponse.getMainAddress() != null) {
            if (customerResponse.getMainAddress().getStreet() != null)
                personalDataEntity.setAddress(customerResponse.getMainAddress().getStreet());
            if (customerResponse.getMainAddress().getStreetNr() != null)
                personalDataEntity.setAddress(personalDataEntity.getAddress() + ", " + customerResponse.getMainAddress().getStreetNr());
            if (customerResponse.getMainAddress().getProvince() != null)
                personalDataEntity.setProvince(customerResponse.getMainAddress().getProvince());
            if (customerResponse.getMainAddress().getLocality() != null)
                personalDataEntity.setCity(customerResponse.getMainAddress().getLocality());
            if (customerResponse.getMainAddress().getZip() != null)
                personalDataEntity.setZipCode(customerResponse.getMainAddress().getZip());
            if (customerResponse.getMainAddress().getState() != null)
                personalDataEntity.setState(customerResponse.getMainAddress().getState());
        }
        if (customerResponse.getEmail() != null)
            personalDataEntity.setEmail(customerResponse.getEmail());
        if (customerResponse.getPec() != null)
            personalDataEntity.setPec(customerResponse.getPec());

        personalDataEntity.setActive(true);
        personalDataEntity.setCreatedAt(DateUtil.getNowInstant());

        return personalDataEntity;
    }


    public static CustomerResponse adptMiddlewareCustomerResponseToCustomerResposne(MiddlewareCustomerResponse middlewareCustomerResponse) {

        if (middlewareCustomerResponse != null) {

            CustomerResponse customerResponse = new CustomerResponse();
            customerResponse.setCustomerId(middlewareCustomerResponse.getCustomerId()!= null ? middlewareCustomerResponse.getCustomerId().toString() : null);
            customerResponse.setEmail(middlewareCustomerResponse.getEmail());
            customerResponse.setLegalName(middlewareCustomerResponse.getLegalName());
            customerResponse.setMainAddress(AddressAdapter.adptMiddlewareMainAddressResponseToAddress(middlewareCustomerResponse.getMainAddress()));
            customerResponse.setOfficialRegistration(middlewareCustomerResponse.getOfficialRegistration());
            customerResponse.setPec(middlewareCustomerResponse.getPec());
            customerResponse.setPhonenr(middlewareCustomerResponse.getPhoneNumber());
            customerResponse.setTradingName(middlewareCustomerResponse.getTradingName());
            customerResponse.setVatNumber(middlewareCustomerResponse.getVatNumber());
            customerResponse.setStatus(middlewareCustomerResponse.getStatus());
            return customerResponse;
        }
        return null;
    }

    //region NUOVI ADAPTER
    public static ClaimsDamagedCustomerEntity adptCustomerRequestToClaimsDamagedCustomerEntity(Customer customerEntity) {

        if (customerEntity != null) {

            ClaimsDamagedCustomerEntity customer = new ClaimsDamagedCustomerEntity();
            if(customerEntity.getCustomerId() != null) {
                try {
                    customer.setCustomerId(Long.parseLong(customerEntity.getCustomerId()));
                }catch (NumberFormatException exception){
                    Double doubleVal = Double.parseDouble(customerEntity.getCustomerId());
                    customer.setCustomerId(new Long(doubleVal.longValue()));
                }
            }
            customer.setEmail(customerEntity.getEmail());
            customer.setLegalName(customerEntity.getLegalName());
            customer.setMainAddress(customerEntity.getMainAddress());
            customer.setOfficialRegistration(customerEntity.getOfficialRegistration());
            customer.setPec(customerEntity.getPec());
            customer.setPhonenr(customerEntity.getPhonenr());
            customer.setTradingName(customerEntity.getTradingName());
            customer.setVatNumber(customerEntity.getVatNumber());
            customer.setStatus(customerEntity.getStatus());
            return customer;
        }

        return null;
    }

    public static Customer adptClaimsCustomerToCustomerJsonb(ClaimsDamagedCustomerEntity customerJsonb) {

        if (customerJsonb != null) {

            Customer customer = new Customer();

            if(customerJsonb.getCustomerId() != null) {
                customer.setCustomerId(customerJsonb.getCustomerId().toString());
            }
            customer.setEmail(customerJsonb.getEmail());
            customer.setLegalName(customerJsonb.getLegalName());
            customer.setMainAddress(customerJsonb.getMainAddress());
            customer.setOfficialRegistration(customerJsonb.getOfficialRegistration());
            customer.setPec(customerJsonb.getPec());
            customer.setPhonenr(customerJsonb.getPhonenr());
            customer.setTradingName(customerJsonb.getTradingName());
            customer.setVatNumber(customerJsonb.getVatNumber());
            customer.setStatus(customerJsonb.getStatus());
            return customer;
        }

        return null;
    }

    //endregion

}
