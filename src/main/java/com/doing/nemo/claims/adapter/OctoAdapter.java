package com.doing.nemo.claims.adapter;


import com.doing.nemo.claims.crashservices.octo.GetVoucherInfoRequest;
import com.doing.nemo.claims.crashservices.octo.GetVoucherInfoResponse;
import com.doing.nemo.claims.entity.AntiTheftRequestEntity;
import com.doing.nemo.claims.entity.AntiTheftRequestNewEntity;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbAntiTheftService.AntiTheftRequest;
import org.springframework.stereotype.Component;


@Component
public class OctoAdapter {

    public static GetVoucherInfoRequest adptFromInputToOctoRequest(String claimNumber, String companyCode, String dateTime, String user, String pwd, String requestType, String plate, Long voucher){

        GetVoucherInfoRequest request  = new GetVoucherInfoRequest();
        request.setClaimNumber(claimNumber);
        request.setCompanyCode(companyCode);
        request.setDateTime(dateTime);
        request.setUser(user);
        request.setPwd(pwd);
        request.setRequestType(requestType);
        request.setPlate(plate);
        request.setVoucher(voucher.toString());

        return request;
    }

    public static AntiTheftRequest adptFromOctoResponseToAntiTheftRequest(GetVoucherInfoResponse response, AntiTheftRequest antiTheftRequest){
        //AntiTheftRequest antiTheftRequest = new AntiTheftRequest();
        antiTheftRequest.setAnomaly(response.getAnomaly() > 0);
        antiTheftRequest.setgPower(response.getgValue());
        antiTheftRequest.setCrashNumber(response.getReportNumber());
        antiTheftRequest.setOutcome(response.getOutcome());
        String pdf = null;
        if(response.getPdfReport() != null)
           pdf  = response.getPdfReport();
        antiTheftRequest.setPdf(pdf);
        antiTheftRequest.setSubReport(response.getReportNumber());
        Long code = null;
        if(response.getRespCode() != null){
            code  = response.getRespCode();
            antiTheftRequest.setResponseType(code.toString());
        }
        antiTheftRequest.setResponseMessage(response.getRespMsg());

        return antiTheftRequest;
    }


    public static AntiTheftRequestNewEntity adptFromOctoResponseToAntiTheftServiceEntity(GetVoucherInfoResponse response, AntiTheftRequestNewEntity antiTheftRequestEntity){
        if(response == null){
            return null;
        }

        antiTheftRequestEntity.setAnomaly(response.getAnomaly() > 0);
        antiTheftRequestEntity.setgPower(response.getgValue());
        antiTheftRequestEntity.setCrashNumber(response.getNumberCrash());
        antiTheftRequestEntity.setOutcome(response.getOutcome());
        antiTheftRequestEntity.setSubReport(response.getReportNumber());
        Long code = null;
        if(response.getRespCode() != null){
            code  = response.getRespCode();
            antiTheftRequestEntity.setResponseType(code.toString());
        }
        antiTheftRequestEntity.setResponseMessage(response.getRespMsg());

        return antiTheftRequestEntity;
    }


}
