package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.dto.DwhCounterpartyDTO;
import com.doing.nemo.claims.entity.CounterpartyEntity;
import org.springframework.stereotype.Component;

@Component
public class DwhCounterpartyDTOAdapter {
    public static DwhCounterpartyDTO adptFromCounterpartyToDwhCounterpartyDTO(CounterpartyEntity counterparty){
        if(counterparty == null) return null;
        DwhCounterpartyDTO dwhCounterpartyDTO = new DwhCounterpartyDTO();
        dwhCounterpartyDTO.setCounterpartyId(counterparty.getCounterpartyId());
        dwhCounterpartyDTO.setCreatedAt(counterparty.getCreatedAt());
        dwhCounterpartyDTO.setPracticeIdCounterparty(counterparty.getPracticeIdCounterparty());
        dwhCounterpartyDTO.setType(counterparty.getType());
        dwhCounterpartyDTO.setRepairStatus(counterparty.getRepairStatus());
        dwhCounterpartyDTO.setRepairProcedure(counterparty.getRepairProcedure());
        dwhCounterpartyDTO.setUserCreate(counterparty.getUserCreate());
        dwhCounterpartyDTO.setUserUpdate(counterparty.getUserUpdate());
        dwhCounterpartyDTO.setUpdateAt(counterparty.getUpdateAt());
        dwhCounterpartyDTO.setAssignedTo(counterparty.getAssignedTo());
        dwhCounterpartyDTO.setAssignedAt(counterparty.getAssignedAt());
        dwhCounterpartyDTO.setInsured(counterparty.getInsured());
        dwhCounterpartyDTO.setInsuranceCompany(counterparty.getInsuranceCompany());
        dwhCounterpartyDTO.setDriver(counterparty.getDriver());
        dwhCounterpartyDTO.setVehicle(counterparty.getVehicle());
        dwhCounterpartyDTO.setCaiSigned(counterparty.getCaiSigned());
        dwhCounterpartyDTO.setImpactPoint(counterparty.getImpactPoint());
        dwhCounterpartyDTO.setResponsible(counterparty.getResponsible());
        dwhCounterpartyDTO.setEligibility(counterparty.getEligibility());
        dwhCounterpartyDTO.setAttachments(counterparty.getAttachments());
        dwhCounterpartyDTO.setHistoricals(counterparty.getHistoricals());
        dwhCounterpartyDTO.setDescription(counterparty.getDescription());
        dwhCounterpartyDTO.setLastContact(counterparty.getLastContact());
        dwhCounterpartyDTO.setManager(counterparty.getManager());
        dwhCounterpartyDTO.setCanalization(counterparty.getCanalization());
        dwhCounterpartyDTO.setPolicyNumber(counterparty.getPolicyNumber());
        dwhCounterpartyDTO.setPolicyBeginningValidity(counterparty.getPolicyBeginningValidity());
        dwhCounterpartyDTO.setPolicyEndValidity(counterparty.getPolicyEndValidity());
        dwhCounterpartyDTO.setManagementType(counterparty.getManagementType());
        dwhCounterpartyDTO.setAskedForDamages(counterparty.getAskedForDamages());
        dwhCounterpartyDTO.setLegalOrConsultant(counterparty.getLegalOrConsultant());
        dwhCounterpartyDTO.setDateRequestDamages(counterparty.getInstantRequestDamages());
        dwhCounterpartyDTO.setExpirationInstant(counterparty.getExpirationInstant());
        dwhCounterpartyDTO.setLegal(counterparty.getLegal());
        dwhCounterpartyDTO.setEmailLegal(counterparty.getEmailLegal());
        //dwhCounterpartyDTO.setClaims(counterparty.getClaims());
        dwhCounterpartyDTO.setAld(counterparty.getAld());
        dwhCounterpartyDTO.setAuthorities(counterparty.getAuthorities());
        dwhCounterpartyDTO.setReadMsa(counterparty.getIsReadMsa());
        return dwhCounterpartyDTO;

    }
}
