package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.vehicle.RelatedContractRequest;
import com.doing.nemo.claims.controller.payload.response.vehicle.RelatedContractResponse;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbVehicle.RelatedContract;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

@Component
public class RelatedContractAdapter {

    public static RelatedContract adptRelatedContractRequestToRelatedContract(RelatedContractRequest relatedContractRequest) {

        if (relatedContractRequest != null) {

            RelatedContract relatedContract = new RelatedContract();
            relatedContract.setActive(relatedContractRequest.getActive());
            relatedContract.setEndDate(DateUtil.convertIS08601StringToUTCDate(relatedContractRequest.getEndDate()));
            relatedContract.setId(relatedContractRequest.getId());
            relatedContract.setLinksList(LinkAdapter.adptLinkRequestToLink(relatedContractRequest.getLinksList()));
            relatedContract.setStartDate(DateUtil.convertIS08601StringToUTCDate(relatedContractRequest.getStartDate()));

            return relatedContract;
        }

        return null;
    }

    public static List<RelatedContract> adptRelatedContractRequestToRelatedContract(List<RelatedContractRequest> relatedContractRequestList) {

        if (relatedContractRequestList != null) {

            List<RelatedContract> relatedContractList = new LinkedList<>();
            for (RelatedContractRequest att : relatedContractRequestList) {
                relatedContractList.add(adptRelatedContractRequestToRelatedContract(att));
            }

            return relatedContractList;
        }

        return null;
    }

    public static RelatedContractResponse adptRelatedContractToRelatedContractResponse(RelatedContract relatedContract) {

        RelatedContractResponse relatedContractResponse = new RelatedContractResponse();
        relatedContractResponse.setActive(relatedContract.getActive());
        relatedContractResponse.setEndDate(DateUtil.convertUTCDateToIS08601String(relatedContract.getEndDate()));
        relatedContractResponse.setId(relatedContract.getId());
        relatedContractResponse.setLinksList(LinkAdapter.adptLinkToLinkResponse(relatedContract.getLinksList()));
        relatedContractResponse.setStartDate(DateUtil.convertUTCDateToIS08601String(relatedContract.getStartDate()));

        return relatedContractResponse;
    }

    public static List<RelatedContractResponse> adptRelatedContractToRelatedContractResponse(List<RelatedContract> relatedContractList) {

        if (relatedContractList != null) {

            List<RelatedContractResponse> relatedContractListResponse = new LinkedList<>();
            for (RelatedContract att : relatedContractList) {
                relatedContractListResponse.add(adptRelatedContractToRelatedContractResponse(att));
            }

            return relatedContractListResponse;
        }
        return null;
    }
}
