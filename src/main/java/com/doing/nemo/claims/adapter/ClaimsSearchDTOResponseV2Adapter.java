package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.response.AntiTheftRequestResponseV1;
import com.doing.nemo.claims.controller.payload.response.AntiTheftServiceResponseV1;
import com.doing.nemo.claims.controller.payload.response.ClaimsResponseV2;
import com.doing.nemo.claims.controller.payload.response.claims.ComplaintResponse;
import com.doing.nemo.claims.controller.payload.response.claims.DamagedResponse;
import com.doing.nemo.claims.controller.payload.response.complaint.DataAccidentResponse;
import com.doing.nemo.claims.controller.payload.response.complaint.EntrustedResponse;
import com.doing.nemo.claims.controller.payload.response.damaged.CustomerResponse;
import com.doing.nemo.claims.dto.ClaimsSearchDTO;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.Customer;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ClaimsSearchDTOResponseV2Adapter {
    public ClaimsResponseV2 adaptToResponseLite(ClaimsSearchDTO dto) {

        if (dto == null) {
            return null;
        }

        ClaimsResponseV2 response = new ClaimsResponseV2();



        response.setPending(dto.getPending());
        response.setInEvidence(dto.getInEvidence());
        response.setPoVariation(dto.getPoVariation());
        response.setType(dto.getType());
        response.setPracticeId(dto.getPracticeId());
        response.setUpdateAt(DateUtil.convertUTCInstantToIS08601String(dto.getUpdatedAt()));

        DamagedResponse damage = new DamagedResponse();
        CustomerResponse customer = new CustomerResponse();
        customer.setCustomerId(dto.getCustomerId());
        damage.setCustomer(customer);

        AntiTheftServiceResponseV1 antiTheftService = new AntiTheftServiceResponseV1();
        antiTheftService.setBusinessName(dto.getBusinessName()!=null?dto.getBusinessName():"");

        if (dto.getResponseType()!=null&&dto.getProviderType()!=null) {
            List<AntiTheftRequestResponseV1> antiTheftRequestResponseV1list = new ArrayList<>();
            AntiTheftRequestResponseV1 antiTheftRequestResponseV1 = new AntiTheftRequestResponseV1();
            antiTheftRequestResponseV1.setResponseType(dto.getResponseType());
            antiTheftRequestResponseV1.setProviderType(dto.getProviderType());
            antiTheftRequestResponseV1list.add(antiTheftRequestResponseV1);
            antiTheftService.setAntiTheftRequestResponseV1list(antiTheftRequestResponseV1list);
        }

        damage.setAntiTheftServiceResponseV1(antiTheftService); //response.setDamagedAntiTheftService_id(dto.getDamagedAntiTheftService_id());
        response.setDamaged(damage);

        ComplaintResponse complaintResponse = new ComplaintResponse();
        DataAccidentResponse dataAccidentResponse = new DataAccidentResponse();

        dataAccidentResponse.setDateAccident(DateUtil.convertUTCInstantToIS08601String(dto.getDateAccident())); //conversione in String
        dataAccidentResponse.setTypeAccident(DataAccidentTypeAccidentEnum.valueOf(dto.getTypeAccident()));
        complaintResponse.setDataAccident(dataAccidentResponse);
        complaintResponse.setClientId(dto.getClientId());
        EntrustedResponse entrustedResponse = new EntrustedResponse();
        entrustedResponse.setAutoEntrust(dto.getAutoEntrust());
        complaintResponse.setEntrusted(entrustedResponse);
        complaintResponse.setPlate(dto.getPlate());

        response.setComplaint(complaintResponse);

        response.setCreatedAt(DateUtil.convertUTCInstantToIS08601String(dto.getCreatedAt())); //conversione in String

      /*  response.setStatusUpdatedAt(dto.getStatusUpdatedAt());*/
        response.setStatus(dto.getStatus());

        response.setId(dto.getId());

        return response;
    }

    public List<ClaimsResponseV2> adaptToResponseLite( List<ClaimsSearchDTO> claimsSearchDTOList) {

        if (CollectionUtils.isEmpty(claimsSearchDTOList)) {
            return Collections.emptyList();
        }

        return claimsSearchDTOList.stream()
                .map(this::adaptToResponseLite)
                .collect(Collectors.toList());
    }
}
