package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.complaint.dataaccident.ReceivedItemRequest;
import com.doing.nemo.claims.controller.payload.response.dataaccident.ReceivedItemResponse;
import com.doing.nemo.claims.entity.jsonb.complaint.jsonbComplaint.jsonbDataAccidents.ReceivedItem;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

@Component
public class ReceivedItemAdapter {

    public static ReceivedItem adptReceivedItemRequestToReceivedItem(ReceivedItemRequest receivedItemRequest) {

        if (receivedItemRequest != null) {

            ReceivedItem receivedItem = new ReceivedItem();
            receivedItem.setDateReceived(DateUtil.convertIS08601StringToUTCDate(receivedItemRequest.getDateReceived()));
            receivedItem.setNote(receivedItemRequest.getNote());
            receivedItem.setReceived(receivedItemRequest.getReceived());
            receivedItem.setReport(receivedItemRequest.getReport());
            receivedItem.setType(receivedItemRequest.getType());

            return receivedItem;
        }
        return null;
    }

    public static List<ReceivedItem> adptReceivedItemRequestToReceivedItem(List<ReceivedItemRequest> receivedItemRequestsList) {

        if (receivedItemRequestsList != null) {

            List<ReceivedItem> receivedItemList = new LinkedList<>();
            for (ReceivedItemRequest att : receivedItemRequestsList) {

                receivedItemList.add(adptReceivedItemRequestToReceivedItem(att));
            }
            return receivedItemList;
        }
        return null;
    }

    public static ReceivedItemResponse adptReceivedItemToReceivedItemResponse(ReceivedItem receivedItem) {

        if (receivedItem != null) {

            ReceivedItemResponse receivedItemResponse = new ReceivedItemResponse();
            receivedItemResponse.setDateReceived(DateUtil.convertUTCDateToIS08601String(receivedItem.getDateReceived()));
            receivedItemResponse.setNote(receivedItem.getNote());
            receivedItemResponse.setReceived(receivedItem.getReceived());
            receivedItemResponse.setReport(receivedItem.getReport());
            receivedItemResponse.setType(receivedItem.getType());

            return receivedItemResponse;
        }
        return null;
    }

    public static List<ReceivedItemResponse> adptReceivedItemToReceivedItemResponse(List<ReceivedItem> receivedItemList) {

        if (receivedItemList != null) {

            List<ReceivedItemResponse> receivedItemResponse = new LinkedList<>();
            for (ReceivedItem att : receivedItemList) {

                receivedItemResponse.add(adptReceivedItemToReceivedItemResponse(att));
            }
            return receivedItemResponse;
        }

        return null;
    }

}
