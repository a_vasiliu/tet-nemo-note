package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.controller.payload.request.driver.LinkRequest;
import com.doing.nemo.claims.controller.payload.response.vehicle.LinkResponse;
import com.doing.nemo.claims.entity.jsonb.Link;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

@Component
public class LinkAdapter {

    public static Link adptLinkRequestToLink(LinkRequest linkRequest) {

        Link link = new Link();
        link.setHref(linkRequest.getHref());
        link.setRel(linkRequest.getRel());
        link.setTestList(TestAdapter.adptTestRequestToTest(linkRequest.getTestList()));

        return link;
    }

    public static List<Link> adptLinkRequestToLink(List<LinkRequest> linkListRequest) {

        if (linkListRequest != null) {

            List<Link> linkList = new LinkedList<>();
            for (LinkRequest att : linkListRequest) {
                linkList.add(adptLinkRequestToLink(att));
            }

            return linkList;
        }

        return null;
    }

    public static LinkResponse adptLinkToLinkResponse(Link link) {

        LinkResponse linkResponse = new LinkResponse();
        linkResponse.setHref(link.getHref());
        linkResponse.setRel(link.getRel());
        linkResponse.setTestList(TestAdapter.adptTestToTestResponse(link.getTestList()));

        return linkResponse;
    }

    public static List<LinkResponse> adptLinkToLinkResponse(List<Link> linkList) {

        if (linkList != null) {

            List<LinkResponse> linkListRequest = new LinkedList<>();
            for (Link att : linkList) {
                linkListRequest.add(adptLinkToLinkResponse(att));
            }

            return linkListRequest;
        }
        return null;
    }

}
