package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.controller.payload.request.counterparty.DriverRequest;
import com.doing.nemo.claims.controller.payload.response.damaged.DriverResponse;
import com.doing.nemo.claims.entity.claims.ClaimsDamagedDriverEntity;
import com.doing.nemo.claims.entity.esb.DriverESB.DriverEsb;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbDriver.Driver;
import org.springframework.stereotype.Component;

@Component
public class DriverAdapter {

    public static Driver adptDriverRequestToDriver(DriverRequest driverRequest) {

        if (driverRequest != null) {

            Driver driver = new Driver();
            driver.setId(driverRequest.getId());
            driver.setIdentification(driverRequest.getIdentification());
            driver.setOfficialRegistration(driverRequest.getOfficialRegistration());
            driver.setTradingName(driverRequest.getTradingName());
            driver.setFirstname(driverRequest.getFirstname());
            driver.setLastname(driverRequest.getLastname());
            driver.setFiscalCode(driverRequest.getFiscalCode());
            driver.setMainAddress(driverRequest.getMainAddress());
            driver.setDateOfBirth(driverRequest.getDateOfBirth());
            driver.setPhone(driverRequest.getPhone());
            driver.setEmail(driverRequest.getEmail());
            driver.setPec(driverRequest.getPec());
            driver.setSex(driverRequest.getSex());
            driver.setCustomerId(driverRequest.getCustomerId());
            driver.setDriverInjury(driverRequest.getDriverInjury());
            driver.setDrivingLicense(DrivingLicenseAdapter.adptDrivingLicenseRequestToDrivingLicense(driverRequest.getDrivingLicense()));

            return driver;
        }

        return null;
    }

    public static DriverResponse adptDriverToDriverResponse(Driver driver) {

        if (driver != null) {

            DriverResponse driverResponse = new DriverResponse();

            driverResponse.setId(driver.getId());
            driverResponse.setIdentification(driver.getIdentification());
            driverResponse.setOfficialRegistration(driver.getOfficialRegistration());
            driverResponse.setTradingName(driver.getTradingName());
            driverResponse.setFirstname(driver.getFirstname());
            driverResponse.setLastname(driver.getLastname());
            driverResponse.setFiscalCode(driver.getFiscalCode());
            driverResponse.setMainAddress(driver.getMainAddress());
            driverResponse.setDateOfBirth(driver.getDateOfBirth());
            driverResponse.setPhone(driver.getPhone());
            driverResponse.setEmail(driver.getEmail());
            driverResponse.setPec(driver.getPec());
            driverResponse.setSex(driver.getSex());
            driverResponse.setCustomerId(driver.getCustomerId());
            driverResponse.setDriverInjury(driver.getDriverInjury());
            driverResponse.setDrivingLicense(DrivingLicenseAdapter.adptDrivingLicenseToDrivingLicenseResponse(driver.getDrivingLicense()));

            return driverResponse;
        }
        return null;

    }

    public static Driver adptDriverResponseToDriver(DriverResponse driverResponse) {

        if (driverResponse != null) {

            Driver driver = new Driver();

            driver.setId(driverResponse.getId());
            driver.setIdentification(driverResponse.getIdentification());
            driver.setOfficialRegistration(driverResponse.getOfficialRegistration());
            driver.setTradingName(driverResponse.getTradingName());
            driver.setFirstname(driverResponse.getFirstname());
            driver.setLastname(driverResponse.getLastname());
            driver.setFiscalCode(driverResponse.getFiscalCode());
            driver.setMainAddress(driverResponse.getMainAddress());
            driver.setDateOfBirth(driverResponse.getDateOfBirth());
            driver.setPhone(driverResponse.getPhone());
            driver.setEmail(driverResponse.getEmail());
            driver.setPec(driverResponse.getPec());
            driver.setSex(driverResponse.getSex());
            driver.setCustomerId(driverResponse.getCustomerId());
            driver.setDriverInjury(driverResponse.getDriverInjury());
            driver.setDrivingLicense(DrivingLicenseAdapter.adptDrivingLicenseResponseToDrivingLicense(driverResponse.getDrivingLicense()));

            return driver;
        }
        return null;

    }

    public static DriverResponse adptDriverEsbToDriverResponse(DriverEsb driverEsb) {

        if (driverEsb != null) {

            DriverResponse driverResponse = new DriverResponse();
            if(driverEsb.getId() != null)
                driverResponse.setId(Integer.parseInt(driverEsb.getId()));
            driverResponse.setIdentification(driverEsb.getIdentification());
            driverResponse.setOfficialRegistration(driverEsb.getOfficialRegistration());
            driverResponse.setTradingName(driverEsb.getTradingName());
            driverResponse.setFirstname(driverEsb.getFirstname());
            driverResponse.setLastname(driverEsb.getLastname());
            driverResponse.setMainAddress(AddressAdapter.adptAddressESBToAddress(driverEsb.getMainAddress()));
            driverResponse.setPhone(driverEsb.getPhone());
            driverResponse.setEmail(driverEsb.getEmail());
            driverResponse.setPec(driverEsb.getPec());
            driverResponse.setSex(driverEsb.getSex());
            driverResponse.setCustomerId(driverEsb.getCustomerId());
            driverResponse.setDrivingLicense(DrivingLicenseAdapter.adptDrivingLicenseEsbToDrivingLicenseResponse(driverEsb.getDrivingLicense()));

            return driverResponse;
        }
        return null;

    }

    //region NUOVI ADAPTER
    public static ClaimsDamagedDriverEntity adptDriverEntityToClaimsDamagedDriverEntity(Driver driverJsonb) {

        if (driverJsonb != null) {

            ClaimsDamagedDriverEntity driver = new ClaimsDamagedDriverEntity();
            driver.setDriverId(driverJsonb.getId());
            driver.setIdentification(driverJsonb.getIdentification());
            driver.setOfficialRegistration(driverJsonb.getOfficialRegistration());
            driver.setTradingName(driverJsonb.getTradingName());
            driver.setFirstname(driverJsonb.getFirstname());
            driver.setLastname(driverJsonb.getLastname());
            driver.setFiscalCode(driverJsonb.getFiscalCode());
            driver.setMainAddress(driverJsonb.getMainAddress());
            driver.setDateOfBirth(driverJsonb.getDateOfBirth());
            driver.setPhone(driverJsonb.getPhone());
            driver.setEmail(driverJsonb.getEmail());
            driver.setPec(driverJsonb.getPec());
            driver.setSex(driverJsonb.getSex());
            driver.setCustomerId(driverJsonb.getCustomerId());
            driver.setDriverInjury(driverJsonb.getDriverInjury());
            driver.setDrivingLicense(driverJsonb.getDrivingLicense());

            return driver;
        }

        return null;
    }

    public static Driver adptfromClaimsDriverEntityToDriverJsonb(ClaimsDamagedDriverEntity driverClaimsEntity) {

        if (driverClaimsEntity != null) {

            Driver driver = new Driver();

            driver.setId(driverClaimsEntity.getDriverId());
            driver.setIdentification(driverClaimsEntity.getIdentification());
            driver.setOfficialRegistration(driverClaimsEntity.getOfficialRegistration());
            driver.setTradingName(driverClaimsEntity.getTradingName());
            driver.setFirstname(driverClaimsEntity.getFirstname());
            driver.setLastname(driverClaimsEntity.getLastname());
            driver.setFiscalCode(driverClaimsEntity.getFiscalCode());
            driver.setMainAddress(driverClaimsEntity.getMainAddress());
            driver.setDateOfBirth(driverClaimsEntity.getDateOfBirth());
            driver.setPhone(driverClaimsEntity.getPhone());
            driver.setEmail(driverClaimsEntity.getEmail());
            driver.setPec(driverClaimsEntity.getPec());
            driver.setSex(driverClaimsEntity.getSex());
            driver.setCustomerId(driverClaimsEntity.getCustomerId());
            driver.setDriverInjury(driverClaimsEntity.getDriverInjury());
            driver.setDrivingLicense(driverClaimsEntity.getDrivingLicense());

            return driver;
        }

        return null;
    }
    //endregion NUOVI ADAPTER

}
