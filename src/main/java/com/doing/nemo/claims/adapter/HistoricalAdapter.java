package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.claims.HistoricalRequest;
import com.doing.nemo.claims.controller.payload.response.claims.HistoricalResponse;
import com.doing.nemo.claims.entity.jsonb.Historical;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

@Component
public class HistoricalAdapter {

    public static Historical adptHistoricalRequestToHistorical(HistoricalRequest historicalRequest) {

        Historical historical = new Historical();
        historical.setStatusEntityNew(historicalRequest.getStatusEntityNew());
        historical.setStatusEntityOld(historicalRequest.getStatusEntityOld());
        historical.setUpdateAt(DateUtil.convertIS08601StringToUTCDate(historicalRequest.getUpdateAt()));
        historical.setUserId(historicalRequest.getUserId());
        historical.setEventType(historicalRequest.getEventType());
        historical.setComunicationDescription(historicalRequest.getComunicationDescription());
        historical.setUserName(historicalRequest.getUserName());
        historical.setOldType(historicalRequest.getOldType());
        historical.setNewType(historicalRequest.getNewType());
        historical.setOldFlow(historicalRequest.getOldFlow());
        historical.setNewFlow(historicalRequest.getNewFlow());

        return historical;
    }

    public static List<Historical> adptHistoricalRequestToHistorical(List<HistoricalRequest> historicalRequestList) {

        if (historicalRequestList != null) {

            List<Historical> historicalList = new LinkedList<>();
            for (HistoricalRequest att : historicalRequestList) {
                historicalList.add(adptHistoricalRequestToHistorical(att));
            }
            return historicalList;
        }
        return null;
    }

    public static HistoricalResponse adptHistoricalToHistoricalResponse(Historical historical) {

        HistoricalResponse historicalResponse = new HistoricalResponse();
        historicalResponse.setStatusEntityNew(historical.getStatusEntityNew());
        historicalResponse.setStatusEntityOld(historical.getStatusEntityOld());
        historicalResponse.setEventType(historical.getEventType());
        historicalResponse.setUpdateAt(DateUtil.convertUTCDateToIS08601String(historical.getUpdateAt()));
        historicalResponse.setUserId(historical.getUserId());
        historicalResponse.setComunicationDescription(historical.getComunicationDescription());
        historicalResponse.setUserName(historical.getUserName());
        historicalResponse.setOldType(historical.getOldType());
        historicalResponse.setNewType(historical.getNewType());
        historicalResponse.setNewFlow(historical.getNewFlow());
        historicalResponse.setOldFlow(historical.getOldFlow());

        return historicalResponse;

    }

    public static List<HistoricalResponse> adptHistoricalToHistoricalResponse(List<Historical> historicalList) {

        if (historicalList != null) {

            List<HistoricalResponse> historicalListResponse = new LinkedList<>();
            for (Historical att : historicalList) {
                historicalListResponse.add(adptHistoricalToHistoricalResponse(att));
            }
            return historicalListResponse;
        }
        return null;
    }
}
