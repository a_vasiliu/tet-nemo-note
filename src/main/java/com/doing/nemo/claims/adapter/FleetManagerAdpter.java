package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.controller.payload.request.damaged.FleetManagerRequest;
import com.doing.nemo.claims.controller.payload.response.FleetManagerResponseV1;
import com.doing.nemo.claims.entity.claims.ClaimsDamagedFleetManagerEntity;
import com.doing.nemo.claims.entity.esb.FleetManagerESB.FleetManagerEsb;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.FleetManager;
import com.doing.nemo.claims.entity.jsonb.personalData.FleetManagerPersonalData;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;

@Component
public class FleetManagerAdpter {

    public static FleetManager adtpFromFMEsbToFM(FleetManagerEsb fleetManagerEsb) {
        FleetManager fleetManager = new FleetManager();
        fleetManager.setCustomerId(fleetManagerEsb.getCustomerId());
        fleetManager.setEmail(fleetManagerEsb.getEmail());
        fleetManager.setFirstName(fleetManagerEsb.getFirstName());
        fleetManager.setLastName(fleetManagerEsb.getLastName());
        fleetManager.setId(fleetManagerEsb.getId());
        fleetManager.setIdentification(fleetManagerEsb.getIdentification());
        fleetManager.setMainAddress(fleetManagerEsb.getMainAddress());
        fleetManager.setOfficialRegistration(fleetManagerEsb.getOfficialRegistration());
        fleetManager.setPhone(fleetManagerEsb.getPhone());
        fleetManager.setPhonePrefix(fleetManagerEsb.getPhonePrefix());
        fleetManager.setSecondaryPhone(fleetManagerEsb.getSecondaryPhone());
        fleetManager.setSecondaryPhonePrefix(fleetManagerEsb.getPhonePrefix());
        fleetManager.setSex(fleetManagerEsb.getSex());
        fleetManager.setTitle(fleetManagerEsb.getTitle());
        fleetManager.setIsImported(true);
        return fleetManager;
    }

    public static List<FleetManager> adtFromFMESBToListFM(List<FleetManagerEsb> fleetManagerEsbsList) {
        List<FleetManager> fleetManagerList = new LinkedList<>();
        if (fleetManagerEsbsList != null) {
            for (FleetManagerEsb currentFM : fleetManagerEsbsList) {
                fleetManagerList.add(adtpFromFMEsbToFM(currentFM));
            }
        }
        return fleetManagerList;
    }

    public static FleetManagerResponseV1 adtpFromFMEsbToFMRespnse(FleetManagerEsb fleetManagerEsb) {
        FleetManagerResponseV1 fleetManager = new FleetManagerResponseV1();
        fleetManager.setCustomerId(fleetManagerEsb.getCustomerId());
        fleetManager.setEmail(fleetManagerEsb.getEmail());
        fleetManager.setFirstName(fleetManagerEsb.getFirstName());
        fleetManager.setLastName(fleetManagerEsb.getLastName());
        fleetManager.setId(fleetManagerEsb.getId());
        fleetManager.setIdentification(fleetManagerEsb.getIdentification());
        fleetManager.setMainAddress(fleetManagerEsb.getMainAddress());
        fleetManager.setOfficialRegistration(fleetManagerEsb.getOfficialRegistration());
        fleetManager.setPhone(fleetManagerEsb.getPhone());
        fleetManager.setPhonePrefix(fleetManagerEsb.getPhonePrefix());
        fleetManager.setSecondaryPhone(fleetManagerEsb.getSecondaryPhone());
        fleetManager.setSecondaryPhonePrefix(fleetManagerEsb.getPhonePrefix());
        fleetManager.setSex(fleetManagerEsb.getSex());
        fleetManager.setTitle(fleetManagerEsb.getTitle());
        fleetManager.setIsImported(true);

        return fleetManager;
    }

    public static FleetManager adptFromFMRequestToFM(FleetManagerRequest fleetManagerRequest) {
        FleetManager fleetManager = new FleetManager();

        fleetManager.setCustomerId(fleetManagerRequest.getCustomerId());
        fleetManager.setEmail(fleetManagerRequest.getEmail());
        fleetManager.setFirstName(fleetManagerRequest.getFirstName());
        fleetManager.setLastName(fleetManagerRequest.getLastName());
        fleetManager.setId(fleetManagerRequest.getId());
        fleetManager.setIdentification(fleetManagerRequest.getIdentification());
        fleetManager.setMainAddress(AddressAdapter.adptAddressToAddressESB(fleetManagerRequest.getMainAddress()));
        fleetManager.setOfficialRegistration(fleetManagerRequest.getOfficialRegistration());
        fleetManager.setPhone(fleetManagerRequest.getPhone());
        fleetManager.setPhonePrefix(fleetManagerRequest.getPhonePrefix());
        fleetManager.setSecondaryPhone(fleetManagerRequest.getSecondaryPhone());
        fleetManager.setSecondaryPhonePrefix(fleetManagerRequest.getPhonePrefix());
        fleetManager.setSex(fleetManagerRequest.getSex());
        fleetManager.setTitle(fleetManagerRequest.getTitle());
        fleetManager.setIsImported(fleetManagerRequest.getIsImported());
        fleetManager.setDisableNotifiaction(fleetManagerRequest.isDisableNotification());

        return fleetManager;
    }

    public static List<FleetManager> adptFromFMRequestToFM(List<FleetManagerRequest> fleetManagerRequestList) {

        if (fleetManagerRequestList != null) {

            List<FleetManager> fleetManagerList = new LinkedList<>();
            for (FleetManagerRequest att : fleetManagerRequestList) {
                fleetManagerList.add(adptFromFMRequestToFM(att));
            }
            return fleetManagerList;
        }
        return null;
    }

    public static FleetManagerResponseV1 adptFromFMToFMResponse(FleetManager fleetManager) {

        FleetManagerResponseV1 fleetManagerResponse = new FleetManagerResponseV1();

        fleetManagerResponse.setCustomerId(fleetManager.getCustomerId());
        fleetManagerResponse.setEmail(fleetManager.getEmail());
        fleetManagerResponse.setFirstName(fleetManager.getFirstName());
        fleetManagerResponse.setLastName(fleetManager.getLastName());
        fleetManagerResponse.setId(fleetManager.getId());
        fleetManagerResponse.setIdentification(fleetManager.getIdentification());
        fleetManagerResponse.setMainAddress(fleetManager.getMainAddress());
        fleetManagerResponse.setOfficialRegistration(fleetManager.getOfficialRegistration());
        fleetManagerResponse.setPhone(fleetManager.getPhone());
        fleetManagerResponse.setPhonePrefix(fleetManager.getPhonePrefix());
        fleetManagerResponse.setSecondaryPhone(fleetManager.getSecondaryPhone());
        fleetManagerResponse.setSecondaryPhonePrefix(fleetManager.getPhonePrefix());
        fleetManagerResponse.setSex(fleetManager.getSex());
        fleetManagerResponse.setTitle(fleetManager.getTitle());
        fleetManagerResponse.setIsImported(fleetManager.getIsImported());
        fleetManagerResponse.setDisableNotification(fleetManager.isDisableNotification());

        return fleetManagerResponse;
    }

    public static List<FleetManagerResponseV1> adptFromFMToFMResponse(List<FleetManager> fleetManagerList) {

        if (fleetManagerList != null) {

            List<FleetManagerResponseV1> fleetManagerResponseList = new LinkedList<>();
            for (FleetManager att : fleetManagerList) {
                fleetManagerResponseList.add(adptFromFMToFMResponse(att));
            }
            return fleetManagerResponseList;
        }
        return null;
    }

    public static FleetManager adptFromFMResponseToFM(FleetManagerResponseV1 fleetManagerResponseV1) {

        FleetManager fleetManager = new FleetManager();

        fleetManager.setCustomerId(fleetManagerResponseV1.getCustomerId());
        fleetManager.setEmail(fleetManagerResponseV1.getEmail());
        fleetManager.setFirstName(fleetManagerResponseV1.getFirstName());
        fleetManager.setLastName(fleetManagerResponseV1.getLastName());
        fleetManager.setId(fleetManagerResponseV1.getId());
        fleetManager.setIdentification(fleetManagerResponseV1.getIdentification());
        fleetManager.setMainAddress(fleetManagerResponseV1.getMainAddress());
        fleetManager.setOfficialRegistration(fleetManagerResponseV1.getOfficialRegistration());
        fleetManager.setPhone(fleetManagerResponseV1.getPhone());
        fleetManager.setPhonePrefix(fleetManagerResponseV1.getPhonePrefix());
        fleetManager.setSecondaryPhone(fleetManagerResponseV1.getSecondaryPhone());
        fleetManager.setSecondaryPhonePrefix(fleetManagerResponseV1.getPhonePrefix());
        fleetManager.setSex(fleetManagerResponseV1.getSex());
        fleetManager.setTitle(fleetManagerResponseV1.getTitle());
        fleetManager.setIsImported(true);
        fleetManager.setDisableNotifiaction(fleetManagerResponseV1.isDisableNotification());

        return fleetManager;
    }

    public static List<FleetManager> adptFromFMResponseToFM(List<FleetManagerResponseV1> fleetManagerResponseV1List) {

        if (fleetManagerResponseV1List != null) {

            List<FleetManager> fleetManagerList = new LinkedList<>();
            for (FleetManagerResponseV1 att : fleetManagerResponseV1List) {
                fleetManagerList.add(adptFromFMResponseToFM(att));
            }
            return fleetManagerList;
        }
        return null;
    }

    public static List<FleetManagerResponseV1> adtFromBlockingQueueFMESBToListFMResponse(BlockingQueue<FleetManagerEsb> fleetManagerEsbsList) {
        List<FleetManagerResponseV1> fleetManagerResponseV1List = new LinkedList<>();
        if (fleetManagerEsbsList != null) {
            for (FleetManagerEsb currentFM : fleetManagerEsbsList) {
                fleetManagerResponseV1List.add(adtpFromFMEsbToFMRespnse(currentFM));
            }
        }
        return fleetManagerResponseV1List;
    }



    public static FleetManagerPersonalData adptFleetManagerEsbtoFleetManagerPersonalData(FleetManagerEsb fleetManagerEsb){
        if(fleetManagerEsb == null) {
            return null;
        }

        FleetManagerPersonalData fleetManagerPersonalData = new FleetManagerPersonalData();
        fleetManagerPersonalData.setFleetManagerId(fleetManagerEsb.getId());
        fleetManagerPersonalData.setFleetManagerName(fleetManagerEsb.getFirstName() + " " + fleetManagerEsb.getLastName());
        fleetManagerPersonalData.setFleetManagerMobilePhone(fleetManagerEsb.getSecondaryPhone());
        fleetManagerPersonalData.setFleetManagerPhone(fleetManagerEsb.getPhonePrefix() + fleetManagerEsb.getPhone());
        fleetManagerPersonalData.setFleetManagerPrimaryEmail(fleetManagerEsb.getEmail());
        fleetManagerPersonalData.setIsImported(true);

        return fleetManagerPersonalData;
    }

    public static List<FleetManagerPersonalData> adptFromFleetManagerESBToFleetManagerPersonalData(List<FleetManagerEsb> listFleetManagerEsb){
        List<FleetManagerPersonalData> fleetManagerResponseV1List = new LinkedList<>();
        if (listFleetManagerEsb != null) {
            for (FleetManagerEsb currentFM : listFleetManagerEsb) {
                fleetManagerResponseV1List.add(adptFleetManagerEsbtoFleetManagerPersonalData(currentFM));
            }
        }
        return fleetManagerResponseV1List;
    }

    public static FleetManager adptFromFleetManagerPersonalDataToFleetManager(FleetManagerPersonalData fleetManagerPersonalData){
        if(fleetManagerPersonalData == null){
            return null;
        }
        FleetManager fleetManager = new FleetManager();
        fleetManager.setId(fleetManagerPersonalData.getFleetManagerId());
        fleetManager.setEmail(fleetManagerPersonalData.getFleetManagerPrimaryEmail());
        fleetManager.setSecondaryPhone(fleetManagerPersonalData.getFleetManagerMobilePhone());
        fleetManager.setPhone(fleetManagerPersonalData.getFleetManagerPhone());
        //Verifico se è presente nome e congnome nel FleetManagerPersonalData
        fleetManager.setFirstName(fleetManagerPersonalData.getFleetManagerName());
        fleetManager.setIsImported(fleetManagerPersonalData.getIsImported());
        fleetManager.setDisableNotifiaction(fleetManagerPersonalData.isDisableNotification());
        return fleetManager;
    }

    public static List<FleetManager> adptFromFleetManagerPersonalDataToFleetManager(List <FleetManagerPersonalData> listFleetManagerPersonalData){
        List<FleetManager> fleetManagerResponseV1List = new LinkedList<>();
        if(listFleetManagerPersonalData != null){
            for (FleetManagerPersonalData f : listFleetManagerPersonalData){
                fleetManagerResponseV1List.add(adptFromFleetManagerPersonalDataToFleetManager(f));
            }
        }
        return fleetManagerResponseV1List;
    }

    public static List<FleetManager> adptFromFleetManagerPersonalDataToFleetManager(List <FleetManagerPersonalData> listFleetManagerPersonalData, List<FleetManager> listFleetManager){

        List<FleetManager> fleetManagerResponse = adptFromFleetManagerPersonalDataToFleetManager(listFleetManagerPersonalData);

        if(listFleetManager != null){
            for(FleetManager fml : fleetManagerResponse){
                //E' necessario verificare che non ci siano già quei fleetManager tra quelli passati
                if(!listFleetManager.contains(fml)){
                    //fml.setIsImported(true);
                    listFleetManager.add(fml);
                }
            }
            return listFleetManager;
        }

        return fleetManagerResponse;
    }

    public static FleetManagerResponseV1 adptFromFleetManagerPersonalDataToFleetManagerResponseV1(FleetManagerPersonalData fleetManagerPersonalData){
        if(fleetManagerPersonalData == null){
            return null;
        }
        FleetManagerResponseV1 fleetManagerRequestV1 = new FleetManagerResponseV1();
        fleetManagerRequestV1.setId(fleetManagerPersonalData.getFleetManagerId());

        if(fleetManagerPersonalData.getFleetManagerName()!=null){
            fleetManagerRequestV1.setFirstName(fleetManagerPersonalData.getFleetManagerName());
        }

        fleetManagerRequestV1.setPhone(fleetManagerPersonalData.getFleetManagerPhone());
        fleetManagerRequestV1.setSecondaryPhone(fleetManagerPersonalData.getFleetManagerMobilePhone());

        if(!StringUtils.isEmpty(fleetManagerPersonalData.getFleetManagerSecondaryEmail())) {
            fleetManagerRequestV1.setEmail(fleetManagerPersonalData.getFleetManagerSecondaryEmail());
        }
        else {
            fleetManagerRequestV1.setEmail(fleetManagerPersonalData.getFleetManagerPrimaryEmail());
        }

        fleetManagerRequestV1.setIsImported(fleetManagerPersonalData.getIsImported());
        fleetManagerRequestV1.setDisableNotification(fleetManagerPersonalData.isDisableNotification());

        return fleetManagerRequestV1;
    }


    // NEW ADAPTER

    public static ClaimsDamagedFleetManagerEntity adptFromFMEntityJsonbToClaimsDamagedFM(FleetManager fleetManagerJsonb) {
        ClaimsDamagedFleetManagerEntity fleetManager = new ClaimsDamagedFleetManagerEntity();


        fleetManager.setId(UUID.randomUUID().toString());
        fleetManager.setFleetManagerId(fleetManagerJsonb.getId());
        fleetManager.setCustomerId(fleetManagerJsonb.getCustomerId());
        fleetManager.setEmail(fleetManagerJsonb.getEmail());
        fleetManager.setFirstName(fleetManagerJsonb.getFirstName());
        fleetManager.setLastName(fleetManagerJsonb.getLastName());
        fleetManager.setIdentification(fleetManagerJsonb.getIdentification());
        fleetManager.setMainAddress(AddressAdapter.adptAddressESBToAddress(fleetManagerJsonb.getMainAddress()));
        fleetManager.setOfficialRegistration(fleetManagerJsonb.getOfficialRegistration());
        fleetManager.setPhone(fleetManagerJsonb.getPhone());
        fleetManager.setPhonePrefix(fleetManagerJsonb.getPhonePrefix());
        fleetManager.setSecondaryPhone(fleetManagerJsonb.getSecondaryPhone());
        fleetManager.setSecondaryPhonePrefix(fleetManagerJsonb.getPhonePrefix());
        fleetManager.setSex(fleetManagerJsonb.getSex());
        fleetManager.setTitle(fleetManagerJsonb.getTitle());
        fleetManager.setDisableNotification(fleetManagerJsonb.isDisableNotification());

        return fleetManager;
    }

    public static List<ClaimsDamagedFleetManagerEntity> adptFromFMListJsonbToClaimsDamagedFMNew(List<FleetManager> fleetManagerJsonb) {

        if (fleetManagerJsonb != null) {

            List<ClaimsDamagedFleetManagerEntity> fleetManagerList = new LinkedList<>();
            for (FleetManager att : fleetManagerJsonb) {
                fleetManagerList.add(adptFromFMEntityJsonbToClaimsDamagedFM(att));
            }
            return fleetManagerList;
        }
        return null;
    }

    public static FleetManager adptFromClaimsFMEntityToFMJsonb(ClaimsDamagedFleetManagerEntity damagedFleetManagerEntity) {
        FleetManager fleetManager = new FleetManager();

        fleetManager.setCustomerId(damagedFleetManagerEntity.getCustomerId());
        fleetManager.setEmail(damagedFleetManagerEntity.getEmail());
        fleetManager.setFirstName(damagedFleetManagerEntity.getFirstName());
        fleetManager.setLastName(damagedFleetManagerEntity.getLastName());
        fleetManager.setId(damagedFleetManagerEntity.getFleetManagerId());
        fleetManager.setIdentification(damagedFleetManagerEntity.getIdentification());
        fleetManager.setMainAddress(AddressAdapter.adptAddressToAddressESB(damagedFleetManagerEntity.getMainAddress()));
        fleetManager.setOfficialRegistration(damagedFleetManagerEntity.getOfficialRegistration());
        fleetManager.setPhone(damagedFleetManagerEntity.getPhone());
        fleetManager.setPhonePrefix(damagedFleetManagerEntity.getPhonePrefix());
        fleetManager.setSecondaryPhone(damagedFleetManagerEntity.getSecondaryPhone());
        fleetManager.setSecondaryPhonePrefix(damagedFleetManagerEntity.getPhonePrefix());
        fleetManager.setSex(damagedFleetManagerEntity.getSex());
        fleetManager.setTitle(damagedFleetManagerEntity.getTitle());
        fleetManager.setDisableNotifiaction(damagedFleetManagerEntity.isDisableNotification());

        return fleetManager;
    }

    public static List<FleetManager> adptFromClaimsFMListToFMListJsonb(List<ClaimsDamagedFleetManagerEntity> damagedFleetManagerEntities) {

        if (damagedFleetManagerEntities != null) {

            List<FleetManager> fleetManagerList = new LinkedList<>();
            for (ClaimsDamagedFleetManagerEntity att : damagedFleetManagerEntities) {
                fleetManagerList.add(adptFromClaimsFMEntityToFMJsonb(att));
            }
            return fleetManagerList;
        }
        return null;
    }


}
