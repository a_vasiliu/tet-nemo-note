package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.controller.payload.request.claims.FormRequest;
import com.doing.nemo.claims.controller.payload.response.claims.FormResponse;
import com.doing.nemo.claims.controller.payload.response.external.FormsExternalResponse;
import com.doing.nemo.claims.entity.jsonb.forms.Forms;
import org.springframework.stereotype.Component;

@Component
public class FormsAdapter {

    public static Forms adptFormRequestToForms(FormRequest formRequest) {

        if (formRequest != null) {

            Forms forms = new Forms();
            forms.setAttachment(AttachmentAdapter.adptAttachmentRequestToAttachment(formRequest.getAttachment()));
            return forms;
        }
        return null;
    }

    public static FormResponse adptFromToFormsResponse(Forms forms) {

        if (forms != null) {

            FormResponse formRequest = new FormResponse();
            formRequest.setAttachment(AttachmentAdapter.adptAttachmentToAttachmentResponse(forms.getAttachment()));
            return formRequest;
        }
        return null;
    }

    public static FormsExternalResponse adptFromToFormsExternalResponse(Forms forms) {

        if (forms != null) {

            FormsExternalResponse  formRequest = new FormsExternalResponse ();
            formRequest.setAttachment(AttachmentAdapter.adptAttachmentToAttachmentExternalResponse(forms.getAttachment()));
            return formRequest;
        }
        return null;
    }


}
