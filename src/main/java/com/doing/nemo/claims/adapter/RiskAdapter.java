package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.controller.payload.request.RiskRequestV1;
import com.doing.nemo.claims.controller.payload.response.RiskResponseV1;
import com.doing.nemo.claims.entity.settings.RiskEntity;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class RiskAdapter {

    public static RiskEntity adptFromRiskRequestToRiskEntity(RiskRequestV1 riskRequest) {

        RiskEntity riskEntity = new RiskEntity();

        riskEntity.setType(riskRequest.getType());
        riskEntity.setDescription(riskRequest.getDescription());
        riskEntity.setMaximal(riskRequest.getMaximal());
        riskEntity.setExemption(riskRequest.getExemption());
        riskEntity.setOverdraft(riskRequest.getOverdraft());
        riskEntity.setActive(riskRequest.getActive());

        return riskEntity;
    }

    public static List<RiskEntity> adptFromRiskRequestToRiskEntityList(List<RiskRequestV1> riskRequestV1List) {

        List<RiskEntity> riskEntityList = new ArrayList<>();

        if (riskRequestV1List != null) {

            for (RiskRequestV1 riskRequestV1 : riskRequestV1List) {
                riskEntityList.add(adptFromRiskRequestToRiskEntity(riskRequestV1));
            }
        }
        return riskEntityList;
    }


    public static RiskResponseV1 adptRiskToRiskResponse(RiskEntity risk) {

        RiskResponseV1 riskResponse = new RiskResponseV1();

        riskResponse.setId(risk.getId());
        riskResponse.setRiskId(risk.getRiskId());
        riskResponse.setType(risk.getType());
        riskResponse.setDescription(risk.getDescription());
        riskResponse.setMaximal(risk.getMaximal());
        riskResponse.setExemption(risk.getExemption());
        riskResponse.setOverdraft(risk.getOverdraft());
        riskResponse.setActive(risk.getActive());

        return riskResponse;
    }

    public static List<RiskResponseV1> adptRiskToRiskResponse(List<RiskEntity> riskList) {

        List<RiskResponseV1> riskResponseList = new ArrayList<>();

        if (riskList != null) {

            for (RiskEntity riskRequestV1 : riskList) {
                riskResponseList.add(adptRiskToRiskResponse(riskRequestV1));
            }
        }

        return riskResponseList;
    }

}
