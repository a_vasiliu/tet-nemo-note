package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.exemption.ExemptionRequest;
import com.doing.nemo.claims.controller.payload.response.claims.ExemptionResponse;
import com.doing.nemo.claims.entity.jsonb.Exemption;
import org.springframework.stereotype.Component;

@Component
public class ExemptionAdapter {

    public static Exemption adptExemptionRequestToExemption(ExemptionRequest exemptionRequest){

        if(exemptionRequest != null){

            Exemption exemption = new Exemption();

            exemption.setCompanyReference(exemptionRequest.getCompanyReference());
            exemption.setDeductEnd1(exemptionRequest.getDeductEnd1());
            exemption.setDeductEnd2(exemptionRequest.getDeductEnd2());
            exemption.setDeductImported1(exemptionRequest.getDeductImported1());
            exemption.setDeductImported2(exemptionRequest.getDeductImported2());
            exemption.setDeductPaid1(exemptionRequest.getDeductPaid1());
            exemption.setDeductPaid2(exemptionRequest.getDeductPaid2());
            exemption.setDeductTotalPaid(exemptionRequest.getDeductTotalPaid());
            exemption.setPolicyNumber(exemptionRequest.getPolicyNumber());

            return exemption;
        }

        return null;
    }

    public static ExemptionResponse adptExemptionToExemptionResponse(Exemption exemption){

        if(exemption != null){

            ExemptionResponse exemptionResponse = new ExemptionResponse();

            exemptionResponse.setCompanyReference(exemption.getCompanyReference());
            exemptionResponse.setDeductEnd1(DateUtil.convertUTCDateToIS08601String(exemption.getDeductEnd1()));
            exemptionResponse.setDeductEnd2(DateUtil.convertUTCDateToIS08601String(exemption.getDeductEnd2()));
            exemptionResponse.setDeductImported1(DateUtil.convertUTCDateToIS08601String(exemption.getDeductImported1()));
            exemptionResponse.setDeductImported2(DateUtil.convertUTCDateToIS08601String(exemption.getDeductImported2()));
            exemptionResponse.setDeductPaid1(exemption.getDeductPaid1());
            exemptionResponse.setDeductPaid2(exemption.getDeductPaid2());
            exemptionResponse.setDeductTotalPaid(exemption.getDeductTotalPaid());
            exemptionResponse.setPolicyNumber(exemption.getPolicyNumber());

            return exemptionResponse;
        }

        return null;
    }
}
