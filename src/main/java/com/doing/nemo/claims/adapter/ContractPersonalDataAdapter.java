package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.ContractRequestV1;
import com.doing.nemo.claims.entity.settings.AntiTheftServiceEntity;
import com.doing.nemo.claims.entity.settings.ContractEntity;
import com.doing.nemo.claims.entity.settings.ContractTypeEntity;
import com.doing.nemo.claims.repository.AntiTheftServiceRepository;
import com.doing.nemo.claims.repository.ContractTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Component
public class ContractPersonalDataAdapter {

    @Autowired
    private ContractTypeRepository contractTypeRepository;

    @Autowired
    private AntiTheftServiceRepository antiTheftServiceRepository;

    public ContractEntity adptFromContractRequestToContractEntity(ContractRequestV1 contractRequestV1) {
        ContractEntity contractEntity = new ContractEntity();
        if (contractRequestV1.getContractType() != null) {
            Optional<ContractTypeEntity> contractTypeEntityOptional = contractTypeRepository.findById(contractRequestV1.getContractType().getId());
            if (contractTypeEntityOptional.isPresent())
                contractEntity.setContractType(contractTypeEntityOptional.get());
        }

        contractEntity.setContractType(contractRequestV1.getContractType());
        contractEntity.setBeginningOfValidity(DateUtil.convertIS08601StringToUTCInstant(contractRequestV1.getBeginningOfValidity()));
        contractEntity.setEndOfValidity(DateUtil.convertIS08601StringToUTCInstant(contractRequestV1.getEndOfValidity()));
        contractEntity.setPlateNumber(contractRequestV1.getPlateNumber());
        contractEntity.setDelivertyCenter(contractRequestV1.getDelivertyCenter());
        contractEntity.setExPlateNumber(contractRequestV1.getExPlateNumber());
        contractEntity.setBrand(contractRequestV1.getBrand());
        contractEntity.setModel(contractRequestV1.getModel());
        contractEntity.setHp(contractRequestV1.getHp());
        contractEntity.setKw(contractRequestV1.getKw());
        contractEntity.setWeight(contractRequestV1.getWeight());
        contractEntity.setMatriculation(DateUtil.convertIS08601StringToUTCDate(contractRequestV1.getMatriculation()));
        contractEntity.setColor(contractRequestV1.getColor());
        contractEntity.setFrame(contractRequestV1.getFrame());
        contractEntity.setJato(contractRequestV1.getJato());
        contractEntity.setEurotax(contractRequestV1.getEurotax());
        contractEntity.setKmLastCheck(contractRequestV1.getKmLastCheck());
        contractEntity.setBookRegister(contractRequestV1.getBookRegister());
        contractEntity.setBookRegisterPai(contractRequestV1.getBookRegisterPai());
        contractEntity.setInsuranceTheftLc(contractRequestV1.getInsuranceTheftLc());
        contractEntity.setInsuranceKaskoMd(contractRequestV1.getInsuranceKaskoMd());
        contractEntity.setInsuranceSearchTpl(contractRequestV1.getInsuranceSearchTpl());
        contractEntity.setInsuranceExThf(contractRequestV1.getInsuranceExThf());
        contractEntity.setInsurancePaiMax(contractRequestV1.getInsurancePaiMax());
        contractEntity.setInsuranceDeductMd(contractRequestV1.getInsuranceDeductMd());
        contractEntity.setInsurancePaiPass(contractRequestV1.getInsurancePaiPass());
        contractEntity.setInsuranceExTpl(contractRequestV1.getInsuranceExTpl());
        contractEntity.setInsuranceMaxPass(contractRequestV1.getInsuranceMaxPass());
        contractEntity.setInsurancePaiEx(contractRequestV1.getInsurancePaiEx());
        if (contractRequestV1.getAntiTheft() != null) {
            Optional<AntiTheftServiceEntity> antiTheftServiceEntityOptional = antiTheftServiceRepository.findById(contractRequestV1.getAntiTheft().getId());
            if (antiTheftServiceEntityOptional.isPresent())
                contractEntity.setAntiTheft(antiTheftServiceEntityOptional.get());
        }

        contractEntity.setVoucherId(contractRequestV1.getVoucherId());
        contractEntity.setActivation(DateUtil.convertIS08601StringToUTCInstant(contractRequestV1.getActivation()));
        contractEntity.setContractAreaBusiness(contractRequestV1.getContractAreaBusiness());
        contractEntity.setKmContract(contractRequestV1.getKmContract());
        contractEntity.setMonthsDuration(contractRequestV1.getMonthsDuration());
        contractEntity.setBeginContract(DateUtil.convertIS08601StringToUTCInstant(contractRequestV1.getBeginContract()));
        contractEntity.setEndContract(DateUtil.convertIS08601StringToUTCInstant((contractRequestV1.getEndContract())));
        contractEntity.setSuspension(DateUtil.convertIS08601StringToUTCInstant(contractRequestV1.getSuspension()));
        contractEntity.setReactivation(DateUtil.convertIS08601StringToUTCInstant(contractRequestV1.getReactivation()));
        contractEntity.setLengthened(DateUtil.convertIS08601StringToUTCInstant(contractRequestV1.getLengthened()));
        contractEntity.setCancellation(DateUtil.convertIS08601StringToUTCInstant(contractRequestV1.getCancellation()));
        contractEntity.setReturned(DateUtil.convertIS08601StringToUTCInstant(contractRequestV1.getReturned()));
        contractEntity.setLockStart(DateUtil.convertIS08601StringToUTCInstant(contractRequestV1.getLockStart()));
        contractEntity.setLockEnd(DateUtil.convertIS08601StringToUTCInstant(contractRequestV1.getLockEnd()));
        return contractEntity;
    }

    public List<ContractEntity> adptFromContractRequestToContractEntityList(List<ContractRequestV1> contractRequestV1List) {
        List<ContractEntity> contractEntityList = new LinkedList<>();
        if (contractRequestV1List != null) {
            for (ContractRequestV1 contractsRequest : contractRequestV1List) {
                contractEntityList.add(this.adptFromContractRequestToContractEntity(contractsRequest));
            }
        }
        return contractEntityList;
    }
}
