package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.controller.payload.request.driver.TestRequest;
import com.doing.nemo.claims.controller.payload.response.vehicle.TestResponse;
import com.doing.nemo.claims.entity.jsonb.Test;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

@Component
public class TestAdapter {

    public static Test adptTestRequestToTest(TestRequest testRequest) {

        Test test = new Test();
        test.setTest(testRequest.getTest());

        return test;
    }

    public static List<Test> adptTestRequestToTest(List<TestRequest> testRequest) {

        if (testRequest != null) {

            List<Test> testList = new LinkedList<>();
            for (TestRequest att : testRequest) {
                testList.add(adptTestRequestToTest(att));
            }
            return testList;
        }

        return null;
    }

    public static TestResponse adptTestToTestResponse(Test test) {

        TestResponse testResponse = new TestResponse();
        testResponse.setTest(test.getTest());

        return testResponse;
    }

    public static List<TestResponse> adptTestToTestResponse(List<Test> testList) {

        if (testList != null) {
            List<TestResponse> testResponseList = new LinkedList<>();
            for (Test att : testList) {
                testResponseList.add(adptTestToTestResponse(att));
            }

            return testResponseList;

        }
        return null;
    }

}
