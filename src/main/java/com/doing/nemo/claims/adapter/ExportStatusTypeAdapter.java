package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.controller.payload.request.ExportStatusTypeRequestV1;
import com.doing.nemo.claims.controller.payload.response.ClaimsResponsePaginationV1;
import com.doing.nemo.claims.controller.payload.response.ExportStatusTypeResponseV1;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.settings.ExportStatusTypeEntity;
import com.doing.nemo.claims.repository.ExportStatusTypeRepository;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
public class ExportStatusTypeAdapter {
    private static Logger LOGGER = LoggerFactory.getLogger(ExportStatusTypeAdapter.class);
    @Autowired
    private ExportStatusTypeRepository exportStatusTypeRepository;

    public static ExportStatusTypeEntity adptFromExportStatusTypeRequestToExportStatusTypeEntity(ExportStatusTypeRequestV1 exportStatusTypeRequestV1) {
        ExportStatusTypeEntity exportStatusTypeEntity = new ExportStatusTypeEntity();
        if (exportStatusTypeRequestV1 != null) {
            exportStatusTypeEntity.setCode(exportStatusTypeRequestV1.getCode());
            exportStatusTypeEntity.setDescription(exportStatusTypeRequestV1.getDescription());
            exportStatusTypeEntity.setStatusType(exportStatusTypeRequestV1.getStatusType());
            exportStatusTypeEntity.setFlowType(exportStatusTypeRequestV1.getFlowType());
            exportStatusTypeEntity.setActive(exportStatusTypeRequestV1.getActive());

        }
        return exportStatusTypeEntity;
    }

    public ExportStatusTypeEntity adptFromExportStatusTypeRequestToExportStatusTypeEntityWithID(ExportStatusTypeRequestV1 exportStatusTypeRequestV1, UUID uuid) {
        Optional<ExportStatusTypeEntity> exportStatusTypeEntity = exportStatusTypeRepository.findById(uuid);
        if (!exportStatusTypeEntity.isPresent()) {
            LOGGER.debug("Export claims type with id " + uuid + " not found");
            throw new NotFoundException("Export claims type with id " + uuid + " not found", com.doing.nemo.claims.validation.MessageCode.CLAIMS_1010);
        }
        ExportStatusTypeEntity exportStatusTypeEntity1 = exportStatusTypeEntity.get();
        ExportStatusTypeEntity exportStatusTypeEntity2 = adptFromExportStatusTypeRequestToExportStatusTypeEntity(exportStatusTypeRequestV1);
        exportStatusTypeEntity2.setId(exportStatusTypeEntity1.getId());
        return exportStatusTypeEntity2;
    }

    public static ExportStatusTypeResponseV1 adptFromExportStatusTypeEntityToExportStatusTypeResponse(ExportStatusTypeEntity exportStatusTypeEntity) {
        ExportStatusTypeResponseV1 exportStatusTypeResponseV1 = new ExportStatusTypeResponseV1();
        if (exportStatusTypeEntity != null) {
            exportStatusTypeResponseV1.setId(exportStatusTypeEntity.getId());
            exportStatusTypeResponseV1.setStatusType(exportStatusTypeEntity.getStatusType());
            exportStatusTypeResponseV1.setFlowType(exportStatusTypeEntity.getFlowType());
            exportStatusTypeResponseV1.setDmSystem(exportStatusTypeEntity.getDmSystem());
            exportStatusTypeResponseV1.setCode(exportStatusTypeEntity.getCode());
            exportStatusTypeResponseV1.setDescription(exportStatusTypeEntity.getDescription());
            exportStatusTypeResponseV1.setActive(exportStatusTypeEntity.getActive());
        }
        return exportStatusTypeResponseV1;
    }

    public static List<ExportStatusTypeResponseV1> adptFromExportStatusTypeEntityToExportStatusTypeResponseList(List<ExportStatusTypeEntity> exportStatusTypeEntityList) {
        List<ExportStatusTypeResponseV1> responseV1 = new LinkedList<>();
        for (ExportStatusTypeEntity exportStatusTypeEntity : exportStatusTypeEntityList) {
            responseV1.add(adptFromExportStatusTypeEntityToExportStatusTypeResponse(exportStatusTypeEntity));
        }
        return responseV1;
    }

    public static ClaimsResponsePaginationV1 adptFromExportStatusTypePaginationToNotificationResponsePagination(Pagination<ExportStatusTypeEntity> pagination) {

        ClaimsResponsePaginationV1<ExportStatusTypeResponseV1> claimsPaginationResponse = new ClaimsResponsePaginationV1<>();

        claimsPaginationResponse.setStats(pagination.getStats());
        claimsPaginationResponse.setItems(adptFromExportStatusTypeEntityToExportStatusTypeResponseList(pagination.getItems()));

        return claimsPaginationResponse;
    }
}
