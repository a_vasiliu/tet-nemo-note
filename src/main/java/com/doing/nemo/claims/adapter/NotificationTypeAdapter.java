package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.controller.payload.request.NotificationTypeRequestV1;
import com.doing.nemo.claims.controller.payload.response.ClaimsResponsePaginationV1;
import com.doing.nemo.claims.controller.payload.response.NotificationTypeResponseV1;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.settings.NotificationTypeEntity;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

@Component
public class NotificationTypeAdapter {

    public static NotificationTypeEntity adptNotificationTypeRequestToNotificationTypeEntity(NotificationTypeRequestV1 notificationTypeRequestV1) {

        NotificationTypeEntity notificationTypeEntity = new NotificationTypeEntity();

        notificationTypeEntity.setDescription(notificationTypeRequestV1.getDescription());
        notificationTypeEntity.setOrderId(notificationTypeRequestV1.getOrderId());
        notificationTypeEntity.setActive(notificationTypeRequestV1.getActive());

        return notificationTypeEntity;
    }

    public static NotificationTypeResponseV1 adptNotificationTypeEntityToNotificationTypeResponse(NotificationTypeEntity notificationTypeEntity) {

        NotificationTypeResponseV1 notificationTypeResponseV1 = new NotificationTypeResponseV1();

        notificationTypeResponseV1.setId(notificationTypeEntity.getId());
        notificationTypeResponseV1.setDescription(notificationTypeEntity.getDescription());
        notificationTypeResponseV1.setOrderId(notificationTypeEntity.getOrderId());
        notificationTypeResponseV1.setActive(notificationTypeEntity.getActive());


        return notificationTypeResponseV1;
    }

    public static List<NotificationTypeResponseV1> adptNotificationTypeEntityListToNotificationTypeResponseList(List<NotificationTypeEntity> notificationTypeEntityList) {

        List<NotificationTypeResponseV1> notificationTypeResponseV1List = new LinkedList<>();

        for (NotificationTypeEntity notificationTypeEntity : notificationTypeEntityList) {
            notificationTypeResponseV1List.add(adptNotificationTypeEntityToNotificationTypeResponse(notificationTypeEntity));
        }

        return notificationTypeResponseV1List;
    }

    public static ClaimsResponsePaginationV1 adptNotificationTypePaginationToClaimsResponsePagination(Pagination<NotificationTypeEntity> pagination) {

        ClaimsResponsePaginationV1<NotificationTypeResponseV1> claimsPaginationResponse = new ClaimsResponsePaginationV1<>();

        claimsPaginationResponse.setStats(pagination.getStats());
        claimsPaginationResponse.setItems(adptNotificationTypeEntityListToNotificationTypeResponseList(pagination.getItems()));

        return claimsPaginationResponse;
    }
}
