package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.authority.practice.ProcessingRequestV1;
import com.doing.nemo.claims.controller.payload.response.practice.ProcessingResponseV1;
import com.doing.nemo.claims.entity.jsonb.Processing;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ProcessingAdapter {
    public static Processing adptProcessingRequestToProcessing(ProcessingRequestV1 processingRequestV1){
        if(processingRequestV1 == null) return null;
        Processing processing = new Processing();
        processing.setStatus(processingRequestV1.getStatus());
        processing.setWorkingType(processingRequestV1.getWorkingType());
        processing.setDossierId(processingRequestV1.getDossierId());
        processing.setWorkingId(processingRequestV1.getWorkingId());
        processing.setDossierNumber(processingRequestV1.getDossierNumber());
        processing.setWorkingNumber(processingRequestV1.getWorkingNumber());
        processing.setCommodityDetails(CommodityDetailsAdapter.adptFromCommodityDetailsRequestToCommodityDetails(processingRequestV1.getCommodityDetails()));
        processing.setProcessingAuthorityDate(DateUtil.convertIS08601StringToUTCDate(processingRequestV1.getProcessingAuthorityDate()));
        processing.setProcessingType(processingRequestV1.getProcessingType());
        processing.setProcessingDate(DateUtil.convertIS08601StringToUTCDate(processingRequestV1.getProcessingDate()));

        return processing;
    }

    public static List<Processing>   adptProcessingRequestToProcessing(List<ProcessingRequestV1> processingRequestV1s){
        if(processingRequestV1s == null) return null;
        List<Processing> processings = new ArrayList<>();
        for(ProcessingRequestV1 processingRequestV1 : processingRequestV1s){
            processings.add(adptProcessingRequestToProcessing(processingRequestV1));
        }

        return processings;
    }


    public static ProcessingResponseV1 adptProcessingToProcessingResponse(Processing processing){
        if(processing == null) return null;
        ProcessingResponseV1 processingResponseV1 = new ProcessingResponseV1();
        processingResponseV1.setStatus(processing.getStatus());
        processingResponseV1.setWorkingType(processing.getWorkingType());
        processingResponseV1.setDossierId(processing.getDossierId());
        processingResponseV1.setWorkingId(processing.getWorkingId());
        processingResponseV1.setDossierNumber(processing.getDossierNumber());
        processingResponseV1.setWorkingNumber(processing.getWorkingNumber());
        processingResponseV1.setCommodityDetails(processing.getCommodityDetails());
        processingResponseV1.setProcessingAuthorityDate(DateUtil.convertUTCDateToIS08601String(processing.getProcessingAuthorityDate()));
        processingResponseV1.setProcessingType(processing.getProcessingType());
        processingResponseV1.setProcessingDate(DateUtil.convertUTCDateToIS08601String(processing.getProcessingDate()));

        return processingResponseV1;
    }

    public static List<ProcessingResponseV1> adptProcessingToProcessingResponse(List<Processing> processingV1s){
        if(processingV1s == null) return null;
        List<ProcessingResponseV1> processings = new ArrayList<>();
        for(Processing processing : processingV1s){
            processings.add(adptProcessingToProcessingResponse(processing));
        }

        return processings;
    }
}
