package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.dto.KafkaEventDTO;
import com.doing.nemo.claims.dto.KafkaEventExportClaimsDTO;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.jsonb.damaged.Damaged;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.Contract;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbVehicle.Vehicle;
import org.springframework.stereotype.Component;

@Component
public class KafkaAdapter {

    private KafkaAdapter() {
    }

    public static KafkaEventDTO adptFromClaimsToKafkaEvent(ClaimsEntity claimsEntity){
        if(claimsEntity == null){
            return null;
        }
        KafkaEventDTO kafkaEventDTO = new KafkaEventDTO();
        kafkaEventDTO.setPractice_id(claimsEntity.getPracticeId());
        Damaged damaged = claimsEntity.getDamaged();
        if(damaged != null){
            Contract contract = damaged.getContract();
            Vehicle vehicle = damaged.getVehicle();
            if(contract != null){
                kafkaEventDTO.setContract_id(contract.getContractId());
                kafkaEventDTO.setContract_status(contract.getStatus());
                kafkaEventDTO.setContract_type("CH" + contract.getContractType());
            }
            if(vehicle != null){
                kafkaEventDTO.setChassis(vehicle.getChassisNumber());
                kafkaEventDTO.setPlate(vehicle.getLicensePlate());
                if(vehicle.getRegistrationDate() != null){
                    kafkaEventDTO.setPlate_registration(DateUtil.convertUTCDateToIS08601String(vehicle.getRegistrationDate()));
                }
            }
        }

        return kafkaEventDTO;
    }

    public static KafkaEventExportClaimsDTO adptFromExportPropertiesToKafkaEventExportClaimsDTO(String part, String processId, String content, String userEmail, String headers, String templateName){
        KafkaEventExportClaimsDTO kafkaEventExportClaimsDTO = new KafkaEventExportClaimsDTO();

        kafkaEventExportClaimsDTO.setPart(part);
        kafkaEventExportClaimsDTO.setProcessId(processId);
        kafkaEventExportClaimsDTO.setContent(content);
        kafkaEventExportClaimsDTO.setUserEmail(userEmail);
        kafkaEventExportClaimsDTO.setHeaders(headers);
        kafkaEventExportClaimsDTO.setTemplateName(templateName);

        return kafkaEventExportClaimsDTO;
    }

}







