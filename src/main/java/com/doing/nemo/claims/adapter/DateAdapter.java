package com.doing.nemo.claims.adapter;

import org.apache.tomcat.jni.Local;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

@Component
public class DateAdapter {

    public static LocalDateTime adptDateToLocalDataTime(Date date){
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
    }

    public static Date adptLocalDateTimeToDate(LocalDateTime date){
        return java.util.Date.from(date.atZone(ZoneId.systemDefault()).toInstant());
    }

    public static Instant adaptDateZone(Instant instant, String timeZone){
        if(instant==null){
            return null;
        }
        if(timeZone==null){
            return instant;
        }
        return instant.atZone(ZoneId.of(timeZone)).toLocalDateTime().atZone(ZoneId.systemDefault()).toInstant();
    }

    public static Date adaptDateZone(Date date, String timeZone){
        if(date==null){
            return null;
        }
        return Date.from(adaptDateZone(date.toInstant(), timeZone));
    }

}
