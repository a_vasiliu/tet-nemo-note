package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.controller.payload.request.practice.SeizureRequestV1;
import com.doing.nemo.claims.controller.payload.response.practice.SeizureResponseV1;
import com.doing.nemo.claims.entity.jsonb.practice.Seizure;
import org.springframework.stereotype.Component;

@Component
public class SeizureAdapter {

    public static Seizure adptFromSeizureRequestToSeizure(SeizureRequestV1 seizureRequest) {
        if(seizureRequest == null)
            return null;
        Seizure seizure = new Seizure();
        seizure.setMisappropriationDate(seizureRequest.getMisappropriationDate());
        seizure.setReturnGoodFaith(seizureRequest.getReturnGoodFaith());
        return seizure;
    }


    public static SeizureResponseV1 adptFromSeizureToSeizureResponse(Seizure seizure) {
        if(seizure == null)
            return null;
        SeizureResponseV1 seizureResponse = new SeizureResponseV1();
        seizureResponse.setMisappropriationDate(seizure.getMisappropriationDate());
        seizureResponse.setReturnGoodFaith(seizure.getReturnGoodFaith());
        return seizureResponse;
    }

}