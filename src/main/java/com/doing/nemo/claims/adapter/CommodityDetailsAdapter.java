package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.controller.payload.request.authority.claims.CommodityDetailsRequestV1;
import com.doing.nemo.claims.controller.payload.response.authority.claims.CommodityDetailsResponseV1;
import com.doing.nemo.claims.entity.jsonb.CommodityDetails;
import org.springframework.stereotype.Component;

@Component
public class CommodityDetailsAdapter {
    public static CommodityDetails adptFromCommodityDetailsRequestToCommodityDetails(CommodityDetailsRequestV1 commodityDetailsRequestV1){
        if(commodityDetailsRequestV1 == null) return null;
        CommodityDetails commodityDetails = new CommodityDetails();
        commodityDetails.setCommodityId(commodityDetailsRequestV1.getCommodityId());
        commodityDetails.setAddress(commodityDetailsRequestV1.getAddress());
        commodityDetails.setAddressNumber(commodityDetailsRequestV1.getAddressNumber());
        commodityDetails.setEmail(commodityDetailsRequestV1.getEmail());
        commodityDetails.setFax(commodityDetailsRequestV1.getFax());
        commodityDetails.setLocation(commodityDetailsRequestV1.getLocation());
        commodityDetails.setName(commodityDetailsRequestV1.getName());
        commodityDetails.setZipCode(commodityDetailsRequestV1.getZipCode());
        commodityDetails.setPhone(commodityDetailsRequestV1.getPhone());
        commodityDetails.setProvinceCode(commodityDetailsRequestV1.getProvinceCode());

        return commodityDetails;
    }

    public static CommodityDetailsResponseV1 adptFromCommodityDetailsToCommodityDetailsResponse(CommodityDetails commodityDetails){
        if(commodityDetails == null) return null;
        CommodityDetailsResponseV1 commodityDetailsResponseV1 = new CommodityDetailsResponseV1();
        commodityDetailsResponseV1.setCommodityId(commodityDetails.getCommodityId());
        commodityDetailsResponseV1.setAddress(commodityDetails.getAddress());
        commodityDetailsResponseV1.setAddressNumber(commodityDetails.getAddressNumber());
        commodityDetailsResponseV1.setEmail(commodityDetails.getEmail());
        commodityDetailsResponseV1.setFax(commodityDetails.getFax());
        commodityDetailsResponseV1.setLocation(commodityDetails.getLocation());
        commodityDetailsResponseV1.setName(commodityDetails.getName());
        commodityDetailsResponseV1.setPhone(commodityDetails.getPhone());
        commodityDetailsResponseV1.setZipCode(commodityDetails.getZipCode());
        commodityDetailsResponseV1.setProvinceCode(commodityDetails.getProvinceCode());

        return commodityDetailsResponseV1;
    }
}
