package com.doing.nemo.claims.adapter;

import org.springframework.stereotype.Component;

@Component
public class ImpactPointDetectionImpactPointAdapter {

    public static String adptFromImPointDetectionImpactPointEntityToJsonKeyCar(String value) {
        if (value.equalsIgnoreCase("front"))
            return "car_central_1";
        if (value.equalsIgnoreCase("front_r"))
            return "car_right_1";
        if (value.equalsIgnoreCase("front_l"))
            return "car_left_1";
        if (value.equalsIgnoreCase("windshield"))
            return "car_central_2";
        if (value.equalsIgnoreCase("rear"))
            return "car_central_5";
        if (value.equalsIgnoreCase("rear_r"))
            return "car_right_4";
        if (value.equalsIgnoreCase("rear_l"))
            return "car_left_4";
        if (value.equalsIgnoreCase("rear_window"))
            return "car_central_4";
        if (value.equalsIgnoreCase("roof"))
            return "car_central_3";
        if (value.equalsIgnoreCase("side_front_l"))
            return "car_left_2";
        if (value.equalsIgnoreCase("side_rear_l"))
            return "car_left_3";
        if (value.equalsIgnoreCase("side_front_r"))
            return "car_right_2";
        if (value.equalsIgnoreCase("side_rear_r"))
            return "car_right_3";

        return "";
    }

    public static String adptFromImPointDetectionImpactPointEntityToJsonKeyMoto(String value) {
        if (value.equalsIgnoreCase("front")) {
            return "moto_central_1";
        } else if (value.equalsIgnoreCase("rear")) {
            return "moto_central_2";
        } else if (value.equalsIgnoreCase("side_front_l")) {
            return "moto_left_1";
        } else if (value.equalsIgnoreCase("side_front_r")) {
            return "moto_right_1";
        } else {
            return "";
        }
    }

    public static String adptFromImPointDetectionImpactPointEntityToJsonKeyVan(String value) {
        if (value.equalsIgnoreCase("front"))
            return "van_central_1";
        if (value.equalsIgnoreCase("front_r"))
            return "van_right_1";
        if (value.equalsIgnoreCase("front_l"))
            return "van_left_1";
        if (value.equalsIgnoreCase("windshield"))
            return "van_central_2";
        if (value.equalsIgnoreCase("rear"))
            return "van_central_4";
        if (value.equalsIgnoreCase("rear_r"))
            return "van_right_4";
        if (value.equalsIgnoreCase("rear_l"))
            return "van_left_4";
        if (value.equalsIgnoreCase("roof"))
            return "van_central_3";
        if (value.equalsIgnoreCase("side_front_l"))
            return "van_left_2";
        if (value.equalsIgnoreCase("side_rear_l"))
            return "van_left_3";
        if (value.equalsIgnoreCase("side_front_r"))
            return "van_right_2";
        if (value.equalsIgnoreCase("side_rear_r"))
            return "van_right_3";

        return "";
    }
}
