package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.counterparty.LastContactRequest;
import com.doing.nemo.claims.controller.payload.response.counterparty.LastContactResponse;
import com.doing.nemo.claims.entity.jsonb.repair.LastContact;
import com.doing.nemo.claims.service.MotivationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

@Component
public class LastContactAdapter {

    @Autowired
    private MotivationService motivationService;

    public LastContact adptFromLastContactRequestToLastContact(LastContactRequest lastContactRequest)
    {
        LastContact lastContact = null;
        if(lastContactRequest != null){
            lastContact = new LastContact();
            lastContact.setLastCall(DateUtil.convertIS08601StringToUTCDate(lastContactRequest.getLastCall()));
            lastContact.setNote(lastContactRequest.getNote());
            lastContact.setRecall(DateUtil.convertIS08601StringToUTCDate(lastContactRequest.getRecall()));
            lastContact.setResult(lastContactRequest.getResult());
            if(lastContactRequest.getMotivation() != null)
                lastContact.setMotivation(motivationService.selectMotivation(lastContactRequest.getMotivation().getId()));
        }
        return lastContact;
    }

    public static LastContactResponse adptFromLastContactToLastContactResponse(LastContact lastContact)
    {
        LastContactResponse lastContactResponse = null;
        if(lastContact != null){
            lastContactResponse = new LastContactResponse();
            lastContactResponse.setLastCall(DateUtil.convertUTCDateToIS08601String(lastContact.getLastCall()));
            lastContactResponse.setNote(lastContact.getNote());
            lastContactResponse.setRecall(DateUtil.convertUTCDateToIS08601String(lastContact.getRecall()));
            lastContactResponse.setResult(lastContact.getResult());
            if(lastContact.getMotivation() != null)
                lastContactResponse.setMotivation(MotivationAdapter.adptFromMotivationEntityToMotivationResponseV1(lastContact.getMotivation()));
        }
        return lastContactResponse;
    }



    public List<LastContact> adtpFromLastContactListRequestToLastContactListEntity (List<LastContactRequest> lastContactRequestList){
        if(lastContactRequestList == null){
            return null;
        }

        List<LastContact> lastContactList = new LinkedList<>();
        for(LastContactRequest currentLastContact : lastContactRequestList){
            lastContactList.add(this.adptFromLastContactRequestToLastContact(currentLastContact));
        }

        return lastContactList;
    }

    public static List<LastContactResponse> adtpFromLastContactListEntityToLastContactListResponse(List<LastContact> lastContactEntityList){
        if(lastContactEntityList == null){
            return null;
        }

        List<LastContactResponse> lastContactList = new LinkedList<>();
        for(LastContact currentLastContact: lastContactEntityList){
            lastContactList.add(adptFromLastContactToLastContactResponse(currentLastContact));
        }

        return lastContactList;
    }
}
