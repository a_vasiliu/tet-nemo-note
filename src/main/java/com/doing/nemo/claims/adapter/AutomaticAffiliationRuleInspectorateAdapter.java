package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.common.validation.MessageCode;
import com.doing.nemo.claims.common.validation.RequestValidator;
import com.doing.nemo.claims.controller.payload.request.AutomaticAffiliationRuleInspectorateRequestV1;
import com.doing.nemo.claims.controller.payload.request.ClaimsTypeForRuleRequestV1;
import com.doing.nemo.claims.controller.payload.request.InsuranceCompanyRequestV1;
import com.doing.nemo.claims.entity.settings.AutomaticAffiliationRuleInspectorateEntity;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

@Component
public class AutomaticAffiliationRuleInspectorateAdapter {

    private static Logger LOGGER = LoggerFactory.getLogger(AutomaticAffiliationRuleInspectorateAdapter.class);

    @Autowired
    private RequestValidator requestValidator;

    public AutomaticAffiliationRuleInspectorateEntity adptFromAutomaticAffiliationRuleInspectorateRequestToAutomaticAffiliationRuleInspectorateEntity(AutomaticAffiliationRuleInspectorateRequestV1 automaticAffiliationRuleInspectorateRequestV1, HttpMethod httpMethod) {
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/yyyy");
        String dateString = simpleDateFormat.format(date);

        AutomaticAffiliationRuleInspectorateEntity automaticAffiliationRuleInspectorateEntity = new AutomaticAffiliationRuleInspectorateEntity();
        automaticAffiliationRuleInspectorateEntity.setSelectionName(automaticAffiliationRuleInspectorateRequestV1.getSelectionName());
        automaticAffiliationRuleInspectorateEntity.setMonthlyVolume(automaticAffiliationRuleInspectorateRequestV1.getMonthlyVolume());
        automaticAffiliationRuleInspectorateEntity.setCurrentMonth(dateString);

        if (httpMethod.equals(HttpMethod.POST)) automaticAffiliationRuleInspectorateEntity.setCurrentVolume(0);
        else
            automaticAffiliationRuleInspectorateEntity.setCurrentVolume(automaticAffiliationRuleInspectorateRequestV1.getCurrentVolume());
        automaticAffiliationRuleInspectorateEntity.setDetail(automaticAffiliationRuleInspectorateRequestV1.getDetail());
        automaticAffiliationRuleInspectorateEntity.setAnnotations(automaticAffiliationRuleInspectorateRequestV1.getAnnotations());
        automaticAffiliationRuleInspectorateEntity.setSelectOnlyComplaintsWithoutCTP(automaticAffiliationRuleInspectorateRequestV1.getSelectOnlyComplaintsWithoutCTP());
        automaticAffiliationRuleInspectorateEntity.setForeignCountryClaim(automaticAffiliationRuleInspectorateRequestV1.getForeignCountryClaim());
        automaticAffiliationRuleInspectorateEntity.setCounterPartType(automaticAffiliationRuleInspectorateRequestV1.getCounterPartType());
        automaticAffiliationRuleInspectorateEntity.setClaimWithInjured(automaticAffiliationRuleInspectorateRequestV1.getClaimWithInjured());
        automaticAffiliationRuleInspectorateEntity.setDamageCanNotBeRepaired(automaticAffiliationRuleInspectorateRequestV1.getDamageCanNotBeRepaired());
        automaticAffiliationRuleInspectorateEntity.setMinImport(automaticAffiliationRuleInspectorateRequestV1.getMinImport());
        automaticAffiliationRuleInspectorateEntity.setMaxImport(automaticAffiliationRuleInspectorateRequestV1.getMaxImport());


        if (automaticAffiliationRuleInspectorateRequestV1.getClaimsTypeEntityList() != null) {
            for (ClaimsTypeForRuleRequestV1 claimRequest : automaticAffiliationRuleInspectorateRequestV1.getClaimsTypeEntityList())
                requestValidator.validateRequest(claimRequest, MessageCode.E00X_1000);
            automaticAffiliationRuleInspectorateEntity.setClaimsTypeEntityList(ClaimsTypeAdapter.adptClaimsTyreRequestToClaimsType(automaticAffiliationRuleInspectorateRequestV1.getClaimsTypeEntityList()));
        }

        if (automaticAffiliationRuleInspectorateRequestV1.getDamagedInsuranceCompanyList() != null) {
            for (InsuranceCompanyRequestV1 insuranceCompanyRequest : automaticAffiliationRuleInspectorateRequestV1.getDamagedInsuranceCompanyList())
                requestValidator.validateRequest(insuranceCompanyRequest, MessageCode.E00X_1000);
            automaticAffiliationRuleInspectorateEntity.setDamagedInsuranceCompanyList(InsuranceCompanyAdapter.adptFromICompRequestToICompEntityList(automaticAffiliationRuleInspectorateRequestV1.getDamagedInsuranceCompanyList()));
        }
        if (automaticAffiliationRuleInspectorateRequestV1.getCounterParterInsuranceCompanyList() != null) {
            for (InsuranceCompanyRequestV1 insuranceCompanyRequest : automaticAffiliationRuleInspectorateRequestV1.getCounterParterInsuranceCompanyList())
                requestValidator.validateRequest(insuranceCompanyRequest, MessageCode.E00X_1000);
            automaticAffiliationRuleInspectorateEntity.setCounterParterInsuranceCompanyList(InsuranceCompanyAdapter.adptFromICompRequestToICompEntityList(automaticAffiliationRuleInspectorateRequestV1.getCounterParterInsuranceCompanyList()));
        }
        return automaticAffiliationRuleInspectorateEntity;
    }

    /*public List<AutomaticAffiliationRuleInspectorateEntity> adptFromAutomaticAffiliationRuleInspectorateRequestToAutomaticAffiliationRuleInspectorateEntityList(AutomaticAffiliationRuleInspectorateListRequestV1 automaticAffiliationRuleInspectorateRequestV1List) {
        List<AutomaticAffiliationRuleInspectorateEntity> automaticAffiliationRuleInspectorateEntityList = new LinkedList<>();
        for (AutomaticAffiliationRuleInspectorateRequestV1 automaticAffiliationRuleInspectoratesRequest : automaticAffiliationRuleInspectorateRequestV1List.getData()) {
            automaticAffiliationRuleInspectorateEntityList.add(this.adptFromAutomaticAffiliationRuleInspectorateRequestToAutomaticAffiliationRuleInspectorateEntity(automaticAffiliationRuleInspectoratesRequest));
        }
        return automaticAffiliationRuleInspectorateEntityList;
    }*/

    public List<AutomaticAffiliationRuleInspectorateEntity> adptFromAutomaticAffiliationRuleInspectorateRequestToAutomaticAffiliationRuleInspectorateEntityList(List<AutomaticAffiliationRuleInspectorateRequestV1> automaticAffiliationRuleInspectorateRequestV1List, HttpMethod httpMethod) {
        List<AutomaticAffiliationRuleInspectorateEntity> automaticAffiliationRuleInspectorateEntityList = new LinkedList<>();
        if (automaticAffiliationRuleInspectorateRequestV1List != null) {
            for (AutomaticAffiliationRuleInspectorateRequestV1 automaticAffiliationRuleInspectoratesRequest : automaticAffiliationRuleInspectorateRequestV1List) {
                if (automaticAffiliationRuleInspectoratesRequest.getCurrentVolume() == null || automaticAffiliationRuleInspectoratesRequest.getMonthlyVolume() == null) {
                    LOGGER.debug(com.doing.nemo.claims.validation.MessageCode.CLAIMS_1010.value());
                    throw new BadRequestException("Current volume and Monthly volume must not be null", com.doing.nemo.claims.validation.MessageCode.CLAIMS_1010);
                }
                //if (httpMethod.equals(HttpMethod.PUT) && automaticAffiliationRuleInspectoratesRequest.getCurrentVolume() > automaticAffiliationRuleInspectoratesRequest.getMonthlyVolume()) throw new BadRequestException("Current volume must be smaller than Monthly volume",com.doing.nemo.claims.validation.MessageCode.CLAIMS_1010);
                automaticAffiliationRuleInspectorateEntityList.add(this.adptFromAutomaticAffiliationRuleInspectorateRequestToAutomaticAffiliationRuleInspectorateEntity(automaticAffiliationRuleInspectoratesRequest, httpMethod));
            }
        }
        return automaticAffiliationRuleInspectorateEntityList;
    }
}
