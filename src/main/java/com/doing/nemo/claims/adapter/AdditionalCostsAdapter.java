package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.controller.payload.request.damaged.AdditionalCostsRequest;
import com.doing.nemo.claims.controller.payload.response.damaged.AdditionalCostsResponse;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.AdditionalCosts;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

@Component
public class AdditionalCostsAdapter {

    public static AdditionalCosts adptAdditionalCostsRequestToAdditionalCosts(AdditionalCostsRequest additionalCostsRequest) {

        AdditionalCosts additionalCosts = new AdditionalCosts();
        additionalCosts.setAmount(additionalCostsRequest.getAmount());
        additionalCosts.setDate(additionalCostsRequest.getDate());
        additionalCosts.setTypology(additionalCostsRequest.getTypology());
        additionalCosts.setAttachments(additionalCostsRequest.getAttachments());

        return additionalCosts;
    }

    public static List<AdditionalCosts> adptAdditionalCostsRequestToAdditionalCosts(List<AdditionalCostsRequest> additionalCostsRequestList) {

        if (additionalCostsRequestList != null) {

            List<AdditionalCosts> additionalCostsList = new LinkedList<>();
            for (AdditionalCostsRequest att : additionalCostsRequestList) {
                additionalCostsList.add(adptAdditionalCostsRequestToAdditionalCosts(att));
            }

            return additionalCostsList;
        }
        return null;
    }

    public static AdditionalCostsResponse adptAdditionalCostsToAdditionalCostsResponse(AdditionalCosts additionalCosts) {

        AdditionalCostsResponse additionalCostsResponse = new AdditionalCostsResponse();
        additionalCostsResponse.setAmount(additionalCosts.getAmount());
        additionalCostsResponse.setAttachments(additionalCosts.getAttachments());
        additionalCostsResponse.setDate(additionalCosts.getDate());
        additionalCostsResponse.setTypology(additionalCosts.getTypology());

        return additionalCostsResponse;
    }

    public static List<AdditionalCostsResponse> adptAdditionalCostsToAdditionalCostsResponse(List<AdditionalCosts> additionalCostsList) {

        if (additionalCostsList != null) {
            List<AdditionalCostsResponse> additionalCostsResponseList = new LinkedList<>();
            for (AdditionalCosts att : additionalCostsList) {
                additionalCostsResponseList.add(adptAdditionalCostsToAdditionalCostsResponse(att));
            }

            return additionalCostsResponseList;
        }
        return null;
    }
}
