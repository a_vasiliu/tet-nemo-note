package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.controller.payload.request.claims.WoundedRequest;
import com.doing.nemo.claims.controller.payload.response.GoLiveMSAResponseV1;
import com.doing.nemo.claims.controller.payload.response.claims.WoundedResponse;
import com.doing.nemo.claims.entity.enumerated.DamagedEnum.WoundedEnum.WoundedTypeEnum;
import com.doing.nemo.claims.entity.enumerated.DamagedEnum.WoundedEnum.WoundedWoundEnum;
import com.doing.nemo.claims.entity.enumerated.GoLiveSystemEnum;
import com.doing.nemo.claims.entity.jsonb.Wounded;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

@Component
public class GoLiveMSAAdapter {

    public static GoLiveMSAResponseV1 adptFromGoLiveStrategyBooleanToGoLiveStrategyResponse(Boolean isNemo) {
        GoLiveMSAResponseV1 responseV1 = null;
        if(isNemo){
            responseV1 = new GoLiveMSAResponseV1(GoLiveSystemEnum.NEMO);
        } else {
            responseV1 = new GoLiveMSAResponseV1(GoLiveSystemEnum.WEBSIN);
        }
        return responseV1;
    }

}
