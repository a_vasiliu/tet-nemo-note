package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.controller.payload.request.claims.WoundedRequest;
import com.doing.nemo.claims.controller.payload.response.claims.WoundedResponse;
import com.doing.nemo.claims.entity.enumerated.DamagedEnum.WoundedEnum.WoundedTypeEnum;
import com.doing.nemo.claims.entity.enumerated.DamagedEnum.WoundedEnum.WoundedWoundEnum;
import com.doing.nemo.claims.entity.jsonb.Wounded;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

@Component
public class WoundedAdapter {

    public static Wounded adptWoundedRequestToWounded(WoundedRequest woundedRequest) {

        Wounded wounded = new Wounded();
        wounded.setAddress(woundedRequest.getAddress());
        wounded.setEmergencyRoom(woundedRequest.getEmergencyRoom());
        wounded.setFirstname(woundedRequest.getFirstname());
        wounded.setLastname(woundedRequest.getLastname());
        wounded.setType(woundedRequest.getType());
        wounded.setWound(woundedRequest.getWound());
        wounded.setAttachmentList(woundedRequest.getAttachmentList());
        wounded.setPrivacyAccepted(woundedRequest.getPrivacyAccepted());

        return wounded;
    }

    public static List<Wounded> adptWoundedRequestToWounded(List<WoundedRequest> woundedRequestList) {

        if (woundedRequestList != null) {

            List<Wounded> woundedList = new LinkedList<>();
            for (WoundedRequest att : woundedRequestList) {
                woundedList.add(adptWoundedRequestToWounded(att));
            }
            return woundedList;
        }
        return null;
    }

    public static WoundedResponse adptWoundedToWoundedResponse(Wounded wounded) {

        WoundedResponse woundedResponse = new WoundedResponse();
        woundedResponse.setAddress(wounded.getAddress());
        woundedResponse.setEmergencyRoom(wounded.getEmergencyRoom());
        woundedResponse.setFirstname(wounded.getFirstname());
        woundedResponse.setLastname(wounded.getLastname());
        woundedResponse.setType(wounded.getType());
        woundedResponse.setWound(wounded.getWound());
        woundedResponse.setAttachmentList(wounded.getAttachmentList());
        woundedResponse.setPrivacyAccepted(wounded.getPrivacyAccepted());

        return woundedResponse;
    }

    public static List<WoundedResponse> adptWoundedToWoundedResponse(List<Wounded> woundedList) {

        if (woundedList != null) {

            List<WoundedResponse> woundedListResponse = new LinkedList<>();
            for (Wounded att : woundedList) {
                woundedListResponse.add(adptWoundedToWoundedResponse(att));
            }
            return woundedListResponse;
        }
        return null;
    }

    public static String adptFromWoundedTypeEnumToItalian(WoundedTypeEnum woundedTypeEnum){
        if(woundedTypeEnum.getValue().equalsIgnoreCase("DRIVER")){
            return "Conducente";
        }
        if(woundedTypeEnum.getValue().equalsIgnoreCase("PASSENGER")){
            return "Passeggero";
        }
        return "Altro";
    }

    public static String adptFromWoundedWoundEnumToItalian(WoundedWoundEnum woundedWoundEnum){
        if(woundedWoundEnum.getValue().equalsIgnoreCase("MINOR")){
            return "Minori";
        }
        if(woundedWoundEnum.getValue().equalsIgnoreCase("SERIOUS")){
            return "Gravi";
        }
        return "No";
    }
}
