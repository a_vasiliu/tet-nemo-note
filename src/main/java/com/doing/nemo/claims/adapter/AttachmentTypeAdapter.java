package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.controller.payload.request.AttachmentTypeRequestV1;
import com.doing.nemo.claims.controller.payload.response.AttachmentTypeResponseV1;
import com.doing.nemo.claims.controller.payload.response.PaginationResponseV1;
import com.doing.nemo.claims.entity.enumerated.DTO.PageStats;
import com.doing.nemo.claims.entity.settings.AttachmentTypeEntity;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

@Component
public class AttachmentTypeAdapter {

    public static AttachmentTypeEntity getAttachmentTypeEntity(AttachmentTypeRequestV1 attachmentTypeRequestV1) {

        AttachmentTypeEntity attachmentTypeEntity = new AttachmentTypeEntity();

        attachmentTypeEntity.setName(attachmentTypeRequestV1.getName());
        attachmentTypeEntity.setGroups(attachmentTypeRequestV1.getGroups());
        attachmentTypeEntity.setActive(attachmentTypeRequestV1.getActive());


        return attachmentTypeEntity;
    }

    public static AttachmentTypeResponseV1 getAttachmentTypeAdapter(AttachmentTypeEntity attachmentTypeEntity) {

        if(attachmentTypeEntity != null) {

            AttachmentTypeResponseV1 attachmentTypeResponseV1 = new AttachmentTypeResponseV1();

            attachmentTypeResponseV1.setId(attachmentTypeEntity.getId());
            attachmentTypeResponseV1.setName(attachmentTypeEntity.getName());
            attachmentTypeResponseV1.setGroups(attachmentTypeEntity.getGroups());
            attachmentTypeResponseV1.setActive(attachmentTypeEntity.getActive());
            attachmentTypeResponseV1.setTypeComplaint(attachmentTypeEntity.getTypeComplaint());


            return attachmentTypeResponseV1;
        }
        return null;
    }

    public static List<AttachmentTypeResponseV1> getAttachmentTypeListAdapter(List<AttachmentTypeEntity> attachmentTypeEntityList) {

        List<AttachmentTypeResponseV1> attachmentTypeResponseV1List = new LinkedList<>();

        for (AttachmentTypeEntity attachmentTypeEntity : attachmentTypeEntityList) {
            attachmentTypeResponseV1List.add(getAttachmentTypeAdapter(attachmentTypeEntity));
        }

        return attachmentTypeResponseV1List;
    }

    public static PaginationResponseV1<AttachmentTypeResponseV1> adptPagination(List<AttachmentTypeEntity> attachmentTypeEntityList, int page, int pageSize, Long itemCount) {

        double totPages = Math.ceil((double) itemCount.intValue() / pageSize);
        PageStats pageStats = new PageStats();
        pageStats.setCurrentPage(page);
        pageStats.setItemCount(itemCount.intValue());
        pageStats.setPageSize(pageSize);
        pageStats.setPageCount((long)totPages);

        PaginationResponseV1<AttachmentTypeResponseV1> paginationResponseV1 = new PaginationResponseV1(pageStats,getAttachmentTypeListAdapter(attachmentTypeEntityList));

        return paginationResponseV1;
    }
}
