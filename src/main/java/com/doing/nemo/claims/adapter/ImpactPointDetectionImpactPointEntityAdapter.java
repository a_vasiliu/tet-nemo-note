package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.entity.enumerated.DamagedEnum.VehicleEnum.VehicleNatureEnum;
import org.springframework.stereotype.Component;

@Component

public class ImpactPointDetectionImpactPointEntityAdapter {

    public static String adptFromEnglishToItalian(String damage, VehicleNatureEnum vehicleNature) {

        if (vehicleNature.equals(VehicleNatureEnum.MOTOR_VEHICLE) || vehicleNature.equals(VehicleNatureEnum.VAN)){
            if (damage.equalsIgnoreCase("front"))
                return "PARAURTI ANTERIORE";
            else if (damage.equalsIgnoreCase("front_r"))
                return "PARAURTI ANTERIORE DX";
            else if (damage.equalsIgnoreCase("front_l"))
                return "PARAURTI ANTERIORE SX";
            else if (damage.equalsIgnoreCase("windshield"))
                return "PARABREZZA";
            else if (damage.equalsIgnoreCase("rear"))
                return "PARAURTI POSTERIORE";
            else if (damage.equalsIgnoreCase("rear_r"))
                return "PARAURTI POSTERIORE DX";
            else if (damage.equalsIgnoreCase("rear_l"))
                return "PARAURTI POSTERIORE SX";
            else if (damage.equalsIgnoreCase("rear_window"))
                return "LUNOTTO";
            else if (damage.equalsIgnoreCase("roof"))
                return "TETTO";
            else if (damage.equalsIgnoreCase("side_front_l"))
                return "LATO ANTERIORE SX";
            else if (damage.equalsIgnoreCase("side_rear_l"))
                return "LATO POSTERIORE SX";
            else if (damage.equalsIgnoreCase("side_front_r"))
                return "LATO ANTERIORE DX";
            else if (damage.equalsIgnoreCase("side_rear_r"))
                return "LATO POSTERIORE DX";
        }else if(vehicleNature.equals(VehicleNatureEnum.MOTOR_CYCLE)){
            if( damage.equalsIgnoreCase("front"))
                return "FRONTALE";
            else if(damage.equalsIgnoreCase("rear"))
                return "POSTERIORE";
            else if(damage.equalsIgnoreCase("side_front_r"))
                return "LATERALE DESTRA";
            else if(damage.equalsIgnoreCase("side_front_L"))
                return "LATERALE SINISTRA";
        }

        return null;
    }
}
