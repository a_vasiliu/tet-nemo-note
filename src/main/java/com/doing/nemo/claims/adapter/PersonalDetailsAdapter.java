package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.controller.payload.request.PersonalDetailsRepairRequestV1;
import com.doing.nemo.claims.controller.payload.response.PersonalDetailsRepairResponseV1;
import com.doing.nemo.claims.dto.PersonalDetailsRepair;
import org.springframework.stereotype.Component;

@Component
public class PersonalDetailsAdapter {
    public static PersonalDetailsRepair adptFromPersonalDetailsRepairRequestToPersonalDetailsRepair(PersonalDetailsRepairRequestV1 personalDetailsRepairRequestV1){
        if(personalDetailsRepairRequestV1 == null) return null;
        PersonalDetailsRepair personalDetailsRepair = new PersonalDetailsRepair();
        personalDetailsRepair.setBirthCountry(personalDetailsRepairRequestV1.getBirthCountry());
        personalDetailsRepair.setBirthDate(personalDetailsRepairRequestV1.getBirthDate());
        personalDetailsRepair.setBirthPlace(personalDetailsRepairRequestV1.getBirthPlace());
        personalDetailsRepair.setBirthProvince(personalDetailsRepairRequestV1.getBirthProvince());
        personalDetailsRepair.setFirstname(personalDetailsRepairRequestV1.getFirstname());
        personalDetailsRepair.setGender(personalDetailsRepairRequestV1.getGender());
        personalDetailsRepair.setLastname(personalDetailsRepairRequestV1.getLastname());
        personalDetailsRepair.setOfficeRole(personalDetailsRepairRequestV1.getOfficeRole());
        personalDetailsRepair.setPhone(personalDetailsRepairRequestV1.getPhone());
        personalDetailsRepair.setTaxCode(personalDetailsRepairRequestV1.getTaxCode());

        return personalDetailsRepair;
    }

    public static PersonalDetailsRepairResponseV1 adptFromPersonalDetailsRepairToPersonalDetailsResponse(PersonalDetailsRepair personalDetailsRepair){
        if(personalDetailsRepair == null) return null;
        PersonalDetailsRepairResponseV1 personalDetailsRepairResponseV1 = new PersonalDetailsRepairResponseV1();
        personalDetailsRepairResponseV1.setBirthCountry(personalDetailsRepair.getBirthCountry());
        personalDetailsRepairResponseV1.setBirthDate(personalDetailsRepair.getBirthDate());
        personalDetailsRepairResponseV1.setBirthPlace(personalDetailsRepair.getBirthPlace());
        personalDetailsRepairResponseV1.setBirthProvince(personalDetailsRepair.getBirthProvince());
        personalDetailsRepairResponseV1.setFirstname(personalDetailsRepair.getFirstname());
        personalDetailsRepairResponseV1.setGender(personalDetailsRepair.getGender());
        personalDetailsRepairResponseV1.setLastname(personalDetailsRepair.getLastname());
        personalDetailsRepairResponseV1.setOfficeRole(personalDetailsRepair.getOfficeRole());
        personalDetailsRepairResponseV1.setPhone(personalDetailsRepair.getPhone());
        personalDetailsRepairResponseV1.setTaxCode(personalDetailsRepairResponseV1.getBirthCountry());

        return personalDetailsRepairResponseV1;
    }
}
