package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.controller.payload.request.insurancecompany.LegalCostRequest;
import com.doing.nemo.claims.controller.payload.response.insurancecompany.LegalCostResponse;
import com.doing.nemo.claims.entity.esb.InsuranceCompanyESB.LegalCostEsb;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbInsuranceCompany.LegalCost;
import com.doing.nemo.middleware.client.payload.response.MiddlewareLegalCostResponse;
import org.springframework.stereotype.Component;

@Component
public class LegalCostAdapter {

    public static LegalCost adptLegalCostRequestToLegalCost(LegalCostRequest legalCostRequest) {

        if (legalCostRequest != null) {

            LegalCost legalCost = new LegalCost();

            legalCost.setServiceId(legalCostRequest.getServiceId());
            legalCost.setService(legalCostRequest.getService());
            legalCost.setCompanyId(legalCostRequest.getCompanyId());
            legalCost.setCompany(legalCostRequest.getCompany());
            legalCost.setTariff(legalCostRequest.getTariff());
            legalCost.setTariffId(legalCostRequest.getTariffId());
            legalCost.setTariffCode(legalCostRequest.getTariffCode());
            legalCost.setPolicyNumber(legalCostRequest.getPolicyNumber());
            legalCost.setStartDate(legalCostRequest.getStartDate());
            legalCost.setEndDate(legalCostRequest.getEndDate());
            legalCost.setInsurancePolicyId(legalCostRequest.getInsurancePolicyId());
            legalCost.setInsuranceCompanyId(legalCostRequest.getInsuranceCompanyId());

            return legalCost;
        }
        return null;
    }

    public static LegalCostResponse adptLegalCostToLegalCostResponse(LegalCost legalCost) {

        if (legalCost != null) {

            LegalCostResponse legalCostResponse = new LegalCostResponse();

            legalCostResponse.setServiceId(legalCost.getServiceId());
            legalCostResponse.setService(legalCost.getService());
            legalCostResponse.setCompanyId(legalCost.getCompanyId());
            legalCostResponse.setCompany(legalCost.getCompany());
            legalCostResponse.setTariff(legalCost.getTariff());
            legalCostResponse.setTariffId(legalCost.getTariffId());
            legalCostResponse.setTariffCode(legalCost.getTariffCode());
            legalCostResponse.setPolicyNumber(legalCost.getPolicyNumber());
            legalCostResponse.setStartDate(legalCost.getStartDate());
            legalCostResponse.setEndDate(legalCost.getEndDate());
            legalCostResponse.setInsurancePolicyId(legalCost.getInsurancePolicyId());
            legalCostResponse.setInsuranceCompanyId(legalCost.getInsuranceCompanyId());

            return legalCostResponse;
        }
        return null;
    }

    public static LegalCost adptLegalCostResponseToLegalCost(LegalCostResponse legalCostResponse) {

        if (legalCostResponse != null) {

            LegalCost legalCost = new LegalCost();

            legalCost.setServiceId(legalCostResponse.getServiceId());
            legalCost.setService(legalCostResponse.getService());
            legalCost.setCompanyId(legalCostResponse.getCompanyId());
            legalCost.setCompany(legalCostResponse.getCompany());
            legalCost.setTariff(legalCostResponse.getTariff());
            legalCost.setTariffId(legalCostResponse.getTariffId());
            legalCost.setTariffCode(legalCostResponse.getTariffCode());
            legalCost.setPolicyNumber(legalCostResponse.getPolicyNumber());
            legalCost.setStartDate(legalCostResponse.getStartDate());
            legalCost.setEndDate(legalCostResponse.getEndDate());
            legalCost.setInsuranceCompanyId(legalCostResponse.getInsuranceCompanyId());
            legalCost.setInsurancePolicyId(legalCostResponse.getInsurancePolicyId());

            return legalCost;
        }
        return null;
    }

    public static LegalCostResponse adptLegalCostEsbToLegalCostResponse(LegalCostEsb legalCostEsb) {

        if (legalCostEsb != null) {

            LegalCostResponse legalCostResponse = new LegalCostResponse();

            legalCostResponse.setServiceId(legalCostEsb.getServiceId());
            legalCostResponse.setService(legalCostEsb.getService());
            legalCostResponse.setCompanyId(legalCostEsb.getCompanyId());
            legalCostResponse.setCompany(legalCostEsb.getCompany());
            legalCostResponse.setTariff(legalCostEsb.getTariff());
            if(legalCostEsb.getTariffId() != null) {
                Double doubleValue = Double.parseDouble(legalCostEsb.getTariffId());
                Long longValue = doubleValue.longValue();
                legalCostEsb.setTariffId(longValue.toString());
            }
            legalCostResponse.setTariffId(legalCostEsb.getTariffId());
            legalCostResponse.setTariffCode(legalCostEsb.getTariffCode());
            legalCostResponse.setPolicyNumber(legalCostEsb.getPolicyNumber());
            legalCostResponse.setStartDate(legalCostEsb.getStartDate());
            legalCostResponse.setEndDate(legalCostEsb.getEndDate());

            return legalCostResponse;
        }
        return null;
    }

    public static LegalCostResponse adptLegalCostMiddlewareToLegalCostResponse(MiddlewareLegalCostResponse legalCostEsb) {

        if (legalCostEsb != null) {

            LegalCostResponse legalCostResponse = new LegalCostResponse();

            if(legalCostEsb.getCompanyId() != null) {
                legalCostResponse.setCompanyId(legalCostEsb.getCompanyId().toString());
            }
            legalCostResponse.setCompany(legalCostEsb.getCompany());
            legalCostResponse.setTariff(legalCostEsb.getTariff());
            if(legalCostEsb.getTariffId() != null) {
                Double doubleValue = Double.parseDouble(legalCostEsb.getTariffId().toString());
                Long longValue = doubleValue.longValue();
                legalCostEsb.setTariffId(longValue);
            }
            if(legalCostEsb.getTariffId() != null) {
                legalCostResponse.setTariffId(legalCostEsb.getTariffId().toString());
            }
            legalCostResponse.setTariffCode(legalCostEsb.getTariffCode());
            legalCostResponse.setPolicyNumber(legalCostEsb.getPolicyNumber());
            legalCostResponse.setStartDate(legalCostEsb.getStartdate());
            legalCostResponse.setEndDate(legalCostEsb.getEnddate());

            return legalCostResponse;
        }
        return null;
    }
}
