package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.controller.payload.request.CenterRequestV1;
import com.doing.nemo.claims.controller.payload.response.CenterResponseV1;
import com.doing.nemo.claims.entity.CenterEntity;
import org.springframework.stereotype.Component;

@Component
public class CenterAdpter {

    public static CenterEntity adptFromCenterRequestToCenterEntity(CenterRequestV1 centerRequest) {
        if (centerRequest == null) {
            return null;
        }
        CenterEntity center = new CenterEntity();
        center.setAddress(centerRequest.getAddress());
        center.setAddressNumber(centerRequest.getAddressNumber());
        center.setAldCommodityId(centerRequest.getAldCommodityId());
        center.setContactPhone(centerRequest.getContactPhone());
        center.setAldSupplieCode(centerRequest.getAldSupplieCode());
        center.setDistance(centerRequest.getDistance());
        center.setFiscalCode(centerRequest.getFiscalCode());
        center.setHasDedicatedExpert(centerRequest.getHasDedicatedExpert());
        center.setMunicipality(centerRequest.getMunicipality());
        center.setName(centerRequest.getName());
        center.setPlaceId(centerRequest.getPlaceId());
        center.setProvinceCode(centerRequest.getProvinceCode());
        center.setRegion(centerRequest.getRegion());
        center.setType(centerRequest.getType());
        center.setVatNumber(centerRequest.getType());
        center.setZipCode(centerRequest.getZipCode());
        center.setEmail(centerRequest.getEmail());
        return center;
    }


    public static CenterResponseV1 adptFromCenterEntityToCenterResponse(CenterEntity center) {
        if (center == null) {
            return null;
        }
        CenterResponseV1 centerResponse = new CenterResponseV1();
        centerResponse.setAddress(center.getAddress());
        centerResponse.setAddressNumber(center.getAddressNumber());
        centerResponse.setAldCommodityId(center.getAldCommodityId());
        centerResponse.setContactPhone(center.getContactPhone());
        centerResponse.setAldSupplieCode(center.getAldSupplieCode());
        centerResponse.setDistance(center.getDistance());
        centerResponse.setFiscalCode(center.getFiscalCode());
        centerResponse.setHasDedicatedExpert(center.getHasDedicatedExpert());
        centerResponse.setMunicipality(center.getMunicipality());
        centerResponse.setName(center.getName());
        centerResponse.setPlaceId(center.getPlaceId());
        centerResponse.setProvinceCode(center.getProvinceCode());
        centerResponse.setRegion(center.getRegion());
        centerResponse.setType(center.getType());
        centerResponse.setVatNumber(center.getType());
        centerResponse.setZipCode(center.getZipCode());
        centerResponse.setEmail(center.getEmail());
        return centerResponse;
    }
}
