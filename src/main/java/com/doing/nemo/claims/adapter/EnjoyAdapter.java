package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.controller.payload.response.ContractInfoResponseV1;
import com.doing.nemo.middleware.client.payload.response.MiddlewareClaimInfoResponse;
import org.springframework.stereotype.Component;

import java.text.ParseException;

@Component
public class EnjoyAdapter {

    public static ContractInfoResponseV1 adptFromMiddlewareResponseToContractInfoResponse (MiddlewareClaimInfoResponse middlewareClaimInfoResponse) throws ParseException {
        if(middlewareClaimInfoResponse == null)
            return null;

        ContractInfoResponseV1 responseV1 = new ContractInfoResponseV1();
        responseV1.setVehicleResponse(VehicleAdapter.adptMiddlewareVehicleResponseToVehicleResponse(middlewareClaimInfoResponse.getVehicle()));
        responseV1.setContractResponse(ContractAdapter.adptMiddlewareContractDetailResponseToContractResponse(middlewareClaimInfoResponse.getContractDetail()));
        responseV1.setCustomerResponse(CustomerAdapter.adptMiddlewareCustomerResponseToCustomerResposne(middlewareClaimInfoResponse.getCustomer()));
        responseV1.setInsuranceCompany(InsuranceCompanyAdapter.adptMiddlewareContractInsuranceDetailResponseToInsuranceCompanyResponse(middlewareClaimInfoResponse.getContractInsuranceDetail()));

        return responseV1;
    }
}
