package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.ClaimRequest;
import com.doing.nemo.claims.controller.payload.response.ClaimsMyAldResponseV1;
import com.doing.nemo.claims.controller.payload.response.ClaimsResponseIdV1;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.middleware.client.payload.request.MiddlewareClaim;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.LinkedList;
import java.util.List;

@Component
public class ClaimAdapter {

    public static List<MiddlewareClaim> adptListClaimRequestToListMiddlewareClaim(List<ClaimRequest> claimRequests) {
        if(claimRequests == null) { return null; }

        List<MiddlewareClaim> middlewareClaims = new LinkedList<>();
        for (ClaimRequest claimRequest : claimRequests) {
            middlewareClaims.add(adptClaimRequestToMiddlewareClaim(claimRequest));
        }

        return middlewareClaims;
    }

    public static MiddlewareClaim adptClaimRequestToMiddlewareClaim(ClaimRequest claimRequest) {
        if(claimRequest == null) { return null; }

        MiddlewareClaim middlewareClaim = new MiddlewareClaim();
        middlewareClaim.setConfirmationDate(DateUtil.convertIS08601StringToUTCInstant(claimRequest.getConfirmationDate()));
        middlewareClaim.setRequestDate(claimRequest.getRequestDate());
        middlewareClaim.setClaimStatus(claimRequest.getClaimStatusEnumId());
        middlewareClaim.setClaimCategory(claimRequest.getClaimCategoryEnumId());
        middlewareClaim.setFileOpenDate(DateUtil.convertIS08601StringToUTCInstant(claimRequest.getFileOpenDate()));
        middlewareClaim.setFileCloseDate(DateUtil.convertIS08601StringToUTCInstant(claimRequest.getFileCloseDate()));
        middlewareClaim.setPaymentDate(claimRequest.getPaymentDate());
        middlewareClaim.setConfirmed(claimRequest.getConfirmed());
        middlewareClaim.setAmount(claimRequest.getAmount());
        middlewareClaim.setDescription(claimRequest.getDescription());
        middlewareClaim.setReference(claimRequest.getReference());
        middlewareClaim.setAcceptanceEnumId(claimRequest.getAcceptanceEnumId());
        middlewareClaim.setRejectionReasonEnumId(claimRequest.getRejectionReasonEnumId());
        middlewareClaim.setPaymentCommunication(claimRequest.getPaymentCommunication());
        middlewareClaim.setPaymentMethodEnumid(claimRequest.getPaymentMethodEnumid());
        middlewareClaim.setReceivedamount(claimRequest.getReceivedamount());
        middlewareClaim.setOutstandingamount(claimRequest.getOutstandingamount());
        middlewareClaim.setVatCodeEnumId(claimRequest.getVatCodeEnumId());
        middlewareClaim.setServiceTypeEnumId(claimRequest.getServiceTypeEnumId());
        middlewareClaim.setPolicyNumber(claimRequest.getPolicyNumber());
        middlewareClaim.setFileNumber(claimRequest.getFileNumber());
        middlewareClaim.setLiabilityCode(claimRequest.getLiabilityCode());
        middlewareClaim.setDamageType(claimRequest.getDamageType());
        middlewareClaim.setNatureEnumId(claimRequest.getNatureEnumId());
        middlewareClaim.setPaymentComplete(claimRequest.getPaymentComplete());

        return middlewareClaim;
    }
}
