package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.claims.NoteRequest;
import com.doing.nemo.claims.controller.payload.response.ClaimsResponseNoteV1;
import com.doing.nemo.claims.controller.payload.response.claims.NoteResponse;
import com.doing.nemo.claims.entity.NoteEntity;
import com.doing.nemo.claims.entity.enumerated.NotesEnum.NotesTypeEnum;
import com.doing.nemo.claims.entity.jsonb.Notes;
import com.doing.nemo.claims.util.Util;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

@Component
public class NotesAdapter {

    public static Notes adptNotesRequestToNotes(NoteRequest noteRequest) {

        if (noteRequest == null)
            return null;

        Notes note = new Notes();

        if (noteRequest.getNoteId() == null)
            noteRequest.setNoteId(UUID.randomUUID().toString());

        note.setNoteId(noteRequest.getNoteId());
        note.setCreatedAt(DateUtil.getNowDate());
        note.setDescription(noteRequest.getDescription());
        note.setImportant(noteRequest.getImportant());
        note.setNoteTitle(noteRequest.getNoteTitle());
        note.setNoteType(noteRequest.getNoteType());
        note.setHidden(noteRequest.getHidden());

        return note;
    }

    public static List<Notes> adptNotesRequestToNotes(List<NoteRequest> noteRequestList) {

        if (noteRequestList != null) {

            List<Notes> notesList = new LinkedList<>();
            for (NoteRequest att : noteRequestList) {
                notesList.add(adptNotesRequestToNotes(att));
            }
            return notesList;
        }
        return null;
    }

    public static NoteResponse adptNotesToNotesResponse(Notes note) {

        NoteResponse noteResponse = new NoteResponse();

        noteResponse.setCreatedAt(DateUtil.convertUTCDateToIS08601String(note.getCreatedAt()));
        noteResponse.setDescription(note.getDescription());
        noteResponse.setImportant(note.getImportant());
        noteResponse.setNoteId(note.getNoteId());
        noteResponse.setNoteTitle(note.getNoteTitle());
        noteResponse.setNoteType(note.getNoteType());
        noteResponse.setUserId(note.getUserId());
        noteResponse.setHidden(note.getHidden());

        return noteResponse;
    }

    public static List<NoteResponse> adptNotesToNotesResponse(List<Notes> noteList) {

        if (noteList != null) {

            List<NoteResponse> notesListResponse = new LinkedList<>();
            for (Notes att : noteList) {
                notesListResponse.add(adptNotesToNotesResponse(att));
            }
            return notesListResponse;
        }
        return null;
    }

    public static NoteResponse adptNotesEntityObjectToNotesResponse(NoteEntity noteEntity) {

        if(noteEntity == null){
            return null;
        }

        NoteResponse noteResponse = new NoteResponse();

        noteResponse.setNoteTitle(noteEntity.getNoteTitle());
        noteResponse.setNoteType(noteEntity.getNoteType());
        noteResponse.setDescription(noteEntity.getDescription());
        noteResponse.setImportant(noteEntity.getImportant());
        noteResponse.setNoteId(noteEntity.getNoteId());
        noteResponse.setUserId(noteEntity.getUserId());
        noteResponse.setCreatedAt(DateUtil.convertUTCDateToIS08601String(noteEntity.getCreatedAt()));
        noteResponse.setHidden(noteEntity.getHidden());

        return noteResponse;

    }



    public static NoteResponse adptNotesObjectToNotesResponse(List<Object> noteObj) {


        NoteResponse noteResponse = new NoteResponse();
        for (Object obj : noteObj) {

            Object[] note = (Object[]) obj;
            noteResponse.setNoteTitle((String) note[0]);
            noteResponse.setNoteType(NotesTypeEnum.create((String) note[1]));
            noteResponse.setDescription((String) note[2]);
            noteResponse.setImportant(Boolean.valueOf((String) note[3]));
            noteResponse.setNoteId((String) note[4]);
            noteResponse.setUserId((String) note[5]);
            noteResponse.setCreatedAt(DateUtil.convertUTCDateToIS08601String(Util.fromStringToDate((String) note[6])));
            noteResponse.setHidden(Boolean.valueOf((String) note[7]));
        }
        return noteResponse;
    }

    /* NEW ADAPTER */
    public static NoteEntity adptNotesToNoteEntity(Notes notes) {

        if (notes == null) { return null; }

        NoteEntity noteEntity = new NoteEntity();

        if (notes.getNoteId() == null){
            notes.setNoteId(UUID.randomUUID().toString());
        }

        noteEntity.setNoteId(notes.getNoteId());
        noteEntity.setCreatedAt(DateUtil.getNowDate());
        noteEntity.setDescription(notes.getDescription());
        noteEntity.setImportant(notes.getImportant());
        noteEntity.setNoteTitle(notes.getNoteTitle());
        noteEntity.setNoteType(notes.getNoteType());
        noteEntity.setHidden(notes.getHidden());
        noteEntity.setUserId(notes.getUserId());

        return noteEntity;
    }

    public static List<NoteEntity> adptNotesListRequestToNotesList(List<Notes> notesList) {

        if (notesList != null) {

            List<NoteEntity> noteEntities = new LinkedList<>();
            for (Notes att : notesList) {
                noteEntities.add(adptNotesToNoteEntity(att));
            }
            return noteEntities;
        }
        return null;
    }

    public static Notes adptNoteEntityToNotes(NoteEntity noteEntity) {

        if (noteEntity == null) { return null; }

        Notes notes = new Notes();

        if (noteEntity.getNoteId() == null){
            noteEntity.setNoteId(UUID.randomUUID().toString());
        }

        notes.setNoteId(noteEntity.getNoteId());
        notes.setCreatedAt(DateUtil.getNowDate());
        notes.setDescription(noteEntity.getDescription());
        notes.setImportant(noteEntity.getImportant());
        notes.setNoteTitle(noteEntity.getNoteTitle());
        notes.setNoteType(noteEntity.getNoteType());
        notes.setHidden(noteEntity.getHidden());
        notes.setUserId(noteEntity.getUserId());

        return notes;
    }

    public static List<Notes> adptNoteEntityListToNotesList(List<NoteEntity> noteEntityList) {

        if (noteEntityList != null && noteEntityList.size()!=0) {

            List<Notes> notes = new LinkedList<>();
            for (NoteEntity att : noteEntityList) {
                notes.add(adptNoteEntityToNotes(att));
            }
            return notes;
        }
        return null;
    }
}
