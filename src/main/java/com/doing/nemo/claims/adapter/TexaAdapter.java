package com.doing.nemo.claims.adapter;


import com.doing.nemo.claims.crashservices.texa.org.datacontract.schemas._2004._07.texa_tmd_bol_m2mtrack_ald_webservice_service.GetVoucherInfoRequest;
import com.doing.nemo.claims.crashservices.texa.org.datacontract.schemas._2004._07.texa_tmd_bol_m2mtrack_ald_webservice_service.GetVoucherInfoResponse;
import com.doing.nemo.claims.crashservices.texa.org.datacontract.schemas._2004._07.texa_tmd_bol_m2mtrack_ald_webservice_service.ObjectFactory;
import com.doing.nemo.claims.entity.AntiTheftRequestEntity;
import com.doing.nemo.claims.entity.AntiTheftRequestNewEntity;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbAntiTheftService.AntiTheftRequest;
import org.springframework.stereotype.Component;
import org.springframework.util.Base64Utils;

import javax.xml.bind.JAXBElement;


@Component
public class TexaAdapter {

    public static GetVoucherInfoRequest adptFromInputToTexaRequest(String claimNumber, String companyCode, String dateTime, String user, String pwd, String requestType, String plate, Long voucher){

        GetVoucherInfoRequest request  = new GetVoucherInfoRequest();
        ObjectFactory objectFactory = new ObjectFactory();
        JAXBElement<String> a = objectFactory.createGetVoucherInfoRequestClaimNumber(claimNumber);
        request.setClaimNumber(a);
        JAXBElement<String> b = objectFactory.createGetVoucherInfoRequestCompanyCode(companyCode);
        request.setCompanyCode(b);
        JAXBElement<String> c = objectFactory.createGetVoucherInfoRequestDateTime(dateTime);
        request.setDateTime(c);
        JAXBElement<String> d = objectFactory.createGetVoucherInfoRequestUser(user);
        request.setUser(d);
        JAXBElement<String> e = objectFactory.createGetVoucherInfoRequestPwd(pwd);
        request.setPwd(e);
        JAXBElement<String> f = objectFactory.createGetVoucherInfoRequestRequestType(requestType);
        request.setRequestType(f);
        JAXBElement<String> g = objectFactory.createGetVoucherInfoRequestPlate(plate);
        request.setPlate(g);
        request.setVoucher(voucher);

        return request;
    }

    public static AntiTheftRequest adptFromTexaResponseToAntiTheftRequest(GetVoucherInfoResponse response, AntiTheftRequest antiTheftRequest){
        //AntiTheftRequest antiTheftRequest = new AntiTheftRequest();
        antiTheftRequest.setAnomaly(response.getAnomaly() > 0);
        antiTheftRequest.setgPower(response.getGValue());
        antiTheftRequest.setCrashNumber(response.getReportNumber());
        antiTheftRequest.setOutcome(response.getOutcome().getValue());
        String pdf = null;
        if(response.getPdfReport() != null && response.getPdfReport().getValue()!=null)
           pdf  = Base64Utils.encodeToString(response.getPdfReport().getValue());
        antiTheftRequest.setPdf(pdf);
        antiTheftRequest.setSubReport(response.getReportNumber());
        Long code = null;
        if(response.getRespCode() != null){
            code  = response.getRespCode().getValue();
            antiTheftRequest.setResponseType(code.toString());
        }
        antiTheftRequest.setResponseMessage(response.getRespMsg().getValue());

        return antiTheftRequest;
    }


    public static AntiTheftRequestNewEntity adptFromTexaResponseToAntiTheftServiceEntity(GetVoucherInfoResponse response, AntiTheftRequestNewEntity antiTheftRequestEntity){
        if(response == null){
            return null;
        }

        antiTheftRequestEntity.setAnomaly(response.getAnomaly() > 0);
        antiTheftRequestEntity.setgPower(response.getGValue());
        antiTheftRequestEntity.setCrashNumber(response.getNumberCrash());
        antiTheftRequestEntity.setOutcome(response.getOutcome().getValue());
        antiTheftRequestEntity.setSubReport(response.getReportNumber());
        Long code = null;
        if(response.getRespCode() != null){
            code  = response.getRespCode().getValue();
            antiTheftRequestEntity.setResponseType(code.toString());
        }
        antiTheftRequestEntity.setResponseMessage(response.getRespMsg().getValue());

        return antiTheftRequestEntity;
    }

}
