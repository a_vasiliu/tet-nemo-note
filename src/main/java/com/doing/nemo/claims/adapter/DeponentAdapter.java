package com.doing.nemo.claims.adapter;


import com.doing.nemo.claims.controller.payload.request.claims.DeponentRequest;
import com.doing.nemo.claims.controller.payload.response.claims.DeponentResponse;
import com.doing.nemo.claims.entity.jsonb.Deponent;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

@Component
public class DeponentAdapter {

    public static Deponent adptDeponentRequestToDeponent(DeponentRequest deponentRequest) {

        Deponent deponent = new Deponent();
        deponent.setAddress(deponentRequest.getAddress());
        deponent.setEmail(deponentRequest.getEmail());
        deponent.setFirstname(deponentRequest.getFirstname());
        deponent.setLastname(deponentRequest.getLastname());
        deponent.setPhone(deponentRequest.getPhone());
        deponent.setFiscalCode(deponentRequest.getFiscalCode());
        deponent.setAttachments(deponentRequest.getAttachments());

        return deponent;
    }

    public static List<Deponent> adptDeponentRequestToDeponent(List<DeponentRequest> deponentRequestList) {

        if (deponentRequestList != null) {

            List<Deponent> deponentList = new LinkedList<>();
            for (DeponentRequest att : deponentRequestList) {
                deponentList.add(adptDeponentRequestToDeponent(att));
            }
            return deponentList;
        }
        return null;
    }

    public static DeponentResponse adptDeponentToDeponentResponse(Deponent deponent) {

        DeponentResponse deponentResponse = new DeponentResponse();
        deponentResponse.setAddress(deponent.getAddress());
        deponentResponse.setEmail(deponent.getEmail());
        deponentResponse.setFirstname(deponent.getFirstname());
        deponentResponse.setLastname(deponent.getLastname());
        deponentResponse.setPhone(deponent.getPhone());
        deponentResponse.setFiscalCode(deponent.getFiscalCode());
        deponentResponse.setAttachments(deponent.getAttachments());

        return deponentResponse;
    }

    public static List<DeponentResponse> adptDeponentToDeponentResponse(List<Deponent> deponentList) {

        if (deponentList != null) {

            List<DeponentResponse> deponentListResponse = new LinkedList<>();
            for (Deponent att : deponentList) {
                deponentListResponse.add(adptDeponentToDeponentResponse(att));
            }
            return deponentListResponse;
        }
        return null;
    }
}
