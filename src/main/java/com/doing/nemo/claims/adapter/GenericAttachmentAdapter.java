package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.response.GenericAttachmentResponsePaginationV1;
import com.doing.nemo.claims.controller.payload.response.GenericAttachmentResponseV1;
import com.doing.nemo.claims.entity.enumerated.DTO.PageStats;
import com.doing.nemo.claims.entity.settings.GenericAttachmentEntity;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

@Component
public class GenericAttachmentAdapter {

    public static GenericAttachmentResponseV1 adptGenericAttachmentToGenericAttachmentResponse(GenericAttachmentEntity genericAttachment){

        if(genericAttachment == null)
            return null;

        GenericAttachmentResponseV1 genericAttachmentResponse = new GenericAttachmentResponseV1();

        genericAttachmentResponse.setAttachmentId(genericAttachment.getAttachmentId());
        genericAttachmentResponse.setCreatedAt(DateUtil.convertUTCInstantToIS08601String(genericAttachment.getCreatedAt()));
        genericAttachmentResponse.setDescription(genericAttachment.getDescription());
        genericAttachmentResponse.setId(genericAttachment.getId());
        genericAttachmentResponse.setOriginalName(genericAttachment.getOriginalName());
        genericAttachmentResponse.setActive(genericAttachment.getActive());
        genericAttachmentResponse.setTypeComplaint(genericAttachment.getTypeComplaint());

        return genericAttachmentResponse;
    }

    public static List<GenericAttachmentResponseV1> adptGenericAttachmentToGenericAttachmentResponse(List<GenericAttachmentEntity> genericAttachmentList ){

        if(genericAttachmentList == null)
            return null;

        List<GenericAttachmentResponseV1> genericAttachmentResponseList = new LinkedList<>();
        for (GenericAttachmentEntity att : genericAttachmentList){
            genericAttachmentResponseList.add(adptGenericAttachmentToGenericAttachmentResponse(att));
        }

        return genericAttachmentResponseList;
    }

    public static GenericAttachmentResponsePaginationV1 adptPagination(List<GenericAttachmentEntity> genericAttachmentList, int page, int pageSize, Long itemCount) {

        double totPages = Math.ceil((double) itemCount.intValue() / pageSize);
        PageStats pageStats = new PageStats();
        pageStats.setCurrentPage(page);
        pageStats.setItemCount(itemCount.intValue());
        pageStats.setPageSize(pageSize);
        pageStats.setPageCount((long)totPages);

        GenericAttachmentResponsePaginationV1 dataManagementImportResponsePaginationV1 = new GenericAttachmentResponsePaginationV1(pageStats, adptGenericAttachmentToGenericAttachmentResponse(genericAttachmentList));

        return dataManagementImportResponsePaginationV1;
    }

}
