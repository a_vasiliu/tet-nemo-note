package com.doing.nemo.claims.adapter;


import com.doing.nemo.claims.controller.payload.response.claims.Claims;
import com.doing.nemo.claims.entity.ClaimsEntity;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

@Component
public class ClaimsCheckerAdapter {

    public static List<Claims> adptClaimsEntityListToClaimsCheckerResponseList(List<ClaimsEntity> claimsEntityList) {

        List<Claims> claims = new LinkedList<>();
        for (ClaimsEntity claimsEntity : claimsEntityList) {
            claims.add(adptClaimsEntityToClaimsCheckerResponse(claimsEntity));
        }
        return claims;
    }

    public static Claims adptClaimsEntityToClaimsCheckerResponse (ClaimsEntity claimsEntity) {

        if (claimsEntity == null) return null;
        Claims claims = new Claims();
        claims.setId(claimsEntity.getId());
        claims.setPracticeId(claimsEntity.getPracticeId());
        return claims;
    }
}
