package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.claims.FoundModelRequest;
import com.doing.nemo.claims.controller.payload.response.claims.FoundModelResponse;
import com.doing.nemo.claims.entity.jsonb.foundModel.FoundModel;
import org.springframework.stereotype.Component;

@Component
public class FoundModelAdapter {

    public static FoundModelResponse adptFoundModelToFoundModelResponse(FoundModel foundModel) {

        if (foundModel != null) {

            FoundModelResponse foundModelResponse = new FoundModelResponse();
            foundModelResponse.setAddress(foundModel.getAddress());
            foundModelResponse.setAuthorityData(foundModel.getAuthorityData());
            foundModelResponse.setCc(foundModel.getCc());
            foundModelResponse.setDate(DateUtil.convertUTCDateToIS08601String(foundModel.getDate()));
            foundModelResponse.setFoundedAbroad(foundModel.getFoundedAbroad());
            foundModelResponse.setFoundNote(foundModel.getFoundNote());
            foundModelResponse.setHour(foundModel.getHour());
            foundModelResponse.setPhone(foundModel.getPhone());
            foundModelResponse.setPolice(foundModel.getPolice());
            foundModelResponse.setUnderSeizure(foundModel.getUnderSeizure());
            foundModelResponse.setVehicleCo(foundModel.getVehicleCo());
            foundModelResponse.setVvuu(foundModel.getVvuu());

            return foundModelResponse;
        }
        return null;
    }

    public static FoundModel adptFoundModelRequestToFoundModel(FoundModelRequest foundModelRequest) {

        if (foundModelRequest != null) {

            FoundModel foundModel = new FoundModel();

            foundModel.setAddress(foundModelRequest.getAddress());
            foundModel.setAuthorityData(foundModelRequest.getAuthorityData());
            foundModel.setCc(foundModelRequest.getCc());
            foundModel.setDate(DateUtil.convertIS08601StringToUTCDate(foundModelRequest.getDate()));
            foundModel.setFoundedAbroad(foundModelRequest.getFoundedAbroad());
            foundModel.setFoundNote(foundModelRequest.getFoundNote());
            foundModel.setHour(foundModelRequest.getHour());
            foundModel.setPhone(foundModelRequest.getPhone());
            foundModel.setPolice(foundModelRequest.getPolice());
            foundModel.setUnderSeizure(foundModelRequest.getUnderSeizure());
            foundModel.setVehicleCo(foundModelRequest.getVehicleCo());
            foundModel.setVvuu(foundModelRequest.getVvuu());

            return foundModel;
        }
        return null;
    }
}
