package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.dto.FilterView;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsFlowEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ClaimsSearchRequestDTOAdapter {

    @Autowired
    private Util util;

    public FilterView adaptRequestToDTO(Boolean inEvidence, Long practiceId, String flow, List<Long> clientListId, String plate,
                                        String status, String locator, String dateAccidentFrom,
                                        String dateAccidentTo, String typeAccident, Boolean withContinuation,
                                        String company, String clientName, Boolean poVariation, Boolean waitingForNumberSx, Long fleetVehicleId, Boolean waitingForAuthority){


        List<String> flowsList = null;
        if (flow != null)
            flowsList = util.splitRequest(flow, ClaimsFlowEnum.class);

        List<String> statusList = null;
        if (status != null)
            statusList = util.splitRequest(status, ClaimsStatusEnum.class);

        List<String> typeAccidentList = null;
        if (typeAccident != null)
            typeAccidentList = util.splitRequest(typeAccident, DataAccidentTypeAccidentEnum.class);


        FilterView claimsFilterDTO = new FilterView();
        claimsFilterDTO.setInEvidence(inEvidence);
        claimsFilterDTO.setWithContinuation(withContinuation);
        claimsFilterDTO.setPracticeId(practiceId);
        claimsFilterDTO.setFlow(flowsList);
        claimsFilterDTO.setClientListId(clientListId);
        claimsFilterDTO.setPlate(plate);
        claimsFilterDTO.setStatus(statusList);
        claimsFilterDTO.setLocator(locator);
        claimsFilterDTO.setDateAccidentFrom(DateUtil.convertIS08601StringToUTCInstant(dateAccidentFrom));
        claimsFilterDTO.setDateAccidentTo(DateUtil.convertIS08601StringToUTCInstant(dateAccidentTo));
        claimsFilterDTO.setTypeAccident(typeAccidentList);
        claimsFilterDTO.setWithContinuation(withContinuation);
        claimsFilterDTO.setCompany(company);
        claimsFilterDTO.setClientName(clientName);
        claimsFilterDTO.setPoVariation(poVariation);
        claimsFilterDTO.setWaitingForNumberSx(waitingForNumberSx);
        claimsFilterDTO.setFleetVehicleId(fleetVehicleId);
        claimsFilterDTO.setWaitingForAuthority(waitingForAuthority);
        return claimsFilterDTO;
    }


}
