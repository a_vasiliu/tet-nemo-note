package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.dto.ClaimsSearchDTO;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsFlowEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.view.SearchDashboardClaimsView;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Component;

import javax.persistence.Column;
import java.time.Instant;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ClaimsViewSearchAdapter {
    public List<ClaimsSearchDTO> adaptClaimsSearchViewToDTO(List<SearchDashboardClaimsView> entityList) {

        if (CollectionUtils.isEmpty(entityList)) {
            return Collections.emptyList();
        }

        return entityList.stream()
                .map(this::adaptSearchDashboardClaimsViewToDTO)
                .collect(Collectors.toList());
    }

    public ClaimsSearchDTO adaptSearchDashboardClaimsViewToDTO(SearchDashboardClaimsView entity) {
        if (entity == null) {
            return null;
        }
        ClaimsSearchDTO claimsSearchDTO = new ClaimsSearchDTO();
        claimsSearchDTO.setId(entity.getId());
        claimsSearchDTO.setCreatedAt(entity.getCreatedAt());
        claimsSearchDTO.setUpdatedAt(entity.getUpdatedAt());
        claimsSearchDTO.setStatusUpdatedAt(entity.getStatusUpdatedAt());
        claimsSearchDTO.setType(entity.getType());
        claimsSearchDTO.setPracticeId(entity.getPracticeId());
        claimsSearchDTO.setStatus(entity.getStatus());
        claimsSearchDTO.setInEvidence(entity.getInEvidence());
        claimsSearchDTO.setPoVariation(entity.getPoVariation());
        claimsSearchDTO.setPlate(entity.getPlate());
        claimsSearchDTO.setTypeAccident(entity.getTypeAccident());
        claimsSearchDTO.setDateAccident(entity.getDateAccident());
        claimsSearchDTO.setClientId(String.valueOf(entity.getClientId()!=null?entity.getClientId():""));
        claimsSearchDTO.setAutoEntrust(entity.getAutoEntrust()!=null?entity.getAutoEntrust():null);
        claimsSearchDTO.setCustomerId(String.valueOf(entity.getCustomerId()!=null?entity.getCustomerId():""));
        claimsSearchDTO.setBusinessName(entity.getBusinessName()!=null?entity.getBusinessName():"");
        claimsSearchDTO.setDamagedAntiTheftService_id(entity.getDamagedAntiTheftService_id()!=null?entity.getDamagedAntiTheftService_id():null);

        claimsSearchDTO.setProviderType(entity.getProviderType());
        claimsSearchDTO.setResponseType(entity.getResponseType());



        claimsSearchDTO.setPending(entity.getPending());

        return claimsSearchDTO;
    }
}
