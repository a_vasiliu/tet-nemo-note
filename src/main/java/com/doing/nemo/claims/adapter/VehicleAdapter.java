package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.counterparty.VehicleRequest;
import com.doing.nemo.claims.controller.payload.response.damaged.VehicleResponse;
import com.doing.nemo.claims.entity.claims.ClaimsDamagedVehicleEntity;
import com.doing.nemo.claims.entity.enumerated.DamagedEnum.VehicleEnum.VehicleNatureEnum;
import com.doing.nemo.claims.entity.esb.VehicleESB.VehicleEsb;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbVehicle.Vehicle;
import com.doing.nemo.middleware.client.payload.response.MiddlewareVehicleResponse;
import org.springframework.stereotype.Component;

@Component
public class VehicleAdapter {

    private static VehicleNatureEnum converVehicleEsbToVehicleNature(String vehicleToConvert){
        VehicleNatureEnum vehicleNatureEnum;

        if(vehicleToConvert == null){
            vehicleNatureEnum = null;
        } else {
            switch (vehicleToConvert.toLowerCase().replaceAll(" ", "").replaceAll("_", ""))
            {
                case "lightcommercialicle":
                case "v" :
                case "van":
                case "lightcommercialvehicle": {
                    vehicleNatureEnum = VehicleNatureEnum.VAN;
                    break;
                }
                case "passengercar":
                case "quadriciclo" :
                case "undefined":
                case "motorvehicle":
                case "c" : {
                    vehicleNatureEnum = VehicleNatureEnum.MOTOR_VEHICLE;
                    break;
                }
                case "motorcycle":
                case "scooter":
                case "ciclomotore":
                case "1": {
                    vehicleNatureEnum = VehicleNatureEnum.MOTOR_CYCLE;
                    break;
                }
                default:
                    vehicleNatureEnum = VehicleNatureEnum.MOTOR_VEHICLE;
                    break;
            }
        }
        return vehicleNatureEnum;
    }

    public static Vehicle adptVehicleRequestToVehicle(VehicleRequest vehicleRequest) {

        if (vehicleRequest != null) {

            Vehicle vehicle = new Vehicle();
            vehicle.setBodyStyle(vehicleRequest.getBodyStyle());
            vehicle.setCc3(vehicleRequest.getCc3());
            vehicle.setChassisNumber(vehicleRequest.getChassisNumber());
            vehicle.setChassisNumberTrailer(vehicleRequest.getChassisNumberTrailer());
            vehicle.setCo2emission(vehicleRequest.getCo2emission());
            vehicle.setConsumption(vehicleRequest.getConsumption());
            vehicle.setDinHp(vehicleRequest.getDinHp());
            vehicle.setDoors(vehicleRequest.getDoors());
            vehicle.setExternalColor(vehicleRequest.getExternalColor());
            vehicle.setFleetVehicleId(vehicleRequest.getFleetVehicleId());
            vehicle.setFuelType(vehicleRequest.getFuelType());
            vehicle.setHp(vehicleRequest.getHp());
            vehicle.setInternalColor(vehicleRequest.getInternalColor());
            vehicle.setKw(vehicleRequest.getKw());
            vehicle.setLastKnownMileage(vehicleRequest.getLastKnownMileage());
            vehicle.setLicensePlate(vehicleRequest.getLicensePlate());
            vehicle.setLicensePlateTrailer(vehicleRequest.getLicensePlateTrailer());
            vehicle.setMake(vehicleRequest.getMake());
            vehicle.setMaxWeight(vehicleRequest.getMaxWeight());
            vehicle.setModel(vehicleRequest.getModel());
            vehicle.setModelYear(vehicleRequest.getModelYear());
            vehicle.setNature(converVehicleEsbToVehicleNature(vehicleRequest.getNature()));
            vehicle.setNetWeight(vehicleRequest.getNetWeight());
            vehicle.setOwnership(vehicleRequest.getOwnership());
            vehicle.setRegistrationCountry(vehicleRequest.getRegistrationCountry());
            vehicle.setRegistrationCountryTrailer(vehicleRequest.getRegistrationCountryTrailer());
            vehicle.setRegistrationDate(vehicleRequest.getRegistrationDate());
            vehicle.setSeats(vehicleRequest.getSeats());
            vehicle.setWreck(vehicleRequest.getWreck());
            vehicle.setNetBookValue(vehicleRequest.getNetBookValue());
            vehicle.setWreckDate(vehicleRequest.getWreckDate());

            return vehicle;
        }

        return null;

    }

    public static VehicleResponse adptVehicleToVehicleResponse(Vehicle vehicle) {

        if (vehicle != null) {

            VehicleResponse vehicleResponse = new VehicleResponse();
            vehicleResponse.setBodyStyle(vehicle.getBodyStyle());
            vehicleResponse.setCc3(vehicle.getCc3());
            vehicleResponse.setChassisNumber(vehicle.getChassisNumber());
            vehicleResponse.setChassisNumberTrailer(vehicle.getChassisNumberTrailer());
            vehicleResponse.setCo2emission(vehicle.getCo2emission());
            vehicleResponse.setConsumption(vehicle.getConsumption());
            vehicleResponse.setDinHp(vehicle.getDinHp());
            vehicleResponse.setDoors(vehicle.getDoors());
            vehicleResponse.setExternalColor(vehicle.getExternalColor());
            vehicleResponse.setFleetVehicleId(vehicle.getFleetVehicleId());
            vehicleResponse.setFuelType(vehicle.getFuelType());
            vehicleResponse.setHp(vehicle.getHp());
            vehicleResponse.setInternalColor(vehicle.getInternalColor());
            vehicleResponse.setKw(vehicle.getKw());
            vehicleResponse.setLastKnownMileage(vehicle.getLastKnownMileage());
            vehicleResponse.setLicensePlate(vehicle.getLicensePlate());
            vehicleResponse.setLicensePlateTrailer(vehicle.getLicensePlateTrailer());
            vehicleResponse.setMake(vehicle.getMake());
            vehicleResponse.setMaxWeight(vehicle.getMaxWeight());
            vehicleResponse.setModel(vehicle.getModel());
            vehicleResponse.setModelYear(vehicle.getModelYear());
            if (vehicle.getNature() != null) {
                vehicleResponse.setNature(vehicle.getNature().name());
            }
            vehicleResponse.setNetWeight(vehicle.getNetWeight());
            vehicleResponse.setOwnership(vehicle.getOwnership());
            vehicleResponse.setRegistrationCountry(vehicle.getRegistrationCountry());
            vehicleResponse.setRegistrationCountryTrailer(vehicle.getRegistrationCountryTrailer());
            vehicleResponse.setRegistrationDate(vehicle.getRegistrationDate());
            vehicleResponse.setSeats(vehicle.getSeats());
            vehicleResponse.setWreck(vehicle.getWreck());
            vehicleResponse.setNetBookValue(vehicle.getNetBookValue());
            vehicleResponse.setWreckDate(vehicle.getWreckDate());
            return vehicleResponse;
        }
        return null;

    }





    public static Vehicle adptVehicleResponseToVehicle(VehicleResponse vehicleResponse) {

        if (vehicleResponse != null) {

            Vehicle vehicle = new Vehicle();
            vehicle.setBodyStyle(vehicleResponse.getBodyStyle());
            vehicle.setCc3(vehicleResponse.getCc3());
            vehicle.setChassisNumber(vehicleResponse.getChassisNumber());
            vehicle.setChassisNumberTrailer(vehicleResponse.getChassisNumberTrailer());
            vehicle.setCo2emission(vehicleResponse.getCo2emission());
            vehicle.setConsumption(vehicleResponse.getConsumption());
            vehicle.setDinHp(vehicleResponse.getDinHp());
            vehicle.setDoors(vehicleResponse.getDoors());
            vehicle.setExternalColor(vehicleResponse.getExternalColor());
            vehicle.setFleetVehicleId(vehicleResponse.getFleetVehicleId());
            vehicle.setFuelType(vehicleResponse.getFuelType());
            vehicle.setHp(vehicleResponse.getHp());
            vehicle.setInternalColor(vehicleResponse.getInternalColor());
            vehicle.setKw(vehicleResponse.getKw());
            vehicle.setLastKnownMileage(vehicleResponse.getLastKnownMileage());
            vehicle.setLicensePlate(vehicleResponse.getLicensePlate());
            vehicle.setLicensePlateTrailer(vehicleResponse.getLicensePlateTrailer());
            vehicle.setMake(vehicleResponse.getMake());
            vehicle.setMaxWeight(vehicleResponse.getMaxWeight());
            vehicle.setModel(vehicleResponse.getModel());
            vehicle.setModelYear(vehicleResponse.getModelYear());
            //adattiamo i valori ritornati dell'esb nella vehicle
            vehicle.setNature(converVehicleEsbToVehicleNature(vehicleResponse.getNature()));
            vehicle.setNetWeight(vehicleResponse.getNetWeight());
            vehicle.setOwnership(vehicleResponse.getOwnership());
            vehicle.setRegistrationCountry(vehicleResponse.getRegistrationCountry());
            vehicle.setRegistrationCountryTrailer(vehicleResponse.getRegistrationCountryTrailer());
            vehicle.setRegistrationDate(vehicleResponse.getRegistrationDate());
            vehicle.setSeats(vehicleResponse.getSeats());
            vehicle.setNetBookValue(vehicleResponse.getNetBookValue());
            vehicle.setWreck(vehicleResponse.getWreck());
            vehicle.setWreckDate(vehicleResponse.getWreckDate());
            return vehicle;
        }
        return null;

    }

    public static VehicleResponse adptMiddlewareVehicleResponseToVehicleResponse(VehicleEsb vehicleEsb) {

        if (vehicleEsb != null) {

            VehicleResponse vehicleResponse = new VehicleResponse();
            vehicleResponse.setBodyStyle(vehicleEsb.getBodyStyle());
            vehicleResponse.setCc3(vehicleEsb.getCc3());
            vehicleResponse.setChassisNumber(vehicleEsb.getChassisNumber());
            vehicleResponse.setCo2emission(vehicleEsb.getCo2emission());
            vehicleResponse.setConsumption(vehicleEsb.getConsumption());
            vehicleResponse.setDinHp(vehicleEsb.getDinHp());
            vehicleResponse.setDoors(vehicleEsb.getDoors());
            vehicleResponse.setExternalColor(vehicleEsb.getExternalColor());
            vehicleResponse.setFleetVehicleId(vehicleEsb.getFleetVehicleId());
            vehicleResponse.setFuelType(vehicleEsb.getFuelType());
            vehicleResponse.setHp(vehicleEsb.getHp());
            vehicleResponse.setInternalColor(vehicleEsb.getInternalColor());
            vehicleResponse.setKw(vehicleEsb.getKw());
            vehicleResponse.setLastKnownMileage(vehicleEsb.getLastKnownMileage());
            vehicleResponse.setLicensePlate(vehicleEsb.getLicensePlate());
            vehicleResponse.setMake(vehicleEsb.getMake());
            vehicleResponse.setMaxWeight(vehicleEsb.getMaxWeight());
            vehicleResponse.setModel(vehicleEsb.getModel());
            vehicleResponse.setModelYear(vehicleEsb.getModelYear());
            if(vehicleEsb.getNetBookValue() != null) {
                vehicleResponse.setNetBookValue(vehicleEsb.getNetBookValue());
                vehicleResponse.setWreck(false);
            }
            else
            {
                vehicleResponse.setNetBookValue(0.0);
                vehicleResponse.setWreck(false);
            }

            //da fixare con gli enum di esb
            //cambiato il tipo della risposta
            vehicleResponse.setNature(vehicleEsb.getNature());
            vehicleResponse.setNetWeight(vehicleEsb.getNetWeight());
            vehicleResponse.setRegistrationDate(vehicleEsb.getRegistrationDate());
            vehicleResponse.setSeats(vehicleEsb.getSeats());

            return vehicleResponse;
        }
        return null;

    }


    public static VehicleResponse adptMiddlewareVehicleResponseToVehicleResponse(MiddlewareVehicleResponse vehicleEsb) {

        if (vehicleEsb != null) {

            VehicleResponse vehicleResponse = new VehicleResponse();
            vehicleResponse.setBodyStyle(vehicleEsb.getBodyStyle());
            if(vehicleEsb.getCc3()!=null)
                vehicleResponse.setCc3(vehicleEsb.getCc3().toString());
            vehicleResponse.setChassisNumber(vehicleEsb.getChassisNumber());
            if(vehicleEsb.getCo2Emission()!=null)
                vehicleResponse.setCo2emission(vehicleEsb.getCo2Emission().toString());
            if(vehicleEsb.getConsumption()!=null)
                vehicleResponse.setConsumption(vehicleEsb.getConsumption().toString());
            if(vehicleEsb.getDinHp()!=null)
                vehicleResponse.setDinHp(vehicleEsb.getDinHp().intValue());
            vehicleResponse.setDoors(vehicleEsb.getDoors());
            vehicleResponse.setExternalColor(vehicleEsb.getExternalColor());
            vehicleResponse.setFleetVehicleId(vehicleEsb.getFleetVehicleId());
            vehicleResponse.setFuelType(vehicleEsb.getFuelType());
            if(vehicleEsb.getHp()!=null)
                vehicleResponse.setHp(vehicleEsb.getHp().intValue());
            vehicleResponse.setInternalColor(vehicleEsb.getInternalColor());
            if(vehicleEsb.getKw()!=null)
                vehicleResponse.setKw(vehicleEsb.getKw().toString());
            if(vehicleEsb.getLastKnownMileage()!=null)
                vehicleResponse.setLastKnownMileage(vehicleEsb.getLastKnownMileage().toString());
            vehicleResponse.setLicensePlate(vehicleEsb.getLicensePlate());
            vehicleResponse.setMake(vehicleEsb.getMake());
            if(vehicleEsb.getMaxWeight()!=null)
                vehicleResponse.setMaxWeight(vehicleEsb.getMaxWeight().doubleValue());
            vehicleResponse.setModel(vehicleEsb.getModel());
            vehicleResponse.setModelYear(vehicleEsb.getModelYear());
            if(vehicleEsb.getNetBookValue() != null) {
                vehicleResponse.setNetBookValue(vehicleEsb.getNetBookValue().doubleValue());
                vehicleResponse.setWreck(false);
            }
            else
            {
                vehicleResponse.setNetBookValue(0.0);
                vehicleResponse.setWreck(false);
            }

            //da fixare con gli enum di esb
            //cambiato il tipo della risposta
            vehicleResponse.setNature(vehicleEsb.getNature());
            vehicleResponse.setNetWeight(vehicleEsb.getNetWeight().doubleValue());
            //bisogna capire come è formattata la stringa e fare l'adapter
            /*if(vehicleEsb.getRegistrationDate()!=null)
                vehicleResponse.setRegistrationDate(vehicleEsb.getRegistrationDate());*/
            if(vehicleEsb.getSeats()!=null)
                vehicleResponse.setSeats(vehicleEsb.getSeats().intValue());

            return vehicleResponse;
        }
        return null;

    }

    //region NUOVI ADAPTER
    public static ClaimsDamagedVehicleEntity adptVehicleEntityToClDamNewVehicle(Vehicle vehicleJsonb) {

                if (vehicleJsonb != null) {

            ClaimsDamagedVehicleEntity vehicle = new ClaimsDamagedVehicleEntity();
            vehicle.setBodyStyle(vehicleJsonb.getBodyStyle());
            vehicle.setCc3(vehicleJsonb.getCc3());
            vehicle.setChassisNumber(vehicleJsonb.getChassisNumber());
            vehicle.setChassisNumberTrailer(vehicleJsonb.getChassisNumberTrailer());
            vehicle.setCo2emission(vehicleJsonb.getCo2emission());
            vehicle.setConsumption(vehicleJsonb.getConsumption());
            vehicle.setDinHp(vehicleJsonb.getDinHp());
            vehicle.setDoors(vehicleJsonb.getDoors());
            vehicle.setExternalColor(vehicleJsonb.getExternalColor());
            vehicle.setFleetVehicleId(vehicleJsonb.getFleetVehicleId());
            vehicle.setFuelType(vehicleJsonb.getFuelType());
            vehicle.setHp(vehicleJsonb.getHp());
            vehicle.setInternalColor(vehicleJsonb.getInternalColor());
            vehicle.setKw(vehicleJsonb.getKw());
            vehicle.setLastKnownMileage(vehicleJsonb.getLastKnownMileage());
            vehicle.setLicensePlate(vehicleJsonb.getLicensePlate());
            vehicle.setLicensePlateTrailer(vehicleJsonb.getLicensePlateTrailer());
            vehicle.setMake(vehicleJsonb.getMake());
            vehicle.setMaxWeight(vehicleJsonb.getMaxWeight());
            vehicle.setModel(vehicleJsonb.getModel());
            vehicle.setModelYear(vehicleJsonb.getModelYear());
            vehicle.setNature(vehicleJsonb.getNature());
            vehicle.setNetWeight(vehicleJsonb.getNetWeight());
            vehicle.setOwnership(vehicleJsonb.getOwnership());
            vehicle.setRegistrationCountry(vehicleJsonb.getRegistrationCountry());
            vehicle.setRegistrationCountryTrailer(vehicleJsonb.getRegistrationCountryTrailer());
            vehicle.setRegistrationDate(vehicleJsonb.getRegistrationDate());
            vehicle.setSeats(vehicleJsonb.getSeats());
            vehicle.setWreck(vehicleJsonb.getWreck());
            vehicle.setNetBookValue(vehicleJsonb.getNetBookValue());
            vehicle.setWreckDate(DateUtil.convertIS08601StringToUTCInstant(DateUtil.convertUTCDateToIS08601String(vehicleJsonb.getWreckDate())));

            return vehicle;
        }

        return null;

    }

    public static Vehicle adptFromClaimsVehicleToVehicleJsonb(ClaimsDamagedVehicleEntity claimsVehicle) {

        if (claimsVehicle != null) {

            Vehicle vehicle = new Vehicle();
            vehicle.setBodyStyle(claimsVehicle.getBodyStyle());
            vehicle.setCc3(claimsVehicle.getCc3());
            vehicle.setChassisNumber(claimsVehicle.getChassisNumber());
            vehicle.setChassisNumberTrailer(claimsVehicle.getChassisNumberTrailer());
            vehicle.setCo2emission(claimsVehicle.getCo2emission());
            vehicle.setConsumption(claimsVehicle.getConsumption());
            vehicle.setDinHp(claimsVehicle.getDinHp());
            vehicle.setDoors(claimsVehicle.getDoors());
            vehicle.setExternalColor(claimsVehicle.getExternalColor());
            vehicle.setFleetVehicleId(claimsVehicle.getFleetVehicleId());
            vehicle.setFuelType(claimsVehicle.getFuelType());
            vehicle.setHp(claimsVehicle.getHp());
            vehicle.setInternalColor(claimsVehicle.getInternalColor());
            vehicle.setKw(claimsVehicle.getKw());
            vehicle.setLastKnownMileage(claimsVehicle.getLastKnownMileage());
            vehicle.setLicensePlate(claimsVehicle.getLicensePlate());
            vehicle.setLicensePlateTrailer(claimsVehicle.getLicensePlateTrailer());
            vehicle.setMake(claimsVehicle.getMake());
            vehicle.setMaxWeight(claimsVehicle.getMaxWeight());
            vehicle.setModel(claimsVehicle.getModel());
            vehicle.setModelYear(claimsVehicle.getModelYear());
            vehicle.setNature(claimsVehicle.getNature());
            vehicle.setNetWeight(claimsVehicle.getNetWeight());
            vehicle.setOwnership(claimsVehicle.getOwnership());
            vehicle.setRegistrationCountry(claimsVehicle.getRegistrationCountry());
            vehicle.setRegistrationCountryTrailer(claimsVehicle.getRegistrationCountryTrailer());
            vehicle.setRegistrationDate(claimsVehicle.getRegistrationDate());
            vehicle.setSeats(claimsVehicle.getSeats());
            vehicle.setWreck(claimsVehicle.getWreck());
            vehicle.setNetBookValue(claimsVehicle.getNetBookValue());
            vehicle.setWreckDate(DateUtil.convertIS08601StringToUTCDate(DateUtil.convertUTCInstantToIS08601String(claimsVehicle.getWreckDate())));

            return vehicle;
        }

        return null;

    }
    //endregion

}
