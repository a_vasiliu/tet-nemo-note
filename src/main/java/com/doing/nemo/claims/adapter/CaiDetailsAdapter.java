package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.controller.payload.request.cai.CaiDetailsRequest;
import com.doing.nemo.claims.controller.payload.response.cai.CaiDetailsResponse;
import com.doing.nemo.claims.entity.jsonb.cai.CaiDetails;
import org.springframework.stereotype.Component;

@Component
public class  CaiDetailsAdapter {

    public static CaiDetails adptCaiDetailsRequestToCaiDetails(CaiDetailsRequest caiDetailsRequest) {

        if (caiDetailsRequest != null) {

            CaiDetails caiDetails = new CaiDetails();
            caiDetails.setCondition1(new CaiDetails.Pair("1", caiDetailsRequest.getCondition1()));
            caiDetails.setCondition2(new CaiDetails.Pair("2", caiDetailsRequest.getCondition2()));
            caiDetails.setCondition3(new CaiDetails.Pair("3", caiDetailsRequest.getCondition3()));
            caiDetails.setCondition4(new CaiDetails.Pair("4", caiDetailsRequest.getCondition4()));
            caiDetails.setCondition5(new CaiDetails.Pair("5", caiDetailsRequest.getCondition5()));
            caiDetails.setCondition6(new CaiDetails.Pair("6", caiDetailsRequest.getCondition6()));
            caiDetails.setCondition7(new CaiDetails.Pair("7", caiDetailsRequest.getCondition7()));
            caiDetails.setCondition8(new CaiDetails.Pair("8", caiDetailsRequest.getCondition8()));
            caiDetails.setCondition9(new CaiDetails.Pair("9", caiDetailsRequest.getCondition9()));
            caiDetails.setCondition10(new CaiDetails.Pair("10", caiDetailsRequest.getCondition10()));
            caiDetails.setCondition11(new CaiDetails.Pair("11", caiDetailsRequest.getCondition11()));
            caiDetails.setCondition12(new CaiDetails.Pair("12", caiDetailsRequest.getCondition12()));
            caiDetails.setCondition13(new CaiDetails.Pair("13", caiDetailsRequest.getCondition13()));
            caiDetails.setCondition14(new CaiDetails.Pair("14", caiDetailsRequest.getCondition14()));
            caiDetails.setCondition15(new CaiDetails.Pair("15", caiDetailsRequest.getCondition15()));
            caiDetails.setCondition16(new CaiDetails.Pair("16", caiDetailsRequest.getCondition16()));
            caiDetails.setCondition17(new CaiDetails.Pair("17", caiDetailsRequest.getCondition17()));


            return caiDetails;
        }
        return null;
    }

    public static CaiDetailsResponse adptCaiDetailsToCaiDetailsResponse(CaiDetails caiDetails) {

        if (caiDetails != null) {

            CaiDetailsResponse caiDetailsResponse = new CaiDetailsResponse();
            caiDetailsResponse.setCondition1(caiDetails.getCondition1().getValue());
            caiDetailsResponse.setCondition2(caiDetails.getCondition2().getValue());
            caiDetailsResponse.setCondition3(caiDetails.getCondition3().getValue());
            caiDetailsResponse.setCondition4(caiDetails.getCondition4().getValue());
            caiDetailsResponse.setCondition5(caiDetails.getCondition5().getValue());
            caiDetailsResponse.setCondition6(caiDetails.getCondition6().getValue());
            caiDetailsResponse.setCondition7(caiDetails.getCondition7().getValue());
            caiDetailsResponse.setCondition8(caiDetails.getCondition8().getValue());
            caiDetailsResponse.setCondition9(caiDetails.getCondition9().getValue());
            caiDetailsResponse.setCondition10(caiDetails.getCondition10().getValue());
            caiDetailsResponse.setCondition11(caiDetails.getCondition11().getValue());
            caiDetailsResponse.setCondition12(caiDetails.getCondition12().getValue());
            caiDetailsResponse.setCondition13(caiDetails.getCondition13().getValue());
            caiDetailsResponse.setCondition14(caiDetails.getCondition14().getValue());
            caiDetailsResponse.setCondition15(caiDetails.getCondition15().getValue());
            caiDetailsResponse.setCondition16(caiDetails.getCondition16().getValue());
            caiDetailsResponse.setCondition17(caiDetails.getCondition17().getValue());

            return caiDetailsResponse;
        }
        return null;
    }
}