package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.entity.enumerated.*;
import com.doing.nemo.claims.entity.enumerated.AuthorityEnum.AdministrativePracticeTypeEnum;
import com.doing.nemo.claims.entity.enumerated.AuthorityEnum.AuthorityPracticeTypeEnum;
import com.doing.nemo.claims.entity.enumerated.AuthorityEnum.CarPracticeTypeEnum;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.enumerated.DamagedEnum.VehicleEnum.VehicleAuthorityNatureEnum;
import com.doing.nemo.claims.entity.enumerated.DamagedEnum.VehicleEnum.VehicleNatureEnum;
import com.doing.nemo.claims.entity.enumerated.DamagedEnum.VehicleEnum.VehicleTypeEnum;
import com.doing.nemo.claims.entity.enumerated.DamagedEnum.WoundedEnum.WoundedTypeEnum;
import com.doing.nemo.claims.entity.enumerated.DamagedEnum.WoundedEnum.WoundedWoundEnum;
import com.doing.nemo.claims.entity.enumerated.RefundEnum.RefundTypeEnum;
import com.doing.nemo.claims.entity.enumerated.RefundEnum.TypeEnum;
import com.doing.nemo.claims.service.impl.AuthorityServiceImpl;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class EnumAdapter {
    private static Logger LOGGER = LoggerFactory.getLogger(EnumAdapter.class);
    public static String translateClaimsStatus(ClaimsStatusEnum claimsStatusEnum) {
        switch (claimsStatusEnum) {
            case CLOSED:
                return "Chiuso senza seguito";
            case INCOMPLETE:
                return "Incompleto";
            case WAITING_FOR_VALIDATION:
                return "Da validare";
            case WAITING_FOR_AUTHORITY:
                return "In attesa";
            case TO_ENTRUST:
                return "Da affidare";
            case WAITING_FOR_REFUND:
                return "In attesa di rimborso";
            case DRAFT:
                return "Bozza";
            case MANAGED:
                return "Autogestito";
            case REJECTED:
                return "Rifiutato";
            case DELETED:
                return "Cancellato";
            case CLOSED_TOTAL_REFUND:
                return "Chiuso rimborsato";
            case TO_SEND_TO_CUSTOMER:
                return "Da inviare al cliente";
            case CLOSED_PARTIAL_REFUND:
                return "Chiuso rimborsato";
            case SEND_TO_CLIENT:
                return "Inviato al cliente";
            case STAMP:
                return "Riponzunatura";
            case KASKO:
                return "Kasko";
            case TO_WORK:
                return "Da lavorare";
            case ACCEPTED:
                return "Accettata";
            case VALIDATED:
                return "Validata";
            case DEMOLITION:
                return "Demolizione";
            case ENTRUSTED_AT:
                return "Affidata a ";
            case LOST_POSSESSION:
                return "Perdita possesso";
            case BOOK_DUPLICATION:
                return "Duplicazione libretto";
            case RETURN_TO_POSSESSION:
                return "Rientro in possesso";
            default:
                return claimsStatusEnum.getValue();
        }
    }

    public static String adptFromEventTypeToItalian(EventTypeEnum eventTypeEnum){
        if(eventTypeEnum == null)
            return "";
        switch (eventTypeEnum){
            case SEND_TO_CLIENT:
                return "Inviato al cliente";
            case NOTE:
                return "Note";
            case ENTRUST:
                return "Affido";
            case REFUNDED:
                return "Rimborsato";
            case INCOMPLETE:
                return "Incompleta";
            case ENTRUST_PAI:
                return "Affido PAI";
            case TO_ENTRUSTED:
                return "Da affidare";
            case EDIT_POLICY_DATA:
                return "Modifica dati polizza";
            case EDIT_PRACTICE_DATA:
                return "Modifica dati denuncia";
            case PARTIAL_REFUND:
                return "Rimborso parziale";
            case IMPORT_NUMBER_SX:
                return "Importo numero sinistro";
            case INCIDENT_SERVICE:
                return "Servizio Incident";
            case ACCOUNTING:
                return  "Contabilità";
            case SEND_WITH_SEQUEL:
                return "Inviata con seguito";
            case SEQUEL_INCOMPLETE:
                return "Seguito incompleto";
            case COMPLAINT_REJECTED:
                return "Denuncia rifiutata";
            case IMPORT_FRANCHISE_1:
                return "Import franchigia 1";
            case IMPORT_FRANCHISE_2:
                return "Import franchigia 2";
            case LEGAL_COMUNICATION:
                return "Comunicazione legale";
            case COMPLAINT_INSERTION:
                return "Inserimento denuncia";
            case COMPLAINT_VALIDATION:
                return "Validazione denuncia";
            case ENTRUST_TO_THE_LEGAL:
                return "Affido al legale";
            case AUTHORITY_ASSOCIATION:
                return "Associazione authority";
            case FLOW_VARIATION_REPAIR:
                return "Variazione flusso repair";
            case CLOSING_WITHOUT_SEQUEL:
                return "Chiusura senza seguito";
            case ENTRUST_TO_THE_COMPANY:
                return "Affido alla compagnia";
            case NOTIFYING_VARIATION_PO:
                return "Notifica variazione PO";
            case POSSESION_LOSS_VEHICLE:
                return "Perdita possesso veicolo";
            case INCIDENT_SERVICE_INSERT:
                return "Inserimento servizio Incident";
            case INCIDENT_SERVICE_UPDATE:
                return "Modifica servizio Incident";
            case STATUS_VARIATION_REPAIR:
                return "Variazione stato Repair";
            case AUTHORITY_DISASSOCIATION:
                return "Disassociazione authority";
            case COUNTERPARTY_CALL_REPAIR:
                return "Recall repair";
            case TAKE_IN_CHARGE_VALIDATED:
                return "Presa in carico validata";
            case TAKE_IN_CHARGE_INCOMPLETE:
                return "Presa in carico incompleta";
            case COMPLAINT_INSERTION_REPAIR:
                return "Inserimento denuncia repair";
            case GENERIC_LEGAL_COMUNICATION:
                return "Comunicazione legale generica";
            case AUTHORITY_GLASS_ASSOCIATION:
                return "Associazione Cristalli Authority";
            case OPERATOR_ASSIGNMENT_REPAIR:
                return "Assegnata ad operatore repair";
            case AUTOMATIC_CLOSING_COMPLAINT:
                return "Chiusura automatica denuncia";
            case GENERIC_MANAGER_COMUNICATION:
                return "Comunicazione generica manager";
            case ENTRUST_TO_THE_CLAIMS_MANAGER:
                return "Affido a gestore sinistri";
            case MANAGEMENT_VARIATION_REPAIR:
                return "Variazione gestione repair";
            case DOCUMENTATION_REQUEST_THEFT:
                return "Richiesta documentazione furto";
            case GENERIC_REPAIRER_COMUNICATION:
                return "Comunicazione riparatore generica";
            case REENTRY_IN_POSSESSION_VEHICLE:
                return "Rientro in possesso veicolo";
            case TAKE_IN_CHARGE_INCOMPLETE_FCM:
                return "Presa in carico incompleta FCM";
            case TAKE_IN_CHARGE_INCOMPLETE_FNI:
                return "Presa in carico incompleta FNI";
            case AUTHORITY_GLASS_DISASSOCIATION:
                return "Disassociazione Cristalli Authority";
            case DISCOVERED_VEHICLE_BEYOND_60GG:
                return "Ritrovamento veicolo oltre 60gg";
            case DISCOVERED_VEHICLE_WITHIN_60GG:
                return "Ritrovamento veicolo prima 60 gg";
            case TAKE_IN_CHARGE_VALIDATED_THEFT:
                return "Presa in carico furto validato";
            case PRACTICAL_DATA_VARIATION_REPAIR:
                return "Variazione dati pratica repair";
            case CONFIRM_SEQUEL_WITH_NOTIFICATION:
                return "Conferma seguito con notifica";
            case REQUIRED_INFORMATION_INCOMPLETE:
                return "Richiesta informazioni incompleta";
            case COMUNICATION_REPAIRER_CANALIZATION:
                return "Comunicazione canalizzazione riparatore";
            case COMUNICATION_REPAIRER_COUNTERPARTY:
                return "Comunicazione controparte riparatore";
            case CONFIRM_SEQUEL_WITHOUT_NOTIFICATION:
                return "Conferma seguito senza notifica";
            case GENERIC_COUNTERPARTY_DRIVER_COMUNICATION:
                return "Comunicazione driver controparte";
            case GENERIC_COUNTERPARTY_INSURED_COMUNICATION:
                return "Comunicazione assicurato controparte";
            case TAKE_IN_CHARGE_VALIDATED_THEFT_AND_DISCOVERY:
                return "Presa in carico furto e ritrovamento validato";
            case INCOMPLETE_FCM:
                return "Incompleta FCM";
            case INCOMPLETE_FNI:
                return "Incompleta FNI";
            case ENTRUSTED_DOWNLOAD:
                return "Download affido";
            case POLICE_EXPIRATION_NOTICE:
                return "Notifica scadenza polizza";
            case FINDING:
                return "Ritrovamento";
            default:
                return eventTypeEnum.getValue();
        }
    }

    public static VehicleAuthorityNatureEnum adptFromVehicleNatureToVehicleAuthorityNature(VehicleNatureEnum vehicleNatureEnum){
        if (vehicleNatureEnum == null) return null;
        switch (vehicleNatureEnum){
            case MOTOR_VEHICLE:
                return VehicleAuthorityNatureEnum.CAR;
            case MOTOR_CYCLE:
                return  VehicleAuthorityNatureEnum.BIKE;
            case VAN:
                return VehicleAuthorityNatureEnum.VAN;
            default:
                throw new BadRequestException("Vehicle nature not valid");
        }
    }

    public static ProcessingTypeEnum adptFromAuthorityPracticeTypeToProcessingType(AuthorityPracticeTypeEnum practiceTypeEnum){
        switch (practiceTypeEnum){
            case STAMP:
                return ProcessingTypeEnum.STAMP;
            case TO_RE_REGISTER:
                return ProcessingTypeEnum.TO_RE_REGISTER;
            default:
                LOGGER.debug("Authority type enum not valid");
                return null;
        }
    }

    public static AuthorityPracticeTypeEnum adptFromAdministrativePracticeTypeToAuthorityPracticeType(AdministrativePracticeTypeEnum practiceTypeEnum){
        switch (practiceTypeEnum){
            case RE_PUNCHING:
                return AuthorityPracticeTypeEnum.STAMP;
            case RE_REGISTRATION:
                return AuthorityPracticeTypeEnum.TO_RE_REGISTER;
            default:
                LOGGER.debug("Administrative practice type enum not valid");
                return null;
        }
    }

    public static ProcessingTypeEnum adptFromAdministrativePracticeTypeToProcessingType(AdministrativePracticeTypeEnum practiceTypeEnum){
        switch (practiceTypeEnum){
            case RE_PUNCHING:
                return ProcessingTypeEnum.STAMP;
            case RE_REGISTRATION:
                return ProcessingTypeEnum.TO_RE_REGISTER;
            default:
                LOGGER.debug("Administrative practice type enum not valid");
                return null;
        }
    }

    public static AdministrativePracticeTypeEnum adptFromProcessingTypeToAdministrativePracticeType(ProcessingTypeEnum practiceTypeEnum){
        switch (practiceTypeEnum){
            case STAMP:
                return AdministrativePracticeTypeEnum.RE_PUNCHING;
            case TO_RE_REGISTER:
                return AdministrativePracticeTypeEnum.RE_REGISTRATION;
            default:
                LOGGER.debug("Administrative practice type enum not valid");
                return null;
        }
    }

    public static List<AdministrativePracticeTypeEnum> adptFromProcessingTypeToAdministrativePracticeType(List<ProcessingTypeEnum> practiceTypeEnum){
        List<AdministrativePracticeTypeEnum> administrativePracticeTypeEnums = new ArrayList<>();
        for(ProcessingTypeEnum processingTypeEnum : practiceTypeEnum){
            administrativePracticeTypeEnums.add(adptFromProcessingTypeToAdministrativePracticeType(processingTypeEnum));
        }
        return administrativePracticeTypeEnums;
    }

    public static CarPracticeTypeEnum adptFromPracticeTypeToCarPracticeType(PracticeTypeEnum practiceTypeEnum){
        switch (practiceTypeEnum){
            case RECOVERY:
                return CarPracticeTypeEnum.RECOVERY;
            case RELEASE_FROM_SEIZURE:
                return CarPracticeTypeEnum.RELEASE_FROM_SEIZURE;
        }
        return null;
    }

    public static String adptFromVehicleTypeToString(VehicleTypeEnum vehicleTypeEnum){
        if(vehicleTypeEnum == null) return "NO";
        switch (vehicleTypeEnum){
            case VEHICLE:
                return "SI";
            default:
                return "NO";
        }
    }

    public static String adptFromVehicleNatureToString(VehicleNatureEnum vehicleNatureEnum){
        if(vehicleNatureEnum == null) return "";
        switch (vehicleNatureEnum){
            case MOTOR_VEHICLE:
                return "AUTO";
            case MOTOR_CYCLE:
                return "MOTO";
            case VAN:
                return "VAN";
            default:
                return "";
        }
    }

    public static String adptFromSplitTypeEnumToString(TypeEnum typeEnum){
        if(typeEnum == null) return "";
        switch (typeEnum){
            case ACC:
                return "ACCONTO";
            case BALANCE:
                return "SALDO";
            default:
                return "";
        }
    }

    public static String adptFromRefundTypeToString(RefundTypeEnum refundTypeEnum){
        if(refundTypeEnum == null) return "";
        switch (refundTypeEnum){
            case ALLOWANCE:
                return "ASSEGNO";
            case BANK_TRANSFER:
                return "BONIFICO BANCARIO";
            default:
                return "";
        }
    }

    public static String adptFromWoundedTypeToString(WoundedTypeEnum woundedTypeEnum){
        if(woundedTypeEnum == null) return "";
        switch (woundedTypeEnum){
            case PASSENGER:
                return "PASSEGGERO";
            case DRIVER:
                return "CONDUCENTE";
            case OTHER:
                return "ALTRO";
            default:
                return "";
        }
    }

    public static String adptFromWoundedWoundToString(WoundedWoundEnum woundedWoundEnum){
        if(woundedWoundEnum == null) return "";
        switch (woundedWoundEnum){
            case NO:
                return "NO";
            case MINOR:
                return "MINORI";
            case SERIOUS:
                return "GRAVI";
            default:
                return "";
        }
    }

    public static String adptFromPracticeStatusToItalianString(PracticeStatusEnum practiceStatusEnum){
        if(practiceStatusEnum == null) return "";
        switch (practiceStatusEnum){
            case OPEN:
                return "APERTA";
            case CLOSED:
                return "CHIUSA";
            default:
                LOGGER.debug("Practice status not valid");
                return "";
        }
    }
}
