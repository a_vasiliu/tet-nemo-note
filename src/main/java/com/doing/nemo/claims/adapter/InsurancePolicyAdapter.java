package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.InsurancePolicyRequestV1;
import com.doing.nemo.claims.controller.payload.response.ClaimsResponsePaginationV1;
import com.doing.nemo.claims.controller.payload.response.InsurancePolicyResponseV1;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.settings.InsuranceCompanyEntity;
import com.doing.nemo.claims.entity.settings.InsuranceManagerEntity;
import com.doing.nemo.claims.entity.settings.InsurancePolicyEntity;
import com.doing.nemo.claims.exception.NotFoundException;
import com.doing.nemo.claims.repository.InsuranceCompanyRepository;
import com.doing.nemo.claims.repository.InsuranceManagerRepository;
import com.doing.nemo.claims.repository.InsurancePolicyRepository;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
public class InsurancePolicyAdapter {

    @Autowired
    private InsuranceCompanyRepository insuranceCompanyRepository;

    @Autowired
    private InsurancePolicyRepository insurancePolicyRepository;

    @Autowired
    private InsuranceManagerRepository insuranceManagerRepository;

    @Autowired
    private EventAdapter eventAdapter;


    private static Logger LOGGER = LoggerFactory.getLogger(InsurancePolicyAdapter.class);

    public InsurancePolicyEntity adptFromIPolicyRequToIPolicyEntityWithID(InsurancePolicyRequestV1 insurancePolicyRequestV1, UUID uuid) {
        Optional<InsurancePolicyEntity> insurancePolicyEntityOptional = insurancePolicyRepository.findById(uuid);
        if (!insurancePolicyEntityOptional.isPresent())
        {
            LOGGER.debug("Insurance policy with id " + uuid + " not found");
            throw new NotFoundException("Insurance policy with id " + uuid + " not found", com.doing.nemo.claims.validation.MessageCode.CLAIMS_1010);
        }
        InsurancePolicyEntity insurancePolicyEntity = insurancePolicyEntityOptional.get();
        InsurancePolicyEntity insurancePolicyEntity1 = adptInsurancePolicyRequestToInsurancePolicy(insurancePolicyRequestV1);
        insurancePolicyEntity1.setId(insurancePolicyEntity.getId());
        insurancePolicyEntity1.setInsurancePolicyId(insurancePolicyEntity.getInsurancePolicyId());

        return insurancePolicyEntity1;
    }

    public InsurancePolicyEntity adptInsurancePolicyRequestToInsurancePolicy(InsurancePolicyRequestV1 insurancePolicyRequest) {

        if(insurancePolicyRequest == null)
            return null;

        InsurancePolicyEntity insurancePolicyEntity = new InsurancePolicyEntity();

        if (insurancePolicyRequest.getInsuranceCompanyEntity() != null) {

            Optional<InsuranceCompanyEntity> insuranceCompanyEntity = insuranceCompanyRepository.findById(insurancePolicyRequest.getInsuranceCompanyEntity().getId());

            if (insuranceCompanyEntity.isPresent())
                insurancePolicyEntity.setInsuranceCompanyEntity(insuranceCompanyEntity.get());
        }

        if (insurancePolicyRequest.getInsuranceManagerEntity() != null) {

            Optional<InsuranceManagerEntity> insuranceManagerEntity = insuranceManagerRepository.findById(insurancePolicyRequest.getInsuranceManagerEntity().getId());

            if (insuranceManagerEntity.isPresent())
                insurancePolicyEntity.setInsuranceManagerEntity(insuranceManagerEntity.get());
        }

        insurancePolicyEntity.setType(insurancePolicyRequest.getType());
        insurancePolicyEntity.setDescription(insurancePolicyRequest.getDescription());
        insurancePolicyEntity.setNumberPolicy(insurancePolicyRequest.getNumberPolicy());
        insurancePolicyEntity.setClientCode(insurancePolicyRequest.getClientCode());
        insurancePolicyEntity.setClient(insurancePolicyRequest.getClient());
        insurancePolicyEntity.setAnnotations(insurancePolicyRequest.getAnnotations());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX");

        if(insurancePolicyRequest.getBeginningValidity() != null){
            String formattedBeginning = simpleDateFormat.format(insurancePolicyRequest.getBeginningValidity());
            insurancePolicyEntity.setBeginningValidity(DateUtil.convertIS08601StringToUTCInstant(formattedBeginning));
        }

        if(insurancePolicyRequest.getEndValidity() != null ){
            String formattedEnd = simpleDateFormat.format(insurancePolicyRequest.getEndValidity());
            insurancePolicyEntity.setEndValidity(DateUtil.convertIS08601StringToUTCInstant(formattedEnd));
        }
        insurancePolicyEntity.setBookRegister(insurancePolicyRequest.getBookRegister());
        insurancePolicyEntity.setBookRegisterCode(insurancePolicyRequest.getBookRegisterCode());
        insurancePolicyEntity.setLastRenewalNotification(DateUtil.convertIS08601StringToUTCInstant(insurancePolicyRequest.getLastRenewalNotification()));
        insurancePolicyEntity.setRenewalNotification(insurancePolicyRequest.getRenewalNotification());

        if (insurancePolicyRequest.getRiskEntityList() != null && !insurancePolicyRequest.getRiskEntityList().isEmpty()) {
            insurancePolicyEntity.setRiskEntityList(RiskAdapter.adptFromRiskRequestToRiskEntityList(insurancePolicyRequest.getRiskEntityList()));
        }

        //insurancePolicyEntity.setAttachmentList(insurancePolicyRequest.getAttachmentEntityList());

        if (insurancePolicyRequest.getEventEntityList() != null && !insurancePolicyRequest.getEventEntityList().isEmpty())
            insurancePolicyEntity.setEventEntityList(eventAdapter.adptFromEventRequestToEventEntityList(insurancePolicyRequest.getEventEntityList()));

        insurancePolicyEntity.setActive(insurancePolicyRequest.getActive());
        insurancePolicyEntity.setFranchise(insurancePolicyRequest.getFranchise());

        return insurancePolicyEntity;

    }

    public static InsurancePolicyResponseV1 adptFromIPolicyEntityToIPolicyRespo(InsurancePolicyEntity insurancePolicyEntity) {
        if (insurancePolicyEntity == null) return null;
        InsurancePolicyResponseV1 insurancePolicyResponseV1 = new InsurancePolicyResponseV1();
        insurancePolicyResponseV1.setId(insurancePolicyEntity.getId());
        insurancePolicyResponseV1.setType(insurancePolicyEntity.getType());
        insurancePolicyResponseV1.setDescription(insurancePolicyEntity.getDescription());
        insurancePolicyResponseV1.setNumberPolicy(insurancePolicyEntity.getNumberPolicy());
        insurancePolicyResponseV1.setInsuranceCompanyEntity(insurancePolicyEntity.getInsuranceCompanyEntity());
        insurancePolicyResponseV1.setInsuranceManagerEntity(insurancePolicyEntity.getInsuranceManagerEntity());
        insurancePolicyResponseV1.setClientCode(insurancePolicyEntity.getClientCode());
        insurancePolicyResponseV1.setClient(insurancePolicyEntity.getClient());
        insurancePolicyResponseV1.setAnnotations(insurancePolicyEntity.getAnnotations());
        insurancePolicyResponseV1.setBeginningValidity(DateUtil.convertIS08601StringToUTCDate(DateUtil.convertUTCInstantToIS08601String(insurancePolicyEntity.getBeginningValidity())));
        insurancePolicyResponseV1.setEventEntityList(insurancePolicyEntity.getEventEntityList());
        insurancePolicyResponseV1.setEndValidity(DateUtil.convertIS08601StringToUTCDate(DateUtil.convertUTCInstantToIS08601String(insurancePolicyEntity.getEndValidity())));
        insurancePolicyResponseV1.setAttachmentEntityList(AttachmentAdapter.adptAttachmentToAttachmentResponse(insurancePolicyEntity.getAttachmentList()));
        insurancePolicyResponseV1.setRiskEntityList(RiskAdapter.adptRiskToRiskResponse(insurancePolicyEntity.getRiskEntityList()));
        insurancePolicyResponseV1.setBookRegister(insurancePolicyEntity.getBookRegister());
        insurancePolicyResponseV1.setBookRegisterCode(insurancePolicyEntity.getBookRegisterCode());
        insurancePolicyResponseV1.setLastRenewalNotification(DateUtil.convertUTCInstantToIS08601String(insurancePolicyEntity.getLastRenewalNotification()));
        insurancePolicyResponseV1.setRenewalNotification(insurancePolicyEntity.getRenewalNotification());
        insurancePolicyResponseV1.setActive(insurancePolicyEntity.getActive());
        insurancePolicyResponseV1.setFranchise(insurancePolicyEntity.getFranchise());

        return insurancePolicyResponseV1;
    }

    public static List<InsurancePolicyResponseV1> adptFromIPolicyEntityToIPolicyRespoList(List<InsurancePolicyEntity> insurancePolicyEntities) {
        List<InsurancePolicyResponseV1> insurancePolicyResponseV1List = new ArrayList<>();
        for (InsurancePolicyEntity insurancePolicyEntity : insurancePolicyEntities) {
            insurancePolicyResponseV1List.add(adptFromIPolicyEntityToIPolicyRespo(insurancePolicyEntity));
        }
        return insurancePolicyResponseV1List;
    }

    public static ClaimsResponsePaginationV1 adptInsurancePolicyPaginationToClaimsResponsePagination(Pagination<InsurancePolicyEntity> pagination) {

        ClaimsResponsePaginationV1<InsurancePolicyResponseV1> claimsPaginationResponse = new ClaimsResponsePaginationV1<>();

        claimsPaginationResponse.setStats(pagination.getStats());
        claimsPaginationResponse.setItems(adptFromIPolicyEntityToIPolicyRespoList(pagination.getItems()));

        return claimsPaginationResponse;
    }
}
