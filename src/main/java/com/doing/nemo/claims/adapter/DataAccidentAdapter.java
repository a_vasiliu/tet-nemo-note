package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.complaint.DataAccidentRequest;
import com.doing.nemo.claims.controller.payload.response.authority.claims.DataAccidentAuthorityResponseV1;
import com.doing.nemo.claims.controller.payload.response.complaint.DataAccidentResponse;
import com.doing.nemo.claims.entity.jsonb.complaint.jsonbComplaint.jsonbDataAccidents.DataAccident;
import org.springframework.stereotype.Component;

@Component
public class DataAccidentAdapter {

    public static DataAccident adptDataAccidentRequestToDataAccident(DataAccidentRequest dataAccidentRequest) {

        if (dataAccidentRequest != null) {

            DataAccident dataAccident = new DataAccident();
            dataAccident.setAddress(dataAccidentRequest.getAddress());
            dataAccident.setAuthorityData(dataAccidentRequest.getAuthorityData());
            dataAccident.setCc(dataAccidentRequest.getCc());
            dataAccident.setCenterNotified(dataAccidentRequest.getCenterNotified());
            dataAccident.setDamageToObjects(dataAccidentRequest.getDamageToObjects());
            dataAccident.setDamageToVehicles(dataAccidentRequest.getDamageToVehicles());
            dataAccident.setDateAccident(DateUtil.convertIS08601StringToUTCDate(dataAccidentRequest.getDateAccident()));
            dataAccident.setHappenedAbroad(dataAccidentRequest.getHappenedAbroad());
            dataAccident.setIncompleteMotivation(dataAccidentRequest.getIncompleteMotivation());
            dataAccident.setHappenedOnCenter(dataAccidentRequest.getHappenedOnCenter());
            dataAccident.setInterventionAuthority(dataAccidentRequest.getInterventionAuthority());
            dataAccident.setItemToReceive(ReceivedItemAdapter.adptReceivedItemRequestToReceivedItem(dataAccidentRequest.getItemToReceive()));
            dataAccident.setOldMotorcyclePlates(dataAccidentRequest.getOldMotorcyclePlates());
            dataAccident.setPolice(dataAccidentRequest.getPolice());
            dataAccident.setProviderCode(dataAccidentRequest.getProviderCode());
            dataAccident.setRecoverability(dataAccidentRequest.getRecoverability());
            dataAccident.setRecoverabilityPercent(dataAccidentRequest.getRecoverabilityPercent());
            dataAccident.setResponsible(dataAccidentRequest.getResponsible());
            dataAccident.setRobbery(dataAccidentRequest.getRobbery());
            dataAccident.setTheftDescription(dataAccidentRequest.getTheftDescription());
            dataAccident.setTypeAccident(dataAccidentRequest.getTypeAccident());
            dataAccident.setVvuu(dataAccidentRequest.getVvuu());
            dataAccident.setWithCounterparty(dataAccidentRequest.getWithCounterparty());
            dataAccident.setWitnessDescription(dataAccidentRequest.getWitnessDescription());

            return dataAccident;
        }
        return null;
    }

    public static DataAccidentResponse adptDataAccidentToDataAccidentResponse(DataAccident dataAccident) {

        if (dataAccident != null) {

            DataAccidentResponse dataAccidentResponse = new DataAccidentResponse();
            dataAccidentResponse.setAddress(dataAccident.getAddress());
            dataAccidentResponse.setAuthorityData(dataAccident.getAuthorityData());
            dataAccidentResponse.setCc(dataAccident.getCc());
            dataAccidentResponse.setCenterNotified(dataAccident.getCenterNotified());
            dataAccidentResponse.setDamageToObjects(dataAccident.getDamageToObjects());
            dataAccidentResponse.setDamageToVehicles(dataAccident.getDamageToVehicles());
            dataAccidentResponse.setDateAccident(DateUtil.convertUTCDateToIS08601String(dataAccident.getDateAccident()));
            dataAccidentResponse.setHappenedAbroad(dataAccident.getHappenedAbroad());
            dataAccidentResponse.setIncompleteMotivation(dataAccident.getIncompleteMotivation());
            dataAccidentResponse.setHappenedOnCenter(dataAccident.getHappenedOnCenter());
            dataAccidentResponse.setInterventionAuthority(dataAccident.getInterventionAuthority());
            dataAccidentResponse.setItemToReceive(ReceivedItemAdapter.adptReceivedItemToReceivedItemResponse(dataAccident.getItemToReceive()));
            dataAccidentResponse.setOldMotorcyclePlates(dataAccident.getOldMotorcyclePlates());
            dataAccidentResponse.setPolice(dataAccident.getPolice());
            dataAccidentResponse.setProviderCode(dataAccident.getProviderCode());
            dataAccidentResponse.setRecoverability(dataAccident.getRecoverability());
            dataAccidentResponse.setRecoverabilityPercent(dataAccident.getRecoverabilityPercent());
            dataAccidentResponse.setResponsible(dataAccident.getResponsible());
            dataAccidentResponse.setRobbery(dataAccident.getRobbery());
            dataAccidentResponse.setTheftDescription(dataAccident.getTheftDescription());
            dataAccidentResponse.setTypeAccident(dataAccident.getTypeAccident());
            dataAccidentResponse.setVvuu(dataAccident.getVvuu());
            dataAccidentResponse.setWithCounterparty(dataAccident.getWithCounterparty());
            dataAccidentResponse.setWitnessDescription(dataAccident.getWitnessDescription());

            return dataAccidentResponse;
        }
        return null;
    }

    public static DataAccidentAuthorityResponseV1 adptFromDataAccidentToDataAccidentAuthorityResponse(DataAccident dataAccident){
        if (dataAccident == null) return null;
        DataAccidentAuthorityResponseV1 dataAccidentAuthorityResponseV1 = new DataAccidentAuthorityResponseV1();
        dataAccidentAuthorityResponseV1.setTypeAccident(dataAccident.getTypeAccident());
        dataAccidentAuthorityResponseV1.setDateAccident(DateUtil.convertUTCDateToIS08601String(dataAccident.getDateAccident()));

        return dataAccidentAuthorityResponseV1;
    }


}
