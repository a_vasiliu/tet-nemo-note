package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.controller.payload.request.ClaimsInsertEnjoyRequest.ComplaintEnjoyRequest;
import com.doing.nemo.claims.controller.payload.request.ClaimsInsertExternalRequest;
import com.doing.nemo.claims.controller.payload.request.claims.ComplaintRequest;
import com.doing.nemo.claims.controller.payload.response.authority.claims.ComplaintAuthorityResponseV1;
import com.doing.nemo.claims.controller.payload.response.claims.ComplaintResponse;
import com.doing.nemo.claims.controller.payload.response.external.ComplaintExternalResponse;
import com.doing.nemo.claims.entity.jsonb.complaint.Complaint;
import org.springframework.stereotype.Component;

@Component
public class ComplaintAdapter {

    public static ComplaintResponse adptComplaintToComplaintResponse(Complaint complaint) {

        if (complaint != null) {

            ComplaintResponse complaintResponse = new ComplaintResponse();
            complaintResponse.setActivation(complaint.getActivation());
            complaintResponse.setClientId(complaint.getClientId());
            complaintResponse.setDataAccident(DataAccidentAdapter.adptDataAccidentToDataAccidentResponse(complaint.getDataAccident()));
            complaintResponse.setEntrusted(EntrustedAdapter.adptEntrustedToEntrustedREsponse(complaint.getEntrusted()));
            complaintResponse.setFromCompany(FromCompanyAdapter.adptFromCompanyToFromCompanyFromCompanyResponse(complaint.getFromCompany()));
            complaintResponse.setLocator(complaint.getLocator());
            complaintResponse.setMod(complaint.getMod());
            complaintResponse.setNotification(complaint.getNotification());
            complaintResponse.setPlate(complaint.getPlate());
            complaintResponse.setProperty(complaint.getProperty());
            complaintResponse.setQuote(complaint.getQuote());
            complaintResponse.setRepair(RepairAdapter.adptRepairToRepairReaponse(complaint.getRepair()));

            return complaintResponse;
        }
        return null;

    }

    public static ComplaintExternalResponse adptComplaintToComplaintExternalResponse(Complaint complaint) {

        if (complaint != null) {

            ComplaintExternalResponse complaintResponse = new ComplaintExternalResponse();
            complaintResponse.setActivation(complaint.getActivation());
            complaintResponse.setDataAccident(DataAccidentAdapter.adptDataAccidentToDataAccidentResponse(complaint.getDataAccident()));
            complaintResponse.setEntrusted(EntrustedAdapter.adptEntrustedToEntrustedREsponse(complaint.getEntrusted()));
            complaintResponse.setFromCompany(FromCompanyAdapter.adptFromCompanyToFromCompanyFromCompanyResponse(complaint.getFromCompany()));
            complaintResponse.setLocator(complaint.getLocator());
            complaintResponse.setMod(complaint.getMod());
            complaintResponse.setNotification(complaint.getNotification());
            complaintResponse.setPlate(complaint.getPlate());
            complaintResponse.setProperty(complaint.getProperty());
            complaintResponse.setQuote(complaint.getQuote());
            complaintResponse.setRepair(RepairAdapter.adptRepairToRepairExternalResponse(complaint.getRepair()));

            return complaintResponse;
        }
        return null;

    }

    public static Complaint adptComplaintRequestToComplaint(ComplaintRequest complaintRequest) {

        if (complaintRequest != null) {

            Complaint complaint = new Complaint();

            complaint.setActivation(complaintRequest.getActivation());
            complaint.setClientId(complaintRequest.getClientId());
            complaint.setDataAccident(DataAccidentAdapter.adptDataAccidentRequestToDataAccident(complaintRequest.getDataAccident()));
            complaint.setEntrusted(EntrustedAdapter.adptEntrustedRequestToEntrusted(complaintRequest.getEntrusted()));
            complaint.setFromCompany(FromCompanyAdapter.adptFromCompanyRequestToFromCompany(complaintRequest.getFromCompany()));
            complaint.setLocator(complaintRequest.getLocator());
            complaint.setMod(complaintRequest.getMod());
            complaint.setNotification(complaintRequest.getNotification());
            complaint.setPlate(complaintRequest.getPlate());
            complaint.setProperty(complaintRequest.getProperty());
            complaint.setQuote(complaintRequest.getQuote());
            complaint.setRepair(RepairAdapter.adptRepairRequestToRepair(complaintRequest.getRepair()));

            return complaint;
        }
        return null;
    }

    public static Complaint adptComplaintRequestToComplaintUpdate(ComplaintRequest complaintRequest, Complaint oldComplaint) {

        if (complaintRequest != null) {

            Complaint complaint = new Complaint();

            complaint.setActivation(complaintRequest.getActivation());
            complaint.setClientId(complaintRequest.getClientId());
            complaint.setDataAccident(DataAccidentAdapter.adptDataAccidentRequestToDataAccident(complaintRequest.getDataAccident()));
            if(oldComplaint != null) {
                complaint.setEntrusted(EntrustedAdapter.adptEntrustedRequestToEntrustedUpdate(complaintRequest.getEntrusted(), oldComplaint.getEntrusted()));
                complaint.setFromCompany(FromCompanyAdapter.adptFromCompanyRequestToFromCompanyAdmin(complaintRequest.getFromCompany(), oldComplaint.getFromCompany()));
            } else {
                complaint.setEntrusted(EntrustedAdapter.adptEntrustedRequestToEntrustedUpdate(complaintRequest.getEntrusted(), null));
                complaint.setFromCompany(FromCompanyAdapter.adptFromCompanyRequestToFromCompanyAdmin(complaintRequest.getFromCompany(), null));
            }
            complaint.setLocator(complaintRequest.getLocator());
            complaint.setMod(complaintRequest.getMod());
            complaint.setNotification(complaintRequest.getNotification());
            complaint.setPlate(complaintRequest.getPlate());
            complaint.setProperty(complaintRequest.getProperty());
            complaint.setQuote(complaintRequest.getQuote());
            complaint.setRepair(RepairAdapter.adptRepairRequestToRepair(complaintRequest.getRepair()));

            return complaint;
        }
        return null;
    }

    public static Complaint adptComplaintExternalRequestToComplaint(ClaimsInsertExternalRequest.ComplaintExternalRequest complaintRequest) {

        if (complaintRequest != null) {

            Complaint complaint = new Complaint();

            complaint.setActivation(complaintRequest.getActivation());
            complaint.setDataAccident(DataAccidentAdapter.adptDataAccidentRequestToDataAccident(complaintRequest.getDataAccident()));
            complaint.setFromCompany(FromCompanyAdapter.adptFromCompanyRequestToFromCompany(complaintRequest.getFromCompany()));
            complaint.setLocator(complaintRequest.getLocator());
            complaint.setMod(complaintRequest.getMod());
            complaint.setNotification(complaintRequest.getNotification());
            complaint.setPlate(complaintRequest.getPlate());
            complaint.setProperty(complaintRequest.getProperty());
            complaint.setQuote(complaintRequest.getQuote());
            complaint.setRepair(RepairAdapter.adptRepairExternalRequestToRepair(complaintRequest.getRepair()));

            return complaint;
        }
        return null;
    }

    public static ComplaintAuthorityResponseV1 adptFromComplaintToComplaintAuthorityResponse(Complaint complaint){
        if(complaint == null) return null;
        ComplaintAuthorityResponseV1 complaintAuthorityResponseV1 = new ComplaintAuthorityResponseV1();
        complaintAuthorityResponseV1.setDataAccident(DataAccidentAdapter.adptFromDataAccidentToDataAccidentAuthorityResponse(complaint.getDataAccident()));

        return complaintAuthorityResponseV1;
    }

    public static Complaint adtpFromEnjoyComplaintToComplaint(ComplaintEnjoyRequest complaintRequest){
        if (complaintRequest != null) {

            Complaint complaint = new Complaint();

            complaint.setActivation(complaintRequest.getActivation());
            complaint.setDataAccident(DataAccidentAdapter.adptDataAccidentRequestToDataAccident(complaintRequest.getDataAccident()));
            complaint.setLocator(complaintRequest.getLocator());
            complaint.setMod(complaintRequest.getMod());
            complaint.setNotification(complaintRequest.getNotification());
            complaint.setPlate(complaintRequest.getPlate());
            complaint.setProperty(complaintRequest.getProperty());
            complaint.setQuote(complaintRequest.getQuote());
            return complaint;
        }
        return null;
    }
}
