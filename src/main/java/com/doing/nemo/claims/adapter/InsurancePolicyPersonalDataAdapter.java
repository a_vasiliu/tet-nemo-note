package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.InsurancePolicyPersonalDataRequestV1;
import com.doing.nemo.claims.entity.settings.BrokerEntity;
import com.doing.nemo.claims.entity.settings.InsuranceCompanyEntity;
import com.doing.nemo.claims.entity.settings.InsuranceManagerEntity;
import com.doing.nemo.claims.entity.settings.InsurancePolicyPersonalDataEntity;
import com.doing.nemo.claims.repository.BrokerRepository;
import com.doing.nemo.claims.repository.InsuranceCompanyRepository;
import com.doing.nemo.claims.repository.InsuranceManagerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Component
public class InsurancePolicyPersonalDataAdapter {

    @Autowired
    private InsuranceCompanyRepository insuranceCompanyRepository;

    @Autowired
    private InsuranceManagerRepository insuranceManagerRepository;

    @Autowired
    private BrokerRepository brokerRepository;

    public InsurancePolicyPersonalDataEntity adptFromIPolicyRequToIPolicyEntity(InsurancePolicyPersonalDataRequestV1 insurancePolicyRequestV1) {
        InsurancePolicyPersonalDataEntity insurancePolicyEntity = new InsurancePolicyPersonalDataEntity();
        if (insurancePolicyRequestV1.getInsuranceCompany() != null) {
            Optional<InsuranceCompanyEntity> insuranceCompanyEntity = insuranceCompanyRepository.findById(insurancePolicyRequestV1.getInsuranceCompany().getId());
            if (insuranceCompanyEntity.isPresent())
                insurancePolicyEntity.setInsuranceCompanyEntity(insuranceCompanyEntity.get());
        }
        if (insurancePolicyRequestV1.getInsuranceManager() != null) {
            Optional<InsuranceManagerEntity> insuranceManagerEntity = insuranceManagerRepository.findById(insurancePolicyRequestV1.getInsuranceManager().getId());
            if (insuranceManagerEntity.isPresent())
                insurancePolicyEntity.setInsuranceManagerEntity(insuranceManagerEntity.get());
        }
        if (insurancePolicyRequestV1.getBroker() != null) {
            Optional<BrokerEntity> brokerEntity = brokerRepository.findById(insurancePolicyRequestV1.getBroker().getId());
            if (brokerEntity.isPresent())
                insurancePolicyEntity.setBrokerEntity(brokerEntity.get());
        }
        insurancePolicyEntity.setType(insurancePolicyRequestV1.getType());
        insurancePolicyEntity.setDescription(insurancePolicyRequestV1.getDescription());
        insurancePolicyEntity.setNumberPolicy(insurancePolicyRequestV1.getNumberPolicy());
        insurancePolicyEntity.setAnnotations(insurancePolicyRequestV1.getAnnotations());
        insurancePolicyEntity.setBeginningValidity(DateUtil.convertIS08601StringToUTCInstant(insurancePolicyRequestV1.getBeginningValidity()));
        insurancePolicyEntity.setEndValidity(DateUtil.convertIS08601StringToUTCInstant(insurancePolicyRequestV1.getEndValidity()));
        insurancePolicyEntity.setBookRegister(insurancePolicyRequestV1.getBookRegister());
        insurancePolicyEntity.setBookRegisterCode(insurancePolicyRequestV1.getBookRegisterCode());
        insurancePolicyEntity.setBookRegisterPaiCode(insurancePolicyRequestV1.getBookRegisterPaiCode());
        insurancePolicyEntity.setCrystalsFr(insurancePolicyRequestV1.getCrystalsFr());
        insurancePolicyEntity.setFurinc(insurancePolicyRequestV1.getFurinc());
        insurancePolicyEntity.setMassimal(insurancePolicyRequestV1.getMassimal());
        insurancePolicyEntity.setFurincUncover(insurancePolicyRequestV1.getFurincUncover());
        insurancePolicyEntity.setKaskoFr(insurancePolicyRequestV1.getKaskoFr());
        insurancePolicyEntity.setCrystalsMax(insurancePolicyRequestV1.getCrystalsMax());

        return insurancePolicyEntity;

    }

    public List<InsurancePolicyPersonalDataEntity> adptFromInsurancePolicyPersonalDataRequestToInsurancePolicyPersonalDataEntityList(List<InsurancePolicyPersonalDataRequestV1> insurancePolicyPersonalDataRequestV1List) {
        List<InsurancePolicyPersonalDataEntity> insurancePolicyPersonalDataEntityList = new LinkedList<>();
        if (insurancePolicyPersonalDataRequestV1List != null) {
            for (InsurancePolicyPersonalDataRequestV1 insurancePolicyPersonalDatasRequest : insurancePolicyPersonalDataRequestV1List) {
                insurancePolicyPersonalDataEntityList.add(this.adptFromIPolicyRequToIPolicyEntity(insurancePolicyPersonalDatasRequest));
            }
        }
        return insurancePolicyPersonalDataEntityList;
    }
}
