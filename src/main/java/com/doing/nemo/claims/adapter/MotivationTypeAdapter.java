package com.doing.nemo.claims.adapter;


import com.doing.nemo.claims.controller.payload.request.MotivationTypeRequestV1;
import com.doing.nemo.claims.controller.payload.response.MotivationTypeResponseV1;
import com.doing.nemo.claims.controller.payload.response.PaginationResponseV1;
import com.doing.nemo.claims.entity.enumerated.DTO.PageStats;
import com.doing.nemo.claims.entity.settings.MotivationTypeEntity;
import com.doing.nemo.claims.exception.NotFoundException;
import com.doing.nemo.claims.repository.MotivationTypeRepository;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
public class MotivationTypeAdapter {

    @Autowired
    private MotivationTypeRepository motivationTypeRepository;

    private static Logger LOGGER = LoggerFactory.getLogger(MotivationTypeAdapter.class);


    public MotivationTypeEntity adptFromMotivationTypeRequestToMotivationTypeEntityWithID(MotivationTypeRequestV1 motivationTypeRequestV1, UUID uuid) {
        Optional<MotivationTypeEntity> motivationTypeEntityOptional = motivationTypeRepository.findById(uuid);
        if (!motivationTypeEntityOptional.isPresent())
        {
            LOGGER.debug("Motivation type with id " + uuid + " not found");
            throw new NotFoundException("Motivation type with id " + uuid + " not found", com.doing.nemo.claims.validation.MessageCode.CLAIMS_1010);
        }
        MotivationTypeEntity motivationTypeEntity = motivationTypeEntityOptional.get();
        MotivationTypeEntity motivationTypeEntity1 = adptFromMotivationTypeRequestToMotivationTypeEntity(motivationTypeRequestV1);
        motivationTypeEntity1.setId(motivationTypeEntity.getId());
        motivationTypeEntity1.setMotivationTypeId(motivationTypeEntity.getMotivationTypeId());


        return motivationTypeEntity1;
    }

    public static MotivationTypeEntity adptFromMotivationTypeRequestToMotivationTypeEntity(MotivationTypeRequestV1 motivationTypeRequestV1) {
        MotivationTypeEntity motivationTypeEntity = new MotivationTypeEntity();
        motivationTypeEntity.setDescription(motivationTypeRequestV1.getDescription());
        motivationTypeEntity.setOrderId(motivationTypeRequestV1.getOrder());
        motivationTypeEntity.setActive(motivationTypeRequestV1.getActive());

        return motivationTypeEntity;
    }

    public static List<MotivationTypeEntity> adptFromMotivationTypeRequestToMotivationTypeEntityList(List<MotivationTypeRequestV1> motivationTypeRequestV1List) {
        List<MotivationTypeEntity> motivationTypeEntities = new ArrayList<>();
        for (MotivationTypeRequestV1 motivationTypeRequestV1 : motivationTypeRequestV1List) {
            MotivationTypeEntity motivationTypeEntity = adptFromMotivationTypeRequestToMotivationTypeEntity(motivationTypeRequestV1);
            motivationTypeEntities.add(motivationTypeEntity);
        }
        return motivationTypeEntities;
    }

    public static MotivationTypeResponseV1 adptFromMotivationTypeEntityToMotivationTypeResponse(MotivationTypeEntity motivationTypeEntity) {
        if (motivationTypeEntity == null) return null;
        MotivationTypeResponseV1 motivationTypeResponseV1 = new MotivationTypeResponseV1();
        motivationTypeResponseV1.setId(motivationTypeEntity.getId());
        motivationTypeResponseV1.setDescription(motivationTypeEntity.getDescription());
        motivationTypeResponseV1.setOrder(motivationTypeEntity.getOrderId());
        motivationTypeResponseV1.setMotivationTypeId(motivationTypeEntity.getMotivationTypeId());
        motivationTypeResponseV1.setActive(motivationTypeEntity.getActive());

        return motivationTypeResponseV1;
    }

    public static List<MotivationTypeResponseV1> adptFromMotivationTypeEntityToMotivationTypeResponseList(List<MotivationTypeEntity> motivationTypeEntities) {
        List<MotivationTypeResponseV1> motivationTypeResponseV1List = new ArrayList<>();
        for (MotivationTypeEntity motivationTypeEntity : motivationTypeEntities) {
            motivationTypeResponseV1List.add(adptFromMotivationTypeEntityToMotivationTypeResponse(motivationTypeEntity));
        }
        return motivationTypeResponseV1List;
    }

    public static PaginationResponseV1<MotivationTypeResponseV1> adptPagination(List<MotivationTypeEntity> motivationTypeEntityList, int page, int pageSize, Long itemCount) {

        double totPages = Math.ceil((double) itemCount.intValue() / pageSize);
        PageStats pageStats = new PageStats();
        pageStats.setCurrentPage(page);
        pageStats.setItemCount(itemCount.intValue());
        pageStats.setPageSize(pageSize);
        pageStats.setPageCount((long)totPages);

        PaginationResponseV1<MotivationTypeResponseV1> paginationResponseV1 = new PaginationResponseV1(pageStats,adptFromMotivationTypeEntityToMotivationTypeResponseList(motivationTypeEntityList));

        return paginationResponseV1;
    }
}
