package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.controller.payload.request.ManagerRequestV1;
import com.doing.nemo.claims.controller.payload.response.ManagerResponseV1;
import com.doing.nemo.claims.controller.payload.response.PaginationResponseV1;
import com.doing.nemo.claims.entity.claims.ClaimsMetadataEntity;
import com.doing.nemo.claims.entity.enumerated.DTO.PageStats;
import com.doing.nemo.claims.entity.jsonb.MetadataClaim;
import com.doing.nemo.claims.entity.jsonb.metadata.Metadata;
import com.doing.nemo.claims.entity.settings.ManagerEntity;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Component
public class MetadataAdapter {

    public static ClaimsMetadataEntity adptFromMetadataRequestToMetadataEntity (Map<String,Object> metadataRequest) {

        ClaimsMetadataEntity claimsMetadataEntity = null;
        if(metadataRequest!=null){
            claimsMetadataEntity = new ClaimsMetadataEntity();
            if(metadataRequest.get("role")!=null){
                claimsMetadataEntity.setRole((String)metadataRequest.get("role"));
            }
            if(metadataRequest.get("email")!=null){
                claimsMetadataEntity.setEmail((String)metadataRequest.get("email"));
            }
            if(metadataRequest.get("user_id")!=null){
                claimsMetadataEntity.setUserId((Integer)metadataRequest.get("user_id"));
            }
            if(metadataRequest.get("url_mail")!=null){
                claimsMetadataEntity.setUrlMail((String) metadataRequest.get("url_mail"));
            }
            if(metadataRequest.get("cod_plate")!=null){
                claimsMetadataEntity.setCodPlate((String) metadataRequest.get("cod_plate"));
            }
            if(metadataRequest.get("contract_id")!=null){
                claimsMetadataEntity.setContractId((Integer) metadataRequest.get("contract_id"));
            }
            if(metadataRequest.get("customer_id")!=null){
                claimsMetadataEntity.setCustomerId((String) metadataRequest.get("customer_id"));
            }
            if(metadataRequest.get("fiscal_code")!=null){
                claimsMetadataEntity.setFiscalCode((String) metadataRequest.get("fiscal_code"));
            }
            if(metadataRequest.get("user_session_token")!=null){
                claimsMetadataEntity.setUserSessionToken((String) metadataRequest.get("user_session_token"));
            }
            if(metadataRequest.get("plate_customer_id")!=null){
                if(metadataRequest.get("plate_customer_id") instanceof  Integer) {
                    claimsMetadataEntity.setPlateCustomerId((Integer) metadataRequest.get("plate_customer_id"));
                }else{
                    claimsMetadataEntity.setPlateCustomerId(Integer.valueOf((String) metadataRequest.get("plate_customer_id")));
                }
            }
        }
        return claimsMetadataEntity;
    }


    public static MetadataClaim adptFromMetadataEntityToMetadataJsonb (ClaimsMetadataEntity claimsMetadataEntity) {

        if(claimsMetadataEntity == null) {
            return null;
        }

        MetadataClaim metedataJsonB = new MetadataClaim();
        Map<String,Object> metedataMap = new HashMap<>();

        metedataMap.put("role", claimsMetadataEntity.getRole());
        metedataMap.put("email", claimsMetadataEntity.getEmail());
        metedataMap.put("user_id", claimsMetadataEntity.getUserId());
        metedataMap.put("url_mail", claimsMetadataEntity.getUrlMail());
        metedataMap.put("cod_plate", claimsMetadataEntity.getCodPlate());
        metedataMap.put("contract_id", claimsMetadataEntity.getContractId());
        metedataMap.put("customer_id", claimsMetadataEntity.getCustomerId());
        metedataMap.put("fiscal_code", claimsMetadataEntity.getFiscalCode());
        metedataMap.put("user_session_token", claimsMetadataEntity.getUserSessionToken());
        metedataMap.put("plate_customer_id", claimsMetadataEntity.getPlateCustomerId());

        metedataJsonB.setMetadata(metedataMap);

        return metedataJsonB;
    }

}
