package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.counterparty.HistoricalCounterpartyRequest;
import com.doing.nemo.claims.controller.payload.response.counterparty.HistoricalCounterpartyResponse;
import com.doing.nemo.claims.entity.jsonb.repair.HistoricalCounterparty;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Component
public class HistoricalCounterpartyAdapter {

    public static HistoricalCounterparty adptHistoricalCounterpartyRequestToHistoricalCounterparty(HistoricalCounterpartyRequest historicalCounterpartyRequest) {

        HistoricalCounterparty historical = new HistoricalCounterparty();
        historical.setStatusEntityNew(historicalCounterpartyRequest.getStatusEntityNew());
        historical.setStatusEntityOld(historicalCounterpartyRequest.getStatusEntityOld());
        historical.setDescription(historicalCounterpartyRequest.getDescription());
        historical.setUpdateAt(DateUtil.convertIS08601StringToUTCDate(historicalCounterpartyRequest.getUpdateAt()));
        historical.setUserId(historicalCounterpartyRequest.getUserId());
        historical.setComunicationDescription(historicalCounterpartyRequest.getComunicationDescription());
        historical.setUserName(historicalCounterpartyRequest.getUserName());
        historical.setEventType(historicalCounterpartyRequest.getEventType());

        return historical;
    }

    public static List<HistoricalCounterparty> adptHistoricalCounterpartyRequestToHistoricalCounterparty(List<HistoricalCounterpartyRequest> historicalCounterpartyRequestList) {

        if (historicalCounterpartyRequestList != null) {

            List<HistoricalCounterparty> historicalList = new LinkedList<>();
            for (HistoricalCounterpartyRequest att : historicalCounterpartyRequestList) {
                historicalList.add(adptHistoricalCounterpartyRequestToHistoricalCounterparty(att));
            }
            return historicalList;
        }
        return null;
    }

    public static HistoricalCounterpartyResponse adptHistoricalCounterpartyToHistoricalCounterpartyResponse(HistoricalCounterparty historical) {

        HistoricalCounterpartyResponse historicalResponse = new HistoricalCounterpartyResponse();
        if(historical != null) {
            historicalResponse.setStatusEntityNew(historical.getStatusEntityNew());
            historicalResponse.setStatusEntityOld(historical.getStatusEntityOld());
            historicalResponse.setDescription(historical.getDescription());
            historicalResponse.setUpdateAt(DateUtil.convertUTCDateToIS08601String(historical.getUpdateAt()));
            historicalResponse.setUserId(historical.getUserId());
            historicalResponse.setComunicationDescription(historical.getComunicationDescription());
            historicalResponse.setUserName(historical.getUserName());
            historicalResponse.setEventType(historical.getEventType());
        }

        return historicalResponse;

    }

    public static List<HistoricalCounterpartyResponse> adptHistoricalCounterpartyToHistoricalCounterpartyResponse(List<HistoricalCounterparty> historicalList) {

        if (historicalList != null) {

            List<HistoricalCounterpartyResponse> historicalListResponse = new ArrayList<>();
            for (HistoricalCounterparty att : historicalList) {
                historicalListResponse.add(adptHistoricalCounterpartyToHistoricalCounterpartyResponse(att));
            }
            return historicalListResponse;
        }
        return null;
    }
}
