package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.controller.payload.request.practice.FindingRequestV1;
import com.doing.nemo.claims.controller.payload.response.practice.FindingResponseV1;
import com.doing.nemo.claims.entity.jsonb.Theft;
import com.doing.nemo.claims.entity.jsonb.practice.Finding;
import com.doing.nemo.claims.util.Util;
import org.springframework.stereotype.Component;

@Component
public class FindingAdapter {

    public static Finding adptFromFindingRequestToFinding(FindingRequestV1 findingRequest) {
        if(findingRequest == null)
            return null;
        Finding finding = new Finding();
        finding.setFindingDate(findingRequest.getFindingDate());
        finding.setFindingId(findingRequest.getFindingId());
        finding.setSequestered(findingRequest.getSequestered());
        finding.setProvider(findingRequest.getProvider());
        finding.setProviderCode(findingRequest.getProviderCode());
        finding.setComplaintPolice(findingRequest.getComplaintPolice());
        finding.setComplaintCc(findingRequest.getComplaintCc());
        finding.setComplaintVvuu(findingRequest.getComplaintVvuu());
        finding.setComplaintGdf(findingRequest.getComplaintGdf());
        finding.setAuthorityData(findingRequest.getAuthorityData());
        finding.setNote(findingRequest.getNote());
        finding.setHappenedAbroad(findingRequest.getHappenedAbroad());
        finding.setFoundLocation(findingRequest.getFoundLocation());
        finding.setProvince(findingRequest.getProvince());
        finding.setState(findingRequest.getState());
        finding.setDoubleKey(findingRequest.getDoubleKey());
        return finding;
    }


    public static FindingResponseV1 adptFromFindingToFindingResponse(Finding finding) {
        if(finding == null)
            return null;
        FindingResponseV1 findingResponse = new FindingResponseV1();
        findingResponse.setFindingDate(finding.getFindingDate());
        findingResponse.setFindingId(finding.getFindingId());
        findingResponse.setSequestered(finding.getSequestered());
        findingResponse.setProvider(finding.getProvider());
        findingResponse.setProviderCode(finding.getProviderCode());
        findingResponse.setComplaintPolice(finding.getComplaintPolice());
        findingResponse.setComplaintCc(finding.getComplaintCc());
        findingResponse.setComplaintVvuu(finding.getComplaintVvuu());
        findingResponse.setComplaintGdf(finding.getComplaintGdf());
        findingResponse.setAuthorityData(finding.getAuthorityData());
        findingResponse.setNote(finding.getNote());
        findingResponse.setHappenedAbroad(finding.getHappenedAbroad());
        findingResponse.setFoundLocation(finding.getFoundLocation());
        findingResponse.setProvince(finding.getProvince());
        findingResponse.setState(finding.getState());
        findingResponse.setDoubleKey(finding.getDoubleKey());
        return findingResponse;
    }


    public static Finding adptFromFindingClaimsToFinding(Theft theft, Long practiceId) {
        if(theft == null)
            return null;
        Finding finding = new Finding();
        finding.setFindingDate(theft.getFindDate());
        finding.setFindingId(practiceId.toString());
        finding.setSequestered(theft.getSequestered());
        finding.setProvider(null);
        finding.setProviderCode(theft.getSupplierCode());
        finding.setComplaintPolice(theft.getFindAuthorityPolice());
        finding.setComplaintCc(theft.getFindAuthorityCc());
        finding.setComplaintVvuu(theft.getFindAuthorityVvuu());
        finding.setComplaintGdf(false);
        finding.setAuthorityData(theft.getFindAuthorityData());
        finding.setNote(theft.getFindNotes());
        finding.setHappenedAbroad(theft.getFoundAbroad());
        if(theft.getAddress()!=null){
            finding.setProvince(theft.getAddress().getProvince());
            finding.setState(theft.getAddress().getState());
            finding.setFoundLocation(Util.replaceNull(theft.getAddress().getStreet()) +" "+ Util.replaceNull(theft.getAddress().getStreetNr())+" , "+Util.replaceNull(theft.getAddress().getZip())+" "+Util.replaceNull(theft.getAddress().getLocality())+" "+Util.replaceNull(theft.getAddress().getProvince())+" "+Util.replaceNull(theft.getAddress().getRegion())+" "+Util.replaceNull(theft.getAddress().getState()));
        }
        finding.setDoubleKey(theft.getWithReceptions());
        return finding;
    }

}