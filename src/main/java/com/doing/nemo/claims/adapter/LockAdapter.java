package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.controller.payload.response.LockResponseV1;
import com.doing.nemo.claims.entity.settings.LockEntity;
import org.springframework.stereotype.Component;

@Component
public class LockAdapter {

    public static LockResponseV1 adptFromLockEntityToLockResponse(LockEntity lockEntity) {
        LockResponseV1 lockResponseV1 = new LockResponseV1();
        if (lockEntity != null) {
            lockResponseV1.setClaimId(lockEntity.getClaimId());
            lockResponseV1.setUserId(lockEntity.getUserId());
            lockResponseV1.setNow(lockEntity.getNow());
        }
        return lockResponseV1;
    }
}
