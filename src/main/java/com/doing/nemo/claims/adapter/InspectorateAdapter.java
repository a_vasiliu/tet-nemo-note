package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.controller.payload.request.InspectorateRequestV1;
import com.doing.nemo.claims.controller.payload.response.InspectorateResponseV1;
import com.doing.nemo.claims.controller.payload.response.PaginationResponseV1;
import com.doing.nemo.claims.entity.enumerated.DTO.PageStats;
import com.doing.nemo.claims.entity.settings.InspectorateEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Component
public class InspectorateAdapter {
    @Autowired
    private AutomaticAffiliationRuleInspectorateAdapter automaticAffiliationRuleInspectorateAdapter;

    public InspectorateEntity adptFromInspectorateRequestToInspectorateEntity(InspectorateRequestV1 inspectorateRequestV1, HttpMethod httpMethod) {
        InspectorateEntity inspectorateEntity = new InspectorateEntity();
        inspectorateEntity.setName(inspectorateRequestV1.getName());
        inspectorateEntity.setEmail(inspectorateRequestV1.getEmail());
        inspectorateEntity.setAddress(inspectorateRequestV1.getAddress());
        inspectorateEntity.setZipCode(inspectorateRequestV1.getZipCode());
        inspectorateEntity.setCity(inspectorateRequestV1.getCity());
        inspectorateEntity.setProvince(inspectorateRequestV1.getProvince());
        inspectorateEntity.setState(inspectorateRequestV1.getState());
        inspectorateEntity.setVatNumber(inspectorateRequestV1.getVatNumber());
        inspectorateEntity.setFiscalCode(inspectorateRequestV1.getFiscalCode());
        inspectorateEntity.setTelephone(inspectorateRequestV1.getTelephone());
        inspectorateEntity.setFax(inspectorateRequestV1.getFax());
        inspectorateEntity.setWebSite(inspectorateRequestV1.getWebSite());
        inspectorateEntity.setReferencePerson(inspectorateRequestV1.getReferencePerson());
        inspectorateEntity.setAttorney(inspectorateRequestV1.getAttorney());
        inspectorateEntity.setExternalCode(inspectorateRequestV1.getExternalCode());
        inspectorateEntity.setActive(inspectorateRequestV1.getActive());

        if (inspectorateRequestV1.getAutomaticAffiliationRuleInspectorateRequestV1List() != null && !inspectorateRequestV1.getAutomaticAffiliationRuleInspectorateRequestV1List().isEmpty())
            inspectorateEntity.setAutomaticAffiliationRuleEntityList(automaticAffiliationRuleInspectorateAdapter.adptFromAutomaticAffiliationRuleInspectorateRequestToAutomaticAffiliationRuleInspectorateEntityList(inspectorateRequestV1.getAutomaticAffiliationRuleInspectorateRequestV1List(), httpMethod));
        else inspectorateEntity.setAutomaticAffiliationRuleEntityList(new ArrayList<>());
        return inspectorateEntity;
    }


    public static InspectorateResponseV1<InspectorateEntity> adptFromInspectorateEntityToInspectorateResponseV1(InspectorateEntity inspectorateEntity) {
        InspectorateResponseV1<InspectorateEntity> inspectorateResponseV1 = new InspectorateResponseV1<>();
        if (inspectorateEntity == null) return null;
        inspectorateResponseV1.setId(inspectorateEntity.getId());
        inspectorateResponseV1.setName(inspectorateEntity.getName());
        inspectorateResponseV1.setEmail(inspectorateEntity.getEmail());
        inspectorateResponseV1.setAddress(inspectorateEntity.getAddress());
        inspectorateResponseV1.setZipCode(inspectorateEntity.getZipCode());
        inspectorateResponseV1.setCity(inspectorateEntity.getCity());
        inspectorateResponseV1.setProvince(inspectorateEntity.getProvince());
        inspectorateResponseV1.setState(inspectorateEntity.getState());
        inspectorateResponseV1.setVatNumber(inspectorateEntity.getVatNumber());
        inspectorateResponseV1.setFiscalCode(inspectorateEntity.getFiscalCode());
        inspectorateResponseV1.setTelephone(inspectorateEntity.getTelephone());
        inspectorateResponseV1.setFax(inspectorateEntity.getFax());
        inspectorateResponseV1.setWebSite(inspectorateEntity.getWebSite());
        inspectorateResponseV1.setReferencePerson(inspectorateEntity.getReferencePerson());
        inspectorateResponseV1.setAttorney(inspectorateEntity.getAttorney());
        inspectorateResponseV1.setExternalCode(inspectorateEntity.getExternalCode());
        inspectorateResponseV1.setActive(inspectorateEntity.getActive());
        inspectorateResponseV1.setAutomaticAffiliationRuleEntityList(inspectorateEntity.getAutomaticAffiliationRuleEntityList());
        inspectorateResponseV1.setCode(inspectorateEntity.getCode());

        return inspectorateResponseV1;
    }
    public static List<InspectorateResponseV1<InspectorateEntity>> adptFromInspectorateEntityToInspectorateResponseV1List(List<InspectorateEntity> inspectorateEntityList) {
        List<InspectorateResponseV1<InspectorateEntity>> inspectorateResponseV1 = new LinkedList<>();
        for (InspectorateEntity inspectoratesEntity : inspectorateEntityList) {
            inspectorateResponseV1.add(adptFromInspectorateEntityToInspectorateResponseV1(inspectoratesEntity));
        }
        return inspectorateResponseV1;
    }


    public static PaginationResponseV1<InspectorateResponseV1> adptPagination(List<InspectorateEntity> inspectorateEntityList, int page, int pageSize, Long itemCount) {

        double totPages = Math.ceil((double) itemCount.intValue() / pageSize);
        PageStats pageStats = new PageStats();
        pageStats.setCurrentPage(page);
        pageStats.setItemCount(itemCount.intValue());
        pageStats.setPageSize(pageSize);
        pageStats.setPageCount((long)totPages);

        PaginationResponseV1<InspectorateResponseV1> paginationResponseV1 = new PaginationResponseV1(pageStats,adptFromInspectorateEntityToInspectorateResponseV1List(inspectorateEntityList));

        return paginationResponseV1;
    }

}