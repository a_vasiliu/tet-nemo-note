package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.controller.payload.response.damaged.LeaseServiceComponentsResponse;
import com.doing.nemo.claims.entity.esb.ContractESB.LeaseServiceComponentsEsb;
import com.doing.nemo.middleware.client.payload.response.MiddlewareLeaseServiceComponent;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

@Component
public class LeaseServiceComponentsAdapter {


    public static LeaseServiceComponentsResponse adptLeaseServiceComponentsEsbToLeaseServiceComponentsResponse(LeaseServiceComponentsEsb leaseServiceComponentsEsb) {

        if (leaseServiceComponentsEsb != null) {

            LeaseServiceComponentsResponse leaseServiceComponentsResponse = new LeaseServiceComponentsResponse();
            leaseServiceComponentsResponse.setId(leaseServiceComponentsEsb.getId());
            leaseServiceComponentsResponse.setName(leaseServiceComponentsEsb.getName());
            leaseServiceComponentsResponse.setProductId(leaseServiceComponentsEsb.getProductId());
            leaseServiceComponentsResponse.setQualifiersList(QualifiersAdapter.adptFromQualifiersESBToQualifiersResponseList(leaseServiceComponentsEsb.getQualifiersList()));


            return leaseServiceComponentsResponse;
        }
        return null;
    }

    public static List<LeaseServiceComponentsResponse> adptFromLeaseServiceComponentsESBToLeaseServiceComponentsResponseList(List<LeaseServiceComponentsEsb> leaseServiceComponentsEsbs) {
        List<LeaseServiceComponentsResponse> leaseServiceComponentsResponseList = new LinkedList<>();
        for (LeaseServiceComponentsEsb leaseServiceComponentsEsb : leaseServiceComponentsEsbs) {
            leaseServiceComponentsResponseList.add(adptLeaseServiceComponentsEsbToLeaseServiceComponentsResponse(leaseServiceComponentsEsb));
        }
        return leaseServiceComponentsResponseList;
    }

    public static List<LeaseServiceComponentsResponse> adptFromMiddlewareLeaseServiceComponentToLeaseServiceComponentsResponseList(List<MiddlewareLeaseServiceComponent> middlewareLeaseServiceComponentList) {
        List<LeaseServiceComponentsResponse> leaseServiceComponentsResponseList = new LinkedList<>();
        for (MiddlewareLeaseServiceComponent middlewareLeaseServiceComponent : middlewareLeaseServiceComponentList) {
            leaseServiceComponentsResponseList.add(adptFromMiddlewareLeaseServiceComponentToLeaseServiceComponentsResponse(middlewareLeaseServiceComponent));
        }
        return leaseServiceComponentsResponseList;
    }

    public static LeaseServiceComponentsResponse adptFromMiddlewareLeaseServiceComponentToLeaseServiceComponentsResponse(MiddlewareLeaseServiceComponent leaseServiceComponentsEsb) {

        if (leaseServiceComponentsEsb != null) {

            LeaseServiceComponentsResponse leaseServiceComponentsResponse = new LeaseServiceComponentsResponse();
            leaseServiceComponentsResponse.setId(leaseServiceComponentsEsb.getId());
            leaseServiceComponentsResponse.setName(leaseServiceComponentsEsb.getName());
            leaseServiceComponentsResponse.setProductId(leaseServiceComponentsEsb.getProductId());
            leaseServiceComponentsResponse.setQualifiersList(QualifiersAdapter.adptFromMiddlewareQualifierResponseToQualifiersResponseList(leaseServiceComponentsEsb.getQualifiers()));


            return leaseServiceComponentsResponse;
        }
        return null;
    }

    
}
