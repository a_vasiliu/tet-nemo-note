package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.controller.payload.request.ExportNotificationTypeRequestV1;
import com.doing.nemo.claims.controller.payload.response.ClaimsResponsePaginationV1;
import com.doing.nemo.claims.controller.payload.response.ExportNotificationTypeResponseV1;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.settings.ExportNotificationTypeEntity;
import com.doing.nemo.claims.entity.settings.NotificationTypeEntity;
import com.doing.nemo.claims.exception.NotFoundException;
import com.doing.nemo.claims.repository.ExportNotificationTypeRepository;
import com.doing.nemo.claims.repository.NotificationTypeRepository;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
public class ExportNotificationTypeAdapter {

    @Autowired
    private ExportNotificationTypeRepository exportNotificationTypeRepository;

    @Autowired
    private NotificationTypeRepository notificationTypeRepository;

    private static Logger LOGGER = LoggerFactory.getLogger(ExportNotificationTypeAdapter.class);

    public ExportNotificationTypeEntity adptFromExportNotificationTypeRequestToExportNotificationTypeEntity(ExportNotificationTypeRequestV1 exportNotificationTypeRequestV1)
    {
        ExportNotificationTypeEntity exportNotificationTypeEntity = new ExportNotificationTypeEntity();
        if(exportNotificationTypeRequestV1 != null)
        {
            exportNotificationTypeEntity.setCode(exportNotificationTypeRequestV1.getCode());
            exportNotificationTypeEntity.setDescription(exportNotificationTypeRequestV1.getDescription());
            if(exportNotificationTypeRequestV1.getNotificationType() != null) {
                Optional<NotificationTypeEntity> notificationTypeEntity = notificationTypeRepository.findById(exportNotificationTypeRequestV1.getNotificationType().getId());
                if (notificationTypeEntity.isPresent())
                    exportNotificationTypeEntity.setNotificationType(notificationTypeEntity.get());
            }
            exportNotificationTypeEntity.setActive(exportNotificationTypeRequestV1.getActive());

        }
        return exportNotificationTypeEntity;
    }

    public ExportNotificationTypeEntity adptFromExportNotificationTypeRequestToExportNotificationTypeEntityWithID(ExportNotificationTypeRequestV1 exportNotificationTypeRequestV1, UUID uuid)
    {
        Optional<ExportNotificationTypeEntity> exportNotificationTypeEntity = exportNotificationTypeRepository.findById(uuid);
        if(!exportNotificationTypeEntity.isPresent()) {
            LOGGER.debug("Export claims type with id "+uuid+ " not found");
            throw new NotFoundException("Export claims type with id "+uuid+ " not found", com.doing.nemo.claims.validation.MessageCode.CLAIMS_1010);
        }
        ExportNotificationTypeEntity exportNotificationTypeEntity1 = exportNotificationTypeEntity.get();
        ExportNotificationTypeEntity exportNotificationTypeEntity2 = adptFromExportNotificationTypeRequestToExportNotificationTypeEntity(exportNotificationTypeRequestV1);
        exportNotificationTypeEntity2.setId(exportNotificationTypeEntity1.getId());
        return exportNotificationTypeEntity2;
    }

    public static ExportNotificationTypeResponseV1 adptFromExportNotificationTypeEntityToExportNotificationTypeResponse(ExportNotificationTypeEntity exportNotificationTypeEntity)
    {
        ExportNotificationTypeResponseV1 exportNotificationTypeResponseV1 = new ExportNotificationTypeResponseV1();
        if(exportNotificationTypeEntity != null)
        {
            exportNotificationTypeResponseV1.setId(exportNotificationTypeEntity.getId());
            exportNotificationTypeResponseV1.setNotificationType(exportNotificationTypeEntity.getNotificationType());
            exportNotificationTypeResponseV1.setDmSystem(exportNotificationTypeEntity.getDmSystem());
            exportNotificationTypeResponseV1.setCode(exportNotificationTypeEntity.getCode());
            exportNotificationTypeResponseV1.setDescription(exportNotificationTypeEntity.getDescription());
            exportNotificationTypeResponseV1.setActive(exportNotificationTypeEntity.getActive());
        }
        return exportNotificationTypeResponseV1;
    }

    public static List<ExportNotificationTypeResponseV1> adptFromExportNotificationTypeEntityToExportNotificationTypeResponseList(List<ExportNotificationTypeEntity> exportNotificationTypeEntityList)
    {
        List<ExportNotificationTypeResponseV1> responseV1 = new LinkedList<>();
        for(ExportNotificationTypeEntity exportNotificationTypeEntity : exportNotificationTypeEntityList)
        {
            responseV1.add(adptFromExportNotificationTypeEntityToExportNotificationTypeResponse(exportNotificationTypeEntity));
        }
        return responseV1;
    }

    public static ClaimsResponsePaginationV1 adptFromExportNotificationTypePaginationToNotificationResponsePagination(Pagination<ExportNotificationTypeEntity> pagination) {

        ClaimsResponsePaginationV1<ExportNotificationTypeResponseV1> claimsPaginationResponse = new ClaimsResponsePaginationV1<>();

        claimsPaginationResponse.setStats(pagination.getStats());
        claimsPaginationResponse.setItems(adptFromExportNotificationTypeEntityToExportNotificationTypeResponseList(pagination.getItems()));

        return claimsPaginationResponse;
    }
}
