package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.common.validation.MessageCode;
import com.doing.nemo.claims.common.validation.RequestValidator;
import com.doing.nemo.claims.controller.payload.request.RecipientTypeRequestV1;
import com.doing.nemo.claims.controller.payload.response.ClaimsResponsePaginationV1;
import com.doing.nemo.claims.controller.payload.response.RecipientTypeResponseV1;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.settings.RecipientTypeEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

@Component
public class RecipientTypeAdapter {

    @Autowired
    private RequestValidator requestValidator;

    public static RecipientTypeEntity getRecipientTypeEntity(RecipientTypeRequestV1 recipientTypeRequestV1) {

        RecipientTypeEntity recipientTypeEntity = new RecipientTypeEntity();

        recipientTypeEntity.setDescription(recipientTypeRequestV1.getDescription());
        recipientTypeEntity.setFixed(recipientTypeRequestV1.getFixed());
        recipientTypeEntity.setEmail(recipientTypeRequestV1.getEmail());
        recipientTypeEntity.setDbFieldEmail(recipientTypeRequestV1.getDbFieldEmail());
        recipientTypeEntity.setActive(recipientTypeRequestV1.getActive());
        recipientTypeEntity.setRecipientType(recipientTypeRequestV1.getRecipientType());

        return recipientTypeEntity;
    }

    public static RecipientTypeEntity getRecipientTypeEntityWithId(RecipientTypeRequestV1 recipientTypeRequestV1) {

        RecipientTypeEntity recipientTypeEntity = new RecipientTypeEntity();

        recipientTypeEntity.setId(recipientTypeRequestV1.getId());
        recipientTypeEntity.setDescription(recipientTypeRequestV1.getDescription());
        recipientTypeEntity.setFixed(recipientTypeRequestV1.getFixed());
        recipientTypeEntity.setEmail(recipientTypeRequestV1.getEmail());
        recipientTypeEntity.setDbFieldEmail(recipientTypeRequestV1.getDbFieldEmail());
        recipientTypeEntity.setActive(recipientTypeRequestV1.getActive());
        recipientTypeEntity.setRecipientType(recipientTypeRequestV1.getRecipientType());

        return recipientTypeEntity;
    }

    public static RecipientTypeResponseV1 getRecipientTypeAdapter(RecipientTypeEntity recipientTypeEntity) {

        RecipientTypeResponseV1 recipientTypeResponseV1 = new RecipientTypeResponseV1();

        recipientTypeResponseV1.setId(recipientTypeEntity.getId());
        recipientTypeResponseV1.setDescription(recipientTypeEntity.getDescription());
        recipientTypeResponseV1.setFixed(recipientTypeEntity.getFixed());
        recipientTypeResponseV1.setEmail(recipientTypeEntity.getEmail());
        recipientTypeResponseV1.setDbFieldEmail(recipientTypeEntity.getDbFieldEmail());
        recipientTypeResponseV1.setActive(recipientTypeEntity.getActive());
        recipientTypeResponseV1.setRecipientType(recipientTypeEntity.getRecipientType());
        recipientTypeResponseV1.setTypeComplaint(recipientTypeEntity.getTypeComplaint());

        return recipientTypeResponseV1;
    }

    public static List<RecipientTypeResponseV1> getRecipientTypeListAdapter(List<RecipientTypeEntity> recipientTypeEntityList) {

        List<RecipientTypeResponseV1> recipientTypeResponseV1List = new LinkedList<>();

        for (RecipientTypeEntity recipientTypeEntity : recipientTypeEntityList) {
            recipientTypeResponseV1List.add(getRecipientTypeAdapter(recipientTypeEntity));
        }

        return recipientTypeResponseV1List;
    }

    public List<RecipientTypeEntity> getRecipientTypeListEntity(List<RecipientTypeRequestV1> recipientTypeRequestV1List) {

        List<RecipientTypeEntity> recipientTypeEntityList = new LinkedList<>();

        for (RecipientTypeRequestV1 recipientTypeRequestV1 : recipientTypeRequestV1List) {
            requestValidator.validateRequest(recipientTypeRequestV1, MessageCode.E00X_1000);
            recipientTypeEntityList.add(getRecipientTypeEntityWithId(recipientTypeRequestV1));
        }

        return recipientTypeEntityList;
    }

    public static ClaimsResponsePaginationV1 adptRecipientTypePaginationToClaimsResponsePagination(Pagination<RecipientTypeEntity> pagination) {

        ClaimsResponsePaginationV1<RecipientTypeResponseV1> claimsPaginationResponse = new ClaimsResponsePaginationV1<>();

        claimsPaginationResponse.setStats(pagination.getStats());
        claimsPaginationResponse.setItems(getRecipientTypeListAdapter(pagination.getItems()));

        return claimsPaginationResponse;
    }
}
