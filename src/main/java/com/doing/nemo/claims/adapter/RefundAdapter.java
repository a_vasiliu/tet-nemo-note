package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.claims.RefundRequest;
import com.doing.nemo.claims.controller.payload.response.claims.RefundResponse;
import com.doing.nemo.claims.entity.claims.ClaimsRefundEntity;
import com.doing.nemo.claims.entity.jsonb.refund.Refund;
import com.doing.nemo.claims.entity.jsonb.refund.Split;
import com.doing.nemo.claims.util.Util;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.stereotype.Component;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class RefundAdapter {

    private static Logger LOGGER = LoggerFactory.getLogger(RefundAdapter.class);



    public static Refund adptFromRefundRequestToRefund(RefundRequest refundRequest) {

        if (refundRequest != null) {

            Refund refund = new Refund();

            refund.setDefinitionDate(DateUtil.convertIS08601StringToUTCDate(refundRequest.getDefinitionDate()));
            refund.setFranchiseAmountFcm(refundRequest.getFranchiseAmountFcm());
            refund.setNBV(refundRequest.getNBV());
            refund.setPoSum(refundRequest.getPoSum());
            refund.setSplitList(SplitAdapter.adptFromSplitRequestListToSplitList(refundRequest.getSplitList()));
            refund.setTotalLiquidationReceived(refundRequest.getTotalLiquidationReceived());
            refund.setTotalRefundExpected(refundRequest.getTotalRefundExpected());
            refund.setWreck(refundRequest.getWreck());
            refund.setWreckValuePre(refundRequest.getWreckValuePre());
            refund.setWreckValuePost(refundRequest.getWreckValuePost());
            refund.setAmountToBeDebited(refundRequest.getAmountToBeDebited());
            refund.setBluEurotax(refundRequest.getBluEurotax());
            refund.setDeltaDebitDate(refundRequest.getDeltaDebitDate());

            return refund;
        }
        return null;
    }

    public static RefundResponse adptFromRefundToRefundResponse(Refund refund) {

        if (refund != null) {

            RefundResponse refundResponse = new RefundResponse();
            refundResponse.setDefinitionDate(DateUtil.convertUTCDateToIS08601String(refund.getDefinitionDate()));

            refundResponse.setFranchiseAmountFcm(refund.getFranchiseAmountFcm());
            refundResponse.setNBV(refund.getNBV());
            Double poSum = null;
            if(refund.getPoSum() != null) {
                poSum =  Double.parseDouble(String.format("%.2f", refund.getPoSum()).replace(",", "."));
            }
            refund.setPoSum(poSum);
            refundResponse.setPoSum(refund.getPoSum());
            refundResponse.setSplitList(SplitAdapter.adptFromSplitListToSplitResponseList(refund.getSplitList()));
            refundResponse.setTotalLiquidationReceived(refund.getTotalLiquidationReceived());
            refundResponse.setTotalRefundExpected(refund.getTotalRefundExpected());
            refundResponse.setWreck(refund.getWreck());
            refundResponse.setWreckValuePre(refund.getWreckValuePre());
            refundResponse.setWreckValuePost(refund.getWreckValuePost());
            refundResponse.setAmountToBeDebited(refund.getAmountToBeDebited());
            refundResponse.setBluEurotax(refund.getBluEurotax());
            refundResponse.setDeltaDebitDate(refund.getDeltaDebitDate());

            return refundResponse;
        }
        return null;
    }

    //New Adapter
    public static Refund adptFromRefundJsonbToClaimsRefund(Refund refundJson) {

        if (refundJson != null) {

            Refund refund = new Refund();

            refund.setDefinitionDate( refund.getDefinitionDate());
            refund.setFranchiseAmountFcm(refundJson.getFranchiseAmountFcm());
            refund.setNBV(refundJson.getNBV());
            refund.setPoSum(refundJson.getPoSum());

            if (refundJson.getSplitList()!=null){
                List<Split> newSplitList = new ArrayList<>();
                Split spliAdd = new Split();
                for (Split splitItem:refundJson.getSplitList())
                {
                    if (splitItem.getRefundType()!=null){
                        spliAdd = new  Split();
                        spliAdd.setId(splitItem.getId());
                        spliAdd.setIssueDate(splitItem.getIssueDate());
                        spliAdd.setRefundType(splitItem.getRefundType());
                        spliAdd.setLegalFees(splitItem.getLegalFees());
                        spliAdd.setType(splitItem.getType());
                        spliAdd.setNote(splitItem.getNote());
                        spliAdd.setMaterialAmount(splitItem.getMaterialAmount());
                        spliAdd.setReceivedSum(splitItem.getReceivedSum());
                        spliAdd.setTechnicalStop(splitItem.getTechnicalStop());
                        spliAdd.setTotalPaid(splitItem.getTotalPaid());
                        newSplitList.add(spliAdd);
                    }
                }
                refund.setSplitList(newSplitList);
            }else{
                refund.setSplitList(null);
            }


            refund.setTotalLiquidationReceived(refundJson.getTotalLiquidationReceived());
            refund.setTotalRefundExpected(refundJson.getTotalRefundExpected());
            refund.setWreck(refundJson.getWreck());
            refund.setWreckValuePre(refundJson.getWreckValuePre());
            refund.setWreckValuePost(refundJson.getWreckValuePost());
            refund.setAmountToBeDebited(refundJson.getAmountToBeDebited());
            refund.setBluEurotax(refundJson.getBluEurotax());
            refund.setDeltaDebitDate(refundJson.getDeltaDebitDate());

            return refund;
        }

        return null;
    }

    public static ClaimsRefundEntity adptFromRefundJsonbToClaimsRefundEntity(Refund refundJson) {

        if (refundJson != null) {

            ClaimsRefundEntity refund = new ClaimsRefundEntity();
            refund.setDefinitionDate(DateUtil.convertIS08601StringToUTCInstant(DateUtil.convertUTCDateToIS08601String(refundJson.getDefinitionDate())));
            refund.setFranchiseAmountFcm(refundJson.getFranchiseAmountFcm());
            refund.setNBV(refundJson.getNBV());
            refund.setPoSum(refundJson.getPoSum());

            if (refundJson.getSplitList()!=null){
                List<Split> newSplitList = new ArrayList<>();
                Split spliAdd = new Split();
                for (Split splitItem:refundJson.getSplitList())
                {
                   if (splitItem.getRefundType()!=null){
                       spliAdd = new  Split();
                       spliAdd.setId(splitItem.getId());
                       spliAdd.setIssueDate(splitItem.getIssueDate());
                       spliAdd.setRefundType(splitItem.getRefundType());
                       spliAdd.setLegalFees(splitItem.getLegalFees());
                       spliAdd.setType(splitItem.getType());
                       spliAdd.setNote(splitItem.getNote());
                       spliAdd.setMaterialAmount(splitItem.getMaterialAmount());
                       spliAdd.setReceivedSum(splitItem.getReceivedSum());
                       spliAdd.setTechnicalStop(splitItem.getTechnicalStop());
                       spliAdd.setTotalPaid(splitItem.getTotalPaid());
                       newSplitList.add(spliAdd);
                   }
                }
                refund.setSplitList(newSplitList);
            }


            refund.setTotalLiquidationReceived(refundJson.getTotalLiquidationReceived());
            refund.setTotalRefundExpected(refundJson.getTotalRefundExpected());
            refund.setWreck(refundJson.getWreck());
            refund.setWreckValuePre(refundJson.getWreckValuePre());
            refund.setWreckValuePost(refundJson.getWreckValuePost());
            refund.setAmountToBeDebited(refundJson.getAmountToBeDebited());
            refund.setBluEurotax(refundJson.getBluEurotax());
            refund.setDeltaDebitDate(refundJson.getDeltaDebitDate());

            return refund;
        }
        return null;
    }

    public static Refund adptClaimsFromRefundToRefundJsonb(ClaimsRefundEntity claimsRefund) {

        if (claimsRefund != null) {

            Refund refund = new Refund();
            refund.setDefinitionDate(DateUtil.convertIS08601StringToUTCDate(DateUtil.convertUTCInstantToIS08601String(claimsRefund.getDefinitionDate())));
            refund.setFranchiseAmountFcm(claimsRefund.getFranchiseAmountFcm());
            refund.setNBV(claimsRefund.getNBV());
            refund.setPoSum(claimsRefund.getPoSum());
            refund.setSplitList(claimsRefund.getSplitList());
            refund.setTotalLiquidationReceived(claimsRefund.getTotalLiquidationReceived());
            refund.setTotalRefundExpected(claimsRefund.getTotalRefundExpected());
            refund.setWreck(claimsRefund.getWreck());
            refund.setWreckValuePre(claimsRefund.getWreckValuePre());
            refund.setWreckValuePost(claimsRefund.getWreckValuePost());
            refund.setAmountToBeDebited(claimsRefund.getAmountToBeDebited());
            refund.setBluEurotax(claimsRefund.getBluEurotax());
            refund.setDeltaDebitDate(claimsRefund.getDeltaDebitDate());

            return refund;
        }
        return null;
    }


}
