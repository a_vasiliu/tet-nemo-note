package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.response.DataManagementImportResponsePaginationV1;
import com.doing.nemo.claims.controller.payload.response.DataManagementImportResponseV1;
import com.doing.nemo.claims.controller.payload.response.DataManagementImportResponseV1.ProcessingFileResponseV1;
import com.doing.nemo.claims.entity.enumerated.DTO.PageStats;
import com.doing.nemo.claims.entity.settings.DataManagementImportEntity;
import com.doing.nemo.claims.entity.settings.ProcessingFile;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

@Component
public class DataManagementImportAdapter {

    public static DataManagementImportResponseV1 adptDataManagementImportToDataManagementImportResponse(DataManagementImportEntity dataManagementImport){

        if(dataManagementImport == null)
            return null;

        DataManagementImportResponseV1 dataManagementImportResponse = new DataManagementImportResponseV1();

        dataManagementImportResponse.setId(dataManagementImport.getId().toString());
        dataManagementImportResponse.setDescription(dataManagementImport.getDescription());
        dataManagementImportResponse.setFileType(dataManagementImport.getFileType());
        dataManagementImportResponse.setResult(dataManagementImport.getResult());
        dataManagementImportResponse.setUser(dataManagementImport.getUser());
        dataManagementImportResponse.setDate(DateUtil.convertUTCInstantToIS08601String(dataManagementImport.getDate()));
        dataManagementImportResponse.setProcessingFiles(adptProcessingFileToProcessingFileResponse(dataManagementImport.getProcessingFiles()));

        return dataManagementImportResponse;
    }

    public static List<DataManagementImportResponseV1> adptDataManagementImportToDataManagementImportResponse(List<DataManagementImportEntity> dataManagementImportList){

        if(dataManagementImportList == null)
            return null;

        List<DataManagementImportResponseV1> dataImportResponseList = new LinkedList<>();

        for (DataManagementImportEntity att : dataManagementImportList){
            dataImportResponseList.add(adptDataManagementImportToDataManagementImportResponse(att));
        }

        return dataImportResponseList;
    }

    public static ProcessingFileResponseV1 adptProcessingFileToProcessingFileResponse(ProcessingFile processingFile){

        if(processingFile == null)
            return null;

        ProcessingFileResponseV1 processingFileResponse = new ProcessingFileResponseV1();

        processingFileResponse.setAttachmentId(processingFile.getAttachmentId());
        processingFileResponse.setFileName(processingFile.getFileName());
        processingFileResponse.setFileSize(processingFile.getFileSize());
        processingFileResponse.setId(processingFile.getId());

        return processingFileResponse;
    }

    public static List<ProcessingFileResponseV1> adptProcessingFileToProcessingFileResponse(List<ProcessingFile> processingFileList){

        if(processingFileList == null)
            return null;

        List<ProcessingFileResponseV1> processingFileResponseList = new LinkedList<>();

        for (ProcessingFile att : processingFileList){
            processingFileResponseList.add(adptProcessingFileToProcessingFileResponse(att));
        }

        return processingFileResponseList;
    }

    public static DataManagementImportResponsePaginationV1 adptPagination(List<DataManagementImportEntity> dataManagementImportList, int page, int pageSize, Long itemCount) {

        long totPages = (int) Math.ceil((double) itemCount.intValue() / pageSize);
        PageStats pageStats = new PageStats();
        if(page > totPages || page < 1)
            pageStats.setCurrentPage(1);
        else
            pageStats.setCurrentPage(page);
        pageStats.setItemCount(itemCount.intValue());
        pageStats.setPageSize(pageSize);
        pageStats.setPageCount(totPages);

        DataManagementImportResponsePaginationV1 dataManagementImportResponsePaginationV1 = new DataManagementImportResponsePaginationV1(pageStats,adptDataManagementImportToDataManagementImportResponse(dataManagementImportList));

        return dataManagementImportResponsePaginationV1;
    }


}
