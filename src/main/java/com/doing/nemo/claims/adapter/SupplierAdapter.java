package com.doing.nemo.claims.adapter;


import com.doing.nemo.claims.controller.payload.request.SupplierRequestV1;
import com.doing.nemo.claims.controller.payload.response.PaginationResponseV1;
import com.doing.nemo.claims.controller.payload.response.SupplierResponseV1;
import com.doing.nemo.claims.entity.SupplierEntity;
import com.doing.nemo.claims.entity.enumerated.DTO.PageStats;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

@Component
public class SupplierAdapter {


    public static SupplierEntity adptFromSupplierRequestToSupplierEntity(SupplierRequestV1 supplierRequestV1) {
        SupplierEntity supplierEntity = new SupplierEntity();
        supplierEntity.setCodSupplier(supplierRequestV1.getCodSupplier());
        supplierEntity.setName(supplierRequestV1.getName());
        supplierEntity.setEmail(supplierRequestV1.getEmail());
        supplierEntity.setActive(supplierRequestV1.getActive());



        return supplierEntity;
    }


    public static SupplierResponseV1 adptFromSupplierEntityToSupplierResponseV1(SupplierEntity supplierEntity) {
        SupplierResponseV1 supplierResponseV1 = new SupplierResponseV1();
        supplierResponseV1.setId(supplierEntity.getId());
        supplierResponseV1.setCodSupplier(supplierEntity.getCodSupplier());
        supplierResponseV1.setName(supplierEntity.getName());
        supplierResponseV1.setEmail(supplierEntity.getEmail());
        supplierResponseV1.setActive(supplierEntity.getActive());




        return supplierResponseV1;
    }

    public static List<SupplierResponseV1> adptFromSupplierEntityToSupplierResponseV1List(List<SupplierEntity> supplierEntityList) {
        List<SupplierResponseV1> supplierResponseV1 = new LinkedList<>();
        for (SupplierEntity suppliersEntity : supplierEntityList) {
            supplierResponseV1.add(adptFromSupplierEntityToSupplierResponseV1(suppliersEntity));
        }
        return supplierResponseV1;
    }

    public static PaginationResponseV1<SupplierResponseV1> adptPagination(List<SupplierEntity> supplierEntityList, int page, int pageSize, Long itemCount) {

        double totPages = Math.ceil((double) itemCount.intValue() / pageSize);
        PageStats pageStats = new PageStats();
        pageStats.setCurrentPage(page);
        pageStats.setItemCount(itemCount.intValue());
        pageStats.setPageSize(pageSize);
        pageStats.setPageCount((long)totPages);

        PaginationResponseV1<SupplierResponseV1> paginationResponseV1 = new PaginationResponseV1(pageStats,adptFromSupplierEntityToSupplierResponseV1List(supplierEntityList));

        return paginationResponseV1;
    }

}