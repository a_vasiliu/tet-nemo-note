package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.controller.payload.response.ClaimsResponseStatsEvidenceV1;
import com.doing.nemo.claims.controller.payload.response.ClaimsResponseStatsV1;
import com.doing.nemo.claims.entity.Stats;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

@Component
public class StatsAdapter {


    public static List<ClaimsResponseStatsV1.StatsElementResponse> adptStatsElementResponseToStatsElement(List<Stats.StatsElement> statsElementList) {

        List<ClaimsResponseStatsV1.StatsElementResponse> statsElementResponseList = new LinkedList<>();
        for (Stats.StatsElement att : statsElementList) {
            statsElementResponseList.add(adptStatsElementResponseToStatsElement(att));
        }

        return statsElementResponseList;
    }

    public static ClaimsResponseStatsV1.StatsElementResponse adptStatsElementResponseToStatsElement(Stats.StatsElement statsElement) {

        ClaimsResponseStatsV1.StatsElementResponse statsElementResponse = new ClaimsResponseStatsV1.StatsElementResponse();

        statsElementResponse.setClaimsStatus(statsElement.getClaimsStatus());
        //statsElementResponse.setClientList(this.adptClientListTOClientListResponse(statsElement.getClientList()));

        return statsElementResponse;
    }


    public static List<ClaimsResponseStatsEvidenceV1.StatsElementEvidenceResponse> adptStatsElementEvidenceToStatsElementEvidenceResponse(List<Stats.StatsElement> statsElementList) {

        List<ClaimsResponseStatsEvidenceV1.StatsElementEvidenceResponse> statsElementResponseList = new LinkedList<>();
        for (Stats.StatsElement att : statsElementList) {
            statsElementResponseList.add(adptStatsElementEvidenceToStatsElementEvidenceResponse(att));
        }

        return statsElementResponseList;
    }

    public static ClaimsResponseStatsEvidenceV1.StatsElementEvidenceResponse adptStatsElementEvidenceToStatsElementEvidenceResponse(Stats.StatsElement statsElement) {

        if (statsElement == null)
            return null;

        ClaimsResponseStatsEvidenceV1.StatsElementEvidenceResponse statsElementResponse = new ClaimsResponseStatsEvidenceV1.StatsElementEvidenceResponse();

        statsElementResponse.setClaimsStatus(statsElement.getClaimsStatus());
        statsElementResponse.setFul(adptInternalCounterToInternalCounterResponse(statsElement.getFul()));
        statsElementResponse.setFcm(adptInternalCounterToInternalCounterResponse(statsElement.getFcm()));
        statsElementResponse.setFni(adptInternalCounterToInternalCounterResponse(statsElement.getFni()));

        return statsElementResponse;
    }

    public static ClaimsResponseStatsEvidenceV1.StatsElementEvidenceResponse.InternalCounterResponse adptInternalCounterToInternalCounterResponse(Stats.InternalCounter internalCounter){

        if(internalCounter == null)
            return null;

        ClaimsResponseStatsEvidenceV1.StatsElementEvidenceResponse.InternalCounterResponse internalCounterResponse = new ClaimsResponseStatsEvidenceV1.StatsElementEvidenceResponse.InternalCounterResponse();

        internalCounterResponse.setInEvidence(internalCounter.getInEvidence());
        internalCounterResponse.setTotal(internalCounter.getTotal());
        internalCounterResponse.setWithContinuation(internalCounter.getWithContinuation());
        internalCounterResponse.setWithContinuationInEvidence(internalCounter.getWithContinuationInEvidence());

        return internalCounterResponse;

    }




}
