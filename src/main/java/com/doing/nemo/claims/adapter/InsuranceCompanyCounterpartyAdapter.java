package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.controller.payload.request.InsuranceCompanyCounterpartyRequest;
import com.doing.nemo.claims.controller.payload.response.InsuranceCompanyCounterpartyResponse;
import com.doing.nemo.claims.entity.jsonb.counterparty.InsuranceCompanyCounterparty;
import com.doing.nemo.claims.entity.settings.InsuranceCompanyEntity;
import com.doing.nemo.claims.repository.InsuranceCompanyRepository;
import com.doing.nemo.claims.validation.MessageCode;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class InsuranceCompanyCounterpartyAdapter {
    private static Logger LOGGER = LoggerFactory.getLogger(InsuranceCompanyCounterpartyAdapter.class);

    @Autowired
    private InsuranceCompanyRepository insuranceCompanyRepository;

    public InsuranceCompanyCounterparty adptFromCompanyCounterpartyRequestToCompanyCounterparty(InsuranceCompanyCounterpartyRequest insuranceCompanyCounterpartyRequest) {
        if (insuranceCompanyCounterpartyRequest == null) return null;
        InsuranceCompanyCounterparty insuranceCompanyCounterparty = new InsuranceCompanyCounterparty();
        Optional<InsuranceCompanyEntity> insuranceCompanyEntityOptional = null;
        if(insuranceCompanyCounterpartyRequest.getEntity() != null && insuranceCompanyCounterpartyRequest.getEntity().getId() != null)
        {
            insuranceCompanyEntityOptional = insuranceCompanyRepository.findById(insuranceCompanyCounterpartyRequest.getEntity().getId());
            if(!insuranceCompanyEntityOptional.isPresent()) {
                throw new BadRequestException(MessageCode.CLAIMS_1050);
            }
            insuranceCompanyCounterparty.setEntity(insuranceCompanyEntityOptional.get());
        } else if(insuranceCompanyCounterpartyRequest.getEntity() == null || insuranceCompanyCounterpartyRequest.getEntity().getId() == null)
        {
            LOGGER.debug(MessageCode.CLAIMS_1051.value());
            throw new BadRequestException(MessageCode.CLAIMS_1051);
        }

        insuranceCompanyCounterparty.setName(insuranceCompanyCounterpartyRequest.getName());
        return insuranceCompanyCounterparty;
    }

    public static InsuranceCompanyCounterpartyResponse adptFromCompanyCounterpartyToCompanyCounterpartyResponse(InsuranceCompanyCounterparty insuranceCompanyCounterparty) {
        if (insuranceCompanyCounterparty == null) return null;
        InsuranceCompanyCounterpartyResponse insuranceCompanyCounterpartyResponse = new InsuranceCompanyCounterpartyResponse();
        insuranceCompanyCounterpartyResponse.setEntity(InsuranceCompanyAdapter.adptFromICompEntityToICompResp(insuranceCompanyCounterparty.getEntity()));
        insuranceCompanyCounterpartyResponse.setName(insuranceCompanyCounterparty.getName());
        return insuranceCompanyCounterpartyResponse;
    }
}
