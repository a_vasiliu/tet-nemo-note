package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.controller.payload.UploadFileClaimsRequestV1;
import com.doing.nemo.claims.controller.payload.request.UploadFileRequestV1;
import com.doing.nemo.claims.controller.payload.response.DownloadFileBase64ResponseV1;
import com.doing.nemo.claims.entity.jsonb.UploadFile;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class UploadFileAdapter {

    public static UploadFile adptUploadFileRequestToUploadFile(UploadFileRequestV1 uploadFileRequest){

        UploadFile uploadFile = new UploadFile();
        uploadFile.setBlobType(uploadFileRequest.getBlobType());
        uploadFile.setDescription(uploadFileRequest.getDescription());
        uploadFile.setFileContent(uploadFileRequest.getFileContent());
        uploadFile.setFileName(uploadFileRequest.getFileName());
        uploadFile.setUuid(uploadFileRequest.getUuid());

        return uploadFile;
    }

    public static UploadFile dptFromUploadFileClaimsRequestV1ToUploadFile(UploadFileClaimsRequestV1 uploadFileRequest){

        UploadFile uploadFile = new UploadFile();
        uploadFile.setBlobType(uploadFileRequest.getBlobType());
        uploadFile.setDescription(uploadFileRequest.getDescription());
        uploadFile.setFileContent(uploadFileRequest.getFileContent());
        uploadFile.setFileName(uploadFileRequest.getFileName());
        uploadFile.setUuid(uploadFileRequest.getUuid());

        return uploadFile;
    }
    public static UploadFileRequestV1 adptFromUploadFileClaimsRequestV1ToUploadFileRequestV1(UploadFileClaimsRequestV1 uploadFileClaimsRequestV1){
        if(uploadFileClaimsRequestV1 == null)
            return null;

        UploadFileRequestV1 uploadFileRequestV1 = new UploadFileRequestV1();
        uploadFileRequestV1.setAttachmentType(uploadFileClaimsRequestV1.getAttachmentType());
        uploadFileRequestV1.setResourceId(uploadFileClaimsRequestV1.getResourceId());
        uploadFileRequestV1.setBlobType(uploadFileClaimsRequestV1.getBlobType());
        uploadFileRequestV1.setFileName(uploadFileClaimsRequestV1.getFileName());
        uploadFileRequestV1.setFileContent(uploadFileClaimsRequestV1.getFileContent());
        uploadFileRequestV1.setDescription(uploadFileClaimsRequestV1.getDescription());
        uploadFileRequestV1.setUuid(uploadFileClaimsRequestV1.getUuid());
        uploadFileRequestV1.setResourceType(uploadFileClaimsRequestV1.getResourceType());
        uploadFileRequestV1.setExternalId(uploadFileClaimsRequestV1.getExternalId());

        return uploadFileRequestV1;

    }


    public static List<UploadFile> adptFromListUploadFileClaimsRequestV1ToListUploadFile(List<UploadFileClaimsRequestV1> uploadFileClaimsRequestV1List ){
        List<UploadFile> uploadFileList = new ArrayList<>();

        for(UploadFileClaimsRequestV1 uploadFileClaimsRequestV1 : uploadFileClaimsRequestV1List){
            UploadFileRequestV1 uploadFileRequestV1 = adptFromUploadFileClaimsRequestV1ToUploadFileRequestV1(uploadFileClaimsRequestV1);
            UploadFile uploadFile = adptUploadFileRequestToUploadFile(uploadFileRequestV1);
            uploadFileList.add(uploadFile);
        }
        return uploadFileList;
    }

    public static UploadFile adptFromDownloadFileBase64ResponseV1ToListUploadFile (DownloadFileBase64ResponseV1 downloadFileBase64ResponseV1){
        UploadFile uploadFile = new UploadFile();
        uploadFile.setFileName(downloadFileBase64ResponseV1.getFileName());
        uploadFile.setFileContent(downloadFileBase64ResponseV1.getFileContent());
        uploadFile.setDescription(downloadFileBase64ResponseV1.getDescription());
        uploadFile.setUuid(downloadFileBase64ResponseV1.getResourceId());
        uploadFile.setBlobType(downloadFileBase64ResponseV1.getBlobType());
        return  uploadFile;
    }
}
