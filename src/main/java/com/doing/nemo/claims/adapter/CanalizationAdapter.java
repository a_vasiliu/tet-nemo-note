package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.counterparty.CanalizationRequest;
import com.doing.nemo.claims.controller.payload.response.counterparty.CanalizationResponse;
import com.doing.nemo.claims.entity.jsonb.repair.Canalization;
import org.springframework.stereotype.Component;

@Component
public class CanalizationAdapter {

    public static Canalization adptFromCanalizationRequestToCanalization(CanalizationRequest canalizationRequest)
    {
        Canalization canalization = null;
        if(canalizationRequest != null) {
            canalization = new Canalization();
            canalization.setDate(DateUtil.convertIS08601StringToUTCDate(canalizationRequest.getDate()));
            if(canalizationRequest.getCenter() != null)
            canalization.setCenterEntity(CenterAdpter.adptFromCenterRequestToCenterEntity(canalizationRequest.getCenter()));
        }
        return canalization;

    }

    public static CanalizationResponse adptFromCanalizationToCanalizationResponse(Canalization canalization)
    {
        CanalizationResponse canalizationResponse = null;
        if(canalization != null) {
            canalizationResponse = new CanalizationResponse();
            canalizationResponse.setDate(DateUtil.convertUTCDateToIS08601String(canalization.getDate()));
            //canalizationResponse.setRepairer(RepairerAdapter.adptFromRepairerEntityToRepairerResponse(canalization.getRepairer()));
            canalizationResponse.setCenter(CenterAdpter.adptFromCenterEntityToCenterResponse(canalization.getCenterEntity()));

        }
        return canalizationResponse;

    }
}
