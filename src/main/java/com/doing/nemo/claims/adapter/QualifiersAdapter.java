package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.controller.payload.response.damaged.QualifiersResponse;
import com.doing.nemo.claims.entity.esb.ContractESB.QualifiersEsb;
import com.doing.nemo.middleware.client.payload.response.MiddlewareQualifierResponse;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

@Component
public class QualifiersAdapter {

    public static QualifiersResponse adptQualifiersEsbToQualifiersResponse(QualifiersEsb qualifierEsb) {

        if (qualifierEsb != null) {

            QualifiersResponse qualifierResponse = new QualifiersResponse();
            qualifierResponse.setAttribute(qualifierEsb.getAttribute());
            qualifierResponse.setAttributeDescription(qualifierEsb.getAttributeDescription());
            qualifierResponse.setValue(qualifierEsb.getValue());
            qualifierResponse.setValueDescription(qualifierEsb.getValueDescription());
            return qualifierResponse;
        }
        return null;
    }

    public static List<QualifiersResponse> adptFromQualifiersESBToQualifiersResponseList(List<QualifiersEsb> qualifiersEsbs) {
        List<QualifiersResponse> qualifiersResponseList = new LinkedList<>();
        for (QualifiersEsb qualifiersEsb : qualifiersEsbs) {
            qualifiersResponseList.add(adptQualifiersEsbToQualifiersResponse(qualifiersEsb));
        }
        return qualifiersResponseList;
    }

    public static List<QualifiersResponse> adptFromMiddlewareQualifierResponseToQualifiersResponseList(List<MiddlewareQualifierResponse>  middlewareQualifierResponses) {
        List<QualifiersResponse> qualifiersResponseList = new LinkedList<>();
        for (MiddlewareQualifierResponse middlewareQualifierResponse : middlewareQualifierResponses) {
            qualifiersResponseList.add(adptMiddlewareQualifierResponseToQualifiersResponse(middlewareQualifierResponse));
        }
        return qualifiersResponseList;
    }

    public static QualifiersResponse adptMiddlewareQualifierResponseToQualifiersResponse(MiddlewareQualifierResponse middlewareQualifierResponse) {

        if (middlewareQualifierResponse != null) {

            QualifiersResponse qualifierResponse = new QualifiersResponse();
            qualifierResponse.setAttribute(middlewareQualifierResponse.getAttribute());
            qualifierResponse.setAttributeDescription(middlewareQualifierResponse.getAttributeDescription());
            qualifierResponse.setValue(middlewareQualifierResponse.getValue());
            qualifierResponse.setValueDescription(middlewareQualifierResponse.getValueDescription());
            return qualifierResponse;
        }
        return null;
    }
}
