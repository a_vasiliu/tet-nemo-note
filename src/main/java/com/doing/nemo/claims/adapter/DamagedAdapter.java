package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.controller.payload.request.ClaimsInsertEnjoyRequest.DamagedEnjoyRequest;
import com.doing.nemo.claims.controller.payload.request.ClaimsInsertExternalRequest;
import com.doing.nemo.claims.controller.payload.request.claims.DamagedRequest;
import com.doing.nemo.claims.controller.payload.response.authority.claims.DamagedAuthorityResponseV1;
import com.doing.nemo.claims.controller.payload.response.claims.DamagedResponse;
import com.doing.nemo.claims.controller.payload.response.external.DamagedExternalResponse;
import com.doing.nemo.claims.entity.AntiTheftRequestEntity;
import com.doing.nemo.claims.entity.jsonb.damaged.Damaged;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbAntiTheftService.AntiTheftService;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class DamagedAdapter {

    public static Damaged adptDamagedExternalRequestToDamaged(ClaimsInsertExternalRequest.DamagedExternalRequest damagedRequest) {

        if (damagedRequest != null) {

            Damaged damaged = new Damaged();

            damaged.setAdditionalCosts(AdditionalCostsAdapter.adptAdditionalCostsRequestToAdditionalCosts(damagedRequest.getAdditionalCosts()));
            damaged.setCaiSigned(damagedRequest.getCaiSigned());
            damaged.setImpactPoint(ImpactPointAdapter.adptImpactPointRequestToImpactPoint(damagedRequest.getImpactPoint()));

            return damaged;
        }
        return null;
    }

    public static Damaged adptDamagedRequestToDamaged(DamagedRequest damagedRequest) {

        if (damagedRequest != null) {

            Damaged damaged = new Damaged();

            damaged.setAdditionalCosts(AdditionalCostsAdapter.adptAdditionalCostsRequestToAdditionalCosts(damagedRequest.getAdditionalCosts()));
            damaged.setCaiSigned(damagedRequest.getCaiSigned());
            damaged.setDriver(DriverAdapter.adptDriverRequestToDriver(damagedRequest.getDriver()));
            damaged.setImpactPoint(ImpactPointAdapter.adptImpactPointRequestToImpactPoint(damagedRequest.getImpactPoint()));
            damaged.setInsuranceCompany(InsuranceCompanyAdapter.adptInsuranceCompanyRequestToInsuranceCompany(damagedRequest.getInsuranceCompany()));
            damaged.setContract(ContractAdapter.adptContractRequestToContract(damagedRequest.getContract()));
            damaged.setCustomer(CustomerAdapter.adptCustomerRequestToCustomer(damagedRequest.getCustomer()));
            damaged.setVehicle(VehicleAdapter.adptVehicleRequestToVehicle(damagedRequest.getVehicle()));
            damaged.setFleetManagerList(FleetManagerAdpter.adptFromFMRequestToFM(damagedRequest.getFleetManagerList()));
            damaged.setAntiTheftService(AntiTheftServiceAdapter.adptAntiTheftServiceRequestToAntiTheftService(damagedRequest.getAntiTheftServiceRequest()));
            if(damaged.getAntiTheftService() != null ) {
                if (damagedRequest.getAntiTheftServiceRequest() != null) {
                    damaged.getAntiTheftService().setRegistryList(RegistryAdapter.adptFromRegistryRequestToRegistryList(damagedRequest.getAntiTheftServiceRequest().getRegistryRequestV1List()));
                    damaged.getAntiTheftService().setAntiTheftList(damagedRequest.getAntiTheftServiceRequest().getAntiTheftList());
                }
            }
            return damaged;
        }
        return null;
    }

    public static Damaged adptDamagedRequestInsertToDamaged(DamagedRequest damagedRequest) {

        if (damagedRequest != null) {

            Damaged damaged = new Damaged();
            damaged.setAdditionalCosts(AdditionalCostsAdapter.adptAdditionalCostsRequestToAdditionalCosts(damagedRequest.getAdditionalCosts()));
            damaged.setCaiSigned(damagedRequest.getCaiSigned());
            damaged.setDriver(DriverAdapter.adptDriverRequestToDriver(damagedRequest.getDriver()));
            damaged.setImpactPoint(ImpactPointAdapter.adptImpactPointRequestToImpactPoint(damagedRequest.getImpactPoint()));
            damaged.setInsuranceCompany(InsuranceCompanyAdapter.adptInsuranceCompanyRequestToInsuranceCompany(damagedRequest.getInsuranceCompany()));
            damaged.setContract(ContractAdapter.adptContractRequestToContract(damagedRequest.getContract()));
            damaged.setCustomer(CustomerAdapter.adptCustomerRequestToCustomer(damagedRequest.getCustomer()));
            damaged.setVehicle(VehicleAdapter.adptVehicleRequestToVehicle(damagedRequest.getVehicle()));
            damaged.setFleetManagerList(FleetManagerAdpter.adptFromFMRequestToFM(damagedRequest.getFleetManagerList()));
            if(damagedRequest.getAntiTheftServiceRequest() != null) {
                damaged.setAntiTheftService(new AntiTheftService());
                damaged.getAntiTheftService().setRegistryList(RegistryAdapter.adptFromRegistryRequestToRegistryList(damagedRequest.getAntiTheftServiceRequest().getRegistryRequestV1List()));
            }
            return damaged;
        }
        return null;
    }

    public static DamagedResponse adptDamagedToDamagedResponse(Damaged damaged, List<AntiTheftRequestEntity> antiTheftRequestEntityList) {

        if (damaged != null) {

            DamagedResponse damagedResponse = new DamagedResponse();
            damagedResponse.setAdditionalCosts(AdditionalCostsAdapter.adptAdditionalCostsToAdditionalCostsResponse(damaged.getAdditionalCosts()));
            damagedResponse.setCaiSigned(damaged.getCaiSigned());
            damagedResponse.setDriver(DriverAdapter.adptDriverToDriverResponse(damaged.getDriver()));
            damagedResponse.setImpactPoint(ImpactPointAdapter.adptImpactPointToImpactPointResponse(damaged.getImpactPoint()));
            damagedResponse.setInsuranceCompany(InsuranceCompanyAdapter.adptInsuranceCompanyToInsuranceCompanyResponse(damaged.getInsuranceCompany()));
            damagedResponse.setContract(ContractAdapter.adptContractToContractResponse(damaged.getContract()));
            damagedResponse.setCustomer(CustomerAdapter.adptCustomerToCustomerResposne(damaged.getCustomer()));
            damagedResponse.setVehicle(VehicleAdapter.adptVehicleToVehicleResponse(damaged.getVehicle()));
            damagedResponse.setFleetManagerList(FleetManagerAdpter.adptFromFMToFMResponse(damaged.getFleetManagerList()));
            damagedResponse.setAntiTheftServiceResponseV1(AntiTheftServiceAdapter.adptAntiTheftServiceToAntiTheftServiceResponse(damaged.getAntiTheftService(), antiTheftRequestEntityList));

            return damagedResponse;
        }
        return null;
    }

    public static DamagedExternalResponse adptDamagedToDamagedExternalResponse(Damaged damaged, List<AntiTheftRequestEntity> antiTheftRequestEntityList) {

        if (damaged != null) {

            DamagedExternalResponse damagedResponse = new DamagedExternalResponse();
            damagedResponse.setAdditionalCosts(AdditionalCostsAdapter.adptAdditionalCostsToAdditionalCostsResponse(damaged.getAdditionalCosts()));
            damagedResponse.setCaiSigned(damaged.getCaiSigned());
            damagedResponse.setDriver(DriverAdapter.adptDriverToDriverResponse(damaged.getDriver()));
            damagedResponse.setImpactPoint(ImpactPointAdapter.adptImpactPointToImpactPointResponse(damaged.getImpactPoint()));
            damagedResponse.setInsuranceCompany(InsuranceCompanyAdapter.adptInsuranceCompanyToInsuranceCompanyResponse(damaged.getInsuranceCompany()));
            damagedResponse.setContract(ContractAdapter.adptContractToContractResponse(damaged.getContract()));
            damagedResponse.setCustomer(CustomerAdapter.adptCustomerToCustomerResposne(damaged.getCustomer()));
            damagedResponse.setVehicle(VehicleAdapter.adptVehicleToVehicleResponse(damaged.getVehicle()));
            damagedResponse.setFleetManagerList(FleetManagerAdpter.adptFromFMToFMResponse(damaged.getFleetManagerList()));
            damagedResponse.setAntiTheftService(AntiTheftServiceAdapter.adptAntiTheftServiceToAntiTheftServiceResponse(damaged.getAntiTheftService(), antiTheftRequestEntityList));
            return damagedResponse;
        }
        return null;
    }

    public static DamagedAuthorityResponseV1 adptFromDamagedToDamagedAuthorityResponse(Damaged damaged){
        if(damaged == null) return null;
        DamagedAuthorityResponseV1 damagedAuthorityResponseV1 = new DamagedAuthorityResponseV1();
        damagedAuthorityResponseV1.setImpactPoint(ImpactPointAdapter.adptImpactPointToImpactPointResponse(damaged.getImpactPoint()));

        return damagedAuthorityResponseV1;
    }


    public static Damaged adptDamagedEnjoyRequestToDamaged(DamagedEnjoyRequest damagedEnjoyRequest){
        if (damagedEnjoyRequest != null) {

            Damaged damaged = new Damaged();

            damaged.setAdditionalCosts(AdditionalCostsAdapter.adptAdditionalCostsRequestToAdditionalCosts(damagedEnjoyRequest.getAdditionalCosts()));
            damaged.setCaiSigned(damagedEnjoyRequest.getCaiSigned());
            damaged.setImpactPoint(ImpactPointAdapter.adptImpactPointRequestToImpactPoint(damagedEnjoyRequest.getImpactPoint()));

            return damaged;
        }
        return null;
    }
}
