package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.common.validation.MessageCode;
import com.doing.nemo.claims.common.validation.RequestValidator;
import com.doing.nemo.claims.controller.payload.request.EmailTemplateRequestV1;
import com.doing.nemo.claims.controller.payload.response.ClaimsResponsePaginationV1;
import com.doing.nemo.claims.controller.payload.response.EmailTemplateResponseV1;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.enumerated.EmailTemplateEnum;
import com.doing.nemo.claims.entity.settings.EmailTemplateEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

@Component
public class EmailTemplateAdapter {

    @Autowired
    private RecipientTypeAdapter recipientTypeAdapter;

    @Autowired
    private RequestValidator requestValidator;

    public EmailTemplateEntity getEmailTemplateEntity(EmailTemplateRequestV1 emailTemplateRequestV1) {

        EmailTemplateEntity emailTemplateEntity = new EmailTemplateEntity();

        requestValidator.validateRequest(emailTemplateRequestV1.getApplicationEvent(), MessageCode.E00X_1000);
        emailTemplateEntity.setApplicationEvent(EventTypeAdapter.getEventTypeEntityWithId(emailTemplateRequestV1.getApplicationEvent()));
        emailTemplateEntity.setType(emailTemplateRequestV1.getType());
        emailTemplateEntity.setDescription(emailTemplateRequestV1.getDescription());
        emailTemplateEntity.setObject(emailTemplateRequestV1.getObject());
        emailTemplateEntity.setHeading(emailTemplateRequestV1.getHeading());
        emailTemplateEntity.setBody(emailTemplateRequestV1.getBody());
        emailTemplateEntity.setFoot(emailTemplateRequestV1.getFoot());
        emailTemplateEntity.setAttachFile(emailTemplateRequestV1.getAttachFile());
        emailTemplateEntity.setSplittingRecipientsEmail(emailTemplateRequestV1.getSplittingRecipientsEmail());
        emailTemplateEntity.setClientCode(emailTemplateRequestV1.getClientCode());
        emailTemplateEntity.setActive(emailTemplateRequestV1.getActive());
        emailTemplateEntity.setFlows(emailTemplateRequestV1.getFlows());
        emailTemplateEntity.setAccidentTypeList(emailTemplateRequestV1.getAccidentTypeList());
        emailTemplateEntity.setType(EmailTemplateEnum.GENERIC);
        if (emailTemplateRequestV1.getCustomers() != null)
            emailTemplateEntity.setCustomers(recipientTypeAdapter.getRecipientTypeListEntity(emailTemplateRequestV1.getCustomers()));

        return emailTemplateEntity;
    }

    public static EmailTemplateResponseV1 getEmailTemplateAdapter(EmailTemplateEntity emailTemplateEntity) {

        EmailTemplateResponseV1 emailTemplateResponseV1 = new EmailTemplateResponseV1();

        emailTemplateResponseV1.setId(emailTemplateEntity.getId());
        emailTemplateResponseV1.setApplicationEvent(EventTypeAdapter.getEventTypeAdapter(emailTemplateEntity.getApplicationEvent()));
        emailTemplateResponseV1.setType(emailTemplateEntity.getType());
        emailTemplateResponseV1.setDescription(emailTemplateEntity.getDescription());
        emailTemplateResponseV1.setObject(emailTemplateEntity.getObject());
        emailTemplateResponseV1.setHeading(emailTemplateEntity.getHeading());
        emailTemplateResponseV1.setBody(emailTemplateEntity.getBody());
        emailTemplateResponseV1.setFoot(emailTemplateEntity.getFoot());
        emailTemplateResponseV1.setAttachFile(emailTemplateEntity.getAttachFile());
        emailTemplateResponseV1.setSplittingRecipientsEmail(emailTemplateEntity.getSplittingRecipientsEmail());
        emailTemplateResponseV1.setClientCode(emailTemplateEntity.getClientCode());
        emailTemplateResponseV1.setCustomers(RecipientTypeAdapter.getRecipientTypeListAdapter(emailTemplateEntity.getCustomers()));
        emailTemplateResponseV1.setActive(emailTemplateEntity.getActive());
        emailTemplateResponseV1.setTypeComplaint(emailTemplateEntity.getTypeComplaint());
        emailTemplateResponseV1.setFlows(emailTemplateEntity.getFlows());
        emailTemplateResponseV1.setAccidentTypeList(emailTemplateEntity.getAccidentTypeList());

        return emailTemplateResponseV1;
    }

    public static List<EmailTemplateResponseV1> getEmailTemplateListAdapter(List<EmailTemplateEntity> emailTemplateEntityList) {

        List<EmailTemplateResponseV1> emailTemplateResponseV1List = new LinkedList<>();

        for (EmailTemplateEntity emailTemplateEntity : emailTemplateEntityList) {
            emailTemplateResponseV1List.add(getEmailTemplateAdapter(emailTemplateEntity));
        }

        return emailTemplateResponseV1List;
    }

    public static ClaimsResponsePaginationV1 adptEmailTemplatePaginationToClaimsResponsePagination(Pagination<EmailTemplateEntity> pagination) {

        ClaimsResponsePaginationV1<EmailTemplateResponseV1> claimsPaginationResponse = new ClaimsResponsePaginationV1<>();

        claimsPaginationResponse.setStats(pagination.getStats());
        claimsPaginationResponse.setItems(getEmailTemplateListAdapter(pagination.getItems()));

        return claimsPaginationResponse;
    }
}
