package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.controller.payload.response.StatsResponseV2;
import com.doing.nemo.claims.dto.StatsDTO;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;



@Component
public class StatsDTOResponseV2Adapter {

    public StatsResponseV2 adaptToResponse(StatsDTO statsDTO) {

        if (statsDTO == null) {
            return null;
        }

        StatsResponseV2 statsResponse = new StatsResponseV2();

        statsResponse.setCurrentPage(statsDTO.getCurrentPage());
        statsResponse.setItemCount(statsDTO.getItemCount());
        statsResponse.setPageCount(statsDTO.getPageCount());
        statsResponse.setPageSize(statsDTO.getPageSize());

        return statsResponse;
    }

    public List<StatsResponseV2> adaptToResponse(List<StatsDTO> input) {
        List<StatsResponseV2> output = new ArrayList<>();
        if (input != null) {
            input.forEach(element -> output.add(adaptToResponse(element)));
        }
        return output;
    }
}
