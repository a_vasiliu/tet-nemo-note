package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.controller.payload.request.insurancecompany.PaiRequest;
import com.doing.nemo.claims.controller.payload.response.insurancecompany.PaiResponse;
import com.doing.nemo.claims.entity.esb.InsuranceCompanyESB.PaiEsb;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbInsuranceCompany.Pai;
import com.doing.nemo.middleware.client.payload.response.MiddlewarePaiResponse;
import org.springframework.stereotype.Component;

@Component
public class PaiAdapter {

    public static Pai adptPaiRequestToPai(PaiRequest paiRequest) {

        if (paiRequest != null) {

            Pai pai = new Pai();

            pai.setServiceId(paiRequest.getServiceId());
            pai.setService(paiRequest.getService());
            pai.setMaxCoverage(paiRequest.getMaxCoverage());
            pai.setDeductibleId(paiRequest.getDeductibleId());
            pai.setDeductible(paiRequest.getDeductible());
            pai.setCompanyId(paiRequest.getCompanyId());
            pai.setCompany(paiRequest.getCompany());
            pai.setTariffId(paiRequest.getTariffId());
            pai.setTariff(paiRequest.getTariff());
            pai.setTariffCode(paiRequest.getTariffCode());
            pai.setPercentage(paiRequest.getPercentage());
            pai.setStartDate(paiRequest.getStartDate());
            pai.setEndDate(paiRequest.getEndDate());
            pai.setPolicyNumber(paiRequest.getPolicyNumber());
            pai.setMedicalCosts(paiRequest.getMedicalCosts());
            pai.setInsuranceCompanyId(paiRequest.getInsuranceCompanyId());
            pai.setInsurancePolicyId(paiRequest.getInsurancePolicyId());

            return pai;
        }
        return null;
    }

    public static PaiResponse adptPaiToPaiResponse(Pai pai) {

        if (pai != null) {

            PaiResponse paiResponse = new PaiResponse();

            paiResponse.setServiceId(pai.getServiceId());
            paiResponse.setService(pai.getService());
            paiResponse.setMaxCoverage(pai.getMaxCoverage());
            paiResponse.setDeductibleId(pai.getDeductibleId());
            paiResponse.setDeductible(pai.getDeductible());
            paiResponse.setCompanyId(pai.getCompanyId());
            paiResponse.setCompany(pai.getCompany());
            paiResponse.setTariffId(pai.getTariffId());
            paiResponse.setTariff(pai.getTariff());
            paiResponse.setTariffCode(pai.getTariffCode());
            paiResponse.setPercentage(pai.getPercentage());
            paiResponse.setStartDate(pai.getStartDate());
            paiResponse.setEndDate(pai.getEndDate());
            paiResponse.setPolicyNumber(pai.getPolicyNumber());
            paiResponse.setMedicalCosts(pai.getMedicalCosts());
            paiResponse.setInsuranceCompanyId(pai.getInsuranceCompanyId());
            paiResponse.setInsurancePolicyId(pai.getInsurancePolicyId());

            return paiResponse;
        }
        return null;
    }

    public static Pai adptPaiResponseToPai(PaiResponse paiResponse) {

        if (paiResponse != null) {

            Pai pai = new Pai();

            pai.setServiceId(paiResponse.getServiceId());
            pai.setService(paiResponse.getService());
            pai.setMaxCoverage(paiResponse.getMaxCoverage());
            pai.setDeductibleId(paiResponse.getDeductibleId());
            pai.setDeductible(paiResponse.getDeductible());
            pai.setCompanyId(paiResponse.getCompanyId());
            pai.setCompany(paiResponse.getCompany());
            pai.setTariffId(paiResponse.getTariffId());
            pai.setTariff(paiResponse.getTariff());
            pai.setTariffCode(paiResponse.getTariffCode());
            pai.setPercentage(paiResponse.getPercentage());
            pai.setStartDate(paiResponse.getStartDate());
            pai.setEndDate(paiResponse.getEndDate());
            pai.setPolicyNumber(paiResponse.getPolicyNumber());
            pai.setMedicalCosts(paiResponse.getMedicalCosts());
            pai.setInsuranceCompanyId(paiResponse.getInsuranceCompanyId());
            pai.setInsurancePolicyId(paiResponse.getInsurancePolicyId());

            return pai;
        }
        return null;
    }

    public static PaiResponse adptPaiEsbToPaiResponse(PaiEsb paiEsb) {

        if (paiEsb != null) {

            PaiResponse paiResponse = new PaiResponse();

            paiResponse.setServiceId(paiEsb.getServiceId());
            paiResponse.setService(paiEsb.getService());
            paiResponse.setMaxCoverage(paiEsb.getMaxCoverage());
            paiResponse.setDeductibleId(paiEsb.getDeductibleId());
            paiResponse.setDeductible(paiEsb.getDeductible());
            paiResponse.setCompanyId(paiEsb.getCompanyId());
            paiResponse.setCompany(paiEsb.getCompany());
            if (paiEsb.getTariffId() != null){
                Double doubleValue = Double.parseDouble(paiEsb.getTariffId());
                Long longValue = doubleValue.longValue();
                paiResponse.setTariffId(longValue.toString());
            }
            paiResponse.setTariff(paiEsb.getTariff());
            paiResponse.setTariffCode(paiEsb.getTariffCode());
            paiResponse.setPercentage(paiEsb.getPercentage());
            paiResponse.setStartDate(paiEsb.getStartDate());
            paiResponse.setEndDate(paiEsb.getEndDate());
            paiResponse.setPolicyNumber(paiEsb.getPolicyNumber());
            paiResponse.setMedicalCosts(paiEsb.getMedicalCosts());

            return paiResponse;
        }
        return null;
    }

    public static PaiResponse adptPaiMiddlewareToPaiResponse(MiddlewarePaiResponse paiEsb) {

        if (paiEsb != null) {

            PaiResponse paiResponse = new PaiResponse();

            if(paiEsb.getMaxCoverage() != null) {
                paiResponse.setMaxCoverage(paiEsb.getMaxCoverage().toString());
            }
            if(paiEsb.getCompanyId() != null) {
                paiResponse.setCompanyId(paiEsb.getCompanyId().toString());
            }
            paiResponse.setCompany(paiEsb.getCompany());
            if (paiEsb.getTariffId() != null){
                Double doubleValue = Double.parseDouble(paiEsb.getTariffId().toString());
                Long longValue = doubleValue.longValue();
                paiResponse.setTariffId(longValue.toString());
            }
            paiResponse.setTariff(paiEsb.getTariff());
            paiResponse.setTariffCode(paiEsb.getTariffCode());
            paiResponse.setPercentage(paiEsb.getPercentage());
            paiResponse.setStartDate(paiEsb.getStartdate());
            paiResponse.setEndDate(paiEsb.getEnddate());
            paiResponse.setPolicyNumber(paiEsb.getPolicyNumber());
            //paiResponse.setMedicalCosts(paiEsb.getMedicalCosts());

            return paiResponse;
        }
        return null;
    }
}
