package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.controller.payload.response.VehicleRegistrationResponse;
import com.doing.nemo.claims.controller.payload.response.registration.PlateRegistrationHistoryItem;
import com.doing.nemo.middleware.client.payload.response.MiddlewareVehicleRegistrationsResponse;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class VehicleRegistrationResponseAdapter {
    @Autowired
    PlateRegistrationAdapter plateRegistrationAdapter;

    public VehicleRegistrationResponse adaptToDTO(MiddlewareVehicleRegistrationsResponse input, VehicleRegistrationResponse output) {

        if (input == null) {
            return output;
        }

        if (CollectionUtils.isEmpty(input.getPlateHistoryItems())) {
            return output;
        }

        if (output == null) {
            output = new VehicleRegistrationResponse();
        }

        List<PlateRegistrationHistoryItem> responseList = plateRegistrationAdapter.adaptToResponse(input.getPlateHistoryItems());
        output.setPlateHistoryItems(responseList);
        output.setPlate(input.getPlate());
        output.setCurrentPlate(input.getCurrentPlate());

        return output;
    }
}
