package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.controller.payload.request.MotivationRequestV1;
import com.doing.nemo.claims.controller.payload.response.MotivationResponseV1;
import com.doing.nemo.claims.controller.payload.response.PaginationResponseV1;
import com.doing.nemo.claims.entity.enumerated.DTO.PageStats;
import com.doing.nemo.claims.entity.settings.MotivationEntity;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

@Component
public class MotivationAdapter {


    public static MotivationEntity adptFromMotivationRequestToMotivationEntity(MotivationRequestV1 motivationRequestV1) {
        MotivationEntity motivationEntity = new MotivationEntity();
        motivationEntity.setAnswerType(motivationRequestV1.getAnswerType());
        motivationEntity.setDescription(motivationRequestV1.getDescription());
        motivationEntity.setActive(motivationRequestV1.getActive());

        return motivationEntity;
    }


    public static MotivationResponseV1 adptFromMotivationEntityToMotivationResponseV1(MotivationEntity motivationEntity) {
        MotivationResponseV1 motivationResponseV1 = new MotivationResponseV1();
        motivationResponseV1.setId(motivationEntity.getId());
        motivationResponseV1.setAnswerType(motivationEntity.getAnswerType());
        motivationResponseV1.setDescription(motivationEntity.getDescription());
        motivationResponseV1.setActive(motivationEntity.getActive());
        motivationResponseV1.setTypeComplaint(motivationEntity.getTypeComplaint());


        return motivationResponseV1;
    }

    public static List<MotivationResponseV1> adptFromMotivationEntityToMotivationResponseV1List(List<MotivationEntity> motivationEntityList) {
        List<MotivationResponseV1> motivationResponseV1 = new LinkedList<>();
        for (MotivationEntity motivationsEntity : motivationEntityList) {
            motivationResponseV1.add(adptFromMotivationEntityToMotivationResponseV1(motivationsEntity));
        }
        return motivationResponseV1;
    }

    public static PaginationResponseV1<MotivationResponseV1> adptPagination(List<MotivationEntity> motivationEntityList, int page, int pageSize, Long itemCount) {

        double totPages = Math.ceil((double) itemCount.intValue() / pageSize);
        PageStats pageStats = new PageStats();
        pageStats.setCurrentPage(page);
        pageStats.setItemCount(itemCount.intValue());
        pageStats.setPageSize(pageSize);
        pageStats.setPageCount((long)totPages);

        PaginationResponseV1<MotivationResponseV1> paginationResponseV1 = new PaginationResponseV1(pageStats,adptFromMotivationEntityToMotivationResponseV1List(motivationEntityList));

        return paginationResponseV1;
    }

}