package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.controller.payload.request.EmailTemplateMessagingRequestV1;
import com.doing.nemo.claims.controller.payload.request.SendMailRequestV1;
import com.doing.nemo.claims.controller.payload.response.EmailTemplateMessagingResponseV1;
import com.doing.nemo.claims.entity.enumerated.BodyType;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Component
public class MessagingAdapter {

    public static EmailTemplateMessagingRequestV1 adptEmailTemplateMessagingRequestToEmailTemplateMessagingResponse(EmailTemplateMessagingResponseV1 emailTemplateMessagingResponseV1)
    {
        EmailTemplateMessagingRequestV1 emailTemplateMessagingRequestV1 = new EmailTemplateMessagingRequestV1();
        if(emailTemplateMessagingResponseV1 != null)
        {
            emailTemplateMessagingRequestV1.setTos(emailTemplateMessagingResponseV1.getTos());
            emailTemplateMessagingRequestV1.setSplittingRecipientsEmail(emailTemplateMessagingResponseV1.getSplittingRecipientsEmail());
            emailTemplateMessagingRequestV1.setObject(emailTemplateMessagingResponseV1.getObject());
            emailTemplateMessagingRequestV1.setHeading(emailTemplateMessagingResponseV1.getHeading());
            emailTemplateMessagingRequestV1.setFoot(emailTemplateMessagingResponseV1.getFoot());
            emailTemplateMessagingRequestV1.setCcs(emailTemplateMessagingResponseV1.getCcs());
            emailTemplateMessagingRequestV1.setBody(emailTemplateMessagingResponseV1.getBody());
            emailTemplateMessagingRequestV1.setBccs(emailTemplateMessagingResponseV1.getBccs());
            emailTemplateMessagingRequestV1.setAttachFile(emailTemplateMessagingResponseV1.getAttachFile());
        }
        return emailTemplateMessagingRequestV1;
    }

    public static List<EmailTemplateMessagingRequestV1> adptEmailTemplateMessagingRequestToEmailTemplateMessagingResponseList(List<EmailTemplateMessagingResponseV1> emailTemplateMessagingResponseV1) {

        if (emailTemplateMessagingResponseV1 == null)
            return null;

        List<EmailTemplateMessagingRequestV1> emailTemplateMessagingRequestV1s = new ArrayList<>();
        for (EmailTemplateMessagingResponseV1 att : emailTemplateMessagingResponseV1) {
            emailTemplateMessagingRequestV1s.add(adptEmailTemplateMessagingRequestToEmailTemplateMessagingResponse(att));
        }

        return emailTemplateMessagingRequestV1s;
    }

    public static EmailTemplateMessagingResponseV1 adptEmailTemplateMessagingResponseToEmailTemplateMessagingRequest(EmailTemplateMessagingRequestV1 emailTemplateMessagingRequestV1)
    {
        EmailTemplateMessagingResponseV1 emailTemplateMessagingResponseV1 = new EmailTemplateMessagingResponseV1();
        if(emailTemplateMessagingRequestV1 != null)
        {
            emailTemplateMessagingResponseV1.setTos(emailTemplateMessagingResponseV1.getTos());
            emailTemplateMessagingResponseV1.setSplittingRecipientsEmail(emailTemplateMessagingResponseV1.getSplittingRecipientsEmail());
            emailTemplateMessagingResponseV1.setObject(emailTemplateMessagingResponseV1.getObject());
            emailTemplateMessagingResponseV1.setHeading(emailTemplateMessagingResponseV1.getHeading());
            emailTemplateMessagingResponseV1.setFoot(emailTemplateMessagingResponseV1.getFoot());
            emailTemplateMessagingResponseV1.setCcs(emailTemplateMessagingResponseV1.getCcs());
            emailTemplateMessagingResponseV1.setBody(emailTemplateMessagingResponseV1.getBody());
            emailTemplateMessagingResponseV1.setBccs(emailTemplateMessagingResponseV1.getBccs());
            emailTemplateMessagingResponseV1.setAttachFile(emailTemplateMessagingResponseV1.getAttachFile());
        }
        return emailTemplateMessagingResponseV1;
    }

    public static List<EmailTemplateMessagingResponseV1> adptEmailTemplateMessagingResponseToEmailTemplateMessagingRequest(List<EmailTemplateMessagingRequestV1> emailTemplateMessagingRequestV1) {

        if (emailTemplateMessagingRequestV1 == null)
            return null;

        List<EmailTemplateMessagingResponseV1> emailTemplateMessagingRequestV1s = new ArrayList<>();
        for (EmailTemplateMessagingRequestV1 att : emailTemplateMessagingRequestV1) {
            emailTemplateMessagingRequestV1s.add(adptEmailTemplateMessagingResponseToEmailTemplateMessagingRequest(att));
        }

        return emailTemplateMessagingRequestV1s;
    }


    public static SendMailRequestV1 adptEmailTemplateMessagingRequestToSendMailRequest(EmailTemplateMessagingRequestV1 emailTemplateMessagingReques) {

        SendMailRequestV1 sendMailRequest = new SendMailRequestV1();
        sendMailRequest.setTos(emailTemplateMessagingReques.getTos());
        sendMailRequest.setCcs(emailTemplateMessagingReques.getCcs());
        sendMailRequest.setBccs(emailTemplateMessagingReques.getBccs());
        sendMailRequest.setBodyType(BodyType.HTML);
        sendMailRequest.setSubject(emailTemplateMessagingReques.getObject());
        String bodyContent = "";
        if(emailTemplateMessagingReques.getHeading()!=null)
            bodyContent = emailTemplateMessagingReques.getHeading() + "\n";
        bodyContent += emailTemplateMessagingReques.getBody() + "\n";
        if(emailTemplateMessagingReques.getFoot()!=null)
            bodyContent += emailTemplateMessagingReques.getFoot();
        sendMailRequest.setBodyContent(bodyContent);
        sendMailRequest.setFilemanagerIdAttachmentList(emailTemplateMessagingReques.getAttachmentList());
        return sendMailRequest;
    }


    public static List<SendMailRequestV1> adptEmailTemplateMessagingRequestToSendMailRequest(List<EmailTemplateMessagingRequestV1> emailTemplateMessagingRequesList) {

        if (emailTemplateMessagingRequesList == null)
            return null;

        List<SendMailRequestV1> sendMailRequestList = new LinkedList<>();
        for (EmailTemplateMessagingRequestV1 att : emailTemplateMessagingRequesList) {
            sendMailRequestList.add(adptEmailTemplateMessagingRequestToSendMailRequest(att));
        }

        return sendMailRequestList;
    }
}
