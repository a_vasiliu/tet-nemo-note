package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.controller.payload.request.counterparty.InsuredRequest;
import com.doing.nemo.claims.controller.payload.response.counterparty.InsuredResponse;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.Insured;
import org.springframework.stereotype.Component;

@Component
public class InsuredAdapter {

    public static Insured adptInsuredRequestToInsured(InsuredRequest insuredRequest) {

        if (insuredRequest != null) {

            Insured insured = new Insured();
            insured.setAddress(insuredRequest.getAddress());
            insured.setDrivingLicense(DrivingLicenseAdapter.adptDrivingLicenseRequestToDrivingLicense(insuredRequest.getDrivingLicense()));
            insured.setEmail(insuredRequest.getEmail());
            insured.setFirstname(insuredRequest.getFirstname());
            insured.setFiscalCode(insuredRequest.getFiscalCode());
            insured.setLastname(insuredRequest.getLastname());
            insured.setPhone(insuredRequest.getPhone());
            insured.setCustomerId(insuredRequest.getCustomerId());

            return insured;
        }
        return null;
    }

    public static InsuredResponse adptInsuredToInsuredResponse(Insured insured) {

        if (insured != null) {

            InsuredResponse insuredResponse = new InsuredResponse();
            insuredResponse.setAddress(insured.getAddress());
            insuredResponse.setDrivingLicense(DrivingLicenseAdapter.adptDrivingLicenseToDrivingLicenseResponse(insured.getDrivingLicense()));
            insuredResponse.setEmail(insured.getEmail());
            insuredResponse.setFirstname(insured.getFirstname());
            insuredResponse.setFiscalCode(insured.getFiscalCode());
            insuredResponse.setLastname(insured.getLastname());
            insuredResponse.setPhone(insured.getPhone());
            insuredResponse.setCustomerId(insured.getCustomerId());

            return insuredResponse;
        }
        return null;
    }
}
