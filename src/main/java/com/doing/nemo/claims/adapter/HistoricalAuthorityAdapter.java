package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.response.HistoricalAuthorityResponseV1;
import com.doing.nemo.claims.entity.jsonb.HistoricalAuthority;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

@Component
public class HistoricalAuthorityAdapter {

    public static HistoricalAuthorityResponseV1 adptHistoricalAuthorityToHistoricalAuthorityResponse(HistoricalAuthority historicalAuthority) {

        HistoricalAuthorityResponseV1 historical = new HistoricalAuthorityResponseV1();
        historical.setUpdateAt(DateUtil.convertUTCDateToIS08601String(historicalAuthority.getUpdateAt()));
        historical.setUserId(historicalAuthority.getUserId());
        historical.setEventType(historicalAuthority.getEventType());
        historical.setDescription(historicalAuthority.getDescription());
        historical.setUserName(historicalAuthority.getUserName());
        historical.setId(historicalAuthority.getId());
        historical.setOldTotal(historicalAuthority.getOldTotal());
        historical.setNewTotal(historicalAuthority.getNewTotal());


        return historical;
    }

    public static List<HistoricalAuthorityResponseV1> adptHistoricalAuthorityToHistoricalAuthorityResponse(List<HistoricalAuthority> historicalRequestList) {

        if (historicalRequestList != null) {

            List<HistoricalAuthorityResponseV1> historicalList = new LinkedList<>();
            for (HistoricalAuthority att : historicalRequestList) {
                historicalList.add(adptHistoricalAuthorityToHistoricalAuthorityResponse(att));
            }
            return historicalList;
        }
        return null;
    }

}
