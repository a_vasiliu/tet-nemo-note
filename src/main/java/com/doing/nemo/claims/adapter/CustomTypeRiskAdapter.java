package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.controller.payload.request.CustomTypeRiskRequestV1;
import com.doing.nemo.claims.entity.settings.ClaimsTypeEntity;
import com.doing.nemo.claims.entity.settings.CustomTypeRiskEntity;
import com.doing.nemo.claims.repository.ClaimsTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Component
public class CustomTypeRiskAdapter {

    @Autowired
    private ClaimsTypeRepository claimsTypeRepository;

    public CustomTypeRiskEntity adptFromCustomTypeRiskRequestToCustomTypeRiskEntity(CustomTypeRiskRequestV1 customTypeRiskRequestV1) {
        CustomTypeRiskEntity customTypeRiskEntity = new CustomTypeRiskEntity();
        if (customTypeRiskRequestV1.getClaimsType() != null) {
            Optional<ClaimsTypeEntity> claimsTypeEntityOptional = claimsTypeRepository.findById(customTypeRiskRequestV1.getClaimsType().getId());
            if (claimsTypeEntityOptional.isPresent())
                customTypeRiskEntity.setClaimsTypeEntity(claimsTypeEntityOptional.get());
        }
        customTypeRiskEntity.setFlow(customTypeRiskRequestV1.getFlow());
        customTypeRiskEntity.setTypeOfChargeback(customTypeRiskRequestV1.getTypeOfChargeback());

        return customTypeRiskEntity;
    }


    public List<CustomTypeRiskEntity> adptFromCustomTypeRiskRequestToCustomTypeRiskEntityList(List<CustomTypeRiskRequestV1> customTypeRiskRequestV1List) {
        List<CustomTypeRiskEntity> customTypeRiskEntityList = new LinkedList<>();
        if (customTypeRiskRequestV1List != null) {
            for (CustomTypeRiskRequestV1 customTypeRisksRequest : customTypeRiskRequestV1List) {
                customTypeRiskEntityList.add(this.adptFromCustomTypeRiskRequestToCustomTypeRiskEntity(customTypeRisksRequest));
            }
        }
        return customTypeRiskEntityList;
    }
}
