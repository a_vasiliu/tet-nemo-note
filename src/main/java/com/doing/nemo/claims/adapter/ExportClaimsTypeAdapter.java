package com.doing.nemo.claims.adapter;


import com.doing.nemo.claims.controller.payload.request.ExportClaimsTypeRequestV1;
import com.doing.nemo.claims.controller.payload.response.ClaimsResponsePaginationV1;
import com.doing.nemo.claims.controller.payload.response.ExportClaimsTypeResponseV1;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.settings.ExportClaimsTypeEntity;
import com.doing.nemo.claims.exception.NotFoundException;
import com.doing.nemo.claims.repository.ExportClaimsTypeRepository;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
public class ExportClaimsTypeAdapter {
    @Autowired
    private ExportClaimsTypeRepository exportClaimsTypeRepository;

    private static Logger LOGGER = LoggerFactory.getLogger(ExportClaimsTypeAdapter.class);

    public static ExportClaimsTypeEntity adptFromExportClaimsTypeRequestToExportClaimsTypeEntity(ExportClaimsTypeRequestV1 exportClaimsTypeRequestV1)
    {
        ExportClaimsTypeEntity exportClaimsTypeEntity = new ExportClaimsTypeEntity();
        if(exportClaimsTypeRequestV1 != null)
        {
            exportClaimsTypeEntity.setClaimsType(exportClaimsTypeRequestV1.getClaimsType());
            exportClaimsTypeEntity.setCode(exportClaimsTypeRequestV1.getCode());
            exportClaimsTypeEntity.setDescription(exportClaimsTypeRequestV1.getDescription());
            exportClaimsTypeEntity.setActive(exportClaimsTypeRequestV1.getActive());
        }
        return exportClaimsTypeEntity;
    }

    public ExportClaimsTypeEntity adptFromExportClaimsTypeRequestToExportClaimsTypeEntityWithID(ExportClaimsTypeRequestV1 exportClaimsTypeRequestV1, UUID uuid)
    {
        Optional<ExportClaimsTypeEntity> exportClaimsTypeEntity = exportClaimsTypeRepository.findById(uuid);
        if(!exportClaimsTypeEntity.isPresent()) {
            LOGGER.debug("Export claims type with id "+uuid+ " not found");
            throw new NotFoundException("Export claims type with id "+uuid+ " not found", com.doing.nemo.claims.validation.MessageCode.CLAIMS_1010);
        }
        ExportClaimsTypeEntity exportClaimsTypeEntity1 = exportClaimsTypeEntity.get();
        ExportClaimsTypeEntity exportClaimsTypeEntity2 = adptFromExportClaimsTypeRequestToExportClaimsTypeEntity(exportClaimsTypeRequestV1);
        exportClaimsTypeEntity2.setId(exportClaimsTypeEntity1.getId());
        return exportClaimsTypeEntity2;
    }

    public static ExportClaimsTypeResponseV1 adptFromExportClaimsTypeEntityToExportClaimsTypeResponse(ExportClaimsTypeEntity exportClaimsTypeEntity)
    {
        ExportClaimsTypeResponseV1 exportClaimsTypeResponseV1 = new ExportClaimsTypeResponseV1();
        if(exportClaimsTypeEntity != null)
        {
            exportClaimsTypeResponseV1.setId(exportClaimsTypeEntity.getId());
            exportClaimsTypeResponseV1.setClaimsType(exportClaimsTypeEntity.getClaimsType());
            exportClaimsTypeResponseV1.setDmSystem(exportClaimsTypeEntity.getDmSystem());
            exportClaimsTypeResponseV1.setCode(exportClaimsTypeEntity.getCode());
            exportClaimsTypeResponseV1.setDescription(exportClaimsTypeEntity.getDescription());
            exportClaimsTypeResponseV1.setActive(exportClaimsTypeEntity.getActive());
        }
        return exportClaimsTypeResponseV1;
    }

    public static List<ExportClaimsTypeResponseV1> adptFromExportClaimsTypeEntityToExportClaimsTypeResponseList(List<ExportClaimsTypeEntity> exportClaimsTypeEntityList)
    {
        List<ExportClaimsTypeResponseV1> responseV1 = new LinkedList<>();
        for(ExportClaimsTypeEntity exportClaimsTypeEntity : exportClaimsTypeEntityList)
        {
            responseV1.add(adptFromExportClaimsTypeEntityToExportClaimsTypeResponse(exportClaimsTypeEntity));
        }
        return responseV1;
    }

    public static ClaimsResponsePaginationV1 adptFromExportClaimsTypePaginationToClaimsResponsePagination(Pagination<ExportClaimsTypeEntity> pagination) {

        ClaimsResponsePaginationV1<ExportClaimsTypeResponseV1> claimsPaginationResponse = new ClaimsResponsePaginationV1<>();

        claimsPaginationResponse.setStats(pagination.getStats());
        claimsPaginationResponse.setItems(adptFromExportClaimsTypeEntityToExportClaimsTypeResponseList(pagination.getItems()));

        return claimsPaginationResponse;
    }
}
