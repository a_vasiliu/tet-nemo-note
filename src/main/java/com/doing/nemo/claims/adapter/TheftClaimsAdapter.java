package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.ClaimsInsertExternalRequest;
import com.doing.nemo.claims.controller.payload.request.ClaimsInsertEnjoyRequest.TheftEnjoyRequest;
import com.doing.nemo.claims.controller.payload.request.claims.TheftRequestV1;
import com.doing.nemo.claims.controller.payload.response.claims.TheftResponseV1;
import com.doing.nemo.claims.controller.payload.response.external.TheftExternalResponse;
import com.doing.nemo.claims.entity.claims.ClaimsTheftEntity;
import com.doing.nemo.claims.entity.jsonb.Theft;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

@Component
public class TheftClaimsAdapter {

    public static Theft adptTheftRequestV1ToTheft(TheftRequestV1 theftRequestV1) {

        Theft theft = new Theft();
        theft.setOperationsCenterNotified(theftRequestV1.getOperationsCenterNotified());
        theft.setTheftOccurredOnCenterAld(theftRequestV1.getTheftOccurredOnCenterAld());
        theft.setSupplierCode(theftRequestV1.getSupplierCode());
        theft.setComplaintAuthorityPolice(theftRequestV1.getComplaintAuthorityPolice());
        theft.setComplaintAuthorityCc(theftRequestV1.getComplaintAuthorityCc());
        theft.setComplaintAuthorityVvuu(theftRequestV1.getComplaintAuthorityVvuu());
        theft.setAuthorityData(theftRequestV1.getAuthorityData());
        theft.setAuthorityTelephone(theftRequestV1.getAuthorityTelephone());
        theft.setFindDate(DateUtil.convertIS08601StringToUTCDate(theftRequestV1.getFindDate()));
        theft.setHour(theftRequestV1.getHour());
        theft.setFoundAbroad(theftRequestV1.getFoundAbroad());
        theft.setVehicleCo(theftRequestV1.getVehicleCo());
        theft.setSequestered(theftRequestV1.getSequestered());
        theft.setAddress(theftRequestV1.getAddress());
        theft.setFindAuthorityPolice(theftRequestV1.getFindAuthorityPolice());
        theft.setFindAuthorityCc(theftRequestV1.getFindAuthorityCc());
        theft.setFindAuthorityVvuu(theftRequestV1.getFindAuthorityVvuu());
        theft.setFindAuthorityData(theftRequestV1.getFindAuthorityData());
        theft.setFindAuthorityTelephone(theftRequestV1.getFindAuthorityTelephone());
        theft.setTheftNotes(theftRequestV1.getTheftNotes());
        theft.setFindNotes(theftRequestV1.getFindNotes());
        theft.setFirstKeyReception(DateUtil.convertIS08601StringToUTCDate(theftRequestV1.getFirstKeyReception()));
        theft.setSecondKeyReception(DateUtil.convertIS08601StringToUTCDate(theftRequestV1.getSecondKeyReception()));
        theft.setOriginalComplaintReception(DateUtil.convertIS08601StringToUTCDate(theftRequestV1.getOriginalComplaintReception()));
        theft.setCopyComplaintReception(DateUtil.convertIS08601StringToUTCDate(theftRequestV1.getCopyComplaintReception()));
        theft.setOriginalReportReception(DateUtil.convertIS08601StringToUTCDate(theftRequestV1.getOriginalReportReception()));
        theft.setCopyReportReception(DateUtil.convertIS08601StringToUTCDate(theftRequestV1.getCopyReportReception()));
        theft.setLossPossessionRequest(DateUtil.convertIS08601StringToUTCDate(theftRequestV1.getLossPossessionRequest()));
        theft.setLossPossessionReception(DateUtil.convertIS08601StringToUTCDate(theftRequestV1.getLossPossessionReception()));
        theft.setReturnPossessionRequest(DateUtil.convertIS08601StringToUTCDate(theftRequestV1.getReturnPossessionRequest()));
        theft.setReturnPossessionReception(DateUtil.convertIS08601StringToUTCDate(theftRequestV1.getReturnPossessionReception()));
        theft.setRobbery(theftRequestV1.getRobbery());
        theft.setKeyManagement(theftRequestV1.getKeyManagement());
        theft.setAccountManagement(theftRequestV1.getAccountManagement());
        theft.setAdministrativePosition(theftRequestV1.getAdministrativePosition());
        theft.setPracticeId(theftRequestV1.getPracticeId());
        theft.setReRegistration(theftRequestV1.getReRegistration());
        theft.setReRegistrationRequest(DateUtil.convertIS08601StringToUTCDate(theftRequestV1.getReRegistrationRequest()));
        theft.setReRegistrationAt(DateUtil.convertIS08601StringToUTCDate(theftRequestV1.getReRegistrationAt()));
        theft.setReRegistrationSaleforcesCase(theftRequestV1.getReRegistrationSaleforcesCase());
        theft.setCertificateDuplication(theftRequestV1.getCertificateDuplication());
        theft.setCertificateDuplicationRequest(DateUtil.convertIS08601StringToUTCDate(theftRequestV1.getCertificateDuplicationRequest()));
        theft.setCertificateDuplicationAt(DateUtil.convertIS08601StringToUTCDate(theftRequestV1.getCertificateDuplicationAt()));
        theft.setCertificateDuplicationSalesforceCase(theftRequestV1.getCertificateDuplicationSalesforceCase());
        theft.setStamp(theftRequestV1.getStamp());
        theft.setStampRequest(DateUtil.convertIS08601StringToUTCDate(theftRequestV1.getStampRequest()));
        theft.setStampAt(DateUtil.convertIS08601StringToUTCDate(theftRequestV1.getStampAt()));
        theft.setStampSalesforceCase(theftRequestV1.getStampSalesforceCase());
        theft.setDemolition(theftRequestV1.getDemolition());
        theft.setDemolitionRequest(DateUtil.convertIS08601StringToUTCDate(theftRequestV1.getDemolitionRequest()));
        theft.setDemolitionAt(DateUtil.convertIS08601StringToUTCDate(theftRequestV1.getDemolitionAt()));
        theft.setDemolitionSalesforceCase(theftRequestV1.getDemolitionSalesforceCase());
        theft.setUnderSeizure(theftRequestV1.getUnderSeizure());
        theft.setWithReceptions(null);
        if(theftRequestV1.getFound() == null){
            theft.setFound(false);
        }else{
            theft.setFound(theftRequestV1.getFound());
        }
        return theft;

    }


    public static Theft adptTheftRequestV1ToTheftFinding(Theft theft, Theft newFindingTheft) {


        theft.setSequestered(newFindingTheft.getSequestered());
        theft.setFindDate(newFindingTheft.getFindDate());
        theft.setFindAuthorityPolice(newFindingTheft.getFindAuthorityPolice());
        theft.setFindAuthorityCc(newFindingTheft.getFindAuthorityCc());
        theft.setFindAuthorityVvuu(newFindingTheft.getFindAuthorityVvuu());
        theft.setFindAuthorityData(newFindingTheft.getFindAuthorityData());
        theft.setFindNotes(newFindingTheft.getFindNotes());
        theft.setFoundAbroad(newFindingTheft.getFoundAbroad());
        theft.setFindAuthorityTelephone(newFindingTheft.getFindAuthorityTelephone());
        theft.setUnderSeizure(newFindingTheft.getUnderSeizure());
        theft.setVehicleCo(newFindingTheft.getVehicleCo());
        theft.setWithReceptions(newFindingTheft.getWithReceptions());
        theft.setAddress(newFindingTheft.getAddress());
        return theft;

    }

    public static Theft adptTheftExternalRequestToTheft(ClaimsInsertExternalRequest.TheftExternalRequest theftRequestV1) {

        Theft theft = new Theft();
        theft.setOperationsCenterNotified(theftRequestV1.getOperationsCenterNotified());
        theft.setTheftOccurredOnCenterAld(theftRequestV1.getTheftOccurredOnCenterAld());
        theft.setComplaintAuthorityPolice(theftRequestV1.getComplaintAuthorityPolice());
        theft.setComplaintAuthorityCc(theftRequestV1.getComplaintAuthorityCc());
        theft.setComplaintAuthorityVvuu(theftRequestV1.getComplaintAuthorityVvuu());
        theft.setAuthorityData(theftRequestV1.getAuthorityData());
        theft.setAuthorityTelephone(theftRequestV1.getAuthorityTelephone());
        theft.setFindDate(DateUtil.convertIS08601StringToUTCDate(theftRequestV1.getFindDate()));
        theft.setHour(theftRequestV1.getHour());
        theft.setFoundAbroad(theftRequestV1.getFoundAbroad());
        theft.setVehicleCo(theftRequestV1.getVehicleCo());
        theft.setSequestered(theftRequestV1.getSequestered());
        theft.setAddress(theftRequestV1.getAddress());
        theft.setFindAuthorityPolice(theftRequestV1.getFindAuthorityPolice());
        theft.setFindAuthorityCc(theftRequestV1.getFindAuthorityCc());
        theft.setFindAuthorityVvuu(theftRequestV1.getFindAuthorityVvuu());
        theft.setFindAuthorityData(theftRequestV1.getFindAuthorityData());
        theft.setFindAuthorityTelephone(theftRequestV1.getFindAuthorityTelephone());
        theft.setTheftNotes(theftRequestV1.getTheftNotes());
        theft.setFindNotes(theftRequestV1.getFindNotes());
        if(theftRequestV1.getFound() == null){
            theft.setFound(false);
        }else{
            theft.setFound(theftRequestV1.getFound());
        }
        return theft;

    }

    public static TheftResponseV1 adptTheftToTheftResponseV1(Theft theft) {

        TheftResponseV1 theftResponseV1 = new TheftResponseV1();
        theftResponseV1.setOperationsCenterNotified(theft.getOperationsCenterNotified());
        theftResponseV1.setTheftOccurredOnCenterAld(theft.getTheftOccurredOnCenterAld());
        theftResponseV1.setSupplierCode(theft.getSupplierCode());
        theftResponseV1.setComplaintAuthorityPolice(theft.getComplaintAuthorityPolice());
        theftResponseV1.setComplaintAuthorityCc(theft.getComplaintAuthorityCc());
        theftResponseV1.setComplaintAuthorityVvuu(theft.getComplaintAuthorityVvuu());
        theftResponseV1.setAuthorityData(theft.getAuthorityData());
        theftResponseV1.setAuthorityTelephone(theft.getAuthorityTelephone());
        theftResponseV1.setFindDate(DateUtil.convertUTCDateToIS08601String(theft.getFindDate()));
        theftResponseV1.setHour(theft.getHour());
        theftResponseV1.setFoundAbroad(theft.getFoundAbroad());
        theftResponseV1.setVehicleCo(theft.getVehicleCo());
        theftResponseV1.setSequestered(theft.getSequestered());
        theftResponseV1.setUnderSeizure(theft.getUnderSeizure());
        theftResponseV1.setAddress(theft.getAddress());
        theftResponseV1.setFindAuthorityPolice(theft.getFindAuthorityPolice());
        theftResponseV1.setFindAuthorityCc(theft.getFindAuthorityCc());
        theftResponseV1.setFindAuthorityVvuu(theft.getFindAuthorityVvuu());
        theftResponseV1.setFindAuthorityData(theft.getFindAuthorityData());
        theftResponseV1.setFindAuthorityTelephone(theft.getFindAuthorityTelephone());
        theftResponseV1.setTheftNotes(theft.getTheftNotes());
        theftResponseV1.setFindNotes(theft.getFindNotes());
        theftResponseV1.setFirstKeyReception(DateUtil.convertUTCDateToIS08601String(theft.getFirstKeyReception()));
        theftResponseV1.setSecondKeyReception(DateUtil.convertUTCDateToIS08601String(theft.getSecondKeyReception()));
        theftResponseV1.setOriginalComplaintReception(DateUtil.convertUTCDateToIS08601String(theft.getOriginalComplaintReception()));
        theftResponseV1.setCopyComplaintReception(DateUtil.convertUTCDateToIS08601String(theft.getCopyComplaintReception()));
        theftResponseV1.setOriginalReportReception(DateUtil.convertUTCDateToIS08601String(theft.getOriginalReportReception()));
        theftResponseV1.setCopyReportReception(DateUtil.convertUTCDateToIS08601String(theft.getCopyReportReception()));
        theftResponseV1.setLossPossessionRequest(DateUtil.convertUTCDateToIS08601String(theft.getLossPossessionRequest()));
        theftResponseV1.setLossPossessionReception(DateUtil.convertUTCDateToIS08601String(theft.getLossPossessionReception()));
        theftResponseV1.setReturnPossessionRequest(DateUtil.convertUTCDateToIS08601String(theft.getReturnPossessionRequest()));
        theftResponseV1.setReturnPossessionReception(DateUtil.convertUTCDateToIS08601String(theft.getReturnPossessionReception()));
        theftResponseV1.setRobbery(theft.getRobbery());
        theftResponseV1.setKeyManagement(theft.getKeyManagement());
        theftResponseV1.setAccountManagement(theft.getAccountManagement());
        theftResponseV1.setAdministrativePosition(theft.getAdministrativePosition());
        theftResponseV1.setPracticeId(theft.getPracticeId());
        theftResponseV1.setReRegistration(theft.getReRegistration());
        theftResponseV1.setReRegistrationRequest(DateUtil.convertUTCDateToIS08601String(theft.getReRegistrationRequest()));
        theftResponseV1.setReRegistrationAt(DateUtil.convertUTCDateToIS08601String(theft.getReRegistrationAt()));
        theftResponseV1.setReRegistrationSaleforcesCase(theft.getReRegistrationSaleforcesCase());
        theftResponseV1.setCertificateDuplication(theft.getCertificateDuplication());
        theftResponseV1.setCertificateDuplicationRequest(DateUtil.convertUTCDateToIS08601String(theft.getCertificateDuplicationRequest()));
        theftResponseV1.setCertificateDuplicationAt(DateUtil.convertUTCDateToIS08601String(theft.getCertificateDuplicationAt()));
        theftResponseV1.setCertificateDuplicationSalesforceCase(theft.getCertificateDuplicationSalesforceCase());
        theftResponseV1.setStamp(theft.getStamp());
        theftResponseV1.setStampRequest(DateUtil.convertUTCDateToIS08601String(theft.getStampRequest()));
        theftResponseV1.setStampAt(DateUtil.convertUTCDateToIS08601String(theft.getStampAt()));
        theftResponseV1.setStampSalesforceCase(theft.getStampSalesforceCase());
        theftResponseV1.setDemolition(theft.getDemolition());
        theftResponseV1.setDemolitionRequest(DateUtil.convertUTCDateToIS08601String(theft.getDemolitionRequest()));
        theftResponseV1.setDemolitionAt(DateUtil.convertUTCDateToIS08601String(theft.getDemolitionAt()));
        theftResponseV1.setDemolitionSalesforceCase(theft.getDemolitionSalesforceCase());
        theftResponseV1.setWithReceptions(theft.getWithReceptions());
        theftResponseV1.setFound(theft.isFound());

        return theftResponseV1;

    }

    public static TheftExternalResponse adptTheftToTheftExternalResponse(Theft theft) {

        TheftExternalResponse theftResponseV1 = new TheftExternalResponse();
        theftResponseV1.setOperationsCenterNotified(theft.getOperationsCenterNotified());
        theftResponseV1.setTheftOccurredOnCenterAld(theft.getTheftOccurredOnCenterAld());
        theftResponseV1.setComplaintAuthorityPolice(theft.getComplaintAuthorityPolice());
        theftResponseV1.setComplaintAuthorityCc(theft.getComplaintAuthorityCc());
        theftResponseV1.setComplaintAuthorityVvuu(theft.getComplaintAuthorityVvuu());
        theftResponseV1.setAuthorityData(theft.getAuthorityData());
        theftResponseV1.setAuthorityTelephone(theft.getAuthorityTelephone());
        theftResponseV1.setFindDate(DateUtil.convertUTCDateToIS08601String(theft.getFindDate()));
        theftResponseV1.setHour(theft.getHour());
        theftResponseV1.setFoundAbroad(theft.getFoundAbroad());
        theftResponseV1.setVehicleCo(theft.getVehicleCo());
        theftResponseV1.setSequestered(theft.getSequestered());

        theftResponseV1.setAddress(theft.getAddress());
        theftResponseV1.setFindAuthorityPolice(theft.getFindAuthorityPolice());
        theftResponseV1.setFindAuthorityCc(theft.getFindAuthorityCc());
        theftResponseV1.setFindAuthorityVvuu(theft.getFindAuthorityVvuu());
        theftResponseV1.setFindAuthorityData(theft.getFindAuthorityData());
        theftResponseV1.setFindAuthorityTelephone(theft.getFindAuthorityTelephone());
        theftResponseV1.setTheftNotes(theft.getTheftNotes());
        theftResponseV1.setFindNotes(theft.getFindNotes());
        theftResponseV1.setFound(theft.isFound());

        return theftResponseV1;

    }

    public static List<TheftResponseV1> adptTheftToTheftResponseV1(List<Theft> appropriationPracticeCarPracticeCarList) {

        if (appropriationPracticeCarPracticeCarList != null) {

            List<TheftResponseV1> appropriationPracticeCarPracticeCarListResponse = new LinkedList<>();
            for (Theft att : appropriationPracticeCarPracticeCarList) {
                appropriationPracticeCarPracticeCarListResponse.add(adptTheftToTheftResponseV1(att));
            }
            return appropriationPracticeCarPracticeCarListResponse;
        }
        return null;
    }

    public static Theft adptTheftEnjoyRequestToTheft(TheftEnjoyRequest theftRequestV1) {

        Theft theft = new Theft();
        theft.setOperationsCenterNotified(theftRequestV1.getOperationsCenterNotified());
        theft.setTheftOccurredOnCenterAld(theftRequestV1.getTheftOccurredOnCenterAld());
        theft.setComplaintAuthorityPolice(theftRequestV1.getComplaintAuthorityPolice());
        theft.setComplaintAuthorityCc(theftRequestV1.getComplaintAuthorityCc());
        theft.setComplaintAuthorityVvuu(theftRequestV1.getComplaintAuthorityVvuu());
        theft.setAuthorityData(theftRequestV1.getAuthorityData());
        theft.setAuthorityTelephone(theftRequestV1.getAuthorityTelephone());
        theft.setFindDate(DateUtil.convertIS08601StringToUTCDate(theftRequestV1.getFindDate()));
        theft.setHour(theftRequestV1.getHour());
        theft.setFoundAbroad(theftRequestV1.getFoundAbroad());
        theft.setVehicleCo(theftRequestV1.getVehicleCo());
        theft.setSequestered(theftRequestV1.getSequestered());
        theft.setAddress(theftRequestV1.getAddress());
        theft.setFindAuthorityPolice(theftRequestV1.getFindAuthorityPolice());
        theft.setFindAuthorityCc(theftRequestV1.getFindAuthorityCc());
        theft.setFindAuthorityVvuu(theftRequestV1.getFindAuthorityVvuu());
        theft.setFindAuthorityData(theftRequestV1.getFindAuthorityData());
        theft.setFindAuthorityTelephone(theftRequestV1.getFindAuthorityTelephone());
        theft.setTheftNotes(theftRequestV1.getTheftNotes());
        theft.setFindNotes(theftRequestV1.getFindNotes());
        if(theftRequestV1.getFound() == null){
            theft.setFound(false);
        }else{
            theft.setFound(theftRequestV1.getFound());
        }
        return theft;

    }


    // NEW ADAPTER

    public static ClaimsTheftEntity adptTheftJsonbToClaimsTheftEntity(Theft theftJsonb) {

        ClaimsTheftEntity theft = new ClaimsTheftEntity();
        theft.setOperationsCenterNotified(theftJsonb.getOperationsCenterNotified());
        theft.setTheftOccurredOnCenterAld(theftJsonb.getTheftOccurredOnCenterAld());
        theft.setSupplierCode(theftJsonb.getSupplierCode());
        theft.setComplaintAuthorityPolice(theftJsonb.getComplaintAuthorityPolice());
        theft.setComplaintAuthorityCc(theftJsonb.getComplaintAuthorityCc());
        theft.setComplaintAuthorityVvuu(theftJsonb.getComplaintAuthorityVvuu());
        theft.setAuthorityData(theftJsonb.getAuthorityData());
        theft.setAuthorityTelephone(theftJsonb.getAuthorityTelephone());
        theft.setFindDate(DateUtil.convertIS08601StringToUTCInstant(DateUtil.convertUTCDateToIS08601String(theftJsonb.getFindDate())));
        theft.setHour(theftJsonb.getHour());
        theft.setFoundAbroad(theftJsonb.getFoundAbroad());
        theft.setVehicleCo(theftJsonb.getVehicleCo());
        theft.setSequestered(theftJsonb.getSequestered());
        theft.setAddress(theftJsonb.getAddress());
        theft.setFindAuthorityPolice(theftJsonb.getFindAuthorityPolice());
        theft.setFindAuthorityCc(theftJsonb.getFindAuthorityCc());
        theft.setFindAuthorityVvuu(theftJsonb.getFindAuthorityVvuu());
        theft.setFindAuthorityData(theftJsonb.getFindAuthorityData());
        theft.setFindAuthorityTelephone(theftJsonb.getFindAuthorityTelephone());
        theft.setTheftNotes(theftJsonb.getTheftNotes());
        theft.setFindNotes(theftJsonb.getFindNotes());
        theft.setFirstKeyReception(DateUtil.convertIS08601StringToUTCInstant(DateUtil.convertUTCDateToIS08601String(theftJsonb.getFirstKeyReception())));
        theft.setSecondKeyReception(DateUtil.convertIS08601StringToUTCInstant(DateUtil.convertUTCDateToIS08601String(theftJsonb.getSecondKeyReception())));
        theft.setOriginalComplaintReception(DateUtil.convertIS08601StringToUTCInstant(DateUtil.convertUTCDateToIS08601String(theftJsonb.getOriginalComplaintReception())));
        theft.setCopyComplaintReception(DateUtil.convertIS08601StringToUTCInstant(DateUtil.convertUTCDateToIS08601String(theftJsonb.getCopyComplaintReception())));
        theft.setOriginalReportReception(DateUtil.convertIS08601StringToUTCInstant(DateUtil.convertUTCDateToIS08601String(theftJsonb.getOriginalReportReception())));
        theft.setCopyReportReception(DateUtil.convertIS08601StringToUTCInstant(DateUtil.convertUTCDateToIS08601String(theftJsonb.getCopyReportReception())));
        theft.setLossPossessionRequest(DateUtil.convertIS08601StringToUTCInstant(DateUtil.convertUTCDateToIS08601String(theftJsonb.getLossPossessionRequest())));
        theft.setLossPossessionReception(DateUtil.convertIS08601StringToUTCInstant(DateUtil.convertUTCDateToIS08601String(theftJsonb.getLossPossessionReception())));
        theft.setReturnPossessionRequest(DateUtil.convertIS08601StringToUTCInstant(DateUtil.convertUTCDateToIS08601String(theftJsonb.getReturnPossessionRequest())));
        theft.setReturnPossessionReception(DateUtil.convertIS08601StringToUTCInstant(DateUtil.convertUTCDateToIS08601String(theftJsonb.getReturnPossessionReception())));
        theft.setRobbery(theftJsonb.getRobbery());
        theft.setKeyManagement(theftJsonb.getKeyManagement());
        theft.setAccountManagement(theftJsonb.getAccountManagement());
        theft.setAdministrativePosition(theftJsonb.getAdministrativePosition());
        theft.setPracticeId(theftJsonb.getPracticeId());
        theft.setReRegistration(theftJsonb.getReRegistration());
        theft.setReRegistrationRequest(DateUtil.convertIS08601StringToUTCInstant(DateUtil.convertUTCDateToIS08601String(theftJsonb.getReRegistrationRequest())));
        theft.setReRegistrationAt(DateUtil.convertIS08601StringToUTCInstant(DateUtil.convertUTCDateToIS08601String(theftJsonb.getReRegistrationAt())));
        theft.setReRegistrationSaleforcesCase(theftJsonb.getReRegistrationSaleforcesCase());
        theft.setCertificateDuplication(theftJsonb.getCertificateDuplication());
        theft.setCertificateDuplicationRequest(DateUtil.convertIS08601StringToUTCInstant(DateUtil.convertUTCDateToIS08601String(theftJsonb.getCertificateDuplicationRequest())));
        theft.setCertificateDuplicationAt(DateUtil.convertIS08601StringToUTCInstant(DateUtil.convertUTCDateToIS08601String(theftJsonb.getCertificateDuplicationAt())));
        theft.setCertificateDuplicationSalesforceCase(theftJsonb.getCertificateDuplicationSalesforceCase());
        theft.setStamp(theftJsonb.getStamp());
        theft.setStampRequest(DateUtil.convertIS08601StringToUTCInstant(DateUtil.convertUTCDateToIS08601String(theftJsonb.getStampRequest())));
        theft.setStampAt(DateUtil.convertIS08601StringToUTCInstant(DateUtil.convertUTCDateToIS08601String(theftJsonb.getStampAt())));
        theft.setStampSalesforceCase(theftJsonb.getStampSalesforceCase());
        theft.setDemolition(theftJsonb.getDemolition());
        theft.setDemolitionRequest(DateUtil.convertIS08601StringToUTCInstant(DateUtil.convertUTCDateToIS08601String(theftJsonb.getDemolitionRequest())));
        theft.setDemolitionAt(DateUtil.convertIS08601StringToUTCInstant(DateUtil.convertUTCDateToIS08601String(theftJsonb.getDemolitionAt())));
        theft.setDemolitionSalesforceCase(theftJsonb.getDemolitionSalesforceCase());
        theft.setUnderSeizure(theftJsonb.getUnderSeizure());
        theft.setWithReceptions(null);
        theft.setFound(theftJsonb.isFound());
        return theft;

    }

    public static Theft adptClaimsTheftToTheftJsonb(ClaimsTheftEntity claimsTheftEntity) {

        Theft theft = new Theft();

        theft.setOperationsCenterNotified(claimsTheftEntity.getOperationsCenterNotified());
        theft.setTheftOccurredOnCenterAld(claimsTheftEntity.getTheftOccurredOnCenterAld());
        theft.setSupplierCode(claimsTheftEntity.getSupplierCode());
        theft.setComplaintAuthorityPolice(claimsTheftEntity.getComplaintAuthorityPolice());
        theft.setComplaintAuthorityCc(claimsTheftEntity.getComplaintAuthorityCc());
        theft.setComplaintAuthorityVvuu(claimsTheftEntity.getComplaintAuthorityVvuu());
        theft.setAuthorityData(claimsTheftEntity.getAuthorityData());
        theft.setAuthorityTelephone(claimsTheftEntity.getAuthorityTelephone());
        theft.setFindDate(DateUtil.convertIS08601StringToUTCDate(DateUtil.convertUTCInstantToIS08601String(claimsTheftEntity.getFindDate())));
        theft.setHour(claimsTheftEntity.getHour());
        theft.setFoundAbroad(claimsTheftEntity.getFoundAbroad());
        theft.setVehicleCo(claimsTheftEntity.getVehicleCo());
        theft.setSequestered(claimsTheftEntity.getSequestered());
        theft.setAddress(claimsTheftEntity.getAddress());
        theft.setFindAuthorityPolice(claimsTheftEntity.getFindAuthorityPolice());
        theft.setFindAuthorityCc(claimsTheftEntity.getFindAuthorityCc());
        theft.setFindAuthorityVvuu(claimsTheftEntity.getFindAuthorityVvuu());
        theft.setFindAuthorityData(claimsTheftEntity.getFindAuthorityData());
        theft.setFindAuthorityTelephone(claimsTheftEntity.getFindAuthorityTelephone());
        theft.setTheftNotes(claimsTheftEntity.getTheftNotes());
        theft.setFindNotes(claimsTheftEntity.getFindNotes());
        theft.setFirstKeyReception(DateUtil.convertIS08601StringToUTCDate(DateUtil.convertUTCInstantToIS08601String(claimsTheftEntity.getFirstKeyReception())));
        theft.setSecondKeyReception(DateUtil.convertIS08601StringToUTCDate(DateUtil.convertUTCInstantToIS08601String(claimsTheftEntity.getSecondKeyReception())));
        theft.setOriginalComplaintReception(DateUtil.convertIS08601StringToUTCDate(DateUtil.convertUTCInstantToIS08601String(claimsTheftEntity.getOriginalComplaintReception())));
        theft.setCopyComplaintReception(DateUtil.convertIS08601StringToUTCDate(DateUtil.convertUTCInstantToIS08601String(claimsTheftEntity.getCopyComplaintReception())));
        theft.setOriginalReportReception(DateUtil.convertIS08601StringToUTCDate(DateUtil.convertUTCInstantToIS08601String(claimsTheftEntity.getOriginalReportReception())));
        theft.setCopyReportReception(DateUtil.convertIS08601StringToUTCDate(DateUtil.convertUTCInstantToIS08601String(claimsTheftEntity.getCopyReportReception())));
        theft.setLossPossessionRequest(DateUtil.convertIS08601StringToUTCDate(DateUtil.convertUTCInstantToIS08601String(claimsTheftEntity.getLossPossessionRequest())));
        theft.setLossPossessionReception(DateUtil.convertIS08601StringToUTCDate(DateUtil.convertUTCInstantToIS08601String(claimsTheftEntity.getLossPossessionReception())));
        theft.setReturnPossessionRequest(DateUtil.convertIS08601StringToUTCDate(DateUtil.convertUTCInstantToIS08601String(claimsTheftEntity.getReturnPossessionRequest())));
        theft.setReturnPossessionReception(DateUtil.convertIS08601StringToUTCDate(DateUtil.convertUTCInstantToIS08601String(claimsTheftEntity.getReturnPossessionReception())));
        theft.setRobbery(claimsTheftEntity.getRobbery());
        theft.setKeyManagement(claimsTheftEntity.getKeyManagement());
        theft.setAccountManagement(claimsTheftEntity.getAccountManagement());
        theft.setAdministrativePosition(claimsTheftEntity.getAdministrativePosition());
        theft.setPracticeId(claimsTheftEntity.getPracticeId());
        theft.setReRegistration(claimsTheftEntity.getReRegistration());
        theft.setReRegistrationRequest(DateUtil.convertIS08601StringToUTCDate(DateUtil.convertUTCInstantToIS08601String(claimsTheftEntity.getReRegistrationRequest())));
        theft.setReRegistrationAt(DateUtil.convertIS08601StringToUTCDate(DateUtil.convertUTCInstantToIS08601String(claimsTheftEntity.getReRegistrationAt())));
        theft.setReRegistrationSaleforcesCase(claimsTheftEntity.getReRegistrationSaleforcesCase());
        theft.setCertificateDuplication(claimsTheftEntity.getCertificateDuplication());
        theft.setCertificateDuplicationRequest(DateUtil.convertIS08601StringToUTCDate(DateUtil.convertUTCInstantToIS08601String(claimsTheftEntity.getCertificateDuplicationRequest())));
        theft.setCertificateDuplicationAt(DateUtil.convertIS08601StringToUTCDate(DateUtil.convertUTCInstantToIS08601String(claimsTheftEntity.getCertificateDuplicationAt())));
        theft.setCertificateDuplicationSalesforceCase(claimsTheftEntity.getCertificateDuplicationSalesforceCase());
        theft.setStamp(claimsTheftEntity.getStamp());
        theft.setStampRequest(DateUtil.convertIS08601StringToUTCDate(DateUtil.convertUTCInstantToIS08601String(claimsTheftEntity.getStampRequest())));
        theft.setStampAt(DateUtil.convertIS08601StringToUTCDate(DateUtil.convertUTCInstantToIS08601String(claimsTheftEntity.getStampAt())));
        theft.setStampSalesforceCase(claimsTheftEntity.getStampSalesforceCase());
        theft.setDemolition(claimsTheftEntity.getDemolition());
        theft.setDemolitionRequest(DateUtil.convertIS08601StringToUTCDate(DateUtil.convertUTCInstantToIS08601String(claimsTheftEntity.getDemolitionRequest())));
        theft.setDemolitionAt(DateUtil.convertIS08601StringToUTCDate(DateUtil.convertUTCInstantToIS08601String(claimsTheftEntity.getDemolitionAt())));
        theft.setDemolitionSalesforceCase(claimsTheftEntity.getDemolitionSalesforceCase());
        theft.setUnderSeizure(claimsTheftEntity.getUnderSeizure());
        theft.setWithReceptions(null);
        theft.setFound(claimsTheftEntity.isFound());
        return theft;

    }


}
