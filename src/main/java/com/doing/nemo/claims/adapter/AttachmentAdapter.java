package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.forms.AttachmentRequest;
import com.doing.nemo.claims.controller.payload.response.DownloadFileBase64ResponseV1;
import com.doing.nemo.claims.controller.payload.response.FileManagerResponseV1;
import com.doing.nemo.claims.controller.payload.response.UploadFileResponseV1;
import com.doing.nemo.claims.controller.payload.response.external.AttachmentExternalResponse;
import com.doing.nemo.claims.controller.payload.response.forms.AttachmentResponse;
import com.doing.nemo.claims.entity.jsonb.forms.jsonbForms.Attachment;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

@Component
public class AttachmentAdapter {

    public static Attachment adptAttachmentRequestToAttachment(AttachmentRequest attachmentRequest) {

        Attachment attachment = new Attachment();
        attachment.setBlobName(attachmentRequest.getBlobName());
        attachment.setBlobSize(attachmentRequest.getBlobSize());
        attachment.setCreatedAt(DateUtil.convertIS08601StringToUTCDate(attachmentRequest.getCreatedAt()));
        attachment.setDescription(attachmentRequest.getDescription());
        attachment.setFileManagerId(attachmentRequest.getFileManagerId());
        attachment.setFileManagerName(attachmentRequest.getFileManagerName());
        attachment.setMimeType(attachmentRequest.getMimeType());
        attachment.setHidden(attachmentRequest.getHidden());
        attachment.setValidate(attachmentRequest.getValidate());

        return attachment;
    }

    public static List<Attachment> adptAttachmentRequestToAttachment(List<AttachmentRequest> listAttachmentRequest) {

        if (listAttachmentRequest != null) {

            List<Attachment> attachmentList = new LinkedList<>();
            for (AttachmentRequest att : listAttachmentRequest) {
                attachmentList.add(adptAttachmentRequestToAttachment(att));
            }
            return attachmentList;
        }
        return null;
    }

    public static Attachment adptAuthorityAttachmentToAttachment(UploadFileResponseV1 uploadFileResponseV1) {

        Attachment attachment = new Attachment();
        attachment.setBlobName(uploadFileResponseV1.getBlobName());
        //attachment.setCreatedAt(DateUtil.convertIS08601StringToUTCDate(uploadFileResponseV1.get));
        attachment.setDescription(uploadFileResponseV1.getDescription());
        attachment.setFileManagerId(uploadFileResponseV1.getUuid());
        attachment.setFileManagerName("AUTH_"+uploadFileResponseV1.getFileName());
        attachment.setMimeType(uploadFileResponseV1.getMimeType());
        attachment.setHidden(false);
        attachment.setValidate(true);
        attachment.setMsaDownloaded(false);
        return attachment;
    }

    public static List<Attachment> adptAuthorityAttachmentToAttachment(List<UploadFileResponseV1> listAttachmentRequest) {

        if (listAttachmentRequest != null) {

            List<Attachment> attachmentList = new LinkedList<>();
            for (UploadFileResponseV1 att : listAttachmentRequest) {
                attachmentList.add(adptAuthorityAttachmentToAttachment(att));
            }
            return attachmentList;
        }
        return null;
    }

    public static AttachmentResponse adptAttachmentToAttachmentResponse(Attachment attachment) {

        AttachmentResponse attachmentResponse = new AttachmentResponse();
        attachmentResponse.setBlobName(attachment.getBlobName());
        attachmentResponse.setBlobSize(attachment.getBlobSize());
        attachmentResponse.setCreatedAt(DateUtil.convertUTCDateToIS08601String(attachment.getCreatedAt()));
        attachmentResponse.setDescription(attachment.getDescription());
        attachmentResponse.setFileManagerId(attachment.getFileManagerId());
        attachmentResponse.setFileManagerName(attachment.getFileManagerName());
        attachmentResponse.setMimeType(attachment.getMimeType());
        attachmentResponse.setHidden(attachment.getHidden());
        attachmentResponse.setValidate(attachment.getValidate());
        attachmentResponse.setUser(attachment.getUserId());
        attachmentResponse.setUserName(attachment.getUserName());
        attachmentResponse.setThumbnailId(attachment.getThumbnail());
        attachmentResponse.setExternalId(attachment.getExternalId());
        attachmentResponse.setAttachmentTypeResponse(AttachmentTypeAdapter.getAttachmentTypeAdapter(attachment.getAttachmentType()));
        attachmentResponse.setMsaDownloaded(attachment.getMsaDownloaded());
        return attachmentResponse;
    }

    public static AttachmentExternalResponse adptAttachmentToAttachmentExternalResponse(Attachment attachment) {

        AttachmentExternalResponse attachmentResponse = new AttachmentExternalResponse();
        attachmentResponse.setBlobName(attachment.getBlobName());
        attachmentResponse.setBlobSize(attachment.getBlobSize());
        attachmentResponse.setCreatedAt(attachment.getCreatedAt());
        attachmentResponse.setDescription(attachment.getDescription());
        attachmentResponse.setFileManagerId(attachment.getFileManagerId());
        attachmentResponse.setFileManagerName(attachment.getFileManagerName());
        attachmentResponse.setMimeType(attachment.getMimeType());
        attachmentResponse.setUser(attachment.getUserId());
        attachmentResponse.setThumbnailId(attachment.getThumbnail());
        attachmentResponse.setUserName(attachment.getUserName());
        attachmentResponse.setExternalId(attachment.getExternalId());
        return attachmentResponse;
    }

    public static List<AttachmentExternalResponse> adptAttachmentToAttachmentExternalResponse(List<Attachment> listAttachmentList) {

        if (listAttachmentList != null) {
            List<AttachmentExternalResponse> attachmentListResponse = new LinkedList<>();
            for (Attachment att : listAttachmentList) {
                attachmentListResponse.add(adptAttachmentToAttachmentExternalResponse(att));
            }
            return attachmentListResponse;

        }
        return null;
    }

    public static List<AttachmentResponse> adptAttachmentToAttachmentResponse(List<Attachment> listAttachmentList) {

        if (listAttachmentList != null) {
            List<AttachmentResponse> attachmentListResponse = new LinkedList<>();
            for (Attachment att : listAttachmentList) {
                attachmentListResponse.add(adptAttachmentToAttachmentResponse(att));
            }
            return attachmentListResponse;

        }
        return null;
    }

    public static List<AttachmentResponse> adptAttachmentToAttachmentResponseExternal(List<Attachment> listAttachmentList) {

        if (listAttachmentList != null) {
            List<AttachmentResponse> attachmentListResponse = new LinkedList<>();
            for (Attachment att : listAttachmentList) {
                if(att.getMsaDownloaded() == null || !att.getMsaDownloaded()){
                    attachmentListResponse.add(adptAttachmentToAttachmentResponse(att));
                }
            }
            return attachmentListResponse;

        }
        return null;
    }

    public static Attachment adptFromDownloadFileBase64ResponseV1toAttachment(DownloadFileBase64ResponseV1 file64Attachment, String idFileManager , String userId, String userName){
        Attachment attachment = new Attachment();
        attachment.setMimeType(file64Attachment.getMimeType());
        attachment.setBlobName(file64Attachment.getBlobType());
        attachment.setCreatedAt(new Date());
        attachment.setDescription("DOGE");
        attachment.setFileManagerId(idFileManager);
        attachment.setFileManagerName(file64Attachment.getFileName());
        attachment.setBlobSize(file64Attachment.getFileSize());
        attachment.setValidate(true);
        attachment.setUserId(userId);
        attachment.setUserName(userName);

        return attachment;
    }

    public static Attachment adptFromDownloadFileBase64ResponseV1toAttachmentPAIAndLegal(DownloadFileBase64ResponseV1 file64Attachment, String idFileManager , String userId, String userName, String description){
        Attachment attachment = new Attachment();
        attachment.setMimeType(file64Attachment.getMimeType());
        attachment.setBlobName(file64Attachment.getBlobType());
        attachment.setCreatedAt(new Date());
        attachment.setDescription(description);
        attachment.setFileManagerId(idFileManager);
        attachment.setFileManagerName(file64Attachment.getFileName());
        attachment.setBlobSize(file64Attachment.getFileSize());
        attachment.setValidate(true);
        attachment.setUserId(userId);
        attachment.setUserName(userName);

        return attachment;
    }



    public static Attachment adptFromFileManagerInvocingToAttachment(UploadFileResponseV1 fileManagerResponseV1){
        Attachment attachment = new Attachment();
        attachment.setMimeType(fileManagerResponseV1.getMimeType());
        attachment.setBlobName(fileManagerResponseV1.getBlobType());
        attachment.setCreatedAt(new Date());
        attachment.setDescription("Olsa Invoicing");

        String filemanagerid = null;
        if(fileManagerResponseV1.getUuid() != null){
            filemanagerid = fileManagerResponseV1.getUuid();
        }else if(fileManagerResponseV1.getId() != null){
            filemanagerid = fileManagerResponseV1.getId() ;
        }
        attachment.setFileManagerId(filemanagerid);
        attachment.setThumbnail(fileManagerResponseV1.getThumbnailId());
        attachment.setFileManagerName(fileManagerResponseV1.getFileName());
        attachment.setBlobSize(fileManagerResponseV1.getFileSize());
        attachment.setValidate(true);
        attachment.setUserName("NEMO");

        return attachment;
    }



}
