package com.doing.nemo.claims.adapter;


import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.ClaimsInsertEnjoyRequest.CounterpartyEnjoyRequest;
import com.doing.nemo.claims.controller.payload.request.ClaimsInsertExternalRequest.CounterpartyExternalRequest;
import com.doing.nemo.claims.controller.payload.request.CounterpartyMigrationRequestV1;
import com.doing.nemo.claims.controller.payload.request.CounterpartyRequest;
import com.doing.nemo.claims.controller.payload.response.CounterpartyResponse;
import com.doing.nemo.claims.controller.payload.response.CounterpartyResponsePaginationV1;
import com.doing.nemo.claims.controller.payload.response.CounterpartyResponseV1;
import com.doing.nemo.claims.controller.payload.response.external.CounterpartyExternalResponse;
import com.doing.nemo.claims.entity.*;
import com.doing.nemo.claims.entity.enumerated.DamagedEnum.VehicleEnum.VehicleTypeEnum;
import com.doing.nemo.claims.entity.enumerated.RepairEnum.RepairProcedureEnum;
import com.doing.nemo.claims.entity.enumerated.RepairEnum.RepairStatusEnum;
import com.doing.nemo.claims.repository.CounterpartyNewRepository;
import com.doing.nemo.claims.repository.CounterpartyRepository;
import com.doing.nemo.claims.service.ManagerService;
import com.doing.nemo.claims.util.Util;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;


@Component
public class CounterpartyAdapter {


    @Autowired
    private InsuranceCompanyCounterpartyAdapter insuranceCompanyAdapter;

    @Autowired
    private LastContactAdapter lastContactAdapter;

    @Autowired
    private ManagerService managerService;

    @Autowired
    private CounterpartyRepository counterpartyRepository;

    @Autowired
    private CounterpartyNewRepository counterpartyNewRepository;



    private static Logger LOGGER = LoggerFactory.getLogger(CounterpartyAdapter.class);


    public static CounterpartyResponseV1 adptFromCounterpartyToCounterpartyResponseV1(CounterpartyEntity counterparty){
        if(counterparty == null) return null;
        CounterpartyResponseV1 counterpartyResponse = new CounterpartyResponseV1();
        counterpartyResponse.setAttachmentList(AttachmentAdapter.adptAttachmentToAttachmentResponse(counterparty.getAttachments()));
        counterpartyResponse.setCaiSigned(counterparty.getCaiSigned());
        counterpartyResponse.setCounterpartyId(counterparty.getCounterpartyId());
        counterpartyResponse.setPracticeIdCounterparty(counterparty.getPracticeIdCounterparty());
        counterpartyResponse.setDescription(counterparty.getDescription());
        counterpartyResponse.setDriver(DriverAdapter.adptDriverToDriverResponse(counterparty.getDriver()));
        counterpartyResponse.setImpactPoint(ImpactPointAdapter.adptImpactPointToImpactPointResponse(counterparty.getImpactPoint()));
        counterpartyResponse.setInsuranceCompany(InsuranceCompanyCounterpartyAdapter.adptFromCompanyCounterpartyToCompanyCounterpartyResponse(counterparty.getInsuranceCompany()));
        counterpartyResponse.setInsured(InsuredAdapter.adptInsuredToInsuredResponse(counterparty.getInsured()));
        counterpartyResponse.setRepairStatus(counterparty.getRepairStatus());
        counterpartyResponse.setResponsible(counterparty.getResponsible());
        counterpartyResponse.setType(counterparty.getType());
        counterpartyResponse.setVehicle(VehicleAdapter.adptVehicleToVehicleResponse(counterparty.getVehicle()));
        counterpartyResponse.setRepairProcedure(counterparty.getRepairProcedure());
        counterpartyResponse.setUserCreate(counterparty.getUserCreate());
        counterpartyResponse.setCreatedAt(DateUtil.convertUTCInstantToIS08601String(counterparty.getCreatedAt()));
        counterpartyResponse.setUserUpdate(counterparty.getUserUpdate());
        counterpartyResponse.setUpdateAt(DateUtil.convertUTCInstantToIS08601String(counterparty.getUpdateAt()));

        counterpartyResponse.setRepairCreatedAt(DateUtil.convertUTCInstantToIS08601String(counterparty.getRepairCreatedAt()));

        counterpartyResponse.setAssignedAt(DateUtil.convertUTCInstantToIS08601String(counterparty.getAssignedAt()));
        counterpartyResponse.setAssignedTo(AssigneeRepairAdapter.adptFromAssignationRepairToAssignationRepairResponse(counterparty.getAssignedTo()));
        counterpartyResponse.setHistoricals(HistoricalCounterpartyAdapter.adptHistoricalCounterpartyToHistoricalCounterpartyResponse(counterparty.getHistoricals()));
        counterpartyResponse.setManager(ManagerAdapter.getManagerAdapter(counterparty.getManager()));
        counterpartyResponse.setLastContact(LastContactAdapter.adtpFromLastContactListEntityToLastContactListResponse(counterparty.getLastContact()));
        counterpartyResponse.setCanalization(CanalizationAdapter.adptFromCanalizationToCanalizationResponse(counterparty.getCanalization()));
        counterpartyResponse.setEligibility(counterparty.getEligibility());
        counterpartyResponse.setReplacementCar(counterparty.getReplacementCar());
        counterpartyResponse.setReplacementPlate(counterparty.getReplacementPlate());
        counterpartyResponse.setPolicyNumber(counterparty.getPolicyNumber());
        counterpartyResponse.setPolicyBeginningValidity(DateUtil.convertUTCInstantToIS08601String(counterparty.getPolicyBeginningValidity()));
        counterpartyResponse.setPolicyEndValidity(DateUtil.convertUTCInstantToIS08601String(counterparty.getPolicyEndValidity()));
        counterpartyResponse.setManagementType(counterparty.getManagementType());
        counterpartyResponse.setAskedForDamages(counterparty.getAskedForDamages());
        counterpartyResponse.setLegalOrConsultant(counterparty.getLegalOrConsultant());
        counterpartyResponse.setLegal(counterparty.getLegal());
        counterpartyResponse.setEmailLegal(counterparty.getEmailLegal());
        counterpartyResponse.setDateRequestDamages(DateUtil.convertUTCInstantToIS08601String(counterparty.getInstantRequestDamages()));
        counterpartyResponse.setExpirationDate(DateUtil.convertUTCInstantToIS08601String(counterparty.getExpirationInstant()));
        counterpartyResponse.setClaimsId(counterparty.getClaims().getId());
        counterpartyResponse.setPracticeId(counterparty.getClaims().getPracticeId());
        counterpartyResponse.setClaimsCreatedAt(DateUtil.convertUTCDateToIS08601String(counterparty.getClaims().getComplaint().getDataAccident().getDateAccident()));
        counterpartyResponse.setRegion(counterparty.getClaims().getComplaint().getDataAccident().getAddress().getRegion());
        counterpartyResponse.setProvince(counterparty.getClaims().getComplaint().getDataAccident().getAddress().getProvince());
        counterpartyResponse.setLocality(counterparty.getClaims().getComplaint().getDataAccident().getAddress().getLocality());
        counterpartyResponse.setAddress(counterparty.getClaims().getComplaint().getDataAccident().getAddress().getFormattedAddress());
        counterpartyResponse.setDateAccident(DateUtil.convertUTCDateToIS08601String(counterparty.getClaims().getComplaint().getDataAccident().getDateAccident()));
        counterpartyResponse.setLocator(counterparty.getClaims().getComplaint().getLocator());
        counterpartyResponse.setAld(counterparty.getIsAld());
        counterpartyResponse.setAuthorities(AuthorityAdapter.adptFromAuthorityToAuthorityRepairResponse(counterparty.getAuthorities()));
        counterpartyResponse.setRead(counterparty.getIsReadMsa());
        counterpartyResponse.setMotivation(counterparty.getMotivation());
        counterpartyResponse.setCompleteDocumentation(counterparty.getIsCompleteDocumentation());
        return counterpartyResponse;
    }

    public CounterpartyResponseV1 adptCounterpartsToCounterpartyResponseV1List(CounterpartyEntity counterparty) {

        CounterpartyResponseV1 counterpartyResponse = new CounterpartyResponseV1();
        ClaimsEntity claimsEntity = counterparty.getClaims();
        counterpartyResponse.setClaimsId(claimsEntity.getId());
        counterpartyResponse.setPracticeId(claimsEntity.getPracticeId());
        counterpartyResponse.setPracticeIdCounterparty(counterparty.getPracticeIdCounterparty());
        if (claimsEntity.getComplaint()!=null)
           counterpartyResponse.setLocator(claimsEntity.getComplaint().getLocator()!=null?claimsEntity.getComplaint().getLocator():"");
       
        counterpartyResponse.setClaimsCreatedAt(DateUtil.convertUTCInstantToIS08601String(claimsEntity.getCreatedAt()));
        counterpartyResponse.setRegion(claimsEntity.getComplaint().getDataAccident().getAddress().getRegion());
        counterpartyResponse.setProvince(claimsEntity.getComplaint().getDataAccident().getAddress().getProvince());
        counterpartyResponse.setLocality(claimsEntity.getComplaint().getDataAccident().getAddress().getLocality());
        counterpartyResponse.setAddress(claimsEntity.getComplaint().getDataAccident().getAddress().getFormattedAddress());
        if(claimsEntity.getComplaint().getFromCompany() != null)
            counterpartyResponse.setNumberSx(claimsEntity.getComplaint().getFromCompany().getNumberSx());
        counterpartyResponse.setDateAccident(DateUtil.convertUTCDateToIS08601String(claimsEntity.getComplaint().getDataAccident().getDateAccident()));
        counterpartyResponse.setAttachmentList(AttachmentAdapter.adptAttachmentToAttachmentResponse(counterparty.getAttachments()));
        counterpartyResponse.setCaiSigned(counterparty.getCaiSigned());
        counterpartyResponse.setCounterpartyId(counterparty.getCounterpartyId());
        counterpartyResponse.setDescription(counterparty.getDescription());
        counterpartyResponse.setDriver(DriverAdapter.adptDriverToDriverResponse(counterparty.getDriver()));
        counterpartyResponse.setImpactPoint(ImpactPointAdapter.adptImpactPointToImpactPointResponse(counterparty.getImpactPoint()));
        counterpartyResponse.setInsuranceCompany(insuranceCompanyAdapter.adptFromCompanyCounterpartyToCompanyCounterpartyResponse(counterparty.getInsuranceCompany()));
        counterpartyResponse.setInsured(InsuredAdapter.adptInsuredToInsuredResponse(counterparty.getInsured()));
        counterpartyResponse.setRepairStatus(counterparty.getRepairStatus());
        counterpartyResponse.setResponsible(counterparty.getResponsible());
        counterpartyResponse.setType(counterparty.getType());
        counterpartyResponse.setVehicle(VehicleAdapter.adptVehicleToVehicleResponse(counterparty.getVehicle()));
        counterpartyResponse.setRepairProcedure(counterparty.getRepairProcedure());
        counterpartyResponse.setUserCreate(counterparty.getUserCreate());
        counterpartyResponse.setCreatedAt(DateUtil.convertUTCInstantToIS08601String(counterparty.getCreatedAt()));
        counterpartyResponse.setUserUpdate(counterparty.getUserUpdate());
        counterpartyResponse.setUpdateAt(DateUtil.convertUTCInstantToIS08601String(counterparty.getUpdateAt()));
        counterpartyResponse.setAssignedAt(DateUtil.convertUTCInstantToIS08601String(counterparty.getAssignedAt()));
        counterpartyResponse.setRepairCreatedAt(DateUtil.convertUTCInstantToIS08601String(counterparty.getRepairCreatedAt()));
        counterpartyResponse.setAssignedTo(AssigneeRepairAdapter.adptFromAssignationRepairToAssignationRepairResponse(counterparty.getAssignedTo()));
        counterpartyResponse.setReplacementCar(counterparty.getReplacementCar());
        counterpartyResponse.setReplacementPlate(counterparty.getReplacementPlate());
        counterpartyResponse.setHistoricals(HistoricalCounterpartyAdapter.adptHistoricalCounterpartyToHistoricalCounterpartyResponse(counterparty.getHistoricals()));
        counterpartyResponse.setManager(ManagerAdapter.getManagerAdapter(counterparty.getManager()));
        counterpartyResponse.setLastContact(LastContactAdapter.adtpFromLastContactListEntityToLastContactListResponse(counterparty.getLastContact()));
        counterpartyResponse.setCanalization(CanalizationAdapter.adptFromCanalizationToCanalizationResponse(counterparty.getCanalization()));
        counterpartyResponse.setEligibility(counterparty.getEligibility());
        counterpartyResponse.setPolicyNumber(counterparty.getPolicyNumber());
        counterpartyResponse.setPolicyBeginningValidity(DateUtil.convertUTCInstantToIS08601String(counterparty.getPolicyBeginningValidity()));
        counterpartyResponse.setPolicyEndValidity(DateUtil.convertUTCInstantToIS08601String(counterparty.getPolicyEndValidity()));
        counterpartyResponse.setManagementType(counterparty.getManagementType());
        counterpartyResponse.setAskedForDamages(counterparty.getAskedForDamages());
        counterpartyResponse.setLegalOrConsultant(counterparty.getLegalOrConsultant());
        counterpartyResponse.setLegal(counterparty.getLegal());
        counterpartyResponse.setEmailLegal(counterparty.getEmailLegal());
        counterpartyResponse.setDateRequestDamages(DateUtil.convertUTCInstantToIS08601String(counterparty.getInstantRequestDamages()));
        counterpartyResponse.setExpirationDate(DateUtil.convertUTCInstantToIS08601String(counterparty.getExpirationInstant()));
        counterpartyResponse.setAld(counterparty.getIsAld());
        counterpartyResponse.setAuthorities(AuthorityAdapter.adptFromAuthorityToAuthorityRepairResponse(counterparty.getAuthorities()));
        counterpartyResponse.setRead(counterparty.getIsReadMsa());
        counterpartyResponse.setMotivation(counterparty.getMotivation());
        return counterpartyResponse;
    }

    public CounterpartyResponseV1 adptCounterpartsToCounterpartyResponseV1ListExternal(CounterpartyEntity counterparty) {


        CounterpartyResponseV1 counterpartyResponse = new CounterpartyResponseV1();
        ClaimsEntity claimsEntity = counterparty.getClaims();
        counterpartyResponse.setClaimsId(claimsEntity.getId());
        counterpartyResponse.setPracticeId(claimsEntity.getPracticeId());
        counterpartyResponse.setPracticeIdCounterparty(counterparty.getPracticeIdCounterparty());
        counterpartyResponse.setLocator(claimsEntity.getComplaint().getLocator());
        counterpartyResponse.setClaimsCreatedAt(DateUtil.convertUTCInstantToIS08601String(claimsEntity.getCreatedAt()));
        counterpartyResponse.setRegion(claimsEntity.getComplaint().getDataAccident().getAddress().getRegion());
        counterpartyResponse.setProvince(claimsEntity.getComplaint().getDataAccident().getAddress().getProvince());
        counterpartyResponse.setLocality(claimsEntity.getComplaint().getDataAccident().getAddress().getLocality());
        counterpartyResponse.setAddress(claimsEntity.getComplaint().getDataAccident().getAddress().getFormattedAddress());
        if(claimsEntity.getComplaint().getFromCompany() != null)
            counterpartyResponse.setNumberSx(claimsEntity.getComplaint().getFromCompany().getNumberSx());
        counterpartyResponse.setDateAccident(DateUtil.convertUTCDateToIS08601String(claimsEntity.getComplaint().getDataAccident().getDateAccident()));
        counterpartyResponse.setAttachmentList(AttachmentAdapter.adptAttachmentToAttachmentResponseExternal(counterparty.getAttachments()));
        counterpartyResponse.setCaiSigned(counterparty.getCaiSigned());
        counterpartyResponse.setCounterpartyId(counterparty.getCounterpartyId());
        counterpartyResponse.setDescription(counterparty.getDescription());
        counterpartyResponse.setDriver(DriverAdapter.adptDriverToDriverResponse(counterparty.getDriver()));
        counterpartyResponse.setImpactPoint(ImpactPointAdapter.adptImpactPointToImpactPointResponse(counterparty.getImpactPoint()));
        counterpartyResponse.setInsuranceCompany(insuranceCompanyAdapter.adptFromCompanyCounterpartyToCompanyCounterpartyResponse(counterparty.getInsuranceCompany()));
        counterpartyResponse.setInsured(InsuredAdapter.adptInsuredToInsuredResponse(counterparty.getInsured()));
        counterpartyResponse.setRepairStatus(counterparty.getRepairStatus());
        counterpartyResponse.setResponsible(counterparty.getResponsible());
        counterpartyResponse.setType(counterparty.getType());
        counterpartyResponse.setVehicle(VehicleAdapter.adptVehicleToVehicleResponse(counterparty.getVehicle()));
        counterpartyResponse.setRepairProcedure(counterparty.getRepairProcedure());
        counterpartyResponse.setUserCreate(counterparty.getUserCreate());
        counterpartyResponse.setCreatedAt(DateUtil.convertUTCInstantToIS08601String(counterparty.getCreatedAt()));
        counterpartyResponse.setUserUpdate(counterparty.getUserUpdate());
        counterpartyResponse.setUpdateAt(DateUtil.convertUTCInstantToIS08601String(counterparty.getUpdateAt()));
        counterpartyResponse.setAssignedAt(DateUtil.convertUTCInstantToIS08601String(counterparty.getAssignedAt()));
        counterpartyResponse.setRepairCreatedAt(DateUtil.convertUTCInstantToIS08601String(counterparty.getRepairCreatedAt()));
        counterpartyResponse.setAssignedTo(AssigneeRepairAdapter.adptFromAssignationRepairToAssignationRepairResponse(counterparty.getAssignedTo()));
        counterpartyResponse.setReplacementCar(counterparty.getReplacementCar());
        counterpartyResponse.setReplacementPlate(counterparty.getReplacementPlate());
        counterpartyResponse.setHistoricals(HistoricalCounterpartyAdapter.adptHistoricalCounterpartyToHistoricalCounterpartyResponse(counterparty.getHistoricals()));
        counterpartyResponse.setManager(ManagerAdapter.getManagerAdapter(counterparty.getManager()));
        counterpartyResponse.setLastContact(LastContactAdapter.adtpFromLastContactListEntityToLastContactListResponse(counterparty.getLastContact()));
        counterpartyResponse.setCanalization(CanalizationAdapter.adptFromCanalizationToCanalizationResponse(counterparty.getCanalization()));
        counterpartyResponse.setEligibility(counterparty.getEligibility());
        counterpartyResponse.setPolicyNumber(counterparty.getPolicyNumber());
        counterpartyResponse.setPolicyBeginningValidity(DateUtil.convertUTCInstantToIS08601String(counterparty.getPolicyBeginningValidity()));
        counterpartyResponse.setPolicyEndValidity(DateUtil.convertUTCInstantToIS08601String(counterparty.getPolicyEndValidity()));
        counterpartyResponse.setManagementType(counterparty.getManagementType());
        counterpartyResponse.setAskedForDamages(counterparty.getAskedForDamages());
        counterpartyResponse.setLegalOrConsultant(counterparty.getLegalOrConsultant());
        counterpartyResponse.setLegal(counterparty.getLegal());
        counterpartyResponse.setEmailLegal(counterparty.getEmailLegal());
        counterpartyResponse.setDateRequestDamages(DateUtil.convertUTCInstantToIS08601String(counterparty.getInstantRequestDamages()));
        counterpartyResponse.setExpirationDate(DateUtil.convertUTCInstantToIS08601String(counterparty.getExpirationInstant()));
        counterpartyResponse.setAld(counterparty.getIsAld());
        counterpartyResponse.setAuthorities(AuthorityAdapter.adptFromAuthorityToAuthorityRepairResponse(counterparty.getAuthorities()));
        counterpartyResponse.setRead(counterparty.getIsReadMsa());
        counterpartyResponse.setMotivation(counterparty.getMotivation());
        return counterpartyResponse;
    }




    public List<CounterpartyResponseV1> adptCounterpartsToCounterpartyResponseV1List(List<CounterpartyEntity> counterpartyObj) {
        if (counterpartyObj != null) {

            List<CounterpartyResponseV1> counterpartyList = new LinkedList<>();

            for (CounterpartyEntity att : counterpartyObj) {
                counterpartyList.add(this.adptCounterpartsToCounterpartyResponseV1List(att));
            }
            return counterpartyList;
        }
        return null;
    }

    public List<CounterpartyResponseV1> adptCounterpartsToCounterpartyResponseV1ListExternal(List<CounterpartyEntity> counterpartyObj) {
        if (counterpartyObj != null) {

            List<CounterpartyResponseV1> counterpartyList = new LinkedList<>();

            for (CounterpartyEntity att : counterpartyObj) {
                counterpartyList.add(this.adptCounterpartsToCounterpartyResponseV1ListExternal(att));
            }
            return counterpartyList;
        }
        return null;
    }

    public CounterpartyEntity adptCounterpartyRequestToCounterparty(CounterpartyRequest counterpartyRequest) {

        CounterpartyEntity counterparty = new CounterpartyEntity();
        counterparty.setCounterpartyId(UUID.randomUUID().toString());
        counterparty.setPracticeIdCounterparty(counterpartyRequest.getPracticeIdCounterparty());
        counterparty.setAttachments(AttachmentAdapter.adptAttachmentRequestToAttachment(counterpartyRequest.getAttachments()));
        counterparty.setCaiSigned(counterpartyRequest.getCaiSigned());
        counterparty.setDescription(counterpartyRequest.getDescription());
        counterparty.setDriver(DriverAdapter.adptDriverRequestToDriver(counterpartyRequest.getDriver()));
        counterparty.setImpactPoint(ImpactPointAdapter.adptImpactPointRequestToImpactPoint(counterpartyRequest.getImpactPoint()));
        counterparty.setInsuranceCompany(insuranceCompanyAdapter.adptFromCompanyCounterpartyRequestToCompanyCounterparty(counterpartyRequest.getInsuranceCompany()));
        counterparty.setInsured(InsuredAdapter.adptInsuredRequestToInsured(counterpartyRequest.getInsured()));
        counterparty.setEligibility(counterpartyRequest.getEligibility() != null ? counterpartyRequest.getEligibility() : false);
        counterparty.setReplacementCar(counterpartyRequest.getReplacementCar() != null ? counterpartyRequest.getReplacementCar() : false);
        counterparty.setReplacementPlate(counterpartyRequest.getReplacementPlate());
        if(counterpartyRequest.getRepairStatus() == null)
            counterpartyRequest.setRepairStatus(RepairStatusEnum.TO_SORT);

        counterparty.setRepairStatus(counterpartyRequest.getRepairStatus());
        counterparty.setResponsible(counterpartyRequest.getResponsible());
        counterparty.setType(counterpartyRequest.getType());
        counterparty.setVehicle(VehicleAdapter.adptVehicleRequestToVehicle(counterpartyRequest.getVehicle()));
        if(counterpartyRequest.getRepairProcedure() == null) {
            counterparty.setRepairProcedure(RepairProcedureEnum.ALD_REPAIRING);
        }else{
            counterparty.setRepairProcedure(counterpartyRequest.getRepairProcedure());
        }
        counterparty.setUserCreate(counterpartyRequest.getUserCreate());
        if(counterpartyRequest.getCreatedAt() != null)
            counterparty.setCreatedAt(DateUtil.convertIS08601StringToUTCInstant(counterpartyRequest.getCreatedAt()));
        else
            counterparty.setCreatedAt(DateUtil.getNowInstant());
        counterparty.setUserUpdate(counterpartyRequest.getUserUpdate());
        counterparty.setUpdateAt(DateUtil.getNowInstant());
        counterparty.setAssignedAt(DateUtil.convertIS08601StringToUTCInstant(counterpartyRequest.getAssignedAt()));
        counterparty.setRepairCreatedAt(DateUtil.convertIS08601StringToUTCInstant(counterpartyRequest.getRepairCreatedAt()));
        counterparty.setAssignedTo(AssigneeRepairAdapter.adptFromAssignationRepairRequestToAssignationRepair(counterpartyRequest.getAssignedTo()));
        counterparty.setHistoricals(HistoricalCounterpartyAdapter.adptHistoricalCounterpartyRequestToHistoricalCounterparty(counterpartyRequest.getHistoricals()));
        if(counterpartyRequest.getManager() != null)
            counterparty.setManager(managerService.getManager(counterpartyRequest.getManager().getId()));
        counterparty.setLastContact(lastContactAdapter.adtpFromLastContactListRequestToLastContactListEntity(counterpartyRequest.getLastContact()));
        counterparty.setCanalization(CanalizationAdapter.adptFromCanalizationRequestToCanalization(counterpartyRequest.getCanalization()));
        counterparty.setPolicyNumber(counterpartyRequest.getPolicyNumber());
        if(counterpartyRequest.getPolicyBeginningValidity() != null){
            String beginningValidity = counterpartyRequest.getPolicyBeginningValidity();
            if(!beginningValidity.contains("T"))
                beginningValidity+="T00:00:00Z";
            counterparty.setPolicyBeginningValidity(DateUtil.convertIS08601StringToUTCInstant(beginningValidity));
        } else counterparty.setPolicyBeginningValidity(null);
        if(counterpartyRequest.getPolicyEndValidity() != null){
            String endValidity = counterpartyRequest.getPolicyEndValidity();
            if(!endValidity.contains("T"))
                endValidity+="T00:00:00Z";
            counterparty.setPolicyEndValidity(DateUtil.convertIS08601StringToUTCInstant(endValidity));
        } else counterparty.setPolicyEndValidity(null);

        counterparty.setManagementType(counterpartyRequest.getManagementType());
        counterparty.setAskedForDamages(counterpartyRequest.getAskedForDamages());
        counterparty.setLegalOrConsultant(counterpartyRequest.getLegalOrConsultant());
        if(counterpartyRequest.getDateRequestDamages() != null){
            String dateRequestDamages = counterpartyRequest.getDateRequestDamages();
            if(!dateRequestDamages.contains("T"))
                dateRequestDamages+="T00:00:00Z";
            counterparty.setInstantRequestDamages(DateUtil.convertIS08601StringToUTCInstant(dateRequestDamages));
        }else counterparty.setInstantRequestDamages(null);
        if(counterpartyRequest.getExpirationDate() != null){
            String expirationDate = counterpartyRequest.getExpirationDate();
            if(!expirationDate.contains("T"))
                expirationDate+="T00:00:00Z";
            counterparty.setExpirationInstant(DateUtil.convertIS08601StringToUTCInstant(expirationDate));
        }else counterparty.setExpirationInstant(null);
        counterparty.setLegal(counterpartyRequest.getLegal());
        counterparty.setEmailLegal(counterpartyRequest.getEmailLegal());
        counterparty.setIsAld(counterpartyRequest.getAld());
        counterparty.setMotivation(counterpartyRequest.getMotivation());
        return counterparty;
    }

    public static CounterpartyEntity adptCounterpartyRequestMigrationToCounterparty(CounterpartyMigrationRequestV1 counterpartyRequest) {

        CounterpartyEntity counterparty = new CounterpartyEntity();
        counterparty.setCounterpartyId(counterpartyRequest.getCounterpartyId());
        counterparty.setPracticeIdCounterparty(counterpartyRequest.getPracticeIdCounterparty());
        counterparty.setAttachments(counterpartyRequest.getAttachments());
        counterparty.setCaiSigned(counterpartyRequest.getCaiSigned());
        counterparty.setDescription(counterpartyRequest.getDescription());
        counterparty.setDriver(counterpartyRequest.getDriver());
        counterparty.setImpactPoint(counterpartyRequest.getImpactPoint());
        counterparty.setInsuranceCompany(counterpartyRequest.getInsuranceCompany());
        counterparty.setInsured(counterpartyRequest.getInsured());
        counterparty.setEligibility(counterpartyRequest.getEligibility());

        if(counterpartyRequest.getRepairStatus() == null)
            counterpartyRequest.setRepairStatus(RepairStatusEnum.TO_SORT);

        counterparty.setRepairStatus(counterpartyRequest.getRepairStatus());
        counterparty.setResponsible(counterpartyRequest.getResponsible());
        counterparty.setType(counterpartyRequest.getType());
        counterparty.setVehicle(counterpartyRequest.getVehicle());
        if(counterpartyRequest.getRepairProcedure() == null) {
            counterparty.setRepairProcedure(RepairProcedureEnum.ALD_REPAIRING);
        }else{
            counterparty.setRepairProcedure(counterpartyRequest.getRepairProcedure());
        }
        counterparty.setUserCreate(counterpartyRequest.getUserCreate());
        if(counterpartyRequest.getCreatedAt() != null)
            counterparty.setCreatedAt(counterpartyRequest.getCreatedAt());
        else
            counterparty.setCreatedAt(DateUtil.getNowInstant());
        counterparty.setUserUpdate(counterpartyRequest.getUserUpdate());
        counterparty.setUpdateAt(DateUtil.getNowInstant());
        counterparty.setAssignedAt(counterpartyRequest.getAssignedAt());
        counterparty.setAssignedTo(counterpartyRequest.getAssignedTo());
        counterparty.setHistoricals(counterpartyRequest.getHistoricals());
        counterparty.setLastContact(counterpartyRequest.getLastContact());
        counterparty.setCanalization(counterpartyRequest.getCanalization());
        counterparty.setPolicyNumber(counterpartyRequest.getPolicyNumber());
        if(counterpartyRequest.getPolicyBeginningValidity() != null){
            counterparty.setPolicyBeginningValidity(counterpartyRequest.getPolicyBeginningValidity());
        } else counterparty.setPolicyBeginningValidity(null);
        if(counterpartyRequest.getPolicyEndValidity() != null){
            counterparty.setPolicyEndValidity(counterpartyRequest.getPolicyEndValidity());
        } else counterparty.setPolicyEndValidity(null);

        counterparty.setManagementType(counterpartyRequest.getManagementType());
        counterparty.setAskedForDamages(counterpartyRequest.getAskedForDamages());
        counterparty.setLegalOrConsultant(counterpartyRequest.getLegalOrConsultant());
        if(counterpartyRequest.getDateRequestDamages() != null){
            counterparty.setInstantRequestDamages(counterpartyRequest.getDateRequestDamages());
        }else counterparty.setInstantRequestDamages(null);
        if(counterpartyRequest.getExpirationInstant() != null){
            counterparty.setExpirationInstant(counterpartyRequest.getExpirationInstant());
        }else counterparty.setExpirationInstant(null);
        counterparty.setLegal(counterpartyRequest.getLegal());
        counterparty.setEmailLegal(counterpartyRequest.getEmailLegal());
        counterparty.setIsAld(counterpartyRequest.getAld());
        counterparty.setAuthorities(counterpartyRequest.getAuthorities());
        counterparty.setIsReadMsa(counterpartyRequest.getReadMsa());
        return counterparty;
    }

    public CounterpartyEntity adptCounterpartyRequestToCounterpartyUpdate(CounterpartyRequest counterpartyRequest) {

        if(counterpartyRequest == null){
            return null;
        }

        CounterpartyNewEntity counterpartyNewEntity = null;
        CounterpartyEntity counterparty = null;
        //bisogna chiamare la controparte dal repository

        if(counterpartyRequest.getCounterpartyId() != null){

            Optional<CounterpartyNewEntity> counterpartyEntityOptional = counterpartyNewRepository.findById(counterpartyRequest.getCounterpartyId());
            if(counterpartyEntityOptional.isPresent()){
                counterpartyNewEntity = counterpartyEntityOptional.get();
                counterparty = CounterpartyAdapter.adptCounterpartyNewToCounterpartyOld(counterpartyNewEntity);
            }else{
                counterparty = new CounterpartyEntity();
                counterparty.setCounterpartyId(UUID.randomUUID().toString());
                if(counterpartyRequest.getRepairStatus() == null)
                    counterpartyRequest.setRepairStatus(RepairStatusEnum.TO_SORT);
            }
        }else{
            counterparty = new CounterpartyEntity();
            counterparty.setCounterpartyId(UUID.randomUUID().toString());
            if(counterpartyRequest.getRepairStatus() == null)
                counterpartyRequest.setRepairStatus(RepairStatusEnum.TO_SORT);
        }

        counterparty.setCaiSigned(counterpartyRequest.getCaiSigned());
        counterparty.setDescription(counterpartyRequest.getDescription());
        counterparty.setDriver(DriverAdapter.adptDriverRequestToDriver(counterpartyRequest.getDriver()));
        counterparty.setImpactPoint(ImpactPointAdapter.adptImpactPointRequestToImpactPoint(counterpartyRequest.getImpactPoint()));
        counterparty.setInsuranceCompany(insuranceCompanyAdapter.adptFromCompanyCounterpartyRequestToCompanyCounterparty(counterpartyRequest.getInsuranceCompany()));
        counterparty.setInsured(InsuredAdapter.adptInsuredRequestToInsured(counterpartyRequest.getInsured()));
        counterparty.setResponsible(counterpartyRequest.getResponsible());
        counterparty.setType(counterpartyRequest.getType());
        counterparty.setVehicle(VehicleAdapter.adptVehicleRequestToVehicle(counterpartyRequest.getVehicle()));
        if(counterpartyRequest.getRepairProcedure() == null) {
            counterparty.setRepairProcedure(RepairProcedureEnum.ALD_REPAIRING);
        }else{
            counterparty.setRepairProcedure(counterpartyRequest.getRepairProcedure());
        }
        counterparty.setPolicyNumber(counterpartyRequest.getPolicyNumber());
        if(counterpartyRequest.getPolicyBeginningValidity() != null){
            String beginningValidity = counterpartyRequest.getPolicyBeginningValidity();
            if(!beginningValidity.contains("T")) {
                beginningValidity += "T00:00:00Z";
            }
            counterparty.setPolicyBeginningValidity(DateUtil.convertIS08601StringToUTCInstant(beginningValidity));
        } else{
            counterparty.setPolicyBeginningValidity(null);
        }

        if(counterpartyRequest.getPolicyEndValidity() != null){
            String endValidity = counterpartyRequest.getPolicyEndValidity();
            if(!endValidity.contains("T")) {
                endValidity += "T00:00:00Z";
            }
            counterparty.setPolicyEndValidity(DateUtil.convertIS08601StringToUTCInstant(endValidity));
        } else {
            counterparty.setPolicyEndValidity(null);
        }

        counterparty.setIsAld(counterpartyRequest.getAld());
        counterparty.setMotivation(counterpartyRequest.getMotivation());
        return counterparty;
    }

    public CounterpartyEntity adptCounterpartyExternalRequestToCounterparty(CounterpartyExternalRequest counterpartyRequest) {

        CounterpartyEntity counterparty = new CounterpartyEntity();
        counterparty.setCaiSigned(counterpartyRequest.getCaiSigned());
        counterparty.setDescription(counterpartyRequest.getDescription());
        counterparty.setDriver(DriverAdapter.adptDriverRequestToDriver(counterpartyRequest.getDriver()));
        counterparty.setImpactPoint(ImpactPointAdapter.adptImpactPointRequestToImpactPoint(counterpartyRequest.getImpactPoint()));
        counterparty.setInsuranceCompany(insuranceCompanyAdapter.adptFromCompanyCounterpartyRequestToCompanyCounterparty(counterpartyRequest.getInsuranceCompany()));
        counterparty.setInsured(InsuredAdapter.adptInsuredRequestToInsured(counterpartyRequest.getInsured()));
        counterparty.setResponsible(counterpartyRequest.getResponsible());
        counterparty.setType(counterpartyRequest.getType());
        counterparty.setVehicle(VehicleAdapter.adptVehicleRequestToVehicle(counterpartyRequest.getVehicle()));
        counterparty.setCreatedAt(DateUtil.getNowInstant());
        counterparty.setPolicyNumber(counterpartyRequest.getPolicyNumber());
        counterparty.setPolicyBeginningValidity(DateUtil.convertIS08601StringToUTCInstant(DateUtil.convertUTCDateToIS08601String(counterpartyRequest.getPolicyBeginningValidity())));
        counterparty.setPolicyEndValidity(DateUtil.convertIS08601StringToUTCInstant(DateUtil.convertUTCDateToIS08601String(counterpartyRequest.getPolicyEndValidity())));

        return counterparty;
    }

    public List<CounterpartyEntity> adptCounterpartyExternalRequestToCounterparty(List<CounterpartyExternalRequest> counterpartyRequestList) {

        if (counterpartyRequestList != null) {

            List<CounterpartyEntity> counterpartyList = new LinkedList<>();

            for (CounterpartyExternalRequest att : counterpartyRequestList) {
                counterpartyList.add(this.adptCounterpartyExternalRequestToCounterparty(att));
            }
            return counterpartyList;
        }

        return null;
    }

    public List<CounterpartyEntity> adptCounterpartyRequestToCounterparty(List<CounterpartyRequest> counterpartyRequestList) {

        if (counterpartyRequestList != null) {

            List<CounterpartyEntity> counterpartyList = new LinkedList<>();

            for (CounterpartyRequest att : counterpartyRequestList) {
                counterpartyList.add(this.adptCounterpartyRequestToCounterparty(att));
            }
            return counterpartyList;
        }

        return null;
    }

    public static List<CounterpartyEntity> adptCounterpartyRequestMigrationToCounterparty(List<CounterpartyMigrationRequestV1> counterpartyRequestList) {

        if (counterpartyRequestList != null) {

            List<CounterpartyEntity> counterpartyList = new LinkedList<>();

            for (CounterpartyMigrationRequestV1 att : counterpartyRequestList) {
                counterpartyList.add(adptCounterpartyRequestMigrationToCounterparty(att));
            }
            return counterpartyList;
        }

        return null;
    }

    public static CounterpartyResponse adptCounterpartyToCounterpartyResponse(CounterpartyEntity counterparty) {

        CounterpartyResponse counterpartyResponse = new CounterpartyResponse();
        counterpartyResponse.setAttachments(AttachmentAdapter.adptAttachmentToAttachmentResponse(counterparty.getAttachments()));
        counterpartyResponse.setCaiSigned(counterparty.getCaiSigned());
        counterpartyResponse.setCounterpartyId(counterparty.getCounterpartyId());
        counterpartyResponse.setPracticeIdCounterparty(counterparty.getPracticeIdCounterparty());
        counterpartyResponse.setDescription(counterparty.getDescription());
        counterpartyResponse.setDriver(DriverAdapter.adptDriverToDriverResponse(counterparty.getDriver()));
        counterpartyResponse.setImpactPoint(ImpactPointAdapter.adptImpactPointToImpactPointResponse(counterparty.getImpactPoint()));
        counterpartyResponse.setInsuranceCompany(InsuranceCompanyCounterpartyAdapter.adptFromCompanyCounterpartyToCompanyCounterpartyResponse(counterparty.getInsuranceCompany()));
        counterpartyResponse.setInsured(InsuredAdapter.adptInsuredToInsuredResponse(counterparty.getInsured()));
        counterpartyResponse.setRepairStatus(counterparty.getRepairStatus());
        counterpartyResponse.setResponsible(counterparty.getResponsible());
        counterpartyResponse.setReplacementCar(counterparty.getReplacementCar());
        counterpartyResponse.setReplacementPlate(counterparty.getReplacementPlate());
        counterpartyResponse.setType(counterparty.getType());
        counterpartyResponse.setIsCompleteDocumentation(counterparty.getIsCompleteDocumentation());
        counterpartyResponse.setVehicle(VehicleAdapter.adptVehicleToVehicleResponse(counterparty.getVehicle()));
        counterpartyResponse.setRepairProcedure(counterparty.getRepairProcedure());
        counterpartyResponse.setUserCreate(counterparty.getUserCreate());
        counterpartyResponse.setCreatedAt(DateUtil.convertUTCInstantToIS08601String(counterparty.getCreatedAt()));
        counterpartyResponse.setUserUpdate(counterparty.getUserUpdate());
        counterpartyResponse.setUpdateAt(DateUtil.convertUTCInstantToIS08601String(counterparty.getUpdateAt()));
        counterpartyResponse.setAssignedAt(DateUtil.convertUTCInstantToIS08601String(counterparty.getAssignedAt()));
        counterpartyResponse.setRepairCreatedAt(DateUtil.convertUTCInstantToIS08601String(counterparty.getRepairCreatedAt()));
        counterpartyResponse.setAssignedTo(AssigneeRepairAdapter.adptFromAssignationRepairToAssignationRepairResponse(counterparty.getAssignedTo()));
        counterpartyResponse.setHistoricals(HistoricalCounterpartyAdapter.adptHistoricalCounterpartyToHistoricalCounterpartyResponse(counterparty.getHistoricals()));
        counterpartyResponse.setManager(ManagerAdapter.getManagerAdapter(counterparty.getManager()));
        counterpartyResponse.setLastContact(LastContactAdapter.adtpFromLastContactListEntityToLastContactListResponse(counterparty.getLastContact()));
        counterpartyResponse.setCanalization(CanalizationAdapter.adptFromCanalizationToCanalizationResponse(counterparty.getCanalization()));
        counterpartyResponse.setEligibility(counterparty.getEligibility());
        counterpartyResponse.setPolicyNumber(counterparty.getPolicyNumber());
        counterpartyResponse.setPolicyBeginningValidity(DateUtil.convertUTCInstantToIS08601String(counterparty.getPolicyBeginningValidity()));
        counterpartyResponse.setPolicyEndValidity(DateUtil.convertUTCInstantToIS08601String(counterparty.getPolicyEndValidity()));
        counterpartyResponse.setManagementType(counterparty.getManagementType());
        counterpartyResponse.setAskedForDamages(counterparty.getAskedForDamages());
        counterpartyResponse.setLegalOrConsultant(counterparty.getLegalOrConsultant());
        counterpartyResponse.setLegal(counterparty.getLegal());
        counterpartyResponse.setEmailLegal(counterparty.getEmailLegal());
        counterpartyResponse.setDateRequestDamages(DateUtil.convertUTCInstantToIS08601String(counterparty.getInstantRequestDamages()));
        counterpartyResponse.setExpirationDate(DateUtil.convertUTCInstantToIS08601String(counterparty.getExpirationInstant()));
        counterpartyResponse.setAld(counterparty.getAld());
        counterpartyResponse.setAuthorities(AuthorityAdapter.adptFromAuthorityToAuthorityRepairResponse(counterparty.getAuthorities()));
        counterpartyResponse.setRead(counterparty.getIsReadMsa());
        counterpartyResponse.setMotivation(counterparty.getMotivation());
        return counterpartyResponse;
    }

    public static List<CounterpartyResponse> adptCounterpartyToCounterpartyResponse(List<CounterpartyEntity> counterpartyList) {

        if (counterpartyList != null) {
            List<CounterpartyResponse> counterpartyListResponse = new LinkedList<>();
            for (CounterpartyEntity att : counterpartyList) {
                counterpartyListResponse.add(adptCounterpartyToCounterpartyResponse(att));
            }
            return counterpartyListResponse;

        }
        return null;
    }

    public static CounterpartyExternalResponse adptCounterpartyToCounterpartyExternalResponse(CounterpartyEntity counterparty) {

        CounterpartyExternalResponse counterpartyResponse = new  CounterpartyExternalResponse();
        counterpartyResponse.setId(counterparty.getCounterpartyId());
        counterpartyResponse.setPracticeIdCounterparty(counterparty.getPracticeIdCounterparty());
        counterpartyResponse.setAttachments(AttachmentAdapter.adptAttachmentToAttachmentResponse(counterparty.getAttachments()));
        counterpartyResponse.setCaiSigned(counterparty.getCaiSigned());
        counterpartyResponse.setDescription(counterparty.getDescription());
        counterpartyResponse.setDriver(DriverAdapter.adptDriverToDriverResponse(counterparty.getDriver()));
        counterpartyResponse.setImpactPoint(ImpactPointAdapter.adptImpactPointToImpactPointResponse(counterparty.getImpactPoint()));
        counterpartyResponse.setInsuranceCompany(InsuranceCompanyCounterpartyAdapter.adptFromCompanyCounterpartyToCompanyCounterpartyResponse(counterparty.getInsuranceCompany()));
        counterpartyResponse.setInsured(InsuredAdapter.adptInsuredToInsuredResponse(counterparty.getInsured()));
        counterpartyResponse.setResponsible(counterparty.getResponsible());
        counterpartyResponse.setType(counterparty.getType());
        counterpartyResponse.setVehicle(VehicleAdapter.adptVehicleToVehicleResponse(counterparty.getVehicle()));
        counterpartyResponse.setPolicyNumber(counterparty.getPolicyNumber());
        counterpartyResponse.setPolicyBeginningValidity(DateUtil.convertIS08601StringToUTCDate(DateUtil.convertUTCInstantToIS08601String(counterparty.getPolicyBeginningValidity())));
        counterpartyResponse.setPolicyEndValidity(DateUtil.convertIS08601StringToUTCDate(DateUtil.convertUTCInstantToIS08601String(counterparty.getPolicyEndValidity())));

        return counterpartyResponse;
    }

    public static List<CounterpartyExternalResponse> adptCounterpartyToCounterpartyExternalResponse(List<CounterpartyEntity> counterpartyList) {

        if (counterpartyList != null) {
            List<CounterpartyExternalResponse> counterpartyListResponse = new LinkedList<>();
            for (CounterpartyEntity att : counterpartyList) {
                counterpartyListResponse.add(adptCounterpartyToCounterpartyExternalResponse(att));
            }
            return counterpartyListResponse;

        }
        return null;
    }

    public static CounterpartyResponsePaginationV1 adptCounterpartyPaginationToCounterpartyPaginationResponse(Pagination pagination) {

        CounterpartyResponsePaginationV1 counterpartyPaginationResponse = new CounterpartyResponsePaginationV1();
        counterpartyPaginationResponse.setStats(pagination.getStats());
        counterpartyPaginationResponse.setItems(pagination.getItems());

        return counterpartyPaginationResponse;
    }



    public CounterpartyEntity adptCounterpartyEnjoyRequestToCounterparty(CounterpartyEnjoyRequest counterpartyRequest) {

        CounterpartyEntity counterparty = new CounterpartyEntity();
        counterparty.setCaiSigned(counterpartyRequest.getCaiSigned());
        counterparty.setDescription(counterpartyRequest.getDescription());
        counterparty.setDriver(DriverAdapter.adptDriverRequestToDriver(counterpartyRequest.getDriver()));
        counterparty.setImpactPoint(ImpactPointAdapter.adptImpactPointRequestToImpactPoint(counterpartyRequest.getImpactPoint()));
        counterparty.setInsuranceCompany(insuranceCompanyAdapter.adptFromCompanyCounterpartyRequestToCompanyCounterparty(counterpartyRequest.getInsuranceCompany()));
        counterparty.setInsured(InsuredAdapter.adptInsuredRequestToInsured(counterpartyRequest.getInsured()));
        counterparty.setResponsible(counterpartyRequest.getResponsible());
        counterparty.setType(counterpartyRequest.getType());
        counterparty.setVehicle(VehicleAdapter.adptVehicleRequestToVehicle(counterpartyRequest.getVehicle()));
        if(counterparty.getVehicle() != null && Util.isNotEmpty(counterparty.getVehicle().getLicensePlate())){
            counterparty.setType(VehicleTypeEnum.VEHICLE);
        } else {
            counterparty.setType(VehicleTypeEnum.OTHER);
        }
        counterparty.setCreatedAt(DateUtil.getNowInstant());
        counterparty.setPolicyNumber(counterpartyRequest.getPolicyNumber());
        counterparty.setPolicyBeginningValidity(DateUtil.convertIS08601StringToUTCInstant(DateUtil.convertUTCDateToIS08601String(counterpartyRequest.getPolicyBeginningValidity())));
        counterparty.setPolicyEndValidity(DateUtil.convertIS08601StringToUTCInstant(DateUtil.convertUTCDateToIS08601String(counterpartyRequest.getPolicyEndValidity())));

        return counterparty;
    }

    public List<CounterpartyEntity> adptCounterpartyEnjoyRequestToCounterparty(List<CounterpartyEnjoyRequest> counterpartyRequestList) {

        if (counterpartyRequestList != null) {

            List<CounterpartyEntity> counterpartyList = new LinkedList<>();

            for (CounterpartyEnjoyRequest att : counterpartyRequestList) {
                counterpartyList.add(this.adptCounterpartyEnjoyRequestToCounterparty(att));
            }
            return counterpartyList;
        }

        return null;
    }


    //NUOVI ADAPTER
    public static CounterpartyNewEntity adptCounterpartyOldToCounterpartyNew(CounterpartyEntity counterpartyOldEntity) {

        CounterpartyNewEntity counterparty = new CounterpartyNewEntity();
        counterparty.setCounterpartyId(counterpartyOldEntity.getCounterpartyId());
        counterparty.setPracticeIdCounterparty(counterpartyOldEntity.getPracticeIdCounterparty());
        counterparty.setAttachments(counterpartyOldEntity.getAttachments());
        counterparty.setCaiSigned(counterpartyOldEntity.getCaiSigned());
        counterparty.setDescription(counterpartyOldEntity.getDescription());
        counterparty.setDriver(counterpartyOldEntity.getDriver());
        counterparty.setImpactPoint(counterpartyOldEntity.getImpactPoint());
        counterparty.setInsuranceCompany(counterpartyOldEntity.getInsuranceCompany());
        counterparty.setInsured(counterpartyOldEntity.getInsured());
        counterparty.setEligibility(counterpartyOldEntity.getEligibility());
        counterparty.setReplacementCar(counterpartyOldEntity.getReplacementCar());
        counterparty.setReplacementPlate(counterpartyOldEntity.getReplacementPlate());
        counterparty.setRepairStatus(counterpartyOldEntity.getRepairStatus());
        counterparty.setResponsible(counterpartyOldEntity.getResponsible());
        counterparty.setType(counterpartyOldEntity.getType());
        counterparty.setVehicle(counterpartyOldEntity.getVehicle());
        counterparty.setRepairProcedure(counterpartyOldEntity.getRepairProcedure());
        counterparty.setUserCreate(counterpartyOldEntity.getUserCreate());
        counterparty.setCreatedAt(counterpartyOldEntity.getCreatedAt());
        counterparty.setUserUpdate(counterpartyOldEntity.getUserUpdate());
        counterparty.setUpdateAt(counterpartyOldEntity.getUpdateAt());
        counterparty.setAssignedAt(counterpartyOldEntity.getAssignedAt());
        counterparty.setRepairCreatedAt(counterpartyOldEntity.getRepairCreatedAt());
        counterparty.setAssignedTo(counterpartyOldEntity.getAssignedTo());
        counterparty.setHistoricals(counterpartyOldEntity.getHistoricals());
        counterparty.setManager(counterpartyOldEntity.getManager());
        counterparty.setLastContact(counterpartyOldEntity.getLastContact());
        counterparty.setCanalization(counterpartyOldEntity.getCanalization());
        counterparty.setPolicyNumber(counterpartyOldEntity.getPolicyNumber());
        counterparty.setPolicyBeginningValidity(counterpartyOldEntity.getPolicyBeginningValidity());
        counterparty.setPolicyEndValidity(counterpartyOldEntity.getPolicyEndValidity());
        counterparty.setManagementType(counterpartyOldEntity.getManagementType());
        counterparty.setAskedForDamages(counterpartyOldEntity.getAskedForDamages());
        counterparty.setLegalOrConsultant(counterpartyOldEntity.getLegalOrConsultant());
        counterparty.setDateRequestDamages(counterpartyOldEntity.getDateRequestDamages());
        counterparty.setExpirationInstant(counterpartyOldEntity.getExpirationInstant());
        counterparty.setLegal(counterpartyOldEntity.getLegal());
        counterparty.setEmailLegal(counterpartyOldEntity.getEmailLegal());
        counterparty.setIsAld(counterpartyOldEntity.getAld());
        counterparty.setMotivation(counterpartyOldEntity.getMotivation());
        counterparty.setAuthorities(counterpartyOldEntity.getAuthorities());
        if(counterpartyOldEntity.getClaims() != null) {
            ClaimsNewEntity claimsNewEntity = new ClaimsNewEntity();
            claimsNewEntity.setId(counterpartyOldEntity.getClaims().getId());
            counterparty.setClaims(claimsNewEntity);
        }

        return counterparty;
    }

    public static CounterpartyEntity adptCounterpartyNewToCounterpartyOld(CounterpartyNewEntity counterpartyNewEntity) {

        CounterpartyEntity counterparty = new CounterpartyEntity();
        counterparty.setCounterpartyId(counterpartyNewEntity.getCounterpartyId());
        counterparty.setPracticeIdCounterparty(counterpartyNewEntity.getPracticeIdCounterparty());
        counterparty.setAttachments(counterpartyNewEntity.getAttachments());
        counterparty.setCaiSigned(counterpartyNewEntity.getCaiSigned());
        counterparty.setDescription(counterpartyNewEntity.getDescription());
        counterparty.setDriver(counterpartyNewEntity.getDriver());
        counterparty.setImpactPoint(counterpartyNewEntity.getImpactPoint());
        counterparty.setInsuranceCompany(counterpartyNewEntity.getInsuranceCompany());
        counterparty.setInsured(counterpartyNewEntity.getInsured());
        counterparty.setEligibility(counterpartyNewEntity.getEligibility());
        counterparty.setReplacementCar(counterpartyNewEntity.getReplacementCar() );
        counterparty.setReplacementPlate(counterpartyNewEntity.getReplacementPlate());
        counterparty.setRepairStatus(counterpartyNewEntity.getRepairStatus());
        counterparty.setResponsible(counterpartyNewEntity.getResponsible());
        counterparty.setType(counterpartyNewEntity.getType());
        counterparty.setVehicle(counterpartyNewEntity.getVehicle());
        counterparty.setRepairProcedure(counterpartyNewEntity.getRepairProcedure());
        counterparty.setUserCreate(counterpartyNewEntity.getUserCreate());
        counterparty.setCreatedAt(counterpartyNewEntity.getCreatedAt());
        counterparty.setUserUpdate(counterpartyNewEntity.getUserUpdate());
        counterparty.setUpdateAt(counterpartyNewEntity.getUpdateAt());
        counterparty.setAssignedAt(counterpartyNewEntity.getAssignedAt());
        counterparty.setRepairCreatedAt(counterpartyNewEntity.getRepairCreatedAt());
        counterparty.setAssignedTo(counterpartyNewEntity.getAssignedTo());
        counterparty.setHistoricals(counterpartyNewEntity.getHistoricals());
        counterparty.setManager(counterpartyNewEntity.getManager());
        counterparty.setLastContact(counterpartyNewEntity.getLastContact());
        counterparty.setCanalization(counterpartyNewEntity.getCanalization());
        counterparty.setPolicyNumber(counterpartyNewEntity.getPolicyNumber());
        counterparty.setPolicyBeginningValidity(counterpartyNewEntity.getPolicyBeginningValidity());
        counterparty.setPolicyEndValidity(counterpartyNewEntity.getPolicyEndValidity());
        counterparty.setManagementType(counterpartyNewEntity.getManagementType());
        counterparty.setAskedForDamages(counterpartyNewEntity.getAskedForDamages());
        counterparty.setLegalOrConsultant(counterpartyNewEntity.getLegalOrConsultant());
        counterparty.setDateRequestDamages(counterpartyNewEntity.getDateRequestDamages());
        counterparty.setExpirationInstant(counterpartyNewEntity.getExpirationInstant());
        counterparty.setLegal(counterpartyNewEntity.getLegal());
        counterparty.setEmailLegal(counterpartyNewEntity.getEmailLegal());
        counterparty.setIsAld(counterpartyNewEntity.getAld());
        counterparty.setMotivation(counterpartyNewEntity.getMotivation());
        counterparty.setAuthorities(counterpartyNewEntity.getAuthorities());

        if(counterpartyNewEntity.getClaims() != null) {
            ClaimsEntity claimsEntity = new ClaimsEntity();
            claimsEntity.setId(counterpartyNewEntity.getClaims().getId());
            counterparty.setClaims(claimsEntity);
        }
        return counterparty;
    }


    public static CounterpartyEntity adptCounterpartyNewToCounterpartyOldWithClaimsEntity(CounterpartyNewEntity counterpartyNewEntity) {

        CounterpartyEntity counterparty = new CounterpartyEntity();
        counterparty.setCounterpartyId(counterpartyNewEntity.getCounterpartyId());
        counterparty.setPracticeIdCounterparty(counterpartyNewEntity.getPracticeIdCounterparty());
        counterparty.setAttachments(counterpartyNewEntity.getAttachments());
        counterparty.setCaiSigned(counterpartyNewEntity.getCaiSigned());
        counterparty.setDescription(counterpartyNewEntity.getDescription());
        counterparty.setDriver(counterpartyNewEntity.getDriver());
        counterparty.setImpactPoint(counterpartyNewEntity.getImpactPoint());
        counterparty.setInsuranceCompany(counterpartyNewEntity.getInsuranceCompany());
        counterparty.setInsured(counterpartyNewEntity.getInsured());
        counterparty.setEligibility(counterpartyNewEntity.getEligibility());
        counterparty.setReplacementCar(counterpartyNewEntity.getReplacementCar() );
        counterparty.setReplacementPlate(counterpartyNewEntity.getReplacementPlate());
        counterparty.setRepairStatus(counterpartyNewEntity.getRepairStatus());
        counterparty.setResponsible(counterpartyNewEntity.getResponsible());
        counterparty.setType(counterpartyNewEntity.getType());
        counterparty.setVehicle(counterpartyNewEntity.getVehicle());
        counterparty.setRepairProcedure(counterpartyNewEntity.getRepairProcedure());
        counterparty.setUserCreate(counterpartyNewEntity.getUserCreate());
        counterparty.setCreatedAt(counterpartyNewEntity.getCreatedAt());
        counterparty.setUserUpdate(counterpartyNewEntity.getUserUpdate());
        counterparty.setUpdateAt(counterpartyNewEntity.getUpdateAt());
        counterparty.setAssignedAt(counterpartyNewEntity.getAssignedAt());
        counterparty.setRepairCreatedAt(counterpartyNewEntity.getRepairCreatedAt());
        counterparty.setAssignedTo(counterpartyNewEntity.getAssignedTo());
        counterparty.setHistoricals(counterpartyNewEntity.getHistoricals());
        counterparty.setManager(counterpartyNewEntity.getManager());
        counterparty.setLastContact(counterpartyNewEntity.getLastContact());
        counterparty.setCanalization(counterpartyNewEntity.getCanalization());
        counterparty.setPolicyNumber(counterpartyNewEntity.getPolicyNumber());
        counterparty.setPolicyBeginningValidity(counterpartyNewEntity.getPolicyBeginningValidity());
        counterparty.setPolicyEndValidity(counterpartyNewEntity.getPolicyEndValidity());
        counterparty.setManagementType(counterpartyNewEntity.getManagementType());
        counterparty.setAskedForDamages(counterpartyNewEntity.getAskedForDamages());
        counterparty.setLegalOrConsultant(counterpartyNewEntity.getLegalOrConsultant());
        counterparty.setDateRequestDamages(counterpartyNewEntity.getDateRequestDamages());
        counterparty.setExpirationInstant(counterpartyNewEntity.getExpirationInstant());
        counterparty.setLegal(counterpartyNewEntity.getLegal());
        counterparty.setEmailLegal(counterpartyNewEntity.getEmailLegal());
        counterparty.setIsAld(counterpartyNewEntity.getAld());
        counterparty.setMotivation(counterpartyNewEntity.getMotivation());
        counterparty.setAuthorities(counterpartyNewEntity.getAuthorities());

        if(counterpartyNewEntity.getClaims() != null) {
            ClaimsEntity claimsEntity = new ClaimsEntity();
            claimsEntity.setId(counterpartyNewEntity.getClaims().getId());
            counterparty.setClaims(claimsEntity);
        }
        return counterparty;
    }



    public static CounterpartyEntity adptCounterpartyNewToCounterpartyOldWithClaimsEntityFull(CounterpartyNewEntity counterpartyNewEntity) {

        CounterpartyEntity counterparty = new CounterpartyEntity();
        counterparty.setCounterpartyId(counterpartyNewEntity.getCounterpartyId());
        counterparty.setPracticeIdCounterparty(counterpartyNewEntity.getPracticeIdCounterparty());
        counterparty.setAttachments(counterpartyNewEntity.getAttachments());
        counterparty.setCaiSigned(counterpartyNewEntity.getCaiSigned());
        counterparty.setDescription(counterpartyNewEntity.getDescription());
        counterparty.setDriver(counterpartyNewEntity.getDriver());
        counterparty.setImpactPoint(counterpartyNewEntity.getImpactPoint());
        counterparty.setInsuranceCompany(counterpartyNewEntity.getInsuranceCompany());
        counterparty.setInsured(counterpartyNewEntity.getInsured());
        counterparty.setEligibility(counterpartyNewEntity.getEligibility());
        counterparty.setReplacementCar(counterpartyNewEntity.getReplacementCar() );
        counterparty.setReplacementPlate(counterpartyNewEntity.getReplacementPlate());
        counterparty.setRepairStatus(counterpartyNewEntity.getRepairStatus());
        counterparty.setResponsible(counterpartyNewEntity.getResponsible());
        counterparty.setType(counterpartyNewEntity.getType());
        counterparty.setVehicle(counterpartyNewEntity.getVehicle());
        counterparty.setRepairProcedure(counterpartyNewEntity.getRepairProcedure());
        counterparty.setUserCreate(counterpartyNewEntity.getUserCreate());
        counterparty.setCreatedAt(counterpartyNewEntity.getCreatedAt());
        counterparty.setUserUpdate(counterpartyNewEntity.getUserUpdate());
        counterparty.setUpdateAt(counterpartyNewEntity.getUpdateAt());
        counterparty.setAssignedAt(counterpartyNewEntity.getAssignedAt());
        counterparty.setRepairCreatedAt(counterpartyNewEntity.getRepairCreatedAt());
        counterparty.setAssignedTo(counterpartyNewEntity.getAssignedTo());
        counterparty.setHistoricals(counterpartyNewEntity.getHistoricals());
        counterparty.setManager(counterpartyNewEntity.getManager());
        counterparty.setLastContact(counterpartyNewEntity.getLastContact());
        counterparty.setCanalization(counterpartyNewEntity.getCanalization());
        counterparty.setPolicyNumber(counterpartyNewEntity.getPolicyNumber());
        counterparty.setPolicyBeginningValidity(counterpartyNewEntity.getPolicyBeginningValidity());
        counterparty.setPolicyEndValidity(counterpartyNewEntity.getPolicyEndValidity());
        counterparty.setManagementType(counterpartyNewEntity.getManagementType());
        counterparty.setAskedForDamages(counterpartyNewEntity.getAskedForDamages());
        counterparty.setLegalOrConsultant(counterpartyNewEntity.getLegalOrConsultant());
        counterparty.setDateRequestDamages(counterpartyNewEntity.getDateRequestDamages());
        counterparty.setExpirationInstant(counterpartyNewEntity.getExpirationInstant());
        counterparty.setLegal(counterpartyNewEntity.getLegal());
        counterparty.setEmailLegal(counterpartyNewEntity.getEmailLegal());
        counterparty.setIsAld(counterpartyNewEntity.getAld());
        counterparty.setMotivation(counterpartyNewEntity.getMotivation());
        counterparty.setAuthorities(counterpartyNewEntity.getAuthorities());

        if(counterpartyNewEntity.getClaims() != null) {
            counterparty.setClaims(ClaimsAdapter.adaptFromNewClaimsEntityToOldClaimsEntityWithoutCounterparty(counterpartyNewEntity.getClaims()));
        }
        return counterparty;
    }

    public static List<CounterpartyEntity> adptCounterpartyNewToCounterpartyOldWithClaimsEntityFullList(List<CounterpartyNewEntity> counterpartyNewEntityList) {

        if (counterpartyNewEntityList != null) {

            List<CounterpartyEntity> counterpartyList = new LinkedList<>();

            for (CounterpartyNewEntity current : counterpartyNewEntityList) {
                counterpartyList.add(adptCounterpartyNewToCounterpartyOldWithClaimsEntityFull(current));
            }
            return counterpartyList;
        }

        return null;
    }




    public static List<CounterpartyNewEntity> adptCounterpartyOldToCounterpartyNewList(List<CounterpartyEntity> counterpartyOldEntityList) {

        if (counterpartyOldEntityList != null) {

            List<CounterpartyNewEntity> counterpartyList = new LinkedList<>();

            for (CounterpartyEntity current : counterpartyOldEntityList) {
                counterpartyList.add(adptCounterpartyOldToCounterpartyNew(current));
            }
            return counterpartyList;
        }

        return null;
    }


    public static List<CounterpartyEntity> adptCounterpartyNewToCounterpartyOldList(List<CounterpartyNewEntity> counterpartyNewEntityList) {

        if (counterpartyNewEntityList != null) {

            List<CounterpartyEntity> counterpartyList = new LinkedList<>();

            for (CounterpartyNewEntity current : counterpartyNewEntityList) {
                counterpartyList.add(adptCounterpartyNewToCounterpartyOld(current));
            }
            return counterpartyList;
        }

        return null;
    }


    //REFACTOR
    public CounterpartyResponseV1 adptCounterpartsNewToCounterpartyResponseV1ListExternal(CounterpartyEntity counterpartyEntity , ClaimsNewEntity claimsNewEntity) {
        System.out.println(counterpartyEntity.getClaims());

        CounterpartyResponseV1 counterpartyResponse = new CounterpartyResponseV1();
        ClaimsNewEntity claimsEntity = claimsNewEntity;
        counterpartyResponse.setClaimsId(claimsEntity.getId());
        counterpartyResponse.setPracticeId(claimsEntity.getPracticeId());
        counterpartyResponse.setPracticeIdCounterparty(counterpartyEntity.getPracticeIdCounterparty());
        counterpartyResponse.setLocator(claimsEntity.getLocator());
        counterpartyResponse.setClaimsCreatedAt(DateUtil.convertUTCInstantToIS08601String(claimsEntity.getCreatedAt()));
        counterpartyResponse.setRegion(claimsEntity.getClaimAddressStreetRegion());
        counterpartyResponse.setProvince(claimsEntity.getClaimAddressStreetProvince());
        counterpartyResponse.setLocality(claimsEntity.getClaimAddressStreetLocality());
        counterpartyResponse.setAddress(claimsEntity.getClaimAddressFormatted());
        if(claimsEntity.getClaimsFromCompanyEntity()!= null)
            counterpartyResponse.setNumberSx(claimsEntity.getClaimsFromCompanyEntity().getNumberSx());
        counterpartyResponse.setDateAccident(DateUtil.convertUTCInstantToIS08601String(claimsEntity.getDateAccident()));
        counterpartyResponse.setAttachmentList(AttachmentAdapter.adptAttachmentToAttachmentResponseExternal(counterpartyEntity.getAttachments()));
        counterpartyResponse.setCaiSigned(counterpartyEntity.getCaiSigned());
        counterpartyResponse.setCounterpartyId(counterpartyEntity.getCounterpartyId());
        counterpartyResponse.setDescription(counterpartyEntity.getDescription());
        counterpartyResponse.setDriver(DriverAdapter.adptDriverToDriverResponse(counterpartyEntity.getDriver()));
        counterpartyResponse.setImpactPoint(ImpactPointAdapter.adptImpactPointToImpactPointResponse(counterpartyEntity.getImpactPoint()));
        counterpartyResponse.setInsuranceCompany(insuranceCompanyAdapter.adptFromCompanyCounterpartyToCompanyCounterpartyResponse(counterpartyEntity.getInsuranceCompany()));
        counterpartyResponse.setInsured(InsuredAdapter.adptInsuredToInsuredResponse(counterpartyEntity.getInsured()));
        counterpartyResponse.setRepairStatus(counterpartyEntity.getRepairStatus());
        counterpartyResponse.setResponsible(counterpartyEntity.getResponsible());
        counterpartyResponse.setType(counterpartyEntity.getType());
        counterpartyResponse.setVehicle(VehicleAdapter.adptVehicleToVehicleResponse(counterpartyEntity.getVehicle()));
        counterpartyResponse.setRepairProcedure(counterpartyEntity.getRepairProcedure());
        counterpartyResponse.setUserCreate(counterpartyEntity.getUserCreate());
        counterpartyResponse.setCreatedAt(DateUtil.convertUTCInstantToIS08601String(counterpartyEntity.getCreatedAt()));
        counterpartyResponse.setUserUpdate(counterpartyEntity.getUserUpdate());
        counterpartyResponse.setUpdateAt(DateUtil.convertUTCInstantToIS08601String(counterpartyEntity.getUpdateAt()));
        counterpartyResponse.setAssignedAt(DateUtil.convertUTCInstantToIS08601String(counterpartyEntity.getAssignedAt()));
        counterpartyResponse.setRepairCreatedAt(DateUtil.convertUTCInstantToIS08601String(counterpartyEntity.getRepairCreatedAt()));
        counterpartyResponse.setAssignedTo(AssigneeRepairAdapter.adptFromAssignationRepairToAssignationRepairResponse(counterpartyEntity.getAssignedTo()));
        counterpartyResponse.setReplacementCar(counterpartyEntity.getReplacementCar());
        counterpartyResponse.setReplacementPlate(counterpartyEntity.getReplacementPlate());
        counterpartyResponse.setHistoricals(HistoricalCounterpartyAdapter.adptHistoricalCounterpartyToHistoricalCounterpartyResponse(counterpartyEntity.getHistoricals()));
        counterpartyResponse.setManager(ManagerAdapter.getManagerAdapter(counterpartyEntity.getManager()));
        counterpartyResponse.setLastContact(LastContactAdapter.adtpFromLastContactListEntityToLastContactListResponse(counterpartyEntity.getLastContact()));
        counterpartyResponse.setCanalization(CanalizationAdapter.adptFromCanalizationToCanalizationResponse(counterpartyEntity.getCanalization()));
        counterpartyResponse.setEligibility(counterpartyEntity.getEligibility());
        counterpartyResponse.setPolicyNumber(counterpartyEntity.getPolicyNumber());
        counterpartyResponse.setPolicyBeginningValidity(DateUtil.convertUTCInstantToIS08601String(counterpartyEntity.getPolicyBeginningValidity()));
        counterpartyResponse.setPolicyEndValidity(DateUtil.convertUTCInstantToIS08601String(counterpartyEntity.getPolicyEndValidity()));
        counterpartyResponse.setManagementType(counterpartyEntity.getManagementType());
        counterpartyResponse.setAskedForDamages(counterpartyEntity.getAskedForDamages());
        counterpartyResponse.setLegalOrConsultant(counterpartyEntity.getLegalOrConsultant());
        counterpartyResponse.setLegal(counterpartyEntity.getLegal());
        counterpartyResponse.setEmailLegal(counterpartyEntity.getEmailLegal());
        counterpartyResponse.setDateRequestDamages(DateUtil.convertUTCInstantToIS08601String(counterpartyEntity.getInstantRequestDamages()));
        counterpartyResponse.setExpirationDate(DateUtil.convertUTCInstantToIS08601String(counterpartyEntity.getExpirationInstant()));
        counterpartyResponse.setAld(counterpartyEntity.getIsAld());
        counterpartyResponse.setAuthorities(AuthorityAdapter.adptFromAuthorityToAuthorityRepairResponse(counterpartyEntity.getAuthorities()));
        counterpartyResponse.setRead(counterpartyEntity.getIsReadMsa());
        counterpartyResponse.setMotivation(counterpartyEntity.getMotivation());
        return counterpartyResponse;
    }



}
