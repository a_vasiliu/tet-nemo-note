package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.response.AntiTheftRequestResponseV1;
import com.doing.nemo.claims.entity.AntiTheftRequestEntity;
import com.doing.nemo.claims.entity.AntiTheftRequestNewEntity;
import com.doing.nemo.claims.entity.ClaimsNewEntity;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbAntiTheftService.AntiTheftRequest;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

@Component
public class AntiTheftRequestAdapter {

    public static AntiTheftRequestResponseV1 adptAntiTheftRequestToAntiTheftRequestResponseV1(AntiTheftRequest antiTheftRequest){

        AntiTheftRequestResponseV1 antiTheftRequestResponseV1 = new AntiTheftRequestResponseV1();
        antiTheftRequestResponseV1.setIdTransaction(Integer.parseInt(antiTheftRequest.getIdTransaction()));
        antiTheftRequestResponseV1.setRequest(DateUtil.convertUTCDateToIS08601String(antiTheftRequest.getRequest()));
        antiTheftRequestResponseV1.setIdWebSin(antiTheftRequest.getIdWebSin());
        antiTheftRequestResponseV1.setProviderType(antiTheftRequest.getProviderType());
        antiTheftRequestResponseV1.setResponse(DateUtil.convertUTCDateToIS08601String(antiTheftRequest.getResponse()));
        antiTheftRequestResponseV1.setResponseType(antiTheftRequest.getResponseType());
        antiTheftRequestResponseV1.setOutcome(antiTheftRequest.getOutcome());
        antiTheftRequestResponseV1.setSubReport(antiTheftRequest.getSubReport());
        antiTheftRequestResponseV1.setgPower(antiTheftRequest.getgPower());
        antiTheftRequestResponseV1.setCrashNumber(antiTheftRequest.getCrashNumber());
        antiTheftRequestResponseV1.setAnomaly(antiTheftRequest.getAnomaly());
        antiTheftRequestResponseV1.setPdf(antiTheftRequest.getPdf());
        antiTheftRequestResponseV1.setResponseMessage(antiTheftRequest.getResponseMessage());
        //antiTheftRequestResponseV1.setReportNumber(antiTheftRequest.getReportNumber());

        return antiTheftRequestResponseV1;
    }

    public static List<AntiTheftRequestResponseV1> adptAntiTheftRequestToAntiTheftRequestResponseV1(List<AntiTheftRequest> antiTheftRequestList){
        List<AntiTheftRequestResponseV1> antiTheftRequestListResponse = new LinkedList<>();
        if( antiTheftRequestList != null){

            for (AntiTheftRequest att : antiTheftRequestList){
                antiTheftRequestListResponse.add(adptAntiTheftRequestToAntiTheftRequestResponseV1(att));
            }
        }
        return antiTheftRequestListResponse;
    }



    //adpter per la nuova tabella relazione per i crash report

    public static AntiTheftRequestResponseV1 adptAntiTheftRequestEntityToAntiTheftRequestResponseV1(AntiTheftRequestEntity antiTheftRequest){

        AntiTheftRequestResponseV1 antiTheftRequestResponseV1 = new AntiTheftRequestResponseV1();
        antiTheftRequestResponseV1.setIdTransaction(Integer.parseInt(antiTheftRequest.getIdTransaction()));
        antiTheftRequestResponseV1.setRequest(DateUtil.convertUTCDateToIS08601String(antiTheftRequest.getCreatedAt()));
        antiTheftRequestResponseV1.setIdWebSin(antiTheftRequest.getPracticeId());
        antiTheftRequestResponseV1.setProviderType(antiTheftRequest.getProviderType());
        antiTheftRequestResponseV1.setResponse(DateUtil.convertUTCDateToIS08601String(antiTheftRequest.getUpdatedAt()));
        antiTheftRequestResponseV1.setResponseType(antiTheftRequest.getResponseType());
        antiTheftRequestResponseV1.setOutcome(antiTheftRequest.getOutcome());
        antiTheftRequestResponseV1.setSubReport(antiTheftRequest.getSubReport());
        antiTheftRequestResponseV1.setgPower(antiTheftRequest.getgPower());
        antiTheftRequestResponseV1.setCrashNumber(antiTheftRequest.getCrashNumber());
        antiTheftRequestResponseV1.setAnomaly(antiTheftRequest.getAnomaly());
        antiTheftRequestResponseV1.setPdf(antiTheftRequest.getPdf());
        antiTheftRequestResponseV1.setResponseMessage(antiTheftRequest.getResponseMessage());
        //antiTheftRequestResponseV1.setReportNumber(antiTheftRequest.getReportNumber());

        return antiTheftRequestResponseV1;
    }



    public static List<AntiTheftRequestResponseV1> adptAntiTheftRequestEntityListToAntiTheftRequestResponseV1List(List<AntiTheftRequestEntity> antiTheftRequestList){
        List<AntiTheftRequestResponseV1> antiTheftRequestListResponse = new LinkedList<>();
        if( antiTheftRequestList != null){

            for (AntiTheftRequestEntity att : antiTheftRequestList){
                antiTheftRequestListResponse.add(adptAntiTheftRequestEntityToAntiTheftRequestResponseV1(att));
            }
        }
        return antiTheftRequestListResponse;
    }

    //NUOVI ADAPTER
    public static AntiTheftRequestNewEntity adptAntiTheftRequestOldToAntiTheftRequestNew(AntiTheftRequestEntity antiTheftRequest, ClaimsNewEntity claimsNewEntity){
        if(antiTheftRequest == null){
            return null;
        }

        AntiTheftRequestNewEntity antiTheftRequestNewEntity = new AntiTheftRequestNewEntity();
        antiTheftRequestNewEntity.setId(antiTheftRequest.getId());
        antiTheftRequestNewEntity.setIdTransaction(antiTheftRequest.getIdTransaction());
        antiTheftRequestNewEntity.setProviderType(antiTheftRequest.getProviderType());
        antiTheftRequestNewEntity.setResponseType(antiTheftRequest.getResponseType());
        antiTheftRequestNewEntity.setOutcome(antiTheftRequest.getOutcome());
        antiTheftRequestNewEntity.setSubReport(antiTheftRequest.getSubReport());
        antiTheftRequestNewEntity.setgPower(antiTheftRequest.getgPower());
        antiTheftRequestNewEntity.setCrashNumber(antiTheftRequest.getCrashNumber());
        antiTheftRequestNewEntity.setAnomaly(antiTheftRequest.getAnomaly());
        antiTheftRequestNewEntity.setPdf(antiTheftRequest.getPdf());
        antiTheftRequestNewEntity.setResponseMessage(antiTheftRequest.getResponseMessage());
        antiTheftRequestNewEntity.setCreatedAt(antiTheftRequest.getCreatedAt());
        antiTheftRequestNewEntity.setUpdatedAt(antiTheftRequest.getUpdatedAt());
        antiTheftRequestNewEntity.setPracticeId(antiTheftRequest.getPracticeId());
        antiTheftRequestNewEntity.setClaim(claimsNewEntity);
        antiTheftRequestNewEntity.setCrashReport(antiTheftRequest.getCrashReport());

        return antiTheftRequestNewEntity;
    }

    public static AntiTheftRequestEntity adptAntiTheftRequestNewToAntiTheftRequestOld(AntiTheftRequestNewEntity antiTheftRequestNewEntity){

        AntiTheftRequestEntity antiTheftRequestEntity = new AntiTheftRequestEntity();
        antiTheftRequestEntity.setId(antiTheftRequestNewEntity.getId());
        antiTheftRequestEntity.setIdTransaction(antiTheftRequestNewEntity.getIdTransaction());
        antiTheftRequestEntity.setProviderType(antiTheftRequestNewEntity.getProviderType());
        antiTheftRequestEntity.setResponseType(antiTheftRequestNewEntity.getResponseType());
        antiTheftRequestEntity.setOutcome(antiTheftRequestNewEntity.getOutcome());
        antiTheftRequestEntity.setSubReport(antiTheftRequestNewEntity.getSubReport());
        antiTheftRequestEntity.setgPower(antiTheftRequestNewEntity.getgPower());
        antiTheftRequestEntity.setCrashNumber(antiTheftRequestNewEntity.getCrashNumber());
        antiTheftRequestEntity.setAnomaly(antiTheftRequestNewEntity.getAnomaly());
        antiTheftRequestEntity.setPdf(antiTheftRequestNewEntity.getPdf());
        antiTheftRequestEntity.setResponseMessage(antiTheftRequestNewEntity.getResponseMessage());
        antiTheftRequestEntity.setCreatedAt(antiTheftRequestNewEntity.getCreatedAt());
        antiTheftRequestEntity.setUpdatedAt(antiTheftRequestNewEntity.getUpdatedAt());
        antiTheftRequestEntity.setPracticeId(antiTheftRequestNewEntity.getPracticeId());
        antiTheftRequestEntity.setCrashReport(antiTheftRequestNewEntity.getCrashReport());



        return antiTheftRequestEntity;
    }

    public static List<AntiTheftRequestNewEntity> adptAntiTheftRequestOldToAntiTheftRequestNewList(List<AntiTheftRequestEntity> antiTheftRequestList, ClaimsNewEntity claimsNewEntity){
        List<AntiTheftRequestNewEntity> antiTheftRequestNewEntities = new LinkedList<>();
        if( antiTheftRequestList != null){

            for (AntiTheftRequestEntity current : antiTheftRequestList){
                antiTheftRequestNewEntities.add(adptAntiTheftRequestOldToAntiTheftRequestNew(current,claimsNewEntity));
            }
        }
        return antiTheftRequestNewEntities;
    }

    public static List<AntiTheftRequestEntity> adptAntiTheftRequestNewToAntiTheftRequestOldList(List<AntiTheftRequestNewEntity> antiTheftRequestList){
        List<AntiTheftRequestEntity> antiTheftRequestOldEntities = new LinkedList<>();
        if( antiTheftRequestList != null){

            for (AntiTheftRequestNewEntity current : antiTheftRequestList){
                antiTheftRequestOldEntities.add(adptAntiTheftRequestNewToAntiTheftRequestOld(current));
            }
        }
        return antiTheftRequestOldEntities;
    }

}
