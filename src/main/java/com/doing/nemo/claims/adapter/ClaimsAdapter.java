package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.ClaimsCheckRequestV1;
import com.doing.nemo.claims.controller.payload.response.*;
import com.doing.nemo.claims.controller.payload.response.claims.ComplaintResponse;
import com.doing.nemo.claims.controller.payload.response.claims.DamagedResponse;
import com.doing.nemo.claims.controller.payload.response.claims.FormResponse;
import com.doing.nemo.claims.controller.payload.response.claims.HistoricalResponse;
import com.doing.nemo.claims.controller.payload.response.complaint.DataAccidentResponse;
import com.doing.nemo.claims.controller.payload.response.complaint.EntrustedResponse;
import com.doing.nemo.claims.controller.payload.response.damaged.CustomerResponse;
import com.doing.nemo.claims.controller.payload.response.forms.AttachmentResponse;
import com.doing.nemo.claims.entity.*;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.enumerated.NotesEnum.NotesTypeEnum;
import com.doing.nemo.claims.entity.enumerated.myAld.MyAldInsertedByEnum;
import com.doing.nemo.claims.entity.jsonb.Address;
import com.doing.nemo.claims.entity.jsonb.Authority;
import com.doing.nemo.claims.entity.jsonb.Historical;
import com.doing.nemo.claims.entity.jsonb.Theft;
import com.doing.nemo.claims.entity.jsonb.complaint.Complaint;
import com.doing.nemo.claims.entity.jsonb.complaint.jsonbComplaint.Entrusted;
import com.doing.nemo.claims.entity.jsonb.complaint.jsonbComplaint.FromCompany;
import com.doing.nemo.claims.entity.jsonb.complaint.jsonbComplaint.jsonbDataAccidents.DataAccident;
import com.doing.nemo.claims.entity.jsonb.damaged.Damaged;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.Contract;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.Customer;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.FleetManager;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.InsuranceCompany;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbAntiTheftService.AntiTheftService;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbDriver.Driver;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbVehicle.Vehicle;
import com.doing.nemo.claims.entity.jsonb.forms.jsonbForms.Attachment;
import com.doing.nemo.claims.entity.jsonb.refund.Refund;
import com.doing.nemo.claims.entity.view.SearchDashboardClaimsView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.time.Instant;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

@Component
public class ClaimsAdapter {

    @Autowired
    private CounterpartyAdapter counterpartyAdapter;

    public static ClaimsResponseV1 adptClaimsToClaimsResponse(ClaimsEntity claimsEntity) {
        if(claimsEntity == null){
            return null;
        }
        ClaimsResponseV1 claimsResponseV1 = new ClaimsResponseV1();
        claimsResponseV1.setId(claimsEntity.getId());
        if(claimsEntity.getStatus() != null)
            claimsResponseV1.setStatus(claimsEntity.getStatus());
        claimsResponseV1.setType(claimsEntity.getType());
        claimsResponseV1.setCreatedAt(DateUtil.convertUTCInstantToIS08601String(claimsEntity.getCreatedAt()));
        claimsResponseV1.setComplaint(ComplaintAdapter.adptComplaintToComplaintResponse(claimsEntity.getComplaint()));
        claimsResponseV1.setDamaged(DamagedAdapter.adptDamagedToDamagedResponse(claimsEntity.getDamaged(), claimsEntity.getAntiTheftRequestEntities()));
        claimsResponseV1.setCounterparty(CounterpartyAdapter.adptCounterpartyToCounterpartyResponse(claimsEntity.getCounterparts()));
        claimsResponseV1.setWoundedList(WoundedAdapter.adptWoundedToWoundedResponse(claimsEntity.getWoundedList()));
        claimsResponseV1.setDeponentList(DeponentAdapter.adptDeponentToDeponentResponse(claimsEntity.getDeponentList()));
        claimsResponseV1.setForms(FormsAdapter.adptFromToFormsResponse(claimsEntity.getForms()));
        List<AttachmentResponse> crashReportAtt = new LinkedList<>();
        if(claimsEntity.getAntiTheftRequestEntities()!=null && claimsEntity.getAntiTheftRequestEntities().size()!=0){
            List<AntiTheftRequestEntity> antiTheftRequestEntities = claimsEntity.getAntiTheftRequestEntities();
            for(AntiTheftRequestEntity current: antiTheftRequestEntities){
                if(current.getCrashReport()!=null){
                    if(claimsResponseV1.getForms()==null){
                        claimsResponseV1.setForms(new FormResponse());
                    }
                    if(claimsResponseV1.getForms().getAttachment()==null){
                        claimsResponseV1.getForms().setAttachment(new LinkedList<>());
                    }
                    crashReportAtt.add(AttachmentAdapter.adptAttachmentToAttachmentResponse(current.getCrashReport()));

                }
            }
        }
        if(claimsResponseV1.getForms()!=null && claimsResponseV1.getForms().getAttachment()!=null){
            crashReportAtt.addAll(claimsResponseV1.getForms().getAttachment());
            claimsResponseV1.getForms().setAttachment(crashReportAtt);
        }
        System.out.println(claimsResponseV1.getForms());
        claimsResponseV1.setPaiComunication(claimsEntity.getPaiComunication());
        claimsResponseV1.setFoundModel(FoundModelAdapter.adptFoundModelToFoundModelResponse(claimsEntity.getFoundModel()));
        claimsResponseV1.setCaiDetails(CaiAdapter.adptCaiToCaiResponse(claimsEntity.getCaiDetails()));
        claimsResponseV1.setNotes(NotesAdapter.adptNotesToNotesResponse(claimsEntity.getNotes()));
        claimsResponseV1.setHistorical(HistoricalAdapter.adptHistoricalToHistoricalResponse(claimsEntity.getHistorical()));
        claimsResponseV1.setUpdateAt(DateUtil.convertUTCInstantToIS08601String(claimsEntity.getUpdateAt()));
        claimsResponseV1.setPracticeId(claimsEntity.getPracticeId());
        claimsResponseV1.setWithCounterparty(claimsEntity.getWithCounterparty());
        claimsResponseV1.setForced(claimsEntity.getForced());
        claimsResponseV1.setInEvidence(claimsEntity.getInEvidence());
        claimsResponseV1.setIdSaleforce(claimsEntity.getIdSaleforce());
        claimsResponseV1.setExemption(ExemptionAdapter.adptExemptionToExemptionResponse(claimsEntity.getExemption()));
        claimsResponseV1.setRefund(RefundAdapter.adptFromRefundToRefundResponse(claimsEntity.getRefund()));
        claimsResponseV1.setAuthorityResponseV1List(AuthorityAdapter.adtpFromAuthorityListToAuthorityResponseList(claimsEntity.getAuthorities()));
        if(claimsEntity.getMetadata() != null ) {
            claimsResponseV1.setMetadata(claimsEntity.getMetadata().getMetadata());
        }
        if(claimsEntity.getTheft() != null){
            claimsResponseV1.setTheft(TheftClaimsAdapter.adptTheftToTheftResponseV1(claimsEntity.getTheft()));
        }
        claimsResponseV1.setWithContinuation(claimsEntity.getWithContinuation());
        claimsResponseV1.setMotivation(claimsEntity.getMotivation());
        claimsResponseV1.setRead(claimsEntity.getRead());
        claimsResponseV1.setReadAcclaims(claimsEntity.getReadAcclaims());
        claimsResponseV1.setAuthorityLinkable(claimsEntity.getAuthorityLinkable());
        claimsResponseV1.setPoVariation(claimsEntity.getPoVariation());
        claimsResponseV1.setLegalComunication(claimsEntity.getLegalComunication());
        claimsResponseV1.setTotalPenalty(claimsEntity.getTotalPenalty());
        claimsResponseV1.setMigrated(claimsEntity.getMigrated());
        claimsResponseV1.setCompleteDocumentation(claimsEntity.getCompleteDocumentation());
        return claimsResponseV1;
    }

    public static ClaimsResponseComplaintOperatorV2 adptClaimsToClaimsResponseComplaintOperator(SearchDashboardClaimsView viewEntityItem){
        if(viewEntityItem == null){
            return null;
        }

        ClaimsResponseComplaintOperatorV2 claimsResponseComplaintOperatorV2 = new ClaimsResponseComplaintOperatorV2();


        claimsResponseComplaintOperatorV2.setId(viewEntityItem.getId());

        claimsResponseComplaintOperatorV2.setCreatedAt(DateUtil.convertUTCInstantToIS08601String(viewEntityItem.getCreatedAt()));
        claimsResponseComplaintOperatorV2.setUpdateAt(DateUtil.convertUTCInstantToIS08601String(viewEntityItem.getUpdatedAt()));
        claimsResponseComplaintOperatorV2.setType(viewEntityItem.getType());
        claimsResponseComplaintOperatorV2.setPracticeId(viewEntityItem.getPracticeId());
        if(claimsResponseComplaintOperatorV2.getStatus() != null) claimsResponseComplaintOperatorV2.setStatus(viewEntityItem.getStatus());
        claimsResponseComplaintOperatorV2.setInEvidence(viewEntityItem.getInEvidence());
        claimsResponseComplaintOperatorV2.setPoVariation(viewEntityItem.getPoVariation());

        ComplaintResponse complaint = new ComplaintResponse();


        DataAccidentResponse dataAccidentResponse = new DataAccidentResponse();
        dataAccidentResponse.setDateAccident(DateUtil.convertUTCInstantToIS08601String(viewEntityItem.getDateAccident()));
        dataAccidentResponse.setTypeAccident(DataAccidentTypeAccidentEnum.valueOf(viewEntityItem.getTypeAccident()));
        complaint.setClientId(viewEntityItem.getClientId()!=null?viewEntityItem.getClientId().toString():"");
        complaint.setPlate(viewEntityItem.getPlate());
        EntrustedResponse entrusted = new EntrustedResponse();
        entrusted.setAutoEntrust(viewEntityItem.getAutoEntrust());
        complaint.setEntrusted(entrusted);
        complaint.setDataAccident(dataAccidentResponse);

        claimsResponseComplaintOperatorV2.setComplaint(complaint);

        /* "damaged" */
        /* "customer" */


        DamagedResponse damage = new DamagedResponse();
        CustomerResponse customer = new CustomerResponse();
        customer.setCustomerId(viewEntityItem.getCustomerId()!=null?viewEntityItem.getCustomerId().toString(): "");
        damage.setCustomer(customer);
        AntiTheftServiceResponseV1 antiTheftServiceResponseV1;

    /*    List<AntiTheftRequestResponseV1> antiTheftRequestListResponse = new LinkedList<>();
        if( antiTheftRequestList != null){

            for (AntiTheftRequestEntity att : antiTheftRequestList){
                antiTheftRequestListResponse.add(adptAntiTheftRequestEntityToAntiTheftRequestResponseV1(att));
            }
        }*/


    /*    claimsResponseComplaintOperatorV2.setDamaged(DamagedAdapter.adptDamagedToDamagedResponse(claimsEntity.getDamaged(), claimsEntity.getAntiTheftRequestEntities()));
*/
        claimsResponseComplaintOperatorV2.setPending(viewEntityItem.getPending());
        return claimsResponseComplaintOperatorV2;

    }
    public static ClaimsResponseComplaintOperatorV1 adptClaimsToClaimsResponseComplaintOperator(ClaimsEntity claimsEntity) {
        if(claimsEntity == null){
            return null;
        }
        ClaimsResponseComplaintOperatorV1 claimsResponseComplaintOperatorV1 = new ClaimsResponseComplaintOperatorV1();
        claimsResponseComplaintOperatorV1.setId(claimsEntity.getId());

        claimsResponseComplaintOperatorV1.setCreatedAt(DateUtil.convertUTCInstantToIS08601String(claimsEntity.getCreatedAt()));
        claimsResponseComplaintOperatorV1.setUpdateAt(DateUtil.convertUTCInstantToIS08601String(claimsEntity.getUpdateAt()));
        claimsResponseComplaintOperatorV1.setType(claimsEntity.getType());
        claimsResponseComplaintOperatorV1.setPracticeId(claimsEntity.getPracticeId());
        if(claimsEntity.getStatus() != null) claimsResponseComplaintOperatorV1.setStatus(claimsEntity.getStatus());
        claimsResponseComplaintOperatorV1.setInEvidence(claimsEntity.getInEvidence());
        claimsResponseComplaintOperatorV1.setPoVariation(claimsEntity.getPoVariation());
        claimsResponseComplaintOperatorV1.setComplaint(ComplaintAdapter.adptComplaintToComplaintResponse(claimsEntity.getComplaint()));

        /*
        List<AttachmentResponse> crashReportAtt = new LinkedList<>();
        if(claimsEntity.getAntiTheftRequestEntities()!=null && claimsEntity.getAntiTheftRequestEntities().size()!=0){
            List<AntiTheftRequestEntity> antiTheftRequestEntities = claimsEntity.getAntiTheftRequestEntities();
            for(AntiTheftRequestEntity current: antiTheftRequestEntities){
                if(current.getCrashReport()!=null){
                    crashReportAtt.add(AttachmentAdapter.adptAttachmentToAttachmentResponse(current.getCrashReport()));
                }
            }
        }
         */
        claimsResponseComplaintOperatorV1.setDamaged(DamagedAdapter.adptDamagedToDamagedResponse(claimsEntity.getDamaged(), claimsEntity.getAntiTheftRequestEntities()));

        claimsResponseComplaintOperatorV1.setPending(claimsEntity.getPending());
        return claimsResponseComplaintOperatorV1;
    }

    public ClaimsEntity adptClaimsCheckRequestToClaimsEntity (ClaimsCheckRequestV1 claimsCheckRequestV1){
        ClaimsEntity claimsEntity = new ClaimsEntity();
        if(claimsCheckRequestV1 == null)
            return null;

        claimsEntity.setStatus(claimsCheckRequestV1.getStatus());
        claimsEntity.setDamaged(DamagedAdapter.adptDamagedRequestInsertToDamaged(claimsCheckRequestV1.getDamaged()));
        claimsEntity.setComplaint(ComplaintAdapter.adptComplaintRequestToComplaint(claimsCheckRequestV1.getComplaint()));
        claimsEntity.setCounterparts(counterpartyAdapter.adptCounterpartyRequestToCounterparty(claimsCheckRequestV1.getCounterparty()));
        return claimsEntity;
    }

    public static ClaimsExternalResponse adptClaimsToClaimsExternalResponse(ClaimsEntity claimsEntity) {

        ClaimsExternalResponse claimsResponseV1 = new ClaimsExternalResponse();
        claimsResponseV1.setId(claimsEntity.getId());
        if(claimsEntity.getStatus() != null)
            claimsResponseV1.setStatus(claimsEntity.getStatus());
        claimsResponseV1.setType(claimsEntity.getType());
        claimsResponseV1.setCreatedAt(DateUtil.convertUTCInstantToIS08601String(claimsEntity.getCreatedAt()));
        claimsResponseV1.setComplaint(ComplaintAdapter.adptComplaintToComplaintExternalResponse(claimsEntity.getComplaint()));
        claimsResponseV1.setDamaged(DamagedAdapter.adptDamagedToDamagedExternalResponse(claimsEntity.getDamaged(), claimsEntity.getAntiTheftRequestEntities()));
        claimsResponseV1.setCounterparty(CounterpartyAdapter.adptCounterpartyToCounterpartyExternalResponse(claimsEntity.getCounterparts()));
        claimsResponseV1.setWounded(WoundedAdapter.adptWoundedToWoundedResponse(claimsEntity.getWoundedList()));
        claimsResponseV1.setDeponent(DeponentAdapter.adptDeponentToDeponentResponse(claimsEntity.getDeponentList()));
        claimsResponseV1.setForms(FormsAdapter.adptFromToFormsExternalResponse(claimsEntity.getForms()));
        claimsResponseV1.setCaiDetails(CaiAdapter.adptCaiToCaiResponse(claimsEntity.getCaiDetails()));
        claimsResponseV1.setUpdateAt(DateUtil.convertUTCInstantToIS08601String(claimsEntity.getUpdateAt()));
        claimsResponseV1.setPracticeId(claimsEntity.getPracticeId());
        claimsResponseV1.setWithCounterparty(claimsEntity.getWithCounterparty());
        claimsResponseV1.setIdSaleforce(claimsEntity.getIdSaleforce());
        claimsResponseV1.setExemption(ExemptionAdapter.adptExemptionToExemptionResponse(claimsEntity.getExemption()));
        claimsResponseV1.setRefund(RefundAdapter.adptFromRefundToRefundResponse(claimsEntity.getRefund()));
        if(claimsEntity.getTheft() != null){
            claimsResponseV1.setTheftResponse(TheftClaimsAdapter.adptTheftToTheftExternalResponse(claimsEntity.getTheft()));
        }
        claimsResponseV1.setMotivation(claimsEntity.getMotivation());
        claimsResponseV1.setRead(claimsEntity.getRead());
        claimsResponseV1.setReadAcclaims(claimsEntity.getReadAcclaims());
        claimsResponseV1.setPoVariation(claimsEntity.getPoVariation());
        claimsResponseV1.setTotalPenalty(claimsEntity.getTotalPenalty());
        claimsResponseV1.setMigrated(claimsEntity.getMigrated());
        claimsResponseV1.setCompleteDocumentation(claimsEntity.getCompleteDocumentation());
        return claimsResponseV1;
    }

    public static List<ClaimsExternalResponse> adptClaimsToClaimsExternalResponse(List<ClaimsEntity> claimsEntityList) {

        List<ClaimsExternalResponse> claimsEntityResponseList = new LinkedList<>();
        for (ClaimsEntity att : claimsEntityList) {
            claimsEntityResponseList.add(adptClaimsToClaimsExternalResponse(att));
        }

        return claimsEntityResponseList;
    }

    public static List<ClaimsResponseV1> adptClaimsToClaimsResponse(List<ClaimsEntity> claimsEntityList) {

        List<ClaimsResponseV1> claimsEntityResponseList = new LinkedList<>();
        for (ClaimsEntity att : claimsEntityList) {
            claimsEntityResponseList.add(adptClaimsToClaimsResponse(att));
        }

        return claimsEntityResponseList;
    }

    public static List<ClaimsResponseComplaintOperatorV2> adptClaimsToClaimsViewResponseComplaintOperator(List<SearchDashboardClaimsView> claimsViewEntityList ){
        List<ClaimsResponseComplaintOperatorV2> claimsViewEntityResponseComplaintOperatorList = new LinkedList<>();
        for (SearchDashboardClaimsView viewItem: claimsViewEntityList){
            claimsViewEntityResponseComplaintOperatorList.add(adptClaimsToClaimsResponseComplaintOperator(viewItem));
        }
        return claimsViewEntityResponseComplaintOperatorList;
    }

    public static List<ClaimsResponseComplaintOperatorV1> adptClaimsToClaimsResponseComplaintOperator(List<ClaimsEntity> claimsEntityList) {

        List<ClaimsResponseComplaintOperatorV1> claimsEntityResponseComplaintOperatorList = new LinkedList<>();
        for (ClaimsEntity att : claimsEntityList) {
            claimsEntityResponseComplaintOperatorList.add(adptClaimsToClaimsResponseComplaintOperator(att));
        }

        return claimsEntityResponseComplaintOperatorList;
    }

    public static ClaimsResponseIdV1 adptIdToIdResponse(String id) {

        ClaimsResponseIdV1 claimsResponseIdV1 = new ClaimsResponseIdV1();
        claimsResponseIdV1.setId(id);

        return claimsResponseIdV1;
    }

    public static ClaimsResponseIdV1 adptIdToIdResponse(String id, Long practiceId) {

        ClaimsResponseIdV1 claimsResponseIdV1 = new ClaimsResponseIdV1();
        claimsResponseIdV1.setId(id);
        claimsResponseIdV1.setPracticeId(practiceId);

        return claimsResponseIdV1;
    }

    public static ClaimsResponseStatsV1 adpdStatsToStatsResponse(Stats stats) {

        ClaimsResponseStatsV1 claimsResponseStats = new ClaimsResponseStatsV1();

        claimsResponseStats.setStats(StatsAdapter.adptStatsElementResponseToStatsElement(stats.getStats()));

        return claimsResponseStats;

    }

    public static ClaimsResponseStatsEvidenceV1 adpdStatsEvidenceToStatsEvidenceResponse(Stats stats) {

        ClaimsResponseStatsEvidenceV1 claimsResponseStats = new ClaimsResponseStatsEvidenceV1();

        claimsResponseStats.setStats(StatsAdapter.adptStatsElementEvidenceToStatsElementEvidenceResponse(stats.getStats()));

        return claimsResponseStats;

    }



    public static HistoricalResponse adptHistoricalToHistoricalResponse(Historical historical) {
        HistoricalResponse historicalResponse = new HistoricalResponse();
        historicalResponse.setEventType(historical.getEventType());
        historicalResponse.setComunicationDescription(historical.getComunicationDescription());
        historicalResponse.setStatusEntityNew(historical.getStatusEntityNew());
        historicalResponse.setStatusEntityOld(historical.getStatusEntityOld());
        historicalResponse.setUpdateAt(DateUtil.convertUTCDateToIS08601String(historical.getUpdateAt()));
        historicalResponse.setUserId(historical.getUserId());
        historicalResponse.setUserName(historical.getUserName());

        return historicalResponse;
    }

    public static List<HistoricalResponse> adptHistoricalToHistoricalResponse(List<Historical> historicalList) {

        List<HistoricalResponse> claimsResponseHistoricalV1List = new LinkedList<>();
        for (Historical att : historicalList) {
            claimsResponseHistoricalV1List.add(adptHistoricalToHistoricalResponse(att));
        }
        return claimsResponseHistoricalV1List;
    }

    //PER refactor
    public static ClaimsResponseNoteV1 adptNoteToNoteEntityResponse(NoteEntity noteEntity) {

        if(noteEntity == null){
            return null;
        }

        ClaimsResponseNoteV1 claimsResponseNoteV1 = new ClaimsResponseNoteV1();

        claimsResponseNoteV1.setNoteTitle(noteEntity.getNoteTitle());
        claimsResponseNoteV1.setNoteType(noteEntity.getNoteType());
        claimsResponseNoteV1.setDescription(noteEntity.getDescription());
        claimsResponseNoteV1.setImportant(noteEntity.getImportant());
        claimsResponseNoteV1.setNoteId(noteEntity.getNoteId());
        claimsResponseNoteV1.setUserId(noteEntity.getUserId());
        claimsResponseNoteV1.setCreatedAt(DateUtil.convertUTCDateToIS08601String(noteEntity.getCreatedAt()));
        claimsResponseNoteV1.setHidden(noteEntity.getHidden());

        return claimsResponseNoteV1;
    }





    public static ClaimsResponseNoteV1 adptObjectToNoteResponse(List<Object> notes) {

        ClaimsResponseNoteV1 claimsResponseNoteV1 = new ClaimsResponseNoteV1();
        for (Object obj : notes) {
            Object[] note = (Object[]) obj;
            claimsResponseNoteV1.setNoteTitle((String) note[0]);
            claimsResponseNoteV1.setNoteType(NotesTypeEnum.create((String) note[1]));
            claimsResponseNoteV1.setDescription((String) note[2]);
            claimsResponseNoteV1.setImportant(Boolean.valueOf((String) note[3]));
            claimsResponseNoteV1.setNoteId((String) note[4]);
            claimsResponseNoteV1.setUserId((String) note[5]);
            claimsResponseNoteV1.setCreatedAt(DateUtil.convertUTCDateToIS08601String(DateUtil.convertIS08601StringToUTCDate((String) note[6])));
            claimsResponseNoteV1.setHidden(Boolean.valueOf((String) note[7]));

        }
        return claimsResponseNoteV1;
    }

    public static ClaimsResponsePaginationV1 adptClaimsPaginationToClaimsPaginationResponse(Pagination<ClaimsEntity> pagination) {

        ClaimsResponsePaginationV1<ClaimsResponseV1> claimsPaginationResponse = new ClaimsResponsePaginationV1<>();

        claimsPaginationResponse.setStats(pagination.getStats());
        claimsPaginationResponse.setItems(adptClaimsToClaimsResponse(pagination.getItems()));

        return claimsPaginationResponse;
    }

    public static ClaimsResponsePaginationV1 adptClaimsPaginationToClaimsPaginationExternalResponse(Pagination<ClaimsEntity> pagination) {

        ClaimsResponsePaginationV1<ClaimsExternalResponse> claimsPaginationResponse = new ClaimsResponsePaginationV1<>();

        claimsPaginationResponse.setStats(pagination.getStats());
        claimsPaginationResponse.setItems(adptClaimsToClaimsExternalResponse(pagination.getItems()));

        return claimsPaginationResponse;
    }

  /*  public static ClaimsResponsePaginationV2 adptClaimsPaginationToClaimsViewPaginationResponseComplaintOperator(Pagination<SearchDashboardClaimsView> pagination) {

        ClaimsResponsePaginationV2<ClaimsResponseComplaintOperatorV2> claimsPaginationResponseComplaintOperator = new ClaimsResponsePaginationV2<>();

        claimsPaginationResponseComplaintOperator.setStats(pagination.getStats());
        claimsPaginationResponseComplaintOperator.setItems(adptClaimsToClaimsViewResponseComplaintOperator(pagination.getItems()));

        return claimsPaginationResponseComplaintOperator;
    }*/

    public static ClaimsResponsePaginationV1 adptClaimsPaginationToClaimsPaginationResponseComplaintOperator(Pagination<ClaimsEntity> pagination) {

        ClaimsResponsePaginationV1<ClaimsResponseComplaintOperatorV1> claimsPaginationResponseComplaintOperator = new ClaimsResponsePaginationV1<>();

        claimsPaginationResponseComplaintOperator.setStats(pagination.getStats());
        claimsPaginationResponseComplaintOperator.setItems(adptClaimsToClaimsResponseComplaintOperator(pagination.getItems()));

        return claimsPaginationResponseComplaintOperator;
    }

    public static String adptClaimsStatusEnumToItalian(ClaimsStatusEnum claimsStatus){
        if(claimsStatus == null) return null;
        switch (claimsStatus){
            case WAITING_FOR_VALIDATION:
                return "Da validare";

            case WAITING_FOR_AUTHORITY:
                return "In attesa di autorizzazione";

            case DRAFT:
                return "Bozza";

            case DELETED:
                return "Cancellato";

            case TO_ENTRUST:
                return "Da affidare";

            case CLOSED:
                return "Chiuso senza seguito";

            case INCOMPLETE:
                return "Incompleta";

            case ENTRUSTED_AT:
                return "Affidato a";

            case REJECTED:
                return "Rifiutata";

            case MANAGED:
                return "Autogestione";

            case PROPOSED_ACCEPTED:
                return "Proposta accettata";

            case WAIT_LOST_POSSESSION:
                return "In attesa di perdita possesso";

            case LOST_POSSESSION:
                return "Perdita possesso";

            case WAIT_TO_RETURN_POSSESSION:
                return "In attesa di rientro in possesso";

            case RETURN_TO_POSSESSION:
                return "Rientro in possesso";

            case WAITING_FOR_REFUND:
                return "In attesa di rimborso";

            case KASKO:
                return "Kasko";

            case TO_REREGISTER:
                return "Da reimmatricolare";

            case BOOK_DUPLICATION:
                return "Duplicazione libretto";

            case DEMOLITION:
                return "Demolizione";

            case RECEPTIONS_TO_BE_CONFIRMED:
                return "Ricezione da confermare";

            case TO_WORK:
                return "Da lavorare";

            case CLOSED_PARTIAL_REFUND:
                return "Chiudo rimborso parziale";

            case CLOSED_TOTAL_REFUND:
                return "Chiudo rimborso totale";

            case SEND_TO_CLIENT:
                return "Inviato al cliente";

            case DEMOLITION_CLOSED:
                return "Demolizione chiusa";

            case BOOK_DUPLICATION_CLOSED:
                return "Duplicazione libretto chiusa";

            case TO_REREGISTER_CLOSED:
                return "Reimmatricolazione chiusa";

            case TO_SEND_TO_CUSTOMER:
                return "Da inviare al cliente";
            default:
                return "";

        }
    }

    public static ClaimsMyAldResponseV1 adptFromClaimsToMyAldResponse(ClaimsEntity claimsEntity, String userId){
        if(claimsEntity == null)
            return null;

        ClaimsMyAldResponseV1 claimsMyAldResponseV1 = new ClaimsMyAldResponseV1();
        claimsMyAldResponseV1.setId(claimsEntity.getId());
        claimsMyAldResponseV1.setCreatedAt(DateUtil.convertUTCInstantToIS08601String(claimsEntity.getCreatedAt()));
        claimsMyAldResponseV1.setUpdateAt(DateUtil.convertUTCInstantToIS08601String(claimsEntity.getUpdateAt()));
        claimsMyAldResponseV1.setPracticeId(claimsEntity.getPracticeId());
        claimsMyAldResponseV1.setPaiComunication(claimsEntity.getPaiComunication());
        claimsMyAldResponseV1.setForced(claimsEntity.getForced());
        claimsMyAldResponseV1.setInEvidence(claimsEntity.getInEvidence());
        claimsMyAldResponseV1.setStatus(StatusAdapter.adptFromClaimsStatusToMyAldStatus(claimsEntity.getStatus()));
        claimsMyAldResponseV1.setMotivation(claimsEntity.getMotivation());
        claimsMyAldResponseV1.setType(claimsEntity.getType());
        claimsMyAldResponseV1.setComplaint(ComplaintAdapter.adptComplaintToComplaintResponse(claimsEntity.getComplaint()));
        claimsMyAldResponseV1.setDamaged(DamagedAdapter.adptDamagedToDamagedResponse(claimsEntity.getDamaged(), claimsEntity.getAntiTheftRequestEntities()));
        claimsMyAldResponseV1.setCounterparty(CounterpartyAdapter.adptCounterpartyToCounterpartyResponse(claimsEntity.getCounterparts()));
        claimsMyAldResponseV1.setWoundedList(WoundedAdapter.adptWoundedToWoundedResponse(claimsEntity.getWoundedList()));
        claimsMyAldResponseV1.setDeponentList(DeponentAdapter.adptDeponentToDeponentResponse(claimsEntity.getDeponentList()));
        claimsMyAldResponseV1.setForms(FormsAdapter.adptFromToFormsResponse(claimsEntity.getForms()));
        claimsMyAldResponseV1.setFoundModel(FoundModelAdapter.adptFoundModelToFoundModelResponse(claimsEntity.getFoundModel()));
        claimsMyAldResponseV1.setCaiDetails(CaiAdapter.adptCaiToCaiResponse(claimsEntity.getCaiDetails()));
        claimsMyAldResponseV1.setNotes(NotesAdapter.adptNotesToNotesResponse(claimsEntity.getNotes()));
        claimsMyAldResponseV1.setHistorical(HistoricalAdapter.adptHistoricalToHistoricalResponse(claimsEntity.getHistorical()));
        claimsMyAldResponseV1.setWithCounterparty(claimsEntity.getWithCounterparty());
        claimsMyAldResponseV1.setIdSaleforce(claimsEntity.getIdSaleforce());
        claimsMyAldResponseV1.setExemption(ExemptionAdapter.adptExemptionToExemptionResponse(claimsEntity.getExemption()));
        claimsMyAldResponseV1.setRefund(RefundAdapter.adptFromRefundToRefundResponse(claimsEntity.getRefund()));
        claimsMyAldResponseV1.setAuthorityResponseV1List(AuthorityAdapter.adtpFromAuthorityListToAuthorityResponseList(claimsEntity.getAuthorities()));
        claimsMyAldResponseV1.setCompleteDocumentation(claimsEntity.getCompleteDocumentation());
        if(claimsEntity.getMetadata() != null ) {
            claimsMyAldResponseV1.setMetadata(claimsEntity.getMetadata().getMetadata());
        }
        if(claimsEntity.getTheft() != null){
            claimsMyAldResponseV1.setTheft(TheftClaimsAdapter.adptTheftToTheftResponseV1(claimsEntity.getTheft()));
        }
        claimsMyAldResponseV1.setWithContinuation(claimsEntity.getWithContinuation());
        claimsMyAldResponseV1.setMotivation(claimsEntity.getMotivation());
        claimsMyAldResponseV1.setRead(claimsEntity.getRead());
        claimsMyAldResponseV1.setReadAcclaims(claimsEntity.getReadAcclaims());
        claimsMyAldResponseV1.setAuthorityLinkable(claimsEntity.getAuthorityLinkable());
        claimsMyAldResponseV1.setPoVariation(claimsEntity.getPoVariation());


        //settare me/altri utenti/operatore ald
        //rotta per i profili -> https://nemo-dev.aldautomotive.it/permission/api/v1/profiles
        //rotta recuperare i dettagli dell'utente contenuto nel nemo-user-id https://nemo-dev.aldautomotive.it/permission/api/v1/users/9efe432f-9ea5-49d4-badc-8b91b7f77e49

        if(claimsMyAldResponseV1.getMetadata() != null){
            String userIdClaims = null;
            if(claimsMyAldResponseV1.getMetadata().get("user_id") != null) {
                userIdClaims = ((Integer) claimsMyAldResponseV1.getMetadata().get("user_id")).toString();
                if( userId != null && userIdClaims != null){
                    if ( userIdClaims.equals(userId) ){
                        claimsMyAldResponseV1.setInsertedBy(MyAldInsertedByEnum.ME);
                    }else{
                        claimsMyAldResponseV1.setInsertedBy(MyAldInsertedByEnum.OTHER);
                    }
                }
            }else{
                claimsMyAldResponseV1.setInsertedBy(MyAldInsertedByEnum.OTHER);
            }


        }else{
            claimsMyAldResponseV1.setInsertedBy(MyAldInsertedByEnum.ALD_OPERATOR);
        }
        claimsMyAldResponseV1.setCompleteDocumentation(claimsEntity.getCompleteDocumentation());

        return claimsMyAldResponseV1;
    }

    public static List<ClaimsMyAldResponseV1> adptFromClaimsToMyAldResponse(List<ClaimsEntity> claimsEntityList, String userId) {
        List<ClaimsMyAldResponseV1> claimsEntityResponseList = new LinkedList<>();
        for (ClaimsEntity att : claimsEntityList) {
            claimsEntityResponseList.add(adptFromClaimsToMyAldResponse(att,userId));
        }

        return claimsEntityResponseList;
    }




    public static ExportLogResponse adptClaimsToExportLogResponse(ClaimsEntity claimsEntity){

        if(claimsEntity == null)
            return null;

        ExportLogResponse exportLogResponse = new ExportLogResponse();

        exportLogResponse.setId(UUID.fromString(claimsEntity.getId()));
        exportLogResponse.setPracticeId(claimsEntity.getPracticeId());
        exportLogResponse.setClaimsType(claimsEntity.getComplaint().getDataAccident().getTypeAccident());
        exportLogResponse.setFlow(claimsEntity.getType());
        exportLogResponse.setHistoricalList(HistoricalAdapter.adptHistoricalToHistoricalResponse(claimsEntity.getHistorical()));

        return exportLogResponse;
    }

    public static List<ExportLogResponse> adptClaimsToExportLogResponse(List<ClaimsEntity> claimsEntityList){

        if(claimsEntityList == null)
            return null;

        List<ExportLogResponse> exportLogResponseList = new LinkedList<>();

        for (ClaimsEntity att : claimsEntityList)
            exportLogResponseList.add(adptClaimsToExportLogResponse(att));

        return exportLogResponseList;

    }


    //REFACTOR
    public static ClaimsEntity adaptFromNewClaimsEntityToOldClaimsEntityWithoutCounterparty(ClaimsNewEntity claimsNewEntity) {

        if(claimsNewEntity == null){
            return null;
        }
        ClaimsEntity claimsEntity = new ClaimsEntity();
        claimsEntity.setId(claimsNewEntity.getId());
        claimsEntity.setCreatedAt(claimsNewEntity.getCreatedAt());
        claimsEntity.setUpdateAt(claimsNewEntity.getUpdateAt());
        claimsEntity.setPracticeId(claimsNewEntity.getPracticeId());
        claimsEntity.setStatus(claimsNewEntity.getStatus());
        claimsEntity.setPracticeManager(claimsNewEntity.getPracticeManager());
        claimsEntity.setPaiComunication(claimsNewEntity.getPaiComunication());
        claimsEntity.setInEvidence(claimsNewEntity.getInEvidence());
        claimsEntity.setUserId(claimsNewEntity.getUserId());
        claimsEntity.setMotivation(claimsNewEntity.getMotivation());
        claimsEntity.setType(claimsNewEntity.getType());
        claimsEntity.setForced(claimsNewEntity.getForced());
        claimsEntity.setCompleteDocumentation(claimsNewEntity.getCompleteDocumentation());
        claimsEntity.setCaiDetails(claimsNewEntity.getCaiDetails());
        claimsEntity.setWithCounterparty(claimsNewEntity.getWithCounterparty());
        claimsEntity.setDeponentList(claimsNewEntity.getDeponentList());
        claimsEntity.setWoundedList(claimsNewEntity.getWoundedList());
        claimsEntity.setNotes(NotesAdapter.adptNoteEntityListToNotesList(claimsNewEntity.getNotes()));
        claimsEntity.setHistorical(claimsNewEntity.getHistorical());
        claimsEntity.setExemption(claimsNewEntity.getExemption());
        claimsEntity.setIdSaleforce(claimsNewEntity.getIdSaleforce());
        claimsEntity.setWithContinuation(claimsNewEntity.getWithContinuation());
        claimsEntity.setRead(claimsNewEntity.getRead());
        claimsEntity.setAuthorityLinkable(claimsNewEntity.getAuthorityLinkable());
        claimsEntity.setPoVariation(claimsNewEntity.getPoVariation());
        claimsEntity.setLegalComunication(claimsNewEntity.getLegalComunication());
        claimsEntity.setTotalPenalty(claimsNewEntity.getTotalPenalty());
        claimsEntity.setReadAcclaims(claimsNewEntity.getReadAcclaims());
        claimsEntity.setMigrated(claimsNewEntity.getMigrated());
        claimsEntity.setForms(claimsNewEntity.getForms());
        claimsEntity.setAntiTheftRequestEntities(AntiTheftRequestAdapter.adptAntiTheftRequestNewToAntiTheftRequestOldList(claimsNewEntity.getAntiTheftRequestEntities()));

        //COMPLAINT



        Complaint complaint = new Complaint();
        DataAccident dataAccident = new DataAccident();

        complaint.setClientId(claimsNewEntity.getClientId());
        complaint.setActivation(claimsNewEntity.getActivation());
        complaint.setMod(claimsNewEntity.getMod());
        complaint.setLocator(claimsNewEntity.getLocator());
        complaint.setQuote(claimsNewEntity.getQuote());
        complaint.setNotification(claimsNewEntity.getNotification());
        complaint.setProperty(claimsNewEntity.getProperty());
        complaint.setPlate(claimsNewEntity.getPlate());


        dataAccident.setTypeAccident(claimsNewEntity.getTypeAccident());
        dataAccident.setResponsible(claimsNewEntity.getResponsible());
        dataAccident.setDateAccident(DateUtil.convertIS08601StringToUTCDate(DateUtil.convertUTCInstantToIS08601String(claimsNewEntity.getDateAccident())));
        dataAccident.setHappenedAbroad(claimsNewEntity.getHappenedAbroad());
        dataAccident.setDamageToObjects(claimsNewEntity.getDamageToObjects());
        dataAccident.setDamageToVehicles(claimsNewEntity.getDamageToVehicles());
        dataAccident.setOldMotorcyclePlates(claimsNewEntity.getOldMotorcyclePlates());
        dataAccident.setInterventionAuthority(claimsNewEntity.getInterventionAuthority());
        dataAccident.setPolice(claimsNewEntity.getPolice());
        dataAccident.setCc(claimsNewEntity.getCc());
        dataAccident.setVvuu(claimsNewEntity.getVvuu());
        dataAccident.setAuthorityData(claimsNewEntity.getAuthorityData());
        dataAccident.setWitnessDescription(claimsNewEntity.getWitnessDescription());
        dataAccident.setRecoverability(claimsNewEntity.getRecoverability());
        dataAccident.setRecoverabilityPercent(claimsNewEntity.getRecoverabilityPercent());
        dataAccident.setIncompleteMotivation(claimsNewEntity.getIncompleteMotivation());
        dataAccident.setRobbery(claimsNewEntity.getRobbery());
        dataAccident.setCenterNotified(claimsNewEntity.getCenterNotified());
        dataAccident.setHappenedOnCenter(claimsNewEntity.getHappenedOnCenter());
        dataAccident.setProviderCode(claimsNewEntity.getProviderCode());
        dataAccident.setTheftDescription(claimsNewEntity.getTheftDescription());
        dataAccident.setWithCounterparty(claimsNewEntity.getWithCounterparty());

        if(claimsNewEntity.getClaimAddressStreet()!=null || claimsNewEntity.getClaimAddressStreetZip()!=null || claimsNewEntity.getClaimAddressStreetNr()!=null
                || claimsNewEntity.getClaimAddressStreetLocality()!=null || claimsNewEntity.getClaimAddressStreetProvince()!=null || claimsNewEntity.getClaimAddressStreetRegion()!=null
                || claimsNewEntity.getClaimAddressStreetState()!=null || claimsNewEntity.getClaimAddressFormatted()!=null){
            Address addressDataAccident = new Address();
            addressDataAccident.setStreet(claimsNewEntity.getClaimAddressStreet());
            addressDataAccident.setStreetNr(claimsNewEntity.getClaimAddressStreetNr());
            addressDataAccident.setZip(claimsNewEntity.getClaimAddressStreetZip());
            addressDataAccident.setLocality(claimsNewEntity.getClaimAddressStreetLocality());
            addressDataAccident.setProvince(claimsNewEntity.getClaimAddressStreetProvince());
            addressDataAccident.setRegion(claimsNewEntity.getClaimAddressStreetRegion());
            addressDataAccident.setState(claimsNewEntity.getClaimAddressStreetState());
            addressDataAccident.setFormattedAddress(claimsNewEntity.getClaimAddressFormatted());
            dataAccident.setAddress(addressDataAccident);
        }


        complaint.setDataAccident(dataAccident);

        //FROM COMPANY
        if(claimsNewEntity.getClaimsFromCompanyEntity()!=null){
            FromCompany fromCompany  = FromCompanyAdapter.adptFromClaimsFromCompanyToFromClaimsJsonb(claimsNewEntity.getClaimsFromCompanyEntity());
            complaint.setFromCompany(fromCompany);
        }

        //ENTRUSTED
        if(claimsNewEntity.getClaimsEntrustedEntity()!= null){
            Entrusted entrusted  = EntrustedAdapter.adptFromClaimsEntrustedToEntrustedJsonb(claimsNewEntity.getClaimsEntrustedEntity());
            complaint.setEntrusted(entrusted);
        }


        claimsEntity.setComplaint(complaint);


        //DAMAGED
        Damaged damaged = new Damaged();
        damaged.setCaiSigned(claimsNewEntity.getCaiSigned());
        damaged.setImpactPoint(claimsNewEntity.getImpactPoint());
        damaged.setAdditionalCosts(claimsNewEntity.getAdditionalCosts());

        if(claimsNewEntity.getClaimsDamagedContractEntity()!=null){
            Contract contract= ContractAdapter.adptFromContractEntityToClaimsDamagedContractNewEntity(claimsNewEntity.getClaimsDamagedContractEntity());
            damaged.setContract(contract);
        }
        if(claimsNewEntity.getClaimsDamagedCustomerEntity()!=null){
            Customer customer = CustomerAdapter.adptClaimsCustomerToCustomerJsonb(claimsNewEntity.getClaimsDamagedCustomerEntity());
            damaged.setCustomer(customer);

        }
        if(claimsNewEntity.getClaimsDamagedDriverEntity()!=null){
            Driver driver = DriverAdapter.adptfromClaimsDriverEntityToDriverJsonb(claimsNewEntity.getClaimsDamagedDriverEntity());
            damaged.setDriver(driver);

        }
        if(claimsNewEntity.getClaimsDamagedVehicleEntity()!=null){
            Vehicle vehicle = VehicleAdapter.adptFromClaimsVehicleToVehicleJsonb(claimsNewEntity.getClaimsDamagedVehicleEntity());
            if( vehicle != null && claimsNewEntity.getClaimsDamagedVehicleEntity() != null) {
                vehicle.setLicensePlate(claimsNewEntity.getClaimsDamagedVehicleEntity().getLicensePlate());
            }
            damaged.setVehicle(vehicle);
        }
        if(claimsNewEntity.getClaimsDamagedInsuranceInfoEntity()!=null){
            InsuranceCompany insuranceCompany = InsuranceCompanyAdapter.adptClaimsInsuranceCompanyEntityToInsuranceCompanyJsonb(claimsNewEntity.getClaimsDamagedInsuranceInfoEntity());
            damaged.setInsuranceCompany(insuranceCompany);
        }
        if(claimsNewEntity.getClaimsDamagedFleetManagerEntityList()!=null && !claimsNewEntity.getClaimsDamagedFleetManagerEntityList().isEmpty()){
            List<FleetManager> fleetManagerList = FleetManagerAdpter.adptFromClaimsFMListToFMListJsonb(claimsNewEntity.getClaimsDamagedFleetManagerEntityList());
            damaged.setFleetManagerList(fleetManagerList);
        }


        //settare AntitheftService

        if(claimsNewEntity.getAntiTheftServiceEntity() != null || claimsNewEntity.getClaimsDamagedRegistryEntityList()!=null){
            AntiTheftService antiTheftService = AntiTheftServiceAdapter.adptAntiTheftServiceEntityToAntiTheftService(claimsNewEntity.getAntiTheftServiceEntity());
            if(claimsNewEntity.getClaimsDamagedRegistryEntityList() != null && !claimsNewEntity.getClaimsDamagedRegistryEntityList().isEmpty()) {
                antiTheftService.setRegistryList(RegistryAdapter.adptFromClaimsRegistryListToRegistryListJsonb(claimsNewEntity.getClaimsDamagedRegistryEntityList()));
            }
            damaged.setAntiTheftService(antiTheftService);
        }

        claimsEntity.setDamaged(damaged);

        if(claimsNewEntity.getClaimsRefundEntity() != null){
            Refund refund = RefundAdapter.adptClaimsFromRefundToRefundJsonb(claimsNewEntity.getClaimsRefundEntity() );
            claimsEntity.setRefund(refund);
        }



        if(claimsNewEntity.getClaimsTheftEntity() != null){
            Theft theft = TheftClaimsAdapter.adptClaimsTheftToTheftJsonb(claimsNewEntity.getClaimsTheftEntity());
            claimsEntity.setTheft(theft);
        }

        if(claimsNewEntity.getClaimsMetadataEntity() != null){
            claimsEntity.setMetadata(MetadataAdapter.adptFromMetadataEntityToMetadataJsonb(claimsNewEntity.getClaimsMetadataEntity()));
        }


        return claimsEntity;


    }



}
