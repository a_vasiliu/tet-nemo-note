package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.controller.payload.request.insurancecompany.TheftRequest;
import com.doing.nemo.claims.controller.payload.response.insurancecompany.TheftResponse;
import com.doing.nemo.claims.entity.esb.InsuranceCompanyESB.TheftEsb;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbInsuranceCompany.Theft;
import org.springframework.stereotype.Component;

@Component
public class TheftInsuranceCompanyAdapter {

    public static Theft adptTheftRequestToTheft(TheftRequest theftRequest) {

        if (theftRequest != null) {

            Theft theft = new Theft();

            theft.setServiceId(theftRequest.getServiceId());
            theft.setService(theftRequest.getService());
            theft.setDeductibleId(theftRequest.getDeductibleId());
            theft.setDeductible(theftRequest.getDeductible());

            return theft;
        }
        return null;
    }

    public static TheftResponse adptTheftToTheftResponse(Theft theft) {

        if (theft != null) {

            TheftResponse theftResponse = new TheftResponse();

            theftResponse.setServiceId(theft.getServiceId());
            theftResponse.setService(theft.getService());
            theftResponse.setDeductibleId(theft.getDeductibleId());
            theftResponse.setDeductible(theft.getDeductible());

            return theftResponse;
        }
        return null;
    }

    public static Theft adptTheftResponseToTheft(TheftResponse theftResponse) {

        if (theftResponse != null) {

            Theft theft = new Theft();

            theft.setServiceId(theftResponse.getServiceId());
            theft.setService(theftResponse.getService());
            theft.setDeductibleId(theftResponse.getDeductibleId());
            theft.setDeductible(theftResponse.getDeductible());

            return theft;
        }
        return null;
    }

    public static TheftResponse adptTheftEsbToTheftResponse(TheftEsb theftEsb) {

        if (theftEsb != null) {

            TheftResponse theftResponse = new TheftResponse();

            theftResponse.setServiceId(theftEsb.getServiceId());
            theftResponse.setService(theftEsb.getService());
            theftResponse.setDeductibleId(theftEsb.getDeductibleId());
            theftResponse.setDeductible(theftEsb.getDeductible());

            return theftResponse;
        }
        return null;
    }
}
