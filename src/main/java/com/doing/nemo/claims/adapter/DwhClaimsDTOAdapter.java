package com.doing.nemo.claims.adapter;


import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.dto.DwhClaimsDTO;
import com.doing.nemo.claims.entity.AntiTheftRequestEntity;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbAntiTheftService.AntiTheftRequest;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

@Component
public class DwhClaimsDTOAdapter {


    public static AntiTheftRequest adptFromAntiTheftRequestEntityToAntiTheftRequestJsonb(AntiTheftRequestEntity antiTheftRequestEntity){
        if( antiTheftRequestEntity != null){
            return null;
        }

        AntiTheftRequest antiTheftRequest = new AntiTheftRequest();
        antiTheftRequest.setResponseMessage(antiTheftRequestEntity.getResponseMessage());
        antiTheftRequest.setResponse(antiTheftRequestEntity.getUpdatedAt());
        antiTheftRequest.setPdf(antiTheftRequestEntity.getPdf());
        antiTheftRequest.setAnomaly(antiTheftRequestEntity.getAnomaly());
        antiTheftRequest.setResponseType(antiTheftRequestEntity.getResponseType());
        antiTheftRequest.setRequest(antiTheftRequestEntity.getCreatedAt());
        antiTheftRequest.setCrashNumber(antiTheftRequestEntity.getCrashNumber());
        antiTheftRequest.setgPower(antiTheftRequestEntity.getgPower());
        antiTheftRequest.setOutcome(antiTheftRequestEntity.getOutcome());
        antiTheftRequest.setIdTransaction(antiTheftRequestEntity.getIdTransaction());
        antiTheftRequest.setIdWebSin(antiTheftRequestEntity.getPracticeId());
        antiTheftRequest.setSubReport(antiTheftRequestEntity.getSubReport());
        antiTheftRequest.setProviderType(antiTheftRequestEntity.getProviderType());

        return antiTheftRequest;

    }

    public static List<AntiTheftRequest> adptFromAntiTheftReqtEntityListToAntiTheftReqJsonbList(List<AntiTheftRequestEntity> antiTheftRequestEntityList){
        if(antiTheftRequestEntityList != null){
            return null;
        }

        List<AntiTheftRequest> antiTheftRequestList = new LinkedList<>();
        for(AntiTheftRequestEntity currentRequest : antiTheftRequestEntityList){
            antiTheftRequestList.add(adptFromAntiTheftRequestEntityToAntiTheftRequestJsonb(currentRequest));
        }

        return antiTheftRequestList;
    }


    public static DwhClaimsDTO adptFromClaimsEntityToDwhClaimsDTO(ClaimsEntity claimsEntity){
        if(claimsEntity == null) return null;
        DwhClaimsDTO dwhClaimsDTO = new DwhClaimsDTO();
        dwhClaimsDTO.setId(claimsEntity.getId());
        dwhClaimsDTO.setCreatedAt(DateUtil.convertIS08601StringToUTCDate(DateUtil.convertUTCInstantToIS08601String(claimsEntity.getCreatedAt())));
        dwhClaimsDTO.setUpdateAt(DateUtil.convertIS08601StringToUTCDate(DateUtil.convertUTCInstantToIS08601String(claimsEntity.getUpdateAt())));
        dwhClaimsDTO.setPracticeId(claimsEntity.getPracticeId());
        dwhClaimsDTO.setPracticeManager(claimsEntity.getPracticeManager());
        dwhClaimsDTO.setPaiComunication(claimsEntity.getPaiComunication());
        dwhClaimsDTO.setForced(claimsEntity.getForced());
        dwhClaimsDTO.setInEvidence(claimsEntity.getInEvidence());
        dwhClaimsDTO.setUserId(claimsEntity.getUserId());
        dwhClaimsDTO.setStatus(claimsEntity.getStatus());
        dwhClaimsDTO.setMotivation(claimsEntity.getMotivation());
        dwhClaimsDTO.setType(claimsEntity.getType());
        dwhClaimsDTO.setCompleteDocumentation(claimsEntity.getCompleteDocumentation());
        dwhClaimsDTO.setComplaint(claimsEntity.getComplaint());
        dwhClaimsDTO.setForms(claimsEntity.getForms());
        dwhClaimsDTO.setDamaged(claimsEntity.getDamaged());
        if(dwhClaimsDTO.getDamaged() != null && dwhClaimsDTO.getDamaged().getAntiTheftService()!= null){
            dwhClaimsDTO.getDamaged().getAntiTheftService().setAntiTheftList(adptFromAntiTheftReqtEntityListToAntiTheftReqJsonbList(claimsEntity.getAntiTheftRequestEntities()));
        }
        dwhClaimsDTO.setCaiDetails(claimsEntity.getCaiDetails());
        dwhClaimsDTO.setWithContinuation(claimsEntity.getWithContinuation());
        dwhClaimsDTO.setWithCounterparty(claimsEntity.getWithCounterparty());
        //dwhClaimsDTO.setCounterparty(claimsEntity.getCounterparts());
        dwhClaimsDTO.setDeponentList(claimsEntity.getDeponentList());
        dwhClaimsDTO.setWoundedList(claimsEntity.getWoundedList());
        dwhClaimsDTO.setNotes(claimsEntity.getNotes());
        dwhClaimsDTO.setHistorical(claimsEntity.getHistorical());
        dwhClaimsDTO.setFoundModel(claimsEntity.getFoundModel());
        dwhClaimsDTO.setExemption(claimsEntity.getExemption());
        dwhClaimsDTO.setIdSaleforce(claimsEntity.getIdSaleforce());
        dwhClaimsDTO.setRefund(claimsEntity.getRefund());
        dwhClaimsDTO.setMetadata(claimsEntity.getMetadata());
        dwhClaimsDTO.setTheft(claimsEntity.getTheft());
        dwhClaimsDTO.setRead(claimsEntity.getRead());
        dwhClaimsDTO.setAuthorities(claimsEntity.getAuthorities());
        dwhClaimsDTO.setAuthorityLinkable(claimsEntity.getAuthorityLinkable());
        dwhClaimsDTO.setPoVariation(claimsEntity.getPoVariation());
        dwhClaimsDTO.setLegalComunication(claimsEntity.getLegalComunication());
        dwhClaimsDTO.setTotalPenalty(claimsEntity.getTotalPenalty());

        return dwhClaimsDTO;

    }






}
