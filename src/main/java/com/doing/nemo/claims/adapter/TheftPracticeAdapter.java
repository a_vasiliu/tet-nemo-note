package com.doing.nemo.claims.adapter;


import com.doing.nemo.claims.controller.payload.request.practice.TheftPracticeRequestV1;
import com.doing.nemo.claims.controller.payload.response.practice.TheftPracticeResponseV1;
import com.doing.nemo.claims.entity.jsonb.Address;
import com.doing.nemo.claims.entity.jsonb.Theft;
import com.doing.nemo.claims.entity.jsonb.practice.TheftPractice;
import com.doing.nemo.claims.util.Util;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class TheftPracticeAdapter {


    public static TheftPractice fromTheftPracticeResponseV1ToTheft(TheftPracticeRequestV1 theftPracticeRequestV1){
        if(theftPracticeRequestV1  == null ){
            return null;
        }

        TheftPractice theft = new TheftPractice();
        theft.setAuthorityData(theftPracticeRequestV1.getAuthorityData());
        theft.setComplaintCc(theftPracticeRequestV1.getComplaintCc());
        theft.setComplaintGdf(theftPracticeRequestV1.getComplaintGdf());
        theft.setComplaintPolice(theftPracticeRequestV1.getComplaintPolice());
        theft.setComplaintVvuu(theftPracticeRequestV1.getComplaintVvuu());
        theft.setHappenedAbroad(theftPracticeRequestV1.getHappenedAbroad());
        theft.setNote(theftPracticeRequestV1.getNote());
        theft.setProvince(theftPracticeRequestV1.getProvince());
        theft.setTheftDate(theftPracticeRequestV1.getTheftDate());
        theft.setTheftId(theftPracticeRequestV1.getTheftId());
        theft.setTheftLocation(theftPracticeRequestV1.getTheftLocation());
        theft.setState(theftPracticeRequestV1.getState());

        return theft;
    }

    public static TheftPracticeResponseV1 adtpFromTheftPracticeToTheftPracticeResponseV1(TheftPractice theft){
        if(theft == null){
            return null;
        }

        TheftPracticeResponseV1 theftResponse = new TheftPracticeResponseV1();


        theftResponse.setAuthorityData(theft.getAuthorityData());
        theftResponse.setComplaintCc(theft.getComplaintCc());
        theftResponse.setComplaintGdf(theft.getComplaintGdf());
        theftResponse.setComplaintPolice(theft.getComplaintPolice());
        theftResponse.setComplaintVvuu(theft.getComplaintVvuu());
        theftResponse.setHappenedAbroad(theft.getHappenedAbroad());
        theftResponse.setNote(theft.getNote());
        theftResponse.setProvince(theft.getProvince());
        theftResponse.setTheftDate(theft.getTheftDate());
        theftResponse.setTheftId(theft.getTheftId());
        theftResponse.setTheftLocation(theft.getTheftLocation());
        theftResponse.setState(theft.getState());

        return theftResponse;

    }


    public static TheftPractice adtpFromTheftClaimsToTheftPractice(Theft theft, Date theftDate, Address address, Long practiceId){
        if(theft == null){
            return null;
        }

        TheftPractice theftPractice = new TheftPractice();
        theftPractice.setTheftId(practiceId.toString());
        theftPractice.setAuthorityData(theft.getAuthorityData());
        theftPractice.setComplaintCc(theft.getComplaintAuthorityCc());
        theftPractice.setComplaintGdf(false);
        theftPractice.setComplaintPolice(theft.getComplaintAuthorityPolice());
        theftPractice.setComplaintVvuu(theft.getComplaintAuthorityVvuu());
        theftPractice.setHappenedAbroad(theft.getFoundAbroad());
        theftPractice.setNote(theft.getTheftNotes());
        if(address!=null){
            theftPractice.setProvince(address.getProvince());
            theftPractice.setState(address.getState());
            theftPractice.setTheftLocation(Util.replaceNull(address.getStreet()) +" "+ Util.replaceNull(address.getStreetNr())+" , "+Util.replaceNull(address.getZip())+" "+Util.replaceNull(address.getLocality())+" "+Util.replaceNull(address.getProvince())+" "+Util.replaceNull(address.getRegion())+" "+Util.replaceNull(address.getState()));
        }
        theftPractice.setTheftDate(theftDate);


        return theftPractice;

    }




}


