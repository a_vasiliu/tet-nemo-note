package com.doing.nemo.claims.adapter;


import com.doing.nemo.claims.controller.payload.request.InsuranceCompanyRequestV1;
import com.doing.nemo.claims.controller.payload.request.damaged.InsuranceCompanyRequest;
import com.doing.nemo.claims.controller.payload.request.insurancecompany.*;
import com.doing.nemo.claims.controller.payload.response.ClaimsResponsePaginationV1;
import com.doing.nemo.claims.controller.payload.response.InsuranceCompanyExternalResponseV1;
import com.doing.nemo.claims.controller.payload.response.InsuranceCompanyResponseV1;
import com.doing.nemo.claims.controller.payload.response.damaged.InsuranceCompanyResponse;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.claims.ClaimsDamagedInsuranceInfoEntity;
import com.doing.nemo.claims.entity.esb.InsuranceCompanyESB.InsuranceCompanyEsb;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.InsuranceCompany;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbInsuranceCompany.*;
import com.doing.nemo.claims.entity.settings.InsuranceCompanyEntity;
import com.doing.nemo.claims.entity.settings.LegalEntity;
import com.doing.nemo.claims.exception.NotFoundException;
import com.doing.nemo.claims.repository.InsuranceCompanyRepository;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import com.doing.nemo.middleware.client.payload.response.MiddlewareContractInsuranceDetailResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
public class InsuranceCompanyAdapter {

    @Autowired
    private InsuranceCompanyRepository insuranceCompanyRepository;

    private static Logger LOGGER = LoggerFactory.getLogger(InsuranceCompanyAdapter.class);

    public static InsuranceCompany adptInsuranceCompanyRequestToInsuranceCompany(InsuranceCompanyRequest insuranceCompanyRequest) {

        if (insuranceCompanyRequest != null) {

            InsuranceCompany insuranceCompany = new InsuranceCompany();
            //insuranceCompany.setDenomination(insuranceCompanyRequest.getDenomination());
            insuranceCompany.setCard(insuranceCompanyRequest.getCard());
            insuranceCompany.setId(insuranceCompanyRequest.getId());
            insuranceCompany.setTpl(TplAdapter.adptTplRequestToTpl(insuranceCompanyRequest.getTpl()));
            insuranceCompany.setTheft(TheftInsuranceCompanyAdapter.adptTheftRequestToTheft(insuranceCompanyRequest.getTheft()));
            insuranceCompany.setMaterialDamage(MaterialDamageAdapter.adptMaterialDamageRequestToMaterialDamage(insuranceCompanyRequest.getMaterialDamage()));
            insuranceCompany.setPai(PaiAdapter.adptPaiRequestToPai(insuranceCompanyRequest.getPai()));
            insuranceCompany.setLegalCost(LegalCostAdapter.adptLegalCostRequestToLegalCost(insuranceCompanyRequest.getLegalCost()));
            insuranceCompany.setKasko(KaskoAdapter.adptFromKaskoRequestToKasko(insuranceCompanyRequest.getKasko()));

            return insuranceCompany;
        }

        return null;
    }


    public static InsuranceCompanyResponse adptInsuranceCompanyToInsuranceCompanyResponse(InsuranceCompany insuranceCompany) {

        if (insuranceCompany != null) {

            InsuranceCompanyResponse insuranceCompanyResponse = new InsuranceCompanyResponse();

            //insuranceCompanyResponse.setDenomination(insuranceCompany.getDenomination());
            insuranceCompanyResponse.setCard(insuranceCompany.getCard());
            insuranceCompanyResponse.setId(insuranceCompany.getId());
            insuranceCompanyResponse.setTpl(TplAdapter.adptTplToTplResponse(insuranceCompany.getTpl()));
            insuranceCompanyResponse.setTheft(TheftInsuranceCompanyAdapter.adptTheftToTheftResponse(insuranceCompany.getTheft()));
            insuranceCompanyResponse.setMaterialDamage(MaterialDamageAdapter.adptMaterialDamageToMaterialDamageResponse(insuranceCompany.getMaterialDamage()));
            insuranceCompanyResponse.setPai(PaiAdapter.adptPaiToPaiResponse(insuranceCompany.getPai()));
            insuranceCompanyResponse.setLegalCost(LegalCostAdapter.adptLegalCostToLegalCostResponse(insuranceCompany.getLegalCost()));
            insuranceCompanyResponse.setKasko(KaskoAdapter.adptFromKaskoToKaskoResponse(insuranceCompany.getKasko()));

            return insuranceCompanyResponse;
        }
        return null;
    }

    public static InsuranceCompany adptInsuranceCompanyResponseToInsuranceCompany(InsuranceCompanyResponse insuranceCompany) {

        if (insuranceCompany != null) {

            InsuranceCompany insuranceCompanyResponse = new InsuranceCompany();

            //insuranceCompanyResponse.setDenomination(insuranceCompany.getDenomination());
            insuranceCompanyResponse.setCard(insuranceCompany.getCard());
            insuranceCompanyResponse.setId(insuranceCompany.getId());
            insuranceCompanyResponse.setTpl(TplAdapter.adptTplResponseToTpl(insuranceCompany.getTpl()));
            insuranceCompanyResponse.setTheft(TheftInsuranceCompanyAdapter.adptTheftResponseToTheft(insuranceCompany.getTheft()));
            insuranceCompanyResponse.setMaterialDamage(MaterialDamageAdapter.adptMaterialDamageResponseToMaterialDamage(insuranceCompany.getMaterialDamage()));
            insuranceCompanyResponse.setPai(PaiAdapter.adptPaiResponseToPai(insuranceCompany.getPai()));
            insuranceCompanyResponse.setLegalCost(LegalCostAdapter.adptLegalCostResponseToLegalCost(insuranceCompany.getLegalCost()));
            return insuranceCompanyResponse;
        }
        return null;
    }

    public static InsuranceCompanyResponse adptInsuranceCompanyEsbToInsuranceCompanyResponse(InsuranceCompanyEsb insuranceCompanyEsb) {

        if (insuranceCompanyEsb != null) {

            InsuranceCompanyResponse insuranceCompanyResponse = new InsuranceCompanyResponse();

            insuranceCompanyResponse.setId(insuranceCompanyEsb.getId());
            insuranceCompanyResponse.setTpl(TplAdapter.adptTplESBToTplResponse(insuranceCompanyEsb.getTpl()));
            insuranceCompanyResponse.setTheft(TheftInsuranceCompanyAdapter.adptTheftEsbToTheftResponse(insuranceCompanyEsb.getTheft()));
            insuranceCompanyResponse.setMaterialDamage(MaterialDamageAdapter.adptMaterialDamageEsbToMaterialDamageResponse(insuranceCompanyEsb.getMaterialDamage()));
            insuranceCompanyResponse.setPai(PaiAdapter.adptPaiEsbToPaiResponse(insuranceCompanyEsb.getPai()));
            insuranceCompanyResponse.setLegalCost(LegalCostAdapter.adptLegalCostEsbToLegalCostResponse(insuranceCompanyEsb.getLegalCost()));

            return insuranceCompanyResponse;
        }
        return null;
    }

    public InsuranceCompanyEntity adptFromICompRequToICompEntityWithID(InsuranceCompanyRequestV1 insuranceCompanyRequest, UUID uuid) {
        Optional<InsuranceCompanyEntity> insuranceCompanyEntity1 = insuranceCompanyRepository.findById(uuid);
        if (!insuranceCompanyEntity1.isPresent()) {
            LOGGER.debug("Insurance Company not found");
            throw new NotFoundException("Insurance Company not found", com.doing.nemo.claims.validation.MessageCode.CLAIMS_1010);
        }
        InsuranceCompanyEntity insuranceCompanyEntity = insuranceCompanyEntity1.get();
        InsuranceCompanyEntity insuranceCompanyEntity2 = adptFromICompRequToICompEntity(insuranceCompanyRequest);
        insuranceCompanyEntity2.setId(insuranceCompanyEntity.getId());
        return insuranceCompanyEntity2;
    }


    public static InsuranceCompanyEntity adptFromICompRequToICompEntity(InsuranceCompanyRequestV1 insuranceCompanyRequest) {
        InsuranceCompanyEntity insuranceCompanyEntity = new InsuranceCompanyEntity();
        insuranceCompanyEntity.setName(insuranceCompanyRequest.getName());
        insuranceCompanyEntity.setAddress(insuranceCompanyRequest.getAddress());
        insuranceCompanyEntity.setZipCode(insuranceCompanyRequest.getZipCode());
        insuranceCompanyEntity.setCity(insuranceCompanyRequest.getCity());
        insuranceCompanyEntity.setProvince(insuranceCompanyRequest.getProvince());
        insuranceCompanyEntity.setState(insuranceCompanyRequest.getState());
        insuranceCompanyEntity.setTelephone(insuranceCompanyRequest.getTelephone());
        insuranceCompanyEntity.setFax(insuranceCompanyRequest.getFax());
        insuranceCompanyEntity.setEmail(insuranceCompanyRequest.getEmail());
        insuranceCompanyEntity.setWebSite(insuranceCompanyRequest.getWebSite());
        insuranceCompanyEntity.setContact(insuranceCompanyRequest.getContact());
        insuranceCompanyEntity.setAniaCode(insuranceCompanyRequest.getAniaCode());
        insuranceCompanyEntity.setJoinCard(insuranceCompanyRequest.getJoinCard());
        insuranceCompanyEntity.setContactPai(insuranceCompanyRequest.getContactPai());
        insuranceCompanyEntity.setEmailPai(insuranceCompanyRequest.getEmailPai());
        insuranceCompanyEntity.setActive(insuranceCompanyRequest.getActive());

        return insuranceCompanyEntity;
    }


    public static InsuranceCompanyEntity adptFromICompRequToICompEntityWithId(InsuranceCompanyRequestV1 insuranceCompanyRequest) {
        if(insuranceCompanyRequest == null) return null;
        InsuranceCompanyEntity insuranceCompanyEntity = new InsuranceCompanyEntity();
        insuranceCompanyEntity.setId(insuranceCompanyRequest.getId());
        insuranceCompanyEntity.setName(insuranceCompanyRequest.getName());
        insuranceCompanyEntity.setAddress(insuranceCompanyRequest.getAddress());
        insuranceCompanyEntity.setZipCode(insuranceCompanyRequest.getZipCode());
        insuranceCompanyEntity.setCity(insuranceCompanyRequest.getCity());
        insuranceCompanyEntity.setProvince(insuranceCompanyRequest.getProvince());
        insuranceCompanyEntity.setState(insuranceCompanyRequest.getState());
        insuranceCompanyEntity.setTelephone(insuranceCompanyRequest.getTelephone());
        insuranceCompanyEntity.setFax(insuranceCompanyRequest.getFax());
        insuranceCompanyEntity.setEmail(insuranceCompanyRequest.getEmail());
        insuranceCompanyEntity.setWebSite(insuranceCompanyRequest.getWebSite());
        insuranceCompanyEntity.setContact(insuranceCompanyRequest.getContact());
        insuranceCompanyEntity.setAniaCode(insuranceCompanyRequest.getAniaCode());
        insuranceCompanyEntity.setJoinCard(insuranceCompanyRequest.getJoinCard());
        insuranceCompanyEntity.setContactPai(insuranceCompanyRequest.getContactPai());
        insuranceCompanyEntity.setEmailPai(insuranceCompanyRequest.getEmailPai());
        insuranceCompanyEntity.setActive(insuranceCompanyRequest.getActive());

        return insuranceCompanyEntity;
    }

    public static InsuranceCompanyResponseV1 adptFromICompEntityToICompResp(InsuranceCompanyEntity insuranceCompanyEntity) {
        if(insuranceCompanyEntity == null) return null;
        InsuranceCompanyResponseV1<InsuranceCompanyEntity> responseV1 = new InsuranceCompanyResponseV1<>();

        responseV1.setId(insuranceCompanyEntity.getId());
        responseV1.setName(insuranceCompanyEntity.getName());
        responseV1.setAddress(insuranceCompanyEntity.getAddress());
        responseV1.setZipCode(insuranceCompanyEntity.getZipCode());
        responseV1.setCity(insuranceCompanyEntity.getCity());
        responseV1.setProvince(insuranceCompanyEntity.getProvince());
        responseV1.setState(insuranceCompanyEntity.getState());
        responseV1.setTelephone(insuranceCompanyEntity.getTelephone());
        responseV1.setFax(insuranceCompanyEntity.getFax());
        responseV1.setEmail(insuranceCompanyEntity.getEmail());
        responseV1.setWebSite(insuranceCompanyEntity.getWebSite());
        responseV1.setContact(insuranceCompanyEntity.getContact());
        responseV1.setAniaCode(insuranceCompanyEntity.getAniaCode());
        responseV1.setJoinCard(insuranceCompanyEntity.getJoinCard());
        responseV1.setContactPai(insuranceCompanyEntity.getContactPai());
        responseV1.setEmailPai(insuranceCompanyEntity.getEmailPai());
        responseV1.setActive(insuranceCompanyEntity.getActive());
        responseV1.setCode(insuranceCompanyEntity.getCode());

        return responseV1;
    }

    public static List<InsuranceCompanyResponseV1> adptFromICompEntityToICompRespList(List<InsuranceCompanyEntity> insuranceCompanyEntityList) {
        List<InsuranceCompanyResponseV1> responseV1 = new LinkedList<>();
        for (InsuranceCompanyEntity insuranceCompanyEntity : insuranceCompanyEntityList) {
            responseV1.add(adptFromICompEntityToICompResp(insuranceCompanyEntity));
        }
        return responseV1;
    }


    public static List<InsuranceCompanyEntity> adptFromICompRequestToICompEntityList(List<InsuranceCompanyRequestV1> insuranceCompanyRequestList) {
        List<InsuranceCompanyEntity> requestV1 = new LinkedList<>();
        for (InsuranceCompanyRequestV1 insuranceCompanyRequestV1 : insuranceCompanyRequestList) {
            requestV1.add(adptFromICompRequToICompEntityWithId(insuranceCompanyRequestV1));
        }
        return requestV1;
    }

    public static ClaimsResponsePaginationV1 adptInsuranceCompanyPaginationToClaimsResponsePagination(Pagination<InsuranceCompanyEntity> pagination) {

        ClaimsResponsePaginationV1<InsuranceCompanyResponseV1> claimsPaginationResponse = new ClaimsResponsePaginationV1<>();

        claimsPaginationResponse.setStats(pagination.getStats());
        claimsPaginationResponse.setItems(adptFromICompEntityToICompRespList(pagination.getItems()));

        return claimsPaginationResponse;
    }


    public static InsuranceCompanyExternalResponseV1 adptFromICompEntityToICompRespExternal(InsuranceCompanyEntity insuranceCompanyEntity) {
        if(insuranceCompanyEntity == null) return null;
        InsuranceCompanyExternalResponseV1 responseV1 = new InsuranceCompanyExternalResponseV1();

        responseV1.setId(insuranceCompanyEntity.getId());
        responseV1.setName(insuranceCompanyEntity.getName());
        responseV1.setAniaCode(insuranceCompanyEntity.getAniaCode());


        return responseV1;
    }

    public static List<InsuranceCompanyExternalResponseV1> adptFromICompEntityToICompRespListExternal(List<InsuranceCompanyEntity> insuranceCompanyEntityList) {
        List<InsuranceCompanyExternalResponseV1> responseV1 = new LinkedList<>();
        for (InsuranceCompanyEntity insuranceCompanyEntity : insuranceCompanyEntityList) {
            responseV1.add(adptFromICompEntityToICompRespExternal(insuranceCompanyEntity));
        }
        return responseV1;
    }



    public static InsuranceCompanyResponse adptMiddlewareContractInsuranceDetailResponseToInsuranceCompanyResponse(MiddlewareContractInsuranceDetailResponse insuranceCompanyEsb){

        if (insuranceCompanyEsb != null) {

            InsuranceCompanyResponse insuranceCompanyResponse = new InsuranceCompanyResponse();
            insuranceCompanyResponse.setTpl(TplAdapter.adptTplMiddlewareToTplResponse(insuranceCompanyEsb.getTpl()));
            insuranceCompanyResponse.setPai(PaiAdapter.adptPaiMiddlewareToPaiResponse(insuranceCompanyEsb.getPai()));
            insuranceCompanyResponse.setLegalCost(LegalCostAdapter.adptLegalCostMiddlewareToLegalCostResponse(insuranceCompanyEsb.getLegalCost()));

            return insuranceCompanyResponse;
        }
        return null;
    }


    //NEW ADAPTER


    public static ClaimsDamagedInsuranceInfoEntity adptInsuranceCompanyEntityToClDamInsuranceNewCompany(InsuranceCompany insuranceCompanyJsonb) {

        if (insuranceCompanyJsonb != null) {

            ClaimsDamagedInsuranceInfoEntity insuranceCompany = new ClaimsDamagedInsuranceInfoEntity();
            insuranceCompany.setCard(insuranceCompanyJsonb.getCard());
            insuranceCompany.setInsuranceInfoId(insuranceCompanyJsonb.getId());
            if(insuranceCompanyJsonb.getTpl()!=null){
                Tpl tlp = insuranceCompanyJsonb.getTpl();
                insuranceCompany.setTplServiceId(tlp.getServiceId());
                insuranceCompany.setTplService(tlp.getService());
                insuranceCompany.setTplMaxCoverage(tlp.getMaxCoverage());
                insuranceCompany.setTplDeductible(tlp.getDeductible());
                insuranceCompany.setTplCompanyId(tlp.getCompanyId());
                insuranceCompany.setTplCompany(tlp.getCompany());
                insuranceCompany.setTplTariffId(tlp.getTariffId());
                insuranceCompany.setTplTariff(tlp.getTariff());
                insuranceCompany.setTplTariffCode(tlp.getTariffCode());
                insuranceCompany.setTplStartDate(tlp.getStartDate());
                insuranceCompany.setTplEndDate(tlp.getEndDate());
                insuranceCompany.setTplPolicyNumber(tlp.getPolicyNumber());
                insuranceCompany.setTplCompanyDescription(tlp.getCompanyDescription());
                insuranceCompany.setTplInsuranceCompanyId(tlp.getInsuranceCompanyId());
                insuranceCompany.setTplInsurancePolicyId(tlp.getInsurancePolicyId());
            }
            if(insuranceCompanyJsonb.getTheft()!=null){
                Theft theft = insuranceCompanyJsonb.getTheft();
                insuranceCompany.setTheftServiceId(theft.getServiceId());
                insuranceCompany.setTheftService(theft.getService());
                insuranceCompany.setTheftDeductible(theft.getDeductible());
                insuranceCompany.setTheftDeductibleId(theft.getDeductibleId());
            }
            if(insuranceCompanyJsonb.getPai()!=null){
                Pai pai = insuranceCompanyJsonb.getPai();
                insuranceCompany.setPaiServiceId(pai.getServiceId());
                insuranceCompany.setPaiService(pai.getService());
                insuranceCompany.setPaiMaxCoverage(pai.getMaxCoverage());
                insuranceCompany.setPaiDeductible(pai.getDeductible());
                insuranceCompany.setPaiDeductibleId(pai.getDeductibleId());
                insuranceCompany.setPaiCompanyId(pai.getCompanyId());
                insuranceCompany.setPaiCompany(pai.getCompany());
                insuranceCompany.setPaiTariffId(pai.getTariffId());
                insuranceCompany.setPaiTariff(pai.getTariff());
                insuranceCompany.setPaiTariffCode(pai.getTariffCode());
                insuranceCompany.setPaiStartDate(pai.getStartDate());
                insuranceCompany.setPaiEndDate(pai.getEndDate());
                insuranceCompany.setPaiPolicyNumber(pai.getPolicyNumber());
                insuranceCompany.setPaiInsuranceCompanyId(pai.getInsuranceCompanyId());
                insuranceCompany.setPaiInsurancePolicyId(pai.getInsurancePolicyId());
                insuranceCompany.setPaiPercentage(pai.getPercentage());
                insuranceCompany.setPaiMedicalCosts(pai.getMedicalCosts());
            }
            if(insuranceCompanyJsonb.getLegalCost()!=null){
                LegalCost legalCost = insuranceCompanyJsonb.getLegalCost();
                insuranceCompany.setLegalCostServiceId(legalCost.getServiceId());
                insuranceCompany.setLegalCostService(legalCost.getService());
                insuranceCompany.setLegalCostCompanyId(legalCost.getCompanyId());
                insuranceCompany.setLegalCostCompany(legalCost.getCompany());
                insuranceCompany.setLegalCostTariffId(legalCost.getTariffId());
                insuranceCompany.setLegalCostTariff(legalCost.getTariff());
                insuranceCompany.setLegalCostTariffCode(legalCost.getTariffCode());
                insuranceCompany.setLegalCostStartDate(legalCost.getStartDate());
                insuranceCompany.setLegalCostEndDate(legalCost.getEndDate());
                insuranceCompany.setLegalCostPolicyNumber(legalCost.getPolicyNumber());
                insuranceCompany.setLegalCostInsuranceCompanyId(legalCost.getInsuranceCompanyId());
                insuranceCompany.setLegalCostInsurancePolicyId(legalCost.getInsurancePolicyId());
            }
            if(insuranceCompanyJsonb.getKasko()!=null){
                Kasko kasko = insuranceCompanyJsonb.getKasko();
                insuranceCompany.setKaskoDeductibleId(kasko.getDeductibleId());
                insuranceCompany.setKaskoDeductibleValue(kasko.getDeductibleValue());
            }
            return insuranceCompany;
        }

        return null;
    }

    public static InsuranceCompany adptClaimsInsuranceCompanyEntityToInsuranceCompanyJsonb(ClaimsDamagedInsuranceInfoEntity claimsInsuranceCompany) {

        if (claimsInsuranceCompany != null) {

            InsuranceCompany insuranceCompany = new InsuranceCompany();

            insuranceCompany.setCard(claimsInsuranceCompany.getCard());
            insuranceCompany.setId(claimsInsuranceCompany.getInsuranceInfoId());


            if(claimsInsuranceCompany.getTplServiceId()!=null || claimsInsuranceCompany.getTplService()!=null ||
                    claimsInsuranceCompany.getTplMaxCoverage()!=null || claimsInsuranceCompany.getTplDeductible()!=null
            ||claimsInsuranceCompany.getTplCompanyId()!=null || claimsInsuranceCompany.getTplCompany()!=null || claimsInsuranceCompany.getTplTariffId()!=null
            || claimsInsuranceCompany.getTplTariff()!=null || claimsInsuranceCompany.getTplTariffCode()!=null || claimsInsuranceCompany.getTplStartDate()!=null
            || claimsInsuranceCompany.getTplEndDate()!=null || claimsInsuranceCompany.getTplPolicyNumber()!=null || claimsInsuranceCompany.getTplCompanyDescription()!=null
            || claimsInsuranceCompany.getTplInsuranceCompanyId()!=null || claimsInsuranceCompany.getTplInsurancePolicyId()!=null ){
                Tpl tpl = new Tpl();
                tpl.setServiceId(claimsInsuranceCompany.getTplServiceId());
                tpl.setService(claimsInsuranceCompany.getTplService());
                tpl.setMaxCoverage(claimsInsuranceCompany.getTplMaxCoverage());
                tpl.setDeductible(claimsInsuranceCompany.getTplDeductible());
                tpl.setCompanyId(claimsInsuranceCompany.getTplCompanyId());
                tpl.setCompany(claimsInsuranceCompany.getTplCompany());
                tpl.setTariffId(claimsInsuranceCompany.getTplTariffId());
                tpl.setTariff(claimsInsuranceCompany.getTplTariff());
                tpl.setTariffCode(claimsInsuranceCompany.getTplTariffCode());
                tpl.setStartDate(claimsInsuranceCompany.getTplStartDate());
                tpl.setEndDate(claimsInsuranceCompany.getTplEndDate());
                tpl.setPolicyNumber(claimsInsuranceCompany.getTplPolicyNumber());
                tpl.setCompanyDescription(claimsInsuranceCompany.getTplCompanyDescription());
                tpl.setInsuranceCompanyId(claimsInsuranceCompany.getTplInsuranceCompanyId());
                tpl.setInsurancePolicyId(claimsInsuranceCompany.getTplInsurancePolicyId());
                insuranceCompany.setTpl(tpl);
            }
            
            if(claimsInsuranceCompany.getTheftDeductible()!=null || claimsInsuranceCompany.getTheftDeductibleId()!=null ||
            claimsInsuranceCompany.getTheftService()!=null || claimsInsuranceCompany.getTheftServiceId()!=null){
                Theft theft = new Theft();
                theft.setServiceId(claimsInsuranceCompany.getTheftServiceId());
                theft.setService(claimsInsuranceCompany.getTheftService());
                theft.setDeductible(claimsInsuranceCompany.getTheftDeductible());
                theft.setDeductibleId(claimsInsuranceCompany.getTheftDeductibleId());
                insuranceCompany.setTheft(theft);
            }



            if(claimsInsuranceCompany.getPaiServiceId()!=null || claimsInsuranceCompany.getPaiService()!=null ||
                    claimsInsuranceCompany.getPaiMaxCoverage()!=null || claimsInsuranceCompany.getPaiDeductible()!=null || claimsInsuranceCompany.getPaiDeductibleId()!=null
                    ||claimsInsuranceCompany.getPaiCompanyId()!=null || claimsInsuranceCompany.getPaiCompany()!=null || claimsInsuranceCompany.getPaiTariffId()!=null
                    || claimsInsuranceCompany.getPaiTariff()!=null || claimsInsuranceCompany.getPaiTariffCode()!=null || claimsInsuranceCompany.getPaiStartDate()!=null
                    || claimsInsuranceCompany.getPaiEndDate()!=null || claimsInsuranceCompany.getPaiPolicyNumber()!=null
                    || claimsInsuranceCompany.getPaiInsuranceCompanyId()!=null || claimsInsuranceCompany.getPaiInsurancePolicyId()!=null
                    || claimsInsuranceCompany.getPaiPercentage()!=null || claimsInsuranceCompany.getPaiMedicalCosts()!=null ) {
                Pai pai = new Pai();
                pai.setServiceId(claimsInsuranceCompany.getPaiServiceId());
                pai.setService(claimsInsuranceCompany.getPaiService());
                pai.setMaxCoverage(claimsInsuranceCompany.getPaiMaxCoverage());
                pai.setDeductible(claimsInsuranceCompany.getPaiDeductible());
                pai.setDeductibleId(claimsInsuranceCompany.getPaiDeductibleId());
                pai.setCompanyId(claimsInsuranceCompany.getPaiCompanyId());
                pai.setCompany(claimsInsuranceCompany.getPaiCompany());
                pai.setTariffId(claimsInsuranceCompany.getPaiTariffId());
                pai.setTariff(claimsInsuranceCompany.getPaiTariff());
                pai.setTariffCode(claimsInsuranceCompany.getPaiTariffCode());
                pai.setStartDate(claimsInsuranceCompany.getPaiStartDate());
                pai.setEndDate(claimsInsuranceCompany.getPaiEndDate());
                pai.setPolicyNumber(claimsInsuranceCompany.getPaiPolicyNumber());
                pai.setInsuranceCompanyId(claimsInsuranceCompany.getPaiInsuranceCompanyId());
                pai.setInsurancePolicyId(claimsInsuranceCompany.getPaiInsurancePolicyId());
                pai.setPercentage(claimsInsuranceCompany.getPaiPercentage());
                pai.setMedicalCosts(claimsInsuranceCompany.getPaiMedicalCosts());
                insuranceCompany.setPai(pai);
            }



            if(claimsInsuranceCompany.getLegalCostServiceId()!=null || claimsInsuranceCompany.getLegalCostService()!=null
                    ||claimsInsuranceCompany.getLegalCostCompanyId()!=null || claimsInsuranceCompany.getLegalCostCompany()!=null || claimsInsuranceCompany.getLegalCostTariffId()!=null
                    || claimsInsuranceCompany.getLegalCostTariff()!=null || claimsInsuranceCompany.getLegalCostTariffCode()!=null || claimsInsuranceCompany.getLegalCostStartDate()!=null
                    || claimsInsuranceCompany.getLegalCostEndDate()!=null || claimsInsuranceCompany.getLegalCostPolicyNumber()!=null
                    || claimsInsuranceCompany.getLegalCostInsuranceCompanyId()!=null || claimsInsuranceCompany.getLegalCostInsurancePolicyId()!=null) {
                LegalCost legalCost = new LegalCost();
                legalCost.setServiceId(claimsInsuranceCompany.getLegalCostServiceId());
                legalCost.setService(claimsInsuranceCompany.getLegalCostService());
                legalCost.setCompanyId(claimsInsuranceCompany.getLegalCostCompanyId());
                legalCost.setCompany(claimsInsuranceCompany.getLegalCostCompany());
                legalCost.setTariffId(claimsInsuranceCompany.getLegalCostTariffId());
                legalCost.setTariff(claimsInsuranceCompany.getLegalCostTariff());
                legalCost.setTariffCode(claimsInsuranceCompany.getLegalCostTariffCode());
                legalCost.setStartDate(claimsInsuranceCompany.getLegalCostStartDate());
                legalCost.setEndDate(claimsInsuranceCompany.getLegalCostEndDate());
                legalCost.setPolicyNumber(claimsInsuranceCompany.getLegalCostPolicyNumber());
                legalCost.setInsuranceCompanyId(claimsInsuranceCompany.getLegalCostInsuranceCompanyId());
                legalCost.setInsurancePolicyId(claimsInsuranceCompany.getLegalCostInsurancePolicyId());
                insuranceCompany.setLegalCost(legalCost);
            }



            if(claimsInsuranceCompany.getKaskoDeductibleValue()!=null || claimsInsuranceCompany.getKaskoDeductibleId()!=null){
                Kasko kasko = new Kasko();
                kasko.setDeductibleId(claimsInsuranceCompany.getKaskoDeductibleId());
                kasko.setDeductibleValue(claimsInsuranceCompany.getKaskoDeductibleValue());
                insuranceCompany.setKasko(kasko);
            }
            

            return insuranceCompany;
        }

        return null;
    }
}
