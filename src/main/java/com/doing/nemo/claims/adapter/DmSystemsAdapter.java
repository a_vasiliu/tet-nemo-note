package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.controller.payload.request.DmSystemsRequestV1;
import com.doing.nemo.claims.controller.payload.response.DmSystemsResponseV1;
import com.doing.nemo.claims.entity.settings.DmSystemsEntity;
import com.doing.nemo.claims.exception.NotFoundException;
import com.doing.nemo.claims.repository.DmSystemsRepository;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
public class DmSystemsAdapter {
    @Autowired
    private DmSystemsRepository dmSystemsRepository;
    private static Logger LOGGER = LoggerFactory.getLogger(DmSystemsAdapter.class);

    public static DmSystemsEntity adptFromDmSystemsRequestToDmSystemsEntity(DmSystemsRequestV1 dmSystemsRequestV1)
    {
        DmSystemsEntity dmSystemsEntity = new DmSystemsEntity();
        if(dmSystemsRequestV1 != null)
        {
            dmSystemsEntity.setSystemName(dmSystemsRequestV1.getSystemName());
        }
        return dmSystemsEntity;
    }

    public DmSystemsEntity adptFromDmSystemsRequestToDmSystemsEntityWithID(DmSystemsRequestV1 dmSystemsRequestV1, UUID uuid)
    {
        Optional<DmSystemsEntity> dmSystemsEntity1 = dmSystemsRepository.findById(uuid);
        if(!dmSystemsEntity1.isPresent()) {
            LOGGER.debug("DM System not found");
            throw new NotFoundException("DM System not found", com.doing.nemo.claims.validation.MessageCode.CLAIMS_1010);
        }
        DmSystemsEntity dmSystemsEntity = dmSystemsEntity1.get();
        DmSystemsEntity dmSystemsEntity2 = adptFromDmSystemsRequestToDmSystemsEntity(dmSystemsRequestV1);
        dmSystemsEntity2.setId(dmSystemsEntity.getId());
        return dmSystemsEntity2;
    }

    public static DmSystemsResponseV1 adptFromDmSystemEntityToDmSystemResponse(DmSystemsEntity dmSystemsEntity)
    {
        DmSystemsResponseV1 dmSystemsResponseV1 = new DmSystemsResponseV1();
        if(dmSystemsEntity != null)
        {
            dmSystemsResponseV1.setId(dmSystemsEntity.getId());
            dmSystemsResponseV1.setSystemName(dmSystemsEntity.getSystemName());
        }
        return dmSystemsResponseV1;
    }

    public static List<DmSystemsResponseV1> adptFromDmSystemEntityToDmSystemResponseList(List<DmSystemsEntity> dmSystemsEntityList)
    {
        List<DmSystemsResponseV1> responseV1 = new LinkedList<>();
        for(DmSystemsEntity dmSystemsEntity : dmSystemsEntityList)
        {
            responseV1.add(adptFromDmSystemEntityToDmSystemResponse(dmSystemsEntity));
        }
        return responseV1;
    }

}
