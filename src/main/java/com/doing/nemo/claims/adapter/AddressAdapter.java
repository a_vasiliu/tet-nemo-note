package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.entity.esb.AddressEsb;
import com.doing.nemo.claims.entity.jsonb.Address;
import com.doing.nemo.middleware.client.payload.response.MiddlewareMainAddressResponse;
import org.springframework.stereotype.Component;

@Component
public class AddressAdapter {

    public static Address adptAddressESBToAddress(AddressEsb addressEsb) {
        if(addressEsb == null) return null;
        Address address = new Address();

        address.setLocality(addressEsb.getLocality());
        address.setProvince(addressEsb.getProvince());
        address.setRegion(addressEsb.getRegion());
        address.setState(addressEsb.getState());
        address.setStreet(addressEsb.getStreet());
        address.setStreetNr(addressEsb.getStreetNr());
        address.setZip(addressEsb.getZip());
        address.setFormattedAddress(addressEsb.getFormattedAddress());

        return address;
    }

    

    public static Address adptMiddlewareMainAddressResponseToAddress(MiddlewareMainAddressResponse middlewareMainAddressResponse) {
        if(middlewareMainAddressResponse == null) return null;
        Address address = new Address();
        
        address.setProvince(middlewareMainAddressResponse.getProvince());
        address.setRegion(middlewareMainAddressResponse.getRegion());
        address.setState(middlewareMainAddressResponse.getState());
        address.setStreet(middlewareMainAddressResponse.getStreet());
        address.setStreetNr(middlewareMainAddressResponse.getStreetNumber());
        address.setZip(middlewareMainAddressResponse.getZip());

        return address;
    }

    public static AddressEsb adptAddressToAddressESB(Address address) {
        if(address != null) {

            AddressEsb addressEsb = new AddressEsb();

            addressEsb.setLocality(address.getLocality());
            addressEsb.setProvince(address.getProvince());
            addressEsb.setRegion(address.getRegion());
            addressEsb.setState(address.getState());
            addressEsb.setStreet(address.getStreet());
            addressEsb.setStreetNr(address.getStreetNr());
            addressEsb.setZip(address.getZip());
            addressEsb.setFormattedAddress(address.getFormattedAddress());

            return addressEsb;
        }
        return null;
    }
}
