package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.EventRequestV1;
import com.doing.nemo.claims.entity.settings.EventEntity;
import com.doing.nemo.claims.entity.settings.EventTypeEntity;
import com.doing.nemo.claims.repository.EventTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class EventAdapter {

    @Autowired
    private EventTypeRepository eventTypeRepository;

    public EventEntity adptFromEventRequestToEventEntity(EventRequestV1 eventRequestV1) {
        EventEntity eventEntity = new EventEntity();
        if (eventRequestV1.getEventType() != null) {
            Optional<EventTypeEntity> eventTypeEntity = eventTypeRepository.findById(eventRequestV1.getEventType().getId());
            if (eventTypeEntity.isPresent())
                eventEntity.setEventType(eventTypeEntity.get());
        }
        eventEntity.setCreatedAt(DateUtil.getNowInstant());
        eventEntity.setUser(eventRequestV1.getUserId());
        eventEntity.setDescription(eventRequestV1.getDescription());
        return eventEntity;
    }

    public List<EventEntity> adptFromEventRequestToEventEntityList(List<EventRequestV1> eventRequestV1List) {
        List<EventEntity> eventEntityList = new ArrayList<>();
        if (eventRequestV1List != null) {
            for (EventRequestV1 eventRequestV1 : eventRequestV1List) {
                eventEntityList.add(this.adptFromEventRequestToEventEntity(eventRequestV1));
            }
        }
        return eventEntityList;
    }
}
