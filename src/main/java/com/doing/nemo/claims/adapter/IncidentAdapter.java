package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.ClaimRequest;
import com.doing.nemo.claims.controller.payload.request.IncidentRequestV1;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.enumerated.ClaimsEnum.ClaimsStatusEnum;
import com.doing.nemo.claims.entity.enumerated.ComplaintEnum.DataAccidentEnum.DataAccidentTypeAccidentEnum;
import com.doing.nemo.claims.entity.enumerated.RefundEnum.RefundTypeEnum;
import com.doing.nemo.claims.entity.jsonb.refund.Split;
import com.doing.nemo.claims.entity.settings.FlowContractTypeEntity;
import com.doing.nemo.claims.service.ContractService;
import com.doing.nemo.claims.util.Util;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import com.doing.nemo.middleware.client.payload.request.MiddlewareUpdateIncidentsRequest;
import com.doing.nemo.middleware.client.payload.request.enums.MiddlewareSystemEnum;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.*;

@Component
public class IncidentAdapter {

    @Value("${time.zone}")
    private String timeZone;

    @Autowired
    private ContractService contractService;

    private Set<DataAccidentTypeAccidentEnum> passiveSet = new HashSet<DataAccidentTypeAccidentEnum>() {{
        add(DataAccidentTypeAccidentEnum.RC_PASSIVA);
        add(DataAccidentTypeAccidentEnum.CARD_PASSIVA_FIRMA_SINGOLA);
        add(DataAccidentTypeAccidentEnum.CARD_PASSIVA_DOPPIA_FIRMA);
    }};

    private Set<ClaimsStatusEnum> statusSet = new HashSet<ClaimsStatusEnum>() {{
        add(ClaimsStatusEnum.CLOSED_PARTIAL_REFUND);
        add(ClaimsStatusEnum.CLOSED_TOTAL_REFUND);
        add(ClaimsStatusEnum.WAITING_FOR_REFUND);
    }};


    private static Logger LOGGER = LoggerFactory.getLogger(IncidentAdapter.class);


    public IncidentRequestV1 adptFromClaimsEntityToIncidentRequest(ClaimsEntity claimsEntity, ClaimsStatusEnum oldStatus, Boolean franchise, Boolean lastPayment) {
        if (claimsEntity == null) {
            return null;
        }
        /*SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSXXX"); //note: time zone not in format!
        sdf1.setTimeZone(TimeZone.getTimeZone(timeZone));

        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd"); //note: time zone not in format!
        sdf2.setTimeZone(TimeZone.getTimeZone(timeZone));*/

        IncidentRequestV1 incidentRequestV1 = new IncidentRequestV1();
        ClaimRequest claimRequest = new ClaimRequest();
        incidentRequestV1.setSystem("NEMO");
        if(claimsEntity.getStatus()!=null){
            Integer newStatus = adptIncidentStatus(claimsEntity.getStatus());
            if(newStatus == null)
                newStatus = adptIncidentStatus(oldStatus);
            incidentRequestV1.setIncidentStatusEnumId(newStatus);
        }
        if(claimsEntity.getComplaint()!=null && claimsEntity.getComplaint().getDataAccident()!=null && claimsEntity.getComplaint().getDataAccident().getTypeAccident()!=null )
            incidentRequestV1.setIncidentTypeEnumId(adptIncidentType(claimsEntity.getComplaint().getDataAccident().getTypeAccident()));
        if(claimsEntity.getComplaint()!=null && claimsEntity.getComplaint().getDataAccident()!=null && claimsEntity.getComplaint().getDataAccident().getDateAccident()!=null )
            incidentRequestV1.setIncidentDateTime(DateUtil.convertIS08601StringToUTCInstant(DateUtil.convertUTCDateToIS08601String(claimsEntity.getComplaint().getDataAccident().getDateAccident())));
        if(claimsEntity.getPracticeId()!=null)
            incidentRequestV1.setReference(claimsEntity.getPracticeId().toString());
        if(claimsEntity.getComplaint()!=null && claimsEntity.getComplaint().getDataAccident()!=null && claimsEntity.getComplaint().getDataAccident().getAddress()!=null )
            incidentRequestV1.setIncidentLocation(Util.replaceNull(claimsEntity.getComplaint().getDataAccident().getAddress().getStreet()) +" "+Util.replaceNull(claimsEntity.getComplaint().getDataAccident().getAddress().getStreetNr())+" , "+Util.replaceNull(claimsEntity.getComplaint().getDataAccident().getAddress().getZip())+" "+Util.replaceNull(claimsEntity.getComplaint().getDataAccident().getAddress().getLocality())+" "+Util.replaceNull(claimsEntity.getComplaint().getDataAccident().getAddress().getProvince())+" "+Util.replaceNull(claimsEntity.getComplaint().getDataAccident().getAddress().getRegion())+" "+Util.replaceNull(claimsEntity.getComplaint().getDataAccident().getAddress().getState()));
        if(claimsEntity.getComplaint()!=null && claimsEntity.getComplaint().getDataAccident()!=null && claimsEntity.getComplaint().getDataAccident().getRecoverability()!=null )
            incidentRequestV1.setRecoverable(claimsEntity.getComplaint().getDataAccident().getRecoverability());
        if(claimsEntity.getTheft()!=null && claimsEntity.getTheft().getFindDate()!=null  )
            incidentRequestV1.setRetrievalDate(DateUtil.convertIS08601StringToUTCInstant(DateUtil.convertUTCDateToIS08601String(claimsEntity.getTheft().getFindDate())));
        if(claimsEntity.getTheft()!=null && claimsEntity.getTheft().getAddress()!=null)
            incidentRequestV1.setRetrievAllocation(Util.replaceNull(claimsEntity.getTheft().getAddress().getStreet()) +" "+Util.replaceNull(claimsEntity.getTheft().getAddress().getStreetNr())+" , "+Util.replaceNull(claimsEntity.getTheft().getAddress().getZip())+" "+Util.replaceNull(claimsEntity.getTheft().getAddress().getLocality())+" "+Util.replaceNull(claimsEntity.getTheft().getAddress().getProvince())+" "+Util.replaceNull(claimsEntity.getTheft().getAddress().getRegion())+" "+Util.replaceNull(claimsEntity.getTheft().getAddress().getState()));
        if(claimsEntity.getRefund()!=null && claimsEntity.getRefund().getWreckValuePre()!=null)
            incidentRequestV1.setVehicleValue(claimsEntity.getRefund().getWreckValuePre());
        if(claimsEntity.getRefund()!=null && claimsEntity.getRefund().getPoSum()!=null)
            incidentRequestV1.setExpectedRepairCost(claimsEntity.getRefund().getPoSum());
        if(claimsEntity.getDamaged()!=null && claimsEntity.getDamaged().getInsuranceCompany()!=null && claimsEntity.getDamaged().getInsuranceCompany().getTpl()!=null){
            incidentRequestV1.setInternalInsured(true);
        } else {
            incidentRequestV1.setInternalInsured(false);
        }
        if(claimsEntity.getDamaged()!=null && claimsEntity.getDamaged().getDriver() !=null)
            incidentRequestV1.setAccidentDriver(claimsEntity.getDamaged().getDriver().getFirstname() + " "+claimsEntity.getDamaged().getDriver().getLastname());
        if(claimsEntity.getComplaint()!=null && claimsEntity.getComplaint().getDataAccident()!=null && claimsEntity.getComplaint().getDataAccident().getRecoverabilityPercent()!=null )
            incidentRequestV1.setPctRecoverable(claimsEntity.getComplaint().getDataAccident().getRecoverabilityPercent());
        if(claimsEntity.getRefund()!=null && claimsEntity.getRefund().getWreckValuePost()!=null)
            incidentRequestV1.setExpectedWreckValue(claimsEntity.getRefund().getWreckValuePost());
        if(claimsEntity.getComplaint()!=null && claimsEntity.getComplaint().getDataAccident()!=null && claimsEntity.getComplaint().getDataAccident().getTypeAccident()!=null){
            incidentRequestV1.setDescriptionCodeEnumId(getMilesCodeFromTypeAccident(claimsEntity.getComplaint().getDataAccident().getTypeAccident()));

        }

        String date = DateUtil.convertUTCInstantToIS08601String(claimsEntity.getCreatedAt());
        Date dateCreated = DateUtil.convertIS08601StringToUTCDate(date);

        if(claimsEntity.getComplaint() != null && claimsEntity.getComplaint().getDataAccident() != null && claimsEntity.getComplaint().getDataAccident().getResponsible() != null){
            incidentRequestV1.setCustomerResponsible(claimsEntity.getComplaint().getDataAccident().getResponsible());
        }

        if(claimsEntity.getWithCounterparty() != null){
            incidentRequestV1.setThirdpartyInvolved(claimsEntity.getWithCounterparty());
        }

        if( claimsEntity.getCreatedAt() != null){
            incidentRequestV1.setRegistrationDate(DateUtil.convertIS08601StringToUTCInstant(DateUtil.convertUTCDateToIS08601String(dateCreated)));
        }


        if(     claimsEntity.getDamaged() != null &&
                claimsEntity.getDamaged().getContract() != null &&
                claimsEntity.getDamaged().getContract().getContractType() != null &&
                claimsEntity.getComplaint() != null &&
                claimsEntity.getComplaint().getDataAccident() != null &&
                claimsEntity.getComplaint().getDataAccident().getTypeAccident() != null

        ) {
            String typeOfChargeBack = contractService.getTypeOfChargeBack(claimsEntity.getDamaged().getContract().getContractType(),
                    claimsEntity.getComplaint().getDataAccident().getTypeAccident(),
                    claimsEntity.getDamaged().getCustomer().getCustomerId());

            if( typeOfChargeBack != null){
                LOGGER.info(typeOfChargeBack);
                incidentRequestV1.setRebillType(adptRebillType(typeOfChargeBack.toUpperCase()));
            }
        }

        incidentRequestV1.setDateSafReceived(DateUtil.convertIS08601StringToUTCInstant(DateUtil.convertUTCDateToIS08601String(dateCreated)));
        //incidentRequestV1.setReference(claimsEntity.getPracticeId().toString()); //id pratica
        claimRequest.setClaimStatusEnumId(1105); //REGISTERED
        claimRequest.setClaimCategoryEnumId(402044); //INSURANCE_REFUND
        if( claimsEntity.getDamaged() != null &&
                claimsEntity.getDamaged().getInsuranceCompany() != null &&
                claimsEntity.getDamaged().getInsuranceCompany().getTpl() != null &&
                claimsEntity.getDamaged().getInsuranceCompany().getTpl().getPolicyNumber() != null
        ){
            claimRequest.setPolicyNumber(claimsEntity.getDamaged().getInsuranceCompany().getTpl().getPolicyNumber());
        }


        if( claimsEntity.getComplaint() != null &&
                claimsEntity.getComplaint().getFromCompany() != null &&
                claimsEntity.getComplaint().getFromCompany().getNumberSx() != null &&
                !claimsEntity.getComplaint().getFromCompany().getNumberSx().isEmpty()
        ){
            claimRequest.setFileNumber(claimsEntity.getComplaint().getFromCompany().getNumberSx());
        }

        if( claimsEntity.getRefund() != null &&
                claimsEntity.getRefund().getTotalLiquidationReceived() != null
        ){
            if(!lastPayment)
                claimRequest.setReceivedamount( claimsEntity.getRefund().getTotalLiquidationReceived());
            else
                claimRequest.setReceivedamount(0.0);
        }

        if( claimsEntity.getDamaged() != null && claimsEntity.getDamaged().getVehicle()!=null &&
                claimsEntity.getDamaged().getVehicle().getWreck() != null )
        {
            incidentRequestV1.setTotalLoss(claimsEntity.getDamaged().getVehicle().getWreck());
        }

        if( claimsEntity.getDamaged() != null && claimsEntity.getDamaged().getVehicle()!=null &&
                claimsEntity.getDamaged().getVehicle().getWreckDate() != null
        ){
            incidentRequestV1.setTotalLossDate(DateUtil.convertIS08601StringToUTCInstant(DateUtil.convertUTCDateToIS08601String(claimsEntity.getDamaged().getVehicle().getWreckDate())));
        }

        if( claimsEntity.getRefund() != null &&
                claimsEntity.getRefund().getTotalRefundExpected() != null
        ){
            claimRequest.setAmount(claimsEntity.getRefund().getTotalRefundExpected());
        }

        if( claimsEntity.getRefund() != null &&
                claimsEntity.getRefund().getDefinitionDate() != null
        ){
            claimRequest.setPaymentDate(DateUtil.convertIS08601StringToUTCInstant(DateUtil.convertUTCDateToIS08601String(claimsEntity.getRefund().getDefinitionDate())));
        }

        if(claimsEntity.getStatus().equals(ClaimsStatusEnum.CLOSED_TOTAL_REFUND))
        {
            claimRequest.setPaymentComplete(true);
        } else {
            claimRequest.setPaymentComplete(false);

        }
        if(claimsEntity.getTotalPenalty() != null)
        {
            Integer totalRebill = adptIncidentTotalRebillFromClaimsToMiles(claimsEntity.getTotalPenalty().intValue());
            if(totalRebill != null)
                incidentRequestV1.setnRebills(totalRebill);
            else LOGGER.info("Impossible to parse total rebill.");
        }

        if(claimsEntity.getRefund()!=null && claimsEntity.getRefund().getSplitList()!=null && !claimsEntity.getRefund().getSplitList().isEmpty()){
            Split first = claimsEntity.getRefund().getSplitList().get(claimsEntity.getRefund().getSplitList().size()-1);
            claimRequest.setPaymentMethodEnumid(adptPaymentMethod(first.getRefundType()));
           // claimRequest.setPaymentCommunication(first.getNote());
            claimRequest.setPaymentCommunication(StringUtils.EMPTY);
        }

        if(passiveSet.contains(claimsEntity.getComplaint().getDataAccident().getTypeAccident()) || !statusSet.contains(claimsEntity.getStatus())){
            claimRequest=null;
        } else if(claimsEntity.getComplaint()!=null && claimsEntity.getComplaint().getEntrusted()!=null && claimsEntity.getComplaint().getEntrusted().getEntrustedDay()!=null){
            claimRequest.setRequestDate(DateUtil.convertIS08601StringToUTCInstant(DateUtil.convertUTCDateToIS08601String(claimsEntity.getComplaint().getEntrusted().getEntrustedDay())));
            claimRequest.setConfirmed(true);
        } else {
            claimRequest.setConfirmed(false);
        }
        List<ClaimRequest> claimsList = new LinkedList<>();
        if(claimRequest!=null){
            claimsList.add(claimRequest);
        }
        incidentRequestV1.setClaims(claimsList);
        return incidentRequestV1;
    }

    private static Integer adptIncidentStatus (ClaimsStatusEnum status){
        if( status == null )
            return null;
        if(status.equals(ClaimsStatusEnum.CLOSED))
            return 401388;
        else if(status.equals(ClaimsStatusEnum.MANAGED))
            return 401386;
        else if(status.equals(ClaimsStatusEnum.TO_ENTRUST))
            return 401389;
        else if(status.equals(ClaimsStatusEnum.TO_SEND_TO_CUSTOMER))
            return 401391;
        else if(status.equals(ClaimsStatusEnum.WAITING_FOR_VALIDATION))
            return 401392;
        else if(status.equals(ClaimsStatusEnum.WAITING_FOR_AUTHORITY))
            return 401393;
        else if(status.equals(ClaimsStatusEnum.WAITING_FOR_REFUND))
            return 401394;
        else if(status.equals(ClaimsStatusEnum.INCOMPLETE))
            return 401395;
        else if(status.equals(ClaimsStatusEnum.REJECTED))
            return 401396;
        else if(status.equals(ClaimsStatusEnum.CLOSED_TOTAL_REFUND))
            return 401397;
        else if(status.equals(ClaimsStatusEnum.CLOSED_PARTIAL_REFUND))
            return 401398;
        else if(status.equals(ClaimsStatusEnum.DELETED))
            return 401037;
        else if(status.equals(ClaimsStatusEnum.SEND_TO_CLIENT))
            return 401127;
        else return null;
    }

    public static String adptIncidentStatusFromMilesToClaims(Integer status){
        if(status == null)
            return null;
        if(status.equals(401388))
            return "Chiuso senza seguito";
        else if(status.equals(401386))
            return "Autogestita";
        else if(status.equals(401389))
            return "Da affidare";
        else if(status.equals(401391))
            return "Da inviare al cliente";
        else if(status.equals(401392))
            return "Da validare";
        else if(status.equals(401393))
            return "In attesa";
        else if(status.equals(401394))
            return "In attesa di rimborso";
        else if(status.equals(401395))
            return "Incompleta";
        else if(status.equals(401396))
            return "Rifiutata";
        else if(status.equals(401397))
            return "Chiuso rimborsato";
        else if(status.equals(401398))
            return "Chiuso rimborsato";
        else if(status.equals(401037))
            return "Cancellato";
        else if(status.equals(401127))
            return "Inviato al cliente";
        else return null;
    }

    private static  Integer adptIncidentTotalRebillFromClaimsToMiles(Integer value){
        if(value == null )
            return null;
        if(value.equals(0))
            return 401316;
        else if(value.equals(1))
            return 401307;
        else if(value.equals(2))
            return 401308;
        else if(value.equals(3))
            return 401309;
        else if(value.equals(4))
            return 401310;
        else if(value.equals(5))
            return 401311;
        else if(value.equals(6))
            return 401312;
        else if(value.equals(7))
            return 401313;
        else if(value.equals(8))
            return 401314;
        else if(value.equals(9))
            return 401315;
        else return null;
    }

    public static  Integer adptIncidentTotalRebillFromMilesToClaims(Integer value){
        if( value == null)
            return null;
        if(value.equals(401316))
            return 0;
        else if(value.equals(401307))
            return 1;
        else if(value.equals(401308))
            return 2;
        else if(value.equals(401309))
            return 3;
        else if(value.equals(401310))
            return 4;
        else if(value.equals(401311))
            return 5;
        else if(value.equals(401312))
            return 6;
        else if(value.equals(401313))
            return 7;
        else if(value.equals(401314))
            return 8;
        else if(value.equals(401315))
            return 9;
        else return null;
    }


    private static  Integer adptPaymentMethod (RefundTypeEnum value) {
        if(value == null)
            return null;
        if(value.equals(RefundTypeEnum.BANK_TRANSFER))
            return 741;
        else if(value.equals(RefundTypeEnum.ALLOWANCE))
            return 742;
        else return null;
    }

    public static String adptPaymentMethodFromMilesToClaims(Integer paymentMethod){
        if(paymentMethod == null)
            return null;
        if(paymentMethod.equals(741))
            return "Bonifico bancario";
        else if(paymentMethod.equals(742))
            return "Assegno";
        else
            return null;
    }

    private static Integer adptIncidentType (DataAccidentTypeAccidentEnum typeAccident){
   /*****************************   Riga commentata causa  duplicazione sinistri in MILES NE-1457 NEMOCLAIMS-1180  ***************************
        if(typeAccident != null && (typeAccident.equals(DataAccidentTypeAccidentEnum.FURTO_E_RITROVAMENTO) || typeAccident.equals(DataAccidentTypeAccidentEnum.FURTO_TOTALE)))
          return 393;
    */
        if(typeAccident != null && (typeAccident.equals(DataAccidentTypeAccidentEnum.FURTO_E_RITROVAMENTO) || typeAccident.equals(DataAccidentTypeAccidentEnum.FURTO_TOTALE)))
            return 393;
         return 390;

    }

    public static String adptIncidentTypeFromMilesToClaims(Integer typeAccident){
        if( typeAccident == null )
            return null;
        if(typeAccident.equals(393))
            return "Furto";
        return "Sinistro";
    }

    private static Integer getMilesCodeFromTypeAccident(DataAccidentTypeAccidentEnum typeAccident){
        if(typeAccident == null )
            return null;
        switch (typeAccident){
            case CARD_PASSIVA_FIRMA_SINGOLA:
                return 401146;
            case CARD_CONCORSUALE_DOPPIA_FIRMA:
                return 401147;
            case CARD_CONCORSUALE_FIRMA_SINGOLA:
                return 401148;
            case CRISTALLI:
                return 401149;
            case ATTO_VANDALICO:
                return 401132;
            case INCENDIO:
                return 401133;
            case RC_CONCORSUALE:
                return 401134;
            case DANNO_RITROVATO_IN_PARCHEGGIO:
                return 401135;
            case RC_PASSIVA:
                return 401136;
            case KASKO_URTO_CONTRO_OGGETTI_FISSI:
                return 401137;
            case RC_ATTIVA:
                return 401138;
            case FURTO_E_RITROVAMENTO:
                return 401139;
            case FURTO_TOTALE:
                return 401140;
            case EVENTI_NATURALI:
                return 401141;
            case CHIAVI_SMARRITE_O_RUBATE:
                return 401142;
            case CARD_ATTIVA_DOPPIA_FIRMA:
                return 401143;
            case CARD_ATTIVA_FIRMA_SINGOLA:
                return 401144;
            case CARD_PASSIVA_DOPPIA_FIRMA:
                return 401145;
            case TENTATO_FURTO:
                return 401131;
            case FURTO_PARZIALE:
                return 401131;
            default:
                return 401130;
        }
    }

    public static String getMilesCodeFromTypeAccidentFromMilesToClaims(Integer typeAccident){
        if( typeAccident == null )
            return null;
        switch (typeAccident){
            case 401146:
                return "Card passiva firma singola";
            case 401147:
                return "Card concorsuale doppia firma";
            case 401148:
                return "Card concorsuale firma singola";
            case 401149:
                return "Cristalli";
            case 401132:
                return "Atto vandalico";
            case 401133:
                return "Incendio";
            case 401134:
                return "RC Concorsuale";
            case 401135:
                return "Danno ritrovato in parcheggio";
            case 401136:
                return "RC Passiva";
            case 401137:
                return "Urto contro oggetti fissi";
            case 401138:
                return "RC Attiva";
            case 401139:
                return "Furto e ritrovamento";
            case 401140:
                return "Furto totale";
            case 401141:
                return "Eventi naturali";
            case 401142:
                return "Chiavi smarrite o rubate";
            case 401143:
                return "Card attiva doppia firma";
            case 401144:
                return "Card attiva firma singola";
            case 401145:
                return "Card passiva doppia firma";
            case 401131:
                return "Tentato furto/Furto parziale";
            default:
                return "";
        }
    }


    public static String getMilesCodeFromCategoryEnumIdToClaims(Integer claimCategory){
        if( claimCategory == null )
            return null;
        switch (claimCategory){
            case 402044:
                return "Insurance Refund";
            default:
                return "";
        }
    }

    private static Integer adptRebillType(String rebillType){
        if( rebillType == null )
            return null;
        switch (rebillType){
            case "DEDUCTIBLE":
            case "EXEMPTION":
                 return 401304;
            case "NO REBILL":
                return 401306;
            case "TOTAL REBILL":
                return 401305;
            default:
                LOGGER.info("Error to parse rebill type " + rebillType);
        }
        return null;
    }

    public static String adptRebillTypeFromMilesToClaim(Integer rebillType){
        if( rebillType == null )
            return null;
        switch (rebillType){
            case 401304:
                return "Exemption";
            case 401306:
                return "No Rebill";
            case 401305:
                return "Total Rebill";
            default:
                LOGGER.info("Error to parse rebill type " + rebillType);
        }
        return "";
    }

    public static MiddlewareUpdateIncidentsRequest adptIncidentRequestToMiddlewareUpdateIncidentsRequest(IncidentRequestV1 incidentRequestV1){
        if(incidentRequestV1 == null){ return null; }

        MiddlewareUpdateIncidentsRequest middlewareUpdateIncidentsRequest = new MiddlewareUpdateIncidentsRequest();
        middlewareUpdateIncidentsRequest.setSystem(MiddlewareSystemEnum.NEMO);
        middlewareUpdateIncidentsRequest.setIncidentStatus(incidentRequestV1.getIncidentStatusEnumId());
        middlewareUpdateIncidentsRequest.setImmobilityStatusEnumId(incidentRequestV1.getImmobilityStatusEnumId());
        middlewareUpdateIncidentsRequest.setIncidentType(incidentRequestV1.getIncidentTypeEnumId());
        middlewareUpdateIncidentsRequest.setDescription(incidentRequestV1.getDescription());
        middlewareUpdateIncidentsRequest.setCustomerResponsible(incidentRequestV1.getCustomerResponsible());
        middlewareUpdateIncidentsRequest.setIncidentDateTime(incidentRequestV1.getIncidentDateTime());
        middlewareUpdateIncidentsRequest.setDistance(incidentRequestV1.getDistance());
        middlewareUpdateIncidentsRequest.setIncidentLocation(incidentRequestV1.getIncidentLocation());
        middlewareUpdateIncidentsRequest.setReference(incidentRequestV1.getReference());
        middlewareUpdateIncidentsRequest.setReportNumber(incidentRequestV1.getReportNumber());
        middlewareUpdateIncidentsRequest.setPctliability(incidentRequestV1.getPctliability());
        middlewareUpdateIncidentsRequest.setSafReceived(incidentRequestV1.getSafReceived());
        middlewareUpdateIncidentsRequest.setDateSAFReceived(incidentRequestV1.getDateSafReceived());
        middlewareUpdateIncidentsRequest.setTotalLoss(incidentRequestV1.getTotalLoss());
        middlewareUpdateIncidentsRequest.setTotalLossDate(incidentRequestV1.getTotalLossDate());
        middlewareUpdateIncidentsRequest.setTotalLossTypeEnumId(incidentRequestV1.getTotalLossTypeEnumId());
        middlewareUpdateIncidentsRequest.setSpecificAttentionEnumId(incidentRequestV1.getSpecificAttentionEnumId());
        middlewareUpdateIncidentsRequest.setRegistrationDate(incidentRequestV1.getRegistrationDate());
        middlewareUpdateIncidentsRequest.setDescriptionCodeEnumId(incidentRequestV1.getDescriptionCodeEnumId());
        middlewareUpdateIncidentsRequest.setRecoverable(incidentRequestV1.getRecoverable());
        middlewareUpdateIncidentsRequest.setWeatherConditionEnumId(incidentRequestV1.getWeatherConditionEnumId());
        middlewareUpdateIncidentsRequest.setPoliceReportDate(incidentRequestV1.getPoliceReportDate());
        middlewareUpdateIncidentsRequest.setThief(incidentRequestV1.getThief());
        middlewareUpdateIncidentsRequest.setTheftTypeEnumId(incidentRequestV1.getTheftTypeEnumId());
        middlewareUpdateIncidentsRequest.setRetrievalDate(incidentRequestV1.getRetrievalDate());
        middlewareUpdateIncidentsRequest.setRetrievAllocation(incidentRequestV1.getRetrievAllocation());
        middlewareUpdateIncidentsRequest.setVehicleValue(incidentRequestV1.getVehicleValue());
        middlewareUpdateIncidentsRequest.setExpectedRepairCost(incidentRequestV1.getExpectedRepairCost());
        middlewareUpdateIncidentsRequest.setRelationshipTypeEnumId(incidentRequestV1.getRelationshipTypeEnumId());
        middlewareUpdateIncidentsRequest.setLocationTypeEnumId(incidentRequestV1.getLocationTypeEnumId());
        middlewareUpdateIncidentsRequest.setSpeedLimitEnumId(incidentRequestV1.getSpeedLimitEnumId());
        middlewareUpdateIncidentsRequest.setIncidentSpeed(incidentRequestV1.getIncidentSpeed());
        middlewareUpdateIncidentsRequest.setSeatBelt(incidentRequestV1.getSeatBelt());
        middlewareUpdateIncidentsRequest.setInternalInsured(incidentRequestV1.getInternalInsured());
        middlewareUpdateIncidentsRequest.setThirdpartyInvolved(incidentRequestV1.getThirdpartyInvolved());
        middlewareUpdateIncidentsRequest.setnRebills(incidentRequestV1.getnRebills());
        middlewareUpdateIncidentsRequest.setRebillType(incidentRequestV1.getRebillType());
        middlewareUpdateIncidentsRequest.setAccidentDriver(incidentRequestV1.getAccidentDriver());
        middlewareUpdateIncidentsRequest.setPctRecoverable(incidentRequestV1.getPctRecoverable());
        middlewareUpdateIncidentsRequest.setExpectedWreckValue(incidentRequestV1.getExpectedWreckValue());
        middlewareUpdateIncidentsRequest.setClaims(ClaimAdapter.adptListClaimRequestToListMiddlewareClaim(incidentRequestV1.getClaims()));

        return middlewareUpdateIncidentsRequest;
    }
}
