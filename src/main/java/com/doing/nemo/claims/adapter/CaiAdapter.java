package com.doing.nemo.claims.adapter;


import com.doing.nemo.claims.controller.payload.request.claims.CaiRequest;
import com.doing.nemo.claims.controller.payload.response.claims.CaiResponse;
import com.doing.nemo.claims.entity.jsonb.cai.Cai;
import com.doing.nemo.claims.entity.jsonb.cai.CaiDetails;
import org.springframework.stereotype.Component;

@Component
public class CaiAdapter {

    public static Cai adptCaiToCaiResponse(CaiRequest caiRequest) {

        if (caiRequest != null) {

            Cai cai = new Cai();
            cai.setVehicleA(CaiDetailsAdapter.adptCaiDetailsRequestToCaiDetails(caiRequest.getVehicleA()));
            cai.setVehicleB(CaiDetailsAdapter.adptCaiDetailsRequestToCaiDetails(caiRequest.getVehicleB()));
            cai.setDriverSide(caiRequest.getDriverSide());
            cai.setNote(caiRequest.getNote());
            /*String note = new String();
            note+= "VEICOLO A: ";
            note+=adptCaiConditionToCaiNote(cai.getVehicleA());
            note+="\nVEICOLO B: ";
            note += adptCaiConditionToCaiNote(cai.getVehicleB());
            cai.setNote(note);*/

            return cai;
        }
        return null;
    }



    public static String adptCaiConditionToCaiNote(CaiDetails caiDetails) {
        String note = new String();
        if (caiDetails != null) {
            if (caiDetails.getCondition1() != null && caiDetails.getCondition1().getName() .equals( "1") && caiDetails.getCondition1().getValue())
                note += "In fermata / in sosta. ";
            if (caiDetails.getCondition2() != null && caiDetails.getCondition2().getName() .equals( "2") && caiDetails.getCondition2().getValue())
                note += "Ripartiva dopo una sosta o apriva una portiera. ";
            if (caiDetails.getCondition3() != null && caiDetails.getCondition3().getName() .equals( "3") && caiDetails.getCondition3().getValue())
                note += "Stava parcheggiando. ";
            if (caiDetails.getCondition4() != null && caiDetails.getCondition4().getName() .equals( "4") && caiDetails.getCondition4().getValue())
                note += "Usciva da un parcheggio, da un luogo privato, da una strada vicinale. ";
            if (caiDetails.getCondition5() != null && caiDetails.getCondition5().getName() .equals( "5") && caiDetails.getCondition5().getValue())
                note += "Entrava in un parcheggio, da un luogo privato, da una strada vicinale. ";
            if (caiDetails.getCondition6() != null && caiDetails.getCondition6().getName() .equals( "6") && caiDetails.getCondition6().getValue())
                note += "Si immetteva in una piazza a senso rotatorio ";
            if (caiDetails.getCondition7() != null && caiDetails.getCondition7().getName() .equals( "7") && caiDetails.getCondition7().getValue())
                note += "Circolava su una piazza a senso rotatorio ";
            if (caiDetails.getCondition8() != null && caiDetails.getCondition8().getName() .equals( "8") && caiDetails.getCondition8().getValue())
                note += "Tamponava procedendo nello stesso senso e nella stessa fila. ";
            if (caiDetails.getCondition9() != null && caiDetails.getCondition9().getName() .equals( "9") && caiDetails.getCondition9().getValue())
                note += "Procedeva nello stesso senso, ma in una fila diversa. ";
            if (caiDetails.getCondition10() != null && caiDetails.getCondition10().getName() .equals( "10") && caiDetails.getCondition10().getValue())
                note += "Cambiava fila. ";
            if (caiDetails.getCondition11() != null && caiDetails.getCondition11().getName() .equals( "11") && caiDetails.getCondition11().getValue())
                note += "Sorpassava. ";
            if (caiDetails.getCondition12() != null && caiDetails.getCondition12().getName() .equals( "12") && caiDetails.getCondition12().getValue())
                note += "Girava a destra. ";
            if (caiDetails.getCondition13() != null && caiDetails.getCondition13().getName() .equals( "13") && caiDetails.getCondition13().getValue())
                note += "Girava a sinistra. ";
            if (caiDetails.getCondition14() != null && caiDetails.getCondition14().getName() .equals( "14") && caiDetails.getCondition14().getValue())
                note += "Retrocedeva. ";
            if (caiDetails.getCondition15() != null && caiDetails.getCondition15().getName() .equals( "15") && caiDetails.getCondition15().getValue())
                note += "Invadeva la sede stradale riservata alla circolazione in senso inverso. ";
            if (caiDetails.getCondition16() != null && caiDetails.getCondition16().getName() .equals( "16") && caiDetails.getCondition16().getValue())
                note += "Proveniva da destra. ";
            if (caiDetails.getCondition17() != null && caiDetails.getCondition17().getName() .equals( "17" )&& caiDetails.getCondition17().getValue())
                note += "Non aveva osservato il segnale di precedenza o di semaforo rosso. ";
            return note;
        }
        return "Nessuno";
    }

    public static CaiResponse adptCaiToCaiResponse(Cai cai) {

        if (cai != null) {

            CaiResponse caiResponse = new CaiResponse();
            caiResponse.setVehicleA(CaiDetailsAdapter.adptCaiDetailsToCaiDetailsResponse(cai.getVehicleA()));
            caiResponse.setVehicleB(CaiDetailsAdapter.adptCaiDetailsToCaiDetailsResponse(cai.getVehicleB()));
            caiResponse.setDriverSide(cai.getDriverSide());
            caiResponse.setNote(cai.getNote());

            return caiResponse;
        }
        return null;
    }

}
