package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.response.DataManagementExportResponsePaginationV1;
import com.doing.nemo.claims.controller.payload.response.DataManagementExportResponseV1;
import com.doing.nemo.claims.controller.payload.response.DataManagementExportResponseV1.ExportingFileResponseV1;
import com.doing.nemo.claims.entity.enumerated.DTO.PageStats;
import com.doing.nemo.claims.entity.settings.DataManagementExportEntity;
import com.doing.nemo.claims.entity.settings.ExportingFile;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

@Component
public class DataManagementExportAdapter {
    
    public static DataManagementExportResponseV1 adptDataManagementExportToDataManagementExportResponse(DataManagementExportEntity dataManagementExport){

        if(dataManagementExport == null)
            return null;

        DataManagementExportResponseV1 dataManagementExportResponse = new DataManagementExportResponseV1();

        dataManagementExportResponse.setDate(DateUtil.convertUTCInstantToIS08601String(dataManagementExport.getDate()));
        dataManagementExportResponse.setDescription(dataManagementExport.getDescription());
        dataManagementExportResponse.setId(dataManagementExport.getId());
        dataManagementExportResponse.setResult(dataManagementExport.getResult());
        dataManagementExportResponse.setUser(dataManagementExport.getUser());
        dataManagementExportResponse.setUserFirstname(dataManagementExport.getUserFirstname());
        dataManagementExportResponse.setUserLastname(dataManagementExport.getUserLastname());
        dataManagementExportResponse.setRole(dataManagementExport.getRole());
        dataManagementExportResponse.setProcessingFiles(adptExportingFileToExportingFileResponse(dataManagementExport.getProcessingFiles()));

        return dataManagementExportResponse;
    }

    public static List<DataManagementExportResponseV1> adptDataManagementExportToDataManagementExportResponse(List<DataManagementExportEntity> dataManagementExportList){

        if(dataManagementExportList == null)
            return null;

        List<DataManagementExportResponseV1> dataManagementExportResponseList = new LinkedList<>();

        for(DataManagementExportEntity att : dataManagementExportList){
            dataManagementExportResponseList.add(adptDataManagementExportToDataManagementExportResponse(att));
        }

        return dataManagementExportResponseList;
    }

    public static ExportingFileResponseV1 adptExportingFileToExportingFileResponse(ExportingFile exportingFile){

        if(exportingFile == null)
            return null;

        ExportingFileResponseV1 exportingFileResponse = new ExportingFileResponseV1();

        exportingFileResponse.setAttachmentId(exportingFile.getAttachmentId());
        exportingFileResponse.setFileName(exportingFile.getFileName());
        exportingFileResponse.setFileSize(exportingFile.getFileSize());
        exportingFileResponse.setId(exportingFile.getId());

        return exportingFileResponse;
    }

    public static List<ExportingFileResponseV1> adptExportingFileToExportingFileResponse(List<ExportingFile> exportingFileList){

        if(exportingFileList == null)
            return null;

        List<ExportingFileResponseV1> exportingFileResponseList = new LinkedList<>();

        for (ExportingFile att : exportingFileList){

            exportingFileResponseList.add(adptExportingFileToExportingFileResponse(att));
        }

        return exportingFileResponseList;
    }

    public static DataManagementExportResponsePaginationV1 adptPagination(List<DataManagementExportEntity> dataManagementImportList, int page, int pageSize, Long itemCount) {

        long totPages = (long) Math.ceil((double) itemCount.intValue() / pageSize);
        PageStats pageStats = new PageStats();
        if(page > totPages || page < 1)
            pageStats.setCurrentPage(1);
        else
            pageStats.setCurrentPage(page);
        pageStats.setItemCount(itemCount.intValue());
        pageStats.setPageSize(pageSize);
        pageStats.setPageCount(totPages);

        DataManagementExportResponsePaginationV1 dataManagementImportResponsePaginationV1 = new DataManagementExportResponsePaginationV1(pageStats,adptDataManagementExportToDataManagementExportResponse(dataManagementImportList));

        return dataManagementImportResponsePaginationV1;
    }

}
