package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.complaint.EntrustedRequest;
import com.doing.nemo.claims.controller.payload.request.complaint.EntrustedRequestLight;
import com.doing.nemo.claims.controller.payload.response.ClaimsEntrustedResponseV1;
import com.doing.nemo.claims.controller.payload.response.complaint.EntrustedResponse;
import com.doing.nemo.claims.entity.ClaimsEntity;
import com.doing.nemo.claims.entity.claims.ClaimsEntrustedEntity;
import com.doing.nemo.claims.entity.jsonb.complaint.jsonbComplaint.Entrusted;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class EntrustedAdapter {

    public static Entrusted adptEntrustedRequestToEntrusted(EntrustedRequest entrustedRequest) {

        if (entrustedRequest != null) {

            Entrusted entrusted = new Entrusted();
            entrusted.setAutoEntrust(entrustedRequest.getAutoEntrust());
            entrusted.setDescription(entrustedRequest.getDescription());
            entrusted.setEntrustedDay(entrustedRequest.getEntrustedDay());
            entrusted.setEntrustedTo(entrustedRequest.getEntrustedTo());
            entrusted.setEntrustedType(entrustedRequest.getEntrustedType());
            entrusted.setPerito(entrustedRequest.getPerito());
            entrusted.setEntrustedEmail(entrustedRequest.getEntrustedEmail());
            if(entrustedRequest.getIdEntrustedTo() != null){
                entrusted.setIdEntrustedTo(UUID.fromString(entrustedRequest.getIdEntrustedTo()));
            }
            entrusted.setNumberSxCounterparty(entrustedRequest.getNumberSxCounterparty());
            entrusted.setStatus(entrusted.getStatus());
            return entrusted;
        }

        return null;
    }

    public static Entrusted adptEntrustedRequestToEntrustedUpdate(EntrustedRequest entrustedRequest, Entrusted entrusted) {

        if (entrustedRequest != null) {
            if(entrusted == null)
                entrusted = new Entrusted();
            entrusted.setAutoEntrust(entrustedRequest.getAutoEntrust());
            entrusted.setDescription(entrustedRequest.getDescription());
            entrusted.setEntrustedDay(entrustedRequest.getEntrustedDay());
            entrusted.setEntrustedTo(entrustedRequest.getEntrustedTo());
            entrusted.setEntrustedType(entrustedRequest.getEntrustedType());
            entrusted.setPerito(entrustedRequest.getPerito());
            entrusted.setEntrustedEmail(entrustedRequest.getEntrustedEmail());
            if(entrustedRequest.getIdEntrustedTo() != null){
                entrusted.setIdEntrustedTo(UUID.fromString(entrustedRequest.getIdEntrustedTo()));
            }

            return entrusted;
        }

        return entrusted;
    }

    public static EntrustedResponse adptEntrustedToEntrustedREsponse(Entrusted entrusted) {

        if (entrusted != null) {

            EntrustedResponse entrustedResponse = new EntrustedResponse();
            entrustedResponse.setDescription(entrusted.getDescription());
            entrustedResponse.setEntrustedDay(entrusted.getEntrustedDay());
            entrustedResponse.setEntrustedTo(entrusted.getEntrustedTo());
            entrustedResponse.setEntrustedType(entrusted.getEntrustedType());
            entrustedResponse.setPerito(entrusted.getPerito());
            entrustedResponse.setAutoEntrust(entrusted.getAutoEntrust());
            entrustedResponse.setEntrustedEmail(entrusted.getEntrustedEmail());
            if(entrusted.getIdEntrustedTo() != null){
                entrustedResponse.setIdEntrustedTo(entrusted.getIdEntrustedTo().toString());
            }
            entrustedResponse.setDwlMan(DateUtil.convertUTCDateToIS08601String(entrusted.getDwlMan()));
            entrustedResponse.setNumberSxCounterparty(entrusted.getNumberSxCounterparty());
            entrustedResponse.setStatus(entrusted.getStatus());
            return entrustedResponse;
        }
        return null;

    }

    public static ClaimsEntrustedResponseV1 adptEntrustedToClaimsEntrustedResponse(ClaimsEntity claimsEntity) {

        ClaimsEntrustedResponseV1 claimsEntrustedResponseV1 = new ClaimsEntrustedResponseV1();
        if (claimsEntity != null) {

            claimsEntrustedResponseV1.setIdClaims(claimsEntity.getId());
            claimsEntrustedResponseV1.setStatus(claimsEntity.getStatus());
            Entrusted entrusted = claimsEntity.getComplaint().getEntrusted();
            if(entrusted != null) {
                claimsEntrustedResponseV1.setDescription(entrusted.getDescription());
                claimsEntrustedResponseV1.setEntrustedDay(entrusted.getEntrustedDay());
                claimsEntrustedResponseV1.setEntrustedTo(entrusted.getEntrustedTo());
                claimsEntrustedResponseV1.setEntrustedType(entrusted.getEntrustedType());
                claimsEntrustedResponseV1.setPerito(entrusted.getPerito());
                claimsEntrustedResponseV1.setAutoEntrust(entrusted.getAutoEntrust());
                claimsEntrustedResponseV1.setEntrustedEmail(entrusted.getEntrustedEmail());
                if (entrusted.getIdEntrustedTo() != null)
                    claimsEntrustedResponseV1.setIdEntrustedTo(entrusted.getIdEntrustedTo().toString());
                return claimsEntrustedResponseV1;
            }
            return null;
        }
        return null;

    }

    public static Entrusted adptEntrustedRequestLightToEntrusted(EntrustedRequestLight entrustedRequest) {

        if (entrustedRequest != null) {

            Entrusted entrusted = new Entrusted();

            entrusted.setEntrustedTo(entrustedRequest.getEntrustedTo());
            entrusted.setEntrustedType(entrustedRequest.getEntrustedType());
            entrusted.setEntrustedEmail(entrustedRequest.getEntrustedEmail());
            if(entrustedRequest.getIdEntrustedTo() != null){
                entrusted.setIdEntrustedTo(UUID.fromString(entrustedRequest.getIdEntrustedTo()));
            }

            return entrusted;
        }

        return null;
    }



    //new adapter


    public static ClaimsEntrustedEntity adptEntrustedEntityToEntrustedNewEntity(Entrusted entrustedEntity) {

            if (entrustedEntity != null) {

                ClaimsEntrustedEntity entrusted = new ClaimsEntrustedEntity();
                entrusted.setAutoEntrust(entrustedEntity.getAutoEntrust());
                entrusted.setDescription(entrustedEntity.getDescription());
                entrusted.setEntrustedDay(DateUtil.convertIS08601StringToUTCInstant(DateUtil.convertUTCDateToIS08601String(entrustedEntity.getEntrustedDay())));
                entrusted.setEntrustedTo(entrustedEntity.getEntrustedTo());
                entrusted.setEntrustedType(entrustedEntity.getEntrustedType());
                entrusted.setExpert(entrustedEntity.getPerito());
                entrusted.setEntrustedEmail(entrustedEntity.getEntrustedEmail());
                entrusted.setDwlMan(DateUtil.convertIS08601StringToUTCInstant(DateUtil.convertUTCDateToIS08601String(entrustedEntity.getDwlMan())));
                if(entrustedEntity.getIdEntrustedTo() != null){
                    entrusted.setIdEntrustedTo(entrustedEntity.getIdEntrustedTo());
                }
                entrusted.setNumberSxCounterparty(entrustedEntity.getNumberSxCounterparty());

                entrusted.setStatus(entrustedEntity.getStatus());
                return entrusted;
            }

            return null;
        }

    public static Entrusted adptFromClaimsEntrustedToEntrustedJsonb(ClaimsEntrustedEntity entrustedJsonb) {

        if (entrustedJsonb != null) {

            Entrusted entrusted = new Entrusted();

            entrusted.setAutoEntrust(entrustedJsonb.getAutoEntrust());
            entrusted.setDescription(entrustedJsonb.getDescription());
            entrusted.setEntrustedDay(DateUtil.convertIS08601StringToUTCDate(DateUtil.convertUTCInstantToIS08601String(entrustedJsonb.getEntrustedDay())));
            entrusted.setEntrustedTo(entrustedJsonb.getEntrustedTo());
            entrusted.setEntrustedType(entrustedJsonb.getEntrustedType());
            entrusted.setPerito(entrustedJsonb.getExpert());
            entrusted.setEntrustedEmail(entrustedJsonb.getEntrustedEmail());
            entrusted.setDwlMan(DateUtil.convertIS08601StringToUTCDate(DateUtil.convertUTCInstantToIS08601String(entrustedJsonb.getDwlMan())));
            if(entrustedJsonb.getIdEntrustedTo() != null){
                entrusted.setIdEntrustedTo(entrustedJsonb.getIdEntrustedTo());
            }
            entrusted.setNumberSxCounterparty(entrustedJsonb.getNumberSxCounterparty());
            entrusted.setStatus(entrustedJsonb.getStatus());
            return entrusted;
        }

        return null;
    }
}
