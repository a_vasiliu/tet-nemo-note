package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.controller.payload.request.ManagerRequestV1;
import com.doing.nemo.claims.controller.payload.response.ManagerResponseV1;
import com.doing.nemo.claims.controller.payload.response.PaginationResponseV1;
import com.doing.nemo.claims.entity.enumerated.DTO.PageStats;
import com.doing.nemo.claims.entity.settings.ManagerEntity;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

@Component
public class ManagerAdapter {

    public static ManagerEntity getManagerEntity(ManagerRequestV1 managerRequestV1) {

        ManagerEntity managerEntity = null;
        if(managerRequestV1 != null) {
            managerEntity = new ManagerEntity();
            managerEntity.setId(managerRequestV1.getId());
            managerEntity.setName(managerRequestV1.getName());
            managerEntity.setAddress(managerRequestV1.getAddress());
            managerEntity.setZipCode(managerRequestV1.getZipCode());
            managerEntity.setLocality(managerRequestV1.getLocality());
            managerEntity.setProv(managerRequestV1.getProv());
            managerEntity.setCountry(managerRequestV1.getCountry());
            managerEntity.setPhone(managerRequestV1.getPhone());
            managerEntity.setFax(managerRequestV1.getFax());
            managerEntity.setEmail(managerRequestV1.getEmail());
            managerEntity.setWebsite(managerRequestV1.getWebsite());
            managerEntity.setContact(managerRequestV1.getContact());
            managerEntity.setExternalExportId(managerRequestV1.getExternalExportId());
            managerEntity.setActive(managerRequestV1.getActive());
        }

        return managerEntity;
    }

    public static ManagerResponseV1 getManagerAdapter(ManagerEntity managerEntity) {

        ManagerResponseV1 managerResponseV1 = null;

        if(managerEntity != null) {
            managerResponseV1 = new ManagerResponseV1();
            managerResponseV1.setId(managerEntity.getId());
            managerResponseV1.setName(managerEntity.getName());
            managerResponseV1.setAddress(managerEntity.getAddress());
            managerResponseV1.setZipCode(managerEntity.getZipCode());
            managerResponseV1.setLocality(managerEntity.getLocality());
            managerResponseV1.setProv(managerEntity.getProv());
            managerResponseV1.setCountry(managerEntity.getCountry());
            managerResponseV1.setPhone(managerEntity.getPhone());
            managerResponseV1.setFax(managerEntity.getFax());
            managerResponseV1.setEmail(managerEntity.getEmail());
            managerResponseV1.setWebsite(managerEntity.getWebsite());
            managerResponseV1.setContact(managerEntity.getContact());
            managerResponseV1.setExternalExportId(managerEntity.getExternalExportId());
            managerResponseV1.setActive(managerEntity.getActive());
            managerResponseV1.setTypeComplaint(managerEntity.getTypeComplaint());
        }

        return managerResponseV1;
    }

    public static List<ManagerResponseV1> getManagerListAdapter(List<ManagerEntity> managerEntityList) {

        List<ManagerResponseV1> managerResponseV1List = new LinkedList<>();

        for (ManagerEntity managerEntity : managerEntityList) {
            managerResponseV1List.add(getManagerAdapter(managerEntity));
        }

        return managerResponseV1List;
    }

    public static PaginationResponseV1<ManagerResponseV1> adptPagination(List<ManagerEntity> managerEntityList, int page, int pageSize, Long itemCount) {

        double totPages = Math.ceil((double) itemCount.intValue() / pageSize);
        PageStats pageStats = new PageStats();
        pageStats.setCurrentPage(page);
        pageStats.setItemCount(itemCount.intValue());
        pageStats.setPageSize(pageSize);
        pageStats.setPageCount((long)totPages);

        PaginationResponseV1<ManagerResponseV1> paginationResponseV1 = new PaginationResponseV1(pageStats,getManagerListAdapter(managerEntityList));

        return paginationResponseV1;
    }
}
