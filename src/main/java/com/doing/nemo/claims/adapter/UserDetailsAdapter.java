package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.controller.payload.request.authority.claims.UserDetailsRequestV1;
import com.doing.nemo.claims.controller.payload.response.authority.claims.UserDetailsResponseV1;
import com.doing.nemo.claims.entity.jsonb.UserDetails;
import org.springframework.stereotype.Component;

@Component
public class UserDetailsAdapter {

    public static UserDetails adptFromUserDetailsRequestToUserDetails(UserDetailsRequestV1 userDetailsRequestV1){
        if(userDetailsRequestV1 == null) return null;
        UserDetails userDetails = new UserDetails();
        userDetails.setUserId(userDetailsRequestV1.getUserId());
        userDetails.setFirstname(userDetailsRequestV1.getFirstname());
        userDetails.setLastname(userDetailsRequestV1.getLastname());

        return userDetails;
    }


    public static UserDetailsResponseV1 adptFromUserDetailsToUserDetailsResponse(UserDetails userDetails){
        if(userDetails == null) return null;
        UserDetailsResponseV1 userDetailsResponseV1 = new UserDetailsResponseV1();
        userDetailsResponseV1.setUserId(userDetails.getUserId());
        userDetailsResponseV1.setFirstname(userDetails.getFirstname());
        userDetailsResponseV1.setLastname(userDetails.getLastname());

        return userDetailsResponseV1;
    }
}
