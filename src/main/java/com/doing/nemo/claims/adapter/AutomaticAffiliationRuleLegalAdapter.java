package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.common.validation.MessageCode;
import com.doing.nemo.claims.common.validation.RequestValidator;
import com.doing.nemo.claims.controller.payload.request.AutomaticAffiliationRuleLegalRequestV1;
import com.doing.nemo.claims.controller.payload.request.ClaimsTypeForRuleRequestV1;
import com.doing.nemo.claims.controller.payload.request.InsuranceCompanyRequestV1;
import com.doing.nemo.claims.entity.enumerated.DamagedEnum.VehicleEnum.VehicleTypeEnum;
import com.doing.nemo.claims.entity.settings.AutomaticAffiliationRuleLegalEntity;
import com.doing.nemo.commons.exception.BadRequestException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

@Component
public class AutomaticAffiliationRuleLegalAdapter {

    private static Logger LOGGER = LoggerFactory.getLogger(AutomaticAffiliationRuleLegalAdapter.class);
    @Autowired
    private RequestValidator requestValidator;

    public AutomaticAffiliationRuleLegalEntity adptFromAARuleLegalRequToAARuleLegalEntity(AutomaticAffiliationRuleLegalRequestV1 automaticAffiliationRuleLegalRequestV1, HttpMethod httpMethod) {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/yyyy");
        String dataStr = sdf.format(new Date());
        AutomaticAffiliationRuleLegalEntity automaticAffiliationRuleLegalEntity = new AutomaticAffiliationRuleLegalEntity();

        automaticAffiliationRuleLegalEntity.setSelectionName(automaticAffiliationRuleLegalRequestV1.getSelectionName());
        automaticAffiliationRuleLegalEntity.setMonthlyVolume(automaticAffiliationRuleLegalRequestV1.getMonthlyVolume());
        automaticAffiliationRuleLegalEntity.setCurrentMonth(dataStr);

        if (httpMethod.equals(HttpMethod.POST)) automaticAffiliationRuleLegalEntity.setCurrentVolume(0);

        else
            automaticAffiliationRuleLegalEntity.setCurrentVolume(automaticAffiliationRuleLegalRequestV1.getCurrentVolume());
        automaticAffiliationRuleLegalEntity.setDetail(automaticAffiliationRuleLegalRequestV1.getDetail());
        automaticAffiliationRuleLegalEntity.setAnnotations(automaticAffiliationRuleLegalRequestV1.getAnnotations());
        automaticAffiliationRuleLegalEntity.setComplaintsWithoutCtp(automaticAffiliationRuleLegalRequestV1.getComplaintsWithoutCtp());
        automaticAffiliationRuleLegalEntity.setForeignCountryClaim(automaticAffiliationRuleLegalRequestV1.getForeignCountryClaim());
        automaticAffiliationRuleLegalEntity.setMinImport(automaticAffiliationRuleLegalRequestV1.getMinImport());
        automaticAffiliationRuleLegalEntity.setMaxImport(automaticAffiliationRuleLegalRequestV1.getMaxImport());


        if (automaticAffiliationRuleLegalRequestV1.getCounterpartType() != null) {
            VehicleTypeEnum counterpartType = automaticAffiliationRuleLegalRequestV1.getCounterpartType();
            automaticAffiliationRuleLegalEntity.setCounterpartType(counterpartType);
        }
        automaticAffiliationRuleLegalEntity.setForeignCountryClaim(automaticAffiliationRuleLegalRequestV1.getForeignCountryClaim());
        automaticAffiliationRuleLegalEntity.setClaimWithInjured(automaticAffiliationRuleLegalRequestV1.getClaimWithInjured());
        automaticAffiliationRuleLegalEntity.setDamageCanNotBeRepaired(automaticAffiliationRuleLegalRequestV1.getDamageCanNotBeRepaired());
        //automaticAffiliationRuleLegalEntity.setClaimsType(automaticAffiliationRuleLegalRequestV1.getClaimsType());
        //automaticAffiliationRuleLegalEntity.setDamagedInsuranceCompanyList(automaticAffiliationRuleLegalRequestV1.getDamagedInsuranceCompanyList());
        //automaticAffiliationRuleLegalEntity.setCounterpartInsuranceCompanyList(automaticAffiliationRuleLegalRequestV1.getCounterpartInsuranceCompanyList());


        if (automaticAffiliationRuleLegalRequestV1.getClaimsType() != null) {
            for (ClaimsTypeForRuleRequestV1 claimRequest : automaticAffiliationRuleLegalRequestV1.getClaimsType())
                requestValidator.validateRequest(claimRequest, MessageCode.E00X_1000);
            automaticAffiliationRuleLegalEntity.setClaimsType(ClaimsTypeAdapter.adptClaimsTyreRequestToClaimsType(automaticAffiliationRuleLegalRequestV1.getClaimsType()));
        }

        if (automaticAffiliationRuleLegalRequestV1.getDamagedInsuranceCompanyList() != null) {
            for (InsuranceCompanyRequestV1 insuranceCompanyRequest : automaticAffiliationRuleLegalRequestV1.getDamagedInsuranceCompanyList())
                requestValidator.validateRequest(insuranceCompanyRequest, MessageCode.E00X_1000);
            automaticAffiliationRuleLegalEntity.setDamagedInsuranceCompanyList(InsuranceCompanyAdapter.adptFromICompRequestToICompEntityList(automaticAffiliationRuleLegalRequestV1.getDamagedInsuranceCompanyList()));
        }

        if (automaticAffiliationRuleLegalRequestV1.getCounterpartInsuranceCompanyList() != null) {
            for (InsuranceCompanyRequestV1 insuranceCompanyRequest : automaticAffiliationRuleLegalRequestV1.getCounterpartInsuranceCompanyList())
                requestValidator.validateRequest(insuranceCompanyRequest, MessageCode.E00X_1000);
            automaticAffiliationRuleLegalEntity.setCounterpartInsuranceCompanyList(InsuranceCompanyAdapter.adptFromICompRequestToICompEntityList(automaticAffiliationRuleLegalRequestV1.getCounterpartInsuranceCompanyList()));
        }

        return automaticAffiliationRuleLegalEntity;
    }

    public List<AutomaticAffiliationRuleLegalEntity> adptFromAARuleLegalRequToAARuleLegalEntityList(List<AutomaticAffiliationRuleLegalRequestV1> automaticAffiliationRuleLegalRequestV1List, HttpMethod httpMethod) {
        List<AutomaticAffiliationRuleLegalEntity> responseV1 = new LinkedList<>();
        if (automaticAffiliationRuleLegalRequestV1List != null) {
            for (AutomaticAffiliationRuleLegalRequestV1 automaticAffiliationRuleLegalRequestV1 : automaticAffiliationRuleLegalRequestV1List) {
                if (automaticAffiliationRuleLegalRequestV1.getCurrentVolume() == null || automaticAffiliationRuleLegalRequestV1.getMonthlyVolume() == null) {
                    LOGGER.debug(com.doing.nemo.claims.validation.MessageCode.CLAIMS_1010.value());
                    throw new BadRequestException("Current volume and Monthly volume must not be null", com.doing.nemo.claims.validation.MessageCode.CLAIMS_1010);
                }
                //if (httpMethod.equals(HttpMethod.PUT) && automaticAffiliationRuleLegalRequestV1.getCurrentVolume() > automaticAffiliationRuleLegalRequestV1.getMonthlyVolume()) throw new BadRequestException("Current volume must be smaller than Monthly volume",com.doing.nemo.claims.validation.MessageCode.CLAIMS_1010);
                responseV1.add(this.adptFromAARuleLegalRequToAARuleLegalEntity(automaticAffiliationRuleLegalRequestV1, httpMethod));
            }
        }
        return responseV1;
    }
}
