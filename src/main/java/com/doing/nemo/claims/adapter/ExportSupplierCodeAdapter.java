package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.controller.payload.request.ExportSupplierCodeRequestV1;
import com.doing.nemo.claims.controller.payload.response.ClaimsResponsePaginationV1;
import com.doing.nemo.claims.controller.payload.response.ExportSupplierCodeResponseV1;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.settings.ExportSupplierCodeEntity;
import com.doing.nemo.claims.repository.ExportSupplierCodeRepository;
import com.doing.nemo.commons.exception.NotFoundException;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
public class ExportSupplierCodeAdapter {

    private static Logger LOGGER = LoggerFactory.getLogger(ExportSupplierCodeAdapter.class);
    @Autowired
    private ExportSupplierCodeRepository exportSupplierCodeRepository;

    public static ExportSupplierCodeEntity adptFromExportSupplierCodeRequestToExportSupplierCodeEntity(ExportSupplierCodeRequestV1 exportSupplierCodeRequestV1) {
        ExportSupplierCodeEntity exportSupplierCodeEntity = new ExportSupplierCodeEntity();
        if (exportSupplierCodeRequestV1 != null) {
            exportSupplierCodeEntity.setCodeToExport(exportSupplierCodeRequestV1.getCodeToExport());
            exportSupplierCodeEntity.setDenomination(exportSupplierCodeRequestV1.getDenomination());
            exportSupplierCodeEntity.setVatNumber(exportSupplierCodeRequestV1.getVatNumber());
            exportSupplierCodeEntity.setActive(exportSupplierCodeRequestV1.getActive());

        }
        return exportSupplierCodeEntity;
    }

    public ExportSupplierCodeEntity adptFromExportSupplierCodeRequestToExportSupplierCodeEntityWithID(ExportSupplierCodeRequestV1 exportSupplierCodeRequestV1, UUID uuid) {
        Optional<ExportSupplierCodeEntity> exportSupplierCodeEntity = exportSupplierCodeRepository.findById(uuid);
        if (!exportSupplierCodeEntity.isPresent()) {
            LOGGER.debug("Export claims type with id " + uuid + " not found");
            throw new NotFoundException("Export claims type with id " + uuid + " not found", com.doing.nemo.claims.validation.MessageCode.CLAIMS_1010);
        }
        ExportSupplierCodeEntity exportSupplierCodeEntity1 = exportSupplierCodeEntity.get();
        ExportSupplierCodeEntity exportSupplierCodeEntity2 = adptFromExportSupplierCodeRequestToExportSupplierCodeEntity(exportSupplierCodeRequestV1);
        exportSupplierCodeEntity2.setId(exportSupplierCodeEntity1.getId());
        return exportSupplierCodeEntity2;
    }

    public static ExportSupplierCodeResponseV1 adptFromExportSupplierCodeEntityToExportSupplierCodeResponse(ExportSupplierCodeEntity exportSupplierCodeEntity) {
        ExportSupplierCodeResponseV1 exportSupplierCodeResponseV1 = new ExportSupplierCodeResponseV1();
        if (exportSupplierCodeEntity != null) {
            exportSupplierCodeResponseV1.setId(exportSupplierCodeEntity.getId());
            exportSupplierCodeResponseV1.setCodeToExport(exportSupplierCodeEntity.getCodeToExport());
            exportSupplierCodeResponseV1.setDenomination(exportSupplierCodeEntity.getDenomination());
            exportSupplierCodeResponseV1.setVatNumber(exportSupplierCodeEntity.getVatNumber());
            exportSupplierCodeResponseV1.setActive(exportSupplierCodeEntity.getActive());
        }
        return exportSupplierCodeResponseV1;
    }

    public static List<ExportSupplierCodeResponseV1> adptFromExportSupplierCodeEntityToExportSupplierCodeResponseList(List<ExportSupplierCodeEntity> exportSupplierCodeEntityList) {
        List<ExportSupplierCodeResponseV1> responseV1 = new LinkedList<>();
        for (ExportSupplierCodeEntity exportSupplierCodeEntity : exportSupplierCodeEntityList) {
            responseV1.add(adptFromExportSupplierCodeEntityToExportSupplierCodeResponse(exportSupplierCodeEntity));
        }
        return responseV1;
    }

    public static ClaimsResponsePaginationV1 adptFromExportSupplierCodePaginationToNotificationResponsePagination(Pagination<ExportSupplierCodeEntity> pagination) {

        ClaimsResponsePaginationV1<ExportSupplierCodeResponseV1> claimsPaginationResponse = new ClaimsResponsePaginationV1<>();

        claimsPaginationResponse.setStats(pagination.getStats());
        claimsPaginationResponse.setItems(adptFromExportSupplierCodeEntityToExportSupplierCodeResponseList(pagination.getItems()));

        return claimsPaginationResponse;
    }
}
