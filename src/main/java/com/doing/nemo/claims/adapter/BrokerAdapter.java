package com.doing.nemo.claims.adapter;


import com.doing.nemo.claims.controller.payload.request.BrokerRequestV1;
import com.doing.nemo.claims.controller.payload.response.BrokerResponseV1;
import com.doing.nemo.claims.controller.payload.response.PaginationResponseV1;
import com.doing.nemo.claims.entity.enumerated.DTO.PageStats;
import com.doing.nemo.claims.entity.settings.BrokerEntity;
import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.util.LinkedList;
import java.util.List;

@Component
public class BrokerAdapter {


    public static BrokerEntity adptFromBrokerRequestToBrokerEntity(BrokerRequestV1 brokerRequestV1) {
        BrokerEntity brokerEntity = new BrokerEntity();
        brokerEntity.setBusinessName(brokerRequestV1.getBusinessName());
        brokerEntity.setAddress(brokerRequestV1.getAddress());
        brokerEntity.setZipCode(brokerRequestV1.getZipCode());
        brokerEntity.setCity(brokerRequestV1.getCity());
        brokerEntity.setProvince(brokerRequestV1.getProvince());
        brokerEntity.setState(brokerRequestV1.getState());
        brokerEntity.setPhoneNumber(brokerRequestV1.getPhoneNumber());
        brokerEntity.setFax(brokerRequestV1.getFax());
        brokerEntity.setEmail(brokerRequestV1.getEmail());
        brokerEntity.setWebSite(brokerRequestV1.getWebSite());
        brokerEntity.setContact(brokerRequestV1.getContact());
        brokerEntity.setActive(brokerRequestV1.getActive());


        return brokerEntity;
    }


    public static BrokerResponseV1<BrokerEntity> adptFromBrokerEntityToBrokerResponseV1(BrokerEntity brokerEntity) {
        BrokerResponseV1<BrokerEntity> brokerResponseV1 = new BrokerResponseV1<>();
        brokerResponseV1.setId(brokerEntity.getId());
        brokerResponseV1.setBusinessName(brokerEntity.getBusinessName());
        brokerResponseV1.setAddress(brokerEntity.getAddress());
        brokerResponseV1.setZipCode(brokerEntity.getZipCode());
        brokerResponseV1.setCity(brokerEntity.getCity());
        brokerResponseV1.setProvince(brokerEntity.getProvince());
        brokerResponseV1.setState(brokerEntity.getState());
        brokerResponseV1.setPhoneNumber(brokerEntity.getPhoneNumber());
        brokerResponseV1.setFax(brokerEntity.getFax());
        brokerResponseV1.setEmail(brokerEntity.getEmail());
        brokerResponseV1.setWebSite(brokerEntity.getWebSite());
        brokerResponseV1.setContact(brokerEntity.getContact());
        brokerResponseV1.setActive(brokerEntity.getActive());


        return brokerResponseV1;
    }

    public static List<BrokerResponseV1<BrokerEntity>> adptFromBrokerEntityToBrokerResponseV1List(List<BrokerEntity> brokerEntityList) {
        List<BrokerResponseV1<BrokerEntity>> brokerResponseV1 = new LinkedList<>();
        for (BrokerEntity brokersEntity : brokerEntityList) {
            brokerResponseV1.add(adptFromBrokerEntityToBrokerResponseV1(brokersEntity));
        }
        return brokerResponseV1;
    }

    public static PaginationResponseV1<BrokerResponseV1> adptPagination(List<BrokerEntity> brokerEntityList, int page, int pageSize, BigInteger itemCount) {

        double totPages = Math.ceil((double) itemCount.intValue() / pageSize);
        PageStats pageStats = new PageStats();
        pageStats.setCurrentPage(page);
        pageStats.setItemCount(itemCount.intValue());
        pageStats.setPageSize(pageSize);
        pageStats.setPageCount((long)totPages);

        PaginationResponseV1<BrokerResponseV1> paginationResponseV1 = new PaginationResponseV1(pageStats,adptFromBrokerEntityToBrokerResponseV1List(brokerEntityList));

        return paginationResponseV1;
    }

}