package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.entity.esb.VehicleESB.VehicleEsb;
import com.doing.nemo.middleware.client.payload.response.MiddlewareVehicleResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class VehicleEsbResponseAdapter {
    @Autowired
    PlateRegistrationAdapter plateRegistrationAdapter;

    public VehicleEsb adaptToDTO(MiddlewareVehicleResponse input, VehicleEsb output) {

        if (input == null) {
            return output;
        }


        if (output == null) {
            output = new VehicleEsb();
        }

        output.setNetBookValue(input.getNetBookValue()!=null? input.getNetBookValue().doubleValue(): null);
        output.setFleetVehicleId(input.getFleetVehicleId());
        output.setMake(input.getMake());
        output.setModel(input.getModel());
        output.setModelYear(input.getModelYear());
        output.setKw(input.getKw() != null? String.valueOf(input.getKw()): null);
        output.setCc3(input.getCc3() !=null ?String.valueOf(input.getCc3()): null);
        output.setHp(input.getHp()!=null ? input.getHp().intValue() :null);
        output.setDinHp(input.getDinHp()!=null ? input.getDinHp().intValue() : null);
        output.setSeats(input.getSeats()!=null ? input.getSeats().intValue() : null);
        output.setNetWeight(input.getNetWeight()!=null ? input.getNetWeight().doubleValue(): null);
        output.setMaxWeight(input.getMaxWeight()!=null? input.getMaxWeight().doubleValue(): null);
        output.setDoors(input.getDoors());
        output.setBodyStyle(input.getBodyStyle());
        output.setNature(input.getNature());
        output.setFuelType(input.getFuelType());
        output.setCo2emission(input.getCo2Emission()!=null? String.valueOf(input.getCo2Emission()): null);
        output.setConsumption(input.getConsumption()!=null?input.getConsumption().toString():null);
        output.setLicensePlate(input.getLicensePlate());
        output.setChassisNumber(input.getChassisNumber());
        output.setLastKnownMileage(input.getLastKnownMileage()!=null?String.valueOf(input.getLastKnownMileage()): null);
        try {
            output.setRegistrationDate(input.getRegistrationDate()!=null ? new SimpleDateFormat("dd/MM/yyyy").parse(input.getRegistrationDate()) :null);
        } catch (ParseException e) {
            output.setRegistrationDate(null);
        }
        output.setExternalColor(input.getExternalColor());
        output.setInternalColor(input.getInternalColor());

//output.setRelatedContractsList(input.getRelatedContracts());

        return output;
    }
}
