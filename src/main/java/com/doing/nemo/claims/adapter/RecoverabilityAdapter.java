package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.controller.payload.request.RecoverabilityRequestV1;
import com.doing.nemo.claims.controller.payload.response.ClaimsResponsePaginationV1;
import com.doing.nemo.claims.controller.payload.response.RecoverabilityResponseV1;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.settings.ClaimsTypeEntity;
import com.doing.nemo.claims.entity.settings.RecoverabilityEntity;
import com.doing.nemo.claims.repository.ClaimsTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Component
public class RecoverabilityAdapter {
    @Autowired
    private ClaimsTypeRepository claimsTypeRepository;

    public RecoverabilityEntity adptFromRecoverabilityRequestToRecoverabilityEntity(RecoverabilityRequestV1 recoverabilityRequestV1) {

        if(recoverabilityRequestV1 == null)
            return null;

        RecoverabilityEntity recoverabilityEntity = new RecoverabilityEntity();
        recoverabilityEntity.setManagement(recoverabilityRequestV1.getManagement());
        if (recoverabilityRequestV1.getClaimsType() != null) {
            Optional<ClaimsTypeEntity> claimsTypeRepositoryById = claimsTypeRepository.findById(recoverabilityRequestV1.getClaimsType().getId());
            if (claimsTypeRepositoryById.isPresent())
                recoverabilityEntity.setClaimsType(claimsTypeRepositoryById.get());
        }
        recoverabilityEntity.setCounterpart(recoverabilityRequestV1.getCounterpart());
        recoverabilityEntity.setRecoverability(recoverabilityRequestV1.getRecoverability());
        recoverabilityEntity.setPercentRecoverability(recoverabilityRequestV1.getPercentRecoverability());
        recoverabilityEntity.setActive(recoverabilityRequestV1.getActive());

        return recoverabilityEntity;
    }


    public static RecoverabilityResponseV1 adptFromRecoverabilityEntityToRecoverabilityResponseV1(RecoverabilityEntity recoverabilityEntity) {
        RecoverabilityResponseV1 recoverabilityResponseV1 = new RecoverabilityResponseV1();
        if (recoverabilityEntity == null) return null;
        recoverabilityResponseV1.setId(recoverabilityEntity.getId());
        recoverabilityResponseV1.setManagement(recoverabilityEntity.getManagement());
        recoverabilityResponseV1.setClaimsType(recoverabilityEntity.getClaimsType());
        recoverabilityResponseV1.setCounterpart(recoverabilityEntity.getCounterpart());
        recoverabilityResponseV1.setRecoverability(recoverabilityEntity.getRecoverability());
        recoverabilityResponseV1.setPercentRecoverability(recoverabilityEntity.getPercentRecoverability());
        recoverabilityResponseV1.setActive(recoverabilityEntity.getActive());


        return recoverabilityResponseV1;
    }


    public static List<RecoverabilityResponseV1> adptFromRecoverabilityEntityToRecoverabilityResponseV1List(List<RecoverabilityEntity> recoverabilityEntityList) {
        List<RecoverabilityResponseV1> recoverabilityResponseV1 = new LinkedList<>();
        for (RecoverabilityEntity recoverabilitysEntity : recoverabilityEntityList) {
            recoverabilityResponseV1.add(adptFromRecoverabilityEntityToRecoverabilityResponseV1(recoverabilitysEntity));
        }
        return recoverabilityResponseV1;
    }

    public static ClaimsResponsePaginationV1 adptRecoverabilityPaginationToClaimsResponsePagination(Pagination<RecoverabilityEntity> pagination) {

        ClaimsResponsePaginationV1<RecoverabilityResponseV1> claimsPaginationResponse = new ClaimsResponsePaginationV1<>();

        claimsPaginationResponse.setStats(pagination.getStats());
        claimsPaginationResponse.setItems(adptFromRecoverabilityEntityToRecoverabilityResponseV1List(pagination.getItems()));

        return claimsPaginationResponse;
    }

}