package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.controller.payload.request.ClaimsInsertExternalRequest;
import com.doing.nemo.claims.controller.payload.request.complaint.RepairRequest;
import com.doing.nemo.claims.controller.payload.response.complaint.RepairResponse;
import com.doing.nemo.claims.controller.payload.response.external.RepairExternalResponse;
import com.doing.nemo.claims.entity.jsonb.complaint.jsonbComplaint.Repair;
import org.springframework.stereotype.Component;

@Component
public class RepairAdapter {

    public static Repair adptRepairRequestToRepair(RepairRequest repairRequest) {

        if (repairRequest != null) {

            Repair repair = new Repair();
            repair.setAddress(repairRequest.getAddress());
            repair.setBlocksRepair(repairRequest.getBlocksRepair());
            repair.setEmail(repairRequest.getEmail());
            repair.setMotivation(repairRequest.getMotivation());
            repair.setPhone(repairRequest.getPhone());
            repair.setRepairer(repairRequest.getRepairer());
            repair.setStatusRepair(repairRequest.getStatusRepair());
            repair.setUnrepairable(repairRequest.getUnrepairable());
            repair.setVehicleValue(repairRequest.getVehicleValue());

            return repair;
        }
        return null;
    }

    public static Repair adptRepairExternalRequestToRepair(ClaimsInsertExternalRequest.ComplaintExternalRequest.RepairExternalRequest repairRequest) {

        if (repairRequest != null) {

            Repair repair = new Repair();
            repair.setAddress(repairRequest.getAddress());
            repair.setBlocksRepair(repairRequest.getBlocksRepair());
            repair.setEmail(repairRequest.getEmail());
            repair.setMotivation(repairRequest.getMotivation());
            repair.setPhone(repairRequest.getPhone());
            repair.setRepairer(repairRequest.getRepairer());
            repair.setUnrepairable(repairRequest.getUnrepairable());
            repair.setVehicleValue(repairRequest.getVehicleValue());

            return repair;
        }
        return null;
    }

    public static RepairResponse adptRepairToRepairReaponse(Repair repair) {

        if (repair != null) {

            RepairResponse repairResponse = new RepairResponse();
            repairResponse.setAddress(repair.getAddress());
            repairResponse.setBlocksRepair(repair.getBlocksRepair());
            repairResponse.setEmail(repair.getEmail());
            repairResponse.setMotivation(repair.getMotivation());
            repairResponse.setPhone(repair.getPhone());
            repairResponse.setRepairer(repair.getRepairer());
            repairResponse.setStatusRepair(repair.getStatusRepair());
            repairResponse.setUnrepairable(repair.getUnrepairable());
            repairResponse.setVehicleValue(repair.getVehicleValue());

            return repairResponse;
        }
        return null;

    }

    public static RepairExternalResponse adptRepairToRepairExternalResponse(Repair repair) {

        if (repair != null) {

            RepairExternalResponse repairResponse = new RepairExternalResponse();
            repairResponse.setAddress(repair.getAddress());
            repairResponse.setBlocksRepair(repair.getBlocksRepair());
            repairResponse.setEmail(repair.getEmail());
            repairResponse.setMotivation(repair.getMotivation());
            repairResponse.setPhone(repair.getPhone());
            repairResponse.setRepairer(repair.getRepairer());
            repairResponse.setUnrepairable(repair.getUnrepairable());
            repairResponse.setVehicleValue(repair.getVehicleValue());

            return repairResponse;
        }
        return null;

    }
}
