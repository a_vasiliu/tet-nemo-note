package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.authority.claims.PODetailsRequestV1;
import com.doing.nemo.claims.controller.payload.response.authority.claims.PODetailsResponseV1;
import com.doing.nemo.claims.entity.jsonb.PODetails;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PODetailsAdapter {

    public static PODetails adptFromPODetailsRequestToPODetails(PODetailsRequestV1 poDetailsRequestV1){
        if(poDetailsRequestV1 == null) return null;
        PODetails poDetails = new PODetails();
        poDetails.setCreatedAt(DateUtil.getNowDate());
        poDetails.setPoAmount(poDetailsRequestV1.getPoAmount());
        poDetails.setPoNumber(poDetailsRequestV1.getPoNumber());
        poDetails.setPoTarget(poDetailsRequestV1.getPoTarget());
        return poDetails;
    }

    public static  PODetailsResponseV1 adptFromPODetailsToPODetailsResponse(PODetails poDetails){
        if(poDetails == null) return null;
        PODetailsResponseV1 poDetailsResponseV1 = new PODetailsResponseV1();
        poDetailsResponseV1.setCreatedAt(DateUtil.convertUTCDateToIS08601String(poDetails.getCreatedAt()));
        poDetailsResponseV1.setPoAmount(poDetails.getPoAmount());
        poDetailsResponseV1.setPoNumber(poDetails.getPoNumber());
        poDetailsResponseV1.setPoTarget(poDetails.getPoTarget());
        poDetailsResponseV1.setIdFilemanager(poDetails.getIdFilemanager());
        return poDetailsResponseV1;
    }

    public static List<PODetailsResponseV1> adptFromPODetailsToPODetailsResponse(List<PODetails> poDetails){
        if(poDetails == null) return null;
        List<PODetailsResponseV1> poDetailsResponseV1s = new ArrayList<>();
        for(PODetails poDetails1 : poDetails){
            poDetailsResponseV1s.add(adptFromPODetailsToPODetailsResponse(poDetails1));
        }

        return poDetailsResponseV1s;
    }

    public static List<PODetails> adptFromPODetailsRequestToPODetails(List<PODetailsRequestV1> poDetailsRequestV1s){
        if(poDetailsRequestV1s == null) return null;
        List<PODetails> poDetailsResponseV1s = new ArrayList<>();
        for(PODetailsRequestV1 poDetails1 : poDetailsRequestV1s){
            poDetailsResponseV1s.add(adptFromPODetailsRequestToPODetails(poDetails1));
        }

        return poDetailsResponseV1s;
    }

}
