package com.doing.nemo.claims.adapter;


import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.request.practice.PracticeRequestV1;
import com.doing.nemo.claims.controller.payload.response.PaginationResponseV1;
import com.doing.nemo.claims.controller.payload.response.practice.PracticeResponseV1;
import com.doing.nemo.claims.entity.enumerated.DTO.PageStats;
import com.doing.nemo.claims.entity.settings.PracticeEntity;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

@Component
public class PracticeAdapter {

    public static PracticeEntity adptPracticeRequestToPracticeEntity(PracticeRequestV1 practiceRequestV1) {
        PracticeEntity practiceEntity = new PracticeEntity();

        practiceEntity.setStatus(practiceRequestV1.getStatus());
        practiceEntity.setPracticeType(practiceRequestV1.getPracticeType());
        practiceEntity.setClaimsId(practiceRequestV1.getClaimsId());
        //practiceEntity.setParentId(practiceRequestV1.getParentId());
        practiceEntity.setProcessingList(ProcessingAdapter.adptProcessingRequestToProcessing(practiceRequestV1.getProcessingRequestV1List()));
        //practiceEntity.setAttachmentList(practiceRequestV1.getAttachmentList());
        practiceEntity.setContract(ContractAdapter.adptContractRequestToContract(practiceRequestV1.getContract()));
        practiceEntity.setCustomer(CustomerAdapter.adptCustomerRequestToCustomer(practiceRequestV1.getCustomer()));
        practiceEntity.setVehicle(VehicleAdapter.adptVehicleRequestToVehicle(practiceRequestV1.getVehicle()));
        practiceEntity.setFleetManagers(FleetManagerAdpter.adptFromFMRequestToFM(practiceRequestV1.getFleetManagers()));
        practiceEntity.setSeizure(SeizureAdapter.adptFromSeizureRequestToSeizure(practiceRequestV1.getSeizure()));
        practiceEntity.setReleaseFromSeizure(ReleaseFromSeizureAdapter.adptFromReleaseFromSeizureRequestToReleaseFromSeizure(practiceRequestV1.getReleaseFromSeizure()));
        practiceEntity.setFinding(FindingAdapter.adptFromFindingRequestToFinding(practiceRequestV1.getFinding()));
        practiceEntity.setMisappropriation(MisappropriationAdapter.adptFromMisappropriationRequestToMisappropriation(practiceRequestV1.getMisappropriation()));
        practiceEntity.setTheft(TheftPracticeAdapter.fromTheftPracticeResponseV1ToTheft(practiceRequestV1.getTheft()));
        return practiceEntity;
    }


    public static PracticeResponseV1 adptPracticeEntityToPracticeResponseV1(PracticeEntity practiceEntity) {
        PracticeResponseV1 practiceResponseV1 = new PracticeResponseV1 ();

        practiceResponseV1.setId(practiceEntity.getId());
        practiceResponseV1.setPracticeId(practiceEntity.getPracticeId());
        //practiceResponseV1.setParentId(practiceEntity.getParentId());
        practiceResponseV1.setCreatedAt(DateUtil.convertUTCInstantToIS08601String(practiceEntity.getCreatedAt()));
        practiceResponseV1.setCreatedBy(practiceEntity.getCreatedBy());
        practiceResponseV1.setUpdatedAt(DateUtil.convertUTCInstantToIS08601String(practiceEntity.getUpdatedAt()));
        practiceResponseV1.setStatus(practiceEntity.getStatus());
        practiceResponseV1.setPracticeType(practiceEntity.getPracticeType());
        practiceResponseV1.setClaimsId(practiceEntity.getClaimsId());
        practiceResponseV1.setProcessingList(ProcessingAdapter.adptProcessingToProcessingResponse(practiceEntity.getProcessingList()));
        practiceResponseV1.setAttachmentList(practiceEntity.getAttachmentList());
        practiceResponseV1.setContract(ContractAdapter.adptContractToContractResponse(practiceEntity.getContract()));
        practiceResponseV1.setCustomer(CustomerAdapter.adptCustomerToCustomerResposne(practiceEntity.getCustomer()));
        practiceResponseV1.setVehicle(VehicleAdapter.adptVehicleToVehicleResponse(practiceEntity.getVehicle()));
        practiceResponseV1.setFleetManagers(FleetManagerAdpter.adptFromFMToFMResponse(practiceEntity.getFleetManagers()));
        practiceResponseV1.setSeizure(SeizureAdapter.adptFromSeizureToSeizureResponse(practiceEntity.getSeizure()));
        practiceResponseV1.setReleaseFromSeizure(ReleaseFromSeizureAdapter.adptFromReleaseFromSeizureToReleaseFromSeizureResponse(practiceEntity.getReleaseFromSeizure()));
        practiceResponseV1.setFinding(FindingAdapter.adptFromFindingToFindingResponse(practiceEntity.getFinding()));
        practiceResponseV1.setMisappropriation(MisappropriationAdapter.adptFromMisappropriationToMisappropriationResponse(practiceEntity.getMisappropriation()));
        practiceResponseV1.setTheft(TheftPracticeAdapter.adtpFromTheftPracticeToTheftPracticeResponseV1(practiceEntity.getTheft()));
        return practiceResponseV1;
    }

    public static List<PracticeResponseV1> adptPracticeEntityToPracticeResponseV1List(List<PracticeEntity> practiceEntityList) {
        List<PracticeResponseV1> practiceResponseV1 = new LinkedList<>();
        for (PracticeEntity practicesEntity : practiceEntityList) {
            practiceResponseV1.add(adptPracticeEntityToPracticeResponseV1(practicesEntity));
        }
        return practiceResponseV1;
    }


    public static PaginationResponseV1<PracticeResponseV1> adptPagination(List<PracticeEntity> practiceEntityList, int page, int pageSize, Long itemCount) {

        double totPages = Math.ceil((double) itemCount.intValue() / pageSize);
        PageStats pageStats = new PageStats();
        pageStats.setCurrentPage(page);
        pageStats.setItemCount(itemCount.intValue());
        pageStats.setPageSize(pageSize);
        pageStats.setPageCount((long) totPages);

        PaginationResponseV1<PracticeResponseV1> searchPracticeResponseV1 = new PaginationResponseV1<>(pageStats, adptPracticeEntityToPracticeResponseV1List(practiceEntityList));

        return searchPracticeResponseV1;
    }

}