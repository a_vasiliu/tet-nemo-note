package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.common.util.DateUtil;
import com.doing.nemo.claims.controller.payload.response.authority.claims.*;
import com.doing.nemo.claims.controller.payload.response.authority.practice.AuthorityPracticePaginationResponseV1;
import com.doing.nemo.claims.controller.payload.response.authority.repair.AuthorityPaginationRepairResponseV1;
import com.doing.nemo.claims.controller.payload.response.authority.repair.AuthorityRepairResponseV1;
import com.doing.nemo.claims.entity.*;
import com.doing.nemo.claims.entity.claims.ClaimsAuthorityEntity;
import com.doing.nemo.claims.entity.enumerated.WorkingStatusEnum;
import com.doing.nemo.claims.entity.jsonb.Authority;
import com.doing.nemo.claims.entity.jsonb.Processing;
import com.doing.nemo.claims.entity.jsonb.ProcessingAuthorityResponse;
import com.doing.nemo.claims.entity.jsonb.UserDetails;
import com.doing.nemo.claims.entity.settings.PracticeEntity;
import com.doing.nemo.claims.repository.ClaimsAuthorityEmbeddedRepositorty;
import com.doing.nemo.claims.repository.ClaimsAuthorityRepositorty;
import com.doing.nemo.claims.service.GoLiveStrategyService;
import com.doing.nemo.claims.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class AuthorityAdapter {

    @Autowired
    private GoLiveStrategyService goLiveStrategyService;


    @Autowired
    private ClaimsAuthorityRepositorty authorityRepositorty;


    public static AuthorityResponseV1 adptFromAuthorityToAuthorityResponse(Authority authorityJsonb){

        if(authorityJsonb != null){
            AuthorityResponseV1 authority = new AuthorityResponseV1();
            authority.setInvoiced(authorityJsonb.getInvoiced());
            authority.setAuthorityDossierId(authorityJsonb.getAuthorityDossierId());
            authority.setAuthorityWorkingId(authorityJsonb.getAuthorityWorkingId());
            authority.setTotal(authorityJsonb.getTotal());
            authority.setWorkingNumber(authorityJsonb.getWorkingNumber());
            authority.setAcceptingDate(DateUtil.convertUTCDateToIS08601String(authorityJsonb.getAcceptingDate()));
            authority.setAuthorizationDate(DateUtil.convertUTCDateToIS08601String(authorityJsonb.getAuthorizationDate()));
            authority.setRejectionDate(DateUtil.convertUTCDateToIS08601String(authorityJsonb.getRejectionDate()));
            authority.setRejection(authorityJsonb.getRejection());
            authority.setWorkingCreatedAt(DateUtil.convertUTCDateToIS08601String(authorityJsonb.getWorkingCreatedAt()));
            authority.setNumberFranchise(authorityJsonb.getNumberFranchise());
            authority.setWreck(authorityJsonb.getWreck());
            authority.setWreckCasual(authorityJsonb.getWreckCasual());
            authority.setEventDate(DateUtil.convertUTCDateToIS08601String(authorityJsonb.getEventDate()));
            authority.setEventType(authorityJsonb.getEventType());
            authority.setPoDetails(PODetailsAdapter.adptFromPODetailsToPODetailsResponse(authorityJsonb.getPoDetails()));
            //authority.setStatus(authorityJsonb.getStatus());
            authority.setType(authorityJsonb.getType());
            authority.setNotDuplicate(authorityJsonb.getNotDuplicate());
            authority.setOldest(authorityJsonb.getOldest());
            authority.setAuthorityDossierNumber(authorityJsonb.getAuthorityDossierNumber());
            authority.setCommodityDetails(CommodityDetailsAdapter.adptFromCommodityDetailsToCommodityDetailsResponse(authorityJsonb.getCommodityDetails()));
            authority.setWorkingStatus(authorityJsonb.getWorkingStatus());
            authority.setUserDetails(UserDetailsAdapter.adptFromUserDetailsToUserDetailsResponse(authorityJsonb.getUserDetails()));
            authority.setNbv(authorityJsonb.getNbv());
            authority.setWreckValue(authorityJsonb.getWreckValue());
            authority.setClaimsLinkedSize(authorityJsonb.getClaimsLinkedSize());
            authority.setHistorical(HistoricalAuthorityAdapter.adptHistoricalAuthorityToHistoricalAuthorityResponse(authorityJsonb.getHistorical()));
            authority.setInvoiced(authorityJsonb.getInvoiced());
            authority.setNoteRejection(authorityJsonb.getNoteRejection());
            return authority;
        }
        return null;
    }

    public static List<AuthorityResponseV1> adtpFromAuthorityListToAuthorityResponseList(List<Authority> authorityListJsonb){
        if(authorityListJsonb != null && !authorityListJsonb.isEmpty()){
            List<AuthorityResponseV1> authorityListResponse= new LinkedList<>();
            for(Authority currentAuthority : authorityListJsonb) {
                authorityListResponse.add(adptFromAuthorityToAuthorityResponse(currentAuthority));
            }
            return authorityListResponse;
        }
        return null;
    }



    private static Boolean isClaimRepair(List<CounterpartyEntity> counterpartyList){
        if(counterpartyList == null || counterpartyList.isEmpty())
            return false;

        Boolean isClaimRepair = false;
        Iterator<CounterpartyEntity> itC = counterpartyList.iterator();
        while(itC.hasNext() && !isClaimRepair){
            CounterpartyEntity counterparty = itC.next();
            if(counterparty.getEligibility() != null && counterparty.getEligibility()){
                isClaimRepair = true;
            }
        }
        return isClaimRepair;
    }


    public AuthorityPaginationResponseV1 adptFromClaimToAuthorityResponse(ClaimsEntity claimsEntity, List<String> authorityLinked, Boolean isFirstCheck){
        if(claimsEntity == null)
            return null;
        AuthorityPaginationResponseV1 authorityPaginationResponse = new AuthorityPaginationResponseV1();
        authorityPaginationResponse.setId(claimsEntity.getId());
        authorityPaginationResponse.setPracticeId(claimsEntity.getPracticeId());
        authorityPaginationResponse.setCreatedAt(DateUtil.convertUTCInstantToIS08601String(claimsEntity.getCreatedAt()));
        authorityPaginationResponse.setDamaged(DamagedAdapter.adptFromDamagedToDamagedAuthorityResponse(claimsEntity.getDamaged()));
        authorityPaginationResponse.setComplaint(ComplaintAdapter.adptFromComplaintToComplaintAuthorityResponse(claimsEntity.getComplaint()));
        authorityPaginationResponse.setAssociable(claimsEntity.getAuthorityLinkable());
        authorityPaginationResponse.setRepair(isClaimRepair(claimsEntity.getCounterparts()));
        authorityPaginationResponse.setType(claimsEntity.getType());
        authorityPaginationResponse.setClaimStatus(claimsEntity.getStatus());
        authorityPaginationResponse.setAuthorityPracticeLinked(authorityLinked);
        if(isFirstCheck){
            authorityPaginationResponse.setSourceType(goLiveStrategyService.goLiveStrategyChecksSystemForWorking(claimsEntity));
        }

        return authorityPaginationResponse;

    }


    public Pagination<AuthorityPaginationResponseV1> adptFromPaginationClaimsToPaginationAuthority(Pagination<ClaimsEntity> claimsPagination, Map<String, List<String>> mapAuthorityLinked, Boolean isFirstCheck){
        Pagination<AuthorityPaginationResponseV1> pagination = new Pagination<>();
        List<AuthorityPaginationResponseV1> listAuthorities = new LinkedList<>();
        for(ClaimsEntity currentClaims : claimsPagination.getItems()){
            listAuthorities.add(this.adptFromClaimToAuthorityResponse(currentClaims, mapAuthorityLinked.get(currentClaims.getId()),isFirstCheck));
        }

        pagination.setItems(listAuthorities);
        pagination.setStats(claimsPagination.getStats());

        return pagination;
    }

    public static String adptWorkingStatusFromEnglishToItalian(WorkingStatusEnum workingStatusEnum){
        if(workingStatusEnum == null){
            return "Nessuno";
        }
        switch (workingStatusEnum){
            case TO_SEND:
                return "da inviare";
            case CLOSED:
                return "chiusa";

            case WORKING:
                return "in lavorazione";

            case APPROVING:
                return "in approvazione";

            case DECLINED:
                return "rifiutata";
            case WAITING_PARTS:
                return "in attesa di pezzi";
            case TO_ESTIMATE:
                return "da stimare";
            case SUPERVISOR_APPROVING:
                return "approvazione del supervisore";
            case SUPERVISOR_REJECTED:
                return "rifiutato dal supervisore";
            case RE_DECLINED:
                return "rifiutato nuovamente";
            case RE_APPROVING:
                return "in re-approvazione";
            case C_FLOW_REJECTED:
                return "c flow rifiutato";
            case C_FLOW_APPROVING:
                return "c flow in approvazione";
            case INTEGRATION_TO_SEND:
                return "integrazione da inviare";
            case WAITING_FOR_WORKING:
                return "in attesa di lavori";
            case WAITING_FOR_UNFREEZE:
                return "in attesa di sblocco";
            case REJECTED:
                return "rifiutato";
            case AUTHORIZED:
                return "autorizzato";
            case TO_CHANNEL:
                return "da canalizzare";
            case PRE_AUTHORITY_REJECTED:
                return "rifiutato pre authority";
            case CLOSED_WITH_REJECTION:
                return "chiuso rifiutato";
            case WAITING_FOR_ACCEPTANCE:
                return "in attesa di accettazione";
            case CLOSED_WITH_LIQUIDATION:
                return "chiuso rifiutato";
            default:
                return "";
        }
    }

    public static AuthorityClaimsResponseV1 adptFromAuthorityToAuthorityClaimsResponse(Authority authorityJsonb){

        if(authorityJsonb != null){
            AuthorityClaimsResponseV1 authority = new AuthorityClaimsResponseV1();
            authority.setAuthorityDossierId(authorityJsonb.getAuthorityDossierId());
            authority.setAuthorityWorkingId(authorityJsonb.getAuthorityWorkingId());
            authority.setTotal(authorityJsonb.getTotal());
            authority.setWorkingNumber(authorityJsonb.getWorkingNumber());
            authority.setAcceptingDate(DateUtil.convertUTCDateToIS08601String(authorityJsonb.getAcceptingDate()));
            authority.setAuthorizationDate(DateUtil.convertUTCDateToIS08601String(authorityJsonb.getAuthorizationDate()));
            authority.setRejectionDate(DateUtil.convertUTCDateToIS08601String(authorityJsonb.getRejectionDate()));
            authority.setRejection(authorityJsonb.getRejection());
            authority.setWorkingCreatedAt(DateUtil.convertUTCDateToIS08601String(authorityJsonb.getWorkingCreatedAt()));
            authority.setNumberFranchise(authorityJsonb.getNumberFranchise());
            authority.setWreck(authorityJsonb.getWreck());
            authority.setWreckCasual(authorityJsonb.getWreckCasual());
            authority.setEventDate(DateUtil.convertUTCDateToIS08601String(authorityJsonb.getEventDate()));
            authority.setEventType(authorityJsonb.getEventType());
            authority.setPoDetails(PODetailsAdapter.adptFromPODetailsToPODetailsResponse(authorityJsonb.getPoDetails()));
            //authority.setStatus(authorityJsonb.getStatus());
            authority.setType(authorityJsonb.getType());
            authority.setNotDuplicate(authorityJsonb.getNotDuplicate());
            authority.setOldest(authorityJsonb.getOldest());
            authority.setAuthorityDossierNumber(authorityJsonb.getAuthorityDossierNumber());
            authority.setCommodityDetails(CommodityDetailsAdapter.adptFromCommodityDetailsToCommodityDetailsResponse(authorityJsonb.getCommodityDetails()));
            authority.setWorkingStatus(authorityJsonb.getWorkingStatus());
            authority.setUserDetails(UserDetailsAdapter.adptFromUserDetailsToUserDetailsResponse(authorityJsonb.getUserDetails()));
            authority.setNbv(authorityJsonb.getNbv());
            authority.setWreckValue(authorityJsonb.getWreckValue());
            authority.setClaimsLinkedSize(authorityJsonb.getClaimsLinkedSize());
            authority.setHistorical(HistoricalAuthorityAdapter.adptHistoricalAuthorityToHistoricalAuthorityResponse(authorityJsonb.getHistorical()));
            authority.setInvoiced(authorityJsonb.getInvoiced());
            authority.setNoteRejection(authorityJsonb.getNoteRejection());
            return authority;
        }
        return null;
    }

    public static CounterpartyAuthorityResponseV1 adptFromCounterpartyToAuthorityRepairResponse(CounterpartyEntity counterpartyEntity){
        if(counterpartyEntity == null) return null;
        CounterpartyAuthorityResponseV1 authorityRepairResponseV1 = new CounterpartyAuthorityResponseV1();
        //authorityRepairResponseV1.setId(counterpartyEntity.getCounterpartyId());
        if(counterpartyEntity.getVehicle() != null){
            authorityRepairResponseV1.setPlate(counterpartyEntity.getVehicle().getLicensePlate());
            authorityRepairResponseV1.setBrand(counterpartyEntity.getVehicle().getModel());
            authorityRepairResponseV1.setModel(counterpartyEntity.getVehicle().getMake());
            authorityRepairResponseV1.setNature(EnumAdapter.adptFromVehicleNatureToVehicleAuthorityNature(counterpartyEntity.getVehicle().getNature()));
        }
        if(counterpartyEntity.getInsured() != null){
            authorityRepairResponseV1.setCounterpartName(counterpartyEntity.getInsured().getLastname()+" "+counterpartyEntity.getInsured().getFirstname());
            authorityRepairResponseV1.setEmail(counterpartyEntity.getInsured().getEmail());
            authorityRepairResponseV1.setPhone(counterpartyEntity.getInsured().getPhone());
        }
        if(Util.isNotEmpty(counterpartyEntity.getInsuranceCompany().getName())){
            authorityRepairResponseV1.setInsuranceCompany(counterpartyEntity.getInsuranceCompany().getName());
        } else {
            authorityRepairResponseV1.setInsuranceCompany(counterpartyEntity.getInsuranceCompany().getEntity().getName());
        }

        return authorityRepairResponseV1;
    }

    public static List<CounterpartyAuthorityResponseV1> adptFromCounterpartyToAuthorityRepairResponse(List<CounterpartyEntity> counterpartyEntity){
        if(counterpartyEntity != null && !counterpartyEntity.isEmpty()){
            List<CounterpartyAuthorityResponseV1> authorityListResponse= new ArrayList<>();
            for(CounterpartyEntity currentAuthority : counterpartyEntity) {
                authorityListResponse.add(adptFromCounterpartyToAuthorityRepairResponse(currentAuthority));
            }
            return authorityListResponse;
        }
        return null;
    }

    public static AuthorityPaginationRepairResponseV1 adptFromCounterpartyToAuthorityResponse(CounterpartyEntity counterpartyEntity, List<String> authorityLinked){
        if(counterpartyEntity == null)
            return null;
        AuthorityPaginationRepairResponseV1 authorityPaginationResponse = new AuthorityPaginationRepairResponseV1();
        authorityPaginationResponse.setId(counterpartyEntity.getCounterpartyId());
        authorityPaginationResponse.setPracticeId(counterpartyEntity.getPracticeIdCounterparty());
        authorityPaginationResponse.setPlate(counterpartyEntity.getVehicle().getLicensePlate());
        authorityPaginationResponse.setCreatedAt(DateUtil.convertUTCInstantToIS08601String(counterpartyEntity.getCreatedAt()));
        authorityPaginationResponse.setClaimPracticeId(counterpartyEntity.getClaims().getPracticeId());
        authorityPaginationResponse.setImpactPoint(ImpactPointAdapter.adptImpactPointToImpactPointResponse(counterpartyEntity.getImpactPoint()));
        authorityPaginationResponse.setStatus(counterpartyEntity.getRepairStatus());
        authorityPaginationResponse.setAuthorityPracticeLinked(authorityLinked);

        return authorityPaginationResponse;

    }

    public static Pagination<AuthorityPaginationRepairResponseV1> adptFromPaginationCounterpartyToPaginationAuthority(Pagination<CounterpartyEntity> counterpartyPagination, Map<String, List<String>> mapAuthorityLinked){
        Pagination<AuthorityPaginationRepairResponseV1> pagination = new Pagination<>();
        List<AuthorityPaginationRepairResponseV1> listAuthorities = new LinkedList<>();
        for(CounterpartyEntity currentCounterparty : counterpartyPagination.getItems()){
            listAuthorities.add(adptFromCounterpartyToAuthorityResponse(currentCounterparty, mapAuthorityLinked.get(currentCounterparty.getCounterpartyId())));
        }

        pagination.setItems(listAuthorities);
        pagination.setStats(counterpartyPagination.getStats());

        return pagination;
    }

    public static AuthorityRepairResponseV1 adptFromAuthorityToAuthorityRepairResponse(Authority authority){
        if(authority == null) return  null;
        AuthorityRepairResponseV1 authorityRepairResponseV1 = new AuthorityRepairResponseV1();
        authorityRepairResponseV1.setWorkingStatus(authority.getWorkingStatus());
        authorityRepairResponseV1.setAcceptingDate(DateUtil.convertUTCDateToIS08601String(authority.getAcceptingDate()));
        authorityRepairResponseV1.setAuthorityDossierId(authority.getAuthorityDossierId());
        authorityRepairResponseV1.setAuthorityDossierNumber(authority.getAuthorityDossierNumber());
        authorityRepairResponseV1.setAuthorityWorkingId(authority.getAuthorityWorkingId());
        authorityRepairResponseV1.setAuthorizationDate(DateUtil.convertUTCDateToIS08601String(authority.getAuthorizationDate()));
        authorityRepairResponseV1.setCommodityDetails(CommodityDetailsAdapter.adptFromCommodityDetailsToCommodityDetailsResponse(authority.getCommodityDetails()));
        authorityRepairResponseV1.setEventDate(DateUtil.convertUTCDateToIS08601String(authority.getEventDate()));
        authorityRepairResponseV1.setEventType(authority.getEventType());
        authorityRepairResponseV1.setHistorical(HistoricalAuthorityAdapter.adptHistoricalAuthorityToHistoricalAuthorityResponse(authority.getHistorical()));
        authorityRepairResponseV1.setNoteRejection(authority.getNoteRejection());
        authorityRepairResponseV1.setRejection(authority.getRejection());
        authorityRepairResponseV1.setRejectionDate(DateUtil.convertUTCDateToIS08601String(authority.getRejectionDate()));
        authorityRepairResponseV1.setTotal(authority.getTotal());
        authorityRepairResponseV1.setType(authority.getType());
        authorityRepairResponseV1.setUserDetails(UserDetailsAdapter.adptFromUserDetailsToUserDetailsResponse(authority.getUserDetails()));
        authorityRepairResponseV1.setWorkingCreatedAt(DateUtil.convertUTCDateToIS08601String(authority.getWorkingCreatedAt()));
        authorityRepairResponseV1.setWorkingNumber(authority.getWorkingNumber());
        authorityRepairResponseV1.setMsaDownloaded(authority.getMsaDownloaded());
        return authorityRepairResponseV1;
    }

    public static List<AuthorityRepairResponseV1> adptFromAuthorityToAuthorityRepairResponse(List<Authority> authorityListJsonb){
        if(authorityListJsonb != null && !authorityListJsonb.isEmpty()){
            List<AuthorityRepairResponseV1> authorityListResponse= new ArrayList<>();
            for(Authority currentAuthority : authorityListJsonb) {
                authorityListResponse.add(adptFromAuthorityToAuthorityRepairResponse(currentAuthority));
            }
            return authorityListResponse;
        }
        return null;
    }

    public static Pagination<AuthorityPracticePaginationResponseV1> adptFromPaginationPracticeToPaginationAuthority(Pagination<PracticeEntity> practiceEntityPagination, Map<String, List<String>> mapAuthorityLinked){
        Pagination<AuthorityPracticePaginationResponseV1> pagination = new Pagination<>();
        List<AuthorityPracticePaginationResponseV1> listAuthorities = new LinkedList<>();
        for(PracticeEntity practiceEntity : practiceEntityPagination.getItems()){
            listAuthorities.add(adptFromPaginationPracticeToPaginationAuthority(practiceEntity, mapAuthorityLinked.get(practiceEntity.getId().toString())));
        }

        pagination.setItems(listAuthorities);
        pagination.setStats(practiceEntityPagination.getStats());

        return pagination;
    }

    public static AuthorityPracticePaginationResponseV1 adptFromPaginationPracticeToPaginationAuthority(PracticeEntity practiceEntity, List<String> authorityLinked){
        if(practiceEntity == null)
            return null;
        AuthorityPracticePaginationResponseV1 authorityPaginationResponse = new AuthorityPracticePaginationResponseV1();
        authorityPaginationResponse.setId(practiceEntity.getId().toString());
        authorityPaginationResponse.setPracticeId(practiceEntity.getPracticeId());
        authorityPaginationResponse.setPracticeType(practiceEntity.getPracticeType());
        authorityPaginationResponse.setCreatedAt(DateUtil.convertUTCInstantToIS08601String(practiceEntity.getCreatedAt()));
        authorityPaginationResponse.setProcessingList(adptFromProcessingToProcessingAuthorityResponse(practiceEntity.getProcessingList()));
        authorityPaginationResponse.setStatus(practiceEntity.getStatus());
        authorityPaginationResponse.setAuthorities(authorityLinked);

        return authorityPaginationResponse;

    }

    private static ProcessingAuthorityResponse adptFromProcessingToProcessingAuthorityResponse(Processing processing){
        if(processing == null) return null;
        ProcessingAuthorityResponse processingAuthorityResponse = new ProcessingAuthorityResponse();
        processingAuthorityResponse.setProcessingType(processing.getProcessingType());
        processingAuthorityResponse.setProcessingAuthorityDate(processing.getProcessingAuthorityDate());
        processingAuthorityResponse.setProcessingDate(processing.getProcessingDate());

        return processingAuthorityResponse;
    }

    private static List<ProcessingAuthorityResponse> adptFromProcessingToProcessingAuthorityResponse(List<Processing> processing){
        if(processing != null && !processing.isEmpty()){
            List<ProcessingAuthorityResponse> authorityListResponse= new ArrayList<>();
            for(Processing currentAuthority : processing) {
                authorityListResponse.add(adptFromProcessingToProcessingAuthorityResponse(currentAuthority));
            }
            return authorityListResponse;
        }
        return null;
    }


    public  ClaimsAuthorityEntity adptFromAuthorityJsonbToClaimsAuthorityEntity(Authority authorityJsonb){

        if(authorityJsonb != null &&
                ((authorityJsonb.getWorkingNumber() != null && authorityJsonb.getAuthorityDossierNumber() != null) ||
                (authorityJsonb.getAuthorityDossierId()!= null && authorityJsonb.getAuthorityDossierId() != null))
        ){
            ClaimsAuthorityEntity claimsAuthorityEntity = authorityRepositorty.searchByWorkingNumberAuthorityDossierNumber(authorityJsonb.getWorkingNumber(),authorityJsonb.getAuthorityDossierNumber());
            if(claimsAuthorityEntity == null){
                //cerco anche con il workingId e il dossierid
               claimsAuthorityEntity = authorityRepositorty.searchByWorkingIdAuthorityDossierId(authorityJsonb.getAuthorityWorkingId(),authorityJsonb.getAuthorityDossierId());

            }


            if(claimsAuthorityEntity == null) {
                //se la lista è ancora vuota creo una nuova entità
                claimsAuthorityEntity = new ClaimsAuthorityEntity();
                claimsAuthorityEntity.setId(UUID.randomUUID().toString());
            }


            claimsAuthorityEntity.setInvoiced(authorityJsonb.getInvoiced());
            claimsAuthorityEntity.setAuthorityDossierId(authorityJsonb.getAuthorityDossierId());
            claimsAuthorityEntity.setAuthorityWorkingId(authorityJsonb.getAuthorityWorkingId());
            claimsAuthorityEntity.setTotal(authorityJsonb.getTotal());
            claimsAuthorityEntity.setWorkingNumber(authorityJsonb.getWorkingNumber());
            claimsAuthorityEntity.setAcceptingDate(DateUtil.convertIS08601StringToUTCInstant(DateUtil.convertUTCDateToIS08601String(authorityJsonb.getAcceptingDate())));
            claimsAuthorityEntity.setAuthorizationDate(DateUtil.convertIS08601StringToUTCInstant(DateUtil.convertUTCDateToIS08601String(authorityJsonb.getAuthorizationDate())));
            claimsAuthorityEntity.setRejectionDate(DateUtil.convertIS08601StringToUTCInstant(DateUtil.convertUTCDateToIS08601String(authorityJsonb.getRejectionDate())));
            claimsAuthorityEntity.setRejection(authorityJsonb.getRejection());
            claimsAuthorityEntity.setWorkingCreatedAt(DateUtil.convertIS08601StringToUTCInstant(DateUtil.convertUTCDateToIS08601String(authorityJsonb.getWorkingCreatedAt())));
            claimsAuthorityEntity.setNumberFranchise(authorityJsonb.getNumberFranchise());
            claimsAuthorityEntity.setWreck(authorityJsonb.getWreck());
            claimsAuthorityEntity.setWreckCasual(authorityJsonb.getWreckCasual());
            claimsAuthorityEntity.setEventDate(DateUtil.convertIS08601StringToUTCInstant(DateUtil.convertUTCDateToIS08601String(authorityJsonb.getEventDate())));
            claimsAuthorityEntity.setEventType(authorityJsonb.getEventType());
            claimsAuthorityEntity.setPoDetails(authorityJsonb.getPoDetails());
            claimsAuthorityEntity.setStatus(authorityJsonb.getStatus());
            claimsAuthorityEntity.setType(authorityJsonb.getType());
            claimsAuthorityEntity.setNotDuplicate(authorityJsonb.getNotDuplicate());
            claimsAuthorityEntity.setOldest(authorityJsonb.getOldest());
            claimsAuthorityEntity.setAuthorityDossierNumber(authorityJsonb.getAuthorityDossierNumber());
            claimsAuthorityEntity.setCommodityDetails(authorityJsonb.getCommodityDetails());
            claimsAuthorityEntity.setWorkingStatus(authorityJsonb.getWorkingStatus());
            if(authorityJsonb.getUserDetails() != null) {
                claimsAuthorityEntity.setUserId(authorityJsonb.getUserDetails().getUserId());
                claimsAuthorityEntity.setFirstname(authorityJsonb.getUserDetails().getFirstname());
                claimsAuthorityEntity.setLastname(authorityJsonb.getUserDetails().getLastname());
            }
            claimsAuthorityEntity.setNbv(authorityJsonb.getNbv());
            claimsAuthorityEntity.setWreckValue(authorityJsonb.getWreckValue());
            claimsAuthorityEntity.setClaimsLinkedSize(authorityJsonb.getClaimsLinkedSize());
            claimsAuthorityEntity.setHistorical(authorityJsonb.getHistorical());
            claimsAuthorityEntity.setInvoiced(authorityJsonb.getInvoiced());
            claimsAuthorityEntity.setNoteRejection(authorityJsonb.getNoteRejection());
            claimsAuthorityEntity.setMigrated(false);
            if(claimsAuthorityEntity.getAuthorityWorkingId()==null && claimsAuthorityEntity.getAuthorityDossierId()==null){
                claimsAuthorityEntity.setMigrated(true);
            }
            return claimsAuthorityEntity;
        }
        return null;
    }

    public  List<ClaimsAuthorityEmbeddedEntity> adtpFromAuthorityListJsonbToClaimsAuthorityListEntity(ClaimsNewEntity claimsNewEntity, List<Authority> authorityListJsonb){

        if(authorityListJsonb != null && !authorityListJsonb.isEmpty()){
            List<ClaimsAuthorityEmbeddedEntity> claimsAuthorityEmbeddedEntities= new LinkedList<>();
            for(Authority currentAuthority : authorityListJsonb) {

                ClaimsAuthorityEntity claimsAuthorityEntity = adptFromAuthorityJsonbToClaimsAuthorityEntity(currentAuthority);

                if(claimsAuthorityEntity != null) {

                    ClaimsAuthorityEmbeddedEntity claimsAuthorityEmbeddedEntity = new ClaimsAuthorityEmbeddedEntity();
                    claimsAuthorityEmbeddedEntity.setId(new ClaimsAuthorityId(claimsNewEntity.getId(), claimsAuthorityEntity.getId()));
                    claimsAuthorityEmbeddedEntity.setFranchiseNumber(currentAuthority.getNumberFranchise());
                    claimsAuthorityEmbeddedEntity.setOldest(currentAuthority.getOldest());
                    claimsAuthorityEmbeddedEntity.setNotDuplicate(currentAuthority.getNotDuplicate());
                    claimsAuthorityEmbeddedEntity.setClaims(claimsNewEntity);
                    claimsAuthorityEmbeddedEntity.setClaimsAuthorityEntity(claimsAuthorityEntity);
                    /*System.out.println(claimsAuthorityEntity.getWorkingNumber());
                    System.out.println(claimsAuthorityEntity.getAuthorityDossierNumber());*/
                    List<ClaimsAuthorityEntity> result = new LinkedList<>();

                    /*List<ClaimsAuthorityEntity> result = claimsAuthorityEntityList.stream()
                            .filter(r -> "working_number".equals(claimsAuthorityEntity.getWorkingNumber()) &&  "authority_dossier_number".equals(claimsAuthorityEntity.getAuthorityDossierNumber()))
                            .collect(Collectors.toList());*/
                    for (ClaimsAuthorityEmbeddedEntity currentEmbeddedAuth : claimsAuthorityEmbeddedEntities ){
                        ClaimsAuthorityEntity currentAuth = currentEmbeddedAuth.getClaimsAuthorityEntity();
                        if( (currentAuth.getWorkingNumber() != null && currentAuth.getAuthorityDossierNumber() != null && currentAuth.getWorkingNumber().equalsIgnoreCase(claimsAuthorityEntity.getWorkingNumber()) && currentAuth.getAuthorityDossierNumber().equalsIgnoreCase(claimsAuthorityEntity.getAuthorityDossierNumber())) ||
                                (currentAuth.getAuthorityWorkingId() != null && currentAuth.getAuthorityDossierId() != null && currentAuth.getAuthorityWorkingId().equalsIgnoreCase(claimsAuthorityEntity.getAuthorityWorkingId()) && currentAuth.getAuthorityDossierId().equalsIgnoreCase(claimsAuthorityEntity.getAuthorityDossierId()))
                        ){
                            result.add(currentAuth);
                        }

                    }

                    //System.out.println("list result " + result);
                    if(result == null || result.isEmpty()) {
                        claimsAuthorityEmbeddedEntities.add(claimsAuthorityEmbeddedEntity);
                    }
                }
            }
            return claimsAuthorityEmbeddedEntities;
        }
        return null;
    }



    public  Authority adptClaimsFromAuthorityToAuthorityJsonb(ClaimsAuthorityEntity authorityEntity){

        if(authorityEntity != null){

            Authority authority = new Authority();

            authority.setInvoiced(authorityEntity.getInvoiced());
            authority.setAuthorityDossierId(authorityEntity.getAuthorityDossierId());
            authority.setAuthorityWorkingId(authorityEntity.getAuthorityWorkingId());
            authority.setTotal(authorityEntity.getTotal());
            authority.setWorkingNumber(authorityEntity.getWorkingNumber());
            authority.setAcceptingDate(DateUtil.convertIS08601StringToUTCDate(DateUtil.convertUTCInstantToIS08601String(authorityEntity.getAcceptingDate())));
            authority.setAuthorizationDate(DateUtil.convertIS08601StringToUTCDate(DateUtil.convertUTCInstantToIS08601String(authorityEntity.getAuthorizationDate())));
            authority.setRejectionDate(DateUtil.convertIS08601StringToUTCDate(DateUtil.convertUTCInstantToIS08601String(authorityEntity.getRejectionDate())));
            authority.setRejection(authorityEntity.getRejection());
            authority.setWorkingCreatedAt(DateUtil.convertIS08601StringToUTCDate(DateUtil.convertUTCInstantToIS08601String(authorityEntity.getWorkingCreatedAt())));
            authority.setNumberFranchise(authorityEntity.getNumberFranchise());
            authority.setWreck(authorityEntity.getWreck());
            authority.setWreckCasual(authorityEntity.getWreckCasual());
            authority.setEventDate(DateUtil.convertIS08601StringToUTCDate(DateUtil.convertUTCInstantToIS08601String(authorityEntity.getEventDate())));
            authority.setEventType(authorityEntity.getEventType());
            authority.setPoDetails(authorityEntity.getPoDetails());
            authority.setStatus(authorityEntity.getStatus());
            authority.setType(authorityEntity.getType());
            authority.setNotDuplicate(authorityEntity.getNotDuplicate());
            authority.setOldest(authorityEntity.getOldest());
            authority.setAuthorityDossierNumber(authorityEntity.getAuthorityDossierNumber());
            authority.setCommodityDetails(authorityEntity.getCommodityDetails());
            authority.setWorkingStatus(authorityEntity.getWorkingStatus());

            if(authorityEntity.getUserId() != null || authorityEntity.getFirstname() != null || authorityEntity.getLastname() != null) {
                UserDetails userDetails = new UserDetails();
                userDetails.setUserId(authorityEntity.getUserId());
                userDetails.setFirstname(authorityEntity.getFirstname());
                userDetails.setLastname(authorityEntity.getLastname());
                authority.setUserDetails(userDetails);
            }
            authority.setNbv(authorityEntity.getNbv());
            authority.setWreckValue(authorityEntity.getWreckValue());
            authority.setClaimsLinkedSize(authorityEntity.getClaimsLinkedSize());
            authority.setHistorical(authorityEntity.getHistorical());
            authority.setInvoiced(authorityEntity.getInvoiced());
            authority.setNoteRejection(authorityEntity.getNoteRejection());


            return authority;
        }
        return null;
    }






    public  List<Authority> adtpClaimsFromAuthorityListToAuthorityListJsonb(List<ClaimsAuthorityEmbeddedEntity> claimsAuthorityEmbeddedEntities){
        if(claimsAuthorityEmbeddedEntities != null && !claimsAuthorityEmbeddedEntities.isEmpty()){
            List<Authority> claimsAuthorityEntityList= new LinkedList<>();
            for(ClaimsAuthorityEmbeddedEntity currentEmbedded : claimsAuthorityEmbeddedEntities) {
                if(currentEmbedded.getClaimsAuthorityEntity() != null) {
                    Authority claimsAuthorityEntity = adptClaimsFromAuthorityToAuthorityJsonb(currentEmbedded.getClaimsAuthorityEntity());
                    claimsAuthorityEntity.setNotDuplicate(currentEmbedded.getNotDuplicate());
                    claimsAuthorityEntity.setOldest(currentEmbedded.getOldest());
                    claimsAuthorityEntity.setNumberFranchise(currentEmbedded.getFranchiseNumber());
                    if (claimsAuthorityEntity != null) {
                        claimsAuthorityEntityList.add(claimsAuthorityEntity);
                    }
                }
            }
            return claimsAuthorityEntityList;
        }
        return null;
    }



}
