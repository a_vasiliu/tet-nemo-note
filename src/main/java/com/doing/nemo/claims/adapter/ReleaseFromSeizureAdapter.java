package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.controller.payload.request.practice.ReleaseFromSeizureRequestV1;
import com.doing.nemo.claims.controller.payload.response.practice.ReleaseFromSeizureResponseV1;
import com.doing.nemo.claims.entity.jsonb.practice.ReleaseFromSeizure;
import org.springframework.stereotype.Component;

@Component
public class ReleaseFromSeizureAdapter {
    
    public static ReleaseFromSeizure adptFromReleaseFromSeizureRequestToReleaseFromSeizure(ReleaseFromSeizureRequestV1 releaseFromSeizureRequest) {
        if(releaseFromSeizureRequest == null)
            return null;
        ReleaseFromSeizure releaseFromSeizure = new ReleaseFromSeizure();
        releaseFromSeizure.setNote(releaseFromSeizureRequest.getNote());
        releaseFromSeizure.setProvider(releaseFromSeizureRequest.getProvider());
        releaseFromSeizure.setProviderCode(releaseFromSeizureRequest.getProviderCode());
        releaseFromSeizure.setSeizureDate(releaseFromSeizureRequest.getSeizureDate());
        releaseFromSeizure.setReleaseFromSeizureDate(releaseFromSeizureRequest.getReleaseFromSeizureDate());
        return releaseFromSeizure;
    }


    public static ReleaseFromSeizureResponseV1 adptFromReleaseFromSeizureToReleaseFromSeizureResponse(ReleaseFromSeizure releaseFromSeizure) {
        if(releaseFromSeizure == null)
            return null;
        ReleaseFromSeizureResponseV1 releaseFromSeizureResponse = new ReleaseFromSeizureResponseV1();
        releaseFromSeizureResponse.setNote(releaseFromSeizure.getNote());
        releaseFromSeizureResponse.setProvider(releaseFromSeizure.getProvider());
        releaseFromSeizureResponse.setProviderCode(releaseFromSeizure.getProviderCode());
        releaseFromSeizureResponse.setSeizureDate(releaseFromSeizure.getSeizureDate());
        releaseFromSeizureResponse.setReleaseFromSeizureDate(releaseFromSeizure.getReleaseFromSeizureDate());
        return releaseFromSeizureResponse;
    }
    
}