package com.doing.nemo.claims.adapter;


import com.doing.nemo.claims.controller.payload.request.AntiTheftServiceRequestV1;
import com.doing.nemo.claims.controller.payload.request.damaged.AntiTheftServiceRequest;
import com.doing.nemo.claims.controller.payload.response.AntiTheftRequestResponseV1;
import com.doing.nemo.claims.controller.payload.response.AntiTheftServiceResponseV1;
import com.doing.nemo.claims.controller.payload.response.PaginationResponseV1;
import com.doing.nemo.claims.entity.AntiTheftRequestEntity;
import com.doing.nemo.claims.entity.enumerated.DTO.PageStats;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbAntiTheftService.AntiTheftService;
import com.doing.nemo.claims.entity.settings.AntiTheftServiceEntity;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

@Component
public class AntiTheftServiceAdapter {

    public static AntiTheftServiceEntity adptFromAntiTheftServiceRequestToAntiTheftServiceEntity(AntiTheftServiceRequestV1 antiTheftServiceRequestV1) {
        AntiTheftServiceEntity antiTheftServiceEntity = new AntiTheftServiceEntity();
        antiTheftServiceEntity.setCodeAntiTheftService(antiTheftServiceRequestV1.getCodeAntiTheftService());
        antiTheftServiceEntity.setName(antiTheftServiceRequestV1.getName());
        antiTheftServiceEntity.setBusinessName(antiTheftServiceRequestV1.getBusinessName());
        antiTheftServiceEntity.setSupplierCode(antiTheftServiceRequestV1.getSupplierCode());
        antiTheftServiceEntity.setEmail(antiTheftServiceRequestV1.getEmail());
        antiTheftServiceEntity.setPhoneNumber(antiTheftServiceRequestV1.getPhoneNumber());
        antiTheftServiceEntity.setProviderType(antiTheftServiceRequestV1.getProviderType());
        antiTheftServiceEntity.setTimeOutInSeconds(antiTheftServiceRequestV1.getTimeOutInSeconds());
        antiTheftServiceEntity.setEndPointUrl(antiTheftServiceRequestV1.getEndPointUrl());
        antiTheftServiceEntity.setUsername(antiTheftServiceRequestV1.getUsername());
        antiTheftServiceEntity.setPassword(antiTheftServiceRequestV1.getPassword());
        antiTheftServiceEntity.setCompanyCode(antiTheftServiceRequestV1.getCompanyCode());
        antiTheftServiceEntity.setActive(antiTheftServiceRequestV1.getActive());
        antiTheftServiceEntity.setIsActive(antiTheftServiceRequestV1.getIsActive());
        antiTheftServiceEntity.setType(antiTheftServiceRequestV1.getType());

        return antiTheftServiceEntity;
    }


    public static AntiTheftServiceResponseV1 adptFromAntiTheftServiceEntityToAntiTheftServiceResponseV1(AntiTheftServiceEntity antiTheftServiceEntity) {
        AntiTheftServiceResponseV1 antiTheftServiceResponseV1 = new AntiTheftServiceResponseV1();
        antiTheftServiceResponseV1.setId(antiTheftServiceEntity.getId());
        antiTheftServiceResponseV1.setCodeAntiTheftService(antiTheftServiceEntity.getCodeAntiTheftService());
        antiTheftServiceResponseV1.setName(antiTheftServiceEntity.getName());
        antiTheftServiceResponseV1.setBusinessName(antiTheftServiceEntity.getBusinessName());
        antiTheftServiceResponseV1.setSupplierCode(antiTheftServiceEntity.getSupplierCode());
        antiTheftServiceResponseV1.setEmail(antiTheftServiceEntity.getEmail());
        antiTheftServiceResponseV1.setPhoneNumber(antiTheftServiceEntity.getPhoneNumber());
        antiTheftServiceResponseV1.setProviderType(antiTheftServiceEntity.getProviderType());
        antiTheftServiceResponseV1.setTimeOutInSeconds(antiTheftServiceEntity.getTimeOutInSeconds());
        antiTheftServiceResponseV1.setEndPointUrl(antiTheftServiceEntity.getEndPointUrl());
        antiTheftServiceResponseV1.setUsername(antiTheftServiceEntity.getUsername());
        antiTheftServiceResponseV1.setPassword(antiTheftServiceEntity.getPassword());
        antiTheftServiceResponseV1.setCompanyCode(antiTheftServiceEntity.getCompanyCode());
        antiTheftServiceResponseV1.setActive(antiTheftServiceEntity.getActive());
        antiTheftServiceResponseV1.setIsActive(antiTheftServiceEntity.getIsActive());
        antiTheftServiceResponseV1.setType(antiTheftServiceEntity.getType());

        return antiTheftServiceResponseV1;
    }

    public static List<AntiTheftServiceResponseV1> adptFromAntiTheftServiceEntityToAntiTheftServiceResponseV1List(List<AntiTheftServiceEntity> antiTheftServiceEntityList) {
        List<AntiTheftServiceResponseV1> antiTheftServiceResponseV1 = new LinkedList<>();
        for (AntiTheftServiceEntity antiTheftServicesEntity : antiTheftServiceEntityList) {
            antiTheftServiceResponseV1.add(adptFromAntiTheftServiceEntityToAntiTheftServiceResponseV1(antiTheftServicesEntity));
        }
        return antiTheftServiceResponseV1;
    }



    public static AntiTheftService adptAntiTheftServiceRequestToAntiTheftService(AntiTheftServiceRequest antiTheftServiceRequest){

        if(antiTheftServiceRequest != null){

            AntiTheftService antiTheftService = new AntiTheftService();
            antiTheftService.setId(antiTheftServiceRequest.getId());
            antiTheftService.setCodeAntiTheftService(antiTheftServiceRequest.getCodeAntiTheftService());
            antiTheftService.setName(antiTheftServiceRequest.getName());
            antiTheftService.setBusinessName(antiTheftServiceRequest.getBusinessName());
            antiTheftService.setSupplierCode(antiTheftServiceRequest.getSupplierCode());
            antiTheftService.setEmail(antiTheftServiceRequest.getEmail());
            antiTheftService.setPhoneNumber(antiTheftServiceRequest.getPhoneNumber());
            antiTheftService.setProviderType(antiTheftServiceRequest.getProviderType());
            antiTheftService.setTimeOutInSeconds(antiTheftServiceRequest.getTimeOutInSeconds());
            antiTheftService.setEndPointUrl(antiTheftServiceRequest.getEndPointUrl());
            antiTheftService.setUsername(antiTheftServiceRequest.getUsername());
            antiTheftService.setPassword(antiTheftServiceRequest.getPassword());
            antiTheftService.setCompanyCode(antiTheftServiceRequest.getCompanyCode());
            antiTheftService.setActive(antiTheftServiceRequest.getActive());
            antiTheftService.setType(antiTheftServiceRequest.getType());


            return antiTheftService;
        }

        return null;

    }

    public static AntiTheftServiceResponseV1 adptAntiTheftServiceToAntiTheftServiceResponse(AntiTheftService antiTheftService, List<AntiTheftRequestEntity> antiTheftRequestEntityList){

           if( antiTheftService != null){

            AntiTheftServiceResponseV1 antiTheftServiceResponseV1 = new AntiTheftServiceResponseV1();
            antiTheftServiceResponseV1.setId(antiTheftService.getId());
            antiTheftServiceResponseV1.setCodeAntiTheftService(antiTheftService.getCodeAntiTheftService());
            antiTheftServiceResponseV1.setName(antiTheftService.getName());
            antiTheftServiceResponseV1.setBusinessName(antiTheftService.getBusinessName());
            antiTheftServiceResponseV1.setSupplierCode(antiTheftService.getSupplierCode());
            antiTheftServiceResponseV1.setEmail(antiTheftService.getEmail());
            antiTheftServiceResponseV1.setPhoneNumber(antiTheftService.getPhoneNumber());
            antiTheftServiceResponseV1.setProviderType(antiTheftService.getProviderType());
            antiTheftServiceResponseV1.setTimeOutInSeconds(antiTheftService.getTimeOutInSeconds());
            antiTheftServiceResponseV1.setEndPointUrl(antiTheftService.getEndPointUrl());
            antiTheftServiceResponseV1.setUsername(antiTheftService.getUsername());
            antiTheftServiceResponseV1.setPassword(antiTheftService.getPassword());
            antiTheftServiceResponseV1.setCompanyCode(antiTheftService.getCompanyCode());
            antiTheftServiceResponseV1.setActive(antiTheftService.getActive());
            antiTheftServiceResponseV1.setIsActive(antiTheftService.getIsActive());
            antiTheftServiceResponseV1.setType(antiTheftService.getType());

            //modificare qui chiamando l'altro adapter
            //antiTheftServiceResponseV1.setAntiTheftRequestResponseV1list(AntiTheftRequestAdapter.adptAntiTheftRequestToAntiTheftRequestResponseV1(antiTheftService.getAntiTheftList()));

            //
               List<AntiTheftRequestResponseV1> originalList = AntiTheftRequestAdapter.adptAntiTheftRequestEntityListToAntiTheftRequestResponseV1List(antiTheftRequestEntityList);
               Collections.sort(originalList);
               antiTheftServiceResponseV1.setAntiTheftRequestResponseV1list(originalList);


             //
            antiTheftServiceResponseV1.setRegistryResponseV1List(RegistryAdapter.adptRegistryToRegistryResponseV1(antiTheftService.getRegistryList()));

            return antiTheftServiceResponseV1;
        }
        return null;

    }


    public static AntiTheftService adptAntiTheftServiceEntityToAntiTheftService(AntiTheftServiceEntity antiTheftServiceEntity){

        if( antiTheftServiceEntity != null){

            AntiTheftService antiTheftService = new AntiTheftService();
            antiTheftService.setId(antiTheftServiceEntity.getId());
            antiTheftService.setCodeAntiTheftService(antiTheftServiceEntity.getCodeAntiTheftService());
            antiTheftService.setName(antiTheftServiceEntity.getName());
            antiTheftService.setBusinessName(antiTheftServiceEntity.getBusinessName());
            antiTheftService.setSupplierCode(antiTheftServiceEntity.getSupplierCode());
            antiTheftService.setEmail(antiTheftServiceEntity.getEmail());
            antiTheftService.setPhoneNumber(antiTheftServiceEntity.getPhoneNumber());
            antiTheftService.setProviderType(antiTheftServiceEntity.getProviderType());
            antiTheftService.setTimeOutInSeconds(antiTheftServiceEntity.getTimeOutInSeconds());
            antiTheftService.setEndPointUrl(antiTheftServiceEntity.getEndPointUrl());
            antiTheftService.setUsername(antiTheftServiceEntity.getUsername());
            antiTheftService.setPassword(antiTheftServiceEntity.getPassword());
            antiTheftService.setCompanyCode(antiTheftServiceEntity.getCompanyCode());
            antiTheftService.setActive(antiTheftServiceEntity.getActive());
            antiTheftService.setIsActive(antiTheftServiceEntity.getIsActive());
            antiTheftService.setType(antiTheftServiceEntity.getType());

            return antiTheftService;
        }
        return new AntiTheftService();

    }

    public static PaginationResponseV1<AntiTheftServiceResponseV1> adptPagination(List<AntiTheftServiceEntity> antiTheftServiceEntityList, int page, int pageSize, Long itemCount) {

        double totPages = Math.ceil((double) itemCount.intValue() / pageSize);
        PageStats pageStats = new PageStats();
        pageStats.setCurrentPage(page);
        pageStats.setItemCount(itemCount.intValue());
        pageStats.setPageSize(pageSize);
        pageStats.setPageCount((long)totPages);

        PaginationResponseV1<AntiTheftServiceResponseV1> paginationResponseV1 = new PaginationResponseV1(pageStats,adptFromAntiTheftServiceEntityToAntiTheftServiceResponseV1List(antiTheftServiceEntityList));

        return paginationResponseV1;
    }


    public static String adptProviderToAntiTheftServiceEntity(String provider){
        String websinProvider = "";
        switch(provider.trim()) {
            case "FM&ED" :
                websinProvider= "OCTO2";
                break;
            case "SEAUT" :
                websinProvider= "OCT10";
                break;
            case "FM&EDP" :
                websinProvider= "OCTO3";
                break;
            case "EVS" :
                websinProvider= "OCTO6";
                break;
            case "RMOTO" :
                websinProvider= "OCTO9";
                break;
            case "SEASY" :
                websinProvider= "OCTO5";
                break;
            case "S&CM" :
                websinProvider= "OCTO8";
                break;
            case "EVSAUT" :
                websinProvider= "OCTO7";
                break;
            case "BASIC" :
                websinProvider= "OCTO1";
                break;
            case "OBD" :
                websinProvider= "OCTO5";
                break;
            case "FM&EDV" :
                websinProvider= "OCT11";
                break;
            case "S&CMP" :
                websinProvider= "OCTO4";
                break;
            case "AVA000" :
                websinProvider= "TEXA";
                break;
            case "AVA001" :
                websinProvider= "TEXA";
                break;
            case "AVA002" :
                websinProvider= "TEXA";
                break;
            case "AVA003" :
                websinProvider= "TEXA";
                break;
            case "AVA004" :
                websinProvider= "TEXA";
                break;
            case "CCS000" :
                websinProvider= "TEXA";
                break;

            default : // Optional
                //lancia eccezione
                // Statements
        }

        return websinProvider;
    }

}
