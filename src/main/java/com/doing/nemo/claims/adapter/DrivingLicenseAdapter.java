package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.controller.payload.request.driver.DrivingLicenseRequest;
import com.doing.nemo.claims.controller.payload.response.driver.DrivingLicenseResponse;
import com.doing.nemo.claims.entity.esb.DriverESB.DrivingLicenseEsb;
import com.doing.nemo.claims.entity.jsonb.damaged.jsonbDamaged.jsonbDriver.DrivingLicense;
import org.springframework.stereotype.Component;

@Component
public class DrivingLicenseAdapter {

    public static DrivingLicense adptDrivingLicenseRequestToDrivingLicense(DrivingLicenseRequest drivingLicenseRequest) {

        if (drivingLicenseRequest != null) {

            DrivingLicense drivingLicense = new DrivingLicense();
            drivingLicense.setCategory(drivingLicenseRequest.getCategory());
            drivingLicense.setIssuingCountry(drivingLicenseRequest.getIssuingCountry());
            drivingLicense.setNumber(drivingLicenseRequest.getNumber());
            drivingLicense.setValidFrom(drivingLicenseRequest.getValidFrom());
            drivingLicense.setValidTo(drivingLicenseRequest.getValidTo());

            return drivingLicense;
        }
        return null;
    }

    public static DrivingLicenseResponse adptDrivingLicenseToDrivingLicenseResponse(DrivingLicense drivingLicense) {

        if (drivingLicense != null) {

            DrivingLicenseResponse drivingLicenseResponse = new DrivingLicenseResponse();
            drivingLicenseResponse.setCategory(drivingLicense.getCategory());
            drivingLicenseResponse.setIssuingCountry(drivingLicense.getIssuingCountry());
            drivingLicenseResponse.setNumber(drivingLicense.getNumber());
            drivingLicenseResponse.setValidFrom(drivingLicense.getValidFrom());
            drivingLicenseResponse.setValidTo(drivingLicense.getValidTo());

            return drivingLicenseResponse;
        }
        return null;
    }

    public static DrivingLicense adptDrivingLicenseResponseToDrivingLicense(DrivingLicenseResponse drivingLicenseResponse) {

        if (drivingLicenseResponse != null) {

            DrivingLicense drivingLicense = new DrivingLicense();
            drivingLicense.setCategory(drivingLicenseResponse.getCategory());
            drivingLicense.setIssuingCountry(drivingLicenseResponse.getIssuingCountry());
            drivingLicense.setNumber(drivingLicenseResponse.getNumber());
            drivingLicense.setValidFrom(drivingLicenseResponse.getValidFrom());
            drivingLicense.setValidTo(drivingLicenseResponse.getValidTo());

            return drivingLicense;
        }
        return null;
    }

    public static DrivingLicenseResponse adptDrivingLicenseEsbToDrivingLicenseResponse(DrivingLicenseEsb drivingLicenseEsb) {

        if (drivingLicenseEsb != null) {

            DrivingLicenseResponse drivingLicenseResponse = new DrivingLicenseResponse();
            drivingLicenseResponse.setIssuingCountry(drivingLicenseEsb.getIssuingCountry());
            drivingLicenseResponse.setNumber(drivingLicenseEsb.getNumber());
            drivingLicenseResponse.setValidFrom(drivingLicenseEsb.getValidFrom());
            drivingLicenseResponse.setValidTo(drivingLicenseEsb.getValidTo());

            return drivingLicenseResponse;
        }
        return null;
    }
}
