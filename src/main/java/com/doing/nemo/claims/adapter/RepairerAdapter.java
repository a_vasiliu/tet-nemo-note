package com.doing.nemo.claims.adapter;

import com.doing.nemo.claims.controller.payload.request.RepairerRequestV1;
import com.doing.nemo.claims.controller.payload.response.ClaimsResponsePaginationV1;
import com.doing.nemo.claims.controller.payload.response.RepairerResponseV1;
import com.doing.nemo.claims.entity.Pagination;
import com.doing.nemo.claims.entity.settings.RepairerEntity;
import com.doing.nemo.claims.exception.NotFoundException;
import com.doing.nemo.claims.repository.RepairerRepository;
import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
public class RepairerAdapter {
    @Autowired
    private RepairerRepository repairerRepository;

    private static Logger LOGGER = LoggerFactory.getLogger(RepairerAdapter.class);

    public RepairerEntity adptFromRepairerRequestToRepairerEntityWithID(RepairerRequestV1 repairerRequestV1, UUID uuid) {
        Optional<RepairerEntity> repairerEntityOptional = repairerRepository.findById(uuid);
        if (!repairerEntityOptional.isPresent()) {
            LOGGER.debug("Insurance Company not found");
            throw new NotFoundException("Insurance Company not found",com.doing.nemo.claims.validation.MessageCode.CLAIMS_1010);
        }
        RepairerEntity repairerEntity = repairerEntityOptional.get();
        RepairerEntity repairerEntity1 = adptFromRepairerRequestToRepairerEntity(repairerRequestV1);
        if(repairerEntity1 != null)
        {
            repairerEntity1.setId(repairerEntity.getId());
        }
        return repairerEntity1;
    }

    public static RepairerEntity adptFromRepairerRequestToRepairerEntity(RepairerRequestV1 repairerRequestV1){
        RepairerEntity repairerEntity = null;
        if(repairerRequestV1 != null)
        {
            repairerEntity = new RepairerEntity();
            repairerEntity.setAddress(repairerRequestV1.getAddress());
            repairerEntity.setBusinessName(repairerRequestV1.getBusinessName());
            repairerEntity.setEmail(repairerRequestV1.getEmail());
            repairerEntity.setFax(repairerRequestV1.getFax());
            repairerEntity.setFiscalCode(repairerRequestV1.getFiscalCode());
            repairerEntity.setLegalRepresentative(repairerRequestV1.getLegalRepresentative());
            repairerEntity.setLocality(repairerRequestV1.getLocality());
            repairerEntity.setPhone(repairerRequestV1.getPhone());
            repairerEntity.setProv(repairerRequestV1.getProv());
            repairerEntity.setReferencePerson(repairerRequestV1.getReferencePerson());
            repairerEntity.setSupplierCode(repairerRequestV1.getSupplierCode());
            repairerEntity.setVatNumber(repairerRequestV1.getVatNumber());
            repairerEntity.setWebSite(repairerRequestV1.getWebSite());
            repairerEntity.setZipCode(repairerRequestV1.getZipCode());
            repairerEntity.setActive(repairerRequestV1.getActive());
        }
        return repairerEntity;
    }

    public static RepairerResponseV1 adptFromRepairerEntityToRepairerResponse(RepairerEntity repairerEntity){
        RepairerResponseV1 repairerResponseV1 = null;
        if(repairerEntity != null)
        {
            repairerResponseV1 = new RepairerResponseV1();
            repairerResponseV1.setId(repairerEntity.getId());
            repairerResponseV1.setAddress(repairerEntity.getAddress());
            repairerResponseV1.setBusinessName(repairerEntity.getBusinessName());
            repairerResponseV1.setEmail(repairerEntity.getEmail());
            repairerResponseV1.setFax(repairerEntity.getFax());
            repairerResponseV1.setFiscalCode(repairerEntity.getFiscalCode());
            repairerResponseV1.setLegalRepresentative(repairerEntity.getLegalRepresentative());
            repairerResponseV1.setLocality(repairerEntity.getLocality());
            repairerResponseV1.setPhone(repairerEntity.getPhone());
            repairerResponseV1.setProv(repairerEntity.getProv());
            repairerResponseV1.setReferencePerson(repairerEntity.getReferencePerson());
            repairerResponseV1.setSupplierCode(repairerEntity.getSupplierCode());
            repairerResponseV1.setVatNumber(repairerEntity.getVatNumber());
            repairerResponseV1.setWebSite(repairerEntity.getWebSite());
            repairerResponseV1.setZipCode(repairerEntity.getZipCode());
            repairerResponseV1.setActive(repairerEntity.getActive());
        }
        return repairerResponseV1;
    }

    public static List<RepairerResponseV1> adptFromRepairerEntityToRepairerResponseList(List<RepairerEntity> repairerEntityList) {
        List<RepairerResponseV1> responseV1 = new LinkedList<>();
        for (RepairerEntity repairerEntity : repairerEntityList) {
            responseV1.add(adptFromRepairerEntityToRepairerResponse(repairerEntity));
        }
        return responseV1;
    }


    public static List<RepairerEntity> adptFromRepairerRequestToRepairerEntityList(List<RepairerRequestV1> repairerRequestV1List) {
        List<RepairerEntity> requestV1 = new LinkedList<>();
        for (RepairerRequestV1 repairerRequestV1 : repairerRequestV1List) {
            requestV1.add(adptFromRepairerRequestToRepairerEntity(repairerRequestV1));
        }
        return requestV1;
    }
    public static ClaimsResponsePaginationV1 adptRepairerPaginationToClaimsResponsePagination(Pagination<RepairerEntity> pagination) {

        ClaimsResponsePaginationV1<RepairerResponseV1> claimsPaginationResponse = new ClaimsResponsePaginationV1<>();

        claimsPaginationResponse.setStats(pagination.getStats());
        claimsPaginationResponse.setItems(adptFromRepairerEntityToRepairerResponseList(pagination.getItems()));

        return claimsPaginationResponse;
    }

}
