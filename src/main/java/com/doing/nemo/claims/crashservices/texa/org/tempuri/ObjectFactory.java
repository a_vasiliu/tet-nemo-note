
package com.doing.nemo.claims.crashservices.texa.org.tempuri;

import com.doing.nemo.claims.crashservices.texa.org.datacontract.schemas._2004._07.texa_tmd_bol_m2mtrack_ald_webservice_service.GetVoucherInfoRequest;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.tempuri package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetVoucherInfoResponseGetVoucherInfoResult_QNAME = new QName("http://tempuri.org/", "GetVoucherInfoResult");
    private final static QName _GetVoucherInfoRequest_QNAME = new QName("http://tempuri.org/", "request");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.tempuri
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetVoucherInfo }
     * 
     */
    public GetVoucherInfo createGetVoucherInfo() {
        return new GetVoucherInfo();
    }

    /**
     * Create an instance of {@link com.doing.nemo.claims.crashservices.texa.org.tempuri.GetVoucherInfoResponse }
     * 
     */
    public com.doing.nemo.claims.crashservices.texa.org.tempuri.GetVoucherInfoResponse createGetVoucherInfoResponse() {
        return new com.doing.nemo.claims.crashservices.texa.org.tempuri.GetVoucherInfoResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link com.doing.nemo.claims.crashservices.texa.org.datacontract.schemas._2004._07.texa_tmd_bol_m2mtrack_ald_webservice_service.GetVoucherInfoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "GetVoucherInfoResult", scope = com.doing.nemo.claims.crashservices.texa.org.tempuri.GetVoucherInfoResponse.class)
    public JAXBElement<com.doing.nemo.claims.crashservices.texa.org.datacontract.schemas._2004._07.texa_tmd_bol_m2mtrack_ald_webservice_service.GetVoucherInfoResponse> createGetVoucherInfoResponseGetVoucherInfoResult(com.doing.nemo.claims.crashservices.texa.org.datacontract.schemas._2004._07.texa_tmd_bol_m2mtrack_ald_webservice_service.GetVoucherInfoResponse value) {
        return new JAXBElement<com.doing.nemo.claims.crashservices.texa.org.datacontract.schemas._2004._07.texa_tmd_bol_m2mtrack_ald_webservice_service.GetVoucherInfoResponse>(_GetVoucherInfoResponseGetVoucherInfoResult_QNAME, com.doing.nemo.claims.crashservices.texa.org.datacontract.schemas._2004._07.texa_tmd_bol_m2mtrack_ald_webservice_service.GetVoucherInfoResponse.class, com.doing.nemo.claims.crashservices.texa.org.tempuri.GetVoucherInfoResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetVoucherInfoRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "request", scope = GetVoucherInfo.class)
    public JAXBElement<GetVoucherInfoRequest> createGetVoucherInfoRequest(GetVoucherInfoRequest value) {
        return new JAXBElement<GetVoucherInfoRequest>(_GetVoucherInfoRequest_QNAME, GetVoucherInfoRequest.class, GetVoucherInfo.class, value);
    }

}
