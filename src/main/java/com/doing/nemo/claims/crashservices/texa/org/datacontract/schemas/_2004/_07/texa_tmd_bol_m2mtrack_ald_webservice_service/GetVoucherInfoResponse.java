
package com.doing.nemo.claims.crashservices.texa.org.datacontract.schemas._2004._07.texa_tmd_bol_m2mtrack_ald_webservice_service;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.*;


/**
 * <p>Classe Java per GetVoucherInfoResponse complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="GetVoucherInfoResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GValue" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="anomaly" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="numberCrash" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="outcome" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pdfReport" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="reportNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="respCode" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="respMsg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetVoucherInfoResponse", propOrder = {
    "gValue",
    "anomaly",
    "numberCrash",
    "outcome",
    "pdfReport",
    "reportNumber",
    "respCode",
    "respMsg"
})
public class GetVoucherInfoResponse {

    @XmlElement(name = "GValue")
    protected Double gValue;
    protected Integer anomaly;
    protected Integer numberCrash;
    @XmlElementRef(name = "outcome", namespace = "http://schemas.datacontract.org/2004/07/Texa.TMD.BOL.M2MTrack.ALD.WebService.Service.Response", type = JAXBElement.class, required = false)
    protected JAXBElement<String> outcome;
    @XmlElementRef(name = "pdfReport", namespace = "http://schemas.datacontract.org/2004/07/Texa.TMD.BOL.M2MTrack.ALD.WebService.Service.Response", type = JAXBElement.class, required = false)
    protected JAXBElement<byte[]> pdfReport;
    protected Integer reportNumber;
    @XmlElementRef(name = "respCode", namespace = "http://schemas.datacontract.org/2004/07/Texa.TMD.BOL.M2MTrack.ALD.WebService.Service.Response", type = JAXBElement.class, required = false)
    protected JAXBElement<Long> respCode;
    @XmlElementRef(name = "respMsg", namespace = "http://schemas.datacontract.org/2004/07/Texa.TMD.BOL.M2MTrack.ALD.WebService.Service.Response", type = JAXBElement.class, required = false)
    protected JAXBElement<String> respMsg;

    /**
     * Recupera il valore della proprietà gValue.
     *
     * @return possible object is
     * {@link Double }
     */
    public Double getGValue() {
        return gValue;
    }

    /**
     * Imposta il valore della proprietà gValue.
     *
     * @param value allowed object is
     *              {@link Double }
     */
    public void setGValue(Double value) {
        this.gValue = value;
    }

    /**
     * Recupera il valore della proprietà anomaly.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getAnomaly() {
        return anomaly;
    }

    /**
     * Imposta il valore della proprietà anomaly.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setAnomaly(Integer value) {
        this.anomaly = value;
    }

    /**
     * Recupera il valore della proprietà numberCrash.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getNumberCrash() {
        return numberCrash;
    }

    /**
     * Imposta il valore della proprietà numberCrash.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setNumberCrash(Integer value) {
        this.numberCrash = value;
    }

    /**
     * Recupera il valore della proprietà outcome.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    public JAXBElement<String> getOutcome() {
        return outcome;
    }

    /**
     * Imposta il valore della proprietà outcome.
     *
     * @param value allowed object is
     *              {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    public void setOutcome(JAXBElement<String> value) {
        this.outcome = value;
    }

    /**
     * Recupera il valore della proprietà pdfReport.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link byte[]}{@code >}
     */
    public JAXBElement<byte[]> getPdfReport() {
        return pdfReport;
    }

    /**
     * Imposta il valore della proprietà pdfReport.
     *
     * @param value allowed object is
     *              {@link JAXBElement }{@code <}{@link byte[]}{@code >}
     */
    public void setPdfReport(JAXBElement<byte[]> value) {
        this.pdfReport = value;
    }

    /**
     * Recupera il valore della proprietà reportNumber.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getReportNumber() {
        return reportNumber;
    }

    /**
     * Imposta il valore della proprietà reportNumber.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setReportNumber(Integer value) {
        this.reportNumber = value;
    }

    /**
     * Recupera il valore della proprietà respCode.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link Long }{@code >}
     */
    public JAXBElement<Long> getRespCode() {
        return respCode;
    }

    /**
     * Imposta il valore della proprietà respCode.
     *
     * @param value allowed object is
     *              {@link JAXBElement }{@code <}{@link Long }{@code >}
     */
    public void setRespCode(JAXBElement<Long> value) {
        this.respCode = value;
    }

    /**
     * Recupera il valore della proprietà respMsg.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    public JAXBElement<String> getRespMsg() {
        return respMsg;
    }

    /**
     * Imposta il valore della proprietà respMsg.
     *
     * @param value allowed object is
     *              {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    public void setRespMsg(JAXBElement<String> value) {
        this.respMsg = value;
    }


    @Override
    public String toString() {

        String string = "GetVoucherInfoResponse{";

        string += "gValue=" + gValue;
        string += ", anomaly=" + anomaly;
        string += ", numberCrash=" + numberCrash;
        if (outcome != null) {
            string += ", outcome=" + outcome.getValue();
        }else{
            string += ", outcome=" + outcome;
        }
        if (pdfReport != null) {
            string += ",pdfReport= content to large";
        }else{
            string += ",pdfReport= null";
        }
            string += ", reportNumber=" + reportNumber;
        if (respCode != null) {
            string += ", respCode=" + respCode.getValue();
        }else{
            string += ", respCode=" + respCode;
        }
        if (respMsg != null) {
            string += ", respMsg=" + respMsg.getValue();
        }else{
            string += ", respMsg=" + respMsg;
        }
        string += '}';

        return string;
    }
}
