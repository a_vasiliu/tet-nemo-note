
package com.doing.nemo.claims.crashservices.texa.org.datacontract.schemas._2004._07.texa_tmd_bol_m2mtrack_ald_webservice_service;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.datacontract.schemas._2004._07.texa_tmd_bol_m2mtrack_ald_webservice_service package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetVoucherInfoRequest_QNAME = new QName("http://schemas.datacontract.org/2004/07/Texa.TMD.BOL.M2MTrack.ALD.WebService.Service.Request", "GetVoucherInfoRequest");
    private final static QName _GetVoucherInfoResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/Texa.TMD.BOL.M2MTrack.ALD.WebService.Service.Response", "GetVoucherInfoResponse");
    private final static QName _GetVoucherInfoResponsePdfReport_QNAME = new QName("http://schemas.datacontract.org/2004/07/Texa.TMD.BOL.M2MTrack.ALD.WebService.Service.Response", "pdfReport");
    private final static QName _GetVoucherInfoResponseRespMsg_QNAME = new QName("http://schemas.datacontract.org/2004/07/Texa.TMD.BOL.M2MTrack.ALD.WebService.Service.Response", "respMsg");
    private final static QName _GetVoucherInfoResponseOutcome_QNAME = new QName("http://schemas.datacontract.org/2004/07/Texa.TMD.BOL.M2MTrack.ALD.WebService.Service.Response", "outcome");
    private final static QName _GetVoucherInfoResponseRespCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Texa.TMD.BOL.M2MTrack.ALD.WebService.Service.Response", "respCode");
    private final static QName _GetVoucherInfoRequestPwd_QNAME = new QName("http://schemas.datacontract.org/2004/07/Texa.TMD.BOL.M2MTrack.ALD.WebService.Service.Request", "pwd");
    private final static QName _GetVoucherInfoRequestUser_QNAME = new QName("http://schemas.datacontract.org/2004/07/Texa.TMD.BOL.M2MTrack.ALD.WebService.Service.Request", "user");
    private final static QName _GetVoucherInfoRequestClaimNumber_QNAME = new QName("http://schemas.datacontract.org/2004/07/Texa.TMD.BOL.M2MTrack.ALD.WebService.Service.Request", "claimNumber");
    private final static QName _GetVoucherInfoRequestPlate_QNAME = new QName("http://schemas.datacontract.org/2004/07/Texa.TMD.BOL.M2MTrack.ALD.WebService.Service.Request", "plate");
    private final static QName _GetVoucherInfoRequestRequestType_QNAME = new QName("http://schemas.datacontract.org/2004/07/Texa.TMD.BOL.M2MTrack.ALD.WebService.Service.Request", "requestType");
    private final static QName _GetVoucherInfoRequestCompanyCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Texa.TMD.BOL.M2MTrack.ALD.WebService.Service.Request", "companyCode");
    private final static QName _GetVoucherInfoRequestDateTime_QNAME = new QName("http://schemas.datacontract.org/2004/07/Texa.TMD.BOL.M2MTrack.ALD.WebService.Service.Request", "dateTime");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.datacontract.schemas._2004._07.texa_tmd_bol_m2mtrack_ald_webservice_service
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetVoucherInfoRequest }
     * 
     */
    public GetVoucherInfoRequest createGetVoucherInfoRequest() {
        return new GetVoucherInfoRequest();
    }

    /**
     * Create an instance of {@link GetVoucherInfoResponse }
     * 
     */
    public GetVoucherInfoResponse createGetVoucherInfoResponse() {
        return new GetVoucherInfoResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetVoucherInfoRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Texa.TMD.BOL.M2MTrack.ALD.WebService.Service.Request", name = "GetVoucherInfoRequest")
    public JAXBElement<GetVoucherInfoRequest> createGetVoucherInfoRequest(GetVoucherInfoRequest value) {
        return new JAXBElement<GetVoucherInfoRequest>(_GetVoucherInfoRequest_QNAME, GetVoucherInfoRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetVoucherInfoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Texa.TMD.BOL.M2MTrack.ALD.WebService.Service.Response", name = "GetVoucherInfoResponse")
    public JAXBElement<GetVoucherInfoResponse> createGetVoucherInfoResponse(GetVoucherInfoResponse value) {
        return new JAXBElement<GetVoucherInfoResponse>(_GetVoucherInfoResponse_QNAME, GetVoucherInfoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Texa.TMD.BOL.M2MTrack.ALD.WebService.Service.Response", name = "pdfReport", scope = GetVoucherInfoResponse.class)
    public JAXBElement<byte[]> createGetVoucherInfoResponsePdfReport(byte[] value) {
        return new JAXBElement<byte[]>(_GetVoucherInfoResponsePdfReport_QNAME, byte[].class, GetVoucherInfoResponse.class, ((byte[]) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Texa.TMD.BOL.M2MTrack.ALD.WebService.Service.Response", name = "respMsg", scope = GetVoucherInfoResponse.class)
    public JAXBElement<String> createGetVoucherInfoResponseRespMsg(String value) {
        return new JAXBElement<String>(_GetVoucherInfoResponseRespMsg_QNAME, String.class, GetVoucherInfoResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Texa.TMD.BOL.M2MTrack.ALD.WebService.Service.Response", name = "outcome", scope = GetVoucherInfoResponse.class)
    public JAXBElement<String> createGetVoucherInfoResponseOutcome(String value) {
        return new JAXBElement<String>(_GetVoucherInfoResponseOutcome_QNAME, String.class, GetVoucherInfoResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Texa.TMD.BOL.M2MTrack.ALD.WebService.Service.Response", name = "respCode", scope = GetVoucherInfoResponse.class)
    public JAXBElement<Long> createGetVoucherInfoResponseRespCode(Long value) {
        return new JAXBElement<Long>(_GetVoucherInfoResponseRespCode_QNAME, Long.class, GetVoucherInfoResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Texa.TMD.BOL.M2MTrack.ALD.WebService.Service.Request", name = "pwd", scope = GetVoucherInfoRequest.class)
    public JAXBElement<String> createGetVoucherInfoRequestPwd(String value) {
        return new JAXBElement<String>(_GetVoucherInfoRequestPwd_QNAME, String.class, GetVoucherInfoRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Texa.TMD.BOL.M2MTrack.ALD.WebService.Service.Request", name = "user", scope = GetVoucherInfoRequest.class)
    public JAXBElement<String> createGetVoucherInfoRequestUser(String value) {
        return new JAXBElement<String>(_GetVoucherInfoRequestUser_QNAME, String.class, GetVoucherInfoRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Texa.TMD.BOL.M2MTrack.ALD.WebService.Service.Request", name = "claimNumber", scope = GetVoucherInfoRequest.class)
    public JAXBElement<String> createGetVoucherInfoRequestClaimNumber(String value) {
        return new JAXBElement<String>(_GetVoucherInfoRequestClaimNumber_QNAME, String.class, GetVoucherInfoRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Texa.TMD.BOL.M2MTrack.ALD.WebService.Service.Request", name = "plate", scope = GetVoucherInfoRequest.class)
    public JAXBElement<String> createGetVoucherInfoRequestPlate(String value) {
        return new JAXBElement<String>(_GetVoucherInfoRequestPlate_QNAME, String.class, GetVoucherInfoRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Texa.TMD.BOL.M2MTrack.ALD.WebService.Service.Request", name = "requestType", scope = GetVoucherInfoRequest.class)
    public JAXBElement<String> createGetVoucherInfoRequestRequestType(String value) {
        return new JAXBElement<String>(_GetVoucherInfoRequestRequestType_QNAME, String.class, GetVoucherInfoRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Texa.TMD.BOL.M2MTrack.ALD.WebService.Service.Request", name = "companyCode", scope = GetVoucherInfoRequest.class)
    public JAXBElement<String> createGetVoucherInfoRequestCompanyCode(String value) {
        return new JAXBElement<String>(_GetVoucherInfoRequestCompanyCode_QNAME, String.class, GetVoucherInfoRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Texa.TMD.BOL.M2MTrack.ALD.WebService.Service.Request", name = "dateTime", scope = GetVoucherInfoRequest.class)
    public JAXBElement<String> createGetVoucherInfoRequestDateTime(String value) {
        return new JAXBElement<String>(_GetVoucherInfoRequestDateTime_QNAME, String.class, GetVoucherInfoRequest.class, value);
    }

}
