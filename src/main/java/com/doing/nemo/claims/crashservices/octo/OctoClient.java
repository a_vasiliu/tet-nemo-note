package com.doing.nemo.claims.crashservices.octo;

import com.doing.nemo.commons.logging.Logger;
import com.doing.nemo.commons.logging.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.xml.soap.*;
import java.io.IOException;

@Service
public class OctoClient {

    @Value("${octo.endpoint}")
    private  String octoEndPoint;

    private static Logger LOGGER = LoggerFactory.getLogger(OctoClient.class);


    public  GetVoucherInfoResponse callGetVoucherInfo(GetVoucherInfoRequest getVoucherInfoRequest) throws SOAPException {
        GetVoucherInfoResponse getVoucherInfoResponse = null;
        try {
            SOAPMessage soapMessage = createSoapEnvelope(getVoucherInfoRequest);
            String soapEndpointUrl = octoEndPoint;
            LOGGER.debug("OCTO Endpoint: " + octoEndPoint);
            String soapAction = "getVoucherInfo";
            SOAPBody soapBody = OctoClient.callSoapWebService(soapEndpointUrl, soapAction, soapMessage);

            getVoucherInfoResponse = new GetVoucherInfoResponse();
            String value;
            value = soapBody.getElementsByTagName("GValue").item(0).getTextContent();
            getVoucherInfoResponse.setgValue(Double.parseDouble(value));
            value = soapBody.getElementsByTagName("anomaly").item(0).getTextContent();
            getVoucherInfoResponse.setAnomaly(Integer.parseInt(value));
            value = soapBody.getElementsByTagName("numberCrash").item(0).getTextContent();
            getVoucherInfoResponse.setNumberCrash(Integer.parseInt(value));
            value = soapBody.getElementsByTagName("outcome").item(0).getTextContent();
            getVoucherInfoResponse.setOutcome(value);
            value = soapBody.getElementsByTagName("pdfReport").item(0).getTextContent();
            getVoucherInfoResponse.setPdfReport(value);
            value = soapBody.getElementsByTagName("reportNumber").item(0).getTextContent();
            getVoucherInfoResponse.setReportNumber(Integer.parseInt(value));
            value = soapBody.getElementsByTagName("respCode").item(0).getTextContent();
            getVoucherInfoResponse.setRespCode(Long.parseLong(value));
            value = soapBody.getElementsByTagName("respMsg").item(0).getTextContent();
            getVoucherInfoResponse.setRespMsg(value);
        } catch (Exception e) {
            throw new SOAPException();
        }

        return getVoucherInfoResponse;
    }


    private static SOAPMessage createSoapEnvelope(GetVoucherInfoRequest getVoucherInfoRequest) throws SOAPException {
        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();
        SOAPPart soapPart = soapMessage.getSOAPPart();

        // SOAP Envelope
        SOAPEnvelope envelope = soapPart.getEnvelope();
        String ns1 = "ns1";
        envelope.addNamespaceDeclaration(ns1, "http://bean.dossiercrash.octo");
        String ns2 = "ns2";
        envelope.addNamespaceDeclaration(ns2, "http://dossiercrash.octo");

        /*
        <?xml version="1.0" encoding="UTF-8"?>
        <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://bean.dossiercrash.octo" xmlns:ns2="http://dossiercrash.octo">
            <SOAP-ENV:Body>
                <ns2:getVoucherInfo>
                    <ns2:request>
                        <ns1:claimNumber>0</ns1:claimNumber>
                        <ns1:companyCode>ALD1</ns1:companyCode>
                        <ns1:dateTime>2019-06-09 21:00:00</ns1:dateTime>
                        <ns1:plate>FH414RB</ns1:plate>
                        <ns1:pwd>tZddsappP32sdk6Jfr</ns1:pwd>
                        <ns1:requestType>c</ns1:requestType>
                        <ns1:user>ald-wscrash01</ns1:user>
                        <ns1:voucher>12815521</ns1:voucher>
                    </ns2:request>
                </ns2:getVoucherInfo>
            </SOAP-ENV:Body>
        </SOAP-ENV:Envelope>
        */

        SOAPBody soapBody = envelope.getBody();
        SOAPElement soapBodyElem = soapBody.addChildElement("getVoucherInfo", ns2);
        SOAPElement soapBodyElem1 = soapBodyElem.addChildElement("request", ns2);
        soapBodyElem1.addChildElement("claimNumber", ns1).addTextNode(getVoucherInfoRequest.getClaimNumber());
        soapBodyElem1.addChildElement("companyCode", ns1).addTextNode(getVoucherInfoRequest.getCompanyCode());
        soapBodyElem1.addChildElement("dateTime", ns1).addTextNode(getVoucherInfoRequest.getDateTime());
        soapBodyElem1.addChildElement("plate", ns1).addTextNode(getVoucherInfoRequest.getPlate());
        soapBodyElem1.addChildElement("pwd", ns1).addTextNode(getVoucherInfoRequest.getPwd());
        soapBodyElem1.addChildElement("requestType", ns1).addTextNode(getVoucherInfoRequest.getRequestType());
        soapBodyElem1.addChildElement("user", ns1).addTextNode(getVoucherInfoRequest.getUser());
        soapBodyElem1.addChildElement("voucher", ns1).addTextNode(getVoucherInfoRequest.getVoucher());

        return soapMessage;
    }

    public static SOAPBody callSoapWebService(String soapEndpointUrl, String soapAction, SOAPMessage soapMessage) throws SOAPException, IOException {
        SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
        SOAPConnection soapConnection = soapConnectionFactory.createConnection();

        SOAPMessage soapResponse = soapConnection.call(createSOAPRequest(soapAction, soapMessage), soapEndpointUrl);

        soapConnection.close();
        return soapResponse.getSOAPBody();
    }

    public static SOAPMessage createSOAPRequest(String soapAction, SOAPMessage soapMessage) throws SOAPException, IOException {
        MimeHeaders headers = soapMessage.getMimeHeaders();
        headers.addHeader("SOAPAction", soapAction);
        soapMessage.saveChanges();

        return soapMessage;
    }

}