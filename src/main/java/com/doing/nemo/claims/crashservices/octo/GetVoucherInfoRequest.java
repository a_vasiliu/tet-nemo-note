
package com.doing.nemo.claims.crashservices.octo;





public class GetVoucherInfoRequest {

    private String claimNumber;
    private String companyCode;
    private String dateTime;
    private String plate;
    private String pwd;
    private String requestType;
    private String user;
    private String voucher;

    public GetVoucherInfoRequest() {
    }

    public String getClaimNumber() {
        return claimNumber;
    }

    public void setClaimNumber(String claimNumber) {
        this.claimNumber = claimNumber;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getVoucher() {
        return voucher;
    }

    public void setVoucher(String voucher) {
        this.voucher = voucher;
    }

    @Override
    public String toString() {
        return "GetVoucherInfoRequest{" +
                "claimNumber='" + claimNumber + '\'' +
                ", companyCode='" + companyCode + '\'' +
                ", dateTime='" + dateTime + '\'' +
                ", plate='" + plate + '\'' +
                ", pwd='" + pwd + '\'' +
                ", requestType='" + requestType + '\'' +
                ", user='" + user + '\'' +
                ", voucher='" + voucher + '\'' +
                '}';
    }
}
