
package com.doing.nemo.claims.crashservices.texa.org.tempuri;

import com.doing.nemo.claims.crashservices.texa.org.datacontract.schemas._2004._07.texa_tmd_bol_m2mtrack_ald_webservice_service.GetVoucherInfoRequest;
import com.doing.nemo.claims.crashservices.texa.org.datacontract.schemas._2004._07.texa_tmd_bol_m2mtrack_ald_webservice_service.GetVoucherInfoResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebService(name = "ICrashService", targetNamespace = "http://tempuri.org/")
@XmlSeeAlso({
        com.doing.nemo.claims.crashservices.texa.com.microsoft.schemas._2003._10.serialization.ObjectFactory.class,
        com.doing.nemo.claims.crashservices.texa.org.datacontract.schemas._2004._07.texa_tmd_bol_m2mtrack_ald_webservice_service.ObjectFactory.class,
        com.doing.nemo.claims.crashservices.texa.org.tempuri.ObjectFactory.class
})
public interface ICrashService {


    /**
     * 
     * @param request
     * @return
     *     returns org.datacontract.schemas._2004._07.texa_tmd_bol_m2mtrack_ald_webservice_service.GetVoucherInfoResponse
     */
    @WebMethod(operationName = "GetVoucherInfo", action = "http://tempuri.org/ICrashService/GetVoucherInfo")
    @WebResult(name = "GetVoucherInfoResult", targetNamespace = "http://tempuri.org/")
    @RequestWrapper(localName = "GetVoucherInfo", targetNamespace = "http://tempuri.org/", className = "com.doing.nemo.claims.crashservices.texa.org.tempuri.GetVoucherInfo")
    @ResponseWrapper(localName = "GetVoucherInfoResponse", targetNamespace = "http://tempuri.org/", className = "com.doing.nemo.claims.crashservices.texa.org.tempuri.GetVoucherInfoResponse")
    public GetVoucherInfoResponse getVoucherInfo(
        @WebParam(name = "request", targetNamespace = "http://tempuri.org/")
        GetVoucherInfoRequest request);

}
