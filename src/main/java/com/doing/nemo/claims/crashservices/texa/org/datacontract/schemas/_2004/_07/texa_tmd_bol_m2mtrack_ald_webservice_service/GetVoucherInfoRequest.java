
package com.doing.nemo.claims.crashservices.texa.org.datacontract.schemas._2004._07.texa_tmd_bol_m2mtrack_ald_webservice_service;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per GetVoucherInfoRequest complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="GetVoucherInfoRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="claimNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="companyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dateTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="plate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pwd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="requestType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="user" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="voucher" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetVoucherInfoRequest", namespace = "http://schemas.datacontract.org/2004/07/Texa.TMD.BOL.M2MTrack.ALD.WebService.Service.Request", propOrder = {
    "claimNumber",
    "companyCode",
    "dateTime",
    "plate",
    "pwd",
    "requestType",
    "user",
    "voucher"
})
public class GetVoucherInfoRequest {

    @XmlElementRef(name = "claimNumber", namespace = "http://schemas.datacontract.org/2004/07/Texa.TMD.BOL.M2MTrack.ALD.WebService.Service.Request", type = JAXBElement.class, required = false)
    protected JAXBElement<String> claimNumber;
    @XmlElementRef(name = "companyCode", namespace = "http://schemas.datacontract.org/2004/07/Texa.TMD.BOL.M2MTrack.ALD.WebService.Service.Request", type = JAXBElement.class, required = false)
    protected JAXBElement<String> companyCode;
    @XmlElementRef(name = "dateTime", namespace = "http://schemas.datacontract.org/2004/07/Texa.TMD.BOL.M2MTrack.ALD.WebService.Service.Request", type = JAXBElement.class, required = false)
    protected JAXBElement<String> dateTime;
    @XmlElementRef(name = "plate", namespace = "http://schemas.datacontract.org/2004/07/Texa.TMD.BOL.M2MTrack.ALD.WebService.Service.Request", type = JAXBElement.class, required = false)
    protected JAXBElement<String> plate;
    @XmlElementRef(name = "pwd", namespace = "http://schemas.datacontract.org/2004/07/Texa.TMD.BOL.M2MTrack.ALD.WebService.Service.Request", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pwd;
    @XmlElementRef(name = "requestType", namespace = "http://schemas.datacontract.org/2004/07/Texa.TMD.BOL.M2MTrack.ALD.WebService.Service.Request", type = JAXBElement.class, required = false)
    protected JAXBElement<String> requestType;
    @XmlElementRef(name = "user", namespace = "http://schemas.datacontract.org/2004/07/Texa.TMD.BOL.M2MTrack.ALD.WebService.Service.Request", type = JAXBElement.class, required = false)
    protected JAXBElement<String> user;
    protected Long voucher;

    /**
     * Recupera il valore della proprietà claimNumber.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getClaimNumber() {
        return claimNumber;
    }

    /**
     * Imposta il valore della proprietà claimNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setClaimNumber(JAXBElement<String> value) {
        this.claimNumber = value;
    }

    /**
     * Recupera il valore della proprietà companyCode.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCompanyCode() {
        return companyCode;
    }

    /**
     * Imposta il valore della proprietà companyCode.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCompanyCode(JAXBElement<String> value) {
        this.companyCode = value;
    }

    /**
     * Recupera il valore della proprietà dateTime.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDateTime() {
        return dateTime;
    }

    /**
     * Imposta il valore della proprietà dateTime.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDateTime(JAXBElement<String> value) {
        this.dateTime = value;
    }

    /**
     * Recupera il valore della proprietà plate.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPlate() {
        return plate;
    }

    /**
     * Imposta il valore della proprietà plate.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPlate(JAXBElement<String> value) {
        this.plate = value;
    }

    /**
     * Recupera il valore della proprietà pwd.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPwd() {
        return pwd;
    }

    /**
     * Imposta il valore della proprietà pwd.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPwd(JAXBElement<String> value) {
        this.pwd = value;
    }

    /**
     * Recupera il valore della proprietà requestType.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRequestType() {
        return requestType;
    }

    /**
     * Imposta il valore della proprietà requestType.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRequestType(JAXBElement<String> value) {
        this.requestType = value;
    }

    /**
     * Recupera il valore della proprietà user.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUser() {
        return user;
    }

    /**
     * Imposta il valore della proprietà user.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUser(JAXBElement<String> value) {
        this.user = value;
    }

    /**
     * Recupera il valore della proprietà voucher.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getVoucher() {
        return voucher;
    }

    /**
     * Imposta il valore della proprietà voucher.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setVoucher(Long value) {
        this.voucher = value;
    }


    @Override
    public String toString() {

        String string =   "GetVoucherInfoRequest{";

        if(claimNumber!= null) {
            string += "claimNumber=" + claimNumber.getValue();
        }else{
            string += "claimNumber=" + claimNumber;
        }
        if(companyCode!= null) {
            string += ", companyCode=" + companyCode.getValue();
        }else{
            string += ", companyCode=" + companyCode;
        }
        if(dateTime!= null) {
            string += ", dateTime=" + dateTime.getValue();
        }else{
            string += ", dateTime=" + dateTime;
        }
        if(plate!= null) {
            string += ", plate=" + plate.getValue();
        }else{
            string += ", plate=" + plate;
        }
        if(pwd!= null) {
            string += ", pwd=" + pwd.getValue();
        }else{
            string += ", pwd=" + pwd;
        }
        if(requestType!= null) {
            string += ", requestType=" + requestType.getValue();
        }else{
            string += ", requestType=" + requestType;
        }
        if(user!= null) {
            string += ", user=" + user.getValue();
        }else{
            string += ", user=" + user;
        }
        string += ", voucher=" + voucher;
        string +='}';

        return string;
    }
}
