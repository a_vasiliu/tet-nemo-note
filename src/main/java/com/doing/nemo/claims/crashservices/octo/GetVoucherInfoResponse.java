
package com.doing.nemo.claims.crashservices.octo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;



public class GetVoucherInfoResponse {

    private Double gValue;
    private Integer anomaly;
    private Integer numberCrash;
    private String outcome;
    private String pdfReport;
    private Integer reportNumber;
    private Long respCode;
    private String respMsg;

    public GetVoucherInfoResponse() {
    }

    public Double getgValue() {
        return gValue;
    }

    public void setgValue(Double gValue) {
        this.gValue = gValue;
    }

    public Integer getAnomaly() {
        return anomaly;
    }

    public void setAnomaly(Integer anomaly) {
        this.anomaly = anomaly;
    }

    public Integer getNumberCrash() {
        return numberCrash;
    }

    public void setNumberCrash(Integer numberCrash) {
        this.numberCrash = numberCrash;
    }

    public String getOutcome() {
        return outcome;
    }

    public void setOutcome(String outcome) {
        this.outcome = outcome;
    }

    public String getPdfReport() {
        return pdfReport;
    }

    public void setPdfReport(String pdfReport) {
        this.pdfReport = pdfReport;
    }

    public Integer getReportNumber() {
        return reportNumber;
    }

    public void setReportNumber(Integer reportNumber) {
        this.reportNumber = reportNumber;
    }

    public Long getRespCode() {
        return respCode;
    }

    public void setRespCode(Long respCode) {
        this.respCode = respCode;
    }

    public String getRespMsg() {
        return respMsg;
    }

    public void setRespMsg(String respMsg) {
        this.respMsg = respMsg;
    }

    @Override
    public String toString() {
        return "GetVoucherInfoResponse{" +
                "gValue=" + gValue +
                ", anomaly=" + anomaly +
                ", numberCrash=" + numberCrash +
                ", outcome='" + outcome + '\'' +
                ", pdfReport='" + pdfReport + '\'' +
                ", reportNumber=" + reportNumber +
                ", respCode=" + respCode +
                ", respMsg='" + respMsg + '\'' +
                '}';
    }
}
